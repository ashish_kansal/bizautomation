﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmPrebuildThreeColumn
    Inherits BACRMUserControl
    Private _objDashboard As Object
    Private _objCustomReport As Object

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID

            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitAmountCompanyWide Then
                imgReportImage.ImageUrl = "~/images/Icon/organization70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitAmountCompanyWide

                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitMarginCompanyWide Then
                imgReportImage.ImageUrl = "~/images/Icon/organization70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitMarginCompanyWide
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitMarginCompanyWide Then
                imgReportImage.ImageUrl = "~/images/Icon/item70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitMarginCompanyWide
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByReturnedVsSold Then
                imgReportImage.ImageUrl = "~/images/Icon/item70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByReturnedVsSold
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CampaignsByROI Then
                imgReportImage.ImageUrl = "~/images/Icon/campaign70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CampaignsByROI
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.LeadSource Then
                imgReportImage.ImageUrl = "~/images/Icon/organization70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.LeadSource
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsWinningDeals Then
                imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsWinningDeals
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsLossingDeals Then
                imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsLossingDeals
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ReasonsForSalesReturns Then
                imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ReasonsForSalesReturns
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.First10SavedSearches Then
                imgReportImage.ImageUrl = "~/images/Icon/search70x70.png"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.First10SavedSearches
                Dim ds As DataSet = objReportManage.GetPrebuildReportData()
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Function

#End Region

#Region "Event Handlers"

    Private Sub rptReport_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim lblValue As Label = DirectCast(e.Item.FindControl("lblValue"), Label)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitMarginCompanyWide Then
                    hplTitle.Text = CCommon.ToString(item("vcCompanyName"))
                    hplTitle.NavigateUrl = ResolveUrl(CCommon.ToString(item("URL")))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("BlendedProfit"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitAmountCompanyWide Then
                    hplTitle.Text = CCommon.ToString(item("vcCompanyName"))
                    hplTitle.NavigateUrl = ResolveUrl(CCommon.ToString(item("URL")))
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("Profit")))
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitMarginCompanyWide Then
                    hplTitle.Text = CCommon.ToString(item("vcItemName"))
                    hplTitle.NavigateUrl = ResolveUrl(CCommon.ToString(item("URL")))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("BlendedProfit"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByReturnedVsSold Then
                    hplTitle.Text = CCommon.ToString(item("vcItemName"))
                    hplTitle.NavigateUrl = ResolveUrl(CCommon.ToString(item("URL")))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("ReturnPercent"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CampaignsByROI Then
                    hplTitle.Text = CCommon.ToString(item("vcCampaignName"))
                    hplTitle.NavigateUrl = ResolveUrl(CCommon.ToString(item("URL")))
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("ROI")))

                    If CCommon.ToDouble(item("ROI")) > 0 Then
                        lblValue.Style.Add("color", "#00a65a")
                    Else
                        lblValue.Style.Add("color", "#dd4b39")
                    End If
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.LeadSource Then
                    lblTitle.Text = CCommon.ToString(item("vcData"))
                    lblTitle.Visible = True
                    hplTitle.Visible = False
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("LeadSourcePercent"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsWinningDeals Then
                    lblTitle.Text = CCommon.ToString(item("Reason"))
                    lblTitle.Visible = True
                    hplTitle.Visible = False
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("TotalDealWonPercent"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsLossingDeals Then
                    lblTitle.Text = CCommon.ToString(item("Reason"))
                    lblTitle.Visible = True
                    hplTitle.Visible = False
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("TotalDealLostPercent"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ReasonsForSalesReturns Then
                    lblTitle.Text = CCommon.ToString(item("Reason"))
                    lblTitle.Visible = True
                    hplTitle.Visible = False
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("TotalReturnPercent"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.First10SavedSearches Then
                    hplTitle.Text = CCommon.ToString(item("vcSearchName"))
                    hplTitle.NavigateUrl = "#"
                    hplTitle.Attributes.Add("onclick", "OpenSearchResult(" & CCommon.ToLong(item("numSearchID")) & "," & CCommon.ToLong(item("numFormID")) & ");")
                    lblValue.Text = ""
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub

#End Region

#Region "Event Handle ExportExcel"
    Protected Sub imgExportExcel_Click(sender As Object, e As ImageClickEventArgs)

        Dim dtTable As DataTable
        dtTable = _objDashboard.GetReptDTL
        lblHeader.Text = dtTable.Rows(0).Item("vcHeader")
        lblFooter.Text = dtTable.Rows(0).Item("vcFooter")

        'Dim dtTable2 As New DataTable
        dtTable.Rows(0).Item("textQueryGrp") = dtTable.Rows(0).Item("textQueryGrp").split("/*sum*/")(0)
        _objCustomReport.DynamicQuery = dtTable.Rows(0).Item("textQueryGrp")
        _objCustomReport.UserCntID = Session("UserContactID")
        _objCustomReport.DomainID = Session("DomainID")
        Dim ds As DataSet
        ds = _objCustomReport.ExecuteDynamicSql()

        Dim fileName As String = "ARAging" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".xlsx"

        Dim objCSVExport As New CSVExport
        objCSVExport.DataSetToExcel(ds, fileName, CCommon.ToLong(Session("DomainID")))

        Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        response.ClearContent()
        response.Clear()
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment; filename=" & fileName & ";")
        response.TransmitFile(BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session("DomainID"))) & fileName)
        response.Flush()

        Try
            System.IO.File.Delete(BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session("DomainID"))) & fileName)
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try

        response.Redirect("../ReportDashboard/frmNewDashBoard.aspx")
    End Sub

#End Region

End Class