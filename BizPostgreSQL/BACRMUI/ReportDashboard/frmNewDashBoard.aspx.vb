﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports System.Web.Services
Imports System.Xml
Imports Newtonsoft.Json
Imports System.Collections.Generic

Public Class frmNewDashBoard1
    Inherits BACRMPage

#Region "Member Variables"
    Dim objReportManage As New CustomReportsManage
    Private _objDashboard As Object
    Private _objCustomReport As Object
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                LoadDropdowns()
                LoadDashBoard(If(GetQueryStringVal("IsRefresh") = "1", True, False))


                'KEEP PERMISSION CHECK AT LAST
                Dim m_aryRightsForCreateTemplate() As Integer
                m_aryRightsForCreateTemplate = GetUserRightsForPage_Other(MODULEID.Dashboard, 1)
                If m_aryRightsForCreateTemplate(RIGHTSTYPE.VIEW) = 0 Then
                    btnCreateTemplate.Visible = False
                End If

                Dim m_aryRightsForUpdateTemplate() As Integer
                m_aryRightsForUpdateTemplate = GetUserRightsForPage_Other(MODULEID.Dashboard, 6)
                If m_aryRightsForUpdateTemplate(RIGHTSTYPE.VIEW) = 0 Then
                    btnUpdateTemplate.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub LoadDashBoard(ByVal IsRefresh As Boolean)
        Try
            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage

            objReportManage.DomainID = Session("DomainID")
            objReportManage.UserCntID = Session("UserContactID")
            Dim ds As DataSet
            ds = objReportManage.GetReportDashBoard(-1)


            If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                btnUpdateTemplate.Visible = True
                ddlDashboardTemplate.SelectedValue = CCommon.ToString(ds.Tables(2).Rows(0)("numTemplateID"))
                hdnDashboardTemplateID.Value = CCommon.ToLong(ds.Tables(2).Rows(0)("numTemplateID"))
            Else
                btnUpdateTemplate.Visible = False
                lblTemplateName.Text = "Dashboard template is not assigned"
                lblTemplateName.ForeColor = Color.Red
                hdnDashboardTemplateID.Value = 0
            End If

            Dim dtTable1 As DataTable = ds.Tables(1)
            If dtTable1.Rows.Count > 0 Then
                For i = 0 To dtTable1.Rows.Count - 1
                    Dim ReptControl As New Control

                    If CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.AR Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.AP Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.MoneyInBank Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.AccountingReports Then
                        ReptControl = LoadControl("frmPrebuildAccountingReports.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitAmountCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildChartReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ItemsByRevenueSoldCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildChartReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.ProfitMarginByItemClassificationCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildChartReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitMarginCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitAmountCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.ActionItemsAndMeetingsCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.RPEYTDvsSamePeriodLastYear Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.RPELastMonthvsSamePeriodLastYear Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.SalesVsExpense Then
                        ReptControl = LoadControl("frmPrebuildChartReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopSourcesOfSalesOrders Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitMarginCompanyWide Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ItemsByReturnedVsSold Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10CampaignsByROI Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopCustomersByPortionOfTotalSales Then
                        ReptControl = LoadControl("frmPrebuildChartReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.KPI Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.LeadSource Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10Email Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Reminders Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopReasonsWinningDeals Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopReasonsLossingDeals Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ReasonsForSalesReturns Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.First10SavedSearches Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso (CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.SalesOppWonLost) Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso (CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.SalesOppPipeLine) Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso (CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.BackOrderValuation) Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso (CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.MonthToDateRevenue) Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso (CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.InventoryValueByWareHouse) Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf CCommon.ToBool(dtTable1.Rows(i).Item("bitDefault")) AndAlso (CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.PendingReportDetails) Then
                        ReptControl = LoadControl("frmPrebuildHtmlReport.ascx")
                    ElseIf dtTable1.Rows(i).Item("tintReportType") = 1 Then
                        ReptControl = LoadControl("frmReptTable.ascx")
                    ElseIf dtTable1.Rows(i).Item("tintReportType") = 2 Then
                        If dtTable1.Rows(i).Item("tintChartType") = 1 Then
                            ReptControl = LoadControl("frmReptChart.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 2 Then
                            ReptControl = LoadControl("frmReptBar.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 3 Then
                            ReptControl = LoadControl("frmReptPyramid.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 4 Then
                            ReptControl = LoadControl("frmRept3dPyramid.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 5 Then
                            ReptControl = LoadControl("frmReptFunnel.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 6 Then
                            ReptControl = LoadControl("frmRept3DFunnel.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 7 Then
                            ReptControl = LoadControl("frmRept3DCone.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 8 Then
                            ReptControl = LoadControl("frmReptLine.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 12 Then
                            ReptControl = LoadControl("frmReptPie.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 13 Then
                            ReptControl = LoadControl("frmRept3DPie.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 14 Then
                            ReptControl = LoadControl("frmReptDou.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 15 Then
                            ReptControl = LoadControl("frmRept3DDough.ascx")
                        Else : ReptControl = LoadControl("frmReptChart.ascx")
                        End If
                    End If

                    Dim newLi As New HtmlGenericControl
                    newLi.TagName = "li"
                    newLi.Attributes.Add("class", "layout_block")
                    newLi.Attributes.Add("data-id", dtTable1.Rows(i).Item("numDashBoardID"))
                    newLi.Attributes.Add("data-row", dtTable1.Rows(i).Item("tintRow"))
                    newLi.Attributes.Add("data-col", dtTable1.Rows(i).Item("tintColumn"))
                    newLi.Attributes.Add("data-sizex", dtTable1.Rows(i).Item("intWidth"))
                    newLi.Attributes.Add("data-sizey", dtTable1.Rows(i).Item("intHeight"))
                    newLi.Controls.Add(ReptControl)

                    phColumn1.Controls.Add(newLi)

                    Dim _myControlType As Type = ReptControl.GetType()

                    Dim _DashBoardID As PropertyInfo = _myControlType.GetProperty("DashBoardID")
                    _DashBoardID.SetValue(ReptControl, CLng(dtTable1.Rows(i).Item("numDashBoardID")), Nothing)

                    Dim _Edit As PropertyInfo = _myControlType.GetProperty("EditRept")
                    _Edit.SetValue(ReptControl, True, Nothing)

                    Dim SystemName As PropertyInfo = _myControlType.GetProperty("DefaultReportID")
                    If Not SystemName Is Nothing Then
                        SystemName.SetValue(ReptControl, CCommon.ToInteger(dtTable1.Rows(i).Item("intDefaultReportID")), Nothing)
                    End If

                    Dim piRefresh As PropertyInfo = _myControlType.GetProperty("IsRefresh")
                    If Not piRefresh Is Nothing Then
                        piRefresh.SetValue(ReptControl, IsRefresh, Nothing)
                    End If

                    Dim _Width As PropertyInfo = _myControlType.GetProperty("Width")
                    _Width.SetValue(ReptControl, 200, Nothing)

                    Dim _objReportManage As PropertyInfo = _myControlType.GetProperty("objReportManage")
                    _objReportManage.SetValue(ReptControl, objReportManage, Nothing)

                    Dim _myfunction As System.Reflection.MethodInfo = _myControlType.GetMethod("CreateReport")
                    _myfunction.Invoke(ReptControl, Nothing)
                Next
            Else

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDropdowns()
        Try
            Dim objDashboardTemplate As New BACRM.BusinessLogic.CustomReports.DashboardTemplate
            objDashboardTemplate.DomainID = CCommon.ToLong(Session("DomainID"))
            objDashboardTemplate.UserCntID = CCommon.ToLong(Session("UserContactID"))
            ddlDashboardTemplate.DataSource = objDashboardTemplate.GetAllByDomain()
            ddlDashboardTemplate.DataTextField = "vcTemplateName"
            ddlDashboardTemplate.DataValueField = "numTemplateID"
            ddlDashboardTemplate.DataBind()
            ddlDashboardTemplate.Items.Insert(0, New ListItem("--Select One--", "0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Public Methods"
    <WebMethod(EnableSession:=True)>
    Public Shared Function UpdateOrder(ByVal positions As String) As String
        Try
            Dim objReport As List(Of PositionObject)

            objReport = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of PositionObject))(positions)

            Dim dt As New DataTable
            dt.Columns.Add("numDashBoardID")
            dt.Columns.Add("tintColumn")
            dt.Columns.Add("tintRow")
            dt.Columns.Add("intWidth")
            dt.Columns.Add("intHeight")

            Dim dr As DataRow

            For Each objPosition As PositionObject In objReport
                dr = dt.NewRow()

                dr("numDashBoardID") = objPosition.id
                dr("tintColumn") = objPosition.col
                dr("tintRow") = objPosition.row
                dr("intWidth") = objPosition.width
                dr("intHeight") = objPosition.height

                dt.Rows.Add(dr)
            Next

            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim objReportManage As New CustomReportsManage
            objReportManage.strText = ds.GetXml
            objReportManage.DomainID = HttpContext.Current.Session("DomainID")

            Return objReportManage.ManageDashboardOpe("A")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

    Public Class PositionObject

        Private _col As String
        Public Property col() As String
            Get
                Return _col
            End Get
            Set(ByVal value As String)
                _col = value
            End Set
        End Property

        Private _row As String
        Public Property row() As String
            Get
                Return _row
            End Get
            Set(ByVal value As String)
                _row = value
            End Set
        End Property

        Private _width As String
        Public Property width() As String
            Get
                Return _width
            End Get
            Set(ByVal value As String)
                _width = value
            End Set
        End Property

        Private _height As String
        Public Property height() As String
            Get
                Return _height
            End Get
            Set(ByVal value As String)
                _height = value
            End Set
        End Property

        Private _id As String
        Public Property id() As String
            Get
                Return _id
            End Get
            Set(ByVal value As String)
                _id = value
            End Set
        End Property

    End Class

    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs)
        Try
            Response.Redirect("~/ReportDashboard/frmNewDashBoard.aspx?IsRefresh=1", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub ddlDashboardTemplate_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
            objUserAccess.UserId = CCommon.ToLong(Session("UserContactID"))

            Dim dashboardTemplateID As String = ""

            If ddlDashboardTemplate.SelectedValue <> "" Then
                dashboardTemplateID = ddlDashboardTemplate.SelectedValue
            End If

            objUserAccess.UpdateDefaultDashboardTemplate(CCommon.ToLong(dashboardTemplateID))
            Response.Redirect("~/ReportDashboard/frmNewDashBoard.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class