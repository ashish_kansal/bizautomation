﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmPrebuildSalesOpportunityWonLost
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID

            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If

            imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

            objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
            objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.SalesOppWonLost
            objReportManage.DashBoardID = DashBoardID
            Dim ds As DataSet
            If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & DashBoardID) Is Nothing Then
                ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & DashBoardID), DataSet)
            Else
                ds = objReportManage.GetPrebuildReportData()
                Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
            End If

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                rptReport.DataSource = ds.Tables(0)
                rptReport.DataBind()
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Function

#End Region

End Class