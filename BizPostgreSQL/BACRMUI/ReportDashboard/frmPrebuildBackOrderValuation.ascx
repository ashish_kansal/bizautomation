﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildBackOrderValuation.ascx.vb"
    Inherits=".frmPrebuildBackOrderValuation" %>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent">
         <div class="row" id="divReportContent" runat="server" style="margin-right:0px;margin-left:0px; display: flex; align-items: center;">
    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12" style="padding-left: 5px;padding-right: 5px;">
                <asp:Repeater ID="rptReport" runat="server">
                    <HeaderTemplate>
                        <div class="row" style="margin-right:0px;margin-left:0px">
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-left">
                                <asp:Label ID="lblHeader1" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-center">
                                <asp:Label ID="lblHeader2" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-right">
                                <asp:Label ID="lblHeader3" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                            <%--<div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-right">
                                <asp:Label ID="lblHeader4" runat="server" Text="" Font-Bold="true" Visible="false" ></asp:Label>
                            </div>--%>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row" style="margin-right:0px;margin-left:0px">
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-left">
                                <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-center">
                                <asp:Label ID="lblValue1" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-right" >
                                <asp:Label ID="lblValue2" runat="server" Text="" ForeColor="Green"></asp:Label>
                                <asp:HyperLink ID="hplValue2" runat="server" Target="_blank" ForeColor="Green"></asp:HyperLink>
                                <asp:Label ID="lblValue3" runat="server" Text=""  ForeColor="red" ></asp:Label>
                                <asp:HyperLink ID="hplValue3" runat="server" Target="_blank" ForeColor="red"></asp:HyperLink>
                            </div>
                            <%--<div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-right"  style="color:red"  >
                                
                                
                            </div>--%>
                            <%--<div class="col-xs-4 col-md-4 col-sm-4  col-lg-4 text-right">
                                <asp:Label ID="lblValue4" runat="server" Text="" ></asp:Label>
                            </div>--%>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="row" style="margin-right: 0px; margin-left: 0px">
                        <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 text-left">
                        <asp:Label ID="lblLabel" runat="server" Font-Bold="true" />
                        </div>
                        <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 text-center">
                        <asp:Label ID="lblTotal" runat="server"  Font-Bold="true"/>
                        </div>
                        <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 text-right">
                        <asp:Label ID="lblGrandTotal" runat="server" Font-Bold="true" ForeColor="Green"/>
                       <asp:HyperLink ID="hplGrandTotal" runat="server" Target="_blank" ForeColor="Green" Font-Bold="true"></asp:HyperLink>
                        <asp:Label ID="lblGrandTotal1" runat="server" Font-Bold="true" ForeColor="Red"/>
                       <asp:HyperLink ID="hplGrandTotal1" runat="server" Target="_blank" ForeColor="red" Font-Bold="true"></asp:HyperLink>     
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div> 
             </div>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
