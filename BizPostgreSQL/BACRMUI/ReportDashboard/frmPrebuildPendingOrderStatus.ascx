﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildPendingOrderStatus.ascx.vb" Inherits=".frmPrebuildPendingOrderStatus" %>
  <script type="text/javascript" src="../JavaScript/MassPurchaseFulfillment.js"></script> 

<div class="box box-primary">
<div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent">
        <table id="divReportContent" runat="server" style="width:100%">
        <tr>
            <td runat="server" id="rpt1" style="vertical-align:top;padding-right: 5px;">
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader1" runat="server" Text="" Style="white-space: nowrap;" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport1" runat="server">
                                    <HeaderTemplate>
                                        <table style="width:100%;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:13px;text-align:center;" >
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap"  Font-Size="25px" ForeColor="Purple" ></asp:Label>
                                                <asp:HyperLink ID="hplValue" runat="server" Target="_blank"   Font-Size="25px" ForeColor="Purple"></asp:HyperLink>
                                            </td>
                                            <%--<td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
            <td runat="server" id="rpt2" style="vertical-align:top;padding-right: 5px;">
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader2" runat="server" Text="" Style="white-space: nowrap" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport2" runat="server">
                                    <HeaderTemplate>
                                        <table style="width:100%;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:13px;text-align:center;">
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap"  Font-Size="25px" ForeColor="Blue"></asp:Label>
                                                <asp:HyperLink ID="hplValue" runat="server" Target="_blank" Font-Size="25px" ForeColor="Blue"></asp:HyperLink>
                                            </td>
                                            <%--<td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
            <td runat="server" id="rpt3" style="vertical-align:top;padding-right: 5px;">
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader3" runat="server" Text="" Style="white-space: nowrap" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport3" runat="server">
                                    <HeaderTemplate>
                                        <table style="width:100%;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:13px;text-align:center;">
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap" Font-Size="25px" ForeColor="Green"></asp:Label>
                                                 <asp:HyperLink ID="hplValue" runat="server" Target="_blank" Font-Size="25px" ForeColor="Green"></asp:HyperLink>
                                            </td>
                                            <%--<td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
        
        
        </tr>
        </table>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>