﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports System.Web.Services

Public Class frmNewDashBoard
    Inherits BACRMPage

    Dim objReportManage As New CustomReportsManage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadDashBoard()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDashBoard()
        Try
            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage

            objReportManage.DomainID = Session("DomainID")
            objReportManage.UserCntID = Session("UserContactID")
            objReportManage.GroupId = Session("UserGroupID")
            Dim ds As DataSet
            ds = objReportManage.GetReportDashBoard

            Dim dtTable, dtTable1 As DataTable
            dtTable = ds.Tables(0)
            Dim i As Integer
            If dtTable.Rows.Count = 3 Then
                If dtTable.Rows(0).Item("tintColumn") = 1 Then
                    If dtTable.Rows(0).Item("tintSize") = 1 Then
                        tblRept1.Width = "300"
                    ElseIf dtTable.Rows(0).Item("tintSize") = 2 Then
                        tblRept1.Width = "400"
                    ElseIf dtTable.Rows(0).Item("tintSize") = 3 Then
                        tblRept1.Width = "500"
                    Else : tblRept1.Width = "300"
                    End If
                End If
                If dtTable.Rows(1).Item("tintColumn") = 2 Then
                    If dtTable.Rows(1).Item("tintSize") = 1 Then
                        tblRept2.Width = "300"
                    ElseIf dtTable.Rows(1).Item("tintSize") = 2 Then
                        tblRept2.Width = "400"
                    ElseIf dtTable.Rows(1).Item("tintSize") = 3 Then
                        tblRept2.Width = "500"
                    Else : tblRept2.Width = "300"
                    End If
                End If
                If dtTable.Rows(2).Item("tintColumn") = 3 Then
                    If dtTable.Rows(2).Item("tintSize") = 1 Then
                        tblRept3.Width = "300"
                    ElseIf dtTable.Rows(2).Item("tintSize") = 2 Then
                        tblRept3.Width = "400"
                    ElseIf dtTable.Rows(2).Item("tintSize") = 3 Then
                        tblRept3.Width = "500"
                    Else : tblRept3.Width = "300"
                    End If
                End If
            Else
                tblRept1.Width = "300"
                tblRept2.Width = "300"
                tblRept3.Width = "300"
            End If


            dtTable1 = ds.Tables(1)
            Dim tr As HtmlTableRow
            Dim td As HtmlTableCell
            If dtTable1.Rows.Count > 0 Then
                For i = 0 To dtTable1.Rows.Count - 1
                    Dim ReptControl As New Control

                    If dtTable1.Rows(i).Item("tintReportType") = 1 Then
                        ReptControl = LoadControl("frmReptTable.ascx")
                    ElseIf dtTable1.Rows(i).Item("tintReportType") = 2 Then
                        If dtTable1.Rows(i).Item("tintChartType") = 1 Then
                            ReptControl = LoadControl("frmReptChart.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 2 Then
                            ReptControl = LoadControl("frmReptBar.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 3 Then
                            ReptControl = LoadControl("frmReptPyramid.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 4 Then
                            ReptControl = LoadControl("frmRept3dPyramid.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 5 Then
                            ReptControl = LoadControl("frmReptFunnel.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 6 Then
                            ReptControl = LoadControl("frmRept3DFunnel.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 7 Then
                            ReptControl = LoadControl("frmRept3DCone.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 8 Then
                            ReptControl = LoadControl("frmReptLine.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 12 Then
                            ReptControl = LoadControl("frmReptPie.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 13 Then
                            ReptControl = LoadControl("frmRept3DPie.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 14 Then
                            ReptControl = LoadControl("frmReptDou.ascx")
                        ElseIf dtTable1.Rows(i).Item("tintChartType") = 15 Then
                            ReptControl = LoadControl("frmRept3DDough.ascx")
                        Else : ReptControl = LoadControl("frmReptChart.ascx")
                        End If
                    End If

                    tr = New HtmlTableRow
                    td = New HtmlTableCell
                    td.ColSpan = 3

                    Dim Width As Integer
                    If dtTable1.Rows(i).Item("tintColumn") = 1 Then
                        phColumn1.Controls.Add(ReptControl)
                        If dtTable.Rows.Count > 0 Then
                            If dtTable.Rows(0).Item("tintSize") = 1 Then
                                Width = 300
                            ElseIf dtTable.Rows(0).Item("tintSize") = 2 Then
                                Width = 400
                            ElseIf dtTable.Rows(0).Item("tintSize") = 3 Then
                                Width = 500
                            End If
                        Else
                            Width = 300
                        End If
                    ElseIf dtTable1.Rows(i).Item("tintColumn") = 2 Then
                        phColumn2.Controls.Add(ReptControl)
                        If dtTable.Rows.Count > 0 Then
                            If dtTable.Rows(1).Item("tintSize") = 1 Then
                                Width = 300
                            ElseIf dtTable.Rows(1).Item("tintSize") = 2 Then
                                Width = 400
                            ElseIf dtTable.Rows(1).Item("tintSize") = 3 Then
                                Width = 500
                            End If
                        Else
                            Width = 300
                        End If
                    ElseIf dtTable1.Rows(i).Item("tintColumn") = 3 Then
                        phColumn3.Controls.Add(ReptControl)
                        If dtTable.Rows.Count > 0 Then
                            If dtTable.Rows(2).Item("tintSize") = 1 Then
                                Width = 300
                            ElseIf dtTable.Rows(2).Item("tintSize") = 2 Then
                                Width = 400
                            ElseIf dtTable.Rows(2).Item("tintSize") = 3 Then
                                Width = 500
                            End If
                        Else
                            Width = 300
                        End If
                    End If

                    Dim _myControlType As Type = ReptControl.GetType()

                    Dim _DashBoardID As PropertyInfo = _myControlType.GetProperty("DashBoardID")
                    _DashBoardID.SetValue(ReptControl, CLng(dtTable1.Rows(i).Item("numDashBoardID")), Nothing)
                    'If txtEdit.Text = "1" Then
                    Dim _Edit As PropertyInfo = _myControlType.GetProperty("EditRept")
                    _Edit.SetValue(ReptControl, True, Nothing)
                    'End If

                    Dim _Width As PropertyInfo = _myControlType.GetProperty("Width")
                    _Width.SetValue(ReptControl, Width, Nothing)

                    Dim _objReportManage As PropertyInfo = _myControlType.GetProperty("objReportManage")
                    _objReportManage.SetValue(ReptControl, objReportManage, Nothing)

                    Dim _myfunction As System.Reflection.MethodInfo = _myControlType.GetMethod("CreateReport")
                    _myfunction.Invoke(ReptControl, Nothing)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            td1.Visible = True
            td3.Visible = True
            td5.Visible = True
            LoadDashBoard()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNarrow1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNarrow1.Click
        Try
            ManageDashboardSize(1, 1)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNarrow2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNarrow2.Click
        Try
            ManageDashboardSize(2, 1)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNarrow3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNarrow3.Click
        Try
            ManageDashboardSize(3, 1)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkMedium1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedium1.Click
        Try
            ManageDashboardSize(1, 2)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkMedium2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedium2.Click
        Try
            ManageDashboardSize(2, 2)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkMedium3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedium3.Click
        Try
            ManageDashboardSize(3, 2)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkWide1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWide1.Click
        Try
            ManageDashboardSize(1, 3)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkWide2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWide2.Click
        Try
            ManageDashboardSize(2, 3)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkWide3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWide3.Click
        Try
            ManageDashboardSize(3, 3)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub ManageDashboardSize(tintColumn As Short, tintSize As Short)
        Try
            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage

            objReportManage.DomainID = Session("DomainID")
            objReportManage.UserCntID = Session("UserContactID")

            objReportManage.ManageReportDashboardSize(tintColumn, tintSize)
            LoadDashBoard()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    <WebMethod(EnableSession:=True)>
    Public Shared Function UpdateOrder(ByVal Column1Values As String, ByVal Column2Values As String, ByVal Column3Values As String) As String
        Try
            Dim dt As New DataTable
            dt.Columns.Add("numDashBoardID")
            dt.Columns.Add("tintColumn")
            dt.Columns.Add("tintRow")

            Dim strCol1() As String = Column1Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim strCol2() As String = Column2Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim strCol3() As String = Column3Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim dr As DataRow

            For i As Integer = 0 To strCol1.Length - 1
                If CCommon.ToLong(strCol1(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numDashBoardID") = strCol1(i)
                    dr("tintColumn") = "1"
                    dr("tintRow") = i + 1
                    dt.Rows.Add(dr)
                End If
            Next

            For i As Integer = 0 To strCol2.Length - 1
                If CCommon.ToLong(strCol2(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numDashBoardID") = strCol2(i)
                    dr("tintColumn") = "2"
                    dr("tintRow") = i + 1
                    dt.Rows.Add(dr)
                End If
            Next
            For i As Integer = 0 To strCol3.Length - 1
                If CCommon.ToLong(strCol3(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numDashBoardID") = strCol3(i)
                    dr("tintColumn") = "3"
                    dr("tintRow") = i + 1
                    dt.Rows.Add(dr)
                End If
            Next

            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim objReportManage As New CustomReportsManage
            objReportManage.strText = ds.GetXml
            objReportManage.DomainID = HttpContext.Current.Session("DomainID")

            Return objReportManage.ManageDashboardOpe("A")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class