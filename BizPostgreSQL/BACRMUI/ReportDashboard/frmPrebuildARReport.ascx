﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildARReport.ascx.vb" Inherits=".frmPrebuildARReport" %>
<asp:HyperLink ID="hplARAging" Target="_blank" runat="server">
    <table class="rptcontent bg-green handle">
        <tr>
            <td>
                <table style="white-space: nowrap; text-align: center; border-spacing: 5px; width: 100%">
                    <tr>
                        <td rowspan="2" style="white-space: nowrap">
                            <table>
                                <tr>
                                    <td>
                                        <img src="../images/PiggyBank.png" width="100" /></td>
                                    <td>
                                        <h2 style="font-weight: 600;">
                                            <asp:Label ID="lblTotalAR" runat="server" Text=""></asp:Label>
                                        </h2>
                                        <p>Total A/R</p>
                                    </td>
                                </tr>
                            </table>

                        </td>
                        <td style="width: 50%">
                            <h4 style="font-weight: 600;">
                                <asp:Label ID="lbl30Days" runat="server" Text=""></asp:Label>
                            </h4>
                            <p>Within 30 days</p>
                        </td>
                        <td style="width: 50%">
                            <h4 style="font-weight: 600;">
                                <asp:Label ID="lbl60Days" runat="server" Text=""></asp:Label>
                            </h4>
                            <p>31-60 Days</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4 style="font-weight: 600;">
                                <asp:Label ID="lbl90Days" runat="server" Text=""></asp:Label>
                            </h4>
                            <p>61-90 Days</p>
                        </td>
                        <td>
                            <h4 style="font-weight: 600;">
                                <asp:Label ID="lblPastDue" runat="server" Text=""></asp:Label>
                            </h4>
                            <p>Past Due</p>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top">
                <div class="box-tools pull-right">
                    <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
                </div>
            </td>
        </tr>
    </table>
</asp:HyperLink>

