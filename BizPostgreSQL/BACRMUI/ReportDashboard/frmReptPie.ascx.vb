Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports System.Collections.Generic
Imports System.Linq.Expressions
Imports System.Reflection
Imports DotNet.Highcharts

Partial Public Class frmNewReptPie
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private _DashBoardID As Long
    Public Property DashBoardID() As Long
        Get
            Try
                Return _DashBoardID
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Long)
            Try
                _DashBoardID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _EditRept As Boolean
    Public Property EditRept() As Boolean
        Get
            Try
                Return _EditRept
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Boolean)
            Try
                _EditRept = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _Width As Integer
    Public Property Width() As Integer
        Get
            Try
                Return _Width
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Integer)
            Try
                _Width = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Private _objReportManage As Object
    Public Property objReportManage() As Object
        Get
            Try
                Return _objReportManage
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Object)
            Try
                _objReportManage = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Function CreateReport() As Boolean
        Try
            If _EditRept = True Then tdEdit.Visible = True
            'UltraChart1.Width = _Width
            dvTable.ID = _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtTable As DataTable

            dtTable = objReportManage.GetReportDashboardDTL

            lblHeader.Text = dtTable.Rows(0).Item("vcHeaderText")
            lblFooter.Text = dtTable.Rows(0).Item("vcFooterText")
            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID
            hplDelete.NavigateUrl = "../ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            pnlChart.Attributes.Add("ondblclick", "return Redirect(" & dtTable.Rows(0).Item("numReportID") & ")")

            Try
                BindReport(dtTable.Rows(0).Item("numReportID"), dtTable.Rows(0).Item("vcXAxis"), dtTable.Rows(0).Item("vcYAxis"), dtTable.Rows(0).Item("vcAggType"))
            Catch ex As Exception

            End Try

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub BindReport(ByVal lngReportID As Long, ByVal vcXAxis As String, ByVal vcYAxis As String, ByVal vcAggType As String)
        Try
            Dim ds As DataSet

            _objReportManage.ReportID = lngReportID
            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            _objReportManage.UserCntID = Session("UserContactID")

            ds = _objReportManage.GetReportListMasterDetail

            Dim trHeader As TableHeaderRow = New TableHeaderRow()
            Dim tc As TableHeaderCell = New TableHeaderCell()

            Dim trRow As TableRow = New TableRow()
            Dim cell As New TableCell()

            If ds.Tables.Count > 0 Then

                Dim objReport As ReportObject = _objReportManage.GetDatasetToReportObject(ds)

                Dim dsColumnList As DataSet
                dsColumnList = _objReportManage.GetReportModuleGroupFieldMaster

                _objReportManage.textQuery = _objReportManage.GetReportQuery(_objReportManage, ds, objReport, False, True, dsColumnList)
                Dim dtData As DataTable = _objReportManage.USP_ReportQueryExecute()

                Dim dr As DataRow

                Dim query = From row In dtData.AsEnumerable() _
            Group row By GroupColumns = New With {Key .Key1 = row.Field(Of String)(vcXAxis)} Into GroupDetail = Group _
            Select New With { _
                Key GroupColumns.Key1, _
                .CountTotal = GroupDetail.Count(), _
                .Detail = GroupDetail _
           }

                If ds.Tables(0).Rows(0)("tintReportType") = 1 Then 'Summary
                    If objReport.SortColumn.Length > 0 Then
                        Dim AggField = From row In objReport.Aggregate Where row.Column = objReport.SortColumn
                        If AggField.Count > 0 Then
                            If objReport.SortDirection = "desc" Then
                                query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r(objReport.SortColumn))) Descending Take (objReport.NoRows)
                            Else
                                query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r(objReport.SortColumn))) Ascending Take (objReport.NoRows)
                            End If
                        End If
                    End If

                    If objReport.NoRows > 0 Then
                        query = query.Take(objReport.NoRows)
                    End If
                End If

                Dim dtChartTable As New DataTable
                dtChartTable.Columns.Add("Column1", GetType(String))
                dtChartTable.Columns.Add("Column2", GetType(Decimal))

                Dim dataYAxis = New List(Of Object)()
                'Dim dataYAxis As Object() = New Object(query.Count - 1) {}
                Dim dataXAxis As String() = New String(query.Count - 1) {}
                Dim j As Integer = 0

                For Each x In query
                    dr = dtChartTable.NewRow

                    Dim total As Integer = x.Detail.Count()
                    Dim Aggregate As Decimal = 0

                    Select Case vcAggType
                        Case "sum"
                            If total = 0 Then
                                Aggregate = 0
                            Else
                                Aggregate = x.Detail.Sum(Function(r) r(vcYAxis))
                            End If

                        Case "avg"
                            If total = 0 Then
                                Aggregate = 0
                            Else
                                Aggregate = x.Detail.Average(Function(r) r(vcYAxis))
                            End If

                        Case "max"
                            If total = 0 Then
                                Aggregate = 0
                            Else
                                Aggregate = x.Detail.Max(Function(r) r(vcYAxis))
                            End If

                        Case "min"
                            If total = 0 Then
                                Aggregate = 0
                            Else
                                Aggregate = x.Detail.Min(Function(r) r(vcYAxis))
                            End If

                        Case Else
                            Aggregate = total

                    End Select

                    dr("Column1") = IIf(x.Key1 = "", "-", x.Key1)
                    dr("Column2") = String.Format("{0:#.00}", total)
                    dtChartTable.Rows.Add(dr)

                    dataXAxis(j) = IIf(x.Key1 = "", "[None]", x.Key1)
                    'dataYAxis(j) = String.Format("{0:#.00}", total)

                    dataYAxis.Add(New Options.Point() With {.Name = IIf(x.Key1 = "", "[None]", x.Key1), .Y = Aggregate})
                    j += 1
                Next

                Dim chart1 As DotNet.Highcharts.Highcharts = New DotNet.Highcharts.Highcharts("chart" & _DashBoardID)

                chart1.SetCredits(New DotNet.Highcharts.Options.Credits() With { _
                 .Enabled = False _
             })

                chart1.InitChart(New DotNet.Highcharts.Options.Chart() With { _
                    .DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie _
                               })
                '         .BackgroundColor = New Helpers.BackColorOrGradient(New Helpers.Gradient() With { _
                '     .LinearGradient = {0, 0, 0, 400}, _
                '     .Stops = New Object(,) {{0, Color.FromArgb(255, 96, 96, 96)}, {1, Color.FromArgb(255, 16, 16, 16)}} _
                '})

                'Dim palette_colors() As System.Drawing.Color = {Color.FromName("#910000"), Color.FromName("#8bbc21"), Color.FromName("#2f7ed8"),
                '                                               Color.FromName("#0d233a"), Color.FromName("#1aadce"), Color.FromName("#492970"),
                '                                                Color.FromName("#f28f43"), Color.FromName("#77a1e5"), Color.FromName("#c42525")}

                'chart1.SetOptions(New DotNet.Highcharts.Helpers.GlobalOptions() With { _
                '            .Colors = palette_colors _
                '        })

                chart1.SetTitle(New DotNet.Highcharts.Options.Title() With {.Text = ""})
                chart1.SetTooltip(New DotNet.Highcharts.Options.Tooltip() With {.PointFormat = "{series.name}: <b>{point.y} - {point.percentage:.2f}%</b>"})

                Dim strColumn As String() = vcXAxis.Split("_")
                Dim dr1() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                chart1.SetXAxis(New DotNet.Highcharts.Options.XAxis() With { _
                    .Categories = dataXAxis.ToArray,
                    .Labels = New DotNet.Highcharts.Options.XAxisLabels() With { _
                        .Align = DotNet.Highcharts.Enums.HorizontalAligns.Center, _
                        .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                        .Title = New DotNet.Highcharts.Options.XAxisTitle() With { _
                        .Text = dr1(0)("vcFieldname")} _
                })

                strColumn = vcYAxis.Split("_")
                dr1 = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                Dim yAxis As String = "Record Count"

                If dr1.Length > 0 Then
                    yAxis = dr1(0)("vcFieldname")
                End If

                chart1.SetYAxis(New DotNet.Highcharts.Options.YAxis() With { _
                   .Labels = New DotNet.Highcharts.Options.YAxisLabels() With { _
                   .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                   .Title = New DotNet.Highcharts.Options.YAxisTitle() With { _
                   .Text = yAxis} _
               })

                chart1.SetSeries(New DotNet.Highcharts.Options.Series() With { _
                  .Data = New DotNet.Highcharts.Helpers.Data(dataYAxis.ToArray()) _
                })

                chart1.SetPlotOptions(New Options.PlotOptions() With { _
                                       .Pie = New Options.PlotOptionsPie() With { _
         .AllowPointSelect = True, .ShowInLegend = True, _
         .Cursor = Enums.Cursors.Pointer, _
         .DataLabels = New Options.PlotOptionsPieDataLabels() With { _
             .Color = ColorTranslator.FromHtml("#000000"), _
             .ConnectorColor = ColorTranslator.FromHtml("#000000"), _
             .Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }" _
        } _
     } _
                })

                ltrChart.Text = chart1.ToHtmlString()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class