﻿Imports System.Configuration
Imports System.Web.HttpContext
Imports System
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Web.UI

Imports DotNetOpenAuth.Messaging
Imports DotNetOpenAuth.OAuth2
Imports Google.Apis.Calendar.v3
Imports Google.Apis.Calendar.v3.Data
Imports Google.Apis.Util
Imports Google.Apis.Auth
Imports Google.Apis.Auth.OAuth2
Imports Google.Apis.Auth.OAuth2.Web
Imports Google.Apis.Auth.OAuth2.Flows
Imports Google.Apis.Util.Store
Imports System.Threading

Imports Newtonsoft.Json.Linq
Imports GoogleAuthenticate
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Threading.Tasks

Public Class GoogleAuthenticateFirst
    Inherits BACRMPage

    Public BizClientID As String = ConfigurationManager.AppSettings("GoogleClientID")
    Public BizClientSecret As String = ConfigurationManager.AppSettings("GoogleClientSecret")


    Dim service As CalendarService = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("GoogleEmailId") IsNot Nothing AndAlso Session("GoogleContactId") IsNot Nothing Then
                Dim flow As IAuthorizationCodeFlow = New OfflineAccessGoogleAuthorizationCodeFlow(New OfflineAccessGoogleAuthorizationCodeFlow.Initializer() With { _
                        .ClientSecrets = New ClientSecrets() With {.ClientId = BizClientID, .ClientSecret = BizClientSecret}, _
                        .DataStore = Nothing, _
                        .Scopes = New String() {CalendarService.Scope.Calendar} _
                    })

                Dim uri = Request.Url.ToString()

                If Not Page.IsPostBack Then
                    If Not Request.QueryString("code") Is Nothing Then
                        Dim token = flow.ExchangeCodeForTokenAsync("Bizautomation.com", Request.QueryString("code"), uri.Substring(0, uri.IndexOf("?")), CancellationToken.None).Result

                        Dim objUserAccess As New UserAccess
                        objUserAccess.ContactID = Session("GoogleContactId")
                        objUserAccess.DomainID = Session("DomainID")
                        objUserAccess.UpdateGoogleRefreshToken(token.RefreshToken)

                        Response.Write("<script>self.close();</script>")
                    Else
                        tblAuthorization.Visible = True
                        lblMessage.Text = "You will be redirected to Google Login Page for authorization.<br /><br /> Please login using your ""<b>" & Session("GoogleEmailId") & "</b>"" email and say ""Allow"" when prompted for authorization."
                    End If
                Else
                    Dim result = New AuthorizationCodeWebApp(flow, uri, uri).AuthorizeAsync("Bizautomation.com", CancellationToken.None).Result

                    If result.RedirectUri IsNot Nothing Then
                        Response.Redirect(result.RedirectUri)
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Class OfflineAccessGoogleAuthorizationCodeFlow
        Inherits GoogleAuthorizationCodeFlow
        Public Sub New(initializer As GoogleAuthorizationCodeFlow.Initializer)
            MyBase.New(initializer)
        End Sub

        Public Overrides Function CreateAuthorizationCodeRequest(redirectUri As String) As Google.Apis.Auth.OAuth2.Requests.AuthorizationCodeRequestUrl
            Return New Google.Apis.Auth.OAuth2.Requests.GoogleAuthorizationCodeRequestUrl(New Uri(AuthorizationServerUrl)) With { _
                .ClientId = ClientSecrets.ClientId, _
                .Scope = String.Join(" ", Scopes), _
                .RedirectUri = redirectUri, _
                .AccessType = "offline", _
                .ApprovalPrompt = "force" _
            }
        End Function
    End Class
End Class