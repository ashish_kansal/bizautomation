﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmStagePercentage.aspx.vb"
    Inherits=".frmStagePercentage" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Project Stage Percentage</title>
    <script language="javascript" type="text/javascript">

        //        function CheckNumber(cint, e) {
        //            var k;
        //            document.all ? k = e.keyCode : k = e.which;
        //            if (cint == 1) {
        //                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
        //                    if (e.preventDefault) {
        //                        e.preventDefault();
        //                    }
        //                    else
        //                        e.returnValue = false;
        //                    return false;
        //                }
        //            }
        //            if (cint == 2) {
        //                if (!(k > 47 && k < 58)) {
        //                    if (e.preventDefault) {
        //                        e.preventDefault();
        //                    }
        //                    else
        //                        e.returnValue = false;
        //                    return false;
        //                }
        //            }
        //        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" Text="Save & Close" CssClass="button" runat="server"
                ValidationGroup="vgStage"></asp:Button>&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Stage Comprises % of Completion
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvStage" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
        AutoGenerateColumns="False" DataKeyNames="numStageDetailsId" ClientIDMode="AutoID">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is"></RowStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundField DataField="vcStageName" HeaderText="Stage" ItemStyle-HorizontalAlign="Center" />
            <asp:TemplateField HeaderText="Percentage" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:TextBox ID="txtPercentage" runat="server" Text='<%#Bind("tintPercentage") %>'
                        onkeypress="CheckNumber(2,event)" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPercentage" Display="Dynamic" SetFocusOnError="true" ValidationGroup="vgStage">
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtPercentage"
                        Display="Dynamic" SetFocusOnError="true" ValidationGroup="vgStage" MaximumValue="100"
                        MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
