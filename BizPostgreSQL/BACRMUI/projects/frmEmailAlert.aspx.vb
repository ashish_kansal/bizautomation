﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing

Public Class frmEmailAlert
    Inherits BACRMPage
    Dim dtTable As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                 ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                LoadTemplates(ddlEmailTemplate1)
                LoadTemplates(ddlEmailTemplate2)

                'loadDetails()

                btnClose.Attributes.Add("onclick", "return Close()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
        Try
            If dtTable Is Nothing Then
                Dim objCampaign As New Campaign
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
            End If
            ddlEmailTemplate.DataSource = dtTable
            ddlEmailTemplate.DataTextField = "VcDocName"
            ddlEmailTemplate.DataValueField = "numGenericDocID"
            ddlEmailTemplate.DataBind()
            ddlEmailTemplate.Items.Insert(0, "--Select One--")
            ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            'Dim objAlerts As New CAlerts

            ''first row
            'objAlerts.AlertDTLID = 4
            'objAlerts.AlertID = 2
            'objAlerts.AlertOn = IIf(chk1.Checked = True, 1, 0)
            'objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
            'objAlerts.CCManager = IIf(chkCCManager.Checked = True, 1, 0)
            'objAlerts.DomainID = Session("DomainID")
            'objAlerts.ManageAlerts()

            '' Inserted By : Pratik Vasani 08/Dec/2008
            'Dim dtIds As DataTable
            'dtIds = objAlerts.GetAlerDetailIds()
            'If dtIds.Rows.Count = 2 Then


            '    objAlerts.AlertDTLID = dtIds.Rows(0).Item("numAlertDtlId")
            '    objAlerts.AlertID = 2
            '    objAlerts.AlertOn = IIf(chkEmailLeadContact.Checked = True, 1, 0)
            '    objAlerts.EmailTemplateID = IIf(objAlerts.AlertOn = 1, ddlEmailTemplate2.SelectedItem.Value, 0)
            '    objAlerts.CCManager = 0
            '    objAlerts.DomainID = Session("DomainID")
            '    objAlerts.ManageAlerts()

            '    objAlerts.AlertDTLID = dtIds.Rows(1).Item("numAlertDtlId")
            '    objAlerts.AlertID = 2
            '    objAlerts.AlertOn = IIf(chkECamp.Checked = True, 1, 0)
            '    objAlerts.EmailCampaignId = IIf(objAlerts.AlertOn = 1, ddlECamp.SelectedItem.Value, 0)
            '    objAlerts.CCManager = 0
            '    objAlerts.DomainID = Session("DomainID")
            '    objAlerts.ManageAlerts()
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class