<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProSubStages.aspx.vb" Inherits="BACRM.UserInterface.Projects.frmProSubStages"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

		<title>Sub Stages</title>

		<script language="javascript">
			function Close()
			{
				window.close()
			}
		</script>
	</HEAD>
	<body  >
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center" >
				<tr >
					<td align="right" colSpan="2">
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save"></asp:Button>
						<asp:Button ID="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
					</td>
				</tr>
				<tr>
				</tr>
				<tr >
					<td class=normal1 align=right >Choose Sub Stage&nbsp;
					</td>
					<td><asp:dropdownlist id="ddlSubStage" AutoPostBack="true" Width="150" CssClass="signup" Runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td colspan="2"><asp:Table ID="tblSubStage" Runat="server" Width="100%"></asp:Table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
