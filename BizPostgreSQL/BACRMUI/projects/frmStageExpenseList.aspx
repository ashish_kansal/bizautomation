﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmStageExpenseList.aspx.vb"
    Inherits=".frmStageExpenseList" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenBill(a, b, c, d) {
            window.open("../Accounting/frmAddBill.aspx?rtyWR=" + d + "&uihTR=&tyrCV=&pluYR=&fghTY=&ProID=" + a + "&StageID=" + b + "&StageName=" + c, '', 'toolbar=no,titlebar=no,top=200,width=750,height=450,left=200,scrollbars=yes,resizable=yes')
            self.close();
            return false;
        }
        function OpenPO(a, b, c, d) {
            window.open("../Opportunity/frmNewPurchaseOrder.aspx?Source=" + a + "&rtyWR=" + d + "&uihTR=&tyrCV=&pluYR=&fghTY=" + "&StageID=" + b + "&StageName=" + c, '', 'toolbar=no,titlebar=no,top=200,width=1000,height=600,left=200,scrollbars=yes,resizable=yes')
            self.close();
            return false;
        }
        function Close() {
            opener.__doPostBack('btnChangeTimeExpense', '');
            self.close();
        }
        function OpenOpp(a, b) {
            if (a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                window.opener.location.href = str;
                window.close();
            }
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenBillPaymentDetail(a) {
            window.open('../Accounting/frmAddBill.aspx?BillId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <span style="float: left">
                <asp:HyperLink ID="hplAddBill" runat="server" CssClass="hyperlink" Text="Add Bill"></asp:HyperLink>
                &nbsp;
                <asp:HyperLink ID="hplPO" runat="server" CssClass="hyperlink" Text="Add Purchase Order"></asp:HyperLink>
                &nbsp;</span>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="" ID="lblStageName" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvBizDocs" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="tbl" Width="100%" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="" Visible="false">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProOppID" Text='<%# Eval("numProjOppID") %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblBillID" Text='<%# Eval("numBillID") %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="numBizDocsPaymentDetailsId" Text='<%# Eval("numBizDocsPaymentDetailsId") %>'
                                    Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblIntegrated" Text='<%# Eval("bitIntegratedToAcnt") %>'
                                    Style="display: none;"></asp:Label>
                                <asp:Label runat="server" ID="lblOppId" Text='<%# Eval("numOppId") %>' Style="display: none"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Type" HeaderText="Type" ControlStyle-ForeColor="White" />
                        <asp:BoundField DataField="vcItemName" HeaderText="Name" ControlStyle-ForeColor="White">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Order ID">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblLink" Visible="false" Text='<%# Eval("ExpenseAccount") %>'></asp:Label>
                                <asp:HyperLink ID="hplDetails" runat="server" CssClass="hyperlink"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" SortExpression="">
                            <ItemTemplate>
                                <%# String.Format("{0:##,#00.00}", Eval("ExpenseAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    CommandArgument='<%# CType(Container, GridViewRow).RowIndex %>'></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
