﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDeleteStage.aspx.vb"
    Inherits=".frmDeleteStage" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Project Stage Percentage</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" Text="Save & Close" CssClass="button" runat="server"
                ValidationGroup="vgStage"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Stage Comprises % of Completion
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMile" runat="server" Width="600px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="lblMsg" runat="server" CssClass="normal4"></asp:Label><br />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:GridView ID="gvStage" runat="server" Width="100%" CssClass="dg" AllowSorting="true"
                   AutoGenerateColumns="False" DataKeyNames="numStageDetailsId" ClientIDMode="AutoID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundField DataField="vcStageName" HeaderText="Stage" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Percentage" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPercentage" runat="server" Text='<%#Bind("tintPercentage") %>'
                                    onkeypress="CheckNumber(2,event)" MaxLength="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtPercentage" Display="Dynamic" SetFocusOnError="true" ValidationGroup="vgStage">
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtPercentage"
                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="vgStage" MaximumValue="100"
                                    MinimumValue="0" Type="Integer"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
