﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports System.Globalization
Public Class frmStagePercentage
    Inherits BACRMPage
    Dim objProject As New Project
    Dim dtStages As New DataTable
    Dim lngStageID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngStageID = GetQueryStringVal( "StageID")
            If Not IsPostBack Then
                Session("StagePercentage") = Nothing
                objProject = New Project
                objProject.DomainID = Session("DomainId")
                objProject.StageID = lngStageID
                objProject.strType = GetQueryStringVal( "Type")
                dtStages = objProject.GetProjectChildStage()

                Dim dr As DataRow
                dr = dtStages.NewRow()
                dr("numStageDetailsId") = 0
                dr("numStagePercentageId") = lngStageID
                dr("vcStageName") = "New Stage"
                dr("tintPercentage") = 0

                dtStages.Rows.Add(dr)

                gvStage.DataSource = dtStages
                gvStage.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveClose.Click
        Try
            If Page.IsValid Then
                Dim sum As Integer = 0
                Dim dtStagePercentage As New DataTable

                With dtStagePercentage.Columns
                    .Add("numStageDetailsId", Type.GetType("System.Int32"))
                    .Add("tintPercentage", Type.GetType("System.Int32"))
                End With
                Dim dr As DataRow
                For Each gvr As GridViewRow In gvStage.Rows
                    sum = sum + CInt(CType(gvr.FindControl("txtPercentage"), TextBox).Text.Trim)

                    dr = dtStagePercentage.NewRow()

                    dr("numStageDetailsId") = gvStage.DataKeys(gvr.RowIndex).Value
                    dr("tintPercentage") = CInt(CType(gvr.FindControl("txtPercentage"), TextBox).Text.Trim)

                    dtStagePercentage.Rows.Add(dr)
                Next

                If sum <> 100 Then
                    Response.Write("<script language='javascript'>alert('You must add values to all the percentages within stages in milestones so that the sum total is equal to 100%')</script>")
                Else
                    Session("StagePercentage") = dtStagePercentage
                    'Response.Write("<script language='javascript'>opener.document.getElementById('lblAStagePercentage').innerHTML =""" & dtStagePercentage.Rows(dtStagePercentage.Rows.Count - 1)("tintPercentage") & """;opener.document.getElementById('hfSetPercentage').style.display='none';self.close();</script>")
                    Response.Write("<script language='javascript'>opener.document.getElementById('lblAStagePercentage').innerHTML =""" & dtStagePercentage.Rows(dtStagePercentage.Rows.Count - 1)("tintPercentage") & " ,"";opener.document.getElementById('hfSetPercentage').innerHTML=""Change % of completion"";self.close();</script>")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class