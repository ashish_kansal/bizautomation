<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProTime.aspx.vb"
    Inherits="BACRM.UserInterface.Projects.frmProTime" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="~/Accounting/TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Time</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function Close() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }
            window.close();
        }
        function Save() {
            if (document.getElementById("txtMode").value == "TE") {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Organization")
                    return false;
                }
                if (document.getElementById("ddlPro").value == 0) {
                    alert("Select Project")
                    document.getElementById("ddlPro").focus();
                    return false;
                }
            }
            if (document.getElementById("txtMode").value == "BD") {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Organization")
                    return false;
                }
            }
            if (document.getElementById("radBill").checked == true) {
                if (document.getElementById("txtRate").value == "") {
                    alert("Enter Rate/Hour")
                    document.getElementById("txtRate").focus();
                    return false;
                }
                if (document.getElementById("ddEmployee").value == "0") {
                    alert("Select Employee")
                    document.getElementById("ddEmployee").focus();
                    return false;
                }
            }
            if (document.getElementById("ctl00_Content_calFrom_txtDate").value == "") {
                alert("Enter Start Date")
                document.getElementById("ctl00_Content_calFrom_txtDate").focus();
                return false;
            }
            if (document.getElementById("ctl00_Content_calto_txtDate").value == "") {
                alert("Enter End Date")
                document.getElementById("ctl00_Content_calto_txtDate").focus();
                return false;
            }
            if (document.getElementById("ddlServiceItem") != null)
                if (document.getElementById("ddlServiceItem").value == 0) {
                    alert("Select a Service Item")
                    document.getElementById("ddlServiceItem").focus();
                    return false;
                }
            if (document.getElementById("radBill").checked == true) {
                if (document.getElementById("ddlOpp").value == "0") {
                    alert("Select Opportunity")
                    document.getElementById("ddlOpp").focus();
                    return false;
                }
            }
        }
        function OpenRec(a) {
            window.open('../opportunity/frmRecomBillTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivID=' + a, '', 'toolbar=no,titlebar=no,left=300,top=300,width=400,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function FillBox(a, b) {
            document.getElementById("txtRate").value = a
            document.getElementById("txtItemID").value = b;
        }

        function OpenOpp(a, b) {
            if (a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;

                if (window.opener == null)
                    window.parent.location.href = str;
                else if (window.opener.opener == null)
                    window.opener.location.href = str;
                else
                    window.opener.opener.frames.document.location.href = str;
            }
        }

        function OpenProject(a) {
            var str;
            str = "../projects/frmProjects.aspx?ProId=" + a;

            if (window.opener == null)
                window.parent.location.href = str;
            else if (window.opener.opener == null)
                window.opener.location.href = str;
            else
                window.opener.opener.frames.document.location.href = str;
        }

        function OpenProjectStageDetail(a, b) {
            var str;
            str = "../projects/frmProjects.aspx?ProId=" + a + "&StageId=" + b + "&SelectedIndex=1";

            if (window.opener == null)
                window.parent.location.href = str;
            else if (window.opener.opener == null)
                window.opener.location.href = str;
            else
                window.opener.opener.frames.document.location.href = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td align="center" colspan="3" class="normal4">
                <asp:Literal runat="server" ID="litMessage" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btndelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label runat="server" ID="lblTitle" Text="Select Project"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table width="750px" border="0">
        <tr>
            <td></td>
            <td>
                <asp:RadioButton ID="radBill" AutoPostBack="true" runat="server" CssClass="normal1"
                    GroupName="Bill" Text="Billable" Checked="true" />
                <asp:RadioButton ID="radNonBill" AutoPostBack="true" runat="server" CssClass="normal1"
                    GroupName="Bill" Text="Non-Billable" />
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlProjSelect" Visible="false">
            <tr>
                <td class="text" align="right">Organization
                </td>
                <td colspan="3">
                    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                        ClientIDMode="Static"
                        ShowMoreResultsBox="true"
                        Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                    </telerik:RadComboBox>
                    <%--   <rad:radcombobox id="radCmbCompany" externalcallbackpage="../include/LoadCompany.aspx"
                                    width="195px" dropdownwidth="200px" skin="WindowsXP" runat="server" autopostback="True"
                                    allowcustomtext="True" enableloadondemand="True" skinspath="~/RadControls/ComboBox/Skins">
                                </rad:radcombobox>--%>
                </td>
            </tr>
            <tr id="trProject" runat="server">
                <td class="text" align="right">Open Project
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddlPro" runat="server" AutoPostBack="true" Width="350" CssClass="signup">
                    </asp:DropDownList>
                </td>
            </tr>
        </asp:Panel>
        <tr id="trClass" runat="server">
            <td class="text" align="right">Class
            </td>
            <td colspan="3">
                <asp:DropDownList runat="server" ID="ddlClass" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlRate">
            <tr>
                <td class="normal1" align="right">Rate/Hour<%--<asp:HyperLink ID="hplRec" runat="server" CssClass="hyperlink"></asp:HyperLink>--%>
                </td>
                <td>
                    <asp:TextBox ID="txtRate" CssClass="signup" runat="server" Width="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text" align="right">Employee
                </td>
                <td colspan="3">
                    <asp:DropDownList runat="server" ID="ddEmployee" CssClass="signup">
                    </asp:DropDownList>
                </td>
            </tr>
        </asp:Panel>
        <tr runat="server" id="trTime1">
            <td class="normal1" align="right">From Date
            </td>
            <td>
                <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
            </td>
            <td class="text" id="tdStartTime" runat="server">
                <img src="../images/Clock-16.gif" align="absMiddle">
                <asp:DropDownList ID="ddltime" runat="server" Width="55" CssClass="signup">
                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                    <asp:ListItem Selected="True" Value="15">8:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                </asp:DropDownList>
                &nbsp;AM<input id="chkAM" type="radio" checked value="0" name="AM" runat="server" />PM<input
                    id="chkPM" type="radio" value="1" name="AM" runat="server" />
            </td>
        </tr>
        <tr runat="server" id="trTime2">
            <td class="normal1" align="right">End Date
            </td>
            <td>
                <BizCalendar:Calendar ID="calto" runat="server" ClientIDMode="AutoID" />
            </td>
            <td class="text" id="tdEndTime" runat="server">
                <img src="../images/Clock-16.gif" align="absMiddle" />
                <asp:DropDownList ID="ddlEndTime" runat="server" Width="55" CssClass="signup">
                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="15">8:00</asp:ListItem>
                    <asp:ListItem Selected="True" Value="16">8:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                </asp:DropDownList>
                &nbsp;AM<input id="chkEndAM" type="radio" checked value="0" name="EndAM" runat="server" />PM<input
                    id="chkEndPM" type="radio" value="1" name="EndAM" runat="server" />
            </td>
        </tr>
        <tr style="display: none">
            <td class="normal1" align="right">Contract
            </td>
            <td colspan="3" class="normal1">
                <asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="200" runat="server"
                    CssClass="signup">
                </asp:DropDownList>
                (Apply hours to contract)
            </td>
        </tr>
        <tr style="display: none">
            <td class="normal1" align="right">Contract Hours Remaining :
            </td>
            <td colspan="3" class="normal1">
                <asp:Label ID="lblRemHours" runat="server" CssClass="normal1"></asp:Label>
            </td>
        </tr>
        <%--    <tr class="normal1" align="right">
                        <td align="right" class="normal1">
                            Opportunity
                        </td>
                        <td colspan="3" align="left" class="normal1">
                            <asp:DropDownList ID="ddlOpp" runat="server" CssClass="signup" Width="300">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
        <asp:Panel ID="pnlInvoice" runat="server">
            <tr>
                <td class="normal1" align="right">Select a service Item
                </td>
                <td>
                    <asp:DropDownList ID="ddlServiceItem" runat="server" CssClass="signup" Width="150"
                        AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trDesc" runat="server" visible="false">
                <td class="normal1" align="right">Service Description
                </td>
                <td>
                    <asp:Label ID="lblServiceDesc" runat="server" CssClass="text"></asp:Label>
                </td>
            </tr>
            <tr class="normal1" align="right">
                <td align="right" class="normal1">Open Sales Order
                </td>
                <td colspan="3" align="left" class="normal1">
                    <asp:DropDownList ID="ddlOpp" runat="server" CssClass="signup" Width="300">
                    </asp:DropDownList>
                </td>
            </tr>
        </asp:Panel>
        <tr id="trNotes" runat="server">
            <td class="normal1" align="right">Notes
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtNotes" CssClass="signup" runat="server" Width="405" Height="50"
                    MaxLength="500" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <asp:Panel ID="pnlOtherInfo" runat="server" Visible="false">
            <tr>
                <td class="normal1" align="right">Order
                </td>
                <td colspan="3">
                    <asp:HyperLink ID="hplOrder" runat="server" CssClass="hyperlink"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="normal1" align="right">Project
                </td>
                <td colspan="3">
                    <asp:HyperLink ID="hplProject" runat="server" CssClass="hyperlink"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="normal1" align="right">Project Stage
                </td>
                <td colspan="3">
                    <asp:HyperLink ID="hplProjectStage" runat="server" CssClass="hyperlink"></asp:HyperLink>
                </td>
            </tr>
        </asp:Panel>
    </table>
    <asp:TextBox ID="txtCategoryHDRID" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtItemID" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtProId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtStageId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtDivId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtMode" runat="server" Text="" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hfTimeBudget" runat="server" />
    <asp:HiddenField ID="hfbitTimeBudget" runat="server" />
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
    <asp:HiddenField ID="hdnCategoryID" runat="server" />
    <asp:HiddenField ID="hdnCategoryType" runat="server" />
    <asp:HiddenField ID="hdnReimburse" runat="server" />
</asp:Content>
