﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProjectIncomeExpenseDetails.aspx.vb"
    Inherits=".frmProjectIncomeExpenseDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <script>
        function Close() {
            window.close();
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table width="100%">
        <tr>
            <td class="normal2" align="right">
            </td>
            <td align="left">
                <asp:Label ID="lblProjectName" runat="server" CssClass="normal2" Font-Bold="true"></asp:Label>
            </td>
            <td align="right">
            <asp:Button ID="btnRefresh" runat="server" CssClass="button" Text="Refresh" />&nbsp;
                <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export to Excel" />&nbsp;<asp:Button
                    ID="btnClose" runat="server" CssClass="button" OnClientClick="return Close();"
                    Text="Close" />
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <asp:DataGrid ID="dgDetails" CssClass="dg" Width="100%" runat="server" AutoGenerateColumns="false" AllowSorting="true">
                    <AlternatingItemStyle CssClass="ais" />
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="vcAccountName" HeaderText="Account"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Income">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#ReturnMoney(Eval("IncomeAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Expense">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#ReturnMoney(Eval("ExpenseAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <%--<div style="float: right">
        <asp:Label ID="lblProfit" runat="server" CssClass="normal1"></asp:Label>&nbsp;&nbsp;&nbsp;</div>--%>
    </form>
</body>
</html>
