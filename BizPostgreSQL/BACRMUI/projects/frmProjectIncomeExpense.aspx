﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProjectIncomeExpense.aspx.vb"
    Inherits=".frmProjectIncomeExpense" %>

<%@ Register Assembly="Infragistics35.WebUI.Misc.v9.1, Version=9.1.20091.1015, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.UltraChart.Data" TagPrefix="igchartdata" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Project Income & Expense</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <script>
        function Close() {
            window.close();
            return false;
        }
        function OpenDetails(b) {
            window.open("../Projects/frmProjectIncomeExpenseDetails.aspx?proId=" + document.form1.ddlProjects.value + "&Type=" + b, '', 'toolbar=no,titlebar=no,top=200,width=700,height=300,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <table align="right" width="100%">
        <tr>
            <td colspan="7">
            </td>
            <td class="normal1" align="right">
                Project
            </td>
            <td class="normal1" align="left">
                <asp:DropDownList runat="server" ID="ddlProjects" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnColse" runat="server" Text="Close" CssClass="button" OnClientClick="return Close();" />
            </td>
        </tr>
        <tr>
            <td class="normal1" colspan="10" align="right">
            <font size="2">
                <asp:Label ID="lblBalance" runat="server"></asp:Label></font>
            </td>
        </tr>
        <tr>
            <td class="normal1">
            </td>
            <td class="normal1" style="color: Green" colspan="2">
                <span onclick="return OpenDetails('1')"><u>Income:<asp:Label ID="lblIncome" runat="server"></asp:Label></u></span>
            </td>
            <td class="normal1"  style="color: Red" colspan="2">
                <span onclick="return OpenDetails('2')"><u>Expense:<asp:Label ID="lblExpense" runat="server"></asp:Label></u></span>
            </td>
            <td class="normal1">
                Billable Hours:
            </td>
            <td class="normal1">
                <asp:Label ID="lblBillableHours" runat="server"></asp:Label>
            </td>
            <td class="normal1">
                Non-Billable Hours:
            </td>
            <td class="normal1">
                <asp:Label ID="lblNonBillableHours" runat="server"></asp:Label>
            </td>
            <td class="normal1">
            </td>
        </tr>
        <tr>
            <td class="normal1">
                Contract:
            </td>
            <td class="normal1">
                <asp:Label ID="lblContract" runat="server"></asp:Label>
            </td>
            <td class="normal1">
                Amount Balance:
            </td>
            <td class="normal1">
                <asp:Label ID="lblAmountBalance" runat="server"></asp:Label>
            </td>
            <td class="normal1">
                Days Remaining:
            </td>
            <td class="normal1">
                <asp:Label ID="lblDays" runat="server"></asp:Label>
            </td>
            <td class="normal1">
                Incidents Remaining:
            </td>
            <td class="normal1">
                <asp:Label ID="lblIncidents" runat="server"></asp:Label>
            </td>
            <td class="normal1">
                Hours Remaining:
            </td>
            <td class="normal1">
                <asp:Label ID="lblHours" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1">
                Comments:
            </td>
            <td colspan="9" class="normal1">
                <asp:Label ID="lblComments" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center"> 
              <igchart:UltraChart ID="UltraChart1" runat="server" ChartType="PieChart" BackgroundImageFileName=""
                    Border-Color="Black" Border-Thickness="0" EmptyChartText="Data Not Available. Please call UltraChart.Data.DataBind() after setting valid Data.DataSource"
                    Version="9.1">
                    <Legend Font="Microsoft Sans Serif, 7pt" Location="Top" SpanPercentage="35" Visible="True">
                    </Legend>
                    <ColorModel AlphaLevel="150" ColorBegin="Pink" ColorEnd="DarkRed" ModelStyle="CustomLinear">
                        <Skin>
                            <PEs>
                                <igchartprop:PaintElement ElementType="Gradient" Fill="108, 162, 36" FillGradientStyle="Horizontal"
                                    FillStopColor="148, 244, 17" StrokeWidth="0" />
                                <igchartprop:PaintElement ElementType="Gradient" Fill="7, 108, 176" FillGradientStyle="Horizontal"
                                    FillStopColor="53, 200, 255" StrokeWidth="0" />
                                <igchartprop:PaintElement ElementType="Gradient" Fill="230, 190, 2" FillGradientStyle="Horizontal"
                                    FillStopColor="255, 255, 81" StrokeWidth="0" />
                                <igchartprop:PaintElement ElementType="Gradient" Fill="215, 0, 5" FillGradientStyle="Horizontal"
                                    FillStopColor="254, 117, 16" StrokeWidth="0" />
                                <igchartprop:PaintElement ElementType="Gradient" Fill="252, 122, 10" FillGradientStyle="Horizontal"
                                    FillStopColor="255, 108, 66" StrokeWidth="0" />
                            </PEs>
                        </Skin>
                    </ColorModel>
                    <PieChart>
                        <Labels Font="Verdana, 7pt" />
                    </PieChart>
                    <Axis>
                        <Y LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;DATA_VALUE:00.##&gt;"
                                Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" HorizontalAlign="Near"
                                    Orientation="Horizontal" VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                                <Layout Behavior="Auto">
                                </Layout>
                            </Labels>
                        </Y>
                        <PE ElementType="None" Fill="Cornsilk" />
                        <X LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" HorizontalAlign="Near"
                                    Orientation="Horizontal" VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                                <Layout Behavior="Auto">
                                </Layout>
                            </Labels>
                        </X>
                        <Y2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                    Orientation="Horizontal" VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                                <Layout Behavior="Auto">
                                </Layout>
                            </Labels>
                        </Y2>
                        <X2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                    Orientation="Horizontal" VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                                <Layout Behavior="Auto">
                                </Layout>
                            </Labels>
                        </X2>
                        <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString=""
                                Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" Orientation="Horizontal"
                                    VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                                <Layout Behavior="Auto">
                                </Layout>
                            </Labels>
                        </Z>
                        <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                            <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                Visible="True" />
                            <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                Visible="False" />
                            <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                Orientation="Horizontal" VerticalAlign="Center">
                                <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" Orientation="Horizontal"
                                    VerticalAlign="Center">
                                    <Layout Behavior="Auto">
                                    </Layout>
                                </SeriesLabels>
                                <Layout Behavior="Auto">
                                </Layout>
                            </Labels>
                        </Z2>
                    </Axis>
                    <TitleLeft Extent="33" Font="Microsoft Sans Serif, 7pt" Location="Left" Visible="True">
                    </TitleLeft>
                    <Effects>
                        <Effects>
                            <igchartprop:GradientEffect />
                        </Effects>
                    </Effects>
                    <Data>
                        <EmptyStyle>
                            <LineStyle DrawStyle="Dash" />
                        </EmptyStyle>
                    </Data>
                    <Border Thickness="0" />
                </igchart:UltraChart>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
