
'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  20/3/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow
Imports System.IO
Imports System.Web.Services
Imports Newtonsoft.Json
Imports System.Collections.Generic

Namespace BACRM.UserInterface.Projects
    Public Class frmProjects : Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objOpportunity As MOpportunity
        Dim objProject As New ProjectIP
        Private dtEmployee As DataTable
        Private dtTeam As DataTable
        Private dtGrades As DataTable
        Dim ObjCus As CustomFields
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim myRow As DataRow
        Dim arrOutPut() As String = New String(2) {}
        Dim m_aryRightsForViewLayoutButton() As Integer
        Dim m_aryRightsProjectTasksTab() As Integer
        Dim m_aryRightsManageProjectTasksTab() As Integer
        Dim m_aryRightsProjectTeamsTab() As Integer
        Dim m_aryRightsForPLTab() As Integer
        Dim lngProID As Long
        Dim lngDivId As Long
        Dim strColumn As String
        Dim boolIntermediatoryPage As Boolean = False
        Dim dtCustomFieldTable, dtTableInfo, dtProjectProcess As DataTable
        Dim objPageControls As New PageControls


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                m_aryRightsProjectTasksTab = GetUserRightsForPage_Other(12, 4)
                m_aryRightsManageProjectTasksTab = GetUserRightsForPage_Other(12, 5)
                m_aryRightsProjectTeamsTab = GetUserRightsForPage_Other(12, 6)
                m_aryRightsForPLTab = GetUserRightsForPage_Other(12, 7)

                If Not IsPostBack Then
                    Dim m_aryRightsEditBusinessProcess() As Integer = GetUserRightsForPage_Other(12, 8)
                    If m_aryRightsEditBusinessProcess(RIGHTSTYPE.UPDATE) = 0 Then
                        ddlBusinessProcess.Visible = False
                        btnRemoveProcessOpportunity.Visible = False
                    End If
                    If (CCommon.ToInteger(Session("UserGroupID")) <> 2) Then
                        chkIsForExternalUser.Visible = True
                    Else
                        chkIsForExternalUser.Visible = True
                    End If
                    hdnDateRange.Value = "1"
                    rdManageWIPDate.SelectedDate = DateTime.UtcNow.AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    m_aryRightsForViewLayoutButton = GetUserRightsForPage_Other(12, 13)
                    If m_aryRightsForViewLayoutButton(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If
                End If
                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()

                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                lngProID = GetQueryStringVal("ProId")
                Dim hdnCollabarationLoggedInUser As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnLoggedInUser"), HiddenField)
                Dim hdnCollabarationRecordId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordId"), HiddenField)
                Dim hdnCollabarationRecordType As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordType"), HiddenField)
                Dim hdnCollabarationDomainId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnDomainId"), HiddenField)
                Dim hdnCollabarationPortalDocUrl As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnCollabarationPortalDocUrl"), HiddenField)
                Dim hdnClientMachineUTCTimeOffset As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnClientMachineUTCTimeOffset"), HiddenField)
                hdnCollabarationLoggedInUser.Value = CCommon.ToString(Session("UserContactID"))
                hdnCollabarationDomainId.Value = CCommon.ToString(Session("DomainID"))
                hdnClientMachineUTCTimeOffset.Value = CCommon.ToString(Session("ClientMachineUTCTimeOffset"))
                hdnCollabarationRecordType.Value = "4"
                hdnCollabarationRecordId.Value = 0
                hdnCollabarationPortalDocUrl.Value = CCommon.GetDocumentPath(Convert.ToInt64(Session("DomainID")))
                hdnProjectID.Value = lngProID

                GetUserRightsForPage(12, 3)

                If Not IsPostBack Then
                    BindProcessList()
                    AddToRecentlyViewed(RecetlyViewdRecordType.Project, lngProID)

                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnTSave.Visible = False
                        btnTSaveAndCancel.Visible = False
                        ibChangeCustomer.Visible = False
                    End If

                    If GetQueryStringVal("SelectedIndex") <> "" Then
                        radOppTab.SelectedIndex = GetQueryStringVal("SelectedIndex")
                    End If
                    If lngProID <> 0 Then
                        LoadSavedInformation()
                        tblMenu.Visible = True
                    Else
                        tblMenu.Visible = False
                    End If
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('R','" & lngProID & "','13');")
                    hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Projects&tyrCV=" & lngProID & "')")
                    btnTSave.Attributes.Add("onclick", "return Save()")
                    btnTSaveAndCancel.Attributes.Add("onclick", "return Save()")

                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        If GetQueryStringVal("SelectedIndex") = "" Then
                            radOppTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))
                        End If
                    End If
                    If radOppTab.SelectedIndex = 3 Then
                        BindProjectIncomeAndExpense()
                    End If

                    If GetQueryStringVal("SI") <> "" Then
                        If radOppTab.Tabs.Count > SI Then radOppTab.SelectedIndex = SI
                    ElseIf GetQueryStringVal("SelectedIndex") <> "" Then
                        If radOppTab.Tabs.Count > CCommon.ToInteger(GetQueryStringVal("SelectedIndex")) Then radOppTab.SelectedIndex = CCommon.ToInteger(GetQueryStringVal("SelectedIndex"))
                    End If

                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    If radOppTab.SelectedTab.Value = "ProjectTask" Then
                        BindWIP(False)
                    End If
                    If radOppTab.SelectedTab.Value = "ManageProjectTask" Then
                        BindWIP(True)
                    End If
                Else
                    LoadSavedInformation()
                End If

                btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadSavedInformation()
            Try
                Dim dtDetails As DataTable
                objProject.ProjectID = lngProID
                objProject.DomainID = Session("DomainID")
                objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtDetails = objProject.ProjectDetail(Of DataTable)()

                If CCommon.ToLong(dtDetails.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) AndAlso CCommon.ToLong(Session("UserDivisionID")) <> CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionId")) Then
                    GetUserRightsForRecord(12, 3, CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")), CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")))
                End If
                chkIsForExternalUser.Checked = CCommon.ToBool(dtDetails.Rows(0).Item("bitDisplayTimeToExternalUsers"))
                If (CCommon.ToInteger(Session("UserGroupID")) = 2 AndAlso CCommon.ToBool(dtDetails.Rows(0).Item("bitDisplayTimeToExternalUsers")) = True) Then
                    hdnIsExternalUser.Value = "none"
                Else
                    hdnIsExternalUser.Value = ""
                End If
                'UPDATE RIGHTS
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnTSave.Visible = False
                    btnTSaveAndCancel.Visible = False
                    ibChangeCustomer.Visible = False
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 1 Then
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")) <> CCommon.ToLong(Session("UserContactID")) Then
                        btnTSave.Visible = False
                        btnTSaveAndCancel.Visible = False
                        ibChangeCustomer.Visible = False
                    End If
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = 0 Then
                        btnTSave.Visible = False
                        btnTSaveAndCancel.Visible = False
                        ibChangeCustomer.Visible = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            btnTSave.Visible = False
                            btnTSaveAndCancel.Visible = False
                            ibChangeCustomer.Visible = False
                        End If
                    End If
                End If

                'DELETE RIGHTS
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                    btnActDelete.Visible = False
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")) <> CCommon.ToLong(Session("UserContactID")) Then
                        btnActDelete.Visible = False
                    End If
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = 0 Then
                        btnActDelete.Visible = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            btnActDelete.Visible = False
                        End If
                    End If
                End If

                'FINISH PROJECT RIGHTS
                Dim m_aryRightsForFinishProject() As Integer = GetUserRightsForPage_Other(12, 17)
                If m_aryRightsForFinishProject(RIGHTSTYPE.VIEW) = 0 Then
                    btnFinish.Visible = False
                ElseIf m_aryRightsForFinishProject(RIGHTSTYPE.DELETE) = 1 Then
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")) <> CCommon.ToLong(Session("UserContactID")) Then
                        btnFinish.Visible = False
                    End If
                ElseIf m_aryRightsForFinishProject(RIGHTSTYPE.DELETE) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = 0 Then
                        btnFinish.Visible = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            btnFinish.Visible = False
                        End If
                    End If
                End If

                'MANAGE PROJECT TASKS TAB VIEW RIGHTS
                If m_aryRightsManageProjectTasksTab(RIGHTSTYPE.VIEW) = 0 Then
                    radOppTab.FindTabByValue("ManageProjectTask").Visible = False
                ElseIf m_aryRightsManageProjectTasksTab(RIGHTSTYPE.VIEW) = 1 Then
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")) <> CCommon.ToLong(Session("UserContactID")) Then
                        radOppTab.FindTabByValue("ManageProjectTask").Visible = False
                    End If
                ElseIf m_aryRightsManageProjectTasksTab(RIGHTSTYPE.VIEW) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = 0 Then
                        radOppTab.FindTabByValue("ManageProjectTask").Visible = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            radOppTab.FindTabByValue("ManageProjectTask").Visible = False
                        End If
                    End If
                End If

                'MANAGE PROJECT TASKS TAB UPDATE RIGHTS
                If m_aryRightsManageProjectTasksTab(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSaveManageTasks.Visible = False
                ElseIf m_aryRightsManageProjectTasksTab(RIGHTSTYPE.UPDATE) = 1 Then
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")) <> CCommon.ToLong(Session("UserContactID")) Then
                        btnSaveManageTasks.Visible = False
                    End If
                ElseIf m_aryRightsManageProjectTasksTab(RIGHTSTYPE.UPDATE) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = 0 Then
                        btnSaveManageTasks.Visible = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            btnSaveManageTasks.Visible = False
                        End If
                    End If
                End If

                'P&L TAB VIEW RIGHTS
                If m_aryRightsForPLTab(RIGHTSTYPE.VIEW) = 0 Then
                    radOppTab.FindTabByValue("Report").Visible = False
                ElseIf m_aryRightsForPLTab(RIGHTSTYPE.VIEW) = 1 Then
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")) <> CCommon.ToLong(Session("UserContactID")) Then
                        radOppTab.FindTabByValue("Report").Visible = False
                    End If
                ElseIf m_aryRightsForPLTab(RIGHTSTYPE.VIEW) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = 0 Then
                        radOppTab.FindTabByValue("Report").Visible = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            radOppTab.FindTabByValue("Report").Visible = False
                        End If
                    End If
                End If

                If CCommon.ToBool(dtDetails.Rows(0).Item("bitContractExists")) Then
                    lblTimeLeft.Text = CCommon.ToString(dtDetails.Rows(0).Item("timeLeft"))
                    If CCommon.ToLong(dtDetails.Rows(0).Item("timeLeftInMinutes")) < 0 Then
                        lblTimeLeft.CssClass = "lblHeighlightRed"
                    End If
                Else
                    lblTimeLeft.Visible = False
                End If

                hdnBuildProcess.Value = CCommon.ToLong(dtDetails.Rows(0)("numBusinessProcessID"))

                Dim dt As DataTable = objCommon.GetMasterListItems(52, CCommon.ToLong(Session("DomainID")))
                ddlPauseReasonProject.DataSource = dt
                ddlPauseReasonProject.DataTextField = "vcData"
                ddlPauseReasonProject.DataValueField = "numListItemID"
                ddlPauseReasonProject.DataBind()
                ddlPauseReasonProject.Items.Insert(0, New ListItem("-- Select One --", "0"))

                ddlAddReasonForPauseProject.DataSource = dt
                ddlAddReasonForPauseProject.DataTextField = "vcData"
                ddlAddReasonForPauseProject.DataValueField = "numListItemID"
                ddlAddReasonForPauseProject.DataBind()
                ddlAddReasonForPauseProject.Items.Insert(0, New ListItem("-- Select One --", "0"))

                Dim dtEmployee As DataTable = objCommon.ConEmpList(CCommon.ToLong(Session("DomainID")), False, 0)
                ddlTaskAssignTo.DataSource = dtEmployee
                ddlTaskAssignTo.DataValueField = "numContactID"
                ddlTaskAssignTo.DataTextField = "vcUserName"
                ddlTaskAssignTo.DataBind()
                ddlTaskAssignTo.Items.Insert(0, New ListItem("-- Select One --", "0"))

                ddlBusinessProcess.Enabled = True
                btnAddProcessOpportunity.Visible = True
                btnRemoveProcessOpportunity.Visible = False
                If CCommon.ToLong(dtDetails.Rows(0)("numBusinessProcessID")) > 0 Then
                    ddlBusinessProcess.Items.Clear()
                    ddlBusinessProcess.Items.Insert(0, New ListItem(CCommon.ToString(dtDetails.Rows(0).Item("vcProcessName")), CCommon.ToLong(dtDetails.Rows(0).Item("numBusinessProcessID"))))

                    ddlBusinessProcess.Enabled = False
                    btnAddProcessOpportunity.Visible = False
                    btnRemoveProcessOpportunity.Visible = True
                End If

                hplCustomer.Text = dtDetails.Rows(0).Item("vcCompanyName")
                If CCommon.ToString(dtDetails.Rows(0).Item("vcContactName")) <> "" Then
                    lblContactName.Text = dtDetails.Rows(0).Item("vcContactName")
                Else
                    lblContactName.Text = ""
                End If
                If CCommon.ToString(dtDetails.Rows(0).Item("Phone")) <> "" Then
                    lblPhone.Text = dtDetails.Rows(0).Item("Phone")
                Else
                    lblPhone.Text = ""
                End If
                If CCommon.ToString(dtDetails.Rows(0).Item("PhoneExtension")) <> "" Then
                    lblPhone.Text = lblPhone.Text & "(" & CCommon.ToString(dtDetails.Rows(0).Item("PhoneExtension")) & ")"
                End If
                If CCommon.ToString(dtDetails.Rows(0).Item("vcEmail")) <> "" Then
                    btnSendEmail.Visible = True
                    btnSendEmail.Attributes.Add("onclick", "return OpenEmail('" & CCommon.ToString(dtDetails.Rows(0).Item("vcEmail")) & "','')")
                Else
                    btnSendEmail.Visible = False
                End If
                If CCommon.ToString(dtDetails.Rows(0).Item("vcCompanyType")) <> "" Then
                    lblRelationCustomerType.Text = "(" & dtDetails.Rows(0).Item("vcCompanyType") & ")"
                Else
                    lblRelationCustomerType.Text = ""
                End If
                If dtDetails.Rows(0).Item("tintCRMType") = 0 Then
                    hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProDetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&ProId=" & lngProID & "&frm1=" & GetQueryStringVal("frm")
                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 1 Then
                    hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProDetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&ProId=" & lngProID & "&frm1=" & GetQueryStringVal("frm")
                ElseIf dtDetails.Rows(0).Item("tintCRMType") = 2 Then
                    hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProDetail&klds+7kldf=fjk-las&DivId=" & dtDetails.Rows(0).Item("numDivisionID") & "&ProId=" & lngProID & "&frm1=" & GetQueryStringVal("frm")
                End If
                txtDivId.Text = dtDetails.Rows(0).Item("numDivisionID")
                lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                txtProName.Text = dtDetails.Rows(0).Item("vcprojectName")

                lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcModifiedby")), "", dtDetails.Rows(0).Item("vcModifiedby"))
                lblFinishedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcFinishedby")), "", dtDetails.Rows(0).Item("vcFinishedby"))
                lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcCreatedBy")), "", dtDetails.Rows(0).Item("vcCreatedBy"))
                lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcRecOwner")), "", dtDetails.Rows(0).Item("vcRecOwner"))
                hdnRecordOwner.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner"))
                hdnTerritory.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numTerID"))
                ViewState("AssignedTo") = CCommon.ToLong(dtDetails.Rows(0).Item("numAssignedTo"))

                imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngProID & ")")

                'MileStone1.ProjectRecOwner = dtDetails.Rows(0).Item("numRecOwner")
                LoadControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindProcessList()
            Dim objAdmin = New CAdmin()
            Dim dtTable As DataTable
            objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
            objAdmin.Mode = 1
            dtTable = objAdmin.LoadProcessList()
            ddlBusinessProcess.DataSource = dtTable
            ddlBusinessProcess.DataTextField = "Slp_Name"
            ddlBusinessProcess.DataValueField = "Slp_Id"
            ddlBusinessProcess.DataBind()
            ddlBusinessProcess.Items.Insert(0, New ListItem("-- Select One --", "0"))
        End Sub

        Protected Overrides Function OnBubbleEvent(ByVal sender As Object, ByVal e As EventArgs) As Boolean
            Dim args As CommandEventArgs = DirectCast(e, CommandEventArgs)
            'Rebind tabs
            If args.CommandName = "Rebind" Then
                'MileStone1.BindProcessControl()
                'MileStone1.BindTotalProgress()
            End If
            Return True

        End Function
        
        Sub LoadControls()
            Try
                tblMain.Controls.Clear()

                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "R"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngProID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 11
                objPageLayout.FormId = 13
                objPageLayout.PageType = 3
                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 


                dtTableInfo = ds.Tables(0)
                objProject.ProjectDetailIP()

                lblProjectDetails.Text = objProject.strProjectID
                txtProjectName.Text = objProject.strProjectID
                If objProject.ProjectStatus = 27492 Then
                    btnActDelete.Visible = False
                    btnFinish.Visible = False
                End If


                objPageControls.ProjectID = objProject.ProjectID
                objPageControls.DivisionID = lngDivId
                objPageControls.TerritoryID = objProject.TerritoryID
                objPageControls.strPageType = "Project"
                objPageControls.CreateTemplateRow(tblMain)
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)

                Dim intAddedColumns As Int16 = 0

                Dim divRow As HtmlGenericControl
                Dim divMain1 As HtmlGenericControl
                Dim divMain2 As HtmlGenericControl
                Dim divForm1 As HtmlGenericControl
                Dim divForm2 As HtmlGenericControl

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0

                Dim isFirstColumnUsed As Boolean = False
                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                Dim DataRows = (From row In dtTableInfo.AsEnumerable() Select row)
                Dim distinctNames = DataRows.OrderBy(Function(x) x("numOrder")).Select(Function(x) x("vcGroupName")).Distinct().ToList()
                For Each drGroupName As String In distinctNames
                    If drGroupName Is Nothing Then
                        drGroupName = ""
                    End If

                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = drGroupName
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)
                    dvExcutedView.RowFilter = "vcGroupName = '" & drGroupName & "'"
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND vcGroupName='" & drGroupName & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND vcGroupName='" & drGroupName & "'")
                    intCol1 = 0
                    intCol2 = 0
                    dvExcutedView.RowFilter = "vcGroupName = '" & drGroupName & "'"
                    intAddedColumns = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        'If intAddedColumns = 0 Then
                        '    divRow = New HtmlGenericControl("div")
                        '    divRow.Attributes.Add("class", "row rowpadding")

                        '    divMain1 = New HtmlGenericControl("div")
                        '    divMain1.Attributes.Add("class", "col-xs-12 col-sm-6")

                        '    divForm1 = New HtmlGenericControl("div")
                        '    divForm1.Attributes.Add("class", "form-group")

                        '    divMain2 = New HtmlGenericControl("div")
                        '    divMain2.Attributes.Add("class", "col-xs-12 col-sm-6")

                        '    divForm2 = New HtmlGenericControl("div")
                        '    divForm2.Attributes.Add("class", "form-group")

                        '    isFirstColumnUsed = False
                        'End If

                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                                dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            dr("vcValue") = objProject.GetType.GetProperty(dr("vcPropertyName")).GetValue(objProject, Nothing)
                            If CStr(dr("fld_type")) = "Label" Then
                                ' lblProjectDetails.Text = "(" & dr("vcValue") & ")"
                            End If
                        End If
                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable
                            Dim dtSelOpportunity As DataTable

                            If dr("vcPropertyName") = "AssignedTo" Or dr("vcPropertyName") = "IntPrjMgr" Then
                                If boolIntermediatoryPage = False Then
                                    If Session("PopulateUserCriteria") = 1 Then
                                        objCommon.DivisionID = txtDivId.Text
                                        objCommon.charModule = "D"
                                        objCommon.GetCompanySpecificValues1()
                                        dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objCommon.TerittoryID)
                                    ElseIf Session("PopulateUserCriteria") = 2 Then
                                        dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                    Else
                                        dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                    End If
                                End If
                            ElseIf dr("vcPropertyName") = "CusPrjMgr" Then
                                If boolIntermediatoryPage = False Then
                                    Dim fillCombo As New COpportunities
                                    With fillCombo
                                        .DivisionID = lngDivId
                                        dtData = fillCombo.ListContact().Tables(0)
                                    End With
                                End If
                            ElseIf dr("vcPropertyName") = "ContractID" Then
                                Dim objContracts As New CContracts
                                objContracts.DivisionId = txtDivId.Text
                                objContracts.UserCntID = Session("UserContactId")
                                objContracts.DomainID = Session("DomainId")
                                dtData = objContracts.GetContractDdlList()
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objProject)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If
                            End If

                            If boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, dtSelOpportunity, objProject.ProjectID)
                            Else
                                Dim ddl As DropDownList
                                ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, dtSelOpportunity, objProject.ProjectID)

                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objProject)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngProID)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If

                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If


                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next
                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If
                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetParentDIV(ByVal intColumn As Int16, ByRef isFirstColumnUsed As Boolean, ByRef divForm1 As HtmlGenericControl, ByRef divForm2 As HtmlGenericControl) As HtmlGenericControl
            If intColumn = 1 Then
                Return divForm1
            ElseIf intColumn = 2 Then
                Return divForm2
            ElseIf isFirstColumnUsed Then
                isFirstColumnUsed = False
                Return divForm2
            Else
                isFirstColumnUsed = True
                Return divForm1
            End If
        End Function

        Function SaveProjects() As Integer
            Try
                objProject.ProjectID = lngProID
                objProject.OpportunityId = 0
                objProject.UserCntID = Session("UserContactID")
                objProject.DomainID = Session("DomainId")
                objProject.DivisionID = CCommon.ToLong(txtDivId.Text)
                For Each dr As DataRow In dtTableInfo.Rows
                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        objPageControls.SetValueForStaticFields(dr, objProject, radOppTab.MultiPage)
                    End If
                Next


                arrOutPut = objProject.Save()

                If arrOutPut(0) = 0 Then
                    Return 1
                    Exit Function
                End If
                objProject.ProjectID = arrOutPut(0)

                'Added By Sachin Sadhu||Date:29thJul2014
                'Purpose :To Added Projects data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = CCommon.ToLong(arrOutPut(0))
                objWfA.SaveWFProjectsQueue()
                'end of code

                'objProject.SaveMilestone()

                If objProject.AssignedTo > 0 And ViewState("AssignedTo") <> objProject.AssignedTo Then
                    CAlerts.SendAlertToAssignee(4, hdnRecordOwner.Value, objProject.AssignedTo, objProject.ProjectID, Session("DomainID")) 'bugid 767
                    ViewState("AssignedTo") = objProject.AssignedTo
                End If

                Return 0
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnOpenEmailProjectSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenEmailProjectSummary.Click
            Try
                Dim Str As String
                Dim StrBody As String = ""
                Dim vcNotes As String = ""
                Dim ts As TimeSpan
                Dim objEmail As New Email
                Dim dtTemplateTable As New DataTable
                dtTemplateTable = objEmail.GetEmailTemplateByCode("#SYS#EMAIL_ALERT:PROJECT_TASK_SUMMARY", CCommon.ToLong(Session("DomainID")))
                Dim strSubject As String = ""
                If dtTemplateTable.Rows.Count > 0 Then
                    strSubject = dtTemplateTable.Rows(0)("vcSubject")
                    StrBody = dtTemplateTable.Rows(0)("vcDocDesc")


                    Dim dtTable As DataTable
                    Dim objProject = New ProjectIP()
                    objProject.DomainID = Session("DomainId")
                    objProject.ProjectID = hdnProjectID.Value
                    objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtTable = objProject.GetProjectSummaryDetails()
                    If dtTable.Rows.Count > 0 Then

                        strSubject = strSubject.Replace("##ProjectName##", dtTable.Rows(0)("vcProjectID"))
                        StrBody = StrBody.Replace("##ProjectName##", dtTable.Rows(0)("vcProjectID"))
                        StrBody = StrBody.Replace("##ProjectDescription##", dtTable.Rows(0)("txtComments"))
                        StrBody = StrBody.Replace("##ProjectCompanyFor##", dtTable.Rows(0)("vcCompanyName"))
                        StrBody = StrBody.Replace("##ProjectCompanyBy##", dtTable.Rows(0)("vcByCompanyName"))

                        Dim starttag As Int16
                        Dim endtag As Int16
                        starttag = StrBody.IndexOf("{##ProjectLineTask## }")
                        endtag = StrBody.IndexOf("{##/ProjectLineTask## }")
                        Dim htmlItemTable As String
                        htmlItemTable = StrBody.Substring(starttag, endtag - starttag - 1)
                        Dim tblTaskLineItems As String = ""
                        Dim strColor As String = ""

                        For Each dr As DataRow In dtTable.Rows
                            Dim totalMinuteSpan As TimeSpan
                            Dim totalBalanceMinuteSpan As TimeSpan
                            totalMinuteSpan = TimeSpan.FromMinutes(dr("TaskDurationComplete"))
                            totalBalanceMinuteSpan = TimeSpan.FromMinutes(dr("BalanceTime"))
                            Dim strTempHtmlTable As String
                            strTempHtmlTable = htmlItemTable
                            strTempHtmlTable = strTempHtmlTable.Replace("##color##", strColor)
                            strTempHtmlTable = strTempHtmlTable.Replace("##TotalProgress##", dr("numTotalProgress"))
                            strTempHtmlTable = strTempHtmlTable.Replace("##FinishDate##", dr("dtFinishDate"))
                            strTempHtmlTable = strTempHtmlTable.Replace("##TaskName##", dr("vcTaskName"))
                            strTempHtmlTable = strTempHtmlTable.Replace("##AssignedTo##", dr("AssignedTo"))
                            If (CCommon.ToInteger(Session("UserGroupID")) = 2 AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitDisplayTimeToExternalUsers")) = True) Then
                                strTempHtmlTable = strTempHtmlTable.Replace("##BalanceTime##", "")
                                strTempHtmlTable = strTempHtmlTable.Replace("##DurationTakenForProject##", "")
                                strTempHtmlTable = strTempHtmlTable.Replace("##StartTiime##", "")
                                strTempHtmlTable = strTempHtmlTable.Replace("##FinishTime##", "")
                            Else
                                strTempHtmlTable = strTempHtmlTable.Replace("##BalanceTime##", "" & Convert.ToInt32(totalBalanceMinuteSpan.TotalHours).ToString() & " hr " & totalBalanceMinuteSpan.Minutes.ToString() & " min")
                                strTempHtmlTable = strTempHtmlTable.Replace("##DurationTakenForProject##", "" & Convert.ToInt32(totalMinuteSpan.TotalHours).ToString() & " hr " & totalMinuteSpan.Minutes.ToString() & " min")
                                strTempHtmlTable = strTempHtmlTable.Replace("##StartTiime##", dr("StartTiime"))
                                strTempHtmlTable = strTempHtmlTable.Replace("##FinishTime##", dr("FinishTime"))
                            End If

                            tblTaskLineItems = tblTaskLineItems & strTempHtmlTable
                        Next
                        StrBody = StrBody.Replace(htmlItemTable, tblTaskLineItems)
                        StrBody = StrBody.Replace("{##ProjectLineTask## }", "")
                        StrBody = StrBody.Replace("{##/ProjectLineTask## }", "")
                        Session("Content") = StrBody
                        Session("Subj") = strSubject
                        Str = "window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&Lsemail=" & hdnAssignToForContract.Value & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')"
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Email", Str, True)

                    End If
                End If

            Catch ex As Exception

            End Try
        End Sub
        Private Sub btnOpenEmailContract_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenEmailContract.Click
            Try
                Dim Str As String
                Dim StrBody As String = ""
                Dim vcNotes As String = ""
                Dim ts As TimeSpan
                Dim objEmail As New Email
                Dim dtTemplateTable As New DataTable
                dtTemplateTable = objEmail.GetEmailTemplateByCode("#SYS#EMAIL_ALERT:TIMECONTRACT_USED", CCommon.ToLong(Session("DomainID")))
                Dim strSubject As String = ""
                If dtTemplateTable.Rows.Count > 0 Then
                    strSubject = dtTemplateTable.Rows(0)("vcSubject")
                    StrBody = dtTemplateTable.Rows(0)("vcDocDesc")

                    If CCommon.ToLong(hdnSelectedTaskID.Value) > 0 Then
                        Dim objStagePercentageDetailsTaskNotes As New StagePercentageDetailsTaskNotes
                        objStagePercentageDetailsTaskNotes.TaskID = CCommon.ToLong(hdnSelectedTaskID.Value)
                        Dim dt As DataTable = objStagePercentageDetailsTaskNotes.GetByTaskID()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            vcNotes = CCommon.ToString(dt.Rows(0)("vcNotes"))
                        End If
                    End If

                    ts = TimeSpan.FromSeconds(hdnContractBalanceRemaining.Value)

                    strSubject = strSubject.Replace("##TaskTitle##", hdnSelectedTaskTitle.Value)
                    StrBody = StrBody.Replace("##CustomerContactName##", hdnAssignToForContractName.Value)
                    StrBody = StrBody.Replace("##TaskTitle##", hdnSelectedTaskTitle.Value)
                    StrBody = StrBody.Replace("##TaskNotes##", "<br/>" & vcNotes)
                    StrBody = StrBody.Replace("##TaskDuration##", hdnActualTaskCompletionTime.Value.Split(":")(0) + " hr " + hdnActualTaskCompletionTime.Value.Split(":")(1) + " min")
                    StrBody = StrBody.Replace("##TaskStartEndTime##", Convert.ToDateTime(hdnStartDateTime.Value).ToString("hh:mm tt") + " to " + Convert.ToDateTime(hdnEndDateTime.Value).ToString("hh:mm tt"))
                    StrBody = StrBody.Replace("##TaskDate##", Convert.ToDateTime(hdnEndDateTime.Value).ToString("dd/MM/yyyy"))
                    StrBody = StrBody.Replace("##TimeContractBalance##", CCommon.ToString(Math.Floor(ts.TotalHours)) + " hour(s) " + CCommon.ToString(ts.Minutes) + " minutes")
                    Session("Content") = StrBody
                    Session("Subj") = strSubject
                    Str = "window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&Lsemail=" & hdnAssignToForContract.Value & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')"
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Email", Str, True)
                End If

            Catch ex As Exception

            End Try
        End Sub
        Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
            objProject = New ProjectIP
            objProject.DomainID = CCommon.ToLong(Session("DomainID"))
            objProject.ProjectID = CCommon.ToLong(hdnProjectID.Value)
            If objProject.IsTaskPending() Then
                ShowMessage("Project could not be completed until all tasks are finished.")
            Else

                objProject = New ProjectIP
                objProject.ProjectID = lngProID
                objProject.OpportunityId = 0
                objProject.UserCntID = Session("UserContactID")
                objProject.DomainID = Session("DomainId")
                objProject.ProjectStatus = 27492
                objProject.UpdateProjectStatus()
            End If
            LoadSavedInformation()
        End Sub
        Private Sub btnTSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTSave.Click
            Try
                If SaveProjects() = 1 Then
                    ShowMessage("No of Incidents Not Sufficient")
                    Exit Sub
                End If
                SaveCusField()
                dtProjectProcess = objProject.SalesProcessDtlByProId

                LoadSavedInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnTSaveAndCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTSaveAndCancel.Click
            Try
                If SaveProjects() = 1 Then
                    ShowMessage("No of Incidents Not Sufficient")
                    Exit Sub
                End If
                SaveCusField()
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnTCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Function FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
            Try

                If objCommon Is Nothing Then objCommon = New CCommon
                With objCommon
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .Filter = Trim(strName) & "%"
                    ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    ddlCombo.DataTextField = "vcCompanyname"
                    ddlCombo.DataValueField = "numDivisionID"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub PageRedirect()
            Try
                If GetQueryStringVal("frm") = "prospects" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "accounts" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx?SelectedIndex=1")
                ElseIf GetQueryStringVal("frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContacList.aspx")
                ElseIf GetQueryStringVal("frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal("typ") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ProjectStatus" Then
                    Response.Redirect("../reports/frmProjectStatus.aspx")
                ElseIf GetQueryStringVal("frm") = "OPPDetails" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?OpID=" & GetQueryStringVal("OpID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AdvSearch" Then
                    Response.Redirect("../Admin/frmAdvProjectsRes.aspx")
                Else : Response.Redirect("../projects/frmProjectList.aspx")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function FillContact(ByVal ddl As DropDownList, ByVal DivisionID As Long)
            Try
                Dim objOpport As New COpportunities
                With objOpport
                    .DivisionID = DivisionID
                End With
                With ddl
                    .DataSource = objOpport.ListContact().Tables(0).DefaultView()
                    .DataTextField = "Name"
                    .DataValueField = "numcontactId"
                    .DataBind()
                    .ClearSelection()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                Dim objAdmin As New CAdmin

                objAdmin.DomainID = Session("DomainId")
                objAdmin.ModeType = MileStone1.ModuleType.Projects
                objAdmin.ProjectID = lngProID
                Try
                    objAdmin.RemoveStagePercentageDetails()

                    objProject.ProjectID = lngProID
                    objProject.DomainID = Session("DomainID")
                    If objProject.DeleteProjects = False Then
                        ShowMessage("Dependent Records Exists.Cannot be deleted.")
                    Else : Response.Redirect("../projects/frmProjectList.aspx")
                    End If
                Catch ex As Exception
                    If ex.Message = "DEPENDANT" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "alert", "alert('Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.')", True)
                    End If
                End Try
                LoadSavedInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(lngProID, 0, Session("DomainID"), objPageControls.Location.Projects, radOppTab, 13)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(lngProID, 0, Session("DomainID"), objPageControls.Location.Projects, radOppTab.MultiPage)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                ViewState("IntermediatoryPage") = False
                boolIntermediatoryPage = False
                tblMain.Controls.Clear()
                LoadControls()
                ControlSettings()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnTSave.Visible = False
                btnTSaveAndCancel.Visible = False
                btnEdit.Visible = True
            Else
                btnTSave.Visible = True
                btnTSaveAndCancel.Visible = True
                btnEdit.Visible = False
            End If
        End Sub

        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub

        Sub BindProjectIncomeAndExpense()
            Try
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                objProject.ProjectID = lngProID
                Dim ds As DataSet = objProject.GetProjectExpenseReports

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    gvReports.DataSource = ds.Tables(0)
                    gvReports.DataBind()

                    If ds.Tables.Count > 1 Then
                        lblAR.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monIncomeAR")))
                        lblNonLaborExpense.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monNonLaborExpense")))
                        lblIncome.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monTotalIncome")))
                        lblAP.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monExpenseAP")))
                        lblLaborExpense.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monLaborExpense")))
                        lblPendingExpense.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monIncomePendingExpense")))
                        lblGrossProfit.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00;(" & CCommon.ToString(Session("Currency")) & " #,##0.00);-}", CCommon.ToDouble(ds.Tables(1).Rows(0)("monGrossProfit")))

                        If CCommon.ToDouble(ds.Tables(1).Rows(0)("monTotalIncome")) >= 0 Then
                            lblIncome.CssClass = "text-green"
                        Else
                            lblIncome.CssClass = "text-red"
                        End If

                        If CCommon.ToDouble(ds.Tables(1).Rows(0)("monGrossProfit")) >= 0 Then
                            lblGrossProfit.CssClass = "text-green"
                        Else
                            lblGrossProfit.CssClass = "text-red"
                        End If

                    Else
                        lblAR.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", 0)
                        lblNonLaborExpense.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", 0)
                        lblIncome.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", 0)
                        lblAP.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", 0)
                        lblLaborExpense.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", 0)
                        lblPendingExpense.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00}", 0)
                        lblGrossProfit.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00;(" & CCommon.ToString(Session("Currency")) & " #,##0.00);-}", 0)
                    End If
                End If




                'Dim ReportTotalIncome, ReportTotalExpense, ReportBalance As Decimal
                'ReportTotalIncome = CCommon.ToDecimal(ds.Tables(0).Compute("SUM(ExpenseAmount)", "iTEType=1 or iTEType=6 or iTEType=8"))
                'ReportTotalExpense = CCommon.ToDecimal(ds.Tables(0).Compute("SUM(ExpenseAmount)", "iTEType=2 or iTEType=3 or iTEType=4 or iTEType=5 or iTEType=7 or iTEType=9"))
                'ReportBalance = ReportTotalIncome - ReportTotalExpense

                'lblReportTotalIncome.Text = String.Format("{0:##,#00.00}", ReportTotalIncome)
                'lblReportTotalExpense.Text = String.Format("{0:##,#00.00}", ReportTotalExpense)
                'lblReportBalance.Text = "<font color='" & IIf(ReportBalance > 0, "green", "red") & "'>" & String.Format("{0:##,#00.00}", ReportBalance) & "</font>"
                'imgUPDown.ImageUrl = "../images/" & IIf(ReportBalance > 0, "BalanceUp.png", "BalanceDown.png")

                Dim lobjPayrollExpenses As New PayrollExpenses
                lobjPayrollExpenses.DomainID = Session("DomainId")
                lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                lobjPayrollExpenses.Mode = 2
                Dim dtProjectCommission As DataTable = lobjPayrollExpenses.GetProjectCommission(lngProID)

                tblProjectCommission.Visible = IIf(dtProjectCommission.Rows.Count > 0, True, False)

                gvProjectCommission.DataSource = dtProjectCommission
                gvProjectCommission.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                LoadSavedInformation()
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radOppTab.SelectedIndex)
                PersistTable.Save(boolOnlyURL:=True)

                BindProjectIncomeAndExpense()
                If radOppTab.SelectedTab.Value = "ProjectTask" Then
                    BindWIP(False)
                End If
                If radOppTab.SelectedTab.Value = "ManageProjectTask" Then
                    BindWIP(True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub BindWIP(ByVal isManageWIP As Boolean)
            Try
                Dim objProject As New ProjectIP
                objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                objProject.ProjectID = CCommon.ToLong(GetQueryStringVal("ProId"))
                Dim dt As DataTable = objProject.GetMilestones()
                If dt.Rows.Count > 0 Then
                    hdnTotalProgress.Value = Convert.ToString(dt.Rows(0)("numTotalCompletedProgress"))
                End If
                If isManageWIP Then
                    rptMilestonesManageWIP.DataSource = dt
                    rptMilestonesManageWIP.DataBind()
                Else
                    rptMilestones.DataSource = dt
                    rptMilestones.DataBind()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Protected Sub btnAddProcessOpportunity_Click(sender As Object, e As EventArgs)
            Try
                If ddlBusinessProcess.SelectedValue > 0 Then
                    Dim objAdmin = New CAdmin()
                    Dim output As String = "1"
                    objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
                    objAdmin.UserCntID = CCommon.ToLong(Session("UserContactId"))
                    objAdmin.SalesProsID = ddlBusinessProcess.SelectedValue
                    objAdmin.OppID = 0
                    objAdmin.ProjectID = hdnProjectID.Value
                    objAdmin.ManageProcessForOpportunity()
                    BindWIP(1)
                End If
                LoadSavedInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnRemoveProcessOpportunity_Click(sender As Object, e As EventArgs)
            Try
                Dim objAdmin = New CAdmin()
                Dim output As String = "1"
                objAdmin.OppID = 0
                objAdmin.ProjectID = hdnProjectID.Value

                objAdmin.DeleteProcessFromOpportunity()
                LoadSavedInformation()
                BindWIP(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rdManageWIPDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs)
            Try
                BindWIP(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Protected Sub btnDateRange_Click(sender As Object, e As EventArgs)
            Try
                LoadSavedInformation()
                BindWIP(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptMilestonesManageWIP_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptStagesManageWIP") Is Nothing Then
                        objProject = New ProjectIP
                        objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                        objProject.ProjectID = CCommon.ToLong(hdnProjectID.Value)
                        objProject.MilestoneID = CCommon.ToString(drv("numMileStoneID"))
                        objProject.MilestoneName = CCommon.ToString(drv("vcMileStoneName"))
                        DirectCast(e.Item.FindControl("rptStagesManageWIP"), Repeater).DataSource = objProject.GetStages()
                        DirectCast(e.Item.FindControl("rptStagesManageWIP"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnUpdateProjectTask_Click(sender As Object, e As EventArgs)
            LoadSavedInformation()

            If radOppTab.SelectedTab.Value = "ProjectTask" Then
                BindWIP(False)
            End If
            If radOppTab.SelectedTab.Value = "ManageProjectTask" Then
                BindWIP(True)
            End If
        End Sub

        Protected Sub rptStagesManageWIP_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptTasksManageWIP") Is Nothing Then
                        objProject = New ProjectIP
                        objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                        objProject.ProjectID = CCommon.ToLong(hdnProjectID.Value)
                        objProject.MilestoneID = CCommon.ToString(drv("numMileStoneID"))
                        objProject.StageDetailsID = CCommon.ToString(drv("numStageDetailsId"))
                        objProject.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        DirectCast(e.Item.FindControl("rptTasksManageWIP"), Repeater).DataSource = objProject.GetTasks(2)
                        DirectCast(e.Item.FindControl("rptTasksManageWIP"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptTasksManageWIP_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If dtEmployee Is Nothing Then
                        objProject = New ProjectIP
                        objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                        objProject.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        Dim tempDateTime As DateTime = DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset"))
                        dtEmployee = New DataTable
                        dtEmployee = objProject.GetEmployeesWithCapacityLoad(CCommon.ToLong(hdnProjectID.Value), CCommon.ToShort(hdnDateRange.Value), New DateTime(rdManageWIPDate.SelectedDate.Value.Year, rdManageWIPDate.SelectedDate.Value.Month, rdManageWIPDate.SelectedDate.Value.Day, tempDateTime.Hour, tempDateTime.Minute, tempDateTime.Second))

                        dtEmployee.DefaultView.Sort = "numCapacityLoad ASC"
                        dtEmployee = dtEmployee.DefaultView.ToTable()
                    End If

                    If dtTeam Is Nothing Then
                        objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                        objProject.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        Dim tempDateTime As DateTime = DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset"))
                        dtTeam = New DataTable
                        dtTeam = objProject.GetTeamsWithCapacityLoad(CCommon.ToLong(hdnProjectID.Value), CCommon.ToShort(hdnDateRange.Value), New DateTime(rdManageWIPDate.SelectedDate.Value.Year, rdManageWIPDate.SelectedDate.Value.Month, rdManageWIPDate.SelectedDate.Value.Day, tempDateTime.Hour, tempDateTime.Minute, tempDateTime.Second))

                        dtTeam.DefaultView.Sort = "numCapacityLoad ASC"
                        dtTeam = dtTeam.DefaultView.ToTable()
                    End If

                    If dtGrades Is Nothing Then
                        Dim objStageGradeDetails As New StageGradeDetails
                        objStageGradeDetails.DomainID = CCommon.ToLong(Session("DomainID"))
                        dtGrades = objStageGradeDetails.GetByProcess(CCommon.ToLong(hdnBuildProcess.Value))
                    End If

                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If CCommon.ToLong(drv("numAssignedTo")) = CCommon.ToLong(Session("UserContactID")) AndAlso Not e.Item.FindControl("trTask") Is Nothing Then
                        Dim tr As HtmlTableRow = DirectCast(e.Item.FindControl("trTask"), HtmlTableRow)
                        tr.Attributes.Add("style", "background-color: rgba(255, 255, 0, 0.27);")
                    End If

                    If Not e.Item.FindControl("ddlAssignedTo") Is Nothing Then
                        Dim listItem As ListItem

                        If Not dtEmployee Is Nothing AndAlso dtEmployee.Rows.Count > 0 Then
                            For Each drEmployee As DataRow In dtEmployee.Rows
                                listItem = New ListItem

                                If Not dtGrades Is Nothing AndAlso dtGrades.Select("numTaskId=" & CCommon.ToLong(drv("numReferenceTaskId")) & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID"))).Length > 0 Then
                                    listItem.Text = CCommon.ToString(drEmployee("vcEmployeeName")) & " (" & CCommon.ToString(drEmployee("numCapacityLoad")) & "%)" & " " & dtGrades.Select("numTaskId=" & CCommon.ToLong(drv("numReferenceTaskId")) & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID")))(0)("vcGradeId")
                                    listItem.Attributes.Add("Grade", dtGrades.Select("numTaskId=" & CCommon.ToLong(drv("numReferenceTaskId")) & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID")))(0)("vcGradeId"))
                                Else
                                    listItem.Text = CCommon.ToString(drEmployee("vcEmployeeName")) & " (" & CCommon.ToString(drEmployee("numCapacityLoad")) & "%)"
                                    listItem.Attributes.Add("Grade", "")
                                End If

                                listItem.Value = CCommon.ToLong(drEmployee("numEmployeeID"))

                                If CCommon.ToLong(drEmployee("numCapacityLoad")) <= 50 Then
                                    listItem.Attributes.Add("style", "background-color:#ccfcd9")
                                ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 75 Then
                                    listItem.Attributes.Add("style", "background-color:#ffffcd")
                                ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 100 Then
                                    listItem.Attributes.Add("style", "background-color:#ffdc7d")
                                ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 101 Then
                                    listItem.Attributes.Add("style", "background-color:#ffd1d1")
                                End If

                                If CCommon.ToLong(drEmployee("numEmployeeID")) = CCommon.ToLong(drv("numAssignedTo")) Then
                                    listItem.Selected = True

                                    If CCommon.ToLong(drEmployee("numCapacityLoad")) <= 50 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ccfcd9")
                                    ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 75 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ffffcd")
                                    ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 100 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ffdc7d")
                                    ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 101 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ffd1d1")
                                    End If
                                End If

                                DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Items.Add(listItem)
                            Next
                        End If

                        listItem = New ListItem
                        listItem.Text = "-- Select One --"
                        listItem.Value = "0"
                        listItem.Attributes.Add("style", "background-color:#ffffff")

                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Items.Insert(0, listItem)
                    End If

                    If Not e.Item.FindControl("ddlTeam") Is Nothing Then
                        Dim listItem As ListItem

                        If Not dtTeam Is Nothing AndAlso dtTeam.Rows.Count > 0 Then
                            For Each drTeam As DataRow In dtTeam.Rows
                                listItem = New ListItem
                                listItem.Text = CCommon.ToString(drTeam("vcTeamName")) & " (" & CCommon.ToString(drTeam("numCapacityLoad")) & "%)"
                                listItem.Value = CCommon.ToLong(drTeam("numTeamID"))

                                If CCommon.ToLong(drTeam("numCapacityLoad")) <= 50 Then
                                    listItem.Attributes.Add("style", "background-color:#ccfcd9")
                                ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 75 Then
                                    listItem.Attributes.Add("style", "background-color:#ffffcd")
                                ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 100 Then
                                    listItem.Attributes.Add("style", "background-color:#ffdc7d")
                                ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 101 Then
                                    listItem.Attributes.Add("style", "background-color:#ffd1d1")
                                End If

                                If CCommon.ToLong(drTeam("numTeamID")) = CCommon.ToLong(drv("numTeam")) Then
                                    listItem.Selected = True

                                    If CCommon.ToLong(drTeam("numCapacityLoad")) <= 50 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ccfcd9")
                                    ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 75 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ffffcd")
                                    ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 100 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ffdc7d")
                                    ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 101 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ffd1d1")
                                    End If
                                End If

                                DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Items.Add(listItem)
                            Next
                        End If

                        listItem = New ListItem
                        listItem.Text = "-- Select One --"
                        listItem.Value = "0"
                        listItem.Attributes.Add("style", "background-color:#ffffff")

                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Items.Insert(0, listItem)
                    End If

                    If Not e.Item.FindControl("ddlGrade") Is Nothing Then
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("A+", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("A", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("A-", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("B+", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("B", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("B-", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("C+", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("C", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("C-", CCommon.ToLong(drv("numReferenceTaskId"))))

                        Dim listItem As New ListItem
                        listItem = New ListItem
                        listItem.Text = "-- Select One --"
                        listItem.Value = "0"
                        listItem.Attributes.Add("style", "background-color:#ffffff")

                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Insert(0, listItem)

                        If Not DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).SelectedItem.Attributes("Grade") Is Nothing Then
                            Dim grade As String = CCommon.ToString(DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).SelectedItem.Attributes("Grade"))
                            If Not DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.FindByValue(grade) Is Nothing Then
                                DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.FindByValue(grade).Selected = True
                                DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).BackColor = System.Drawing.ColorTranslator.FromHtml(DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.FindByValue(grade).Attributes("style").Replace("background-color:", ""))
                            End If
                        End If
                    End If

                    If CCommon.ToBool(drv("bitTaskStarted")) Then
                        DirectCast(e.Item.FindControl("btnStartAgain"), Button).Visible = True
                        DirectCast(e.Item.FindControl("txtTaskHours"), HtmlInputGenericControl).Attributes.Add("disabled", "")
                        DirectCast(e.Item.FindControl("txtTaskMinutes"), HtmlInputGenericControl).Attributes.Add("disabled", "")
                    End If

                    DirectCast(e.Item.FindControl("txtTaskHours"), HtmlInputGenericControl).ID = DirectCast(e.Item.FindControl("txtTaskHours"), HtmlInputGenericControl).ID & CCommon.ToString(drv("numTaskID"))
                    DirectCast(e.Item.FindControl("txtTaskMinutes"), HtmlInputGenericControl).ID = DirectCast(e.Item.FindControl("txtTaskMinutes"), HtmlInputGenericControl).ID & CCommon.ToString(drv("numTaskID"))
                    If Not e.Item.FindControl("plhDocuments") Is Nothing Then
                        Dim plhDocuments As PlaceHolder
                        plhDocuments = DirectCast(e.Item.FindControl("plhDocuments"), PlaceHolder)
                        Dim dtDocLinkName As New DataTable
                        Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                        With objOpp
                            .DomainID = Session("DomainID")
                            .DivisionID = CCommon.ToLong(CCommon.ToString(drv("numTaskID")))
                            dtDocLinkName = .GetDocumentFilesLink("T")
                        End With

                        If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then

                            Dim docRows = (From dRow In dtDocLinkName.Rows
                                           Where CCommon.ToString(dRow("cUrlType")) = "L"
                                           Select New With {Key .fileName = dRow("VcFileName"), .fileId = dRow("numGenericDocID")}).Distinct()
                            If docRows.Count() > 0 Then
                                Dim _docName = String.Empty
                                Dim countId = 0

                                For Each dr In docRows
                                    Dim strDocName = CCommon.ToString(dr.fileName)
                                    Dim fileID = CCommon.ToString(dr.fileId)
                                    Dim strFileLogicalPath = String.Empty
                                    If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                        Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                        Dim strFileSize As String = f.Length.ToString
                                        strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                                        'objCommon.AddAttchmentToSession(strDocName, CCommon.GetDocumentPath(Session("DomainID")) & strDocName, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName, strFileType, strFileSize)
                                    End If

                                    Dim docLink As New HyperLink

                                    Dim imgDelete As New ImageButton
                                    imgDelete.ImageUrl = "../images/Delete24.png"
                                    imgDelete.Height = 13
                                    imgDelete.ToolTip = "Delete"
                                    imgDelete.ID = "imgDelete" + CCommon.ToString(countId)
                                    imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                                    Dim Space As New LiteralControl
                                    Space.Text = "&nbsp;"

                                    docLink.Text = strDocName
                                    docLink.NavigateUrl = strFileLogicalPath
                                    docLink.ID = "hpl" + CCommon.ToString(countId)
                                    docLink.Target = "_blank"
                                    Dim createDocDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                                    createDocDiv.Controls.Add(docLink)
                                    createDocDiv.Controls.Add(Space)
                                    createDocDiv.Controls.Add(imgDelete)
                                    plhDocuments.Controls.Add(createDocDiv)
                                    countId = countId + 1
                                Next
                                'imgDocument.Visible = True
                            Else
                                'imgDocument.Visible = False
                            End If

                            Dim linkRows = (From dRow In dtDocLinkName.Rows
                                            Where CCommon.ToString(dRow("cUrlType")) = "U"
                                            Select New With {Key .linkURL = dRow("VcFileName"), Key .linkName = dRow("VcDocName"), .fileId = dRow("numGenericDocID")}).Distinct()
                            If linkRows.Count() > 0 Then
                                Dim _linkName = String.Empty
                                Dim _linkURL = String.Empty
                                Dim fileLinkID = String.Empty
                                Dim countLinkId = 0

                                For Each dr In linkRows
                                    '_linkName = _linkName + ", " + CCommon.ToString(dr.linkName)
                                    '_linkURL = _linkURL + ",<br/> " + CCommon.ToString(dr.linkURL)

                                    _linkName = CCommon.ToString(dr.linkName)
                                    _linkURL = CCommon.ToString(dr.linkURL)
                                    fileLinkID = CCommon.ToString(dr.fileId)
                                    Dim uriBuilder As UriBuilder = New UriBuilder(_linkURL)
                                    'UriBuilder builder = New UriBuilder(myUrl)
                                    'charityNameText.NavigateUrl = builder.Uri.AbsoluteUri

                                    Dim link As New HyperLink
                                    Dim imgLinkDelete As New ImageButton
                                    imgLinkDelete.ImageUrl = "../images/Delete24.png"
                                    imgLinkDelete.Height = 13
                                    imgLinkDelete.ToolTip = "Delete"
                                    imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(countLinkId)
                                    imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileLinkID & ");return false;")

                                    link.Text = _linkName
                                    link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                                    'link.NavigateUrl = _linkURL
                                    link.ID = "hplLink" + CCommon.ToString(countLinkId)
                                    link.Target = "_blank"
                                    Dim createLinkDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                                    createLinkDiv.Controls.Add(link)
                                    createLinkDiv.Controls.Add(imgLinkDelete)
                                    plhDocuments.Controls.Add(createLinkDiv)
                                    countLinkId = countLinkId + 1
                                Next
                                'imgLink.Visible = True
                            Else
                                'imgLink.Visible = False
                            End If
                        Else
                            'imgDocument.Visible = False
                            'imgLink.Visible = False
                        End If
                    End If

                    If m_aryRightsManageProjectTasksTab(RIGHTSTYPE.UPDATE) = 0 Then
                        If Not e.Item.FindControl("ddlAssignedTo") Is Nothing Then
                            DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = False
                        End If
                    ElseIf m_aryRightsManageProjectTasksTab(RIGHTSTYPE.UPDATE) = 1 Then
                        If CCommon.ToLong(Session("UserContactID")) <> CCommon.ToLong(hdnRecordOwner.Value) Then
                            If Not e.Item.FindControl("ddlAssignedTo") Is Nothing Then
                                DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = False
                            End If
                        End If
                    ElseIf m_aryRightsManageProjectTasksTab(RIGHTSTYPE.UPDATE) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If CCommon.ToLong(hdnTerritory.Value) = 0 Then
                            If Not e.Item.FindControl("ddlAssignedTo") Is Nothing Then
                                DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = False
                            End If
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hdnTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = False Then
                                If Not e.Item.FindControl("ddlAssignedTo") Is Nothing Then
                                    DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = False
                                End If
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptMilestones_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptStages") Is Nothing Then
                        objProject = New ProjectIP
                        objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                        objProject.ProjectID = CCommon.ToLong(hdnProjectID.Value)
                        objProject.MilestoneID = CCommon.ToString(drv("numMileStoneID"))
                        objProject.MilestoneName = CCommon.ToString(drv("vcMileStoneName"))
                        DirectCast(e.Item.FindControl("rptStages"), Repeater).DataSource = objProject.GetStages()
                        DirectCast(e.Item.FindControl("rptStages"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptStages_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptTasks") Is Nothing Then
                        objProject = New ProjectIP
                        objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                        objProject.ProjectID = CCommon.ToLong(hdnProjectID.Value)
                        objProject.MilestoneID = CCommon.ToString(drv("numMileStoneID"))
                        objProject.StageDetailsID = CCommon.ToString(drv("numStageDetailsId"))
                        objProject.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        DirectCast(e.Item.FindControl("rptTasks"), Repeater).DataSource = objProject.GetTasks(1)
                        DirectCast(e.Item.FindControl("rptTasks"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptTasks_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If m_aryRightsProjectTasksTab(RIGHTSTYPE.UPDATE) = 0 Then
                        DirectCast(e.Item.FindControl("divTaskControlsProject"), HtmlGenericControl).Visible = False
                    ElseIf m_aryRightsProjectTasksTab(RIGHTSTYPE.UPDATE) = 1 Then
                        If CCommon.ToLong(Session("UserContactID")) <> CCommon.ToLong(drv("numAssignedTo")) Then
                            DirectCast(e.Item.FindControl("divTaskControlsProject"), HtmlGenericControl).Visible = False
                        End If
                    ElseIf m_aryRightsProjectTasksTab(RIGHTSTYPE.UPDATE) = 2 Then
                        DirectCast(e.Item.FindControl("divTaskControlsProject"), HtmlGenericControl).Visible = False
                    End If

                    If CCommon.ToLong(drv("numAssignedTo")) = CCommon.ToLong(Session("UserContactID")) AndAlso Not e.Item.FindControl("trTask") Is Nothing Then
                        Dim tr As HtmlTableRow = DirectCast(e.Item.FindControl("trTask"), HtmlTableRow)
                        tr.Attributes.Add("style", "background-color: rgba(255, 255, 0, 0.27);")
                    End If

                    Dim numEsitmatedTaskMinutes As Integer = 0
                    Dim numActualTaskMinutes As Integer = 0

                    If CCommon.ToString(drv("vcEstimatedTaskTime")).Contains(":") Then
                        numEsitmatedTaskMinutes = (CCommon.ToInteger(CCommon.ToString(drv("vcEstimatedTaskTime")).Split(":")(0)) * 60) + CCommon.ToInteger(CCommon.ToString(drv("vcEstimatedTaskTime")).Split(":")(1))
                    End If

                    If CCommon.ToString(drv("vcActualTaskTime")).Contains(":") Then
                        numActualTaskMinutes = (CCommon.ToInteger(CCommon.ToString(drv("vcActualTaskTime")).Split(":")(0)) * 60) + CCommon.ToInteger(CCommon.ToString(drv("vcActualTaskTime")).Split(":")(1))
                    End If

                    If CCommon.ToBool(drv("bitTaskCompleted")) Then
                        DirectCast(e.Item.FindControl("imgTimeIconn"), HtmlImage).Visible = True
                        DirectCast(e.Item.FindControl("chkTimeAdded"), CheckBox).Visible = True
                        DirectCast(e.Item.FindControl("chkTimeAdded"), CheckBox).Attributes.Add("onclick", "addRemoveTime(" + numActualTaskMinutes.ToString() + "," + drv("numTaskId").ToString() + ",'" + drv("dtStartDate").ToString() + "','" + drv("dtFinishDate").ToString() + "','" + drv("numAssignedTo").ToString() + "','" + drv("vcTaskName").ToString() + "','" + drv("vcActualTaskTime").ToString() + "','" + drv("vcEmail").ToString() + "','" + drv("vcCustomerName").ToString() + "')")

                        Dim decDifferene As Double = Math.Ceiling(((numEsitmatedTaskMinutes - numActualTaskMinutes) / numActualTaskMinutes) * 100)

                        If decDifferene > 5 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = String.Format("{0:#,##0}", decDifferene) & " % under"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-green"
                        ElseIf decDifferene <= 5 And decDifferene >= -5 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = "On time"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-green"
                        ElseIf decDifferene < -5 AndAlso decDifferene >= -25 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = String.Format("{0:#,##0}", decDifferene) & " % Over"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-orange"
                        ElseIf decDifferene < -25 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = String.Format("{0:#,##0}", decDifferene) & " % Over"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-red"
                        End If
                    Else
                        DirectCast(e.Item.FindControl("imgTimeIconn"), HtmlImage).Visible = False
                        DirectCast(e.Item.FindControl("chkTimeAdded"), CheckBox).Visible = False
                    End If

                    If Not e.Item.FindControl("plhDocuments") Is Nothing Then
                        Dim plhDocuments As PlaceHolder
                        plhDocuments = DirectCast(e.Item.FindControl("plhDocuments"), PlaceHolder)
                        Dim dtDocLinkName As New DataTable
                        Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                        With objOpp
                            .DomainID = Session("DomainID")
                            .DivisionID = CCommon.ToLong(CCommon.ToString(drv("numTaskID")))
                            dtDocLinkName = .GetDocumentFilesLink("T")
                        End With

                        If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then

                            Dim docRows = (From dRow In dtDocLinkName.Rows
                                           Where CCommon.ToString(dRow("cUrlType")) = "L"
                                           Select New With {Key .fileName = dRow("VcFileName"), .fileId = dRow("numGenericDocID")}).Distinct()
                            If docRows.Count() > 0 Then
                                Dim _docName = String.Empty
                                Dim countId = 0

                                For Each dr In docRows
                                    Dim strDocName = CCommon.ToString(dr.fileName)
                                    Dim fileID = CCommon.ToString(dr.fileId)
                                    Dim strFileLogicalPath = String.Empty
                                    If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                        Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                        Dim strFileSize As String = f.Length.ToString
                                        strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                                        'objCommon.AddAttchmentToSession(strDocName, CCommon.GetDocumentPath(Session("DomainID")) & strDocName, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName, strFileType, strFileSize)
                                    End If

                                    Dim docLink As New HyperLink

                                    Dim imgDelete As New ImageButton
                                    imgDelete.ImageUrl = "../images/Delete24.png"
                                    imgDelete.Height = 13
                                    imgDelete.ToolTip = "Delete"
                                    imgDelete.ID = "imgDelete" + CCommon.ToString(countId)
                                    imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                                    Dim Space As New LiteralControl
                                    Space.Text = "&nbsp;"

                                    docLink.Text = strDocName
                                    docLink.NavigateUrl = strFileLogicalPath
                                    docLink.ID = "hpl" + CCommon.ToString(countId)
                                    docLink.Target = "_blank"
                                    Dim createDocDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                                    createDocDiv.Controls.Add(docLink)
                                    createDocDiv.Controls.Add(Space)
                                    createDocDiv.Controls.Add(imgDelete)
                                    plhDocuments.Controls.Add(createDocDiv)
                                    countId = countId + 1
                                Next
                                'imgDocument.Visible = True
                            Else
                                'imgDocument.Visible = False
                            End If

                            Dim linkRows = (From dRow In dtDocLinkName.Rows
                                            Where CCommon.ToString(dRow("cUrlType")) = "U"
                                            Select New With {Key .linkURL = dRow("VcFileName"), Key .linkName = dRow("VcDocName"), .fileId = dRow("numGenericDocID")}).Distinct()
                            If linkRows.Count() > 0 Then
                                Dim _linkName = String.Empty
                                Dim _linkURL = String.Empty
                                Dim fileLinkID = String.Empty
                                Dim countLinkId = 0

                                For Each dr In linkRows
                                    '_linkName = _linkName + ", " + CCommon.ToString(dr.linkName)
                                    '_linkURL = _linkURL + ",<br/> " + CCommon.ToString(dr.linkURL)

                                    _linkName = CCommon.ToString(dr.linkName)
                                    _linkURL = CCommon.ToString(dr.linkURL)
                                    fileLinkID = CCommon.ToString(dr.fileId)
                                    Dim uriBuilder As UriBuilder = New UriBuilder(_linkURL)
                                    'UriBuilder builder = New UriBuilder(myUrl)
                                    'charityNameText.NavigateUrl = builder.Uri.AbsoluteUri

                                    Dim link As New HyperLink
                                    Dim imgLinkDelete As New ImageButton
                                    imgLinkDelete.ImageUrl = "../images/Delete24.png"
                                    imgLinkDelete.Height = 13
                                    imgLinkDelete.ToolTip = "Delete"
                                    imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(countLinkId)
                                    imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileLinkID & ");return false;")

                                    link.Text = _linkName
                                    link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                                    'link.NavigateUrl = _linkURL
                                    link.ID = "hplLink" + CCommon.ToString(countLinkId)
                                    link.Target = "_blank"
                                    Dim createLinkDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                                    createLinkDiv.Controls.Add(link)
                                    createLinkDiv.Controls.Add(imgLinkDelete)
                                    plhDocuments.Controls.Add(createLinkDiv)
                                    countLinkId = countLinkId + 1
                                Next
                            End If
                        End If
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Function GradeDropDownItem(ByVal grade As String, ByVal taskID As Long) As ListItem
            Try
                Dim listItem As New ListItem
                listItem.Value = grade

                Dim listCapacityLoad As New System.Collections.Generic.List(Of Double)

                If Not dtEmployee Is Nothing AndAlso dtEmployee.Rows.Count > 0 Then
                    For Each drEmployee As DataRow In dtEmployee.Rows
                        If Not dtGrades Is Nothing AndAlso dtGrades.Select("numTaskId=" & taskID & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID")) & " AND vcGradeId='" & grade & "'").Length > 0 Then
                            listCapacityLoad.Add(CCommon.ToDouble(drEmployee("numCapacityLoad")))
                        End If
                    Next
                End If


                Dim capacityLoad As Double = If(listCapacityLoad.Count > 0, listCapacityLoad.Average(), 0)

                If capacityLoad <= 50 Then
                    listItem.Attributes.Add("style", "background-color:#ccfcd9")
                ElseIf capacityLoad >= 51 AndAlso capacityLoad <= 75 Then
                    listItem.Attributes.Add("style", "background-color:#ffffcd")
                ElseIf capacityLoad >= 76 AndAlso capacityLoad <= 100 Then
                    listItem.Attributes.Add("style", "background-color:#ffdc7d")
                ElseIf capacityLoad >= 101 Then
                    listItem.Attributes.Add("style", "background-color:#ffd1d1")
                End If

                listItem.Text = grade & " (" & capacityLoad & "%)"

                Return listItem
            Catch ex As Exception
                Throw
            End Try
        End Function
        <WebMethod()>
        Public Shared Function WebMethodGetProjectSummary(ByVal DomainID As Integer, ByVal ProjectID As Long, ByVal ClientTimeZoneOffset As Int16) As String
            Try
                Dim dtTable As DataTable
                Dim objProject = New ProjectIP()
                objProject.DomainID = DomainID
                objProject.ProjectID = ProjectID
                objProject.ClientTimeZoneOffset = ClientTimeZoneOffset
                dtTable = objProject.GetProjectSummaryDetails()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtTable, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        Protected Sub chkIsForExternalUser_CheckedChanged(sender As Object, e As EventArgs)
            Dim objProject = New ProjectIP()
            objProject.DomainID = Convert.ToInt64(Session("DomainID"))
            objProject.ProjectID = GetQueryStringVal("ProId")
            objProject.bitDisplayTimeToExternalUsers = chkIsForExternalUser.Checked
            objProject.UpdateDisplayTimeToExternalUsers()
        End Sub

        <WebMethod()>
        Public Shared Function WebMethodSaveProjectId(ByVal DomainID As Integer, ByVal strProjectID As String, ByVal ProjectID As Long) As String
            Try
                Dim objProject = New ProjectIP()
                objProject.DomainID = DomainID
                objProject.ProjectName = strProjectID
                objProject.ProjectID = ProjectID
                objProject.SaveProjectID()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject("1", Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function


        <WebMethod()>
        Public Shared Function WebMethodAddRemoveTimeFromContract(ByVal DomainID As Long, ByVal DivisionId As Long, ByVal TaskId As Long, ByVal numSecounds As Long, ByVal Type As Long, ByVal isProjectActivityEmail As Long) As String
            Try
                Dim objProject = New ProjectIP()
                objProject.DomainID = DomainID
                objProject.DivisionID = DivisionId
                objProject.TaskContractId = TaskId
                objProject.numSecounds = numSecounds
                objProject.isProjectActivityEmail = isProjectActivityEmail
                objProject.strType = Type
                Dim res As Integer = 0
                Dim output As String = ""
                res = objProject.AddRemoveTimeFromContract()
                Dim json As String = String.Empty
                output = CCommon.ToString(res) & "#" & CCommon.ToString(objProject.balanceContractTime)
                Dim lst As New List(Of String)
                lst.Add(CCommon.ToString(res))
                lst.Add(CCommon.ToString(objProject.balanceContractTime))
                json = JsonConvert.SerializeObject(lst, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function
    End Class
End Namespace
