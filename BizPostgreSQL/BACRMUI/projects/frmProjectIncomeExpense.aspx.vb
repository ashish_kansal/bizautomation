﻿Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contract

Partial Public Class frmProjectIncomeExpense
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                BindProject()
                UltraChart1.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindProject()
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            objProject.Mode = 1
            ddlProjects.DataTextField = "vcProjectName"
            ddlProjects.DataValueField = "numProId"
            ddlProjects.DataSource = objProject.GetOpenProject()
            ddlProjects.DataBind()
            ddlProjects.Items.Insert(0, "--Select One--")
            ddlProjects.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlProjects_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProjects.SelectedIndexChanged
        Try
            Dim objProject As New Project
            Dim ds As DataSet
            objProject.DomainID = Session("DomainID")
            objProject.ProjectID = ddlProjects.SelectedValue
            objProject.Mode = 0
            ds = objProject.GetProjectIncomeExpense
            If ds.Tables(0).Rows.Count > 0 Then
                lblBillableHours.Text = ds.Tables(0).Rows(0)("BillableHours")
                lblNonBillableHours.Text = ds.Tables(0).Rows(0)("NonBillableHours")
                lblIncome.Text = String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Income"))
                lblExpense.Text = String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Expense"))
                lblBalance.Text = "Project Profit/Loss: " & String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Income") - ds.Tables(0).Rows(0)("Expense"))
                If ds.Tables(0).Rows(0)("Income") > ds.Tables(0).Rows(0)("Expense") Then
                    lblBalance.ForeColor = Color.Green
                Else
                    lblBalance.ForeColor = Color.Red
                End If
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                lblComments.Text = ds.Tables(1).Rows(0)("txtComments")
                If CCommon.ToLong(ds.Tables(1).Rows(0)("numContractId")) > 0 Then
                    lblContract.Text = ds.Tables(1).Rows(0)("vcContractName")
                    Dim objContract As New CContracts
                    Dim dtTable As DataTable
                    objContract.ContractID = ds.Tables(1).Rows(0)("numContractId")
                    objContract.UserCntId = Session("UserContactId")
                    objContract.DomainId = Session("DomainId")
                    dtTable = objContract.GetContractDtl()
                    If dtTable.Rows.Count > 0 Then
                        lblAmountBalance.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
                        lblHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
                        lblDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
                        If dtTable.Rows(0).Item("bitincidents") = True Then
                            lblIncidents.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
                        Else : lblIncidents.Text = 0
                        End If
                    Else
                        lblAmountBalance.Text = 0
                        lblHours.Text = 0
                        lblDays.Text = 0
                        lblIncidents.Text = 0
                    End If
                End If
            End If
            If ds.Tables(2).Rows.Count > 0 Then
                UltraChart1.DataSource = ds.Tables(2)
                UltraChart1.DataBind()
                UltraChart1.Visible = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


End Class