﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities

Public Class ProjectTeams
    Inherits BACRMUserControl
    Dim objCommon As CCommon
    Public _ProjectId As Integer

    Public Property ProjectId As Integer
        Get
            Return _ProjectId
        End Get
        Set(ByVal value As Integer)
            _ProjectId = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radExternalCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindDatagrid()
            End If

            btnAddExternalResources.Attributes.Add("onclick", "return SaveExternalUser()")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            objCommon = New CCommon
            Dim dtUserList As DataTable
            Dim objProject As New Project
            objProject.DomainID = Session("DomainId")
            objProject.ProjectID = ProjectId

            objProject.bytemode = 1 '1:Internal User 2:External User
            dtUserList = objProject.GetProjectTeamRights()
            gvUsers.DataSource = dtUserList
            gvUsers.DataBind()

            objProject.bytemode = 2 '1:Internal User 2:External User
            dtUserList = objProject.GetProjectTeamRights()
            gvExternalUsers.DataSource = dtUserList
            gvExternalUsers.DataBind()
            'objCommon.sb_FillComboFromDBwithSel(gvUsers.FindControl("ddRight"), 204, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim dtTable As DataTable
    Public Sub gvUsers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddRights As DropDownList = e.Row.FindControl("ddRight")
            If IsNothing(dtTable) Then
                dtTable = objCommon.GetMasterListItems(204, Session("DomainID"))
            End If
            ddRights.DataSource = dtTable
            ddRights.DataTextField = "vcData"
            ddRights.DataValueField = "numListItemID"
            ddRights.DataBind()
            ddRights.Items(2).Attributes.Add("disabled", "true")
            If (ddRights.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numRights")) IsNot Nothing) Then
                ddRights.ClearSelection()
                ddRights.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numRights")).Selected = True
            End If

            'Bind Project Team 
            Dim ddlRole As DropDownList = e.Row.FindControl("ddlRole")
            objCommon.sb_FillComboFromDBwithSel(ddlRole, 384, Session("DomainID"))
            If (ddlRole.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numProjectRole")) IsNot Nothing) Then
                ddlRole.ClearSelection()
                ddlRole.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numProjectRole")).Selected = True
            End If
        End If
    End Sub

    Private Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Try
            Save(1)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save(ByVal UserType As Integer)
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numContactId")
            dt.Columns.Add("numRights")
            dt.Columns.Add("numProjectRole")

            Dim dr As DataRow
            Dim lngVendorAPAccountID As Long

            Dim gv As GridView

            If UserType = 1 Then
                gv = gvUsers
            ElseIf UserType = 2 Then
                gv = gvExternalUsers
            End If

            For Each gvr As GridViewRow In gv.Rows
                dr = dt.NewRow()
                dr("numContactId") = gv.DataKeys(gvr.DataItemIndex).Value
                dr("numRights") = CType(gvr.FindControl("ddRight"), DropDownList).SelectedValue
                dr("numProjectRole") = CType(gvr.FindControl("ddlRole"), DropDownList).SelectedValue
                dt.Rows.Add(dr)
            Next

            ds.Tables.Add(dt)

            Dim objProject As New Project
            Dim dtProTeam As DataTable

            objProject.DomainID = Session("DomainId")
            objProject.UserCntID = Session("UserContactID")
            objProject.ProjectID = ProjectId
            objProject.bytemode = UserType '1:Internal User 2:External User
            objProject.Mode = 1 '1: ADD/DELETE/UPDATE 2:ADD
            objProject.ContactList = ds.GetXml()

            objProject.ManageProjectTeamRights()
            BindDatagrid()

            Dim args As New CommandEventArgs("Rebind", [String].Empty)
            RaiseBubbleEvent(Nothing, args)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSaveExternalResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveExternalResources.Click
        Try
            Save(2)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub radExternalCompany_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radExternalCompany.ItemsRequested
        Try
            If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then

                If objCommon Is Nothing Then objCommon = New CCommon
                With objCommon
                    .DomainID = Session("DomainID")
                    .Filter = Trim(e.Text) & "%"
                    .UserCntID = Session("UserContactID")
                    radExternalCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    radExternalCompany.DataTextField = "vcCompanyname"
                    radExternalCompany.DataValueField = "numDivisionID"
                    radExternalCompany.DataBind()
                End With
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radExternalCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radExternalCompany.SelectedIndexChanged
        Try
            If radExternalCompany.SelectedValue <> "" Then
                ddlExternalUser.Items.Clear()
                Dim objOpportunities As New COpportunities
                objOpportunities.DivisionID = radExternalCompany.SelectedValue
                ddlExternalUser.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
                ddlExternalUser.DataTextField = "ContactType"
                ddlExternalUser.DataValueField = "numcontactId"
                ddlExternalUser.DataBind()
                ddlExternalUser.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddExternalResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddExternalResources.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numContactId")
            dt.Columns.Add("numRights")
            dt.Columns.Add("numProjectRole")

            Dim dr As DataRow
            Dim lngVendorAPAccountID As Long

            dr = dt.NewRow()
            dr("numContactId") = ddlExternalUser.SelectedValue
            dr("numRights") = "0"
            dr("numProjectRole") = "0"
            dt.Rows.Add(dr)

            ds.Tables.Add(dt)

            Dim objProject As New Project
            Dim dtProTeam As DataTable

            objProject.DomainID = Session("DomainId")
            objProject.UserCntID = Session("UserContactID")
            objProject.ProjectID = ProjectId
            objProject.bytemode = 2 '1:Internal User 2:External User
            objProject.Mode = 2 '1: ADD/DELETE/UPDATE 2:ADD
            objProject.ContactList = ds.GetXml()

            objProject.ManageProjectTeamRights()
            BindDatagrid()

            Dim args As New CommandEventArgs("Rebind", [String].Empty)
            RaiseBubbleEvent(Nothing, args)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDeleteExternalResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteExternalResources.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numContactId")
            dt.Columns.Add("numRights")
            dt.Columns.Add("numProjectRole")

            Dim dr As DataRow
            Dim lngVendorAPAccountID As Long

            For Each gvr As GridViewRow In gvExternalUsers.Rows
                Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                If chk.Checked Then
                    dr = dt.NewRow()
                    dr("numContactId") = gvExternalUsers.DataKeys(gvr.DataItemIndex).Value
                    dr("numRights") = CType(gvr.FindControl("ddRight"), DropDownList).SelectedValue
                    dr("numProjectRole") = CType(gvr.FindControl("ddlRole"), DropDownList).SelectedValue
                    dt.Rows.Add(dr)
                End If
            Next

            ds.Tables.Add(dt)

            Dim objProject As New Project
            Dim dtProTeam As DataTable

            objProject.DomainID = Session("DomainId")
            objProject.UserCntID = Session("UserContactID")
            objProject.ProjectID = ProjectId
            objProject.bytemode = 2 '1:Internal User 2:External User
            objProject.Mode = 3 '1: ADD/DELETE/UPDATE 2:ADD 3:Delete
            objProject.ContactList = ds.GetXml()

            objProject.ManageProjectTeamRights()
            BindDatagrid()

            Dim args As New CommandEventArgs("Rebind", [String].Empty)
            RaiseBubbleEvent(Nothing, args)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class