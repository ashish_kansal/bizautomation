<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProjectAdd.aspx.vb"
    Inherits="BACRM.UserInterface.Projects.frmProjectAdd" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Project</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript">
        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Customer")
                return false;
            }
            if (document.getElementById("txtProjectName").value == 0) {
                alert("Enter Project Name")
                document.getElementById("txtProjectName").focus();
                return false;
            }
            if (document.getElementById("ddlCusomer").value == 0) {
                alert("Select Customer Side Project Manager")
                document.getElementById("ddlCusomer").focus();
                return false;
            }
            if (document.getElementById('ctl00_Content_calenderStartDate_txtDate').value == 0) {
                alert("Enter Planned Start Date")
                document.getElementById('ctl00_Content_calenderStartDate_txtDate').focus();
                return false;
            }
            if (document.getElementById('ctl00_Content_calenderFinishDate_txtDate').value == 0) {
                alert("Enter Requested Finish Date")
                document.getElementById('ctl00_Content_calenderFinishDate_txtDate').focus();
                return false;
            }
        }
        function focus() {
            document.getElementById('txtDescription').focus()
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="btn btn-primary"></asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Project
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="scr" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Customer<span class="text-red"> *</span></label>
                <div>
                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" DropDownWidth="600px" Width="100%"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static"
                    ShowMoreResultsBox="true"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Project Name<span class="text-red"> *</span></label>
                <asp:TextBox ID="txtProjectName" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Internal Project Manager</label>
                <asp:DropDownList ID="ddlInternal" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Customer Side Project Manager<span class="text-red"> *</span></label>
                <asp:DropDownList ID="ddlCusomer" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Planned Start<span style="color: red"> *</span></label>
                <BizCalendar:Calendar ID="calenderStartDate" TabIndex="7" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Requested Finish<span style="color: red"> *</span></label>
                <BizCalendar:Calendar ID="calenderFinishDate" TabIndex="8" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Time & Expense Account</label>
                <asp:DropDownList ID="ddlAccount" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Description</label>
                <asp:TextBox ID="txtDescription" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
            </div>
        </div>
    </div>
</asp:Content>
