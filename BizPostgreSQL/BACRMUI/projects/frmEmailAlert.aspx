﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmailAlert.aspx.vb"
    Inherits=".frmEmailAlert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title></title>
    <script language="javascript" type="text/javascript">
        function Close() {
            self.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>&nbsp;
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"></asp:Button>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="normal1">
                <asp:CheckBox ID="chkStageAssignedNotification" Text="Send following email template when a stage is assigned to user:"
                    runat="server" CssClass="signup" />
                &nbsp;
                <asp:DropDownList runat="server" ID="ddlEmailTemplate1" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1">
                <asp:CheckBox ID="chkLateNotification" Text="Send following email template to all dependant stage assignee when a sub-stage is not completed by its due date:"
                    runat="server" CssClass="signup" />
                &nbsp;
                <asp:DropDownList runat="server" ID="ddlEmailTemplate2" CssClass="signup">
                </asp:DropDownList>
                <br />
                and CC the following (separate email addresses by commas):&nbsp;
                <asp:TextBox runat="server" ID="txtCCEmail" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        
    </table>
    </form>
</body>
</html>
