﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MileStone.ascx.vb"
    Inherits=".MileStone1" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../common/frmComments.ascx" TagName="frmComments" TagPrefix="uc1" %>
<script>
    var DomainID = '<%= Session("DomainId")%>';
    var OppID = '<%= OppID%>';
    var ProjectID='<%=ProjectID%>'
</script>
<style>
    .boxForList .box-primary .box-header {
        background-color: #1473b4;
    }

    .boxForList .box-header .form-inline label {
        color: #fff !important;
        margin-right: 10px;
    }

    .boxForList .box-header .form-inline .form-control {
        padding: 9px;
        height: 27px;
        margin-right: 10px;
    }

    .boxForList .box-header .form-inline .btn-xs {
        margin-right: 10px;
    }

    .indivStages .form-inline .form-control, .indivStages .form-inline .btn-xs, .indivStages .form-inline label {
        margin-right: 1px;
    }

    .boxForList .box-title a {
        color: #ffffff !important;
        font-size: 17px;
        display: block;
        padding-top: 4px;
        padding-left: 10px;
    }

    .boxForList .box-body {
        padding: 0px !important;
        min-height: 96px;
        border: 1px solid #c8c2c2;
    }

    .text-delete-danger {
        color: #dd4b39;
    }

    .boxForList .box-title {
        display: block !important;
    }

    .boxForList .box-header {
        padding: 4px 6px 9px 5px !important
    }

    .box-header .col-md-10 {
        padding-left: 0px !important;
    }

    .boxForList .internalBoxHeader {
        background-color: #e2e2e2;
        padding-top: 6px;
    }

    .boxForList .box .box-group > .box {
        margin-bottom: 2px !important;
    }

    .boxForList .indivStages {
        padding: 5px 0px 5px;
        background-color: rgba(243, 243, 243, 0.25);
        margin: 4px;
        border: 1px solid #e1e1e1;
    }

        .boxForList .indivStages .divstageDetails {
            padding: 5px 9px;
        }

        .boxForList .indivStages .indivTask {
            background-color: rgba(221, 221, 221, 0.31);
            padding: 5px 9px;
        }

    .stageName {
        font-size: 17px;
        color: #1473b4;
    }

    .btn-round {
        border-radius: 10px !important;
    }

    .datetimepicker .input-group-addon {
        padding: 3px 10px 3px 10px !important;
        color: #1473b4 !important;
        background-color: transparent;
        border: 0px;
    }

    .datetimepicker .dtmCalender {
        background-color: transparent;
        border: 0px;
        font-weight: bold;
        padding-left: 0px;
    }

    .modal-header .close {
        margin-top: 6px;
        color: #fff;
        opacity: 10;
    }

    .modal-header {
        padding: 8px;
        background-color: #0d8ed2;
        color: #fff;
    }

    #taskModal .col-md-6, #taskModal .col-md-12, #taskModal .col-md-3, #taskModal .col-md-2, #taskModal .col-md-1 {
        padding: 5px;
    }

    .text-large {
        font-size: 16px;
    }

    .autocomplete-items {
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        top: 94%;
        left: 1%;
        right: 0;
        position: absolute;
        width: 98%;
    }

        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

            .autocomplete-items div:hover {
                /*when hovering an item:*/
                background-color: #e9e9e9;
            }

    .autocomplete-active {
        /*when navigating through the items using the arrow keys:*/
        background-color: DodgerBlue !important;
        color: #ffffff;
    }

    .btn-github {
        background-color: #767575 !important;
    }
    
</style>
<script type="text/javascript">
    function $Search(name, tag, elm) {
        var tag = tag || "*";
        var elm = elm || document;
        var elements = (tag == "*" && elm.all) ? elm.all : elm.getElementsByTagName(tag);
        var returnElements = [];
        var current;
        var length = elements.length;

        for (var i = 0; i < length; i++) {
            current = elements[i];
            if (current.id.indexOf(name) > 0) {
                returnElements.push(current);
            }
        }
        return returnElements[0];
    };
    function OpenStageDocuments(a) {
        window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=PS&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=900,height=450,scrollbars=yes,resizable=yes')
        return false;
    }
    function OpenTransfer(url) {
        var StageName;
        StageName = $('#txtStageName').val();
        url = url + '&StageName=' + StageName;

        window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
        return false;
    }
    //var prg_width = 60;
    function progress(ProgInPercent, Container, progress, prg_width) {
        //Set Total Width
        var OuterDiv = document.getElementById(Container);
        OuterDiv.style.width = prg_width + 'px';
        OuterDiv.style.height = '5px';

        if (ProgInPercent > 100) {
            ProgInPercent = 100;
        }
        //Set Progress Percentage
        var node = document.getElementById(progress);
        node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
    }
    function EditSvae(format) {
        if (document.getElementById('txtStageName').value == 0) {
            alert("Enter Stage Name")
            document.getElementById('txtStageName').focus();
            return false;
        }
        if ($Search('calStartDate_txtDate').value == 0) {
            alert("Enter Start Date")
            $Search('calStartDate_txtDate').focus();
            return false;
        }
        if ($Search('calDueDate_txtDate').value == 0) {
            alert("Enter Due Date")
            $Search('calDueDate_txtDate').focus();
            return false;
        }

        if (!isDate($Search('calStartDate_txtDate').value, format)) {
            alert("Enter Valid Start Date");
        }

        if (!isDate($Search('calDueDate_txtDate').value, format)) {
            alert("Enter Valid Due Date");
        }

        if (compareDates($Search('calStartDate_txtDate').value, format, $Search('calDueDate_txtDate').value, format) == 1) {
            alert("Due Date must be greater than or equal to Start Date");
            return false;
        }

    }
    function AddSave(format) {
        if (document.getElementById('txtAStageName').value == 0) {
            alert("Enter Stage Name")
            document.getElementById('txtAStageName').focus();
            return false;
        }
        if ($Search('calAStartDate_txtDate').value == 0) {
            alert("Enter Start Date")
            $Search('calAStartDate_txtDate').focus();
            return false;
        }
        if ($Search('calADueDate_txtDate').value == 0) {
            alert("Enter Due Date")
            $Search('calADueDate_txtDate').focus();
            return false;
        }

        if (!isDate($Search('calAStartDate_txtDate').value, format)) {
            alert("Enter Valid Start Date");
        }

        if (!isDate($Search('calADueDate_txtDate').value, format)) {
            alert("Enter Valid Due Date");
        }

        if (compareDates($Search('calAStartDate_txtDate').value, format, $Search('calADueDate_txtDate').value, format) == 1) {
            alert("Due Date must be greater than or equal to Start Date");
            return false;
        }
    }
    function CheckDependencies() {
        if (document.getElementById("ddlProjectStage").value == 0) {
            alert('Select stage');
            document.getElementById("ddlProjectStage").focus();
            return false;
        }
        return true;
    }
    function CheckStagePercentage() {
        if (document.getElementById("rdbStageType_0").checked) {
            window.open("../projects/frmStagePercentage.aspx?Type=Parent&StageID=" + document.getElementById("hfStageID1").value, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
        }
        else {
            window.open("../projects/frmStagePercentage.aspx?Type=Child&StageID=" + document.getElementById("hfStageID1").value, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
        }
        return false;
    }
    function CheckNumber(cint, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        if (cint == 1) {
            if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        if (cint == 2) {
            if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    }

    function openEmpAvailability() {
        var ProID;
        var ProjName;
        var StageName;

        ProID = $('#hdnProjectID').val();
        ProjName = $('#hdnProjectName').val();
        StageName = $('#hdnStageName').val();

        window.open("../ActionItems/frmEmpAvailability.aspx?frm=Tickler&OrgName=" + ProjName + "&BProcessName=" + StageName + "&ProID=" + ProID, '', 'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
        return false;
    }

    function hideStageProgressAlert() {
        $("#divStageProgress").hide();
        return false;
    }

    function hideOpptoOrderAlert() {
        $("#divOpptoOrderAlert").hide();
        return false;
    }

    function OpptoOrderDeal() {
        var a;
        var b;

        a = $('#hdnid').val();
        b = $('#hdnOppid').val();

        $.ajax({
            type: "POST",
            url: "../include/SaveData.ashx",
            data: { id: a, value: '1' },
            // DO NOT SET CONTENT TYPE to json
            // contentType: "application/json; charset=utf-8", 
            // DataType needs to stay, otherwise the response object
            // will be treated as a single string
            dataType: "html",
            success: function (response) {
                document.location.href = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + b;
            },
            failure: function (response) {
                alert(response.d);
            }

        });
        return false;
    }
</script>
<style type="text/css">
    .MilestoneHeader {
        font-family: Arial;
        font-size: 8pt;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        color: White;
        background-color: #667193;
    }

    .SelectedNodeStyle {
        border: 2px solid highlight;
        padding: 3px 5px;
    }

    .NodeStyle {
        color: Black;
        padding: 3px 5px;
    }
</style>
<div class="col-md-12">
    <div class="pull-left">
        <div class="form-group">
            <label>Total Progress:&nbsp;&nbsp;<asp:Label ID="lblTotalProgress" runat="server" class="badge bg-light-blue"></asp:Label></label>
            <div class="progress progress-xs progress-striped active">
                <div id="TotalProgress" runat="server" class="progress-bar progress-bar-primary"></div>
            </div>
        </div>
    </div>
    <div class=" pull-right">
        <div class="form-inline">
            <label>Business Process : </label>
            <select class="form-control" style="width: 250px" id="ddlProcesses"></select>
            <button type="button" id="btnAddProcessOpportunity" class="btn btn-primary" onclick="AddProcesstoProjectorOportunity()">Add Process</button>
            <button type="button" id="btnRemoveProcessOpportunity" class="btn btn-danger" onclick="DeleteProcessFromOpportunity()">Remove Process</button>
            <button type="button" id="btnGanttChart" class="btn btn-info" onclick="openGanttChart()">Gantt Chart</button>
            <input type="hidden" id="hdnConfigurationId" value="0" />
            <input type="hidden" id="hdnTaskValidatorId" value="0" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12" style="margin-top: 10px !important;">
        <div class="box-group boxForList milestoneStages" id="DropDownListaccordion">
        </div>
    </div>
</div>
<div id="taskModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Task</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="hdnStageTaskDetailsId" />
                <input type="hidden" id="hdnIsAnyPendingTask" value="0" />
                <input type="hidden" id="hdnTaskRowId" />
                <div class="col-md-6">
                    <label><span class="text-danger">*</span>Task</label>
                    <input type="text" class="form-control" id="txtTaskName" autocomplete="off" maxlength="100" />
                    <div id="txtTaskNameautocomplete-list" class="autocomplete-items" style="display: none">
                        <div>No record found</div>
                    </div>
                </div>
                <div class="col-md-1" style="width: 12%;">
                    <label>Hours</label>
                    <input type="number" class="form-control" id="txtTaskHours" max="24" min="0" />
                </div>
                <div class="col-md-1" style="width: 12%;">
                    <label>Minutes</label>
                    <input type="number" class="form-control" id="txtTaskMinutes" max="60" min="1" />
                </div>

                <div class="col-md-3">
                    <label>Assign to</label>
                    <select class="form-control" id="ddlTaskAssignTo">
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-add-task" onclick="AddTasktoStage()">Add Task</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<div id="ganttChartModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Gantt Chart</h4>
            </div>
            <div class="modal-body">
                <div id="embedded-Gantt">
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
<script src="../JavaScript/GanttChart/jsgantt.js"></script>
<script src="../JavaScript/GanttChart/index.js"></script>
<link href="../JavaScript/GanttChart/jsgantt.css" rel="stylesheet" />
<script>
    var ProcessType = '<%= ProType%>';
    var savedBusinessProcessId = '<%= BusinesProcessId%>';
    $(document).ready(function () {
        BindProcessList(savedBusinessProcessId);
    })
    function openGanttChart() {
        start('pt');
        $("#ganttChartModal").modal("show")
    }
    function BindProcessList(selectedProcessId) {
        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',Mode:'" + ProcessType + "'}";
        var itemAppend = '';
        ;
        $("#ddlProcesses").empty();
        if (parseFloat(selectedProcessId) > 0) {
            var Slp_Name ='<%= BusinesProcessName%>'
            itemAppend = "<option selected value=" + selectedProcessId + ">" + Slp_Name + "</option>";
            $("#ddlProcesses").append(itemAppend);
            $("#ddlProcesses").val(selectedProcessId);
            $("#btnAddProcessOpportunity").css("display", "none");
            $("#btnRemoveProcessOpportunity").css("display", "inline-block");
            $("#btnGanttChart").css("display", "inline-block");

            $("#ddlProcesses").prop("disabled", "disable");
            BindStagesMilestone();
        } else {
            $("#btnRemoveProcessOpportunity").css("display", "none");
            $("#btnGanttChart").css("display", "none");

            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetProcessList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    itemAppend = "<option value=\"0\">--Select One--</option>";
                    $("#ddlProcesses").append(itemAppend);
                    itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        itemAppend = itemAppend + "<option value=" + value.Slp_Id + ">" + value.Slp_Name + "</option>";
                    });
                    $("#ddlProcesses").append(itemAppend);

                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
    }
    function BindStagesMilestone() {
        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + savedBusinessProcessId + "',Mode:'" + ProcessType + "'}";
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetConfigurationOnProcessList",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse.length > 0) {
                    var ProcessDetails = Jresponse[0];
                    SetDefaultSetupForConfig(ProcessDetails.tintConfiguration);
                    $("#hdnConfigurationId").val(ProcessDetails.tintConfiguration);
                    $("#hdnTaskValidatorId").val(ProcessDetails.numTaskValidatorId);
                }
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
                $("#UpdateProgress").css("display", "none");
            }
        });
    }
    //Auto Suggest Search Class teacher
    function throttle(f, delay) {
        let timer = null;
        return function () {
            let context = this, args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(function () {
                f.apply(context, args);
            },
                delay || 100);
        };
    }
    var TaskcurrentFocus = -1;
    $(document).ready(function () {
        $("#txtTaskName").keypress(throttle(function () {
            searchServerTask();
        }));
        $("#txtTaskName").keydown(function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the TaskcurrentFocus variable:*/
                TaskcurrentFocus++;
                /*and and make the current item more visible:*/
                addTaskActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the TaskcurrentFocus variable:*/
                TaskcurrentFocus--;
                /*and and make the current item more visible:*/
                addTaskActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (TaskcurrentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[TaskcurrentFocus].click();
                }
            }
        });
    });
    function addTaskActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeTaskActive(x);
        if (TaskcurrentFocus >= x.length) TaskcurrentFocus = 0;
        if (TaskcurrentFocus < 0) TaskcurrentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[TaskcurrentFocus].classList.add("autocomplete-active");
    }
    function removeTaskActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (let i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        let x = document.getElementsByClassName("autocomplete-items");
        for (let i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != $("#txtTaskName")) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    function searchServerTask() {
        $("#txtTaskNameautocomplete-list").css("display", "block");
        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',OppId:'<%= OppID%>',strTaskName:'" + $("#txtTaskName").val() + "'}";
        $.ajax({
            type: 'POST',
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetFilteredTaskList",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var Jresponse = $.parseJSON(response.d);
                $("#txtTaskNameautocomplete-list").html("");
                if (Jresponse.length > 0) {
                    $.each(Jresponse, function (index, iData) {
                        let indivItems =
                            "<div id=" + iData.Id + " onclick='selectTask(\"" + iData.numTaskId + "\", \"" + iData.vcTaskName + "\", \"" + iData.numHours + "\", \"" + iData.numMinutes + "\", \"" + iData.numAssignTo + "\")'>"
                            + iData.vcTaskName
                            + "</div>";
                        $("#txtTaskNameautocomplete-list").append(indivItems);
                    });

                } else {
                    $("#txtTaskNameautocomplete-list").append('<div>No record found</div>');
                }
            }
        });
    }
    function selectTask(numTaskId, vcTaskName, numHours, numMinutes, numAssignTo) {
        $("#txtTaskName").val(vcTaskName);
        $("#txtTaskHours").val(numHours);
        $("#txtTaskMinutes").val(numMinutes);
        $("#ddlTaskAssignTo").val(numAssignTo);
        $("#txtTaskNameautocomplete-list").css("display", "none");
    }
    $(document).click(function () {
        $("#txtTaskNameautocomplete-list").css("display", "none");
    })
    var MileStoneArray = [];
    function SetDefaultSetupForConfig(configId) {
        MileStoneArray = [];
        if (configId == 1) {
            MileStoneArray.push("" + configId + "_12_100");
        } else if (configId == 2) {
            MileStoneArray.push("" + configId + "_6_50");
            MileStoneArray.push("" + configId + "_12_100");
        } else if (configId == 3) {
            MileStoneArray.push("" + configId + "_15_33");
            MileStoneArray.push("" + configId + "_16_66");
            MileStoneArray.push("" + configId + "_12_100");
        } else if (configId == 4) {
            MileStoneArray.push("" + configId + "_3_25");
            MileStoneArray.push("" + configId + "_6_50");
            MileStoneArray.push("" + configId + "_9_75");
            MileStoneArray.push("" + configId + "_12_100");
        } else if (configId == 5) {
            MileStoneArray.push("" + configId + "_2_20");
            MileStoneArray.push("" + configId + "_5_40");
            MileStoneArray.push("" + configId + "_7_60");
            MileStoneArray.push("" + configId + "_10_80");
            MileStoneArray.push("" + configId + "_12_100");
        }
        var itemAppend = '';
        $("#DropDownListaccordion").html("");
        var idToShow = MileStoneArray[0];
        $.each(MileStoneArray, function (index, value) {

            var sData = value.split("_");
            console.log(sData[1]);
            itemAppend = '';
            itemAppend = itemAppend + "<div class=\"box box-primary\">";
            itemAppend = itemAppend + "<div class=\"box-header\">";
            itemAppend = itemAppend + "<div class=\"pull-left col-md-10\">";
            itemAppend = itemAppend + "<h4 class=\"box-title\">";
            itemAppend = itemAppend + "<a data-toggle=\"collapse\" class=\"collapseMenu\" data-parent=\"#DropDownListaccordion\" href=\"#milestone_" + value + "\" aria-expanded=\"false\" class=\"collapsed\"><label id=\"mileStone_spanName_" + sData[1] + "\"></label> (" + sData[2] + "% of total) - <i><span id=\"mileStone_taskPercentageCompleted_" + sData[1] + "\">0</span>% done</i></a>";
            itemAppend = itemAppend + "</h4>";
            itemAppend = itemAppend + "</div>";
            itemAppend = itemAppend + "<div class=\"pull-right\">";

            itemAppend = itemAppend + "</div>";
            itemAppend = itemAppend + "</div>";
            itemAppend = itemAppend + "<div id=\"milestone_" + value + "\" class=\"panel-collapse collapse\" aria-expanded=\"false\" style=\"height: 0px;\">";
            itemAppend = itemAppend + "<div class=\"box-body\">";

            itemAppend = itemAppend + "</div>";
            itemAppend = itemAppend + "</div>";
            itemAppend = itemAppend + "</div>";
            $("#DropDownListaccordion").append(itemAppend);
        });

        getStageDetails();
        $('#milestone_' + idToShow + '').collapse('show');
    }
    var stageDetailsList = [];

    var IsAllMileStoneCompleted = true;
    var IsTeamUserUnCompletedTask = false;
    var IsUserUnCompletedTask = false;

    var TeamStageDetailsOpenId = '';
    var UserStageDetailsOpenId = '';

    var LoggedInUserId = '<%= Session("UserContactId")%>';
    var LoggedInTeamId = '<%= Session("Team")%>';
    var taskList = [];
    var PresentedTaskListIds = [];
    function getStageDetails() {
        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + savedBusinessProcessId + "',OppID:'<%= OppID%>',ProjectID:'<%= ProjectId%>'}";
        $("#UpdateProgress").css("display", "block");
        stageDetailsList = [];
        taskList = [];
        PresentedTaskListIds = [];
        IsAllMileStoneCompleted = true;
        IsTeamUserUnCompletedTask = false;
        IsUserUnCompletedTask = false;

        TeamStageDetailsOpenId = '';
        UserStageDetailsOpenId = '';
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetSavedStageDetails",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse.length > 0) {
                    var completedPercentage = Jresponse[0];
                    $("#lblTotalProgress").text(completedPercentage.intTotalProgress + "%");
                    $('#TotalProgress').attr('style', 'width:' + completedPercentage.intTotalProgress + '%');
                }
                var itemAppend = '';
                $("#DropDownListaccordion .box-body").html('');
                $.each(Jresponse, function (index, value) {

                    itemAppend = '';
                    var configPercentage = 0;
                    if (value.numStagePercentageId == 12) {
                        configPercentage = 100;
                    } else if (value.numStagePercentageId == 6) {
                        configPercentage = 50;
                    } else if (value.numStagePercentageId == 15) {
                        configPercentage = 33;
                    } else if (value.numStagePercentageId == 16) {
                        configPercentage = 66;
                    } else if (value.numStagePercentageId == 3) {
                        configPercentage = 25;
                    } else if (value.numStagePercentageId == 2) {
                        configPercentage = 20;
                    } else if (value.numStagePercentageId == 9) {
                        configPercentage = 75;
                    } else if (value.numStagePercentageId == 5) {
                        configPercentage = 40;
                    } else if (value.numStagePercentageId == 7) {
                        configPercentage = 60;
                    } else if (value.numStagePercentageId == 10) {
                        configPercentage = 80;
                    }
                    var jsonObj = {
                        numStageDetailsId: value.numStageDetailsId,
                        numStagePercentageId: value.numStagePercentageId,
                        configPercentage: configPercentage
                    };
                    stageDetailsList.push(jsonObj);
                    $("#mileStone_spanName_" + value.numStagePercentageId + "").text(value.vcMilestoneName)
                    $("#mileStone_taskPercentageCompleted_" + value.numStagePercentageId + "").text("0");

                    var appendToDiv = "milestone_" + value.tintConfiguration + "_" + value.numStagePercentageId + "_" + configPercentage;
                    itemAppend = itemAppend + "<div class=\"indivStages\">";
                    itemAppend = itemAppend + "<div class=\"divstageDetails\">";
                    itemAppend = itemAppend + "<div class=\"pull-left\">";
                    itemAppend = itemAppend + "<div class=\"form-inline\">";
                    itemAppend = itemAppend + "<label class=\"stageName\" id=\"spanStageName_" + value.numStageDetailsId + "\" Configuration=\"" + value.tintConfiguration + "\" StagePerID=\"" + value.numStagePercentageId + "\"  Percentage=\"" + configPercentage + "\">" + value.vcStageName + "</label> - ";
                    itemAppend = itemAppend + "<input type='hidden' value='" + value.numTeamId + "' id='hdnTeamId_" + value.numStageDetailsId + "' />"
                    var dueChecked = '';
                    var runDynamiModeChecked = '';
                    if (value.bitIsDueDaysUsed == true) {
                        dueChecked = 'checked';
                    }
                    if (value.bitRunningDynamicMode == true) {
                        runDynamiModeChecked = 'checked';
                    }
                    var hours = 0;
                    var minutes = 0;
                    var days = 0;
                    var week = 0;
                    //if (minutes > 59) {
                    //    var addedHours = Math.floor(minutes / 60);
                    //    hours = hours + addedHours;
                    //    minutes = minutes % 60;
                    //}
                    var estimateTime = ' ';
                    var suffix = '';
                    var acessClass = 'text-black';
                    if (value.bitIsDueDaysUsed == true) {

                        if (value.StageStartDate != null) {
                            var stageStartDate = (value.StageStartDate).replace("AM", " AM");
                            stageStartDate = stageStartDate.replace("PM", " PM");
                            var start = moment(new Date(stageStartDate));
                            var end = moment(new Date());
                            estimateTime = end.diff(start, "days");
                            estimateTime = parseFloat(estimateTime) + 1;

                            var totaldueDays = value.intDueDays;
                            totaldueDays = totaldueDays - estimateTime;
                            if (totaldueDays < 0) {
                                totaldueDays = totaldueDays * (-1);
                                suffix = ' late';
                                acessClass = 'text-danger';
                            } else {
                                suffix = ' to go';
                            }
                            estimateTime = totaldueDays + " Days" + suffix;
                        }

                    } else {

                        if (value.StageStartDate != null) {
                            var dayshoursminutes = value.dayshoursTaskDuration;
                            if (dayshoursminutes != null) {
                                var taskDuration = dayshoursminutes.split('_');
                                if (taskDuration.length > 0) {
                                    hours = taskDuration[1];
                                    days = taskDuration[0];
                                    minutes = taskDuration[2];
                                }
                                var seconds = 0;
                                var date_now = new Date();
                                var stageStartDate = (value.StageStartDate).replace("AM", " AM");
                                stageStartDate = stageStartDate.replace("PM", " PM");
                                stageStartDate = formatDateTime(stageStartDate);
                                stageStartDate = new Date(stageStartDate);
                                //stageStartDate.setDate(stageStartDate.getDate() + parseFloat(days));
                                hours = parseFloat(hours) + parseFloat((days * 8));
                                stageStartDate.setHours(stageStartDate.getHours() + parseFloat(hours));
                                //stageStartDate.setMinutes(stageStartDate.getMinutes() + parseFloat(minutes));
                                stageStartDate = new Date(stageStartDate);
                                seconds = Math.floor((stageStartDate - (date_now)) / 1000);

                                minutes = Math.floor(seconds / 60);
                                hours = Math.floor(minutes / 60);
                                days = Math.floor(hours / 8);

                                hours = hours - (days * 8);
                                minutes = minutes - (days * 8 * 60) - (hours * 60);
                                seconds = seconds - (days * 8 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);
                            }


                            if (days > 0) {
                                estimateTime = days + " Days ";
                                suffix = " to go";
                            } if (hours > 0) {
                                estimateTime = estimateTime + hours + " Hours ";
                                suffix = " to go";
                            }
                            if (days < 0) {
                                estimateTime = (days * (-1)) + " Days ";
                            } if (hours < 0) {
                                estimateTime = estimateTime + (hours * (-1)) + " Hours";
                            }
                            if (days < 0 || hours < 0) {
                                suffix = " late";
                                acessClass = 'text-danger';
                            }
                            estimateTime = estimateTime + suffix;
                        }
                    }
                    //var estimateTime = days+ " Days & " + hours + " Hours & " + minutes + " Minutes";

                    itemAppend = itemAppend + "<label class='text-large " + acessClass + "' id=\"spanEstimateTime_" + value.numStageDetailsId + "\"><Span id=\"spanEstimateTimeText_" + value.numStageDetailsId + "\">" + estimateTime + "</span><input type='hidden' id='hdnCompleted_Percentage_" + value.numStageDetailsId + "' value='0' /><span id='spanCompleted_Percentage_" + value.numStageDetailsId + "'>(0% done)</span></label>";
                    itemAppend = itemAppend + "</div>";
                    itemAppend = itemAppend + "</div>";

                    itemAppend = itemAppend + "<div class=\"pull-right\">";
                    itemAppend = itemAppend + "<div class=\"form-inline\">";

                    itemAppend = itemAppend + "<label>Started : </label>"
                    itemAppend = itemAppend + "<div class='input-group date datetimepicker' id='StartDate_datetimepicker_" + value.numStageDetailsId + "'>";
                    itemAppend = itemAppend + "<span class=\"input-group-addon\">";
                    itemAppend = itemAppend + "<span class=\"glyphicon glyphicon-calendar\"></span>";
                    itemAppend = itemAppend + "</span>";
                    itemAppend = itemAppend + "<input type='text' onkeypress='dtkeypress()' style='height:25px' class=\"form-control dtmCalender\" id='stage_StartDate_datetimepicker_" + value.numStageDetailsId + "' StageDetailsId=" + value.numStageDetailsId + " />";
                    itemAppend = itemAppend + "</div>";

                    itemAppend = itemAppend + "</div>";
                    itemAppend = itemAppend + "</div>";
                    itemAppend = itemAppend + "</div>";

                    itemAppend = itemAppend + "<div class=\"clearfix\"></div>";

                    itemAppend = itemAppend + "<div id='stageTask_" + value.numStageDetailsId + "'></div>";

                    itemAppend = itemAppend + "</div>";

                    $("#" + appendToDiv + " .box-body").append(itemAppend);

                    $("#StartDate_datetimepicker_" + value.numStageDetailsId + "").datetimepicker().on('dp.hide', function (e) {

                        UpdateStageDetailsStartDate(value.numStageDetailsId);
                        getStageDetails();
                    });
                    $("#stage_StartDate_datetimepicker_" + value.numStageDetailsId + "").val(value.StageStartDate);
                    $("#mileStone_txtName_" + value.numStagePercentageId + "").val(value.vcMilestoneName);
                    GetParentTask(value.numStageDetailsId, value.bitRunningDynamicMode);
                });

                $("#UpdateProgress").css("display", "block");
                setTimeout(function () {
                    BindSubRecursiveTask();
                }, 1000);
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }
    function formatDateTime(date, formatString = 'MM/DD/YYYY HH:MM') {
        return moment(new Date(date)).format(formatString);
    }
    function GetParentTask(StageDetailsId, StageRunningMode) {
        var DomainID = '<%= Session("DomainId")%>';
        var totalTaskCount = 0;
        var totalCompletedTaskCount = 0;
        var dataParam = "{DomainID:'" + DomainID + "',StageDetailsId:'" + StageDetailsId + "',OppId:'<%= OppID%>',ProjectId:'<%= ProjectId%>',bitAlphabetical:false}";
        $("#UpdateProgress").css("display", "block");
        $("#tblTaskDetails tbody").html('');
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskList",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                var itemAppend = '';
                if (Jresponse.length > 0) {
                    taskList = taskList.concat(Jresponse);
                    if (StageRunningMode == true) {
                        itemAppend = itemAppend + "<div class='indivTask' id='indivTask_" + StageDetailsId + "_0'>";
                        itemAppend = itemAppend + "<div class='form-inline'>";
                        itemAppend = itemAppend + "<label>Task :</label>";
                        itemAppend = itemAppend + "<select style='width:300px'  onchange='UpdateTaskDetails(0," + StageDetailsId + ")' id='ddlTaskList_" + StageDetailsId + "_0' class='form-control'><option value='0'>--Select--</option></select>&nbsp;&nbsp;";
                        itemAppend = itemAppend + "<button type='button' onclick='openaddTaskWindow(0," + StageDetailsId + ")' class='btn btn-default btn-xs btn-round btn-github'><i class='fa fa-plus'></i></button>&nbsp;&nbsp;";
                        itemAppend = itemAppend + "<button type='button' onclick='deleteTask(0," + StageDetailsId + ")' class='btn btn-danger btn-xs btn-round'><i class='fa fa-times'></i></button>&nbsp;&nbsp;";
                        itemAppend = itemAppend + "&nbsp;&nbsp;<label>Assign To :</label>";
                        itemAppend = itemAppend + "<select style='width:200px' class='form-control' onchange='UpdateTaskDetails(0," + StageDetailsId + ")' id='ddlTaskAssignTo_" + StageDetailsId + "_0'></select>&nbsp;&nbsp;";
                        itemAppend = itemAppend + "<a href='#' onclick='openEmpAvailability()'><i>Check Availability</i></a>&nbsp;&nbsp;";
                        itemAppend = itemAppend + "<div class='pull-right' style='padding-top: 4px;'>";
                        itemAppend = itemAppend + "<i><label>Last Modified : </label><span id='span_UpdatedBy_" + StageDetailsId + "_0'>-</span>&nbsp;,&nbsp;<span id='span_UpdatedOn_" + StageDetailsId + "_0'>-</span></i>&nbsp;&nbsp;"
                        itemAppend = itemAppend + "<input id='chkTaskCompleted_" + StageDetailsId + "_0' type='checkbox'  onclick='UpdateTaskDetails(0," + StageDetailsId + ",1)' />";
                        itemAppend = itemAppend + "</div>";

                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "</div>";
                    } else {
                        getSelectedTaskDetails(StageDetailsId);
                    }
                }
                PresentedTaskListIds.push("" + StageDetailsId + "_0");
                $("#stageTask_" + StageDetailsId + "").append(itemAppend);
                var dropDownitemAppend = '';
                var DefaultSavedTask = 0;
                var DefaultAssignTo = 0;
                var vcUpdatedByContactName = '-';
                var dtmUpdatedOn = '-';
                var bitTaskClosed = false;
                var bitSavedTask = false;
                $.each(Jresponse, function (index, value) {
                    bitSavedTask = value.bitSavedTask;
                    if (value.bitDefaultTask == true) {
                        dropDownitemAppend = dropDownitemAppend + "<option value='" + value.numTaskId + "'>" + value.vcTaskName + "</option>"
                        if (value.bitSavedTask == true) {
                            DefaultSavedTask = value.numTaskId;
                            dtmUpdatedOn = value.dtmUpdatedOn;
                            vcUpdatedByContactName = value.vcUpdatedByContactName;
                            DefaultAssignTo = value.numAssignTo;
                            bitTaskClosed = value.bitTaskClosed;
                        }
                    };
                    if (value.bitSavedTask == true) {
                        PresentedTaskListIds.push("" + value.numStageDetailsId + "_" + value.numTaskId + "");
                        totalTaskCount = totalTaskCount + 1;
                    }
                    if (value.bitTaskClosed == true && value.bitSavedTask == true) {
                        totalCompletedTaskCount = totalCompletedTaskCount + 1;
                    }
                });
                var taskPercentage = 0;
                if (totalCompletedTaskCount > 0) {
                    taskPercentage = (totalCompletedTaskCount / totalTaskCount) * 100;
                }
                $("#spanCompleted_Percentage_" + StageDetailsId + "").text("(" + parseFloat(taskPercentage.toFixed(2)) + "% done)");
                $("#hdnCompleted_Percentage_" + StageDetailsId + "").val(parseFloat(taskPercentage.toFixed(2)));

                if (taskPercentage == 0) {
                    $("#spanCompleted_Percentage_" + StageDetailsId + "").addClass("text-danger");
                } else if (taskPercentage == 100) {
                    $("#spanEstimateTimeText_" + StageDetailsId + "").text("");
                    $("#spanCompleted_Percentage_" + StageDetailsId + "").addClass("text-success");
                }
                $("#ddlTaskList_" + StageDetailsId + "_0").append(dropDownitemAppend);
                $("#ddlTaskList_" + StageDetailsId + "_0").val(DefaultSavedTask);
                $("#span_UpdatedBy_" + StageDetailsId + "_0").text(vcUpdatedByContactName);
                $("#span_UpdatedOn_" + StageDetailsId + "_0").text(dtmUpdatedOn);
                $("#chkTaskCompleted_" + StageDetailsId + "_0").prop("checked", bitTaskClosed);

                if (bitTaskClosed == false && DefaultAssignTo == LoggedInUserId && bitSavedTask == true) {
                    $("#indivTask_" + StageDetailsId + "_0").css("background-color", "rgba(255, 255, 0, 0.27)")
                }

                BindAssignTo($("#hdnTeamId_" + StageDetailsId + "").val(), "ddlTaskAssignTo_" + StageDetailsId + "_0", DefaultAssignTo);
                if (StageRunningMode == true) {
                    getSelectedTaskDetails(StageDetailsId, 1);
                }
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }
    function BindSubRecursiveTask() {
        var firstMileStone = '';
        firstMileStone = MileStoneArray[0];
        var i = 0;
        var k = 0;
        $.each(MileStoneArray, function (index, mvalue) {
            var TotalPercentage = 0;
            var totalCount = 0;
            var MileStoneId = mvalue.split("_");
            $.each(stageDetailsList, function (index, value) {
                ;
                if (TeamStageDetailsOpenId == value.numStageDetailsId && value.numStagePercentageId == MileStoneId[1] && i == 0) {
                    TeamStageDetailsOpenId = mvalue;
                    i = 1;
                }
                if (UserStageDetailsOpenId == value.numStageDetailsId && value.numStagePercentageId == MileStoneId[1] && k == 0) {
                    UserStageDetailsOpenId = mvalue;
                    k = 1;
                }
                if (value.numStagePercentageId == MileStoneId[1]) {
                    totalCount = totalCount + 1;
                    TotalPercentage = TotalPercentage + parseFloat($("#hdnCompleted_Percentage_" + value.numStageDetailsId + "").val());
                }
            });
            var avgPercentage = parseFloat(TotalPercentage / totalCount).toFixed(0);
            if (avgPercentage == NaN) {
                avgPercentage = 0;
            }
            $("#mileStone_taskPercentageCompleted_" + MileStoneId[1] + "").text(avgPercentage);

        })
        var IsOpenedTask = '<%=GetQueryStringVal("MileStoneId")%>'

        if (IsOpenedTask.length > 0) {
            hideShowMileStoneBasedOnSelection(IsOpenedTask);
        } else {
            if (IsAllMileStoneCompleted == true) {
                $(".milestoneStages .panel-collapse").collapse('hide');
            } else if (IsUserUnCompletedTask == true) {
                hideShowMileStoneBasedOnSelection(UserStageDetailsOpenId);
            } else if (IsTeamUserUnCompletedTask == true) {
                hideShowMileStoneBasedOnSelection(TeamStageDetailsOpenId);
            } else {
                hideShowMileStoneBasedOnSelection(firstMileStone);
            }
        }

        $("#UpdateProgress").css("display", "none");
    }
    function hideShowMileStoneBasedOnSelection(SelectedOpenId) {
        $.each(MileStoneArray, function (index, mvalue) {
            if (mvalue == SelectedOpenId) {
                var IsOpened = $('#milestone_' + SelectedOpenId + '').attr("aria-expanded");
                if (IsOpened == "false") {
                    $('#milestone_' + SelectedOpenId + '').collapse('show');
                }
            } else {
                var IsOpened = $('#milestone_' + mvalue + '').attr("aria-expanded");
                if (IsOpened == "true") {
                    $('#milestone_' + mvalue + '').collapse('hide');
                }
            }
        });
    }
    function openaddTaskWindow(TaskRowId, StageDetailsId) {
        ClearTaskModelControls();

        var teamId = $("#hdnTeamId_" + StageDetailsId + "").val();
        if (teamId == undefined) {
            teamId = 0;
        }
        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',numTeamId:'" + teamId + "'}";
        $("#UpdateProgress").css("display", "block");
        $("#ddlTaskAssignTo").empty();
        $("#hdnStageTaskDetailsId").val(StageDetailsId);
        $("#hdnTaskRowId").val(TaskRowId);
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskAssignTo",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                var itemAppend = '';
                itemAppend = itemAppend + '<option value=\"0\">--Select Assign to--</option>'
                $("#ddlTaskAssignTo").append(itemAppend);
                $.each(Jresponse, function (index, value) {
                    itemAppend = '';

                    itemAppend = itemAppend + "<option value=\"" + value.numContactID + "\">" + value.vcUserName + "</option>"
                    $("#ddlTaskAssignTo").append(itemAppend);
                });
                $("#UpdateProgress").css("display", "none");
                $("#taskModal").modal('show');
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }
    function AddTasktoStage() {
        var UserContactId ='<%= Session("UserContactId")%>'
        var DomainID = '<%= Session("DomainId")%>';
        var StageDetailsId = $("#hdnStageTaskDetailsId").val();
        var TaskRowId = $("#hdnTaskRowId").val();

        if ($("#txtTaskHours").val() == "") {
            $("#txtTaskHours").val("0");
        }
        if ($("#txtTaskMinutes").val() == "") {
            $("#txtTaskMinutes").val("0");
        }
        if ($("#hdnStageTaskDetailsId").val() == "0") {
            alert("Please select a stage");
            return false;
        }
        if ($("#txtTaskName").val() == "") {
            alert("Please enter task name");
            $("#txtTaskName").focus();
            return false;
        } else {
            $("#UpdateProgress").css("display", "block");
            var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + $("#hdnStageTaskDetailsId").val() + "',TaskName:'" + $("#txtTaskName").val() + "',Hours:'" + $("#txtTaskHours").val() + "',Minutes:'" + $("#txtTaskMinutes").val() + "',Assignto:'" + $("#ddlTaskAssignTo option:selected").val() + "',ParentTaskId:'0',OppID:'<%= OppID%>',IsTaskClosed:false,numTaskId:0,IsTaskSaved:true,IsAutoClosedTaskConfirmed:false,ProjectID:'<%=ProjectId%>',intTaskType:'0',WorkOrderID:0}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTask",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == true) {
                        $("#taskModal").modal('hide');
                        $("#txtTaskMinutes").val('');
                        $("#txtTaskHours").val('');
                        $("#txtTaskName").val('');
                        $("#hdnStageTaskDetailsId").val(0);
                        $("#hdnTaskRowId").val(0);
                        getStageDetails();
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
    }
    function UpdateTaskDetails(TaskRowId, StageDetailsId, IsClickedByTaskClosingCheckBox) {

        var UserContactId ='<%= Session("UserContactId")%>'
        var DomainID = '<%= Session("DomainId")%>';
        var OppID = '<%= OppID%>';
        var TaskId = $("#ddlTaskList_" + StageDetailsId + "_" + TaskRowId + " option:selected").val();
        var IsTaskClosed = 0;
        var IsTaskSaved = true;
        IsTaskClosed = $("#chkTaskCompleted_" + StageDetailsId + "_" + TaskRowId + "").is(':checked')
        if ($("#ddlTaskList_" + StageDetailsId + "_" + TaskRowId + " option:selected").val() == 0) {
            IsTaskSaved = false;
        }
        var IsAutoClosedTaskConfirmed = false;
        var TaskValidatorArray = [];
        if (IsTaskClosed == true && IsClickedByTaskClosingCheckBox == "1" && $("#hdnTaskValidatorId").val() == 2) {
            var TaskSelectedArray = taskList.filter(function (el) {
                return el.numTaskId == TaskId;
            });
            debugger;
            if (TaskSelectedArray.length == 1) {
                if (TaskSelectedArray[0].numOrder > 0) {
                    TaskValidatorArray = taskList.filter(function (el) {
                        return el.numTaskId != TaskId &&
                            el.numStageDetailsId == TaskSelectedArray[0].numStageDetailsId &&
                            el.bitTaskClosed==false &&
                            el.numOrder < TaskSelectedArray[0].numOrder;
                    });
                } else {
                    TaskValidatorArray = taskList.filter(function (el) {
                        return el.numTaskId != TaskId &&
                            el.numStageDetailsId == TaskSelectedArray[0].numStageDetailsId &&
                            el.bitTaskClosed==false &&
                            el.numTaskId < TaskSelectedArray[0].numTaskId;
                    });
                }
            }
            if (TaskValidatorArray.length > 0) {
                if (confirm('One or more previous tasks within this stage aren’t checked complete. Would you like BizAutomation to check them for you ?')) {
                    IsAutoClosedTaskConfirmed = true;
                }
            }
        } if (IsTaskClosed == true && IsClickedByTaskClosingCheckBox == "1" && $("#hdnTaskValidatorId").val() == 4) {
             var TaskSelectedArray = taskList.filter(function (el) {
                return el.numTaskId == TaskId;
            });
            debugger;
            if (TaskSelectedArray.length == 1) {
                if (TaskSelectedArray[0].numOrder > 0) {
                    TaskValidatorArray = taskList.filter(function (el) {
                        return el.numTaskId != TaskId &&
                            el.numStagePercentage == TaskSelectedArray[0].numStagePercentage &&
                            el.bitTaskClosed==false &&
                            el.numOrder < TaskSelectedArray[0].numOrder;
                    });
                } else {
                    TaskValidatorArray = taskList.filter(function (el) {
                        return el.numTaskId != TaskId &&
                            el.numStagePercentage == TaskSelectedArray[0].numStagePercentage &&
                            el.bitTaskClosed==false &&
                            el.numTaskId < TaskSelectedArray[0].numTaskId;
                    });
                }
            }
            if (TaskValidatorArray.length == 0) {
                if (TaskSelectedArray[0].numStageOrder > 0) {
                    TaskValidatorArray = taskList.filter(function (el) {
                        return el.numTaskId != TaskId &&
                            el.numStagePercentage < TaskSelectedArray[0].numStagePercentage &&
                            el.bitTaskClosed == false;
                    });
                } else {
                    TaskValidatorArray = taskList.filter(function (el) {
                        return el.numTaskId != TaskId &&
                            el.numStagePercentage < TaskSelectedArray[0].numStagePercentage &&
                            el.bitTaskClosed == false;
                    });
                }
            }
            if (TaskValidatorArray.length > 0) {
                if (confirm('One or more previous tasks aren’t checked complete. Would you like BizAutomation to check them for you ?')) {
                    IsAutoClosedTaskConfirmed = true;
                }
            }
        }
        $("#UpdateProgress").css("display", "block");
        var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + StageDetailsId + "',TaskName:'',Hours:'0',Minutes:'0',Assignto:'" + $("#ddlTaskAssignTo_" + StageDetailsId + "_" + TaskRowId + " option:selected").val() + "',ParentTaskId:'0',OppID:'" + OppID + "',IsTaskClosed:'" + IsTaskClosed + "','numTaskId':'" + TaskId + "','IsTaskSaved':'" + IsTaskSaved + "',IsAutoClosedTaskConfirmed:'" + IsAutoClosedTaskConfirmed + "',ProjectID:'<%=ProjectId%>',intTaskType:'0',WorkOrderID:0}";
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTask",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse == true) {
                    $("#taskModal").modal('hide');
                    $("#txtTaskMinutes").val('');
                    $("#txtTaskHours").val('');
                    $("#txtTaskName").val('');
                    $("#hdnStageTaskDetailsId").val(0);
                    getStageDetails()
                    start('pt');
                    var IsTaskNotSaved = 0;
                    var OppStatus = '<%= OppStatus%>';
                    if (OppStatus == "0") {
                        setTimeout(function () {
                            if (IsClickedByTaskClosingCheckBox == "1" && IsTaskClosed == true) {
                                $.each(taskList, function (index, value) {
                                    if (value.bitTaskClosed == false && value.bitSavedTask == true && value.numTaskId != TaskId) {
                                        IsTaskNotSaved = 1;
                                    }
                                });
                                if (IsClickedByTaskClosingCheckBox == "1" && IsTaskClosed == true && IsTaskNotSaved == 0) {
                                    $("#openCloseDealDiv").css("display", "inline");
                                    $("#modalConfirmDealStatus").modal("show");
                                }
                            }
                        }, 1000);
                    }
                    return false;
                }
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
                $("#UpdateProgress").css("display", "none");
            }
        });
    }
    function BindAssignTo(TeamId, ddl, DefaultAssignTo) {

        $("#" + ddl + "").empty();
        if (TeamId == undefined) {
            TeamId = 0;
        }
        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',numTeamId:'" + TeamId + "'}";
        $("#UpdateProgress").css("display", "block");
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskAssignTo",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                var itemAppend = '';
                itemAppend = itemAppend + '<option value=\"0\">--Select Assign to--</option>'
                $("#" + ddl + "").append(itemAppend);
                $.each(Jresponse, function (index, value) {
                    itemAppend = '';
                    itemAppend = itemAppend + "<option value=\"" + value.numContactID + "\">" + value.vcUserName + "</option>"
                    $("#" + ddl + "").append(itemAppend);
                });
                $("#" + ddl + "").val(DefaultAssignTo);
                $("#UpdateProgress").css("display", "none");
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }
    function getSelectedTaskDetails(StageDetailsId, dynamicMode) {

        var selectedTaskId = 0;
        var taskName = "";
        var taskHours = 0;
        var taskMinutes = 0;
        var taskAsignee = 0;
        var currentTask = [];
        var dropDownitemAppend = '';
        var itemAppend = '';
        var savedTaskId = 0;
        var vcUpdatedByContactName = '-';
        var dtmUpdatedOn = '-';
        var DefaultAssignTo = 0;
        var bitTaskClosed = false;
        $.each(taskList, function (index, value) {
            selectedTaskId = value.numTaskId;
            taskName = (value.vcTaskName || "");
            taskHours = (value.numHours || 0);
            taskMinutes = (value.numMinutes || 0);
            taskAssignee = (value.numAssignTo || 0);
            if (value.numStageDetailsId == StageDetailsId) {
                itemAppend = "";
                dropDownitemAppend = "";
                dropDownitemAppend = dropDownitemAppend + "<option value='" + value.numTaskId + "'>" + value.vcTaskName + "</option>"
                itemAppend = itemAppend + "<div class='indivTask' id='indivTask_" + StageDetailsId + "_" + selectedTaskId + "'>";
                itemAppend = itemAppend + "<div class='form-inline'>";
                itemAppend = itemAppend + "<label>Task :</label>";
                itemAppend = itemAppend + "<select style='width:300px' onchange='UpdateTaskDetails(" + selectedTaskId + "," + StageDetailsId + ")' id='ddlTaskList_" + StageDetailsId + "_" + selectedTaskId + "' class='form-control'><option value='0'>--Select--</option></select>&nbsp;&nbsp;";
                itemAppend = itemAppend + "<button type='button' class='btn btn-default btn-xs btn-round btn-github' onclick='openaddTaskWindow(" + selectedTaskId + "," + StageDetailsId + ")' ><i class='fa fa-plus'></i></button>&nbsp;&nbsp;";
                itemAppend = itemAppend + "<button type='button' class='btn btn-danger btn-xs btn-round' onclick='deleteTask(" + selectedTaskId + "," + StageDetailsId + ")'><i class='fa fa-times'></i></button>&nbsp;&nbsp;";
                itemAppend = itemAppend + "<button type='button' class='btn btn-info btn-xs btn-round' style='border-radius: 50%;' onclick='openEditTaskWindow(" + selectedTaskId + ",\"" + taskName + "\"," + taskHours + "," + taskMinutes + "," + taskAssignee + ")'><i class='fa fa-pencil'></i></button>";
                itemAppend = itemAppend + "&nbsp;&nbsp;<label>Assign To :</label>";
                itemAppend = itemAppend + "<select style='width:200px'  onchange='UpdateTaskDetails(" + selectedTaskId + "," + StageDetailsId + ")' class='form-control' id='ddlTaskAssignTo_" + StageDetailsId + "_" + selectedTaskId + "'></select>&nbsp;&nbsp;";
                itemAppend = itemAppend + "<a href='#' onclick='openEmpAvailability()'><i>Check Availability</i></a>&nbsp;&nbsp;";
                //itemAppend = itemAppend + "<a href='#'><img src='../images/payrollMileStone.png' style='height: 20px;' /></a>&nbsp;&nbsp;";
                //itemAppend = itemAppend + "<a href='#'><img src='../images/TImes.png' style='height: 32px;margin-top: -3px;' /></a>&nbsp;&nbsp;";
                //itemAppend = itemAppend + "<span id='spanTime_" + StageDetailsId + "_" + selectedTaskId + "'>-</span>&nbsp;&nbsp;";

                itemAppend = itemAppend + "<div class='pull-right' style='padding-top:0px;'>";
                itemAppend = itemAppend + "<i><label>Last Modified : </label><span id='span_UpdatedBy_" + StageDetailsId + "_" + selectedTaskId + "'>-</span>&nbsp;,&nbsp;<span id='span_UpdatedOn_" + StageDetailsId + "_" + selectedTaskId + "'>-</span></i>&nbsp;&nbsp;"
                itemAppend = itemAppend + "<input id='chkTaskCompleted_" + StageDetailsId + "_" + selectedTaskId + "' type='checkbox' onclick='UpdateTaskDetails(" + selectedTaskId + "," + StageDetailsId + ",1)' />&nbsp;";
                itemAppend = itemAppend + "</div>";

                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "</div>";
                if (value.bitSavedTask == true) {
                    savedTaskId = value.numTaskId;
                    vcUpdatedByContactName = value.vcUpdatedByContactName;
                    dtmUpdatedOn = value.dtmUpdatedOn;
                    DefaultAssignTo = value.numAssignTo;
                    bitTaskClosed = value.bitTaskClosed;

                }

                if (dynamicMode == 1) {
                    if (value.bitDefaultTask == true) {
                        itemAppend = '';
                    }
                }
                if (itemAppend != '') {
                    $("#stageTask_" + StageDetailsId + "").append(itemAppend);
                    BindAssignTo($("#hdnTeamId_" + StageDetailsId + "").val(), "ddlTaskAssignTo_" + StageDetailsId + "_" + value.numTaskId + "", DefaultAssignTo);
                    $("#ddlTaskList_" + StageDetailsId + "_" + value.numTaskId + "").append(dropDownitemAppend);
                    $("#ddlTaskList_" + StageDetailsId + "_" + value.numTaskId + "").val(savedTaskId);
                    $("#span_UpdatedBy_" + StageDetailsId + "_" + value.numTaskId + "").text(vcUpdatedByContactName);
                    $("#span_UpdatedOn_" + StageDetailsId + "_" + value.numTaskId + "").text(dtmUpdatedOn);
                    $("#chkTaskCompleted_" + StageDetailsId + "_" + value.numTaskId + "").prop("checked", bitTaskClosed);
                    if (bitTaskClosed == false) {
                        IsAllMileStoneCompleted = false;
                    }
                    if (bitTaskClosed == false && value.numAssignTo == LoggedInUserId) {
                        $("#indivTask_" + StageDetailsId + "_" + value.numTaskId + "").css("background-color", "rgba(255, 255, 0, 0.27)")
                    }
                    if (IsTeamUserUnCompletedTask == false && bitTaskClosed == false && value.numTeam == LoggedInTeamId && value.numTeamId>0) {
                        IsTeamUserUnCompletedTask = true;
                        ;
                        TeamStageDetailsOpenId = StageDetailsId;
                    }
                    if (IsUserUnCompletedTask == false && bitTaskClosed == false && value.numAssignTo == LoggedInUserId) {
                        IsUserUnCompletedTask = true;
                        ;
                        UserStageDetailsOpenId = StageDetailsId;
                    }
                }
            }
        });

    }

    function openEditTaskWindow(TaskId, taskName, hours, minutes, assignedTo) {
        ClearTaskModelControls();

        var DomainID = '<%= Session("DomainId")%>';
        var dataParam = "{DomainID:'" + DomainID + "',numTeamId:'" + 0 + "'}";
        $("#taskModal #ddlTaskAssignTo").empty();
       
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskAssignTo",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                var itemAppend = '';
                itemAppend = itemAppend + '<option value=\"0\">--Select Assign to--</option>'
                $("#taskModal #ddlTaskAssignTo").append(itemAppend);
                $.each(Jresponse, function (index, value) {
                    itemAppend = '';

                    itemAppend = itemAppend + "<option value=\"" + value.numContactID + "\">" + value.vcUserName + "</option>"
                    $("#taskModal #ddlTaskAssignTo").append(itemAppend);
                });
                $("#UpdateProgress").css("display", "none");
               
                $("#taskModal #txtTaskName").val(taskName);
                $("#taskModal #txtTaskMinutes").val(minutes);
                $("#taskModal #txtTaskMinutes").attr("disabled","disabled");
                $("#taskModal #txtTaskHours").val(hours);
                $("#taskModal #txtTaskHours").attr("disabled","disabled");
                $("#taskModal #ddlTaskAssignTo").val(assignedTo);
                $("#taskModal #ddlTaskAssignTo").attr("disabled","disabled");
            
                $("#taskModal #hdnStageTaskDetailsId").val(0);
            
                $("#taskModal .btn-add-task").text("Save");
                $("#taskModal .btn-add-task").attr("onclick","UpdateTask(" + TaskId.toString() + ")");

                $("#taskModal").modal('show');
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }

    function UpdateTask(TaskId){
        $.ajax({
            type: "POST",
            url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "mode": 50
                , "updateRecordID": TaskId
                , "updateValueID": 0
                , "comments": $("#taskModal #txtTaskName").val()
            }),
            beforeSend: function () {
                $("[id$=UpdateProgressProject]").show();
            },
            complete: function () {
                $("[id$=UpdateProgressProject]").hide();
            },
            success: function (data) {
                $("#taskModal").modal('hide');
                $("[id*=ddlTaskList_] option[value=" + TaskId + "]").text($("#taskModal #txtTaskName").val());
                ClearTaskModelControls();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Unknown error ocurred");
            }
        });

    }

    function ClearTaskModelControls(){
        $("#txtTaskMinutes").val('');
        $("#txtTaskHours").val('');
        $("#txtTaskName").val('');
        $("#taskModal #txtTaskMinutes").removeAttr("disabled");
        $("#taskModal #txtTaskHours").removeAttr("disabled");
        $("#taskModal #ddlTaskAssignTo").removeAttr("disabled");
        $("#hdnStageTaskDetailsId").val(0);
        $("#hdnTaskRowId").val(0);
        $("#taskModal .btn-add-task").text("Add Task");
        $("#taskModal .btn-add-task").attr("onclick","AddTasktoStage();");
    }

    function deleteTask(TaskRowId, StageDetailsID) {
        var TaskId = $("#ddlTaskList_" + StageDetailsID + "_" + TaskRowId + " option:selected").val();
        if (TaskId > 0) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',TaskId:'" + TaskId + "'}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteTask",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == "1") {
                        alert("Task Deleted Successfully");
                        getStageDetails();
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        } else {
            alert("Please select a task");
        }
    }
    function AddProcesstoProjectorOportunity() {
        var DomainID = '<%= Session("DomainId")%>';
        var UserContactId = '<%= Session("UserContactId")%>';
        var OppID = '<%= OppID%>';
        var ProjectId = '<%= ProjectId%>';
        var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + $("#ddlProcesses option:selected").val() + "',OppID:'" + OppID + "',ProjectId:'" + ProjectId + "',UserContactId:'" + UserContactId + "'}";
        $("#UpdateProgress").css("display", "block");
        stageDetailsList = [];
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddProcesstoOpportunity",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse == "1") {
                    location.reload(true);
                }
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }


    function UpdateStageDetailsStartDate(numStageDetailsId) {
        var startDate = $("#stage_StartDate_datetimepicker_" + numStageDetailsId + "").val();
        var DomainID = '<%= Session("DomainId")%>';
        var UserContactId = '<%= Session("UserContactId")%>';
        var OppID = '<%= OppID%>';
        var ProjectId = '<%= ProjectId%>';
        var dataParam = "{OppID:'" + OppID + "',ProjectID:'" + ProjectId + "',numStageDetailsId:'" + numStageDetailsId + "',StartDate:'" + startDate + "'}";
        $("#UpdateProgress").css("display", "block");
        stageDetailsList = [];
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodUpdateStartDateProcesstoOpportunity",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }

    function DeleteProcessFromOpportunity() {
        var DomainID = '<%= Session("DomainId")%>';
        var UserContactId = '<%= Session("UserContactId")%>';
        var OppID = '<%= OppID%>';
        var ProjectId = '<%= ProjectId%>';
        var dataParam = "{OppID:'" + OppID + "',ProjectId:'" + ProjectId + "'}";
        $("#UpdateProgress").css("display", "block");
        stageDetailsList = [];
        $.ajax({
            type: "POST",
            url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteProcessFromOpportunity",
            data: dataParam,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#UpdateProgress").css("display", "none");
                var Jresponse = $.parseJSON(response.d);
                if (Jresponse == "1") {
                    location.reload();
                }
            },
            failure: function (response) {
                $("#UpdateProgress").css("display", "none");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#UpdateProgress").css("display", "none");
            }, complete: function () {
            }
        });
    }
</script>




<div style="display: none">
    <div class="row" id="tblProgress" runat="server">
        <div class="form-inline">
            <div class="col-xs-12 col-sm-6 col-sm-3">
                <div class="form-group">
                    <label>Project:</label>
                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-sm-3">
                <div class="form-group">
                    <label>Business Process:</label>
                    <asp:DropDownList ID="ddlProcessList" runat="server" CssClass="form-control"></asp:DropDownList>
                    <asp:HiddenField ID="hfSlpId" runat="server" Value="0" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-3">
                <div class="form-group pull-right">
                    <label></label>
                    <asp:Button ID="btnAddProcess" runat="server" Text="Add Process" CssClass="btn btn-default" />&nbsp;
            <asp:Button ID="btnRemoveProcess" runat="server" Text="Remove Process" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure, you want to remove the selected Process?');" />
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="tblRowStatge" runat="server">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Milestone and Stages</h3>

                    <div class="box-tools pull-right">
                        <asp:LinkButton ID="btnColExp" runat="server" Text="Collapse Tree" CssClass="btn btn-xs btn-default" CommandName="Collapse" ToolTip="Collapse Project Stage" Font-Bold="true" />
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <asp:TreeView ID="tvProjectStage" runat="server" ExpandDepth="0" ImageSet="Arrows" PathSeparator="|">
                        <HoverNodeStyle Font-Underline="false" ForeColor="#5555DD" />
                        <SelectedNodeStyle CssClass="SelectedNodeStyle" />
                        <NodeStyle CssClass="NodeStyle" />
                    </asp:TreeView>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Stage Detail</h3>

                    <div class="box-tools pull-right">
                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-xs btn-default" Style="display: none;"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <asp:MultiView ID="mvStage" runat="server">
                        <asp:View ID="vDetail" runat="server">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Assign To</label>
                                        <div>
                                            <asp:Label ID="lblDAssignTo" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Last Modified By</label>
                                        <div>
                                            <asp:Label ID="lblDLastModify" runat="server"></asp:Label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Stage Name</label>
                                        <div>
                                            <asp:Label ID="lblDStageName" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Created By</label>
                                        <div>
                                            <asp:Label ID="lblDCreatedBy" runat="server" Style="float: left; color: Gray;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Stage Progress&nbsp;&nbsp;<asp:Label ID="lblDStageProgress" runat="server" class="badge bg-light-blue"></asp:Label></label>
                                        <div class="progress progress-xs progress-striped active">
                                            <div id="progress" runat="server" class="progress-bar progress-bar-primary">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div>
                                            <asp:Label ID="lblDStartDate" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>
                                            <img src="../images/folder_add.png" />Documents</label>
                                        <div>
                                            <asp:HyperLink ID="hplDDocuments" runat="server">
                                            </asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Due Date</label>
                                        <div>
                                            <asp:Label ID="lblDDueDate" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <div>
                                            <asp:Label ID="lblDDescription" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trDTimeDetail" runat="server">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            <img src="../images/time_add.png" />
                                            <asp:HyperLink ID="hplDStageTime" runat="server">Time:</asp:HyperLink>
                                            <asp:Label ID="lblDStageTime" runat="server" Text="Time:" Visible="false"></asp:Label>
                                        </label>
                                        <div>
                                            <asp:Label ID="lblDTime" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblTimeBudget" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trDExpnseDetail" runat="server">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            <img src="../images/money_add.png" />
                                            <asp:HyperLink ID="hplDStageExpense" runat="server">Expense:</asp:HyperLink>
                                            <asp:Label ID="lblDStageExpense" runat="server" Text="Expense:" Visible="false"></asp:Label>
                                        </label>
                                        <div>
                                            <asp:Label ID="lblDExpense" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblExpenseBudget" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trDDependencies" runat="server">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Dependencies</label>
                                        <div>
                                            <asp:TreeView ID="tvDependencies" runat="server">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Comments</label>
                                        <div>
                                            <uc1:frmComments ID="frmComments1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View ID="vEdit" runat="server">
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add New Stage</asp:LinkButton>
                                    </div>
                                    <div class="pull-right">
                                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Stage</asp:LinkButton>
                                    </div>
                                    <asp:HiddenField ID="hfStagePerID" runat="server" />
                                    <asp:HiddenField ID="hfConfiguration" runat="server" />
                                    <asp:HiddenField ID="hfSalesProsID" runat="server" />
                                    <asp:HiddenField ID="hfMileStoneName" runat="server" />
                                    <asp:HiddenField ID="hfParentStageID" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hfStageID" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hfDueDays" runat="server" />
                                    <asp:HiddenField ID="hfAssignTo" runat="server" />
                                    <asp:HiddenField ID="hfProjectID" runat="server" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <table style="width: 100%">
                                            <tr>
                                                <td colspan="2">
                                                    <label>Assigned To</label>
                                                    <%-- <asp:Label ID="lblEAssignTo" runat="server"></asp:Label>--%>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="width: 60%">
                                                    <asp:DropDownList ID="ddlAssignToUsers" Width="150px" CssClass="signup" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="hplEmpAvaliability" runat="server" Style="text-decoration: underline;"
                                                        NavigateUrl="#" onclick="return openEmpAvailability()"><font color="#180073">Check Availability</font>
                                                    </asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>

                                        <div>
                                            <%--  <asp:Label ID="lblEAssignTo" runat="server"></asp:Label>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Last Modified By</label>
                                        <div>
                                            <asp:Label ID="lblELastModify" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Stage Name</label>
                                        <asp:TextBox ID="txtStageName" runat="server" Width="250" MaxLength="1000" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Stage Progress</label>
                                        <asp:DropDownList ID="ddStageProgress" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                            <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
                                            <asp:ListItem Value="50">50%</asp:ListItem>
                                            <asp:ListItem Value="75">75%</asp:ListItem>
                                            <asp:ListItem Value="100">100%</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Documents</label>
                                        <div>
                                            <asp:HyperLink ID="hplEDocuments" runat="server"></asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trDate" runat="server">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div>
                                            <BizCalendar:Calendar ID="calStartDate" runat="server" ClientIDMode="AutoID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Due Date</label>
                                        <div>
                                            <BizCalendar:Calendar ID="calDueDate" runat="server" ClientIDMode="AutoID" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trETimeDetail" runat="server">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            <asp:HyperLink ID="hplEStageTime" CssClass="hyperlink" runat="server">Time:</asp:HyperLink>
                                            <asp:Label ID="lblEStageTime" runat="server" Text="Time:" Visible="false"></asp:Label>
                                        </label>
                                        <div class="form-inline">
                                            <asp:Label ID="lblETime" runat="server"></asp:Label>
                                            <span id="spTimeBudget" runat="server">&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkTimeBudget" runat="server" ClientIDMode="Static" />&nbsp;<label
                                        for="chkTimeBudget"><strong>Billable Time Budget (in Hrs):</strong></label>&nbsp;<asp:TextBox
                                            ID="txtTimeBudget" runat="server" Width="60px" onkeypress="CheckNumber(1,event)"
                                            MaxLength="10" CssClass="form-control"></asp:TextBox></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trEExpenseDetail" runat="server">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            <asp:HyperLink ID="hplEStageExpense" CssClass="hyperlink" runat="server">Expense:</asp:HyperLink>
                                            <asp:Label ID="lblEStageExpense" runat="server" Text="Expense:" Visible="false"></asp:Label>
                                        </label>
                                        <div class="form-inline">
                                            <asp:Label ID="lblEExpense" runat="server"></asp:Label>
                                            <span id="spExpenseBudget" runat="server">&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkExpenseBudget" runat="server" ClientIDMode="Static" />&nbsp;<label
                                        for="chkExpenseBudget"><strong>Expense Budget (Amt):</strong></label>&nbsp;<asp:TextBox
                                            ID="txtExpenseBudget" runat="server" Width="60px" onkeypress="CheckNumber(1,event)"
                                            MaxLength="10" CssClass="form-control"></asp:TextBox></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trEDependencies" runat="server">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Upstream Dependencies</label>
                                        <div>
                                            <p>Stages that need to be completed before this one begins.</p>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label></label>
                                                    <asp:DropDownList ID="ddlProjectStage" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                                                </div>
                                                <asp:Button ID="btnDependencies" runat="server" Text="Add" CssClass="button" OnClientClick="return CheckDependencies()" />
                                            </div>
                                            <div class="table-responsive">
                                                <asp:GridView ID="gvDependencies" runat="server" Width="100%" CssClass="table table-bordered table-striped" AllowSorting="true"
                                                    AutoGenerateColumns="False" DataKeyNames="numStageDetailId,numDependantOnId"
                                                    ShowHeader="false" BorderStyle="Solid" BorderWidth="1">
                                                    <AlternatingRowStyle CssClass="is" />
                                                    <RowStyle CssClass="ais"></RowStyle>
                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundField DataField="vcStageName" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="DeleteDependency"
                                                                    CommandArgument="<%# Container.DataItemIndex %>" Text="Delete" OnClientClick="return confirm('Are you sure, you want to delete the selected Dependency?');"> </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View ID="vAdd" runat="server">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="pull-left">
                                        <asp:RadioButtonList ID="rdbStageType" runat="server" ClientIDMode="Static" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0" Selected="True">New Stage on Same Level</asp:ListItem>
                                            <asp:ListItem Value="1">Sub Stage on this One</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="pull-right">
                                        <asp:LinkButton ID="btnAddSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Stage Name</label>
                                        <asp:TextBox ID="txtAStageName" runat="server" MaxLength="1000" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>This stage comprises % of completion</label>
                                        <div>
                                            <asp:HiddenField ID="hfParentStageID1" runat="server" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hfStageID1" runat="server" ClientIDMode="Static" />
                                            <asp:Label ID="lblAStagePercentage" runat="server" ClientIDMode="Static" Font-Bold="true"></asp:Label>
                                            <asp:HyperLink ID="hfSetPercentage" runat="server" ClientIDMode="Static">Set % of completion</asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div>
                                            <BizCalendar:Calendar ID="calAStartDate" runat="server" ClientIDMode="AutoID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Due Date</label>
                                        <div>
                                            <BizCalendar:Calendar ID="calADueDate" runat="server" ClientIDMode="AutoID" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <asp:TextBox ID="txtADescription" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>

    <div class="overlay" id="divStageProgress" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 420px; padding: 10px; border-color: #8A8A8A; border-width: 2px; border-style: solid; height: 120px;">
            <table>
                <tr>
                    <td colspan="2">
                        <p>
                            Some stages before this one don’t have their stage progress set to 100%. Would you like to set them to 100% ?
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="float: right; width: 50%;">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_Click" CssClass="btn btn-primary"></asp:Button>
                    </td>
                    <td style="width: 50%;">
                        <asp:Button ID="btnNo" runat="server" Text="No" OnClick="btnNo_Click" CssClass="btn btn-primary"></asp:Button>
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <div class="overlay" id="divOpptoOrderAlert" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; padding: 10px; border-color: #8A8A8A; border-width: 2px; border-style: solid; height: 160px; width: 420px;">
            <table style="width: 100%">
                <tr>
                    <td>
                        <img src="../images/PartyIcon.png" runat="server" />
                    </td>
                    <td>
                        <p>
                            Congratulations ! you’ve completed the last stage of the last milestone. 
                           Ready to turn this opportunity into an order ?
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;" colspan="2">
                        <asp:Button ID="btnAbsoultely" runat="server" Text="Absolutely woohoo !" OnClick="btnAbsoultely_Click" CssClass="btn btn-primary"></asp:Button>
                        <asp:Button ID="btnNotNow" runat="server" Text="Not now thanks" OnClientClick="return hideOpptoOrderAlert();" CssClass="btn btn-primary"></asp:Button>
                        <asp:HiddenField ID="hdnOppid" runat="server" />
                        <asp:HiddenField ID="hdnid" runat="server" />
                    </td>
                </tr>
            </table>

        </div>
    </div>
</div>
