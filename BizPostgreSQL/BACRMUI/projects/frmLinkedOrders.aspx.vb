﻿Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common

Partial Public Class frmLinkedOrders
    Inherits BACRMPage
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                objCommon.ProID = GetQueryStringVal( "ProID")
                objCommon.charModule = "P"
                objCommon.GetCompanySpecificValues1()
                hdnDivId.Value = objCommon.DivisionID
                LoadAuthBizDoc()
                BindGrid()
                
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadAuthBizDoc()
        Try
            Dim dtOpportunity As DataTable
            Dim objProjectList As New Project
            objProjectList.DomainID = Session("DomainId")
            objProjectList.DivisionID = hdnDivId.Value
            objProjectList.bytemode = 2
            dtOpportunity = objProjectList.GetOpportunities
            ddlAuthBizDoc.DataSource = dtOpportunity
            ddlAuthBizDoc.DataTextField = "vcBizDocName"
            ddlAuthBizDoc.DataValueField = "numOppBizDocsId"
            ddlAuthBizDoc.DataBind()
            ddlAuthBizDoc.Items.Insert(0, "--Select One--")
            ddlAuthBizDoc.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindGrid()
        Try
            Dim dtOpportunity As DataTable
            Dim objProjectList As New Project
            objProjectList.DomainID = Session("DomainId")
            objProjectList.DivisionID = hdnDivId.Value
            objProjectList.bytemode = 1
            objProjectList.ProjectID = GetQueryStringVal( "ProID")
            dtOpportunity = objProjectList.GetOpportunities
            gvBizDocs.DataSource = dtOpportunity
            gvBizDocs.DataBind()
            If dtOpportunity.Rows.Count = 0 Then
                tbl.Visible = False
            Else
                tbl.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If ddlAuthBizDoc.SelectedValue > 0 Then
                Dim objProject As New Project
                objProject.ProjectID = GetQueryStringVal( "ProID")
                objProject.OppBizDocID = ddlAuthBizDoc.SelectedValue
                objProject.DomainID = Session("DomainID")
                objProject.SaveProjectOpportunities()
                BindGrid()
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvBizDocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBizDocs.RowCommand
        If e.CommandName = "Delete" Then
            Dim objProject As New Project
            objProject.ProjOppID = e.CommandArgument
            objProject.DeleteProjectsOpp()
            BindGrid()
        End If
    End Sub

    Private Sub gvBizDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBizDocs.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btnDelete As Button
                btnDelete = e.Row.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                Dim lblBillID As Label
                lblBillID = e.Row.FindControl("lblBillID")

                If CCommon.ToLong(lblBillID.Text) > 0 Then
                    Dim lblLink As Label
                    lblLink = e.Row.FindControl("lblLink")
                    lblLink.Visible = True
                    btnDelete.Visible = False
                End If
                If e.Row.Cells(2).Text.ToLower = "purchase" Then
                    btnDelete.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvBizDocs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvBizDocs.RowDeleting

    End Sub
End Class