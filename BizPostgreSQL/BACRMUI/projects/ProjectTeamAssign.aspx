﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProjectTeamAssign.aspx.vb"
    Inherits=".ProjectTeamAssign" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Project Team Assign</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnAssign" Text="Save" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Manage Team Member
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMile" runat="server" Width="600px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal1">Project Team:
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:DropDownList runat="server" ID="ddProjectTeam" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal1" VerticalAlign="Top">
        User List:
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:CheckBoxList ID="cblUser" runat="server" CssClass="normal1">
                </asp:CheckBoxList>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
