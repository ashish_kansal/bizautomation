﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin

Public Class frmStageExpenseList
    Inherits BACRMPage
    Dim StageID, ProID, DivID As Long
    Dim m_aryRights() As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            StageID = GetQueryStringVal( "ProStageID")
            ProID = GetQueryStringVal( "ProID")
            DivID = GetQueryStringVal( "DivID")
            lblStageName.Text = GetQueryStringVal( "StageName")
            If Not IsPostBack Then
                BindGrid()
                
                hplAddBill.Attributes.Add("onclick", "OpenBill('" & ProID.ToString & "','" & StageID.ToString & "','" & lblStageName.Text.Replace("'", "") & "','" & DivID.ToString() & "')")
                hplPO.Attributes.Add("onclick", "OpenPO('" & ProID.ToString & "','" & StageID.ToString & "','" & lblStageName.Text.Replace("'", "") & "','" & DivID.ToString() & "')")

                'Permission based add bill and PO
                
                m_aryRights =  GetUserRightsForPage( 12, 5)
                If m_aryRights(RIGHTSTYPE.VIEW) = 0 Then
                    hplAddBill.Visible = False
                End If
                m_aryRights =  GetUserRightsForPage( 12, 6)
                If m_aryRights(RIGHTSTYPE.VIEW) = 0 Then
                    hplPO.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim dtOpportunity As DataTable
            Dim objProjectList As New Project
            objProjectList.DomainID = Session("DomainId")
            objProjectList.DivisionID = DivID
            objProjectList.bytemode = 1
            objProjectList.ProjectID = ProID
            objProjectList.StageID = StageID
            dtOpportunity = objProjectList.GetOpportunities
            gvBizDocs.DataSource = dtOpportunity
            gvBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvBizDocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBizDocs.RowCommand
        Try
            If e.CommandName = "Delete" Then

                '2) if bill then Delete Bill journal entry and Bill Entry
                If CCommon.ToLong(CType(gvBizDocs.Rows(e.CommandArgument).FindControl("lblBillID"), Label).Text) > 0 Then

                    'Dim lobjReceivePayment As New ReceivePayment
                    'lobjReceivePayment.BizDocsPaymentDetailsId = CType(gvBizDocs.Rows(e.CommandArgument).FindControl("numBizDocsPaymentDetailsId"), Label).Text
                    'lobjReceivePayment.BizDocsPaymentDetId = CType(gvBizDocs.Rows(e.CommandArgument).FindControl("lblBillID"), Label).Text
                    'lobjReceivePayment.BizDocsId = 0
                    'lobjReceivePayment.OppId = 0
                    'lobjReceivePayment.ReturnID = 0
                    'lobjReceivePayment.PaymentType = 2 'for bills
                    'lobjReceivePayment.DomainId = Session("DomainID")
                    'lobjReceivePayment.DeleteReceivePaymentDetails()

                    Dim objJournalEntry As New JournalEntry

                    objJournalEntry.BillID = CType(gvBizDocs.Rows(e.CommandArgument).FindControl("lblBillID"), Label).Text

                    objJournalEntry.DomainID = Session("DomainId")
                    Try
                        objJournalEntry.DeleteJournalEntryDetails()

                        'DeleteLinkingRow(CType(gvBizDocs.Rows(e.CommandArgument).FindControl("lblProOppID"), Label).Text)
                    Catch ex As Exception
                        If ex.Message = "BILL_PAID" Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                            Exit Sub
                        ElseIf ex.Message = "Undeposited_Account" Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                            Exit Sub
                        Else
                            Throw ex
                        End If
                    End Try
                Else
                    Dim objOpport As New COpportunities
                    With objOpport
                        .OpportID = CCommon.ToLong(CType(gvBizDocs.Rows(e.CommandArgument).FindControl("lblOppId"), Label).Text)
                        .DomainID = Session("DomainID")
                        .UserCntID = Session("UserContactID")
                    End With
                    Dim lintCount As Integer
                    lintCount = objOpport.GetAuthoritativeOpportunityCount()
                    If lintCount = 0 Then
                        Try
                            'Dim objAdmin As New CAdmin

                            'objAdmin.DomainID = Session("DomainId")
                            'objAdmin.ModeType = MileStone1.ModuleType.Opportunity
                            'objAdmin.ProjectID = objOpport.OpportID

                            'objAdmin.RemoveStagePercentageDetails()

                            objOpport.DelOpp()
                            DeleteLinkingRow(CType(gvBizDocs.Rows(e.CommandArgument).FindControl("lblProOppID"), Label).Text)
                        Catch ex As Exception
                            If ex.Message.Contains("SERIAL/LOT#_USED") Then
                                litMessage.Text = "Can not delete record because some serial/lot# is used in any sales order."
                            Else
                                litMessage.Text = "Dependent record exists. Cannot be Deleted."
                            End If

                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        End Try
                    Else : litMessage.Text = "Authoritative bizdoc exist, your option is to delete authoritative bizdoc from Purchase Order Details->BizDocs and try again"
                    End If
                End If
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub DeleteLinkingRow(ByVal lngProOppID As Long)
        Try
            '1)Delete Linking Row
            If lngProOppID > 0 Then
                Dim objProject As New Project
                objProject.ProjOppID = lngProOppID
                objProject.DeleteProjectsOpp()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub gvBizDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBizDocs.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btnDelete As Button
                btnDelete = e.Row.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                Dim lblBillID As Label
                lblBillID = e.Row.FindControl("lblBillID")

                Dim hplDetail As HyperLink = CType(e.Row.FindControl("hplDetails"), HyperLink)

                If CCommon.ToLong(lblBillID.Text) > 0 Then
                    Dim lblLink As Label
                    lblLink = e.Row.FindControl("lblLink")
                    lblLink.Visible = True
                    If CType(e.Row.FindControl("lblIntegrated"), Label).Text.ToLower = "true" Then
                        btnDelete.Visible = False
                    End If
                    hplDetail.Attributes.Add("onclick", "OpenBillPaymentDetail('" & DataBinder.Eval(e.Row.DataItem, "numBillId") & "')")
                    hplDetail.Text = "Bill"
                Else
                    btnDelete.Visible = False

                    hplDetail.Attributes.Add("onclick", "OpenOpp('" & DataBinder.Eval(e.Row.DataItem, "numOppId") & "','2')")
                    hplDetail.Text = DataBinder.Eval(e.Row.DataItem, "vcPOppName")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvBizDocs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvBizDocs.RowDeleting

    End Sub
End Class