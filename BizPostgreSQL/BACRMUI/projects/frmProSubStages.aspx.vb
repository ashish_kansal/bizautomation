Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Projects
    Public Class frmProSubStages : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ddlSubStage As System.Web.UI.WebControls.DropDownList
        Protected WithEvents tblSubStage As System.Web.UI.WebControls.Table
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim objSubStages As New SubStages
                    Dim dtSubStages As DataTable
                    objSubStages.DomainID = Session("DomainID")
                    dtSubStages = objSubStages.GetSubStages
                    ddlSubStage.DataSource = dtSubStages
                    ddlSubStage.DataTextField = "vcSubStageName"
                    ddlSubStage.DataValueField = "numSubStageHdrID"
                    ddlSubStage.DataBind()
                    ddlSubStage.Items.Insert(0, "--Select One--")
                    ddlSubStage.Items.FindByText("--Select One--").Value = 0
                    GetDetails()
                End If
                BuildSubStages()
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub GetDetails()
            Try
                Dim dtSubStages As DataTable
                Dim objProSubStages As New ProSubStages()
                objProSubStages.ProId = GetQueryStringVal("Proid")
                objProSubStages.ProStageID = GetQueryStringVal("ProStageID")
                dtSubStages = objProSubStages.GetSubStage
                Session("SubStages") = dtSubStages
                If dtSubStages.Rows.Count <> 0 Then ddlSubStage.Items.FindByValue(dtSubStages.Rows(0).Item("numSubStageID")).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BuildSubStages()
            Try
                Dim dtSubStages As New DataTable
                Dim tblCell As TableCell
                Dim tblrow As TableRow
                Dim chk As CheckBox
                Dim i As Integer
                dtSubStages = Session("SubStages")
                tblSubStage.Rows.Clear()
                If dtSubStages.Rows.Count <> 0 Then
                    For i = 0 To dtSubStages.Rows.Count - 1
                        tblrow = New TableRow
                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        chk = New CheckBox
                        chk.ID = "chkStage~" & i
                        chk.Text = dtSubStages.Rows(i).Item("vcSubStageDetail")
                        If dtSubStages.Rows(i).Item("bitStageCompleted") = True Then
                            dtSubStages.Rows(i).Item("bitStageCompleted") = 1
                            chk.Checked = True
                        Else
                            dtSubStages.Rows(i).Item("bitStageCompleted") = 0
                            chk.Checked = False
                        End If
                        tblCell.Controls.Add(chk)
                        tblrow.Cells.Add(tblCell)
                        tblSubStage.Rows.Add(tblrow)
                    Next
                End If
                Session("SubStages") = dtSubStages
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlSubStage_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSubStage.SelectedIndexChanged
            Try
                If ddlSubStage.SelectedIndex <> 0 Then
                    Dim dtSubStages As DataTable
                    Dim objOpportunity As New MOpportunity()
                    objOpportunity.SalesProcessId = ddlSubStage.SelectedItem.Value
                    dtSubStages = objOpportunity.GetSubStageBySalesProId
                    Session("SubStages") = dtSubStages
                    BuildSubStages()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Save()
            Try
                Dim dtSubStages As DataTable
                Dim objProSubStages As New ProSubStages()
                Dim i As Integer
                objProSubStages.ProId = GetQueryStringVal("Proid")
                objProSubStages.ProStageID = GetQueryStringVal("ProStageID")
                objProSubStages.SubStageID = ddlSubStage.SelectedItem.Value
                dtSubStages = Session("SubStages")
                ViewState("Checked") = 1
                For i = 0 To dtSubStages.Rows.Count - 1
                    Dim chk As CheckBox
                    chk = Page.FindControl("chkStage~" & i)
                    If chk.Checked = True Then
                        dtSubStages.Rows(i).Item("bitStageCompleted") = 1
                    Else
                        dtSubStages.Rows(i).Item("bitStageCompleted") = 0
                        ViewState("Checked") = 0
                    End If
                Next

                dtSubStages.TableName = "Table"
                Dim dsNew As New DataSet
                dsNew.Tables.Add(dtSubStages.Copy)
                objProSubStages.strSubStages = dsNew.GetXml
                dsNew.Tables.Remove(dsNew.Tables(0))
                objProSubStages.SaveSubStage()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                Response.Write("<script> window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
