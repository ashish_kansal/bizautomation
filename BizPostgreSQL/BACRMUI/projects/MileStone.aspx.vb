﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Public Class MileStone2
    Inherits BACRMPage
    
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

        End If
        GetUserRightsForPage(12, 3)
        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            Response.Redirect("../admin/authentication.aspx?mesg=AC")
        End If
        MileStone1.RightsForPage = m_aryRightsForPage
        MileStone1.ProjectId = GetQueryStringVal( "proid")
        Session("UserContactID") = GetQueryStringVal( "id")
    End Sub

End Class