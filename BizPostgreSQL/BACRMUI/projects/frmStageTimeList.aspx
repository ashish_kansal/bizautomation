﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmStageTimeList.aspx.vb"
    Inherits=".frmStageTimeList" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function OpenTime(a, b, c) {
            window.open('../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&DivId=' + c, '', 'toolbar=no,titlebar=no,left=500, top=300,width=800,height=400,scrollbars=yes,resizable=yes');
            self.close();
            return false;
        }
        function OpenDetails(a, b, c, d) {
            window.open('../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&DivId=' + c + '&CatHdrId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
        function Close() {
            opener.__doPostBack('btnChangeTimeExpense', '');
            self.close();
        }
        function OpenOpp(a, b) {
            if (a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                window.opener.location.href = str;
                window.close();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:HyperLink ID="hplAddTime" runat="server" CssClass="hyperlink" Text="Add time"></asp:HyperLink>
            &nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="" ID="lblStageName" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgTime" CssClass="dg" Width="600px" runat="server" BorderColor="white"
        AutoGenerateColumns="false" AllowSorting="true">
        <AlternatingItemStyle CssClass="ais" />
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="numCategoryHDRID" HeaderText="" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="TimeType" HeaderText="Type"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcItemName" HeaderText="Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="numUnitHour" HeaderText="Hours"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Order ID">
                <ItemTemplate>
                    <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','1')">
                        <%# Eval("vcPOppName") %>
                    </a>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="ContactName" HeaderText="Entered By"></asp:BoundColumn>
            <asp:BoundColumn DataField="dtDateEntered" HeaderText="Date Entered"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Amount">
                <ItemTemplate>
                    <%#String.Format("{0:##,#00.00}", Eval("monPrice") * Eval("numUnitHour"))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Details">
                <ItemTemplate>
                    <asp:HyperLink ID="hplDetails" runat="server" CssClass="hyperlink" Text="Details"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
