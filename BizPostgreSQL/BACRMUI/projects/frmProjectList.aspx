<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/GridMaster.Master"
    CodeBehind="frmProjectList.aspx.vb" Inherits="BACRM.UserInterface.Projects.frmProjectList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="childHead" ContentPlaceHolderID="head" runat="server">
    <title>Projects</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">
        function OpenSetting() {
            window.open('../Prospects/frmConfCompanyList.aspx?FormId=13', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenPro(a, b) {
            var str
            str = "../projects/frmProjects.aspx?frm=ProjectList&ProId=" + a
            document.location.href = str
        }
        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            document.getElementById('txtDelContactIds').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        //var prg_width = 60;
        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }
    </script>
    <style>
        .ProgressBar {
            height: 5px;
            width: 0px;
            background-color: Green;
        }

        .ProgressContainer {
            border: 1px solid black;
            height: 5px;
            font-size: 1px;
            background-color: White;
            line-height: 0;
        }

        .table-bordered th {
            text-align: center;
        }

        .search-right {
            text-align: right;
        }

        @media (max-width:767px) {
            .search-right {
                text-align: initial;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="childUpperContent" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:RadioButton ID="radOpen" GroupName="rad" AutoPostBack="true" runat="server" Text="Open" Checked="true" OnCheckedChanged="radOpen_CheckedChanged" />
                <asp:RadioButton ID="radClosed" GroupName="rad" AutoPostBack="true" runat="server" Text="Finished" OnCheckedChanged="radClosed_CheckedChanged" />
                <asp:RadioButton ID="radCanceled" GroupName="rad" AutoPostBack="true" runat="server" Text="Canceled" OnCheckedChanged="radCanceled_CheckedChanged" />
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnGo" Style="display: none;" CssClass="btn btn-sm btn-primary" runat="server"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
                <button id="btnAddNewProject" onclick="return OpenPopUp('../Projects/frmProjectAdd.aspx');" class="btn btn-sm btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                <asp:LinkButton ID="btnDelete" CssClass="btn btn-sm btn-danger" runat="server"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Projects&nbsp;<a href="#" onclick="return OpenHelpPopUp('projects/frmprojectlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
        <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
            CssClass="table table-bordered table-striped" Width="98%" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="Center">
            <Columns>
            </Columns>
            <EmptyDataTemplate>
                No records Found.
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtDelContactIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
</asp:Content>
