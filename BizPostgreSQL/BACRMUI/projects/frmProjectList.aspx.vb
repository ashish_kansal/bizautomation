'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Projects
    Public Class frmProjectList : Inherits BACRMPage

        Dim strColumn As String
        Dim RegularSearch As String
        Dim CustomSearch As String
        Dim m_aryRightsOpenView() As Integer
        Dim m_aryRightsFinishedView() As Integer
        Dim m_aryRightsCanceledView() As Integer

        Dim m_aryRightsForViewGridConfiguration() As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                m_aryRightsOpenView = GetUserRightsForPage_Other(12, 1)
                m_aryRightsFinishedView = GetUserRightsForPage_Other(12, 2)
                m_aryRightsCanceledView = GetUserRightsForPage_Other(12, 15)

                If Not IsPostBack Then
                    Dim m_aryRightsDelete() As Integer = GetUserRightsForPage_Other(12, 16)
                    If m_aryRightsDelete(RIGHTSTYPE.VIEW) = 0 Then
                        btnDelete.Visible = False
                    End If

                    Session("ProProcessDetails") = Nothing
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                        Dim strprojectstatus As String = CCommon.ToString(PersistTable(PersistKey.OrderStatus))

                        If strprojectstatus = "2" Then
                            radCanceled.Checked = True
                        ElseIf strprojectstatus = "1" Then
                            radClosed.Checked = True
                        Else
                            radOpen.Checked = True
                        End If
                    End If

                    If m_aryRightsOpenView(RIGHTSTYPE.VIEW) = 0 AndAlso m_aryRightsFinishedView(RIGHTSTYPE.VIEW) = 0 AndAlso m_aryRightsCanceledView(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC&Module=Projects&Permission=Projects Grid")
                    End If

                    If m_aryRightsOpenView(RIGHTSTYPE.VIEW) = 0 Then
                        radOpen.Visible = False
                    Else
                        radOpen.Visible = True
                    End If

                    If m_aryRightsFinishedView(RIGHTSTYPE.VIEW) = 0 Then
                        radClosed.Visible = False
                    Else
                        radClosed.Visible = True
                    End If

                    If m_aryRightsCanceledView(RIGHTSTYPE.VIEW) = 0 Then
                        radCanceled.Visible = False
                    Else
                        radCanceled.Visible = True
                    End If

                    If radOpen.Checked AndAlso Not radOpen.Visible Then
                        If radClosed.Visible AndAlso Not radCanceled.Visible Then
                            radClosed.Checked = True
                        ElseIf radCanceled.Visible AndAlso Not radClosed.Visible Then
                            radCanceled.Checked = True
                        Else
                            radClosed.Checked = True
                        End If
                    ElseIf radClosed.Checked AndAlso Not radClosed.Visible Then
                        If radOpen.Visible AndAlso Not radCanceled.Visible Then
                            radOpen.Checked = True
                        ElseIf radCanceled.Visible AndAlso Not radOpen.Visible Then
                            radCanceled.Checked = True
                        Else
                            radOpen.Checked = True
                        End If
                    ElseIf radCanceled.Checked AndAlso Not radCanceled.Visible Then
                        If radOpen.Visible AndAlso Not radClosed.Visible Then
                            radOpen.Checked = True
                        ElseIf radClosed.Visible AndAlso Not radOpen.Visible Then
                            radClosed.Checked = True
                        Else
                            radOpen.Checked = True
                        End If
                    End If

                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(12, 14)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If

                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 Then
                        radOpen.Checked = True
                        txtGridColumnFilter.Text = ""
                    End If

                    BindDatagrid()
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtProjects As DataTable
                Dim objProjects As New Project

                With objProjects
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .SortCharacter = txtSortChar.Text.Trim()
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "Pro.bintcreateddate"
                    End If
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    If radClosed.Checked Then
                        .ProjectStatus = 27492
                        .UserRightType = m_aryRightsFinishedView(RIGHTSTYPE.VIEW)
                    ElseIf radCanceled.Checked Then
                        .ProjectStatus = 27493
                        .UserRightType = m_aryRightsCanceledView(RIGHTSTYPE.VIEW)
                    ElseIf radOpen.Checked Then
                        .ProjectStatus = 0
                        .UserRightType = m_aryRightsOpenView(RIGHTSTYPE.VIEW)
                    Else
                        .ProjectStatus = 0
                        .UserRightType = 0
                    End If

                    GridColumnSearchCriteria()
                    .CustomSearchCriteria = CustomSearch
                    .RegularSearchCriteria = RegularSearch
                    .DashboardReminderType = CCommon.ToShort(GetQueryStringVal("ReminderType"))
                End With

                btnDelete.Visible = IIf(radClosed.Checked, False, True)

                Dim dsList As DataSet

                dsList = objProjects.GetProjectList
                dtProjects = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objProjects.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                'Persist Form Settings
                Dim strprojectstatus As String
                If radCanceled.Checked Then
                    strprojectstatus = 2
                ElseIf radClosed.Checked Then
                    strprojectstatus = 1
                Else
                    strprojectstatus = 0
                End If

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtProjects.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.OrderStatus, strprojectstatus)
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()

                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(12, 3)

                Dim i As Integer

                For i = 0 To dtProjects.Columns.Count - 1
                    dtProjects.Columns(i).ColumnName = dtProjects.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If
                Dim dtColTemplate As DataTable
                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField
                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 13, objProjects.columnName, objProjects.columnSortOrder)
                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 13, objProjects.columnName, objProjects.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 13)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 13)
                    gvSearch.Columns.Add(Tfield)
                End If

                gvSearch.DataSource = dtProjects
                gvSearch.DataBind()

                If m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#Region "RadComboBox Template"
        Public Class SearchTemplate
            Implements ITemplate

            Dim ItemTemplateType As ListItemType
            Dim dtTable1 As DataTable
            Dim i As Integer = 0
            Dim _DropDownWidth As Double

            Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 600)
                Try
                    ItemTemplateType = type
                    dtTable1 = dtTable
                    _DropDownWidth = DropDownWidth
                Catch ex As Exception
                    Throw ex
                End Try
            End Sub

            Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
                Try


                    i = 0
                    Dim label1 As Label
                    Dim label2 As Label
                    Dim img As System.Web.UI.WebControls.Image
                    Dim table1 As New HtmlTable
                    Dim tblCell As HtmlTableCell
                    Dim tblRow As HtmlTableRow
                    table1.Width = "100%"
                    Dim ul As New HtmlGenericControl("ul")

                    Select Case ItemTemplateType
                        Case ListItemType.Header
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                                li.Style.Add("float", "left")
                                li.Style.Add("text-align", "center")
                                li.Style.Add("font-weight", "bold")
                                li.InnerText = dr("vcFieldName").ToString
                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                        Case ListItemType.Item
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                                li.Style.Add("width", Unit.Pixel(width).Value & "px")
                                li.Style.Add("float", "left")

                                If dr("bitCustomField") = "1" Then
                                    label2 = New Label
                                    label2.CssClass = "normal1"
                                    label2.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                    li.Controls.Add(label2)
                                Else
                                    label1 = New Label
                                    label1.CssClass = "normal1"
                                    label1.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                    li.Controls.Add(label1)
                                End If

                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                    End Select
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

            Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcFieldName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

        End Class
#End Region
        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim m_aryRightsForDelete() As Integer = GetUserRightsForPage_Other(12, 3)
                Dim strProid As String() = txtDelContactIds.Text.Split(",")
                Dim i As Int16 = 0
                Dim lngProID As Long
                Dim lngRecOwnID As Long
                Dim lngTerrID As Long
                Dim ObjProjects As New Project
                For i = 0 To strProid.Length - 1
                    lngProID = strProid(i).Split("~")(0)
                    lngRecOwnID = strProid(i).Split("~")(1)
                    lngTerrID = strProid(i).Split("~")(2)
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lngRecOwnID = Session("UserContactID") Then
                                ObjProjects.ProjectID = lngProID
                                ObjProjects.DomainID = Session("DomainID")
                                If ObjProjects.DeleteProjects = False Then
                                    ShowMessage("Dependent Records Exists.Some Projects Cannot be deleted.")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim dtTerritory As New DataTable
                            dtTerritory = Session("UserTerritory")
                            Dim chkDelete As Boolean = False
                            If lngTerrID = 0 Then
                                chkDelete = True
                            Else
                                Dim j As Integer
                                For j = 0 To dtTerritory.Rows.Count - 1
                                    If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                Next
                            End If
                            If chkDelete = True Then
                                ObjProjects.ProjectID = lngProID
                                ObjProjects.DomainID = Session("DomainID")
                                If ObjProjects.DeleteProjects = False Then
                                    ShowMessage("Dependent Records Exists.Some Projects Cannot be deleted.")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                        ObjProjects.ProjectID = lngProID
                        ObjProjects.DomainID = Session("DomainID")
                        If ObjProjects.DeleteProjects = False Then
                            ShowMessage("Dependent Records Exists.Some Projects Cannot be deleted.")
                        End If
                    End If
                Next
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values_Pro CFWInner WHERE CFWInner.RecId=ProjectsMaster.numProID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValuePro(" & strID(0).Replace("CFW.Cust", "") & ",ProjectsMaster.numProID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" pro.numProID in (select distinct ProjectsMaster.numProID from ProjectsMaster left join CFW_Fld_Values_Pro CFW ON ProjectsMaster.numProID=CFW.RecId where ProjectsMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub radOpen_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub radClosed_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub radCanceled_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace


