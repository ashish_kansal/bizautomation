﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.TimeAndExpense
Public Class frmStageTimeList
    Inherits BACRMPage
    Dim StageID, ProID, DivID As Long
    Public m_aryRightsForTime() As Integer
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            StageID = CCommon.ToLong(GetQueryStringVal("ProStageID"))
            ProID = CCommon.ToLong(GetQueryStringVal("ProID"))
            DivID = CCommon.ToLong(GetQueryStringVal("DivID"))
            m_aryRightsForTime =  GetUserRightsForPage( 12, 11)
            If m_aryRightsForTime(RIGHTSTYPE.ADD) = 0 Then
                hplAddTime.Visible = False
            End If
            If Not IsPostBack Then
                BindGrid()
                
                hplAddTime.Attributes.Add("onclick", "OpenTime('" & StageID.ToString & "','" & ProID.ToString & "','" & DivID.ToString & "')")
                lblStageName.Text = GetQueryStringVal( "StageName")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objTime As New TimeExpenseLeave
            Dim dtTimeDetails As DataTable

            objTime.UserCntID = Session("UserContactId")
            objTime.DomainID = Session("DomainID")
            objTime.ProID = ProID
            objTime.StageId = StageID
            objTime.DivisionID = DivID
            objTime.byteMode = 0
            dtTimeDetails = objTime.GetTimeAndExpenseDetails
            
            dgTime.DataSource = dtTimeDetails
            dgTime.DataBind()
            'If dtTime.Rows.Count > 0 Then
            '    If Not IsDBNull(dtTime.Rows(0)("vcProjectName")) Then
            '        'lblProjectName.Text = dtTime.Rows(0)("vcProjectName")
            '    End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgTime_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTime.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplDetail As HyperLink = CType(e.Item.FindControl("hplDetails"), HyperLink)
                hplDetail.Attributes.Add("onclick", "OpenDetails('" & StageID.ToString & "','" & ProID.ToString & "','" & DivID.ToString & "','" & e.Item.Cells(0).Text.Trim & "')")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class