<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProjects.aspx.vb"
    Inherits="BACRM.UserInterface.Projects.frmProjects" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="ProjectTeams.ascx" TagName="ProjectTeams" TagPrefix="uc1" %>
<%@ Register Src="MileStone.ascx" TagName="MileStone" TagPrefix="uc2" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Src="../common/frmCollaborationUserControl.ascx" TagName="frmCollaborationUserControl" TagPrefix="uc1" %>
<%@ Register Src="../admin/ActionAttendees.ascx" TagName="ActionAttendees" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <%-- <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />--%>
    <title>Projects</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script src="../JavaScript/tinymce/jquery.tinymce.min.js"></script>
    <script src="../JavaScript/tinymce/tinymce.min.js"></script>
    <script src="../JavaScript/biz.projects.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var dateRange = $("[id$=hdnDateRange]").val();
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            prm.add_endRequest(endRequestProject);
        });
        function OpenEmail(a, b) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function sendEmailSummary() {
            $("#divProjectSummary").modal("hide");
            $("[id$=btnOpenEmailProjectSummary]").click();
        }
        function openProjectSummary() {
            var DomainID = '<%= Session("DomainId")%>';
            var ProjectID = $("[id$=hdnProjectID]").val();
            var ClientTimeZoneOffset = '<%=Session("ClientMachineUTCTimeOffset")%>';
            $.ajax({
                type: "POST",
                url:  '../projects/frmProjects.aspx/WebMethodGetProjectSummary',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": DomainID,
                    "ProjectID": ProjectID,
                    "ClientTimeZoneOffset": ClientTimeZoneOffset
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgressProject]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgressProject]").hide();
                },
                success: function (data) {
                    debugger;
                    var Jresponse = $.parseJSON(data.d);
                    $("#divProjectProgressTaskDetails").html("");
                    if (Jresponse.length > 0) {
                        $("#lblProjectProgressName").text(Jresponse[0].vcProjectID);
                        $("#lblProjectProgressDescription").text(Jresponse[0].txtComments);
                        $("#lblProjectProgressCompanyFor").text(Jresponse[0].vcCompanyName);
                        $("#lblProjectProgressCompanyBy").text(Jresponse[0].vcByCompanyName);
                        var i = 1;
                        $(Jresponse).each(function (index, value) {
                            var color = '#fff';
                            if (i % 2 == 0) {
                                color = '#e1e1e1';
                            } 
                            i = i + 1;
                            var htmlAppend = '<div style="margin-top:5px;padding:10px;background-color:' + color+'">';
                            htmlAppend = htmlAppend + '<div class="pull-left"><div class="form-group" style="margin-bottom: 0px;">';
                            htmlAppend = htmlAppend + '<label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblProjectProgressTotalProgress">' + value.numTotalProgress+'%</span></label>';
                            htmlAppend = htmlAppend + '<div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">';
                            htmlAppend = htmlAppend + '<div class="progress-bar progress-bar-primary divProjectProgressTotalProgress" style="width: ' + value.numTotalProgress+'%;"></div>';
                            htmlAppend = htmlAppend + '</div>';
                            htmlAppend = htmlAppend + '</div>';
                            htmlAppend = htmlAppend + '</div>';

                            htmlAppend = htmlAppend + '<div class="pull-right">';
                            
                            htmlAppend = htmlAppend + '<label>On</label>:<span>' + value.dtFinishDate + '</span> <label  style="display:' + $("#hdnIsExternalUser").val() + '">From</label>: <span style="display:' + $("#hdnIsExternalUser").val() + '" id="">' + convertHourMinute(value.TaskDurationComplete) + '</span>, <span style="display:' + $("#hdnIsExternalUser").val() + '" id="">' + value.StartTiime + '</span><label style="display:' + $("#hdnIsExternalUser").val() + '"> &nbsp;To:</label> <span style="display:' + $("#hdnIsExternalUser").val() +'" id="">' + value.FinishTime+' </span>';
                            htmlAppend = htmlAppend + '</div>';
                            htmlAppend = htmlAppend + '<div class="clearfix"></div>';

                            htmlAppend = htmlAppend + "<label>Work Description: </label>"
                            htmlAppend = htmlAppend + "<span>" + value.vcTaskName+"</span> <br/>"
                            htmlAppend = htmlAppend + "<label>By: </label>"
                            htmlAppend = htmlAppend + "<span>" + value.AssignedTo+"</span> <br/>"
                            htmlAppend = htmlAppend + "<label style='display:" + $("#hdnIsExternalUser").val()+"'>Balance Remaining: </label>"
                            htmlAppend = htmlAppend + "<span style='display:" + $("#hdnIsExternalUser").val() +"'>" + convertHourMinute(value.BalanceTime) + "</span> <br/>"
                            htmlAppend = htmlAppend + '</div>';
                            $("#divProjectProgressTaskDetails").append(htmlAppend);
                        });
                    }
                    $("#divProjectSummary").modal("show");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error occurred while loading notes.");
                }
            });
            
        }
        function convertHourMinute(totalMinute) {
            var taskDurationHoursMinutesFormat = '0 hr 0 min';
            if (totalMinute > 0) {
                taskDurationHoursMinutesFormat = "";
                if ((totalMinute / 60) > 0) {
                    taskDurationHoursMinutesFormat = Math.floor((totalMinute / 60)) + " hr ";
                    var minutes = Math.round(((totalMinute / 60) - Math.floor((totalMinute / 60))) * 60) + " min";
                    taskDurationHoursMinutesFormat = taskDurationHoursMinutesFormat + minutes;
                } else {
                    taskDurationHoursMinutesFormat = '0 hr ' + value.TaskDurationComplete + ' min';
                }
            }
            return taskDurationHoursMinutesFormat;
        }
        function pageLoaded() {
            $("span.lblTotalProgress").html($("[id$=hdnTotalProgress]").val() + '%');
            $("div.divTotalProgress").css("width", $("[id$=hdnTotalProgress]").val() + '%');

            LoadTaskNotesTinyMCE();

            // on modal show, focus the editor
            $("#divTaskNotes").on("shown.bs.modal", function() {
                tinyMCE.get('txtTaskNotes').selection.select(tinyMCE.get('txtTaskNotes').getBody(), true);
                tinyMCE.get('txtTaskNotes').selection.collapse(false);
                tinyMCE.get('txtTaskNotes').getBody().focus();
            });

            $("#modalCollaborationMessage").on("shown.bs.modal", function() {
                tinyMCE.get('myTextareaCollabaration').selection.select(tinyMCE.get('myTextareaCollabaration').getBody(), true);
                tinyMCE.get('myTextareaCollabaration').selection.collapse(false);
                tinyMCE.get('myTextareaCollabaration').getBody().focus();
            });

            ShowOnlyMyOpenTasks();

            $("#chkShowOnlyMyOpenTasks").change(function(){
                ShowOnlyMyOpenTasks();
            });
        }

        function endRequestProject(){
            tinymce.remove("#txtTaskNotes");
            LoadTaskNotesTinyMCE();
        }
        function LoadTaskNotesTinyMCE() {
            tinymce.init({
                selector: "#txtTaskNotes",
                plugins: "link, autolink",
                default_link_target: "_blank",
                toolbar: "undo redo | bold italic | link unlink | cut copy paste | bullist numlist",
                menubar: false,
                statusbar: false,
                height: "570px",
            });
        }

        function openAddress(a, b) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AddType=' + a + '&ProID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=500,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=P&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTaskDocuments(a) {
            window.open("../Documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=T&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeletMsg() {
            var bln = confirm("You're about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenTimeAndMaterials(a) {
            window.open("../Projects/frmTimeAndMaterials.aspx?ProID=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=300,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }
        function deleteItem() {
            var bln;
            bln = window.confirm("Delete Seleted Row - Are You Sure ?")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenTemplate(a, b) {
            window.open('../common/templates.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pageid=' + a + '&id=' + b, '', 'toolbar=no,titlebar=no,left=500, top=300,width=350,height=200,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c, d) {
            window.open('../projects/frmStageExpenseList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&DivId=' + c + '&StageName=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTime(a, b, c, d) {
            window.open('../projects/frmStageTimelist.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&DivId=' + c + '&StageName=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckBoxCon(a, b, c) {
            if (parseInt(c) == 1) {
                document.getElementById('chkStage~' + a + '~' + b).checked = true
            }
            else {
                document.getElementById('chkStage~' + a + '~' + b).checked = false
            }
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        alert("The Deal is already Lost !")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }
                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        alert("The Deal is already Closed !")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                }
            }
        }
        function ShowWindow1(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //window.location.reload(true);
                return false;

            }

        }
        function ShowLayout(a, b, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTransfer(url) {
            var StageName;
            StageName = $('#txtStageName').val();
            url = url + '&StageName=' + StageName;
            //alert(url);
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function Save() {
            return validateCustomFields();
        }
        function OpenOpp(a, b) {
            if (a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                window.location.href = str;
            }
        }
        function OpenTimeDetail(a, b, c, d) {
            window.open('../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&DivId=' + c + '&CatHdrId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenBillPaymentDetail(a) {
            window.open('../Accounting/frmAddBill.aspx?BillId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenProjectStageDetail(a, b) {
            var str;
            str = "../projects/frmProjects.aspx?ProId=" + a + "&StageId=" + b + "&SelectedIndex=1";
            window.location.href = str;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenShareRecord(a) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=5", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenDeposit(a) {
            var str;
            str = "../Accounting/frmMakeDeposit.aspx?DepositId=" + a;
            window.location.href = str;
        }
        function OpenWriteCheck(a) {
            var str;
            str = "../Accounting/frmWriteCheck.aspx?CheckHeaderID=" + a;
            window.location.href = str;
        }
        function OpenJournal(a) {
            var str;
            str = "../Accounting/frmNewJournalEntry.aspx?JournalId=" + a;
            window.location.href = str;
        }        

        function ConfirmTaskFinish(taskID) {
            if (confirm("You are about to close out this Task. You�ll no longer be able to edit time or qty. Do you want to proceed?")) {
                var a1 = SaveTimeEntryProject(0, taskID, 4, new Date(), 0, 0, "", false);

                $.when(a1).then(function (data) {
                    $("#radPageView_WIP > table > tbody > tr").each(function (index, e) {
                        if ($(e).find("input.hdnTaskID").length > 0 && parseInt($(e).find("input.hdnTaskID").val()) === taskID) {
                            $(e).find("div[id='divTaskControlsProject']").html("<img src='../images/comflag.png' />");
                        }
                    });
                }, function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        if (objError.Message != null) {
                            alert("Error occurred: " + objError.Message);
                        } else {
                            alert(objError);
                        }
                    } else {
                        alert("Unknown error ocurred");
                    }
                });
            }
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function openCollaborationMessageModal(taskId, dataControl) {
            var positionDetails = $(dataControl).position();
            console.log(positionDetails)

            $("#modalCollaborationMessage").modal("show");
            $("#hdnRecordId").val(taskId);
            getTopic();
        }

        function OpenTaskNotes(taskId){
            try {
                tinyMCE.get('txtTaskNotes').setContent("");

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetTaskNotes',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "taskID": taskId
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgressProject]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgressProject]").hide();
                    },
                    success: function (data) {
                        var obj = $.parseJSON(data.GetTaskNotesResult);
                        
                        if (obj != null && obj.length > 0) {
                            tinyMCE.get('txtTaskNotes').setContent(obj[0].vcNotes);

                            if(JSON.parse(obj[0].bitDone)){
                                $("#chkTaskNotesDone").prop("checked","checked");
                            }
                        }

                        $("#divTaskNotes .btn-save-task-note").attr("onclick","return SaveTaskNotes(" + taskId.toString() + ");");
                        $("#divTaskNotes").modal("show");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error occurred while loading notes.");
                    }
                });
            } catch (e) {
                alert("Unknown error occurred while loading notes.")
            }
            
        }

        function SaveTaskNotes(taskId){
            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/SaveTaskNotes',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "taskID": taskId
                        , "notes": tinyMCE.get('txtTaskNotes').getContent()
                        , "isDone":$("#chkTaskNotesDone").is(":checked")
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgressProject]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgressProject]").hide();
                    },
                    success: function (data) {
                        $("#divTaskNotes").modal("hide");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error occurred while saving notes.");
                    }
                });
            } catch (e) {
                alert("Unknown error occurred while saving notes.")
            }
        }

        function AssgiendToChanged(ddl, taskID) {
            if (parseInt($(ddl).val()) > 0) {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/StagePercentageDetailsTaskService.svc/ChangeAssignee',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "taskID": taskID
                        , "assignedTo": parseInt($(ddl).val())
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgressProject]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgressProject]").hide();
                    },
                    success: function (data) {
                        $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } else {
                alert("Select task assignee.");
            }

            return true;
        }

        function TeamChanged(ddl) {
            $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
            return true;
        }

        function GradeChanged(ddl) {
            $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
            return true;
        }

        function OpenCustomerChangeWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../common/frmChangeOrganization.aspx?numProjectID=' + $("[id$=hdnProjectID]").val(), '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }
        function openProjectNameWindow() {
            $("#divProjectNameChange").modal("show");

            return false;
        }
        function SaveProjectId() {
            $.ajax({
                type: "POST",
                url: '../projects/frmProjects.aspx/WebMethodSaveProjectId',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": <%=Session("DomainID")%>,
                    "strProjectID": $("#<%=txtProjectName.ClientID%>").val(),
                    "ProjectID": <%=hdnProjectID.Value%>
                    }),
                beforeSend: function () {
                    $("[id$=UpdateProgressProject]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgressProject]").hide();
                },
                success: function (data) {
                    $("[id$=UpdateProgressProject]").hide();
                    $("#divProjectNameChange").modal("hide");
                    location.reload();
                }
            });
        }
        function DateRangeChanged(dateRange) {
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");
            $("[id$=hdnDateRange]").val(dateRange);
            __doPostBack('<%= btnDateRange.UniqueID%>', '');

            return true;
        }
        function OpenChangeTimeConfirmation() {
            $("#divTaskTimeConfirmation").modal("show");
            return false;
        }
        function addRemoveTime(timeInMinutes, taskId, startDateTime, endDateTime, asssignTo, taskName, actualTaskTime, vcEmail, vcName) {
            debugger;
            var timeInMinutesId = 2;
            if ($(".chk_" + taskId + " input").prop("checked") == false) {
                timeInMinutesId = 1;
            } else {
                timeInMinutesId = 2;
            }
            var timeSecounds = (timeInMinutes * 60);
            $.ajax({
                type: "POST",
                url: '../projects/frmProjects.aspx/WebMethodAddRemoveTimeFromContract',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": <%=Session("DomainID")%>,
                    "DivisionId": $("#<%=txtDivId.ClientID%>").val(),
                    "TaskId": taskId,
                    "numSecounds": timeSecounds,
                    "Type": timeInMinutesId,
                    "isProjectActivityEmail": 1
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgressProject]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgressProject]").hide();

                },
                success: function (data) {
                    $("[id$=UpdateProgressProject]").hide();
                    var dataArr = $.parseJSON(data.d);
                    if (parseInt(dataArr[0]) == 1) {
                        alert("You need to add more time to the contract before attempting to deduct time from this activity / task");
                        $(".chk_" + taskId + " input").removeAttr("checked")
                    } else if (parseInt(dataArr[0]) == 2) {
                        $("#hdnContractBalanceRemaining").val(parseInt(dataArr[1]));
                        $("#hdnStartDateTime").val(startDateTime);
                        $("#hdnAssignToForContract").val(vcEmail);
                        $("#hdnAssignToForContractName").val(vcName);
                        $("#hdnEndDateTime").val(endDateTime);
                        $("#hdnActualTaskCompletionTime").val(actualTaskTime);
                        $("#hdnSelectedTaskTitle").val(taskName);
                        $("#hdnSelectedTaskID").val(taskId);
                        $("#btnOpenEmailContract").click();
                    }

                    $("#btnSearch").click();
                }
            });
        }
        function ChangeTime(isMasterUpdateAlso) {
            $("#divTaskTimeConfirmation").modal("hide");
            var errorMessage = "";
            var promises = [];

            $("#tblManageWIP tr").each(function () {
                if ($(this).find(".hdnTaskID").length > 0 && parseInt($(this).find(".hdnTaskID").val()) > 0) {
                    if ($(this).find("[id*=txtTaskHours]").length > 0 && $(this).find("[id*=txtTaskMinutes]").length > 0) {
                        if (!$(this).find("[id*=txtTaskHours]").attr("disabled") && !$(this).find("[id*=txtTaskMinutes]").attr("disabled")) {
                            var request = $.ajax({
                                type: "POST",
                                url: '../WebServices/StagePercentageDetailsTaskService.svc/ChangeTime',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "taskID": parseInt($(this).find(".hdnTaskID").val())
                                    , "hours": parseInt($(this).find("[id*=txtTaskHours]").val())
                                    , "minutes": parseInt($(this).find("[id*=txtTaskMinutes]").val())
                                    , "isMasterUpdateAlso": isMasterUpdateAlso
                                }),
                                beforeSend: function () {
                                    $("[id$=UpdateProgressProject]").show();
                                },
                                complete: function () {
                                    $("[id$=UpdateProgressProject]").hide();
                                },
                                success: function (data) {

                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        if (objError.Message != null) {
                                            errorMessage += "<br/>" + $(this).find("#lblTaskName").text() + ":" + objError.Message;
                                        } else {
                                            errorMessage += "<br/>" + $(this).find("#lblTaskName").text() + ":" + objError;
                                        }
                                    } else {
                                        errorMessage += "<br/>" + $(this).find("#lblTaskName").text() + ":" + "Unknown error ocurred";
                                    }
                                }
                            });

                            promises.push(request);
                        }
                    }
                }
            });

            $.when.apply(null, promises).done(function () {
                if (errorMessage.length > 0) {
                    alert("Error occurred while upading change for following task(s):<br/>" + errorMessage);
                } else {
                    alert("Changes are updated successfully.");
                }
            })

            return false;
        }

        function openaddTaskWindow(StageDetailsId) {
            var DomainID = '<%= Session("DomainId")%>';

            $("#taskModal [id$=ddlTaskAssignTo]").val("0");
            $("#taskModal #hdnStageTaskDetailsId").val(StageDetailsId);
            $("#taskModal").modal('show');
        }

        function openEditTaskWindow(TaskId, taskName, hours, minutes, assignedTo) {
            var DomainID = '<%= Session("DomainId")%>';

            $("#taskModal #txtTaskName").val(taskName);
            $("#taskModal #txtTaskMinutes").val(minutes);
            $("#taskModal #txtTaskMinutes").attr("disabled","disabled");
            $("#taskModal #txtTaskHours").val(hours);
            $("#taskModal #txtTaskHours").attr("disabled","disabled");
            $("#taskModal [id$=ddlTaskAssignTo]").val(assignedTo);
            $("#taskModal [id$=ddlTaskAssignTo]").attr("disabled","disabled");
            
            $("#taskModal #hdnStageTaskDetailsId").val(0);
            
            $("#taskModal .btn-add-task").text("Save");
            $("#taskModal .btn-add-task").attr("onclick","UpdateTask(" + TaskId.toString() + ")");

            $("#taskModal").modal('show');
        }

        function UpdateTask(TaskId){
            $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 50
                    , "updateRecordID": TaskId
                    , "updateValueID": 0
                    , "comments": $("#taskModal #txtTaskName").val()
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgressProject]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgressProject]").hide();
                },
                success: function (data) {
                    $("#taskModal").modal('hide');
                    document.location.href = document.location.href;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });

        }

        function AddTasktoStage() {
            var UserContactId = '<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            var StageDetailsId = $("#taskModal #hdnStageTaskDetailsId").val();

            if ($("#taskModal #txtTaskHours").val() == "") {
                $("#taskModal #txtTaskHours").val("0");
            }
            if ($("#taskModal #txtTaskMinutes").val() == "") {
                $("#taskModal #txtTaskMinutes").val("0");
            }
            if ($("#taskModal #hdnStageTaskDetailsId").val() == "0") {
                alert("Please select a stage");
                return false;
            }
            if ($("#taskModal #txtTaskName").val() == "") {
                alert("Please enter task name");
                $("#taskModal #txtTaskName").focus();
                return false;
            } else {
                var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + $("#taskModal #hdnStageTaskDetailsId").val() + "',TaskName:'" + $("#taskModal #txtTaskName").val() + "',Hours:'" + $("#taskModal #txtTaskHours").val() + "',Minutes:'" + $("#taskModal #txtTaskMinutes").val() + "',Assignto:'" + $("#taskModal [id$=ddlTaskAssignTo]").val() + "',ParentTaskId:0,OppID:0,IsTaskClosed:false,numTaskId:0,IsTaskSaved:true,IsAutoClosedTaskConfirmed:false,ProjectID:" + $("[id$=hdnProjectID]").val() + ",intTaskType:'0',WorkOrderID:'0'}";
                $.ajax({
                    type: "POST",
                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTask",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var Jresponse = $.parseJSON(response.d);
                        if (Jresponse == true) {
                            $("#taskModal").modal('hide');
                            $("#taskModal #txtTaskMinutes").val('');
                            $("#taskModal #txtTaskHours").val('');
                            $("#taskModal #txtTaskName").val('');
                            $("#taskModal #hdnStageTaskDetailsId").val(0);
                            document.location.href = document.location.href;
                        }
                    },
                    beforeSend: function () {
                        $("[id$=UpdateProgressProject]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgressProject]").hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            }
        }

        function DeleteTask(btn, taskID) {
            var DomainID = '<%= Session("DomainId")%>';

            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteTask",
                data: "{DomainID:'" + DomainID + "',TaskId:'" + taskID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == "1") {
                        $(btn).closest("tr").remove();
                    }
                },
                beforeSend: function () {
                    $("[id$=UpdateProgressProject]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgressProject]").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        if (objError.Message != null) {
                            alert("Error occurred: " + objError.Message);
                        } else {
                            alert(objError);
                        }
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });

            return false;
        }

        function TeamGradeChanged(type, rb) {
            $("th.thTeamGrid").html(type + " (Capacity Load)");

            $("#tblManageWIP tr").each(function () {
                if ($(this).find("[id$=ddlTeam]").length > 0 && $(this).find("[id$=ddlGrade]").length > 0) {
                    if ($(rb).is(":checked")) {
                        if (type === "Team") {
                            $(this).find("[id$=ddlTeam]").show();
                            $(this).find("[id$=ddlGrade]").hide();
                        } else {
                            $(this).find("[id$=ddlTeam]").hide();
                            $(this).find("[id$=ddlGrade]").show();
                        }
                    } else {
                        if (type === "Team") {
                            $(this).find("[id$=ddlTeam]").hide();
                            $(this).find("[id$=ddlGrade]").show();
                        } else {
                            $(this).find("[id$=ddlTeam]").show();
                            $(this).find("[id$=ddlGrade]").hide();
                        }
                    }
                }
            });
        }

        function OpenBizInvoice(a, b, c, d) {
            if (d == 2 || d == 4) {
                if (a > 0) {
                    window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + a, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
                    return false;
                }
                if (parseInt(c) > 0) {
                    window.open('../Accounting/frmAddBill.aspx?BillID=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
                    return false;
                }
            }
            else {
                if (parseInt(a) > 0 && parseInt(b) > 0) {
                    window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                    return false;
                }
            }
        }

        function ExpandCollapseMilestone(a, divClass){
            if($(a).find("i").hasClass("fa-chevron-circle-down")){
                $("tr." + divClass).each(function(){
                    var trID = $(this).attr("id");
                    $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                    $("tr." + trID.replace("trWIPStage","divWIPStage").replace("trStage","divStage")).each(function(){
                        $(this).hide();
                    });
                });

                $("tr." + divClass).hide();
                $(a).find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
            } else {
                $("tr." + divClass).each(function(){
                    var trID = $(this).attr("id");
                    $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                    $("tr." + trID.replace("trWIPStage","divWIPStage").replace("trStage","divStage")).each(function(){
                        $(this).show();
                    });
                });

                $("tr." + divClass).show();
                $(a).find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
            }            

            return false;
        }

        function ExpandCollapseStage(a, divClass){
            if($(a).find("i").hasClass("fa-chevron-circle-down")){
                $("tr." + divClass).hide();
                $(a).find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
            } else {
                $("tr." + divClass).show();
                $(a).find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
            }            

            return false;
        }

        function ExpandCollapseMilestoneWIP(a, divClass){
            if($(a).find("i").hasClass("fa-chevron-circle-down")){
                $("tr." + divClass).each(function(){
                    var trID = $(this).attr("id");
                    $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                    $("tr." + trID.replace("trWIPStage","divWIPStage").replace("trStage","divStage")).hide();
                });

                $("tr." + divClass).hide();
                $(a).find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
            } else {
                $("tr." + divClass).each(function(){
                    var trID = $(this).attr("id");
                    $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                    $("tr." + trID.replace("trWIPStage","divWIPStage").replace("trStage","divStage")).show();
                });

                $("tr." + divClass).show();
                $(a).find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
            }            

            return false;
        }

        function ExpandCollapseStageWIP(a, divClass){
            if($(a).find("i").hasClass("fa-chevron-circle-down")){
                $("tr." + divClass).hide();
                $(a).find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
            } else {
                $("tr." + divClass).show();                
                $(a).find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
            }            

            return false;
        }

        function ShowOnlyMyOpenTasks(){
            if($("#chkShowOnlyMyOpenTasks").is(":checked")){
                $("#tblTasks tr[id*=trTask]").each(function(){
                    if($(this).css("background-color") == "rgba(255, 255, 0, 0.27)" && $(this).find("input.hdnIsClosed").val() == "0") {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });

                $("#tblTasks tr[id*=trStage]").each(function(){
                    var id = $(this).attr("id");
                    var stageID = id.replace("trStage","");

                    if($("#tblTasks tr[stageID='" + stageID.toString() + "']:visible").length == 0){
                        $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                    } else {
                        $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                    }
                });

                $("#tblTasks tr[id*=trMilestone]").each(function(){
                    var id = $(this).attr("id");
                    var milestoneID = id.replace("trMilestone","");

                    if($("#tblTasks tr[milestoneID='" + milestoneID.toString() + "']:visible").length == 0){
                        $(this).find("a.anchorToggleMilestone").find("i").removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-right");
                    } else {
                        $(this).find("a.anchorToggleMilestone").find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                    }
                });
            } else {
                $("#tblTasks tr").each(function(){
                    $(this).find("a.anchorToggleMilestone").find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                    $(this).find("a.anchorToggleStage").find("i").removeClass("fa-chevron-circle-right").addClass("fa-chevron-circle-down");
                });
                
                $("#tblTasks tr").show();
            }
        }
    </script>
    <style>
        
        .imgProgressIcons{
            cursor:pointer;
            height:35px
        }
        /*  #modalCollaborationMessage.modal .modal-dialog.modal-right {
            right: 0px !important;
            position: absolute !important;
            width: 58% !important;
        }*/

        .collabarationBackGround {
            background-image: url("../images/chat-filled.png");
            background-size: 35px;
            background-repeat: no-repeat;
            text-align: center;
            font-weight: bold;
            font-size: 17px;
            margin-top: 28px;
            background-position-x: center;
            background-position-y: center;
        }

        .tableGroupHeader {
            background: #e1e1e1 !important;
            padding: 10px !important;
        }

        #tblMain tr td pre {
            margin: 0 0 0px;
            font-size: inherit !important;
            font-family: inherit !important;
        }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
            padding: 0px;
        }

        #tblMain > tbody > tr > td {
            border-bottom: 1px solid #ddd !important;
        }

        .widget-user-2 .widget-user-header {
            padding: 2px;
            text-align: center;
        }

        .widget-user-2 .widget-user-username, .widget-user-2 .widget-user-desc {
            margin-left: 0px;
        }

        .form-group pre {
            font-family: INHERIT !IMPORTANT;
            PADDING: 0PX !important;
            margin: 0px !important;
            background-color: transparent !important;
            border: none !important;
            font-size: inherit !important;
        }

        .tr1 {
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
            font-variant: normal;
            color: #000000;
            background: #F2F0F0;
            text-decoration: none;
        }

        .rowpadding {
            padding-left: 100px;
            padding-top: 22px;
        }

        #radPageView_WIP .box {
            border-radius: 0px;
            margin-bottom: 10px;
            box-shadow: none;
        }

        #radPageView_WIP .box-header {
            padding: 5px;
        }

            #radPageView_WIP .box-header > .box-tools {
                top: 2px;
            }

            #radPageView_WIP .box-header .box-title {
                font-size: 15px;
            }

        #divTaskLogProject .RadInput_Default .riTextBox, #divTaskLogProject .RadInputMgr_Default {
            border-color: #d2d6de;
        }

        #divTaskLogProject .RadInput table td.riCell {
            padding-right: 0px;
        }

        #divTaskLogProject .riSpin {
            background-color: #dce3ea !important;
        }

        .manage-wip-date-range li {
            background-color: #dae3f3;
            color: #000;
            font-weight: bold;
        }

            .manage-wip-date-range li a {
                padding: 7px 15px;
                border-top: 0px;
            }

        #radPageView_BOM tr.rtlHeader th {
            white-space: nowrap;
            color: #fff;
            padding: 5px;
            border-left-width: 0px;
            border-right-width: 0px;
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 14px;
        }

        #radPageView_BOM tr td {
            border-left-width: 0px;
            border-right-width: 0px;
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 14px;
        }

            #radPageView_BOM tr td.rtlCF {
                border-left-width: 1px;
            }

            #radPageView_BOM tr td.rtlCL {
                border-right-width: 1px;
            }

        #radPageView_BOM .RadTreeList table.rtlLines td.rtlL, .RadTreeList table.rtlVBorders td.rtlL {
            border-bottom-width: 0px;
        }

        #radPageView_BOM .RadTreeList .rtlTable .rtlRBtm td {
            border-bottom-width: 1px;
        }

        #radPageView_BOM ul {
            margin-bottom: 0px;
        }

        #radPageView_BOM .RadTreeList {
            border-style: none;
        }

        #radPageView_BOM .RadTreeList_Default .rtlA {
            background-color: #fff;
        }

        #divTaskControlsProject .taskTimer, #divTaskControlsProject .taskTimerInitial {
            font-size: 14px;
            color: #00a65a;
        }

        #tdGrossProfitToolTip .tooltip-inner 
        { 
            max-width:100% !important;
            width:500px; 
            height:400px;  
            padding:4px; 
        } 

        .lblHeighlight {
            color: #FF9933;
            border-color: #FF9933;
            padding: 1px 10px;
            border: 2px solid;
            font-weight: bold;
            font-size: 18px;
        }

        .lblHeighlightRed {
            color: red;
            border-color: red;
            padding: 1px 10px;
            border: 2px solid;
            font-weight: bold;
            font-size: 18px;
        }

        .btn-task-finish {
            background-color: white;
            color: black;
            border: 1px solid black;
            font-weight: bold;
            padding-left: 5px;
            padding-right: 5px;
        }

        #divTaskControlsProject {
            min-height:34px
        }

        #divTaskControlsProject .btn-xs {
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
        }

        #divTaskControlsProject .btn > img {
            height:17px;
        }
    </style>
    <script type="text/javascript">
        var arrFieldConfig = new Array();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="">
                <div class="form-inline">
                    <div class="pull-left" style="width: 90%">
                        <div class="callout calloutGroup bg-theme">
                            <asp:Label ID="lblCustomerType" CssClass="customerType" Text="Customer" runat="server"></asp:Label>
                            <span>
                                <u>
                                    <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink" Style="font-size: 16px !important; font-weight: bold !important; font-style: italic !important">
                                    </asp:HyperLink></u>

                                <asp:Label ID="lblRelationCustomerType" CssClass="customerType" runat="server"></asp:Label>


                            </span>
                        </div>
                        <div class="record-small-box record-small-group-box">
                            <strong>
                                <asp:Label ID="lblContactName" runat="server" CssClass="text-color-theme" Text=""></asp:Label></strong>
                            <span class="contact">
                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></span>
                            <a id="btnSendEmail" runat="server" href="#">
                                <img src="../images/msg_unread_small.gif" />
                            </a>
                        </div>
                    </div>
                    <div class="pull-left">
                        <asp:ImageButton runat="server" ID="ibChangeCustomer" OnClientClick="return OpenCustomerChangeWindow();" ImageUrl="~/images/edit28.png" Style="height: 20px; margin-top: 13px;" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row" id="tblMenu" runat="server">
                <div id="trRecordOwner" runat="server">
                    <div class="col-xs-12 col-sm-4" style="display: none">
                        <div class="record-small-box">
                            <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" CssClass="small-box-footer" Text="Record Owner:" ToolTip="Transfer Ownership">
                        Record Owner <i class="fa fa-user"></i>
                            </asp:HyperLink>
                            <div class="inner">
                                <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="record-small-box record-small-group-box createdBySection">

                        <a href="#">
                            <label>Created</label>
                        </a>
                        <span class="innerCreated">
                            <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                        </span>
                        <label>
                            Modified
                        </label>
                        <span class="innerCreated">
                            <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                        </span>
                        <label>Finish</label>
                        <span class="innerCreated">
                            <asp:Label ID="lblFinishedBy" runat="server"></asp:Label>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="pull-right createdBySection">

                <img class="imgProgressIcons" onclick="openProjectSummary()" src="../images/WorkProgressicons.png" />
                <asp:LinkButton ID="btnFinish" runat="server" CssClass="btn btn-success"><i class="fa fa-flag-checkered"></i>&nbsp;&nbsp;Finish Project</asp:LinkButton>
                <asp:LinkButton ID="btnTSave" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnTSaveAndCancel" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnEdit" runat="server" Visible="false" CssClass="btn btn-info"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove</asp:LinkButton>
                <asp:LinkButton ID="btnTCancel" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <label>Project : &nbsp;&nbsp;</label><asp:Label ID="lblProjectDetails" runat="server" ForeColor="Gray" />
    &nbsp;<a href="#" onclick="return OpenHelpPopUp('projects/frmprojects.aspx')"><label class="badge bg-yellow">?</label></a>
    <a href="#" onclick="return openProjectNameWindow();">
        <img style="height: 17px;" src="../images/edit28.png" /></a>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server" ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgressProject" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder">
            </asp:Table>
        </div>
    </div>
    <asp:Button ID="btnUpdateProjectTask" runat="server" Text="Button" OnClick="btnUpdateProjectTask_Click" Style="display: none" />
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" SelectedIndex="0" MultiPageID="radMultiPage_OppTab" AutoPostBack="true">
        <Tabs>
            <telerik:RadTab Text="Project Tasks" Value="ProjectTask" PageViewID="radPageView_Details">
            </telerik:RadTab>
            <telerik:RadTab Text="Manage Project Tasks" Value="ManageProjectTask"
                PageViewID="radPageView_Milestone">
            </telerik:RadTab>
            <telerik:RadTab Text="P&L" Value="Report"
                PageViewID="radPageView_Report" PostBack="true">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_Details" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblTotalProgress"></span></label>
                            <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
                                <div class="progress-bar progress-bar-primary divTotalProgress" style="width: 0%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                       <input type="checkbox" id="chkShowOnlyMyOpenTasks" checked="checked" />&nbsp;&nbsp;<label>Show only my open Tasks</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <asp:HiddenField ID="hdnIsExternalUser" Value="" runat="server" />
                    <div class="table-responsive">
                        <table class="table table-borderless tblPrimary" id="tblTasks">
                            <asp:Repeater ID="rptMilestones" runat="server" OnItemDataBound="rptMilestones_ItemDataBound">
                                <ItemTemplate>
                                    <tr style="margin-top: 10px; color: #fff;" id='<%# "trMilestone" & Eval("numMileStoneID")%>'>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center;width:30px;">
                                            <a class="anchorToggleMilestone" style="color:#fff;font-size:20px" href="#" onclick='<%# ("return ExpandCollapseMilestone(this,""divMilestone" & Eval("numMileStoneID") & """)")%>'><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>
                                        </th>
                                        <th style="white-space: nowrap; padding: 5px;vertical-align:middle">
                                            <%# Eval("vcMileStone")%> - <%# Eval("numTotalProgress")%>% done
                                        </th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;width:45px;vertical-align:middle">Notes</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle">Assignee <i>(Team)</i></th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle">Collaboration</th>
                                        <th style="white-space: nowrap; padding: 5px; width: 145px;vertical-align:middle"></th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle">Started On</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle;display:<%= hdnIsExternalUser.Value%>">Actual Time Spent</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle;display:<%= hdnIsExternalUser.Value%>">Performance vs Expected</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle">Finished On</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align:center;vertical-align:middle">Documents</th>
                                    </tr>
                                    <asp:Repeater ID="rptStages" runat="server" OnItemDataBound="rptStages_ItemDataBound">
                                        <ItemTemplate>
                                            <tr id='<%# "trStage" & Eval("numStageDetailsId")%>' class='<%# "divMilestone" & Eval("numMileStoneID")%>'>
                                                <td style="white-space: nowrap; padding: 5px; text-align: center;width:30px;border-bottom: 1px solid #e4e4e4;">
                                                    <a class="anchorToggleStage" style="font-size:20px" href="#" onclick='<%# "return ExpandCollapseStage(this,""divStage" & Eval("numStageDetailsId") & """)"%>'><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>
                                                </td>
                                                <td colspan="10" style="color: #2a7bb1">
                                                    <div class="form-group" style="margin-bottom: 0px;">
                                                        <label><%# Eval("vcStageName")%></label>
                                                        <span style="font-size: 14px;" class="badge bg-green"><%# Eval("numTotalProgress")%>% done</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptTasks" runat="server" OnItemDataBound="rptTasks_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr id="trTask" milestoneID='<%# Eval("numMileStoneID") %>' stageID='<%# Eval("numStageDetailsId") %>' runat="server" class='<%# "divStage" & Eval("numStageDetailsId") %>' style="border-radius: 2px; background: #f4f4f4; margin-bottom: 2px; border-left: 2px solid #e6e7e8;">
                                                        <td colspan="2" style="font-weight: 600;"><%# Eval("vcTaskName")%></td>
                                                        <td style="white-space: nowrap;width:25px;">
                                                            <%# Eval("vcNotesLink")%></td>
                                                        <td style="white-space: nowrap;"><%# Eval("vcAssignedTo")%> <i><%# " (" & Eval("vcWorkStation") & ")"%></i></td>
                                                        <td style="white-space: nowrap;" class="collabarationBackGround">
                                                            <a href="javascript:void(0)" id="projectTask_<%# Eval("numTaskId")%>" onclick="openCollaborationMessageModal('<%# Eval("numTaskId")%>',this)"><%# Eval("TopicCount")%></a>
                                                        </td>
                                                        <td style="width: 145px; text-align: center; white-space: nowrap;">
                                                            <div id="divTaskControlsProject" runat="server">
                                                                <%# Eval("vcTaskControls")%>
                                                            </div>
                                                        </td>
                                                        <td style="white-space: nowrap; text-align: center">
                                                            <%# Eval("dtPlannedStartDate")%>
                                                        </td>
                                                        <td style="white-space: nowrap; text-align: center;display:<%= hdnIsExternalUser.Value%>">
                                                            <ul class="list-inline">
                                                                <li style="padding-left: 0px; padding-right: 0px"><%# Eval("vcActualTaskTimeHtml")%></li>
                                                                <li style="padding-left: 0px; padding-right: 0px">
                                                                    <img src="../images/timeIconnnnn.png" runat="server" id="imgTimeIconn" style="height: 25px" /></li>
                                                                <li style="padding-left: 0px; padding-right: 0px">
                                                                    <asp:CheckBox ID="chkTimeAdded" runat="server" Checked='<%# Eval("bitTimeAddedToContract")%>' class='<%# "chk_" & Eval("numTaskId")%>' /></li>
                                                            </ul>
                                                        </td>
                                                        <td style="white-space: nowrap; text-align: center;display:<%= hdnIsExternalUser.Value%>">
                                                            <asp:Label ID="lblPerformance" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="white-space: nowrap; text-align: center">
                                                            <%# Eval("vcFinishDate")%>                                                            
                                                        </td>
                                                        <td style="width: 10%">
                                                            <input type="hidden" class="hdnTaskEstimationInMinutes" value='<%# Eval("numTaskEstimationInMinutes") %>' />
                                                            <input type="hidden" class="hdnTaskID" value='<%# Eval("numTaskID") %>' />
                                                            <input type="hidden" class="hdnTimeSpentInMinutes" value='<%# Eval("numTimeSpentInMinutes")%>' />
                                                            <input type="hidden" class="hdnLastStartDate" value='<%# Eval("dtLastStartDate")%>' />
                                                            <input type="hidden" class="hdnIsClosed" value='<%# Eval("bitTaskCompleted")%>' />
                                                            <img src="../images/attachemnt.gif" onclick="OpenTaskDocuments('<%# Eval("numTaskID") %>')" />
                                                            <asp:PlaceHolder ID="plhDocuments" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <asp:Button ID="btnOpenEmailProjectSummary" runat="server" Text="OpenEmailContrac" Style="display: none" />
                        <asp:Button ID="btnOpenEmailContract" runat="server" Text="OpenEmailContrac" Style="display: none" />
                        <asp:HiddenField ID="hdnContractBalanceRemaining" runat="server" />
                        <asp:HiddenField ID="hdnStartDateTime" runat="server" />
                        <asp:HiddenField ID="hdnAssignToForContractName" runat="server" />
                        <asp:HiddenField ID="hdnAssignToForContract" runat="server" />

                        <asp:HiddenField ID="hdnActualTaskCompletionTime" runat="server" />
                        <asp:HiddenField ID="hdnEndDateTime" runat="server" />
                        <asp:HiddenField ID="hdnSelectedTaskTitle" runat="server" />
                        <asp:HiddenField ID="hdnSelectedTaskID" runat="server" />
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Milestone" runat="server">
            <div class="row padbotom10">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblTotalProgress"></span></label>
                            <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
                                <div class="progress-bar progress-bar-primary divTotalProgress" style="width: 0%"></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="pull-right">
                        <ul class="list-inline" style="margin-bottom: 0px;">
                            <li style="vertical-align: top;">
                                 <asp:CheckBox ID="chkIsForExternalUser" AutoPostBack="true" OnCheckedChanged="chkIsForExternalUser_CheckedChanged" runat="server" Text="Don�t display time to external users" />
                                 <asp:Label ID="Label44" Text="[?]" CssClass="tip" runat="server" ToolTip="Because BizAutomation provides you with the ability to share the project task view with your clients via �Permissions & Roles� as  �External Users�, if a project is �Fixed Bid� and not �Time & Materials�, you may not want to share how much actual time was spent by your task assignees, so selecting this check box will hide �Actual Time Spent�, �Performance vs Expected� columns  as well as the �Balance Remaining�, and �From / To� time within the task level and summary reports." />
                            </li>
                            <li style="vertical-align: top;">
                                <telerik:RadDatePicker ID="rdManageWIPDate" runat="server" AutoPostBack="true" DateInput-CssClass="form-control" Width="100" OnSelectedDateChanged="rdManageWIPDate_SelectedDateChanged" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png"></telerik:RadDatePicker>
                            </li>
                            <li>
                                <ul class="nav nav-pills manage-wip-date-range">
                                    <li id="liDateRange1" role="presentation" class="active"><a href="javascript:DateRangeChanged(1);">Day</a></li>
                                    <li id="liDateRange2" role="presentation"><a href="javascript:DateRangeChanged(2);">Week</a></li>
                                    <li id="liDateRange3" role="presentation"><a href="javascript:DateRangeChanged(3);">Month</a></li>
                                </ul>
                                <asp:Button runat="server" ID="btnDateRange" Style="display: none" OnClick="btnDateRange_Click" />
                            </li>
                            <li style="vertical-align: top;">
                                <button class="btn btn-flat btn-primary" onclick="return OpenChangeTimeConfirmation();" id="btnSaveManageTasks" runat="server">Save</button>
                            </li>
                            <li style="vertical-align: top;">
                                <asp:DropDownList CssClass="form-control" ID="ddlBusinessProcess" Style="width: 250px" runat="server"></asp:DropDownList>


                            </li>
                            <li style="vertical-align: top;">
                                <asp:Button ID="btnAddProcessOpportunity" OnClick="btnAddProcessOpportunity_Click" CssClass="btn btn-primary" runat="server" Text="Add Process" />
                            </li>
                            <li style="vertical-align: top;">
                                <asp:Button ID="btnRemoveProcessOpportunity" OnClick="btnRemoveProcessOpportunity_Click" CssClass="btn btn-danger" runat="server" Text="Remove" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-borderless tblPrimary" id="tblManageWIP">
                            <asp:Repeater ID="rptMilestonesManageWIP" runat="server" OnItemDataBound="rptMilestonesManageWIP_ItemDataBound">
                                <ItemTemplate>
                                    <tr style="margin-top: 10px; color: #fff;">
                                        <th style="white-space: nowrap; padding: 5px; text-align: center;width:30px;">
                                            <a style="color:#fff;font-size:20px" href="#" onclick='<%# ("return ExpandCollapseMilestoneWIP(this,""divWIPMilestone" & Eval("numMileStoneID") & """)")%>'><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>
                                        </th>
                                        <th style="white-space: nowrap; padding: 5px;">
                                            <%# Eval("vcMileStone")%> - <%# Eval("numTotalProgress")%>% done
                                        </th>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Hours</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Minutes</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Assignee (Capacity Load)</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Work Station / Team (Capacity Load)</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Actual Start</th>
                                        <%--         <th style="white-space: nowrap;  padding: 5px; text-align: center">Finished</th>--%>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Task Hrs</th>
                                        <th style="white-space: nowrap; padding: 5px; text-align: center">Documents</th>
                                    </tr>
                                    <asp:Repeater ID="rptStagesManageWIP" runat="server" OnItemDataBound="rptStagesManageWIP_ItemDataBound">
                                        <ItemTemplate>
                                            <tr id='<%# "trWIPStage" & Eval("numStageDetailsId")%>' class='<%# "divWIPMilestone" & Eval("numMileStoneID")%>'>
                                                <td style="white-space: nowrap; padding: 5px; text-align: center;width:30px;border-bottom: 1px solid #e4e4e4;">
                                                    <a class="anchorToggleStage" style="font-size:20px" href="#" onclick='<%# "return ExpandCollapseStageWIP(this,""divWIPStage" & Eval("numStageDetailsId") & """)"%>'><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>
                                                </td>
                                                <td colspan="9" style="color: #2a7bb1">
                                                    <div class="form-group" style="margin-bottom: 0px;">
                                                        <label><%# Eval("vcStageName")%></label>
                                                        <span style="font-size: 14px;" class="badge bg-green"><%# Eval("numTotalProgress")%>% done</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptTasksManageWIP" runat="server" OnItemDataBound="rptTasksManageWIP_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr id="trTask" runat="server" class='<%# "divWIPStage" & Eval("numStageDetailsId") %>' style="border-radius: 2px; background: #f4f4f4; margin-bottom: 2px; border-left: 2px solid #e6e7e8;">
                                                        <td colspan="2" style="vertical-align:middle">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <label id="lblTaskName"><%# Eval("vcTaskName")%></label>
                                                                </div>
                                                                <input type="hidden" class="hdnTaskID" value='<%# Eval("numTaskId") %>' />
                                                                <button type="button" class="btn btn-default btn-xs btn-github btn-round" style="border-radius: 50%;" onclick='<%# "openaddTaskWindow(" & Eval("numStageDetailsId") & ")"%>'><i class="fa fa-plus"></i></button>
                                                                <button type="button" class="btn btn-danger btn-xs btn-round" onclick='<%# "return DeleteTask(this," & Eval("numTaskId") & ");" %>' style="border-radius: 50%;"><i class="fa fa-times"></i></button>
                                                                <button type="button" class="btn btn-info btn-xs btn-round" style="border-radius: 50%;" onclick='<%# "openEditTaskWindow(" & Eval("numTaskId") & ",""" & Eval("vcTaskName") & """," & Eval("numHours") & "," & Eval("numMinutes") & "," & Eval("numAssignTo") & ")"%>'><i class="fa fa-pencil"></i></button>
                                                                <asp:Button runat="server" ID="btnStartAgain" CssClass="btn btn-xs btn-success pull-right" Text="Start Again" Visible="false" OnClientClick='<%# "return StartTaskAgainProject(" & Eval("numTaskId") & ");"%>' />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <input type="number" class="form-control" id="txtTaskHours" max="24" min="0" runat="server" style="width: 60px" value='<%# Eval("numHours")%>' />
                                                        </td>
                                                        <td>
                                                            <input type="number" class="form-control" id="txtTaskMinutes" max="59" min="0" runat="server" style="width: 60px" value='<%# Eval("numMinutes")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAssignedTo" ClientIDMode="AutoID" runat="server" CssClass="form-control" ForeColor="Black" onchange='<%# "return AssgiendToChanged(this," & Eval("numTaskId") & ");"%>'>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTeam" ClientIDMode="AutoID" runat="server" CssClass="form-control" ForeColor="Black" onchange="return TeamChanged(this);">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="white-space: nowrap; text-align: center">
                                                            <%# IIf(Convert.ToDateTime(Eval("dtStartDate")).Year = 1900, "", Convert.ToDateTime(Eval("dtStartDate")).ToString(BACRM.BusinessLogic.Common.CCommon.GetValidationDateFormat() & " hh:mm tt"))%>
                                                        </td>
                                                        <%-- <td style="white-space: nowrap; text-align: center">
                                                                    <%# IIf(Convert.ToDateTime(Eval("dtFinishDate")).Year = 1900, "", Convert.ToDateTime(Eval("dtFinishDate")).ToString(BACRM.BusinessLogic.Common.CCommon.GetValidationDateFormat() & " hh:mm tt"))%>
                                                                </td>--%>
                                                        <td style="width: 95px; white-space: nowrap; text-align: center">
                                                            <asp:Label ID="lblEstimatedTaskTime" runat="server" Text='<%# Eval("vcEstimatedTaskTime")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <img src="../images/attachemnt.gif" onclick="OpenTaskDocuments('<%# Eval("numTaskID") %>')" />
                                                            <asp:PlaceHolder ID="plhDocuments" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Report" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    
                        <table class="table table-bordered" style="border:0px">
                            <tr>
                                <td style="text-align: right;vertical-align:central">
                                    <label>Income in A/R:</label></td>
                                <td>
                                    <asp:Label ID="lblAR" runat="server" CssClass="text-green"></asp:Label></td>
                                <td style="text-align: right; padding-left: 20px;vertical-align:central">
                                    <label>Non-Labor Expense:</label></td>
                                <td>
                                    <asp:Label ID="lblNonLaborExpense" runat="server" CssClass="text-red"></asp:Label></td>
                                <td style="text-align: right; padding-left: 20px;vertical-align:central">
                                    <label>Total Income:</label></td>
                                <td>
                                    <asp:Label ID="lblIncome" runat="server"></asp:Label></td>
                                <td rowspan="2" style="text-align:right;vertical-align:middle">
                                    <label style="font-size: 24px;margin-bottom: 0px;">Gross Profit:</label></td>
                                 <td rowspan="2" style="vertical-align:middle">
                                    <asp:Label ID="lblGrossProfit" style="font-size:24px;font-weight:bold;" runat="server" CssClass="text-green"></asp:Label>
                                </td>
                                <td rowspan="2" id="tdGrossProfitToolTip" style="vertical-align:top;border: 0px;width:35px;">
                                    <a data-toggle="tooltip" data-html="true" data-placement="left" href="#" title="<b>Income in A/R:</b> Amounts in A/R from items in sales orders mapped to the project.<br/><b>Expense n A/P:</b> Amounts in A/P from items in purchase orders mapped to the project.<br/><b>Non-Labor Expense:</b> Line item item expenses (from their expense sources) from purchase orders, bills created from vendor invoices (Mapped to the project from money-out | enter bill form), and inventory items (Will be either avg or primary vendor cost, depending on const settings from �Global Settings | Accounting�).<br/><b>Labor Expense:</b> The actual time from completed tasks within projects multiplied by the rate per hour for the assignees of the tasks.<br/><b>Total Income:</b> Includes all income from all items in sales orders mapped to the project (Including income from service items with pending expense source).<br/><b>Income Pending Expense Source:</b> Includes all income from all service items from sales orders mapped to the project that have the �Expense� check box selected within the item record, that have not yet been mapped to a bill (Via Money-Out | Enter Bill).<br/><b>Gross Profit:</b> The net difference between the total income, and the sum-total of non-labor and labor expenses.">[?]</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;vertical-align:central">
                                    <label>Expense in A/P:</label></td>
                                <td>
                                    <asp:Label ID="lblAP" runat="server" CssClass="text-red"></asp:Label></td>
                                <td style="text-align: right; padding-left: 20px;vertical-align:central">
                                    <label>Labor Expense:</label></td>
                                <td>
                                    <asp:Label ID="lblLaborExpense" runat="server" CssClass="text-red"></asp:Label></td>
                                <td style="text-align: right; padding-left: 20px;vertical-align:central">
                                    <label>Income Pending Expense Source:</label></td>
                                <td>
                                    <asp:Label ID="lblPendingExpense" runat="server" CssClass="text-orange"></asp:Label></td>
                            </tr>
                        </table>
                        
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped">
                            <Columns>
                                <asp:BoundField HeaderText="Item (Type) & Description" DataField="vcItem" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Units (UOM)" HeaderStyle-Width="170" DataField="vcUnits" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Income Source" DataField="vcIncomeSource" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Expense Source" DataField="vcExpenseSource" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Income (Expense)" HeaderStyle-Width="170" DataField="vcIncomeExpense" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Net Income or Expense" HeaderStyle-Width="170" DataField="vcNetIncome" ItemStyle-HorizontalAlign="Right" HtmlEncode="false" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="row" id="tblProjectCommission" runat="server">
                <div class="col-xs-12">
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-blue">
                            <h5 class="widget-user-username">Commission</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <div class="table-responsive">
                                <asp:GridView ID="gvProjectCommission" AutoGenerateColumns="False" runat="server" Width="100%" CssClass="table table-bordered table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="vcUserName" HeaderText="User"></asp:BoundField>
                                        <asp:BoundField DataField="EmpRole" HeaderText="User Role"></asp:BoundField>
                                        <asp:BoundField DataField="dtCompletionDate" HeaderText="Completion Date"></asp:BoundField>
                                        <asp:BoundField DataField="monProTotalIncome" HeaderText="Income" HtmlEncode="false" DataFormatString="{0:#,###.00}"></asp:BoundField>
                                        <asp:BoundField DataField="monProTotalExpense" HeaderText="Expense" HtmlEncode="false" DataFormatString="{0:#,###.00}"></asp:BoundField>
                                        <asp:BoundField DataField="monProTotalGrossProfit" HeaderText="Gross Profit" HtmlEncode="false" DataFormatString="{0:#,###.00}"></asp:BoundField>
                                        <asp:BoundField DataField="decCommission" HeaderText="Commission %" HtmlEncode="false"></asp:BoundField>
                                        <asp:BoundField DataField="decTotalCommission" HeaderText="Total Commission" HtmlEncode="false" DataFormatString="{0:#,###.00}"></asp:BoundField>
                                        <asp:BoundField DataField="monPaidAmount" HeaderText="Paid Amount" HtmlEncode="false" DataFormatString="{0:#,###.00}"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:TextBox ID="txtTemplateId" Text="0" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtProcessId" Text="0" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox runat="server" ID="txtDivId" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtProName" Style="display: none" runat="server">
    </asp:TextBox>
    <div id="divTaskTimeConfirmation" class="modal fade">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                </div>
                <div class="modal-body">
                    <p>Would you also like this change to modify the default process for use in  future tasks/records that use this process too ?</p>
                    <br />
                    <ul class="text-center list-inline">
                        <li>
                            <button class="btn btn-primary" onclick="return ChangeTime(false);">Just this record</button></li>
                        <li>
                            <button class="btn btn-primary" onclick="return ChangeTime(true);">Change the default for future records</button></li>
                        <li>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="taskModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Task</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="hdnStageTaskDetailsId" />
                    <input type="hidden" id="hdnIsAnyPendingTask" value="0" />
                    <div class="col-md-6">
                        <label><span class="text-danger">*</span>Task</label>
                        <input type="text" class="form-control" id="txtTaskName" autocomplete="off" maxlength="100" />
                        <div id="txtTaskNameautocomplete-list" class="autocomplete-items" style="display: none">
                            <div>No record found</div>
                        </div>
                    </div>
                    <div class="col-md-1" style="width: 12%;">
                        <label>Hours</label>
                        <input type="number" class="form-control" id="txtTaskHours" max="24" min="0" />
                    </div>
                    <div class="col-md-1" style="width: 12%;">
                        <label>Minutes</label>
                        <input type="number" class="form-control" id="txtTaskMinutes" max="59" min="0" />
                    </div>

                    <div class="col-md-3">
                        <label>Assign to</label>
                        <asp:DropDownList runat="server" ID="ddlTaskAssignTo" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-add-task" onclick="AddTasktoStage()">Add Task</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="divTaskLogProject" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width:1080px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Task Time Log</h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>Total Time:</label>
                                        <span id="spnTimeSpendProject" style="font-size: 14px;" class="badge bg-green"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="tblTaskTimeLogProject">
                                    <thead>
                                        <tr>
                                            <th style="max-width:200px;min-width:200px;white-space: nowrap">Employee</th>
                                            <th style="white-space: nowrap;max-width:112px;min-width:112px;">Action</th>
                                            <th style="width: 160px; white-space: nowrap">When</th>
                                            <th style="white-space: nowrap">Reason for Pause</th>
                                            <th style="width: 30%; white-space: nowrap">Notes</th>
                                            <th style="width: 65px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <ul class="list-unstyled">
                                                    <li><input name="TimeEntryTypeProject" type="radio" value="1" checked="checked" onchange="TimeEntryTypeChangedProject();" /> Single-Day Task</li>
                                                    <li><input name="TimeEntryTypeProject" type="radio" value="2" onchange="TimeEntryTypeChangedProject();" /> Multi-Day Task</li>
                                                </ul>
                                            </td>
                                            <td colspan="4" class="tdSingleDay">
                                                <ul class="list-inline">
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Date</label>
                                                            <telerik:RadDatePicker ID="rdpTaskStartDateSingleProject" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" ShowPopupOnFocus="true" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Start Time</label>
                                                            <telerik:RadTimePicker ID="rtpStartTimeProject" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>End Time</label>
                                                            <telerik:RadTimePicker ID="rtpEndTimeProject" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                        
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayProject" style="vertical-align:top;display:none">
                                                <select id="ddlActionProject" class="form-control" tabindex="50" style="width: 95px;" onchange="return TimeEntryActionChangeProject(this)">
                                                    <option value="1">Start</option>
                                                    <option value="2">Pause</option>
                                                    <option value="3">Resume</option>
                                                    <option value="4">Finish</option>
                                                </select>
                                            </td>
                                            <td class="tdMultiDayProject" style="vertical-align: top; white-space: nowrap;display:none">
                                                <ul class="list-inline">
                                                    <li>
                                                        <telerik:RadDatePicker ID="rdpTaskStartDateProject" TabIndex="51" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                    </li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromHoursProject" TabIndex="52" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromMinutesProject" TabIndex="53" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <asp:RadioButtonList ID="rblStartFromTimeProject" runat="server" TabIndex="54" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayProject" style="vertical-align: top;display:none">
                                                <asp:DropDownList ID="ddlAddReasonForPauseProject" runat="server" TabIndex="66" CssClass="form-control" Style="display: none">
                                                </asp:DropDownList>
                                            </td>
                                            <td  class="tdMultiDayProject" style="vertical-align: top;display:none">
                                                <textarea class="form-control" rows="3" id="txtAddNotesProject" tabindex="67" style="display: none"></textarea>
                                            </td>
                                            <td style="vertical-align: top; white-space: nowrap">
                                                <input type="button" class="btn btn-sm btn-success" id="btnAddTimeEntryProject" tabindex="68" onclick="return AddTimeEntryProject(0)" value="Finish Task" />&nbsp;
                                                <input type="button" class="btn btn-sm btn-default" id="btnCancelTimeEntryProject" tabindex="69" onclick="return CancelTimeEntryProject();" value="Cancel" style="display:none;">
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnTaskLogTaskIDProject" />
                    <button type="button" class="btn btn-success btn-start-again">Start Again & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divReasonForPauseProject" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Reason for pause</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Reason for Pause</label>
                                <asp:DropDownList ID="ddlPauseReasonProject" CssClass="form-control" runat="server">
                                    <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Notes</label>
                                <textarea id="txtPauseNotesProject" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnPausedTaskIDProject" />
                    <button type="button" class="btn btn-primary" onclick="return TaskPausedProject();">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div id="divProjectNameChange" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Change Project Name</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Project Name</label>
                                <asp:TextBox ID="txtProjectName" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="return SaveProjectId();">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divTaskNotes" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Notes</h4>
                </div>
                <div class="modal-body" style="min-height:600px;">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <textarea name="txtTaskNotes" id="txtTaskNotes"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="checkbox-inline" style="margin-right: 10px;">
                        <input type="checkbox" class="" name="chkTaskNotesDone" id="chkTaskNotesDone" /><label for="chkTaskNotesDone">Done</label>
                    </div>
                    <button type="button" class="btn btn-primary btn-save-task-note">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade right" id="divProjectSummary" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>--%>
                    <a href="javascript:void(0)" onclick="sendEmailSummary()" style="float:right;color:#fff"><i class="fa fa-envelope"></i></a>
                </div>
                <div class="modal-body" style="font-size:16px !important">
                   <center><h3> Project Work Completed</h3></center>
                    <label style="padding-left:10px;" id="lblProjectProgressName">-</label>:  &nbsp;<span id="lblProjectProgressDescription">-</span><br />
                    <label style="padding-left:10px;">For: &nbsp;</label><span id="lblProjectProgressCompanyFor">-</span><br />
                    <label style="padding-left:10px;">By: &nbsp;</label><span id="lblProjectProgressCompanyBy">-</span><br />
                    <div id="divProjectProgressTaskDetails"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="modalCollaborationMessage" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Collaboration</h4>
                </div>
                <div class="modal-body">
                    <div class="panel with-nav-tabs panel-primary">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#divCollaborationDetail" style="padding: 5px 10px;">Collaboration Detail</a></li>
                                <li><a data-toggle="tab" href="#divAssociatedContacts" style="padding: 5px 10px;">Associated Contacts</a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="divCollaborationDetail" class="tab-pane fade in active">
                                    <uc1:frmCollaborationUserControl ID="frmCollaborationUserControl" runat="server" />
                                </div>
                                <div id="divAssociatedContacts" class="tab-pane fade">
                                    <uc2:ActionAttendees ID="ActionAttendees1" runat="server" AttendeesFor="2" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnProjectID" runat="server" />
    <asp:HiddenField ID="hdnRecordOwner" runat="server" />
    <asp:HiddenField ID="hdnTerritory" runat="server" />
    <asp:HiddenField runat="server" ID="hdnDateRange" />
    <asp:HiddenField runat="server" ID="hdnTotalProgress" />
    <asp:HiddenField runat="server" ID="hdnBuildProcess" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:Label ID="lblTimeLeft" CssClass="lblHeighlight" runat="server" style="vertical-align:middle"></asp:Label>
    &nbsp;
    <asp:LinkButton ID="btnLayout" runat="server" CssClass="btn btn-default btn-sm" ToolTip="Layout"><i class="fa  fa-columns"></i>&nbsp;&nbsp;Layout</asp:LinkButton>
    &nbsp;
    <asp:LinkButton ID="imgOpenDocument" runat="server"><i class="fa fa-files-o fa-lg"></i></asp:LinkButton>
</asp:Content>
