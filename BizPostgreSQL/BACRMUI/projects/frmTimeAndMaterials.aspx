﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTimeAndMaterials.aspx.vb"
    Inherits=".frmTimeAndMaterials" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Time and Materials</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export to Excel" />&nbsp;<asp:Button
                ID="btnClose" runat="server" CssClass="button" OnClientClick="return Close();"
                Text="Close" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <b>Time and Materials:</b>
    <asp:Label ID="lblProjectName" runat="server" CssClass="normal2" Font-Bold="true"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgTime" CssClass="dg" Width="100%" runat="server" BorderColor="white"
        AutoGenerateColumns="false" AllowSorting="true">
        <AlternatingItemStyle CssClass="ais" />
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="TimeType" HeaderText="Type"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcItemName" HeaderText="Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="numUnitHour" HeaderText="Units/Hours"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcBizDocID" HeaderText="ID"></asp:BoundColumn>
            <asp:BoundColumn DataField="ContactName" HeaderText="Entered/Created By"></asp:BoundColumn>
            <asp:BoundColumn DataField="dtDateEntered" HeaderText="Date Entered"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Amount">
                <ItemTemplate>
                    <%#String.Format("{0:##,#00.00}", Eval("monPrice") * Eval("numUnitHour"))%>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
