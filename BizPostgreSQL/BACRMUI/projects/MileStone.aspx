﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MileStone.aspx.vb" Inherits=".MileStone2" %>

<%@ Register src="MileStone.ascx" tagname="MileStone" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>   
     <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:MileStone ID="MileStone1" runat="server" />
    
    </div>
    </form>
</body>
</html>
