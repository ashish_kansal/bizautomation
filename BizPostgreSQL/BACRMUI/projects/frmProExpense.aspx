<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProExpense.aspx.vb"
    Inherits="BACRM.UserInterface.Projects.frmProExpense" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Expense</title>
</head>
<body>

    <script language="javascript">
        //        function Close() {
        //            window.close();
        //        }
        //        function Save() {
        //            if (document.Form1.txtAmount.value == "") {
        //                alert("Enter Amount")
        //                document.Form1.txtAmount.focus();
        //                return false;
        //            }
        //            if (document.Form1.CalCreated_txtDate.value == "") {
        //                alert("Select Created Date")
        //                document.Form1.CalCreated_txtDate.focus();
        //                return false;
        //            }
        //            if (document.Form1.radBill.checked == true) {
        //                if (document.Form1.ddlOpp.value == "0") {
        //                    alert("Select Opportunity")
        //                    document.Form1.ddlOpp.focus();
        //                    return false;
        //                }
        //            }
        //        }
        function OpenPopup() {
            if (document.Form1.rblCreateBill.checked == true) {
                window.open("../Accounting/frmAddBill.aspx?rtyWR=&uihTR=&tyrCV=&pluYR=&fghTY=&ProID=" + document.getElementById('txtProId').value + "&StageID=" + document.getElementById('txtStageId').value + "&StageName=" + document.getElementById('hdnStageName').value, '', 'toolbar=no,titlebar=no,top=200,width=750,height=450,left=200,scrollbars=yes,resizable=yes')
                window.close();
                return false;
            }
            if (document.Form1.rblCreatePO.checked == true) {
                window.open("../Opportunity/frmNewPurchaseOrder.aspx?Source=" + document.getElementById('txtProId').value + "&rtyWR=&uihTR=&tyrCV=" + document.getElementById('txtProId').value + "&pluYR=&fghTY=", '', 'toolbar=no,titlebar=no,top=200,width=1000,height=600,left=200,scrollbars=yes,resizable=yes')
                return false;
            }
        }
    </script>

    <form id="Form1" method="post" runat="server">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnNext" runat="server" CssClass="button" Text="Proceed to next step"
                    OnClientClick="return OpenPopup();"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:RadioButton ID="rblCreatePO" runat="server" GroupName="Expense" Checked="true"
                    Text="Create a  Purchase Order (w the P.O. doc)" CssClass="signup" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:RadioButton ID="rblCreateBill" runat="server" GroupName="Expense" Text="Create a Bill"
                    CssClass="signup" />
            </td>
        </tr>
    </table>
    <%-- <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btndelete" runat="server" CssClass="button" Text="Delete" Width="50">
                </asp:Button>
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                <br>
                <br>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
            </td>
            <td nowrap>
                <asp:RadioButton ID="radBill" runat="server" CssClass="normal1" GroupName="Bill"
                    Text="Billable" Checked="true" />
                <asp:RadioButton ID="radNonBill" runat="server" CssClass="normal1" GroupName="Bill"
                    Text="Non-Billable" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Amount
            </td>
            <td class="normal1">
                <asp:TextBox CssClass="signup" ID="txtAmount" runat="server"></asp:TextBox>
                <asp:CheckBox runat="server" Text="Reimbursable" ID="chkReimb" CssClass="signup" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Created Date
            </td>
            <td>
                <BizCalendar:Calendar ID="CalCreated" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Contract
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="200" runat="server"
                    CssClass="signup">
                </asp:DropDownList>
                (Apply Expense to the contract)
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                Contract Amount Balance :
            </td>
            <td align="left">
                <asp:Label ID="lblRemAmount" runat="server" CssClass="normal1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                Opportunity :
            </td>
            <td>
                <asp:DropDownList ID="ddlOpp" runat="server" CssClass="signup" Width="300">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Description
            </td>
            <td>
                <asp:TextBox TextMode="MultiLine" runat="server" Width="400" Height="50" ID="txtDesc"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2" class="normal4">
                <asp:Literal runat="server" ID="litMessage" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtCategoryHDRID" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtStageId" runat="server" Text="0" Style="display: none"></asp:TextBox>--%>
    
    <asp:TextBox ID="txtDivId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtProId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtStageId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:HiddenField runat="server" ID="hdnStageName" />
    </form>
</body>
</html>
 