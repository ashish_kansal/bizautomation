﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLinkedOrders.aspx.vb"
    Inherits=".frmLinkedOrders" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Linked Sales Orders, Purchase Orders and Bills</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script type="text/javascript" language="javascript">

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function Close() {
            window.close()
            return false;
        }
        function OpenOpp(a, b) {
            if (a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                window.opener.location.href = str;
            }
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <asp:HiddenField ID="hdnDivId" runat="server" />
    <table cellspacing="1" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
            </td>
            <td align="right">
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50"
                    OnClientClick="return Close();"></asp:Button>&nbsp;
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <asp:GridView ID="gvBizDocs" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="dg" Width="100%" AllowSorting="true">
                    <Columns>
                        <asp:BoundField DataField="numProjOppID" HeaderText="numProjOppID" Visible="false" />
                        <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblBillID" Text='<%# Eval("numBillID") %>' Style="display: none"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Type" HeaderText="Type" ControlStyle-ForeColor="White"
                            HeaderStyle-ForeColor="White" />
                        <asp:TemplateField HeaderText="Order ID" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblLink" Visible="false" Text='<%# Eval("ExpenseAccount") %>'></asp:Label>
                                <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>')">
                                    <%# Eval("vcPOppName") %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Auth BizDoc Name" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocID") %>');">
                                    <%#Eval("vcBizDocName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" SortExpression="" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <%#String.Format("{0:##,#00.00}", Eval("monDealAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    CommandArgument='<%# Eval("numProjOppID") %>'></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />
    <fieldset class="normal1">
        <legend><strong>Link Sales BizDoc to Project </strong></legend>
        <br />
        <table width="100%" cellpadding="2" cellspacing="0">
            <tr>
                <td align="right" class="normal1">
                    Select a BizDoc:&nbsp;
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlAuthBizDoc" runat="server" Width="300" CssClass="signup">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left">
                    <asp:Button ID="btnAdd" runat="server" Text="Link to Project " CssClass="button">
                    </asp:Button>
                </td>
            </tr>
        </table>
    </fieldset>
    </form>
</body>
</html>
