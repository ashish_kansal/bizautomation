﻿Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common
Partial Public Class frmTimeAndMaterials
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    
    Sub BindGrid()
        Try
            Dim objTime As New TimeExpenseLeave
            Dim dtTime As DataTable
            objTime.DomainID = Session("DomainId")
            objTime.ProID = GetQueryStringVal( "ProID")
            dtTime = objTime.GetTimeAndMaterials
            dgTime.DataSource = dtTime
            dgTime.DataBind()
            If dtTime.Rows.Count > 0 Then
                If Not IsDBNull(dtTime.Rows(0)("vcProjectName")) Then
                    lblProjectName.Text = dtTime.Rows(0)("vcProjectName")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    
    
    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportToExcel.DataGridToExcel(dgTime, Response)
    End Sub
   
End Class