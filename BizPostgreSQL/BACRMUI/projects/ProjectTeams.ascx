﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProjectTeams.ascx.vb"
    Inherits=".ProjectTeams" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">

    function SaveExternalUser() {
        if ($find('radExternalCompany').get_value() == "") {
            alert("Select Contact Company")
            return false;
        }

        if (document.getElementById("ddlExternalUser").value == 0) {
            alert("Select Contact User")
            document.getElementById("ddlExternalUser").focus();
            return false;
        }
        return true;
    }

    $(document).ready(function () {
        var chkBox = $("[id$=gvExternalUsers] input[id$='chkAll']");
        chkBox.click(
          function () {
              $("[id$=gvExternalUsers] INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
        // To deselect CheckAll when a GridView CheckBox 
        $("[id$=gvExternalUsers] INPUT[type='checkbox']").click(
        function (e) {
            if (!$(this)[0].checked) {
                chkBox.prop("checked", false);
            }
        });
    });
</script>
<telerik:RadTabStrip ID="radOppTabResources" runat="server" UnSelectChildren="True"
    EnableEmbeddedSkins="true" Skin="Default" ClickSelectedTab="True" SelectedIndex="0"
    MultiPageID="radMultiPage_OppTabResources">
    <Tabs>
        <telerik:RadTab Text="Internal Resources" Value="InternalResources" PageViewID="radPageView_InternalResources">
        </telerik:RadTab>
        <telerik:RadTab Text="External Resources" Value="ExternalResources" PageViewID="radPageView_ExternalResources">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="radMultiPage_OppTabResources" runat="server" SelectedIndex="0"
    CssClass="pageView">
    <telerik:RadPageView ID="radPageView_InternalResources" runat="server">
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="pull-right">
                    <asp:LinkButton runat="server" ID="btnTeamManage" CssClass="btn btn-default"
                        OnClientClick="javascript:window.open('ProjectTeamAssign.aspx','','toolbar=no,titlebar=no,top=200,left=400,width=450,height=200,scrollbars=yes,resizable=yes')">
                        Manage Team Member
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnAssign" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvUsers" runat="server" Width="100%" CssClass="table table-bordered table-striped" AllowSorting="true"
                        AutoGenerateColumns="False" DataKeyNames="numContactId" OnRowDataBound="gvUsers_RowDataBound" HeaderStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="vcUserName" HeaderText="Project Member" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="View Rights" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddRight" CssClass="signup" AppendDataBoundItems="true">
                                        <asp:ListItem Value="0" Selected="True">Assigned Stages</asp:ListItem>
                                        <asp:ListItem Value="1">All Stages</asp:ListItem>
                                        <asp:ListItem>---Project Team---</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project Role" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlRole" CssClass="signup">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="radPageView_ExternalResources" runat="server">
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="pull-left">
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="email">Company:</label>
                            <telerik:RadComboBox AccessKey="C" ID="radExternalCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </div>
                        <div class="form-group">
                            <label for="email">Contact</label>
                            <asp:DropDownList ID="ddlExternalUser" CssClass="form-control" runat="server" Width="180">
                            </asp:DropDownList>
                        </div>
                        <asp:Button ID="btnAddExternalResources" Text="Add" CssClass="btn btn-default" runat="server"></asp:Button>
                    </div>
                </div>
                <div class="pull-right">
                    <asp:LinkButton ID="btnSaveExternalResources" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>&nbsp;
                    <asp:LinkButton ID="btnDeleteExternalResources" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvExternalUsers" runat="server" Width="100%" CssClass="table table-bordered table-striped" AllowSorting="true"
                        AutoGenerateColumns="False" DataKeyNames="numContactId" OnRowDataBound="gvUsers_RowDataBound" HeaderStyle-HorizontalAlign="Center">
                        <AlternatingRowStyle CssClass="ais" />
                        <RowStyle CssClass="is"></RowStyle>
                        <HeaderStyle CssClass="hs"></HeaderStyle>
                        <Columns>
                            <asp:BoundField DataField="vcCompanyName" HeaderText="Company" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="vcUserName" HeaderText="Project Member" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="View Rights" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddRight" CssClass="signup" AppendDataBoundItems="true">
                                        <asp:ListItem Value="0" Selected="True">Assigned Stages</asp:ListItem>
                                        <asp:ListItem Value="1">All Stages</asp:ListItem>
                                        <asp:ListItem>---Project Team---</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project Role" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlRole" CssClass="signup">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </telerik:RadPageView>
</telerik:RadMultiPage>


