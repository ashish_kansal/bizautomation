﻿Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common

Partial Public Class frmProjectIncomeExpenseDetails
    Inherits BACRMPage
    Dim lngProId As Long
    'Dim type As Short
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngProId = GetQueryStringVal( "ProId")
            'type = GetQueryStringVal( "Type")
            If Not IsPostBack Then
                
                BindDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDetails()
        Try
            Dim objProject As New Project
            Dim ds As DataSet
            objProject.DomainID = Session("DomainID")
            objProject.ProjectID = lngProId
            objProject.Mode = 1
            ds = objProject.GetProjectIncomeExpense()
            dgDetails.DataSource = ds.Tables(0)
            dgDetails.DataBind()
            'If ds.Tables(0).Rows.Count > 0 Then
            '    lblProfit.Text = "Balance: <b>" & ReturnMoney(CCommon.ToDouble(ds.Tables(0).Compute("sum(IncomeAmount)", "")) - CCommon.ToDouble(ds.Tables(0).Compute("sum(ExpenseAmount)", "")))
            'End If
            If ds.Tables(1).Rows.Count > 0 Then
                lblProjectName.Text = ds.Tables(1).Rows(0)("vcProjectName").ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportToExcel.DataGridToExcel(dgDetails, Response)
    End Sub

    Public Function ReturnMoney(ByVal value As Object) As String
        If value > 0 Then
            Return String.Format("{0:##,#00.00}", value)
        Else
            Return ""
        End If
    End Function

    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            BindDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class