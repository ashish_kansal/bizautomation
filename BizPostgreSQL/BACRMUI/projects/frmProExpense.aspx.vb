Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Projects
    Public Class frmProExpense : Inherits BACRMPage

        'Dim CatHdrId As Long = 0
        Dim m_aryRightsForBill() As Integer
        

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtAmount As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlBizDoc As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtDesc As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
          
            txtProId.Text = GetQueryStringVal( "Proid")
            txtStageId.Text = GetQueryStringVal( "ProStageID")
            hdnStageName.Value = GetQueryStringVal( "StageName").Replace("'", "")
            

            If Not IsPostBack Then
                m_aryRightsForBill =  GetUserRightsForPage( 12, 5)

                If m_aryRightsForBill(RIGHTSTYPE.VIEW) = 0 Then
                    rblCreateBill.Visible = False
                End If
            End If
        End Sub

     
    End Class
End Namespace
