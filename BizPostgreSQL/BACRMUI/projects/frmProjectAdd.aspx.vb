Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow
Namespace BACRM.UserInterface.Projects
    Public Class frmProjectAdd : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region
        

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
            Try
                If Not IsPostBack Then
                    calenderStartDate.SelectedDate = DateTime.Now.Date
                    Dim strModuleName, strPermissionName As String
                    Dim m_aryRightsForProject() As Integer = GetUserRightsForPage_Other(MODULEID.Projects, 1, strModuleName, strPermissionName)

                    If m_aryRightsForProject(RIGHTSTYPE.ADD) = 0 Then
                        Response.Redirect("../admin/authenticationpopup.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                        Exit Sub
                    End If

                    radCmbCompany.Focus()
                    ' = "Project"
                    BindCOA()
                    If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                        objCommon.DivisionID = CCommon.ToLong(Session("UserDivisionID"))
                        objCommon.charModule = "D"
                        radCmbCompany.Enabled = False

                        objCommon.GetCompanySpecificValues1()
                        Dim objAdmin As New Tickler
                        radCmbCompany.Text = objCommon.CompanyName
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                        LoadContacts()
                    ElseIf GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Or GetQueryStringVal("fghTY") <> "" Then
                        If GetQueryStringVal("uihTR") <> "" Then
                            objCommon.ContactID = GetQueryStringVal("uihTR")
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("rtyWR") <> "" Then
                            objCommon.DivisionID = GetQueryStringVal("rtyWR")
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("tyrCV") <> "" Then
                            objCommon.ProID = GetQueryStringVal("tyrCV")
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("pluYR") <> "" Then
                            objCommon.OppID = GetQueryStringVal("pluYR")
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("fghTY") <> "" Then
                            objCommon.CaseID = GetQueryStringVal("fghTY")
                            objCommon.charModule = "S"
                        End If
                        objCommon.GetCompanySpecificValues1()
                        Dim objAdmin As New Tickler
                        'Dim strCompany As String
                        'strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = objCommon.CompanyName
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                        LoadContacts()
                        'If radCmbCompany.SelectedValue <> "" Then LoadOpportunity()
                        If GetQueryStringVal("uihTR") <> "" Then
                            If Not ddlCusomer.Items.FindByValue(objCommon.ContactID) Is Nothing Then
                                ddlCusomer.Items.FindByValue(objCommon.ContactID).Selected = True
                            End If
                        End If
                    End If
                    If GetQueryStringVal("frm") = "outlook" Then
                        Dim objOutlook As New COutlook
                        Dim dttable As DataTable
                        objOutlook.numEmailHstrID = GetQueryStringVal("EmailHstrId")
                        objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dttable = objOutlook.getMail()
                        If dttable.Rows.Count > 0 Then
                            'If dttable.Rows(0).Item("Source") = "I" Then
                            '    Dim objEmails As New Email
                            '    Dim dt As DataTable
                            '    dt = objEmails.GetEmail(GetQueryStringVal( "EmailHstrId"), Session("DomainId"))
                            '    If dt.Rows.Count > 0 Then txtDescription.Text = dt.Rows(0).Item("BodyText")
                            'Else :
                            txtDescription.Text = dttable.Rows(0).Item("vcBodyText")
                            'End If
                        End If
                    End If
                End If
                txtDescription.Focus()
                btnSave.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim arrOutPut As String()
                Dim objProject As New Project
                objProject.ProjectID = 0
                objProject.DivisionID = radCmbCompany.SelectedValue
                objProject.ProComments = ""


                Dim startDate As DateTime = Convert.ToDateTime(calenderStartDate.SelectedDate)
                Dim endDate As DateTime = Convert.ToDateTime(calenderFinishDate.SelectedDate)


                objProject.PlannedStartDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                objProject.RequestedFinish = New DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                objProject.DueDate = New DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                objProject.ProjectName = txtProjectName.Text
                objProject.strProjectID = radCmbCompany.Text & "-" & Format(Now(), "MMMM")
                objProject.UserCntID = Session("UserContactID")
                objProject.OpportunityId = 0
                objProject.IntPrjMgr = ddlInternal.SelectedItem.Value
                objProject.CusPrjMgr = ddlCusomer.SelectedItem.Value
                objProject.DomainID = Session("DomainId")
                objProject.ProComments = txtDescription.Text
                arrOutPut = objProject.Save()
                objProject.ProjectID = arrOutPut(0)

                'Added By Sachin Sadhu||Date:29thJul2014
                'Purpose :To Added Projects data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = CCommon.ToLong(arrOutPut(0))
                objWfA.SaveWFProjectsQueue()
                'end of code


                objProject.AccountID = ddlAccount.SelectedValue
                objProject.UpdateProjectAccount()
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../projects/frmProjects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProjectList&ProId=" & arrOutPut(0) & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Sub LoadOpportunity()
        '    Try
        '        Dim dtOpportunity As DataTable
        '        Dim objProjectList As New Project
        '        objProjectList.DomainID = Session("DomainId")
        '        objProjectList.DivisionID = radCmbCompany.SelectedValue
        '        objProjectList.bytemode = 0
        '        dtOpportunity = objProjectList.GetOpportunities
        '        lstOpportunity.DataSource = dtOpportunity
        '        lstOpportunity.DataTextField = "vcPOppName"
        '        lstOpportunity.DataValueField = "numOppId"
        '        lstOpportunity.DataBind()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Sub LoadContacts()
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlCusomer.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCusomer.DataTextField = "Name"
                    ddlCusomer.DataValueField = "numcontactId"
                    ddlCusomer.DataBind()
                End With
                ddlCusomer.Items.Insert(0, New ListItem("---Select One---", "0"))
                'objCommon.DivisionID = radCmbCompany.SelectedValue
                'objCommon.DomainID = Session("DomainId")
                'objCommon.ContactType = 92
                objCommon.FillAssignToBasedOnPreference(ddlInternal, CCommon.ToLong(radCmbCompany.SelectedValue))
                'ddlInternal.DataSource = objCommon.AssignTo
                'ddlInternal.DataTextField = "vcName"
                'ddlInternal.DataValueField = "ContactID"
                'ddlInternal.DataBind()
                'ddlInternal.Items.Insert(0, "--Select One--")
                'ddlInternal.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub





        'Private Sub radCmbCompany_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbCompany.ItemsRequested
        '    Try
        '        If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then
        '            Dim fillCombo As New COpportunities
        '            With fillCombo
        '                .DomainID = Session("DomainID")
        '                .CompFilter = Trim(e.Text) & "%"
        '                .UserCntID = Session("UserContactID")
        '                radCmbCompany.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
        '                radCmbCompany.DataTextField = "vcCompanyname"
        '                radCmbCompany.DataValueField = "numDivisionID"
        '                radCmbCompany.DataBind()
        '            End With
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                If radCmbCompany.SelectedValue <> "" Then
                    LoadContacts()
                    'LoadOpportunity()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub BindCOA()
            Try
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainId = Session("DomainId")
                objCOA.AccountCode = "0104"
                ddlAccount.DataSource = objCOA.GetParentCategory()
                ddlAccount.DataTextField = "vcAccountName1"
                ddlAccount.DataValueField = "numAccountId"
                ddlAccount.DataBind()
                ddlAccount.Items.Insert(0, "--Select One--")
                ddlAccount.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
