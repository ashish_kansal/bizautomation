Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Projects
    Public Class frmProTime : Inherits BACRMPage

        Dim CatHdrId As Long = 0
        Dim dtOppBiDocItems As DataTable
        Dim dblHourlyRate, dblDailyHours As Double
        Dim type As Short

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtRate As System.Web.UI.WebControls.TextBox
        'Protected WithEvents txtHour As System.Web.UI.WebControls.TextBox
        'Protected WithEvents ddlMin As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim objcom As New CCommon
                CatHdrId = CCommon.ToLong(GetQueryStringVal("CatHdrId"))
                type = CCommon.ToShort(GetQueryStringVal("type"))

                If CCommon.ToLong(GetQueryStringVal("title")) > 0 Then
                    lblTitle.Text = "Change Billable / Non-Billable Hours"
                Else
                    lblTitle.Text = "Select Project"
                End If

                If GetQueryStringVal("Mode") = "TE" Or GetQueryStringVal("Mode") = "BD" Then
                    txtMode.Text = GetQueryStringVal("Mode")

                    pnlProjSelect.Visible = True
                    If txtMode.Text = "BD" Then
                        trProject.Visible = False
                        radNonBill.Enabled = False
                        radBill.Checked = True
                    End If
                End If

                If Not IsPostBack Then
                    
                    Dim objPayroll As New PayrollExpenses

                    txtProId.Text = CCommon.ToLong(GetQueryStringVal("Proid"))
                    txtStageId.Text = CCommon.ToLong(GetQueryStringVal("ProStageID"))
                    txtDivId.Text = CCommon.ToLong(GetQueryStringVal("DivId"))

                    If objPayroll.IsSalariedEmployee(Session("UserID"), Session("DomainID"), dblHourlyRate, dblDailyHours) = True Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "employee", "alert('You will not be able to enter time because you are set-up as a salaried employee. To disable this setting, go to Accounting-> Employee Payroll & Expenses click your name, and within the detail page, uncheck the section that automatically sets salary hours');Close();", True)
                        Exit Sub
                    End If

                    BindUserList()

                    If CatHdrId <> 0 Then
                        getdetails()
                    Else
                        If GetQueryStringVal("Date") <> "" Then
                            calFrom.SelectedDate = GetQueryStringVal("Date")
                            calto.SelectedDate = GetQueryStringVal("Date")
                        Else
                            calFrom.SelectedDate = Now
                            calto.SelectedDate = Now
                        End If
                        LoadContractsInfo(txtDivId.Text)
                        loadOpportunities()
                        LoadServiceItem()
                        getdetails()

                        CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))
                        If CCommon.ToInteger(Session("DefaultClassType")) = 1 AndAlso Not ddlClass.Items.FindByValue(CCommon.ToString(Session("DefaultClass"))) Is Nothing Then
                            ddlClass.ClearSelection()
                            ddlClass.Items.FindByValue(CCommon.ToString(Session("DefaultClass"))).Selected = True
                        End If
                    End If

                    If ddlContract.SelectedValue <> "0" Then
                        txtRate.Enabled = False
                    Else : txtRate.Enabled = True
                    End If
                End If
                If GetQueryStringVal( "frm") <> "TE" Then btnCancel.Attributes.Add("onclick", "window.close();")
                btnSave.Attributes.Add("onclick", "return Save();")
                ' Dim objProject As New ProjectsList
                '                objProject.ProjectId = GetQueryStringVal("Proid")
                'hplRec.Attributes.Add("onclick", "return OpenRec(" & CInt(GetQueryStringVal( "DivId")) & ")")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindUserList()
            'Bind User List 
            Dim dtUserList As DataTable
            
            objCommon.sb_FillConEmpFromDBSel(ddEmployee, Session("DomainID"), 0, 0)

            'Dim objUser As New UserAccess
            'objUser.DomainID = Session("DomainID")
            'objUser.byteMode = 1

            'Dim dt As DataTable = objUser.GetCommissionsContacts

            'Dim item As ListItem
            'Dim dr As DataRow
            'For Each dr In dt.Rows
            '    item = New ListItem()
            '    item.Text = dr("vcUserName")
            '    item.Value = dr("numContactID")
            '    ddEmployee.Items.Add(item)
            'Next
        End Sub
        Sub loadOpportunities()
            Try
                Dim objProjectList As New Project
                objProjectList.DomainID = Session("DomainId")
                objProjectList.DivisionID = txtDivId.Text
                objProjectList.bytemode = 3 'select only sales opportunity
                objProjectList.OppType = 1
                ddlOpp.DataSource = objProjectList.GetOpportunities()
                ddlOpp.DataTextField = "vcPOppName"
                ddlOpp.DataValueField = "numOppId"
                ddlOpp.DataBind()
                ddlOpp.Items.Insert(0, "--Select One--")
                ddlOpp.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadServiceItem()
            Try
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                objItem.Filter = "S"
                objItem.type = 2
                ddlServiceItem.DataSource = objItem.getItemList()
                ddlServiceItem.DataTextField = "vcItemName"
                ddlServiceItem.DataValueField = "numItemCode"
                ddlServiceItem.DataBind()
                ddlServiceItem.Items.Insert(0, "--Select One--")
                ddlServiceItem.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub getdetails()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                Dim dtTimeDetails As DataTable

                objTimeExp.UserCntID = If(CCommon.ToLong(GetQueryStringVal("CntID")) > 0, CCommon.ToLong(GetQueryStringVal("CntID")), Session("UserContactId"))
                objTimeExp.DomainID = Session("DomainID")
                objTimeExp.ProID = CCommon.ToLong(txtProId.Text)
                objTimeExp.StageId = CCommon.ToLong(txtStageId.Text)
                objTimeExp.TEType = 2
                objTimeExp.CategoryID = 1
                objTimeExp.CategoryHDRID = CatHdrId
                objTimeExp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtTimeDetails = objTimeExp.GetTimeAndExpDetails
                If CatHdrId > 0 Then
                    If dtTimeDetails.Rows.Count <> 0 Then

                        If String.IsNullOrEmpty(txtMode.Text) Then
                            If CCommon.ToLong(dtTimeDetails.Rows(0).Item("numOppId")) > 0 Then
                                txtMode.Text = "BD"
                            ElseIf CCommon.ToLong(dtTimeDetails.Rows(0).Item("numProid")) > 0 Then
                                txtMode.Text = "TE"
                            End If
                        End If

                        txtDivId.Text = dtTimeDetails.Rows(0).Item("numDivisionID")
                        txtProId.Text = dtTimeDetails.Rows(0).Item("numProid")
                        txtStageId.Text = dtTimeDetails.Rows(0).Item("numStageid")
                        If CatHdrId <> 0 Then
                            LoadContractsInfo(txtDivId.Text)
                            loadOpportunities()
                        End If
                        LoadServiceItem()

                        If Not dtTimeDetails.Rows(0).Item("numContractId") = 0 AndAlso Not ddlContract.Items.FindByValue(dtTimeDetails.Rows(0).Item("numContractId")) Is Nothing Then
                            ddlContract.Items.FindByValue(dtTimeDetails.Rows(0).Item("numContractId")).Selected = True
                            BindContractinfo()
                        End If

                        txtCategoryHDRID.Text = dtTimeDetails.Rows(0).Item("numCategoryHDRID")
                        txtRate.Text = String.Format("{0:#,##0.00}", dtTimeDetails.Rows(0).Item("monAmount"))

                        If Not ddlOpp.Items.FindByValue(dtTimeDetails.Rows(0).Item("numOppId")) Is Nothing Then
                            ddlOpp.ClearSelection()
                            ddlOpp.Items.FindByValue(dtTimeDetails.Rows(0).Item("numOppId")).Selected = True
                        End If

                        If Not ddlServiceItem.Items.FindByValue(CCommon.ToLong(dtTimeDetails.Rows(0).Item("numItemCode"))) Is Nothing Then
                            ddlServiceItem.ClearSelection()
                            ddlServiceItem.Items.FindByValue(CCommon.ToLong(dtTimeDetails.Rows(0).Item("numItemCode"))).Selected = True
                        End If

                        If Not ddlClass.Items.FindByValue(CCommon.ToLong(dtTimeDetails.Rows(0).Item("numClassID"))) Is Nothing Then
                            ddlClass.ClearSelection()
                            ddlClass.Items.FindByValue(CCommon.ToLong(dtTimeDetails.Rows(0).Item("numClassID"))).Selected = True
                        End If

                        txtNotes.Text = IIf(IsDBNull(dtTimeDetails.Rows(0).Item("txtDesc")), "", dtTimeDetails.Rows(0).Item("txtDesc"))
                        'radBill.Checked = IIf(dtTimeDetails.Rows(0).Item("numType") = 1, True, False)
                        hdnCategoryID.Value = CCommon.ToShort(dtTimeDetails.Rows(0).Item("numCategory"))
                        hdnCategoryType.Value = CCommon.ToShort(dtTimeDetails.Rows(0).Item("numType"))
                        hdnReimburse.Value = CCommon.ToBool(dtTimeDetails.Rows(0).Item("bitReimburse"))

                        If CCommon.ToShort(hdnCategoryID.Value) = 2 AndAlso (CCommon.ToShort(hdnCategoryType.Value) = 1 OrElse CCommon.ToShort(hdnCategoryType.Value) = 6) Then
                            radBill.Checked = True

                            lblTitle.Text = "Change Billable + Reimbursable Expense"
                            trTime1.Visible = False
                            trTime2.Visible = False
                        ElseIf CCommon.ToShort(hdnCategoryID.Value) = 1 AndAlso CCommon.ToShort(hdnCategoryType.Value) = 1 Then
                            radBill.Checked = True
                            trTime1.Visible = True
                            trTime2.Visible = True
                        Else
                            radBill.Checked = False
                            trTime1.Visible = True
                            trTime2.Visible = True
                        End If

                        If radBill.Checked = False Then
                            radNonBill.Checked = True
                            pnlRate.Visible = False
                        End If

                        Dim strNow As Date
                        strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                        If Not IsDBNull(dtTimeDetails.Rows(0).Item("dtFromDate")) Then
                            calFrom.SelectedDate = dtTimeDetails.Rows(0).Item("dtFromDate")
                        Else : calFrom.SelectedDate = strNow
                        End If

                        If Not IsDBNull(dtTimeDetails.Rows(0).Item("dtToDate")) Then
                            calto.SelectedDate = dtTimeDetails.Rows(0).Item("dtToDate")
                        Else : calto.SelectedDate = strNow.AddMinutes(30)
                        End If

                        ddltime.ClearSelection()
                        If Not ddltime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtFromDate"), "h:mm")) Is Nothing Then
                            ddltime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtFromDate"), "h:mm")).Selected = True
                        End If

                        ddlEndTime.ClearSelection()
                        If Not ddlEndTime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtToDate"), "h:mm")) Is Nothing Then
                            ddlEndTime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtToDate"), "h:mm")).Selected = True
                        End If

                        If Format(dtTimeDetails.Rows(0).Item("dtFromDate"), "tt") = "AM" Then
                            chkAM.Checked = True
                        Else : chkPM.Checked = True
                        End If

                        If Format(dtTimeDetails.Rows(0).Item("dtToDate"), "tt") = "AM" Then
                            chkEndAM.Checked = True
                        Else : chkEndPM.Checked = True
                        End If

                        pnlInvoice.Visible = False
                        trNotes.Visible = True
                        'btnSave.Visible = False
                        radBill.Enabled = False
                        radNonBill.Enabled = False

                        pnlOtherInfo.Visible = True

                        If dtTimeDetails.Rows(0).Item("numOppId") > 0 Then
                            hplOrder.Attributes.Add("onclick", "OpenOpp('" & dtTimeDetails.Rows(0).Item("numOppId") & "','1')")
                            hplOrder.Text = dtTimeDetails.Rows(0).Item("vcPOppName")
                        End If

                        If dtTimeDetails.Rows(0).Item("numProId") > 0 Then
                            hplProject.Attributes.Add("onclick", "OpenProject('" & dtTimeDetails.Rows(0).Item("numProId") & "')")
                            hplProject.Text = dtTimeDetails.Rows(0).Item("vcProjectId")
                        End If

                        If dtTimeDetails.Rows(0).Item("numStageId") > 0 Then
                            hplProjectStage.Attributes.Add("onclick", "OpenProjectStageDetail('" & dtTimeDetails.Rows(0).Item("numProId") & "','" & dtTimeDetails.Rows(0).Item("numStageId") & "')")
                            hplProjectStage.Text = dtTimeDetails.Rows(0).Item("vcProjectStageName")
                        End If

                        trClass.Visible = False
                    Else
                        btnSave.Visible = True
                    End If
                Else
                    btndelete.Visible = False

                    dtTimeDetails = objTimeExp.GetTimeAndExpense_BudgetTotal()

                    If dtTimeDetails.Rows.Count <> 0 Then
                        hfTimeBudget.Value = dtTimeDetails.Rows(0).Item("TotalTime")
                        hfbitTimeBudget.Value = dtTimeDetails.Rows(0).Item("bitTimeBudget")
                    Else
                        hfTimeBudget.Value = 0
                        hfbitTimeBudget.Value = False
                    End If
                End If

                If ddEmployee.Items.FindByValue(If(CCommon.ToLong(GetQueryStringVal("CntID")) > 0, CCommon.ToLong(GetQueryStringVal("CntID")), Session("UserContactId"))) IsNot Nothing Then
                    ddEmployee.ClearSelection()
                    ddEmployee.Items.FindByValue(If(CCommon.ToLong(GetQueryStringVal("CntID")) > 0, CCommon.ToLong(GetQueryStringVal("CntID")), Session("UserContactId"))).Selected = True
                End If

                If CCommon.ToLong(txtStageId.Text) > 0 Then
                    Dim _dtStageDetail As New DataTable

                    Dim objAdmin As New CAdmin
                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.SalesProsID = CCommon.ToLong(txtStageId.Text)
                    objAdmin.Mode = 2
                    _dtStageDetail = objAdmin.StageItemDetails()

                    If _dtStageDetail.Rows.Count = 1 Then
                        If ddEmployee.Items.FindByValue(_dtStageDetail.Rows(0).Item("numAssignTo")) IsNot Nothing Then
                            ddEmployee.ClearSelection()
                            ddEmployee.Items.FindByValue(_dtStageDetail.Rows(0).Item("numAssignTo")).Selected = True
                        End If
                    End If
                End If

                TransactionInfo1.ReferenceType = enmReferenceType.TimeAndExpense
                TransactionInfo1.ReferenceID = CatHdrId
                TransactionInfo1.getTransactionInfo()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Dim lngEmpAccountID, lngProjAccountID, lngEmpPayrollExpenseAcntID As Long
        Dim totalAmount As Decimal
        Sub saveDetails()
            Dim objOppBizDocs As New OppBizDocs
            Dim objProject As New Project
            Try
                Dim strStartTime, strEndTime, strStartDate, strEndDate As String
                strStartDate = calFrom.SelectedDate
                strEndDate = calto.SelectedDate
                strStartTime = strStartDate.Trim & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                strEndTime = strEndDate.Trim & " " & ddlEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")

                Dim strSplitStartTimeDate, strSplitEndTimeDate As Date
                If trTime1.Visible = True Then
                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))
                Else
                    strSplitStartTimeDate = CType(strStartTime, DateTime)
                    strSplitEndTimeDate = CType(strEndTime, DateTime)
                End If

                If strSplitStartTimeDate > strSplitEndTimeDate Then
                    litMessage.Text = "Start Time Cannot be Greater Than End Time"
                    Exit Sub
                End If

                Dim hours As Decimal
                hours = DateDiff(DateInterval.Minute, strSplitStartTimeDate, strSplitEndTimeDate) * 0.0167
                If radBill.Checked = True And ddlContract.SelectedValue <> 0 Then

                    If lblRemHours.Text = "-" Then
                        litMessage.Text = "Cannot Save as Contract Hours Not Sufficient"
                        Exit Sub
                    Else
                        If hours > lblRemHours.Text Then
                            litMessage.Text = "Cannot Save as Contract Hours Not Sufficient"
                            Exit Sub
                        End If
                    End If

                End If

                If radBill.Checked = True Then
                    If CCommon.ToBool(hfbitTimeBudget.Value) = True Then
                        If (hours > hfTimeBudget.Value) Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You�ve exceeded the time budgeted for this stage.' );", True)
                            Exit Sub
                        End If

                    End If
                End If

                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.CategoryHDRID = txtCategoryHDRID.Text
                objTimeExp.DivisionID = txtDivId.Text
                objTimeExp.CategoryID = IIf(CCommon.ToShort(hdnCategoryID.Value) = 0, 1, CCommon.ToShort(hdnCategoryID.Value))
                objTimeExp.CategoryType = IIf(radBill.Checked, 1, 2)
                objTimeExp.bitReimburse = CCommon.ToBool(hdnReimburse.Value)
                If radBill.Checked Then
                    If CCommon.ToLong(txtProId.Text) > 0 Then
                        Dim dtDetails As DataTable
                        objProject.ProjectID = txtProId.Text
                        objProject.DomainID = Session("DomainID")
                        objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dtDetails = objProject.ProjectDetail(Of DataTable)()
                        If dtDetails.Rows.Count > 0 Then
                            objTimeExp.ContractID = dtDetails.Rows(0).Item("numcontractId")
                        End If
                    End If
                    objTimeExp.Amount = txtRate.Text.Trim
                Else
                    objTimeExp.ContractID = 0
                    objTimeExp.Amount = 0
                End If

                objTimeExp.FromDate = strSplitStartTimeDate
                objTimeExp.ToDate = strSplitEndTimeDate
                objTimeExp.ProID = txtProId.Text
                objTimeExp.OppID = ddlOpp.SelectedValue
                objTimeExp.StageId = txtStageId.Text
                objTimeExp.CaseID = 0
                objTimeExp.OppBizDocsId = 0
                objTimeExp.Desc = txtNotes.Text.Trim 'IIf(radNonBill.Checked = True, txtNotes.Text, "") '"" '
                objTimeExp.UserCntID = ddEmployee.SelectedValue
                objTimeExp.DomainID = Session("DomainID")
                objTimeExp.TEType = IIf(txtMode.Text = "BD", 1, 2) '2-project time ,1-auth sales bizdoc time
                objTimeExp.ManageTimeAndExpense()

                CatHdrId = objTimeExp.CategoryHDRID

                If radBill.Checked = True Then

                    '2)Add item to invoice
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OpportunityId = 0
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.ItemCode = CCommon.ToLong(ddlServiceItem.SelectedValue)
                    If CCommon.ToLong(ddlServiceItem.SelectedValue) > 0 AndAlso objOpportunity.CheckServiceItemInOrder() = False Then
                        Dim objinvoice As New OppBizDocs
                        objinvoice.ItemCode = ddlServiceItem.SelectedValue
                        objinvoice.ItemDesc = lblServiceDesc.Text & " " & txtNotes.Text.Trim
                        objinvoice.Price = txtRate.Text
                        objinvoice.UnitHour = DateDiff(DateInterval.Minute, strSplitStartTimeDate, strSplitEndTimeDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                        objinvoice.OppId = ddlOpp.SelectedValue
                        objinvoice.OppBizDocId = 0
                        'objinvoice.Notes = txtNotes.Text.Trim
                        objinvoice.CategoryHDRID = CatHdrId
                        objinvoice.ProID = txtProId.Text
                        objinvoice.StageId = IIf(CCommon.ToLong(txtStageId.Text) > 0, CCommon.ToLong(txtStageId.Text), -1)
                        objinvoice.ClassId = ddlClass.SelectedValue
                        objinvoice.AddItemToExistingInvoice()
                    End If

                    '4)Create journal Entry of Time to balance accounting 
                    'If CatHdrId > 0 Then
                    '    'Create Journal Entry
                    '    totalAmount = txtRate.Text * DateDiff(DateInterval.Minute, strSplitStartTimeDate, strSplitEndTimeDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                    '    If totalAmount > 0 Then
                    '        Dim lngJournalId As Long = SaveDataToHeader(TransactionInfo1.JournalID, CatHdrId)
                    '        SaveDataToGeneralJournalDetails(lngJournalId)
                    '    End If
                    'End If

                End If

                If GetQueryStringVal("frm") = "TE" Then
                    Response.Redirect(Page.ResolveClientUrl("~/TimeAndExpense/frmDaydetails.aspx") & "?type=" & type & "&Date=" & GetQueryStringVal("Date") & "&CntID=" & GetQueryStringVal("CntID"))
                ElseIf GetQueryStringVal("frm") = "TimeAndExpDets" Then
                    Response.Redirect(Page.ResolveClientUrl("~/Accounting/frmTimeAndExpensesDetails.aspx") & "?type=" & type & "&NameU=" & GetQueryStringVal("CntID") & "&StartDate=" & GetQueryStringVal("Date") & "&EndDate=" & GetQueryStringVal("EndDate"))
                Else
                    If CCommon.ToLong(GetQueryStringVal("ProStageID")) > 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType, "close", "Close();", True)
                    Else
                        Dim strScript As String = "<script language=JavaScript>"
                        strScript += "Close();"
                        strScript += "</script>"
                        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                saveDetails()
                
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If

            End Try
        End Sub

        Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
            Try
                '0) Delete Time Row and journals entry
                Dim objTimeAndExp As New TimeExpenseLeave
                objTimeAndExp.CategoryHDRID = txtCategoryHDRID.Text
                objTimeAndExp.DomainID = Session("DomainId")
                objTimeAndExp.DeleteTimeExpLeave()
               
                If GetQueryStringVal( "frm") = "TE" Then
                    Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?type=" & type & "&Date=" & GetQueryStringVal("Date") & "&CntID=" & GetQueryStringVal("CntID"))
                ElseIf GetQueryStringVal( "frm") = "TimeAndExpDets" Then
                    Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?type=" & type & "&NameU=" & GetQueryStringVal("CntID") & "&StartDate=" & GetQueryStringVal("Date") & "&EndDate=" & GetQueryStringVal("EndDate"))
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "close", "Close();", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadContractsInfo(ByVal intDivId As Integer)
            Try
                Dim objContract As New CContracts
                Dim dtTable As DataTable
                objContract.DivisionId = intDivId
                objContract.UserCntId = Session("UserContactId")
                objContract.DomainId = Session("DomainId")
                dtTable = objContract.GetContractDdlList()
                ddlContract.DataSource = dtTable

                ddlContract.DataTextField = "vcContractName"
                ddlContract.DataValueField = "numcontractId"
                ddlContract.DataBind()
                ddlContract.Items.Insert(0, "--Select One--")
                ddlContract.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
            Try
                BindContractinfo()
                If ddlContract.SelectedValue <> "0" Then
                    txtRate.Enabled = False
                Else : txtRate.Enabled = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindContractinfo()
            Try
                Dim objContract As New CContracts
                Dim dtTable As DataTable
                objContract.ContractID = ddlContract.SelectedValue
                objContract.CategoryHDRID = txtCategoryHDRID.Text
                objContract.DomainId = Session("DomainId")
                dtTable = objContract.GetContractHrsAmount()
                If dtTable.Rows.Count > 0 Then
                    lblRemHours.Text = IIf(dtTable.Rows(0).Item("bitHour"), String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")), "-")
                    txtRate.Text = dtTable.Rows(0).Item("decRate")
                Else : lblRemHours.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal( "frm") = "TE" Then
                    Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?type=" & type & "&Date=" & GetQueryStringVal("Date") & "&CntID=0")
                ElseIf GetQueryStringVal( "frm") = "TimeAndExpDets" Then
                    Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?type=" & type & "&NameU=" & GetQueryStringVal("CntID") & "&StartDate=" & GetQueryStringVal("Date") & "&EndDate=" & GetQueryStringVal("EndDate"))
                Else : Response.Write("<script>Close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radBill_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBill.CheckedChanged
            Try
                DisplayControls()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub DisplayControls()
            Try
                If radBill.Checked = True Then
                    pnlRate.Visible = True
                    pnlInvoice.Visible = btnSave.Visible
                Else
                    pnlRate.Visible = False
                    pnlInvoice.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radNonBill_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNonBill.CheckedChanged
            Try
                DisplayControls()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlServiceItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlServiceItem.SelectedIndexChanged
            Try
                Dim objItem As New CItems
                If Not objItem.ValidateItemAccount(ddlServiceItem.SelectedValue, Session("DomainID"), 1) = True Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Income Account for selected Item from Administration->Inventory->Item Details')", True)
                    ddlServiceItem.ClearSelection()
                    lblServiceDesc.Text = ""
                    trDesc.Visible = False
                    Exit Sub
                End If
                Dim dtItemDetails As DataTable
                objItem.ItemCode = ddlServiceItem.SelectedValue
                objItem.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
                dtItemDetails = objItem.ItemDetails
                If dtItemDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtItemDetails.Rows(0)("txtItemDesc")) Then
                        lblServiceDesc.Text = dtItemDetails.Rows(0)("txtItemDesc")
                        trDesc.Visible = True
                    End If
                    If Not IsDBNull(dtItemDetails.Rows(0)("monListPrice")) Then
                        txtRate.Text = String.Format("{0:#,##0.00}", dtItemDetails.Rows(0)("monListPrice"))
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Function SaveDataToHeader(lngJournalId As Long, lngCategoryHDRID As Long) As Integer
        '    Try
        '        Dim objJEHeader As New JournalEntryHeader
        '        With objJEHeader
        '            .JournalId = lngJournalId
        '            .RecurringId = 0
        '            .EntryDate = CDate(CDate(calFrom.SelectedDate).ToShortDateString() & " 12:00:00")
        '            .Description = "TimeAndExpense"
        '            .Amount = CCommon.ToDecimal(totalAmount)
        '            .CheckId = 0
        '            .CashCreditCardId = 0
        '            .ChartAcntId = 0
        '            .OppId = 0
        '            .OppBizDocsId = 0
        '            .DepositId = 0
        '            .BizDocsPaymentDetId = 0
        '            .IsOpeningBalance = 0
        '            .LastRecurringDate = Date.Now
        '            .NoTransactions = 0
        '            .CategoryHDRID = lngCategoryHDRID
        '            .ReturnID = 0
        '            .CheckHeaderID = 0
        '            .BillID = 0
        '            .BillPaymentID = 0
        '            .UserCntID = ddEmployee.SelectedValue
        '            .DomainID = Session("DomainID")
        '        End With

        '        lngJournalId = objJEHeader.Save()
        '        Return lngJournalId
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Private Sub SaveDataToGeneralJournalDetails(lngJournalId As Long)
        '    Try
        '        Dim objJEList As New JournalEntryCollection

        '        Dim objJE As New JournalEntryNew()

        '        objJE.TransactionId = 0
        '        objJE.DebitAmt = totalAmount
        '        objJE.CreditAmt = 0
        '        objJE.ChartAcntId = If(txtMode.Text = "BD", lngEmpPayrollExpenseAcntID, lngProjAccountID)
        '        objJE.Description = "TimeAndExpense"
        '        objJE.CustomerId = CCommon.ToLong(txtDivId.Text)
        '        objJE.MainDeposit = 0
        '        objJE.MainCheck = 0
        '        objJE.MainCashCredit = 0
        '        objJE.OppitemtCode = 0
        '        objJE.BizDocItems = ""
        '        objJE.Reference = ""
        '        objJE.PaymentMethod = 0
        '        objJE.Reconcile = False
        '        objJE.CurrencyID = 0
        '        objJE.FltExchangeRate = 0
        '        objJE.TaxItemID = 0
        '        objJE.BizDocsPaymentDetailsId = 0
        '        objJE.ContactID = 0
        '        objJE.ItemID = 0
        '        objJE.ProjectID = CCommon.ToLong(ddlPro.SelectedValue)
        '        objJE.ClassID = CCommon.ToLong(ddlClass.SelectedValue)
        '        objJE.CommissionID = 0
        '        objJE.ReconcileID = 0
        '        objJE.Cleared = 0
        '        objJE.ReferenceType = 0
        '        objJE.ReferenceID = 0

        '        objJEList.Add(objJE)

        '        objJE = New JournalEntryNew()

        '        objJE.TransactionId = 0
        '        objJE.DebitAmt = 0
        '        objJE.CreditAmt = totalAmount
        '        objJE.ChartAcntId = lngEmpAccountID
        '        objJE.Description = "TimeAndExpense"
        '        objJE.CustomerId = CCommon.ToLong(txtDivId.Text)
        '        objJE.MainDeposit = 0
        '        objJE.MainCheck = 0
        '        objJE.MainCashCredit = 0
        '        objJE.OppitemtCode = 0
        '        objJE.BizDocItems = ""
        '        objJE.Reference = ""
        '        objJE.PaymentMethod = 0
        '        objJE.Reconcile = False
        '        objJE.CurrencyID = 0
        '        objJE.FltExchangeRate = 0
        '        objJE.TaxItemID = 0
        '        objJE.BizDocsPaymentDetailsId = 0
        '        objJE.ContactID = 0
        '        objJE.ItemID = 0
        '        objJE.ProjectID = CCommon.ToLong(ddlPro.SelectedValue)
        '        objJE.ClassID = CCommon.ToLong(ddlClass.SelectedValue)
        '        objJE.CommissionID = 0
        '        objJE.ReconcileID = 0
        '        objJE.Cleared = 0
        '        objJE.ReferenceType = 0
        '        objJE.ReferenceID = 0

        '        objJEList.Add(objJE)

        '        objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Private Function SaveDataToHeader(ByVal GrandTotal As Decimal) As Integer
        '    Try
        '        Dim lntJournalId As Integer
        '        Dim lobjAuthoritativeBizDocs As New AuthoritativeBizDocs
        '        Dim dt As DataTable
        '        With lobjAuthoritativeBizDocs
        '            .Amount = Replace(GrandTotal, ",", "")
        '            .DomainId = Session("DomainId")
        '            .JournalId = 0
        '            .UserCntID = Session("UserContactID")
        '            .OppBIzDocID = OppBizDocID
        '            .OppId = ddlOpp.SelectedValue
        '            .CategoryHDRID = CatHdrId
        '            lntJournalId = .SaveDataToJournalEntryHeaderForAuthoritativeBizDocs
        '            Return lntJournalId
        '        End With
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Protected Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                If radCmbCompany.SelectedValue <> "" Then
                    txtDivId.Text = radCmbCompany.SelectedValue
                    If txtMode.Text = "TE" Then LoadPro()
                    loadOpportunities()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub LoadPro()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.DivisionID = radCmbCompany.SelectedValue
                objTimeExp.DomainID = Session("DomainId")
                objTimeExp.byteMode = 0
                ddlPro.DataSource = objTimeExp.GetProByDivID
                ddlPro.DataTextField = "vcProjectName"
                ddlPro.DataValueField = "numProid"
                ddlPro.DataBind()
                ddlPro.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlPro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPro.SelectedIndexChanged
            Try
                txtProId.Text = ddlPro.SelectedValue
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        'Private Sub ValidateEmpAccount()
        '    'Validate Employee's Account ID
        '    Dim ds As New DataSet
        '    Dim objCOA As New ChartOfAccounting
        '    With objCOA
        '        .MappingID = 0
        '        '.ContactTypeID = CContacts.GetContactType(Session("UserContactID"))
        '        .ContactTypeID = CContacts.GetContactType(ddEmployee.SelectedValue)
        '        .DomainId = Session("DomainID")
        '        ds = .GetContactTypeMapping()
        '        If ds.Tables(0).Rows.Count > 0 Then
        '            lngEmpAccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAccountId"))
        '        End If
        '    End With
        'End Sub
    End Class
End Namespace
