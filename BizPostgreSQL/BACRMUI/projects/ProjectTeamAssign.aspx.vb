﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects

Public Class ProjectTeamAssign
    Inherits BACRMPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                btnClose.Attributes.Add("onclick", "return Close()")
                BindControl()
                ProjectUser()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub
    Sub BindControl()
        Try
            'Bind User List 
            Dim dtUserList As DataTable
            objCommon = New CCommon
            dtUserList = objCommon.ConEmpList(Session("DomainId"), False, 0)
            cblUser.DataSource = dtUserList
            cblUser.DataTextField = "vcUserName"
            cblUser.DataValueField = "numContactID"
            cblUser.DataBind()

            'Dim objUser As New UserAccess
            'objUser.DomainID = Session("DomainID")
            'objUser.byteMode = 1

            'Dim dt As DataTable = objUser.GetCommissionsContacts

            'Dim item As ListItem
            'Dim dr As DataRow
            'For Each dr In dt.Rows
            '    item = New ListItem()
            '    item.Text = dr("vcUserName")
            '    item.Value = dr("numContactID")
            '    cblUser.Items.Add(item)
            'Next

            'Bind Project Team 
            objCommon.sb_FillComboFromDBwithSel(ddProjectTeam, 204, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub ProjectUser()
        Try
            Dim objProject As New Project
            Dim dtProTeam As DataTable

            objProject.DomainID = Session("DomainId")
            objProject.ProjectTeamId = ddProjectTeam.SelectedValue
            dtProTeam = objProject.GetProjectTeamContact()
            cblUser.ClearSelection()

            For Each r As DataRow In dtProTeam.Rows
                If cblUser.Items.FindByValue(r.Item("numContactId").ToString()) IsNot Nothing Then
                    cblUser.Items.FindByValue(r.Item("numContactId").ToString()).Selected = True
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ddProjectTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddProjectTeam.SelectedIndexChanged
        ProjectUser()
    End Sub

    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssign.Click

        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numContactId")

            Dim dr As DataRow
            Dim lngVendorAPAccountID As Long

            For i As Integer = 0 To cblUser.Items.Count - 1
                If cblUser.Items(i).Selected Then
                    dr = dt.NewRow()
                    dr("numContactId") = cblUser.Items(i).Value
                    dt.Rows.Add(dr)
                End If
            Next

            ds.Tables.Add(dt)

            Dim objProject As New Project
            Dim dtProTeam As DataTable

            objProject.DomainID = Session("DomainId")
            objProject.ProjectTeamId = ddProjectTeam.SelectedValue
            objProject.ContactList = ds.GetXml()

            objProject.ManageProjectTeamContact()
            ProjectUser()
           
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class