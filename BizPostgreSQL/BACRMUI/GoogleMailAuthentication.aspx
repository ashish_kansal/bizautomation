﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="GoogleMailAuthentication.aspx.vb" Inherits=".GoogleMailAuthentication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Google Mail Authorization</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnContinue" CssClass="btn btn-primary" runat="server" Text="Continue"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Google Mail Authorization
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row" id="divPageError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblPageError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:Label ID="lblInstruction" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>
