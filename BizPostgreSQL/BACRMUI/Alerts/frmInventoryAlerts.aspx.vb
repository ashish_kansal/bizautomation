Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

Public Class frmInventoryAlerts
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents chk1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtEmailAdd As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents ddlDepartment As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chk2 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents ddlEmailTemplate2 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chk3 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtNoofitems As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlEmailTemplate3 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents dgEmaillAddress As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate1)
                    LoadTemplates(ddlEmailTemplate2)
                    LoadTemplates(ddlEmailTemplate3)
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
                    loadDetails()
                    loadEmailAddress()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertID = 15
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetAlertDetails
                hpl1.Attributes.Add("onclick", "return OpenET('" & "25" & "')")
                hpl2.Attributes.Add("onclick", "return OpenET('" & "26" & "')")
                hpl3.Attributes.Add("onclick", "return OpenET('" & "27" & "')")
                If dtDetails.Rows.Count = 3 Then
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintAlertOn")) Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 0 Then
                            chk1.Checked = False
                        Else : chk1.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(1).Item("tintAlertOn")) Then
                        If dtDetails.Rows(1).Item("tintAlertOn") = 0 Then
                            chk2.Checked = False
                        Else : chk2.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(2).Item("tintAlertOn")) Then
                        If dtDetails.Rows(2).Item("tintAlertOn") = 0 Then
                            chk3.Checked = False
                        Else : chk3.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(1).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(1).Item("numDepartment")) Then
                        If Not ddlDepartment.Items.FindByValue(dtDetails.Rows(1).Item("numDepartment")) Is Nothing Then
                            ddlDepartment.Items.FindByValue(dtDetails.Rows(1).Item("numDepartment")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(2).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate3.Items.FindByValue(dtDetails.Rows(2).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate3.Items.FindByValue(dtDetails.Rows(2).Item("numEmailTemplate")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(2).Item("numNoOfItems")) Then txtNoofitems.Text = dtDetails.Rows(2).Item("numNoOfItems")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAlerts As New CAlerts

                'first row
                objAlerts.AlertDTLID = 25
                objAlerts.AlertID = 15
                objAlerts.AlertOn = IIf(chk1.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'second row
                objAlerts.AlertDTLID = 26
                objAlerts.AlertID = 15
                objAlerts.AlertOn = IIf(chk2.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate2.SelectedItem.Value
                objAlerts.Department = ddlDepartment.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                objAlerts.Department = 0

                'Third row
                objAlerts.AlertDTLID = 27
                objAlerts.AlertID = 15
                objAlerts.AlertOn = IIf(chk3.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate3.SelectedItem.Value
                objAlerts.NoofItems = IIf(txtNoofitems.Text = "", 0, txtNoofitems.Text)
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 25
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmailAdd.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress()
                txtEmailAdd.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmaillAddress_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 25
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress.DataSource = dtEmail
                dgEmaillAddress.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
            Try
                LoadTemplates(ddlEmailTemplate1)
                LoadTemplates(ddlEmailTemplate2)
                LoadTemplates(ddlEmailTemplate3)
                loadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
