<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProjectAlert.aspx.vb" Inherits="BACRM.UserInterface.Alerts.frmProjectAlert"%>
<%@OutputCache Duration="1" VaryByParam="*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Alert</title>
	<script language="javascript" type="text/javascript" >
		 function Close()
			 {
			    document.location.href='../Alerts/frmBAM.aspx';
			    return false;
			 }
		function reLoad()
		{
		    document.Form1.btnClick.click();
		}
		function OpenET(a)
		{
		   window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
		</script>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:Button ID="btnClick" runat="server" style="display:none" />
			<table width="100%">
				<tr>
					<td class="text_bold">
						Past Due &amp; Completed Projects
					</td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save" Width="50"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td class="normal1">
						When a Project has not yet been completed
						<asp:TextBox ID="txtDays" Runat="server" CssClass="signup" Width="30"></asp:TextBox>
						days past its due date send
						<asp:DropDownList ID="ddlEmailTemplate1" Runat="server" CssClass="signup"></asp:DropDownList>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk11" Runat="server"></asp:CheckBox>
						To the Internal Project Manager
						<asp:CheckBox ID="chkCCManager11" Runat="server" Text="CC Assignee�s Manager"></asp:CheckBox>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk12" Runat="server"></asp:CheckBox>
						To the External Project Manager
					</td>
				</tr>
				<tr>
					<td class="normal1">
						Also CC the following email addresses:
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox ID="txtEmailAdd1" Runat="server" Width="150" CssClass="signup"></asp:TextBox>
						<asp:Button ID="btnAdd1" Runat="server" CssClass="button" Text="Add" Width="50"></asp:Button>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgEmaillAddress1" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertEmailID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# Container.ItemIndex + 1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="vcEmailID" 
									DataTextField="vcEmailID" HeaderText="Email Address">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:HyperLinkColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="btnHdelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
					
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						When a Project is completed, send
						<asp:DropDownList ID="ddlEmailTemplate2" Runat="server" CssClass="signup"></asp:DropDownList>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl2" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk21" Runat="server"></asp:CheckBox>
						To the Internal Project Manager
						<asp:CheckBox ID="chkCCManager21" Runat="server" Text="CC Assignee�s Manager"></asp:CheckBox>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk22" Runat="server"></asp:CheckBox>
						To the External Project Manager
					</td>
				</tr>
				<tr>
					<td class="normal1">
						Also CC the following email addresses:
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox ID="txtEmail2" Runat="server" Width="150" CssClass="signup"></asp:TextBox>
						<asp:Button ID="btnAdd2" Runat="server" CssClass="button" Text="Add" Width="50"></asp:Button>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgEmaillAddress2" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertEmailID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# Container.ItemIndex + 1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="vcEmailID" 
									DataTextField="vcEmailID" HeaderText="Email Address">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:HyperLinkColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="Button1" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="Button2" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						When a Project Stage is completed, send
						<asp:DropDownList ID="ddlEmailTemplate3" Runat="server" CssClass="signup"></asp:DropDownList>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl3" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk31" Runat="server"></asp:CheckBox>
						To the Internal Project Manager
						<asp:CheckBox ID="chkCCManager31" Runat="server" Text="CC Assignee�s Manager"></asp:CheckBox>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk32" Runat="server"></asp:CheckBox>
						To the External Project Manager
					</td>
				</tr>
				<tr>
					<td class="normal1">
						Also CC the following email addresses:
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox ID="txtEmail3" Runat="server" Width="150" CssClass="signup"></asp:TextBox>
						<asp:Button ID="btnAdd3" Runat="server" CssClass="button" Text="Add" Width="50"></asp:Button>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgEmaillAddress3" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertEmailID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# Container.ItemIndex + 1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="vcEmailID" 
									DataTextField="vcEmailID" HeaderText="Email Address">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:HyperLinkColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="Button3" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="Button4" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
