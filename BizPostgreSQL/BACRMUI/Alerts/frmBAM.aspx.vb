Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

    Public Class frmBAM
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
       

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    GetUserRightsForPage(13, 6)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadBAM()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadBAM()
            Try
                Dim objAlerts As New CAlerts
                Dim dtAlerts As DataTable
                objAlerts.DomainID = Session("DomainID")
                dtAlerts = objAlerts.GetBAM
                ' Show only following 2 Alerts .Other alerts need to tested before we can enable them -chintan

                dtAlerts = dtAlerts.Select(" numAlertID in(2,16)", "").CopyToDataTable()
                dgEmailAlerts.DataSource = dtAlerts
                dgEmailAlerts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgEmailAlerts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEmailAlerts.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hpl As HyperLink
                    hpl = e.Item.FindControl("hplLink")
                    hpl.NavigateUrl = e.Item.Cells(0).Text
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
