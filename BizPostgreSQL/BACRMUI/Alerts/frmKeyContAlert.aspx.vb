Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

Public Class frmKeyContAlert
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents chk1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chkCCManager1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtEmailAdd1 As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd1 As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress As System.Web.UI.WebControls.DataGrid
        Protected WithEvents txtCompName As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnGo As System.Web.UI.WebControls.Button
        Protected WithEvents ddlCompany As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlContact As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnConAdd As System.Web.UI.WebControls.Button
        Protected WithEvents dgContacts As System.Web.UI.WebControls.DataGrid


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate1)
                    loadDetails()
                    loadEmailAddress()
                    LoadContacts()
                    btnClose.Attributes.Add("onclick", "return Close()")
                    btnConAdd.Attributes.Add("onclick", "return Add()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadContacts()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 20
                objAlerts.byteMode = 0
                dtDetails = objAlerts.GetAlertConAndCompany
                dgContacts.DataSource = dtDetails
                dgContacts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertID = 12
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetAlertDetails
                hpl1.Attributes.Add("onclick", "return OpenET('" & "20" & "')")
                If dtDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintAlertOn")) Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 0 Then
                            chk1.Checked = False
                        Else : chk1.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("tintCCManager")) Then
                        If dtDetails.Rows(0).Item("tintCCManager") = 0 Then
                            chkCCManager1.Checked = False
                        Else : chkCCManager1.Checked = True
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAlerts As New CAlerts
                'first row
                objAlerts.AlertDTLID = 20
                objAlerts.AlertID = 12
                objAlerts.AlertOn = IIf(chk1.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.CCManager = IIf(chkCCManager1.Checked = True, 1, 0)
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmaillAddress_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 20
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress.DataSource = dtEmail
                dgEmaillAddress.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 20
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmailAdd1.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress()
                txtEmailAdd1.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                
                With objCommon
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .Filter = Trim(txtCompName.Text) & "%"
                    ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    ddlCompany.DataTextField = "vcCompanyname"
                    ddlCompany.DataValueField = "numDivisionID"
                    ddlCompany.DataBind()
                    ddlCompany.Items.Insert(0, "--Select One--")
                    ddlCompany.Items.FindByText("--Select One--").Value = 0
                End With
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
            Try
                FillContact(ddlContact, ddlCompany.SelectedItem.Value)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Function FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = lngDivision
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
                If ddlCombo.Items.Count = 2 Then
                    ddlCombo.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgContacts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContacts.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertConID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.ManageAlertConAndCompany()
                    LoadContacts()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnConAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConAdd.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.ContactID = ddlContact.SelectedItem.Value
                objAlerts.byteMode = 0
                objAlerts.AlertDTLID = 20
                objAlerts.ManageAlertConAndCompany()
                LoadContacts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
            Try
                LoadTemplates(ddlEmailTemplate1)
                loadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
