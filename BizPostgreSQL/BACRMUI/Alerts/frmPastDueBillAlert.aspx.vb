Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

Public Class frmPastDueBillAlert
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents ddlDepartment As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlBizDocs As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
    Protected WithEvents dgDetails As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate1)
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))
                    btnClose.Attributes.Add("onclick", "return Close()")
                    btnAdd.Attributes.Add("onclick", "return Add()")
                    hpl1.Attributes.Add("onclick", "return OpenET(28)")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 0
                objAlerts.AlertID = 8
                objAlerts.AlertOn = 1
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.DaysAfterDue = txtDays.Text
                objAlerts.BizDocs = ddlBizDocs.SelectedItem.Value
                objAlerts.Department = ddlDepartment.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                bindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.EditItem Then
                    Dim ddlEmailTemp As DropDownList
                    Dim ddlEBizdocs As DropDownList
                    Dim chk As CheckBox
                    chk = e.Item.FindControl("chkOn")
                    ddlEmailTemp = e.Item.FindControl("ddlEEmailTemplate")
                    ddlEBizdocs = e.Item.FindControl("ddlEBizdocs")
                    LoadTemplates(ddlEmailTemp)
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlEBizdocs, 27, Session("DomainID"))
                    If CType(e.Item.FindControl("lblTintAlertIon"), Label).Text = 1 Then
                        chk.Checked = True
                    Else : chk.Checked = False
                    End If
                    If Not ddlEmailTemp.Items.FindByValue(CType(e.Item.FindControl("lblETempID"), Label).Text) Is Nothing Then
                        ddlEmailTemp.Items.FindByValue(CType(e.Item.FindControl("lblETempID"), Label).Text).Selected = True
                    End If
                    If Not ddlEBizdocs.Items.FindByValue(CType(e.Item.FindControl("lblBizDocId"), Label).Text) Is Nothing Then
                        ddlEBizdocs.Items.FindByValue(CType(e.Item.FindControl("lblBizDocId"), Label).Text).Selected = True
                    End If
                End If
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim chk As CheckBox
                    chk = e.Item.FindControl("chkOn")
                    If e.Item.Cells(1).Text = 1 Then
                        chk.Checked = True
                    Else : chk.Checked = False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDetails.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgDetails.EditItemIndex = e.Item.ItemIndex
                    dgDetails.EditItemIndex = -1
                    Call bindGrid()
                End If

                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertDTLID = e.Item.Cells(0).Text
                    objAlerts.DeleteAlertdtlForPastDuebills()
                    Call bindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertID = 8
                    objAlerts.AlertDTLID = CType(e.Item.FindControl("lblEAlertDtlID"), Label).Text
                    objAlerts.DaysAfterDue = CType(e.Item.FindControl("txtEDays"), TextBox).Text
                    objAlerts.BizDocs = CType(e.Item.FindControl("ddlEBizdocs"), DropDownList).SelectedItem.Value
                    objAlerts.EmailTemplateID = CType(e.Item.FindControl("ddlEEmailTemplate"), DropDownList).SelectedItem.Value
                    objAlerts.AlertOn = IIf(CType(e.Item.FindControl("chkOn"), CheckBox).Checked = True, 1, 0)
                    objAlerts.Department = ddlDepartment.SelectedItem.Value
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    dgDetails.EditItemIndex = -1
                    Call bindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDetails_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDetails.EditCommand
            Try
                dgDetails.EditItemIndex = e.Item.ItemIndex
                Call bindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub bindGrid()
            Try
                Dim objAlerts As New CAlerts
                Dim dtTable As DataTable
                objAlerts.Department = ddlDepartment.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                dtTable = objAlerts.GetAlertdtlForPastDuebills()
                dgDetails.DataSource = dtTable
                dgDetails.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
            Try
                bindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
