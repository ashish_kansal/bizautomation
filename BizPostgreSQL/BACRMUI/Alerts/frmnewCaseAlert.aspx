<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmnewCaseAlert.aspx.vb" Inherits="BACRM.UserInterface.Alerts.frmnewCaseAlert"%>
<%@OutputCache Duration="1" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Alert</title>
	<script language="javascript" type="text/javascript" >
		 function Close()
			 {
			    document.location.href='../Alerts/frmBAM.aspx';
			    return false;
			 }
		function reLoad()
		{
		    document.Form1.btnClick.click();
		}
		function OpenET(a)
		{
		    window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
		</script>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:Button ID="btnClick" runat="server" style="display:none" />
			<table width="100%">
				<tr>
					<td class="text_bold">
						New Case
					</td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" Width="50" CssClass="button" Text="Save"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk1" Runat="server"></asp:CheckBox>
						When a new case is generated through the BizPortal, send the following email 
						template to the Case Manager (owner of the Account the case is for):
						<asp:DropDownList ID="ddlEmailTemplate1" Runat="server" CssClass="signup"></asp:DropDownList>
						<asp:CheckBox ID="chkCCManager" Runat="server" Text="CC Assignee�s Manager"></asp:CheckBox>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk2" Runat="server"></asp:CheckBox>
						When a new case is assigned to someone OTHER then the person who is assigning 
						it, send the following email template to the assignee:
						<asp:DropDownList ID="ddlEmailTemplate2" Runat="server" CssClass="signup"></asp:DropDownList>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl2" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
