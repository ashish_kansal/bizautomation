<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBizDocAlert.aspx.vb"
    Inherits="BACRM.UserInterface.Alerts.frmBizDocAlert" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Alert</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            document.location.href = '../Alerts/frmBAM.aspx';
            return false;
        }
        function reLoad() {
            document.getElementById("btnReloadTemplate").click();
        }
        function OpenET(a) {
            window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID=' + a, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function Save() {

            if (document.getElementById("ddlBizDocs").value == 0) {
                alert("Select Bizdoc ")
                document.getElementById("ddlBizDocs").focus()
                return false;
            }
            if (document.getElementById("chkCreated").checked == false && document.getElementById("chkModified").checked == false && document.getElementById("chkApproved").checked == false) {
                alert("Select one action from Create or Modified or Approved")
                return false;
            }
            if (document.getElementById("ddlEmailTemplate").value == 0) {
                alert("Select Email Template ")
                document.getElementById("ddlEmailTemplate").focus()
                return false;
            }
            if (document.getElementById("chkOppOwner").checked == false && document.getElementById("chkCCManager").checked == false && document.getElementById("chkPriContact").checked == false) {
                alert("Select one email recipient")
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                        </asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    BizDoc Alerts
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal1">
                When BizDoc Named
                <asp:DropDownList ID="ddlBizDocs" runat="server" CssClass="signup">
                </asp:DropDownList>
                is :
                <asp:CheckBox ID="chkCreated" runat="server"></asp:CheckBox>
                <label for="chkCreated">
                    Created</label>
                <asp:CheckBox ID="chkModified" runat="server"></asp:CheckBox>
                <label for="chkModified">
                    Modified</label>
                <asp:CheckBox ID="chkApproved" runat="server"></asp:CheckBox>
                <label for="chkApproved">Approved</label>
            </td>
        </tr>
        <tr>
            <td class="normal1">
                send the following email template
                <asp:DropDownList ID="ddlEmailTemplate" runat="server" CssClass="signup" Width="180">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">New Email Template</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="normal1">
                <asp:CheckBox ID="chkOppOwner" runat="server"></asp:CheckBox><label for="chkOppOwner">Opportuntiy/Deal
                    Owner</label>
                <asp:CheckBox ID="chkCCManager" runat="server"></asp:CheckBox><label for="chkCCManager">his
                    or her Manager</label>
                <asp:CheckBox ID="chkPriContact" runat="server"></asp:CheckBox><label for="chkPriContact">Primary
                    Contact the Opportunity/Deal is for.</label>
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add Rule" OnClientClick="return Save();">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgDetails" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
                    >
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numBizDocAlertID" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="When" DataField="BizDoc"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Is">
                            <ItemTemplate>
                                <%# ReturnData1(DataBinder.Eval(Container.DataItem, "Is")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Send Email template" DataField="vcDocName"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="To">
                            <ItemTemplate>
                                <%# ReturnData2(DataBinder.Eval(Container.DataItem, "To")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:Button ID="btnReloadTemplate" runat="server" CssClass="button" Text="" Style="display: none;">
    </asp:Button>
</asp:Content>
