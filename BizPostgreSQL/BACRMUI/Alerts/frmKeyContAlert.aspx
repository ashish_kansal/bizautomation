<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmKeyContAlert.aspx.vb" Inherits="BACRM.UserInterface.Alerts.frmKeyContAlert"%>
<%@OutputCache Duration="1" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Alert</title>
		<script language="javascript" type="text/javascript" >
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function Add()
			{
		
			if (document.Form1.ddlCompany.value==0)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			if (document.Form1.ddlCompany.selectedIndex==-1)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			if (document.Form1.ddlContact.selectedIndex==-1)
						{
							alert("Select Contact")
							document.Form1.ddlContact.focus()
							return false;
						}
			if (document.Form1.ddlContact.value==0)
						{
							alert("Select Contact")
							document.Form1.ddlContact.focus()
							return false;
						}
			}
			function Close()
			 {
			    window.close();
			    return false;
			 }
			 function reLoad()
		        {
		            document.Form1.btnClick.click();
		        }
        function OpenET(a)
		{
		    window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
		</script>
		
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
		<asp:Button ID="btnClick" runat="server" style="display:none" />
			<table width="100%">
				<tr>
					<td class="text_bold">Key Contact Alert
					</td>
					<td align="right">
					    <asp:button id="btnSave" Width="50" Text="Save" CssClass="button" Runat="server"></asp:button>
					    <asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					 </td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td class="normal1"><asp:checkbox id="chk1" Runat="server"></asp:checkbox>When any 
						of the key contacts listed below logs into the portal, or answers a survey send
						<asp:dropdownlist id="ddlEmailTemplate1" CssClass="signup" Runat="server"></asp:dropdownlist>
						to the contact�s record owner
						<asp:checkbox id="chkCCManager1" Text="CC Assignee�s Manager" Runat="server"></asp:checkbox>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
						</td>
				</tr>
				<tr>
					<td class="normal1">Also CC the following email addresses:
					</td>
				</tr>
				<tr>
					<td><asp:textbox id="txtEmailAdd1" Width="150" CssClass="signup" Runat="server"></asp:textbox>
						&nbsp;
						<asp:button id="btnAdd1" Width="50" Text="Add" CssClass="button" Runat="server"></asp:button></td>
				</tr>
				<tr>
					<td><asp:DataGrid ID="dgEmaillAddress" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertEmailID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# Container.ItemIndex + 1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="vcEmailID" 
									DataTextField="vcEmailID" HeaderText="Email Address">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:HyperLinkColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="btnHdelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid></td>
				</tr>
				<tr>
					<td class="normal1">
						<table>
							<tr>
								<td class="normal1">Customer
								</td>
								<td>
									<asp:textbox id="txtCompName" Runat="server" cssclass="signup" width="125px"></asp:textbox>
									&nbsp;
									<asp:button id="btnGo" Text="Go" CssClass="button" Runat="server"></asp:button>
									<asp:dropdownlist id="ddlCompany" Width="200" CssClass="signup" Runat="server" AutoPostBack="True"></asp:dropdownlist>
								</td>
							</tr>
							<tr>
								<td class="normal1">Contact
								</td>
								<td><asp:dropdownlist id="ddlContact" Width="200" CssClass="signup" Runat="server"></asp:dropdownlist>
									&nbsp;
									<asp:button id="btnConAdd" Width="50" Text="Add" CssClass="button" Runat="server"></asp:button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgContacts" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertConID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# Container.ItemIndex + 1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="vcCompanyName" HeaderText="Company Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcDivisionName" HeaderText="Division"></asp:BoundColumn>
								<asp:BoundColumn DataField="Name" HeaderText="Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="Rating" HeaderText="Rating"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="Button1" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="Button2" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
