<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmWebLeadArrival.aspx.vb"
    Inherits="BACRM.UserInterface.Alerts.frmWebLeadArrival" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ OutputCache Duration="1" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Alerts</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            document.location.href = '../Alerts/frmBAM.aspx';
            return false;
        }
        function reLoad() {
            document.form1.btnClick.click();
        }
        function OpenET(a) {
            window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID=' + a, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td class="text_bold">
                        <asp:Button ID="btnClick" runat="server" Style="display: none" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="50">
                        </asp:Button>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                        </asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    New WebLead Arrival
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal1">
                <asp:CheckBox ID="chk1" runat="server"></asp:CheckBox>
                When a WebLead arrives from the company web site (LeadBox) send the following email
                template to the user that the WebLead was assigned to.
                <asp:DropDownList ID="ddlEmailTemplate1" runat="server" CssClass="signup">
                </asp:DropDownList>
                <asp:CheckBox ID="chkCCManager" runat="server" Text="CC Assignee�s Manager"></asp:CheckBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
                <br />
            </td>
        </tr>
        <tr>
            <td class="normal1">
                <asp:CheckBox ID="chkEmailLeadContact" runat="server"></asp:CheckBox>
                When web lead is created, send the following email template to web lead contact.
                <asp:DropDownList ID="ddlEmailTemplate2" runat="server" CssClass="signup">
                </asp:DropDownList>
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="normal1">
                <asp:CheckBox ID="chkECamp" runat="server"></asp:CheckBox>
                Also place this email on the following e-campaign.
                <asp:DropDownList ID="ddlECamp" runat="server" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
