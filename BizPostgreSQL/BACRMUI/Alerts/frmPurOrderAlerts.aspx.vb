Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

Public Class frmPurOrderAlerts
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtEmailAdd1 As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd1 As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress1 As System.Web.UI.WebControls.DataGrid
        Protected WithEvents ddlEmailTemplate2 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtEmail2 As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd2 As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress2 As System.Web.UI.WebControls.DataGrid
        Protected WithEvents chk1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chk2 As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate1)
                    LoadTemplates(ddlEmailTemplate2)
                    loadDetails()
                    loadEmailAddress1()
                    loadEmailAddress2()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertID = 13
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetAlertDetails
                hpl1.Attributes.Add("onclick", "return OpenET('" & "21" & "')")
                hpl2.Attributes.Add("onclick", "return OpenET('" & "22" & "')")
                If dtDetails.Rows.Count = 2 Then
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintAlertOn")) Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 0 Then
                            chk1.Checked = False
                        Else : chk1.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(1).Item("tintAlertOn")) Then
                        If dtDetails.Rows(1).Item("tintAlertOn") = 0 Then
                            chk2.Checked = False
                        Else : chk2.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(1).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAlerts As New CAlerts

                'first row
                objAlerts.AlertDTLID = 21
                objAlerts.AlertID = 13
                objAlerts.AlertOn = IIf(chk1.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'Second row
                objAlerts.AlertDTLID = 22
                objAlerts.AlertID = 13
                objAlerts.AlertOn = IIf(chk2.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate2.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress1()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 21
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress1.DataSource = dtEmail
                dgEmaillAddress1.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 21
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmailAdd1.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress1()
                txtEmailAdd1.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmaillAddress1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress1.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress1()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress2()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 22
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress2.DataSource = dtEmail
                dgEmaillAddress2.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd2.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 22
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmail2.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress2()
                txtEmail2.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmaillAddress2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress2.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress2()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
            Try
                LoadTemplates(ddlEmailTemplate1)
                LoadTemplates(ddlEmailTemplate2)
                loadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
