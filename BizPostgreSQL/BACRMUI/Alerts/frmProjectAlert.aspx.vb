Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

    Public Class frmProjectAlert
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents txtDays As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chk11 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chkCCManager11 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chk12 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtEmailAdd1 As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd1 As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress1 As System.Web.UI.WebControls.DataGrid
        Protected WithEvents ddlEmailTemplate2 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chk21 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chkCCManager21 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chk22 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtEmail2 As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd2 As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress2 As System.Web.UI.WebControls.DataGrid
        Protected WithEvents ddlEmailTemplate3 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chk31 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chkCCManager31 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chk32 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtEmail3 As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd3 As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress3 As System.Web.UI.WebControls.DataGrid


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate1)
                    LoadTemplates(ddlEmailTemplate2)
                    LoadTemplates(ddlEmailTemplate3)
                    loadDetails()
                    loadEmailAddress1()
                    loadEmailAddress2()
                    loadEmailAddress3()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertID = 9
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetAlertDetails
                hpl1.Attributes.Add("onclick", "return OpenET('" & "12" & "')")
                hpl2.Attributes.Add("onclick", "return OpenET('" & "14" & "')")
                hpl3.Attributes.Add("onclick", "return OpenET('" & "16" & "')")
                If dtDetails.Rows.Count = 6 Then
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintAlertOn")) Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 0 Then
                            chk11.Checked = False
                        Else : chk11.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(1).Item("tintAlertOn")) Then
                        If dtDetails.Rows(1).Item("tintAlertOn") = 0 Then
                            chk12.Checked = False
                        Else : chk12.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(2).Item("tintAlertOn")) Then
                        If dtDetails.Rows(2).Item("tintAlertOn") = 0 Then
                            chk21.Checked = False
                        Else : chk21.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(3).Item("tintAlertOn")) Then
                        If dtDetails.Rows(3).Item("tintAlertOn") = 0 Then
                            chk22.Checked = False
                        Else : chk22.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(4).Item("tintAlertOn")) Then
                        If dtDetails.Rows(4).Item("tintAlertOn") = 0 Then
                            chk31.Checked = False
                        Else : chk31.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(5).Item("tintAlertOn")) Then
                        If dtDetails.Rows(5).Item("tintAlertOn") = 0 Then
                            chk32.Checked = False
                        Else : chk32.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(2).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(4).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate3.Items.FindByValue(dtDetails.Rows(2).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate3.Items.FindByValue(dtDetails.Rows(2).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintCCManager")) Then
                        If dtDetails.Rows(0).Item("tintCCManager") = 0 Then
                            chkCCManager11.Checked = False
                        Else : chkCCManager11.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(2).Item("tintCCManager")) Then
                        If dtDetails.Rows(2).Item("tintCCManager") = 0 Then
                            chkCCManager21.Checked = False
                        Else : chkCCManager21.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(4).Item("tintCCManager")) Then
                        If dtDetails.Rows(4).Item("tintCCManager") = 0 Then
                            chkCCManager31.Checked = False
                        Else : chkCCManager31.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(0).Item("numDaysAfterDue")) Then txtDays.Text = dtDetails.Rows(0).Item("numDaysAfterDue")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAlerts As New CAlerts
                'first row
                objAlerts.AlertDTLID = 12
                objAlerts.AlertID = 9
                objAlerts.AlertOn = IIf(chk11.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.CCManager = IIf(chkCCManager11.Checked = True, 1, 0)
                objAlerts.DaysAfterDue = IIf(txtDays.Text = "", 0, txtDays.Text)
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'second row
                objAlerts.AlertDTLID = 13
                objAlerts.AlertID = 9
                objAlerts.AlertOn = IIf(chk12.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value

                objAlerts.DaysAfterDue = 0
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'Third row
                objAlerts.AlertDTLID = 14
                objAlerts.AlertID = 9
                objAlerts.AlertOn = IIf(chk21.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate2.SelectedItem.Value
                objAlerts.CCManager = IIf(chkCCManager21.Checked = True, 1, 0)
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'fourth row
                objAlerts.AlertDTLID = 15
                objAlerts.AlertID = 9
                objAlerts.AlertOn = IIf(chk22.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate2.SelectedItem.Value

                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'Fifth row
                objAlerts.AlertDTLID = 16
                objAlerts.AlertID = 9
                objAlerts.AlertOn = IIf(chk31.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate3.SelectedItem.Value
                objAlerts.CCManager = IIf(chkCCManager31.Checked = True, 1, 0)
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'Sixth row
                objAlerts.AlertDTLID = 17
                objAlerts.AlertID = 9
                objAlerts.AlertOn = IIf(chk32.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate3.SelectedItem.Value

                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress1()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 12
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress1.DataSource = dtEmail
                dgEmaillAddress1.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 12
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmailAdd1.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress1()
                txtEmailAdd1.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmaillAddress1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress1.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress1()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress2()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 14
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress2.DataSource = dtEmail
                dgEmaillAddress2.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgEmaillAddress2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress2.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress2()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress3()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 16
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress3.DataSource = dtEmail
                dgEmaillAddress3.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgEmaillAddress3_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress3.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress3()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd3.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 16
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmail3.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress3()
                txtEmail3.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd2.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 14
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmail2.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress2()
                txtEmail2.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click

            Try
                LoadTemplates(ddlEmailTemplate1)
                LoadTemplates(ddlEmailTemplate2)
                LoadTemplates(ddlEmailTemplate3)
                loadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace

