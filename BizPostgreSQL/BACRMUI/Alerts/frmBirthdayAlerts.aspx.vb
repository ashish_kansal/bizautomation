Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

Public Class frmBirthdayAlerts
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chkEveryYear As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chkCCManager1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chk1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtEmailAdd As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents dgEmaillAddress As System.Web.UI.WebControls.DataGrid
        Protected WithEvents txtAge As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates()
                    loadDetails()
                    loadEmailAddress()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTemplates()
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate1.DataSource = dtTable
                ddlEmailTemplate1.DataTextField = "VcDocName"
                ddlEmailTemplate1.DataValueField = "numGenericDocID"
                ddlEmailTemplate1.DataBind()
                ddlEmailTemplate1.Items.Insert(0, "--Select One--")
                ddlEmailTemplate1.Items.FindByText("--Select One--").Value = 0

                ddlEmailTemplate2.DataSource = dtTable
                ddlEmailTemplate2.DataTextField = "VcDocName"
                ddlEmailTemplate2.DataValueField = "numGenericDocID"
                ddlEmailTemplate2.DataBind()
                ddlEmailTemplate2.Items.Insert(0, "--Select One--")
                ddlEmailTemplate2.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertID = 14
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetAlertDetails
                hpl1.Attributes.Add("onclick", "return OpenET('" & "24" & "')")
                If dtDetails.Rows.Count = 2 Then
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintAlertOn")) Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 0 Then
                            chk1.Checked = False
                        Else : chk1.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("tintCCManager")) Then
                        If dtDetails.Rows(0).Item("tintCCManager") = 0 Then
                            chkCCManager1.Checked = False
                        Else : chkCCManager1.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(1).Item("tintAlertOn")) Then
                        If dtDetails.Rows(1).Item("tintAlertOn") = 0 Then
                            chkEveryYear.Checked = False
                        Else : chkEveryYear.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(1).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate2.Items.FindByValue(dtDetails.Rows(1).Item("numEmailTemplate")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(1).Item("tintCCManager")) Then
                        If dtDetails.Rows(1).Item("tintCCManager") = 0 Then
                            chkCCManager2.Checked = False
                        Else : chkCCManager2.Checked = True
                        End If
                    End If

                    If Not IsDBNull(dtDetails.Rows(0).Item("numAge")) Then txtAge.Text = dtDetails.Rows(0).Item("numAge")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAlerts As New CAlerts
                'second row
                objAlerts.AlertDTLID = 24
                objAlerts.AlertID = 14
                objAlerts.AlertOn = IIf(chkEveryYear.Checked = True, 1, 0)
                objAlerts.CCManager = IIf(chkCCManager2.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate2.SelectedItem.Value
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                'first row
                objAlerts.AlertDTLID = 23
                objAlerts.AlertID = 14
                objAlerts.AlertOn = IIf(chk1.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.CCManager = IIf(chkCCManager1.Checked = True, 1, 0)
                objAlerts.Age = txtAge.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.AlertDTLID = 23
                objAlerts.byteMode = 2 'Adding
                objAlerts.vcEmail = txtEmailAdd.Text
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()
                loadEmailAddress()
                txtEmailAdd.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmaillAddress_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmaillAddress.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.AlertEmailID = e.Item.Cells(0).Text
                    objAlerts.byteMode = 1 'Deleteing
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                    loadEmailAddress()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadEmailAddress()
            Try
                Dim objAlerts As New CAlerts
                Dim dtEmail As DataTable
                objAlerts.AlertDTLID = 23
                objAlerts.DomainID = Session("DomainId")
                dtEmail = objAlerts.GetAlertEmails
                dgEmaillAddress.DataSource = dtEmail
                dgEmaillAddress.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
            Try
                LoadTemplates()
                loadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
