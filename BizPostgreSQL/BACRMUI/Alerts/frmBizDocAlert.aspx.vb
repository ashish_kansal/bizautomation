Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts
Public Class frmBizDocAlert
        Inherits BACRMPage

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate)
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))
                    GetDetails()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub GetDetails()
            Try
                Dim objAlerts As New CAlerts
                Dim dtTable As DataTable
                objAlerts.DomainID = Session("DomainID")
                dtTable = objAlerts.BizDocAlertDTLs
                dgDetails.DataSource = dtTable
                dgDetails.DataBind()
                hpl1.Attributes.Add("onclick", "return OpenET(0)")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objAlerts As New CAlerts
                objAlerts.DomainID = Session("DomainID")
                objAlerts.BizDocs = ddlBizDocs.SelectedItem.Value
                objAlerts.EmailTemplateID = ddlEmailTemplate.SelectedItem.Value
                If chkCreated.Checked = True Then
                    objAlerts.BizDocCreated = 1
                Else : objAlerts.BizDocCreated = 0
                End If
                If chkModified.Checked = True Then
                    objAlerts.BizDocModified = 1
                Else : objAlerts.BizDocModified = 0
                End If
                objAlerts.Approved = 0
                If chkApproved.Checked = True Then
                    objAlerts.Approved = 1
                Else : objAlerts.Approved = 0
                End If
                If chkOppOwner.Checked = True Then
                    objAlerts.OppOwner = 1
                Else : objAlerts.OppOwner = 0
                End If
                If chkCCManager.Checked = True Then
                    objAlerts.CCManager = 1
                Else : objAlerts.CCManager = 0
                End If
                If chkPriContact.Checked = True Then
                    objAlerts.PriContact = 1
                Else : objAlerts.PriContact = 0
                End If
                objAlerts.ManageBizDocAlerts()
                GetDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDetails.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objAlerts As New CAlerts
                    objAlerts.BizDocAlertID = e.Item.Cells(0).Text
                    objAlerts.DeleteBizDocAlerts()
                    GetDetails()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnData1(ByVal strtData1)
            Try
                Dim strReturnData1 As String = ""
                If strtData1.Split("~")(0) = 1 Then strReturnData1 = "Created"
                If strtData1.Split("~")(1) = 1 Then
                    If strReturnData1 <> "" Then
                        strReturnData1 = strReturnData1 & "," & "Modified"
                    Else : strReturnData1 = "Modified"
                    End If
                End If
                If strtData1.Split("~")(2) = 1 Then
                    If strReturnData1 <> "" Then
                        strReturnData1 = strReturnData1 & "," & "Approved"
                    Else : strReturnData1 = "Approved"
                    End If
                End If
                Return strReturnData1
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnData2(ByVal strtData2)
            Try
                Dim strReturnData2 As String
                If strtData2.Split("~")(0) = 1 Then strReturnData2 = "Record Owner"
                If strtData2.Split("~")(1) = 1 Then
                    If strReturnData2 <> "" Then
                        strReturnData2 = strReturnData2 & "," & "Manager"
                    Else : strReturnData2 = "Manager"
                    End If
                End If
                If strtData2.Split("~")(2) = 1 Then
                    If strReturnData2 <> "" Then
                        strReturnData2 = strReturnData2 & "," & "Primary Contact"
                    Else : strReturnData2 = "Primary Contact"
                    End If
                End If
                Return strReturnData2
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnReloadTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReloadTemplate.Click
            Try
                LoadTemplates(ddlEmailTemplate)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    btnDelete = e.Item.FindControl("btnDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
