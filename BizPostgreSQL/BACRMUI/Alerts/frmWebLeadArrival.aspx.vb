Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Alerts

Public Class frmWebLeadArrival
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents chk1 As System.Web.UI.WebControls.CheckBox
        Protected WithEvents ddlEmailTemplate1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chkCCManager As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "BPA"
                    LoadTemplates(ddlEmailTemplate1)
                    LoadTemplates(ddlEmailTemplate2)
                    LoadECampaign()



                    'Load already saved details
                    loadDetails()

                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadTemplates(ByVal ddlEmailTemplate As DropDownList)
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                objCampaign.DomainID = Session("DomainID")
                objCampaign.ModuleID = 1 ' Leads/Prospects/Accounts
                dtTable = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataSource = dtTable
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub loadDetails()
            Try
                Dim dtDetails As DataTable
                Dim objAlerts As New CAlerts
                objAlerts.AlertID = 2
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetAlertDetails
                hpl1.Attributes.Add("onclick", "return OpenET('" & "4" & "')")
                If dtDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintAlertOn")) Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 0 Then
                            chk1.Checked = False
                        Else : chk1.Checked = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(0).Item("numEmailTemplate")) Then
                        If Not ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                            ddlEmailTemplate1.Items.FindByValue(dtDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtDetails.Rows(0).Item("tintCCManager")) Then
                        If dtDetails.Rows(0).Item("tintCCManager") = 0 Then
                            chkCCManager.Checked = False
                        Else : chkCCManager.Checked = True
                        End If
                    End If
                End If

                Dim dtNewRuleDetail As DataTable
                Dim drNewRule As DataRow()


                dtNewRuleDetail = objAlerts.GetAlerDetailIds()
                If dtNewRuleDetail.Rows.Count = 2 Then
                    drNewRule = dtDetails.Select("numAlertDtlID=" & dtNewRuleDetail.Rows(0).Item("numAlertDtlid"))

                    If drNewRule.Length > 0 Then
                        If Not IsDBNull(drNewRule(0).Item("tintAlertOn")) Then
                            If drNewRule(0).Item("tintAlertOn") = 0 Then
                                chkEmailLeadContact.Checked = False
                            Else : chkEmailLeadContact.Checked = True
                            End If
                        End If
                        If Not IsDBNull(drNewRule(0).Item("numEmailTemplate")) Then
                            If Not ddlEmailTemplate2.Items.FindByValue(drNewRule(0).Item("numEmailTemplate")) Is Nothing Then
                                ddlEmailTemplate2.Items.FindByValue(drNewRule(0).Item("numEmailTemplate")).Selected = True
                            End If
                        End If
                    End If


                    'Code to load Email campaign template
                    drNewRule = dtDetails.Select("numAlertDtlID=" & dtNewRuleDetail.Rows(1).Item("numAlertDtlid"))

                    If drNewRule.Length > 0 Then
                        If Not IsDBNull(drNewRule(0).Item("tintAlertOn")) Then
                            If drNewRule(0).Item("tintAlertOn") = 0 Then
                                chkECamp.Checked = False
                            Else : chkECamp.Checked = True
                            End If
                        End If
                        If Not IsDBNull(drNewRule(0).Item("numEmailCampaignId")) Then
                            If Not ddlECamp.Items.FindByValue(drNewRule(0).Item("numEmailCampaignId")) Is Nothing Then
                                ddlECamp.Items.FindByValue(drNewRule(0).Item("numEmailCampaignId")).Selected = True
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAlerts As New CAlerts

                'first row
                objAlerts.AlertDTLID = 4
                objAlerts.AlertID = 2
                objAlerts.AlertOn = IIf(chk1.Checked = True, 1, 0)
                objAlerts.EmailTemplateID = ddlEmailTemplate1.SelectedItem.Value
                objAlerts.CCManager = IIf(chkCCManager.Checked = True, 1, 0)
                objAlerts.DomainID = Session("DomainID")
                objAlerts.ManageAlerts()

                ' Inserted By : Pratik Vasani 08/Dec/2008
                Dim dtIds As DataTable
                dtIds = objAlerts.GetAlerDetailIds()
                If dtIds.Rows.Count = 2 Then


                    objAlerts.AlertDTLID = dtIds.Rows(0).Item("numAlertDtlId")
                    objAlerts.AlertID = 2
                    objAlerts.AlertOn = IIf(chkEmailLeadContact.Checked = True, 1, 0)
                    objAlerts.EmailTemplateID = IIf(objAlerts.AlertOn = 1, ddlEmailTemplate2.SelectedItem.Value, 0)
                    objAlerts.CCManager = 0
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()

                    objAlerts.AlertDTLID = dtIds.Rows(1).Item("numAlertDtlId")
                    objAlerts.AlertID = 2
                    objAlerts.AlertOn = IIf(chkECamp.Checked = True, 1, 0)
                    objAlerts.EmailCampaignId = IIf(objAlerts.AlertOn = 1, ddlECamp.SelectedItem.Value, 0)
                    objAlerts.CCManager = 0
                    objAlerts.DomainID = Session("DomainID")
                    objAlerts.ManageAlerts()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
            Try
                LoadTemplates(ddlEmailTemplate1)
                loadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub LoadECampaign()
            Try
                Dim objCampaign As New Campaign
                objCampaign.DomainID = Session("DomainID")
                Dim dtTable As DataTable
                dtTable = objCampaign.ECampaignTemplates()
                ddlECamp.DataSource = dtTable
                ddlECamp.DataTextField = "vcECampName"
                ddlECamp.DataValueField = "numECampaignId"
                ddlECamp.DataBind()
                ddlECamp.Items.Insert(0, "--Select One--")
                ddlECamp.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
    End Class
End Namespace
