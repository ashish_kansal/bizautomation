<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBAM.aspx.vb"
    Inherits="BACRM.UserInterface.Alerts.frmBAM" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>e-Mail Alerts</title>
    <script language="javascript" type="text/javascript">

        function Close() {
            window.location.href = "../admin/frmAdminSection.aspx";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    E-Mail Alerts
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table1" CellPadding="0" CellSpacing="0"  runat="server"
        Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgEmailAlerts" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                    >
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="vcPage" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="intPageHeight" Visible="False"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Alert Name">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" NavigateUrl="#" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcAlert") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcDesc" HeaderText="Description"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
