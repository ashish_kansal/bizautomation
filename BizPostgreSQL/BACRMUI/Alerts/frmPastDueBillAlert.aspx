<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmPastDueBillAlert.aspx.vb" Inherits="BACRM.UserInterface.Alerts.frmPastDueBillAlert"%>
<%@OutputCache Duration="1" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Alert</title>
	<script language="javascript" type="text/javascript" >
		 function Close()
			 {
			    document.location.href='../Alerts/frmBAM.aspx';
			    return false;
			 }
		function reLoad()
		{
		    document.Form1.btnClick.click();
		}
		function OpenET(a)
		{
		    window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
	function Add()
	{
		if (document.Form1.ddlDepartment.value==0)
		{
			alert("Select Department")
			document.Form1.ddlDepartment.focus()
			return false;
		}
		if (document.Form1.txtDays.value=="")
		{
			alert("Enter No of Days")
			document.Form1.txtDays.focus()
			return false;
		}
		if (document.Form1.ddlBizDocs.value==0)
		{
			alert("Select BizDoc")
			document.Form1.ddlBizDocs.focus()
			return false;
		}
		if (document.Form1.ddlEmailTemplate1.value==0)
		{
			alert("Select Email Template")
			document.Form1.ddlEmailTemplate1.focus()
			return false;
		}
	}
	</script>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:Button ID="btnClick" runat="server" style="display:none" />
			<table width="100%">
				<tr>
					<td class="text_bold">Past Due Bills &amp; Invoices
					</td>
					<td align="right">
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td class="normal1">Send email reminders to contacts at organizations belonging to 
						the following departments
						<asp:dropdownlist id="ddlDepartment" CssClass="signup" Runat="server" AutoPostBack="True"></asp:dropdownlist>when 
						due dates for selected BizDocs are past due as specified below:
					</td>
				</tr>
				<tr>
					<td class="normal1">
						
						<asp:textbox id="txtDays" CssClass="signup" Width="30" Runat="server"></asp:textbox>
						days after due date for BizDocs named
						<asp:dropdownlist id="ddlBizDocs" CssClass="signup" Width="130" Runat="server"></asp:dropdownlist>
						send the following email template to
						<asp:dropdownlist id="ddlEmailTemplate1" CssClass="signup" Width="130" Runat="server"></asp:dropdownlist>
						&nbsp;
						<asp:button id="btnAdd" Text="Add" CssClass="button" Width="50" Runat="server"></asp:button>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
						
						</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgDetails" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							ShowHeader="false">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertDTLid" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="tintAlertOn" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
										<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:CheckBox ID="chkOn" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Label ID="lblDays" Runat="server" CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.numDaysAfterDue") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
									<asp:Label ID="lblEAlertDtlID" Runat="server" Visible=false CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.numAlertDTLid") %>'>
										</asp:Label>
										<asp:Label ID="lblTintAlertIon" Runat="server" Visible=false CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.tintAlertOn") %>'>
										</asp:Label>
										<asp:TextBox ID="txtEDays" Runat="server" Width="30" CssClass="signup" Text= '<%# DataBinder.Eval(Container,"DataItem.numDaysAfterDue") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										days after due date for BizDocs named
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Label ID="lblBizDocs" Runat="server" CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.BizDoc") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label ID="lblBizDocId" Runat="server" Visible=false CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.numBizDocs") %>'>
										</asp:Label>
										<asp:DropDownList ID="ddlEBizdocs" Runat="server" CssClass="signup"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										send the following email template to
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Label ID="lblEmailTemplate" Runat="server" CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.EmailTemplate") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label ID="lblETempID" Runat="server" Visible=false CssClass="text" Text= '<%# DataBinder.Eval(Container,"DataItem.numEmailTemplate") %>'>
										</asp:Label>
										<asp:DropDownList ID="ddlEEmailTemplate" Runat="server" CssClass="signup"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="Button1" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="Button2" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
