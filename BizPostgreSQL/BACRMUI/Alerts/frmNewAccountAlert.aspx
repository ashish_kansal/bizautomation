<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmNewAccountAlert.aspx.vb" Inherits="BACRM.UserInterface.Alerts.frmNewAccountAlert"%>
<%@OutputCache Duration="1" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Alert</title>
		<script language="javascript" type="text/javascript" >
		 function Close()
			 {
			    document.location.href='../Alerts/frmBAM.aspx';
			    return false;
			 }
		function reLoad()
		{
		    document.Form1.btnClick.click();
		}
		function OpenET(a)
		{
		    window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
		</script>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:Button ID="btnClick" runat="server" style="display:none" />
			<table width="100%">
				<tr>
					<td class="text_bold">
						New Account generated from E-Commerce
					</td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save" Width="50"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk1" Runat="server"></asp:CheckBox>
						When a customer purchases something from the E-Commerce site for the first time 
						(and is not already in BizAutomation) send the following email template to the 
						user the customer record is assigned to:
						<asp:DropDownList ID="ddlEmailTemplate1" Runat="server" CssClass="signup"></asp:DropDownList>
						
						
						<br />
						<asp:CheckBox ID="chkCCManager" Runat="server" Text="CC Assignee�s Manager"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
