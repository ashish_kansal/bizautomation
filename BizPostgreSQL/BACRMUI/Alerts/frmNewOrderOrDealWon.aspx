<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmNewOrderOrDealWon.aspx.vb" Inherits="BACRM.UserInterface.Alerts.frmNewOrderOrDealWon"%>
<%@OutputCache Duration="1" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>frmNewOrderOrDealWon</title>
			<script language="javascript" type="text/javascript" >
		 function Close()
			 {
			    document.location.href='../Alerts/frmBAM.aspx';
			    return false;
			 }
		function reLoad()
		{
		    document.Form1.btnClick.click();
		}
		function OpenET(a)
		{
		    window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
		</script>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:Button ID="btnClick" runat="server" style="display:none" />
			<table width="100%">
				<tr>
					<td class="text_bold">
						New Order / Deal Won
					</td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save" Width="50"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%">
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk1" Runat="server"></asp:CheckBox>
						When an new Deal is Won from an internal user, send the following email 
						template to the user�s manager:
						<asp:DropDownList ID="ddlEmailTemplate1" Runat="server" CssClass="signup"></asp:DropDownList>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl1" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
				<tr>
					<td>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						Also CC the following email addresses:
					</td>
				</tr>
				<tr>
					<td>
						<asp:TextBox ID="txtEmailAdd" Runat="server" Width="150" CssClass="signup"></asp:TextBox>
						<asp:Button ID="btnAdd" Runat="server" CssClass="button" Text="Add" Width="50"></asp:Button>
					</td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgEmaillAddress" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAlertEmailID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# Container.ItemIndex + 1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="vcEmailID" 
									DataTextField="vcEmailID" HeaderText="Email Address">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:HyperLinkColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="btnHdelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td class="normal1">
						<asp:CheckBox ID="chk2" Runat="server"></asp:CheckBox>
						When an Order is placed by an existing customer using the Self-Service 
						BizPortal (Order entry), send the owner of that Account the following email 
						template:
						<asp:DropDownList ID="ddlEmailTemplate2" Runat="server" CssClass="signup"></asp:DropDownList>
						<asp:checkbox id="chkCCManager1" Text="CC Assignee�s Manager" Runat="server"></asp:checkbox>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:HyperLink ID="hpl2" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
