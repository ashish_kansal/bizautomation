<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmExtranetUsers.aspx.vb"
    Inherits="BACRM.UserInterface.Extranet.frmExtranetUsers" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Extranet Users</title>
    <script language="javascript" type="text/javascript">

        function Save() {
            if (document.getElementById("ddlGroup").value == "0") {
                alert("Please select Customer & Vendor Portal Access Group");
                return false;
            }
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" OnClientClick="return Save();">
                        </asp:Button>
                        <asp:Button ID="btnSaveClose" Text="Save &amp; Close" runat="server" CssClass="button">
                        </asp:Button>
                        <asp:Button ID="btnClose" Text="Close" runat="server" CssClass="button"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
        <table width="100%" align="center">
            <tr>
                <td class="normal4" align="center">
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    External Access Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="1"
        BorderColor="black" CssClass="aspTable" Height="350" Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table align="center" width="100%">
                    <tr>
                        <td class="normal1" align="right">
                            Company :
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCompany" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Customer & Vendor Portal Access Group
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="signup" Width="150">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--<tr>
								<td class="normal1" align="right">Partner Portal Access Group
								</td>
								<td>
									<asp:DropDownList ID="ddlPartnerGroup" Runat="server" CssClass="signup" Width="150"></asp:DropDownList>
								</td>
							</tr>--%>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSendEmal" runat="server" CssClass="button" Text="Send portal access details to selected users">
                            </asp:Button>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnCheckall" runat="server" CssClass="button" Text="&darr; Check all & generate Passwords">
                            </asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dgExternalUser" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll','chk')" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" CssClass="chk" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="numContactid" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PortalAccess" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PartnerAccess" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Password" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numExtranetDtlID" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="No">
                                        <ItemTemplate>
                                            <%# Container.ItemIndex +1 %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ContactName" HeaderText="Contact Name"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcemail" HeaderText="Contact Email ID"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Password">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPassword" runat="server" CssClass="signup" Text="" TextMode="Password">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Enable Portal / E-Commerce Access" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPortalAccess" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:TemplateColumn HeaderText="Partner Portal Access">
												<ItemTemplate>
													<asp:CheckBox ID="chkPartnerAccess" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>--%>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
