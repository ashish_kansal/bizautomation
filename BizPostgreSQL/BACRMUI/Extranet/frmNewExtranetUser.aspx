﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewExtranetUser.aspx.vb"
    Inherits=".frmNewExtranetUser" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script language="javascript" type="text/javascript">
        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Organization")
                return false;
            }
            if (document.getElementById('ddlContactId').value == "0") {
                alert("Select contact")
                document.getElementById('ddlContactId').focus()
                return false;
            }
        }
        function Close() {
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="sm1" />
    <div class="right-input">
        <div class="input-part">
            <table cellspacing="0" cellpadding="0" width="750px" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button" OnClientClick="javascript:return Save();"></asp:Button>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50" OnClientClick="Close();"></asp:Button>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal4" align="center">
                                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Extranet User
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="150"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table width="100%" border="0">
                    <tr>
                        <td class="text" align="right" width="80">Organization
                        </td>
                        <td>
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True"
                                AutoPostBack="true">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">Contact
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContactId" runat="server" CssClass="signup" Width="180">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
