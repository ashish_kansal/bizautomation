Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Documents

Namespace BACRM.UserInterface.Extranet

    Public Class frmExtranetUsers
        Inherits BACRMPage
        Dim objUserGroups As UserGroups


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim dtGroups As DataTable
                    Dim objUserAccess As New UserAccess
                    objUserAccess.SelectedGroupTypes = "2"
                    objUserAccess.DomainID = Session("DomainID")
                    dtGroups = objUserAccess.GetGroups
                    ddlGroup.DataSource = dtGroups
                    ddlGroup.DataTextField = "vcGroupName"
                    ddlGroup.DataValueField = "numGroupId"
                    ddlGroup.DataBind()
                    ddlGroup.Items.Insert(0, "--Select One--")
                    ddlGroup.Items.FindByText("--Select One--").Value = 0
                    'We no longer use the partner portal concept, So removing its code- by chintan
                    'objUserAccess.GroupType = 3
                    'dtGroups = objUserAccess.GetGroups
                    'ddlPartnerGroup.DataSource = dtGroups
                    'ddlPartnerGroup.DataTextField = "vcGroupName"
                    'ddlPartnerGroup.DataValueField = "numGroupId"
                    'ddlPartnerGroup.DataBind()
                    'ddlPartnerGroup.Items.Insert(0, "--Select One--")
                    'ddlPartnerGroup.Items.FindByText("--Select One--").Value = 0
                    BindInformation()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindInformation()
            Try
                Dim dtDetails As DataTable
                Dim ds As DataSet
                If objUserGroups Is Nothing Then objUserGroups = New UserGroups
                objUserGroups.ExtranetID = GetQueryStringVal( "Extranet")
                objUserGroups.DomainId = Session("DomainID")
                ds = objUserGroups.GetExtranetDetails
                dtDetails = ds.Tables(0)
                If dtDetails.Rows.Count > 0 Then
                    'ViewState("RemPartNerLic") = dtDetails.Rows(0).Item("NoRemPartLic")
                    lblCompany.Text = dtDetails.Rows(0).Item("Company")
                    If Not IsDBNull(dtDetails.Rows(0).Item("numGroupid")) Then
                        If Not ddlGroup.Items.FindByValue(dtDetails.Rows(0).Item("numGroupid")) Is Nothing Then
                            ddlGroup.Items.FindByValue(dtDetails.Rows(0).Item("numGroupid")).Selected = True
                        End If
                    End If
                    'If Not IsDBNull(dtDetails.Rows(0).Item("numPartnerGroupID")) Then
                    '    If Not ddlPartnerGroup.Items.FindByValue(dtDetails.Rows(0).Item("numPartnerGroupID")) Is Nothing Then
                    '        ddlPartnerGroup.Items.FindByValue(dtDetails.Rows(0).Item("numPartnerGroupID")).Selected = True
                    '    End If
                    'End If
                    dgExternalUser.DataSource = ds.Tables(1)
                    dgExternalUser.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                If Save() = True Then Response.Write("<script>self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function Save()
            Try
                If objCommon Is Nothing Then
                    objCommon = New CCommon
                End If
                Dim i As Integer
                Dim dr As DataRow
                Dim dtEmailStatus As New DataTable
                Dim dgItem As DataGridItem
                dtEmailStatus.Columns.Add("ContactID")
                dtEmailStatus.Columns.Add("Portal")
                dtEmailStatus.Columns.Add("Password")
                dtEmailStatus.Columns.Add("ExtranetDTLID")
                dtEmailStatus.TableName = "Table"
                For Each dgItem In dgExternalUser.Items
                    dr = dtEmailStatus.NewRow
                    dr("ContactID") = dgItem.Cells(1).Text
                    dr("Portal") = IIf(CType(dgItem.FindControl("chkPortalAccess"), CheckBox).Checked = True, 1, 0)
                    dr("Password") = objCommon.Encrypt(CType(dgItem.FindControl("txtPassword"), TextBox).Text.Trim)
                    dr("ExtranetDTLID") = dgItem.Cells(5).Text
                    dtEmailStatus.Rows.Add(dr)
                Next

                If objUserGroups Is Nothing Then objUserGroups = New UserGroups
                Dim ds As New DataSet
                ds.Tables.Add(dtEmailStatus)
                objUserGroups.strEmail = ds.GetXml()
                ds.Tables.Remove(dtEmailStatus)

                objUserGroups.ExtranetID = GetQueryStringVal("Extranet")
                objUserGroups.GroupId = ddlGroup.SelectedItem.Value
                objUserGroups.DomainID = Session("DomainID")
                objUserGroups.ManageExtranet()
                BindInformation()
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgExternalUser_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgExternalUser.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim chk As CheckBox
                    chk = e.Item.FindControl("chkPortalAccess")
                    If e.Item.Cells(2).Text = 0 Then
                        chk.Checked = False
                    Else : chk.Checked = True
                    End If
                    Dim txtPassword As TextBox = CType(e.Item.FindControl("txtPassword"), TextBox)
                    If e.Item.Cells(4).Text.Trim.Replace("&nbsp;", "").Length > 0 Then
                        txtPassword.Attributes.Add("value", e.Item.Cells(4).Text.Trim.Replace("&nbsp;", ""))
                    Else
                        txtPassword.Text = ""
                    End If
                    
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSendEmal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendEmal.Click
            Try
                Dim strRecordIds As String
                Dim dtContactPassword As New DataTable
                dtContactPassword.Columns.Add("numContactId")
                dtContactPassword.Columns.Add("PortalPassword")
                Dim dr As DataRow
                For Each dgGridItem As DataGridItem In dgExternalUser.Items
                    If CType(dgGridItem.FindControl("chk"), CheckBox).Checked = True Then
                        strRecordIds = strRecordIds + CCommon.ToLong(dgGridItem.Cells(1).Text).ToString + ","
                        dr = dtContactPassword.NewRow
                        dr("numContactId") = CCommon.ToLong(dgGridItem.Cells(1).Text).ToString
                        dr("PortalPassword") = CType(dgGridItem.FindControl("txtPassword"), TextBox).Text.Trim
                        dtContactPassword.Rows.Add(dr)
                    End If
                Next

                Dim objSendEmail As New Email

                objSendEmail.DomainID = Session("DomainID")
                objSendEmail.TemplateCode = "#SYS#PORTAL_NEW_USER"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = strRecordIds
                objSendEmail.AdditionalMergeFields.Add("PortalURL", Session("PortalURL"))
                objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
                objSendEmail.AdditionalMergeFields.Add("PortalPassword", "") 'will be assigned through datatable dtContactPassword
                objSendEmail.FromEmail = Session("UserEmail")
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                objSendEmail.SendEmailTemplate(dtContactPassword)

                litMessage.Text = "Emails  are Sent"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCheckall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckall.Click
            Try
                Dim i As Integer
                Dim dgGridItem As DataGridItem
                Dim RanNum As New Random
                Dim strRanNO As String = 0
                For Each dgGridItem In dgExternalUser.Items
                    If dgGridItem.Cells(2).Text = 0 Then
                        strRanNO = PasswordGenerator.Generate(10, 10)
                        CType(dgGridItem.FindControl("chkPortalAccess"), CheckBox).Checked = True
                        CType(dgGridItem.FindControl("txtPassword"), TextBox).Attributes.Add("value", strRanNO)
                    End If
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
