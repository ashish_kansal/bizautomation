﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Public Class frmNewExtranetUser
    Inherits BACRMPage
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If radCmbCompany.SelectedValue <> "" Then
                FillContact(ddlContactId, CInt(radCmbCompany.SelectedValue))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = lngDivision
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim RanNum As New Random
            Dim strRanNO As String
            strRanNO = RanNum.Next(10000000, 99999999)

            Dim objUser As New UserAccess
            objUser.DivisionID = radCmbCompany.SelectedValue
            objUser.CompanyID = 0
            objUser.ContactID = ddlContactId.SelectedValue
            If objCommon Is Nothing Then
                objCommon = New CCommon()
            End If
            objUser.Password = objCommon.Encrypt(strRanNO)
            objUser.DomainID = Session("DomainID")
            If objUser.CreateExtAccessFromPortal() Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "close", "window.opener.location.reload(true);Close();", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class