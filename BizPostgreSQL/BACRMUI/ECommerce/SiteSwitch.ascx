﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SiteSwitch.ascx.vb"
    Inherits=".SiteSwitch" %>
<script type="text/javascript">
    function publish() {
        if (document.getElementById('<%= ddlSites.ClientID %>').value == "0") {
            alert('Please select a site.');
            return false;
        }
        return true;
    }
</script>

<asp:UpdatePanel ID="updatedwitch" runat="server" class="row padbottom10">
    <ContentTemplate>
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="form-inline">
                     <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                     <asp:ImageButton ImageUrl="~/images/publish.png" runat="server" ID="ibtnPublish" Style="vertical-align: middle;margin-left: 5px;" ToolTip="Publish All Pages" OnClientClick="return publish();" />
                </div>
            </div>
        </div>
    </ContentTemplate>

    <Triggers>
        <asp:PostBackTrigger ControlID="ddlSites" />
        <asp:PostBackTrigger ControlID="ibtnPublish" />
    </Triggers>
</asp:UpdatePanel>
