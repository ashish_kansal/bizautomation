﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPromotionOfferItems.aspx.vb"
    Inherits=".frmPromotionOfferItems" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content7" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnClose" CssClass="button" runat="server" Text="Close" OnClientClick="window.close();">
                </asp:Button>
            </td>
        </tr>
    </table>
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label runat="server" ID="lblTitle" Text="Promotion & Offer Items"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Table ID="Table3" Width="600px" runat="server" BorderWidth="1" Height="350"
        GridLines="None" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:Panel runat="server" ID="pnlItems" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>
                                Select Item
                                <telerik:RadComboBox ID="radItem" runat="server" Width="150" DropDownWidth="200px"
                                    EnableLoadOnDemand="true">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                </telerik:RadComboBox>
                                &nbsp;
                                <asp:Button ID="btnAddItem" CssClass="button" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnRemItem" CssClass="button" runat="server" Text="Remove" OnClientClick="return DeleteRecord();">
                                </asp:Button>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItems" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProItemId"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcItemName" HeaderText="Item Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="txtItemDesc" HeaderText="Description"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','check1')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="check1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlItemCategoryification" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>
                                Select Item Category
                                <asp:DropDownList ID="ddlItemCategory" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddItemCategory" CssClass="button" runat="server" Text="Add">
                                </asp:Button>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnRemoveItemCategory" CssClass="button" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <%--<asp:Button ID="btnCloseIC" CssClass="button" runat="server" Text="Close" OnClientClick="window.close();">
                                </asp:Button>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItemCategory" AllowSorting="True" runat="server" Width="100%"
                                    CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProItemId"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcCategoryName" HeaderText="Item Category"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','check2')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="check2" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlItemClassification" runat="server" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>
                                <asp:Label runat="server" ID="lblName"></asp:Label>
                                <asp:DropDownList ID="ddlItemClassification" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddItemClassification" CssClass="button" runat="server" Text="Add">
                                </asp:Button>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnRemoveItemClassification" CssClass="button" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <%-- <asp:Button ID="Button3" CssClass="button" runat="server" Text="Close" OnClientClick="window.close();">
                                </asp:Button>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItemClassification" AllowSorting="True" runat="server" Width="100%"
                                    CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProItemId"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcData" HeaderText="Item Classification"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','check2')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="check2" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
