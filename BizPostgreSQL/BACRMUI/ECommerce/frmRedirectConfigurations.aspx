﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRedirectConfigurations.aspx.vb"
    Inherits=".frmRedirectConfigurations" MasterPageFile="~/common/ECommerceMenuMaster.Master"
    ClientIDMode="Static" EnableViewState="true" %>

<%@ Register   Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Redirect Configurations</title>
    <style type="text/css">
        .erText {
            background-color: red !important;
            background: none !important;
            border: 0 !important;
            border-left: solid 1px #757c96 !important;
            border-right: solid 1px #f1f3f8 !important;
            border-bottom: solid 1px #f1f3f8 !important;
            font-size: 8pt !important;
            color: Black !important;
            height: 15pt !important;
        }

        .displayNone {
            display: none;
        }
    </style>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            if ($("#ddlRedirectType").val() > 0) {
                if ($("#ddlRedirectType").val() == 1) {
                    $(".StaticUrl").show();
                    $(".DynamicUrl").hide();
                    $("#txtNewURL").text = '';
                }
                else if ($("#ddlRedirectType").val() == 2) {
                    $(".DynamicUrl").show();
                    $(".rProdCategory").hide();
                    $(".StaticUrl").hide();
                }
            }
            else {
                $(".StaticUrl").hide();
                $(".DynamicUrl").hide();
            }
            if ($("#ddlReferenceType").val() > 0) {
                if ($("#ddlReferenceType").val() == 2) {
                    $(".rProdCategory").show();
                }
                else {
                    $(".rProdCategory").hide();
                }
            }
            else {
                $(".rProdCategory").hide();
            }

            //$(".StaticUrl").hide();
            //$(".DynamicUrl").hide();

            $("#ddlRedirectType").on("change", function () {
                if ($(this).val() != 0) {
                    if ($(this).val() == 1) {
                        $(".StaticUrl").show();
                        $(".DynamicUrl").hide();
                        $("#txtNewURL").val('');
                    }
                    else if ($(this).val() == 2) {
                        $(".DynamicUrl").show();
                        $(".StaticUrl").hide();
                        $("#txtNewURL").val('');
                    }
                }
                else {
                    $(".StaticUrl").hide();
                    $(".DynamicUrl").hide();
                }
            });

            $("#ddlReferenceType").on("change", function () {
                if ($(this).val() != 0) {
                    if ($(this).val() == 2) {
                        $(".rProdCategory").show();
                    }
                    else {
                        $(".rProdCategory").hide();
                    }
                }
                else {
                    $(".StaticUrl").hide();
                    $(".DynamicUrl").hide();
                }
            });

            //Export Checkbox
            var chkBox = $("#ctl00_GridPlaceHolder_gvRedirects_ctl01_chkSelectAll");
            $(chkBox).click(function () {
                $("#ctl00_GridPlaceHolder_gvRedirects INPUT[type='checkbox']").each(function () {
                    if (this.id.indexOf("chkSelect") >= 0) {
                        //                        console.log($(this))
                        $(this).prop('checked', chkBox.is(':checked'))
                    }
                })
            });




        });

        function DeleteRecord_Old() {
            var str;
            str = 'Are you sure, you want to delete the selected record?'
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }

        function DeleteRecord() {

            var str;
            str = 'Are you sure, you want to delete the selected record?'
            if (confirm(str)) {
                var RedirectConfigId = '';
                $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvRedirects INPUT[type='checkbox']").each(function () {
                    if (this.checked) {

                        if (this.id.indexOf("chkSelectAll") == -1) {
                            console.log('this.id : ' + this.id);

                            var lblItem = this.id.replace('chkSelect', 'lblRedirectConfigID');
                            RedirectConfigId = RedirectConfigId + ',' + document.getElementById(lblItem).innerHTML;
                            alert('RedirectConfigId : ' + RedirectConfigId);
                        }
                    }
                });

                if (RedirectConfigId == '') {
                    alert("Please select atleast one row to delete.");
                    return false;
                }
                else {
                    document.getElementById('txtDelRedirectConfigIds').value = RedirectConfigId;
                    return true;
                }
            }
            else {
                return false;
            }
        }

        function ValidateSave() {
            if (document.getElementById("txtOldURL").value == "") {
                alert("Enter Old URL");
                document.getElementById("txtOldURL").focus();
                return false;
            }
            if ($("#ddlRedirectType").val() > 0) {
                if ($("#ddlRedirectType").val() == 1) {
                    if (document.getElementById("txtNewURL").value == "") {
                        alert("Enter New URL");
                        document.getElementById("txtNewURL").focus();
                        return false;
                    }
                }
                else if ($("#ddlRedirectType").val() == 2) {
                    if ($("#ddlReferenceType").val() == 0) {
                        alert("Select Reference Type");
                        $("#ddlReferenceType").focus();
                        return false;
                    }

                    else if ($("#ddlReferenceType").val() == 2) {
                        if ($("#ddlProductCategory").val() == 0) {
                            alert("Select Product Category");
                            $("#ddlProductCategory").focus();
                            return false;
                        }
                        else if ($("#ddlReferenceItem").val() == 0) {
                            alert("Select a Product");
                            $("#ddlReferenceItem").focus();
                            return false;
                        }

                    }
                    else if ($("#ddlReferenceItem").val() == 0) {
                        alert("Select a Reference Item");
                        $("#ddlReferenceItem").focus();
                        return false;
                    }
                }
            }
            else {
                alert("Select Redirect Type");
                $("#ddlRedirectType").focus();
                return false;
            }
            return true;
        }
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <%--<div class="right-input">
        <div class="input-part">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr>
                    <td align="right">
                        <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
                        &nbsp;
                                     
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="center">
                <asp:Label ID="lblErrMessage" Visible="true" Style="color: Crimson;" runat="server" EnableViewState="False"></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Redirect Configurations&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmRedirectConfigurations.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
       <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
           ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <div class="row">
        <div class="col-md-8">
            <div class="col-md-3">
                <label>Select Site :</label>
            </div>
            <div class="col-md-9 form-group">
                <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-8">
        <table width="100%">
        <tr>
            <td colspan="2">

                <asp:Panel runat="server" ID="pnlDetail">
                    <table width="100%" class="table table-responsive">
                        <tr>
                            <td style="width: 100px" align="right">
                                <asp:Label runat="server" ID="lblOldUrl" Text="Old URL "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtOldURL" CssClass="form-control"></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                <asp:Label runat="server" ID="lblRedirectType" Text="Redirect Type "></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ClientIDMode="Static" runat="server" CssClass="signup form-control" ID="ddlRedirectType">
                                    <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Manual Link" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Dynamic Link" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr class="StaticUrl">
                            <td style="width: 100px" align="right">

                                <asp:Label runat="server" ID="lblNewUrl" Text="New URL "></asp:Label></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtNewURL" CssClass="form-control"></asp:TextBox>

                            </td>

                        </tr>
                        <%-- <tr><td colspan="2">   <asp:Label runat="server" ID="lblNewURLstring"></asp:Label></td></tr>--%>
                        <tr class="DynamicUrl">
                            <td colspan="2">
                                <table width="100%">

                                    <tr>
                                        <td style="width: 100px" align="right">
                                            <asp:Label runat="server" ID="lblReferenceType" Text="Reference Type "></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ClientIDMode="Static" AutoPostBack="true" runat="server" CssClass="signup form-control" ID="ddlReferenceType">
                                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Categories" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Produts" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Pages" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="rProdCategory">
                                        <td style="width: 100px" align="right">
                                            <asp:Label runat="server" ID="lblProductCategory" Text="Product Category"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ClientIDMode="Static" runat="server" CssClass="signup form-control" ID="ddlProductCategory" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="width: 100px" align="right">
                                            <asp:Label runat="server" ID="lblReferenceItem" Text="Reference Item "></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ClientIDMode="Static"  runat="server" CssClass="signup form-control" ID="ddlReferenceItem" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>

                        <tr align="left">
                            <td></td>
                            <td>
                                <asp:Button runat="server" ClientIDMode="Static" ID="btnSave" Text="Save" CssClass="button btn btn-primary" OnClientClick="return ValidateSave();" />
                                <asp:Button runat="server" ID="btnSaveClose" Visible="false" Text="Save & Close" CssClass="button btn btn-primary" />
                            </td>

                        </tr>
                        <tr align="right">
                            <td colspan="2">

                                <asp:Button runat="server" ID="btnDelete" Text="X" CssClass="Delete  btn btn-danger" ToolTip="Remove selected rows" OnClientClick="return DeleteRecord();" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" colspan="2" style="padding: 10px 0 10px 0;">
                                <asp:GridView ID="gvRedirects" runat="server" BorderWidth="0" CssClass="table table-striped table-bordered"
                                    AutoGenerateColumns="False" Style="margin-right: 0px" Width="100%" ClientIDMode="AutoID" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                                    <RowStyle CssClass="is" Height="40px" />
                                    <HeaderStyle CssClass="hs" Height="40px" VerticalAlign="Middle" />
                                    <FooterStyle CssClass="hs" Height="40px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                                <asp:Label ID="lblRedirectConfigID" runat="server" Style="display: none" Text='<%# Eval("numRedirectConfigID")%>'></asp:Label>
                                                <asp:Label ID="lblSiteId" runat="server" Style="display: none" Text='<%# Eval("numSiteId")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Old URL">
                                            <HeaderTemplate>
                                                <table width="100%">
                                                    <tr align="center">
                                                        <td>Old URL</td>
                                                    </tr>
                                                    <tr>
                                                        <td>example : 
                                                            <asp:Label runat="server" ID="oldUrlTemplate" Text="http://www.domain.com/Oldurl.aspx"></asp:Label></td>
                                                    </tr>
                                                </table>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox runat="server" ID="txtOldURL1" Width="350" Text='<%# Eval("vcOldUrl")%>'></asp:TextBox>--%>
                                                <asp:Label ID="txtOldURL1" BackColor="Transparent" runat="server" ClientIDMode="Static" Text='<%# Eval("vcOldUrl")%>'></asp:Label>

                                                <%-- <asp:Button runat="server" ID="btnOldUrlAdd" Text="Add" CssClass="button" />
                                <asp:Button runat="server" ID="btnOldUrlCancel" Text="Cancel" CssClass="button" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Redirect Type" HeaderStyle-CssClass="displayNone" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRedirectType1" runat="server" ClientIDMode="Static" Style="display: none" Text='<%# Eval("numRedirectType")%>'></asp:Label>

                                                <asp:DropDownList ID="ddlRedirectType1" Style="display: none" ClientIDMode="Static" Width="207" runat="server" CssClass="signup">
                                                    <asp:ListItem Text="Manual Link" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Dynamic Link" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle VerticalAlign="Bottom" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="New URL">
                                            <HeaderTemplate>
                                                <table width="100%" style="border: 0px !important; margin: 0px !important; padding: 0px !important;">
                                                    <tr align="center">
                                                        <td>New URL</td>
                                                    </tr>
                                                    <tr>
                                                        <td>example : 
                                                            <asp:Label runat="server" ID="NewUrlTemplate" Text="http://www.domain.com/Newurl.aspx"></asp:Label></td>
                                                    </tr>
                                                </table>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="txtNewURL1" BackColor="Transparent" runat="server" ClientIDMode="Static" Text='<%# Eval("vcNewUrl")%>'></asp:Label>
                                                <%-- <asp:TextBox runat="server" ID="txtNewURL1" Width="350" Text='<%# Eval("vcNewUrl")%>'></asp:TextBox>--%>
                                                <%--<tr><td>   <asp:Button runat="server" ID="btnNewUrlAdd" Text="Add" CssClass="button" />
                                <asp:Button runat="server" ID="btnNewUrlCancel" Text="Cancel" CssClass="button" /></td></tr>--%>
                                                <%--<table>
                                    <tr>
                                        <td>
                                         
                                        </td>
                                    </tr>
                                  
                                </table>
                                                --%>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- <asp:TemplateField HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteRow">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    </Columns>
                                    <FooterStyle HorizontalAlign="Right" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    </div>
    </div>
    <asp:TextBox ID="txtCurrentPageCorr" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtDelRedirectConfigIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo" Width="25" runat="server" Style="display: none" />
</asp:Content>
