﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmPromotionItems

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hdnPromotionID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnPromotionID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnRecordType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnRecordType As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnItemSelectionType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnItemSelectionType As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''pnlItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlItems As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtItem As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnAditem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAditem As Global.System.Web.UI.HtmlControls.HtmlInputButton

    '''<summary>
    '''btnRemItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemItem As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnAddItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddItem As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''gvItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvItems As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''pnlItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlItemClassification As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlItemClassification As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAddItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddItemClassification As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRemoveItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveItemClassification As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''gvItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvItemClassification As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''pnlItemCustomFields control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlItemCustomFields As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnAddCustomFields control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddCustomFields As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnDeleteCustomFields control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDeleteCustomFields As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rblCustom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblCustom As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''plhCustomControls control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents plhCustomControls As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''hdnSelectedItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSelectedItems As Global.System.Web.UI.WebControls.HiddenField
End Class
