﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFileManager.aspx.vb"
    Inherits=".frmFileManager" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <style type="text/css">
        #FileManager {
            height: 600px;
            width: 100%;
        }
    </style>
    <script type="text/javascript">

        function OnClientItemSelected(sender, args) {
            //Show preview for only images
            //            var pvwImage = $get("pvwImage");
            //            var imageSrc = args.get_path();

            //            if (imageSrc.match(/\.(gif|jpg)$/gi)) {
            //                pvwImage.src = imageSrc;
            //                pvwImage.style.display = "";
            //            }
            //            else {
            //                pvwImage.src = imageSrc;
            //                pvwImage.style.display = "none";
            //            }
            //            if (args.get_item().get_type() == Telerik.Web.UI.FileExplorerItemType.File) {// if the item is a file
            //                var file = "File path : " + args.get_item().get_path() + "\n";
            //                file = file + "File size : " + args.get_item().get_size();
            //                alert("Selected file: \n" + file);
            //            }
            //            else {// filder
            //                alert("The selected item is a directory");
            //            }
            //            alert("OnClientItemSelected : " + (args instanceof Telerik.Web.UI.RadFileExplorerEventArgs).toString());
        }
        function OnClientFileOpen(sender, args) {
            var item = args.get_item();
            if (item && !item.isDirectory()) //&& (item.get_extension() == "docx" || item.get_extension() == "doc")
            {
                //do not open a new window.  
                args.set_cancel(true);
                var url = item.get_url();
                var DocumentPath = '';
                if (url.indexOf(document.getElementById('SiteSwitch1_ddlSites').value) > -1) {
                    DocumentPath = url.substring(url.indexOf(document.getElementById('SiteSwitch1_ddlSites').value));
                }
                //                console.log(DocumentPath);
                //                console.log(item.get_path());
                //tell browser to open file directly  
                window.open(item.get_url());
                //window.location.href = ;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12" id="trSite" runat="server">
            <div class="pull-right">
                <label>Select a site:</label>
                <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    File Manager&nbsp;<a href="#" onclick="return OpenHelpPopUp('ecommerce/frmfilemanager.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <telerik:RadFileExplorer runat="server" ID="FileExplorer2" Height="520px" Width="100%"
                    EnableCreateNewFolder="true" Skin="Vista" OnClientItemSelected="OnClientItemSelected"
                    OnClientFileOpen="OnClientFileOpen">
                    <Configuration MaxUploadFileSize="2097152" />
                </telerik:RadFileExplorer>
            </div>
            <div class="col-md-6">
                <fieldset>
                    <legend class="normal1"><b>
                        <asp:Label runat="server" ID="lblFileUpload" Text="Multiple file upload"></asp:Label></b></legend>
                    <asp:Label Text="Please make sure all images are within root directory of zip file,not into any subfolder."
                        runat="server" CssClass="normal4" Visible="false" ID="lblNote" />
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <label>Select a zip file</label>
                            </div>
                            <div class="col-md-4">
                                <asp:FileUpload runat="server" ID="fuZipUpload" CssClass="form-control" />
                            </div>
                            <div class="clearfix"></div>
                            <div id="trFolder" runat="server">
                                <div class="col-md-5">
                                    <label>Select Folder to extract files</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:DropDownList runat="server" ID="ddlSiteFolders" CssClass="signup">
                                    </asp:DropDownList>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-4 col-md-offset-3">
                                <asp:CheckBox runat="server" ID="chkOverWrite" Text="Overwrite if file exists?" CssClass="signup" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5">
                                <asp:Label ID="lblMsg" runat="server" CssClass="signup" Text="Select Height Or Width For Thumbnail"></asp:Label>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="col-md-5">
                                <asp:RadioButton ID="rbMeasureHeight" Checked="true" CssClass="signup" Text=" Height "
                                    GroupName="measure" runat="server" />
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtHeight" CssClass="required_integer {required:true ,number:true, messages:{required:'Please provide value!',number:'provide valid value!'}} signup"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-5">
                                <asp:RadioButton ID="rbMeasureWidth" CssClass="signup" Text=" Width " GroupName="measure"
                                    runat="server" />
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtWidth"  CssClass="required_integer {required:true ,number:true, messages:{required:'Please provide value!',number:'provide valid value!'}} "
                                    runat="server"></asp:TextBox>
                                <ul id="messagebox" class="errorInfo" style="display: none">
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="col-md-5">
                                <ContentTemplate>
                                    <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnUpload"><i class="fa fa-upload"></i>&nbsp;&nbsp;Extract All</asp:LinkButton>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUpload" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
