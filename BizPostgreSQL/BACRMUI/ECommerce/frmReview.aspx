﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmReview.aspx.vb" Inherits=".frmReview" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/jquery.rating.css" rel="stylesheet" type="text/css" />
       <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <%--<script src="../JavaScript/jquery.MetaData.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/jquery.rating.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(function () {
            $('.auto-submit-star').rating({
                callback: function (value, link) {
                    // 'this' is the hidden form element holding the current value
                    // 'value' is the value selected
                    // 'element' points to the link element that received the click.
                    // $('#hfRatingValue').val(value);
                    // $('#btnSubmit').click();
                    // rating(value);
                    //rating1(value,"12");
                    // To submit the form automatically:
                    //this.form.submit();

                    // To submit the form via ajax:
                    //$(this.form).ajaxSubmit();
                }
            });
        });
    </script>


    <script language="javascript" type="text/javascript">
        $(function () {
            $('.auto-submit').rating({
                callback: function (value, link) {
                    // 'this' is the hidden form element holding the current value
                    // 'value' is the value selected
                    // 'element' points to the link element that received the click.
                    // $('#hfRatingValue').val(value);
                    // $('#btnSubmit').click();
                    // rating(value);
                    //rating1(value,"12");
                    // To submit the form automatically:
                    //this.form.submit();

                    // To submit the form via ajax:
                    //$(this.form).ajaxSubmit();
                }
            });
        });
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="right-input">
        <div class="input-part">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">&nbsp;
                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="50px" CssClass="button" />
                        &nbsp;
                         <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" Width="100px" CssClass="button" />
                        &nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="button" />
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Review Detail
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="asptable" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />

                <table border="0">
                    <tr>
                        <td class="normal1" align="right" width="180px">Type of review :
                        </td>
                        <td align="left">
                            <asp:Label ID="lblReviewType" runat="server"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Title :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtTitle" Width="600px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Review :
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtReview" TextMode="MultiLine" Width="600px" Height="150px" runat="server">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Hide this review ? :
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="cbHideReview" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Is Approved ? :
                        </td>
                        <td align="left">
                            <asp:CheckBox ID="cbApproved" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Ratings :
                        </td>
                        <td align="left">
                            <input class="star" type="radio" name="test-3A-rating-1" value="1" title="very poor" disabled="disabled" />
                            <input class="star" type="radio" name="test-3A-rating-1" value="2" checked="checked" title="poor" disabled="disabled" />
                            <input class="star" type="radio" name="test-3A-rating-1" value="3" title="ok" disabled="disabled" />
                            <input class="star" type="radio" name="test-3A-rating-1" value="4" title="good" disabled="disabled" />
                            <input class="star" type="radio" name="test-3A-rating-1" value="5" title="very good" disabled="disabled" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
