﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/ECommerceMenuMaster.Master"
    CodeBehind="frmReviewList.aspx.vb" Inherits=".frmReviewList" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row">
        <div class="col-md-12" id="Table1" runat="server">
            <div class="pull-right">
                 <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" CssClass="form-control">
                        </asp:DropDownList>
                        &nbsp;
                        <asp:DropDownList ID="ddlType" Visible ="false" AutoPostBack="true" CssClass="form-control" runat="server">
                            <asp:ListItem Text="--Select--"  Value="0"></asp:ListItem>
                            <asp:ListItem Text="Category" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Product" Selected ="True" Enabled ="true"  Value="2"></asp:ListItem>
                            <asp:ListItem Text="Url" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                <a href="#" class="help">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Review List&nbsp;<a href="#" onclick="return OpenHelpPopUp('Ecommerce/frmReviewList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <asp:DataGrid ID="dgReview" AllowSorting="True" runat="server" Width="100%" CssClass="table table-striped table-bordered"
        AutoGenerateColumns="False" UseAccessibleHeader="true">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:TemplateColumn HeaderText="Reviewed By">
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:Label ID="lblReviewId" Text='<%#Eval("numReviewId") %>' Visible="false" runat="server">
                    </asp:Label>
                    <asp:Label ID="lblReviewType" Text='<%#Eval("vcContactName") %>' runat="server">
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Title">
                <ItemStyle Width="130px" />
                <ItemTemplate>
                    <asp:Label ID="lblTitle" Text='<%#Eval("vcReviewTitle") %>' runat="server">
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Review">
                <ItemStyle Width="130px" />
                <ItemTemplate>
                    <asp:Label ID="lblReview" Text='<%#Eval("vcReviewComment") %>' runat="server">
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Is Email">
                <ItemStyle Width="60px" />
                <ItemTemplate>
                    <img src='<%# iif(Eval("bitEmailMe")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'
                        alt="" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Is Hide">
                <ItemStyle Width="60px" />
                <ItemTemplate>
                    <img src='<%# iif(Eval("bitHide")=true,"../images/PlUploadImages/done.gif","../images/PlUploadImages/cancel-icon.png") %>'
                        alt="" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Ip Address">
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:Label ID="lblIpAddress" Text='<%#Eval("vcIpAddress") %>' runat="server">
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Total Helpful">
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:Label ID="lblTotalHelpful" Text='<%# iif(Eval("vcTotalHelpful")=0,"-",Eval("vcHelpful").toString() + "/" + Eval("vcTotalHelpful").toString()) %>'
                        runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Total Abuse">
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:Label ID="lblTotalAbuse" Text='<%# iif(Eval("vcTotalAbuse")=0,"-",Eval("vcTotalAbuse")) %>'
                        runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Created Date & Time">
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <asp:Label ID="lblCreatedDateTime" Text='<%#Eval("dtCreatedDate") %>' runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-Width="8" ItemStyle-Width="8">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                        CommandArgument='<%#Eval("numReviewId") %>'></asp:Button>
                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-Width="30" ItemStyle-Width="30">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:HyperLink ID="hplImage" NavigateUrl='<%#"frmReview.aspx?ReviewID=" + Eval("numReviewId").toString() %>'
                        runat="server" CssClass="hyperlink">Edit </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
        </div>
</asp:Content>
