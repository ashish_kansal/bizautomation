﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/ECommerceMenuMaster.Master" CodeBehind="frmPostOrderIncentives.aspx.vb" Inherits=".frmPostOrderIncentives" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Post Check-Out Incentives</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <style>
        .hidden {
            display: none;
        }
    </style>
    <script type="text/javascript">

        function SelectIncentiveOption() {
            $("#rdbtnIncentiveSale").prop("checked", true);
        }

        function OpenItemSelectionWindow(type, recordType, promotionId) {
            $("#rdbtnIncentiveSale").prop("checked", true);
            if (type == 1 && recordType == 5) {
                window.open("../ECommerce/frmPromotionItems.aspx?promotionID=" + promotionId + "&itemSelectionType=" + type + "&recordType=" + recordType, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
                return false;
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return Save()"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Save()"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">Order based Promotion</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="col-sm-12 col-md-5">
        <div class="form-inline">
            <div class="form-group">
                <label>Name:</label>
                <asp:TextBox ID="txtOfferName" runat="server" CssClass="form-control" MaxLength="100" Width="400px"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form-inline" runat="server" id="divSaleOptions">
        <div class="form-group" style="margin-left: 10px; margin-right: 10px">
            <br />
            <br />
            <b>
                <label>Cross-Sale</label></b><br />
            <asp:RadioButton ID="rdBtnCrossSale" runat="server" CssClass="signup" Checked="true" GroupName="grpSales"></asp:RadioButton>&nbsp;
            When “Submit Order” is clicked on E-Commerce (not internal orders), present list of post check-out items based on parent items added to the shopping cart.
            <br />
            <br />
            <b>
                <label>Free Item Incentives</label></b><br />
            <asp:RadioButton ID="rdbtnIncentiveSale" Font-Bold="false" runat="server" CssClass="signup" GroupName="grpSales" />&nbsp;When “Submit Order” is clicked on E-Commerce OR Internal Orders, if sub-total amount is = or >&nbsp;<asp:TextBox runat="server" Width="50px" ID="txtSubTotal" Onblur="SelectIncentiveOption()"></asp:TextBox>&nbsp;
            add&nbsp;<asp:HyperLink ID="hplIncentiveSale" runat="server">the following item(s) &nbsp;</asp:HyperLink>to the order for free.
            <asp:LinkButton ID="lnkBtnAddRule" runat="server" CssClass="btn btn-primary" Text="Add Rule" OnClick="lnkBtnAddRule_Click"></asp:LinkButton>
        </div>
        <div class="form-group" style="margin-left: 10px; margin-right: 10px; padding-top: 20px;">
            <asp:GridView ID="gvItemsForRule" CssClass="table table-bordered table-striped" runat="server"
                AutoGenerateColumns="false" Width="1050px" CellPadding="3"
                CellSpacing="3" BorderColor="#f0f0f0" OnRowEditing="gvItemsForRule_RowEditing"
                OnRowCancelingEdit="gvItemsForRule_RowCancelingEdit" OnRowDeleting="gvItemsForRule_RowDeleting"
                OnRowUpdating="gvItemsForRule_RowUpdating">
                <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                <Columns>
                    <asp:BoundField DataField="fltSubTotal" HeaderText="Sub-total amount" ItemStyle-Width="600" ItemStyle-Wrap="true" />
                    <asp:BoundField DataField="Items" HeaderText="Item Name" ItemStyle-Width="600" ItemStyle-Wrap="true" ReadOnly="true" />
                    <asp:BoundField DataField="numBaseUnit" HeaderText="Units" ItemStyle-Width="600" ItemStyle-Wrap="true" ReadOnly="true" />
                    <%--<asp:BoundField DataField="monListPrice" HeaderText="Unit Price" ItemStyle-Width="600" ItemStyle-Wrap="true" />--%>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Your Price" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblFree" runat="server" Width="100" Text="Free"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="fltSubTotal" ItemStyle-CssClass="hidden" />
                    <asp:TemplateField HeaderStyle-Width="64" ItemStyle-Width="64" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CssClass="btn btn-info btn-xs" CommandName="Edit" ID="lnkbtnEdt"><i class="fa fa-pencil"></i></asp:LinkButton>
                            <asp:LinkButton ID="lnkdelete" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CssClass="btn btn-primary" CommandName="Update" OnClientClick="return EditGrid();"></asp:LinkButton>&nbsp;
                    <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl" CssClass="btn btn-default"></asp:LinkButton>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>No Records Found.</EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
    <asp:HiddenField ID="hfProId" runat="server"></asp:HiddenField>
</asp:Content>
