﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmShippingClassifications.aspx.vb" Inherits=".frmShippingClassifications" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
     <link rel="stylesheet" href="../CSS/bootstrap.min.css" />
    <link rel="stylesheet" href="../CSS/font-awesome.min.css" />
    <link rel="stylesheet" href="../CSS/biz.css" />
   
    <script type="text/javascript">
         function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

         function validate() {
             if (document.getElementById('ddlClassifications').value == 0 ) {
                alert("Select Classification.");
                document.getElementById('ddlClassifications').focus();
                return false;
             }

            if (document.getElementById('txtPercentAbove').value == "") {
                alert("Enter % above normal shipping.");
                document.getElementById('txtPercentAbove').focus();
                return false;
             }

            if (document.getElementById('txtFlatAmt').value == "" ) {
                alert("Enter flat amount to charge if shipping is free.");
                document.getElementById('txtFlatAmt').focus();
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server" ClientIDMode="Static">
    <table   width="610px">
        <tr style="float:right">
            <td>
                <asp:Button Text="Add" runat="server" ID="btnAdd" OnClientClick="return validate();" OnClick="btnAdd_Click" CssClass="btn btn-primary" /> &nbsp;
                <asp:Button ID="btnClose" CssClass="btn btn-primary" runat="server" Text="Close" Style="float: right;" OnClientClick="window.close();"></asp:Button>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:Panel runat="server" ID="pnl">
<table width="610px" style="height:120px"  border="0">
    <tr>
        <td style="width:15%">
            <label runat="server">Classifications: </label>  
        </td>
        <td style="width:30%">
           &nbsp; <asp:DropDownList ID="ddlClassifications"  Width="200px" runat="server"></asp:DropDownList>
        </td>
        <td style="width:25%">
            &nbsp;   <label>% above normal shipping</label>
        </td>
        <td style="width:15%;">
             <asp:TextBox ID="txtPercentAbove" CssClass="form-control"  Width="80px" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <label>If shipping is free on normal items, charge this amount for these items</label>
        </td>
        <td>
            <asp:TextBox ID="txtFlatAmt" CssClass="form-control" Width="80px" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="4">
        <asp:DataGrid ID="dgShipClassification" AllowSorting="True" runat="server" Width="100%"
            CssClass="dg" AutoGenerateColumns="False">
            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
            <ItemStyle CssClass="is"></ItemStyle>
            <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
            <Columns>              
                <asp:BoundColumn DataField="vcClassification" HeaderText="Classification"></asp:BoundColumn>
                <asp:BoundColumn DataField="PercentAbove" HeaderText="% above"></asp:BoundColumn>
                <asp:BoundColumn DataField="FlatAmt" HeaderText="Flat Amt if shipping is free elsewhere"></asp:BoundColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                            CommandArgument='<%# Eval("numShippingExceptionID") %>' OnClientClick="return DeleteRecord()"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            </asp:DataGrid>
        </td>
    </tr>
</table>
</asp:Panel>
</asp:Content>
