﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports System.Net
Imports System.IO
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.ShioppingCart





Public Class frmReview
    Inherits BACRMPage

#Region "Global Variable"

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Me.ReviewId <> 0 Then
                    BindReviewData()
                Else
                    Response.Redirect("/ECommerce/frmReviewList.aspx")
                End If
            End If

            'Dim list As New List(Of Review1)

            'Dim index As Integer
            'For index = 1 To 10
            '    Dim rev As New Review1
            '    rev.vcReviewType = "Product"
            '    rev.numReviewId = index
            '    rev.vcUserName = "Kishan_" & Convert.ToString(index)
            '    rev.vcTitle = "Its Bang on  Product i have ever seen"
            '    rev.vcReview = "Its a good Product at all . I have purchased it before few days ago. Delivery service was quite prompt and i also got the product as i saw on the website.so overall its a good deal at all."
            '    rev.bitEmail = True
            '    rev.vcIpAddress = "299.177.78.78"
            '    rev.vcTotalHelpful = "14/20"
            '    rev.vcTotalAbuse = "15"
            '    rev.dtCreatedDatetime = System.DateTime.Now.Date
            '    list.Add(rev)
            'Next

            'Dim rev1 As New Review1
            'rev1 = list.Where(Function(i) i.numReviewId = 1).FirstOrDefault()


            'txtReviewType.Text = rev1.vcReviewType
            'txtReview.Text = rev1.vcReview
            'txtTitle.Text = rev1.vcTitle



        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Ecommerce/frmReviewList.aspx?ReviewType=" + CCommon.ToString(Me.ReviewType))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            UpdateReview()
            Response.Redirect("../Ecommerce/frmReviewList.aspx?ReviewType=" + CCommon.ToString(Me.ReviewType))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            UpdateReview()
            BindReviewData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region
    

#Region "Methods"
    Sub BindReviewData()
        Try

            Dim dtReview As DataTable

            Dim objReview As New Review
            objReview.Mode = 3
            objReview.ReviewId = Me.ReviewId
            objReview.DomainID = CCommon.ToLong(Session("DomainID"))
            objReview.SiteId = CCommon.ToLong(Session("SiteID"))

            dtReview = objReview.GetReviews()

            If dtReview.Rows.Count > 0 Then
                lblReviewType.Text = CCommon.ToString(dtReview.Rows(0)("vcReviewType"))
                txtTitle.Text = CCommon.ToString(dtReview.Rows(0)("vcReviewTitle"))
                txtReview.Text = CCommon.ToString(dtReview.Rows(0)("vcReviewComment"))
                cbHideReview.Checked = CCommon.ToBool(dtReview.Rows(0)("bitHide"))
                cbApproved.Checked = CCommon.ToBool(dtReview.Rows(0)("bitApproved"))
                Me.ReviewType = CCommon.ToString(dtReview.Rows(0)("tintTypeId"))
            End If


        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Sub UpdateReview()
        Try
            Dim objReview As New Review
            objReview.ReviewId = Me.ReviewId
            objReview.Mode = 3

            objReview.ReviewTitle = txtTitle.Text
            objReview.ReviewComment = txtReview.Text
            objReview.Hide = cbHideReview.Checked
            objReview.Approved = cbApproved.Checked
            objReview.CreatedDate = CCommon.ToString(System.DateTime.Now)
            objReview.ManageReview()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Properties"
    Public Property ReviewId() As Long
        Get
            Dim o As Object = ViewState("ReviewID")
            If o IsNot Nothing Then
                Return CType(o, Long)
            Else
                ViewState("ReviewID") = GetQueryStringVal("ReviewID")
                Return CType(ViewState("ReviewID"), Long)
            End If
        End Get
        Set(value As Long)
            ViewState("ReviewID") = value
        End Set
    End Property

    Public Property ReviewType() As String
        Get
            Dim o As Object = ViewState("ReviewType")
            If o IsNot Nothing Then
                Return CType(o, String)
            Else
                Return ""
            End If
        End Get
        Set(value As String)
            ViewState("ReviewType") = value
        End Set
    End Property
#End Region


End Class

'Public Class Review1
'    Public Property vcReviewType As String
'    Public Property numReviewId As Integer
'    Public Property vcUserName As String
'    Public Property vcTitle As String
'    Public Property vcReview As String
'    Public Property bitEmail As Boolean
'    Public Property vcIpAddress As String
'    Public Property vcTotalHelpful As String
'    Public Property vcTotalAbuse As String
'    Public Property dtCreatedDatetime As DateTime

'End Class