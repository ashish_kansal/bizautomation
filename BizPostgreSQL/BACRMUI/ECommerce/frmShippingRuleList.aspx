﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingRuleList.aspx.vb"
    Inherits=".frmShippingRuleList" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Rule</title>
    <script type="text/javascript" language="javascript">
        function New() {
            //if (document.getElementById('ddlSites').selectedIndex == 0) {
            //    //alert("Please select a site to create New Shipping Rule.");
            //    if (confirm("This will create generic shipping rules without using sites, are you sure to want to create generic shipping rules ?")) {
            //        window.location.href = "frmShippingRule.aspx";
            //        return true;
            //    }
            //    else {
            //        return false;
            //    }
            //}
            //else {
                window.location.href = "frmShippingRule.aspx";
            //}
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenClassificationsWindow() {

            window.open("../ECommerce/frmShippingClassifications.aspx", '', 'toolbar=no,titlebar=no,top=200,width=900,height=600,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12" id="Table1" runat="server">
            <table width="100%">
                <tr>
                    <td>
                        <table>
                        <tr>
                            <td>
                                <asp:Label ID="lbl1" runat="server" Text="If any discount causes shopping cart to be =< $0 add the following minimum shipping cost:"></asp:Label>&nbsp;
                                <asp:TextBox ID="txtminShippingCost" runat="server"></asp:TextBox>&nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblRelProfile" runat="server" Text="Relationships / Profiles "></asp:Label>&nbsp;
                                <asp:DropDownList ID="ddlRelProfile" Width="250px" runat="server" CssClass="form-control" style="display:unset;" autopostback="true" OnSelectedIndexChanged="ddlRelProfile_SelectedIndexChanged" ></asp:DropDownList>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkbxExcludeClassifications" runat="server" />&nbsp;
                               Exclude items in the <a href="#" onclick="return OpenClassificationsWindow();">following classifications.</a>
                            </td>
                        </tr>
                        </table>
                    </td>
                    <td style="vertical-align:top; float:right;">
                        <div class="pull-right" >
                       <%-- <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />--%>
                                &nbsp;
                        <asp:LinkButton runat="server" id="btnSave" OnClick="btnSave_Click" class="btn btn-primary">&nbsp;&nbsp;Save</asp:LinkButton>  &nbsp; 
                        <asp:LinkButton runat="server" OnClientClick="return New();" id="btnNew" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Shipping Rule</asp:LinkButton>
                        
                        <a href="#" class="help">&nbsp;</a>
                    </div>
                    </td>
                </tr>
            </table>             
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Shipping Rules&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmShippingRuleList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                 <asp:CheckBox ID="chkbxEnableStaticShip" runat="server" />
              <label>  Enable Static Shipping Rules based on Order-Amounts <asp:Label ID="Label88" Text="[?]" CssClass="tip" runat="server" ToolTip="Checking this box means that BizAutomation will automatically add the shipping language to new orders, and will automatically add the shipping line item when an order is created internally or externally (via the web store), with the amounts calculated from the shipping rule. " /></label>
            </td>
        </tr>
        <tr>
            <td> 
                <asp:DataGrid ID="dgShippingRuleList" runat="server" Width="100%" CssClass="table table-striped table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numRuleID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcRuleName" CommandName="RuleID" HeaderStyle-Width="15%" HeaderText="<font>Rule Name</font>">
                        </asp:ButtonColumn>
                         <asp:BoundColumn  DataField="vcShipFrom" HeaderText="Ship-From" HeaderStyle-Width="15%" ></asp:BoundColumn>
                         <asp:BoundColumn  DataField="vcShipTo" HeaderText="Ship-To" HeaderStyle-Width="28%" ></asp:BoundColumn>
                         <asp:BoundColumn  DataField="vcShippingCharges" HeaderText="Shipping Charges" HeaderStyle-Width="25%" ></asp:BoundColumn>
                         <asp:BoundColumn  DataField="vcAppliesTo" HeaderText="Applies To" HeaderStyle-Width="17%" ></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText ="Shipping Method" Visible="false"> 
                         <ItemTemplate >
                            <asp:Label Text='<%# IIf(Eval("tinShippingMethod")="1","Fixed Shipping Quotes","Real Time Shipping Quotes") %>' runat="server" ID="lblShippingMethod" />
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText ="Fixed shipping Quotes Mode" Visible="false"> 
                         <ItemTemplate >
                            <asp:Label Text='<%# IIf(Eval("tinShippingMethod")="2","-",Eval("vcFixedShippingQuotesMode")) %>' runat="server" ID="lblFixedShippingQuotes" />
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcTaxMode" Visible="false"  HeaderText="<font>Tax Mode</font>">
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn HeaderText="Item it applies to" DataField="ItemList"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Customer it applies to" DataField="CustomerList"></asp:BoundColumn>--%>
                        <asp:TemplateColumn>
                            <ItemStyle width="10px"/>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
								<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
        </div>
</asp:Content>
