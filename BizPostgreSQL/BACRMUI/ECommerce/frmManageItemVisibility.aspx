﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmManageItemVisibility.aspx.vb" Inherits=".frmManageItemVisibility" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="../CSS/bootstrap.min.css" />
    <link rel="Stylesheet" href="../CSS/biz.css" />
    <link rel="Stylesheet" href="../CSS/font-awesome.min.css" />

    <script type="text/javascript">
        function Add() {
            if ($("[id$=ddlRelationship]").val() == 0) {
                alert('Select Relationship');
                return false;
            } else if ($("[id$=ddlProfile]").val() == 0) {
                alert('Select Profile');
                return false;
            } else {
                var combo = $find("<%= radCmbItemClassifications.ClientID %>");
                    if (combo.get_checkedItems().length == 0) {
                        alert('Select atleast one item classification');
                        return false;
                    } else {
                        if (confirm('When users visit this site, items belonging to this item classification will be hidden, even if those items are subscribed to the site’s category list. Are you sure that is what you want to do?')) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

        }

        function Remove() {

            var isRowSelected = false;
            $("#gvRelationshipProfile tr").each(function () {
                if ($(this).find("#chkSelect") != null && $(this).find("#chkSelect").is(":checked")) {
                    isRowSelected = true;
                    return false;
                }
            })

            if (!isRowSelected) {
                alert("Select atleast one row.");
                return false;
            } else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary pull-right" OnClientClick="Close();" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Set items to hide by organization relationship & profile
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="cotainer" style="width: 1026px; min-height: 330px;">
        <div class="row padbottom10" id="divError" runat="server" style="display: none">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <h4><i class="icon fa fa-ban"></i>Error</h4>
                    <p>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="form-inline">
                    <div class="form-group">
                        <label><span style="color: red">*</span>Relationship:</label>
                        <asp:DropDownList ID="ddlRelationship" runat="server" Width="200" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label><span style="color: red">*</span>Profile:</label>
                        <asp:DropDownList ID="ddlProfile" runat="server" Width="200" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label><span style="color: red">*</span>Item Group(s):</label>
                        <telerik:RadComboBox ID="radCmbItemClassifications" runat="server" EmptyMessage="-- Select One --" CheckBoxes="true" EnableCheckAllItemsCheckBox="true" Width="200"></telerik:RadComboBox>
                    </div>
                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add" OnClientClick="return Add();" />
                    <asp:Button ID="btnRemove" runat="server" CssClass="btn btn-danger pull-right" Text="Remove" OnClientClick="return Remove();" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvRelationshipProfile" runat="server" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" AutoGenerateColumns="false" DataKeyNames="ID" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:BoundField HeaderText="Customer Relatioship" DataField="vcRelationship" />
                            <asp:BoundField HeaderText="Customer Profile" DataField="vcProfile" />
                            <asp:BoundField HeaderText="Item Groups" DataField="vcItemClassifications" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No Data Available
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnSite" runat="server" />
</asp:Content>
