﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingLabelBatchProcessingList.aspx.vb"
    Inherits=".frmShippingLabelBatchProcessingList" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Packaging Automation</title>
    <script type="text/javascript">
        function New() {
            window.location.href = "frmShippingLabelBatchProcessing.aspx";
        }
        function NewCB() {
            window.location.href = "frmAddCustomBox.aspx";
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteRecordCB(mode) {
            if (mode == 0) {
                if (confirm('Are you sure, you want to delete the selected record?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('You are not authorised to delete this record.')
                return false;
            }
        }
    </script>
    <style type="text/css">
        .customerdetail {
            min-height: 0!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <telerik:RadTabStrip ID="radTab_Boxes" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" MultiPageID="radMultiPage_Boxes" SelectedIndex="0" AutoPostBack="True">
        <Tabs>
            <telerik:RadTab Text="Packaging/Box Rules" Value="PBRules" PageViewID="radPageView_PBRules">
            </telerik:RadTab>
            <telerik:RadTab Text="Custom Boxes" Value="CB" PageViewID="radPageView_CutomBox">
            </telerik:RadTab>

        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_Boxes" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_PBRules" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="btnNew" runat="server" OnClientClick="New();" UseSubmitBehavior="true" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Rule</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgShippingRuleList" runat="server" Width="100%" CssClass="table table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numShippingRuleID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcRuleName" CommandName="RuleID" HeaderText="Rule Name"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="ShipBasedOn" HeaderText="Ship Based On"></asp:BoundColumn>
                                <asp:BoundColumn DataField="SourceMarketplace" HeaderText="Source Marketplace"></asp:BoundColumn>
                                <asp:BoundColumn DataField="SourceShipMethod" HeaderText="Source Ship Method"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ItemsAffected" HeaderText="Items Affected"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-Width="25">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_CutomBox" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="btnNewCB" runat="server" OnClientClick="NewCB();" UseSubmitBehavior="true" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Box</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgCustomBox" runat="server" Width="100%" CssClass="table table-responsive table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numPackageTypeID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numCustomPackageID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcPackageName" CommandName="CustomPackageID" HeaderText="Box Name"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="fltWidth" HeaderText="Width"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltHeight" HeaderText="Height"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltLength" HeaderText="Length"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltTotalWeight" HeaderText="Weight"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkdelete" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>


</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Packaging Automation
</asp:Content>
