﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.WebAPI

Public Class frmPackagingRules
    Inherits BACRMPage

#Region "OBJECT DECLARATION"

    Dim objShippingRule As ShippingRule

#End Region

#Region "GLOBAL DECLARATION"

    Dim lngRuleID As Long

#End Region

#Region "PAGE EVENTS"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")
            lngRuleID = CCommon.ToLong(GetQueryStringVal("RuleID"))

            If Not Page.IsPostBack Then
                FillShipClass()
                BindPackageType()

                ddlPackageType.Items.Insert(0, New ListItem("-- Select One --", "0"))
                ddlPackageType.SelectedIndex = 0

                If lngRuleID > 0 Then
                    hdnShipRuleID.Value = lngRuleID
                    btnSave.Visible = True
                    LoadDetails()
                    'trMain.Visible = True
                Else
                    'trMain.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "BUTTON EVENTS"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()

            If objShippingRule.ExceptionMsg <> "" Then
                litMessage.Text = objShippingRule.ExceptionMsg
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()

            If objShippingRule.ExceptionMsg <> "" Then
                litMessage.Text = objShippingRule.ExceptionMsg
            Else
                Response.Redirect("../ECommerce/frmPackagingRuleList.aspx")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmPackagingRuleList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "METHODS"

    Public Sub FillShipClass()
        Try
            objCommon.sb_FillComboFromDBwithSel(ddlShipClass, 461, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            objShippingRule = New ShippingRule()
            objShippingRule.RuleID = lngRuleID
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.byteMode = 0
            dtTable = objShippingRule.GetPackagingRules()

            If dtTable.Rows.Count > 0 Then
                txtRuleName.Text = dtTable.Rows(0).Item("vcRuleName")
                If ddlPackageType.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numPackageTypeID"))) IsNot Nothing Then
                    ddlPackageType.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numPackageTypeID"))).Selected = True
                End If

                If ddlShipClass.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numShipClassID"))) IsNot Nothing Then
                    ddlShipClass.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numShipClassID"))).Selected = True
                End If
                txtFrom.Text = CCommon.ToLong(dtTable.Rows(0).Item("numFromQty"))
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub Save()
        Try
            objShippingRule = New ShippingRule
            objShippingRule.RuleID = lngRuleID
            objShippingRule.RuleName = txtRuleName.Text
            objShippingRule.PackageTypeID = ddlPackageType.SelectedItem.Value
            objShippingRule.ShipClassID = ddlShipClass.SelectedItem.Value
            objShippingRule.FromQty = Val(txtFrom.Text)
            objShippingRule.DomainID = DomainID
            If objShippingRule.ManagePackagingRules() > 0 Then
                litMessage.Text = "Package Rule saved successfully."
            Else
                litMessage.Text = "Error occurred while save Package data."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "BIND DROPDOWNS"

    Private Sub BindPackageType()
        Try
            Dim dsPackages As DataSet = Nothing
            dsPackages = OppBizDocs.LoadAllPackages(Session("DomainID"))
            If dsPackages IsNot Nothing AndAlso dsPackages.Tables.Count > 0 AndAlso dsPackages.Tables(0).Rows.Count > 0 Then

                ddlPackageType.DataSource = dsPackages
                ddlPackageType.DataValueField = "numCustomPackageID"
                ddlPackageType.DataTextField = "vcPackageName"
                ddlPackageType.DataBind()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class