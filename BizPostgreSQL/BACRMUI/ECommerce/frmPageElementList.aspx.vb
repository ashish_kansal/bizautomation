﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Partial Public Class frmPageElementList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
        Try


            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If Not IsPostBack Then
                BindData()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindData()
        Try
            If Session("SiteID") > 0 Then
                Dim objSite As New Sites
                Dim dtPageElements As DataTable
                objSite.SiteID = CCommon.ToLong(Session("SiteID"))
                dtPageElements = objSite.GetPageElementsMaster()
                dgPageElements.DataSource = dtPageElements
                dgPageElements.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPageElements_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPageElements.ItemCommand
        Try
            Dim objSites As New Sites
            If e.CommandName = "Add" Then
                'Get Current Row 
                objSites.vcElementName = hdnElementName.Value.ToString
                objSites.vcTagName = e.Item.Cells(2).Text
                objSites.vcUserControlPath = e.Item.Cells(7).Text
                objSites.bitCustomization = 1
                objSites.bitAdd = False
                objSites.bitDelete = True
                objSites.bitFlag = False
                objSites.numElementId = CCommon.ToLong(e.Item.Cells(0).Text)
                objSites.ManagePageElementMaster()
            End If
            If e.CommandName = "Delete" Then
                objSites.numElementId = CCommon.ToLong(e.Item.Cells(0).Text)
                objSites.bitFlag = True
                objSites.ManagePageElementMaster()
            End If
            BindData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPageElements_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPageElements.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                Dim btnAdd As Button
                btnAdd = e.Item.FindControl("btnAdd")
                If e.Item.Cells(3).Text = True Then
                    btnAdd.Visible = True
                    btnAdd.Attributes.Add("onClick", "return InsertElementName();")
                Else
                    btnAdd.Visible = False
                End If

                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If e.Item.Cells(4).Text = True Then
                    btnDelete.Visible = True
                Else
                    btnDelete.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class