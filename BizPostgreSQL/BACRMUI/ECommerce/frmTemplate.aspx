﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTemplate.aspx.vb" Inherits=".frmTemplate"
    ValidateRequest="false" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/JavaScript">
        function Save() {
            if (document.getElementById('txtTemplateName').value == "") {
                alert("Enter Template Name")
                document.getElementById('txtTemplateName').focus()
                return false;
            }
            return true;
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "PageElement" || name == "Template") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
        //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html
        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    //                    console.log(img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    var siteID = document.getElementById('hdnSiteID').value;
                    img.setAttribute("src", img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    //                    console.log(img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }
    </script>
    <style>
        .EditCss
        {
            font-family: arial;
            font-size: 10pt;
            color: Black;
        }
    </style>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                 <asp:LinkButton ID="btnPublish" runat="server" CssClass="btn btn-primary"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Publish</asp:LinkButton>
                        <asp:LinkButton ID="btnSaveOnly" runat="server" Text="Save" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                        <asp:LinkButton ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close
                        </asp:LinkButton>
                        &nbsp;
                <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTemplate" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="table table-responsive tblDataGrid" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-md-3">
                            <label> Template Name<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtTemplateName" runat="server" CssClass="form-control" Width="200"></asp:TextBox>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Template HTML</label>
                        </div>
                        <div class="col-md-7">
                             <telerik:RadEditor ID="RadEditor1" runat="server" Height="515px" OnClientPasteHtml="OnClientPasteHtml"
                                OnClientCommandExecuting="OnClientCommandExecuting" ToolsFile="~/Marketing/EditorTools.xml"
                                AllowScripts="true">
                                <Content>
                                </Content>
                                <Tools>
                                    <telerik:EditorToolGroup>
                                        <telerik:EditorDropDown Name="Template" Text="Template">
                                        </telerik:EditorDropDown>
                                        <telerik:EditorDropDown Name="PageElement" Text="Page Element">
                                        </telerik:EditorDropDown>
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
        </div>
</asp:Content>
