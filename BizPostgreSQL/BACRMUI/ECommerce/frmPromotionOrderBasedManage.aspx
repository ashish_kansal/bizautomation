﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPromotionOrderBasedManage.aspx.vb" Inherits=".frmPromotionOrderBasedManage" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Order based Promotion</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>

    <style>
        .hidden {
            display: none;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $("[id$=chkUseForCouponManagement]").change(function () {
                if ($(this).is(":checked")) {
                    $("[id$=divDiscountRange]").hide();
                } else {
                    $("[id$=divDiscountRange]").show();
                }
            });
        });

        function hideChkCodeDiv() {
            $("#divCouponCodes").hide();
            return false;
        }
        function hideAuditTrailDiv() {
            $("#divAuditTrail").hide();
            return false;
        }

        function CheckCodeAvailability() {
            if ($("#txtCouponCode").val() == "") {
                alert("Please enter 'Require Coupon code'");

                return false;
            }
        }

        function SetSelectedDiscount() {
            var selectedText = $("#ddlDiscountType1 option:selected").text();
            $("#lblDiscountType2").text(selectedText);
            $("#lblDiscountType3").text(selectedText);
        }

        function validateEnteredDiscounts() {

            if (parseInt(document.getElementById('hfProId').value) > 0) {

                var fistInput = document.getElementById("txtDiscount1").value;
                var secondInput = document.getElementById("txtDiscount2").value;

                if (!$('#chkNeverExpires').is(":checked")) {
                    if ($('[id$=calValidFromDate_txtDate]').val() === "") {
                        alert("Enter Validity From Date.");
                        return false;
                    }
                    if ($('[id$=calValidToDate_txtDate]').val() === "") {
                        alert("Enter Validity To Date.");
                        return false;
                    }
                }

                if (!$("[id$=chkUseForCouponManagement]").is(":checked"))
                {
                    if ($("#txtOrder1").val() == "" || $("#txtDiscount1").val() == "") {
                        alert("Please enter 1st Order and Discount values.");
                        return false;
                    } if ($("#txtOrder2").val() != "" && (parseInt($("#txtOrder2").val()) < parseInt($("#txtOrder1").val()))) {
                        alert("2nd Order amount must be greater than 1st Order.");
                        return false;
                    } if ($("#txtDiscount2").val() != "" && (parseInt($("#txtDiscount2").val()) < parseInt($("#txtDiscount1").val()))) {
                        alert("2nd discount must be greater than 1st discount.");
                        return false;
                    } if ($("#txtOrder3").val() != "" && (parseInt($("#txtOrder3").val()) < parseInt($("#txtOrder2").val()))) {
                        alert("3rd Order amount must be greater than 2nd Order.");
                        return false;
                    } if ($("#txtDiscount3").val() != "" && (parseInt($("#txtDiscount3").val()) < parseInt($("#txtDiscount2").val()))) {
                        alert("3rd discount must be greater than 2nd discount.");
                        return false;
                    }
                }

                if ($("#txtCouponCode").val() != "" && !$('#chkbxCouponCode').is(":checked")) {
                    alert("Please check require coupon code to save coupon");
                    return false;
                }
            }
            else {
                if ($("#txtOfferName").val() == "") {
                    alert("Promotion name is required.")
                    return false;
                }
            }
            return true;
        }

        function OpenConfig(StepNo, StepValue, PromotionID) {
            window.open("../ECommerce/frmPromotionOfferCustomers.aspx?Step=" + StepNo + "&StepValue=" + StepValue + "&PromotionID=" + PromotionID, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('#' + headerCheckBox).is(':checked'))
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return validateEnteredDiscounts()"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="return validateEnteredDiscounts()"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">Order based Promotion</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField ID="hfProId" runat="server"></asp:HiddenField>

    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="form-inline">
                <div class="form-group">
                    <label>Name:</label>
                    <asp:TextBox ID="txtOfferName" runat="server" CssClass="form-control" MaxLength="100" Width="400px"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <%--<asp:Panel ID="pnlOrderPromotionDetailss" runat="server" Visible="false" >--%>
    <div id="divOrderPromotionDetail" runat="server" visible="false">
        <div class="row padbottom10">
            <div class="col-sm-12 col-md-5">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:HyperLink ID="hplRelProfile" CssClass="hyperlink" Text="<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles </label>" runat="server"> 
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div style="float: right;" class="col-sm-12 col-md-7">
                <div class="form-group" id="divValidity" runat="server">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Valid From:</label>
                            <BizCalendar:Calendar ID="calValidFromDate" runat="server" ClientIDMode="AutoID" />
                        </div>
                        <div class="form-group">
                            <label>To:</label>
                            <BizCalendar:Calendar ID="calValidToDate" runat="server" ClientIDMode="AutoID" />
                        </div>
                        <asp:CheckBox ID="chkNeverExpires" runat="server" Text="Promotion never expires" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="col-xs-12">
                <asp:CheckBox ID="chkUseForCouponManagement" runat="server" AutoPostBack="false" />
                <b>Use for coupon code management only</b>
            </div>
        </div>
        <div class="row padbottom10" id="divDiscountRange" runat="server">
            <div class="col-xs-12">
                <div class="form-group">
                    <table style="width:100%" border="0">
                        <tr>
                            <td style="padding-left: 15px;">
                                <label runat="server">When order exceeds</label>&nbsp;
                        <asp:TextBox ID="txtOrder1" Width="60px" runat="server"></asp:TextBox>&nbsp;  &nbsp;<b>discount the entire order by </b>&nbsp;
                    
                        <asp:TextBox ID="txtDiscount1" Width="30px" runat="server"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddlDiscountType1" runat="server" Font-Bold="false" onchange="SetSelectedDiscount();" CssClass="form-control" Style="display: unset" Width="120px">
                            <asp:ListItem Value="1" Selected="True">Percentage</asp:ListItem>
                            <asp:ListItem Value="2">Flat Amount</asp:ListItem>
                        </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 5px;">
                                <asp:CheckBox ID="chkboxDiscount2" runat="server" Text="<label> When order exceeds</label>" />
                                &nbsp;                     
                        <asp:TextBox ID="txtOrder2" Width="60px" runat="server"></asp:TextBox>&nbsp;  &nbsp;<b>discount the entire order by </b>&nbsp;
                        <asp:TextBox ID="txtDiscount2" Width="30px" runat="server"></asp:TextBox>&nbsp;&nbsp;
                        <asp:Label ID="lblDiscountType2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 2px;">
                                <asp:CheckBox ID="chkboxDiscount3" runat="server" Text="When order exceeds" />
                                &nbsp;
                        <asp:TextBox ID="txtOrder3" Width="60px" runat="server"></asp:TextBox>&nbsp;  &nbsp;<b>discount the entire order by </b>&nbsp;
                        <asp:TextBox ID="txtDiscount3" Width="30px" runat="server"></asp:TextBox>&nbsp;&nbsp;
                        <asp:Label ID="lblDiscountType3" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="col-xs-12">
                <table style="width:100%" border="0">
                    <tr>
                        <td style="padding-top: 15px;">
                            <asp:CheckBox ID="chkbxCouponCode" runat="server" Text="Require Coupon code" />&nbsp;&nbsp; 
                        </td>
                        <td style="vertical-align: top;">
                            <table>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkChkCodeAvailablity" OnClientClick="return CheckCodeAvailability();" OnClick="lnkChkCodeAvailablity_Click" Style="text-decoration: underline;" runat="server">Check code availability</asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkAuditTrail" OnClick="lnkAuditTrail_Click" Style="text-decoration: underline;" runat="server">Audit Trail</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtCouponCode" Width="220px" runat="server"></asp:TextBox>&nbsp;<b> to execute promotion.</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="padding-top: 15px;">&nbsp; 
                                    <label id="Label1" runat="server">Good for</label>&nbsp; 
                                    <asp:DropDownList ID="ddlUsageLimit" Width="150px" CssClass="form-control" Style="display: unset" runat="server" Font-Bold="false">
                                        <asp:ListItem Text="Unlimited" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                        <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                        <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                        <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                        <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                        <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                        <asp:ListItem Text="32" Value="32"></asp:ListItem>
                                        <asp:ListItem Text="33" Value="33"></asp:ListItem>
                                        <asp:ListItem Text="34" Value="34"></asp:ListItem>
                                        <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                        <asp:ListItem Text="36" Value="36"></asp:ListItem>
                                        <asp:ListItem Text="37" Value="37"></asp:ListItem>
                                        <asp:ListItem Text="38" Value="38"></asp:ListItem>
                                        <asp:ListItem Text="39" Value="39"></asp:ListItem>
                                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                        <asp:ListItem Text="41" Value="41"></asp:ListItem>
                                        <asp:ListItem Text="42" Value="42"></asp:ListItem>
                                        <asp:ListItem Text="43" Value="43"></asp:ListItem>
                                        <asp:ListItem Text="44" Value="44"></asp:ListItem>
                                        <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                        <asp:ListItem Text="46" Value="46"></asp:ListItem>
                                        <asp:ListItem Text="47" Value="47"></asp:ListItem>
                                        <asp:ListItem Text="48" Value="48"></asp:ListItem>
                                        <asp:ListItem Text="49" Value="49"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    </asp:DropDownList>
                            &nbsp;<b>uses</b>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="overlay" id="divCouponCodes" runat="server" style="display: none;">
            <div class="overlayContent" style="color: #000; background-color: aliceblue; width: 300px; border: 1px solid black; margin: 300px auto !important;">
                <table border="0" style="width: 100%;">
                    <tr>
                        <td style="text-align: center; height: 100px; overflow-y: scroll;">
                            <asp:Literal ID="litCouponCodes" runat="server"></asp:Literal><br />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <asp:Button ID="btnOk" Text="OK" runat="server" CssClass="btn btn-primary" OnClientClick="return hideChkCodeDiv();" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="overlay" id="divAuditTrail" runat="server" style="display: none;">
            <div class="overlayContent" style="color: #000; background-color: aliceblue; width: 500px; border: 1px solid black; margin: 100px auto !important;">
                <table style="width: 100%;" border="0">
                    <tr>
                        <td style="float: right; padding-top: 10px;">
                            <asp:Button ID="btnDelAuditrail" OnClick="btnDelAuditrail_Click" runat="server" Text="Delete" CssClass="btn btn-danger" />
                            <asp:Button ID="btnCloseAuditTrail" runat="server" Text="Close" CssClass="btn btn-primary" OnClientClick="return hideAuditTrailDiv();" />
                        </td>
                    </tr>
                    <tr>
                        <td style="overflow-y: scroll; height: 200px; vertical-align: top; padding-top: 10px;" colspan="2">
                            <asp:GridView ID="gvAuditTrail" CssClass="table table-bordered table-striped" OnRowDataBound="gvAuditTrail_RowDataBound" AutoGenerateColumns="false" Width="478px"
                                CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0" runat="server">
                                <Columns>
                                    <asp:BoundField DataField="vcDiscountCode" HeaderStyle-Width="25%" ItemStyle-HorizontalAlign="Center" HeaderText="Discount Code" />
                                    <asp:BoundField DataField="vcPOppName" HeaderStyle-Width="50%" ItemStyle-HorizontalAlign="Center" HeaderText="Sales Order" />
                                    <asp:BoundField DataField="HasBizDoc" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Has BizDoc" />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAllTrails" onclick="SelectAll('chkAllTrails','chk')" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTrail" CssClass="chk" runat="server" />
                                            <asp:HiddenField ID="hdnDiscountId" Value='<%#Eval("numDiscountId")%>' runat="server" />
                                            <asp:HiddenField ID="hdnOppId" Value='<%#Eval("numOppID")%>' runat="server" />
                                            <asp:HiddenField ID="hdnDivisionId" Value='<%#Eval("numDivisionId")%>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="Left" />

                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
