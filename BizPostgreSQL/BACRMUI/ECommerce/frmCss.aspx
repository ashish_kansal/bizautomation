﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCss.aspx.vb" Inherits=".frmCss"
    ValidateRequest="false" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script src="../JavaScript/EditArea/edit_area_full.js" type="text/javascript"></script>
    <script type="text/JavaScript">

        function Save() {
            if (document.getElementById('txtStyleName').value == "") {
                alert("Enter " + document.getElementById("lblName1").innerHTML + " Name")
                document.getElementById('txtStyleName').focus()
                return false;
            }
            if (document.getElementById('fuCss').value == "") {
                alert("Select " + document.getElementById("lblName2").innerHTML + " to upload")
                document.getElementById('fuCss').focus()
                return false;
            }
        }
    </script>
    <style>
        .EditCss {
            font-family: arial;
            font-size: 10pt;
            color: Black;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnSaveOnly" runat="server" CssClass="btn btn-primary"
                                    OnClientClick="javascript:return Save();" ><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                &nbsp;
                             <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                                 &nbsp;
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                        &nbsp;
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSaveOnly" />
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>
                <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    New
    <asp:Label ID="lblName" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <asp:Label ID="lblName1" runat="server"></asp:Label>&nbsp;Name<font color="#ff0000">*</font>
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox ID="txtStyleName" runat="server" CssClass="form-control" style="max-width:200px;margin-bottom:10px"></asp:TextBox>
                                </div>
                                <div class="clearfix"></div>
                                <div id="trFileUpload" runat="server">
                                    <div class="col-md-3">
                                        Select a&nbsp;<asp:Label ID="lblName2" runat="server"></asp:Label>&nbsp;to upload
                                    </div>
                                    <div class="col-md-4">
                                        <asp:FileUpload ID="fuCss" runat="server" Style="max-width: 200px" CssClass="form-control" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div id="trEditCss" runat="server">
                                    <div class="col-md-3">
                                        <asp:Label ID="lblCssFileName" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox TextMode="MultiLine" ID="txtEditCss" runat="server" CssClass="form-control" Height="400"></asp:TextBox>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>

                </asp:UpdatePanel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnCssFileName" runat="server" />
           </div> 
</asp:Content>
