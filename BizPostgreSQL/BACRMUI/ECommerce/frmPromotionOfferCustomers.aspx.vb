﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Promotion

Public Class frmPromotionOfferCustomers
    Inherits BACRMPage

#Region "Member Variables"
    Dim objPromotionOffer As PromotionOffer
    Dim lngPromotionId As Long
    Dim StepNo, StepValue As Short
#End Region

#Region "Page Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngPromotionId = GetQueryStringVal("PromotionID")
            StepNo = GetQueryStringVal("Step")
            StepValue = GetQueryStringVal("StepValue")

            If Not IsPostBack Then
                If lngPromotionId > 0 Then
                    LoadDetails()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region
    Sub LoadDetails()
        Try
            objPromotionOffer = New PromotionOffer
            If StepNo = 1 Then
                Select Case StepValue
                    Case 1
                        pnlOrganizations.Visible = True
                        objPromotionOffer.RuleAppType = 1
                    Case 2
                        pnlRelProfiles.Visible = True
                        objPromotionOffer.RuleAppType = 2
                End Select
            End If
            objPromotionOffer.PromotionID = lngPromotionId
            objPromotionOffer.DomainID = Session("DomainID")

            Dim ds As DataSet
            ds = objPromotionOffer.GetPromotionOfferCustomers

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If objPromotionOffer.RuleAppType = 1 Then

                    dgOrg.DataSource = ds.Tables(0)
                    dgOrg.DataBind()
                ElseIf objPromotionOffer.RuleAppType = 2 Then

                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))

                    dgRelationship.DataSource = ds.Tables(0)
                    dgRelationship.DataBind()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAddOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOrg.Click
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = lngPromotionId
                objPromotionOffer.RuleAppType = StepValue
                objPromotionOffer.DivisionID = radCmbCompany.SelectedValue
                objPromotionOffer.Mode = 0
                objPromotionOffer.ManagePromotionOfferCustomersDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE-LEVEL1" Then
                litMessage.Text = "You’ve already created a promotion rule that targets this customer and item by name."
            ElseIf ex.Message = "DUPLICATE-LEVEL2" Then
                litMessage.Text = "You’ve already created a promotion rule that targets this relationship / profile and one or more item specifically."
            ElseIf ex.Message = "DUPLICATE" Then
                litMessage.Text = "You’ve already created a promotion rule with this selection."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveOrg.Click
        Try
            objPromotionOffer = New PromotionOffer

            For Each Item As DataGridItem In dgOrg.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PromotionOrganisationID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.RuleAppType = StepValue
                    objPromotionOffer.ManagePromotionOfferCustomersDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportOrg.Click
        ExportToExcel.DataGridToExcel(dgOrg, Response)
    End Sub

    Private Sub btnAddRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRelProfile.Click
        Try
            If CCommon.ToLong(ddlRelationship.SelectedValue) > 0 And CCommon.ToLong(ddlProfile.SelectedValue) > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = lngPromotionId
                objPromotionOffer.RuleAppType = 2
                objPromotionOffer.Relationship = ddlRelationship.SelectedValue
                objPromotionOffer.Profile = ddlProfile.SelectedValue
                objPromotionOffer.Mode = 0
                objPromotionOffer.ManagePromotionOfferCustomersDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveRelProfile.Click
        Try
            objPromotionOffer = New PromotionOffer
            For Each Item As DataGridItem In dgRelationship.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PromotionOrganisationID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.ManagePromotionOfferCustomersDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportRelProfile.Click
        ExportToExcel.DataGridToExcel(dgRelationship, Response)
    End Sub
End Class