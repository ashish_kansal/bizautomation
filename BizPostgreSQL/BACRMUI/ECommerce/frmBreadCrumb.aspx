﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBreadCrumb.aspx.vb"
    Inherits=".frmBreadCrumb" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript" language="javascript">
        function Save() {
            if (document.getElementById('txtDisplay').value == "") {
                alert("Enter display name");
                document.getElementById('txtDisplay').focus();
                return false;
            }
            if (document.getElementById('txtLevel').value == "") {
                alert("Enter level");
                document.getElementById('txtLevel').focus();
                return false;
            }
            if (document.getElementById('ddlPages').selectedIndex == 0) {
                alert("Select a target page");
                document.getElementById('ddlPages').focus()
                return false;
            }
        }
    </script>
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSaveOnly" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                       
                        <asp:LinkButton ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                        
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" ><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close
                        </asp:LinkButton>
                <a href="#" class="help">&nbsp;</a>
            </div>
      </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblName" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-12">
                      
                        <div class="col-md-3">
                            <label>Display Name<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:TextBox ID="txtDisplay" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <label>Level<font color="#ff0000">*</font></label>
                    </div>
                    <div class="col-md-4 form-group">
                        <asp:TextBox ID="txtLevel" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <label>Target Page<font color="#ff0000">*</font></label>
                    </div>
                    <div class="col-md-4 form-group">
                        <asp:DropDownList ID="ddlPages" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                    </div>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
