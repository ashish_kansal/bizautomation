﻿Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class frmPackagingRuleList
    Inherits BACRMPage

    Dim objShippingRule As ShippingRule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objCommon = New CCommon
            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If

            If Not IsPostBack Then
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try

            If objShippingRule Is Nothing Then
                objShippingRule = New ShippingRule
            End If
            objShippingRule.RuleID = 0
            objShippingRule.byteMode = 1
            objShippingRule.DomainID = Session("DomainID")

            dgPackagingRuleList.DataSource = objShippingRule.GetPackagingRules()
            dgPackagingRuleList.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPackagingRuleList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPackagingRuleList.DeleteCommand
        Try
            objShippingRule = New ShippingRule
            objShippingRule.RuleID = e.Item.Cells(0).Text
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.DeletePackagingRules()
            LoadDetails()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub dgPackagingRuleList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPackagingRuleList.ItemCommand
        Try
            If e.CommandName = "RuleID" Then
                Response.Redirect("../ECommerce/frmPackagingRules.aspx?RuleID=" & e.Item.Cells(0).Text)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgPackagingRuleList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPackagingRuleList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class