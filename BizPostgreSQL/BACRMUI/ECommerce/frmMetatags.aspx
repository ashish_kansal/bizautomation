﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMetatags.aspx.vb" Inherits=".frmMetatags"
    MasterPageFile="~/common/GridMasterRegular.Master" ValidateRequest="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/JavaScript">
        function Save() {

            if ($find('radItem').get_value() == "") {
                alert("Select Item")
                return false;
            }
            if (document.getElementById("txtMetaTags").value == "") {
                alert("Enter Meta Tag")
                document.getElementById("txtMetaTags").focus()
                return false;
            }
        }
        function GetSelectedItem(combobox) {
            combobox.ClientDataString = '0~1'; //document.form1.txtDivID.value + '~' + 1;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                 <asp:LinkButton ID="btnUpdate" runat="server" CssClass="btn btn-primary" OnClientClick="javascript:return Save();">&nbsp;Update
                        </asp:LinkButton>
                 <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmMetatags.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Item</label>
                        </div>
                        <div class="col-md-4 form-group">
                            <telerik:RadComboBox ID="radItem" runat="server" Width="150" DropDownWidth="200px"
                                AutoPostBack="True" EnableScreenBoundaryDetection="true" EnableLoadOnDemand="true"
                                AllowCustomText="True" ClientIDMode="Static">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                            </telerik:RadComboBox>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>MetaTag HTML<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtMetaTags" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            e.g.<div style="color: Gray">
                                &lt;meta name="keywords" content="Enter a few keywords to describe your pages seperated
                                by commas. Like this-&gt; 8 gb, pen ,drive"&gt; &lt;meta name="description" content="Enter
                                a description for website and this page."&gt; &lt;meta name="robots" content="FOLLOW,INDEX"&gt;
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>

                </div>
               
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
