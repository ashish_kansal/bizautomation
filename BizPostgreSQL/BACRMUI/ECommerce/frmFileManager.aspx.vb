﻿Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports System.IO
Imports Telerik.Web.UI

Imports ImageResizer.Plugins.PrettyGifs
Imports ImageResizer

Partial Public Class frmFileManager
    Inherits BACRMPage
    Dim intMode As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            
            If GetQueryStringVal("Mode") <> "" Then
                intMode = CCommon.ToInteger(GetQueryStringVal("Mode"))
            End If
            If intMode = 1 Then
                'Add rights here 
                GetUserRightsForPage(37, 34)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS", False)
                End If
                FileExplorer2.Visible = False
                trSite.Visible = False
                trFolder.Visible = False
                lblFileUpload.Text = "Upload images of all items in trSite"
                lblNote.Visible = True
            Else
                GetUserRightsForPage(13, 41)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS", False)
                End If
            End If

            
            If Not IsPostBack Then
                
            End If
            If Not Session("SiteID") = Nothing Then
                If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                    FullPermissionsTest(Server.MapPath("~/" & ConfigurationManager.AppSettings("BizCartImageVirtualDirectory") & "/" & CCommon.ToString(Session("SiteID")) + "/"))
                End If

                Dim ViewPath() As String = {"~/" & ConfigurationManager.AppSettings("BizCartImageVirtualDirectory") & "/" & CCommon.ToString(Session("SiteID")) + "/"}
                FileExplorer2.Configuration.ViewPaths = ViewPath
                Dim UploadPath() As String = {"~/" & ConfigurationManager.AppSettings("BizCartImageVirtualDirectory") & "/" & CCommon.ToString(Session("SiteID")) + "/"}
                FileExplorer2.Configuration.UploadPaths = UploadPath
                Dim DeletePath() As String = {"~/" & ConfigurationManager.AppSettings("BizCartImageVirtualDirectory") & "/" & CCommon.ToString(Session("SiteID")) + "/"}
                FileExplorer2.Configuration.DeletePaths = DeletePath
                FileExplorer2.InitialPath = Page.ResolveUrl(ViewPath(0).ToString())
                FileExplorer2.Configuration.ContentProviderTypeName = GetType(ExtendedFileProvider).AssemblyQualifiedName

            End If
            If intMode = 0 Then
                If CCommon.ToInteger(Session("SiteID")) > 0 And ddlSiteFolders.Items.Count = 0 Then
                    DirList(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID"))
                End If
            End If
            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        Try
            If intMode = 0 Then
                If CCommon.ToLong(Session("SiteID")) = 0 Then
                    litMessage.Text = "Please select a site"
                    Exit Sub
                End If
            End If
            
            If fuZipUpload.HasFile Then
                If fuZipUpload.FileName.ToLower.Contains(".zip") Then
                    If txtHeight.Text <> "" Or txtWidth.Text <> "" Then
                        Dim objExport As New CSVExport
                        Dim strExtractPath As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\" & ddlSiteFolders.SelectedValue
                        Dim strArchiveFilePath As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\" & fuZipUpload.FileName

                        If intMode = 1 Then
                            strExtractPath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
                            strArchiveFilePath = strExtractPath & fuZipUpload.FileName
                        End If

                        fuZipUpload.PostedFile.SaveAs(strArchiveFilePath)
                        Dim strImages As ArrayList = New ArrayList()

                        strImages = objExport.ExtractFiles(strExtractPath, strArchiveFilePath, chkOverWrite.Checked)
                        Dim strMeasure As String

                        If txtHeight.Text <> "" Then
                            strMeasure = "Height=" & txtHeight.Text
                        ElseIf txtWidth.Text <> "" Then
                            strMeasure = "Width=" & txtWidth.Text
                        End If


                        For index As Integer = 0 To strImages.Count - 1
                            ImageResizer.ImageBuilder.Current.Build(strExtractPath & CCommon.ToString(strImages(index)), strExtractPath + "Thumb_" + CCommon.ToString(strImages(index)), New ResizeSettings(strMeasure))
                        Next

                        litMessage.Text = "Saved sucessfully."
                        Try
                            File.Delete(strArchiveFilePath)
                        Catch ex As Exception
                        End Try

                    Else
                        litMessage.Text = "Select Height or Width."

                    End If
                Else
                    litMessage.Text = "Only .zip files are allowed"
                End If
            Else
                litMessage.Text = "Please upload a .zip file"
            End If
           


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Dim item As ListItem
    Private Sub DirList(ByVal strPath As String)
        Try
            For Each d As String In Directory.GetDirectories(strPath)
                item = New ListItem
                item.Text = d.ToLower.Replace((ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\").ToString.ToLower, "")
                item.Value = item.Text
                ddlSiteFolders.Items.Add(item)
                DirList(d)
            Next
        Catch ex As System.Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Full permission test
    ''' </summary>
    ''' <param name="testPhysicalPath">Physical path</param>
    ''' <remarks></remarks>
    Private Sub FullPermissionsTest(ByVal testPhysicalPath As String)
        Try
            Dim physicalPathToTestFolder As String = System.IO.Path.Combine(testPhysicalPath, "TestDirectory")
            Dim testDir As System.IO.DirectoryInfo = System.IO.Directory.CreateDirectory(physicalPathToTestFolder) 'Create folder
            testDir.GetDirectories() ' List folders
            Dim testFilePath As String = System.IO.Path.Combine(testDir.FullName, "TestFile1.txt") ' test file paths
            System.IO.File.Create(testFilePath).Close() ' Create a file
            testDir.GetFiles("*.*") ' List files
            System.IO.File.OpenRead(testFilePath).Close() ' Open a file
            System.IO.File.Delete(testFilePath) ' delete the test file
            System.IO.Directory.Delete(physicalPathToTestFolder) ' delete the test folder
        Catch ex As Exception
            ' Show the probelm
            Dim message As String = ex.Message
            Dim script As String = String.Format("alert('{0}');", message.Replace("'", """"))
            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.[GetType](), "KEY", script, True)
        End Try
    End Sub

End Class
Public Class ExtendedFileProvider
    Inherits Telerik.Web.UI.Widgets.FileSystemContentProvider
    'constructor must be present when overriding a base content provider class
    'you can leave it empty
    Public Sub New(ByVal context As HttpContext, ByVal searchPatterns As String(), ByVal viewPaths As String(), ByVal uploadPaths As String(), ByVal deletePaths As String(), ByVal selectedUrl As String, _
     ByVal selectedItemTag As String)
        MyBase.New(context, searchPatterns, viewPaths, uploadPaths, deletePaths, selectedUrl, _
         selectedItemTag)
    End Sub
    Public Overloads Overrides Function ResolveDirectory(ByVal path As String) As Telerik.Web.UI.Widgets.DirectoryItem
        'get the directory information
        Dim baseDirectory As Telerik.Web.UI.Widgets.DirectoryItem = MyBase.ResolveDirectory(path)
        'remove files that we do not want to see
        Dim files As New System.Collections.Generic.List(Of Telerik.Web.UI.Widgets.FileItem)()
        For Each file As Telerik.Web.UI.Widgets.FileItem In baseDirectory.Files
            If Not (file.Name.ToLower.EndsWith(".aspx") Or file.Name.ToLower.EndsWith(".config") Or file.Name.ToLower.EndsWith(".asax")) Then
                files.Add(file)
            End If
        Next
        Dim newDirectory As New Telerik.Web.UI.Widgets.DirectoryItem(baseDirectory.Name, baseDirectory.Location, baseDirectory.FullPath, baseDirectory.Tag, baseDirectory.Permissions, files.ToArray(), _
         baseDirectory.Directories)
        'return the updated directory information
        Return newDirectory
    End Function
    'http://www.telerik.com/community/forums/aspnet-ajax/editor/onfileupload-handler.aspx
    Public Overloads Overrides Function StoreFile(ByVal file As Telerik.Web.UI.UploadedFile, ByVal path As String, ByVal name As String, ByVal ParamArray arguments As String()) As String
        Dim targetFullPath As String = System.IO.Path.Combine(path, name)
        If (name.ToLower.EndsWith(".aspx") Or name.ToLower.EndsWith(".ascx") Or name.ToLower.EndsWith(".ashx") Or name.ToLower.EndsWith(".config") Or name.ToLower.EndsWith(".asax")) Then
            Exit Function
        Else
            file.SaveAs(HttpContext.Current.Server.MapPath(targetFullPath))
        End If
        Return targetFullPath
    End Function
End Class