﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.WebAPI

Public Class frmAddCustomBox
    Inherits BACRMPage

#Region "OBJECT DECLARATION"

    Dim objShippingRule As ShippingRule

#End Region

#Region "GLOBAL DECLARATION"

    Dim lngPackageTypeID As Long
    Dim lngCustomPackageID As Long

#End Region

#Region "PAGE EVENTS"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")
            lngPackageTypeID = CCommon.ToLong(GetQueryStringVal("numPackageTypeID"))
            lngCustomPackageID = CCommon.ToLong(GetQueryStringVal("numCustomPackageID"))

            If Not Page.IsPostBack Then
               
                If lngPackageTypeID > 0 Then
                    hdnCustomPackageID.Value = lngPackageTypeID
                    btnSave.Visible = True
                    LoadDetails()
                    'trMain.Visible = True
                Else
                    'trMain.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "BUTTON EVENTS"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()

            If objShippingRule.ExceptionMsg <> "" Then
                litMessage.Text = objShippingRule.ExceptionMsg
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()

            If objShippingRule.ExceptionMsg <> "" Then
                litMessage.Text = objShippingRule.ExceptionMsg
            Else
                Response.Redirect("../ECommerce/frmCustomBoxList.aspx", False)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Commented by Sachin
            'Response.Redirect("../ECommerce/frmCustomBoxList.aspx")
            'end of code

            Response.Redirect("../ECommerce/frmShippingLabelBatchProcessingList.aspx?tabid=2", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "METHODS"

    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            objShippingRule = New ShippingRule()

            objShippingRule.PackageTypeID = lngPackageTypeID
            objShippingRule.DomainID = DomainID
            objShippingRule.byteMode = 0
            dtTable = objShippingRule.GetCustomPackages()
            If dtTable.Rows.Count > 0 Then
                txtBoxName.Text = dtTable.Rows(0).Item("vcPackageName")
                txtWidth.Text = CCommon.ToDecimal(dtTable.Rows(0).Item("fltWidth"))
                txtHeight.Text = CCommon.ToDecimal(dtTable.Rows(0).Item("fltHeight"))
                txtLength.Text = CCommon.ToDecimal(dtTable.Rows(0).Item("fltLength"))
                txtWeight.Text = CCommon.ToDecimal(dtTable.Rows(0).Item("fltTotalWeight"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Save()
        Try
            objShippingRule = New ShippingRule
            objShippingRule.CustomPackageID = lngCustomPackageID
            objShippingRule.PackageTypeID = lngPackageTypeID
            objShippingRule.PackageName = txtBoxName.Text
            objShippingRule.FltWidth = CCommon.ToDecimal(txtWidth.Text)
            objShippingRule.FltHeight = CCommon.ToDecimal(txtHeight.Text)
            objShippingRule.FltLength = CCommon.ToDecimal(txtLength.Text)
            objShippingRule.FltTotalWeight = CCommon.ToDecimal(txtWeight.Text)

            objShippingRule.DomainID = DomainID
            If objShippingRule.ManageCustomPackages() > 0 Then
                litMessage.Text = "Custom Box added successfully."
            Else
                litMessage.Text = "Error occurred while save adding Custom Box."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class