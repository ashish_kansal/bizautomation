﻿Imports BACRM.BusinessLogic.Promotion
Imports BACRM.BusinessLogic.Common

Public Class frmPromotionOrderBasedManage
    Inherits BACRMPage

#Region "Member Variables"
    Dim objPromotionOffer As PromotionOffer

    Shared strUsedCouponCodes As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If Not IsPostBack Then
                hfProId.Value = CCommon.ToLong(GetQueryStringVal("ProId"))
                Session("ProId") = hfProId.Value
                If CCommon.ToLong(hfProId.Value) > 0 Then
                    divValidity.Visible = True
                    divOrderPromotionDetail.Visible = True
                Else
                    divValidity.Visible = False
                    divOrderPromotionDetail.Visible = False
                    btnSaveClose.Visible = False
                End If

                If CCommon.ToLong(hfProId.Value) > 0 Then
                    LoadDetails()
                End If
                hplRelProfile.Attributes.Add("onclick", "return OpenConfig(1,2," & hfProId.Value & ")")
                divCouponCodes.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmPromotionOrderBasedManage.aspx?ProId=" & lngPromotionId, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmPromotionOfferList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmPromotionOfferList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub LoadDetails()
        Try
            objPromotionOffer = New PromotionOffer
            objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            Dim ds As DataSet = objPromotionOffer.GetPromotionOfferDetailsForOrder()
            Dim dtProDetails = ds.Tables(0)
            Dim dtDiscountDetails = ds.Tables(1)
            Dim DiscountType As String = dtProDetails.Rows(0).Item("tintDiscountType").ToString()

            If dtProDetails IsNot Nothing AndAlso dtProDetails.Rows.Count > 0 Then
                txtOfferName.Text = CCommon.ToString(dtProDetails.Rows(0).Item("vcProName"))

                chkNeverExpires.Checked = CCommon.ToBool(dtProDetails.Rows(0).Item("bitNeverExpires"))
                If Not chkNeverExpires.Checked Then
                    If Not dtProDetails.Rows(0).Item("dtValidFrom") Is DBNull.Value Then
                        calValidFromDate.SelectedDate = CCommon.ToSqlDate(dtProDetails.Rows(0).Item("dtValidFrom"))
                    End If

                    If Not dtProDetails.Rows(0).Item("dtValidTo") Is DBNull.Value Then
                        calValidToDate.SelectedDate = CCommon.ToSqlDate(dtProDetails.Rows(0).Item("dtValidTo"))
                    End If
                End If

                chkUseForCouponManagement.Checked = CCommon.ToBool(dtProDetails.Rows(0).Item("bitUseForCouponManagement"))
                chkbxCouponCode.Checked = CCommon.ToBool(dtProDetails.Rows(0).Item("bitRequireCouponCode"))
                ddlUsageLimit.SelectedValue = CCommon.ToLong(dtProDetails.Rows(0).Item("CodeUsageLimit"))
                txtCouponCode.Text = CCommon.ToString(dtProDetails.Rows(0).Item("vcDiscountCode"))

                If chkUseForCouponManagement.Checked Then
                    divDiscountRange.Style.Add("display", "none")
                End If

                DiscountType = dtProDetails.Rows(0).Item("tintDiscountType").ToString()
                If (DiscountType = "1" Or DiscountType = "") Then
                    lblDiscountType2.Text = "Percentage"
                    lblDiscountType3.Text = "Percentage"
                ElseIf (DiscountType = "2") Then
                    lblDiscountType2.Text = "Flat Amount"
                    lblDiscountType3.Text = "Flat Amount"
                End If
                ddlDiscountType1.SelectedValue = DiscountType

                If Not dtDiscountDetails Is Nothing AndAlso dtDiscountDetails.Rows.Count > 0 Then
                    Dim i As Integer = 1
                    For Each drrow As DataRow In dtDiscountDetails.Rows
                        If i = 1 Then
                            txtOrder1.Text = drrow("numOrderAmount").ToString()
                            txtDiscount1.Text = drrow("fltDiscountValue").ToString()
                        ElseIf i = 2 Then
                            txtOrder2.Text = drrow("numOrderAmount").ToString()
                            txtDiscount2.Text = drrow("fltDiscountValue").ToString()
                            chkboxDiscount2.Checked = True
                        ElseIf i = 3 Then
                            txtOrder3.Text = drrow("numOrderAmount").ToString()
                            txtDiscount3.Text = drrow("fltDiscountValue").ToString()
                            chkboxDiscount3.Checked = True
                        End If
                        i = i + 1
                    Next
                End If
            End If

            
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function Update(subTotal As Decimal, Optional byteMode As Short = 0, Optional oldSubTotal As Decimal = 0) As Long

    End Function

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As Exception)
        Try
            DirectCast(Page.Master, ECommerceMenuMaster).ThrowError(ex)
        Catch exp As Exception

        End Try
    End Sub

    Private Function GetCouponCodes() As DataSet
        Try
            Dim objPromotionOffer As New PromotionOffer
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.CouponCode = txtCouponCode.Text
            objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)

            Dim dsCouponCodes As DataSet

            dsCouponCodes = objPromotionOffer.GetDuplicateCouponCodes()
            Return dsCouponCodes

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Function

    Protected Sub lnkChkCodeAvailablity_Click(sender As Object, e As EventArgs)
        Try
            Dim dsCouponCodes As DataSet = GetCouponCodes()
            litCouponCodes.Text = ""

            If (dsCouponCodes IsNot Nothing And dsCouponCodes.Tables.Count > 0) Then
                If (dsCouponCodes.Tables(0).Rows.Count > 0) Then
                    For Each drrow As DataRow In dsCouponCodes.Tables(0).Rows
                        litCouponCodes.Text = litCouponCodes.Text & " " & "'" & drrow("vcDiscountCode").ToString & "'" & " already used on " & "'" & drrow("vcProName").ToString & "'" & " promotion" & "<br/>"
                    Next
                Else
                    litCouponCodes.Text = "Code is available"
                End If
            Else
                litCouponCodes.Text = "Code is available"
            End If
            divCouponCodes.Style.Add("display", "block")
            divAuditTrail.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function Save() As Long
        Try
            Dim errorMessage As String = ""
            Dim dtValidFrom As Date
            Dim dtValidTo As Date

            Dim objPromotionOffer As New PromotionOffer

            If CCommon.ToLong(hfProId.Value) > 0 Then
                If Not chkNeverExpires.Checked Then
                    If String.IsNullOrEmpty(calValidFromDate.SelectedDate.Trim()) Or String.IsNullOrEmpty(calValidToDate.SelectedDate.Trim()) Then
                        errorMessage = "Promotion From & To date is required. <br/>"
                    Else
                        If Not Date.TryParse(calValidFromDate.SelectedDate, dtValidFrom) Then
                            errorMessage = errorMessage & "Enter valid promotion From date. <br/>"
                        End If

                        If Not Date.TryParse(calValidToDate.SelectedDate, dtValidTo) Then
                            errorMessage = errorMessage & "Enter valid promotion To Date. <br/>"
                        End If

                        If dtValidTo < dtValidFrom Then
                            errorMessage = errorMessage & "To date must be greater than or equal to From date.<br/>"
                        End If
                    End If
                End If

                If chkbxCouponCode.Checked AndAlso String.IsNullOrEmpty(txtCouponCode.Text) Then
                    errorMessage = errorMessage & "Coupon code is required.<br/>"
                Else
                    Dim dsCouponCodes As DataSet = GetCouponCodes()
                    If (dsCouponCodes IsNot Nothing And dsCouponCodes.Tables.Count > 0) Then
                        If (dsCouponCodes.Tables(0).Rows.Count > 0) Then
                            For Each drrow As DataRow In dsCouponCodes.Tables(0).Rows
                                litCouponCodes.Text = litCouponCodes.Text & " " & "'" & drrow("txtCouponCode").ToString & "'" & " already used on " & "'" & drrow("vcProName").ToString & "'" & " promotion" & "<br/>"
                                divCouponCodes.Style.Add("display", "block")
                            Next
                        Else
                            divCouponCodes.Style.Add("display", "none")
                        End If
                    Else
                        divCouponCodes.Style.Add("display", "none")
                    End If
                End If

                If chkUseForCouponManagement.Checked AndAlso Not chkbxCouponCode.Checked Then
                    errorMessage = errorMessage & "Require Coupon code must be checked to use order promotion for coupon code management only.<br/>"
                End If

                objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
                Dim dtUsedCodes As DataTable = objPromotionOffer.GetUsedDiscountCodes()

                For Each dCode As DataRow In dtUsedCodes.Rows
                    If Not (txtCouponCode.Text.Contains(dCode("vcDiscountCode").ToString)) Then
                        errorMessage = errorMessage & " Coupon code " & dCode("vcDiscountCode").ToString & " is used on Order(s), can not exclude.(Click Audit Trail to Check)<br/>"
                        txtCouponCode.Text = txtCouponCode.Text + "," + dCode("vcDiscountCode").ToString
                    End If
                Next

                If (Not String.IsNullOrEmpty(txtDiscount2.Text)) And (Not String.IsNullOrEmpty(txtOrder2.Text)) AndAlso chkboxDiscount2.Checked = False Then
                    errorMessage = errorMessage & "Select checkbox to save 2nd Discount details <br/>"
                End If
                If (Not String.IsNullOrEmpty(txtDiscount3.Text)) And (Not String.IsNullOrEmpty(txtOrder3.Text)) AndAlso chkboxDiscount3.Checked = False Then
                    errorMessage = errorMessage & "Select checkbox to save 3rd Discount details <br/>"
                End If

                If (CCommon.ToShort(ddlDiscountType1.SelectedValue) = 1 AndAlso CCommon.ToInteger(txtDiscount1.Text) > 100) Or (lblDiscountType2.Text = "Percentage" AndAlso CCommon.ToInteger(txtDiscount2.Text) > 100) Or (lblDiscountType3.Text = "Percentage" AndAlso CCommon.ToInteger(txtDiscount3.Text) > 100) Then
                    errorMessage = errorMessage & "Discount percentage must be 100 or less.<br/>"
                End If

                If String.IsNullOrEmpty(errorMessage) Then
                    objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
                    objPromotionOffer.PromotionName = txtOfferName.Text

                    If chkNeverExpires.Checked Then
                        objPromotionOffer.IsNeverExpires = True
                        objPromotionOffer.ValidFrom = Nothing
                        objPromotionOffer.ValidTo = Nothing
                    Else
                        objPromotionOffer.ValidFrom = Convert.ToDateTime(dtValidFrom).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                        objPromotionOffer.ValidTo = Convert.ToDateTime(dtValidTo).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    End If

                    objPromotionOffer.IsUseForCouponManagement = chkUseForCouponManagement.Checked
                    objPromotionOffer.IsAppliesToInternalOrder = True 'chkInternalOrder.Checked
                    objPromotionOffer.IsAppliesToSite = True 'chkSites.Checked
                    objPromotionOffer.IsRequireCouponCode = chkbxCouponCode.Checked
                    objPromotionOffer.CouponCode = txtCouponCode.Text
                    objPromotionOffer.UsageLimit = ddlUsageLimit.SelectedValue

                    objPromotionOffer.PromotionOfferBasedOnCustomer = 2

                    Dim strOrderAmt As String

                    If Not (String.IsNullOrEmpty(txtOrder2.Text)) Then
                        strOrderAmt = txtOrder1.Text + "," + txtOrder2.Text
                    Else
                        strOrderAmt = txtOrder1.Text
                    End If
                    If Not (String.IsNullOrEmpty(txtOrder3.Text)) Then
                        strOrderAmt = txtOrder1.Text + "," + txtOrder2.Text + "," + txtOrder3.Text
                    End If
                    objPromotionOffer.OrderAmount = strOrderAmt

                    Dim strDiscount As String

                    If Not (String.IsNullOrEmpty(txtDiscount2.Text)) Then
                        strDiscount = txtDiscount1.Text + "," + txtDiscount2.Text
                    Else
                        strDiscount = txtDiscount1.Text
                    End If
                    If Not (String.IsNullOrEmpty(txtDiscount3.Text)) Then
                        strDiscount = txtDiscount1.Text + "," + txtDiscount2.Text + "," + txtDiscount3.Text
                    End If
                    objPromotionOffer.DiscountOnOrder = strDiscount
                    objPromotionOffer.DiscountType = ddlDiscountType1.SelectedValue

                    Return objPromotionOffer.InsertUpdatePromotionOfferForOrder()
                Else
                    ShowMessage(errorMessage)
                    Return 0
                End If
            Else
                If String.IsNullOrEmpty(txtOfferName.Text) Then
                    ShowMessage("Promotion name is required.")
                    txtOfferName.Focus()
                    Return 0
                Else
                    objPromotionOffer.PromotionName = txtOfferName.Text.Trim()
                    objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objPromotionOffer.ValidFrom = Nothing
                    objPromotionOffer.ValidTo = Nothing
                    Return objPromotionOffer.InsertUpdatePromotionOfferForOrder()
                End If
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE_COUPON_CODE" Then
                ShowMessage("A Coupon Code already exists.")
            ElseIf ex.Message = "RELATIONSHIPPROFILE ALREADY EXISTS" Then
                ShowMessage("Rule for selected Relationship N Profile already exists.")
            ElseIf ex.Message = "SELECT_SITES" Then
                ShowMessage("If you select Apply rule to these sites option then you have to select at least one site.")
            ElseIf ex.Message = "SELECT_ITEMS_FOR_PROMOTIONS" Then
                ShowMessage("Select items to execute promotion.")
            ElseIf ex.Message = "SELECT_DISCOUNTED_ITEMS" Then
                ShowMessage("Select discounted items.")
            ElseIf ex.Message = "DUPLICATE-LEVEL1" Then
                ShowMessage("You've already created a promotion rule that targets this customer and item by name.")
            ElseIf ex.Message = "DUPLICATE-LEVEL2" Then
                ShowMessage("You’ve already created a promotion rule that targets this relationship / profile and one or more item specifically.")
            ElseIf ex.Message = "DUPLICATE-LEVEL3" Then
                ShowMessage("This option can only be used for a single promotion rule, and there already is one or more promotion rules in the system.")
            ElseIf ex.Message = "SELECT_INDIVIDUAL_CUSTOMER" Then
                ShowMessage("Select individual customer to execute promotion.")
            ElseIf ex.Message = "SELECT_CUSTOMERS" Then
                ShowMessage("Select customers to execute promotion.")
            ElseIf ex.Message = "SELECT_RELATIONSHIP_PROFILE" Then
                ShowMessage("A relationship and minimum of one profile must be selected.")
            ElseIf ex.Message.Contains("ORGANIZATION-ITEM") Then
                ShowMessage("You've already created a promotion rule that targets this customer and item by name.")
            ElseIf ex.Message.Contains("ORGANIZATION-ITEMCLASSIFICATIONS") Then
                ShowMessage("You've already created a promotion rule that targets this customer and item classifications.")
            ElseIf ex.Message.Contains("ORGANIZATION-ALLITEMS") Then
                ShowMessage("You've already created a promotion rule that targets this customer and all items.")
            ElseIf ex.Message.Contains("RELATIONSHIPPROFILE-ITEMS") Then
                ShowMessage("You can’t select this profile because it’s already in use by another promotion rule.")
            ElseIf ex.Message.Contains("RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS") Then
                ShowMessage("You can’t select this profile because it’s already in use by another promotion rule.")
            ElseIf ex.Message.Contains("RELATIONSHIPPROFILE-ALLITEMS") Then
                ShowMessage("You can’t select this profile because it’s already in use by another promotion rule.")
            ElseIf ex.Message.Contains("ALLORGANIZATIONS-ITEMS") Then
                ShowMessage("You've already created a promotion rule that targets all customers and item by name.")
            ElseIf ex.Message.Contains("ALLORGANIZATIONS-ITEMCLASSIFICATIONS") Then
                ShowMessage("You've already created a promotion rule that targets all customers and item classifications.")
            ElseIf ex.Message.Contains("ALLORGANIZATIONS-ALLITEMS") Then
                ShowMessage("You've already created a promotion rule that targets all customers and items.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End If
        End Try
    End Function

    Protected Sub lnkAuditTrail_Click(sender As Object, e As EventArgs)
        Try
            Dim objPromotionOffer As New PromotionOffer
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
            objPromotionOffer.numItemCode = IIf(CCommon.ToLong(Session("DiscountServiceItem")) > 0, CCommon.ToLong(Session("DiscountServiceItem")), 0)

            Dim dsAuditTrail As DataSet = objPromotionOffer.GetAuditTrailDetailsForOrderPromotion()

            If Not dsAuditTrail Is Nothing And dsAuditTrail.Tables.Count > 0 Then
                divAuditTrail.Style.Add("display", "block")

                If dsAuditTrail.Tables(0).Rows.Count > 0 Then
                    gvAuditTrail.DataSource = dsAuditTrail.Tables(0)
                    gvAuditTrail.DataBind()
                End If
            Else
                divAuditTrail.Style.Add("display", "none")
            End If
            divCouponCodes.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub gvAuditTrail_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cellBizDoc As TableCell = e.Row.Cells(2)
                Dim HasBizDoc As String = CCommon.ToString(cellBizDoc.Text)

                If (HasBizDoc.ToUpper() = "YES") Then
                    e.Row.Cells(3).Visible = False
                Else
                    e.Row.Cells(3).Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Protected Sub btnDelAuditrail_Click(sender As Object, e As EventArgs)
        Try
            For Each drrow As GridViewRow In gvAuditTrail.Rows

                Dim chkTrail As CheckBox = CType(drrow.FindControl("chkTrail"), CheckBox)
                Dim hdnDiscountId As HiddenField = CType(drrow.FindControl("hdnDiscountId"), HiddenField)
                Dim hdnOppId As HiddenField = CType(drrow.FindControl("hdnOppId"), HiddenField)
                Dim hdnDivisionId As HiddenField = CType(drrow.FindControl("hdnDivisionId"), HiddenField)

                If chkTrail.Visible = True And chkTrail.Checked = True Then

                    Dim numOppId As Long = CCommon.ToLong(hdnOppId.Value)
                    Dim numDivisionId As Long = CCommon.ToLong(hdnDivisionId.Value)

                    Dim objPromotionOffer As New PromotionOffer
                    objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
                    objPromotionOffer.numDiscountId = hdnDiscountId.Value

                    objPromotionOffer.DeleteDiscountCodeForOrderPromotion(numOppId, numDivisionId)

                End If
            Next
            divAuditTrail.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class