﻿Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Common

Public Class frmPromotionOfferContacts
    Inherits BACRMPage

    Dim objPromotionOffer As PromotionOffer
    Dim lngProID As Long
    Dim StepValue As Short
    Dim shrtRecordType As Short
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngProID = GetQueryStringVal("ProID")
            StepValue = GetQueryStringVal("StepValue")
            shrtRecordType = GetQueryStringVal("RecordType")
            If Not IsPostBack Then

                If lngProID > 0 Then
                    LoadDetails()
                End If
                If shrtRecordType = 2 Then
                    btnRemoveOrg.Text = "Remove from Shipping Rule"
                    btnRemoveRelProfile.Text = "Remove from Shipping Rule"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            objPromotionOffer = New PromotionOffer

            Select Case StepValue
                Case 1
                    pnlOrganizations.Visible = True
                Case 2
                    pnlRelProfiles.Visible = True
            End Select

            objPromotionOffer.RuleAppType = StepValue

            objPromotionOffer.POType = 2
            objPromotionOffer.ProID = lngProID
            objPromotionOffer.DomainID = Session("DomainID")
            objPromotionOffer.SiteID = Session("SiteID")
            objPromotionOffer.RecordType = shrtRecordType
            Dim ds As DataSet

            ds = objPromotionOffer.GetPromotionOfferDtl

            If objPromotionOffer.RuleAppType = 1 Then
                dgOrg.DataSource = ds.Tables(0)
                dgOrg.DataBind()
            ElseIf objPromotionOffer.RuleAppType = 2 Then
                
                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))

                dgRelationship.DataSource = ds.Tables(0)
                dgRelationship.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAddOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOrg.Click
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.ProID = lngProID

                objPromotionOffer.RuleAppType = StepValue
                objPromotionOffer.POType = 2

                objPromotionOffer.POValue = radCmbCompany.SelectedValue
                objPromotionOffer.byteMode = 0
                objPromotionOffer.RecordType = shrtRecordType
                objPromotionOffer.ManagePromotionOfferDTL()

                LoadDetails()
            End If
        Catch ex As Exception
           
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemoveOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveOrg.Click
        Try
            objPromotionOffer = New PromotionOffer
            For Each Item As DataGridItem In dgOrg.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.byteMode = 1
                    objPromotionOffer.POType = 2
                    objPromotionOffer.RecordType = shrtRecordType
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnAddRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRelProfile.Click
        Try
            If CCommon.ToLong(ddlRelationship.SelectedValue) > 0 And CCommon.ToLong(ddlProfile.SelectedValue) > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.ProID = lngProID

                objPromotionOffer.RuleAppType = StepValue
                objPromotionOffer.POType = 2

                objPromotionOffer.POValue = ddlRelationship.SelectedValue
                objPromotionOffer.Profile = ddlProfile.SelectedValue
                objPromotionOffer.byteMode = 0
                objPromotionOffer.RecordType = shrtRecordType
                objPromotionOffer.ManagePromotionOfferDTL()
                LoadDetails()
            End If
        Catch ex As Exception
          
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemoveRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveRelProfile.Click
        Try
            objPromotionOffer = New PromotionOffer
            For Each Item As DataGridItem In dgRelationship.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.byteMode = 1
                    objPromotionOffer.POType = 2
                    objPromotionOffer.RecordType = shrtRecordType
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class