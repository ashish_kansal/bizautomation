﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSitePageList.aspx.vb"
    Inherits=".frmSitePageList" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function NewSitePage() {
            if (document.getElementById('ddlSites').selectedIndex == 0) {
                var hplNew = document.getElementById("hplNew");
                hplNew.href = "#";
                alert("Please select a site to upload " + hplNew.innerHTML);
                document.getElementById('ddlSites').focus();
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    
    <div class="row">
            <div class="col-md-12">
            <div id="Table1" runat="server" class="pull-right">
                <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
                        &nbsp;
                        <asp:HyperLink ID="hplNew" runat="server" CssClass="hyperlink" NavigateUrl="~/ECommerce/frmSitePage.aspx">New Page</asp:HyperLink>
                 <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Site Pages&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmSitePageList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
&nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgPages" AllowSorting="false" runat="server" Width="100%" CssClass="table table-striped table-bordered"
                    AutoGenerateColumns="False" UseAccessibleHeader="true" >
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numPageID" HeaderText="numPageID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcPageName" SortExpression="" HeaderText="<font>Page Name</font>"
                            CommandName="Edit"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcPageTitle" SortExpression="" HeaderText="<font>Title</font>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="<font>Template</font>">
                            <ItemTemplate>
                                <a href="frmTemplate.aspx?TemplateID=<%# Eval("numTemplateID") %>&frm=SitePage">
                                    <%#Eval("vcTemplateName")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="140" FooterStyle-Width="140" ItemStyle-Width="140">
                            <ItemTemplate>
                                <asp:Button ID="btnPublish" runat="server" CssClass="btn btn-primary" Text="Publish"
                                    CommandName="Publish" CommandArgument='<%# Eval("numPageID") %>'></asp:Button>
                                &nbsp; <a target="_blank" href="<%# "http://" & CCommon.ToString(Session("SitePreviewURL")).Replace("http://","") + "/" + Eval("vcPageURL") %>">
                                    Preview</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnIsActive" runat="server" CommandName="Status" CommandArgument='<%# Eval("bitIsActive") %>'
                                    Text='<%# Eval("Status") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    Visible='<%# Boolean.Parse(IIf(Eval("tintPageType")="1","false","true")) %>'>
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
