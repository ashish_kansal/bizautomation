﻿Imports System.Reflection
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Promotion

Public Class frmPromotionItems
    Inherits BACRMPage

#Region "Member Variables"
    Private objPromotionOffer As PromotionOffer
    Dim lngItemCode As Long
#End Region

#Region "Constructor"
    Sub New()
        objPromotionOffer = New PromotionOffer()
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnPromotionID.Value = CCommon.ToLong(GetQueryStringVal("promotionID"))
                hdnRecordType.Value = CCommon.ToShort(GetQueryStringVal("recordType"))
                hdnItemSelectionType.Value = CCommon.ToShort(GetQueryStringVal("itemSelectionType"))

                If hdnItemSelectionType.Value = "1" Then
                    pnlItems.Visible = True
                    lblTitle.Text = "Select Items"
                ElseIf hdnItemSelectionType.Value = "4" Then
                    pnlItems.Visible = True
                    lblTitle.Text = "Select Items"
                ElseIf hdnItemSelectionType.Value = "2" Then
                    pnlItemClassification.Visible = True
                    lblTitle.Text = "Select Item Classifications"
                    BindItemClassification()
                ElseIf hdnItemSelectionType.Value = "3" Then
                    pnlItemCustomFields.Visible = True
                    lblTitle.Text = "Select custom fields"
                    DisplayDynamicFlds()
                End If

                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub LoadDetails()
        Try
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.PromotionID = CCommon.ToLong(hdnPromotionID.Value)
            Dim ds As DataSet = objPromotionOffer.GetPromotionOfferItems(CCommon.ToShort(hdnRecordType.Value), CCommon.ToShort(hdnItemSelectionType.Value))

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If hdnItemSelectionType.Value = "1" Or hdnItemSelectionType.Value = "4" Then
                    gvItems.DataSource = ds.Tables(0)
                    gvItems.DataBind()
                ElseIf hdnItemSelectionType.Value = "2" Then
                    gvItemClassification.DataSource = ds.Tables(0)
                    gvItemClassification.DataBind()
                End If

                ViewState("DataTable") = ds.Tables(0)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindItemClassification()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlItemClassification, 36, Session("DomainID"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Sub DisplayDynamicFlds()
        Try
            Dim strDate As String
            Dim bizCalendar As UserControl
            Dim _myUC_DueDate As PropertyInfo
            Dim PreviousRowID As Integer = 0
            Dim objRow As HtmlTableRow
            Dim objCell As HtmlTableCell
            Dim i, k As Integer
            Dim dtTable As DataTable
            ' Tabstrip3.Items.Clear()
            Dim ObjCus As New CustomFields
            ObjCus.locId = 5
            ObjCus.RecordId = lngItemCode
            ObjCus.DomainID = Session("DomainID")
            dtTable = ObjCus.GetCustFlds.Tables(0)
            Session("CusFields") = dtTable

            If dtTable.Rows.Count > 0 Then
                'Main Detail Section
                k = 0
                objRow = New HtmlTableRow
                For i = 0 To dtTable.Rows.Count - 1
                    If dtTable.Rows(i).Item("TabId") = 0 Then
                        If k = 3 Then
                            k = 0
                            rblCustom.Rows.Add(objRow)
                            objRow = New HtmlTableRow
                        End If

                        objCell = New HtmlTableCell
                        objCell.Align = "Right"
                        objCell.Attributes.Add("class", "normal1")
                        objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " : "
                        objCell.Width = "8%"
                        objRow.Cells.Add(objCell)
                        If dtTable.Rows(i).Item("fld_type") = "TextBox" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "SelectBox" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBox" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "TextArea" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "DateField" Then
                            PreviousRowID = i
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            bizCalendar = LoadControl("../include/calandar.ascx")
                            bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                            objCell.Controls.Add(bizCalendar)
                            objRow.Cells.Add(objCell)
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngItemCode, dtTable.Rows(i).Item("fld_label"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBoxList" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateCheckBoxList(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CCommon.ToString(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                        End If
                        k = k + 1
                    End If

                Next
                Dim intRowCount As Integer = 0
                If k <> 3 Then
                    For intRowCount = 0 To 2 - k
                        '' objRow = New HtmlTableRow
                        objCell = New HtmlTableCell
                        objCell.Width = "8%"
                        objRow.Cells.Add(objCell)
                        objCell = New HtmlTableCell
                        objCell.Width = "20%"
                        objRow.Cells.Add(objCell)
                        objCell.Align = "left"
                    Next
                End If
                rblCustom.Rows.Add(objRow)

            End If
            Dim dvCusFields As DataView
            dvCusFields = dtTable.DefaultView
            dvCusFields.RowFilter = "fld_type='DateField'"
            Dim iViewCount As Integer
            For iViewCount = 0 To dvCusFields.Count - 1
                If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                    bizCalendar = pnlItemCustomFields.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                    Dim _myControlType As Type = bizCalendar.GetType()
                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                    strDate = dvCusFields(iViewCount).Item("Value")
                    If strDate = "0" Then strDate = ""
                    If strDate <> "" Then
                        'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCusField()
        Try
            If Not Session("CusFields") Is Nothing Then
                Dim dtTable As DataTable
                Dim i As Integer
                dtTable = Session("CusFields")
                For i = 0 To dtTable.Rows.Count - 1
                    If dtTable.Rows(i).Item("fld_type") = "TextBox" Then
                        Dim txt As TextBox
                        txt = pnlItemCustomFields.FindControl(dtTable.Rows(i).Item("fld_id"))
                        dtTable.Rows(i).Item("Value") = txt.Text
                    ElseIf dtTable.Rows(i).Item("fld_type") = "SelectBox" Then
                        Dim ddl As DropDownList
                        ddl = pnlItemCustomFields.FindControl(dtTable.Rows(i).Item("fld_id"))
                        dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                    ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBox" Then
                        Dim chk As CheckBox
                        chk = pnlItemCustomFields.FindControl(dtTable.Rows(i).Item("fld_id"))
                        If chk.Checked = True Then
                            dtTable.Rows(i).Item("Value") = "1"
                        Else : dtTable.Rows(i).Item("Value") = "0"
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "TextArea" Then
                        Dim txt As TextBox
                        txt = pnlItemCustomFields.FindControl(dtTable.Rows(i).Item("fld_id"))
                        dtTable.Rows(i).Item("Value") = txt.Text
                    ElseIf dtTable.Rows(i).Item("fld_type") = "DateField" Then
                        Dim BizCalendar As UserControl
                        BizCalendar = pnlItemCustomFields.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))

                        Dim strDueDate As String
                        Dim _myControlType As Type = BizCalendar.GetType()
                        Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                        strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                        If strDueDate <> "" Then
                            dtTable.Rows(i).Item("Value") = strDueDate
                        Else : dtTable.Rows(i).Item("Value") = ""
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBoxList" Then
                        Dim chkbl As CheckBoxList
                        chkbl = CType(pnlItemCustomFields.FindControl(dtTable.Rows(i).Item("fld_id")), CheckBoxList)
                        If Not chkbl Is Nothing Then
                            Dim selectedValue As String = ""

                            For Each listItem As ListItem In chkbl.Items
                                If listItem.Selected Then
                                    selectedValue = selectedValue & CCommon.ToString(IIf(String.IsNullOrEmpty(selectedValue), listItem.Value, "," & listItem.Value))
                                End If
                            Next

                            dtTable.Rows(i).Item("Value") = selectedValue
                        End If

                    End If
                Next

                Dim ds As New DataSet
                Dim strdetails As String
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable.Copy)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim ObjCusfld As New CustomFields
                ObjCusfld.strDetails = strdetails
                ObjCusfld.locId = 5
                ObjCusfld.RecordId = lngItemCode
                ObjCusfld.SaveCustomFldsByRecId()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region


#Region "Event Handlers"

    Protected Sub btnAddItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddItem.Click
        Try
            If hdnSelectedItems.Value <> "" Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = CCommon.ToLong(hdnPromotionID.Value)
                objPromotionOffer.RuleAppType = CCommon.ToShort(GetQueryStringVal("itemSelectionType"))
                objPromotionOffer.POValue = CCommon.ToLong(hdnSelectedItems.Value)
                objPromotionOffer.Mode = 0
                objPromotionOffer.RecordType = CCommon.ToShort(GetQueryStringVal("recordType"))
                objPromotionOffer.ManagePromotionOfferDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message.Contains("ANOTHER_RULE_ALREADY_EXISTS_FOR_SAME_ITEM_OR_CLASSIFICATION_SELECTION") Then
                lblMessage.Text = "Promotion rule with same individual item already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemItem.Click
        Try
            objPromotionOffer = New PromotionOffer

            For Each row As GridViewRow In gvItems.Rows
                If CType(row.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(DirectCast(row.FindControl("hdnProItemId"), HiddenField).Value)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.RuleAppType = CCommon.ToShort(GetQueryStringVal("itemSelectionType"))
                    objPromotionOffer.RecordType = CCommon.ToShort(GetQueryStringVal("recordType"))
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddItemClassification_Click(sender As Object, e As EventArgs) Handles btnAddItemClassification.Click
        Try
            If ddlItemClassification.SelectedValue > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = CCommon.ToLong(hdnPromotionID.Value)
                objPromotionOffer.RuleAppType = CCommon.ToShort(GetQueryStringVal("itemSelectionType"))
                objPromotionOffer.POValue = ddlItemClassification.SelectedValue
                objPromotionOffer.Mode = 0
                objPromotionOffer.RecordType = CCommon.ToShort(GetQueryStringVal("recordType"))
                objPromotionOffer.ManagePromotionOfferDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message.Contains("ANOTHER_RULE_ALREADY_EXISTS_FOR_SAME_ITEM_OR_CLASSIFICATION_SELECTION") Then
                lblMessage.Text = "Promotion rule with same item classification already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveItemClassification_Click(sender As Object, e As EventArgs) Handles btnRemoveItemClassification.Click
        Try
            objPromotionOffer = New PromotionOffer

            For Each row As GridViewRow In gvItemClassification.Rows
                If CType(row.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(DirectCast(row.FindControl("hdnProItemId"), HiddenField).Value)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.RuleAppType = CCommon.ToShort(GetQueryStringVal("itemSelectionType"))
                    objPromotionOffer.RecordType = CCommon.ToShort(GetQueryStringVal("recordType"))
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next

            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnAddCustomFields_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddCustomFields.Click
        Try
            SaveCusField()
        Catch ex As Exception
            If ex.Message.Contains("ANOTHER_RULE_ALREADY_EXISTS_FOR_SAME_ITEM_OR_CLASSIFICATION_SELECTION") Then
                lblMessage.Text = "Promotion rule with same individual item already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub
#End Region

End Class