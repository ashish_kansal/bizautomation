﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Imports BACRM.BusinessLogic.Documents
'Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports

Partial Public Class frmPageElementDetails
    Inherits BACRMPage
    Dim lngElementID As Long
    Dim bitCustomization As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If GetQueryStringVal("ElementID") <> "" Then
                lngElementID = GetQueryStringVal("ElementID")
            End If

            If GetQueryStringVal("Customize") <> "" Then
                bitCustomization = CCommon.ToBool(GetQueryStringVal("Customize"))
            End If

            If GetQueryStringVal("ElementName") <> "" Then
                lblElementName.Text = GetQueryStringVal("ElementName")
            End If
            If Not IsPostBack Then
                If bitCustomization Then
                    trHTMLEditor.Visible = True
                Else
                    trHTMLEditor.Visible = False
                End If

                BindData()
                'Commented by chintan not reqired
                'If (lngElementID = 3) Then
                '    hplSettings.Attributes.Add("onclick", "return OpenSetting('32','" & Session("SiteID") & " ')")
                '    hplSettings.Visible = True
                'Else
                'If (lngElementID = 36) Then
                '    hplSettings.Attributes.Add("onclick", "return ShowLayout('B','" & Session("SiteID") & " ','37')")
                '    hplSettings.Visible = True
                'End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindData()
        Dim objSite As New Sites
        Dim dtAttributes As DataTable
        Dim tbl As New Table
        Dim tr As TableRow
        Dim tdLeft, tdRigth As TableCell
        Dim ddl As DropDownList
        Dim chk As CheckBox

        Try

            objSite.ElementID = lngElementID
            objSite.SiteID = Session("SiteID")
            dtAttributes = objSite.GetPageElementAttributes()

            For Each dr As DataRow In dtAttributes.Rows
                tr = New TableRow

                Dim lbl As New Label
                lbl.CssClass = "text"
                lbl.Text = dr("vcAttributeName") + "&nbsp;"

                tdLeft = New TableCell
                tdLeft.HorizontalAlign = HorizontalAlign.Right
                tdLeft.Controls.Add(lbl)
                tr.Cells.Add(tdLeft)

                Select Case dr("vcControlType").ToString
                    Case "TextBox"

                        Dim txtBox As New TextBox
                        txtBox.CssClass = "form-control"
                        txtBox.ID = dr("vcAttributeName").ToString.Replace(" ", "_")

                        If dr("vcAttributeValue").ToString() = "" Then
                            txtBox.Text = Sites.ToString(dr("vcControlValues"))
                        Else
                            txtBox.Text = HttpUtility.HtmlDecode(dr("vcAttributeValue").ToString())
                        End If

                        tdRigth = New TableCell
                        tdRigth.HorizontalAlign = HorizontalAlign.Left
                        tdRigth.Controls.Add(txtBox)
                        tr.Cells.Add(tdRigth)
                        tbl.Rows.Add(tr)

                    Case "DropDown"

                        ddl = New DropDownList
                        ddl.CssClass = "form-control"

                        ddl.Width = 200
                        ddl.ID = dr("vcAttributeName").ToString.Replace(" ", "_")
                        ddl.DataSource = dr("vcControlValues").ToString.Split(New String() {"|"}, StringSplitOptions.RemoveEmptyEntries).ToArray
                        ddl.DataBind()
                        'ddl.Items.Insert(0, "-- Select One --")
                        'ddl.Items.FindByText("-- Select One --").Value = "0"

                        If Not ddl.Items.FindByValue(dr("vcAttributeValue").ToString()) Is Nothing Then
                            ddl.Items.FindByValue(dr("vcAttributeValue").ToString()).Selected = True
                        End If

                        tdRigth = New TableCell
                        tdRigth.HorizontalAlign = HorizontalAlign.Left
                        tdRigth.Controls.Add(ddl)
                        tr.Cells.Add(tdRigth)
                        tbl.Rows.Add(tr)

                    Case "CheckBox"

                        chk = New CheckBox
                        chk.Checked = Sites.ToBool(dr("vcAttributeValue"))
                        chk.ID = dr("vcAttributeName").ToString.Replace(" ", "_")

                        tdRigth = New TableCell
                        tdRigth.HorizontalAlign = HorizontalAlign.Left
                        tdRigth.Controls.Add(chk)
                        tr.Cells.Add(tdRigth)
                        tbl.Rows.Add(tr)

                    Case "HtmlEditor"
                        If CCommon.ToString(dr("vcAttributeValue")).Length <= 0 Then
                            Dim strContents As String = Sites.ReadFile(ConfigurationManager.AppSettings("CartLocation").ToString() & "\Default\Elements\" & lblElementName.Text.Trim & ".htm")
                            txtEditHTML.Text = Server.HtmlDecode(strContents)
                        Else
                            txtEditHTML.Text = HttpUtility.HtmlDecode(CCommon.ToString(dr("vcAttributeValue")))
                        End If

                        Dim objDocuments As New DocumentList
                        objDocuments.ModuleType = 1
                        objDocuments.BindEmailTemplateMergeFields(Nothing, lngElementID, ddlMergeField)

                        If lngElementID = 2 Or lngElementID = 3 Or lngElementID = 6 Or lngElementID = 36 Then
                            BindCustomField()
                        Else
                            ddlCustomField.Visible = False
                            ddlAttributes.Visible = False
                        End If
                    Case "ReportDropDown"
                        ddl = New DropDownList
                        ddl.ID = "ddlReport"
                        ddl.CssClass = "form-control"
                        ddl.Width = 200
                        Dim objReports As New CustomReports
                        objReports.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objReports.DomainID = CCommon.ToLong(Session("DomainID"))
                        Dim dt As DataTable = objReports.getCustomReportUser()
                        ddl.DataTextField = "vcReportName"
                        ddl.DataValueField = "numReportId"
                        ddl.DataSource = dt
                        ddl.DataBind()
                        If Not ddl.Items.FindByValue(dr("vcAttributeValue").ToString()) Is Nothing Then
                            ddl.Items.FindByValue(dr("vcAttributeValue").ToString()).Selected = True
                        End If
                        tdRigth = New TableCell
                        tdRigth.HorizontalAlign = HorizontalAlign.Left
                        tdRigth.Controls.Add(ddl)
                        tr.Cells.Add(tdRigth)
                        tbl.Rows.Add(tr)

                End Select
            Next
            phAttributes.Controls.Add(tbl)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCustomField()
        Try
            Dim dtCustFld As DataTable

            If lngElementID = 2 Or lngElementID = 3 Then

                Dim objFormWizard As New FormConfigWizard

                objFormWizard.DomainID = Session("DomainID")
                objFormWizard.LocationIds = "5"

                dtCustFld = objFormWizard.GetCustomFormFields()
                For Each row As DataRow In dtCustFld.Rows
                    ddlCustomField.Items.Add(New ListItem(row("vcFormFieldName"), "##" & row("vcFormFieldName").ToString.Trim.Replace(" ", "") & "_" & row("vcFieldType") & "##"))
                Next

                objFormWizard.DomainID = Session("DomainID")
                objFormWizard.LocationIds = "9"

                dtCustFld = objFormWizard.GetCustomFormFields()
                For Each row As DataRow In dtCustFld.Rows
                    ddlAttributes.Items.Add(New ListItem(row("vcFormFieldName"), "##" & row("vcFormFieldName").ToString.Trim.Replace(" ", "") & "_" & row("vcFieldType") & "##"))
                Next
                ddlAttributes.Items.Insert(0, New System.Web.UI.WebControls.ListItem("--Item Attributes--", ""))
            ElseIf lngElementID = 6 Then
                Dim objFormWizard As New FormConfigWizard

                objFormWizard.DomainID = Session("DomainID")

                objFormWizard.LocationIds = "1,4" '1=Lead/Prospect/Account , 4 =Contact Details

                dtCustFld = objFormWizard.GetCustomFormFields()
                For Each row As DataRow In dtCustFld.Rows
                    ddlCustomField.Items.Add(New ListItem(row("vcFormFieldName"), "##" & row("vcFormFieldName").ToString.Trim.Replace(" ", "") & "_" & row("vcFieldType") & "##"))
                Next
            ElseIf lngElementID = 36 Then
                Dim objFormWizard As New FormConfigWizard

                objFormWizard.DomainID = Session("DomainID")

                objFormWizard.LocationIds = "2" 'Sales Opportunity

                dtCustFld = objFormWizard.GetCustomFormFields()
                For Each row As DataRow In dtCustFld.Rows
                    ddlCustomField.Items.Add(New ListItem(row("vcFormFieldName"), "##" & row("vcFormFieldName").ToString.Trim.Replace(" ", "") & "_" & row("vcFieldType") & "##"))
                Next
            End If
            ddlCustomField.Items.Insert(0, New System.Web.UI.WebControls.ListItem("--Custom Field--", ""))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objSite As New Sites
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            objSite.ElementID = lngElementID
            objSite.StrItems = GetItems()
            objSite.ManagePageElementDetails()
            Response.Redirect("../ECommerce/frmPageElementList.aspx", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmPageElementList.aspx", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Function GetItems() As String

        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numAttributeID")
            dt.Columns.Add("vcAttributeValue")
            dt.Columns.Add("vcHtml")

            Dim objSite As New Sites
            Dim dtAttributes As DataTable
            Dim strValue As String = String.Empty
            Dim strHtml As String = String.Empty
            Const strPrefix As String = "ctl00$ctl00$MainContent$GridPlaceHolder$"
            objSite.ElementID = lngElementID
            objSite.SiteID = Session("SiteID")
            dtAttributes = objSite.GetPageElementAttributes()
            For Each dr1 As DataRow In dtAttributes.Rows
                Select Case dr1("vcControlType").ToString
                    Case "TextBox"
                        strValue = HttpUtility.HtmlEncode(Request.Form(strPrefix & CCommon.ToString(dr1("vcAttributeName")).Replace(" ", "_")))
                    Case "DropDown"
                        strValue = Request.Form(strPrefix & CCommon.ToString(dr1("vcAttributeName")).Replace(" ", "_"))
                    Case "ReportDropDown"
                        strValue = Request.Form(strPrefix & "ddlReport")
                    Case "CheckBox"
                        strValue = IIf(Request.Form(strPrefix & CCommon.ToString(dr1("vcAttributeName")).Replace(" ", "_")) = "on", "True", "False")
                    Case "HtmlEditor"
                        strHtml = Server.HtmlEncode(txtEditHTML.Text)
                End Select
                Dim dr As DataRow = dt.NewRow
                dr("numAttributeID") = dr1("numAttributeID")
                dr("vcAttributeValue") = strValue
                dr("vcHtml") = strHtml

                dt.Rows.Add(dr)
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSaveOnly_Click(sender As Object, e As EventArgs) Handles btnSaveOnly.Click
        Try
            Dim objSite As New Sites
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            objSite.ElementID = lngElementID
            objSite.StrItems = GetItems()
            objSite.ManagePageElementDetails()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class