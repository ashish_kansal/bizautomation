﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Promotion

Public Class frmPromotionSites
    Inherits BACRMPage

#Region "Member Variables"
    Private objPromotionOffer As PromotionOffer
#End Region

#Region "Constructor"
    Sub New()
        objPromotionOffer = New PromotionOffer()
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnPromotionID.Value = CCommon.ToLong(GetQueryStringVal("promotionID"))
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region
    
#Region "Private Methods"
    Private Sub BindData()
        Try
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.PromotionID = CCommon.ToLong(hdnPromotionID.Value)
            Dim dt As DataTable = objPromotionOffer.GetPromotionOfferSites()

            chkblSites.DataSource = dt
            chkblSites.DataTextField = "vcSiteName"
            chkblSites.DataValueField = "numSiteID"
            chkblSites.DataBind()

            For Each dr As DataRow In dt.Rows
                If CCommon.ToBool(dr("bitSelcted")) AndAlso Not chkblSites.Items.FindByValue(dr("numSiteID")) Is Nothing Then
                    chkblSites.Items.FindByValue(dr("numSiteID")).Selected = True
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Dim vcSelectedSites As String = ""

            For Each item As ListItem In chkblSites.Items
                If item.Selected Then
                    vcSelectedSites = vcSelectedSites & IIf(String.IsNullOrEmpty(vcSelectedSites), "", ",") & item.Value
                End If
            Next

            If vcSelectedSites.Length > 0 Then
                objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                objPromotionOffer.PromotionID = CCommon.ToLong(hdnPromotionID.Value)
                objPromotionOffer.SavePromotionOfferSites(vcSelectedSites)

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseWindow", "window.close();", True)
            Else
                lblMessage.Text = "Select atleast one site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

    
End Class