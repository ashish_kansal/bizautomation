﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddCustomBox.aspx.vb"
    Inherits=".frmAddCustomBox" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add Custom Box</title>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function Save() {
            if ($("#<%=txtBoxName.ClientID%>").val() == "") {
                alert("Please enter box name");
                $("#<%=txtBoxName.ClientID%>").focus();
                return false;
            }
            if ($("#<%=txtWidth.ClientID%>").val() == "") {
                alert("Please enter width");
                $("#<%=txtWidth.ClientID%>").focus();
                return false;
            }
            if ($("#<%=txtHeight.ClientID%>").val() == "") {
                alert("Please enter Height");
                $("#<%=txtHeight.ClientID%>").focus();
                return false;
            }
            if ($("#<%=txtLength.ClientID%>").val() == "") {
                alert("Please enter Length");
                $("#<%=txtLength.ClientID%>").focus();
                return false;
            }
            if ($("#<%=txtWeight.ClientID%>").val() == "") {
                alert("Please enter Weight");
                $("#<%=txtWeight.ClientID%>").focus();
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" OnClientClick="return Save()" CssClass="btn btn-primary" Visible="false"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" OnClientClick="return Save()" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" UseSubmitBehavior="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <center>
        <span style="color: Red">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal></span></center>
    <asp:HiddenField ID="hdnCustomPackageID" runat="server"></asp:HiddenField>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Add Custom Box
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label>Box Name: <span style="color: Red">*</span></label>
                <asp:TextBox ID="txtBoxName" runat="server" CssClass="form-control required {required:true,number:false, messages:{required:'Enter Box Name! '}}"
                    MaxLength="1000"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="form-group">
                <label>Width:<span style="color: Red">*</span> </label>
                <asp:TextBox runat="server" ID="txtWidth" CssClass="form-control required_decimal {required:true,number:true, min:1, messages:{required:'Enter Width! ',number:'Please provide valid value!'}}"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="form-group">
                <label>Height:<span style="color: Red">*</span> </label>
                <asp:TextBox runat="server" ID="txtHeight" CssClass="form-control required_decimal {required:true,number:true,min:1, messages:{required:'Enter Height! ',number:'Please provide valid value!'}}"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="form-group">
                <label>Length:<span style="color: Red">*</span> </label>
                <asp:TextBox runat="server" ID="txtLength" CssClass="form-control required_decimal {required:true,number:true,min:1, messages:{required:'Enter Length! ',number:'Please provide valid value!'}}"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="form-group">
                <label>Weight:<span style="color: Red">*</span> </label>
                <asp:TextBox runat="server" ID="txtWeight" CssClass="form-control required_decimal {required:true,number:true,min:1, messages:{required:'Enter Weight! ',number:'Please provide valid value!'}}"></asp:TextBox>
            </div>
        </div>
    </div>

</asp:Content>
