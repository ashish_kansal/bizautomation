﻿Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class frmCustomBoxList
    Inherits BACRMPage

    Dim objShippingRule As ShippingRule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            objCommon = New CCommon
            GetUserRightsForPage(13, 41)

            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If

            If Not IsPostBack Then
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDetails()
        Try

            If objShippingRule Is Nothing Then
                objShippingRule = New ShippingRule
            End If
            objShippingRule.CustomPackageID = 0
            objShippingRule.byteMode = 1
            objShippingRule.DomainID = DomainID

            dgCustomBox.DataSource = objShippingRule.GetCustomPackages()
            dgCustomBox.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgCustomBox_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCustomBox.DeleteCommand
        Try
            objShippingRule = New ShippingRule
            objShippingRule.CustomPackageID = e.Item.Cells(0).Text
            objShippingRule.DomainID = DomainID
            objShippingRule.DeletePackagingRules()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgCustomBox_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCustomBox.ItemCommand
        Try

            If e.CommandName = "CustomPackageID" AndAlso CCommon.ToLong(e.Item.Cells(0).Text) > 100 Then
                Response.Redirect("../ECommerce/frmAddCustomBox.aspx?numPackageTypeID=" & e.Item.Cells(0).Text & "&numCustomPackageID=" & e.Item.Cells(1).Text, False)
            ElseIf e.CommandName = "Delete" Then

                objShippingRule = New ShippingRule
                objShippingRule.CustomPackageID = CCommon.ToLong(e.Item.Cells(1).Text)
                objShippingRule.PackageTypeID = CCommon.ToLong(e.Item.Cells(0).Text)
                objShippingRule.DeleteCustomPackages()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            DisplayError(ex.Message)
        End Try

    End Sub

    Private Sub dgCustomBox_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCustomBox.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                If DataBinder.Eval(e.Item.DataItem, "IsUpdatable") = 0 Then
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord(1)")
                    btnDelete.Text = "*"
                Else
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord(0)")
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class