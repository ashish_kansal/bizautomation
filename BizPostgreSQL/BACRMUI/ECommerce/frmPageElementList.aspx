﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPageElementList.aspx.vb"
    Inherits=".frmPageElementList" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Page Element Lists</title>
    <script language="javascript">
        function InsertElementName() {
            var regExp = /\s/;
            var strElementName = prompt("Enter Element Name", "");
            if (strElementName == undefined)
                return false;
            if (strElementName == '' || regExp.test(strElementName)) {
                alert('Invalid Name');
                return false;
            }
            else
                document.getElementById("hdnElementName").value = strElementName;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Page Elements&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmPageElementList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgPageElements" AllowSorting="false" runat="server" Width="100%"
                    CssClass="table table-striped table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numElementID" HeaderText="numElementID">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="<font>Element Name</font>">
                            <ItemTemplate>
                                <a href="frmPageElementDetails.aspx?ElementID=<%# Eval("numElementID") %>&ElementName=<%# Eval("vcElementName") %>&Customize=<%# Eval("bitCustomization") %>">
                                    <%# Eval("vcElementName")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcTagName" SortExpression="" HeaderText="<font>Tag</font>">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="bitAdd" HeaderText="Add"></asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="bitDelete" HeaderText="Delete"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" CommandName="Add" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" Text="X" CssClass="btn btn-danger" CommandName="Delete"
                                    CommandArgument='<%# Eval("numElementID") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcUserControlPath" SortExpression="" HeaderText="<font>User Control Path</font>"
                            Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnElementName" runat="server" />
        </div>
</asp:Content>
