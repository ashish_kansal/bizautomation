﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Partial Public Class frmMenu1
    Inherits BACRMPage

    Dim lngMenuID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
        If GetQueryStringVal("MenuID") <> "" Then
            lngMenuID = GetQueryStringVal("MenuID")
        End If

        Master.SelectedTabValue = "163"

        If Not IsPostBack Then

            BindPages()
            If lngMenuID > 0 Then
                LoadDetails()

                lblName.Text = "Edit Menu"
            Else
                lblName.Text = "New Menu"
            End If
            rblTargetPage.Attributes.Add("onclick", "TargetType();")
        End If
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub BindPages()
        Try
            Dim objSite As New Sites
            objSite.PageID = 0
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            Dim ds As DataSet = objSite.GetPages()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlPages.DataTextField = "vcPageName"
                ddlPages.DataValueField = "numPageID"
                ddlPages.DataSource = ds.Tables(0)
                ddlPages.DataBind()
            End If
            ddlPages.Items.Insert(0, "--Select One--")
            ddlPages.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objMenu As New Sites
        Try
            objMenu.SiteID = Session("SiteID")
            objMenu.MenuID = lngMenuID
            objMenu.MenuTitle = Server.HtmlEncode(txtTitle.Text.Trim())

            objMenu.Status = rblStatus.SelectedValue

            If rblTargetPage.SelectedValue = 1 Then
                objMenu.PageID = ddlPages.SelectedValue
                objMenu.NavigationURL = ""
            ElseIf rblTargetPage.SelectedValue = 2 Then
                objMenu.PageID = 0
                objMenu.NavigationURL = txtTargetURL.Text.Trim()
            End If

            objMenu.DomainID = Session("DomainID")

            If objMenu.ManageSiteMenu() Then
                Response.Redirect("../ECommerce/frmMenuList.aspx", False)
            Else
                litMessage.Text = "Menu title already exist for this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmMenuList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objMenu As New Sites
            Dim dtMenu As DataTable
            objMenu.MenuID = lngMenuID
            objMenu.SiteID = Session("SiteID")
            objMenu.DomainID = Session("DomainID")
            dtMenu = objMenu.GetSiteMenu()
            txtTitle.Text = Server.HtmlDecode(dtMenu.Rows(0)("vcTitle"))
            If dtMenu.Rows(0)("numPageID") > 0 Then
                ddlPages.SelectedValue = dtMenu.Rows(0)("numPageID")
                txtTargetURL.Enabled = False
                rblTargetPage.SelectedValue = 1
            Else
                txtTargetURL.Text = dtMenu.Rows(0)("vcNavigationURL")
                ddlPages.Enabled = False
                rblTargetPage.SelectedValue = 2
            End If
            rblStatus.SelectedValue = IIf(dtMenu.Rows(0)("bitStatus"), 1, 0)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveOnly_Click(sender As Object, e As EventArgs) Handles btnSaveOnly.Click
        Dim objMenu As New Sites
        Try
            objMenu.SiteID = Session("SiteID")
            objMenu.MenuID = lngMenuID
            objMenu.MenuTitle = Server.HtmlEncode(txtTitle.Text.Trim())

            objMenu.Status = rblStatus.SelectedValue

            If rblTargetPage.SelectedValue = 1 Then
                objMenu.PageID = ddlPages.SelectedValue
                objMenu.NavigationURL = ""
            ElseIf rblTargetPage.SelectedValue = 2 Then
                objMenu.PageID = 0
                objMenu.NavigationURL = txtTargetURL.Text.Trim()
            End If

            objMenu.DomainID = Session("DomainID")

            If objMenu.ManageSiteMenu() Then

            Else
                litMessage.Text = "Menu title already exist for this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class