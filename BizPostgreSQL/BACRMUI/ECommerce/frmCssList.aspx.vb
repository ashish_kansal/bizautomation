﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Imports Telerik.Web.UI

Partial Public Class frmCssList
    Inherits BACRMPage
    Dim intType As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            
            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If GetQueryStringVal( "Type") <> "" Then
                intType = CCommon.ToInteger(GetQueryStringVal("Type"))
                hdnType.Value = intType
            End If
            If Not IsPostBack Then
                BindData()
                hplNew.Attributes.Add("onclick", "NewCssAndJs();")
            End If

            If intType = 0 Then
                hplNew.Text = "New Style"
                lblName.Text = "Styles"
                Master.SelectedTabValue = "159"
            ElseIf intType = 1 Then
                hplNew.Text = "New Javascript"
                lblName.Text = "Javascripts"
                Master.SelectedTabValue = "160"
            ElseIf intType = 2 Then
                hplNew.Text = "New Style"
                lblName.Text = "Portal Styles"
                SiteSwitch1.Visible = False
            End If
            hplNew.NavigateUrl = "~/ECommerce/frmCss.aspx?Type=" & intType.ToString()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Sub BindData()
        Try
            If Session("SiteID") > 0 Then
                Dim objSite As New Sites
                Dim dtStyle As DataTable
                If CCommon.ToLong(Session("SiteID")) > 0 Or intType = 2 Then
                    objSite.SiteID = IIf(intType = 2, 0, Session("SiteID"))
                    objSite.DomainID = Session("DomainID")
                    objSite.StyleType = intType
                    dtStyle = objSite.GetStyles()
                    dgStyles.DataSource = dtStyle
                    dgStyles.DataBind()
                    If intType = 0 Then
                        btnSaveOrder.Visible = False
                        dgStyles.Columns(3).Visible = False
                    Else
                        btnSaveOrder.Visible = True
                    End If

                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgStyles_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgStyles.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../ECommerce/frmCss.aspx?CssID=" & e.Item.Cells(0).Text & "&Type=" & intType.ToString())
            If e.CommandName = "Delete" Then
                DeleteFile(e.Item.Cells(2).Text)
                Dim objSite As New Sites
                objSite.CssID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                objSite.DeleteStyle()
                BindData()
            End If
        Catch ex As Exception
            If ex.Message = "Dependant" Then
                If intType = 0 Then
                    lblMessage.Text = "Can not delete Style. Selected style is being used by site page(s), your option is to uncheck selected style from site page(s) and try again."
                    Exit Sub
                ElseIf intType = 1 Then
                    lblMessage.Text = "Can not delete Javascript. Selected Javascript is being used by site page(s), your option is to uncheck selected Javascript from site page(s) and try again."
                    Exit Sub
                End If

            End If
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub DeleteFile(ByVal FileName As String)
        Try
            Dim strUploadFile As String = Sites.GetUploadPath() & "\" & FileName

            If File.Exists(strUploadFile) Then
                File.Delete(strUploadFile)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgStyles_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgStyles.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnSaveOrder_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveOrder.Click
        Try
            Dim objSite As New Sites
            objSite.StrItems = GetJavascript()
            objSite.SaveJavascriptDisplayOrder()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function GetJavascript() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable

            dt.Columns.Add("numCssID")
            dt.Columns.Add("intDisplayOrder")
            Dim txtbox As New TextBox
            For Each row As DataGridItem In dgStyles.Items
                txtbox = CType(row.FindControl("txtOrder"), TextBox)
                If txtbox.Text.Length > 0 Then
                    If IsNumeric(txtbox.Text) Then
                        Dim dr As DataRow = dt.NewRow
                        dr("numCssID") = row.Cells(0).Text
                        dr("intDisplayOrder") = txtbox.Text.Trim()
                        dt.Rows.Add(dr)
                    End If
                End If
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "CssStyles"

            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class