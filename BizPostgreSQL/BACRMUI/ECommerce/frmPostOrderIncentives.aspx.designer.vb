﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmPostOrderIncentives
    
    '''<summary>
    '''divMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnSaveClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveClose As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''UpdateProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''txtOfferName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOfferName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''divSaleOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divSaleOptions As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''rdBtnCrossSale control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdBtnCrossSale As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''rdbtnIncentiveSale control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdbtnIncentiveSale As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''txtSubTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubTotal As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''hplIncentiveSale control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplIncentiveSale As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lnkBtnAddRule control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBtnAddRule As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''gvItemsForRule control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvItemsForRule As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''hfProId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfProId As Global.System.Web.UI.WebControls.HiddenField
End Class
