﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPromotionOfferCustomers.aspx.vb" Inherits=".frmPromotionOfferCustomers" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Table ID="Table3" Width="900px" runat="server" BorderWidth="1" Height="350" GridLines="None"
        CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:Panel runat="server" ID="pnlOrganizations" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Organization
                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    ClientIDMode="Static"
                                    ShowMoreResultsBox="true"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                                &nbsp;
                                <asp:Button ID="btnAddOrg" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnExportOrg" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                <asp:Button ID="btnRemoveOrg" CssClass="btn btn-primary" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnCloseOrg" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgOrg" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProOrgId"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcCompanyName" HeaderText="Organization Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="PrimaryContact" HeaderText="Primary Contact (First / Last Name & Email)"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Relationship" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkOrgSelectAll" runat="server" onclick="SelectAll('chkOrgSelectAll','dgOrgchk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgOrgchk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlRelProfiles" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Relationship & Profile
                                <asp:DropDownList ID="ddlRelationship" runat="server" Width="200" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:DropDownList ID="ddlProfile" runat="server" Width="200" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddRelProfile" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnRemoveRelProfile" CssClass="btn btn-primary" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnExportRelProfile" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />
                                <asp:Button ID="btnCloseRel" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>

                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgRelationship" AllowSorting="True" runat="server" Width="100%"
                                    CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProOrgId"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RelProfile" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkRelationshipSelectAll" runat="server" onclick="SelectAll('chkRelationshipSelectAll','dgRelationshipchk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgRelationshipchk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
