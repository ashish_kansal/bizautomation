﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingLabelBatchProcessing.aspx.vb"
    Inherits=".frmShippingLabelBatchProcessing" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Create Shipping Rules</title>
    <style type="text/css">
        .drpClass {
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
            AddCaption();
            var valDrp = 0;
            $('.drpClass').change(function (e) {

                var $current = $(this);

                $('.drpClass').each(function () {
                    if ($current.val() > 0) {

                        if ($(this).val() == $current.val() && $(this).attr('id') != $current.attr('id')) {

                            if ($current.val("0") == true) {
                                valDrp = 0;
                                console.log("Selected -select-");
                            }
                            else {
                                valDrp = 1;
                                console.log("Selected other");
                            }
                        }

                        if (valDrp == 1) {
                            console.log("duplicate");
                            alert('This package type already exists.Please select another Package type!');
                            //$current.val("0");
                            valDrp = 0;

                        }
                    }




                });
            });
        });

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true; s
            }
            else {
                return false;
            }
        }
        function OpenConfig(StepNo, StepValue, ProID) {
            if (StepNo == 3) {
                if (StepValue == 1) {
                    document.getElementById('rbIndividualItem').checked = true;
                }
                else if (StepValue == 3) {
                    document.getElementById('rbShipClass').checked = true;
                }
                window.open("../ECommerce/frmPromotionOfferItems.aspx?RecordType=3&StepValue=" + StepValue + "&ProID=" + ProID, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
                return false;
            }
        }

        function Save() {

            var idRoot = 'GridPlaceHolder_gvCriteria';
            var From = 0;
            var To = 0;
            var From1 = 0;
            var To1 = 0;
            var idtxt = '';
            var idtxt1 = '';
            var result = 0;
            if (document.getElementById('txtRuleName').value == "") {
                alert("Enter Rule name");
                document.getElementById('txtRuleName').focus();
                return false;
            }
            //ctl00$GridPlaceHolder$gvCriteria$ctl02$txtFrom
            //GridPlaceHolder_gvCriteria_txtFrom_0
            for (var i = 0; i <= document.getElementById(idRoot).rows.length - 1; i++) {
                if (i < 10) {
                    idtxt = 'GridPlaceHolder_'
                }
                else {
                    idtxt = 'GridPlaceHolder_'
                }
                //From = parseInt(document.getElementById(idRoot + idtxt + i + '_' + 'txtFrom').value);
                //To = parseInt(document.getElementById(idRoot + idtxt + i + '_' + 'txtTo').value);

                From = document.getElementById(idRoot + '_' + 'txtFrom' + '_' + i);
                To = document.getElementById(idRoot + '_' + 'txtTo' + '_' + i);

                if (i > 0) {
                }
                else {
                    if (From != null && parseInt(From.value) == 0) {
                        alert('Provide From value!');
                        From.focus();
                        return false;
                    }

                    if (To != null && parseInt(To.value) == 0) {
                        alert('Provide To value!');
                        To.focus();
                        return false;
                    }
                }

                if ((From != null && parseInt(From.value) != 0) && (To != null && parseInt(To.value) != 0)) {

                    var ddlDomestic, ddlInternational, ddlPackageType;
                    ddlDomestic = document.getElementById(idRoot + '_' + 'ddlDomestic' + '_' + i);
                    ddlInternational = document.getElementById(idRoot + '_' + 'ddlInternational' + '_' + i);
                    ddlPackageType = document.getElementById(idRoot + '_' + 'ddlPackageType' + '_' + i);

                    if (ddlDomestic.value <= 0) {
                        alert('Select Domestic service !');
                        ddlDomestic.focus();
                        return false;
                    }
                    if (ddlInternational.value <= 0) {
                        alert('Select International service !');
                        ddlInternational.focus();
                        return false;
                    }
                    if (ddlPackageType.value <= 0) {
                        alert('Select Package Type !');
                        ddlPackageType.focus();
                        return false;
                    }

                    var srvDomestic = $(ddlDomestic).find("option:selected").parent().attr("label");
                    var srvInternational = $(ddlInternational).find("option:selected").parent().attr("label");
                    var srvPackageType = $(ddlPackageType).find("option:selected").parent().attr("label");

                    if (srvDomestic == srvInternational && srvDomestic == srvPackageType) {
                    }
                    else {
                        if (ddlPackageType.value >= 19) {

                        }
                        else if (srvDomestic != srvInternational) {
                            alert('Please select same service type for Domestic & International at row (' + (parseInt(i) + 1) + ')!');
                            return false;
                        }
                        else if (srvDomestic != srvPackageType && ddlPackageType.value < 19 && srvPackageType != 'Custom Box') {
                            alert('Please select same service type for Domestic & Package Type at row (' + (parseInt(i) + 1) + ')!');
                            return false;
                        }
                        else if (srvPackageType != srvInternational && ddlPackageType.value < 19 && srvPackageType != 'Custom Box') {
                            alert('Please select same service type for International & Package Type at row (' + (parseInt(i) + 1) + ')!');
                            return false;
                        }
                    }

                    if (parseInt(From.value) >= parseInt(To.value)) {
                        alert('Qty To must be greater than Qty From (' + parseInt(From.value) + '-' + parseInt(To.value) + ')')
                        return false;
                    }
                    for (var j = 0; j <= document.getElementById(idRoot).rows.length - 1; j++) {
                        if (j < 10) {
                            idtxt1 = 'GridPlaceHolder_'
                        }
                        else {
                            idtxt1 = 'GridPlaceHolder_'
                        }
                        if (j != i) {
                            //From1 = parseInt(document.getElementById(idRoot + idtxt1 + j + '_' + 'txtFrom').value);
                            //To1 = parseInt(document.getElementById(idRoot + idtxt1 + j + '_' + 'txtTo').value);

                            From1 = document.getElementById(idRoot + '_' + 'txtFrom' + '_' + j);
                            To1 = document.getElementById(idRoot + '_' + 'txtTo' + '_' + j);

                            if ((From1 != null && parseInt(From1.value) != 0) && (To1 != null && parseInt(To1.value) != 0)) {

                                if (parseInt(From1.value) >= parseInt(To1.value)) {
                                    alert('From qty [' + parseInt(From1.value) + '] should not greater than To qty [' + parseInt(To1.value) + '].');
                                    return false;
                                }
                                if (parseInt(To1.value) >= parseInt(From.value) && parseInt(To1.value) <= parseInt(To.value)) {
                                    // alert(From1 + '>=' + From + ' && ' + From1 + '<=' + To);
                                    alert('2 Qty value (' + parseInt(From1.value) + '-' + parseInt(To1.value) + ') must be unique');
                                    return false;
                                }

                                ddlDomestic = document.getElementById(idRoot + '_' + 'ddlDomestic' + '_' + j);
                                ddlInternational = document.getElementById(idRoot + '_' + 'ddlInternational' + '_' + j);
                                ddlPackageType = document.getElementById(idRoot + '_' + 'ddlPackageType' + '_' + j);

                                if (ddlDomestic.value <= 0) {
                                    alert('Select Domestic service !');
                                    ddlDomestic.focus();
                                    return false;
                                }
                                if (ddlInternational.value <= 0) {
                                    alert('Select International service !');
                                    ddlInternational.focus();
                                    return false;
                                }
                                if (ddlPackageType.value <= 0) {
                                    alert('Select Package Type !');
                                    ddlPackageType.focus();
                                    return false;
                                }

                                //                                alert('Check Code');
                                //                                var srvDomestic = $(ddlDomestic).find("option:selected").parent().attr("label");
                                //                                alert(srvDomestic);

                                //                                var srvInternational = $(ddlInternational).find("option:selected").parent().attr("label");
                                //                                alert(srvInternational);

                                //                                var srvPackageType = $(ddlPackageType).find("option:selected").parent().attr("label");
                                //                                alert(srvPackageType);

                                //                                if (srvDomestic == srvInternational && srvDomestic == srvPackageType) {
                                //                                    alert('OK');
                                //                                }
                                //                                else {
                                //                                    if (srvDomestic != srvInternational) {
                                //                                        alert('Please same service type for Domestic & International!');
                                //                                    }
                                //                                    else if (srvDomestic != srvPackageType) {
                                //                                        alert('Please same service type for Domestic & Package!');
                                //                                    }
                                //                                    else if (srvPackageType != srvInternational) {
                                //                                        alert('Please same service type for International & Package!');
                                //                                    }
                                //                                    alert('Not Ok');
                                //                                    return false;
                                //                                }

                            }
                        }
                    }
                }
            }


            if (document.getElementById('hfProId').value != "") {

                if (document.getElementById('rbRedimed').checked) {
                    if (document.getElementById('txtLimitation').value == "") {
                        alert("Enter Usage Limitation")
                        document.getElementById('txtLimitation').focus();
                        return false;
                    }

                    if (parseFloat(document.getElementById('txtLimitation').value) <= 0) {
                        alert("Usage Limitation must greater than 0.")
                        document.getElementById('txtLimitation').focus();
                        return false;
                    }
                }

                if (document.getElementById('txtCouponCode').value == "") {
                    alert("Enter Coupon Code")
                    document.getElementById('txtCouponCode').focus();
                    return false;
                }


                if (document.getElementById('txtDiscount').value == "") {
                    alert("Enter Discount Value")
                    document.getElementById('txtDiscount').focus();
                    return false;
                }

                if (document.getElementById('rbBasedOnUnit').checked) {
                    if (document.getElementById('txtBasedOnUnit').value == "") {
                        alert("Enter Based On Each Unit")
                        document.getElementById('txtBasedOnUnit').focus();
                        return false;
                    }

                    if (parseFloat(document.getElementById('txtBasedOnUnit').value) <= 0) {
                        alert("Based On Each Unit must greater than 0.")
                        document.getElementById('txtBasedOnUnit').focus();
                        return false;
                    }
                }
                else if (document.getElementById('rbBasedOnAmount').checked) {
                    if (document.getElementById('txtBasedOnAmount').value == "") {
                        alert("Enter Based On Total Amount")
                        document.getElementById('txtBasedOnAmount').focus();
                        return false;
                    }
                }
            }
        }

        function ServiceValidation(ddl) {

            //            $('#GridPlaceHolder_gvCriteria tr td').click(function () {
            //                var td = $('#GridPlaceHolder_gvCriteria tr td');
            //                //alert('You Clicked row ' + index);

            //                var optGroup;
            //                optGroup = td.find();
            //                var idName = '#' + ddl.id;
            //                var serviceName = $(idName).find("option:selected").parent().attr("label");
            //                alert(optGroup.find("option:selected").parent().attr("label"));
            //            });

            //            var i = 0;
            //            i = ddl.id.toString().charAt(ddl.id.toString().length - 1);
            //            alert(i);

            //            var idName = '#' + ddl.id;
            //            var serviceName = $(idName).find("option:selected").parent().attr("label");
            //            alert(idName);
            //            alert(serviceName);

            //            if ($('#hdnOptGroup').val() == '') {
            //                $('#hdnOptGroup').val(serviceName);
            //                alert($('#hdnOptGroup').val());
            //            }                                              

            //            if ($('#hdnOptGroup').val() == $('#GridPlaceHolder_gvCriteria_ddlDomestic_' + i + ' optgroup').find("option:selected").parent().attr("label")) {
            //                alert('true');
            //            }
            //            else {
            //                alert('Please select ' + serviceName + ' services only');
            //                return false;
            //            }

            //            if ($('#hdnOptGroup').val() == $('#GridPlaceHolder_gvCriteria_ddlInternational_' + i + ' optgroup').find("option:selected").parent().attr("label")) {
            //                alert('true');
            //            }
            //            else {
            //                alert('Please select ' + serviceName + ' services only');
            //                return false;
            //            }

            //            if ($('#hdnOptGroup').val() == $('#GridPlaceHolder_gvCriteria_ddlPackageType_' + i + ' optgroup').find("option:selected").parent().attr("label")) {
            //                alert('true');
            //            }
            //            else {
            //                alert('Please select ' + serviceName + ' services only');
            //                return false;
            //            }
        }

        function AddCustom() {

            if (document.getElementById('txtShippingCaption').value == "") {
                alert("Enter Shipping Caption");
                document.getElementById('txtShippingCaption').focus();
                return false;
            }
            if (document.getElementById('txtFrom').value == "") {
                alert("Enter From Value");
                document.getElementById('txtFrom').focus();
                return false;
            }
            if (document.getElementById('txtTo').value == "") {
                alert("Enter To Value");
                document.getElementById('txtTo').focus();
                return false;
            }
            if (document.getElementById('txtShippingRate').value == "") {
                alert("Enter Shipping Rate");
                document.getElementById('txtShippingRate').focus();
                return false;
            }
            return true;
        }
        function AddState() {

            if (document.getElementById('ddlCountry').value == "0") {
                alert("Select a Country");
                document.getElementById('ddlCountry').focus();
                return false;
            }
            if (document.getElementById('ddlState').value == "0") {
                alert("Select a State");
                document.getElementById('ddlState').focus();
                return false;
            }
            return true;
        }

        function AddCaption() {
            if (document.form1.rblBasedOn_0.checked == true) {
                document.getElementById('lblCaption').innerHTML = "Item's Quantity between";
            }
            if (document.form1.rblBasedOn_1.checked == true) {
                document.getElementById('lblCaption').innerHTML = "Item's Weight between";
            }
            if (document.form1.rblBasedOn_2.checked == true) {
                document.getElementById('lblCaption').innerHTML = "Item's Price between";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                        <asp:LinkButton ID="btnSaveClose" OnClientClick="return Save()" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                        <asp:LinkButton ID="btnClose" UseSubmitBehavior="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <center>
        <span style="color: Red">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal></span></center>
    <asp:HiddenField ID="hdnShipRuleID" runat="server"></asp:HiddenField>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Shipping Label / Tracking # Automation Rule
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="Table3" Width="100%" runat="server" BorderWidth="0" Height="350" GridLines="None"
        CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <div class="col-md-4">
               <div class="row">
                   <div class="form-inline">
                       <label>
                                Rule Name:<span style="color: Red">*</span>
                            </label>
                            &nbsp;
                        <asp:TextBox ID="txtRuleName" runat="server" CssClass="form-control required {required:true,number:false, messages:{required:'Enter Rule Name! '}}"
                                            MaxLength="100"></asp:TextBox>

                   </div>
               </div>
                    </div>
                <div class="col-md-12" runat="server" id="trMain">
                <div class="row">
                    <div class="form-group">
                        <label>Shipping Based On</label>
                         <asp:RadioButtonList ID="rblBasedOn" runat="server" RepeatDirection="Horizontal"
                                            CssClass="signup" RepeatLayout="Flow">
                                            <asp:ListItem Value="3" onclick="AddCaption();" Selected="True">Item's Quantity</asp:ListItem>
                                            <asp:ListItem Value="1" onclick="AddCaption();">Item's Weight</asp:ListItem>
                                            <asp:ListItem Value="2" onclick="AddCaption();">Item's Price</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="i.e Ordered Item's Unit Price. <p> Do not use this option when you are planning to use Pricebook Rules" />
                    </div>
                        <div class=" col-md-4" style="padding-left: 0;">
                    <div class="form-inline">
                                <label>Source Marketplace </label>
                                &nbsp;
                        
                            <asp:DropDownList ID="ddlMarketplace" CssClass="form-control" runat="server" AutoPostBack="false">
                            </asp:DropDownList>
                            </div>
                    
                            <div class="form-inline" style="display: none">
                        <label>Source Ship Method</label>
                        <asp:DropDownList ID="ddlSourceShipMethod" CssClass="form-control col-md-4" runat="server">
                                        </asp:DropDownList>
                    </div>
                    </div>
                        </div>
               
                <div class="clearfix"></div>
                <div class="col-md-12">
                <div class="row">
                    <h4><b>Provide Criteria</b></h4>
                        <asp:GridView ID="gvCriteria" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="false"
                            ClientIDMode="Predictable" UseAccessibleHeader="true">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table width="300px" id="tblHeader">
                                                            <th colspan="2">
                                                                <label id="lblCaption" class="Caption">
                                                                </label>
                                                            </th>
                                                            <tr>
                                                                <td width="150px">From <span style="color: Red">*</span>
                                                                </td>
                                                                <td width="150px">To <span style="color: Red">*</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="300px">
                                                            <tr>
                                                                <td width="150px" align="left">
                                                                    <asp:HiddenField ID="hdnChildShipID" runat="server" Value='<%#Eval("numChildShipID")%>' />
                                                                    <asp:TextBox runat="server" ID="txtFrom" CssClass="form-control required_decimal {required:false,number:true, messages:{required:'Enter from value! ',number:'Please provide valid value!'}}"></asp:TextBox>
                                                                </td>
                                                                <td align="right" width="150px">
                                                    <asp:TextBox runat="server" Style="max-width: 98%" ID="txtTo" CssClass="form-control required_decimal {required:false,number:true, messages:{required:'Enter to value! ',number:'Please provide valid value!'}}"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td colspan="2" rowspan="2">Domestic<span style="color: Red">*</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td rowspan="2" colspan="2">
                                                                    <input id='hdnOptGroup' type="hidden" />
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlDomestic">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td colspan="2" rowspan="2">International<span style="color: Red">*</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td rowspan="2" colspan="2">
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlInternational">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td colspan="2" rowspan="2">Package Type<span style="color: Red">*</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td rowspan="2" colspan="2">
                                                                    <asp:DropDownList runat="server" ID="ddlPackageType" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <span style="color: Red">No data found.</span>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                </div>
                <div class="row">
                        <h4><b>Items Affected</b></h4>
                    </div>
                    <div class="row">
                        <div class="form-inline">
                            <asp:RadioButton ID="rbIndividualItem" runat="server" GroupName="Items" />
                            <asp:HyperLink ID="hplIndividualItems" CssClass="hyperlink" Text="<label for='rbIndividualItem'>Apply to items individually</label>"
                                            runat="server"> </asp:HyperLink>
                        </div>
                        </div>
                    <div class="row">
                        <div class="form-inline">
                            <asp:RadioButton ID="rbShipClass" runat="server" GroupName="Items" />
                            <asp:HyperLink ID="hplClassification" CssClass="hyperlink" Text="<label for='rbShipClass'>Apply to items with the selected Shipping Class</label>"
                                            runat="server"> </asp:HyperLink>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-inline">
                            <asp:RadioButton ID="rbAllItems" runat="server" GroupName="Items" />
                            <label for="rblAllItems">
                                            All Items</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-inline">
                            <label>Country and State</label>
                            <asp:RadioButtonList ID="rblStateInclude" runat="server" RepeatDirection="Horizontal"
                                                        RepeatLayout="Flow">
                                                        <asp:ListItem Value="1" Selected="True">Include</asp:ListItem>
                                                        <asp:ListItem Value="2">Exclude</asp:ListItem>
                                                    </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="row">
                        <table class="table table-responsive">
                                                        <tr>
                                                            <td>Country
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlCountry" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>State
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlState">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>From Zip
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtFromZip" CssClass="required_decimal {required:false,number:true, messages:{required:'Enter to value! ',number:'Please provide valid value!'}}"></asp:TextBox>
                                                            </td>
                                                            <td>To Zip
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtToZip" CssClass="required_decimal {required:false,number:true, messages:{required:'Enter to value! ',number:'Please provide valid value!'}}"></asp:TextBox>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:LinkButton ID="btnAdd" runat="server" OnClientClick="return AddState();" CssClass="btn btn-primary">Add</asp:LinkButton>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                    </div>
                    <div class="row">
                        <asp:GridView ID="gvCountryState" runat="server" CssClass="table table-responsive table-bordered tblDataGrid" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%#Eval("numRuleStateID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Country">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("CountryName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="State">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblState" runat="server" Text='<%#Eval("StateName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="From Zip">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFromZip" runat="server" Text='<%#Eval("FromZip")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="To Zip">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblToZip" runat="server" Text='<%#Eval("ToZip")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDel" OnClientClick="return DeleteRecord()" CommandName="Delete"
                                                                        CssClass="btn btn-danger" Text="X" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                    </div>
                 </div>
                 </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
