﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Net
Imports Telerik.Web.UI

Partial Public Class frmSiteCategory
    Inherits BACRMPage

#Region "Global Declaration"
    Dim lngSiteID As Long
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            GetUserRightsForPage(13, 53)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                BindSites()
                If Not Session("SiteID") Is Nothing And CCommon.ToLong(Session("SiteID")) > 0 Then
                    pnlDetail.Visible = True
                    ddlSites.SelectedValue = Session("SiteID")
                    lngSiteID = CCommon.ToLong(Session("SiteID"))
                    loadCategory()
                    If rtvSiteCategory.Nodes.Count > 0 Then
                        LoadDetails()
                    End If
                Else
                    pnlDetail.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
    Private Sub ddlSites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSites.SelectedIndexChanged
        Try
            Session("SiteID") = ddlSites.SelectedValue
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = ddlSites.SelectedValue
            objSite.DomainID = Session("DomainID")
            dtSite = objSite.GetSites()
            Session("SitePreviewURL") = dtSite.Rows(0)("vcPreviewURL")
            Response.Redirect(Request.Url.OriginalString)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
 
#End Region

#Region "Methods"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objSite As New Sites
        Try
            If CCommon.ToLong(Session("SiteID")) > 0 Then
                If GetSelectedCategory() <> "" Then
                    objSite.SiteID = CCommon.ToLong(Session("SiteID"))
                    objSite.ListOfCategories = GetSelectedCategory()
                    objSite.Mode = 1
                    objSite.ManageSites()

                    loadCategory()
                    If rtvSiteCategory.Nodes.Count > 0 Then
                        LoadDetails()
                    End If
                Else
                    litMessage.Text = "Select atleast one category to save"
                End If
            Else
                litMessage.Text = "Select Site"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#Region "Category"
    Private Sub loadCategory()
        Try
            Dim objItems As New CItems
            objItems.byteMode = 19
            objItems.DomainID = Session("DomainID")
            objItems.SiteID = IIf(Not Session("SiteID") Is Nothing, Session("SiteID"), 0)

            rtvSiteCategory.DataTextField = "vcCategoryName"
            rtvSiteCategory.DataFieldID = "numCategoryID"
            rtvSiteCategory.DataValueField = "numCategoryID"
            rtvSiteCategory.DataFieldParentID = "numDepCategory"
            rtvSiteCategory.DataSource = objItems.SeleDelCategory
            Try
                rtvSiteCategory.DataBind()
            Catch ex As Exception
                litMessage.Text = ex.Message
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadDetails()
        Try
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = CCommon.ToLong(Session("SiteID"))
            objSite.DomainID = Session("DomainID")
            dtSite = objSite.GetSites()

            Dim strListOfCategories As String() = CCommon.ToString(dtSite.Rows(0).Item("vcItemCategories")).Split(",")
            For k As Integer = 0 To strListOfCategories.Length - 1
                For Each tn As RadTreeNode In rtvSiteCategory.Nodes
                    If tn.Value = strListOfCategories(k) Then
                        tn.Checked = True
                    Else
                        FindNode(tn, strListOfCategories(k))
                    End If

                Next
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetSelectedCategory() As String
        Try
            Dim strCategories As String = ""
            If rtvSiteCategory.CheckedNodes.Count > 0 Then
                For Each item As RadTreeNode In rtvSiteCategory.CheckedNodes
                    If item.Checked = True Then
                        strCategories = strCategories + item.Value + ","
                    End If
                Next
            End If
            strCategories = strCategories.TrimEnd(",")
            Return strCategories
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

    


    Private Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "--Select One--")
            ddlSites.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FindNode(treeNode As RadTreeNode, NodeValue As String)
        Try
            For Each treeNodeEach As RadTreeNode In treeNode.Nodes
                If treeNodeEach.Value = NodeValue Then
                    treeNodeEach.Checked = True
                End If
                FindNode(treeNodeEach, NodeValue)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class

