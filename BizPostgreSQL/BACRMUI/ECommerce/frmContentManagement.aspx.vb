﻿Imports System.IO
Imports System.Web.Services
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Newtonsoft.Json

Public Class frmContentManagement
    Inherits BACRMPage
    Dim lngType As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptManager As ScriptManager
        scriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager.RegisterPostBackControl(Me.btnSubmit)
        lngType = CCommon.ToLong(GetQueryStringVal("type"))
        If Not IsPostBack Then
            BindBlogCategory()
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs)

        Try

            ManageContentManagement()
            ManagePublishPages()

            txtTitle.Text = ""
            txtDescription.Text = ""
            hdnContentId.Value = ""
            hdnUrl.Value = ""
            ddlCategory.ClearSelection()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub BindBlogCategory()
        Try
            ddlCategory.Items.Clear()
            Dim dt As New DataTable()
            objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
            objCommon.ListID = CCommon.ToLong(1344)
            dt = objCommon.GetMasterListItemsWithRights()
            Dim item As ListItem = New ListItem("-- All --", "0")
            ddlCategory.Items.Add(item)
            ddlCategory.AutoPostBack = False
            For Each dr As DataRow In dt.Rows
                item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                ddlCategory.Items.Add(item)
            Next
        Catch ex As Exception

        End Try
    End Sub
    Function ManagePublishPages()
        Try
            Dim strHead As String = ""
            Dim strBody As String
            Dim strMaintainScroll As String = ""
            Dim _PageURL As String = ""
            _PageURL = hdnUrl.Value
            Dim objSite As New Sites
            objSite.SiteID = Session("SiteID")
            objSite.PageURL = _PageURL
            objSite.DomainID = Session("DomainID")
            Dim dt As New DataTable()
            dt = objSite.GetSites()

            strBody = objSite.GenerateHTMLfromTemplateForCustomPagesBlogs(objSite.SiteID, "customPageContent", False)

            strMaintainScroll = " MaintainScrollPositionOnPostback=""true"" "
            Dim sbStart As New System.Text.StringBuilder
            sbStart.Append("<%@ Page Language=""C#"" Inherits=""BizCart.BizPage"" " & strMaintainScroll & " %>" & vbCrLf)
            sbStart.Append("<%@ Register TagPrefix=""url"" Namespace=""Intelligencia.UrlRewriter"" Assembly=""Intelligencia.UrlRewriter"" %>" & vbCrLf) ' Fixed this issue http://www.tutorialsasp.net/tutorials/fixing-postbacks-while-using-urlrewriternet/
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso CCommon.ToBool(dt.Rows(0)("bitHtml5")) Then
                sbStart.Append("<!DOCTYPE html>" & vbCrLf)
            Else
                sbStart.Append("<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""> " & vbCrLf)
            End If
            sbStart.Append("<script runat=""server""> " & vbCrLf)
            sbStart.Append("</script> " & vbCrLf)
            sbStart.Append("<html xmlns=""http://www.w3.org/1999/xhtml""> " & vbCrLf)
            sbStart.Append("<head runat=""server"">")

            strHead = strHead & "<title>" & txtTitle.Text & "</title>" & vbCrLf

            strHead = strHead & txtDescription.Text & vbCrLf
            'Add MetaTags added to site level and this tags are added in all pages
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMetaTags"))) Then
                sbStart.Append(CCommon.ToString(dt.Rows(0)("vcMetaTags")))
            End If

            'Start Head 
            sbStart.Append(strHead) 'get Title,get Metatags,get Css and JS 
            'End Head
            sbStart.Append("</head> " & vbCrLf)
            sbStart.Append("<body> " & vbCrLf)
            sbStart.Append("    <url:form runat=""server"" id=""form1"">" & vbCrLf)

            'Start Body
            sbStart.Append(strBody) 'get HTML templete, replace Page Element with Actual User Controls,get generated HTML
            'End Body
            sbStart.Append("</url:form> " & vbCrLf)
            sbStart.Append("</body> " & vbCrLf)
            sbStart.Append("</html>")

            'Save File as aspx page
            Dim strDirectory As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString() & "\" & _PageURL
            If Directory.Exists(strDirectory) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(strDirectory)
            End If
            objSite.WriteToFile(sbStart.ToString(), strDirectory & "\" & "Default.aspx")

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ManageContentManagement()
        Try
            Dim result As String = "0"
            If hdnContentId.Value > 0 Then
                Dim objCM As New ContentManagement
                With objCM
                    .numContentID = hdnContentId.Value
                    .numSiteId = Session("SiteID")
                    .numDomainID = Session("DomainID")
                    .vcContent = hdnContent.Value
                    .vcUrl = hdnUrl.Value
                    .vcTitle = txtTitle.Text
                    .vcMetaDesc = txtDescription.Text
                    .intMode = 1
                    .intType = lngType
                    .numCategoryId = ddlCategory.SelectedValue
                    .numCreatedBy = Session("UserContactID")
                    objCM.ManageEcommercePages()

                End With
            Else
                Dim objCM As New ContentManagement
                With objCM
                    .numContentID = 0
                    .numSiteId = Session("SiteID")
                    .numDomainID = Session("DomainID")
                    .vcContent = hdnContent.Value
                    .vcUrl = hdnUrl.Value
                    .vcTitle = txtTitle.Text
                    .vcMetaDesc = txtDescription.Text
                    .intMode = 0
                    .intType = lngType
                    .numCategoryId = ddlCategory.SelectedValue
                    .numCreatedBy = Session("UserContactID")
                    objCM.ManageEcommercePages()

                End With
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Function

    Function DeleteContentManagement()
        Try
            If hdnContentId.Value > 0 Then
                Dim objCM As New ContentManagement
                With objCM
                    .numContentID = hdnContentId.Value
                    .numSiteId = Session("SiteID")
                    .DomainID = Session("DomainID")
                    .vcContent = ""
                    .vcUrl = hdnUrl.Value
                    .intMode = 2
                    objCM.ManageEcommercePages()
                End With
                Try
                    Dim strDirectory As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\" & hdnUrl.Value
                    If Directory.Exists(strDirectory) = False Then ' If Folder Does not exists create New Folder.
                        Dim d As New System.IO.DirectoryInfo(strDirectory)
                        d.Attributes = IO.FileAttributes.Normal
                        System.IO.Directory.Delete(strDirectory, True)
                    End If
                Catch ex As Exception
                End Try
                Page.Response.Redirect(Page.Request.Url.ToString(), True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Function


    <WebMethod()>
    Public Shared Function WebMethodContentList(ByVal DomainID As Integer, ByVal SiteID As Integer, ByVal Type As Integer) As String
        Try
            Dim objCM As New ContentManagement

            objCM.numContentID = 0
            objCM.numSiteId = SiteID
            objCM.numDomainID = DomainID
            objCM.intMode = 0
            objCM.intType = Type
            Dim dtCM As DataTable = objCM.GetEcommercePages
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtCM, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strEx As String
            strEx = ex.ToString()
        End Try
    End Function


    <WebMethod()>
    Public Shared Function WebMethodGetBizHelpContents(ByVal DomainID As Integer, ByVal ContentKey As String, ByVal IsSpecificContent As Boolean) As String
        Try
            Dim objCM As New ContentManagement

            objCM.bitIsSpecificContent = IsSpecificContent
            objCM.numDomainID = DomainID
            objCM.vcContentKey = ContentKey
            Dim dtCM As DataTable = objCM.GetBizHelpContents
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtCM, Formatting.None)
            Return json
        Catch ex As Exception
            Dim str As String
            str = ex.ToString()
        End Try
    End Function


    <WebMethod()>
    Public Shared Function WebMethodSaveBizHelpContents(ByVal DomainID As Integer, ByVal ContentKey As String, ByVal IsSpecificContent As Boolean, ByVal vcContent As String) As String
        Try
            Dim objCM As New ContentManagement

            objCM.bitIsSpecificContent = IsSpecificContent
            objCM.numDomainID = DomainID
            objCM.vcContentKey = ContentKey
            objCM.vcContent = vcContent
            objCM.bitIsActive = True
            Dim dtCM As String = objCM.ManageBizHelpContents
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtCM, Formatting.None)
            Return json
        Catch ex As Exception

        End Try
    End Function

    <WebMethod()>
    Public Shared Function WebMethodContentDetails(ByVal DomainID As Integer, ByVal SiteID As Integer, ByVal ContentID As Integer) As String
        Try
            Dim objCM As New ContentManagement

            objCM.numContentID = ContentID
            objCM.numSiteId = SiteID
            objCM.numDomainID = DomainID
            objCM.intMode = 1
            Dim dtCM As DataTable = objCM.GetEcommercePages
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtCM, Formatting.None)
            Return json
        Catch ex As Exception

        End Try
    End Function

    Function GetContentManagementByContentId() As DataTable
        Try
            If hdnUrl.Value > 0 Then
                Dim objCM As New ContentManagement
                With objCM
                    .numContentID = hdnContentId.Value
                    .numSiteId = Session("SiteID")
                    .numDomainID = Session("DomainID")
                    .intMode = 0
                    Dim dtCM As DataTable = objCM.GetEcommercePages
                End With
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Function
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        DeleteContentManagement()
    End Sub
End Class