﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPackagingRuleList.aspx.vb"
    Inherits=".frmPackagingRuleList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Packaging Rules</title>
    <script type="text/javascript" language="javascript">
        function New() {
            window.location.href = "frmPackagingRules.aspx";
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr>
                    <td>
                        <input type="button" runat="server" id="btnNew" class="button" value="New Rule"
                            onclick="New()" />
                        &nbsp;
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Packaging Rules
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgPackagingRuleList" runat="server" Width="100%" CssClass="dg"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais" />
                    <HeaderStyle CssClass="hs" />
                    <ItemStyle CssClass="is" />
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numPackagingRuleID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcRuleName" CommandName="RuleID" HeaderText="Rule Name">
                        </asp:ButtonColumn>
                        
                        <asp:BoundColumn DataField="vcPackageName" HeaderText ="Package Type" ></asp:BoundColumn>
                        <asp:BoundColumn DataField="numFromQty" HeaderText ="Qty of Item It Can Hold" ></asp:BoundColumn>
                        <asp:BoundColumn DataField="ShipClass" HeaderText ="Shipping Class" ></asp:BoundColumn>

                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
