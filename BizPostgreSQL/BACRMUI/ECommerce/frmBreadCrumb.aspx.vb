﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Partial Public Class frmBreadCrumb
    Inherits BACRMPage
    Dim lngBreadCrumbID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
        If GetQueryStringVal("BreadCrumbID") <> "" Then
            lngBreadCrumbID = CCommon.ToLong(GetQueryStringVal("BreadCrumbID"))
        End If

        Master.SelectedTabValue = "167"

        If Not IsPostBack Then

            BindPages()
            If lngBreadCrumbID > 0 Then
                LoadDetails()

                lblName.Text = "Edit Bread Crumb"
            Else
                lblName.Text = "New Bread Crumb"
            End If
        End If
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub BindPages()
        Try
            Dim objSite As New Sites
            objSite.PageID = 0
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            Dim ds As DataSet = objSite.GetPages()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlPages.DataTextField = "vcPageName"
                ddlPages.DataValueField = "numPageID"
                ddlPages.DataSource = ds.Tables(0)
                ddlPages.DataBind()
            End If
            ddlPages.Items.Insert(0, "--Select One--")
            ddlPages.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objBreadCrumb As New Sites
        Try
            objBreadCrumb.SiteID = Session("SiteID")
            objBreadCrumb.BreadCrumbID = lngBreadCrumbID
            objBreadCrumb.DisplayName = Server.HtmlEncode(txtDisplay.Text.Trim())
            objBreadCrumb.Level = Short.Parse(txtLevel.Text.Trim())

            objBreadCrumb.PageID = ddlPages.SelectedValue
            objBreadCrumb.DomainID = Session("DomainID")

            If objBreadCrumb.ManageSiteBreadCumbs() Then
                Response.Redirect("../ECommerce/frmBreadCrumbList.aspx", False)
            Else
                litMessage.Text = "Display Name aleady exists for some other page in this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmBreadCrumbList.aspx", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objBreadCrumb As New Sites
            Dim dtBreadCrumb As DataTable
            objBreadCrumb.BreadCrumbID = lngBreadCrumbID
            objBreadCrumb.SiteID = Session("SiteID")
            objBreadCrumb.DomainID = Session("DomainID")
            dtBreadCrumb = objBreadCrumb.GetBreadCrumb()
            txtDisplay.Text = Server.HtmlDecode(dtBreadCrumb.Rows(0)("vcDisplayName"))
            txtLevel.Text = Server.HtmlDecode(dtBreadCrumb.Rows(0)("tintLevel"))
            If dtBreadCrumb.Rows(0)("numPageID") > 0 Then
                ddlPages.SelectedValue = dtBreadCrumb.Rows(0)("numPageID")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveOnly_Click(sender As Object, e As EventArgs) Handles btnSaveOnly.Click
        Dim objBreadCrumb As New Sites
        Try
            objBreadCrumb.SiteID = Session("SiteID")
            objBreadCrumb.BreadCrumbID = lngBreadCrumbID
            objBreadCrumb.DisplayName = Server.HtmlEncode(txtDisplay.Text.Trim())
            objBreadCrumb.Level = Short.Parse(txtLevel.Text.Trim())

            objBreadCrumb.PageID = ddlPages.SelectedValue
            objBreadCrumb.DomainID = Session("DomainID")

            If objBreadCrumb.ManageSiteBreadCumbs() Then

            Else
                litMessage.Text = "Display Name aleady exists for some other page in this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class