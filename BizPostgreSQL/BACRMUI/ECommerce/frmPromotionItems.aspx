﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmPromotionItems.aspx.vb" Inherits=".frmPromotionItems" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../CSS/select2.css" />

    <style type="text/css">
        .select2popup {
            width: 650px;
            height: 200px;
        }
    </style>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        $(function () {
            Scroll(true);


            var columns;
            var userDefaultPageSize = '<%= Session("PagingRows")%>';
            var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
            var varPageSize = parseInt(userDefaultPageSize, 10);
            var varCharSearch = 1;
            if (parseInt(userDefaultCharSearch, 10) > 0) {
                varCharSearch = parseInt(userDefaultCharSearch, 10);
            }

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

            function formatItem(row) {
                var numOfColumns = 0;
                var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
                if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                    ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
                } else {
                    ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
                }

                ui = ui + "</td>";
                ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
                ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                $.each(columns, function (index, column) {
                    if (numOfColumns == 4) {
                        ui = ui + "</tr><tr>";
                        numOfColumns = 0;
                    }

                    if (numOfColumns == 0) {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    else {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    numOfColumns += 2;
                });
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";
                ui = ui + "</td>";
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";

                return ui;
            }


            function getvalue() {
                return 0;
            }

            function getDivisionId() {
                return 0;
            }

            $(document).bind('keypress', function (event) {
                if (event.which === 9 && event.ctrlKey) {
                    $("#txtItem").select2("open");
                    $("#txtItem").focus();
                }

                if (event.which === 13) {
                    var obj = $('#txtItem').select2('data');
                }
            });

            $('#txtItem').on("select2-removed", function (e) {
                document.getElementById("lkbItemRemoved").click();
            })

            $('#txtItem').select2(
                {
                    placeholder: 'Select Items',
                    minimumInputLength: varCharSearch,
                    multiple: false,
                    formatResult: formatItem,
                    width: "100%",
                    dropdownCssClass: 'bigdrop',
                    dataType: "json",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '../common/Common.asmx/GetSearchedItems',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            return JSON.stringify({
                                searchText: term,
                                pageIndex: page,
                                pageSize: varPageSize,
                                divisionId: getDivisionId(),
                                isGetDefaultColumn: false,
                                warehouseID: getvalue(),
                                searchOrdCusHistory: 0,
                                oppType: 1,
                                isTransferTo: false,
                                IsCustomerPartSearch: false,
                                searchType: "1"
                            });
                        },
                        results: function (data, page) {

                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            var more = (page.page * varPageSize) < data.Total;
                            return { results: $.parseJSON(data.results), more: more };
                        }
                    }
                });

            var cancelDropDownClosing = false;
            $('[id$=raditems] .rcbList li.rcbTemplate').click(function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            });
            StopPropagation = function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            }
            onDropDownClosing = function (sender, args) {
                cancelDropDownClosing = false;
            }
            onCheckBoxClick = function (chk, comboBoxID) {
                //Prevent second RadComboBox from closing.
                cancelDropDownClosing = true;
                var text = "";
                var values = "";
                var combo = $find(comboBoxID);
                var items = combo.get_items();
                for (var i = 0; i < items.get_count() ; i++) {
                    var item = items.getItem(i);
                    var chk1 = $get(combo.get_id() + "_i" + i + "_chkSelect");
                    if (chk1.checked) {
                        text += item.get_text() + ",";
                    }
                }
                text = removeLastComma(text);
                if (text.length > 0) {
                    combo.set_text(text);
                }
                else {
                    combo.set_text("");
                }
            }
        });

        function Scroll(isEnabled) {
            if (isEnabled) {
                $('body').css('overflow', '');
            } else {
                $('body').css('overflow', 'hidden');
            }
        }

        function GetSelectedItems(isFromAddButton) {

            var obj = $('#txtItem').select2('data');

            if (obj == null) {
                alert("Please select item.");
                return false;
            }
            var item = new Object();
            item.numItemCode = obj.numItemCode;
            item.numWareHouseItemID = obj.numWareHouseItemID;
            item.bitHasKitAsChild = Boolean(obj.bitHasKitAsChild);
            item.kitChildItems = obj.childKitItems;
            item.numQuantity = obj.numQuantity || 1;
            item.vcAttributes = obj.Attributes || "";
            item.vcAttributeIDs = obj.AttributeIDs || "";
            $("#hdnSelectedItems").val(item.numItemCode);
            if (isFromAddButton) {
                $("[id$=btnAddItem]").click();
            } else {
                return true;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table width="100%">
        <tr style="float: right">
            <td>
                <asp:Button ID="btnClose" CssClass="btn btn-primary" runat="server" Text="Close" Style="float: right;" OnClientClick="window.close();"></asp:Button>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label>
    <asp:HiddenField ID="hdnPromotionID" runat="server" />
    <asp:HiddenField ID="hdnRecordType" runat="server" />
    <asp:HiddenField ID="hdnItemSelectionType" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlItems" Visible="false">
        <div class="row padbottom10">
            <div class="col-xs-12">
                <ul class="list-inline">
                    <li style="width: 75%">
                        <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server" Width="500" TabIndex="300"></asp:TextBox></li>
                    <li>
                        <input type="button" value="Add Item" runat="server" id="btnAditem" onclick="return GetSelectedItems(true);" class="btn btn-primary" /></li>
                    <li>
                        <asp:Button ID="btnRemItem" CssClass="btn btn-danger" runat="server" Text="Remove" OnClientClick="return DeleteRecord();"></asp:Button></li>
                </ul>
                <asp:Button ID="btnAddItem" CssClass="button" Style="display: none;" runat="server" Text="Add"></asp:Button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="gvItems" AllowSorting="True" runat="server" UseAccessibleHeader="true" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" DataKeyField="numValue">
                    <Columns>
                        <asp:BoundField Visible="False" DataField="numProItemId"></asp:BoundField>
                        <asp:BoundField Visible="False" DataField="numProId"></asp:BoundField>
                        <asp:BoundField DataField="vcItemName" HeaderText="Item Name"></asp:BoundField>
                        <asp:BoundField DataField="vcModelID" HeaderText="Model ID"></asp:BoundField>
                        <asp:BoundField DataField="txtItemDesc" HeaderText="Description"></asp:BoundField>
                        <asp:TemplateField HeaderStyle-Width="30" ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','check1')" />
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnProItemId" runat="server" Value='<%# Eval("numProItemId")%>' />
                                <asp:CheckBox ID="chk" CssClass="check1" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlItemClassification" runat="server" Visible="false">
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Select Item Classfication:</label>
                        <asp:DropDownList ID="ddlItemClassification" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnAddItemClassification" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                    <asp:Button ID="btnRemoveItemClassification" CssClass="btn btn-danger" runat="server" Text="Remove" OnClientClick="return DeleteRecord();"></asp:Button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="gvItemClassification" AllowSorting="True" runat="server" Width="100%"
                    CssClass="table table-bordered table-striped" AutoGenerateColumns="False" DataKeyField="numValue">
                    <Columns>
                        <asp:BoundField Visible="False" DataField="numProId"></asp:BoundField>
                        <asp:BoundField DataField="ItemClassification" HeaderText="Item Classification"></asp:BoundField>
                        <asp:TemplateField HeaderStyle-Width="30" ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('chk','check2')" />
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnProItemId" runat="server" Value='<%# Eval("numProItemId")%>' />
                                <asp:CheckBox ID="chk" CssClass="check2" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlItemCustomFields" runat="server" Visible="false">
        <table width="800">
            <tr>
                <td class="normal1">
                    <asp:Button ID="btnAddCustomFields" CssClass="button" runat="server" Text="Add"></asp:Button>
                    <asp:Button ID="btnDeleteCustomFields" CssClass="button" runat="server" Text="Remove"
                        OnClientClick="return DeleteRecord();"></asp:Button>
                </td>
                <td align="center">
                    <table id="rblCustom" class="tblspace" width="100%" runat="server">
                    </table>
                    <asp:PlaceHolder ID="plhCustomControls" runat="server"></asp:PlaceHolder>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdnSelectedItems" ClientIDMode="Static" runat="server" />
</asp:Content>
