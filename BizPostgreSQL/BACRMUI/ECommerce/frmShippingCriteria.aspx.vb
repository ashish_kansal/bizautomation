﻿Imports BACRM.BusinessLogic.Promotion
Imports BACRM.BusinessLogic.Common

Public Class frmShippingCriteria
    Inherits BACRMPage

#Region "Member Variables"
    Dim objPromotionOffer As PromotionOffer
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            objCommon.sb_FillComboFromDBwithSel(ddlFreeShippingCountry, 40, Session("DomainID"))
            LoadDetails()
        End If
    End Sub

#Region "Constructor"
    Sub New()
        objPromotionOffer = New PromotionOffer
    End Sub
#End Region

#Region "Private Methods"

    Public Function GetCookie() As HttpCookie
        Dim cookie As HttpCookie = Request.Cookies("Cart")
        If cookie Is Nothing Then
            SetCookie(cookie)
        End If

        Return cookie
    End Function

    Public Sub SetCookie(ByVal cookie As HttpCookie)
        Dim g As Guid
        g = Guid.NewGuid()
        cookie = New HttpCookie("Cart")
        cookie("KeyId") = CCommon.ToString(g)
        cookie.Expires = DateTime.Now.AddDays(50)
        Response.Cookies.Add(cookie)
    End Sub


    Private Sub LoadDetails()
        Try
            Dim cookie As HttpCookie = Request.Cookies("Cart")
            cookie = GetCookie()

            objPromotionOffer = New PromotionOffer
            objPromotionOffer.DomainID = Session("DomainID")
            objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objPromotionOffer.Mode = 0
            objPromotionOffer.CookieId = CCommon.ToString(cookie("KeyId"))

            Dim dtTable As DataTable = objPromotionOffer.GetShippingPromotions(CCommon.ToLong(Session("DefCountry")))

            If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                chkFreeShipping.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShiping"))
                txtFreeShippingAmount.Text = CCommon.ToInteger(dtTable.Rows(0).Item("monFreeShippingOrderAmount"))
                ddlFreeShippingCountry.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numFreeShippingCountry"))
                chkFixedShipping1.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitFixShipping1"))
                txtFixShipping1OrderAmount.Text = CCommon.ToInteger(dtTable.Rows(0).Item("monFixShipping1OrderAmount"))
                txtFixShipping1Charge.Text = CCommon.ToInteger(dtTable.Rows(0).Item("monFixShipping1Charge"))
                chkFixedShipping2.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitFixShipping2"))
                txtFixShipping2OrderAmount.Text = CCommon.ToInteger(dtTable.Rows(0).Item("monFixShipping2OrderAmount"))
                txtFixShipping2Charge.Text = CCommon.ToInteger(dtTable.Rows(0).Item("monFixShipping2Charge"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function Save() As Long
        Try
            Dim errorMessage As String = ""
            If chkFreeShipping.Checked Then
                If CCommon.ToLong(txtFreeShippingAmount.Text) <= 0 Then
                    errorMessage = errorMessage & "Enter valid amount for free shipping.<br/>"
                End If

                If CCommon.ToLong(ddlFreeShippingCountry.SelectedValue) = 0 Then
                    errorMessage = errorMessage & "Select country for free shipping.<br/>"
                End If
            End If

            If chkFixedShipping1.Checked AndAlso CCommon.ToLong(txtFixShipping1OrderAmount.Text) <= 0 Then
                errorMessage = errorMessage & "Enter sales order total amount range for fix shipping charge.<br/>"
            End If

            If chkFixedShipping2.Checked AndAlso CCommon.ToLong(txtFixShipping2OrderAmount.Text) <= 0 Then
                errorMessage = errorMessage & "Enter sales order total amount range for fix shipping charge.<br/>"
            End If

            If chkFreeShipping.Checked = False And chkFixedShipping1.Checked = False And chkFixedShipping2.Checked = False Then
                errorMessage = errorMessage & "Please select atleast one checkbox.<br/>"
            End If

            If (chkFreeShipping.Checked = False AndAlso (CCommon.ToLong(txtFreeShippingAmount.Text) > 0 Or CCommon.ToLong(ddlFreeShippingCountry.SelectedValue) > 0)) Or (chkFixedShipping1.Checked = False AndAlso CCommon.ToLong(txtFixShipping1OrderAmount.Text) > 0) Or (chkFixedShipping2.Checked = False AndAlso CCommon.ToLong(txtFixShipping2OrderAmount.Text) > 0) Then
                errorMessage = errorMessage & "Please select the checkbox in order to save the shipping amount.<br/>"
            End If

            If String.IsNullOrEmpty(errorMessage) Then
                objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))

                objPromotionOffer.IsEnableFreeShipping = chkFreeShipping.Checked
                objPromotionOffer.FreeShippingOrderAmount = CCommon.ToInteger(txtFreeShippingAmount.Text)
                objPromotionOffer.FreeShippingCountry = CCommon.ToLong(ddlFreeShippingCountry.SelectedValue)
                objPromotionOffer.IsEnableFixShipping1 = chkFixedShipping1.Checked
                objPromotionOffer.FixShipping1OrderAmount = CCommon.ToInteger(txtFixShipping1OrderAmount.Text)
                objPromotionOffer.FixShipping1Charge = CCommon.ToInteger(txtFixShipping1Charge.Text)
                objPromotionOffer.IsEnableFixShipping2 = chkFixedShipping2.Checked
                objPromotionOffer.FixShipping2OrderAmount = CCommon.ToInteger(txtFixShipping2OrderAmount.Text)
                objPromotionOffer.FixShipping2Charge = CCommon.ToInteger(txtFixShipping2Charge.Text)

                Return objPromotionOffer.ManageShippingPromotions
            Else
                ShowMessage(errorMessage)
                Return 0
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Function

    Private Sub DisplayError(ByVal ex As Exception)
        Try
            DirectCast(Page.Master, ECommerceMenuMaster).ThrowError(ex)
        Catch exp As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

#End Region
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmShippingCriteria.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

End Class