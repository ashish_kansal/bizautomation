﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPackagingRules.aspx.vb"
    Inherits=".frmPackagingRules" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Packaging Rules</title>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return Save()"
                            Visible="false" />
                        &nbsp;
                        <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close"
                            OnClientClick="return Save()" />
                        &nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="button"
                            UseSubmitBehavior="false" />
                        &nbsp;
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <center>
        <span style="color: Red">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal></span></center>
    <asp:HiddenField ID="hdnShipRuleID" runat="server"></asp:HiddenField>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Packaging Rule
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table3" Width="100%" runat="server" BorderWidth="1" Height="350" GridLines="None"
        CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="normal1" align="right">
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rule
                                            Name :</b>
                                    </td>
                                    <td class="signup">
                                        <asp:TextBox ID="txtRuleName" runat="server" Width="200" CssClass="required {required:true,number:false, messages:{required:'Enter Rule Name! '}}"
                                            MaxLength="100"></asp:TextBox>
                                        <span style="color: Red">*</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="trMain">
                        <td>
                            <table>
                                <tr>
                                    <td align="right" class="normal1">
                                        <b>Package Type :</b>
                                    </td>
                                    <td class="normal1">
                                        <asp:DropDownList runat="server" ID="ddlPackageType">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="normal1">
                                        <b>Quantity of item it can hold :</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtFrom" Width="50px" CssClass="required_integer {required:false,number:true, messages:{required:'Enter Amount! ',number:'Please provide valid value!'}}"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="StepClass" align="right">
                                        <b>Shipping Class :</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlShipClass" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
