﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingLabelConfiguration.aspx.vb"
    Inherits=".frmShippingLabelConfiguration" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Label Automation Configuration</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <center>
        <span style="color: Red">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
        </span>
    </center>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Shipping Label Automation Configuration
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table cellpadding="2" cellspacing="2" border="0" >
        <tr>
            <td valign="top" align="right" >
                Show Orders with selected Order status in "Shipping Fulfillment" page(can be found in Opportunities
                tab)
            </td>
            <td>
                <%--<asp:CheckBoxList ID="chkOrderStatus" runat="server" CellSpacing="2" CellPadding="2"
                    RepeatDirection="Horizontal">
                </asp:CheckBoxList>--%>
                <div style="max-height: 250px; overflow: scroll; max-width: 400px">
                    <asp:CheckBoxList ID="chkOrderStatus" runat="server" CellSpacing="2" CellPadding="2"
                        RepeatDirection="Vertical" BorderWidth="1px">
                    </asp:CheckBoxList>
                </div>
            </td>
        </tr>
        <tr>
            <td align="right">
                Generate shipping labels, When order status is set to
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlReadyToShipStatus">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                On successful shipping label generation, update order status to
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlSuccessStatus">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                On error in shipping label generation, update order status to
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlErrorStatus">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                Show orders with selected BizDoc in Shipping Fulfillment
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlBizDocs">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
