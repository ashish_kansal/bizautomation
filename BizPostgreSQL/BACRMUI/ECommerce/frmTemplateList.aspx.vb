﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Partial Public Class frmTemplateList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            
            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If Not IsPostBack Then
                
                BindData()
                hplNew.Attributes.Add("onclick", "NewTemplate();")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindData()
        Try
            If Session("SiteID") > 0 Then
                Dim objSite As New Sites
                Dim dtTemplate As DataTable
                objSite.SiteID = Session("SiteID")
                objSite.DomainID = Session("DomainID")
                dtTemplate = objSite.GetSiteTemplates()
                dgTemplates.DataSource = dtTemplate
                dgTemplates.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgTemplates_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTemplates.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../ECommerce/frmTemplate.aspx?TemplateID=" & e.Item.Cells(0).Text)
            If e.CommandName = "Delete" Then
                Dim objSite As New Sites
                objSite.TemplateID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                objSite.DeleteSiteTemplates()
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgTemplates_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTemplates.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

End Class