﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Public Class frmCategoryProfile
    Inherits BACRMPage

#Region "Memeber Variable"
    Private numCategoryProfileID As Long

    Private objSite As Sites
    Private objCategoryProfile As CategoryProfile
#End Region

#Region "Constructor"
    Sub New()
        objSite = New Sites
        objCategoryProfile = New CategoryProfile
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""

            If Not Page.IsPostBack Then
                GetSites()
                numCategoryProfileID = CCommon.ToLong(GetQueryStringVal("numCategoryProfileID"))

                If numCategoryProfileID > 0 Then
                    GetCategoryProfile()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub GetSites()
        Try
            objSite.DomainID = CCommon.ToLong(Session("DomainID"))
            objSite.UserCntID = CCommon.ToLong(Session("UserContactID"))
            Dim dtSites As DataTable = objSite.GetSites()

            If Not dtSites Is Nothing AndAlso dtSites.Rows.Count > 0 Then
                chkblSites.DataSource = dtSites
                chkblSites.DataTextField = "vcSiteName"
                chkblSites.DataValueField = "numSiteID"
                chkblSites.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub GetCategoryProfile()
        Try
            objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
            objCategoryProfile.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objCategoryProfile.ID = numCategoryProfileID
            objCategoryProfile.GetByIDWithSites()

            If objCategoryProfile.ID = 0 Then
                lblException.Text = "Category profile doesn't exists."
                btnSaveClose.Visible = False
            Else
                hdnCategoryProfileID.Value = objCategoryProfile.ID
                txtCategoryProfile.Text = objCategoryProfile.vcName

                If Not objCategoryProfile.ListCategoryProfileSites Is Nothing Then
                    For Each objCategoryProfileSite As CategoryProfileSite In objCategoryProfile.ListCategoryProfileSites
                        If Not chkblSites.Items.FindByValue(objCategoryProfileSite.SiteID) Is Nothing Then
                            chkblSites.Items.FindByValue(objCategoryProfileSite.SiteID).Selected = True
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            If Not String.IsNullOrEmpty(txtCategoryProfile.Text) Then
                objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                objCategoryProfile.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objCategoryProfile.ID = CCommon.ToLong(hdnCategoryProfileID.Value)
                objCategoryProfile.vcName = txtCategoryProfile.Text

                Dim vcSiteIds As New ArrayList

                For Each objListItem As ListItem In chkblSites.Items
                    If objListItem.Selected Then
                        vcSiteIds.Add(objListItem.Value)
                    End If
                Next

                objCategoryProfile.Save(String.Join(",", vcSiteIds.ToArray()))
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SaveClose", "ReloadAndClose();", True)
            Else
                lblException.Text = "Select category profile name."
                txtCategoryProfile.Focus()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

    
End Class