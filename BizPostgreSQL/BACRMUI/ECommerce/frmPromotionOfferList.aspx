﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPromotionOfferList.aspx.vb"
    Inherits=".frmPromotionOfferList" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Promotions & Offers</title>

    <script type="text/javascript">
        function New() {
            window.location.href = "frmPromotionOfferManage.aspx";
        }
        function openOrderBasedPromotionWindow() {
            window.location.href = "frmPromotionOrderBasedManage.aspx";
        }
        function openPostOrderIncentivesWindow() {
            window.location.href = "frmPostOrderIncentives.aspx";
        }
        function openGiftCardsWindow() {
            window.location.href = "frmGiftCards.aspx";
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton runat="server" ID="lnkbtnOrderBasedPromotions" OnClientClick="return openOrderBasedPromotionWindow();" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Order based Promotions</asp:LinkButton>
                &nbsp;&nbsp;
                <asp:LinkButton runat="server" OnClientClick="return New();" ID="btnNew" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Item based Promotions</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Promotions & Offers&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmPromotionOfferList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <asp:DataGrid ID="dgOfferList" runat="server" Width="100%" CssClass="table table-striped table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vcProName" HeaderText="Name" ItemStyle-Width="200"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vcPromotionType" HeaderText="Promotion Type" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vcDateValidation" HeaderText="Date Validation" ItemStyle-Wrap="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vcCouponCode" HeaderText="Coupon Code"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vcPromotionRule" HeaderText="Promotion Rule"></asp:BoundColumn>
                    <asp:TemplateColumn ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnIsOrderPromotion" runat="server" Value='<%# Eval("IsOrderBasedPromotion")%>' />
                            <asp:HiddenField ID="hdnCanEditOrDelete" runat="server" Value='<%# Eval("bitCanEdit")%>' />
                            <asp:LinkButton ID="lkbEnableDisable" runat="server" CssClass="btn btn-primary btn-xs" CommandArgument='<%# Eval("numProId")%>' CommandName="Enable/Disable">Enable Rule</asp:LinkButton>
                            <asp:LinkButton ID="lkbEdit" runat="server" CssClass="btn btn-info btn-xs" CommandArgument='<%# Eval("numProId")%>' CommandName="Edit"><i class="fa fa-pencil"></i></asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandArgument='<%# Eval("numProId")%>' CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</asp:Content>
