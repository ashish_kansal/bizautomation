﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Telerik.Web.UI

Public Class frmManageItemVisibility
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                hdnSite.Value = GetQueryStringVal("numSiteID")

                If CCommon.ToLong(hdnSite.Value) > 0 Then
                    FillDropdowns()
                    BindGridView()
                Else
                    DisplayError("Not able to find site.")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillDropdowns()
        Try
            objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, CCommon.ToLong(Session("DomainID")))
            objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, CCommon.ToLong(Session("DomainID")))
            objCommon.sb_FillComboFromDBwithSel(radCmbItemClassifications, 36, CCommon.ToLong(Session("DomainID")))
            radCmbItemClassifications.Items.Remove(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindGridView()
        Try
            Dim objSites As New Sites
            objSites.SiteID = CCommon.ToLong(hdnSite.Value)
            gvRelationshipProfile.DataSource = objSites.GetSiteRelationProfileSettings()
            gvRelationshipProfile.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal message As String)
        Try
            lblError.Text = message
            divError.Style.Add("display", "")
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            If CCommon.ToLong(ddlRelationship.SelectedValue) = 0 Then
                DisplayError("Select Relationship")
                ddlRelationship.Focus()
            ElseIf CCommon.ToLong(ddlProfile.SelectedValue) = 0 Then
                DisplayError("Select Profile")
                ddlProfile.Focus()
            ElseIf radCmbItemClassifications.CheckedItems.Count = 0 Then
                DisplayError("Select Item Classification(s)")
                radCmbItemClassifications.Focus()
            Else
                Dim ids As String = ""

                For Each item As RadComboBoxItem In radCmbItemClassifications.CheckedItems
                    ids = If(ids.Length = 0, "", ids & ",") & item.Value
                Next

                Dim objSites As New Sites
                objSites.SiteID = CCommon.ToLong(hdnSite.Value)
                objSites.InsertSiteRelationProfileSettings(ddlRelationship.SelectedValue, ddlProfile.SelectedValue, ids)

                BindGridView()
            End If
        Catch ex As Exception
            If ex.Message.Contains("SITE_DOES_NOT_EXISTS") Then
                DisplayError("Site does not exists")
            ElseIf ex.Message.Contains("RELATIONSHIP_REQUIRED") Then
                DisplayError("Select Relationship")
            ElseIf ex.Message.Contains("PROFILE_REQUIRED") Then
                DisplayError("Select Profile")
            ElseIf ex.Message.Contains("ITEMCLASSIFICATION_REQUIRED") Then
                DisplayError("Select Item Classification(s)")
            ElseIf ex.Message.Contains("DUPLICATE_SELECTION") Then
                DisplayError("Selected classification(s) are already added for the selected Customer Relationship / Profile")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Try
            Dim ids As String = ""

            For Each row As GridViewRow In gvRelationshipProfile.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)

                If chkSelect.Checked Then
                    ids = If(ids.Length = 0, "", ids & ",") & gvRelationshipProfile.DataKeys(row.RowIndex).Value
                End If
            Next

            If ids.Length = 0 Then
                DisplayError("Select atleast one row")
            Else
                Dim objSites As New Sites
                objSites.SiteID = CCommon.ToLong(hdnSite.Value)
                objSites.DeleteSiteRelationProfileSettings(ids)

                BindGridView()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

End Class