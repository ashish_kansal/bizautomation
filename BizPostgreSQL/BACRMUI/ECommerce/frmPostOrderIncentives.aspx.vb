﻿Imports BACRM.BusinessLogic.Promotion
Imports BACRM.BusinessLogic.Common

Public Class frmPostOrderIncentives
    Inherits BACRMPage


#Region "Member Variables"
    Dim objPromotionOffer As PromotionOffer
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If Not IsPostBack Then
                hfProId.Value = CCommon.ToLong(GetQueryStringVal("ProId"))
                If CCommon.ToLong(hfProId.Value) > 0 Then
                    divSaleOptions.Visible = True
                Else
                    divSaleOptions.Visible = False
                    btnSaveClose.Visible = False
                End If

                'hplIncentiveSale.Attributes.Add("onclick", "return OpenItemSelectionWindow(1,5," & CCommon.ToLong(hfProId.Value) & "," & txtSubTotal.Text & ")")
                hplIncentiveSale.Attributes.Add("onclick", "return OpenItemSelectionWindow(1,5," & CCommon.ToLong(hfProId.Value) & ")")
                If CCommon.ToLong(hfProId.Value) > 0 Then
                    LoadDetails()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmPromotionOrderBasedManage.aspx?ProId=" & lngPromotionId, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmPromotionOfferList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmPromotionOfferList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub LoadDetails()
        Try
            objPromotionOffer = New PromotionOffer
            objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim ds As DataSet = objPromotionOffer.GetPromotionOfferOrderBasedItems()
            Dim dtProName = ds.Tables(0)
            Dim dtTable = ds.Tables(1)

            If dtProName IsNot Nothing AndAlso dtProName.Rows.Count > 0 Then
                txtOfferName.Text = CCommon.ToString(dtProName.Rows(0).Item("vcProName"))
            End If

            If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then

                If dtTable.Rows(0)("cSaleType") IsNot Nothing AndAlso dtTable.Rows(0)("cSaleType") <> "" Then
                    If Convert.ToString(dtTable.Rows(0)("cSaleType")) = "I" Then
                        rdbtnIncentiveSale.Checked = True
                        txtSubTotal.Text = Convert.ToString(dtTable.Rows(0)("fltSubTotal"))
                        LoadItemSelectionDetails(dtTable)
                    ElseIf Convert.ToString(dtTable.Rows(0)("cSaleType")) = "C" Then
                        rdBtnCrossSale.Checked = True
                    End If
                End If
            Else
                gvItemsForRule.Visible = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function Save() As Long
        Try
            Dim errorMessage As String = ""

            If CCommon.ToLong(hfProId.Value) > 0 Then
                'If String.IsNullOrEmpty(errorMessage) Then
                If rdbtnIncentiveSale.Checked = True AndAlso txtSubTotal.Text = "" Then
                    errorMessage = "Please enter sub-total amount. <br/>"
                End If
                If String.IsNullOrEmpty(errorMessage) Then
                    If txtSubTotal.Text <> "" Then
                        Dim subTotal = CCommon.ToDecimal(txtSubTotal.Text)
                        Return Update(subTotal, 0)
                    End If
                    'End If
                Else
                    ShowMessage(errorMessage)
                    Return 0
                End If
            Else
                If String.IsNullOrEmpty(txtOfferName.Text) Then
                    ShowMessage("Promotion name is required.")
                    txtOfferName.Focus()
                    Return 0
                Else
                    Dim subTotal = CCommon.ToDecimal(txtSubTotal.Text)
                    Return Update(subTotal, 0)
                End If
            End If
        Catch ex As Exception
            If ex.Message = "SELECT_ITEMS_FOR_PROMOTIONS" Then
                ShowMessage("Select items to execute promotion.")
            ElseIf ex.Message = "DUPLICATE_SUBTOTAL_AMOUNT" Then
                ShowMessage("The rule with the same sub total amount already exists.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Function

    Private Function Update(subTotal As Decimal, Optional byteMode As Short = 0, Optional oldSubTotal As Decimal = 0) As Long
        objPromotionOffer = New PromotionOffer
        objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
        objPromotionOffer.PromotionName = txtOfferName.Text.Trim()
        objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
        objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))
        objPromotionOffer.OldSubTotal = oldSubTotal
        objPromotionOffer.Mode = byteMode
        'objPromotionOffer.PromotionOfferBasedID = promotionOfferBasedID
        'If txtSubTotal.Text <> "" Then
        '    objPromotionOffer.SubTotal = CCommon.ToDecimal(txtSubTotal.Text)
        'Else
        '    objPromotionOffer.SubTotal = Nothing
        'End If
        If subTotal <> 0 Then
            objPromotionOffer.SubTotal = subTotal
        Else
            objPromotionOffer.SubTotal = Nothing
        End If
        If rdBtnCrossSale.Checked = True Then
            objPromotionOffer.SaleType = "C"
        Else
            objPromotionOffer.SaleType = "I"
        End If

        Return objPromotionOffer.ManagePromotionOfferOrderBased()
    End Function

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As Exception)
        Try
            DirectCast(Page.Master, ECommerceMenuMaster).ThrowError(ex)
        Catch exp As Exception

        End Try
    End Sub

    Private Sub LoadItemSelectionDetails(ByVal dt As DataTable)
        Try
            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                'dt = dt.AsEnumerable().[Select](Function(row) New With {Key .attribute1_name = row.Field(Of String)("fltSubTotal")}).Distinct()
                'Dim distinctValues = dt.AsEnumerable().[Select](Function(row) New With {
                '             Key .fltSubTotal = row.Field(Of Double)("fltSubTotal")
                '            }).Distinct()

                'Dim distinctValues = (From p In dt
                '                      Select p).Distinct()
                gvItemsForRule.Visible = True
                gvItemsForRule.DataSource = dt
                gvItemsForRule.DataBind()
            Else
                gvItemsForRule.Visible = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub lnkBtnAddRule_Click(sender As Object, e As EventArgs)
        rdbtnIncentiveSale.Checked = True
        Dim errorMessage As String = ""
        If rdbtnIncentiveSale.Checked = True AndAlso txtSubTotal.Text = "" Then
            errorMessage = "Please enter sub-total amount. <br/>"
        End If

        If String.IsNullOrEmpty(errorMessage) Then
            Save()
            LoadDetails()
            txtSubTotal.Text = String.Empty
        Else
            ShowMessage(errorMessage)
            Return
        End If
    End Sub

    'Protected Sub gvItemsForRule_RowDataBound(sender As Object, e As GridViewRowEventArgs)
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim drv As DataRowView = e.Row.DataItem
    '            If (drv IsNot Nothing) Then

    'Dim monListPriceCell As TableCell = e.Row.Cells(3)
    'Dim subTotalCell As TableCell = e.Row.Cells(0)

    'If CCommon.ToString(drv.Row("monListPrice")).Contains(",") Then
    '    Dim str = CCommon.ToString(drv.Row("monListPrice"))
    '    Dim newStr = String.Empty
    '    Dim items As String() = str.Split(New Char() {","c, " "c}, StringSplitOptions.RemoveEmptyEntries)
    '    If items.Length > 0 Then

    '        For Each r As String In items
    '            newStr = newStr + ", " + CCommon.ToString(Session("Currency")) + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(r))
    '        Next
    '    End If
    '    newStr = newStr.Substring(1)
    '    monListPriceCell.Text = newStr
    'Else
    '    Dim price = CCommon.ToDecimal(drv.Row("monListPrice"))
    '    monListPriceCell.Text = CCommon.ToString(Session("Currency")) + " " + CCommon.ToString(Math.Round(price, 2))
    'End If
    'Dim subTotal = CCommon.ToDecimal(drv.Row("fltSubTotal"))
    'subTotalCell.Text = CCommon.ToString(Session("Currency")) + " " + CCommon.ToString(Math.Round(subTotal, 2))
    'End If
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    'Protected Sub gvItemsForRule_RowCommand(sender As Object, e As GridViewCommandEventArgs)
    '    Try
    '        If e.CommandName = "Delete" Then
    '            objPromotionOffer = New PromotionOffer
    '            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
    '            objPromotionOffer.PromotionID = e.CommandArgument
    '            objPromotionOffer.DeleteOrderBasedPromotionRule()
    '            LoadDetails()
    '        ElseIf e.CommandName = "Update" Then
    '            Dim row As GridViewRow = CType(((CType(e.CommandSource, Control)).NamingContainer), GridViewRow)
    '            Dim txtSubTotal = CType(row.FindControl("txtTotalAmount"), TextBox)
    '            Dim subTotal = 0
    '            If txtSubTotal IsNot Nothing Then
    '                subTotal = CCommon.ToDecimal(txtSubTotal.Text)
    '                If subTotal <> "" Then
    '                    Update(subTotal)
    '                End If
    '            End If

    '            LoadDetails()
    '        End If

    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex)
    '    End Try
    'End Sub

    Protected Sub gvItemsForRule_RowEditing(sender As Object, e As GridViewEditEventArgs)
        gvItemsForRule.EditIndex = e.NewEditIndex
        LoadDetails()
    End Sub

    Protected Sub gvItemsForRule_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs)
        gvItemsForRule.EditIndex = -1
        LoadDetails()
    End Sub

    Protected Sub gvItemsForRule_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Try
            'Dim _proId As Long = CCommon.ToLong(gvItemsForRule.DataKeys(e.RowIndex).Value)
            Dim _subTotal = gvItemsForRule.Rows(e.RowIndex).Cells(0).Text

            objPromotionOffer = New PromotionOffer
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.SubTotal = CCommon.ToDecimal(_subTotal)
            objPromotionOffer.DeleteOrderBasedPromotionRule()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Protected Sub gvItemsForRule_RowUpdating(sender As Object, e As GridViewUpdateEventArgs)
        Try
            'Dim _proOrderBasedID As Long = CCommon.ToLong(gvItemsForRule.DataKeys(e.RowIndex).Value)
            'Dim txtSubTotal = CType(gvItemsForRule.Rows(e.RowIndex).FindControl("txtTotalAmount"), TextBox)
            Dim row As GridViewRow = CType(gvItemsForRule.Rows(e.RowIndex), GridViewRow)
            Dim txtSubTotal As TextBox = CType(row.Cells(0).Controls(0), TextBox)
            Dim txtOldSubTotal As TextBox = CType(row.Cells(4).Controls(0), TextBox)

            If txtSubTotal IsNot Nothing Then
                Dim subTotal = CCommon.ToDecimal(txtSubTotal.Text)
                Dim oldSubTotal = CCommon.ToDecimal(txtOldSubTotal.Text)
                If subTotal <> 0 Then
                    'Update(subTotal, _proOrderBasedID)
                    Update(subTotal, 1, oldSubTotal)
                End If
            End If
            gvItemsForRule.EditIndex = -1
            LoadDetails()
        Catch ex As Exception
            If ex.Message = "SELECT_ITEMS_FOR_PROMOTIONS" Then
                ShowMessage("Select items to execute promotion.")
            ElseIf ex.Message = "DUPLICATE_SUBTOTAL_AMOUNT" Then
                ShowMessage("The rule with the same sub total amount already exists.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub
End Class