﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI

Public Class frmShippingRule
    Inherits BACRMPage
    Dim lngRuleID As Long

    Dim objShippingRule As ShippingRule
#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If CCommon.ToLong(Session("DomainID")) > 0 Then
                lngRuleID = CCommon.ToLong(GetQueryStringVal("RuleID"))

                If Not IsPostBack Then
                    objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))

                    If CCommon.ToLong(Session("DefCountry")) > 0 Then
                        If Not ddlCountry.Items.FindByValue(CCommon.ToString(Session("DefCountry"))) Is Nothing Then
                            ddlCountry.Items.FindByValue(CCommon.ToString(Session("DefCountry"))).Selected = True
                            FillState(ddlState, ddlCountry.SelectedValue, Session("DomainID"))
                        End If
                    End If
                    Dim dt As DataTable
                    dt = objCommon.GetMasterListItems(36, Session("DomainID"))
                    rcbAssetItemClass.DataSource = dt
                    rcbAssetItemClass.DataTextField = "vcData"
                    rcbAssetItemClass.DataValueField = "numListItemID"
                    rcbAssetItemClass.DataBind()


                    BindSites()
                    BindRelationshipNProfile()
                    BindWarehouses()

                    If lngRuleID > 0 Then
                        hdnShipRuleID.Value = lngRuleID
                        pnlSel.Visible = True
                        btnSaveClose.Visible = True
                        LoadDetails()
                        BindServiceTypes()
                        trShipping.Visible = True
                        divCountryState.Visible = True
                        tdClone.Visible = True
                    Else
                        trShipping.Visible = False
                        divCountryState.Visible = False
                        btnSaveClose.Visible = False
                        tdClone.Visible = False
                    End If
                End If
            Else
                Response.Redirect("../admin/authentication.aspx?mesg=AC&Module=0&Permission=0")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            Dim i As Integer = 0
            If (dgFixedShippingQuote.Items.Count > 0) Then

                Dim TotalCount As Integer = dgFixedShippingQuote.Items.Count
                Dim ToValue As Integer
                Dim strClassification As String
                Dim IsClassification As Boolean = True
                Dim strCurrentClassificationData As String
                strCurrentClassificationData = GenerateCheckedItemTextString(rcbAssetItemClass)
                For Each drow As DataGridItem In dgFixedShippingQuote.Items
                    i = i + 1
                    If (strCurrentClassificationData <> "") Then
                        ToValue = CCommon.ToInteger(CType(drow.FindControl("txtTo"), TextBox).Text)
                        strClassification = CCommon.ToString(CType(drow.FindControl("lblItemClasification"), Label).Text)
                        If (CCommon.ToInteger(txtFrom.Text) <= ToValue AndAlso strClassification.Split(",").Where(Function(s) strCurrentClassificationData.Contains(s) AndAlso s <> "").Count() > 0) Then
                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidateFrom", "alert(""From value entered is already included."");", True)
                            Exit Sub
                        End If
                    Else
                        If (i = TotalCount) Then
                            ToValue = CCommon.ToInteger(CType(drow.FindControl("txtTo"), TextBox).Text)
                            If (CCommon.ToInteger(txtFrom.Text) <= ToValue) Then
                                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidateFrom", "alert(""From value entered is already included."");", True)
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If

            'Add new Shipping Caption
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.RuleID = lngRuleID
            objShippingRule.byteMode = 0
            objShippingRule.ServiceTypeID = 0
            objShippingRule.ServiceName = ""    'txtShippingCaption.Text.Trim
            objShippingRule.From = CCommon.ToInteger(txtFrom.Text.Trim)
            objShippingRule.ToValue = CCommon.ToInteger(txtTo.Text.Trim)
            objShippingRule.MarkUp = 0
            objShippingRule.MarkUpType = 0
            objShippingRule.Rate = txtShippingRate.Text.Trim
            objShippingRule.Enabled = True
            objShippingRule.ItemClassification = GenerateCheckedItemString(rcbAssetItemClass)
            objShippingRule.ManageShippingServiceTypes()

            ' txtShippingCaption.Text = ""
            txtTo.Text = ""
            txtFrom.Text = ""
            txtShippingRate.Text = ""
            rcbAssetItemClass.ClearCheckedItems()
            BindServiceTypes()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function GenerateCheckedItemString(ByVal radComboBox As RadComboBox) As String
        Dim strTransactionType As String
        For Each item As RadComboBoxItem In radComboBox.CheckedItems
            If CCommon.ToLong(item.Value) > 0 Then
                strTransactionType = strTransactionType & item.Value & ","
            End If
        Next
        Return strTransactionType
    End Function
    Function GenerateCheckedItemTextString(ByVal radComboBox As RadComboBox) As String
        Dim strTransactionType As String
        For Each item As RadComboBoxItem In radComboBox.CheckedItems
            If CCommon.ToString(item.Text) <> "" Then
                strTransactionType = strTransactionType & item.Text & ","
            End If
        Next
        Return strTransactionType
    End Function
    Private Sub btnAddState_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddState.Click
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
            objShippingRule.DomainID = CCommon.ToLong(Session("DomainID"))

            objShippingRule.RuleID = lngRuleID
            objShippingRule.CountryID = ddlCountry.SelectedValue
            objShippingRule.StateID = ddlState.SelectedValue

            objShippingRule.vcZipPostalRange = txtZipCode.Text

            objShippingRule.ManageShippingRuleStates()
            litMessage.Text = ""
            BindStateList()
        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP") Then
                litMessage.Text = "Rule already exists for selected Relationship,Profile and state & zip(s)."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lngRuleID = Save()
            If lngRuleID > 0 Then
                Response.Redirect("../ECommerce/frmShippingRule.aspx?RuleID=" & lngRuleID, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            lngRuleID = Save()
            If lngRuleID > 0 Then
                Response.Redirect("../ECommerce/frmShippingRuleList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmShippingRuleList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub dgFixedShippingQuote_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            If e.CommandName = "Delete" Then
                litMessage.Text = ""
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
                objShippingRule.ServiceTypeID = CCommon.ToLong(e.CommandArgument)
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.byteMode = 1 'delete
                objShippingRule.ManageShippingServiceTypes()
                BindServiceTypes()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Protected Sub dgFixedShippingQuote_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFixedShippingQuote.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                CType(e.Item.FindControl("btnDelete"), Button).Visible = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Protected Sub dgExcludedStates_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgExcludedStates.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                litMessage.Text = ""
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.StateID = CCommon.ToLong(e.CommandArgument)
                objShippingRule.CountryID = CCommon.ToLong(CType(e.Item.FindControl("hdnCountry"), HiddenField).Value)
                objShippingRule.RuleID = lngRuleID

                objShippingRule.DeleteShippingStateRule()
                BindStateList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Try
            FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Methods"
    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()

            objShippingRule.RuleID = lngRuleID
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.byteMode = 0

            Dim ds As DataSet = objShippingRule.GetShippingRule
            dtTable = ds.Tables(0)

            If dtTable.Rows.Count > 0 Then
                txtRuleName.Text = dtTable.Rows(0).Item("vcRuleName")
                txtDescription.Text = dtTable.Rows(0).Item("vcDescription")

                rblBasedOn.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("tintBasedOn"))
                ' rblShippingMethod.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("tinShippingMethod"))
                If CCommon.ToLong(dtTable.Rows(0).Item("tinShippingMethod")) = 1 Then
                    ' rbFixedShippingQuotes.Checked = True
                    If CCommon.ToInteger(dtTable.Rows(0)("tintFixedShippingQuotesMode")) > 0 Then
                        'If Not ddlShippingBasedOn.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("tintFixedShippingQuotesMode"))) Is Nothing Then
                        '    ddlShippingBasedOn.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("tintFixedShippingQuotesMode"))).Selected = True
                        'End If
                    End If
                ElseIf CCommon.ToLong(dtTable.Rows(0).Item("tinShippingMethod")) = 2 Then
                    ' rbRealTimeShippingQuotes.Checked = True
                    'If CCommon.ToInteger(dtTable.Rows(0)("tintFixedShippingQuotesMode")) > 0 Then
                    '    If Not ddlRealTimeShippingBasedOn.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("tintFixedShippingQuotesMode"))) Is Nothing Then
                    '        ddlRealTimeShippingBasedOn.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("tintFixedShippingQuotesMode"))).Selected = True
                    '    End If
                    'End If
                End If


                ViewState("ShippingMethod") = 2
                'Else
                '    pnlRealTimeShippingQuotes.Visible = False
                'End If

                'If CCommon.ToInteger(dtTable.Rows(0)("tintTaxMode")) > 0 Then
                '    ddlTax.ClearSelection()
                '    ddlTax.Items.FindByValue(CCommon.ToInteger(dtTable.Rows(0)("tintTaxMode"))).Selected = True
                'End If

                rblStateInclude.SelectedValue = CCommon.ToLong(dtTable.Rows(0)("tintIncludeType"))

                If (CCommon.ToBool(dtTable.Rows(0)("bitFreeShipping")) = True) Then
                    chkbxFreeShipping.Checked = True
                    txtFreeShipping.Text = dtTable.Rows(0)("FreeShippingOrderAmt").ToString()
                Else
                    chkbxFreeShipping.Checked = False
                End If
                ddlRuleRelationShip.SelectedValue = dtTable.Rows(0)("numRelationship").ToString()
                ddlRuleProfile.SelectedValue = dtTable.Rows(0)("numProfile").ToString()

                Dim strWarehouseIds() As String = CCommon.ToString(dtTable.Rows(0)("numWareHouseID")).Split(New String() {","}, StringSplitOptions.None)
                rblCommissionType.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("bitItemClassification"))
                ddlSites.SelectedValue = dtTable.Rows(0)("numSiteID").ToString()
                For k As Integer = 0 To strWarehouseIds.Length - 1
                    For Each item As RadComboBoxItem In radcmbWarehouse.Items
                        If (strWarehouseIds(k).ToString() = item.Value) Then
                            item.Checked = True
                        End If
                    Next
                Next

                BindServiceTypes()
                BindStateList()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function GetSiteName() As String
        Try
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = CCommon.ToDecimal(If(Session("SiteID") = Nothing Or Session("SiteID") = 0, -1, Session("SiteID")))
            objSite.DomainID = If(Session("SiteID") = Nothing Or Session("SiteID") = 0, -1, Session("DomainID"))
            dtSite = objSite.GetSites()
            If dtSite.Rows.Count > 0 Then
                Return CCommon.ToString(dtSite.Rows(0)("vcSiteName"))
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub BindServiceTypes()
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.RuleID = lngRuleID
            objShippingRule.ServiceTypeID = 0

            objShippingRule.byteMode = 1
            dgFixedShippingQuote.DataSource = objShippingRule.GetShippingServiceType()
            dgFixedShippingQuote.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function Save() As Long
        Try

            If (chkbxFreeShipping.Checked) And dgFixedShippingQuote.Items.Count > 0 Then
                If (CCommon.ToInteger(txtFreeShipping.Text) <= (CCommon.ToInteger(CType(dgFixedShippingQuote.Items(dgFixedShippingQuote.Items.Count - 1).FindControl("txtTo"), TextBox).Text))) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidateFrom", "alert(""Free Shipping amount must be greater than amount included in Shipping charges."");", True)
                    Exit Function
                End If
            End If

            litMessage.Text = ""
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()

            objShippingRule.RuleID = lngRuleID
            objShippingRule.RuleName = txtRuleName.Text
            objShippingRule.Description = txtDescription.Text
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.SiteID = If(Session("SiteID") = Nothing Or Session("SiteID") = 0, -1, Session("SiteID"))
            objShippingRule.BasedOn = rblBasedOn.SelectedValue
            objShippingRule.ShippingMethod = 1
            objShippingRule.Type = rblStateInclude.SelectedValue
            objShippingRule.TaxMode = 1
            objShippingRule.bitFreeshipping = chkbxFreeShipping.Checked
            objShippingRule.FreeShippingAmt = CCommon.ToInteger(txtFreeShipping.Text)
            objShippingRule.numRelationship = ddlRuleRelationShip.SelectedValue
            objShippingRule.numProfile = ddlRuleProfile.SelectedValue
            objShippingRule.bitItemClassification = rblCommissionType.SelectedValue
            objShippingRule.numSiteId = ddlSites.SelectedValue

            Dim strWarehouseIds As String

            For Each item As RadComboBoxItem In radcmbWarehouse.Items
                If (item.Checked) Then
                    strWarehouseIds = strWarehouseIds + item.Value.ToString + ","
                End If
            Next

            If (strWarehouseIds.Length > 0) Then
                objShippingRule.numWarehouseId = strWarehouseIds.Remove(strWarehouseIds.LastIndexOf(","))
            End If

            Dim FromRate As Double
            Dim PrevToRate As Double
            Dim PrevClassification As String = ""
            Dim CurrClassification As String = ""
            If dgFixedShippingQuote.Items.Count Then
                For i = 0 To dgFixedShippingQuote.Items.Count - 1
                    If (i > 0) Then
                        FromRate = CCommon.ToDouble(CType(dgFixedShippingQuote.Items(i).FindControl("txtFrom"), TextBox).Text)
                        PrevToRate = CCommon.ToDouble(CType(dgFixedShippingQuote.Items(i - 1).FindControl("txtTo"), TextBox).Text)
                        CurrClassification = CCommon.ToString(CType(dgFixedShippingQuote.Items(i).FindControl("lblItemClasification"), Label).Text)
                        If CurrClassification = "" Then
                            If FromRate <= PrevToRate Then
                                litMessage.Text = "From Value must be less than To Value"
                                Exit Function
                            End If
                        Else
                            If FromRate <= PrevToRate AndAlso PrevClassification.Split(",").Where(Function(s) CurrClassification.Contains(s) AndAlso s <> "").Count() > 0 Then
                                litMessage.Text = "From Value must be less than To Value"
                                Exit Function
                            End If
                        End If
                        PrevClassification = PrevClassification & CurrClassification & ","
                    End If
                Next
            End If

            Dim lngID As Long = objShippingRule.ManageShippingRule()
            If lngID > 0 Then
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.byteMode = 2 'Update
                objShippingRule.ServiceTypeID = 1
                objShippingRule.Str = GetItemsForFixedShippingQuotes()
                objShippingRule.ManageShippingServiceTypes()
            End If

            Return lngID

        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "Rule Name already exists."
            ElseIf ex.Message = "DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP" Then
                litMessage.Text = "Rule already exists for selected Relationship,Profile and state & zip(s)."
            Else
                Throw ex
            End If
        End Try
    End Function

    Function GetItemsForFixedShippingQuotes() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dr As DataRow
            CCommon.AddColumnsToDataTable(dt, "numServiceTypeID,vcServiceName,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled")

            For Each gvr As DataGridItem In dgFixedShippingQuote.Items
                dr = dt.NewRow()
                dr("numServiceTypeID") = gvr.Cells(0).Text.Trim
                dr("vcServiceName") = ""   'CType(gvr.FindControl("txtShippingCaption"), TextBox).Text
                dr("intFrom") = CType(gvr.FindControl("txtFrom"), TextBox).Text
                dr("intTo") = CType(gvr.FindControl("txtTo"), TextBox).Text
                dr("fltMarkup") = "0.00"
                dr("bitMarkupType") = "0"
                dr("monRate") = CType(gvr.FindControl("txtRate"), TextBox).Text
                dr("bitEnabled") = True
                dt.Rows.Add(dr)
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function GetItemsForRealShippingQuotes() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dr As DataRow
            CCommon.AddColumnsToDataTable(dt, "numServiceTypeID,vcServiceName,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled")

            'For Each gvr As DataGridItem In dgRealTimeShippingQuotes.Items
            '    dr = dt.NewRow()
            '    dr("numServiceTypeID") = gvr.Cells(0).Text.Trim
            '    dr("vcServiceName") = CType(gvr.FindControl("txtShippingCaption"), TextBox).Text
            '    dr("intFrom") = CType(gvr.FindControl("txtFrom"), TextBox).Text
            '    dr("intTo") = CType(gvr.FindControl("txtTo"), TextBox).Text
            '    dr("fltMarkup") = CType(gvr.FindControl("txtMarkup"), TextBox).Text
            '    dr("bitMarkupType") = CType(gvr.FindControl("chkIsPercentage"), CheckBox).Checked
            '    dr("monRate") = CType(gvr.FindControl("txtRate"), TextBox).Text
            '    dr("bitEnabled") = CType(gvr.FindControl("chk"), CheckBox).Checked
            '    dt.Rows.Add(dr)
            'Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub BindStateList()
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.RuleID = lngRuleID
            dgExcludedStates.DataSource = objShippingRule.GetShippingRuleStates()
            dgExcludedStates.DataBind()

            If dgExcludedStates.Items.Count = 0 Then
                dgExcludedStates.Visible = False
            Else
                dgExcludedStates.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = lngCountry
            dtTable = objUserAccess.SelState
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--All States--")
            ddl.Items.FindByText("--All States--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Extra Code"
    'objShippingRule.AppliesTo = IIf(rbIndividualItem.Checked, 1, IIf(rbItemCategory.Checked, 2, IIf(rbAllItems.Checked, 3, 0)))
    'objShippingRule.ContactsType = IIf(rbIndividualOrg.Checked, 1, IIf(rbRelProfile.Checked, 2, IIf(rbAllOrg.Checked, 3, 0)))

    'Load Detail
    'Select Case dtTable.Rows(0).Item("tintAppliesTo")
    '    Case 1
    '        rbIndividualItem.Checked = True
    '        hplIndividualItems.Text = hplIndividualItems.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString + ")"
    '        hplCategory.Text = hplCategory.Text + " (0)"
    '    Case 2
    '        rbItemCategory.Checked = True
    '        hplCategory.Text = hplCategory.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString + ")"
    '        hplIndividualItems.Text = hplIndividualItems.Text + " (0)"
    '    Case 3
    '        rbAllItems.Checked = True
    'End Select

    'Select Case dtTable.Rows(0).Item("tintContactsType")
    '    Case 1
    '        rbIndividualOrg.Checked = True
    '        hplIndividualOrg.Text = hplIndividualOrg.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("CustomerCount")).ToString + ")"
    '        hplRelProfile.Text = hplRelProfile.Text + " (0)"
    '    Case 2
    '        rbRelProfile.Checked = True
    '        hplRelProfile.Text = hplRelProfile.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("CustomerCount")).ToString + ")"
    '        hplIndividualOrg.Text = hplIndividualOrg.Text + " (0)"
    '    Case 3
    '        rbAllOrg.Checked = True
    'End Select

    'FillAllState(ddlState, Session("DomainID"))
    'Private Sub rblShippingMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblShippingMethod.SelectedIndexChanged
    '    Try
    '        If rblShippingMethod.SelectedValue = 1 Then
    '            pnlCustomShippingMethod.Visible = True
    '        ElseIf rblShippingMethod.SelectedValue = 2 Then
    '            pnlCustomShippingMethod.Visible = False
    '        End If

    '        BindServiceTypes()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub
#End Region

    Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "Internal Orders")
            ddlSites.Items.FindByText("Internal Orders").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindRelationshipNProfile()
        Try
            Dim objCommon As New CCommon

            objCommon.sb_FillComboFromDBwithSel(ddlCloneRelationship, 5, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlRuleRelationShip, 5, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlCloneProfile, 21, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlRuleProfile, 21, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub BindWarehouses()
        Try
            Dim objShippingRule As New ShippingRule()

            objShippingRule.DomainID = Session("DomainID")

            Dim dtWarehouses As DataTable = objShippingRule.GetShippingRuleWarehouses()

            If dtWarehouses IsNot Nothing Then
                radcmbWarehouse.DataSource = dtWarehouses
                radcmbWarehouse.DataTextField = "vcWareHouse"
                radcmbWarehouse.DataValueField = "numWareHouseID"
                radcmbWarehouse.DataBind()
            End If

            ' radcmbWarehouse.Items.Insert(0, New RadComboBoxItem("--Select One--", 0))

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnCloneRule_Click(sender As Object, e As EventArgs)
        Try
            'Create Clone Rule 
            If lngRuleID = 0 Then Exit Sub

            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
            objShippingRule.RuleID = lngRuleID
            objShippingRule.RuleName = txtRuleName.Text
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.numRelationship = CCommon.ToLong(ddlCloneRelationship.SelectedValue)
            objShippingRule.numProfile = CCommon.ToLong(ddlCloneProfile.SelectedValue)

            Dim newRuleId As Long = objShippingRule.CreateCloneRule()

            If newRuleId > 0 Then
                Response.Redirect("~/ECommerce/frmShippingRule.aspx?RuleID=" & newRuleId, False)
            End If

        Catch ex As Exception
            If ex.Message = "DUPLICATE RULE" Then
                litMessage.Text = "Rule Name already exists. Please change the Rule Name before Cloning."
            ElseIf ex.Message = "DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP" Then
                litMessage.Text = "Rule already exists for selected Relationship,Profile and state & zip(s)."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class