﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Partial Public Class frmSitePage
    Inherits BACRMPage

    Dim lngPageID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If GetQueryStringVal("PageID") <> "" Then
                lngPageID = GetQueryStringVal("PageID")
            End If

            Master.SelectedTabValue = "162"

            If Not IsPostBack Then

                LoadDropdown()
                If lngPageID > 0 Then
                    LoadDetails()
                    lblName.Text = "Edit Page"
                Else
                    lblName.Text = "New Page"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDropdown()
        Try
            Dim objSite As New Sites
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")

            ddlSiteTemplate.DataSource = objSite.GetSiteTemplates()
            ddlSiteTemplate.DataTextField = "vcTemplateName"
            ddlSiteTemplate.DataValueField = "numTemplateID"
            ddlSiteTemplate.DataBind()
            ddlSiteTemplate.Items.Insert(0, "--Select One--")
            ddlSiteTemplate.Items.FindByText("--Select One--").Value = 0

            objSite.StyleType = 0 'css
            cblStyle.DataSource = objSite.GetStyles()
            cblStyle.DataTextField = "StyleName"
            cblStyle.DataValueField = "numCssID"
            cblStyle.DataBind()

            objSite.StyleType = 1 ' javascript
            cblJS.DataSource = objSite.GetStyles()
            cblJS.DataTextField = "StyleName"
            cblJS.DataValueField = "numCssID"
            cblJS.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objSite As New Sites
            With objSite
                .SiteID = Session("SiteID")
                .PageID = lngPageID
                .PageName = txtPageName.Text
                .PageTitle = HttpUtility.HtmlEncode(txtTitle.Text.Trim())
                .PageURL = txtPageURL.Text & ".aspx"
                .PageType = ddlPageType.SelectedValue
                .TemplateID = ddlSiteTemplate.SelectedValue
                .DomainID = Session("DomainID")
                .StrItems = GetItems()
                .UserCntId = Session("UserContactID")
                .IsActive = rblStatus.SelectedValue
                .IsMaintainScroll = cbMaintainScroll.Checked
                lngPageID = objSite.ManagePages()

                If lngPageID > 0 Then
                    .MetaID = hdnMetaID.Value
                    '.MetaTags = HttpUtility.HtmlEncode(txtMetaTags.Text.Trim())
                    .MetaKeywords = txtMetaKeywords.Text.Trim()
                    .MetaDescription = txtMetaTags.Text
                    .MetaTagFor = 1 '1 for Page, 2 for Items
                    .ReferenceID = lngPageID
                    .ManageMetaTags()
                    Session("SiteID") = .SiteID
                    .ManageAspxPages(.SiteID, .PageURL, cbMaintainScroll.Checked)
                    Response.Redirect("../ECommerce/frmSitePageList.aspx", False)
                Else
                    litMessage.Text = "Page name or file name already exists for this site."
                End If

            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numCssID")
            For Each Item As ListItem In cblStyle.Items
                If Item.Selected Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numCssID") = Item.Value
                    dt.Rows.Add(dr)
                End If
            Next
            For Each Item As ListItem In cblJS.Items
                If Item.Selected Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numCssID") = Item.Value
                    dt.Rows.Add(dr)
                End If
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmSitePageList.aspx", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objSite As New Sites
            Dim dsPage As DataSet

            objSite.PageID = lngPageID
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            dsPage = objSite.GetPages()
            If dsPage.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(dsPage.Tables(0).Rows(0).Item("numTemplateID")) Then
                    If Not ddlSiteTemplate.Items.FindByValue(dsPage.Tables(0).Rows(0).Item("numTemplateID")) Is Nothing Then
                        ddlSiteTemplate.Items.FindByValue(dsPage.Tables(0).Rows(0).Item("numTemplateID")).Selected = True
                    End If
                End If
                txtPageName.Text = dsPage.Tables(0).Rows(0).Item("vcPageName")
                txtTitle.Text = HttpUtility.HtmlDecode(dsPage.Tables(0).Rows(0).Item("vcPageTitle"))
                txtPageURL.Text = dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString().ToLower().Replace(".aspx", "")
                If Not IsDBNull(dsPage.Tables(0).Rows(0).Item("tintPageType")) Then
                    If Not ddlPageType.Items.FindByValue(dsPage.Tables(0).Rows(0).Item("tintPageType")) Is Nothing Then
                        ddlPageType.ClearSelection()
                        ddlPageType.Items.FindByValue(dsPage.Tables(0).Rows(0).Item("tintPageType")).Selected = True
                    End If
                End If
                hdnMetaID.Value = dsPage.Tables(0).Rows(0).Item("numMetaID")
                txtMetaTags.Text = HttpUtility.HtmlDecode(dsPage.Tables(0).Rows(0).Item("vcMetaDescription"))
                txtMetaKeywords.Text = dsPage.Tables(0).Rows(0).Item("vcMetaKeywords")
            End If

            If dsPage.Tables(1).Rows.Count > 0 Then
                For Each dr As DataRow In dsPage.Tables(1).Rows
                    cblStyle.Items.FindByValue(dr("numCssID")).Selected = True
                Next
            End If
            If dsPage.Tables(2).Rows.Count > 0 Then
                For Each dr As DataRow In dsPage.Tables(2).Rows
                    cblJS.Items.FindByValue(dr("numCssID")).Selected = True
                Next
            End If
            If ddlPageType.SelectedValue = "1" Then
                ddlPageType.Enabled = False
                txtPageURL.Enabled = False
            End If
            rblStatus.SelectedValue = IIf(dsPage.Tables(0).Rows(0).Item("bitIsActive"), 1, 0)
            cbMaintainScroll.Checked = Sites.ToBool(dsPage.Tables(0).Rows(0).Item("bitIsMaintainScroll"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveOnly_Click(sender As Object, e As EventArgs) Handles btnSaveOnly.Click
        Try
            Dim objSite As New Sites
            With objSite
                .SiteID = Session("SiteID")
                .PageID = lngPageID
                .PageName = txtPageName.Text
                .PageTitle = HttpUtility.HtmlEncode(txtTitle.Text.Trim())
                .PageURL = txtPageURL.Text & ".aspx"
                .PageType = ddlPageType.SelectedValue
                .TemplateID = ddlSiteTemplate.SelectedValue
                .DomainID = Session("DomainID")
                .StrItems = GetItems()
                .UserCntID = Session("UserContactID")
                .IsActive = rblStatus.SelectedValue
                .IsMaintainScroll = cbMaintainScroll.Checked
                lngPageID = objSite.ManagePages()

                If lngPageID > 0 Then
                    .MetaID = hdnMetaID.Value
                    '.MetaTags = HttpUtility.HtmlEncode(txtMetaTags.Text.Trim())
                    .MetaKeywords = txtMetaKeywords.Text.Trim()
                    .MetaDescription = txtMetaTags.Text
                    .MetaTagFor = 1 '1 for Page, 2 for Items
                    .ReferenceID = lngPageID
                    .ManageMetaTags()
                    Session("SiteID") = .SiteID
                    .ManageAspxPages(.SiteID, .PageURL, cbMaintainScroll.Checked)
                Else
                    litMessage.Text = "Page name or file name already exists for this site."
                End If

            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class