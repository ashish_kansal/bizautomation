﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Admin


Public Class frmShippingLabelConfiguration
    Inherits BACRMPage

#Region "OBJECT DECLARATION "

    Dim objShip As New ShippingRule

#End Region

#Region "BIND DROPDOWNS"

    Private Sub BindDropDown()
        Try
            'objCommon.sb_FillComboFromDBwithSel(ddlStatus, 176, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlErrorStatus, 176, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlReadyToShipStatus, 176, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlSuccessStatus, 176, Session("DomainID"))

            'objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.BizDocType = 1

            Dim dtBizDoc As DataTable = objCommon.GetBizDocType()
            For Each dr As DataRow In dtBizDoc.Rows
                If dr("numListItemID") = IIf(7 = 7, Session("AuthoritativeSalesBizDoc"), Session("AuthoritativePurchaseBizDoc")) Then
                    dr("vcData") = dr("vcData") & " - Authoritative"
                End If
            Next
            ddlBizDocs.DataSource = dtBizDoc
            ddlBizDocs.DataTextField = "vcData"
            ddlBizDocs.DataValueField = "numListItemID"
            ddlBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCheckBoxList()
        Try
            objCommon.sb_FillCheckBoxListFromDB(chkOrderStatus, 176, Session("DomainID"))
            chkOrderStatus.Items.Add(New ListItem("No Status", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


#End Region

#Region "PAGE EVENTS"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            DomainID = Session("DomainID")

            If Not IsPostBack Then
                BindDropDown()
                BindCheckBoxList()
                BindConfiguration()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

#End Region

#Region "BUTTON EVENTS"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            With objShip

                Dim strOrderStatus As String = ""
                For Each strItem As ListItem In chkOrderStatus.Items
                    If strItem.Selected = True Then
                        strOrderStatus = strItem.Value & "," & strOrderStatus
                    End If
                Next

                .OrderStatus = strOrderStatus.Trim(",")
                .ErrorStatus = ddlErrorStatus.SelectedValue
                .SuccessStatus = ddlSuccessStatus.SelectedValue
                .ReadyToShipStatus = ddlReadyToShipStatus.SelectedValue
                .BizDocType = ddlBizDocs.SelectedValue
                .DomainID = DomainID

                If .Save() >= 0 Then
                    litMessage.Text = "Shipping Label Configuration saved successfully."
                Else
                    litMessage.Text = "Error occurred while save configuration."
                End If

            End With

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

#End Region

#Region "BIND CONFIGURATION"

    Public Sub BindConfiguration()
        Try
            Dim dtConfig As New DataTable

            With objShip
                .DomainID = DomainID
                dtConfig = .GetShippingLabelConfiguration()
            End With

            If dtConfig IsNot Nothing AndAlso dtConfig.Rows.Count > 0 Then

                If ddlErrorStatus.Items.FindByValue(dtConfig.Rows(0)("numErrorStatus")) IsNot Nothing Then
                    ddlErrorStatus.SelectedValue = CCommon.ToLong(dtConfig.Rows(0)("numErrorStatus"))
                End If

                If ddlSuccessStatus.Items.FindByValue(dtConfig.Rows(0)("numSuccessStatus")) IsNot Nothing Then
                    ddlSuccessStatus.SelectedValue = CCommon.ToLong(dtConfig.Rows(0)("numSuccessStatus"))
                End If

                If ddlReadyToShipStatus.Items.FindByValue(dtConfig.Rows(0)("numReadyToShipStatus")) IsNot Nothing Then
                    ddlReadyToShipStatus.SelectedValue = CCommon.ToLong(dtConfig.Rows(0)("numReadyToShipStatus"))
                End If

                If ddlBizDocs.Items.FindByValue(dtConfig.Rows(0)("numBizDocType")) IsNot Nothing Then
                    ddlBizDocs.SelectedValue = CCommon.ToLong(dtConfig.Rows(0)("numBizDocType"))
                End If

                Dim strOrderStatus() As String = CCommon.ToString(dtConfig.Rows(0)("vcOrderStatus")).Split(",")

                If strOrderStatus.Length > 0 AndAlso strOrderStatus(0) <> "" Then
                    For intCnt As Integer = 0 To strOrderStatus.Length - 1
                        chkOrderStatus.Items.FindByValue(strOrderStatus(intCnt)).Selected = True
                    Next
                End If

                If ddlReadyToShipStatus.Items.FindByValue(dtConfig.Rows(0)("numReadyToShipStatus")) IsNot Nothing Then
                    ddlReadyToShipStatus.SelectedValue = CCommon.ToLong(dtConfig.Rows(0)("numReadyToShipStatus"))
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class
