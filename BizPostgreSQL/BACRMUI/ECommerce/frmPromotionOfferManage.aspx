﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPromotionOfferManage.aspx.vb"
    Inherits=".frmPromotionOfferManage" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Promotion & Offer</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>

    <style type="text/css">
        #divValidity .input-group input[type=text] {
            width: 110px;
        }

        .spanstar {
            color: red;
            font-weight: bold;
            font-size: 16px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            DiscountTypeChanged(false)

            $("#chkNeverExpires").change(function () {
                if ($(this).is(":checked")) {
                    $("[id$=calValidFromDate_txtDate]").val("");
                    $("[id$=calValidToDate_txtDate]").val("");
                }
            });

            $("#txtValue").change(function () {
                if ($("#ddlBasedOn").val() === "1" && parseInt($(this).val()) > 1000) {
                    $(this).val("1000");
                    alert("if promotion is based on quantity than you can select maximum 1000 as value.");
                } else if ($("#ddlBasedOn").val() === "2" && parseInt($(this).val()) > 100000) {
                    $(this).val("100000");
                    alert("if promotion is based on quantity than you can select maximum 100,000 as value.");
                }
            });

            $("#txtDiscount").change(function () {
                if ($("#ddlDiscountType").val() === "1" && parseFloat($(this).val()) > 100) {
                    $(this).val("100");
                    alert("Discount percentage must be 100 or less.");
                }
            });

            $("[id$=chkUseOrderPromo]").change(function () {
                if ($(this).is(":checked")) {
                    $("[id$=divCustomer]").hide();
                    $("[id$=divValidity]").hide();
                } else {
                    $("[id$=divCustomer]").show();
                    $("[id$=divValidity]").show();
                }
            });
        });

        function DiscountSelectionForQuantity() {
            if ($("#ddlDiscountType").val() === "3") {
                $("#hplIndividualItemsForDiscount").html("Free (for the following selected items)");

            } else {
                $("#hplIndividualItemsForDiscount").html("<label for='rbIndividualItemsForDiscount'>Off the following Items (select individually)</label><br />");
            }
        }

        function DiscountTypeChanged(onDropDown) {
            if ($("#ddlDiscountType").val() === "3") {
                $("#rbFreeForAnyitemequalorlesser").show();
                $("label[for=rbFreeForAnyitemequalorlesser]").show();
                $("#hplIndividualItemsForDiscount").html("Free (for the following selected items)");

                $("[id$=rbFreeForAnyitemequalorlesserWithPrice").show();
                $("label[for=rbFreeForAnyitemequalorlesserWithPrice]").show();
                $("[id$=txtFreeQtyPrice1").show();
                $("[id$=lblFreeForAnyitemequalorlesserWithPrice").show();
                $("[id$=rbFreeForSelectedItemsWithPrice").show();
                $("label[for=rbFreeForSelectedItemsWithPrice]").show();
                $("[id$=txtFreeQtyPrice2").show();
                $("[id$=hplFreeForSelectedItemsWithPrice").show();

                if (onDropDown == false) {
                    if ($('#rbIndividualItemsForDiscount').is(':checked') == true) {
                        $("#rbIndividualItemsForDiscount").prop("checked", true);
                    }
                    else if ($("#rbFreeForAnyitemequalorlesser").is(':checked') == true) {
                        $("#rbFreeForAnyitemequalorlesser").prop("checked", true);
                    }
                }
                else {
                    $("#rbIndividualItemsForDiscount").prop("checked", false);
                }
                $("#rbItemClassificationForDiscount").prop("checked", false);
                $("#rbItemClassificationForDiscount").hide();
                $("#hplItemClassificationForDiscount").hide();

                $("#rbRelatedItemsForDiscount").prop("checked", false);
                $("#rbRelatedItemsForDiscount").hide();
                $("label[for=rbRelatedItemsForDiscount]").hide();

            } else {
                $("#rbItemClassificationForDiscount").show();
                $("#hplItemClassificationForDiscount").show();
                $("#rbRelatedItemsForDiscount").show();
                $("label[for=rbRelatedItemsForDiscount]").show();
                $("#rbFreeForAnyitemequalorlesser").hide();
                $("label[for=rbFreeForAnyitemequalorlesser]").hide();

                $("#rbFreeForAnyitemequalorlesser").hide();
                $("#hplIndividualItemsForDiscount").html("<label for='rbIndividualItemsForDiscount'>Off the following Items (select individually)</label><br />");

                $("[id$=rbFreeForAnyitemequalorlesserWithPrice").hide();
                $("label[for=rbFreeForAnyitemequalorlesserWithPrice]").hide();
                $("[id$=txtFreeQtyPrice1").hide();
                $("[id$=lblFreeForAnyitemequalorlesserWithPrice").hide();
                $("[id$=rbFreeForSelectedItemsWithPrice").hide();
                $("label[for=rbFreeForSelectedItemsWithPrice]").hide();
                $("[id$=txtFreeQtyPrice2").hide();
                $("[id$=hplFreeForSelectedItemsWithPrice").hide();

                if (onDropDown == false) {
                    if ($('#rbIndividualItemsForDiscount').is(':checked') == true) {
                        $("#rbIndividualItemsForDiscount").prop("checked", true);
                    }
                    else if ($("#rbRelatedItemsForDiscount").is(':checked') == true) {
                        $("#rbRelatedItemsForDiscount").prop("checked", true);
                    }
                    else if ($("#rbItemClassificationForDiscount").is(':checked') == true) {
                        $("#rbItemClassificationForDiscount").prop("checked", true);
                    }
                }
                else {
                    $("#rbIndividualItemsForDiscount").prop("checked", false);
                }
            }
        }

        function OpenItemSelectionWindow(type, recordType, promotionId) {
            if (type == 1 && recordType == 5) {
                document.getElementById('rbIndividualItemPurchased').checked = true;
            } else if (type == 2 && recordType == 5) {
                document.getElementById('rbItemClassificationPurchased').checked = true;
            }
                //else if (type == 3 && recordType == 5) {
                //    document.getElementById('rbItemCustomFields').checked = true;
                //}
            else if (type == 1 && recordType == 6) {
                document.getElementById('rbIndividualItemsForDiscount').checked = true;
            } else if (type == 2 && recordType == 6) {
                document.getElementById('rbItemClassificationForDiscount').checked = true;
            }

            window.open("../ECommerce/frmPromotionItems.aspx?promotionID=" + promotionId + "&itemSelectionType=" + type + "&recordType=" + recordType, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenSiteSelectionWindow() {
            $("#chkSites").prop("checked", true);
            window.open("../ECommerce/frmPromotionSites.aspx?promotionID=" + $("#hfProId").val(), '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function Save() {

            if (document.getElementById('txtOfferName').value == "") {
                alert("Enter Promotion/Offer name");
                document.getElementById('txtOfferName').focus();
                return false;
            }

            if (parseInt(document.getElementById('hfProId').value) > 0) {
                var errorMessage = '';

                if (!$("[id$=chkUseOrderPromo]").is(":checked")) {
                    if (!$('#chkNeverExpires').is(":checked")) {
                        if ($('[id$=calValidFromDate_txtDate]').val() === "") {
                            errorMessage = errorMessage + "Enter Validity From Date. \n"
                        }

                        if ($('[id$=calValidToDate_txtDate]').val() === "") {
                            errorMessage = errorMessage + "Enter Validity To Date. \n"
                        }
                    }
                }

                if (!$("#txtValue").val() > 0) {
                    errorMessage = errorMessage + " Enter amount or quantity needs to be purchased to avail promotion offer.\n"
                }

                if (!parseFloat($("#txtDiscount").val()) > 0) {
                    errorMessage = errorMessage + " Enter discount value."
                }

                if (!$("#txtShortDesc").val() > 0) {
                    errorMessage = errorMessage + " Enter Short description."
                }

                if (!$("#txtlongdesc").val() > 0) {
                    errorMessage = errorMessage + " Enter Long description."
                }

                if (errorMessage.length > 0) {
                    alert(errorMessage);
                    return false;
                } else {
                    return true;
                }
            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        /*Function added by Neelam on 10/27/2017 - Added functionality to open Individual Customer or Relationship and Profile*/
        function OpenConfig(StepNo, StepValue, PromotionID) {
            if (StepNo == 1) {
                if (StepValue == 1) {
                    document.getElementById('rbIndividualOrg').checked = true;
                }
                else if (StepValue == 2) {
                    document.getElementById('rbRelProfile').checked = true;
                }
            }
            window.open("../ECommerce/frmPromotionOfferCustomers.aspx?Step=" + StepNo + "&StepValue=" + StepValue + "&PromotionID=" + PromotionID, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return Save()"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Save()"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:HiddenField ID="hfProId" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnSites" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnPromotionItems" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnDiscountItems" runat="server"></asp:HiddenField>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Item based Promotions
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-sm-12 col-md-5">
            <div class="form-inline">
                <div class="form-group">
                    <label>Name:</label>
                    <asp:TextBox ID="txtOfferName" runat="server" CssClass="form-control" MaxLength="100" Width="400px"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="form-group" id="divValidity" runat="server" visible="false">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Valid From:</label>
                        <BizCalendar:Calendar ID="calValidFromDate" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>To:</label>
                        <BizCalendar:Calendar ID="calValidToDate" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <asp:CheckBox ID="chkNeverExpires" runat="server" Text="Promotion never expires" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlPromotionDetail" runat="server" Visible="false">
        <div class="row padbottom10">
            <div class="col-xs-12" id="divCustomer" runat="server">
                <div class="form-group">
                    <asp:RadioButton ID="rbIndividualOrg" runat="server" CssClass="signup" GroupName="Organization" />
                    <asp:HyperLink ID="hplIndividualOrg" CssClass="hyperlink" Text="<label for='rbIndividualOrg'>Apply to customers individually</label>"
                        runat="server"> </asp:HyperLink>
                    <br />
                    <div id="trRelProfile" runat="server">
                        <asp:RadioButton ID="rbRelProfile" runat="server" CssClass="signup" GroupName="Organization" />
                        <asp:HyperLink ID="hplRelProfile" CssClass="hyperlink" Text="<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles </label>"
                            runat="server"> </asp:HyperLink>
                        <br />
                        <div id="trAllOrg" runat="server">
                            <asp:RadioButton ID="rbAllOrg" runat="server" CssClass="signup" GroupName="Organization" />
                            <label for="rbAllOrg">
                                All Customers</label>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" id="divCoupcode">
                <ul class="list-inline">
                    <li><asp:CheckBox ID="chkUseOrderPromo" runat="server" AutoPostBack="false" /></li>
                    <li><b>Use coupon codes and target relationships & profiles from</b></li>
                    <li><asp:DropDownList runat="server" ID="ddlOrderPromo" CssClass="form-control"></asp:DropDownList></li>
                </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table>
                        <tr>
                            <td style="white-space: nowrap;">
                                <div class="form-inline text-bold" style="padding-left: 5px;">
                                    &nbsp;&nbsp;&nbsp;Buy
                                        <asp:DropDownList ID="ddlBasedOnRange" runat="server" CssClass="form-control" Font-Bold="false" AutoPostBack="True">
                                            <asp:ListItem Text="minimum of" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="between" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    <asp:DropDownList ID="ddlBasedOn" runat="server" CssClass="form-control" Font-Bold="false">
                                        <asp:ListItem Text="Quantity" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Amount" Value="2"></asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:TextBox ID="txtValue" runat="server" CssClass="form-control" Font-Bold="false" Width="80px"></asp:TextBox>
                                    <span style="color: red;" id="spnAnd" runat="server" visible="false">*</span>
                                    <label id="lblAnd" runat="server" visible="false">And</label>
                                    <asp:TextBox ID="txtBetween" runat="server" CssClass="form-control" Font-Bold="false" Width="80px" Visible="false"></asp:TextBox>
                                </div>
                            </td>
                            <td style="white-space: nowrap;">
                                <div class="form-inline text-bold">
                                    <div class="form-group" style="margin-left: 10px; margin-right: 10px">
                                        <asp:RadioButton ID="rbIndividualItemPurchased" runat="server" CssClass="signup" GroupName="ItemsPurchased" />
                                        <asp:HyperLink ID="hplIndividualItemsPurchased" Text="<label for='rbIndividualItem'>Of the following Items (select individually)</label>" runat="server"> </asp:HyperLink>
                                        <br />
                                        <asp:RadioButton ID="rbItemClassificationPurchased" runat="server" CssClass="signup" GroupName="ItemsPurchased" />
                                        <asp:HyperLink ID="hplItemClassificationPurchased" Text="<label for='rbItemClassification'>Of items belonging to the following item groups</label>" runat="server"> </asp:HyperLink>
                                        <br />
                                        <%-- <asp:RadioButton ID="rbItemCustomFields" runat="server" CssClass="signup" GroupName="ItemsPurchased" Visible="false" /> <!-- TODO: Remove this option because carl has saild to not implment this option right now -->
                                            <asp:HyperLink ID="hplItemCustomFields" Text="<label for='rbItemCustomFields'>Of items belonging to the following custom drop down field in item details</label>" runat="server" Visible="false"> </asp:HyperLink>
                                            <br />--%>
                                        <asp:RadioButton ID="rbItemAll" runat="server" CssClass="signup" GroupName="ItemsPurchased" Text="All items" />
                                    </div>
                                    & get &nbsp;
                                </div>
                            </td>
                            <td style="white-space: nowrap;">
                                <div class="form-inline text-bold">
                                    <asp:TextBox ID="txtDiscount" runat="server" Width="90" MaxLength="10" CssClass="form-control" Font-Bold="false"></asp:TextBox>
                                    <asp:DropDownList ID="ddlDiscountType" OnSelectedIndexChanged="ddlDiscountType_SelectedIndexChanged" AutoPostBack="true" runat="server" Font-Bold="false" CssClass="form-control">
                                        <asp:ListItem Value="1" Selected="True">Percentage</asp:ListItem>
                                        <asp:ListItem Value="2">Flat Amount</asp:ListItem>
                                        <asp:ListItem Value="3">Quantity</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td style="white-space: nowrap;">
                                <div class="form-group text-bold" style="margin-left: 10px; margin-right: 10px">
                                    <asp:RadioButton ID="rbFreeForAnyitemequalorlesser" GroupName="ItemsDiscount" runat="server" Text="Free (for any item of equal or lesser value)" /><br />
                                    <asp:RadioButton ID="rbIndividualItemsForDiscount" runat="server" GroupName="ItemsDiscount" />
                                    <asp:HyperLink ID="hplIndividualItemsForDiscount" Text="<label for='rbIndividualItemsForDiscount'>Off the following Items (select individually)</label><br />" runat="server"> </asp:HyperLink>
                                    <asp:RadioButton ID="rbItemClassificationForDiscount" runat="server" GroupName="ItemsDiscount" />
                                    <asp:HyperLink ID="hplItemClassificationForDiscount" Text="<label for='rbItemClassificationForDiscount'>Off Items belonging to following item groups</label><br />" runat="server"> </asp:HyperLink>
                                    <asp:RadioButton ID="rbRelatedItemsForDiscount" runat="server" GroupName="ItemsDiscount" Text="Off their “Related Items”" /><br />
                                    <asp:RadioButton ID="rbFreeForAnyitemequalorlesserWithPrice" GroupName="ItemsDiscount" runat="server" Text="$" /><asp:TextBox ID="txtFreeQtyPrice1" runat="server" style="display:inline-block;" CssClass="form-control" Width="80"></asp:TextBox> <asp:Label ID="lblFreeForAnyitemequalorlesserWithPrice" runat="server" Text=" (for any item of equal or lesser value)"></asp:Label> <br />
                                    <asp:RadioButton ID="rbFreeForSelectedItemsWithPrice" GroupName="ItemsDiscount" runat="server" Text="$" /><asp:TextBox ID="txtFreeQtyPrice2" runat="server" style="display:inline-block;" CssClass="form-control" Width="80"></asp:TextBox> <asp:HyperLink ID="hplFreeForSelectedItemsWithPrice" Text="<label for='rbFreeForSelectedItemsWithPrice'> (for following selected items)</label><br />" runat="server"> </asp:HyperLink>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-top: 10px;"><span class="spanstar">*</span><label>Promotion language short description </label>
                            </td>
                            <td style="text-align: left; padding-left: 11px; padding-top: 10px;">
                                <asp:TextBox CssClass="form-control" Width="350px" ID="txtShortDesc" runat="server"></asp:TextBox></td>
                            <td></td>
                            <td style="padding-top: 10px; padding-left: 12px;">
                                <div class="form-inline">
                                    <label>Calculate discount: </label>
                                    <asp:DropDownList ID="ddlCalDisc" Width="130px" CssClass="form-control" Font-Bold="false" runat="server">
                                        <asp:ListItem Text="Off list price" Selected="True" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Off sale price" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; vertical-align: top; padding-top: 10px;"><span class="spanstar">*</span><label>Promotion language long description </label>
                            </td>
                            <td style="text-align: left; padding-left: 11px; padding-top: 10px;">
                                <asp:TextBox CssClass="form-control" TextMode="MultiLine" Width="350px" Height="100px" ID="txtlongdesc" runat="server"></asp:TextBox></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
