﻿Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Public Class frmReviewList
    Inherits BACRMPage

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                BindSites()
                If Not Session("SiteID") Is Nothing Then
                    ddlSites.SelectedValue = Session("SiteID")
                    BindReviewData(GetReviewData())
                End If
                If Me.ReviewType <> "" Then
                    ddlType.SelectedItem.Selected = False
                    ddlType.Items.FindByValue(Me.ReviewType).Selected = True
                    BindReviewData(GetReviewData())
                End If

            End If
            'Dim list As New List(Of Review)

            'Dim index As Integer
            'For index = 1 To 10
            '    Dim rev As New Review
            '    rev.vcReviewType = "Product"
            '    rev.numReviewId = index
            '    rev.vcUserName = "Kishan_" & Convert.ToString(index)
            '    rev.vcTitle = "Its Bang on  Product.[...]"
            '    rev.vcReview = "Its a good Product at all  [...]"
            '    rev.bitEmail = True
            '    rev.vcIpAddress = "299.177.78.78"
            '    rev.vcTotalHelpful = "14/20"
            '    rev.vcTotalAbuse = "15"
            '    rev.dtCreatedDatetime = System.DateTime.Now.Date
            '    list.Add(rev)
            'Next




        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub ddlType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Try
            BindReviewData(GetReviewData())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub ddlSites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSites.SelectedIndexChanged
        Try
            Session("SiteID") = ddlSites.SelectedValue
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = ddlSites.SelectedValue
            objSite.DomainID = Session("DomainID")
            dtSite = objSite.GetSites()
            Session("SitePreviewURL") = dtSite.Rows(0)("vcPreviewURL")
            Response.Redirect(Request.Url.OriginalString)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgReview_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReview.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objReview As New Review
                objReview.ReviewId = CCommon.ToDecimal(e.CommandArgument)

                objReview.DeleteReview()

                BindReviewData(GetReviewData())

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgReview_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReview.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    
#End Region

#Region "Methods"

    Function GetReviewData() As DataTable
        Try

            Dim dtReview As DataTable

            Dim objReview As New Review
            objReview.Mode = 2
            objReview.TypeId = CCommon.ToLong(ddlType.SelectedItem.Value)
            objReview.DomainID = CCommon.ToLong(Session("DomainID"))
            objReview.SiteId = CCommon.ToLong(Session("SiteID"))

            dtReview = objReview.GetReviews()

            Return dtReview

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "--Select One--")
            ddlSites.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindReviewData(ByVal dtReview As DataTable)
        Try
            dgReview.DataSource = dtReview
            dgReview.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
        
    End Sub
#End Region

#Region "Custom Property"

    Public Property ReviewId() As Long
        Get
            Dim o As Object = ViewState("ReviewID")
            If o IsNot Nothing Then
                Return CType(o, Long)
            Else
                If Me.ReviewType <> "" Then
                    ViewState("ReviewID") = GetQueryStringVal("ReviewID")
                    Return CType(ViewState("ReviewID"), Long)
                Else
                    Return 0
                End If
            End If
        End Get
        Set(value As Long)
            ViewState("ReviewID") = value
        End Set
    End Property

    Public ReadOnly Property ReviewType() As String
        Get
            Dim o As Object = ViewState("ReviewType")
            If o IsNot Nothing Then
                Return CType(o, String)
            Else
                If GetQueryStringVal("ReviewType") <> "" Then
                    ViewState("ReviewType") = GetQueryStringVal("ReviewType")
                    Return CType(ViewState("ReviewType"), String)
                Else
                    Return ""
                End If
            End If
        End Get
    End Property
#End Region

End Class


'Public Class Review
'    Public Property vcReviewType As String
'    Public Property numReviewId As Integer
'    Public Property vcUserName As String
'    Public Property vcTitle As String
'    Public Property vcReview As String
'    Public Property bitEmail As Boolean
'    Public Property vcIpAddress As String
'    Public Property vcTotalHelpful As String
'    Public Property vcTotalAbuse As String
'    Public Property dtCreatedDatetime As DateTime

'End Class
