﻿Imports System.IO
Imports System.Web
Imports System.Web.Services
Imports BACRM.BusinessLogic.Common

Public Class ckEditorImageUpload
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        'context.Response.ContentType = "text/plain"
        'context.Response.Write("Hello World!")
        Dim uploads As HttpPostedFile = context.Request.Files("upload")
        Dim CKEditorFuncNum As String = context.Request("CKEditorFuncNum")
        Dim file As String = System.IO.Path.GetFileName(uploads.FileName)

        'uploads.SaveAs(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & context.Request.QueryString("DomainID") & file)


        Dim strFName As String()
        Dim strFilePath, strFileName, strFileType As String
        strFileName = Path.GetFileName(uploads.FileName)
        If Directory.Exists(CCommon.GetDocumentPhysicalPath(context.Request.QueryString("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
            Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(context.Request.QueryString("DomainID")))
        End If
        Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
        strFilePath = CCommon.GetDocumentPhysicalPath(context.Request.QueryString("DomainID")) & newFileName
        strFName = Split(strFileName, ".")
        strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
        uploads.SaveAs(strFilePath)
        'UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
        Dim strPortalLocation As String = System.Configuration.ConfigurationManager.AppSettings("PortalVirtualDirectoryName")
        If String.IsNullOrEmpty(strPortalLocation) = False AndAlso strPortalLocation.Length > 0 Then
            strPortalLocation = "/" & strPortalLocation
        End If
        Dim strServerWithPort As String = HttpContext.Current.Request.ServerVariables("SERVER_NAME")
        If HttpContext.Current.Request.ServerVariables("SERVER_NAME") = "localhost" Then
            strServerWithPort = strServerWithPort & ":" & HttpContext.Current.Request.ServerVariables("SERVER_PORT")
        End If
        Dim url As String = context.Request.Url.Scheme & "://" & strServerWithPort & strPortalLocation & "/Documents/Docs/" & context.Request.QueryString("DomainID") & "/" & newFileName
        context.Response.Write("<script>window.parent.CKEDITOR.tools.callFunction('" & CKEditorFuncNum & "', '" & url & "');</script>")
        context.Response.[End]()
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class