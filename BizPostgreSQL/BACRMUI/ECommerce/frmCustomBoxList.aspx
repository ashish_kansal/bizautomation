﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomBoxList.aspx.vb"
    Inherits=".frmCustomBoxList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Box(s)</title>
    <script type="text/javascript" language="javascript">
        function New() {
            window.location.href = "frmAddCustomBox.aspx";
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord(mode) {
            if (mode == 0) {
                if (confirm('Are you sure, you want to delete the selected record?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('You are not authorised to delete this record.')
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnNew" runat="server" CssClass="btn btn-primary" OnClientClick="New()"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Box</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Box(s)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgCustomBox" runat="server" Width="100%" CssClass="table table-striped table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numPackageTypeID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numCustomPackageID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcPackageName" CommandName="CustomPackageID" HeaderText="Box Name"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="fltWidth" HeaderText="Width"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltHeight" HeaderText="Height"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltLength" HeaderText="Length"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltTotalWeight" HeaderText="Weight"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
