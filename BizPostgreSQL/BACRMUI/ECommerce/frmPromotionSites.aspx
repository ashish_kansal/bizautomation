﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmPromotionSites.aspx.vb" Inherits=".frmPromotionSites" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table width="100%">
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveClose" CssClass="button" runat="server" Text="Save & Close"></asp:Button>
                <asp:Button ID="btnClose" CssClass="button" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Select Sites
    <asp:HiddenField ID="hdnPromotionID" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table style="min-width:700px;">
        <tr>
            <td>
                <asp:CheckBoxList ID="chkblSites" runat="server" RepeatDirection="Horizontal" RepeatColumns="5"></asp:CheckBoxList>
            </td>
        </tr>
    </table>
    
</asp:Content>
