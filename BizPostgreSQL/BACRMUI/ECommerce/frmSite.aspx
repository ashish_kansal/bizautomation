﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSite.aspx.vb" Inherits=".frmSite"
    MasterPageFile="~/common/ECommerceMenuMaster.Master" ValidateRequest="false" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function Save() {
            if (document.getElementById('txtSiteName').value == "") {
                alert("Enter Site Name.")
                document.getElementById('txtSiteName').focus()
                return false;
            }
            if (document.getElementById('txtHostName').value == "") {
                alert("Enter Host Name.")
                document.getElementById('txtHostName').focus()
                return false;
            }
            if (document.getElementById('ddlCurrency').value == 0) {
                alert("Select Currency.")
                document.getElementById('ddlCurrency').focus();
                return false;
            }


            return true;
        }

        $(document).ready(
           function () {
               var mainChkBox = $(".cbHeader input"); //$("input[id$='chkSelectAll']");

               mainChkBox.click(
                 function () {
                     if (this.checked) {
                         $('.rcbCheckBox').each(function () {
                             var checkState = 0;
                             if (!this.checked) {
                                 checkState = 1;
                             }
                             this.checked = true;
                             if (checkState == 1) {
                                 this.click();
                             }
                         });

                     }
                     else {

                         $('.rcbCheckBox').each(
                             function () {
                                 var checkState = 0;
                                 if (this.checked) {
                                     checkState = 1;
                                 }
                                 this.checked = false;
                                 if (checkState == 1) {
                                     this.click();
                                 }

                             });


                     }
                 });
           }
       );




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblSite" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmSite.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Site Name<font color="#ff0000">*</font></label>
                <asp:TextBox ID="txtSiteName" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Host Name<font color="#ff0000">*</font></label>
                <div class="form-inline">
                    http://<asp:TextBox ID="txtHostName" runat="server" CssClass="form-control" MaxLength="20" Width="140"></asp:TextBox><asp:Label Text=".bizautomation.com" runat="server" ID="lblHostNamePostFix" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Default Currency<font color="#ff0000">*</font>&nbsp;&nbsp;<asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Note: You can configure default currancy of BizAutomation Application from  Administration -> Global Settings" /></label>
                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Active</label>
                <div>
                    <asp:RadioButton ID="rbtnActive" runat="server" Checked="true" Text="Yes" GroupName="Activate" />
                    <asp:RadioButton ID="rbtnInactive" runat="server" Text="No" GroupName="Activate" Style="padding-left: 5px" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Enable One Page Checkout</label>
                <div>
                    <asp:CheckBox Text="" runat="server" ID="chkEnableOnePageCheckout" CssClass="signup" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Rate Type&nbsp;&nbsp;<asp:Label ID="lblRateType" Text="[?]" CssClass="tip" runat="server" ToolTip="Specify what kind of Rates would be displayed to your customer when checkout using Realtime Shipping Method i.e Fedex , UPS." /></label>
                <asp:DropDownList ID="ddlRateType" runat="server" CssClass="form-control">
                    <asp:ListItem Text="List Rate" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Discounted Rate" Value="1"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Enable SSL Redirect&nbsp;&nbsp;<asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Adds https redirect rule to web.config. This option will be eanbled after adding live site url and validating it." /></label>
                <div>
                    <asp:CheckBox ID="chkSSLRedirect" runat="server" Enabled="false" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Html 5&nbsp;&nbsp;<asp:Label ID="lblHtml5" Text="[?]" CssClass="tip" runat="server" ToolTip="Checking this option will automatically add html 5 required document type to all the pages." /></label>
                <div>
                    <asp:CheckBox ID="chkHtml5" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Description</label>
                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" Rows="4" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Meta Tags&nbsp;&nbsp;<asp:Label ID="lblMetaTags" Text="[?]" CssClass="tip" runat="server" ToolTip="Text added to Meta Tags will be added in header of all site pages." /></label>
                <asp:TextBox ID="txtMetaTags" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="box box-solid box-primary" id="trGoLive" runat="server" visible="false">
        <div class="box-header">
            <h3 class="box-title">Go Live</h3>
        </div>
        <div class="box-body">
            <div class="form-inline">
                <label>Enter your website URL which you wish to point to site:</label>
                <div class="input-group">
                    <asp:TextBox ID="txtLiveSiteURL" runat="server" CssClass="form-control" Width="250" MaxLength="50"></asp:TextBox>
                    <div class="input-group-btn">
                        <asp:Button ID="btnValidate" Text="Validate" runat="server" CssClass="btn btn-warning" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnOldSiteName" runat="server" />
    <asp:HiddenField ID="hdnOldHostName" runat="server" />
    <asp:HiddenField ID="hdnLiveSiteUrl" runat="server" />
        </div>
</asp:Content>
