﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPageElementDetails.aspx.vb"
    Inherits=".frmPageElementDetails" ValidateRequest="false" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
   

    <script src="../JavaScript/EditArea/edit_area_full.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function pageLoaded() {

        editAreaLoader.init({ id: 'txtEditHTML', syntax: 'html', start_highlight: true, toolbar: 'search, go_to_line, undo, redo, select_font' });

        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField" || name == "CustomField") {
                editor.pasteHtml(val);  
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
        //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html
        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    console.log(img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    var siteID = document.getElementById('hdnSiteID').value;
                    img.setAttribute("src", img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    console.log(img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }
        

        }
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            prm.add_pageLoaded(clicksave);
        });
        function InsertTag(a) {
            var ddl = document.getElementById(a);
            if (ddl != null) {
                if (ddl.value != "") {
                    editAreaLoader.insertTags("txtEditHTML", ddl.value, "");
                }
            }
        }
        function clicksave() {
            //alert("ee");
            $("#txtEditHTML").val(editAreaLoader.getValue("txtEditHTML"));
        }
    </script>
    <style>
        .EditCss {
            font-family: arial;
            font-size: 10pt;
            color: Black;
        }

        .MergeDropdown {
            font-family: "Segoe UI",Arial,sans-serif;
            font-size: 10pt;
            color: Gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:HyperLink Text="Column Settings" CssClass="hyperlink" runat="server" ID="hplSettings"
                    Visible="false"></asp:HyperLink>
                <asp:LinkButton ID="btnSaveOnly" OnClientClick="clicksave()" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save </asp:LinkButton>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" usesubmitbehaviour="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp; Close
                </asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                 <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Settings for
    <asp:Label ID="lblElementName" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:PlaceHolder ID="phAttributes" runat="server"></asp:PlaceHolder>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trHTMLEditor" runat="server">
            <asp:TableCell>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Customize Page Element's HTML</label>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Allows you to control layout of page element. Tags like ##CustomeName## will be dynamically replaced.<br>Tip: To revert HTML to default layout, please remove all content then save and reopen same page element." />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="ddlMergeField" CssClass="form-control" onchange="InsertTag('ddlMergeField');">
                            </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="ddlCustomField" CssClass="form-control" onchange="InsertTag('ddlCustomField');">
                            </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="ddlAttributes" CssClass="form-control" onchange="InsertTag('ddlAttributes');">
                            </asp:DropDownList>
                            </div>
                        <div style="margin-top:50px;">
                             <table class="table table-responsive table-bordered">
                        <tr>
                            <th>
                                Field Name
                            </th>
                            <th>
                                Description
                            </th>
                        </tr>
                            <tr>
                                <td>##CustomerInformationLink##
                                </td>
                                <td>link for edit customer information
                                </td>
                            </tr>
                            <tr>
                                <td>##FirstName##
                                </td>
                                <td>customer firstname
                                </td>
                            </tr>
                            <tr>
                                <td>##LastName##
                                </td>
                                <td>customer lastname
                                </td>
                            </tr>
                            <tr>
                                <td>##Email##
                                </td>
                                <td>customer email id
                                </td>
                            </tr>
                            <tr>
                                <td>##Phone##
                                </td>
                                <td>customer phone number
                                </td>
                            </tr>
                            <tr>
                                <td>##AddressLink##
                                </td>
                                <td>link for edit customer address
                                </td>
                            </tr>
                            <tr>
                                <td>##BillingAddress##
                                </td>
                                <td>customer billing address
                                </td>
                            </tr>
                            <tr>
                                <td>##ShippingAddress##
                                </td>
                                <td>customer Shipping Address
                                </td>
                            </tr>
                            <tr>
                                <td>##CartLink##
                                </td>
                                <td>link for edit Shipping Information
                                </td>
                            </tr>
                            <tr>
                                <td>##ItemName##
                                </td>
                                <td>shopping item name
                                </td>
                            </tr>
                            <tr>
                                <td>##ItemAttributes##
                                </td>
                                <td>item attribute
                                </td>
                            </tr>
                            <tr>
                                <td>##ItemUnits##
                                </td>
                                <td>item unit
                                </td>
                            </tr>
                            <tr>
                                <td>##ItemPricePerUnit##
                                </td>
                                <td>item unit price
                                </td>
                            </tr>
                            <tr>
                                <td>##Discount##
                                </td>
                                <td>item discount price
                                </td>
                            </tr>
                            <tr>
                                <td>##TotalAmount##
                                </td>
                                <td>final price of item
                                </td>
                            </tr>
                            <tr>
                                <td>##ItemURL##
                                </td>
                                <td>link for details of this item
                                </td>
                            </tr>
                            <tr>
                                <td>##SubTotalLabel##
                                </td>
                                <td>subtotal of total order
                                </td>
                            </tr>
                            <tr>
                                <td>##DiscountLabel##
                                </td>
                                <td>discount of total order
                                </td>
                            </tr>
                            <tr>
                                <td>##TaxLabel##
                                </td>
                                <td>tax included of order
                                </td>
                            </tr>
                            <tr>
                                <td>##ShippingChargeLabel##
                                </td>
                                <td>shipping charges for this order
                                </td>
                            </tr>
                            <tr>
                                <td>##TotalAmountLabel##
                                </td>
                                <td>total amount of this order
                                </td>
                            </tr>
                            <tr>
                                <td>##IsBillMeChecked##
                                </td>
                                <td>select for choose "bill me" option
                                </td>
                            </tr>
                            <tr>
                                <td>##TotalBalanceDue##
                                </td>
                                <td>total balance due in your account
                                </td>
                            </tr>
                            <tr>
                                <td>##TotalRemaniningCredit##
                                </td>
                                <td>Total Remaining Credit in your account
                                </td>
                            </tr>
                            <tr>
                                <td>##TotalAmountPastDue##
                                </td>
                                <td>Total Amount Past Due in your account
                                </td>
                            </tr>
                            <tr>
                                <td>##IsPayChecked##
                                </td>
                                <td>select for choose "Pay by Credit Card" option
                                </td>
                            </tr>
                            <tr>
                                <td>##CardTypeDropDownList##
                                </td>
                                <td>choose credit card type
                                </td>
                            </tr>
                            <tr>
                                <td>##CardNumberTextBox##
                                </td>
                                <td>enter credit card number
                                </td>
                            </tr>
                            <tr>
                                <td>##CardHolderTextBox##
                                </td>
                                <td>enter credit card holder name
                                </td>
                            </tr>
                            <tr>
                                <td>##CVV2TextBox##
                                </td>
                                <td>enter credit card cvv number
                                </td>
                            </tr>
                            <tr>
                                <td>##CardExpMonthDropDownList##
                                </td>
                                <td>select credit card expiry month
                                </td>
                            </tr>
                            <tr>
                                <td>##CardExpYearDropDownList##
                                </td>
                                <td>select credit card expiry year
                                </td>
                            </tr>
                            <tr>
                                <td>##IsGoogleCheckoutChecked##
                                </td>
                                <td>select for choose "Google Checkout" option
                                </td>
                            </tr>
                            <tr>
                                <td>##IsPaypalCheckoutChecked##
                                </td>
                                <td>select for choose "Pay by Paypal" option
                                </td>
                            </tr>
                            <tr>
                                <td>##PaypalEmail##
                                </td>
                                <td>enter paypal email id
                                </td>
                            </tr>
                            <tr>
                                <td>##TotalAmount##
                                </td>
                                <td>total amount need to pay
                                </td>
                            </tr>
                            <tr>
                                <td>##Comments##
                                </td>
                                <td>for if you have any note need to type
                                </td>
                            </tr>
                            <tr>
                                <td>##FileUpload##
                                </td>
                                <td>file upload control for attach document
                                </td>
                            </tr>
                        </table>
                                </div>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox TextMode="MultiLine" ID="txtEditHTML" runat="server" CssClass="form-control"
                                Width="600" Height="400"></asp:TextBox>
                        </div>
                        
                    </div>
                   
                </div>
                <%--<table style="margin-left: 50px">
                    
                    <tr>
                        <td valign="top">
                            
                            &nbsp;
                            
                            <br />
                            
                        </td>
                    </tr>
                </table>--%>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
