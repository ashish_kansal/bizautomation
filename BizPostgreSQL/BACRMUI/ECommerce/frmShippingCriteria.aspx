﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingCriteria.aspx.vb" MasterPageFile="~/common/ECommerceMenuMaster.Master" Inherits=".frmShippingCriteria" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Promotions</title>
    <script type="text/javascript">
        function OpenComDTL(a, b) {
            window.open('../Items/frmShippingCMPDTLs.aspx?ListItemID=' + a + '&ListItem=' + b, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <%--<asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>--%>
            </div> 
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Shipping Promotions 
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                  <div class="form-group text-bold">
                    <div class="form-inline">
                        <asp:CheckBox ID="chkFixedShipping1" runat="server" />
                         If sales order exceeds 
                    <asp:TextBox ID="txtFixShipping1OrderAmount" runat="server" CssClass="form-control" Width="150" Font-Bold="false"></asp:TextBox>
                        then shipping is
                    <asp:TextBox ID="txtFixShipping1Charge" runat="server" CssClass="form-control" Width="150" Font-Bold="false"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group text-bold">
                    <div class="form-inline">
                        <asp:CheckBox ID="chkFixedShipping2" runat="server" />
                         If sales order exceeds 
                    <asp:TextBox ID="txtFixShipping2OrderAmount" runat="server" CssClass="form-control" Width="150" Font-Bold="false"></asp:TextBox>
                        then shipping is
                    <asp:TextBox ID="txtFixShipping2Charge" runat="server" CssClass="form-control" Width="150" Font-Bold="false"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group text-bold">
                    <div class="form-inline">
                        <asp:CheckBox ID="chkFreeShipping" runat="server" />
                        If sales order exceeds
                    <asp:TextBox ID="txtFreeShippingAmount" runat="server" CssClass="form-control" Font-Bold="false" Width="150"></asp:TextBox>
                        and ship-to country is
                    <asp:DropDownList ID="ddlFreeShippingCountry" runat="server" CssClass="form-control" Font-Bold="false"></asp:DropDownList>
                        then shipping is free.
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>