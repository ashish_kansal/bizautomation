﻿Imports BACRM.BusinessLogic.Promotion
Imports BACRM.BusinessLogic.Common

Public Class frmPromotionOfferManage
    Inherits BACRMPage

#Region "Member Variables"
    Dim objPromotionOffer As PromotionOffer
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR MESSAGE
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If Not IsPostBack Then
                hfProId.Value = CCommon.ToLong(GetQueryStringVal("ProId"))
                Session("ProId") = hfProId.Value
                If CCommon.ToLong(hfProId.Value) > 0 Then
                    divValidity.Visible = True
                    pnlPromotionDetail.Visible = True
                Else
                    divValidity.Visible = False
                    pnlPromotionDetail.Visible = False
                    btnSaveClose.Visible = False
                End If


                hplIndividualItemsPurchased.Attributes.Add("onclick", "return OpenItemSelectionWindow(1,5," & CCommon.ToLong(hfProId.Value) & ")")
                hplItemClassificationPurchased.Attributes.Add("onclick", "return OpenItemSelectionWindow(2,5," & CCommon.ToLong(hfProId.Value) & ")")
                hplIndividualItemsForDiscount.Attributes.Add("onclick", "return OpenItemSelectionWindow(1,6," & CCommon.ToLong(hfProId.Value) & ")")
                hplItemClassificationForDiscount.Attributes.Add("onclick", "return OpenItemSelectionWindow(2,6," & CCommon.ToLong(hfProId.Value) & ")")
                hplFreeForSelectedItemsWithPrice.Attributes.Add("onclick", "return OpenItemSelectionWindow(4,6," & CCommon.ToLong(hfProId.Value) & ")")

                If CCommon.ToLong(hfProId.Value) > 0 Then
                    BindOrderPromo()
                    LoadDetails()
                    hplIndividualOrg.Attributes.Add("onclick", "return OpenConfig(1,1," & hfProId.Value & ")")
                    hplRelProfile.Attributes.Add("onclick", "return OpenConfig(1,2," & hfProId.Value & ")")
                End If
            Else
                If chkUseOrderPromo.Checked Then
                    divValidity.Style.Add("display", "none")
                    divCustomer.Style.Add("display", "none")
                Else
                    divValidity.Style.Add("display", "")
                    divCustomer.Style.Add("display", "")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
#End Region

#Region "Constructor"
    Sub New()
        objPromotionOffer = New PromotionOffer
    End Sub
#End Region

#Region "Private Methods"

    Private Sub BindOrderPromo()
        Try
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objPromotionOffer.GetCouponBasedOrderPromotion()

            ddlOrderPromo.DataSource = dt
            ddlOrderPromo.DataTextField = "vcProName"
            ddlOrderPromo.DataValueField = "numProId"
            ddlOrderPromo.DataBind()

            ddlOrderPromo.Items.Insert(0, New ListItem("-- Select One --", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub LoadDetails()
        Try
            objPromotionOffer = New PromotionOffer
            objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
            objPromotionOffer.Mode = 0
            Dim dtTable As DataTable = objPromotionOffer.GetPromotionOffer

            If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                txtOfferName.Text = CCommon.ToString(dtTable.Rows(0).Item("vcProName"))
                chkUseOrderPromo.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitUseOrderPromotion"))

                If chkUseOrderPromo.Checked Then
                    divValidity.Visible = False
                    divCustomer.Style.Add("display", "none")

                    If Not ddlOrderPromo.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numOrderPromotionID"))) Is Nothing Then
                        ddlOrderPromo.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numOrderPromotionID"))).Selected = True
                    End If
                Else
                    chkNeverExpires.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitNeverExpires"))

                    If Not chkNeverExpires.Checked Then
                        If Not dtTable.Rows(0).Item("dtValidFrom") Is DBNull.Value Then
                            calValidFromDate.SelectedDate = CCommon.ToSqlDate(dtTable.Rows(0).Item("dtValidFrom"))
                        End If

                        If Not dtTable.Rows(0).Item("dtValidTo") Is DBNull.Value Then
                            calValidToDate.SelectedDate = CCommon.ToSqlDate(dtTable.Rows(0).Item("dtValidTo"))
                        End If
                    End If
                End If

                ddlBasedOn.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("tintOfferTriggerValueType"))
                ddlBasedOnRange.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("tintOfferTriggerValueTypeRange"))
                If ddlBasedOnRange.SelectedValue = 2 Then
                    'divBetween.Visible = True
                    spnAnd.Visible = True
                    lblAnd.Visible = True
                    txtBetween.Visible = True
                ElseIf ddlBasedOnRange.SelectedValue = 1 Then
                    'divBetween.Visible = False
                    spnAnd.Visible = False
                    lblAnd.Visible = False
                    txtBetween.Visible = False
                End If
                txtValue.Text = CCommon.ToInteger(dtTable.Rows(0).Item("fltOfferTriggerValue"))
                txtBetween.Text = CCommon.ToInteger(dtTable.Rows(0).Item("fltOfferTriggerValueRange"))

                If CCommon.ToInteger(dtTable.Rows(0).Item("tintOfferBasedOn")) = 1 Then
                    rbIndividualItemPurchased.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintOfferBasedOn")) = 2 Then
                    rbItemClassificationPurchased.Checked = True
                    'ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintOfferBasedOn")) = 3 Then
                    '    rbItemCustomFields.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintOfferBasedOn")) = 4 Then
                    rbItemAll.Checked = True
                End If

                'Added by Neelam Kapila || 10/27/2017 - To fetch customer selection
                If CCommon.ToInteger(dtTable.Rows(0).Item("tintCustomersBasedOn")) = 1 Then
                    rbIndividualOrg.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintCustomersBasedOn")) = 2 Then
                    rbRelProfile.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintCustomersBasedOn")) = 3 Then
                    rbAllOrg.Checked = True
                End If

                txtDiscount.Text = CCommon.ToDouble(dtTable.Rows(0).Item("fltDiscountValue"))
                ddlDiscountType.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("tintDiscountType"))

                If CCommon.ToInteger(dtTable.Rows(0).Item("tintDiscoutBaseOn")) = 1 Then
                    rbIndividualItemsForDiscount.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintDiscoutBaseOn")) = 2 Then
                    rbItemClassificationForDiscount.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintDiscoutBaseOn")) = 3 Then
                    rbRelatedItemsForDiscount.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintDiscoutBaseOn")) = 4 Then ' For Quantity Selected, any item of equal or lesser value
                    rbFreeForAnyitemequalorlesser.Checked = True
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintDiscoutBaseOn")) = 5 Then
                    rbFreeForAnyitemequalorlesserWithPrice.Checked = True
                    txtFreeQtyPrice1.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monDiscountedItemPrice"))
                ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("tintDiscoutBaseOn")) = 6 Then
                    rbFreeForSelectedItemsWithPrice.Checked = True
                    txtFreeQtyPrice2.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monDiscountedItemPrice"))
                End If

                txtShortDesc.Text = CCommon.ToString(dtTable.Rows(0).Item("vcShortDesc"))
                txtlongdesc.Text = CCommon.ToString(dtTable.Rows(0).Item("vcLongDesc"))
                ddlCalDisc.SelectedValue = CCommon.ToInteger(dtTable.Rows(0).Item("tintItemCalDiscount"))

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function Save() As Long
        Try
            Dim errorMessage As String = ""
            Dim dtValidFrom As Date
            Dim dtValidTo As Date

            MaintainDiscountSelection()

            If CCommon.ToLong(hfProId.Value) > 0 Then
                If Not chkUseOrderPromo.Checked Then
                    If Not chkNeverExpires.Checked Then
                        If String.IsNullOrEmpty(calValidFromDate.SelectedDate.Trim()) Or String.IsNullOrEmpty(calValidToDate.SelectedDate.Trim()) Then
                            errorMessage = "Promotion From & To date is required. <br/>"
                        Else
                            If Not Date.TryParse(calValidFromDate.SelectedDate, dtValidFrom) Then
                                errorMessage = errorMessage & "Enter valid promotion From date. <br/>"
                            End If

                            If Not Date.TryParse(calValidToDate.SelectedDate, dtValidTo) Then
                                errorMessage = errorMessage & "Enter valid promotion To Date. <br/>"
                            End If

                            If dtValidTo < dtValidFrom Then
                                errorMessage = errorMessage & "To date must be greater than or equal to From date.<br/>"
                            End If
                        End If
                    End If
                End If

                If chkUseOrderPromo.Checked AndAlso CCommon.ToLong(ddlOrderPromo.SelectedValue) = 0 Then
                    errorMessage = errorMessage & "Select order promotion.<br/>"
                End If

                If CCommon.ToLong(txtValue.Text) <= 0 Then
                    errorMessage = errorMessage & "Enter Quantity/Amount to avail the promotion offer.<br/>"
                End If

                If CCommon.ToDouble(txtDiscount.Text) <= 0 Then
                    errorMessage = errorMessage & "Enter discount value to avail the promotion offer.<br/>"
                End If

                If CCommon.ToLong(ddlBasedOn.SelectedValue) = 1 AndAlso CCommon.ToInteger(txtValue.Text) > 1000 Then
                    errorMessage = errorMessage & "If promotion is based on the quantity then you can select maximum of 1000 as value.<br/>"
                ElseIf CCommon.ToLong(ddlBasedOn.SelectedValue) = 2 AndAlso CCommon.ToInteger(txtValue.Text) > 100000 Then
                    errorMessage = errorMessage & "If promotion is based on the amount then you can select maximum of 100, 0 as value.<br/>"
                End If

                If CCommon.ToLong(ddlBasedOnRange.SelectedValue) = 2 AndAlso txtBetween.Text = "" Then
                    errorMessage = errorMessage & "If you have selected Between then kindly provide proper range values.<br/>"
                End If

                If CCommon.ToShort(ddlDiscountType.SelectedValue) = 1 AndAlso CCommon.ToDouble(txtDiscount.Text) > 100 Then
                    errorMessage = errorMessage & "Discount percentage must be 100 or less.<br/>"
                End If

                If String.IsNullOrEmpty(errorMessage) Then
                    objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objPromotionOffer.PromotionID = CCommon.ToLong(hfProId.Value)
                    objPromotionOffer.PromotionName = txtOfferName.Text

                    If Not chkUseOrderPromo.Checked Then
                        If chkNeverExpires.Checked Then
                            objPromotionOffer.IsNeverExpires = True
                        Else
                            objPromotionOffer.ValidFrom = Convert.ToDateTime(dtValidFrom).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                            objPromotionOffer.ValidTo = Convert.ToDateTime(dtValidTo).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                        End If
                    Else
                        objPromotionOffer.IsNeverExpires = False
                        objPromotionOffer.ValidFrom = Nothing
                        objPromotionOffer.ValidTo = Nothing
                    End If

                    objPromotionOffer.IsUseOrderPromotion = chkUseOrderPromo.Checked
                    objPromotionOffer.OrderPromotionID = CCommon.ToLong(ddlOrderPromo.SelectedValue)
                    objPromotionOffer.PromotionOfferTriggerValue = CCommon.ToDouble(txtValue.Text)
                    objPromotionOffer.PromotionOfferTriggerValueRange = CCommon.ToDouble(txtBetween.Text)
                    objPromotionOffer.PromotionOfferTriggerValueType = CCommon.ToShort(ddlBasedOn.SelectedValue)
                    objPromotionOffer.PromotionOfferTriggerValueTypeRange = CCommon.ToShort(ddlBasedOnRange.SelectedValue)

                    If rbIndividualItemPurchased.Checked = True Then
                        objPromotionOffer.PromotionOfferBasedOn = 1
                    ElseIf rbItemClassificationPurchased.Checked = True Then
                        objPromotionOffer.PromotionOfferBasedOn = 2
                    ElseIf rbItemAll.Checked = True Then
                        objPromotionOffer.PromotionOfferBasedOn = 4
                    End If
                    objPromotionOffer.DiscountValue = CCommon.ToDouble(txtDiscount.Text)
                    objPromotionOffer.DiscountType = CCommon.ToShort(ddlDiscountType.SelectedValue)

                    If rbIndividualItemsForDiscount.Checked Then
                        objPromotionOffer.DiscountBasedOn = 1
                    ElseIf rbItemClassificationForDiscount.Checked Then
                        objPromotionOffer.DiscountBasedOn = 2
                    ElseIf rbRelatedItemsForDiscount.Checked Then
                        objPromotionOffer.DiscountBasedOn = 3
                    ElseIf rbFreeForAnyitemequalorlesser.Checked Then ' For Quantity Selected, any item of equal or lesser value
                        objPromotionOffer.DiscountBasedOn = 4
                    ElseIf rbFreeForAnyitemequalorlesserWithPrice.Checked Then
                        objPromotionOffer.DiscountBasedOn = 5
                        objPromotionOffer.DiscountedItemPrice = CCommon.ToDecimal(txtFreeQtyPrice1.Text)
                    ElseIf rbFreeForSelectedItemsWithPrice.Checked Then
                        objPromotionOffer.DiscountBasedOn = 6
                        objPromotionOffer.DiscountedItemPrice = CCommon.ToDecimal(txtFreeQtyPrice2.Text)
                    End If

                    'Added by Neelam Kapila || 10/27/2017 - To save customer selection
                    If rbIndividualOrg.Checked Then
                        objPromotionOffer.PromotionOfferBasedOnCustomer = 1
                    ElseIf rbRelProfile.Checked Then
                        objPromotionOffer.PromotionOfferBasedOnCustomer = 2
                    ElseIf rbAllOrg.Checked Then
                        objPromotionOffer.PromotionOfferBasedOnCustomer = 3
                    End If

                    objPromotionOffer.ShortDesc = txtShortDesc.Text
                    objPromotionOffer.LongDesc = txtlongdesc.Text
                    objPromotionOffer.ItemCalculateDiscountOn = CCommon.ToShort(ddlCalDisc.SelectedValue)

                    Return objPromotionOffer.ManagePromotionOffer(hdnSites.Value, hdnPromotionItems.Value, hdnDiscountItems.Value)
                Else
                    ShowMessage(errorMessage)
                    Return 0
                End If
            Else
                If String.IsNullOrEmpty(txtOfferName.Text) Then
                    ShowMessage("Promotion name is required.")
                    txtOfferName.Focus()
                    Return 0
                Else
                    objPromotionOffer.PromotionName = txtOfferName.Text.Trim()
                    objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPromotionOffer.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    Return objPromotionOffer.ManagePromotionOffer(hdnSites.Value, hdnPromotionItems.Value, hdnDiscountItems.Value)
                End If
            End If
        Catch ex As Exception
            If ex.Message = "SELECT_ITEMS_FOR_PROMOTIONS" Then
                ShowMessage("Select items to execute promotion.")
            ElseIf ex.Message = "SELECT_DISCOUNTED_ITEMS" Then
                ShowMessage("Select discounted items.")
            ElseIf ex.Message = "DUPLICATE-LEVEL1" Then
                ShowMessage("You've already created a promotion rule that targets this customer and item by name.")
            ElseIf ex.Message = "DUPLICATE-LEVEL2" Then
                ShowMessage("You’ve already created a promotion rule that targets this relationship / profile and one or more item specifically.")
            ElseIf ex.Message = "DUPLICATE-LEVEL3" Then
                ShowMessage("This option can only be used for a single promotion rule, and there already is one or more promotion rules in the system.")
            ElseIf ex.Message = "SELECT_INDIVIDUAL_CUSTOMER" Then
                ShowMessage("Select individual customer to execute promotion.")
            ElseIf ex.Message = "SELECT_CUSTOMERS" Then
                ShowMessage("Select customers to execute promotion.")
            ElseIf ex.Message = "SELECT_RELATIONSHIP_PROFILE" Then
                ShowMessage("A relationship and minimum of one profile must be selected.")
            ElseIf ex.Message.Contains("ORGANIZATION-ITEM") Then
                ShowMessage("You've already created a promotion rule that targets this customer and item by name.")
            ElseIf ex.Message.Contains("ORGANIZATION-ITEMCLASSIFICATIONS") Then
                ShowMessage("You've already created a promotion rule that targets this customer and item classifications.")
            ElseIf ex.Message.Contains("ORGANIZATION-ALLITEMS") Then
                ShowMessage("You've already created a promotion rule that targets this customer and all items.")
            ElseIf ex.Message.Contains("RELATIONSHIPPROFILE-ITEMS") Then
                ShowMessage("You can’t select this profile because it’s already in use by another promotion rule.")
            ElseIf ex.Message.Contains("RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS") Then
                ShowMessage("You can’t select this profile because it’s already in use by another promotion rule.")
            ElseIf ex.Message.Contains("RELATIONSHIPPROFILE-ALLITEMS") Then
                ShowMessage("You can’t select this profile because it’s already in use by another promotion rule.")
            ElseIf ex.Message.Contains("ALLORGANIZATIONS-ITEMS") Then
                ShowMessage("You've already created a promotion rule that targets all customers and item by name.")
            ElseIf ex.Message.Contains("ALLORGANIZATIONS-ITEMCLASSIFICATIONS") Then
                ShowMessage("You've already created a promotion rule that targets all customers and item classifications.")
            ElseIf ex.Message.Contains("ALLORGANIZATIONS-ALLITEMS") Then
                ShowMessage("You've already created a promotion rule that targets all customers and items.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End If
        End Try
    End Function

    Private Sub DisplayError(ByVal ex As Exception)
        Try
            DirectCast(Page.Master, ECommerceMenuMaster).ThrowError(ex)
        Catch exp As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Function MaintainDiscountSelection()
        If (ddlDiscountType.SelectedValue = 1 Or ddlDiscountType.SelectedValue = 2) Then
            rbFreeForAnyitemequalorlesser.Visible = False
            rbFreeForAnyitemequalorlesserWithPrice.Visible = False
            txtFreeQtyPrice1.Visible = False
            lblFreeForAnyitemequalorlesserWithPrice.Visible = False
            rbFreeForSelectedItemsWithPrice.Visible = False
            txtFreeQtyPrice2.Visible = False
            hplFreeForSelectedItemsWithPrice.Visible = False

            rbIndividualItemsForDiscount.Visible = True
            hplIndividualItemsForDiscount.Visible = True

            rbItemClassificationForDiscount.Visible = True
            hplItemClassificationForDiscount.Visible = True
            rbRelatedItemsForDiscount.Visible = True

            If (rbIndividualItemsForDiscount.Checked = True) Then
                rbIndividualItemsForDiscount.Checked = True
            ElseIf rbItemClassificationForDiscount.Checked = True Then
                rbItemClassificationForDiscount.Checked = True
            ElseIf rbRelatedItemsForDiscount.Checked = True Then
                rbRelatedItemsForDiscount.Checked = True
            End If
        Else
            rbFreeForAnyitemequalorlesser.Visible = True
            rbIndividualItemsForDiscount.Visible = True
            hplIndividualItemsForDiscount.Visible = True

            rbFreeForAnyitemequalorlesserWithPrice.Visible = True
            txtFreeQtyPrice1.Visible = True
            lblFreeForAnyitemequalorlesserWithPrice.Visible = True
            rbFreeForSelectedItemsWithPrice.Visible = True
            txtFreeQtyPrice2.Visible = True
            hplFreeForSelectedItemsWithPrice.Visible = True

            rbItemClassificationForDiscount.Visible = False
            hplItemClassificationForDiscount.Visible = False
            rbRelatedItemsForDiscount.Visible = False
        End If
    End Function

#End Region

#Region "Event Handlers"

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmPromotionOfferManage.aspx?ProId=" & lngPromotionId, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmPromotionOfferList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Dim lngPromotionId As Long = Save()
            If lngPromotionId > 0 Then
                Response.Redirect("../ECommerce/frmPromotionOfferList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Protected Sub ddlBasedOnRange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBasedOnRange.SelectedIndexChanged
        If ddlBasedOnRange.SelectedValue = 2 Then
            spnAnd.Visible = True
            lblAnd.Visible = True
            txtBetween.Visible = True
        ElseIf ddlBasedOnRange.SelectedValue = 1 Then
            spnAnd.Visible = False
            lblAnd.Visible = False
            txtBetween.Visible = False
        End If
    End Sub

    Protected Sub ddlDiscountType_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (ddlDiscountType.SelectedValue = "3") Then
            rbFreeForAnyitemequalorlesser.Visible = True
            rbIndividualItemsForDiscount.Visible = True
            rbItemClassificationForDiscount.Visible = False
            hplItemClassificationForDiscount.Visible = False
            rbRelatedItemsForDiscount.Visible = False
            hplIndividualItemsForDiscount.Text = "<label >Free (for the following selected items)</label><br />"


            rbFreeForAnyitemequalorlesserWithPrice.Visible = True
            txtFreeQtyPrice1.Visible = True
            lblFreeForAnyitemequalorlesserWithPrice.Visible = True
            rbFreeForSelectedItemsWithPrice.Visible = True
            txtFreeQtyPrice2.Visible = True
            hplFreeForSelectedItemsWithPrice.Visible = True
        ElseIf (ddlDiscountType.SelectedValue = "1" Or ddlDiscountType.SelectedValue = "2") Then
            rbFreeForAnyitemequalorlesser.Visible = False
            rbFreeForAnyitemequalorlesserWithPrice.Visible = False
            txtFreeQtyPrice1.Visible = False
            lblFreeForAnyitemequalorlesserWithPrice.Visible = False
            rbFreeForSelectedItemsWithPrice.Visible = False
            txtFreeQtyPrice2.Visible = False
            hplFreeForSelectedItemsWithPrice.Visible = False

            rbIndividualItemsForDiscount.Visible = True
            rbItemClassificationForDiscount.Visible = True
            hplItemClassificationForDiscount.Visible = True
            rbRelatedItemsForDiscount.Visible = True
            hplIndividualItemsForDiscount.Text = ""
            hplIndividualItemsForDiscount.Text = "<label for='rbIndividualItemsForDiscount'>Off the following Items (select individually)</label><br />"
        End If
    End Sub

#End Region

End Class