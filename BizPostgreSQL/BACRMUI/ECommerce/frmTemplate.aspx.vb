﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Imports Telerik.Web.UI
Partial Public Class frmTemplate
    Inherits BACRMPage

    Dim lngTemplateID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If CCommon.ToString(Request.QueryString("TemplateID")) <> "" Then
                lngTemplateID = CCommon.ToString(Request.QueryString("TemplateID"))
            End If
            Master.SelectedTabValue = "161"

            If Not IsPostBack Then
                hdnSiteID.Value = Session("SiteID")
                If lngTemplateID > 0 Then
                    LoadDetails()

                    lblTemplate.Text = "Edit Template"
                Else
                    lblTemplate.Text = "New Template"
                End If
                FillCustomTags()

                If CCommon.ToLong(Session("SiteID")) > 0 Then
                    'apply path for image manager
                    Dim strPath As String = "~/" & ConfigurationManager.AppSettings("BizCartImageVirtualDirectory") & "/" & CCommon.ToString(Session("SiteID"))
                    RadEditor1.ImageManager.ViewPaths = New String() {strPath}
                    RadEditor1.ImageManager.UploadPaths = New String() {strPath}
                    RadEditor1.ImageManager.DeletePaths = New String() {strPath}
                    'Add template manager path
                    strPath = "~/" & ConfigurationManager.AppSettings("EMailTemplatesVirtualDirectory")
                    RadEditor1.TemplateManager.ViewPaths = New String() {strPath}
                    'Add Css class
                    Dim objSite As New Sites
                    Dim dtStyle As DataTable
                    objSite.SiteID = Session("SiteID")
                    objSite.DomainID = Session("DomainID")
                    objSite.StyleType = 0
                    dtStyle = objSite.GetStyles()
                    For Each dr As DataRow In dtStyle.Rows
                        strPath = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(Session("SiteID")) & "\" & dr("StyleFileName").ToString
                        If File.Exists(strPath) Then
                            RadEditor1.CssFiles.Add("~/CartDoc/" & CCommon.ToString(Session("SiteID")) & "/" & dr("StyleFileName").ToString)
                        End If
                        strPath = ""
                    Next
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objTemplate As New Sites
        Try
            objTemplate.SiteID = Session("SiteID")
            objTemplate.TemplateID = lngTemplateID
            objTemplate.TemplateName = Server.HtmlEncode(txtTemplateName.Text.Trim())
            objTemplate.TemplateContent = Server.HtmlEncode(RadEditor1.Content.Trim())
            objTemplate.DomainID = Session("DomainID")
            objTemplate.UserCntId = Session("UserContactID")

            If objTemplate.ManageSiteTemplates() Then
                If CCommon.ToString(Request.QueryString("frm")) = "SitePage" Then
                    Response.Redirect("../ECommerce/frmSitePageList.aspx", True)
                Else
                    Response.Redirect("../ECommerce/frmTemplateList.aspx", True)
                End If
            Else
                litMessage.Text = "Template name already chosen for this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If CCommon.ToString(Request.QueryString("frm")) = "SitePage" Then
                Response.Redirect("../ECommerce/frmSitePageList.aspx", True)
            Else
                Response.Redirect("../ECommerce/frmTemplateList.aspx", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objTemplate As New Sites
            Dim dtTemplate As DataTable
            objTemplate.TemplateID = lngTemplateID
            objTemplate.SiteID = Session("SiteID")
            objTemplate.DomainID = Session("DomainID")
            dtTemplate = objTemplate.GetSiteTemplates()
            txtTemplateName.Text = Server.HtmlDecode(dtTemplate.Rows(0)("vcTemplateName"))
            RadEditor1.Content = Server.HtmlDecode(dtTemplate.Rows(0)("txtTemplateHTML"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub FillCustomTags()
        Try
            Dim objSite As New Sites
            Dim dtPageElements As DataTable
            objSite.SiteID = CCommon.ToLong(Session("SiteID"))
            dtPageElements = objSite.GetPageElementsMaster()
            Dim i As Integer

            Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("PageElement"), EditorDropDown)
            ddl.Items.Clear()
            For i = 0 To dtPageElements.Rows.Count - 1
                ddl.Items.Add(dtPageElements.Rows(i).Item("vcElementName").ToString(), dtPageElements.Rows(i).Item("vcTagName").ToString())
            Next

            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            Dim dtTemplates As DataTable
            dtTemplates = objSite.GetSiteTemplates()
            If lngTemplateID > 0 Then
                Dim keys(0) As DataColumn
                keys(0) = dtTemplates.Columns("numTemplateID")
                dtTemplates.PrimaryKey = keys
                dtTemplates.Rows.Find(lngTemplateID).Delete()
                dtTemplates.AcceptChanges()
            End If
            ddl = CType(RadEditor1.FindTool("Template"), EditorDropDown)
            For i = 0 To dtTemplates.Rows.Count - 1
                ddl.Items.Add(dtTemplates.Rows(i).Item("vcTemplateName"), dtTemplates.Rows(i).Item("vcTemplateCode").ToString().ToLower())
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveOnly_Click(sender As Object, e As EventArgs) Handles btnSaveOnly.Click
        Dim objTemplate As New Sites
        Try
            objTemplate.SiteID = Session("SiteID")
            objTemplate.TemplateID = lngTemplateID
            objTemplate.TemplateName = Server.HtmlEncode(txtTemplateName.Text.Trim())
            objTemplate.TemplateContent = Server.HtmlEncode(RadEditor1.Content.Trim())
            objTemplate.DomainID = Session("DomainID")
            objTemplate.UserCntID = Session("UserContactID")

            If objTemplate.ManageSiteTemplates() Then
               
            Else
                litMessage.Text = "Template name already chosen for this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnPublish_Click(sender As Object, e As EventArgs) Handles btnPublish.Click
        Try
            Dim objTemplate As New Sites
            objTemplate.SiteID = Session("SiteID")
            objTemplate.TemplateID = lngTemplateID
            objTemplate.TemplateName = Server.HtmlEncode(txtTemplateName.Text.Trim())
            objTemplate.TemplateContent = Server.HtmlEncode(RadEditor1.Content.Trim())
            objTemplate.DomainID = Session("DomainID")
            objTemplate.UserCntID = Session("UserContactID")

            If objTemplate.ManageSiteTemplates() Then
                UpdateSitePagesByTemplate()
            Else
                litMessage.Text = "Template name already chosen for this site."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub UpdateSitePagesByTemplate()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            objSite.SiteID = Session("SiteID")
            objSite.PublishSitePagesByTemplate(lngTemplateID)
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class