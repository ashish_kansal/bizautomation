﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Public Class frmShippingClassifications
    Inherits BACRMPage

    Dim objShippingRule As ShippingRule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                BindClassification()
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindClassification()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlClassifications, 461, Session("DomainID")) 'Shipping Class
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()

            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.numClassificationID = CCommon.ToLong(ddlClassifications.SelectedValue)
            objShippingRule.PercentAbove = CCommon.ToDouble(txtPercentAbove.Text)
            objShippingRule.FlatAmt = CCommon.ToDouble(txtFlatAmt.Text)
            objShippingRule.byteMode = 0

            Dim lngOutput As Long = objShippingRule.AddDeleteShippingExceptions()

            LoadDetails()
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                lblMessage.Text = "Classification already added."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub LoadDetails()
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()

            objShippingRule.DomainID = Session("DomainID")

            Dim dtShippingExceptions As DataTable = objShippingRule.GetShippingExceptions()

            dgShipClassification.DataSource = dtShippingExceptions
            dgShipClassification.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgShipClassification_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShipClassification.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.byteMode = 1 'delete
                objShippingRule.numShippingExceptionID = CCommon.ToLong(e.CommandArgument)
                objShippingRule.numClassificationID = 0
                objShippingRule.PercentAbove = 0
                objShippingRule.FlatAmt = 0

                objShippingRule.AddDeleteShippingExceptions()
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class