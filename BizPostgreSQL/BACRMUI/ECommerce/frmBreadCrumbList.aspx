﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBreadCrumbList.aspx.vb"
    Inherits=".frmBreadCrumbList" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript" language="javascript">
        function NewBreadCrumb() {
            if (document.getElementById('ddlSites').selectedIndex == 0) {
                var hplNew = document.getElementById("hplNew");
                hplNew.href = "#";
                alert("Please select a site to upload " + hplNew.innerHTML);
                document.getElementById('ddlSites').focus();
                return false;
            }
            return true;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12">
        <div class="row" id="Table1" runat="server">
            <div class="col-md-4 COL-MD-8 pull-right">
               
                <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
              <asp:HyperLink ID="hplNew" runat="server" CssClass="hyperlink" NavigateUrl="~/ECommerce/frmBreadCrumb.aspx">New Bread Crumb Item</asp:HyperLink>
                <asp:LinkButton ID="btnSaveOrder" runat="server" CssClass="btn btn-primary"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save Level</asp:LinkButton>
                <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Bread Crumb&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmBreadCrumbList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgBreadCrumb" AllowSorting="false" runat="server" Width="100%"
                    CssClass="table table-striped table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numBreadCrumbID" HeaderText="numBreadCrumbID">
                        </asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcDisplayName" SortExpression="" HeaderText="<font>Display Name</font>"
                            CommandName="Edit"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcPageURL" SortExpression="" HeaderText="<font>Target Page</font>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Level">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblBreadCrumbID" runat="server" Text='<%# Eval("numBreadCrumbID") %>'
                                    Visible="false"></asp:Label>
                                <asp:TextBox ID="txtOrder" runat="server" CssClass="signup" Width="20" Text='<%# Eval("tintLevel") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
