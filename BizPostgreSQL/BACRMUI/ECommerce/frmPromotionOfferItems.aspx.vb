﻿Imports BACRM.BusinessLogic.Promotion
Imports BACRM.BusinessLogic.Common

Public Class frmPromotionOfferItems
    Inherits BACRMPage

    Dim objPromotionOffer As PromotionOffer
    Dim lngProID As Long
    Dim StepValue As Short
    Dim Type As Short
    Dim shrtRecordType As Short

    'Type 1= Promotion
    'Type 2= Shipping Rules
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngProID = CCommon.ToLong(GetQueryStringVal("ProID"))
            StepValue = CCommon.ToShort(GetQueryStringVal("StepValue"))
            Type = CCommon.ToShort(GetQueryStringVal("Type"))
            shrtRecordType = CCommon.ToShort(GetQueryStringVal("RecordType"))

            If Not IsPostBack Then

                If lngProID > 0 Then
                    LoadDetails()
                End If
                If shrtRecordType = 2 Then
                    btnRemItem.Text = "Remove"
                    lblTitle.Text = "Promotion & Offer Items"
                ElseIf shrtRecordType = 3 Then
                    btnRemItem.Text = "Remove"
                ElseIf shrtRecordType = 4 Then
                    btnRemItem.Text = "Remove"
                End If
            End If

            If shrtRecordType = 2 Then
                lblTitle.Text = "Promotion & Offer Items"
            ElseIf shrtRecordType = 3 Then

                If StepValue = 1 Then
                    lblTitle.Text = "Select Item"
                ElseIf StepValue = 3 Then
                    lblTitle.Text = "Select Shipping Class"
                End If

            ElseIf shrtRecordType = 4 Then
                lblTitle.Text = "Shipping Class"
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try

            Select Case True
                Case shrtRecordType = 3 AndAlso dgItemClassification.Columns(2).HeaderText <> "Shipping Class"
                    dgItemClassification.Columns(2).HeaderText = "Shipping Class"
                Case shrtRecordType = 1 AndAlso dgItemClassification.Columns(2).HeaderText <> "Item Classification"
                    dgItemClassification.Columns(2).HeaderText = "Item Classification"
            End Select

            objPromotionOffer = New PromotionOffer
            Select Case StepValue
                Case 1
                    pnlItems.Visible = True
                    pnlItemCategoryification.Visible = False
                    pnlItemClassification.Visible = False
                    lblName.Text = "Select Item Classification"
                Case 2
                    pnlItems.Visible = False
                    pnlItemCategoryification.Visible = True
                    pnlItemClassification.Visible = False
                    lblName.Text = "Select Item Classification"
                Case 3
                    pnlItems.Visible = False
                    pnlItemCategoryification.Visible = False
                    pnlItemClassification.Visible = True
                    lblName.Text = "Select Shipping Class"

                Case 4
                    pnlItems.Visible = False
                    pnlItemCategoryification.Visible = False
                    pnlItemClassification.Visible = True
                    lblName.Text = "Select Shipping Class"
            End Select

            objPromotionOffer.RuleAppType = StepValue
            objPromotionOffer.PromotionID = lngProID
            objPromotionOffer.DomainID = Session("DomainID")
            objPromotionOffer.SiteID = Session("SiteID")
            objPromotionOffer.RecordType = shrtRecordType
            Dim ds As DataSet

            ds = objPromotionOffer.GetPromotionOfferDtl

            If objPromotionOffer.RuleAppType = 1 Then

                dgItems.DataSource = ds.Tables(0)
                dgItems.DataBind()

            ElseIf objPromotionOffer.RuleAppType = 2 Then

                ddlItemCategory.DataTextField = "vcCategoryName"
                ddlItemCategory.DataValueField = "numCategoryID"
                ddlItemCategory.DataSource = ds.Tables(1)
                ddlItemCategory.DataBind()
                ddlItemCategory.Items.Insert(0, New ListItem("--Select One--", 0))

                dgItemCategory.DataSource = ds.Tables(0)
                dgItemCategory.DataBind()

            ElseIf objPromotionOffer.RuleAppType = 3 Then

                ddlItemClassification.DataTextField = "vcData"
                ddlItemClassification.DataValueField = "numListItemID"
                ddlItemClassification.DataSource = ds.Tables(0)
                ddlItemClassification.DataBind()
                ddlItemClassification.Items.Insert(0, New ListItem("--Select One--", 0))

                dgItemClassification.DataSource = ds.Tables(1)
                dgItemClassification.DataBind()

            ElseIf objPromotionOffer.RuleAppType = 4 Then

                ddlItemClassification.DataTextField = "vcData"
                ddlItemClassification.DataValueField = "numListItemID"
                ddlItemClassification.DataSource = ds.Tables(0)
                ddlItemClassification.DataBind()
                ddlItemClassification.Items.Insert(0, New ListItem("--Select One--", 0))

                dgItemClassification.DataSource = ds.Tables(1)
                dgItemClassification.DataBind()

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAddItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddItem.Click
        Try
            If radItem.SelectedValue <> "" Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = lngProID
                objPromotionOffer.RuleAppType = StepValue
                objPromotionOffer.POValue = radItem.SelectedValue
                objPromotionOffer.Mode = 0
                objPromotionOffer.RecordType = shrtRecordType
                objPromotionOffer.ManagePromotionOfferDTL()
                LoadDetails()
            End If
        Catch ex As Exception

            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemItem.Click
        Try
            objPromotionOffer = New PromotionOffer
            For Each Item As DataGridItem In dgItems.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.RecordType = shrtRecordType
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddItemCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemCategory.Click
        Try
            If ddlItemCategory.SelectedValue > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = lngProID
                objPromotionOffer.RuleAppType = StepValue
                objPromotionOffer.POValue = ddlItemCategory.SelectedValue
                objPromotionOffer.Mode = 0
                objPromotionOffer.RecordType = shrtRecordType
                objPromotionOffer.ManagePromotionOfferDTL()
                LoadDetails()
            End If
        Catch ex As Exception

            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemoveItemCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveItemCategory.Click
        Try
            objPromotionOffer = New PromotionOffer
            For Each Item As DataGridItem In dgItemCategory.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.RecordType = shrtRecordType
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddItemClassification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemClassification.Click
        Try
            If ddlItemClassification.SelectedValue > 0 Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.PromotionID = lngProID
                objPromotionOffer.RuleAppType = StepValue
                objPromotionOffer.POValue = ddlItemClassification.SelectedValue
                objPromotionOffer.Mode = 0
                objPromotionOffer.RecordType = shrtRecordType
                objPromotionOffer.ManagePromotionOfferDTL()
                LoadDetails()
            End If
        Catch ex As Exception

            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemoveItemClassification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveItemClassification.Click
        Try
            objPromotionOffer = New PromotionOffer
            For Each Item As DataGridItem In dgItemClassification.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPromotionOffer.PODTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPromotionOffer.Mode = 1
                    objPromotionOffer.RecordType = shrtRecordType
                    objPromotionOffer.ManagePromotionOfferDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class