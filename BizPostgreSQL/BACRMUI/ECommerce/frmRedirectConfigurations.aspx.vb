﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Item

Public Class frmRedirectConfigurations
    Inherits BACRMPage

#Region "Global Declaration"
    Dim lngSiteID As Long
#End Region

    Public dtRedirects As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            'If Not IsPostBack Then
            '    ' CreateRedirectsDataTable()
            '    LoadRedirectDetails(boolPostback:=False)
            'Else
            '    Dim SiteId As Long = CCommon.ToLong(Session("SiteID"))

            'End If

            If Not IsPostBack Then
                BindSites()

                If Not Session("SiteID") Is Nothing And CCommon.ToLong(Session("SiteID")) > 0 Then
                    pnlDetail.Visible = True
                    ddlSites.SelectedValue = Session("SiteID")
                    lngSiteID = CCommon.ToLong(Session("SiteID"))
                    LoadRedirectDetails(boolPostback:=False)

                Else
                    pnlDetail.Visible = False
                End If

                ' btnSave.Attributes.Add("onclick", "javascript:return ValidateSave();")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            LoadRedirectDetails(boolAddNew:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveRedirectConfigs()

            LoadRedirectDetails()
            ClearControls()

            'Catch sqlex As SqlClient.SqlException
            '    Dim strError As String = ""
            '    'strError = ex.Message

            '    If sqlex.Message = "URL_ALREADY_MAPPED" Then

            '        strError = "URL already mapped!., You can not map same URL for different redirections which lead to infinite looping. "
            '        lblErrMessage.Text = strError

            '    Else
            '        ' strError = ex.Message
            '        ExceptionModule.ExceptionPublish(sqlex, Session("DomainID"), Session("UserContactID"), Request)
            '        Response.Write(sqlex)
            '    End If

        Catch ex As Exception
            Dim strError As String = ""
            Dim ErrorRedirectConfigId As Long
            Dim strErrorMess As String()
            Dim objSites As Sites

            'strError = ex.Message
            If Not ex.Message.IndexOf("URL_ALREADY_MAPPED") = -1 Then
                strErrorMess = ex.Message.Split(",")
                If strErrorMess.Length > 1 Then
                    ErrorRedirectConfigId = CCommon.ToLong(strErrorMess(1))
                    objSites = New Sites
                    objSites.RedirectConfigID = ErrorRedirectConfigId
                    objSites.DomainID = CCommon.ToLong(Session("DomainID"))
                    objSites.SiteID = ddlSites.SelectedValue
                    objSites.TotRecs = 0
                    objSites.PageSize = 1
                    objSites.CurrentPage = 1
                    dtRedirects = objSites.GetRedirectConfig()

                End If
                'End If

                'If ex.Message = "URL_ALREADY_MAPPED" Then

                strError = "URL already mapped!., You can not map same URL for different redirections which lead to infinite looping. <br />" + Environment.NewLine
                strError += "Either of the URL found in <br />" + Environment.NewLine
                strError += CCommon.ToString(dtRedirects.Rows(0)("vcOldUrl")) + "       ------------->       " + CCommon.ToString(dtRedirects.Rows(0)("vcNewUrl"))

                lblErrMessage.Text = strError

            Else
                ' strError = ex.Message
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If

        End Try
    End Sub

    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            SaveRedirectConfigs()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If txtDelRedirectConfigIds.Text.Trim().Length > 1 Then
                Dim ConfigIds As String() = txtDelRedirectConfigIds.Text.Trim(",").Split(",")
                For i As Integer = 0 To ConfigIds.Length - 1
                    Dim objSites As New Sites
                    objSites.DomainID = Session("DomainID")
                    objSites.SiteID = Session("SiteID")
                    objSites.RedirectConfigID = ConfigIds(i)
                    objSites.DeleteRedirectConfig()

                Next
                LoadRedirectDetails(boolPostback:=False)

            End If



        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub


    Public Sub ClearControls()
        Try
            txtOldURL.Text = String.Empty
            txtNewURL.Text = String.Empty
            ddlProductCategory.ClearSelection()
            ddlRedirectType.ClearSelection()
            ddlReferenceItem.ClearSelection()
            ddlRedirectType.ClearSelection()


        Catch ex As Exception
            Throw ex

        End Try

    End Sub
    Private Sub ddlReferenceType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReferenceType.SelectedIndexChanged
        Try
            If ddlReferenceType.SelectedValue > 0 Then
                If ddlReferenceType.SelectedValue = 1 Then
                    BindCatergory()

                ElseIf ddlReferenceType.SelectedValue = 2 Then
                    BindProdCatergory()
                    ddlReferenceItem.Items.Clear()

                ElseIf ddlReferenceType.SelectedValue = 3 Then

                    BindPages()

                End If
            Else
                ddlReferenceItem.Items.Clear()

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlProductCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductCategory.SelectedIndexChanged
        Try
            If ddlProductCategory.SelectedValue > 0 Then
                BindProducts()

            ElseIf ddlProductCategory.SelectedValue = 0 Then
                ddlReferenceType.Items.Clear()

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try

    End Sub

    'Private Sub ddlReferenceType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReferenceType.SelectedIndexChanged
    '    Try
    '        If ddlReferenceItem.SelectedValue = 2 Then

    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)

    '    End Try

    'End Sub

    Private Sub ddlReferenceItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReferenceItem.SelectedIndexChanged
        Try
            Dim url1 As String
            Dim objSites As New Sites
            Dim dt As DataTable

            If ddlReferenceItem.SelectedValue > 0 Then
                'Dim objSites As New Sites
                'Dim dt As DataTable
                objSites.SiteID = CCommon.ToLong(Session("SiteID"))
                objSites.DomainID = CCommon.ToLong(Session("DomainID"))
                dt = objSites.GetSites()
                Dim SiteLiveUrl As String = "http://" + dt.Rows(0)("vcPreviewURL").ToString
                If ddlReferenceType.SelectedValue = 1 Then
                    url1 = SiteLiveUrl + "/SubCategory/" + Sites.GenerateSEOFriendlyURL(ddlReferenceItem.SelectedItem.Text) + "/" + CCommon.ToString(ddlReferenceItem.SelectedValue)
                    'url1 = SiteLiveUrl + "/ProductList.aspx?Cat=" + CCommon.ToString(ddlReferenceItem.SelectedValue) + "&CatName=" + Sites.GenerateSEOFriendlyURL(ddlReferenceItem.SelectedItem.Text)

                ElseIf ddlReferenceType.SelectedValue = 2 Then
                    Dim objItems As New CItems
                    objItems.byteMode = 13
                    objItems.SiteID = Sites.ToLong(Session("SiteID"))
                    objItems.CategoryID = ddlReferenceItem.SelectedValue
                    dt = objItems.SeleDelCategory()

                    'url1 = SiteLiveUrl + "/SubCategory/" + Sites.GenerateSEOFriendlyURL(ddlReferenceItem.SelectedItem.Text) + "/" + CCommon.ToString(ddlReferenceItem.SelectedValue)
                    url1 = SiteLiveUrl + "/Product/" + Sites.GenerateSEOFriendlyURL(dt.Rows(0)("vcCategoryName")) + "/" + Sites.GenerateSEOFriendlyURL(ddlReferenceItem.SelectedItem.Text) + "/" + ddlReferenceItem.SelectedValue

                    'url1 = SiteLiveUrl + "/Product.aspx?ItemID=" + ddlReferenceItem.SelectedValue

                ElseIf ddlReferenceType.SelectedValue = 3 Then

                    objSites.DomainID = CCommon.ToLong(Session("DomainID"))
                    objSites.SiteID = CCommon.ToLong(Session("SiteID"))
                    objSites.PageID = ddlReferenceItem.SelectedValue
                    dt = objSites.GetPages().Tables(0)

                    url1 = SiteLiveUrl + "/" + dt.Rows(0)("vcPageURL")

                End If

                txtNewURL.Text = url1
                'lblNewURLstring.Text = url1

            Else

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlSites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSites.SelectedIndexChanged
        Try
            If ddlSites.SelectedValue > 0 Then
                pnlDetail.Visible = True
                Session("SiteID") = ddlSites.SelectedValue

                LoadRedirectDetails(boolPostback:=False)
            Else
                pnlDetail.Visible = False
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvRedirects_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRedirects.RowCommand
        Try
            'If e.CommandName = "DeleteRow" Then
            '    Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
            '    Dim RedirectConfigID As Long = CCommon.ToLong(CType(row.FindControl("lblRedirectConfigID"), Label).Text)
            '    If RedirectConfigID > 0 Then
            '        Dim objSites As New Sites
            '        objSites.DomainID = Session("DomainID")
            '        objSites.SiteID = Session("SiteID")
            '        objSites.RedirectConfigID = RedirectConfigID
            '        objSites.DeleteRedirectConfig()

            '        LoadRedirectDetails(boolPostback:=False)
            '    End If

            'End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvRedirects_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRedirects.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Dim btnDelete As Button = CType(e.Row.FindControl("btnDelete"), Button)
                'btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub CreateRedirectsDataTable()
        Try

            If Not IsNothing(dtRedirects) Then

                If dtRedirects.Columns.Count > 0 Then
                    dtRedirects.Columns.Clear()

                End If

                dtRedirects.Columns.Add("numClassID")
                dtRedirects.Columns.Add("ID")
                dtRedirects.Columns.Add("OldURL")
                dtRedirects.Columns.Add("NewURL")

            End If

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "--Select One--")
            ddlSites.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCatergory()
        Try
            Dim objItems As New CItems
            Dim dt As DataTable
            objItems.byteMode = 13
            objItems.SiteID = Sites.ToLong(Session("SiteID"))
            dt = objItems.SeleDelCategory()
            ddlReferenceItem.DataSource = dt
            ddlReferenceItem.DataTextField = "vcCategoryName"
            ddlReferenceItem.DataValueField = "numCategoryID"
            ddlReferenceItem.DataBind()
            ddlReferenceItem.Items.Insert(0, "--Select One--")
            ddlReferenceItem.Items.FindByText("--Select One--").Value = 0


        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub BindProdCatergory()
        Try
            Dim objItems As New CItems
            Dim dt As DataTable
            objItems.byteMode = 13
            objItems.SiteID = Sites.ToLong(Session("SiteID"))
            dt = objItems.SeleDelCategory()
            ddlProductCategory.DataSource = dt
            ddlProductCategory.DataTextField = "vcCategoryName"
            ddlProductCategory.DataValueField = "numCategoryID"
            ddlProductCategory.DataBind()
            ddlProductCategory.Items.Insert(0, "--Select One--")
            ddlProductCategory.Items.FindByText("--Select One--").Value = 0


        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub BindProducts()
        Try
            Dim objItems As New CItems
            Dim dt As DataTable
            objItems.byteMode = 13
            objItems.SiteID = Sites.ToLong(Session("SiteID"))
            objItems.CategoryID = ddlProductCategory.SelectedValue
            objItems.WarehouseID = 0
            objItems.SortBy = 1
            objItems.CurrentPage = 1
            objItems.PageSize = 100

            dt = objItems.GetItemsForECommerce().Tables(0)

            'For Each dr As DataRow In dt.Rows

            'Next
            ddlReferenceItem.DataSource = dt
            ddlReferenceItem.DataTextField = "vcItemName"
            ddlReferenceItem.DataValueField = "numItemCode"
            ddlReferenceItem.DataBind()
            ddlReferenceItem.Items.Insert(0, "--Select One--")
            ddlReferenceItem.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub BindPages()
        Try
            Dim objSites As New Sites
            Dim dt As DataTable
            objSites.DomainID = CCommon.ToLong(Session("DomainID"))
            objSites.SiteID = CCommon.ToLong(Session("SiteID"))
            objSites.PageID = 0
            dt = objSites.GetPages().Tables(0)
            ddlReferenceItem.DataSource = dt
            ddlReferenceItem.DataTextField = "vcPageName"
            ddlReferenceItem.DataValueField = "numPageID"
            ddlReferenceItem.DataBind()
            ddlReferenceItem.Items.Insert(0, "--Select One--")
            ddlReferenceItem.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub SaveRedirectConfigs()
        Try
            Dim objSites As Sites
            If Session("SiteID") > 0 Then
                Dim gvRow As GridViewRow
                Dim i As Integer
                objSites = New Sites
                objSites.RedirectConfigID = 0
                objSites.DomainID = CCommon.ToLong(Session("DomainID"))
                objSites.ContactID = CCommon.ToLong(Session("UserContactID"))
                objSites.SiteID = CCommon.ToLong(Session("SiteID"))
                objSites.OldUrl = txtOldURL.Text.Trim()
                objSites.NewUrl = txtNewURL.Text.Trim()
                objSites.RedirectType = CCommon.ToLong(ddlRedirectType.SelectedValue)
                If objSites.RedirectType = 1 Then
                    objSites.ReferenceType = 0
                    objSites.ReferenceID = 0
                ElseIf objSites.RedirectType = 1 Then

                    objSites.ReferenceType = CCommon.ToLong(ddlReferenceType.SelectedValue)
                    objSites.ReferenceID = CCommon.ToLong(ddlReferenceItem.SelectedValue)
                End If


                objSites.ManageRedirectConfig()

            End If
        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            Dim SiteId As Long = CCommon.ToLong(Session("SiteID"))
            If SiteId > 0 Then
                Dim objSites As Sites
                objSites = New Sites
                objSites.RedirectConfigID = 0
                objSites.DomainID = CCommon.ToLong(Session("DomainID"))
                objSites.SiteID = SiteId
                objSites.TotRecs = 0
                objSites.PageSize = CCommon.ToInteger(Session("PagingRows"))
                If txtCurrentPageCorr.Text.Trim = "" Then
                    txtCurrentPageCorr.Text = 1
                    'Else
                    '    txtCurrentPageCorr.Text = txtCurrentPageCorr1.Text
                End If

                objSites.CurrentPage = CCommon.ToInteger(txtCurrentPageCorr.Text)
                dtRedirects = objSites.GetRedirectConfig()
                gvRedirects.DataSource = dtRedirects
                gvRedirects.DataBind()
                bizPager.PageSize = CCommon.ToInteger(Session("PagingRows"))
                bizPager.RecordCount = objSites.TotRecs
                bizPager.CurrentPageIndex = txtCurrentPageCorr.Text
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try

    End Sub

    Private Sub LoadRedirectDetails(Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Try
            Dim SiteId As Long = CCommon.ToLong(Session("SiteID"))
            If SiteId > 0 Then

                Dim j As Integer
                Dim i As Integer
                Dim objSites As Sites
                Dim dtRedirects As New DataTable
                Dim dtrow As DataRow
                Dim deleteRowIndex As Integer = -1

                objSites = New Sites
                objSites.RedirectConfigID = 0
                objSites.DomainID = CCommon.ToLong(Session("DomainID"))
                objSites.SiteID = SiteId
                objSites.TotRecs = 0
                objSites.PageSize = CCommon.ToInteger(Session("PagingRows"))
                If txtCurrentPageCorr.Text.Trim = "" Then
                    txtCurrentPageCorr.Text = 1
                    'Else
                    '    txtCurrentPageCorr.Text = txtCurrentPageCorr1.Text
                End If

                objSites.CurrentPage = CCommon.ToInteger(txtCurrentPageCorr.Text)

                dtRedirects = objSites.GetRedirectConfig()

                'If boolPostback = True Then
                '    'dtRedirects.Rows.Clear()
                '    Dim gvRow As GridViewRow
                '    For i = 0 To gvRedirects.Rows.Count - 1
                '        If i <> deleteRowIndex Then
                '            dtrow = dtRedirects.NewRow
                '            gvRow = gvRedirects.Rows(i)

                '        End If
                '    Next
                'End If

                'If dtRedirects.Rows.Count < 2 Then
                '    Dim lintRowcount As Int16 = 2 - dtRedirects.Rows.Count
                '    For j = 0 To lintRowcount - 1
                '        dtrow = dtRedirects.NewRow
                '        dtrow.Item("numRedirectConfigID") = 0
                '        dtRedirects.Rows.Add(dtrow)
                '    Next
                'End If

                'If boolAddNew Then
                '    For j = 0 To 1
                '        dtrow = dtRedirects.NewRow
                '        dtrow.Item("numRedirectConfigID") = 0
                '        dtRedirects.Rows.Add(dtrow)
                '    Next
                'End If

                gvRedirects.DataSource = dtRedirects
                gvRedirects.DataBind()

                bizPager.PageSize = CCommon.ToInteger(Session("PagingRows"))
                bizPager.RecordCount = objSites.TotRecs
                bizPager.CurrentPageIndex = txtCurrentPageCorr.Text

            End If

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub LoadNewRow(Optional deleteRowIndex As Integer = -1, Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Dim j As Integer
        ' Dim dtItems As New DataTable
        Dim objSites As New Sites

        Try
            CreateRedirectsDataTable()
            Dim dtrow As DataRow

            If boolPostback = True Then
                dtRedirects.Rows.Clear()

                Dim i As Integer
                Dim gvRow As GridViewRow

                For i = 0 To gvRedirects.Rows.Count - 1
                    If i <> deleteRowIndex Then
                        dtrow = dtRedirects.NewRow
                        gvRow = gvRedirects.Rows(i)


                        dtrow.Item("OldURL") = CCommon.ToDecimal(CType(gvRow.FindControl("txtOldURL"), TextBox).Text)
                        dtrow.Item("NewURL") = CCommon.ToDecimal(CType(gvRow.FindControl("txtNewURL"), TextBox).Text)

                        dtRedirects.Rows.Add(dtrow)
                    End If
                Next
            End If

            If dtRedirects.Rows.Count < 2 Then
                Dim lintRowcount As Int16 = 2 - dtRedirects.Rows.Count
                For j = 0 To lintRowcount - 1
                    dtrow = dtRedirects.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = 0
                    dtrow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtRedirects.Rows.Add(dtrow)
                Next
            End If

            If boolAddNew Then
                For j = 0 To 1
                    dtrow = dtRedirects.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = 0
                    dtrow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtRedirects.Rows.Add(dtrow)
                Next
            End If



            gvRedirects.DataSource = dtRedirects
            gvRedirects.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrentPageCorr.Text = bizPager.CurrentPageIndex
            LoadRedirectDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class