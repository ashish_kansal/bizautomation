﻿Imports BACRM.BusinessLogic.Promotion
Imports BACRM.BusinessLogic.Common

Public Class frmPromotionOfferList
    Inherits BACRMPage

#Region "Member Variables"
    Dim objPromotionOffer As PromotionOffer
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If

            If Not IsPostBack Then
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub LoadDetails()
        Try
            objPromotionOffer = New PromotionOffer
            objPromotionOffer.PromotionID = 0
            objPromotionOffer.Mode = 1
            objPromotionOffer.DomainID = Session("DomainID")
            objPromotionOffer.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            dgOfferList.DataSource = objPromotionOffer.GetPromotionOffer
            dgOfferList.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As Exception)
        DirectCast(Page.Master, ECommerceMenuMaster).ThrowError(ex)
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub dgOfferList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOfferList.ItemCommand
        Try
            If e.CommandName = "Edit" Then
                If CCommon.ToBool(DirectCast(e.Item.FindControl("hdnIsOrderPromotion"), HiddenField).Value) Then
                    Response.Redirect("../ECommerce/frmPromotionOrderBasedManage.aspx?ProID=" & e.CommandArgument)
                Else
                    Response.Redirect("../ECommerce/frmPromotionOfferManage.aspx?ProID=" & e.CommandArgument)
                End If
            ElseIf e.CommandName = "Delete" Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                objPromotionOffer.PromotionID = e.CommandArgument
                objPromotionOffer.DelPromotionOffer()
                LoadDetails()
            ElseIf e.CommandName = "Enable/Disable" Then
                objPromotionOffer = New PromotionOffer
                objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                objPromotionOffer.PromotionID = e.CommandArgument
                objPromotionOffer.EnableDisablePromotionOffer()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMO_USING_ORDER_PROMO") Then
                ShowMessage("Promotion which you want to delete is used in item promotion.")
            ElseIf ex.Message.Contains("PROMOTION_CONFIGURATION_INVALID") Then
                ShowMessage("Can't enable promotion as its configuration is not completed successfully.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End If
        End Try
    End Sub

    Private Sub dgOfferList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOfferList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                Dim lkbEnableDisable As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                lkbEnableDisable = e.Item.FindControl("lkbEnableDisable")

                If DirectCast(e.Item.DataItem, DataRowView)("bitEnabled") = "1" Then
                    lkbEnableDisable.Text = "Disable Rule"
                Else
                    lkbEnableDisable.Text = "Enable Rule"
                End If

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
#End Region

End Class