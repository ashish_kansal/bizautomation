﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSiteCategory.aspx.vb" Inherits=".frmSiteCategory"
    MasterPageFile="~/common/ECommerceMenuMaster.Master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">
        function treeExpandAllNodes() {
            var treeView = $find("rtvSiteCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }

        function treeCollapseAllNodes() {
            var treeView = $find("rtvSiteCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
     <div class="input-part">
        <div class="right-input">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right" class="normal1">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save ">
                        </asp:Button>                       
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="normal4" align="center" colspan="2" valign="top">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblSite" runat="server"></asp:Label>
    Site Category
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
     <asp:Table ID="table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="asptable" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table border="0" cellspacing="2px" cellpadding="2px">
                    <tr>
                        <td align ="right" style="padding-left:10px;" width="170px">
                            Select Site :
                        </td>
                        <td style="padding-left:10px;">
                             <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan ="2">
                            <asp:Panel ID ="pnlDetail"  runat ="server" >
                            <table width="100%">
                                <tr>
                                      <td valign ="top" style="padding-left:10px;" width="180px">
                            Select categories which you <br />
                            want to show on selected site :
                        </td>
                                      <td align="left">
                            
                                <a href="javascript: treeExpandAllNodes();" style="color: Gray;">Expand All </a>
                                &nbsp; <a href="javascript: treeCollapseAllNodes();" style="color: Gray;">Collapse All
                                </a>
                                <telerik:RadTreeView ID="rtvSiteCategory" CheckBoxes="true"  ClientIDMode ="Static"  runat="server" >
                                                        <DataBindings>
                                                            <telerik:RadTreeNodeBinding Expanded="true" ></telerik:RadTreeNodeBinding>
                                                        </DataBindings>
                                                    </telerik:RadTreeView>
                        </td>
                                </tr>
                            </table>
                                    </asp:Panel>
                        </td>
                     
                    </tr>
                </table>
            </asp:TableCell></asp:TableRow></asp:Table>

</asp:Content>
