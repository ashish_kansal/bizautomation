﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Partial Public Class SiteSwitch
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindSites()
                If Not Session("SiteID") Is Nothing Then
                    ddlSites.SelectedValue = Session("SiteID")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "--Select One--")
            ddlSites.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlSites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSites.SelectedIndexChanged
        Try
            Session("SiteID") = ddlSites.SelectedValue
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = ddlSites.SelectedValue
            objSite.DomainID = Session("DomainID")
            dtSite = objSite.GetSites()
            Session("SitePreviewURL") = dtSite.Rows(0)("vcPreviewURL")
            Response.Redirect(Request.Url.OriginalString)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ibtnPublish_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnPublish.Click
        Try
            Dim objSite As New Sites
            Dim dsPage As DataSet

            Dim dtPages As DataTable
            objSite.PageID = 0
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            dtPages = objSite.GetPages().Tables(0)
            For Each drPage As DataRow In dtPages.Rows
                If CCommon.ToBool(drPage("bitIsActive")) = True Then
                    objSite.PageID = drPage("numPageID")
                    objSite.SiteID = Session("SiteID")
                    objSite.DomainID = Session("DomainID")
                    dsPage = objSite.GetPages()
                    If dsPage.Tables(0).Rows.Count > 0 Then
                        objSite.ManageAspxPages(Long.Parse(Session("SiteID")), dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString(), CCommon.ToBool(dsPage.Tables(0).Rows(0).Item("bitIsMaintainScroll")))
                    End If
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class