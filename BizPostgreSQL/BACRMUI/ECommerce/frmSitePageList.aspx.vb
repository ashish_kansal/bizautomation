﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO

Partial Public Class frmSitePageList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(13, 41)
            If Not IsPostBack Then

                BindData()
                hplNew.Attributes.Add("onclick", "NewSitePage();")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Sub BindData()
        Try
            If Session("SiteID") > 0 Then
                Dim objSite As New Sites
                Dim dtPages As DataTable
                objSite.PageID = 0
                objSite.SiteID = Session("SiteID")
                objSite.DomainID = Session("DomainID")
                dtPages = objSite.GetPages().Tables(0)
                dgPages.DataSource = dtPages
                dgPages.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPages.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../ECommerce/frmSitePage.aspx?PageID=" & e.Item.Cells(0).Text)
            If e.CommandName = "Delete" Then
                Dim objSite As New Sites
                objSite.PageID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                objSite.DeleteSitePages()
                BindData()
            End If
            If e.CommandName = "Publish" Then
                Dim objSite As New Sites
                Dim dsPage As DataSet

                objSite.PageID = e.CommandArgument
                objSite.SiteID = Session("SiteID")
                objSite.DomainID = Session("DomainID")
                dsPage = objSite.GetPages()
                If dsPage.Tables(0).Rows.Count > 0 Then
                    objSite.PageID = e.Item.Cells(0).Text
                    objSite.DomainID = Session("DomainID")
                    objSite.UserCntId = Session("UserContactID")
                    objSite.SiteID = Session("SiteID")
                    objSite.IsActive = 1
                    objSite.UpdateSitePageStatus()
                    objSite.ManageAspxPages(Long.Parse(Session("SiteID")), dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString(), CCommon.ToBool(dsPage.Tables(0).Rows(0).Item("bitIsMaintainScroll")))
                End If
                BindData()
            End If

            If e.CommandName = "Status" Then
                Dim objSite As New Sites
                Dim dsPage As DataSet
                objSite.PageID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                objSite.UserCntId = Session("UserContactID")
                objSite.SiteID = Session("SiteID")
                objSite.IsActive = IIf(e.CommandArgument, 0, 1)
                objSite.UpdateSitePageStatus()
                Dim strDirectory As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(Session("SiteID"))
                If Directory.Exists(strDirectory) = True Then
                    dsPage = objSite.GetPages()
                    'Dim SitePage As System.IO.File()
                    If File.Exists(strDirectory & "\" & dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString()) = True Then
                        If CCommon.ToBool(e.CommandArgument) = True Then
                            File.Delete(strDirectory & "\" & dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString())
                        End If
                    End If
                    If CCommon.ToBool(e.CommandArgument) = False Then
                        objSite.ManageAspxPages(CCommon.ToLong(Session("SiteID")), dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString(), CCommon.ToString(dsPage.Tables(0).Rows(0).Item("bitIsMaintainScroll")))
                    End If
                End If
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgPages_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPages.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub


End Class