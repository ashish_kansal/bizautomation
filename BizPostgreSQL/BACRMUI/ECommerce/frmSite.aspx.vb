﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Net
Imports Telerik.Web.UI

Partial Public Class frmSite
    Inherits BACRMPage

#Region "Global Declaration"
    Dim lngSiteID As Long
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If GetQueryStringVal("SiteID") <> "" Then
                lngSiteID = GetQueryStringVal("SiteID")
            End If

            Master.SelectedTabValue = "158"

            If Not IsPostBack Then
                loadCurrencyRates()

                If lngSiteID > 0 Then
                    LoadDetails()
                    lblSite.Text = "Edit Site"
                Else
                    lblSite.Text = "New Site"
                End If

            End If
            If lngSiteID > 0 Then
                trGoLive.Visible = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objSite As New Sites
        Try
            'validate host name
            'create Regular Expression Match pattern object
            Dim myRegex As New Regex("^[a-zA-Z0-9]{4,20}$") 'minimum length 4
            'boolean variable to hold the status
            Dim isValid As Boolean = False
            isValid = myRegex.IsMatch(txtHostName.Text.Trim())
            'return the results
            If isValid = False Then
                litMessage.Text = "invalid host name entered! (only alphanumeric characters are allowed with minimum length 4 characters to max 20)"
                Exit Sub
            End If
            isValid = myRegex.IsMatch(txtSiteName.Text.Trim())
            'return the results
            If isValid = False Then
                litMessage.Text = "invalid site name entered! (only alphanumeric characters are allowed with minimum length 4 characters to max 20)"
                Exit Sub
            End If



            objSite.SiteID = lngSiteID
            objSite.SiteName = txtSiteName.Text.Trim()
            objSite.SiteDesc = txtDescription.Text.Trim()
            objSite.HostName = txtHostName.Text.Trim()
            objSite.IsActive = IIf(rbtnActive.Checked, True, False)
            objSite.DomainID = Session("DomainID")
            objSite.UserCntID = Session("UserContactID")
            objSite.CurrencyID = ddlCurrency.SelectedValue
            objSite.IsOnePageCheckout = chkEnableOnePageCheckout.Checked
            objSite.RateType = ddlRateType.SelectedValue
            objSite.ListOfCategories = ""
            objSite.IsSSLRedirectEnabled = chkSSLRedirect.Checked
            objSite.IsHtml5 = chkHtml5.Checked
            objSite.Html5MetaTags = txtMetaTags.Text
            objSite.Mode = 0
            'objSite.RateType =  

            If objSite.ManageSites() Then
                If lngSiteID = 0 Then CreateDefaultData(objSite.SiteID)

                If objSite.SiteID > 0 Then
                    'Create New Site on IIS 7.5 with custom root path for created site id
                    Try


                        Dim serverManager As Microsoft.Web.Administration.ServerManager
                        serverManager = New Microsoft.Web.Administration.ServerManager

                        'Update Site
                        If hdnOldSiteName.Value.Trim.Length > 2 Then
                            If Not serverManager.Sites(hdnOldSiteName.Value.Trim) Is Nothing Then
                                Dim mysite As Microsoft.Web.Administration.Site = serverManager.Sites(hdnOldSiteName.Value.Trim)
                                mysite.Name = txtSiteName.Text.Trim
                                Dim boolBindingFound As Boolean
                                Dim boolLiveBindingFound As Boolean
                                For Each binding As Microsoft.Web.Administration.Binding In mysite.Bindings
                                    If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                                        Response.Write(binding.Host & "<br>")
                                        Response.Write(binding.BindingInformation & "<br>")
                                        Response.Write(binding.Protocol & "<br>")
                                        Response.Write(hdnOldHostName.Value.Trim & lblHostNamePostFix.Text.Trim & "<br>")
                                    End If
                                    If binding.Protocol = "http" And binding.Host.ToLower() = txtHostName.Text.Trim.ToLower & lblHostNamePostFix.Text.Trim.ToLower() Then
                                        binding.BindingInformation = "*:80:" & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim
                                        If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then Response.Write(binding.BindingInformation & "<br>")
                                        boolBindingFound = True
                                    End If
                                Next
                                If Not boolBindingFound Then
                                    mysite.Bindings.Add("*:80:" & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim, "http")
                                    If Not txtHostName.Text.Trim.ToLower.Contains("www.") Then
                                        mysite.Bindings.Add("*:80:www." & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim, "http")
                                    End If
                                End If

                                ChangeWebConfig(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString() & "\web.config", txtHostName.Text)

                                serverManager.CommitChanges()
                                System.Threading.Thread.Sleep(3000)
                                ValidateLiveSite()
                            Else
                                If serverManager.Sites(txtSiteName.Text.Trim) Is Nothing Then ' check if given site doesn't overwrite existing sites in iis
                                    Dim mySite As Microsoft.Web.Administration.Site = serverManager.Sites.Add(txtSiteName.Text.Trim, "http", "*:80:" & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim, ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString())
                                    If Not txtHostName.Text.Trim.ToLower.Contains("www.") Then
                                        mySite.Bindings.Add("*:80:www." & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim, "http")
                                    End If
                                    mySite.ServerAutoStart = True
                                    'Create Virtual directory for usercontrols
                                    mySite.Applications.Add("/UserControls", ConfigurationManager.AppSettings("CartLocation") & "\UserControls")
                                    'PortalDocs is virtual directory which will be used to access documents of portal from BizCart
                                    mySite.Applications.Add("/PortalDocs", ConfigurationManager.AppSettings("PortalLocation") & "\documents\docs")

                                    ChangeWebConfig(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString() & "\web.config", txtHostName.Text)

                                    serverManager.CommitChanges()
                                    System.Threading.Thread.Sleep(3000)
                                    ValidateLiveSite()
                                Else
                                    litMessage.Text = "Site name alredy exists.Please choose different name."
                                End If
                            End If
                        Else 'Create New Site
                            'Create Site with Host header
                            If serverManager.Sites(txtSiteName.Text.Trim) Is Nothing Then ' check if given site doesn't overwrite existing sites in iis
                                Dim mySite As Microsoft.Web.Administration.Site = serverManager.Sites.Add(txtSiteName.Text.Trim, "http", "*:80:" & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim, ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString())
                                If Not txtHostName.Text.Trim.ToLower.Contains("www.") Then
                                    mySite.Bindings.Add("*:80:www." & txtHostName.Text.Trim & lblHostNamePostFix.Text.Trim, "http")
                                End If
                                mySite.ServerAutoStart = True
                                'Create Virtual directory for usercontrols
                                mySite.Applications.Add("/UserControls", ConfigurationManager.AppSettings("CartLocation") & "\UserControls")
                                'PortalDocs is virtual directory which will be used to access documents of portal from BizCart
                                mySite.Applications.Add("/PortalDocs", ConfigurationManager.AppSettings("PortalLocation") & "\documents\docs")

                                ChangeWebConfig(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString() & "\web.config", txtHostName.Text)

                                serverManager.CommitChanges()
                                System.Threading.Thread.Sleep(3000)
                                ValidateLiveSite()
                            Else
                                litMessage.Text = "Site name alredy exists.Please choose different name."
                            End If
                        End If
                        Dim dtSite As DataTable

                        objSite.DomainID = Session("DomainID")
                        dtSite = objSite.GetSites()
                        Session("SitePreviewURL") = dtSite.Rows(0)("vcPreviewURL")

                    Catch ex As Exception
                        If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                            Throw ex
                        Else
                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        End If
                        'do not throw exception , for developement 
                    End Try
                End If

                If ConfigurationManager.AppSettings("IsDebugMode") = "false" Then Response.Redirect("../ECommerce/frmSiteList.aspx", False)
            Else
                litMessage.Text = "Duplicate Site name/Host Name found.Please choose different Site name/Host Name."
                txtSiteName.Focus()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmSiteList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click
        Try
            ValidateLiveSite()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
#End Region

#Region "Methods"
    Private Sub LoadDetails()
        Try
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = lngSiteID
            objSite.DomainID = Session("DomainID")
            dtSite = objSite.GetSites()
            txtSiteName.Text = dtSite.Rows(0)("vcSiteName")
            hdnOldSiteName.Value = txtSiteName.Text
            txtDescription.Text = dtSite.Rows(0)("vcDescription")
            txtHostName.Text = dtSite.Rows(0)("vcHostName")
            hdnOldHostName.Value = dtSite.Rows(0)("vcHostName")
            txtLiveSiteURL.Text = dtSite.Rows(0)("vcLiveURL")
            hdnLiveSiteUrl.Value = dtSite.Rows(0)("vcLiveURL")
            chkEnableOnePageCheckout.Checked = dtSite.Rows(0)("bitOnePageCheckout")
            ddlRateType.SelectedValue = dtSite.Rows(0)("tintRateType")
            If dtSite.Rows(0)("IsActive") = "Inactivate" Then
                rbtnActive.Checked = True
            Else
                rbtnInactive.Checked = True
            End If

            If Not String.IsNullOrEmpty(txtLiveSiteURL.Text) Then
                chkSSLRedirect.Enabled = True
            Else
                chkSSLRedirect.Enabled = False
            End If

            If Not ddlCurrency.Items.FindByValue(dtSite.Rows(0).Item("numCurrencyID")) Is Nothing Then
                ddlCurrency.Items.FindByValue(dtSite.Rows(0).Item("numCurrencyID")).Selected = True
            End If

            chkSSLRedirect.Checked = CCommon.ToBool(dtSite.Rows(0)("bitSSLRedirect"))
            chkHtml5.Checked = CCommon.ToBool(dtSite.Rows(0)("bitHtml5"))
            txtMetaTags.Text = CCommon.ToString(dtSite.Rows(0)("vcMetaTags"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub CreateDefaultData(ByVal SiteID As Long)
        Try
            Dim lngTemplateID As Long
            Dim objSite As New Sites
            objSite.CreateCSS(SiteID, Session("DomainID"))
            objSite.CreateJavascript(SiteID, Session("DomainID"))

            lngTemplateID = objSite.CreateTemplate(SiteID, "Header", "", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "Header.htm")
            lngTemplateID = objSite.CreateTemplate(SiteID, "Footer", "", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "Footer.htm")
            lngTemplateID = objSite.CreateTemplate(SiteID, "LeftPanel", "", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "LeftPanel.htm")

            lngTemplateID = objSite.CreateTemplate(SiteID, "Home", "", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Home", "Welcome to Home", "Home.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Categories", "{#CategoryList#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Categories", "View All Categories", "Categories.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ProductList", "{#ItemList#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ProductList", "Categorywise View Products", "ProductList.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Product", "{#Product#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Product", "View Detail of selcted Product", "Product.aspx", False, Session("DomainID"), Session("UserContactID"), True)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Cart", "{#Cart#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Cart", "View & Update Cart", "Cart.aspx", True, Session("DomainID"), Session("UserContactID"), True)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Login", "{#Login#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Login", "Login", "Login.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "SignUp", "{#SignUp#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "SignUp", "SignUp", "SignUp.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ForgotPassword", "{#ForgotPassword#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ForgotPassword", "Forgot Password?", "ForgotPassword.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ChangePassword", "{#ChangePassword#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ChangePassword", "Change Password", "ChangePassword.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ConfirmAddress", "{#ConfirmAddress#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ConfirmAddress", "Billing and Shipping Address", "ConfirmAddress.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            'lngTemplateID = objSite.CreateTemplate(SiteID, "Checkout", "{#Checkout#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            'objSite.CreatePage(SiteID, lngTemplateID, "Checkout", "Checkout", "Checkout.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            'lngTemplateID = objSite.CreateTemplate(SiteID, "Checkout", "{#Checkout#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            'objSite.CreatePage(SiteID, lngTemplateID, "Checkout", "Checkout", "Checkout.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Contact US", "{#ContactUs#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Contact Us", "Contact Us", "ContactUs.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Orders", "{#SalesOrders#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Orders", "View Past Orders", "Orders.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Invoice", "{#Invoice#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "Invoice.htm")
            objSite.CreatePage(SiteID, lngTemplateID, "Invoice", "Invoice", "Invoice.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Lists", "{#Lists#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Lists", "Lists", "Lists.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "AddToList", "{#AddToList#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "AddToList", "AddToList", "AddToList.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "CustomerAddress", "{#CustomerAddress#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "CustomerAddress", "CustomerAddress", "CustomerAddress.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ShippingOptions", "{#ShippingOptions#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ShippingOptions", "Shipping Options", "ShippingOptions.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "EstimateShipping", "{#EstimateShipping#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "EstimateShipping", "Estimate Shipping", "EstimateShipping.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ProductImage", "{#ItemImage#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "DefaultEmpty.htm")
            objSite.CreatePage(SiteID, lngTemplateID, "ProductImage", "Product Image", "ItemImage.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "FileNotFound", "Page Not Found!!!", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "FileNotFound.htm")
            objSite.CreatePage(SiteID, lngTemplateID, "FileNotFound", "File Not Found", "FileNotFound.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            'lngTemplateID = objSite.CreateTemplate(SiteID, "RelatedItems.aspx", "{#RelatedItems#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "RelatedItems.htm")
            'objSite.CreatePage(SiteID, lngTemplateID, "RelatedItems", "Related Items", "RelatedItems.aspx", False, Session("DomainID"), Session("UserContactID"))

            lngTemplateID = objSite.CreateTemplate(SiteID, "Account", "{#Account#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "MyAccount", "My Account", "Account.aspx", True, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "AddPayment", "{#AddPayment#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "AddPayment", "Credit Card Information", "AddPayment.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "Unsubscribe", "{#Unsubscribe#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "Unsubscribe", "Unsubscribe", "Unsubscribe.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "OrderSummary", "{#OrderSummary#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "OrderSummary", "Order Summary", "OrderSummary.aspx", False, Session("DomainID"), Session("UserContactID"), True)

            lngTemplateID = objSite.CreateTemplate(SiteID, "CustomerInformation", "{#CustomerInformation#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "CustomerInformation", "Customer Information", "CustomerInformation.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "OnePageCheckout", "", txtSiteName.Text, Session("DomainID"), Session("UserContactID"), "Elements\OnePageCheckout.htm")

            lngTemplateID = objSite.CreateTemplate(SiteID, "OnePageCheckoutTemplate", "{#OnePageCheckout#}<br />{template:onepagecheckout}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "OnePageCheckout", "OnePage Checkout", "OnePageCheckout.aspx", False, Session("DomainID"), Session("UserContactID"), True)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ThankYouTemplate", "{#ThankYou#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ThankYou", "ThankYou", "ThankYou.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "WriteReviewTemplate", "{#WriteReview#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "WriteReview", "WriteReview", "WriteReview.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "ReadReviewTemplate", "{#ReviewList#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "ReadReview", "ReadReview", "ReadReview.aspx", False, Session("DomainID"), Session("UserContactID"), False)

            lngTemplateID = objSite.CreateTemplate(SiteID, "PermaLinkTemplate", "{#PermaLink#}", txtSiteName.Text, Session("DomainID"), Session("UserContactID"))
            objSite.CreatePage(SiteID, lngTemplateID, "PermaLink", "PermaLink", "PermaLink.aspx", False, Session("DomainID"), Session("UserContactID"), False)



            'Copy Default Style
            Dim strDirectory As String = ConfigurationManager.AppSettings("CartLocation")
            Dim SourcePath As String = strDirectory & "\Default"
            Dim targetPath As String = strDirectory & "\Pages\" & SiteID.ToString() & "\"
            'remove readonly flag  from file
            File.SetAttributes(SourcePath & "\Style\default.css", FileAttributes.Normal)
            File.SetAttributes(SourcePath & "\Style\PagingLightStyle.css", FileAttributes.Normal)
            File.SetAttributes(SourcePath & "\Style\PagingDarkStyle.css", FileAttributes.Normal)
            File.SetAttributes(SourcePath & "\Style\smart_wizard.css", FileAttributes.Normal)
            File.SetAttributes(SourcePath & "\Style\SliderStyle.css", FileAttributes.Normal)

            'copy files 
            File.Copy(SourcePath & "\Style\default.css", strDirectory & "\Pages\" & SiteID.ToString() & "\default.css", True)
            File.Copy(SourcePath & "\Style\PagingLightStyle.css", strDirectory & "\Pages\" & SiteID.ToString() & "\PagingLightStyle.css", True) ' add pagerstyle
            File.Copy(SourcePath & "\Style\PagingDarkStyle.css", strDirectory & "\Pages\" & SiteID.ToString() & "\PagingDarkStyle.css", True)
            File.Copy(SourcePath & "\Style\smart_wizard.css", strDirectory & "\Pages\" & SiteID.ToString() & "\smart_wizard.css", True)
            File.Copy(SourcePath & "\Style\SliderStyle.css", strDirectory & "\Pages\" & SiteID.ToString() & "\SliderStyle.css", True)


            SourcePath = strDirectory & "\Default\Images"
            targetPath = targetPath & "Images"
            Directory.CreateDirectory(targetPath)
            CopyAllFiles(SourcePath, targetPath)

            SourcePath = strDirectory & "\Js"
            targetPath = strDirectory & "\Pages\" & SiteID.ToString() & "\" & "Js"
            Directory.CreateDirectory(targetPath)
            CopyAllFiles(SourcePath, targetPath)

            'copy web.config to site's root folder
            If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\web.config") Then
                    File.Copy(ConfigurationManager.AppSettings("CartLocation") & "\web.config", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & SiteID.ToString() & "\web.config", True)
                End If
            End If

            If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax") Then
                    File.Copy(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & SiteID.ToString() & "\Global.asax", True)
                End If
            End If

            If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax.cs") Then
                    File.Copy(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax.cs", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & SiteID.ToString() & "\Global.asax.cs", True)
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Shared Sub CopyAllFiles(ByVal SourcePath As String, ByVal targetPath As String)
        'Copy all files under ~/Default Folder to New creted Site Folder
        Dim fileName, destFile As String
        If System.IO.Directory.Exists(SourcePath) Then
            Dim files() As String = System.IO.Directory.GetFiles(SourcePath)
            'Copy the files and overwrite destination files if they already exist.
            For Each s As String In files
                'remove readonly
                File.SetAttributes(s, FileAttributes.Normal)
                ' Use static Path methods to extract only the file name from the path.
                fileName = System.IO.Path.GetFileName(s)
                destFile = System.IO.Path.Combine(targetPath, fileName)

                System.IO.File.Copy(s, destFile, True)

                'TODO:BizCart/Default/*.Css File in vss will be installed as read only , reset it . so deleting site doesn't throw error
                File.SetAttributes(destFile, FileAttributes.Normal)
            Next
        End If
    End Sub
    Private Sub ValidateLiveSite()
        Try
            If txtLiveSiteURL.Text.Trim.Length > 1 Then
                Dim IP As IPAddress() = Dns.GetHostAddresses(txtLiveSiteURL.Text.Trim.ToLower().Replace("http://", "").Replace("https://", ""))
                If IP.Length > 0 Then
                    '173.255.12.194 is ip addres where out web application server runs
                    If IP(0).ToString = "127.0.0.1" Or IP(0).ToString = "173.255.12.194" Then ' On server it returns "127.0.0.1" for biautomation.com instead of "71.189.47.60"

                        'update Live URL
                        Dim objSite As New Sites
                        objSite.SiteID = lngSiteID
                        objSite.LiveURL = txtLiveSiteURL.Text.Trim
                        objSite.Mode = 0
                        objSite.ManageSites()
                        'Add host header
                        Dim serverManager As New Microsoft.Web.Administration.ServerManager

                        'For Each bd As Microsoft.Web.Administration.Binding In serverManager.Sites(txtSiteName.Text.Trim).Bindings
                        '    Response.Write(bd.BindingInformation & "<br>")
                        'Next
                        'Create New Binding
                        Dim b As Microsoft.Web.Administration.Binding = serverManager.Sites(txtSiteName.Text.Trim).Bindings.CreateElement()
                        b.SetAttributeValue("protocol", "http")
                        b.SetAttributeValue("bindingInformation", "*:80:" + txtLiveSiteURL.Text.Trim)
                        serverManager.Sites(txtSiteName.Text.Trim).Bindings.Add(b)

                        If Not txtLiveSiteURL.Text.Trim.ToLower().Contains("www.") Then
                            Dim bwww As Microsoft.Web.Administration.Binding = serverManager.Sites(txtSiteName.Text.Trim).Bindings.CreateElement()
                            bwww.SetAttributeValue("protocol", "http")
                            bwww.SetAttributeValue("bindingInformation", "*:80:www." + txtLiveSiteURL.Text.Trim)
                            serverManager.Sites(txtSiteName.Text.Trim).Bindings.Add(bwww)
                        End If

                        serverManager.CommitChanges()
                        hdnLiveSiteUrl.Value = txtLiveSiteURL.Text.Trim
                        chkSSLRedirect.Enabled = True
                        litMessage.Text = "Validation passed sucessfully, Now you can surf shopping cart using <a target='_blank' href='http://" & txtLiveSiteURL.Text.Trim.Replace("http://", "").Replace("https://", "") & "'>" & txtLiveSiteURL.Text.Trim & "</a>"
                    Else
                        litMessage.Text = "Validation failed, Please modify your website's DNS record and point " & txtLiveSiteURL.Text.Trim & " to IP address 173.255.12.194"
                    End If
                Else
                    litMessage.Text = "Validation failed, Please modify your dns record and point " & txtLiveSiteURL.Text.Trim & " to IP address 173.255.12.194"
                End If
            Else
                litMessage.Text = "Enter Your Website URL ."
            End If

        Catch ex As Exception
            If ex.Message.Contains("No such host is known") Then
                Response.Write("Please enter valid URL")
            Else
                litMessage.Text = "Validation failed, Please modify your website's DNS record and point " & txtLiveSiteURL.Text.Trim & " to IP address 173.255.12.194"
            End If

        End Try
    End Sub
    Private Sub loadCurrencyRates()
        Try
            Dim objCurrency As New CurrencyRates
            objCurrency.DomainID = Session("DomainID")
            objCurrency.GetAll = 0
            ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
            ddlCurrency.DataTextField = "vcCurrencyDesc"
            ddlCurrency.DataValueField = "numCurrencyID"
            ddlCurrency.DataBind()
            ddlCurrency.Items.Insert(0, "--Select One--")
            ddlCurrency.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ChangeWebConfig(ByVal path As String, ByVal localSiteName As String)
        Try
            Using fs As New FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.Read)
                'Dim config As Microsoft.Web.Administration.Configuration = serverManager.GetWebConfiguration(hdnOldSiteName.Value.Trim)
                Dim xmlDoc As System.Xml.XmlDocument = New System.Xml.XmlDocument()
                xmlDoc.Load(fs)

                Dim sb As New System.Text.StringBuilder
                sb.Append("<rewrite>")
                sb.Append("<rules>")
                sb.Append("<rule name=""Redirect to www local site"" stopProcessing=""true"">")
                sb.Append("<match url=""(.*)"" />")
                sb.Append("<conditions trackAllCaptures=""false"">")
                sb.Append("<add input=""{HTTP_HOST}"" pattern=""^" & localSiteName.ToLower.Replace("www.", "") & ".bizautomation.com$"" />")
                sb.Append("</conditions>")
                sb.Append("<action type=""Redirect"" appendQueryString=""true"" url=""{MapProtocol:{HTTPS}}://www." & localSiteName.ToLower.Replace("www.", "") & ".bizautomation.com/{R:1}"" />")
                sb.Append("</rule>")
                
                If Not String.IsNullOrEmpty(hdnLiveSiteUrl.Value) Then
                    sb.Append("<rule name=""Redirect to www live site"" stopProcessing=""true"">")
                    sb.Append("<match url=""(.*)"" />")
                    sb.Append("<conditions trackAllCaptures=""false"">")
                    sb.Append("<add input=""{HTTP_HOST}"" pattern=""^" & hdnLiveSiteUrl.Value.ToLower.Replace("www.", "") & "$"" />")
                    sb.Append("</conditions>")
                    sb.Append("<action type=""Redirect"" appendQueryString=""true"" url=""{MapProtocol:{HTTPS}}://www." & hdnLiveSiteUrl.Value.ToLower.Replace("www.", "") & "/{R:1}"" />")
                    sb.Append("</rule>")
                    If chkSSLRedirect.Checked Then
                        sb.Append("<rule name=""HTTP to HTTPS redirect"" stopProcessing=""true"">")
                        sb.Append("<match url=""(.*)"" />")
                        sb.Append("<conditions>")
                        sb.Append("<add input=""{HTTPS}"" pattern=""off"" ignoreCase=""true"" />")
                        sb.Append("</conditions>")
                        sb.Append("<action type=""Redirect"" appendQueryString=""true"" redirectType=""Found"" url=""https://www." & hdnLiveSiteUrl.Value.ToLower.Replace("www.", "") & "/{R:1}"" />")
                        sb.Append("</rule>")
                    End If
                End If

                sb.Append("</rules>")
                sb.Append("<rewriteMaps>")
                sb.Append("<rewriteMap name=""MapProtocol"">")
                sb.Append("<add key=""on"" value=""https"" />")
                sb.Append("<add key=""off"" value=""http"" />")
                sb.Append("</rewriteMap>")
                sb.Append("</rewriteMaps>")
                sb.Append("</rewrite>")

                Dim xmReWriteContent As String = sb.ToString()
                Dim doc As System.Xml.XmlDocument = New System.Xml.XmlDocument()
                doc.LoadXml(xmReWriteContent)

                Dim newNode As System.Xml.XmlNode = xmlDoc.ImportNode(doc.DocumentElement, True)

                Dim parentNode As System.Xml.XmlNode = xmlDoc.SelectSingleNode("configuration/system.webServer")
                Dim reWriteNode As System.Xml.XmlNode = xmlDoc.SelectSingleNode("configuration/system.webServer/rewrite")

                If Not reWriteNode Is Nothing Then
                    parentNode.RemoveChild(reWriteNode)
                    parentNode.AppendChild(newNode)
                Else
                    parentNode.AppendChild(newNode)
                End If

                fs.SetLength(0)
                xmlDoc.Save(fs)
            End Using
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class