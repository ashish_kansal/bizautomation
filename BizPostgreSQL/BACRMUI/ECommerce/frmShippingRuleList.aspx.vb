﻿Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class frmShippingRuleList
    Inherits BACRMPage

    Dim objShippingRule As ShippingRule

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(13, 41)

            If ChartOfAccounting.GetDefaultAccount("SI", Session("DomainID")) = 0 Then
                ClientScript.RegisterStartupScript(Me.GetType, "alert", "alert('Please set default account for shipping from Administration->Global Settings->Accounting->Default Accounts and try again');", True)
                btnNew.Visible = False
                Exit Sub
            End If
            If Not IsPostBack Then
                BindRelationshipsProfiles()
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub dgShippingRuleList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingRuleList.DeleteCommand
        Try
            objShippingRule = New ShippingRule
            objShippingRule.RuleID = e.Item.Cells(0).Text
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.DelShippingRule()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgShippingRuleList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingRuleList.ItemCommand
        Try
            If e.CommandName = "RuleID" Then
                Response.Redirect("../ECommerce/frmShippingRule.aspx?RuleID=" & e.Item.Cells(0).Text)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgShippingRuleList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShippingRuleList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Methods"
    Sub LoadDetails()
        Try
            If objShippingRule Is Nothing Then
                objShippingRule = New ShippingRule
            End If
            objShippingRule.RuleID = 0
            objShippingRule.byteMode = 1
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.RelationshipProfileId = ddlRelProfile.SelectedValue

            Dim ds As DataSet = objShippingRule.GetShippingRule

            Dim dtGlobalValues As DataTable = ds.Tables(0)

            txtminShippingCost.Text = dtGlobalValues.Rows(0)("minShippingCost").ToString()
            chkbxEnableStaticShip.Checked = CCommon.ToBool(dtGlobalValues.Rows(0)("bitEnableStaticShippingRule"))
            chkbxExcludeClassifications.Checked = CCommon.ToBool(dtGlobalValues.Rows(0)("bitEnableShippingExceptions"))

            dgShippingRuleList.DataSource = ds.Tables(1)
            dgShippingRuleList.DataBind()
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindRelationshipsProfiles()
        Try
            If objShippingRule Is Nothing Then
                objShippingRule = New ShippingRule
            End If

            objShippingRule.DomainID = Session("DomainID")

            ddlRelProfile.DataSource = objShippingRule.GetShippingRelationshipsProfiles
            ddlRelProfile.DataTextField = "vcRelProfile"
            ddlRelProfile.DataValueField = "numRelProfileID"
            ddlRelProfile.DataBind()

            ddlRelProfile.Items.Insert(0, "--All Relationships & Profiles--")
            ddlRelProfile.Items.FindByText("--All Relationships & Profiles--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlRelProfile_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadDetails()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If objShippingRule Is Nothing Then
            objShippingRule = New ShippingRule
        End If

        objShippingRule.DomainID = Session("DomainID")
        objShippingRule.minShippingCost = CCommon.ToDouble(txtminShippingCost.Text)
        objShippingRule.bitEnableStaticShipping = chkbxEnableStaticShip.Checked
        objShippingRule.bitEnableShippingExceptions = chkbxExcludeClassifications.Checked

        objShippingRule.ManageShippingGlobalValues()

        LoadDetails()
    End Sub
#End Region


End Class