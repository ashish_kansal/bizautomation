﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSitePage.aspx.vb" Inherits=".frmSitePage"
    ValidateRequest="false" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/JavaScript">

        function Save() {

            if (document.getElementById('txtPageName').value == "") {
                alert("Enter page Name")
                document.getElementById('txtPageName').focus()
                return false;
            }
            if (document.getElementById('txtPageURL').value == "") {
                alert("Enter the page url")
                document.getElementById('txtPageURL').focus()
                return false;
            }
            if (document.form1.ddlSiteTemplate.selectedIndex == 0) {
                alert("Select a template for the site")
                document.form1.txtPageURL.focus()
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                 <asp:LinkButton ID="btnSaveOnly" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();">&nbsp;&nbsp;Publish</asp:LinkButton>
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();">&nbsp;&nbsp;Publish &amp; Close</asp:LinkButton>
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmSitePage.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Page Name<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:TextBox ID="txtPageName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>File Name<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4 form-group form-inline">
                            <asp:TextBox ID="txtPageURL" runat="server" CssClass="form-control"></asp:TextBox>.aspx
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Select Style</label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:CheckBoxList ID="cblStyle" runat="server" Style="border: solid 1px gray; width: 250px;">
                                <asp:ListItem Text="header" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Site" Value="10"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Select Javascript</label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:CheckBoxList ID="cblJS" runat="server" Style="border: solid 1px gray; width: 250px;">
                                <asp:ListItem Text="header" Value="1"></asp:ListItem>
                                <asp:ListItem Text="footer" Value="2"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Select Template<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:DropDownList ID="ddlSiteTemplate" CssClass="form-control" runat="server">
                                <asp:ListItem Text="temp1" Value="3"></asp:ListItem>
                                <asp:ListItem Text="asdf" Value="5"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Page Type<font color="#ff0000">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:DropDownList ID="ddlPageType" CssClass="form-control" runat="server">
                                <asp:ListItem Text="System" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Custom" Value="2" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Status</label>
                        </div>
                        <div class="col-md-4 form-group">
                             <asp:RadioButtonList ID="rblStatus" runat="server">
                                <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactive" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Style="padding-bottom: 5px;">
                <fieldset class="panel-block" style="width: 93%;">
                    <legend style="white-space: normal;">
                        <span class="panel-heading">Advance Option</span>
                    </legend>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="col-md-3">
                                <label>Page Title (Optional)</label>
                            </div>
                            <div class="col-md-4 form-group form-inline">
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control">
                                </asp:TextBox>
                                <asp:Label ID="lblPageTitle" Text="[?]" CssClass="tip" runat="server" ToolTip="<strong>Page Title</strong><p>Specify a page title, or leave blank to use the product’s name as the page title.</p>" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                <label>Meta Keywords (Optional)</label>
                            </div>
                            <div class="col-md-4 form-group form-inline">
                                <asp:TextBox ID="txtMetaKeywords" class="form-control" runat="server">
                                </asp:TextBox>
                                <asp:Label ID="lblMetaKeywords" Text="[?]" CssClass="tip" runat="server" ToolTip="<strong>Meta Keywords</strong><p>Specify unique meta keywords, or leave blank to use default site wide keywords as defined in the <em>Settings</em> page.</p>" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3">
                                <label>Meta Description (Optional)</label>
                            </div>
                            <div class="col-md-4 form-group form-inline">
                                <asp:TextBox ID="txtMetaTags" runat="server" class="form-control"></asp:TextBox>
                                <asp:Label ID="lblMetaDescription" Text="[?]" CssClass="tip" runat="server" ToolTip="<strong>Meta Description</strong><p>Specify unique meta description, or leave blank to use default site wide description as defined in the <em>Settings</em> page.</p>" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3 form-inline">
                                <label>Maintain Scroll Position on Webpage</label>
                                <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Enabling this feature will make sure your customer stays on same scrolling position in browser after refresing page." />
                            </div>
                            <div class="col-md-4 form-group">
                                <asp:CheckBox ID="cbMaintainScroll" Checked="true" runat="server" />
                            </div>
                        </div>
                    </div>
                   
                </fieldset>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnMetaID" runat="server" Value="0" />
</asp:Content>
