﻿Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Common

Partial Public Class frmBreadCrumbList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If Not IsPostBack Then
                
                BindData()
                hplNew.Attributes.Add("onclick", "NewBreadCrumb();")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindData()
        Try
            If Session("SiteID") > 0 Then
                Dim objSite As New Sites
                Dim dtBreradCrumb As DataTable
                objSite.MenuID = 0
                objSite.SiteID = Session("SiteID")
                objSite.DomainID = Session("DomainID")
                dtBreradCrumb = objSite.GetBreadCrumb()
                dgBreadCrumb.DataSource = dtBreradCrumb
                dgBreadCrumb.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgMenu_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBreadCrumb.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../ECommerce/frmBreadCrumb.aspx?BreadCrumbID=" & e.Item.Cells(0).Text, False)
            If e.CommandName = "Delete" Then
                Dim objSite As New Sites
                objSite.BreadCrumbID = e.Item.Cells(0).Text
                objSite.SiteID = Session("SiteID")
                objSite.DeleteBreadCrumb()
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgMenu_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBreadCrumb.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnSaveOrder_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveOrder.Click
        Try
            Dim objSite As New Sites
            objSite.StrItems = GetItems()
            objSite.UpdateBreadCrumbLevel()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numBreadCrumbID")
            dt.Columns.Add("tintLevel")
            Dim txtbox As New TextBox
            For Each Item As DataGridItem In dgBreadCrumb.Items
                txtbox = CType(Item.FindControl("txtOrder"), TextBox)
                If txtbox.Text.Length > 0 Then
                    If IsNumeric(txtbox.Text) Then
                        Dim dr As DataRow = dt.NewRow
                        dr("numBreadCrumbID") = CType(Item.FindControl("lblBreadCrumbID"), Label).Text
                        dr("tintLevel") = txtbox.Text.Trim()
                        dt.Rows.Add(dr)
                    End If
                End If
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class