﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/ECommerceMenuMaster.Master" CodeBehind="frmFriendlyURLs.aspx.vb" Inherits=".frmFriendlyURLs" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-md-12 pull-right">
            <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    User Friendly URLs&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmFriendlyURLs.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:Panel ID="pnlContent" runat="server">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline pull-left padbottom10">
                    <label>Friendly URL:</label>
                    <asp:TextBox ID="txtFriendlyURL" runat="server" CssClass="form-control" Width="430"></asp:TextBox>
                    &nbsp;&nbsp;
                <label>Biz URL:</label>
                    <asp:TextBox ID="txtBizURL" runat="server" CssClass="form-control" Width="430"></asp:TextBox>
                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add" />
                    <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="If you want to friendly url for pages inside Design - > Pages than you have to follow following example:
If you want www.xyz.com/ContactUs.aspx url should also be accessible as www.xyz.com/contactus than in Friendly URL field type 'contactus' and in Biz URL field type 'ContactUs.aspx'.
                    
If you want to friendly url for categoris than you have to follow following example:
If you have category name as 'T-Shirts' than biz url for it is something like www.xyz.com/Category/test-category/1/3188. Now you want to display it as 'www.xyz.com/T-Shirts' only than first set Friendly URL value of this category to 'T-Shirts' from Categories tab. Now in Biz URL field type 'ProductList.aspx?Category=T-Shirts'" />
                </div>
                <div class="pull-right">
                    <asp:LinkButton ID="lkbDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvURLs" runat="server" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped">
                        <Columns>
                            <asp:BoundField DataField="vcFriendlyURL" HeaderText="Friendly URL" />
                            <asp:BoundField DataField="vcBizURL" HeaderText="Biz URL" />
                            <asp:TemplateField ItemStyle-Width="30">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete" CommandArgument='<%# Eval("vcTag")%>'><i class="fa fa-trash"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
