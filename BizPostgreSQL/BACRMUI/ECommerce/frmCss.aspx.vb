﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO

Partial Public Class frmCss
    Inherits BACRMPage
    Dim lngCssId As Long
    Dim intType As Integer

    ' Type = 0 means css for site
    ' Type = 1 means js for site
    ' Type = 2 means css for portal (pass null as site id), change location of saved css file
    ' Type = 3 means css for Survey
    ' Type = 3 means js for Survey

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
        If GetQueryStringVal("CssID") <> "" Then
            lngCssId = CCommon.ToLong(GetQueryStringVal("CssID"))
        End If
        If GetQueryStringVal("Type") <> "" Then
            intType = CCommon.ToInteger(GetQueryStringVal("Type"))
            If Not ClientScript.IsStartupScriptRegistered("CSSEditor") Then
                ClientScript.RegisterStartupScript(Me.GetType, "CSSEditor", "editAreaLoader.init({ id: 'txtEditCss', syntax: '" & IIf(intType = 1, "js", "css") & "', start_highlight: true , toolbar: 'search, go_to_line, undo, redo, select_font'});", True)
            End If
        End If
        If Not IsPostBack Then
            ScriptManager.GetCurrent(Me).RegisterPostBackControl(Me.btnSave)
            If lngCssId > 0 Then
                LoadDetails()
            End If
            If intType = 0 Or intType = 2 Or intType = 3 Then
                lblName.Text = "Style"
                lblName1.Text = "Style"
                lblName2.Text = "Style"
                Master.SelectedTabValue = "159"
            ElseIf intType = 1 Or intType = 4 Then
                lblName.Text = "JavaScript"
                lblName1.Text = "JavaScript"
                lblName2.Text = "JavaScript"
                Master.SelectedTabValue = "160"
            End If
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lngCssId > 0 Then
                Dim objSite As New Sites

                If intType = 3 Or intType = 4 Then
                    objSite.WriteToFile(txtEditCss.Text, ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey\" & hdnCssFileName.Value)
                Else
                    objSite.WriteToFile(txtEditCss.Text, IIf(intType = 2, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "PortalCss", Sites.GetUploadPath()) & "\" & hdnCssFileName.Value)
                End If

                objSite.SiteID = IIf(intType = 2 Or intType = 3 Or intType = 4, 0, Session("SiteID"))

                objSite.CssID = lngCssId
                objSite.StyleName = txtStyleName.Text.Trim()
                objSite.DomainID = Session("DomainID")
                objSite.StyleType = intType
                objSite.ManageStyles()

                If intType = 3 Or intType = 4 Then
                    Response.Redirect("../Marketing/frmSurveyCssList.aspx?Type=" & intType.ToString(), False)
                Else
                    Response.Redirect("../ECommerce/frmCssList.aspx?Type=" & intType.ToString(), False)
                End If
            Else
                If uploadCss() Then
                    Dim objSite As New Sites
                    objSite.SiteID = IIf(intType = 2 Or intType = 3 Or intType = 4, 0, Session("SiteID"))
                    objSite.CssID = lngCssId
                    objSite.StyleFile = fuCss.FileName
                    objSite.StyleName = txtStyleName.Text.Trim()
                    objSite.DomainID = Session("DomainID")
                    objSite.StyleType = intType

                    If objSite.ManageStyles() > 0 Then
                        If intType = 3 Or intType = 4 Then
                            Response.Redirect("../Marketing/frmSurveyCssList.aspx?Type=" & intType.ToString(), False)
                        Else
                            Response.Redirect("../ECommerce/frmCssList.aspx?Type=" & intType.ToString(), False)
                        End If
                    Else
                        litMessage.Text = lblName1.Text + " name already chosen for this site."
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub


    Function uploadCss() As Boolean
        Try
            Select Case intType
                Case 0
                    If fuCss.HasFile Then
                        If fuCss.FileName.ToLower.IndexOf(".css") <> -1 Then
                            UploadFile()
                            Return True
                        Else
                            litMessage.Text = "only *.css files are allowed to upload"
                            Return False
                        End If
                    Else
                        litMessage.Text = "Please select a css file upload"
                        Return False
                    End If
                Case 1, 4
                    If fuCss.HasFile Then
                        If fuCss.FileName.ToLower.IndexOf(".js") <> -1 Then
                            UploadFile()
                            Return True
                        Else
                            litMessage.Text = "only *.js files are allowed to upload"
                            Return False
                        End If
                    Else
                        litMessage.Text = "Please select a js file upload"
                        Return False
                    End If
                Case Else
                    If fuCss.HasFile Then
                        If fuCss.FileName.ToLower.IndexOf(".css") <> -1 Then
                            UploadFile()
                            Return True
                        Else
                            litMessage.Text = "only *.css files are allowed to upload"
                            Return False
                        End If
                    Else
                        litMessage.Text = "Please select a css file upload"
                        Return False
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Sub UploadFile()
        Try
            Dim strUploadDirectory As String = String.Empty
            If intType = 2 Then
                strUploadDirectory = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "PortalCss"
            ElseIf intType = 3 Or intType = 4 Then
                strUploadDirectory = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey"
            ElseIf intType = 1 Then
                strUploadDirectory = Sites.GetUploadPath() & "\Js"
            Else
                strUploadDirectory = Sites.GetUploadPath()
            End If

            If Directory.Exists(strUploadDirectory) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(strUploadDirectory)
            End If

            strUploadDirectory = strUploadDirectory & "\" & fuCss.PostedFile.FileName
            If Not fuCss.PostedFile Is Nothing Then fuCss.PostedFile.SaveAs(strUploadDirectory)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If intType = 3 Or intType = 4 Then
                Response.Redirect("../Marketing/frmSurveyCssList.aspx?Type=" & intType.ToString(), False)
            Else
                Response.Redirect("../ECommerce/frmCssList.aspx?Type=" & intType.ToString(), False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objSite As New Sites
            Dim dtStyle As DataTable
            Dim strUploadFile As String
            objSite.CssID = lngCssId
            objSite.SiteID = Session("SiteID")
            objSite.DomainID = Session("DomainID")
            objSite.StyleType = intType
            dtStyle = objSite.GetStyles()
            txtStyleName.Text = dtStyle.Rows(0)("StyleName")
            hdnCssFileName.Value = dtStyle.Rows(0)("StyleFileName")
            lblCssFileName.Text = "Editing File: <b>" & dtStyle.Rows(0)("StyleFileName") & "</b>"
            trEditCss.Visible = True
            trFileUpload.Visible = False

            If intType = 2 Then
                strUploadFile = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "PortalCss" & "\" & dtStyle.Rows(0)("StyleFileName")
            ElseIf intType = 3 Or intType = 4 Then
                strUploadFile = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey\" & dtStyle.Rows(0)("StyleFileName")
            ElseIf intType = 1 Then
                strUploadFile = Sites.GetUploadPath() & "\Js\" & dtStyle.Rows(0)("StyleFileName")
            Else
                strUploadFile = Sites.GetUploadPath() & "\" & dtStyle.Rows(0)("StyleFileName")
            End If

            Try
                txtEditCss.Text = objSite.ReadFile(strUploadFile)
                ''Compress CSS
                'If txtEditCss.Text.Length > 10 Then
                '    txtEditCss.Text = Sites.CompressCSS(txtEditCss.Text)
                'End If
            Catch ex As Exception
                litMessage.Text = "File <b>" & dtStyle.Rows(0)("StyleFileName") & "</b> Does not exist"
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveOnly_Click(sender As Object, e As EventArgs) Handles btnSaveOnly.Click
        Try
            If lngCssId > 0 Then
                Dim objSite As New Sites

                If intType = 3 Or intType = 4 Then
                    objSite.WriteToFile(txtEditCss.Text, ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & Session("DomainID") & "\Survey\" & hdnCssFileName.Value)
                ElseIf intType = 1 Then
                    objSite.WriteToFile(txtEditCss.Text, Sites.GetUploadPath() & "\Js\" & hdnCssFileName.Value)
                Else
                    objSite.WriteToFile(txtEditCss.Text, IIf(intType = 2, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "PortalCss", Sites.GetUploadPath()) & "\" & hdnCssFileName.Value)
                End If

                objSite.SiteID = IIf(intType = 2 Or intType = 3 Or intType = 4, 0, Session("SiteID"))

                objSite.CssID = lngCssId
                objSite.StyleName = txtStyleName.Text.Trim()
                objSite.DomainID = Session("DomainID")
                objSite.StyleType = intType
                objSite.ManageStyles()
            Else
                If uploadCss() Then
                    Dim objSite As New Sites
                    objSite.SiteID = IIf(intType = 2 Or intType = 3 Or intType = 4, 0, Session("SiteID"))
                    objSite.CssID = lngCssId
                    objSite.StyleFile = fuCss.FileName
                    objSite.StyleName = txtStyleName.Text.Trim()
                    objSite.DomainID = Session("DomainID")
                    objSite.StyleType = intType

                    If objSite.ManageStyles() > 0 Then

                    Else
                        litMessage.Text = lblName1.Text + " name already chosen for this site."
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class