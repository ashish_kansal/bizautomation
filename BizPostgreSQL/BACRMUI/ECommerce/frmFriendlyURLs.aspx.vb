﻿Imports System.Xml
Imports BACRM.BusinessLogic.Common

Public Class frmFriendlyURLs
    Inherits System.Web.UI.Page
#Region "Page Methods"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack Then
                SiteSwitch1.FindControl("ibtnPublish").Visible = False

                If Session("SiteID") = 0 Then
                    pnlContent.Visible = False
                Else
                    BindData()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindData()
        Try
            If Session("SiteID") > 0 Then

                gvURLs.DataSource = Nothing
                gvURLs.DataBind()

                'Read web.config file
                Dim path As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\web.config"
                Dim doc As New XmlDocument()
                doc.Load(path)
                Dim list As XmlNodeList = doc.DocumentElement.SelectNodes("rewriter/rewrite")

                Dim dt As New DataTable()
                dt.Columns.Add("vcTag")
                dt.Columns.Add("vcFriendlyURL")
                dt.Columns.Add("vcBizURL")

                Dim dr As DataRow
                For Each node As XmlNode In list


                    If node.OuterXml <> "<rewrite url=""~/product/(.+)/(.+)/(.+)"" to=""~/Product.aspx?ItemID=$3"" />" AndAlso
                        node.OuterXml <> "<rewrite url=""~/Category/(.+)/(.+)/(.+)/(.+)"" to=""~/ProductList.aspx?Cat=$3&amp;Page=$2&amp;CatName=$1&amp;filter=$4"" />" AndAlso
                        node.OuterXml <> "<rewrite url=""~/Category/(.+)/(.+)/(.+)"" to=""~/ProductList.aspx?Cat=$3&amp;Page=$2&amp;CatName=$1"" />" AndAlso
                        node.OuterXml <> "<rewrite url=""~/Search/(.+)/(.+)"" to=""~/ProductList.aspx?Cat=0&amp;Page=$1&amp;ST=$2&amp;CatName=$1"" />" AndAlso
                        node.OuterXml <> "<rewrite url=""~/SubCategory/(.+)/(.+)"" to=""~/Categories.aspx?Cat=$2&amp;CatName=$1"" />" AndAlso
                        node.OuterXml <> "<rewrite url=""^(/.+(\.gif|\.png|\.jpg|\.ico|\.pdf|\.css|\.js)(\?.+)?)$"" to=""$1"" processing=""stop"" />" AndAlso
                        node.OuterXml <> "<rewrite url=""~/AdvancedSearch/(.+)"" to=""~/ProductList.aspx?Cat=0&amp;Page=$1"" />" Then
                        dr = dt.NewRow()
                        dr("vcTag") = Convert.ToString(node.OuterXml)
                        dr("vcFriendlyURL") = Convert.ToString(node.Attributes("url").Value).Replace("~/", "").Replace("$", "")
                        dr("vcBizURL") = Convert.ToString(node.Attributes("to").Value).Replace("~/", "")
                        dt.Rows.Add(dr)
                    End If
                Next

                gvURLs.DataSource = dt
                gvURLs.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        Catch exception As Exception

        End Try
    End Sub

#End Region


#Region "Event Handlers"

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            If CCommon.ToLong(Session("SiteID")) > 0 AndAlso Not String.IsNullOrEmpty(txtFriendlyURL.Text.Trim()) AndAlso Not String.IsNullOrEmpty(txtBizURL.Text.Trim()) Then
                Dim path As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\web.config"
                Dim doc As New XmlDocument()
                doc.Load(path)
                Dim list As XmlNodeList = doc.DocumentElement.SelectNodes("rewriter")

                Dim node As XmlNode = doc.CreateNode(XmlNodeType.Element, "rewrite", Nothing)
                Dim attribute As XmlAttribute = doc.CreateAttribute("url")
                attribute.Value = "~/" & txtFriendlyURL.Text.Trim() & "$"
                node.Attributes.Append(attribute)

                attribute = doc.CreateAttribute("to")
                attribute.Value = "~/" & txtBizURL.Text.Trim()
                node.Attributes.Append(attribute)

                doc.DocumentElement.SelectNodes("rewriter")(0).AppendChild(node)
                doc.Save(path)

                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

    Private Sub gvURLs_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvURLs.RowCommand
        Try
            If e.CommandName = "Delete" Then
                Dim path As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Session("SiteID") & "\web.config"
                Dim doc As New XmlDocument()
                doc.Load(path)
                Dim list As XmlNodeList = doc.DocumentElement.SelectNodes("rewriter/rewrite")

                For Each node As XmlNode In list
                    If node.OuterXml = e.CommandArgument Then
                        doc.DocumentElement.SelectNodes("rewriter")(0).RemoveChild(node)

                        Exit For
                    End If
                Next

                doc.Save(path)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvURLs_RowDeleted(sender As Object, e As GridViewDeletedEventArgs) Handles gvURLs.RowDeleted
        
    End Sub

    Private Sub gvURLs_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvURLs.RowDeleting
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class