﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContentManagement.aspx.vb" 
    Inherits=".frmContentManagement" MasterPageFile="~/common/ECommerceMenuMaster.Master" ValidateRequest="false" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
<%--    <script src="../JavaScript/ckEditor/ckeditor.js"></script>--%>
    <script src="https://cdn.ckeditor.com/4.7.3/full/ckeditor.js"></script>
    <link href="../JavaScript/ckEditor/toolbarconfigurator/lib/codemirror/neo.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            loadAllContentList();
            debugger;
            if ($("#hdnUrl").val() != "") {
                $("#search-url-textbox").val($("#hdnUrl").val());
            }
            CKEDITOR.replace('editor1', {
                height: 300,
                filebrowserImageUploadUrl: 'ckEditorImageUpload.ashx?DomainId=<%=Session("DomainID")%>',

            });
            
            //setTimeout(function () {
            //    CKEDITOR.instances['editor1'].setData("<p></p>");
            //}, 3000);
            setTimeout(function () {
                var ckdata = CKEDITOR.instances['editor1'].getData();
                console.log(ckdata);
            }, 10000);

        })
        function deleteContent() {
            var ckdata = CKEDITOR.instances['editor1'].getData();
            console.log(ckdata);
            $("#hdnUrl").val($("#search-url-textbox").val())
            // $("#hdnContentId").val($("#search-url-textbox").text())
            $("#hdnContent").val(ckdata)
            $("#btnDelete").click();
        }
        function saveContent() {
            var ckdata = CKEDITOR.instances['editor1'].getData();
            console.log(ckdata);
            $("#hdnUrl").val($("#search-url-textbox").val())
           // $("#hdnContentId").val($("#search-url-textbox").text())
            $("#hdnContent").val(ckdata)
            $("#btnSubmit").click();
        }
        function loadContent(contentId) {
            var DomainID = <%= Session("DomainId")%>;
            $("#<%=hdnContentId.ClientID%>").val(contentId);
            var dataParam = "{DomainID:'" + DomainID + "',SiteID:'" + $("#ddlSites").val() + "',ContentID:'" + contentId+"'}";
            $.ajax({
                type: "POST",
                url: "../Ecommerce/frmContentManagement.aspx/WebMethodContentDetails",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse.length > 0) {
                        console.log(Jresponse[0]);
                        $("#txtTitle").val(Jresponse[0].vcpagetitle);
                        $("#ddlCategory").val(Jresponse[0].numcategoryid);
                        $("#txtDescription").val(Jresponse[0].vcmetadesc);
                        CKEDITOR.instances['editor1'].setData(Jresponse[0].vccontent);
                    }
                }
            });
        }
        function loadAllContentList() {
            var DomainID = <%= Session("DomainId")%>;
            var Type = <%= GetQueryStringVal("type")%>;
            var dataParam = "{DomainID:'" + DomainID + "',SiteID:'" + $("#ddlSites").val() + "',Type:'" + Type + "'}";
            $.ajax({
                type: "POST",
                url: "../Ecommerce/frmContentManagement.aspx/WebMethodContentList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    debugger;
                    //numcontentid
                    //vcurl
                    var Jresponse = $.parseJSON(response.d);
                    var dataArr = [];
                    $(Jresponse).each(function (index, values) {
                        dataArr.push({ value: values.numcontentid, label: values.vcurl });
                    })
                    $("input#search-url-textbox").autocomplete({
                       // source: ["c++", "java", "php", "coldfusion", "javascript", "asp", "ruby"]
                        source: dataArr,
                        select: function (event, ui) {
                            event.preventDefault();
                            $("input#search-url-textbox").val(ui.item.label);
                            loadContent(ui.item.value);
                        }
                    });
                },
                failure: function (response) {
                    Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                  
                }, complete: function () {
                }
            });
        }
        function clearContent() {
            location.reload();
        }
    </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Content Management
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:HiddenField ID="hdnUrl" runat="server" />
    <asp:HiddenField ID="hdnContentId" Value="0" runat="server" />
    <asp:HiddenField ID="hdnContent" runat="server" />
    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Submit" style="display:none" />
    <asp:Button ID="btnDelete" OnClick="btnDelete_Click" class="btn btn-danger" runat="server" Text="Delete" style="display:none"/>
    <div class="col-md-6">
            <label>Page URL</label>
        
    <input id="search-url-textbox" width="100%" class="topic-picker ui-autocomplete-input form-control" type="text" name="q" acceskey="b" autocomplete="off" placeholder="Search Page Url" role="textbox" aria-autocomplete="list" aria-haspopup="true">
</div>     <div class="col-md-6"><div style="padding-top:25px">
         <input type="button" class="btn btn-primary" value="Save" onclick="saveContent()" />
         <input type="button" class="btn btn-danger" value="Clear" onclick="clearContent()" />
         <input type="button" class="btn btn-danger" value="Delete" onclick="deleteContent()" />
</div>
         </div>
    <div class="clearfix"></div>
      <div class="col-md-6">
            <label>Title</label>
          <asp:TextBox ID="txtTitle" class="form-control" runat="server"></asp:TextBox>
        
</div>
    <div class="clearfix"></div>
     <div class="col-md-6">
            <label>Meta Description</label>
         <asp:TextBox ID="txtDescription" TextMode="MultiLine"  class="form-control" Rows="5" runat="server"></asp:TextBox>
        
</div>
    <div class="clearfix"></div>
       <div class="col-md-6">
            <label>Blog Category</label>
           <asp:DropDownList ID="ddlCategory" runat="server" class="form-control" AutoPostBack="false"></asp:DropDownList>
        
</div>
    <div class="clearfix"></div>
    <br />
    <div class="col-md-12">
 <textarea name="editor1" id="editor1">&lt;p&gt;&lt;/p&gt;</textarea>
        </div>
</asp:Content>