﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTemplateList.aspx.vb"
    Inherits=".frmTemplateList" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function NewTemplate() {
            if (document.getElementById('ddlSites').selectedIndex == 0) {
                var hplNew = document.getElementById("hplNew");
                hplNew.href = "#";
                alert("Please select a site to upload " + hplNew.innerHTML);
                document.getElementById('ddlSites').focus();
                return false;
            }
            return true;
        }
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12" id="Table1" runat="server">
            <div class="pull-right">
                 <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
                         &nbsp;
                        <asp:HyperLink ID="hplNew" runat="server" CssClass="hyperlink" NavigateUrl="~/ECommerce/frmTemplate.aspx">New Template</asp:HyperLink>&nbsp;
                         &nbsp;
                <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Templates&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmTemplateList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
&nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
            <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgTemplates" AllowSorting="false" runat="server" Width="100%" CssClass="table table-striped table-bordered"
                    AutoGenerateColumns="False" UseAccessibleHeader="true" >
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numTemplateID" HeaderText="numTemplateID">
                        </asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcTemplateName" SortExpression="" HeaderText="<font>Template Name</font>"
                            CommandName="Edit"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcTemplateCode" SortExpression="" HeaderText="<font>Template Code</font>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
