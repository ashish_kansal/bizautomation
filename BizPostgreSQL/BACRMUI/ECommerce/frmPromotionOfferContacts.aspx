﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPromotionOfferContacts.aspx.vb"
    Inherits=".frmPromotionOfferContacts" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Promotion & Offer Contacts
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Table ID="Table3" Width="800px" runat="server" Height="350" GridLines="None"
        CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Panel runat="server" ID="pnlOrganizations" Visible="false">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="normal1" nowrap>Select Organization
                                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                ClientIDMode="Static"
                                                ShowMoreResultsBox="true"
                                                Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                            </telerik:RadComboBox>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnAddOrg" CssClass="button" runat="server" Text="Add"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnRemoveOrg" CssClass="button" runat="server" Text="Remove from Promotion/Offer"
                                    OnClientClick="return DeleteRecord();"></asp:Button>
                            </td>
                            <td>
                                <asp:Button ID="btnCloseOrg" CssClass="button" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgOrg" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProContact"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcCompanyName" HeaderText="Organization Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="PrimaryContact" HeaderText="Primary Contact (First / Last Name & Email)"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Relationship" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkOrg" runat="server" onclick="SelectAll('chkOrg','chk1');" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chk1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlRelProfiles" Visible="false">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="normal1" nowrap>Select Relationship & Profile
                                        </td>
                                        <td class="normal1" nowrap>
                                            <asp:DropDownList ID="ddlRelationship" runat="server" Width="200" CssClass="signup">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="normal1" nowrap>
                                            <asp:DropDownList ID="ddlProfile" runat="server" Width="200" CssClass="signup">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnAddRelProfile" CssClass="button" runat="server" Text="Add"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnRemoveRelProfile" CssClass="button" runat="server" Text="Remove from Promotion/Offer"
                                                OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCloseRel" CssClass="button" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgRelationship" AllowSorting="True" runat="server" Width="100%"
                                    CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numProContact"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RelProfile" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkRelation" runat="server" ClientIDMode="Static" onclick="SelectAll('chkRelation','chk2');" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chk2" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
