﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Microsoft.Web.Administration
Partial Public Class frmSiteList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            
            GetUserRightsForPage(13, 41)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If Not IsPostBack Then
                
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindData()
        Try
            Dim objSite As New Sites
            Dim dtSites As DataTable
            objSite.DomainID = Session("DomainID")
            dtSites = objSite.GetSites()
            dgSites.DataSource = dtSites
            dgSites.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgSites_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSites.ItemCommand
        Try
            If e.CommandName = "Edit" Then Response.Redirect("../ECommerce/frmSite.aspx?SiteID=" & e.Item.Cells(0).Text, False)
            Dim objSite As New Sites
            If e.CommandName = "Delete" Then
                objSite.SiteID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                objSite.DeleteSite()
                Try
                    'Delete website from IIS 7.5
                    Dim serverManager = New Microsoft.Web.Administration.ServerManager
                    Dim strSiteName As String = CType(e.Item.FindControl("lblSiteName"), Label).Text.Trim
                    If Not serverManager.Sites(strSiteName) Is Nothing Then
                        Dim OldSite As Microsoft.Web.Administration.Site = serverManager.Sites(strSiteName)
                        serverManager.Sites.Remove(OldSite)
                        serverManager.CommitChanges()
                    End If
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    'Throw ex
                End Try

                'Delete files and folder of site
                Dim SiteDirectory As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & objSite.SiteID.ToString()
                If System.IO.Directory.Exists(SiteDirectory) Then
                    Dim d As New System.IO.DirectoryInfo(SiteDirectory)
                    d.Attributes = IO.FileAttributes.Normal

                    System.IO.Directory.Delete(SiteDirectory, True)

                End If
                'Delete Index Files
                SiteDirectory = CCommon.GetLuceneIndexPath(objSite.DomainID, objSite.SiteID)
                If System.IO.Directory.Exists(SiteDirectory) Then
                    Dim d As New System.IO.DirectoryInfo(SiteDirectory)
                    d.Attributes = IO.FileAttributes.Normal
                    System.IO.Directory.Delete(SiteDirectory, True)
                End If

            ElseIf e.CommandName = "Activate" Then
                objSite.SiteID = e.Item.Cells(0).Text
                objSite.DomainID = Session("DomainID")
                Dim lbtnActivte As LinkButton
                lbtnActivte = e.Item.FindControl("lbtnActivate")
                objSite.IsActive = IIf(lbtnActivte.Text = "Activate", True, False)
                objSite.UpdateSiteStatus()
            End If

            BindData()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgSites_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSites.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

End Class