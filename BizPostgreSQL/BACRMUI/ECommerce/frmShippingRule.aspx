﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingRule.aspx.vb"
    Inherits=".frmShippingRule" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script src="../Scripts/tooltip/jquery.tooltip.js"></script>
    <link href="../Scripts/tooltip/tooltip.css" rel="stylesheet" />
    <link href="../Scripts/tooltip/tooltip-white.css" rel="stylesheet" />
    <title>Shipping Rule</title>
    <style>
        .tip {
            background: #ffc!important;
    padding: 1px 7px !important;
    border: 1px solid #8a8181 !important;
    color: #000 !important;
    font-weight: 600 !important;
    z-index: 10000;
        }
        .innertooltipTitle .headingsmallfont1 {
   /* text-decoration: underline; */
   text-decoration: underline;
   font-weight: bold;
}
.innertooltipTitle .subheading {
  text-decoration: underline;
}
    </style>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        $(document).ready(function () {
            $('.tip').tooltip({
                position: 'l', forcePosition: true,

            });
        })
        function Save() {

            if (document.getElementById('txtRuleName').value == "") {
                alert("Enter Rule name");
                document.getElementById('txtRuleName').focus();
                return false;
            }

            var combobox = $find("<%=radcmbWarehouse.ClientID %>");
            var checkedItems = combobox.get_checkedItems();

            if (checkedItems.length > 0) { return true; }
            else {
                alert("Please select a Warehouse.")
                return false;
            }

            if (document.getElementById('hdnShipRuleID').value == "") {

                if ((document.getElementById('txtFreeShipping').value != "") && (document.getElementById('chkbxFreeShipping').checked == false)) {
                    alert("Please check 'Free Shipping checkbox'")
                    return false;
                }

                if ((document.getElementById('txtFreeShipping').value == "") && (document.getElementById('chkbxFreeShipping').checked == true)) {
                    alert("Please enter 'Free Shipping amount'")
                    return false;
                }
            }



        }

        function CloneValidation() {
            if (document.getElementById('ddlCloneRelationship').value == "0") {
                alert("Please select Relationship.");
                document.getElementById('ddlCloneRelationship').focus();
                return false;
            }
            if (document.getElementById('ddlCloneProfile').value == "0") {
                alert("Please select Profile.");
                document.getElementById('ddlCloneProfile').focus();
                return false;
            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function AddCustom() {

            //if (document.getElementById('txtShippingCaption').value == "") {
            //    alert("Enter Shipping Caption");
            //    document.getElementById('txtShippingCaption').focus();
            //    return false;
            //}
            if (document.getElementById('txtFrom').value == "") {
                alert("Enter From Value");
                document.getElementById('txtFrom').focus();
                return false;
            }
            if (document.getElementById('txtTo').value == "") {
                alert("Enter To Value");
                document.getElementById('txtTo').focus();
                return false;
            }
            if (document.getElementById('txtShippingRate').value == "") {
                alert("Enter Shipping Rate");
                document.getElementById('txtShippingRate').focus();
                return false;
            }
            if (parseInt($("#txtFrom").val()) >= parseInt($("#txtTo").val())) {
                alert("From Value must be less than To Value");
                document.getElementById('txtFrom').focus();
                return false;
            }

            return true;
        }

        function AddState() {
            if (document.getElementById('ddlCountry').value == "0") {
                alert("Select a Country");
                document.getElementById('ddlCountry').focus();
                return false;
            }
            // if (document.getElementById('ddlState').value == "0") {
            //     alert("Select a State");
            //     document.getElementById('ddlState').focus();
            //     return false;
            //  }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <div class="form-inline">
            <table>
                <tr>
                    <td runat="server" id="tdClone">
                        <asp:Label ID="lblRuleCloneRel" Text="Clone this rule for Relationship" runat="server"></asp:Label>
                        &nbsp;
                    <asp:DropDownList ID="ddlCloneRelationship" runat="server" Width="200" CssClass="form-control">
                    </asp:DropDownList>
                        &nbsp;
                     <asp:Label ID="lblRuleCloneProfile" Text="Profile" runat="server"></asp:Label>
                        &nbsp;
                    <asp:DropDownList ID="ddlCloneProfile" runat="server" Width="200" CssClass="form-control">
                    </asp:DropDownList>
                        &nbsp;
                     <asp:LinkButton ID="btnCloneRule" runat="server" CssClass="button btn btn-primary" OnClientClick="return CloneValidation()" OnClick="btnCloneRule_Click">Create Clone</asp:LinkButton>
                        &nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="button btn btn-primary" OnClientClick="return Save()"><i class="fa fa-floppy-o"></i>&nbsp;Save</asp:LinkButton>
                        &nbsp;
                    <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="button btn btn-primary"
                        OnClientClick="return Save()"><i class="fa fa-floppy-o"></i>&nbsp;Save & Close</asp:LinkButton>
                        &nbsp;
                    <asp:LinkButton ID="btnClose" runat="server" CssClass="button btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;Close</asp:LinkButton>
                        &nbsp; <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>

            </table>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12">
        <center>
            <span style="color:red">
             <asp:Literal ID="litMessage" runat="server" ></asp:Literal>
            </span>
                <asp:HiddenField ID="hdnShipRuleID" runat="server"></asp:HiddenField>
        </center>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Shipping Rule
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div>
        <div class="col-md-6" style="display: none;">
            <div class="form-group">
                <label>Site</label>
                <asp:Label ID="lblSite" Text="" runat="server"></asp:Label>
            </div>
        </div>
        <div class="clearfix"></div>

        <table style="width: 100%">
            <tr>
                <td style="width: 60%; vertical-align: top;">
                    <table style="width: 100%; margin-top: 15px;">
                        <tr>
                            <td>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Shipping Rule Name</label>
                                        <asp:TextBox ID="txtRuleName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label>Apply Rule</label>
                     <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="col-md-3">
                                    <asp:Label ID="Label103" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'><h5>Assume the following:</h5>(1) You have an item group (found in item details) called item-group-1.<br/>(2) Both item-1 and item-2 belong to item-group-1. <br/>(3) Each of these items has a unit price of $4.00 (after it’s added to the internal<br/>sales order or Biz-Commerce shopping cart)<br/>(4) You have 1 shipping rate for group-1 which says that when amounts range from<br/>$1.00 to $20.00 for items in this group the rate should be $9.95.<br/>(5) You add qty 1 of item-1 and item-2 to the cart.<br/><br/>If shipping is based on “Apply rates on order sub-total” then shipping will be $9.95<br/>If shipping is based on “Apply rates at the line item level” then shipping will be <br/>$19.90 (If this option is used, you can display the shipping rate within the item on<br/>the shopping cart, both within the list and detail view).</div>" />
                                    &nbsp;
                                    <div class="form-group" style="float:right;margin-right:26px;">
                                        <asp:RadioButtonList ID="rblCommissionType" runat="server">
                                            <asp:ListItem Text="Apply rates on order sub-total" Value="0">
                                            </asp:ListItem>
                                            <asp:ListItem Text="Apply rates at the line item level" Value="1">
                                            </asp:ListItem>                                            
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr runat="server" id="trShipping">
                            <td>
                                <div class="box box-primary" style="width: 95%">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Shipping Rates</h3>
                                    </div>
                                    <div class="box-body">

                                        <%-- <asp:DropDownList ID="ddlShippingBasedOn" CssClass="form-control" AutoPostBack="true" runat="server">
                            <asp:ListItem Text="Flat rate per item" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Ship by order weight" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Ship by order total" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Flat rate per order" Value="4"></asp:ListItem>
                        </asp:DropDownList>--%>
                                        <asp:Panel ID="pnlCustomShippingMethod" runat="server">
                                            <div class="col-md-12">
                                                <div class="form-inline">
                                                    <%--<div class="form-group">
                                        <label>Shipping Caption</label>
                                        <asp:TextBox ID="txtShippingCaption" runat="server" Width="200" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    </div>--%>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="pnlCustomMethod" CssClass="form-group" runat="server">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="vertical-align: top;">
                                                                                <label>When sales order amount ranges from</label><br />
                                                                                <label><i>(Not including shipping or tax charges)</i></label>
                                                                            </td>
                                                                            <td style="vertical-align: top;">&nbsp;<asp:TextBox ID="txtFrom" runat="server" Width="60px" CssClass="form-control" onkeypress="CheckNumber(2,event)"></asp:TextBox>
                                                                            </td>
                                                                            <td style="vertical-align: top;">&nbsp;
                                                                                <label>To</label>
                                                                                &nbsp;
                                                                                <asp:TextBox ID="txtTo" runat="server" Width="60px" CssClass="form-control" onkeypress="CheckNumber(2,event)"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                            <td style="vertical-align: top;">
                                                                <div class="form-group">
                                                                    <label>Item Group(s)</label>
                                                                        <telerik:RadComboBox ID="rcbAssetItemClass" runat="server" CheckBoxes="true"></telerik:RadComboBox>    
                                                                    
                                                                    &nbsp;<label>Rate</label>
                                                                    &nbsp;$<asp:TextBox ID="txtShippingRate" runat="server" Width="60px" CssClass="form-control" onkeypress="CheckNumber(1,event)"></asp:TextBox>
                                                                </div>
                                                                <div class="form-group">
                                                                    &nbsp;
                                                                    <asp:Button Text="Add" runat="server" ID="btnAdd" CssClass="btn btn-primary" OnClientClick="return AddCustom();" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <asp:Panel ID="pnlFixedShippingQuote" runat="server">
                                                <asp:DataGrid ID="dgFixedShippingQuote" AllowSorting="True" runat="server" CssClass="table table-striped table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="False"
                                                    HorizontalAlign="Left" CellSpacing="2" CellPadding="2" OnItemCommand="dgFixedShippingQuote_ItemCommand">
                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="is"></ItemStyle>
                                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundColumn Visible="False" DataField="numServiceTypeID"></asp:BoundColumn>
                                                        <asp:TemplateColumn Visible="False">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAllServicetype" runat="server" onclick="SelectAll('chkAllServicetype','chk')"
                                                                    Text="Display in shipping message" />
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chk" CssClass="chk" runat="server" Checked='<%# Eval("bitEnabled") %>' />
                                                                <asp:Label Text='<%# Eval("intNsoftEnum") %>' runat="server" ID="lblNSoftEnm" Style='display: none;' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20px" />
                                                        </asp:TemplateColumn>
                                                        <%-- <asp:TemplateColumn HeaderText="Caption" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Eval("vcServiceName") %>' runat="server" ID="lblServiceName" />
                                                <asp:TextBox runat="server" ID="txtShippingCaption" Text='<%# Eval("vcServiceName") %>'
                                                    CssClass="form-control" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>--%>
                                                        <asp:TemplateColumn HeaderText="From (Amount)" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtFrom" Text='<%#Eval("intFrom")%>' CssClass="form-control"
                                                                    onkeypress="CheckNumber(2,event)" Width="100px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="To (Amount)" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtTo" Text='<%#Eval("intTo")%>' CssClass="form-control"
                                                                    onkeypress="CheckNumber(2,event)" Width="100px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Item Groups(s)" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lblItemClasification" Text='<%#Eval("vcItemClassificationName")%>'
                                                                    onkeypress="CheckNumber(2,event)"  />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>

                                                        <asp:TemplateColumn HeaderText="Rate" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtRate" Text='<%# Eval("monRate") %>' CssClass="form-control"
                                                                    Width="100px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                                                                    Visible="false" CommandArgument='<%# Eval("numServiceTypeID") %>' OnClientClick="return DeleteRecord()"></asp:Button>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                                <div class="form-group">
                                                    <asp:CheckBox ID="chkbxFreeShipping" runat="server" />&nbsp;
                                    <label>Free shipping when order amount reaches: $</label>
                                                    <asp:TextBox ID="txtFreeShipping" runat="server"></asp:TextBox>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top; width: 50%">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <div class="form-group">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label>Rule will apply to </label>
                                                <label>Relationship </label>
                                                <asp:DropDownList ID="ddlRuleRelationShip" runat="server" Style="display: unset" CssClass="form-control" Width="150px"></asp:DropDownList>
                                                <label>Profile </label>
                                                <asp:DropDownList ID="ddlRuleProfile" runat="server" Style="display: unset" CssClass="form-control" Width="150px"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 5px; text-align: left;">
                                                <label>Select a Ship from Warehouse</label>

                                                <telerik:RadComboBox ID="radcmbWarehouse" runat="server" DropDownWidth="200px" Width="200px" CheckBoxes="true"></telerik:RadComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="display: none;">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" MaxLength="1000"
                                                        TextMode="MultiLine" Columns="50" Rows="3"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <div class="box box-primary" runat="server" id="divCountryState" style="width: 95%">

                                        <div class="box-header with-border">
                                            <h3 class="box-title">Country and State(<i>Include following countries and states</i>)</h3>
                                        </div>
                                        <div class="box-body">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="form-inline" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="form-group">
                                                                <label>Country</label>
                                                                <asp:DropDownList ID="ddlCountry" runat="server" Style="width: 150px" AutoPostBack="True" CssClass="form-control"></asp:DropDownList>
                                                                <label>State</label>
                                                                <asp:DropDownList ID="ddlState" runat="server" Width="150px" CssClass="form-control"></asp:DropDownList>
                                                                <label>1st 2 digits of zip code</label>
                                                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" Width="60px"></asp:TextBox>
                                                                <asp:Button Text="Add" runat="server" ID="btnAddState" CssClass="btn btn-primary" OnClientClick="return AddState();" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:DataGrid ID="dgExcludedStates" AllowSorting="True" runat="server" CssClass="dg table table-striped table-bordered" UseAccessibleHeader="true"
                                                HorizontalAlign="Left" AutoGenerateColumns="False">
                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                <ItemStyle CssClass="is"></ItemStyle>
                                                <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="vcCountry" HeaderText="Country">
                                                        <ItemStyle Width="342px" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="vcZipPostalRange" HeaderText="Ship-to Zip/Postal Code ranges">
                                                        <ItemStyle Width="342px" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="vcState" HeaderText="Ship-to State/Province">
                                                        <ItemStyle Width="211px" />
                                                        <ItemStyle />
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                                                                CommandArgument='<%# Eval("numStateID") %>' OnClientClick="return DeleteRecord()"></asp:Button>
                                                            <asp:HiddenField ID="hdnCountry" runat="server" Value='<%# Eval("numCountryID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                            <asp:RadioButtonList ID="rblStateInclude" runat="server" Visible="false" RepeatDirection="Horizontal"
                                                CssClass="signup" RepeatLayout="Flow">
                                                <asp:ListItem Value="1" Selected="True">Exclude</asp:ListItem>
                                                <asp:ListItem Value="2">Include</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <div class="clearfix"></div>
                                            <%-- <div class="form-inline" style="margin-bottom:10px;margin-top:10px;">
                            <label>Apply tax to shipping</label>
                            <asp:DropDownList ID="ddlTax" CssClass="form-control" Style="max-width: 50%;" runat="server">
                                    <asp:ListItem Text="Do not tax" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Tax when taxable items exists" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Always tax" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                        </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <div class="row" style="display: none;">
            <asp:Panel ID="pnlSel" runat="server" Visible="false">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <asp:RadioButtonList ID="rblBasedOn" Visible="false" runat="server" RepeatDirection="Horizontal"
                            CssClass="signup" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Selected="True">Item's Price</asp:ListItem>
                            <asp:ListItem Value="2">Item's Weight</asp:ListItem>
                            <asp:ListItem Value="3">Sum-total of Cart</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-12">
                        <div class="box box-primary">
                            <%--<div class="box-header with-border">
                                <h3 class="box-title">
                                    <asp:RadioButton ID="rbFixedShippingQuotes" Style="font-size: 16px;" AutoPostBack="true" GroupName="ShippingMethod"
                                        Text="Fixed Shipping Quotes" runat="server"></asp:RadioButton></h3>
                            </div>--%>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <%--<div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">

                                <h3 class="box-title">
                                    <asp:RadioButton ID="rbRealTimeShippingQuotes" Style="font-size: 16px;" Text="Real time shipping quotes" GroupName="ShippingMethod"
                                        AutoPostBack="true" runat="server" /></h3>
                            </div>

                            <div class="box-body">
                                <asp:DropDownList ID="ddlRealTimeShippingBasedOn" CssClass="form-control" AutoPostBack="true" runat="server">
                                    <asp:ListItem Text="Ship by order weight" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Ship by order total" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Panel ID="pnlRealTimeShippingQuotes" runat="server">
                                    <asp:DataGrid ID="dgRealTimeShippingQuotes" AllowSorting="True" runat="server" CssClass="table table-striped table-bordered" UseAccessibleHeader="true"
                                        AutoGenerateColumns="False" HorizontalAlign="Left" CellSpacing="2"
                                        CellPadding="2">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="numServiceTypeID"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAllServicetype" runat="server" onclick="SelectAll('chkAllServicetype','chk')"
                                                        Text="Enabled?" />
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk" CssClass="chk" runat="server" Checked='<%# Eval("bitEnabled") %>' />
                                                    <asp:Label Text='<%# Eval("intNsoftEnum") %>' runat="server" ID="lblNSoftEnm" Style='display: none;' />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Caption" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# Eval("vcServiceName") %>' runat="server" ID="lblServiceName" />
                                                    <asp:TextBox runat="server" ID="txtShippingCaption" Text='<%# Eval("vcServiceName") %>'
                                                        CssClass="form-control" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="From" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtFrom" Text='<%#Eval("intFrom")%>' CssClass="form-control"
                                                        onkeypress="CheckNumber(2,event)" Width="75px" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="To" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTo" Text='<%#Eval("intTo")%>' CssClass="form-control"
                                                        onkeypress="CheckNumber(2,event)" Width="75px" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="MarkUp" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120px">
                                                <HeaderTemplate>
                                                    <span style="float: left">MarkUp</span> <span style="float: right">
                                                        <asp:CheckBox ID="chkIsPercentage" Text="" runat="server" onclick="SelectAll('chkIsPercentage','chkIsPercentageClass')" /></span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="form-inline">
                                                        <asp:TextBox runat="server" ID="txtMarkup" Text='<%# Eval("fltMarkup") %>' CssClass="form-control"
                                                            onkeypress="CheckNumber(1,event)" Width="50px" />
                                                        <label for="chkIsPercentage" runat="server" id="lblIsPercentage">
                                                            Is %</label><asp:CheckBox class="chkIsPercentageClass" Text="" runat="server" ID="chkIsPercentage"
                                                                Checked='<%# Eval("bitMarkupType") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Rate" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtRate" Text='<%# Eval("monRate") %>' CssClass="form-control"
                                                        onkeypress="CheckNumber(1,event)" Visible="false" Width="50px" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                                                        Visible="false" CommandArgument='<%# Eval("numServiceTypeID") %>' OnClientClick="return DeleteRecord()"></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>

                        </div>

                    </div>--%>
                </div>

            </asp:Panel>

        </div>



    </div>
</asp:Content>
