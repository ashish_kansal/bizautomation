﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSiteList.aspx.vb" Inherits=".frmSiteList"
    MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script>
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                  <asp:HyperLink ID="hplNew" runat="server" CssClass="btn btn-primary" NavigateUrl="~/ECommerce/frmSite.aspx">New Site</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Sites
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgSites" AllowSorting="false" runat="server" Width="100%" CssClass="table table-striped table-bordered"
                    AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numSiteID" HeaderText="numSiteID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcSiteName" SortExpression="" HeaderText="<font>Site Name</font>"
                            CommandName="Edit"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcDescription" SortExpression="" HeaderText="<font>Description</font>">
                        </asp:BoundColumn>
                        <%-- <asp:BoundColumn DataField="" SortExpression="" HeaderText="<font>Preview</font>">
                        </asp:BoundColumn>--%>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <font>Preview</font>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <a target="_blank" href='<%# "http://" & Eval("vcPreviewURL").ToString.Replace("http://","") %>'>
                                    <%# Eval("vcPreviewURL")%></a>
                                <asp:Label Text='<%# Eval("vcSiteName")%>' runat="server" ID="lblSiteName" Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnActivate" CommandName="Activate" Text='<%# Eval("IsActive") %>'
                                    Style="padding-left: 5px" runat="server"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
