﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMenu.aspx.vb" MasterPageFile="~/common/ECommerceMenuMaster.Master"
    Inherits=".frmMenu1" %>

<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/JavaScript">

        function Save() {
            if (document.getElementById('txtTitle').value == "") {
                alert("Enter a menu title");
                document.getElementById('txtTitle').focus();
                return false;
            }
            if (document.getElementById('rblTargetPage_0').checked == false && document.getElementById('rblTargetPage_1').checked == false) {
                alert("Select a target page type");
                return false;
            }
            if (document.getElementById('rblTargetPage_0').checked == true && document.getElementById('ddlPages').selectedIndex == 0) {
                alert("Select a target page");
                document.getElementById('ddlPages').focus()
                return false;
            }
            if (document.getElementById('rblTargetPage_1').checked == true && document.getElementById('txtTargetURL').value == "") {
                alert("Enter a target page url");
                document.getElementById('txtTargetURL').focus()
                return false;
            }
        }

        function TargetType() {
            if (document.getElementById('rblTargetPage_0').checked == true) {
                document.getElementById('txtTargetURL').disabled = true;
                document.getElementById('ddlPages').disabled = false;
            }
            else if (document.getElementById('rblTargetPage_1').checked == true) {
                document.getElementById('txtTargetURL').disabled = false;
                document.getElementById('ddlPages').disabled = true;

            }
        }
        
    </script>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
   
        <div class="row">
            <div class="col-md-12">
                <div class="form-group pull-right">
                     <asp:LinkButton ID="btnSaveOnly" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save </asp:LinkButton>
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close
                        </asp:LinkButton>
                    <a href="#" class="help">&nbsp;</a>
                </div>
            </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmMenu.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <div class="row" style="margin-top:5px;">
                    <div class="col-md-6">
                        <div class="col-md-3">
                            <label>
                                Menu Title
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <div class="col-md-3">
                            <label>
                                Target Page
                            </label>
                        </div>
                        <div class="col-md-4">
                            <asp:RadioButtonList ID="rblTargetPage" runat="server">
                                <asp:ListItem Text="Site Page &rArr;" Value="1"></asp:ListItem>
                                <asp:ListItem Text="External URL &rArr;" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                            </div>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlPages" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                          
                            <asp:TextBox ID="txtTargetURL" runat="server" style="margin-top:5px;" CssClass="form-control"></asp:TextBox>
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <div class="col-md-3">

                            <label>Status</label>
                        </div>
                        <div class="col-md-4">
                            <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactive" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
