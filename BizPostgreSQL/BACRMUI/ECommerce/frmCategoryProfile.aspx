﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmCategoryProfile.aspx.vb" Inherits=".frmCategoryProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ReloadAndClose() {
            window.opener.document.getElementById("btnReload").click();
            window.close();
            return false;
        }

        function Save() {
            if ($("#txtCategoryProfile").val().length == 0) {
                alert("Select category profile name.");
                $("#txtCategoryProfile").focus();
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            vertical-align: middle;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float: right;">
        <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="button" OnClientClick="return Save();" />
        <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="ReloadAndClose();" CssClass="button" />
    </div>
    <div>
        <asp:Label ID="lblException" runat="server" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Category Profile
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div style="width: 750px">
        <table>
            <tr>
                <td><b>Category Profile Name</b> <span style="color: red">*</span></td>
                <td>
                    <asp:TextBox ID="txtCategoryProfile" runat="server" Width="200" MaxLength="200"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <b>Check sites this profile should apply to:</b>
        <br />
        <asp:CheckBoxList ID="chkblSites" runat="server" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CellSpacing="20" Font-Size="10" ></asp:CheckBoxList>
    </div>
    <asp:HiddenField ID="hdnCategoryProfileID" runat="server" />
</asp:Content>
