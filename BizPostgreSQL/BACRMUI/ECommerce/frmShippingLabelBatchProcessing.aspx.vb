﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.WebAPI

Public Class frmShippingLabelBatchProcessing
    Inherits BACRMPage

#Region "OBJECT DECLARATION"

    Dim objShippingRule As ShippingRule

#End Region

#Region "GLOBAL DECLARATION"

    Dim lngRuleID As Long

#End Region

#Region "PAGE EVENTS"



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")
            lngRuleID = CCommon.ToLong(GetQueryStringVal("RuleID"))

            If Not Page.IsPostBack Then
                BindDropDowns()

                If lngRuleID > 0 Then
                    hdnShipRuleID.Value = lngRuleID
                    'btnSave.Visible = True
                    LoadDetails()
                    hplIndividualItems.Attributes.Add("onclick", "return OpenConfig(3,1," & lngRuleID & ")")
                    hplClassification.Attributes.Add("onclick", "return OpenConfig(3,3," & lngRuleID & ")")
                    btnAdd.Visible = True
                    trMain.Visible = True
                Else
                    btnAdd.Visible = False
                    trMain.Visible = False
                End If

                'BindCriteriaGridView()
                LoadChildCriteria()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
#Region "BIND DROPDOWNS"

    Private Sub BindDropDowns()
        Try
            'Bind Source Marketplace
            Dim objAPI As New WebAPI
            objAPI.WebApiId = CCommon.ToLong(GetQueryStringVal("WebApiID"))
            objAPI.DomainID = CCommon.ToLong(DomainID)
            objAPI.Mode = 1

            ddlMarketplace.DataSource = objAPI.GetWebApi()
            ddlMarketplace.DataTextField = "vcProviderName"
            ddlMarketplace.DataValueField = "WebApiId"
            ddlMarketplace.DataBind()
            ddlMarketplace.Items.Insert(0, New ListItem("All", "0"))

            'Bind Source Ship Method
            BindServiceTypes(ddlSourceShipMethod, 101)
            'BindServiceTypes(ddlSourceShipMethod, 102)
            'BindServiceTypes(ddlSourceShipMethod, 103)

            'Fill Country 
            objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
            If ddlState.Items.Count = 0 Then
                ddlState.Items.Insert(0, New ListItem("-- Select One --", "0"))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindServiceTypes(ByVal ddlName As DropDownList, Optional ByVal intServiceTypeID As Integer = 0)

        If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
        objShippingRule.DomainID = Session("DomainID")
        objShippingRule.RuleID = 0 'lngRuleID
        objShippingRule.ServiceTypeID = intServiceTypeID
        objShippingRule.byteMode = 2

        'ddlName.DataSource = objShippingRule.GetShippingServiceType()
        'ddlName.DataTextField = "vcServiceName"
        'ddlName.DataValueField = "intNsoftEnum"
        'ddlName.DataBind()

        Dim dtItems As DataTable = objShippingRule.GetShippingServiceType()
        Dim dtItemCopy As DataTable = dtItems.Copy.DefaultView.ToTable(True, "vcServiceName", "intNsoftEnum", "vcShippingCompany")
        Dim item As ListItem

        If intServiceTypeID > 0 Then
            For Each dr As DataRow In dtItemCopy.Rows
                item = New ListItem(dr("vcServiceName"), dr("intNsoftEnum"))
                item.Attributes("OptionGroup") = dr("vcShippingCompany")
                ddlName.Items.Add(item)
            Next
        Else
            For Each dr As DataRow In dtItemCopy.Rows
                If CCommon.ToString(dr("vcServiceName")) = "Amazon Standard" OrElse _
                   CCommon.ToString(dr("vcServiceName")) = "ShippingMethodStandard" OrElse _
                   CCommon.ToString(dr("vcServiceName")) = "Other" Then

                Else
                    item = New ListItem(dr("vcServiceName"), dr("intNsoftEnum"))
                    item.Attributes("OptionGroup") = dr("vcShippingCompany")
                    ddlName.Items.Add(item)
                End If

            Next
        End If

        ddlName.Items.Insert(0, New ListItem("--Select One --", "0"))
    End Sub

    Private Sub BindPackageType(ByVal ddlPackage As DropDownList)
        Try
            Dim dsPackages As DataSet = Nothing
            dsPackages = OppBizDocs.LoadAllPackages(Session("DomainID"))
            If dsPackages IsNot Nothing AndAlso dsPackages.Tables.Count > 0 AndAlso dsPackages.Tables(0).Rows.Count > 0 Then

                'ddlPackage.DataSource = dsPackages
                'ddlPackage.DataValueField = "numCustomPackageID"
                'ddlPackage.DataTextField = "vcPackageName"
                'ddlPackage.DataBind()
                Dim item As ListItem
                For Each dr As DataRow In dsPackages.Tables(0).Rows
                    item = New ListItem(dr("vcPackageName"), dr("numCustomPackageID"))
                    item.Attributes("OptionGroup") = dr("vcShippingCompany")
                    ddlPackage.Items.Add(item)
                Next

                ddlPackage.Items.Insert(0, New ListItem("-- Select One --", "0"))
                ddlPackage.SelectedIndex = 0

            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = DomainID
            objUserAccess.Country = lngCountry
            dtTable = objUserAccess.SelState
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

#Region "Bind Gridview"

    'Private Sub BindCriteriaGridView()
    '    Try
    '        Dim dtCriteria As New DataTable
    '        Dim dtIDCol As New DataColumn
    '        dtIDCol.AutoIncrement = True
    '        dtIDCol.AutoIncrementSeed = 1
    '        dtIDCol.AutoIncrementStep = 1
    '        dtIDCol.ColumnName = "ID"
    '        dtIDCol.DataType = GetType(System.Int32)

    '        dtCriteria.Columns.Add(dtIDCol)
    '        dtCriteria.Columns.Add("numChildShipID", GetType(System.Double))
    '        dtCriteria.Columns.Add("FromValue", GetType(System.Double))
    '        dtCriteria.Columns.Add("ToValue", GetType(System.Double))
    '        dtCriteria.Columns.Add("Domestic", GetType(System.Double))
    '        dtCriteria.Columns.Add("International", GetType(System.Double))
    '        dtCriteria.Columns.Add("PackageType", GetType(System.Double))
    '        dtCriteria.AcceptChanges()

    '        'If ViewState("ShippingCriteria") Is Nothing Then
    '        '    Dim dtIDCol As New DataColumn
    '        '    dtIDCol.AutoIncrement = True
    '        '    dtIDCol.AutoIncrementSeed = 1
    '        '    dtIDCol.AutoIncrementStep = 1
    '        '    dtIDCol.ColumnName = "ID"
    '        '    dtIDCol.DataType = GetType(System.Int32)

    '        '    dtCriteria.Columns.Add(dtIDCol)
    '        '    dtCriteria.Columns.Add("numChildShipID", GetType(System.Double))
    '        '    dtCriteria.Columns.Add("FromValue", GetType(System.Double))
    '        '    dtCriteria.Columns.Add("ToValue", GetType(System.Double))
    '        '    dtCriteria.Columns.Add("Domestic", GetType(System.Double))
    '        '    dtCriteria.Columns.Add("International", GetType(System.Double))
    '        '    dtCriteria.Columns.Add("PackageType", GetType(System.Double))
    '        '    dtCriteria.AcceptChanges()
    '        'Else
    '        '    dtCriteria = ViewState("ShippingCriteria")
    '        'End If

    '        If dtCriteria.Rows.Count = 0 Then
    '            For intRow As Integer = 0 To 9
    '                Dim drRow As DataRow
    '                drRow = dtCriteria.NewRow
    '                dtCriteria.Rows.Add(drRow)
    '            Next
    '        Else
    '            For intRow As Integer = 0 To (9 - dtCriteria.Rows.Count)
    '                Dim drRow As DataRow
    '                drRow = dtCriteria.NewRow
    '                dtCriteria.Rows.Add(drRow)
    '            Next
    '        End If

    '        gvCriteria.DataSource = dtCriteria
    '        gvCriteria.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub BindCountryStateCriteriaGrid(Optional ByVal intMode As Integer = -1)
    '    Try
    '        Dim dtCriteria As New DataTable
    '        'If ViewState("CountryState") Is Nothing Then
    '        '    Dim dtIDCol As New DataColumn

    '        '    dtIDCol.AutoIncrement = True
    '        '    dtIDCol.AutoIncrementSeed = 1
    '        '    dtIDCol.AutoIncrementStep = 1
    '        '    dtIDCol.ColumnName = "ID"
    '        '    dtIDCol.DataType = GetType(System.Int32)

    '        '    dtCriteria.Columns.Add(dtIDCol)
    '        '    dtCriteria.Columns.Add("Country", GetType(System.Int32))
    '        '    dtCriteria.Columns.Add("State", GetType(System.Int32))
    '        '    dtCriteria.Columns.Add("CountryName", GetType(System.String))
    '        '    dtCriteria.Columns.Add("StateName", GetType(System.String))

    '        '    dtCriteria.Columns.Add("FromZip", GetType(System.String))
    '        '    dtCriteria.Columns.Add("ToZip", GetType(System.String))
    '        '    dtCriteria.AcceptChanges()

    '        '    ViewState("CountryState") = dtCriteria
    '        'Else
    '        '    dtCriteria = CType(ViewState("CountryState"), DataTable)

    '        '    If dtCriteria.Columns.Contains("numCountryID") Then
    '        '        dtCriteria.Columns("numCountryID").ColumnName = "Country"
    '        '    End If

    '        '    If dtCriteria.Columns.Contains("numStateID") Then
    '        '        dtCriteria.Columns("numStateID").ColumnName = "State"
    '        '    End If

    '        '    dtCriteria.AcceptChanges()
    '        'End If

    '        If intMode >= 0 Then
    '            dtCriteria.Rows.RemoveAt(intMode)
    '            dtCriteria.AcceptChanges()
    '        End If

    '        dtCriteria = GetCountryStateCriteria()
    '        dtCriteria.AcceptChanges()
    '        ViewState("CountryState") = dtCriteria
    '        gvCountryState.DataSource = ViewState("CountryState")
    '        gvCountryState.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

#End Region

#Region "GRIDVIEW EVENTS"

    Private Sub gvCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCriteria.RowDataBound
        Try
            Select Case e.Row.RowType

                Case DataControlRowType.Header

                Case DataControlRowType.DataRow
                    Dim txtFromValue As TextBox = e.Row.FindControl("txtFrom")
                    If txtFromValue IsNot Nothing Then
                        txtFromValue.Text = CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "FromValue"))
                    End If

                    Dim txtToValue As TextBox = e.Row.FindControl("txtTo")
                    If txtToValue IsNot Nothing Then
                        txtToValue.Text = CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "ToValue"))
                    End If

                    Dim ddlDomestic As DropDownList = e.Row.FindControl("ddlDomestic")
                    If ddlDomestic IsNot Nothing Then
                        'OppBizDocs.LoadServiceTypes(ddlSourceShipMethod.SelectedValue, ddlDomestic)
                        BindServiceTypes(ddlDomestic)

                        If ddlDomestic.Items.FindByValue(IIf(DataBinder.Eval(e.Row.DataItem, "Domestic") Is DBNull.Value, "", DataBinder.Eval(e.Row.DataItem, "Domestic"))) IsNot Nothing Then
                            ddlDomestic.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "Domestic")).Selected = True
                            'Else
                            '    ddlDomestic.Items.Insert(0, New ListItem("-- Select One --", "0"))
                        End If
                    End If

                    Dim ddlInternational As DropDownList = e.Row.FindControl("ddlInternational")
                    If ddlInternational IsNot Nothing Then
                        'OppBizDocs.LoadServiceTypes(ddlSourceShipMethod.SelectedValue, ddlInternational)
                        BindServiceTypes(ddlInternational)

                        If ddlInternational.Items.FindByValue(IIf(DataBinder.Eval(e.Row.DataItem, "International") Is DBNull.Value, "", DataBinder.Eval(e.Row.DataItem, "International"))) IsNot Nothing Then
                            ddlInternational.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "International")).Selected = True
                            'Else
                            'ddlInternational.Items.Insert(0, New ListItem("-- Select One --", "0"))
                        End If
                    End If

                    Dim ddlPackagingType As DropDownList = e.Row.FindControl("ddlPackageType")
                    If ddlPackagingType IsNot Nothing Then
                        BindPackageType(ddlPackagingType)

                        If ddlPackagingType.Items.FindByValue(IIf(DataBinder.Eval(e.Row.DataItem, "PackageType") Is DBNull.Value, "", DataBinder.Eval(e.Row.DataItem, "PackageType"))) IsNot Nothing Then
                            ddlPackagingType.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "PackageType")).Selected = True
                        End If

                    End If

                    Dim txtFromZip As TextBox = e.Row.FindControl("txtFromZip")
                    If txtFromZip IsNot Nothing Then
                        txtFromZip.Text = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, "FromZip"))
                    End If

                    Dim txtToZip As TextBox = e.Row.FindControl("txtToZip")
                    If txtToZip IsNot Nothing Then
                        txtToZip.Text = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, "ToZip"))
                    End If

            End Select
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvCountryState_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles gvCountryState.RowDeleted
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvCountryState_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvCountryState.RowDeleting
        Try
            'BindCountryStateCriteriaGrid(e.RowIndex)
            Dim lblRuleStateID As Label = gvCountryState.Rows(e.RowIndex).FindControl("lblID")
            If lblRuleStateID IsNot Nothing Then
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule

                objShippingRule.RuleStateID = CCommon.ToLong(lblRuleStateID.Text)
                objShippingRule.byteMode = 1
                objShippingRule.DomainID = DomainID
                objShippingRule.ManageShippingRuleStates()
            End If

            LoadCountryStateCriteria()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "DROPDOWN EVENTS"

    Private Sub ddlSourceShipMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSourceShipMethod.SelectedIndexChanged
        Try
            'BindCriteriaGridView()
            'LoadChildCriteria()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlMarketplace_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMarketplace.SelectedIndexChanged
        Try
            If ddlMarketplace.SelectedValue > 0 Then
                ddlSourceShipMethod.Enabled = True
                ddlSourceShipMethod.Items.Clear()

                If ddlMarketplace.SelectedValue = 2 Then
                    BindServiceTypes(ddlSourceShipMethod, 101)
                Else
                    BindServiceTypes(ddlSourceShipMethod, 1)
                End If
            Else
                ddlSourceShipMethod.Enabled = False
                ddlSourceShipMethod.SelectedValue = 0
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Try
            'Fill State
            FillState(ddlState, ddlCountry.SelectedItem.Value, DomainID)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "BUTTON EVENTS"

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If ddlCountry.SelectedValue > 0 AndAlso ddlState.SelectedValue > 0 Then

                'Save Child Country/State/Zip Criteria
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule

                objShippingRule.RuleID = lngRuleID
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.byteMode = 0
                objShippingRule.CountryID = CCommon.ToLong(ddlCountry.SelectedValue)
                objShippingRule.StateID = CCommon.ToLong(ddlState.SelectedValue)
                objShippingRule.Mode = 0
                objShippingRule.FromZip = txtFromZip.Text
                objShippingRule.ToZip = txtToZip.Text
                objShippingRule.ManageShippingRuleStates()
                LoadCountryStateCriteria()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Save()

    '        If objShippingRule.ExceptionMsg <> "" Then
    '            litMessage.Text = objShippingRule.ExceptionMsg
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()

            If objShippingRule.ExceptionMsg <> "" Then
                litMessage.Text = objShippingRule.ExceptionMsg
            Else
                Response.Redirect("../ECommerce/frmShippingLabelBatchProcessingList.aspx")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ECommerce/frmShippingLabelBatchProcessingList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "PRIVATE METHODS"

    Private Sub Save()
        Try
            objShippingRule = New ShippingRule
            objShippingRule.RuleID = lngRuleID
            objShippingRule.RuleName = txtRuleName.Text
            objShippingRule.ShipBasedOn = rblBasedOn.SelectedItem.Value
            objShippingRule.SourceCompanyID = ddlMarketplace.SelectedItem.Value
            objShippingRule.SourceShipID = ddlSourceShipMethod.SelectedItem.Value

            If rbIndividualItem.Checked = True Then
                objShippingRule.ItemAffected = 1
            ElseIf rbShipClass.Checked = True Then
                objShippingRule.ItemAffected = 2
            Else
                objShippingRule.ItemAffected = 3
            End If

            objShippingRule.Type = IIf(rblStateInclude.Items(0).Selected = True, 1, 2)
            objShippingRule.DomainID = DomainID
            If objShippingRule.ManageShippingLabelRuleMaster() > 0 Then

                
                'Save Child Table Data
                Dim dtChild As DataTable = GetShippingLabelCriteria()

               


                'Added by :Sachin Sadhu||date:31stJuly2014
                'Purpose :Delete Existing Packaging Rules
                Dim objShipRule As ShippingRule
                objShipRule = New ShippingRule
                objShipRule.PackagingShipRuleID = lngRuleID
                objShipRule.DomainID = Session("DomainID")
                objShipRule.DeletePackagingRulesByShipRuleID()
                'end of code


                If dtChild IsNot Nothing AndAlso dtChild.Rows.Count > 0 Then

                    For intCnt As Integer = 0 To dtChild.Rows.Count - 1
                        objShippingRule.ChildShipID = CCommon.ToDecimal(dtChild.Rows(intCnt)("numChildShipID"))
                        objShippingRule.FromVal = CCommon.ToDecimal(dtChild.Rows(intCnt)("FromValue"))
                        objShippingRule.ToVal = CCommon.ToDecimal(dtChild.Rows(intCnt)("ToValue"))
                        objShippingRule.DomesticShipID = CCommon.ToDecimal(dtChild.Rows(intCnt)("Domestic"))
                        objShippingRule.InternationalShipID = CCommon.ToDecimal(dtChild.Rows(intCnt)("International"))
                        objShippingRule.PackageTypeID = CCommon.ToDecimal(dtChild.Rows(intCnt)("PackageType"))

                        If objShippingRule.ChildShipID > 0 OrElse (objShippingRule.FromVal > 0 AndAlso objShippingRule.ToVal > 0) Then

                            If objShippingRule.FromVal = 0 AndAlso objShippingRule.ToVal = 0 Then
                                objShippingRule.DomesticShipID = 0
                                objShippingRule.InternationalShipID = 0
                                objShippingRule.PackageTypeID = 0
                            End If

                            'Added by :Sachin Sadhu||date:31stJuly2014
                            'Purpose :To consolidate Packaging n shipping rules
                            Dim objShippingRule1 As ShippingRule
                            objShippingRule1 = New ShippingRule
                            objShippingRule1.RuleID = 0
                            objShippingRule1.RuleName = "PR" & txtRuleName.Text & CCommon.ToString(intCnt)
                            objShippingRule1.PackageTypeID = CCommon.ToDecimal(dtChild.Rows(intCnt)("PackageType"))
                            objShippingRule1.ShipClassID = 0
                            objShippingRule1.FromQty = CCommon.ToDecimal(dtChild.Rows(intCnt)("ToValue"))
                            objShippingRule1.DomainID = DomainID
                            objShippingRule1.PackagingShipRuleID = lngRuleID
                            If objShippingRule1.ManagePackagingRules() > 0 Then
                                objShippingRule.ManageShippingLabelRuleChild()
                                ' litMessage.Text = "Package Rule saved successfully."
                            Else
                                'litMessage.Text = "Error occurred while save Package data."
                            End If
                            'end of sachin

                            'commented by sachin--objShippingRule.ManageShippingLabelRuleChild()
                        End If

                    Next

                End If

            End If

            'ViewState("ShippingCriteria") = Nothing
            'ViewState("CountryState") = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetShippingLabelCriteria() As DataTable
        Dim dtResult As DataTable = Nothing
        Try

            dtResult = New DataTable
            dtResult.Columns.Add("ID", GetType(System.Int32))
            dtResult.Columns.Add("numChildShipID", GetType(System.Int32))
            dtResult.Columns.Add("FromValue", GetType(System.Decimal))
            dtResult.Columns.Add("ToValue", GetType(System.Decimal))
            dtResult.Columns.Add("Domestic", GetType(System.Decimal))
            dtResult.Columns.Add("International", GetType(System.Decimal))
            dtResult.Columns.Add("PackageType", GetType(System.Decimal))
            dtResult.AcceptChanges()

            For intCnt As Integer = 0 To gvCriteria.Rows.Count - 1

                Dim drRow As DataRow
                drRow = dtResult.NewRow

                Dim hdnChildShipID As HiddenField = gvCriteria.Rows(intCnt).FindControl("hdnChildShipID")
                If hdnChildShipID IsNot Nothing Then
                    drRow("numChildShipID") = CCommon.ToDecimal(hdnChildShipID.Value)
                End If

                Dim txtFrom As TextBox = gvCriteria.Rows(intCnt).FindControl("txtFrom")

                If txtFrom IsNot Nothing Then
                    drRow("FromValue") = CCommon.ToDecimal(txtFrom.Text)
                Else
                    drRow("FromValue") = ""
                End If

                Dim txtTo As TextBox = gvCriteria.Rows(intCnt).FindControl("txtTo")
                If txtTo IsNot Nothing Then
                    drRow("ToValue") = CCommon.ToDecimal(txtTo.Text)
                Else
                    drRow("ToValue") = ""
                End If

                Dim ddlDomestic As DropDownList = gvCriteria.Rows(intCnt).FindControl("ddlDomestic")
                If ddlDomestic IsNot Nothing Then
                    drRow("Domestic") = CCommon.ToDecimal(ddlDomestic.SelectedItem.Value)
                Else
                    drRow("Domestic") = ""
                End If

                Dim ddlInternational As DropDownList = gvCriteria.Rows(intCnt).FindControl("ddlInternational")
                If ddlInternational IsNot Nothing Then
                    drRow("International") = CCommon.ToDecimal(ddlInternational.SelectedItem.Value)
                Else
                    drRow("International") = ""
                End If

                Dim ddlPackageType As DropDownList = gvCriteria.Rows(intCnt).FindControl("ddlPackageType")
                If ddlPackageType IsNot Nothing Then
                    drRow("PackageType") = CCommon.ToDecimal(ddlPackageType.SelectedItem.Value)
                Else
                    drRow("PackageType") = ""
                End If

                If ddlDomestic.SelectedIndex > 0 OrElse ddlInternational.SelectedIndex > 0 OrElse ddlPackageType.SelectedIndex > 0 Then

                    If dtResult.Select("FromValue=" & Val(txtFrom.Text) & _
                                       " AND ToValue=" & Val(txtTo.Text) & _
                                       " AND ISNULL(Domestic,0) = " & ddlDomestic.SelectedItem.Value & _
                                       " AND ISNULL(International,0) = " & ddlInternational.SelectedItem.Value & _
                                       " AND ISNULL(PackageType,0) = " & ddlPackageType.SelectedItem.Value).Length > 0 Then

                    Else
                        'If dtResult.Rows.Count > 0 Then
                        '    drRow("FromValue") = Val(txtFrom.Text)
                        '    drRow("ToValue") = Val(txtTo.Text)
                        '    drRow("Domestic") = ddlDomestic.SelectedItem.Value
                        '    drRow("International") = ddlInternational.SelectedItem.Value
                        '    drRow("PackageType") = ddlPackageType.SelectedItem.Value
                        '    dtResult.AcceptChanges()
                        'Else
                        dtResult.Rows.Add(drRow)
                        'End If
                    End If
                    dtResult.AcceptChanges()

                End If

            Next

            'ViewState("ShippingCriteria") = dtResult
        Catch ex As Exception
            dtResult = Nothing
            Throw ex
        End Try

        Return dtResult
    End Function

    'Private Function GetCountryStateCriteria() As DataTable
    '    Dim dtResult As DataTable = Nothing
    '    Try
    '        dtResult = ViewState("CountryState")

    '        If ddlCountry.SelectedIndex > 0 Then
    '            Dim drRow As DataRow
    '            drRow = dtResult.NewRow
    '            drRow("FromZip") = CCommon.ToDecimal(txtFromZip.Text)
    '            drRow("ToZip") = CCommon.ToDecimal(txtToZip.Text)
    '            drRow("Country") = CCommon.ToDecimal(ddlCountry.SelectedItem.Value)
    '            drRow("State") = CCommon.ToDecimal(ddlState.SelectedItem.Value)
    '            drRow("CountryName") = CCommon.ToString(ddlCountry.SelectedItem.Text)
    '            drRow("StateName") = CCommon.ToString(ddlState.SelectedItem.Text)
    '            dtResult.Rows.Add(drRow)
    '            dtResult.AcceptChanges()

    '            ViewState("CountryState") = dtResult
    '        End If
    '    Catch ex As Exception
    '        dtResult = Nothing
    '        Throw ex
    '    End Try
    '    Return dtResult
    'End Function

    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            objShippingRule = New ShippingRule()

            objShippingRule.RuleID = lngRuleID
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.byteMode = 0
            dtTable = objShippingRule.GetShippingLabelRuleMaster
            If dtTable.Rows.Count > 0 Then
                txtRuleName.Text = dtTable.Rows(0).Item("vcRuleName")

                Select Case dtTable.Rows(0).Item("intItemAffected")
                    Case 1
                        rbIndividualItem.Checked = True
                        hplIndividualItems.Text = hplIndividualItems.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString + ")"
                        hplClassification.Text = hplClassification.Text + " (0)"
                    Case 2
                        rbShipClass.Checked = True
                        hplClassification.Text = hplClassification.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString + ")"
                        hplIndividualItems.Text = hplIndividualItems.Text + " (0)"
                    Case 3
                        rbAllItems.Checked = True
                End Select

                rblBasedOn.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("tintShipBasedOn"))
                rblStateInclude.SelectedValue = CCommon.ToLong(dtTable.Rows(0)("tintType"))

                If ddlMarketplace.SelectedValue > 0 Then
                    ddlSourceShipMethod.Enabled = True
                    ddlSourceShipMethod.Items.Clear()

                    If ddlMarketplace.SelectedValue = 2 Then
                        BindServiceTypes(ddlSourceShipMethod, 101)
                    Else
                        BindServiceTypes(ddlSourceShipMethod, 1)
                    End If
                Else
                    ddlSourceShipMethod.Enabled = False
                    ddlSourceShipMethod.SelectedValue = 0
                End If

                ddlMarketplace.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numSourceCompanyID"))
                ddlSourceShipMethod.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numSourceShipID"))

                LoadChildCriteria()
                LoadCountryStateCriteria()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadChildCriteria()
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule
            Dim dtChild As DataTable = objShippingRule.GetShippingLabelRuleChild()

            If dtChild IsNot Nothing AndAlso dtChild.Rows.Count > 0 Then
                For intRow As Integer = 0 To (9 - dtChild.Rows.Count)
                    Dim drRow As DataRow
                    drRow = dtChild.NewRow
                    dtChild.Rows.Add(drRow)
                Next
            Else
                'BindCriteriaGridView()
                Dim dcIDCol As New DataColumn
                dcIDCol.AutoIncrement = True
                dcIDCol.AutoIncrementSeed = 1
                dcIDCol.AutoIncrementStep = 1
                dcIDCol.ColumnName = "ID"
                dcIDCol.DataType = GetType(System.Int32)

                dtChild.Columns.Add(dcIDCol)
                If Not dtChild.Columns.Contains("numChildShipID") Then dtChild.Columns.Add("numChildShipID", GetType(System.Double))
                If Not dtChild.Columns.Contains("FromValue") Then dtChild.Columns.Add("FromValue", GetType(System.Double))
                If Not dtChild.Columns.Contains("ToValue") Then dtChild.Columns.Add("ToValue", GetType(System.Double))
                If Not dtChild.Columns.Contains("Domestic") Then dtChild.Columns.Add("Domestic", GetType(System.Double))
                If Not dtChild.Columns.Contains("International") Then dtChild.Columns.Add("International", GetType(System.Double))
                If Not dtChild.Columns.Contains("PackageType") Then dtChild.Columns.Add("PackageType", GetType(System.Double))
                dtChild.AcceptChanges()

                For intRow As Integer = 0 To 9
                    Dim drRow As DataRow
                    drRow = dtChild.NewRow
                    dtChild.Rows.Add(drRow)
                Next
            End If

            gvCriteria.DataSource = dtChild
            gvCriteria.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadCountryStateCriteria()
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule
            objShippingRule.RuleID = lngRuleID
            objShippingRule.DomainID = DomainID
            Dim dtChild As DataTable = objShippingRule.GetShippingRuleStates
            If dtChild IsNot Nothing AndAlso dtChild.Rows.Count > 0 Then

                If Not dtChild.Columns.Contains("ID") Then
                    dtChild.Columns.Add("ID", GetType(System.Int32))
                    dtChild.Columns("vcCountry").ColumnName = "CountryName"
                    dtChild.Columns("vcState").ColumnName = "StateName"

                    For intCnt As Integer = 0 To dtChild.Rows.Count - 1
                        dtChild.Rows(intCnt)("ID") = intCnt + 1
                        dtChild.AcceptChanges()
                    Next
                End If
            End If

            gvCountryState.DataSource = dtChild
            gvCountryState.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region


End Class