﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmShippingRule

    '''<summary>
    '''tdClone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdClone As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lblRuleCloneRel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRuleCloneRel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlCloneRelationship control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCloneRelationship As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblRuleCloneProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRuleCloneProfile As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlCloneProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCloneProfile As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnCloneRule control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloneRule As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnSaveClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveClose As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''hdnShipRuleID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnShipRuleID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''UpdateProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''lblSite control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSite As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtRuleName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRuleName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlSites control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSites As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Label103 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label103 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rblCommissionType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblCommissionType As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''trShipping control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trShipping As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''pnlCustomShippingMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCustomShippingMethod As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlCustomMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCustomMethod As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFrom As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rcbAssetItemClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rcbAssetItemClass As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''txtShippingRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtShippingRate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAdd As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlFixedShippingQuote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFixedShippingQuote As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dgFixedShippingQuote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgFixedShippingQuote As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''chkbxFreeShipping control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkbxFreeShipping As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtFreeShipping control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFreeShipping As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlRuleRelationShip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRuleRelationShip As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlRuleProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRuleProfile As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''radcmbWarehouse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radcmbWarehouse As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''txtDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDescription As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''divCountryState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divCountryState As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCountry As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlState As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtZipCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtZipCode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnAddState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddState As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''dgExcludedStates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgExcludedStates As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''rblStateInclude control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblStateInclude As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''pnlSel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rblBasedOn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblBasedOn As Global.System.Web.UI.WebControls.RadioButtonList
End Class
