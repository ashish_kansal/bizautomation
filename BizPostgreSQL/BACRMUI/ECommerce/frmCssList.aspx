﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCssList.aspx.vb" MasterPageFile="~/common/ECommerceMenuMaster.Master"
    Inherits=".frmCssList" %>
<%@ MasterType VirtualPath="~/common/ECommerceMenuMaster.Master" %>
<%@ Register Src="SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function NewCssAndJs() {
            if (document.getElementById('hdnType').value < 2) {
                if (document.getElementById('ddlSites').selectedIndex == 0) {
                    var hplNew = document.getElementById("hplNew");
                    hplNew.href = "#";
                    alert("Please select a site to upload " + hplNew.innerHTML);
                    document.getElementById('ddlSites').focus();
                    return false;
                }
            }
            return true;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12" id="Table1" runat="server">
            <div class="pull-right">
                 <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />
                        <asp:HyperLink ID="hplNew" runat="server" CssClass="hyperlink"></asp:HyperLink>
                <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="normal4" align="center" colspan="2" valign="top">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('ECommerce/frmCssList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <asp:LinkButton ID="btnSaveOrder" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save Display Order
                        </asp:LinkButton>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
    <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgStyles" AllowSorting="false" runat="server" Width="100%" CssClass="table table-striped table-bordered"
                    AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                         <asp:BoundColumn Visible="false" DataField="numCssID" HeaderText="numCssID"></asp:BoundColumn>     
                        <asp:ButtonColumn DataTextField="StyleName" SortExpression="" HeaderText="<font>Name</font>"
                            CommandName="Edit">
                        <ItemStyle Width="400px"/>
                            </asp:ButtonColumn>
                        <asp:BoundColumn DataField="StyleFileName" SortExpression="" HeaderText="<font>File Name</font>">
                        
                        </asp:BoundColumn>
                         <asp:TemplateColumn HeaderText="Display Order"  >
                                        <ItemStyle HorizontalAlign="Center" Width="100px"/>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" CssClass="signup" Width="20" Text='<%# Eval("intDisplayOrder") %>'
                                                onkeypress="CheckNumber(2,event)"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField runat="server" ID="hdnType" />
        </div>
</asp:Content>
