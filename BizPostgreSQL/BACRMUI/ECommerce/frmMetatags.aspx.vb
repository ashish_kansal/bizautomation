﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Partial Public Class frmMetatags
    Inherits BACRMPage
    Dim lngMetaId As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
        
        GetUserRightsForPage(13, 41)
        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            Response.Redirect("../admin/authentication.aspx?mesg=AS")
        End If
        If GetQueryStringVal( "CssID") <> "" Then
            lngMetaId = GetQueryStringVal( "CssID")
        End If
        If Not IsPostBack Then
            
            If lngMetaId > 0 Then
                LoadDetails()
            End If
            lblName.Text = "Update MetaTags"
        End If
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If radItem.SelectedValue <> "" Then
                UpdateMetaTag()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub UpdateMetaTag()
        Try
            Dim objSite As New Sites
            objSite.MetaID = lngMetaId
            objSite.MetaTags = HttpUtility.HtmlEncode(txtMetaTags.Text.Trim())
            objSite.MetaTagFor = 2
            objSite.ReferenceID = radItem.SelectedValue
            objSite.ManageMetaTags()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objSite As New Sites
            Dim dtMeta As DataTable
            objSite.MetaID = lngMetaId
            objSite.MetaTagFor = 2 '1-page,2-Item
            objSite.ReferenceID = radItem.SelectedValue
            dtMeta = objSite.GetMetaTag()
            txtMetaTags.Text = ""
            If dtMeta.Rows.Count > 0 Then
                txtMetaTags.Text = dtMeta.Rows(0)("vcMetaTag").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub radItem_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radItem.SelectedIndexChanged
        Try
            If radItem.SelectedValue <> "" Then
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

End Class