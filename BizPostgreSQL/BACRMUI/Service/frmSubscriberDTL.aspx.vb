Imports System.IO
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Service
    Partial Public Class frmSubscriberDTL : Inherits BACRMPage

        Dim lngSubcriberID As Long
        Dim objSubscribers As Subscribers


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                lngSubcriberID = CCommon.ToLong(GetQueryStringVal("SubID"))
                If Not IsPostBack Then
                    'If Session("UserContactID") <> "1" Then
                    '    Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                    'End If

                    objSubscribers = New Subscribers
                    LoadDetails()
                    BindSourceDomain()
                    BindSourceGroup()
                    BindSites(ddlDestinationDomainSite, CCommon.ToLong(txtTarDomain.Text))
                End If
                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                hplOpenHstr.Attributes.Add("onclick", "return OpenHstr(" & lngSubcriberID & ")")

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Sub LoadDetails()
            Try
                Dim dtTable As DataTable
                objSubscribers.SubscriberID = lngSubcriberID
                objSubscribers.DomainID = Session("DomainID")
                dtTable = objSubscribers.GetSubscriberDetail
                If dtTable.Rows.Count > 0 Then
                    lblCompany.Text = dtTable.Rows(0).Item("vcCompanyName")
                    If Not IsDBNull(dtTable.Rows(0).Item("dtSubStartDate")) Then calSubStartDate.SelectedDate = dtTable.Rows(0).Item("dtSubStartDate")
                    If Not IsDBNull(dtTable.Rows(0).Item("dtSubEndDate")) Then calSubRenewDate.SelectedDate = dtTable.Rows(0).Item("dtSubEndDate")
                    txtFullUser.Text = CCommon.ToInteger(dtTable.Rows(0).Item("intNoofUsersSubscribed"))
                    txtLAUser.Text = CCommon.ToInteger(dtTable.Rows(0).Item("intNoofLimitedAccessUsers"))
                    txtBPUser.Text = CCommon.ToInteger(dtTable.Rows(0).Item("intNoofBusinessPortalUsers"))
                    txtSite.Text = CCommon.ToInteger(dtTable.Rows(0).Item("intNoofSites"))

                    If Not dtTable.Rows(0).Item("monFullUserCost") Is DBNull.Value Then
                        txtFullUserCost.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monFullUserCost"))
                    End If

                    If Not dtTable.Rows(0).Item("monLimitedAccessUserCost") Is DBNull.Value Then
                        txtLAUserCost.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monLimitedAccessUserCost"))
                    End If

                    If Not dtTable.Rows(0).Item("monBusinessPortalUserCost") Is DBNull.Value Then
                        txtBPUserCost.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monBusinessPortalUserCost"))
                    End If

                    If Not dtTable.Rows(0).Item("monSiteCost") Is DBNull.Value Then
                        txtSiteCost.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monSiteCost"))
                    End If

                    txtUserConcurrency.Text = CCommon.ToInteger(dtTable.Rows(0)("intFullUserConcurrency"))

                    lblFullUserTotalCost.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(txtFullUserCost.Text) * CCommon.ToInteger(txtFullUser.Text))
                    lblLAUserTotalCost.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(txtLAUserCost.Text) * CCommon.ToInteger(txtLAUser.Text))
                    lblBAUserTotalCost.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(txtBPUserCost.Text) * CCommon.ToInteger(txtBPUser.Text))
                    lblSiteTotalCost.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(txtSiteCost.Text) * CCommon.ToInteger(txtSite.Text))

                    chkPayingCustomer.Checked = Not CCommon.ToBool(dtTable.Rows(0).Item("bitTrial"))
                    ddlStatus.SelectedValue = dtTable.Rows(0).Item("bitActive")
                    txtTarDomain.Text = dtTable.Rows(0).Item("numTargetDomainID")
                    txtSusReason.Text = dtTable.Rows(0).Item("vcSuspendedReason")
                    If Not IsDBNull(dtTable.Rows(0).Item("dtSuspendedDate")) Then
                        lblSupendedDate.Text = FormattedDateFromDate(dtTable.Rows(0).Item("dtSuspendedDate"), Session("DateFormat"))
                    End If
                    hplBranches.Text = "Subsidiaries & Branches (" & dtTable.Rows(0).Item("BranchCount").ToString() & ")"
                    hplBranches.Attributes.Add("onclick", "return OpenBranches('" & lngSubcriberID.ToString() & "','" & lblCompany.Text & "')")
                    objSubscribers.DomainID = dtTable.Rows(0).Item("numTargetDomainID")
                    ddlContact.DataSource = objSubscribers.ContactsFromDomain
                    ddlContact.DataTextField = "Name"
                    ddlContact.DataValueField = "numContactID"
                    ddlContact.DataBind()
                    'bug fix 1600 
                    If ddlContact.Items.Count = 0 Then
                        ddlContact.Items.Insert(0, "--Select One--")
                        ddlContact.Items.FindByText("--Select One--").Value = "0"
                    End If
                    If Not ddlContact.Items.FindByValue(dtTable.Rows(0).Item("numAdminContactID")) Is Nothing Then
                        ddlContact.Items.FindByValue(dtTable.Rows(0).Item("numAdminContactID")).Selected = True
                    End If

                    chkEnableAccountingAudit.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitEnabledAccountingAudit"))
                    chkNotifyAdmin.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitEnabledNotifyAdmin"))
                    chkAllowToEditHelpFile.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowToEditHelpFile"))

                    BindUsers()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindSourceDomain()
            Try
                Dim objSubscribers As New MultiCompany
                objSubscribers.Mode = 3
                ddlSourceDomain.DataSource = objSubscribers.GetDomain
                ddlSourceDomain.DataTextField = "vcDomainName"
                ddlSourceDomain.DataValueField = "numDomainID"
                ddlSourceDomain.DataBind()
                'bug fix 1600 
                ddlSourceDomain.Items.Insert(0, "--Select One--")
                ddlSourceDomain.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception

            End Try
        End Sub
        Sub BindSourceGroup()
            Try
                Dim objConfigWizard As New FormConfigWizard
                Dim dtAuthenticationGroups As DataTable                                 'declare a datatable
                objConfigWizard.AuthenticationGroupID = 0
                objConfigWizard.DomainID = CCommon.ToLong(txtTarDomain.Text)
                dtAuthenticationGroups = objConfigWizard.getAuthenticationGroups()      'call to get the authentication groups
                ddlDestinationGroup.DataSource = dtAuthenticationGroups.DefaultView 'set the datasource
                ddlDestinationGroup.DataTextField = "vcGroupName"             'set the text attribute
                ddlDestinationGroup.DataValueField = "numGroupID"             'set the value attribute
                ddlDestinationGroup.DataBind()
                If ddlDestinationGroup.Items.Count = 0 Then
                    ddlDestinationGroup.Items.Insert(0, "--Select One--")
                    ddlDestinationGroup.Items.FindByText("--Select One--").Value = "0"
                End If


            Catch ex As Exception

            End Try
        End Sub
        Private Sub BindUsers()
            Try
                objSubscribers = New Subscribers
                objSubscribers.SubscriberID = lngSubcriberID
                objSubscribers.DomainID = CCommon.ToLong(txtTarDomain.Text)
                objSubscribers.GroupType = CCommon.ToShort(hdnGroupType.Value)
                dgUsers.DataSource = objSubscribers.ContactListFromDomain
                dgUsers.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
                BindUsers()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                Response.Redirect("../Service/frmSubscriberList.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub Save()
            Try
                objSubscribers = New Subscribers
                objSubscribers.SubscriberID = lngSubcriberID
                objSubscribers.DomainID = Session("DomainID")
                objSubscribers.SubStartDate = calSubStartDate.SelectedDate
                objSubscribers.SubEndDate = calSubRenewDate.SelectedDate
                objSubscribers.NoOfUsers = CCommon.ToInteger(txtFullUser.Text)
                objSubscribers.FullUserConcurrency = CCommon.ToInteger(txtUserConcurrency.Text)
                objSubscribers.FullUserCost = CCommon.ToDouble(txtFullUserCost.Text)
                objSubscribers.NoOfLimitedAccessUsers = CCommon.ToInteger(txtLAUser.Text)
                objSubscribers.LimitedAccessUserCost = CCommon.ToDouble(txtLAUserCost.Text)
                objSubscribers.NoOfBusinessPortalUsers = CCommon.ToInteger(txtBPUser.Text)
                objSubscribers.BusinessPortalUserCost = CCommon.ToDouble(txtBPUserCost.Text)
                objSubscribers.SiteCost = CCommon.ToDouble(txtSiteCost.Text)
                objSubscribers.Trial = IIf(chkPayingCustomer.Checked = True, 0, 1)
                objSubscribers.Active = ddlStatus.SelectedValue
                objSubscribers.SuspendedReason = txtSusReason.Text
                objSubscribers.AdminCntID = ddlContact.SelectedValue
                objSubscribers.UserCntID = Session("UserContactID")
                objSubscribers.IsEnabledAccountingAudit = chkEnableAccountingAudit.Checked
                objSubscribers.IsEnabledNotifyAdmin = chkNotifyAdmin.Checked
                objSubscribers.IsAllowToEditHelpFile = chkAllowToEditHelpFile.Checked

                Dim dtUsers As New DataTable
                dtUsers.Columns.Add("numContactID")
                dtUsers.Columns.Add("UserName")
                dtUsers.Columns.Add("numUserID")
                dtUsers.Columns.Add("Email")
                dtUsers.Columns.Add("vcPassword")
                dtUsers.Columns.Add("Active")
                dtUsers.TableName = "Users"

                Dim dgItem As DataGridItem
                Dim ds As New DataSet
                Dim dr As DataRow
                For Each dgItem In dgUsers.Items
                    dr = dtUsers.NewRow
                    dr("numContactID") = dgItem.Cells(0).Text
                    dr("UserName") = dgItem.Cells(6).Text
                    dr("numUserID") = IIf(IsNumeric(dgItem.Cells(1).Text), dgItem.Cells(1).Text, 0)
                    dr("Email") = CType(dgItem.FindControl("txtEmail"), TextBox).Text
                    Dim plainText As String
                    Dim cipherText As String
                    plainText = CType(dgItem.FindControl("txtPassword"), TextBox).Text
                    cipherText = objCommon.Encrypt(plainText)
                    dr("vcPassword") = cipherText
                    dr("Active") = IIf(CType(dgItem.FindControl("chkActive"), CheckBox).Checked, 1, 0)
                    dtUsers.Rows.Add(dr)
                Next
                ds.Tables.Add(dtUsers)
                objSubscribers.strUsers = ds.GetXml
                objSubscribers.ManageSubscribers()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                Response.Redirect("../Service/frmSubscriberList.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnCreateSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateSub.Click
            Try
                objSubscribers = New Subscribers
                objSubscribers.SubscriberID = lngSubcriberID
                objSubscribers.UserCntID = Session("UserContactID")
                objSubscribers.CreateNewSubscription()
                Response.Redirect("../Service/frmSubscriberDTL.aspx?SubID=" & lngSubcriberID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgUsers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUsers.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    If Not e.Item.FindControl("ddlLicenseType") Is Nothing Then
                        If Not DirectCast(e.Item.FindControl("ddlLicenseType"), DropDownList).Items.FindByValue(hdnGroupType.Value) Is Nothing Then
                            DirectCast(e.Item.FindControl("ddlLicenseType"), DropDownList).Items.FindByValue(hdnGroupType.Value).Selected = True
                        End If
                    End If
                ElseIf e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    If e.Item.Cells(4).Text = "True" Then CType(e.Item.FindControl("chkActive"), CheckBox).Checked = True
                    CType(e.Item.FindControl("txtEmail"), TextBox).Text = IIf(e.Item.Cells(3).Text = "&nbsp;", "", e.Item.Cells(3).Text)

                    If e.Item.Cells(2).Text <> "&nbsp;" Then
                        Try
                            Dim strPassword As String
                            strPassword = objCommon.Decrypt(e.Item.Cells(2).Text)
                            CType(e.Item.FindControl("txtPassword"), TextBox).Text = strPassword
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindSites(ByVal ddlSites As DropDownList, ByVal DomainId As Long)
            Try
                Dim objSite As New Sites
                objSite.DomainID = DomainId
                ddlSites.DataSource = objSite.GetSites()
                ddlSites.DataTextField = "vcSiteName"
                ddlSites.DataValueField = "numSiteID"
                ddlSites.DataBind()
                ddlSites.Items.Insert(0, "--Select One--")
                ddlSites.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub ddlLicenseType_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                hdnGroupType.Value = CCommon.ToShort(DirectCast(sender, DropDownList).SelectedValue)
                BindUsers()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlSourceGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSourceGroup.SelectedIndexChanged

        End Sub

        Protected Sub ddlSourceDomain_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSourceDomain.SelectedIndexChanged
            Try
                Dim objConfigWizard As New FormConfigWizard
                Dim dtAuthenticationGroups As DataTable                                 'declare a datatable
                objConfigWizard.AuthenticationGroupID = 0
                objConfigWizard.DomainID = ddlSourceDomain.SelectedValue
                ddlSourceGroup.Items.Clear()
                dtAuthenticationGroups = objConfigWizard.getAuthenticationGroups()      'call to get the authentication groups
                ddlSourceGroup.DataSource = dtAuthenticationGroups.DefaultView 'set the datasource
                ddlSourceGroup.DataTextField = "vcGroupName"             'set the text attribute
                ddlSourceGroup.DataValueField = "numGroupID"             'set the value attribute
                ddlSourceGroup.DataBind()
                ddlSourceGroup.Items.Insert(0, "--Select One--")
                ddlSourceGroup.Items.FindByText("--Select One--").Value = "0"

                BindSites(ddlSourceDomainSite, CCommon.ToLong(ddlSourceDomain.SelectedValue))
            Catch ex As Exception

            End Try
        End Sub


        Sub SaveDomainGroup()
            Try
                objSubscribers = New Subscribers
                objSubscribers.sourceDomainId = ddlSourceDomain.SelectedValue
                objSubscribers.destinationDomainId = CCommon.ToLong(txtTarDomain.Text)
                objSubscribers.sourceGroupId = ddlSourceGroup.SelectedValue
                objSubscribers.destinationGroupId = ddlDestinationGroup.SelectedValue
                objSubscribers.taskToPerform = GenerateCheckedItemString(rcbtaskToPerform)
                If (objSubscribers.taskToPerform.Split(",").Contains("4")) Then
                    'CreateDefaultData(0)
                End If
                objSubscribers.ManageDomainDataMigration()
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub CreateDefaultData()
            Try
                Try

                    Dim objSite As New Sites
                    Dim dsPage As DataSet

                    Dim dtPages As DataTable
                    objSite.PageID = 0
                    objSite.SiteID = CCommon.ToLong(ddlDestinationDomainSite.SelectedValue)
                    objSite.DomainID = CCommon.ToLong(txtTarDomain.Text)
                    dtPages = objSite.GetPages().Tables(0)
                    For Each drPage As DataRow In dtPages.Rows
                        If CCommon.ToBool(drPage("bitIsActive")) = True Then
                            objSite.PageID = drPage("numPageID")
                            objSite.SiteID = CCommon.ToLong(ddlDestinationDomainSite.SelectedValue)
                            objSite.DomainID = CCommon.ToLong(txtTarDomain.Text)
                            dsPage = objSite.GetPages()
                            If dsPage.Tables(0).Rows.Count > 0 Then
                                objSite.ManageAspxPages(CCommon.ToLong(ddlDestinationDomainSite.SelectedValue), dsPage.Tables(0).Rows(0).Item("vcPageURL").ToString(), CCommon.ToBool(dsPage.Tables(0).Rows(0).Item("bitIsMaintainScroll")))
                            End If
                        End If
                    Next
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Shared Sub CopyAllFiles(ByVal SourcePath As String, ByVal targetPath As String)
            'Copy all files under ~/Default Folder to New creted Site Folder
            Dim fileName, destFile As String
            If System.IO.Directory.Exists(SourcePath) Then
                Dim files() As String = System.IO.Directory.GetFiles(SourcePath)
                'Copy the files and overwrite destination files if they already exist.
                For Each s As String In files
                    'remove readonly
                    File.SetAttributes(s, FileAttributes.Normal)
                    ' Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s)
                    destFile = System.IO.Path.Combine(targetPath, fileName)

                    System.IO.File.Copy(s, destFile, True)

                    'TODO:BizCart/Default/*.Css File in vss will be installed as read only , reset it . so deleting site doesn't throw error
                    File.SetAttributes(destFile, FileAttributes.Normal)
                Next
            End If
        End Sub
        Function GenerateCheckedItemString(ByVal radComboBox As RadComboBox) As String
            Dim strTransactionType As String
            For Each item As RadComboBoxItem In radComboBox.CheckedItems
                If CCommon.ToLong(item.Value) > 0 Then
                    strTransactionType = strTransactionType & item.Value & ","
                End If
            Next
            Return strTransactionType
        End Function
        Protected Sub btnExcute_Click(sender As Object, e As EventArgs) Handles btnExcute.Click
            SaveDomainGroup()
        End Sub
        Protected Sub btnExecuteEcommerce_Click(sender As Object, e As EventArgs) Handles btnExecuteEcommerce.Click
            Try
                objSubscribers = New Subscribers
                objSubscribers.sourceDomainId = ddlSourceDomain.SelectedValue
                objSubscribers.destinationDomainId = CCommon.ToLong(txtTarDomain.Text)
                objSubscribers.sourceSiteId = ddlSourceDomainSite.SelectedValue
                objSubscribers.destinationSiteId = ddlDestinationDomainSite.SelectedValue
                objSubscribers.taskToPerform = "4"
                objSubscribers.ManageDomainDataMigration()
                CreateDefaultData()
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace