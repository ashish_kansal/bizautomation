﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Service
    Public Class frmSubscriberReport
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    If Session("UserContactID") <> "1" Then
                        Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                    End If

                    txtSortColumn.Text = "monTotalBiz"
                    txtSortOrder.Text = "DESC"
                    txtCurrrentPage.Text = "1"
                    BindDatagrid()
                End If

                btnAddNewSubscriber.Attributes.Add("onclick", "return AddNewSubscriber()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#Region "Private Methods"

        Private Sub BindDatagrid()
            Try
                Dim objSubscribers As New Subscribers
                objSubscribers.Trial = chkPayingCustomer.Checked
                objSubscribers.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objSubscribers.SearchWord = txtSearch.Text.Trim
                objSubscribers.SortCharacter = txtSortChar.Text.Trim()
                objSubscribers.SortOrder = ddlSort.SelectedValue
                objSubscribers.LastLoginFilter = ddlDateFilter.SelectedValue
                objSubscribers.columnName = txtSortColumn.Text
                objSubscribers.columnSortOrder = txtSortOrder.Text
                objSubscribers.CurrentPage = txtCurrrentPage.Text.Trim()
                objSubscribers.PageSize = Session("PagingRows")

                Dim dt As DataTable = objSubscribers.GetCustomerReport()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    lblTotalIncome.Text = String.Format("$ {0:#,##0.00}", CCommon.ToDouble(dt.Rows(0)("numTotalIncome")))
                Else
                    lblTotalIncome.Text = String.Format("$ {0:#,##0.00", 0)
                End If

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objSubscribers.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                gvSubscribers.DataSource = dt
                gvSubscribers.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub gvSubscribers_RowDataBound(sender As Object, e As GridViewRowEventArgs)
            Try
                If e.Row.RowType = DataControlRowType.Header Then
                    Dim lbOrganization As LinkButton = DirectCast(e.Row.FindControl("lbOrganization"), LinkButton)
                    Dim lbRenewal As LinkButton = DirectCast(e.Row.FindControl("lbRenewal"), LinkButton)
                    Dim lbLastLogin As LinkButton = DirectCast(e.Row.FindControl("lbLastLogin"), LinkButton)
                    Dim lbFullUser As LinkButton = DirectCast(e.Row.FindControl("lbFullUser"), LinkButton)
                    Dim lbUniqueLogins As LinkButton = DirectCast(e.Row.FindControl("lbUniqueLogins"), LinkButton)
                    Dim lbFullUserUsage As LinkButton = DirectCast(e.Row.FindControl("lbFullUserUsage"), LinkButton)
                    Dim lbFullUserCost As LinkButton = DirectCast(e.Row.FindControl("lbFullUserCost"), LinkButton)
                    Dim lbLtdAccessUsers As LinkButton = DirectCast(e.Row.FindControl("lbLtdAccessUsers"), LinkButton)
                    Dim lbLtdAccessUsersUsage As LinkButton = DirectCast(e.Row.FindControl("lbLtdAccessUsersUsage"), LinkButton)
                    Dim lbBizPortalUsers As LinkButton = DirectCast(e.Row.FindControl("lbBizPortalUsers"), LinkButton)
                    Dim lbBizPortalUsersUsage As LinkButton = DirectCast(e.Row.FindControl("lbBizPortalUsersUsage"), LinkButton)
                    Dim lbECommerce As LinkButton = DirectCast(e.Row.FindControl("lbECommerce"), LinkButton)
                    Dim lbTotalBiz As LinkButton = DirectCast(e.Row.FindControl("lbTotalBiz"), LinkButton)
                    Dim lbTotalOrders As LinkButton = DirectCast(e.Row.FindControl("lbTotalOrders"), LinkButton)
                    Dim lbTotalGrossProfit As LinkButton = DirectCast(e.Row.FindControl("lbTotalGrossProfit"), LinkButton)
                    Dim lbTotalBizPercent As LinkButton = DirectCast(e.Row.FindControl("lbTotalBizPercent"), LinkButton)
                    Dim lbSpaceUsed As LinkButton = DirectCast(e.Row.FindControl("lbSpaceUsed"), LinkButton)

                    lbOrganization.Text = lbOrganization.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbRenewal.Text = lbRenewal.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbLastLogin.Text = lbLastLogin.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbFullUser.Text = lbFullUser.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbUniqueLogins.Text = lbUniqueLogins.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbFullUserUsage.Text = lbFullUserUsage.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbFullUserCost.Text = lbFullUserCost.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbLtdAccessUsers.Text = lbLtdAccessUsers.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbLtdAccessUsersUsage.Text = lbLtdAccessUsersUsage.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbBizPortalUsers.Text = lbBizPortalUsers.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbBizPortalUsersUsage.Text = lbBizPortalUsersUsage.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbECommerce.Text = lbECommerce.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbTotalBiz.Text = lbTotalBiz.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbTotalOrders.Text = lbTotalOrders.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbTotalGrossProfit.Text = lbTotalGrossProfit.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbTotalBizPercent.Text = lbTotalBizPercent.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbSpaceUsed.Text = lbSpaceUsed.Text.Replace("&#9660;", "").Replace("&#9650;", "")

                    If Not String.IsNullOrEmpty(txtSortColumn.Text) Then
                        Dim sortArrow As String = IIf(txtSortOrder.Text = "DESC", "&#9660;", "&#9650;")

                        If txtSortColumn.Text = "vcCompanyName" Then
                            lbOrganization.Text = lbOrganization.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "dtSubEndDate" Then
                            lbRenewal.Text = lbRenewal.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "dtLastLoggedIn" Then
                            lbLastLogin.Text = lbLastLogin.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "intSubscribedFullUsers" Then
                            lbFullUser.Text = lbFullUser.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "intFullUsersUniqueLogin" Then
                            lbUniqueLogins.Text = lbUniqueLogins.Text & " " & sortArrow

                        ElseIf txtSortColumn.Text = "intUsedFullUsers" Then
                            lbFullUserUsage.Text = lbFullUserUsage.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "monFullUserCost" Then
                            lbFullUserCost.Text = lbFullUserCost.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "intSubscribedLimitedAccessUsers" Then
                            lbLtdAccessUsers.Text = lbLtdAccessUsers.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "intUsedLimitedAccessUsers" Then
                            lbLtdAccessUsersUsage.Text = lbLtdAccessUsersUsage.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "intSubscribedBusinessPortalUsers" Then
                            lbBizPortalUsers.Text = lbBizPortalUsers.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "intUsedBusinessPortalUsers" Then
                            lbBizPortalUsersUsage.Text = lbBizPortalUsersUsage.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "monSiteCost" Then
                            lbECommerce.Text = lbECommerce.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "monTotalBiz" Then
                            lbTotalBiz.Text = lbTotalBiz.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "numTotalSalesOrder" Then
                            lbTotalOrders.Text = lbTotalOrders.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "monTotalGrossProfit" Then
                            lbTotalGrossProfit.Text = lbTotalGrossProfit.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "numTotalBizPercent" Then
                            lbTotalBizPercent.Text = lbTotalBizPercent.Text & " " & sortArrow
                        ElseIf txtSortColumn.Text = "numTotalSize" Then
                            lbSpaceUsed.Text = lbSpaceUsed.Text & " " & sortArrow
                        End If
                    Else
                        lbTotalBiz.Text = lbTotalBiz.Text & " " & "&#9660;"
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub gvSubscribers_RowCommand(sender As Object, e As GridViewCommandEventArgs)
            Try
                If e.CommandName = "Sort" Then
                    If txtSortColumn.Text <> CCommon.ToString(e.CommandArgument) Then
                        txtSortColumn.Text = CCommon.ToString(e.CommandArgument)
                        txtSortOrder.Text = "ASC"
                    Else
                        If txtSortOrder.Text = "DESC" Then
                            txtSortOrder.Text = "ASC"
                        Else
                            txtSortOrder.Text = "DESC"
                        End If
                    End If

                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub chkPayingCustomer_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlDateFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnGo_Click(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region

        
        
        
    End Class
End Namespace