﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountingDiagnosis.aspx.vb" Inherits=".frmAccountingDiagnosis" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Accounting Diagnosis</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Accounting Diagnosis
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <label>Select Domain:</label>
                <asp:DropDownList ID="ddlDomain" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                <asp:Button ID="btnUpdateDiagnosis" runat="server" CssClass="btn btn-primary" Text="Refresh Diagnosis" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Repeater ID="rptMain" runat="server">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblTitle" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' Style="color: green; font-weight: bold"></asp:Label>
                                    <p>
                                        <asp:Label runat="server" ID="lblDesc" Text='<%# DataBinder.Eval(Container.DataItem, "vcDesc") %>' Style="color: green"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hdnTableName" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "TableName")%>' />
                                    <asp:GridView ID='gvBalance' runat="server" AutoGenerateColumns="true" ClientIDMode="AutoID" Width="100%" CssClass="table table-bordered table-striped">
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server"
    ClientIDMode="Static">
</asp:Content>
