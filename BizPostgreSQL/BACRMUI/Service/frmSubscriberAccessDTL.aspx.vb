Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Service
    Partial Public Class frmSubscriberAccessDTL : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                Dim objSubscribers As New Subscribers
                objSubscribers.SubscriberID = GetQueryStringVal("SubID")
                objSubscribers.DomainID = Session("DomainID")
                objSubscribers.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dgAccessDetails.DataSource = objSubscribers.AccessDetails
                dgAccessDetails.DataBind()

                If Not Page.IsPostBack Then
                    If Session("UserContactID") <> "1" Then
                        Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgAccessDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAccessDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    If e.Item.Cells(1).Text <> "&nbsp;" Then e.Item.Cells(1).Text = FormattedDateTimeFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), CDate(e.Item.Cells(1).Text)), Session("DateFormat"))
                    If e.Item.Cells(2).Text <> "&nbsp;" Then e.Item.Cells(2).Text = FormattedDateTimeFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), CDate(e.Item.Cells(2).Text)), Session("DateFormat"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace