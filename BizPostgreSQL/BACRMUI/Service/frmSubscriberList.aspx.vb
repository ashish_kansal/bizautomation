Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Service
    Partial Public Class frmSubscriberList : Inherits BACRMPage

        Dim strColumn As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If Not IsPostBack Then
                    'If Session("UserContactID") <> "1" Then
                    '    Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                    'End If

                    BindDatagrid()
                End If
                btnAddNewSubscriber.Attributes.Add("onclick", "return AddNewSubscriber()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim dtTable As DataTable
                Dim objSubscribers As New Subscribers
                With objSubscribers
                    .UserCntID = Session("UserContactID")
                    .SortOrder = ddlSort.SelectedItem.Value
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SearchWord = Replace(txtSearch.Text, "'", "''")
                    .DomainID = Session("DomainID")
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                     If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "numSubscriberID"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                End With
                dtTable = objSubscribers.GetSubscribers

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objSubscribers.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                dgSubscribers.DataSource = dtTable
                dgSubscribers.DataBind()

                lblTotalActiveSubscriber.Text = objSubscribers.TotalActiveUsers
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgSubscribers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSubscribers.ItemCommand
            Dim lngSubID As Long
            Try
                If e.CommandName <> "Sort" Then lngSubID = CLng(e.Item.Cells(0).Text)
                If e.CommandName = "Company" Then
                    Response.Redirect("../Service/frmSubscriberDTL.aspx?SubID=" & lngSubID, False)
                ElseIf e.CommandName = "Delete" Then
                    Dim objSubscribers As New Subscribers
                    With objSubscribers
                        .SubscriberID = lngSubID
                    End With
                    If objSubscribers.DeleteSubscriber = True Then
                        BindDatagrid()
                    Else : ShowMessage("Dependent Records Exists.Cannot be deleted.")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgSubscribers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSubscribers.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplEmail As HyperLink
                    hplEmail = e.Item.FindControl("hplEmail")
                    hplEmail.NavigateUrl = "#"
                    If Session("CompWindow") = 1 Then
                        hplEmail.NavigateUrl = "mailto:" & hplEmail.Text
                    Else : hplEmail.Attributes.Add("onclick", "return OpemEmail('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=" & hplEmail.Text & "')")
                    End If

                    Dim lblSize As Label = e.Item.FindControl("lblSize")
                    lblSize.Text = SetBytes(lblSize.Text)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub dgSubscribers_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgSubscribers.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If txtSortColumn.Text <> strColumn Then
                    txtSortColumn.Text = strColumn
                    txtSortOrder.Text = "A"
                Else
                    If txtSortOrder.Text = "D" Then
                        txtSortOrder.Text = "A"
                    Else : txtSortOrder.Text = "D"
                    End If
                End If

                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Function SetBytes(ByVal Bytes) As String
            Try
                If Bytes >= 1073741824 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
                ElseIf Bytes >= 1048576 Then
                    SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
                ElseIf Bytes >= 1024 Then
                    SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
                ElseIf Bytes < 1024 Then
                    SetBytes = Fix(Bytes) & " B"
                End If
                Exit Function
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace
