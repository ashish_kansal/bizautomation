﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmCustomReports
    Inherits BACRMPage
#Region "Member Variables"
    Private objCustomReportQuery As CustomQueryReport
#End Region

#Region "Constructor"
    Sub New()
        Try
            objCustomReportQuery = New CustomQueryReport
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack() Then
                If Session("UserContactID") <> "1" Then
                    Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                End If

                If Session("DomainID") = 1 AndAlso Session("UserContactID") = 1 Then
                    BindCustomReports()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindCustomReports()
        Try
            Dim objCustomQueryReport As New CustomQueryReport
            gvCustomReport.DataSource = objCustomQueryReport.GetAll()
            gvCustomReport.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub gvCustomReport_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvCustomReport.RowCommand
        Try
            If e.CommandName = "DeleteReport" Then
                objCustomReportQuery.ReportID = CCommon.ToLong(e.CommandArgument)
                objCustomReportQuery.Delete()
                BindCustomReports()
            ElseIf e.CommandName = "" Then

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

End Class