<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubscriberAccessDTL.aspx.vb" Inherits="BACRM.UserInterface.Service.frmSubscriberAccessDTL" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Access Details
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgAccessDetails" AllowSorting="true" runat="server" Width="100%" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="User"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtLoggedInTime" SortExpression="dtLoggedInTime" HeaderText="Logged In"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtLoggedOutTime" SortExpression="dtLoggedOutTime" HeaderText="Logged out"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Total" SortExpression="Total" HeaderText="Total"></asp:BoundColumn>
                        <asp:BoundColumn DataField="bitPortal" SortExpression="bitPortal" HeaderText="Portal"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>



