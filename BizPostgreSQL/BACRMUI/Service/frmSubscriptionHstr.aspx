<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubscriptionHstr.aspx.vb"
    Inherits="BACRM.UserInterface.Service.frmSubscriptionHstr" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Subscription History</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="60" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Subscription History
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="700px">
        <tr>
            <td>
                <asp:DataGrid ID="dgSubscribers" AllowSorting="true" runat="server" Width="100%"
                    CssClass="dg" AutoGenerateColumns="False" BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numSubscriberID"></asp:BoundColumn>
                        <asp:BoundColumn DataField="intNoofSubscribers" SortExpression="intNoofSubscribers"
                            HeaderText="Licensed Subscribers"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtSubStartDate" SortExpression="dtSubStartDate" HeaderText="Subscription Start Date">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dtSubRenewDate" SortExpression="dtSubRenewDate" HeaderText="Subscription Renew Date">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Trial" SortExpression="bitTrial" HeaderText="Trial">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Status" SortExpression="bitStatus" HeaderText="Status">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dtSuspendedDate" SortExpression="dtSuspendedDate" HeaderText="Suspended Date">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcSuspendedReason" SortExpression="vcSuspendedReason"
                            HeaderText="Suspended Reason"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
