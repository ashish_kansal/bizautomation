<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubUsageReport.aspx.vb"
    Inherits="BACRM.UserInterface.Service.frmSubUsageReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Usage Report</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="pull-left">
                <a href="frmSubscriberList.aspx">Subscriber List</a>
            </div>
            <div class="pull-right">
                <b>Number of unique users that have logged in within selected date range:</b>
                <asp:Label ID="lblNoOfUsers" runat="server" CssClass="text"></asp:Label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Filter by Company:</label>
                <asp:TextBox ID="txtFilter" runat="server" Width="128" CssClass="form-control"></asp:TextBox>
                <asp:Button ID="btnGo" Text="Go" runat="server" CssClass="btn btn-md btn-primary" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline pull-right">
                <label>Date Filter:</label>
                <asp:DropDownList ID="ddlDateFilter" AutoPostBack="true" runat="server" CssClass="form-control" Width="150">
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="1" Selected="True">Within last 7 days</asp:ListItem>
                    <asp:ListItem Value="2">Within last 14 days</asp:ListItem>
                    <asp:ListItem Value="3">Within last 30 days</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Usage Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="dgUsageReport" AllowSorting="true" OnSorting="dgUsageReport_Sorting" OnRowCreated="dgUsageReport_RowCreated" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundField Visible="False" DataField="numSubscriberID"></asp:BoundField>
                        <asp:HyperLinkField DataTextField="vcCompanyName" Target="_blank" DataNavigateUrlFields="numSubscriberID"
                            DataNavigateUrlFormatString='~/Service/frmSubscriberAccessDTL.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SubID={0}'
                            SortExpression="vcCompanyName" HeaderText="Organization Name"></asp:HyperLinkField>
                        <asp:BoundField DataField="dtSubStartDate" SortExpression="dtSubStartDate" HeaderText="Subscription Start Date"></asp:BoundField>
                        <asp:BoundField DataField="dtSubEndDate" SortExpression="dtSubEndDate" HeaderText="Subscription Renew Date"></asp:BoundField>
                        <asp:BoundField DataField="bitTrial" SortExpression="bitTrial" HeaderText="Trial"></asp:BoundField>
                        <asp:BoundField DataField="Name" SortExpression="vcFirstName" HeaderText="Administrator"></asp:BoundField>
                        <asp:TemplateField HeaderText="Last Log-in Date & Time"
                            SortExpression="dtLastLoggedIn">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "dtLastLoggedIn"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TotalCount" SortExpression="TotalCount" HeaderText="Times logged in within selected date range"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
