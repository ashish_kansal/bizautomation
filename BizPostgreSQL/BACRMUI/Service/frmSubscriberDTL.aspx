 <%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubscriberDTL.aspx.vb"
    Inherits="BACRM.UserInterface.Service.frmSubscriberDTL" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Subscriber Details</title>
    <script type="text/javascript">
        function Save() {
            if ($('[id$=calSubStartDate_txtDate]').val() == '') {
                alert("Select Start date")
                $('[id$=calSubStartDate_txtDate]').focus()
                return false;
            }
            if ($('[id$=calSubRenewDate_txtDate]').val() == '') {
                alert("Select Renew date")
                $('[id$=calSubRenewDate_txtDate]').focus()
                return false;
            }
        }
        function OpenHstr(a) {
            window.open('../Service/frmSubscriptionHstr.aspx?SubID=' + a, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenBranches(a, b) {
            window.open('../MultiComp/SubScriberDomain.aspx?SubID=' + a + '&SubName=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-8">
            <div class="form-inline">
                Copy Configuration from this domain
                <asp:DropDownList ID="ddlSourceDomain" Width="200px" runat="server" CssClass="form-control" AutoPostBack="True">
                </asp:DropDownList>
                &nbsp;
                Source Group
                <asp:DropDownList ID="ddlSourceGroup" runat="server" CssClass="form-control">
                </asp:DropDownList>
                &nbsp;
                Destination Group
                <asp:DropDownList ID="ddlDestinationGroup" runat="server" CssClass="form-control">
                </asp:DropDownList>
                &nbsp;
                <telerik:Radcombobox id="rcbtaskToPerform" runat="server" checkboxes="true">
                    <Items>
                        <telerik:RadComboBoxItem Value="1" Text="Dropdown List" />
                        <telerik:RadComboBoxItem Value="2" Text="Biz Form Wizard" />
                        <telerik:RadComboBoxItem Value="3" Text="Global Setting" />
                    </Items>
                </telerik:Radcombobox>
                &nbsp;
                <asp:Button ID="btnExcute" runat="server" CssClass="btn btn-primary" Text="Excute" />
                <br />
                <br />
                &nbsp;
                Source Site
                  <asp:DropDownList ID="ddlSourceDomainSite" runat="server" CssClass="form-control">
                </asp:DropDownList>
                &nbsp;
                Destination Site
                <asp:DropDownList ID="ddlDestinationDomainSite" runat="server" CssClass="form-control">
                </asp:DropDownList>
                <asp:Button ID="btnExecuteEcommerce" runat="server" CssClass="btn btn-primary" Text="Copy Ecommerce" />
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="pull-right">
                <asp:Button ID="btnCreateSub" runat="server" CssClass="btn btn-primary" Text="Create New Subscription" />
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Subscriber Detail
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12 col-sm-8">
            <div class="col-xs-12 col-sm-6" style="padding: 0px">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-6 text-right" style="line-height: 34px;">Company Name</label>
                        <div class="col-xs-6" style="line-height: 34px;">
                            <asp:Label ID="lblCompany" runat="server" CssClass="text"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-inline">
                        <label class="col-xs-6 text-right" style="line-height: 34px;">Paying Customer</label>
                        <div class="col-xs-6" style="line-height: 34px;">
                            <asp:CheckBox CssClass="checkbox" ID="chkPayingCustomer" runat="server" />
                        </div>
                    </div>
                </div>
                 <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-7 text-right" style="line-height: 34px;">Allow to Edit Help File</label>
                        <div class="col-xs-5">
                            <asp:CheckBox CssClass="checkbox" ID="chkAllowToEditHelpFile" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-6 text-right" style="line-height: 34px;">Administrator</label>
                        <div class="col-xs-6">
                            <asp:DropDownList ID="ddlContact" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-6 text-right" style="line-height: 34px;">Subscription Start Date</label>
                        <div class="col-xs-6">
                            <BizCalendar:Calendar ID="calSubStartDate" runat="server" ClientIDMode="AutoID" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-6 text-right" style="line-height: 34px;">Subscription Renew Date</label>
                        <div class="col-xs-6">
                            <BizCalendar:Calendar ID="calSubRenewDate" runat="server" ClientIDMode="AutoID" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6" style="padding: 0px">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-7 text-right" style="line-height: 34px;">Status</label>
                        <div class="col-xs-5">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">Suspended</asp:ListItem>
                                <asp:ListItem Value="2">Trial</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-inline">
                        <label class="col-xs-7 text-right" style="line-height: 34px;">Enable Accounting Audit</label>
                        <div class="col-xs-5" style="line-height: 34px;">
                            <asp:CheckBox ID="chkEnableAccountingAudit" runat="server" CssClass="checkbox" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-inline">
                        <label class="col-xs-7 text-right" style="line-height: 34px;">Notify of Balance Mismatch</label>
                        <div class="col-xs-5" style="line-height: 34px;">
                            <asp:CheckBox ID="chkNotifyAdmin" runat="server" CssClass="checkbox" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-7 text-right" style="line-height: 34px;">Suspended Date</label>
                        <div class="col-xs-5" style="line-height: 34px;">
                            <asp:Label ID="lblSupendedDate" runat="server" CssClass="text"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-7 text-right" style="line-height: 34px;">Full-User Login Concurrency</label>
                        <div class="col-xs-5">
                            <asp:TextBox ID="txtUserConcurrency" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" style="padding: 0px">
                <div class="form-group">
                    <label class="col-xs-3 text-right" style="width: 24%">Notes</label>
                    <div class="col-xs-9" style="width: 74.3%">
                        <asp:TextBox ID="txtSusReason" runat="server" CssClass="form-control" TextMode="multiLine" Rows="3"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <table class="table table-bordered">
                <tr>
                    <th>License Type</th>
                    <th>No. of User</th>
                    <th>Price / User / mo.</th>
                    <th>($ Price / mo.)</th>
                </tr>
                <tr>
                    <td class="text-right">Full Users</td>
                    <td>
                        <asp:TextBox ID="txtFullUser" runat="server" CssClass="form-control" Width="60"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtFullUserCost" Text="79.95" CssClass="form-control" Width="80"></asp:TextBox>
                    </td>
                    <td>$
                        <asp:Label runat="server" ID="lblFullUserTotalCost"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="text-right" style="white-space: nowrap">Limited Access Users</td>
                    <td>
                        <asp:TextBox ID="txtLAUser" runat="server" CssClass="form-control" Width="60"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtLAUserCost" Text="24.95" CssClass="form-control" Width="80"></asp:TextBox>
                    </td>
                    <td>$
                        <asp:Label runat="server" ID="lblLAUserTotalCost"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="text-right" style="white-space: nowrap">Business Portal Users</td>
                    <td>
                        <asp:TextBox ID="txtBPUser" runat="server" CssClass="form-control" Width="60"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtBPUserCost" Text="9.95" CssClass="form-control" Width="80"></asp:TextBox>
                    </td>
                    <td>$
                        <asp:Label runat="server" ID="lblBAUserTotalCost"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">E-Commerce Sites</td>
                    <td>
                        <asp:TextBox ID="txtSite" runat="server" CssClass="form-control" Width="60" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSiteCost" Text="0" CssClass="form-control" Width="80"></asp:TextBox>
                    </td>
                    <td>$
                        <asp:Label runat="server" ID="lblSiteTotalCost"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%-- <div class="form-group">
        <label>Multiple User Login</label>
        <asp:RadioButtonList ID="rblLogin" runat="server" RepeatDirection="Horizontal" CssClass="signup">
            <asp:ListItem Text="No" Value="1" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Yes" Value="0"></asp:ListItem>
        </asp:RadioButtonList>
    </div>--%>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgUsers" AllowSorting="true" runat="server" Width="100%" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numContactID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numUserID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="vcPassword"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="EmailID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="bitActivateFlag"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Name" HeaderText="User"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="User Email">
                            <ItemTemplate>
                                <asp:TextBox ID="txtEmail" runat="server" Width="200" CssClass="signup"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Password">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPassword" runat="server" Width="200" MaxLength="10" CssClass="signup"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcGroupName" SortExpression="vcGroupName" HeaderText="Group" HeaderStyle-Width="195" ItemStyle-Width="195"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="195" ItemStyle-Width="195">
                            <HeaderTemplate>
                                <b>License Type</b>
                                <asp:DropDownList ID="ddlLicenseType" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLicenseType_SelectedIndexChanged">
                                    <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Full Users"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="Limited Access Users"></asp:ListItem>
                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcGroupType")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnGroupType" Value="0" />
    <asp:TextBox ID="txtTarDomain" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:HyperLink ID="hplOpenHstr" runat="server" CssClass="btn btn-xs btn-primary">Open Subscription History</asp:HyperLink>
    <asp:HyperLink ID="hplBranches" runat="server" CssClass="btn btn-xs btn-primary"></asp:HyperLink>
</asp:Content>
