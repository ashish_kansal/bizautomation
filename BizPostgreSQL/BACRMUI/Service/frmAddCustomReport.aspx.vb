﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmAddCustomReport
    Inherits BACRMPage
#Region "Memeber Variables"
    Private objCommmon As New CCommon
    Private objCustomQueryReport As CustomQueryReport
#End Region

#Region "Constructor"
    Sub New()
        Try
            objCommmon = New CCommon
            objCustomQueryReport = New CustomQueryReport
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""

            If Not Page.IsPostBack Then
                BindDomainNames()

                If CCommon.ToLong(GetQueryStringVal("numReportID")) > 0 Then
                    hdnReportID.Value = CCommon.ToLong(GetQueryStringVal("numReportID"))
                    BindReportData()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If CCommon.ToLong(ddlDomain.SelectedValue) > 0 _
                AndAlso Not String.IsNullOrEmpty(txtReportName.Text.Trim()) _
                AndAlso Not String.IsNullOrEmpty(txtQuery.Text.Trim()) Then
                objCustomQueryReport.ReportID = CCommon.ToLong(hdnReportID.Value)
                objCustomQueryReport.ReportDomainID = CCommon.ToLong(ddlDomain.SelectedValue)
                objCustomQueryReport.ReportName = txtReportName.Text.Trim()
                objCustomQueryReport.ReportDescription = txtReportDescription.Text.Trim()
                objCustomQueryReport.EmailTo = txtEmailTo.Text.Trim()

                Dim tempEmailFrequency As Int16
                If rdbDaily.Checked Then
                    tempEmailFrequency = 1
                ElseIf rdbWeekly.Checked Then
                    tempEmailFrequency = 2
                ElseIf rdbMonthly.Checked Then
                    tempEmailFrequency = 3
                ElseIf rdbYearly.Checked Then
                    tempEmailFrequency = 4
                End If
                objCustomQueryReport.EmailFrequency = tempEmailFrequency
                objCustomQueryReport.Query = txtQuery.Text.Trim()
                objCustomQueryReport.CSS = txtCSS.Text.Trim()

                objCustomQueryReport.Save()

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseWindow", "CloseAndRefresh()", True)
            Else
                lblException.Text = "Required fields detail is not provided."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindDomainNames()
        Try
            ddlDomain.DataSource = objCommmon.GetDomainIDName()
            ddlDomain.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindReportData()
        Try
            objCustomQueryReport.ReportID = CCommon.ToLong(hdnReportID.Value)
            Dim objNewCustomQueryReport As CustomQueryReport = objCustomQueryReport.GetByID()

            If objCustomQueryReport.ReportID > 0 Then
                ddlDomain.SelectedValue = objNewCustomQueryReport.ReportDomainID
                txtReportName.Text = objNewCustomQueryReport.ReportName
                txtReportDescription.Text = objNewCustomQueryReport.ReportDescription
                txtEmailTo.Text = objNewCustomQueryReport.EmailTo

                Select Case objNewCustomQueryReport.EmailFrequency
                    Case 1
                        rdbDaily.Checked = True
                    Case 2
                        rdbWeekly.Checked = True
                    Case 3
                        rdbMonthly.Checked = True
                    Case 4
                        rdbYearly.Checked = True
                End Select

                txtQuery.Text = objNewCustomQueryReport.Query
                txtCSS.Text = objNewCustomQueryReport.CSS
            Else
                lblException.Text = "Report doesn't exists."
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

End Class