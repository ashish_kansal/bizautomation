<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddNewSubscriber.aspx.vb"
    Inherits="BACRM.UserInterface.Service.frmAddNewSubscriber" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add Subscribers</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <%--<asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="60" />--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add Subscriber
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="700px" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table>
                    <tr>
                        <td width="120">
                        </td>
                        <td class="normal1" align="right">
                            Company Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="signup" Width="150"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnGo" runat="server" CssClass="button" Text="Go" />
                        </td>
                    </tr>
                </table>
                <asp:DataGrid ID="dgCompanies" AllowSorting="true" runat="server" Width="100%" CssClass="dg"
                   AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numDivisionID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numContactID"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcCompanyName" SortExpression="vcCompanyName" HeaderText="Company">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Employees" SortExpression="Employees" HeaderText="Employees">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcWebsite" SortExpression="vcWebsite" HeaderText="WebSite">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Primary Contact">
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="vcEmail" HeaderText="Email">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplEmail" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnAdd" CommandName="Add" runat="server" CssClass="button" Text="Add"
                                    Width="60" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
