﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMaster.Master" CodeBehind="frmSubscriberReport.aspx.vb" Inherits="BACRM.UserInterface.Service.frmSubscriberReport" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function AddNewSubscriber() {
            window.open('../Service/frmAddNewSubscriber.aspx', '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-3">
            <asp:CheckBox ID="chkPayingCustomer" runat="server" Text="Paying Customers Only" Checked="true" AutoPostBack="true" OnCheckedChanged="chkPayingCustomer_CheckedChanged" />
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-inline">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                <b>Filter by</b>
                <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged">
                    <asp:ListItem Value="1" Selected="true">Company Name</asp:ListItem>
                    <asp:ListItem Value="2">Email</asp:ListItem>
                    <asp:ListItem Value="3">Trial</asp:ListItem>
                    <asp:ListItem Value="4">Active</asp:ListItem>
                    <asp:ListItem Value="5">Suspended</asp:ListItem>
                    <asp:ListItem Value="6">Suspended < 30 days</asp:ListItem>
                    <asp:ListItem Value="7">Suspended > 30 days</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnGo" CssClass="btn btn-primary" Text="Go" runat="server" OnClick="btnGo_Click"></asp:Button>
                <asp:Button ID="btnAddNewSubscriber" runat="Server" Text="Add New Subscriber" CssClass="btn btn-primary" OnClientClick="return AddNewSubscriber();" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-inline pull-right">
                <label>Date Filter</label>
                <asp:DropDownList ID="ddlDateFilter" AutoPostBack="true" runat="server" CssClass="form-control" Width="150" OnSelectedIndexChanged="ddlDateFilter_SelectedIndexChanged">
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="1">Within last 7 days</asp:ListItem>
                    <asp:ListItem Value="2">Within last 14 days</asp:ListItem>
                    <asp:ListItem Value="3">Within last 30 days</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Customer Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MultiSelectFunctionCluster" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10" >
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Label ID="lblTotalIncome" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView runat="server" ID="gvSubscribers" AutoGenerateColumns="false" UseAccessibleHeader="true" CssClass="table table-bordered table-striped" OnRowCommand="gvSubscribers_RowCommand" OnRowDataBound="gvSubscribers_RowDataBound">
                    <Columns>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbOrganization" runat="server" CommandName="Sort" Text="Organization" CommandArgument="vcCompanyName"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcCompanyNameURL")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbRenewal" runat="server" CommandName="Sort" Text="Renewal" CommandArgument="dtSubEndDate"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcSubEndDate")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbLastLogin" runat="server" CommandName="Sort" Text="Last Login" CommandArgument="dtLastLoggedIn"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcLastLoggedIn")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbFullUser" runat="server" CommandName="Sort" Text="Full Users" CommandArgument="intSubscribedFullUsers"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("intSubscribedFullUsers")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbUniqueLogins" runat="server" CommandName="Sort" Text="Unique-Logins " CommandArgument="intFullUsersUniqueLogin "></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" Text='<%# Eval("intFullUsersUniqueLogin")%>'></asp:HyperLink>
                                (<asp:HyperLink runat="server" Text='<%# Eval("intFullUsersAllLogin")%>'></asp:HyperLink>)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbFullUserUsage" runat="server" CommandName="Sort" Text="Usage" CommandArgument="intUsedFullUsers"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("intUsedFullUsers")%>%
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbFullUserCost" runat="server" CommandName="Sort" Text="$/U/Mo" CommandArgument="monFullUserCost"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("$ {0:#,##0.00}", Eval("monFullUserCost"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbLtdAccessUsers" runat="server" CommandName="Sort" Text="Ltd Access Users" CommandArgument="intSubscribedLimitedAccessUsers"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("intSubscribedLimitedAccessUsers")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbLtdAccessUsersUsage" runat="server" CommandName="Sort" Text="Usage" CommandArgument="intUsedLimitedAccessUsers"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("intUsedLimitedAccessUsers")%>%
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbBizPortalUsers" runat="server" CommandName="Sort" Text="Biz Portal-Users " CommandArgument="intSubscribedBusinessPortalUsers"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("intSubscribedBusinessPortalUsers")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbBizPortalUsersUsage" runat="server" CommandName="Sort" Text="Usage" CommandArgument="intUsedBusinessPortalUsers"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("intUsedBusinessPortalUsers")%>%
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbECommerce" runat="server" CommandName="Sort" Text="E-Commerce" CommandArgument="monSiteCost"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("$ {0:#,##0.00}", Eval("monSiteCost"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbTotalBiz" runat="server" CommandName="Sort" Text="Tot Biz$/Mo" CommandArgument="monTotalBiz"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("$ {0:#,##0.00}", Eval("monTotalBiz"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbTotalOrders" runat="server" CommandName="Sort" Text="Cust SOs/Mo" CommandArgument="numTotalSalesOrder"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("numTotalSalesOrder")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbTotalGrossProfit" runat="server" CommandName="Sort" Text="$GP/Mo" CommandArgument="monTotalGrossProfit"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# String.Format("$ {0:#,##0.00}", Eval("monTotalGrossProfit"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbTotalBizPercent" runat="server" CommandName="Sort" Text="Biz%" CommandArgument="numTotalBizPercent"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("numTotalBizPercent")%>%
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:LinkButton ID="lbSpaceUsed" runat="server" CommandName="Sort" Text="Space Used" CommandArgument="numTotalSize"></asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("numTotalSize")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
</asp:Content>
