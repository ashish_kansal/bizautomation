<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubscriberList.aspx.vb"
    Inherits="BACRM.UserInterface.Service.frmSubscriberList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Subscribers</title>
    <script type="text/javascript">
        function reDirectPage(url) {
            document.location.href = url
        }
        function OpemEmail(URL) {
            window.open(URL, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accounts";
            return false;
        }
        function DeleteRecord() {
            if (confirm('Deleting Subscriber would not delete Its data, if you are planning to delete (subscription + data) contact chintan, countinue with deleting subscription?')) {
                if (confirm('Are you sure, you want to delete the selected record?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else
                return false;
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function AddNewSubscriber() {
            window.open('../Service/frmAddNewSubscriber.aspx', '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label><a href="frmSubUsageReport.aspx">Total Active Users</a>:</label>
                <asp:Label Text="" runat="server" ID="lblTotalActiveSubscriber" ForeColor="Green" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Target:</label>
                <span style="color: red">500</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-inline pull-right">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                <b>Filter by:</b>
                <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control">
                    <asp:ListItem Value="1" Selected="true">Company Name</asp:ListItem>
                    <asp:ListItem Value="2">Email</asp:ListItem>
                    <asp:ListItem Value="3">Trial</asp:ListItem>
                    <asp:ListItem Value="4">Active</asp:ListItem>
                    <asp:ListItem Value="5">Suspended</asp:ListItem>
                    <asp:ListItem Value="6">Suspended < 30 days</asp:ListItem>
                    <asp:ListItem Value="7">Suspended > 30 days</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnGo" CssClass="btn btn-primary" Text="Go" runat="server"></asp:Button>
                <asp:Button ID="btnAddNewSubscriber" runat="Server" Text="Add New Subscriber" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Subscribers
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgSubscribers" AllowSorting="true" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numSubscriberID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcCompanyName" SortExpression="vcCompanyName" HeaderText="Organization Name" CommandName="Company"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="intNoofUsersSubscribed" SortExpression="intNoofUsersSubscribed" HeaderText="Licensed Subscribers"></asp:BoundColumn>
                        <asp:BoundColumn DataField="intNoOfPartners" SortExpression="intNoOfPartners" HeaderText="No of Partner Licence"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtSubStartDate" SortExpression="dtSubStartDate" HeaderText="Subscription Start Date"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtSubEndDate" SortExpression="dtSubEndDate" HeaderText="Subscription Renew Date"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Trial" SortExpression="bitTrial" HeaderText="Trial"></asp:BoundColumn>
                        <asp:BoundColumn DataField="AdminContact" SortExpression="vcFirstName" HeaderText="Administrator"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="vcEmail" HeaderText="Email">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplEmail" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Status" SortExpression="bitActive" HeaderText="Status"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtSuspendedDate" SortExpression="dtSuspendedDate" HeaderText="Suspended Date"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcSuspendedReason" SortExpression="vcSuspendedReason"
                            HeaderText="Suspended Reason"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="" HeaderText="Space Used">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSize" Font-Size="X-Small" ForeColor="Green" Text='<%# DataBinder.Eval(Container.DataItem, "TotalSize") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" Style="font-family: fontawesome" Text="&#xf1f8;" CommandName="Delete" OnClientClick="return DeleteRecord();"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server"
    ClientIDMode="Static">
</asp:Content>
