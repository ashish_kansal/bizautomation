Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Service
    Partial Public Class frmSubscriptionHstr : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    Dim objSubscribers As New Subscribers
                    objSubscribers.SubscriberID = GetQueryStringVal("SubID")
                    dgSubscribers.DataSource = objSubscribers.GetSubscriptionHistory
                    dgSubscribers.DataBind()

                End If
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace