﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Service
    Partial Public Class frmSubUsageReport : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    If Session("UserContactID") <> "1" Then
                        Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                    End If

                    Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                    If Not tdGridConfiguration Is Nothing Then
                        tdGridConfiguration.Visible = False
                    End If

                    ViewState("SortExpression") = "vcCompanyName"
                    ViewState("SortDirection") = "ASC"

                    LoadDetails()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadDetails()
            Try
                Dim objSubscribers As New Subscribers
                Dim dtTable As DataTable
                objSubscribers.DomainID = Session("DomainID")
                objSubscribers.SearchWord = txtFilter.Text.Trim
                objSubscribers.SortOrder = ddlDateFilter.SelectedValue
                objSubscribers.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objSubscribers.columnName = CCommon.ToString(ViewState("SortExpression"))
                objSubscribers.columnSortOrder = CCommon.ToString(ViewState("SortDirection"))
                dtTable = objSubscribers.UsageReport
                dgUsageReport.DataSource = dtTable
                dgUsageReport.DataBind()
                lblNoOfUsers.Text = dtTable.Rows.Count
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetSortDirection(ByVal column As String) As String
            ' By default, set the sort direction to ascending.
            Dim sortDirection = "ASC"

            ' Retrieve the last column that was sorted.
            Dim sortExpression = TryCast(ViewState("SortExpression"), String)

            If sortExpression IsNot Nothing Then
                ' Check if the same column is being sorted.
                ' Otherwise, the default value can be returned.
                If sortExpression = column Then
                    Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                    If lastDirection IsNot Nothing _
                      AndAlso lastDirection = "ASC" Then
                        sortDirection = "DESC"
                    End If
                End If
            End If

            ' Save new values in ViewState.
            ViewState("SortDirection") = sortDirection
            ViewState("SortExpression") = column
            'PersistData()
            Return sortDirection

        End Function

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                LoadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlDateFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDateFilter.SelectedIndexChanged
            Try
                LoadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Function ReturnDateTime(ByVal dt)
            Try
                Dim strDate As String = ""
                If Not IsDBNull(dt) Then
                    strDate = FormattedDateTimeFromDate(dt, Session("DateFormat"))
                End If
                Return strDate
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub dgUsageReport_Sorting(sender As Object, e As GridViewSortEventArgs)
            Try
                If CCommon.ToString(ViewState("SortExpression")) = e.SortExpression Then
                    If CCommon.ToString(ViewState("SortDirection")) = "ASC" Then
                        ViewState("SortDirection") = "DESC"
                    Else
                        ViewState("SortDirection") = "ASC"
                    End If
                Else
                    ViewState("SortExpression") = e.SortExpression
                    ViewState("SortDirection") = "ASC"
                End If

                LoadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub dgUsageReport_RowCreated(sender As Object, e As GridViewRowEventArgs)
            Try
                If e.Row.RowType = DataControlRowType.Header Then
                    For Each tc As TableCell In e.Row.Cells

                        If tc.HasControls Then
                            Dim lnk As LinkButton = CType(tc.Controls(0), LinkButton)

                            If lnk IsNot Nothing AndAlso CCommon.ToString(ViewState("SortExpression")) = lnk.CommandArgument Then
                                If CCommon.ToString(ViewState("SortDirection")) = "DESC" Then
                                    tc.Controls.Add(New LiteralControl(" <i class='fa fa-caret-down'></i>"))
                                Else
                                    tc.Controls.Add(New LiteralControl(" <i class='fa fa-caret-up'></i>"))
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace