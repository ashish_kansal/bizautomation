﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.IO
Imports BACRM.BusinessLogic.Common

Public Class frmReadAPILogs
    Inherits BACRMPage

    Dim LogFilePath As String = ConfigurationManager.AppSettings("APILogFilePath")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If GetQueryStringVal("Log") <> "" Then
                binddata(CCommon.ToString(GetQueryStringVal("Log")))
            Else
                binddata()

            End If
        End If
    End Sub

    Sub binddata(Optional ByVal LogFiles As String = "")
        hdnFldLogsType.Value = LogFiles
        dtFileDetails = GetLogFiles(LogFiles)
        GridView1.DataSource = dtFileDetails
        GridView1.DataBind()

    End Sub

    'Private objLogFiles As New CreateLogFiles()
    Private dtFileDetails As New DataTable()
    Dim dtLogDetails As New DataTable
    Public Function GetLogFiles(ByVal LogFiles As String) As DataTable
        Dim dr As DataRow
        Dim DomainID As Long = CCommon.ToLong(Session("DomainID"))
        Dim LogPath As String
        Dim TrashPath As String = ""

        If LogFiles = "TransactionLogs" Then
            LogPath = CCommon.GetDocumentPhysicalPath(DomainID) & "TransactionLogs\"
            TrashPath = CCommon.GetDocumentPhysicalPath(DomainID) & "TRASH_TransLogs\"
        ElseIf LogFiles = "MarketplaceAPILog" Then
            LogPath = CCommon.GetDocumentPhysicalPath(DomainID) & "MarketplaceAPILog\"
            TrashPath = CCommon.GetDocumentPhysicalPath(DomainID) & "TRASH_APILogs\"
        Else

            LogPath = LogFilePath & DomainID.ToString("00000") & "\"
            TrashPath = LogFilePath & DomainID.ToString("00000") & "\TRASH\"
        End If
        If Not Directory.Exists(TrashPath) Then
            Directory.CreateDirectory(TrashPath)
        End If
        hdnFldTrashDirectory.Value = TrashPath

        If dtLogDetails.Columns.Count = 0 Then
            DefineLogDetails()
        End If

        Dim i As Integer = 0
        If Directory.Exists(LogPath) Then
            hdnFldDirectory.Value = LogPath
            Dim files As FileInfo() = New DirectoryInfo(LogPath).GetFiles("*").OrderByDescending(Function(f) f.CreationTime).ToArray()

            For Each fileinfo As FileInfo In files
                If dtLogDetails.Columns.Count = 0 Then
                    DefineLogDetails()
                End If
                dr = dtLogDetails.NewRow()
                dr("S.No") = System.Threading.Interlocked.Increment(i)
                dr("Name") = fileinfo.Name
                dr("FullName") = fileinfo.FullName
                dr("Size") = fileinfo.Length.ToString() + " Bytes"
                dr("CreateTime") = fileinfo.CreationTime
                dr("LastModified") = fileinfo.LastWriteTime
                dr("LastAccessTime") = fileinfo.LastAccessTime
                dr("FileExtention") = fileinfo.Extension
                dtLogDetails.Rows.Add(dr)
            Next
        End If
        Return dtLogDetails
    End Function

    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim DomainID As Integer = Convert.ToInt32(Session("DomainID"))

            Dim FilePath As String = hdnFldDirectory.value & GridView1.Rows(index).Cells(2).Text.ToString()
            Dim TrashPath1 As String = hdnFldTrashDirectory.Value & GridView1.Rows(index).Cells(2).Text.ToString()
           
            If e.CommandName.Equals("Select") Then
                Dim fs As New StreamReader(FilePath)
                Dim FileContent As String = fs.ReadToEnd()
                fs.Close()
                txtFileContent.Text = FileContent
            End If
            If e.CommandName.Equals("Delete") Then
                File.Copy(FilePath, TrashPath1)
                File.Delete(FilePath)
                binddata(CCommon.ToString(hdnFldLogsType.Value))
            End If
        Catch ex As Exception

            txtFileContent.Text += ex.Message
            txtFileContent.Text += ex.StackTrace

        End Try

    End Sub

    Public Sub DefineLogDetails()
        If dtLogDetails.Columns.Count > 0 Then
            dtLogDetails.Columns.Clear()
        End If
        dtLogDetails.Columns.Add("S.No")
        dtLogDetails.Columns.Add("Name")
        dtLogDetails.Columns.Add("FullName")
        dtLogDetails.Columns.Add("Size")
        dtLogDetails.Columns.Add("CreateTime")
        dtLogDetails.Columns.Add("LastModified")
        dtLogDetails.Columns.Add("LastAccessTime")
        dtLogDetails.Columns.Add("FileExtention")

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="FilePath">FilePath</param>
    Private Sub ReadFile(FilePath As String)
        Try
            Dim fs As New StreamReader(FilePath)
            Dim FileContent As String = fs.ReadToEnd()
            txtFileContent.Text = FileContent

        Catch ex As Exception
            txtFileContent.Text += ex.Message
            txtFileContent.Text += ex.StackTrace

        End Try

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowIndex >= 0 Then
                Dim row As GridViewRow = DirectCast(e.Row, GridViewRow)
                Dim selectCell As TableCell = row.Cells(0)
                If selectCell.Controls.Count > 0 Then
                    Dim selectControl As LinkButton = TryCast(selectCell.Controls(2), LinkButton)
                    If selectControl IsNot Nothing Then
                        selectControl.Text = "ReadFile"
                    End If
                End If
            End If
        Catch ex As Exception
            txtFileContent.Text += ex.Message
            txtFileContent.Text += ex.StackTrace
        End Try
    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs)
        DeleteLogFiles(CCommon.ToLong(Session("DomainID")), 5)

    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="DomainId"></param>
    ''' <param name="DaysOld"></param>
    Public Sub DeleteLogFiles(DomainId As Long, DaysOld As Integer)
        Dim LogPath As String = LogFilePath & DomainId.ToString("00000") & "\"
        Dim DestPath__1 As String = LogFilePath & DomainId.ToString("00000") & "_Trash" & "\"
        Dim filedate As String = ""
        If Directory.Exists(LogPath) Then
            Dim fileEntries As String() = Directory.GetFiles(LogPath, "*", SearchOption.AllDirectories)

            If Not Directory.Exists(DestPath__1) Then
                Directory.CreateDirectory(DestPath__1)
            End If

            For Each filePath As String In fileEntries
                Dim fileInfo As New FileInfo(filePath)
                Dim destPath__2 As String = DestPath__1 & Path.GetFileName(filePath)
                'TimeSpan ts = fileInfo.CreationTime.Subtract(DateTime.Now);
                Dim ts As TimeSpan = DateTime.Now.Subtract(fileInfo.CreationTime)

                If ts.Days >= DaysOld Then
                    File.Copy(filePath, destPath__2)
                    File.Delete(filePath)
                End If

            Next

        End If

    End Sub

    Private Sub GridView1_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        'Dim DomainID As Integer = Convert.ToInt32(Session("DomainID"))
        'Dim DestPath_1 As String = LogFilePath & DomainID.ToString("00000") & "_Trash" & "\"
        'File.Copy(e.Keys("Name").ToString(), (DestPath_1 & e.Keys("Name").ToString()))
        'File.Delete(LogFilePath + e.Keys("Name").ToString())
        'binddata()

    End Sub

End Class