Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Namespace BACRM.UserInterface.Service
    Partial Public Class frmAddNewSubscriber : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Session("UserContactID") <> "1" Then
                    Response.Redirect("~/admin/authenticationpopup.aspx?mesg=AC")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadCompany()
            Try
                Dim objSubscribers As New Subscribers
                objSubscribers.DomainID = Session("DomainID")
                objSubscribers.SearchWord = txtCompany.Text.Trim
                dgCompanies.DataSource = objSubscribers.GetCompaniesForSubscribing
                dgCompanies.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                LoadCompany()
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgCompanies_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCompanies.ItemCommand
            Try
                If e.CommandName = "Add" Then
                    Dim objSubscribers As New Subscribers
                    objSubscribers.DivisionID = e.Item.Cells(0).Text
                    objSubscribers.AdminCntID = e.Item.Cells(1).Text
                    objSubscribers.DomainID = Session("DomainID")
                    objSubscribers.UserCntID = Session("UserContactID")
                    objSubscribers.EmailStartDate = Date.Now
                    objSubscribers.EmailEndDate = Date.Now

                    Dim SubscriberId As Long
                    Dim TargetDomainId As Long
                    Dim TargetGroupId As Long
                    SubscriberId = objSubscribers.ManageSubscribers()
                    TargetDomainId = objSubscribers.TargetDomainID
                    TargetGroupId = objSubscribers.TargetGroupId
                    If objSubscribers.bitExists = False Then
                        'File.Exists()
                        Dim sXMLFilePath As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\"

                        If Not Directory.Exists(sXMLFilePath & "\" & TargetDomainId) Then
                            Directory.CreateDirectory(sXMLFilePath & "\" & TargetDomainId)
                        End If
                        'Dim strAdvSearch As String
                        'strAdvSearch = "DynamicFormConfig_" & TargetDomainId & "_" & 1 & "_" & TargetGroupId & ".xml"

                        Dim strEmailBroadCast As String
                        strEmailBroadCast = "DynamicFormConfig_" & TargetDomainId & "_" & 2 & "_" & TargetGroupId & ".xml"

                        Dim strLeadBox As String
                        strLeadBox = "DynamicFormConfig_" & TargetDomainId & "_" & 3 & "_" & 0 & ".xml"

                        Dim strOrderForm As String
                        strOrderForm = "DynamicFormConfig_" & TargetDomainId & "_" & 4 & "_" & 0 & ".xml"

                        Dim strSurvey As String
                        strSurvey = "DynamicFormConfig_" & TargetDomainId & "_" & 5 & "_" & 0 & ".xml"

                        Dim strSmpSearch As String
                        strSmpSearch = "DynamicFormConfig_" & TargetDomainId & "_" & 6 & "_" & TargetGroupId & ".xml"

                        'File.Copy(sXMLFilePath & "AdvancedSearchTemplate.xml", sXMLFilePath & strAdvSearch, True)
                        File.Copy(sXMLFilePath & "EmailCampaigSearch.xml", sXMLFilePath & strEmailBroadCast, True)
                        File.Copy(sXMLFilePath & "Leadbox.xml", sXMLFilePath & strLeadBox, True)
                        File.Copy(sXMLFilePath & "Order.xml", sXMLFilePath & strOrderForm, True)
                        File.Copy(sXMLFilePath & "Survey.xml", sXMLFilePath & strSurvey, True)
                        File.Copy(sXMLFilePath & "SimpleSearch.xml", sXMLFilePath & strSmpSearch, True)


                        'update template data for selected domain where its empty
                        Dim objOppBizDocs As New BACRM.BusinessLogic.Opportunities.OppBizDocs
                        objOppBizDocs.strText = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\BizDocTemplate.htm")
                        objOppBizDocs.Description = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\PackingListTemplate.htm")
                        objOppBizDocs.BizDocCSS = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\BizDocTemplateDefaultCSS.css")
                        objOppBizDocs.DomainID = TargetDomainId
                        objOppBizDocs.byteMode = 3
                        objOppBizDocs.CreateBizDocTemplateByDefault()


                        Dim dtTable As DataTable
                        Dim objUserAccess As New UserAccess
                        objUserAccess.DomainID = TargetDomainId
                        dtTable = objUserAccess.GetDomainDetails()

                        Dim lngBaseUOM As Long
                        objCommon.DomainID = TargetDomainId
                        objCommon.Mode = 30
                        lngBaseUOM = CCommon.ToLong(objCommon.GetSingleFieldValue())
                        'Save a new Sample Warehouse for new "Sample Inventory Item"
                        Dim lngWareHouseID As Long = 0
                        Dim objItems As New CItems
                        objItems.Warehouse = "Sample WareHouse"
                        objItems.WarehouseID = lngWareHouseID
                        objItems.wStreet = ""
                        objItems.wCity = ""
                        objItems.wPinccode = ""
                        objItems.wState = 0
                        objItems.wCountry = 0
                        objItems.DomainID = TargetDomainId
                        lngWareHouseID = objItems.ManageWarehouse()

                        'Creating a new "Sample Inventory Item"
                        objItems = New CItems
                        Dim dr As DataRow
                        Dim strEditedfldlst As String = ""

                        With objItems
                            .ItemCode = 0
                            .ItemName = "Sample Inventory Item"
                            .ItemDesc = "Sample Desc"
                            .ItemType = "P"
                            .ListPrice = 10
                            .ItemClassification = 0
                            .ModelID = "Model1"
                            .KitParent = False
                            .bitSerialized = False
                            .bitLotNo = False
                            .bitArchieve = False
                            .Manufacturer = "Sample Manufacturer"
                            .BarCodeID = ""
                            .Taxable = False

                            .SKU = ""
                            .ItemGroupID = 0
                            .AverageCost = 10
                            .LabourCost = 5

                            .BaseUnit = lngBaseUOM
                            .PurchaseUnit = lngBaseUOM
                            .SaleUnit = lngBaseUOM

                            .strFieldList = "<NewDataset></NewDataset>"

                            .UserCntID = objSubscribers.AdminCntID
                            .DomainID = TargetDomainId

                            .COGSChartAcntId = dtTable.Rows(0).Item("numCOGSAccID")
                            .AssetChartAcntId = dtTable.Rows(0).Item("numAssetAccID")
                            .IncomeChartAcntId = dtTable.Rows(0).Item("numIncomeAccID")
                            .Weight = 10
                            .Height = 2
                            .Length = 2
                            .Width = 1
                            .FreeShipping = False
                            .AllowBackOrder = False

                            .ShowDeptItem = False
                            .ShowDeptItemDesc = False
                            .CalPriceBasedOnIndItems = False
                            .Assembly = False

                            .ItemClass = 0
                            .StandardProductIDType = 0
                            .ListOfAPI = ""
                            .ShippingClass = 0
                        End With

                        Dim strError As String = objItems.ManageItemsAndKits()
                        If strError.Length > 2 Then
                            'litMessage.Text = strError
                            Exit Sub
                        End If

                        'Creating a new "Sample Non Inventory Item"
                        objItems = New CItems
                        strEditedfldlst = ""

                        With objItems
                            .ItemCode = 0
                            .ItemName = "Sample Non Inventory Item"
                            .ItemDesc = "Sample Desc"
                            .ItemType = "N"
                            .ListPrice = 10
                            .ItemClassification = 0
                            .ModelID = "Model1"
                            .KitParent = False
                            .bitSerialized = False
                            .bitLotNo = False
                            .bitArchieve = False
                            .Manufacturer = "Sample Manufacturer"
                            .BarCodeID = ""
                            .Taxable = False

                            .SKU = ""
                            .ItemGroupID = 0
                            .AverageCost = 10
                            .LabourCost = 5

                            .BaseUnit = lngBaseUOM
                            .PurchaseUnit = lngBaseUOM
                            .SaleUnit = lngBaseUOM

                            .strFieldList = "<NewDataset></NewDataset>"

                            .UserCntID = objSubscribers.AdminCntID
                            .DomainID = TargetDomainId

                            .COGSChartAcntId = dtTable.Rows(0).Item("numCOGSAccID")
                            .AssetChartAcntId = 0
                            .IncomeChartAcntId = dtTable.Rows(0).Item("numIncomeAccID")
                            .Weight = 10
                            .Height = 2
                            .Length = 2
                            .Width = 1
                            .FreeShipping = False
                            .AllowBackOrder = False


                            .ShowDeptItem = False
                            .ShowDeptItemDesc = False
                            .CalPriceBasedOnIndItems = False
                            .Assembly = False

                            .ItemClass = 0
                            .StandardProductIDType = 0
                            .ListOfAPI = ""
                            .ShippingClass = 0
                        End With
                        strError = objItems.ManageItemsAndKits()

                        'Creating a new "Sample Service Item"
                        objItems = New CItems
                        strEditedfldlst = ""

                        With objItems
                            .ItemCode = 0
                            .ItemName = "Sample Service Item"
                            .ItemDesc = "Sample Desc"
                            .ItemType = "S"
                            .ListPrice = 10
                            .ItemClassification = 0
                            .ModelID = "Model1"
                            .KitParent = False
                            .bitSerialized = False
                            .bitLotNo = False
                            .bitArchieve = False
                            .Manufacturer = "Sample Manufacturer"
                            .BarCodeID = ""
                            .Taxable = False

                            .SKU = ""
                            .ItemGroupID = 0
                            .AverageCost = 10
                            .LabourCost = 5

                            .BaseUnit = lngBaseUOM
                            .PurchaseUnit = lngBaseUOM
                            .SaleUnit = lngBaseUOM

                            .strFieldList = "<NewDataset></NewDataset>"

                            .UserCntID = objSubscribers.AdminCntID
                            .DomainID = TargetDomainId

                            .COGSChartAcntId = dtTable.Rows(0).Item("numCOGSAccID")
                            .AssetChartAcntId = 0
                            .IncomeChartAcntId = dtTable.Rows(0).Item("numIncomeAccID")
                            .Weight = 10
                            .Height = 2
                            .Length = 2
                            .Width = 1
                            .FreeShipping = False
                            .AllowBackOrder = False


                            .ShowDeptItem = False
                            .ShowDeptItemDesc = False
                            .CalPriceBasedOnIndItems = False
                            .Assembly = False

                            .ItemClass = 0
                            .StandardProductIDType = 0
                            .ListOfAPI = ""
                            .ShippingClass = 0
                        End With
                        strError = objItems.ManageItemsAndKits()

                        'Creating a new "Shipping service item"
                        objItems = New CItems
                        strEditedfldlst = ""

                        With objItems
                            .ItemCode = 0
                            .ItemName = "Shipping charge"
                            .ItemDesc = "Shipping service item"
                            .ItemType = "S"
                            .ListPrice = 10
                            .ItemClassification = 0
                            .ModelID = ""
                            .KitParent = False
                            .bitSerialized = False
                            .bitLotNo = False
                            .bitArchieve = False
                            .Manufacturer = ""
                            .BarCodeID = ""
                            .Taxable = False

                            .SKU = ""
                            .ItemGroupID = 0
                            .AverageCost = 10
                            .LabourCost = 5

                            .BaseUnit = lngBaseUOM
                            .PurchaseUnit = lngBaseUOM
                            .SaleUnit = lngBaseUOM

                            .strFieldList = "<NewDataset></NewDataset>"

                            .UserCntID = objSubscribers.AdminCntID
                            .DomainID = TargetDomainId

                            .COGSChartAcntId = dtTable.Rows(0).Item("numCOGSAccID")
                            .AssetChartAcntId = 0
                            .IncomeChartAcntId = dtTable.Rows(0).Item("numIncomeAccID")
                            .Weight = 0
                            .Height = 0
                            .Length = 0
                            .Width = 0
                            .FreeShipping = False
                            .AllowBackOrder = False


                            .ShowDeptItem = False
                            .ShowDeptItemDesc = False
                            .CalPriceBasedOnIndItems = False
                            .Assembly = False

                            .ItemClass = 0
                            .StandardProductIDType = 0
                            .ListOfAPI = ""
                            .ShippingClass = 0
                        End With
                        strError = objItems.ManageItemsAndKits()

                        'SET Shipping service item on Domain Details
                        objCommon = New CCommon
                        objCommon.Mode = 41
                        objCommon.DomainID = TargetDomainId
                        objCommon.UpdateValueID = objItems.ItemCode
                        objCommon.UpdateSingleFieldValue()

                        'Creating a new "Discount service item"
                        objItems = New CItems
                        strEditedfldlst = ""

                        With objItems
                            .ItemCode = 0
                            .ItemName = "Discount"
                            .ItemDesc = "Discount service item"
                            .ItemType = "S"
                            .ListPrice = 10
                            .ItemClassification = 0
                            .ModelID = ""
                            .KitParent = False
                            .bitSerialized = False
                            .bitLotNo = False
                            .bitArchieve = False
                            .Manufacturer = ""
                            .BarCodeID = ""
                            .Taxable = False

                            .SKU = ""
                            .ItemGroupID = 0
                            .AverageCost = 10
                            .LabourCost = 5

                            .BaseUnit = lngBaseUOM
                            .PurchaseUnit = lngBaseUOM
                            .SaleUnit = lngBaseUOM

                            .strFieldList = "<NewDataset></NewDataset>"

                            .UserCntID = objSubscribers.AdminCntID
                            .DomainID = TargetDomainId

                            .COGSChartAcntId = dtTable.Rows(0).Item("numCOGSAccID")
                            .AssetChartAcntId = 0
                            .IncomeChartAcntId = dtTable.Rows(0).Item("numIncomeAccID")
                            .Weight = 10
                            .Height = 2
                            .Length = 2
                            .Width = 1
                            .FreeShipping = False
                            .AllowBackOrder = False


                            .ShowDeptItem = False
                            .ShowDeptItemDesc = False
                            .CalPriceBasedOnIndItems = False
                            .Assembly = False

                            .ItemClass = 0
                            .StandardProductIDType = 0
                            .ListOfAPI = ""
                            .ShippingClass = 0
                        End With
                        strError = objItems.ManageItemsAndKits()

                        'SET Discount service item on Domain Details
                        objCommon = New CCommon
                        objCommon.Mode = 42
                        objCommon.DomainID = TargetDomainId
                        objCommon.UpdateValueID = objItems.ItemCode
                        objCommon.UpdateSingleFieldValue()
                    End If
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "window.opener.reDirectPage('../Service/frmSubscriberDTL.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SubID=" & SubscriberId & "'); self.close();"
                    strScript += "</script>"
                    If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace