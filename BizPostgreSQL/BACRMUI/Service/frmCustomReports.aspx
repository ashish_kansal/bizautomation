﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmCustomReports.aspx.vb" Inherits=".frmCustomReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function OpenAddReportWindow(numReportID) {
            window.open("../service/frmAddCustomReport.aspx" + (numReportID > 0 ? "?numReportID=" + numReportID : ""), "AddNewReport", "toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=300,scrollbars=yes,resizable=yes");
            return false;
        }

        function DeleteReport() {
            if (confirm("Report will be deleted. Do you want to proceed?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnAddCustomReport" runat="server" Text="New Report" CssClass="btn btn-primary" OnClientClick="return OpenAddReportWindow(0);" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Custom Reports
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvCustomReport" class="table table-bordered table-striped" runat="server" Width="100%" AutoGenerateColumns="false" CellSpacing="0" CellPadding="0">
                    <Columns>
                        <asp:BoundField Visible="false" DataField="numReportID" />
                        <asp:BoundField Visible="false" DataField="numDomainID" />
                        <asp:BoundField DataField="vcDomainName" HeaderText="Domain" />
                        <asp:BoundField DataField="vcReportName" HeaderText="Report Name" />
                        <asp:BoundField DataField="vcReportDescription" HeaderText="Report Description" />
                        <asp:BoundField DataField="vcEmailTo" HeaderText="Email To" />
                        <asp:BoundField DataField="vcEmailFrequency" HeaderText="Email Frequency" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="80">
                            <ItemTemplate>
                                <asp:LinkButton ID="imgEdit" runat="server" CssClass="btn btn-xs btn-info" CommandName="EditReport" ToolTip="Edit" OnClientClick='<%# "return OpenAddReportWindow(" & Eval("numReportID") & ")"%>'><i class="fa fa-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="imgDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="DeleteReport" CommandArgument='<%#Eval("numReportID")%>' OnClientClick="return DeleteReport();" ToolTip="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table class="table table-bordered table-striped" width="100%" style="padding: 0px; border: 0px none; border-collapse: collapse">
                            <tr>
                                <th>Domain</th>
                                <th>Report Name</th>
                                <th>Report Description</th>
                                <th>Email To</th>
                                <th>Email Frequency</th>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center; font-weight: bold; padding: 0px;">No Data
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
