﻿Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports BACRM.BusinessLogic.Accounting
Imports System.Net
Imports System.Xml
Imports BACRM.BusinessLogic.Admin
Imports System.Web.Services
Imports System.Data.SqlClient

Public Class frmAccountingDiagnosis
    Inherits BACRMPage

    Dim objGL As GeneralLedger
    Dim dsDiagnosis As DataSet

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                If Session("UserContactID") <> "1" Then
                    Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                End If

                BindDomain()
                BindRepeater()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Other Methods"

    Private Sub BindDomain()
        Try
            'Get Domains to pass as parameters
            Dim dtTable As DataTable
            Dim objSubscribers As New Subscribers
            With objSubscribers
                .UserCntID = 1
                .SortOrder = 1
                .SortCharacter = "0"
                .SearchWord = ""
                .DomainID = 1
                .ClientTimeZoneOffset = 0
                .CurrentPage = 1
                .PageSize = 1000
                .TotalRecords = 1000
                .columnName = "numSubscriberID"
                .columnSortOrder = "Asc"
            End With
            dtTable = objSubscribers.GetSubscribers()
            Dim dv As DataView = dtTable.DefaultView
            dv.RowFilter = "numDomainID <> 0"
            dv.Sort = "vcCompanyName ASC"
            dtTable = dv.ToTable()

            ddlDomain.DataSource = dtTable
            ddlDomain.DataTextField = "vcCompanyName"
            ddlDomain.DataValueField = "numDomainID"
            ddlDomain.DataBind()
            ddlDomain.SelectedIndex = 0

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindRepeater()
        Try
            Dim lngDomainID As Long = CCommon.ToLong(ddlDomain.SelectedValue)
            If lngDomainID = 0 Then Exit Sub

            If dsDiagnosis Is Nothing Then dsDiagnosis = New DataSet
            Dim xmlFilePath As String = CCommon.GetDocumentPhysicalPath(0) & "AccountingDiagnosis_" & lngDomainID & ".xml"
            If Not File.Exists(xmlFilePath) Then
                ShowMessage("Xml file is not available for selected company. Please try again after some time.")
                rptMain.DataSource = Nothing
                rptMain.DataBind()
                Exit Sub
            End If
            dsDiagnosis.ReadXml(xmlFilePath)

            If dsDiagnosis IsNot Nothing AndAlso dsDiagnosis.Tables.Count > 0 Then
                Dim dtFirst As DataTable = dsDiagnosis.Tables(0).Copy()
                'dsDiagnosis.Tables.RemoveAt(0)
                rptMain.DataSource = dtFirst
                rptMain.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Control Events"

    Private Sub rptMain_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptMain.ItemDataBound
        Try

            'BASICS TO STICK TO :
            '1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). 
            '   Given query will get records from General Header table which records have not General Detail records
            '2. Check details of every transactions whether they are in respective tables or not.
            '3. Execute Select Statement For [#MissedTransactionDetails]
            '4. List entries if General header amount is not matching Total Credit value of General Details table
            '5. List entries if General header amount is not matching Total Debit value of General Details table
            '6. ---- Accounts Receivable Diagnosis ----
            '7.  Journal Entries Total (AR)
            '8.  Transaction Total (AR)
            '9.  AR Aging Summary
            '10. AR Aging Details
            '11. Execute AR Aging Detail table
            '12. Transaction v/s GL Entries mapping details.
            '13. Check whether any Invoiced transaction are in general entries or not (vice versa).
            '14. ---- Accounts Payable Diagnosis ----- 
            '15. Journal Entries Total (AP)
            '16. Transaction Total (AP)
            '17. -- AP Aging Summary
            '18. -- AP Aging Details
            '19. 'Execute AP Aging Details'

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim gvBalance As GridView = Nothing
                gvBalance = e.Item.FindControl("gvBalance")
                If gvBalance IsNot Nothing Then
                    Dim hdnTableName As HiddenField
                    hdnTableName = e.Item.FindControl("hdnTableName")

                    If hdnTableName IsNot Nothing AndAlso CCommon.ToString(hdnTableName.Value).Length > 0 Then
                        gvBalance.DataSource = dsDiagnosis.Tables(CCommon.ToString(hdnTableName.Value))
                        gvBalance.DataBind()
                    End If

                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlDomain_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDomain.SelectedIndexChanged
        Try
            BindRepeater()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Using Delegates"

    'Public Delegate Sub LongTimeTask_Delegate(s As String)
    Public Delegate Sub AccountingDiagnosis_Delegate(s As String)

    Private Sub btnUpdateDiagnosis_Click(sender As Object, e As EventArgs) Handles btnUpdateDiagnosis.Click
        Try
            Dim lngDomainID As Long = 0
            lngDomainID = CCommon.ToLong(ddlDomain.SelectedValue)

            ' databind of all the controls
            Dim d As AccountingDiagnosis_Delegate = Nothing
            d = New AccountingDiagnosis_Delegate(AddressOf GetAccountingDiagnosis)

            Dim R As IAsyncResult = Nothing
            'invoking the method
            'R = d.BeginInvoke(lngDomainID, Nothing, Nothing)
            R = d.BeginInvoke(lngDomainID, New AsyncCallback(AddressOf TaskCompleted), Nothing)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub GetAccountingDiagnosis(ByVal lngDomainID As Long)
        Try
            ' Create the DataAdapter & DataSet
            Dim dataAdatpter As New Npgsql.NpgsqlDataAdapter
            Dim dsDiagnosis As New DataSet
            Dim strCompanyName As String = ""

            'this function takes more time to complete .. so not using existing dataaccess layer which times out
            Dim connForDiagnosis As New Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings("ConnectionString").ToString()) 'in seconds 

            Dim cmdForDiagnosis As New Npgsql.NpgsqlCommand()
            cmdForDiagnosis.Connection = connForDiagnosis
            cmdForDiagnosis.CommandText = "usp_accountdiagnosis"
            cmdForDiagnosis.CommandType = CommandType.StoredProcedure
            cmdForDiagnosis.CommandTimeout = 100000 'in seconds
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter("v_numdomainid", NpgsqlTypes.NpgsqlDbType.Numeric, CInt(DomainID)))
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur2", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur3", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur4", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur5", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur6", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur7", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur8", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur9", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur10", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur11", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur12", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur13", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur14", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur15", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur16", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur17", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
            cmdForDiagnosis.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur18", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

            Try
                Dim i As Int32 = 0
                connForDiagnosis.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connForDiagnosis.BeginTransaction()
                    cmdForDiagnosis.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In cmdForDiagnosis.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                dataAdatpter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connForDiagnosis)
                                dsDiagnosis.Tables.Add(parm.Value.ToString())
                                dataAdatpter.Fill(dsDiagnosis.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()

                End Using
                connForDiagnosis.Close()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            Finally
                If (Not dataAdatpter Is Nothing) Then dataAdatpter.Dispose()
            End Try

            If dsDiagnosis IsNot Nothing AndAlso dsDiagnosis.Tables.Count > 0 Then

                'Naming for tables.
                Dim dtFirst As DataTable = dsDiagnosis.Tables(0)
                For intCnt As Integer = 0 To dtFirst.Rows.Count - 1
                    If dsDiagnosis.Tables(intCnt) IsNot Nothing Then
                        dsDiagnosis.Tables(intCnt).TableName = CCommon.ToString(dtFirst.Rows(intCnt)("TableName"))
                        dsDiagnosis.Tables(intCnt).AcceptChanges()
                    End If
                Next
                dsDiagnosis.AcceptChanges()
                Dim dsTemp As DataSet = dsDiagnosis.Copy()
                'dsTemp.Tables.RemoveAt(0)

                'Create XML file as per dataset. Delete existing file if any.
                Dim xmlFilePath As String = CCommon.GetDocumentPhysicalPath(0) & "AccountingDiagnosis_" & lngDomainID & ".xml"
                If File.Exists(xmlFilePath) Then
                    Try
                        File.Delete(xmlFilePath)
                    Catch ex As Exception
                        DisplayError("Error occurred while deleting file. File Path : " & xmlFilePath & ". " & ex.Message.ToString())
                    End Try

                End If
                dsTemp.WriteXml(xmlFilePath)
                dsTemp.Dispose()
                dsDiagnosis.Tables.Clear()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub TaskCompleted(R As IAsyncResult)
        ' Write here code to handle the completion of your asynchronous method
        Try
            If R.IsCompleted = True Then
                BindRepeater()
                ShowMessage("Accounting Diagnosis updated successfully.")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class