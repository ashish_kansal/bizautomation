﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmAddCustomReport.aspx.vb" Inherits=".frmAddCustomReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tblFormat td:first-child {
            font-weight: bold;
            white-space: nowrap;
            text-align: right;
        }

        input[type=checkbox], input[type=radio] {
            vertical-align: middle;
            position: relative;
            bottom: 1px;
        }

        input[type=radio] {
            bottom: 2px;
        }
    </style>
    <script type="text/javascript">
        function CloseAndRefresh() {
            window.opener.location.href = window.opener.location.href;
            window.close();
            return false;
        }

        function ValidateData() {
            if ($("#txtReportName").val().length == 0) {
                $("#txtReportName").focus();
                alert("Report name is required");
                return false;
            } else if ($("#txtQuery").val().length == 0) {
                $("#txtQuery").focus();
                alert("Query is required");
                return false;
            }

            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float:right">
        <asp:Button ID="btnSave" runat="server" Text="Save & Close" CssClass="ImageButton SaveClose" OnClientClick="return ValidateData();" />
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ImageButton Cancel" OnClientClick="return CloseAndRefresh();" />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" runat="server">
    Custom Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <asp:HiddenField ID="hdnReportID" runat="server" />
    <table width="1024px">
        <tr>
            <td colspan="2" style="text-align:center">
                <asp:Label ID="lblException" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; min-width: 400px;">
                <table width="100%" class="tblFormat">
                    <tr>
                        <td>Domain<span style="color:red"> *</span></td>
                        <td>
                            <asp:DropDownList ID="ddlDomain" runat="server" Width="200" DataTextField="vcDomainName" DataValueField="numDomainID"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Report Name<span style="color:red"> *</span></td>
                        <td>
                            <asp:TextBox ID="txtReportName" runat="server" MaxLength="300" Width="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Report Description</td>
                        <td>
                            <asp:TextBox ID="txtReportDescription" TextMode="MultiLine" Rows="5" MaxLength="300" runat="server" Width="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Email To</td>
                        <td>
                            <asp:TextBox ID="txtEmailTo" TextMode="MultiLine" Rows="3" runat="server" Width="250" MaxLength="1000"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Email Frequency</td>
                        <td style="padding-top: 4px;">
                            <asp:RadioButton ID="rdbDaily" runat="server" Text="Daily" GroupName="EmailFrequency" />
                            <asp:RadioButton ID="rdbWeekly" runat="server" Text="Weekly" GroupName="EmailFrequency" />
                            <asp:RadioButton ID="rdbMonthly" runat="server" Text="Monthly" GroupName="EmailFrequency" />
                            <asp:RadioButton ID="rdbYearly" runat="server" Text="Yearly" GroupName="EmailFrequency" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CSS
                        </td>
                        <td>
                            <asp:TextBox ID="txtCSS" TextMode="MultiLine" Rows="10" runat="server" Width="350"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100%">
                <b>Query</b><span style="color:red"> *</span><br />
                <asp:TextBox ID="txtQuery" TextMode="MultiLine" Rows="25" runat="server" Width="98%"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
