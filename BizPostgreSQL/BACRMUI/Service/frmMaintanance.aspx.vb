﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports Microsoft.Web.Administration
Imports System.Net
Imports System.Xml

Public Class frmMaintanance
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""


            txtAppendCss.Text = "/* jQuery.Rating Plugin CSS - http://www.fyneworks.com/jquery/star-rating/ */" & _
                                "div.rating-cancel,div.star-rating{float:left;width:17px;height:15px;text-indent:-999em;cursor:pointer;display:block;background:transparent;overflow:hidden}" & _
                               "div.rating-cancel,div.rating-cancel a{background:url(delete.gif) no-repeat 0 -16px}" & _
                               "div.star-rating,div.star-rating a{background:url(../Images/star.gif) no-repeat 0 0px}" & _
                               "div.rating-cancel a,div.star-rating a{display:block;width:16px;height:100%;background-position:0 0px;border:0}" & _
                               "div.star-rating-on a{background-position:0 -16px!important}" & _
                               "div.star-rating-hover a{background-position:0 -32px}" & _
                               "/* Read Only CSS */" & _
                               "div.star-rating-readonly a{cursor:default !important}" & _
                                "/* Partial Star CSS */" & _
                               "div.star-rating{background:transparent!important;overflow:hidden!important}" & _
                               "/* END jQuery.Rating Plugin CSS */"

            If Not Page.IsPostBack Then
                If Session("UserContactID") <> "1" Then
                    Response.Redirect("~/admin/authentication.aspx?mesg=AC")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub
    Protected Sub btnRePublish_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRePublish.Click
        Try
            Dim objSite As New Sites
            Dim dtSites As DataTable = Sites.GetSitesAll()
            Dim dtPages, dtPage As DataTable
            Dim sb As New System.Text.StringBuilder
            Dim i As Integer = 0
            For Each dr As DataRow In dtSites.Rows
                Try
                    objSite.PageID = 0
                    objSite.SiteID = Sites.ToLong(dr("numSiteID"))
                    objSite.DomainID = Sites.ToLong(dr("numDomainID"))
                    dtPages = objSite.GetPages().Tables(0)
                    For Each drPage As DataRow In dtPages.Rows
                        objSite.PageID = drPage("numPageID")
                        dtPage = objSite.GetPages().Tables(0)
                        If dtPage.Rows.Count > 0 Then
                            objSite.ManageAspxPages(Sites.ToLong(dr("numSiteID")), CCommon.ToString(dtPage.Rows(0).Item("vcPageURL")), CCommon.ToBool(dtPage.Rows(0).Item("bitIsMaintainScroll")))
                        End If
                    Next
                Catch ex As Exception
                    sb.AppendLine("error in publishing site: " + Sites.ToString(dr("vcSiteName")) + "(SiteID:" + Sites.ToLong(dr("numSiteID")).ToString + ") ErrorMessage:" + ex.Message & "<br>")
                End Try
                i = i + 1
            Next
            sb.AppendLine("Total Site Published:" + i.ToString())
            ShowMessage(sb.ToString())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#Region "Delete and Create Websites from IIS"
    Protected Sub btnCreateWebsite_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCreateWebsite.Click
        Try
            Dim objSite As New Sites
            Dim dtSites As DataTable = Sites.GetSitesAll()
            'dtSites = dtSites.Select("numsiteID = 101").CopyToDataTable()
            'dtSites = dtSites.Select("numsiteID > 138").CopyToDataTable()
            Dim dtPages, dtPage As DataTable
            Dim sb As New System.Text.StringBuilder
            Dim i As Integer = 0
            For Each dr As DataRow In dtSites.Rows
                Try
                    Dim serverManager As Microsoft.Web.Administration.ServerManager
                    serverManager = New Microsoft.Web.Administration.ServerManager
                    If serverManager.Sites(CCommon.ToString(dr("vcSiteName"))) Is Nothing Then ' check if given site doesn't overwrite existing sites in iis
                        Dim mySite As Microsoft.Web.Administration.Site = serverManager.Sites.Add(CCommon.ToString(dr("vcSiteName")), "http", "*:80:" & CCommon.ToString(dr("vcHostName")) & ".bizautomation.com", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID")))
                        mySite.ServerAutoStart = True
                        'Create Virtual directory for usercontrols
                        mySite.Applications.Add("/UserControls", ConfigurationManager.AppSettings("CartLocation") & "\UserControls")
                        'PortalDocs is virtual directory which will be used to access documents of portal from BizCart
                        mySite.Applications.Add("/PortalDocs", ConfigurationManager.AppSettings("PortalLocation") & "\documents\docs")
                        serverManager.CommitChanges()
                        ' System.Threading.Thread.Sleep(3000)

                        ValidateLiveSite(CCommon.ToLong(dr("numSiteID")), CCommon.ToString(dr("vcSiteName")), CCommon.ToString(dr("vcLiveUrl")))
                        i = i + 1
                    Else
                        ShowMessage("Site name alredy exists.Please choose different name.")
                    End If
                Catch ex As Exception
                    sb.AppendLine("error in publishing site: " + Sites.ToString(dr("vcSiteName")) + "(SiteID:" + Sites.ToLong(dr("numSiteID")).ToString + ") ErrorMessage:" + ex.Message & "<br>")
                End Try

            Next
            sb.AppendLine("Total Site Created on IIS : " + i.ToString())
            ShowMessage(sb.ToString())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub btnDeleteFromIIS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteFromIIS.Click
        Try
            Dim lngTemplateID As Long
            Dim objSite As New Sites

            Dim dtSites As DataTable = Sites.GetSitesAll()

            For Each dr As DataRow In dtSites.Rows
                objSite.SiteID = Sites.ToLong(dr("numSiteID"))
                Try
                    'Delete website from IIS 7.5
                    Dim serverManager = New Microsoft.Web.Administration.ServerManager
                    Dim strSiteName As String = Sites.ToString(dr("vcSiteName"))
                    If Not serverManager.Sites(strSiteName) Is Nothing Then
                        Dim OldSite As Microsoft.Web.Administration.Site = serverManager.Sites(strSiteName)
                        serverManager.Sites.Remove(OldSite)
                        serverManager.CommitChanges()
                    End If
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                End Try
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region


    Private Sub ValidateLiveSite(ByVal SiteID As Long, ByVal SiteName As String, ByVal LiveSiteUrl As String)
        Try
            If LiveSiteUrl.Trim().Length() > 1 Then
                Dim IP As IPAddress() = Dns.GetHostAddresses(LiveSiteUrl.Trim().ToLower().Replace("http://", "").Replace("https://", ""))
                If IP.Length > 0 Then
                    '173.255.12.194 is ip addres where out web application server runs
                    If IP(0).ToString = "127.0.0.1" Or IP(0).ToString = "173.255.12.194" Then ' On server it returns "127.0.0.1" for biautomation.com instead of "173.255.12.194"

                        'update Live URL
                        Dim objSite As New Sites
                        objSite.SiteID = SiteID
                        objSite.LiveURL = LiveSiteUrl.Trim()
                        objSite.Mode = 0
                        objSite.ManageSites()
                        'Add host header
                        Dim serverManager As New Microsoft.Web.Administration.ServerManager

                        'Create New Binding
                        Dim b As Microsoft.Web.Administration.Binding = serverManager.Sites(SiteName.Trim()).Bindings.CreateElement()
                        b.SetAttributeValue("protocol", "http")
                        b.SetAttributeValue("bindingInformation", "*:80:" + LiveSiteUrl.Trim())
                        serverManager.Sites(SiteName.Trim()).Bindings.Add(b)
                        serverManager.CommitChanges()
                        'System.Threading.Thread.Sleep(3000)
                        ShowMessage("Validation passed sucessfully, Now you can surf shopping cart using <a target='_blank' href='http://" & LiveSiteUrl.Trim().Replace("http://", "").Replace("https://", "") & "'>" & LiveSiteUrl.Trim() & "</a>")
                    Else
                        ShowMessage("Validation failed, Please modify your website's DNS record and point " & LiveSiteUrl.Trim() & " to IP address 173.255.12.194")
                    End If
                Else
                    ShowMessage("Validation failed, Please modify your dns record and point " & LiveSiteUrl.Trim() & " to IP address 173.255.12.194")
                End If
            Else
                ShowMessage("Enter Your Website URL .")
            End If

        Catch ex As Exception
            If ex.Message.Contains("No such host is known") Then
                ShowMessage("Please enter valid URL")
            Else
                ShowMessage("Validation failed, Please modify your website's DNS record and point " & LiveSiteUrl.Trim() & " to IP address 173.255.12.194")
            End If

        End Try
    End Sub
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCopy.Click
        Try
            Dim objSite As New Sites
            Dim dtSites As DataTable = Sites.GetSitesAll()
            Dim sb As New System.Text.StringBuilder
            Dim i As Integer = 0
            For Each dr As DataRow In dtSites.Rows
                Try
                    'copy web.config to site's root folder
                    If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                        If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\web.config") Then
                            If (Directory.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Sites.ToLong(dr("numSiteID")).ToString())) Then

                                If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Sites.ToLong(dr("numSiteID")).ToString() & "\web.config") Then
                                    'remove readonly
                                    File.SetAttributes(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Sites.ToLong(dr("numSiteID")).ToString() & "\web.config", FileAttributes.Normal)
                                End If
                                File.Copy(ConfigurationManager.AppSettings("CartLocation") & "\web.config", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Sites.ToLong(dr("numSiteID")).ToString() & "\web.config", True)
                            Else
                                sb.AppendLine("Directory does not exist: " & ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Sites.ToLong(dr("numSiteID")).ToString() & "<br>")
                            End If
                        End If
                    End If
                Catch ex As Exception
                    sb.AppendLine("error in copy for site: " + Sites.ToString(dr("vcSiteName")) + "(SiteID:" + Sites.ToLong(dr("numSiteID")).ToString() + ") ErrorMessage:" + ex.Message & "<br>")
                End Try
                i = i + 1
            Next

            sb.AppendLine("Total copy occurance:" + i.ToString() & "<br>")
            sb.AppendLine("From Path:" + ConfigurationManager.AppSettings("CartLocation") & "<br>")
            ShowMessage(sb.ToString())

            CCommon.SendPushNotification(litMessage.Text)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Try
            Dim objSite As New Sites
            Dim dtSites As DataTable = Sites.GetSitesAll()
            Dim i As Integer = 0
            Dim lngTemplateID As Long
            For Each dr As DataRow In dtSites.Rows
                'lngTemplateID = objSite.CreateTemplate(Sites.ToLong(dr("numSiteID")), "ThankYouTemplate", "{#ThankYou#}", Sites.ToString(dr("vcSiteName")), Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")))
                'objSite.CreatePage(Sites.ToLong(dr("numSiteID")), lngTemplateID, "ThankYou", "ThankYou", "ThankYou.aspx", False, Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")))

                lngTemplateID = objSite.CreateTemplate(Sites.ToLong(dr("numSiteID")), "WriteReviewTemplate", "{#WriteReview#}", Sites.ToString(dr("vcSiteName")), Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")))
                objSite.CreatePage(Sites.ToLong(dr("numSiteID")), lngTemplateID, "WriteReview", "WriteReview", "WriteReview.aspx", False, Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")), Sites.ToBool(dr("bitIsMaintainScroll")))

                lngTemplateID = objSite.CreateTemplate(Sites.ToLong(dr("numSiteID")), "ReadReviewTemplate", "{#ReviewList#}", Sites.ToString(dr("vcSiteName")), Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")))
                objSite.CreatePage(Sites.ToLong(dr("numSiteID")), lngTemplateID, "ReadReview", "ReadReview", "ReadReview.aspx", False, Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")), Sites.ToBool(dr("bitIsMaintainScroll")))

                lngTemplateID = objSite.CreateTemplate(Sites.ToLong(dr("numSiteID")), "PermaLinkTemplate", "{#PermaLink#}", Sites.ToString(dr("vcSiteName")), Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")))
                objSite.CreatePage(Sites.ToLong(dr("numSiteID")), lngTemplateID, "PermaLink", "PermaLink", "PermaLink.aspx", False, Sites.ToLong(dr("numDomainID")), Sites.ToLong(dr("numCreatedBy")), Sites.ToBool(dr("bitIsMaintainScroll")))
            Next
            ShowMessage("Done")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#Region "JS and CSS"

    Private Sub btnCopyJs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopyJs.Click
        Try
            Dim objSite As New Sites
            Dim sb As New System.Text.StringBuilder
            Dim dtSites As DataTable = Sites.GetSitesAll()
            Dim i As Integer = 0
            Dim targetPath As String
            Dim SourcePath As String
            Dim strDirectory As String
            For Each dr As DataRow In dtSites.Rows
                Try
                    'copy web.config to site's root folder
                    If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                        strDirectory = ConfigurationManager.AppSettings("CartLocation")
                        SourcePath = strDirectory & "\Js"
                        targetPath = strDirectory & "\Pages\" & Sites.ToLong(dr("numSiteID").ToString()) & "\Js"
                        If Not Directory.Exists(targetPath) Then
                            Directory.CreateDirectory(targetPath)
                        End If
                        CopyAllFiles(SourcePath, targetPath)
                    End If
                Catch ex As Exception
                    sb.AppendLine("error in copy for site: " + Sites.ToString(dr("vcSiteName")) + "(SiteID:" + Sites.ToLong(dr("numSiteID")).ToString() + ") ErrorMessage:" + ex.Message & "<br>")
                End Try
                i = i + 1
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnCopyNewJs_Click(sender As Object, e As EventArgs) Handles btnCopyNewJs.Click
        Try
            Dim targetPath As String
            Dim SourcePath As String
            Dim strDirectory As String
            Dim sb As New System.Text.StringBuilder
            Dim dtSites As DataTable = Sites.GetSitesAll()
            Dim i As Integer = 0

            For Each dr As DataRow In dtSites.Rows
                Try
                    'copy web.config to site's root folder
                    If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                        strDirectory = ConfigurationManager.AppSettings("CartLocation")
                        SourcePath = strDirectory & "\Js"
                        targetPath = strDirectory & "\Pages\" & Sites.ToLong(dr("numSiteID").ToString()) & "\Js"
                        If Not Directory.Exists(targetPath) Then
                            Directory.CreateDirectory(targetPath)
                        End If
                        CopyAllNewFiles(SourcePath, targetPath)
                    End If
                Catch ex As Exception
                    sb.AppendLine("error in copy for site: " + Sites.ToString(dr("vcSiteName")) + "(SiteID:" + Sites.ToLong(dr("numSiteID")).ToString() + ") ErrorMessage:" + ex.Message & "<br>")
                End Try
                i = i + 1
            Next

            '      Dim string as String [] = {""} 
            '      If (File.Exists(resumeFile)) Then
            ' {
            '    File.Copy(resumeFile, newFile);
            '}
            '   Else
            '{
            '   Console.WriteLine("Resume file does not exist.");
            '}
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnAppendCSS_Click(sender As Object, e As EventArgs) Handles btnAppendCSS.Click
        Try
            Dim targetPath As String
            Dim SourcePath As String
            Dim strDirectory As String
            Dim sb As New System.Text.StringBuilder
            Dim dtSites As DataTable = Sites.GetSitesAll()

            Dim i As Integer = 0

            For Each dr As DataRow In dtSites.Rows
                Try
                    'copy web.config to site's root folder
                    If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                        strDirectory = ConfigurationManager.AppSettings("CartLocation")
                        SourcePath = strDirectory & "\Default\Style"
                        targetPath = strDirectory & "\Pages\" & Sites.ToLong(dr("numSiteID").ToString()) & ""
                        If Not Directory.Exists(targetPath) Then
                            Directory.CreateDirectory(targetPath)
                        End If
                        AppendCss(SourcePath, targetPath)
                    End If
                Catch ex As Exception
                    sb.AppendLine("error in copy for site: " + Sites.ToString(dr("vcSiteName")) + "(SiteID:" + Sites.ToLong(dr("numSiteID")).ToString() + ") ErrorMessage:" + ex.Message & "<br>")
                End Try
                i = i + 1
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub btnReCreateJournal_Click(sender As Object, e As EventArgs) Handles btnReCreateJournal.Click
        Dim objOppBizDocs As New OppBizDocs
        Dim dtOppBiDocItems As DataTable
        Dim OppBizDocID As Long
        Dim lngOppID As Long
        Dim numJournal_Id As Long
        Dim numDomainId As Long
        Dim numShipVia As Long
        Dim numDivisionID As Long
        Dim OppType As Short
        Dim ds As New DataSet

        Try
            Dim dt As DataTable
            Dim objCommon As New CCommon
            dt = objCommon.GetBizDocsWithBuggyJournals()
            For Each dr As DataRow In dt.Rows
                Try
                    OppBizDocID = CCommon.ToLong(dr("numOppBizDocsId"))
                    lngOppID = CCommon.ToLong(dr("numOppId"))
                    numJournal_Id = CCommon.ToLong(dr("numJournal_Id"))
                    numDomainId = CCommon.ToLong(dr("numDomainId"))
                    numShipVia = CCommon.ToLong(dr("numShipVia"))
                    numDivisionID = CCommon.ToLong(dr("numDivisionID"))
                    OppType = CCommon.ToLong(dr("tintOppType"))

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    objOppBizDocs.OppId = lngOppID
                    objOppBizDocs.DomainID = numDomainId
                    objOppBizDocs.UserCntID = CCommon.ToLong(dr("numCreatedBy"))
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)


                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, 1, numDomainId, dtOppBiDocItems)

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        If numJournal_Id = 0 Then
                            numJournal_Id = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, CDate(dr("datEntry_Date")).Date)
                        End If

                        Dim objJournalEntries As New JournalEntry

                        If OppType = 1 Then
                            If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                objJournalEntries.SaveJournalEntriesSales(lngOppID, numDomainId, dtOppBiDocItems, numJournal_Id, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, numDivisionID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=0, lngShippingMethod:=numShipVia)
                            Else
                                objJournalEntries.SaveJournalEntriesSalesNew(lngOppID, numDomainId, dtOppBiDocItems, numJournal_Id, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, numDivisionID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=0, lngShippingMethod:=numShipVia)
                            End If
                        Else
                            If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, numDomainId, dtOppBiDocItems, numJournal_Id, numDivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                            Else
                                objJournalEntries.SaveJournalEntriesPurchase(lngOppID, numDomainId, dtOppBiDocItems, numJournal_Id, numDivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=0, lngShippingMethod:=numShipVia)
                            End If
                        End If

                        objTransactionScope.Complete()
                    End Using

                    ShowMessage("Recreated Journal for OppBizDocsID:" & OppBizDocID.ToString & "<br>")
                Catch ex As Exception
                    DisplayError("Error found for OppBizDocsID:" & OppBizDocID.ToString & "<br>" & ex.Message.ToString() & "<br>")
                End Try

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnErrorMessage_Click(sender As Object, e As EventArgs) Handles btnErrorMessage.Click
        Try
            Dim dtSites As DataTable = Sites.GetSitesAll()

            For Each dr As DataRow In dtSites.Rows


                Dim e1 As New ErrorMessage
                Dim dtErrorMessages As DataTable = New DataTable()

                e1.DomainID = Sites.ToLong(dr("numDomainID"))
                e1.SiteID = Sites.ToLong(dr("numSiteID"))
                e1.CultureID = 0
                e1.Application = CCommon.ToInteger(ErrorMessage.GetApplication.ECommerce)


                If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(e1.DomainID)) Then
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(e1.DomainID))
                End If

                Dim strXmlfile As String = CCommon.GetDocumentPhysicalPath(e1.DomainID) & "ErrorMsg" & "_" & e1.DomainID & "_" & e1.SiteID & "_" & e1.Application & "_" & e1.CultureID & ".xml"

                dtErrorMessages = e1.GetAllErrorMessages()

                If File.Exists(strXmlfile) Then
                    File.Delete(strXmlfile)
                End If

                CreateXml(dtErrorMessages, strXmlfile)

            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub CreateXml(ByVal dtErrorMessages As DataTable, ByVal strXmlfile As String)
        Try
            Dim xDoc As XmlDocument = New XmlDocument()
            Dim root As XmlElement = xDoc.CreateElement("ErrorMessages")
            For Each Row As DataRow In dtErrorMessages.Rows
                Dim nm As XmlElement = xDoc.CreateElement("ErrorMessage")
                nm.SetAttribute("ErrorCode", CCommon.ToString(Row("vcErrorCode")))
                nm.SetAttribute("ErrorDesc", CCommon.ToString(Row("vcErrorDesc")))

                root.AppendChild(nm)
            Next
            xDoc.AppendChild(root)
            xDoc.Save(strXmlfile)
            xDoc.Load(strXmlfile)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Shared Sub CopyAllFiles(ByVal SourcePath As String, ByVal targetPath As String)
        'Copy all files under ~/Default Folder to New creted Site Folder
        Dim fileName, destFile As String
        If System.IO.Directory.Exists(SourcePath) Then
            Dim files() As String = System.IO.Directory.GetFiles(SourcePath)
            'Copy the files and overwrite destination files if they already exist.
            For Each s As String In files
                'remove readonly
                File.SetAttributes(s, FileAttributes.Normal)
                ' Use static Path methods to extract only the file name from the path.
                fileName = System.IO.Path.GetFileName(s)
                destFile = System.IO.Path.Combine(targetPath, fileName)

                System.IO.File.Copy(s, destFile, True)

                File.SetAttributes(destFile, FileAttributes.Normal)
            Next
        End If
    End Sub
    Private Shared Sub CopyAllNewFiles(ByVal SourcePath As String, ByVal targetPath As String)
        Try
            Dim fileName, destFile As String
            If System.IO.Directory.Exists(SourcePath) Then
                Dim files() As String = System.IO.Directory.GetFiles(SourcePath)
                For Each s As String In files
                    fileName = System.IO.Path.GetFileName(s)
                    destFile = System.IO.Path.Combine(targetPath, fileName)

                    If Not File.Exists(destFile) Then
                        System.IO.File.Copy(s, destFile, True)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub AppendCss(ByVal strCssAppend As String, ByVal targetPath As String)
        Try
            Dim fileName, destFile As String
            destFile = System.IO.Path.Combine(targetPath, "default.css")
            Using fs As New FileStream(destFile, FileMode.Append, FileAccess.Write)
                Using sw As New StreamWriter(fs)
                    sw.WriteLine(txtAppendCss.Text)
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnOpeningBalanceJournal_Click(sender As Object, e As EventArgs) Handles btnOpeningBalanceJournal.Click
        Dim objAccounting As New ChartOfAccounting

        Dim dt As DataTable
        With objAccounting

            dt = .GetCOAwithOpeningBalanceFirsttimeOnly()

            For Each dr As DataRow In dt.Rows
                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    If Math.Abs(CCommon.ToDecimal(dr("numOriginalOpeningBal"))) > 0 Then
                        .UserCntID = dr("numUserCntID")
                        .DomainID = dr("numDomainId")
                        .decOriginalOpeningBal = CCommon.ToDecimal(dr("numOriginalOpeningBal"))
                        .CategoryName = dr("vcAccountName")
                        .MakeChartOfAccountOpeningBalanceJournalEntry(CCommon.ToLong(dr("numAccountId")), dr("dtOpeningDate"))
                    End If

                    objTransactionScope.Complete()
                End Using
            Next
        End With

    End Sub

    Private Sub btnAddGL_Click(sender As Object, e As EventArgs) Handles btnAddGL.Click
        Dim objOppBizDocs As New OppBizDocs
        Dim dtOppBiDocItems As DataTable
        Dim OppBizDocID As Long = 0
        Dim lngOppID As Long
        Dim numJournal_Id As Long
        Dim numDomainId As Long
        Dim numDivisionID As Long
        Dim OppType As Short
        Dim ds As New DataSet

        Dim objOpp As New MOpportunity
        Dim objOpportunity As New COpportunities
        Dim objTransaction As Npgsql.NpgsqlTransaction

        Try

            Dim dt As DataTable
            Dim objCommon As New CCommon
            dt = objCommon.GetOrderWithoutJournals()
            For Each dr As DataRow In dt.Rows
                Try

                    lngOppID = CCommon.ToLong(dr("numOppId"))
                    numDomainId = CCommon.ToLong(dr("numDomainId"))
                    numDivisionID = CCommon.ToLong(dr("numDivisionID"))
                    OppType = CCommon.ToLong(dr("tintOppType"))

                    objOppBizDocs.OppId = lngOppID
                    objOppBizDocs.DomainID = numDomainId
                    objOppBizDocs.UserCntID = CCommon.ToLong(dr("numCreatedBy"))
                    ds = objOppBizDocs.GetOppInItemsForAccounting()
                    dtOppBiDocItems = ds.Tables(0)

                    If dtOppBiDocItems IsNot Nothing AndAlso dtOppBiDocItems.Rows.Count > 0 Then
                        Dim dtCopyItem As New DataTable
                        For Each drItem As DataRow In dtOppBiDocItems.Rows
                            If dtCopyItem.Rows.Count = 0 Then
                                dtCopyItem = If(dtOppBiDocItems.Select("ItemCode = " & CCommon.ToLong(drItem("ItemCode"))) IsNot Nothing, _
                                                dtOppBiDocItems.Select("ItemCode = " & CCommon.ToLong(drItem("ItemCode"))).CopyToDataTable(), _
                                                New DataTable)
                            End If

                            objTransaction = objOpportunity.BeginTransaction()
                            numJournal_Id = objOppBizDocs.SaveDataToHeader(drItem("numUnitHourReceived") * drItem("monPrice") * drItem("fltExchangeRate"), _
                                                                           CDate(drItem("datEntry_Date")).Date, _
                                                                           Description:=CCommon.ToString(drItem("vcPOppName")))

                            If numJournal_Id = 0 Then objOpportunity.RollbackTransaction(objTransaction)

                            Dim objJournalEntries As New JournalEntry
                            If objJournalEntries Is Nothing Then objJournalEntries = New JournalEntry
                            objJournalEntries.SaveJournalEntriesPurchaseClearing(lngOppID, numDomainId, dtCopyItem, numJournal_Id)
                            objOpportunity.CommitTransaction(objTransaction)
                            dtCopyItem.Rows.Clear()

                        Next
                    End If

                    ShowMessage("Recreated Journal for OrderID: " & CCommon.ToString(dr("vcPOppName")) & " and DomainID: " & numDomainId & "<br>")
                Catch ex As Exception
                    objOpportunity.RollbackTransaction(objTransaction)
                    DisplayError("Error found for OrderID:" & CCommon.ToString(dr("vcPOppName")) & " and DomainID: " & numDomainId & "<br>" & ex.Message.ToString() & "<br>")
                End Try

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class