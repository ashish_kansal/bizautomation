﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMaintanance.aspx.vb" Inherits=".frmMaintanance" MasterPageFile="~/common/BizMaster.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Accounting" %>

<%@ Import Namespace="MailBee.Mime" %>
<%@ Import Namespace="MailBee" %>
<%@ Import Namespace="MailBee.Security" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Import Namespace="BACRM.BusinessLogic.Admin" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="MailBee.SmtpMail" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="BACRM.BusinessLogic.Outlook" %>
<%@ Import Namespace="BACRM.BusinessLogic.ShioppingCart" %>
<asp:Content runat="server" ContentPlaceHolderID="head">
    <title>Maintanance</title>
    <script type="text/javascript">
        function OpenDiagnosis() {
            document.location.href = "../Service/frmAccountingDiagnosis.aspx";
            return false;
        }
    </script>
    <script language="vb" runat="server">
        Private Sub btnExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExecute.Click
            Dim dt As DataTable = Sites.GetSitesAll()
            For Each dr As DataRow In dt.Rows
                If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                    If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax") Then
                        If Directory.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID"))) Then
                            If Not File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID")) & "\Global.asax") Then
                                File.Copy(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID")) & "\Global.asax", True)
                            End If
                           
                        End If
                       
                    End If
                    
                End If

                If Directory.Exists(ConfigurationManager.AppSettings("CartLocation")) Then
                    If File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax.cs") Then
                        If Directory.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID"))) Then
                            If Not File.Exists(ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID")) & "\Global.asax.cs") Then
                                File.Copy(ConfigurationManager.AppSettings("CartLocation") & "\Global.asax.cs", ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & CCommon.ToString(dr("numSiteID")) & "\Global.asax.cs", True)
                                
                            End If
                            
                        End If
                        
                    End If
                    
                End If
                
            Next
            
        End Sub
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-bars"></i>

            <h3 class="box-title">Maintanance
            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Re Publish All websites</label>
                        <div>
                            <asp:Button Text="Re-Publish" runat="server" CssClass="btn btn-primary" ID="btnRePublish" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Create Website on IIS</label>
                        <div>
                            <asp:Button Text="Create website" runat="server" CssClass="btn btn-primary" ID="btnCreateWebsite" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Delete all Websites from IIS</label>
                        <div>
                            <asp:Button ID="btnDeleteFromIIS" CssClass="btn btn-danger" Text="Delete websites" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Create New Pages in BizCart to all domains</label>
                        <div>
                            <asp:Button Text="Create" runat="server" CssClass="btn btn-primary" ID="Button1" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Execute inline code</label>
                        <div>
                            <asp:Button Text="Execute" runat="server" CssClass="btn btn-primary" ID="btnExecute" OnClick="btnExecute_Click" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Copy All Js File</label>
                        <div>
                            <asp:Button Text="Copy Js" runat="server" CssClass="btn btn-primary" ID="btnCopyJs" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Test Custom reports</label>
                        <div>
                            <a href="../admin/frmCustomReportTest.aspx">Click Here</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>ReCreate Journal Entries for buggy Records</label>
                        <div>
                            <asp:Button ID="btnReCreateJournal" runat="server" Text="Create" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Copy Newly added Js Files</label>
                        <div>
                            <asp:Button Text="Copy Js" runat="server" CssClass="btn btn-primary" ID="btnCopyNewJs" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Manage Newly Added Error Messages</label>
                        <div>
                            <asp:Button Text="Add newly added error message" runat="server" CssClass="btn btn-primary" ID="btnErrorMessage" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Open Accounting Diagnosis</label>
                        <div>
                            <asp:Button Text="Accounting Diagnosis" runat="server" CssClass="btn btn-primary" ID="btnOpenDiagnosis" OnClientClick="return OpenDiagnosis();" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Create Order General Entries</label>
                        <div>
                            <asp:Button Text="Add General Entries" runat="server" CssClass="btn btn-primary" ID="btnAddGL" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Copy latest Web.config to All Sites</label>
                        <div>
                            <asp:Button Text="Copy Web.config" runat="server" CssClass="btn btn-primary" ID="btnCopy" />
                            (Will reset curent sessions, Do only when no one is logged in)
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label>Journal entry for opening balance</label>
                        <div>
                            <asp:Button Text="Create journal 4 opening balance" runat="server" CssClass="btn btn-primary" ID="btnOpeningBalanceJournal" /></td>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label>Append CSS</label>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:TextBox ID="txtAppendCss" TextMode="MultiLine" Height="200px" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <br />
                                <asp:Button Text="Append CSS" runat="server" CssClass="btn btn-primary" ID="btnAppendCSS" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
