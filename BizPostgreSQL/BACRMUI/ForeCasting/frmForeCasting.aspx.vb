' Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Forecasting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.ForeCasting

    Public Class frmForeCasting
        Inherits BACRMPage
        Dim intYear As Integer
        Dim objCommon As CCommon
        Dim dtTable As New DataTable
        Dim objForecast As Forecast
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        '  Protected WithEvents tsVert As Microsoft.Web.UI.WebControls.TabStrip
        


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ' Checking the rights for View
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    objCommon = New CCommon

                    GetUserRightsForPage(5, 1)
                    ''''call function for sub-tab management  - added on 29jul09 by Mohan
                    'objCommon.ManageSubTabs(radOppTab, Session("DomainID"), 5)
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSave.Visible = False
                        Else : btnSave.Visible = True
                        End If
                        If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                            btnSave.Visible = False
                        Else : btnSave.Visible = True
                        End If
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                            btnDelete.Visible = False
                        Else : btnDelete.Visible = True
                        End If
                    End If
                    ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Forecasting"
                    LoadDropDowns()

                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
                txtForAmt1.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtForAmt2.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtForAmt3.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtQuota1.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtQuota2.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtQuota3.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtForQFI1.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtForQFI2.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtForQFI3.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtQuotaFI1.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtQuotaFI2.Attributes.Add("onkeypress", "CheckNumber(1)")
                txtQuotaFI3.Attributes.Add("onkeypress", "CheckNumber(1)")
                btnSave.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadDropDowns()
            Try
                ddlYearFR.Items.Add(Year(Now()) - 2)
                ddlYearFR.Items.Add(Year(Now()) - 1)
                ddlYearFR.Items.Add(Year(Now()))
                ddlYearFR.Items.Add(Year(Now()) + 1)
                ddlYearFR.Items.Add(Year(Now()) + 2)
                ddlYearFR.Items.Add(Year(Now()) + 3)
                ddlYearFR.Items.Add(Year(Now()) + 4)

                ddlYearFI.Items.Add(Year(Now()) - 2)
                ddlYearFI.Items.Add(Year(Now()) - 1)
                ddlYearFI.Items.Add(Year(Now()))
                ddlYearFI.Items.Add(Year(Now()) + 1)
                ddlYearFI.Items.Add(Year(Now()) + 2)
                ddlYearFI.Items.Add(Year(Now()) + 3)
                ddlYearFI.Items.Add(Year(Now()) + 4)

                ddlYearFIH.Items.Add(Year(Now()) - 2)
                ddlYearFIH.Items.Add(Year(Now()) - 1)
                ddlYearFIH.Items.Add(Year(Now()))
                ddlYearFIH.Items.Add(Year(Now()) + 1)
                ddlYearFIH.Items.Add(Year(Now()) + 2)
                ddlYearFIH.Items.Add(Year(Now()) + 3)
                ddlYearFIH.Items.Add(Year(Now()) + 4)

                ddlYearFRH.Items.Add(Year(Now()) - 2)
                ddlYearFRH.Items.Add(Year(Now()) - 1)
                ddlYearFRH.Items.Add(Year(Now()))
                ddlYearFRH.Items.Add(Year(Now()) + 1)
                ddlYearFRH.Items.Add(Year(Now()) + 2)
                ddlYearFRH.Items.Add(Year(Now()) + 3)
                ddlYearFRH.Items.Add(Year(Now()) + 4)

                ''ddlYearFR.SelectedItem.Text = Year(Now)
                ''ddlYearFI.SelectedItem.Text = Year(Now)
                ''ddlYearFIH.SelectedItem.Text = Year(Now)
                ''ddlYearFRH.SelectedItem.Text = Year(Now)

                If Not ddlYearFR.Items.FindByText(Year(Now)) Is Nothing Then ddlYearFR.Items.FindByText(Year(Now)).Selected = True
                If Not ddlYearFI.Items.FindByText(Year(Now)) Is Nothing Then ddlYearFI.Items.FindByText(Year(Now)).Selected = True
                If Not ddlYearFIH.Items.FindByText(Year(Now)) Is Nothing Then ddlYearFIH.Items.FindByText(Year(Now)).Selected = True
                If Not ddlYearFIH.Items.FindByText(Year(Now)) Is Nothing Then ddlYearFIH.Items.FindByText(Year(Now)).Selected = True
                If Not ddlYearFRH.Items.FindByText(Year(Now)) Is Nothing Then ddlYearFRH.Items.FindByText(Year(Now)).Selected = True

                Dim lintmonth As Integer
                lintmonth = Month(Now)
                If lintmonth <= 3 Then
                    ddlQuarterFR.SelectedIndex = 0
                    ddlQFRH.SelectedIndex = 0
                    ddlQuarterFI.SelectedIndex = 0
                    ddlQuarterFIH.SelectedIndex = 0
                ElseIf lintmonth > 3 And lintmonth <= 6 Then
                    ddlQuarterFR.SelectedIndex = 1
                    ddlQFRH.SelectedIndex = 1
                    ddlQuarterFI.SelectedIndex = 1
                    ddlQuarterFIH.SelectedIndex = 1
                ElseIf lintmonth > 6 And lintmonth <= 9 Then
                    ddlQuarterFR.SelectedIndex = 2
                    ddlQFRH.SelectedIndex = 2
                    ddlQuarterFI.SelectedIndex = 2
                    ddlQuarterFIH.SelectedIndex = 2
                ElseIf lintmonth > 9 Then
                    ddlQuarterFR.SelectedIndex = 3
                    ddlQFRH.SelectedIndex = 3
                    ddlQuarterFI.SelectedIndex = 3
                    ddlQuarterFIH.SelectedIndex = 3
                End If

                Dim dtItem As DataTable
                Dim objOpportunity As New MOpportunity
                objOpportunity.DomainID = Session("DomainId")
                dtItem = objOpportunity.ItemByDomainID
                ddlItemFI.DataSource = dtItem
                ddlItemFI.DataTextField = "vcItemname"
                ddlItemFI.DataValueField = "numItemcode"
                ddlItemFI.DataBind()
                ddlItemFI.Items.Insert(0, "--Select One--")
                ddlItemFI.Items.FindByText("--Select One--").Value = 0

                ForecastOppReport()
                ForecastingOppHstr()
            Catch ex As Exception
                Throw (ex)
            End Try
        End Sub

        Sub ForecastOppReport()
            Try
                txtQuota1.Text = ""
                txtQuota2.Text = ""
                txtQuota3.Text = ""
                txtForAmt1.Text = ""
                txtForAmt2.Text = ""
                txtForAmt3.Text = ""
                intYear = ddlYearFR.SelectedItem.Value
                If objForecast Is Nothing Then objForecast = New Forecast
                If ddlQuarterFR.SelectedItem.Value = 0 Then  ' if it is first quarter
                    firstmonth.Text = "January"
                    secondmonth.Text = "February"
                    thirdmonth.Text = "March"
                    objForecast.firstMonth = 1
                    objForecast.secondMonth = 2
                    objForecast.thirdMonth = 3
                ElseIf ddlQuarterFR.SelectedItem.Value = 1 Then ' if it is second quarter
                    firstmonth.Text = "April"
                    secondmonth.Text = "May"
                    thirdmonth.Text = "June"
                    objForecast.firstMonth = 4
                    objForecast.secondMonth = 5
                    objForecast.thirdMonth = 6
                ElseIf ddlQuarterFR.SelectedItem.Value = 2 Then ' if it is thrid quarter
                    firstmonth.Text = "July"
                    secondmonth.Text = "August"
                    thirdmonth.Text = "September"

                    objForecast.firstMonth = 7
                    objForecast.secondMonth = 8
                    objForecast.thirdMonth = 9
                ElseIf ddlQuarterFR.SelectedItem.Value = 3 Then ' if it is fourth quarter
                    firstmonth.Text = "October"
                    secondmonth.Text = "November"
                    thirdmonth.Text = "December"

                    objForecast.firstMonth = 10
                    objForecast.secondMonth = 11
                    objForecast.thirdMonth = 12
                End If

                Dim dtForecast As DataTable
                objForecast.UserCntID = Session("UserContactID")
                objForecast.DomainID = Session("DomainID")
                objForecast.byteMode = 0
                objForecast.IntYear = intYear
                objForecast.Type = 1
                dtForecast = objForecast.GetForeCastOpp
                Dim i As Integer
                closed1.Attributes.Add("onclick", "return OpenWindow('" & ddlQuarterFR.SelectedItem.Value + 1 & "','" & objForecast.IntYear & "','5')")
                closed2.Attributes.Add("onclick", "return OpenWindow('" & ddlQuarterFR.SelectedItem.Value + 1 & "','" & objForecast.IntYear & "','5')")
                closed3.Attributes.Add("onclick", "return OpenWindow('" & ddlQuarterFR.SelectedItem.Value + 1 & "','" & objForecast.IntYear & "','5')")

                pipeline1.Attributes.Add("onclick", "return OpenWindow('" & ddlQuarterFR.SelectedItem.Value + 1 & "','" & objForecast.IntYear & "','6')")
                pipeline2.Attributes.Add("onclick", "return OpenWindow('" & ddlQuarterFR.SelectedItem.Value + 1 & "','" & objForecast.IntYear & "','6')")
                pipeline3.Attributes.Add("onclick", "return OpenWindow('" & ddlQuarterFR.SelectedItem.Value + 1 & "','" & objForecast.IntYear & "','6')")

                For i = 0 To dtForecast.Rows.Count - 1
                    If i = 0 Then
                        If Not IsDBNull(dtForecast.Rows(i)("sold")) Then closed1.Text = Format(dtForecast.Rows(i)("sold"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("Pipeline")) Then pipeline1.Text = Format(dtForecast.Rows(i)("Pipeline"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("Probability")) Then prob1.Text = Format(dtForecast.Rows(i)("Probability"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("ProbableAmt")) Then probAmt1.Text = Format(dtForecast.Rows(i)("ProbableAmt"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("QuotaAmt")) Then txtQuota1.Text = Format(dtForecast.Rows(i)("QuotaAmt"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("ForeCastAmount")) Then txtForAmt1.Text = Format(dtForecast.Rows(i)("ForeCastAmount"), "#,##0.00")
                    End If
                    If i = 1 Then
                        If Not IsDBNull(dtForecast.Rows(i)("sold")) Then closed2.Text = Format(dtForecast.Rows(i)("sold"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("Pipeline")) Then pipeline2.Text = Format(dtForecast.Rows(i)("Pipeline"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("Probability")) Then prob2.Text = Format(dtForecast.Rows(i)("Probability"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("ProbableAmt")) Then probAmt2.Text = Format(dtForecast.Rows(i)("ProbableAmt"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("QuotaAmt")) Then txtQuota2.Text = Format(dtForecast.Rows(i)("QuotaAmt"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("ForeCastAmount")) Then txtForAmt2.Text = Format(dtForecast.Rows(i)("ForeCastAmount"), "#,##0.00")
                    End If
                    If i = 2 Then
                        If Not IsDBNull(dtForecast.Rows(i)("sold")) Then closed3.Text = Format(dtForecast.Rows(i)("sold"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("Pipeline")) Then pipeline3.Text = Format(dtForecast.Rows(i)("Pipeline"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("Probability")) Then prob3.Text = Format(dtForecast.Rows(i)("Probability"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("ProbableAmt")) Then probAmt3.Text = Format(dtForecast.Rows(i)("ProbableAmt"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("QuotaAmt")) Then txtQuota3.Text = Format(dtForecast.Rows(i)("QuotaAmt"), "#,##0.00")
                        If Not IsDBNull(dtForecast.Rows(i)("ForeCastAmount")) Then txtForAmt3.Text = Format(dtForecast.Rows(i)("ForeCastAmount"), "#,##0.00")
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ForecastItemReport()
            Try
                intYear = ddlYearFI.SelectedItem.Value
                txtQuotaFI1.Text = ""
                txtQuotaFI2.Text = ""
                txtQuotaFI3.Text = ""
                txtForQFI1.Text = ""
                txtForQFI2.Text = ""
                txtForQFI3.Text = ""
                If objForecast Is Nothing Then objForecast = New Forecast
                If ddlQuarterFI.SelectedItem.Value = 0 Then  ' if it is first quarter
                    lblMonthFI1.Text = "January"
                    lblMonthFI2.Text = "February"
                    lblMonthFI3.Text = "March"

                    objForecast.firstMonth = 1
                    objForecast.secondMonth = 2
                    objForecast.thirdMonth = 3
                ElseIf ddlQuarterFI.SelectedItem.Value = 1 Then ' if it is second quarter
                    lblMonthFI1.Text = "April"
                    lblMonthFI2.Text = "May"
                    lblMonthFI3.Text = "June"
                    objForecast.firstMonth = 4
                    objForecast.secondMonth = 5
                    objForecast.thirdMonth = 6
                ElseIf ddlQuarterFI.SelectedItem.Value = 2 Then ' if it is thrid quarter
                    lblMonthFI1.Text = "July"
                    lblMonthFI2.Text = "August"
                    lblMonthFI3.Text = "September"
                    objForecast.firstMonth = 7
                    objForecast.secondMonth = 8
                    objForecast.thirdMonth = 9
                ElseIf ddlQuarterFI.SelectedItem.Value = 3 Then ' if it is fourth quarter
                    lblMonthFI1.Text = "October"
                    lblMonthFI2.Text = "November"
                    lblMonthFI3.Text = "December"
                    objForecast.firstMonth = 10
                    objForecast.secondMonth = 11
                    objForecast.thirdMonth = 12
                End If

                Dim i As Integer
                If ddlItemFI.SelectedItem.Value <> 0 Then
                    Dim dtForecast As DataTable
                    objForecast.UserCntID = Session("UserContactID")
                    objForecast.DomainID = Session("DomainID")
                    objForecast.byteMode = 0
                    objForecast.IntYear = intYear
                    objForecast.Type = 1
                    objForecast.ItemCode = ddlItemFI.SelectedItem.Value
                    dtForecast = objForecast.GetForecastItems

                    lblSoldFI1.Text = Format(dtForecast.Rows(0).Item("sold"), "#,##0")
                    lblSoldFI2.Text = Format(dtForecast.Rows(1).Item("sold"), "#,##0")
                    lblSoldFI3.Text = Format(dtForecast.Rows(2).Item("sold"), "#,##0")

                    lblPipeFI1.Text = Format(dtForecast.Rows(0).Item("Pipeline"), "#,##0")
                    lblPipeFI2.Text = Format(dtForecast.Rows(1).Item("Pipeline"), "#,##0")
                    lblPipeFI3.Text = Format(dtForecast.Rows(2).Item("Pipeline"), "#,##0")

                    lblPropFI1.Text = Format(dtForecast.Rows(0).Item("Probability"), "#,##0.00")
                    lblPropFI2.Text = Format(dtForecast.Rows(1).Item("Probability"), "#,##0.00")
                    lblPropFI3.Text = Format(dtForecast.Rows(2).Item("Probability"), "#,##0.00")

                    lblProbUnitsFI1.Text = Format(dtForecast.Rows(0).Item("ProbableAmt"), "#,##0")
                    lblProbUnitsFI2.Text = Format(dtForecast.Rows(1).Item("ProbableAmt"), "#,##0")
                    lblProbUnitsFI3.Text = Format(dtForecast.Rows(2).Item("ProbableAmt"), "#,##0")

                    txtQuotaFI1.Text = Format(dtForecast.Rows(0).Item("QuotaAmt"), "#,##0")
                    txtQuotaFI2.Text = Format(dtForecast.Rows(1).Item("QuotaAmt"), "#,##0")
                    txtQuotaFI3.Text = Format(dtForecast.Rows(2).Item("QuotaAmt"), "#,##0")

                    txtForQFI1.Text = Format(dtForecast.Rows(0).Item("ForeCastAmount"), "#,##0")
                    txtForQFI2.Text = Format(dtForecast.Rows(1).Item("ForeCastAmount"), "#,##0")
                    txtForQFI3.Text = Format(dtForecast.Rows(2).Item("ForeCastAmount"), "#,##0")
                Else
                    lblSoldFI1.Text = 0
                    lblSoldFI2.Text = 0
                    lblSoldFI3.Text = 0

                    lblPipeFI1.Text = 0
                    lblPipeFI2.Text = 0
                    lblPipeFI3.Text = 0

                    lblPropFI1.Text = 0
                    lblPropFI2.Text = 0
                    lblPropFI3.Text = 0

                    lblProbUnitsFI1.Text = 0
                    lblProbUnitsFI2.Text = 0
                    lblProbUnitsFI3.Text = 0

                    lblMonthFI1.Text = ""
                    lblMonthFI2.Text = ""
                    lblMonthFI3.Text = ""

                    txtQuotaFI1.Text = 0
                    txtQuotaFI2.Text = 0
                    txtQuotaFI3.Text = 0

                    txtForQFI1.Text = 0
                    txtForQFI2.Text = 0
                    txtForQFI3.Text = 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ForecastingOppHstr()
            Try
                Dim dtForecast As DataTable
                If objForecast Is Nothing Then objForecast = New Forecast
                objForecast.UserCntID = Session("UserContactID")
                objForecast.DomainID = Session("DomainID")
                objForecast.byteMode = 1
                objForecast.IntYear = ddlYearFRH.SelectedValue
                objForecast.Type = 1

                If ddlQFRH.SelectedItem.Value = 0 Then  ' if it is first quarter
                    lblMFRH1.Text = "January"
                    lblMFRH2.Text = "February"
                    lblMFRH3.Text = "March"
                    objForecast.firstMonth = 1
                    objForecast.secondMonth = 2
                    objForecast.thirdMonth = 3
                ElseIf ddlQFRH.SelectedItem.Value = 1 Then ' if it is second quarter
                    lblMFRH1.Text = "April"
                    lblMFRH2.Text = "May"
                    lblMFRH3.Text = "June"
                    objForecast.firstMonth = 4
                    objForecast.secondMonth = 5
                    objForecast.thirdMonth = 6
                ElseIf ddlQFRH.SelectedItem.Value = 2 Then ' if it is thrid quarter
                    lblMFRH1.Text = "July"
                    lblMFRH2.Text = "August"
                    lblMFRH3.Text = "September"
                    objForecast.firstMonth = 7
                    objForecast.secondMonth = 8
                    objForecast.thirdMonth = 9
                ElseIf ddlQFRH.SelectedItem.Value = 3 Then ' if it is fourth quarter
                    lblMFRH1.Text = "October"
                    lblMFRH2.Text = "November"
                    lblMFRH3.Text = "December"
                    objForecast.firstMonth = 10
                    objForecast.secondMonth = 11
                    objForecast.thirdMonth = 12
                End If
                dtForecast = objForecast.GetForeCastOpp

                If dtForecast.Rows.Count > 0 Then
                    lblQFRH1.Text = String.Format("{0:#,##0.00}", CDec(dtForecast.Rows(0).Item("QuotaAmt")))
                    lblQFRH2.Text = String.Format("{0:#,##0.00}", CDec(dtForecast.Rows(1).Item("QuotaAmt")))
                    lblQFRH3.Text = String.Format("{0:#,##0.00}", CDec(dtForecast.Rows(2).Item("QuotaAmt")))

                    lblFFRH1.Text = String.Format("{0:#,##0.00}", dtForecast.Rows(0).Item("ForeCastAmount"))
                    lblFFRH2.Text = String.Format("{0:#,##0.00}", dtForecast.Rows(1).Item("ForeCastAmount"))
                    lblFFRH3.Text = String.Format("{0:#,##0.00}", dtForecast.Rows(2).Item("ForeCastAmount"))
                Else
                    lblQFRH1.Text = ""
                    lblQFRH2.Text = ""
                    lblQFRH3.Text = ""

                    lblFFRH1.Text = ""
                    lblFFRH2.Text = ""
                    lblFFRH3.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ForecastingItemHstr()
            Try
                If objForecast Is Nothing Then objForecast = New Forecast
                If ddlItemFI.SelectedItem.Value <> 0 Then
                    Dim dtForecast As DataTable
                    objForecast.UserCntID = Session("UserContactID")
                    objForecast.DomainID = Session("DomainID")
                    objForecast.byteMode = 0
                    objForecast.IntYear = ddlYearFIH.SelectedItem.Value
                    objForecast.Type = 1
                    objForecast.ItemCode = ddlItemFI.SelectedItem.Value
                    If ddlQuarterFIH.SelectedItem.Value = 0 Then  ' if it is first quarter
                        lblMonthFIH1.Text = "January"
                        lblMonthFIH2.Text = "February"
                        lblMonthFIH3.Text = "March"
                        objForecast.firstMonth = 1
                        objForecast.secondMonth = 2
                        objForecast.thirdMonth = 3
                    ElseIf ddlQuarterFIH.SelectedItem.Value = 1 Then ' if it is second quarter
                        lblMonthFIH1.Text = "April"
                        lblMonthFIH2.Text = "May"
                        lblMonthFIH3.Text = "June"
                        objForecast.firstMonth = 4
                        objForecast.secondMonth = 5
                        objForecast.thirdMonth = 6
                    ElseIf ddlQuarterFIH.SelectedItem.Value = 2 Then ' if it is thrid quarter
                        lblMonthFIH1.Text = "July"
                        lblMonthFIH2.Text = "August"
                        lblMonthFIH3.Text = "September"
                        objForecast.firstMonth = 7
                        objForecast.secondMonth = 8
                        objForecast.thirdMonth = 9
                    ElseIf ddlQuarterFIH.SelectedItem.Value = 3 Then ' if it is fourth quarter
                        lblMonthFIH1.Text = "October"
                        lblMonthFIH2.Text = "November"
                        lblMonthFIH3.Text = "December"
                        objForecast.firstMonth = 10
                        objForecast.secondMonth = 11
                        objForecast.thirdMonth = 12
                    End If
                    dtForecast = objForecast.GetForecastItems
                    If dtForecast.Rows.Count > 0 Then

                        lblQuotaFIH1.Text = String.Format("{0:#,###.##}", CDec(dtForecast.Rows(0).Item("QuotaAmt")))
                        lblQuotaFIH2.Text = String.Format("{0:#,###.##}", CDec(dtForecast.Rows(1).Item("QuotaAmt")))
                        lblQuotaFIH3.Text = String.Format("{0:#,###.##}", CDec(dtForecast.Rows(2).Item("QuotaAmt")))

                        lblForQFIH1.Text = String.Format("{0:#,###.##}", dtForecast.Rows(0).Item("ForeCastAmount"))
                        lblForQFIH2.Text = String.Format("{0:#,###.##}", dtForecast.Rows(1).Item("ForeCastAmount"))
                        lblForQFIH3.Text = String.Format("{0:#,###.##}", dtForecast.Rows(2).Item("ForeCastAmount"))
                    Else
                        lblQuotaFIH1.Text = ""
                        lblQuotaFIH2.Text = ""
                        lblQuotaFIH3.Text = ""

                        lblForQFIH1.Text = ""
                        lblForQFIH2.Text = ""
                        lblForQFIH3.Text = ""
                    End If
                Else

                    lblQuotaFIH1.Text = ""
                    lblQuotaFIH2.Text = ""
                    lblQuotaFIH3.Text = ""

                    lblForQFIH1.Text = ""
                    lblForQFIH2.Text = ""
                    lblForQFIH3.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objForecast Is Nothing Then objForecast = New Forecast
                Dim ds As New DataSet
                Dim str As String
                GetValues()
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable.Copy)
                str = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                objForecast.strForecast = str
                objForecast.UserCntID = Session("UserContactID")
                objForecast.DomainID = Session("DomainId")
                If radOppTab.SelectedIndex = 0 Then
                    objForecast.ForecastType = 1
                    objForecast.Year = ddlYearFR.SelectedItem.Value
                    objForecast.Quarter = ddlQuarterFR.SelectedItem.Value
                Else
                    objForecast.ForecastType = 2
                    objForecast.Year = ddlYearFI.SelectedItem.Value
                    objForecast.Quarter = ddlQuarterFI.SelectedItem.Value
                    objForecast.ItemCode = ddlItemFI.SelectedItem.Value
                End If
                objForecast.SaveForeCast()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                If objForecast Is Nothing Then objForecast = New Forecast
                If radOppTab.SelectedIndex = 0 Then
                    objForecast.ForecastType = 1
                    objForecast.UserCntID = Session("UserContactID")
                    objForecast.Year = ddlYearFR.SelectedItem.Value
                    objForecast.Quarter = ddlQuarterFR.SelectedItem.Value
                    objForecast.DomainID = Session("DomainId")
                    objForecast.DeleteForeCast()
                    txtQuota1.Text = ""
                    txtQuota2.Text = ""
                    txtQuota3.Text = ""

                    txtForAmt1.Text = ""
                    txtForAmt2.Text = ""
                    txtForAmt3.Text = ""
                Else
                    objForecast.ForecastType = 2
                    objForecast.UserCntID = Session("UserContactID")
                    objForecast.Year = ddlYearFI.SelectedItem.Value
                    objForecast.Quarter = ddlQuarterFI.SelectedItem.Value
                    objForecast.DomainID = Session("DomainId")
                    objForecast.ItemCode = ddlItemFI.SelectedItem.Value
                    objForecast.DeleteForeCast()
                    txtQuotaFI1.Text = ""
                    txtQuotaFI2.Text = ""
                    txtQuotaFI3.Text = ""

                    txtForQFI1.Text = ""
                    txtForQFI2.Text = ""
                    txtForQFI3.Text = ""
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub createColumns()
            Try
                dtTable.Columns.Add("sintYear")
                dtTable.Columns.Add("tintquarter")
                dtTable.Columns.Add("tintMonth")
                dtTable.Columns.Add("monQuota")
                dtTable.Columns.Add("monForecastAmount")
                If radOppTab.SelectedIndex = 1 Then dtTable.Columns.Add("numItemCode")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GetValues()
            Try
                Dim dr As DataRow
                createColumns()
                If radOppTab.SelectedIndex = 0 Then
                    dr = dtTable.NewRow
                    dr("sintYear") = ddlYearFR.SelectedItem.Value
                    dr("tintquarter") = ddlQuarterFR.SelectedItem.Value
                    dr("tintMonth") = (ddlQuarterFR.SelectedItem.Value) * 3 + 1
                    dr("monQuota") = IIf(txtQuota1.Text = "", 0, txtQuota1.Text)
                    dr("monForecastAmount") = IIf(txtForAmt1.Text = "", 0, txtForAmt1.Text)
                    dtTable.Rows.Add(dr)
                    dr = dtTable.NewRow
                    dr("sintYear") = ddlYearFR.SelectedItem.Value
                    dr("tintquarter") = ddlQuarterFR.SelectedItem.Value
                    dr("tintMonth") = (ddlQuarterFR.SelectedItem.Value) * 3 + 2
                    dr("monQuota") = IIf(txtQuota2.Text = "", 0, txtQuota2.Text)
                    dr("monForecastAmount") = IIf(txtForAmt2.Text = "", 0, txtForAmt2.Text)
                    dtTable.Rows.Add(dr)
                    dr = dtTable.NewRow
                    dr("sintYear") = ddlYearFR.SelectedItem.Value
                    dr("tintquarter") = ddlQuarterFR.SelectedItem.Value
                    dr("tintMonth") = (ddlQuarterFR.SelectedItem.Value) * 3 + 3
                    dr("monQuota") = IIf(txtQuota3.Text = "", 0, txtQuota3.Text)
                    dr("monForecastAmount") = IIf(txtForAmt3.Text = "", 0, txtForAmt3.Text)
                    dtTable.Rows.Add(dr)
                Else
                    dr = dtTable.NewRow
                    dr("sintYear") = ddlYearFI.SelectedItem.Value
                    dr("tintquarter") = ddlQuarterFI.SelectedItem.Value
                    dr("tintMonth") = (ddlQuarterFI.SelectedItem.Value) * 3 + 1
                    dr("monQuota") = IIf(txtQuotaFI1.Text = "", 0, txtQuotaFI1.Text)
                    dr("monForecastAmount") = IIf(txtForQFI1.Text = "", 0, txtForQFI1.Text)
                    dr("numItemCode") = ddlItemFI.SelectedItem.Value
                    dtTable.Rows.Add(dr)
                    dr = dtTable.NewRow
                    dr("sintYear") = ddlYearFI.SelectedItem.Value
                    dr("tintquarter") = ddlQuarterFI.SelectedItem.Value
                    dr("tintMonth") = (ddlQuarterFI.SelectedItem.Value) * 3 + 2
                    dr("monQuota") = IIf(txtQuotaFI2.Text = "", 0, txtQuotaFI2.Text)
                    dr("monForecastAmount") = IIf(txtForQFI2.Text = "", 0, txtForQFI2.Text)
                    dr("numItemCode") = ddlItemFI.SelectedItem.Value
                    dtTable.Rows.Add(dr)
                    dr = dtTable.NewRow
                    dr("sintYear") = ddlYearFI.SelectedItem.Value
                    dr("tintquarter") = ddlQuarterFI.SelectedItem.Value
                    dr("tintMonth") = (ddlQuarterFI.SelectedItem.Value) * 3 + 3
                    dr("monQuota") = IIf(txtQuotaFI3.Text = "", 0, txtQuotaFI3.Text)
                    dr("monForecastAmount") = IIf(txtForQFI3.Text = "", 0, txtForQFI3.Text)
                    dr("numItemCode") = ddlItemFI.SelectedItem.Value
                    dtTable.Rows.Add(dr)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
   
        Private Sub ddlYearFR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYearFR.SelectedIndexChanged
            Try
                ForecastOppReport()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlQuarterFR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuarterFR.SelectedIndexChanged
            Try
                ForecastOppReport()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlYearFRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYearFRH.SelectedIndexChanged
            Try
                ForecastingOppHstr()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlQFRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQFRH.SelectedIndexChanged
            Try
                ForecastingOppHstr()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlYearFI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYearFI.SelectedIndexChanged
            Try
                ForecastItemReport()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlQuarterFI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuarterFI.SelectedIndexChanged
            Try
                ForecastItemReport()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlYearFIH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYearFIH.SelectedIndexChanged
            Try
                ForecastingItemHstr()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlQuarterFIH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuarterFIH.SelectedIndexChanged
            Try
                ForecastingItemHstr()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlItemFI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItemFI.SelectedIndexChanged
            Try
                ForecastItemReport()
                ForecastingItemHstr()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace