<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmForeCasting.aspx.vb"
    Inherits="BACRM.UserInterface.ForeCasting.frmForeCasting" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>ForeCasting</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">
        function Save() {
            var radTab = $find("radOppTab");

            var selectedIndex = Math.abs(radTab.get_selectedIndex());
            // alert(tabindex);

            if (selectedIndex == 1) {

                if (document.getElementById('ddlItemFI').value == 0) {
                    alert("Please Select Item");
                    radTab.set_selectedIndex(1);
                    document.getElementById('ddlItemFI').focus();
                    return false;
                }
            }
        }

        function OpenWindow(a, b, c) {
            window.open('../prospects/frmOpenDivisionInfo.aspx?Quarter=' + a + '&Year=' + b + '&type=' + c, '', 'toolbar=no,titlebar=no,top=200,left=200,scrollbars=yes,resizable=yes')
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Forecasting&nbsp;<a href="#" onclick="return OpenHelpPopUp('forecasting/frmforecasting.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server"
    ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
        MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="Forecasting (Revenue)" Value="ForecastingRevenue"
                PageViewID="radPageView_ForecastingRevenue">
            </telerik:RadTab>
            <telerik:RadTab Text="Forecasting (Units)" Visible="false"
                Value="ForecastingUnits" PageViewID="radPageView_ForecastingUnits">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_ForecastingRevenue" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Year:</label>
                            <asp:DropDownList ID="ddlYearFR" runat="server" CssClass="form-control" Width="120" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>Quarter:</label>
                            <asp:DropDownList ID="ddlQuarterFR" TabIndex="2" runat="server" CssClass="form-control" AutoPostBack="true">
                                <asp:ListItem Value="0" Selected="true">First Quarter</asp:ListItem>
                                <asp:ListItem Value="1">Second Quarter</asp:ListItem>
                                <asp:ListItem Value="2">Third Quarter</asp:ListItem>
                                <asp:ListItem Value="3">Fourth Quarter</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table width="100%" class="table table-bordered table-responsive">
                            <tr>
                                <th>Month
                                </th>
                                <th>Quota
                                </th>
                                <th>Sold
                                </th>
                                <th>Opportunity Pipeline
                                </th>
                                <th>Opportunity Probability %
                                </th>
                                <th>Opportunity Probable Amount
                                </th>
                                <th>Forecast Amount
                                </th>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="firstmonth" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtQuota1" runat="server" CssClass="form-control" Width="60">
                                    </asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:HyperLink ID="closed1" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                                <td align="center">
                                    <asp:HyperLink ID="pipeline1" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                                <td align="center">
                                    <asp:Label ID="prob1" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="probAmt1" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtForAmt1" runat="server" CssClass="form-control" Width="60">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr class="tr1">
                                <td align="center">
                                    <asp:Label ID="secondmonth" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtQuota2" runat="server" CssClass="form-control" Width="60">
                                    </asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:HyperLink ID="closed2" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                                <td align="center">
                                    <asp:HyperLink ID="pipeline2" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                                <td align="center">
                                    <asp:Label ID="prob2" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="probAmt2" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtForAmt2" runat="server" CssClass="form-control" Width="60">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="thirdmonth" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtQuota3" runat="server" CssClass="form-control" Width="60">
                                    </asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:HyperLink ID="closed3" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                                <td align="center">
                                    <asp:HyperLink ID="pipeline3" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                                <td align="center">
                                    <asp:Label ID="prob3" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="probAmt3" runat="server"></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtForAmt3" runat="server" CssClass="form-control" Width="60">
                                    </asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header">
                            <h3 class="box-title">Forecasting History</h3>

                            <div class="pull-right">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label>Year:</label>
                                            <asp:DropDownList ID="ddlYearFRH" runat="server" CssClass="form-control" Width="120" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        &nbsp;&nbsp;
                                        <div class="form-group">
                                            <label>Quarter:</label>
                                            <asp:DropDownList ID="ddlQFRH" TabIndex="2" runat="server" CssClass="form-control" AutoPostBack="true">
                                                <asp:ListItem Value="0" Selected="true">First Quarter</asp:ListItem>
                                                <asp:ListItem Value="1">Second Quarter</asp:ListItem>
                                                <asp:ListItem Value="2">Third Quarter</asp:ListItem>
                                                <asp:ListItem Value="3">Fourth Quarter</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table width="100%" class="table table-bordered table-responsive">
                                            <tr>
                                                <th>Month
                                                </th>
                                                <th>Quota
                                                </th>
                                                <th>Forecast Amount
                                                </th>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblMFRH1" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblQFRH1" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblFFRH1" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblMFRH2" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblQFRH2" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblFFRH2" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblMFRH3" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblQFRH3" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblFFRH3" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ForecastingUnits" runat="server">
            <asp:Table ID="tblForUnits" CellPadding="0" CellSpacing="0" Height="400" runat="server"
                CssClass="aspTable" Width="100%" GridLines="None">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <br>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                        <tr>
                                            <td align="right">Item
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlItemFI" runat="server" CssClass="form-control" Width="250" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right">Year
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlYearFI" runat="server" CssClass="form-control" Width="100" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right">Quarter
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlQuarterFI" TabIndex="2" runat="server" CssClass="form-control"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="0" Selected="true">First Quarter</asp:ListItem>
                                                    <asp:ListItem Value="1">Second Quarter</asp:ListItem>
                                                    <asp:ListItem Value="2">Third Quarter</asp:ListItem>
                                                    <asp:ListItem Value="3">Fourth Quarter</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                        <tr>
                                            <td class="normalbol">Month
                                            </td>
                                            <td class="normalbol">Quota
                                            </td>
                                            <td class="normalbol">Sold
                                            </td>
                                            <td class="normalbol">Opportunity Pipeline
                                            </td>
                                            <td class="normalbol">Opportunity Probability %
                                            </td>
                                            <td class="normalbol">Opportunity Probable Units
                                            </td>
                                            <td class="normalbol">Forecast Quantity
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblMonthFI1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtQuotaFI1" runat="server" CssClass="form-control" Width="60">
                                                </asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblSoldFI1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPipeFI1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPropFI1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblProbUnitsFI1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtForQFI1" runat="server" CssClass="form-control" Width="60">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="tr1">
                                            <td align="center">
                                                <asp:Label ID="lblMonthFI2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtQuotaFI2" runat="server" CssClass="form-control" Width="60">
                                                </asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblSoldFI2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPipeFI2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPropFI2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblProbUnitsFI2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtForQFI2" runat="server" CssClass="form-control" Width="60">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblMonthFI3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtQuotaFI3" runat="server" CssClass="form-control" Width="60">
                                                </asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblSoldFI3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPipeFI3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblPropFI3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblProbUnitsFI3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtForQFI3" runat="server" CssClass="form-control" Width="60">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="text_bold">
                                    <br>
                                    <br>
                                    &nbsp;Forecasting History
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                        <tr>
                                            <td align="right">Year:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlYearFIH" runat="server" CssClass="form-control" Width="100" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right">Quarter:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlQuarterFIH" TabIndex="2" runat="server" CssClass="form-control"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="0" Selected="true">First Quarter</asp:ListItem>
                                                    <asp:ListItem Value="1">Second Quarter</asp:ListItem>
                                                    <asp:ListItem Value="2">Third Quarter</asp:ListItem>
                                                    <asp:ListItem Value="3">Fourth Quarter</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                        <tr>
                                            <td class="normalbol">Month
                                            </td>
                                            <td class="normalbol">Quota
                                            </td>
                                            <td class="normalbol">Forecast Quantity
                                            </td>
                                        </tr>
                                        <tr height="20">
                                            <td align="center">
                                                <asp:Label ID="lblMonthFIH1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuotaFIH1" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblForQFIH1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="tr1" height="20">
                                            <td align="center">
                                                <asp:Label ID="lblMonthFIH2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuotaFIH2" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblForQFIH2" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr height="20">
                                            <td align="center">
                                                <asp:Label ID="lblMonthFIH3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuotaFIH3" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblForQFIH3" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
