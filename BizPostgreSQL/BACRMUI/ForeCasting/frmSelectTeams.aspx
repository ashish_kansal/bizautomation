<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSelectTeams.aspx.vb"
    Inherits="BACRM.UserInterface.ForeCasting.frmSelectTeams" MasterPageFile="~/common/Popup.Master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Select Teams</title>
    <script language="javascript" type="text/javascript">
        function MoveUp(tbox) {

            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i - 1].value;
                    tbox.options[i].text = tbox.options[i - 1].text;
                    tbox.options[i - 1].value = SelectedValue;
                    tbox.options[i - 1].text = SelectedText;
                }
            }
            return false;
        }
        function MoveDown(tbox) {

            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i + 1].value;
                    tbox.options[i].text = tbox.options[i + 1].text;
                    tbox.options[i + 1].value = SelectedValue;
                    tbox.options[i + 1].text = SelectedText;
                }
            }
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1); y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function Save() {
            var str = '';
            for (var i = 0; i < document.form1.lstTeamAdd.options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.form1.lstTeamAdd.options[i].value;
                SelectedText = document.form1.lstTeamAdd.options[i].text;
                str = str + SelectedValue + ','
            }
            document.form1.hdnValue.value = str;

        }
        function Close() {
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table align="right">
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnClose" runat="server" Text="Close" Width="50" CssClass="button">
                </asp:Button>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Select Teams
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:Table ID="tblTeams" runat="server" BorderColor="black" GridLines="None" Width="100%"
        CellSpacing="0" CellPadding="0" BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell>
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            Teams
                        </td>
                        <td class="normal1" nowrap>
                            Available Teams<br>
                            <br>
                            <asp:ListBox ID="lstTeamAvail" runat="server" Width="200" Height="180" CssClass="signup"
                                SelectionMode="Multiple"></asp:ListBox>
                        </td>
                        <td align="center">
                            <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add >"></asp:Button>
                            <br>
                            <asp:Button ID="btnRemove" CssClass="button" runat="server" Text="< Remove"></asp:Button>
                        </td>
                        <td class="normal1">
                            The user will ONLY be able to access
                            <br>
                            records from the following teams.<br>
                            <asp:ListBox ID="lstTeamAdd" Width="200" Height="180" runat="server" CssClass="signup"
                                SelectionMode="Multiple"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="hdnValue" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
