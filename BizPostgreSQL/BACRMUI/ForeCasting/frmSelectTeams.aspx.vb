Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Forecasting
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.ForeCasting


    Public Class frmSelectTeams
        Inherits BACRMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblTeams As System.Web.UI.WebControls.Table
        Protected WithEvents ddlTerritory As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnRemove As System.Web.UI.WebControls.Button
        Protected WithEvents ddlTerritoryAdd As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstTeamAvail As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstTeamAdd As System.Web.UI.WebControls.ListBox
        Protected WithEvents hdnValue As System.Web.UI.WebControls.TextBox

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim objUserAccess As New UserAccess
                    Dim dtTeamUsers As DataTable
                    objUserAccess.UserCntID = Session("UserContactID")
                    objUserAccess.DomainID = Session("DomainID")
                    dtTeamUsers = objUserAccess.GetTeamForUsers
                    lstTeamAvail.DataSource = dtTeamUsers
                    lstTeamAvail.DataTextField = "vcData"
                    lstTeamAvail.DataValueField = "numlistitemid"
                    lstTeamAvail.DataBind()

                    Dim objForeCast As New Forecast
                    Dim dtTeamForForecast As DataTable
                    objForeCast.UserCntID = Session("UserContactID")
                    objForeCast.DomainID = Session("DomainID")
                    objForeCast.ForecastType = GetQueryStringVal( "Type")
                    dtTeamForForecast = objForeCast.GetTeamsForForRep
                    lstTeamAdd.DataSource = dtTeamForForecast
                    lstTeamAdd.DataTextField = "vcData"
                    lstTeamAdd.DataValueField = "numlistitemid"
                    lstTeamAdd.DataBind()
                End If
                btnAdd.Attributes.Add("OnClick", "return move(document.form1.lstTeamAvail,document.form1.lstTeamAdd)")
                btnRemove.Attributes.Add("OnClick", "return remove1(document.form1.lstTeamAdd,document.form1.lstTeamAvail)")
                btnSave.Attributes.Add("onclick", "Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim ObjForeCast As New Forecast
                ObjForeCast.strTeam = hdnValue.Text
                ObjForeCast.UserCntID = Session("UserContactID")
                ObjForeCast.DomainID = Session("DomainID")
                ObjForeCast.ForecastType = GetQueryStringVal( "Type")
                ObjForeCast.ManageTeamsForForRept()
                Response.Write("<script>opener.PopupCheck(); self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace

