﻿Imports BACRM.BusinessLogic.Common
Imports Google.Apis.Auth.OAuth2.Flows
Imports Google.Apis.Auth.OAuth2.Web
Imports Google.Apis.Auth.OAuth2
Imports Google.Apis.Oauth2.v2
Imports BACRM.BusinessLogic.Admin
Imports System.Threading
Imports System.Security.Cryptography.X509Certificates

Public Class GoogleMailAuthentication
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim uri = CCommon.ToString(Request.Url)

            If uri.Contains("?") Then
                uri = uri.Substring(0, uri.IndexOf("?"))
            End If


            Dim flow As IAuthorizationCodeFlow = New OfflineGmailAuthorizationCodeFlow(New OfflineGmailAuthorizationCodeFlow.Initializer() With { _
                        .ClientSecrets = New ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}, _
                        .DataStore = Nothing, _
                        .Scopes = New String() {"https://mail.google.com/", Oauth2Service.Scope.UserinfoEmail} _
                    })

            If Not Page.IsPostBack Then
                If Not String.IsNullOrEmpty(GetQueryStringVal("code")) Then
                    Dim token = flow.ExchangeCodeForTokenAsync("Bizautomation.com", CCommon.ToString(GetQueryStringVal("code")), uri, CancellationToken.None).Result

                    Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = New ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}}), "Bizautomation.com", token)
                    Dim oauthService As New Oauth2Service(New Google.Apis.Services.BaseClientService.Initializer() With {.HttpClientInitializer = credentials})

                    Dim userInfo As Data.Userinfoplus = oauthService.Userinfo.Get().ExecuteAsync().Result
                    Dim userEmail As String = userInfo.Email

                    Dim objUserAccess As New UserAccess
                    objUserAccess.UserCntID = Session("GoogleContactId")
                    objUserAccess.DomainID = Session("DomainID")
                    objUserAccess.Email = userEmail
                    objUserAccess.UpdateMailRefreshToken(1, token.AccessToken, token.RefreshToken, 0)

                    Response.Write("<script>self.close();</script>")
                ElseIf Not String.IsNullOrEmpty(GetQueryStringVal("error")) Then
                    Dim errorMessage As String = CCommon.ToString(GetQueryStringVal("error"))
                    Dim errorDescription As String = CCommon.ToString(GetQueryStringVal("error_description"))

                    btnContinue.Visible = False
                    DisplayError(errorMessage & ": " & errorDescription)
                Else
                    lblInstruction.Text = "You will be redirected to Google Mail login page for authorization on continue click.<br /><br /> Please login using your email address ""<b>" & Session("GoogleEmailId") & "</b>"" email and say ""Allow"" when prompted for authorization."
                End If
            Else
                Dim result = New AuthorizationCodeWebApp(flow, uri, uri).AuthorizeAsync("Bizautomation.com", CancellationToken.None).Result

                If result.RedirectUri IsNot Nothing Then
                    Response.Redirect(result.RedirectUri)
                End If
            End If

        Catch ex As Google.Apis.Auth.OAuth2.Responses.TokenResponseException
            If ex.Error.Error = "invalid_grant" Then
                lblInstruction.Text = "Access to Google Mail is already enabled. If you are trying again, you have to first remove access from your Google Account -> Security -> Google apps with account access > Click Manage and remove BizAutomation."
            Else
                btnContinue.Visible = False
                DisplayError(ex.Error.Error & ": " & ex.Error.ErrorDescription)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#Region "Private Methods"

    Private Sub DisplayError(ByVal exception As String)
        Try
            lblPageError.Text = exception
            divPageError.Style.Add("display", "")
            divPageError.Focus()
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Public Class OfflineGmailAuthorizationCodeFlow
        Inherits GoogleAuthorizationCodeFlow
        Public Sub New(initializer As GoogleAuthorizationCodeFlow.Initializer)
            MyBase.New(initializer)
        End Sub

        Public Overrides Function CreateAuthorizationCodeRequest(redirectUri As String) As Google.Apis.Auth.OAuth2.Requests.AuthorizationCodeRequestUrl
            Return New Google.Apis.Auth.OAuth2.Requests.GoogleAuthorizationCodeRequestUrl(New Uri(AuthorizationServerUrl)) With { _
                .ClientId = ClientSecrets.ClientId, _
                .Scope = String.Join(" ", Scopes), _
                .RedirectUri = redirectUri, _
                .AccessType = "offline", _
                .ApprovalPrompt = "force" _
            }
        End Function
    End Class

End Class