''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmRecurringTemplateList
    Inherits BACRMPage

    Dim objRecurringEvents As RecurringEvents
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            'To Set Permission
            GetUserRightsForPage(13, 44)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
            If Not IsPostBack Then LoadGridDetails()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub LoadGridDetails()
        Dim dtTempDetails As DataTable
        Try
            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
            objRecurringEvents.DomainId = Session("DomainId")
            dtTempDetails = objRecurringEvents.GetTemplateDetails()
            dgRecurringTempList.DataSource = dtTempDetails
            dgRecurringTempList.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgRecurringTempList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRecurringTempList.ItemCommand
        Try
            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
            If e.CommandName = "Edit" Then
                Response.Redirect("../Accounting/frmRecurringEvents.aspx?RecurringId=" & e.Item.Cells(0).Text)
            ElseIf e.CommandName = "Delete" Then
                objRecurringEvents.RecurringId = e.Item.Cells(0).Text
                objRecurringEvents.DomainId = Session("DomainId")
                If objRecurringEvents.DeleteRecurringTemplate() = False Then litMessage.Text = "Dependent Records Exists. Cannot Be deleted"
                LoadGridDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgRecurringTempList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRecurringTempList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim btnDelete As New Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = CType(e.Item.FindControl("btnDelete"), Button)
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnNewTemplate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNewTemplate.Click
        Response.Redirect("frmRecurringEvents.aspx", False)
    End Sub
End Class