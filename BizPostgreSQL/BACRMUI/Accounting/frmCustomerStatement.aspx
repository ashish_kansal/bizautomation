﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomerStatement.aspx.vb"
    Inherits=".frmCustomerStatement" MasterPageFile="~/common/PopUp.Master" ClientIDMode="Static"
    ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Customer Statement</title>
    <script type="text/javascript">
        //function OpenOpp(a, b) {
        //    //var str;
        //    //str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
        //    //window.opener.reDirectPage(str);
        //    if (b == '0') {
        //        str = "../Accounting/frmNewJournalEntry.aspx?JournalId=" + a;
        //    }
        //    else {
        //        var str;
        //        str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
        //    }
        //    opener.top.frames['mainframe'].location.href = str;
        //    return;
        //}
        function OpenOpp(a, b, c) {
            var str;

            if (a > 0) {
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=1";
            }
            else if (b > 0) {
                str = "../Accounting/frmNewJournalEntry.aspx?JournalId=" + b;
            }
            else if (c > 0) {
                //if (d == 3) {
                str = "../opportunity/frmReturnDetail.aspx?ReturnID=" + c;
                //}
                //else {
                //    str = "../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + c;
                //}
            }

            opener.top.frames['mainframe'].location.href = str;
            return;
        }

        function OpenBizInvoice(a, b, c) {
            if (a > 0) {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            //else if(c > 0) {
            //    window.open('../opportunity/frmReturnDetail.aspx?ReturnID=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            //}
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            if (b > 0) {
                var BalanceAmt = c;
                RPPopup = window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
                console.log(RPPopup.name);
            }
            return false;
        }

        function GrabHTML() {
            document.getElementById('hdnHTML').value = "";
            document.getElementById('hdnHTML').value = document.getElementById("divHtml").innerHTML.replace(/https/gi, "http"); //case in-sensetive replace
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="right">
        <tr>
            <td>
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                    Text="Close" />
            </td>
            <td>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Customer Statement
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:ImageButton runat="server" ID="ibtnSendEmail" AlternateText="Email as PDF" ToolTip="Email as PDF"
                    OnClientClick="return GrabHTML();" ImageUrl="~/images/Email.png" CommandName="Email" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <div id="divHtml">
                    <table cellpadding="2" cellspacing="2" border="0" width="100%">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: larger" colspan="2">
                                <span>Customer Statement</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                    <tr>
                                        <td class="normal" style="font-weight: bold" align="center" colspan="2"><u>Customer Information</u>
                                        </td>
                                        <td class="normal" style="font-weight: bold" align="center" colspan="2"><u>Vendor Information</u>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal" width="25%" style="font-weight: bold" align="right">Company Name :
                                        </td>
                                        <td width="25%">
                                            <asp:Label runat="server" ID="lblCompanyNameAddress">
                                            </asp:Label>
                                        </td>
                                        <td class="normal" width="25%" style="font-weight: bold" align="right">Company Name :
                                        </td>
                                        <td width="25%">
                                            <asp:Label runat="server" ID="lblVendorCompanyNameAddress">
                                            </asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="normal" style="font-weight: bold" align="right" valign="top">Billing Address :
                                        </td>
                                        <td align="left">
                                            <asp:Label runat="server" ID="lblBillingAddress">
                                            </asp:Label>
                                        </td>
                                        <td class="normal" style="font-weight: bold" align="right" valign="top">Billing Address :
                                        </td>
                                        <td align="left">
                                            <asp:Label runat="server" ID="lblVendorBillingAddress">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="normal" width="25%" style="font-weight: bold" align="right">Statement Date :
                                        </td>
                                        <td width="25%">
                                            <asp:Label runat="server" ID="lblStatementDate">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" align="center">Current Amount Due
                            </td>
                            <td style="font-weight: bold" align="center">Past Due
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="center">
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td width="60%" align="right">Today-30 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblthirtyField1" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">31-60 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSixtyField1" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">61-90 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblNintyField1" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">Over 91 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblTwentyField1" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" align="center">
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td width="60%" align="right">Today-30 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblthirtyField2"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">31-60 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSixtyField2"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">61-90 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblNintyField2"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">Over 91 :
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblTwentyField2"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <asp:TextBox ID="txtConEmail" runat="server" Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdnContactId" />
                                <asp:TextBox ID="txtBizDocTemplate" runat="server" Style="display: none" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvARDetails" runat="server" AutoGenerateColumns="False" CssClass="dg"
                                                Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true">
                                                <AlternatingRowStyle CssClass="ais" />
                                                <RowStyle CssClass="is" />
                                                <HeaderStyle CssClass="hs" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Order Id" SortExpression="vcPOppName">
                                                        <ItemTemplate>
                                                            <%--<a href="javascript:void(0)" onclick="OpenOpp('<%#IIf(BACRM.BusinessLogic.Common.CCommon.ToInteger(Eval("numOppId")) = 0 OrElse BACRM.BusinessLogic.Common.CCommon.ToInteger(Eval("numOppId")) = -1, Eval("numJournal_Id"), Eval("numOppId"))%>','<%# Eval("numoppbizdocsid") %>');">
                                                                <%# Eval("vcPOppName")%>
                                                            </a>--%>
                                                            <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("numJournal_Id")%>','<%# Eval("numReturnHeaderID")%>')">
                                                                <%# Eval("vcPOppName") %>
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Invoice Id" SortExpression="vcbizdocid">
                                                        <ItemTemplate>
                                                            <a href="javascript:void(0)" onclick="OpenBizInvoice('<%#Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>','<%# Eval("numReturnHeaderID")%>');">
                                                                <%#IIf(Eval("numOppId") = 0 Or Eval("numOppId") = -1, Eval("numReturnHeaderID"), Eval("vcBizDocID"))%>
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Order Amount">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("TotalAmount"))%>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Balance Due">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <ItemTemplate>
                                                            <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue")) + "</b></font>",Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue"))) %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Billing Date" DataField="dtFromDate" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField HeaderText="Due Date" DataField="DueDate" ItemStyle-HorizontalAlign="Left" />
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No Record Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <span id="spnSplit" style="display: none"></span>
                <asp:HiddenField runat="server" ID="hdnHTML" />
            </td>
        </tr>
    </table>
</asp:Content>
