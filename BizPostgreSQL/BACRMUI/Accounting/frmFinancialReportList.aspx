﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFinancialReportList.aspx.vb"
    Inherits=".frmFinancialReportList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript" language="javascript">
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function PriceTable(a) {
            window.open("../Items/frmPricingTable.aspx?RuleID=" + a, '', 'toolbar=no,titlebar=no,top=200,width=400,height=600,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenItem(a) {
            window.location.href = '../Items/frmKitDetails.aspx?ItemCode=' + a;
            return false;
        }
        function OpenFormWizard(url) {
            if (url == null) {
                url = "frmFinancialReportWizard.aspx";
            }

            window.open(url, '', 'toolbar=no,titlebar=no,top=200,width=400,height=600,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <a href="#" onclick="OpenFormWizard()" class="btn btn-primary" >New Report</a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Financial Reports&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmfinancialreportlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgFinReport" AllowSorting="false" runat="server" Width="100%" CssClass="table table-striped table-bordered"
                    AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numFRID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="<font >Report Name</font>">
                            <ItemTemplate>
                                <a onclick="OpenFormWizard('frmFinancialReportWizard.aspx?FRID=<%# Eval("numFRID")%>')"><%# Eval("vcReportName")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="FinancialView" HeaderText="<font >Financial View</font>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="FinancialDimension" HeaderText="<font >Financial Dimension</font>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="DateRange" HeaderText="<font >Date Range</font>"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:Linkbutton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:Linkbutton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
           <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
