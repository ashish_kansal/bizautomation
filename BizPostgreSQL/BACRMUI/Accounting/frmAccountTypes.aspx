﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountTypes.aspx.vb"
    Inherits=".frmAccountTypes" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Accounts Types</title>
    <script type="text/javascript">
        function onNodeClicking(sender, args) {
            UnCheckAll();
            var node = args.get_node();
            if (node.get_checked() == true)
                node.set_checked(false);
            else
                node.set_checked(true);
            document.getElementById('AccountTypeId').value = node.get_value();
        }
        function UnCheckAll() {
            var tree = $find("RadTreeView1");
            for (var i = 0; i < tree.get_allNodes().length; i++) {
                var node = tree.get_allNodes()[i];
                node.set_checked(false);
            }
        }
        function onNodeChecked(sender, args) {
            var node = args.get_node();
            if (node.get_checked() == true) {
                UnCheckAll();
                node.set_checked(true);
                node.select();
                document.getElementById('AccountTypeId').value = node.get_value();
            }
            else {
                UnCheckAll();
                node.unselect();
                document.getElementById('AccountTypeId').value = 0;
            }

        }
        function OpenNewAccountType() {
            window.open("../Accounting/frmNewAccountType.aspx", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=200,scrollbars=no,resizable=yes')
            return false;
        }

        function EditSelected() {
            if (document.getElementById('AccountTypeId').value == "") {
                alert("Please select one account type");
                return false;
            }
            window.open('../Accounting/frmNewAccountType.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AcntTypeId=' + document.getElementById('AccountTypeId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=220,scrollbars=no,resizable=yes');
            return false;
        }
        function DeleteSelected() {
            if (confirm('Are you sure you want to delete selected account type?') == true) {
                if (document.getElementById('AccountTypeId').value == "") {
                    alert("Please select one account type");
                    return false;
                }
                return true;
            }
            else
                return false;
        }
        function reload() {
            document.form1.btnRefresh.click();
            return false;
        }
        function treeExpandAllNodes() {
            var treeView = $find("RadTreeView1");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }

        function treeCollapseAllNodes() {
            var treeView = $find("RadTreeView1");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSaveHierarchy" runat="server" CssClass="btn btn-primary" OnClick="btnSaveHierarchy_Click"><i class="fa fa-plus"></i>&nbsp;&nbsp;Save Hierarchy</asp:LinkButton>
                <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add New</asp:LinkButton>
                <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table id="table3" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td colspan="1" class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Types Of Accounts&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmaccounttypes.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" class="text">
                <a href="javascript: treeExpandAllNodes();" style="color: Gray;">Expand All </a>
                &nbsp; <a href="javascript: treeCollapseAllNodes();" style="color: Gray;">Collapse All
                </a>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <telerik:RadTreeView ID="RadTreeView1" runat="server" AllowNodeEditing="false" Skin="Default"
                    CheckBoxes="True" OnClientNodeClicking="onNodeClicking" OnNodeDrop="RadTreeView1_NodeDrop" EnableDragAndDrop="true" EnableDragAndDropBetweenNodes="true" OnClientNodeChecked="onNodeChecked"
                    ClientIDMode="Static">
                </telerik:RadTreeView>
            </td>
        </tr>
    </table>
    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" />
    <asp:HiddenField ID="AccountTypeId" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
