﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmProfitLossReport.aspx.vb" Inherits=".frmProfitLossReport" ValidateRequest="false" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .radtreelist .rtltable {
            border: none !important;
            border-collapse: collapse;
        }

        .RadTreeList_Default {
            border-color: #cbcbcb !important;
        }

            .RadTreeList_Default .rtlHeader {
                color: #000 !important;
                background: #e5e5e5 !important;
                height: 30px;
            }

                .radtreelist_default .rtlheader, .radtreelist_default .rtlheader th {
                    border-collapse: collapse;
                }

                    .radtreelist_default .rtlheader th {
                        border: 1px solid #cbcbcb !important;
                        border-collapse: collapse;
                    }

        .RadTreeList .rtlHeader th {
            font-weight: 900 !important;
            border-color: #d9d9d9 !important;
            border-width: 0px 0px 3px 1px !important;
        }

        .rtlR, .rtlA {
            height: 30px;
        }

        .rtlR {
            background-color: #f2f2f2 !important;
        }

        .rtlA {
            background-color: #fff !important;
        }

        .linkColor {
            color: Green;
            cursor: pointer;
        }

        .rcbInput {
            height: 19px !important;
        }

        .RadTreeList_Default tr:not(.rtlHeader):hover {
            color: #000 !important;
            background: #FFFFDE !important;
        }

        .RadTreeList_Default .rtlRSel {
            color: #000 !important;
            background: #FFFFDE !important;
        }

            .RadTreeList_Default .rtlRSel td {
                border-color: #d9d9d9 !important;
            }

        .RadInput .riTextBox, html body .RadInputMgr {
            border-width: 1px;
            border-style: solid;
            padding: 2px 1px 3px;
            vertical-align: middle;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_endRequest(function () {

                $(".rtlR").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () {
                        $(this).find("#imgGL").hide();
                    }
                );

                $(".rtlA").hover(
                        function () {
                            if ($(this).find(".linkColor").length > 0) {
                                $(this).find("#imgGL").show();
                            }
                        },
                        function () {
                            $(this).find("#imgGL").hide();
                        }
                );
            });

            $(".rtlR").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () {
                        $(this).find("#imgGL").hide();
                    }
            );

            $(".rtlA").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () {
                        $(this).find("#imgGL").hide();
                    }
            );
        });

        function ExpandCollapse(id) {
            try {
                var uniqueCode = id.getAttribute("UniqueID")
                var className = id.className;
                var newClassName;

                if (className.indexOf("rtlCollapse") >= 0) {
                    newClassName = className.replace("rtlCollapse", "rtlExpand");
                    id.className = newClassName;
                } else if (className.indexOf("rtlExpand") >= 0) {
                    newClassName = className.replace("rtlExpand", "rtlCollapse");
                    id.className = newClassName;
                }


                if (id != null) {
                    $('[id*=rtlProfitLoss] > table > tbody > tr').each(function () {
                        if ($(this).attr("UniqueID") != uniqueCode && $(this).attr("UniqueID").indexOf(uniqueCode) >= 0) {
                            var btn = $(this).find("[id*=ExpandCollapseButton]")

                            if (className.indexOf("rtlCollapse") >= 0) {
                                $(this).hide();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlCollapse", "rtlExpand");
                                }

                            } else if (className.indexOf("rtlExpand") >= 0) {
                                $(this).show();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlExpand", "rtlCollapse");
                                }
                            }
                        }
                    });
                }
            }
            catch (ex) {
            }
            finally {
                return false;
            }
        }

        function OpenMonthySummary(a, b, c, d, e) {
            var fromDate = $("#hdnFromDate").val();
            var toDate = $("#hdnToDate").val();
            var accountClassID = 0;

            if ($("#ddlUserLevelClass").length > 0) {
                accountClassID = $("#ddlUserLevelClass").val();
            }

            if (e == 1 || e == 3) {
                if (a == -1) {
                    window.location.href = '../Accounting/frmJournalEntry.aspx?ModeID=2&FromDate=' + fromDate + '&ToDate=' + toDate + '&frm=frmProfitLoss' + "&From=" + fromDate + "&To=" + toDate + "&AccountClassID=" + accountClassID;
                }
                else {
                    window.location.href = '../Accounting/frmMonthlySummary.aspx?ChartAcntId=' + a + '&Opening=' + b + '&Code=' + c + '&frm=frmProfitLoss' + '&Name=' + d + "&From=" + fromDate + "&To=" + toDate + "&AccountClassID=" + accountClassID;
                }
            }
        }

        function OpenGLReport(a, b) {
            var fromDate = $("#hdnFromDate").val();
            var toDate = $("#hdnToDate").val();
            var accountClassID = 0;

            if ($("#ddlUserLevelClass").length > 0) {
                accountClassID = $("#ddlUserLevelClass").val();
            }

            window.location.href = "../Accounting/frmGeneralLedger.aspx?Mode=4&AcntTypeID=" + a + "&Month=0&AccountID=" + b + "&From=" + fromDate + "&To=" + toDate + "&AccountClassID=" + accountClassID;
            return false;
        }

        function GenerateExportHTML() {
            try {
                $("#txtExportHtml").val("");
                var html = "<table>";

                $('[id*=rtlProfitLoss] tr:visible').each(function () {
                    var bold = false;
                    if ($(this).css("font-weight") == "bold") {
                        bold = true;
                    }

                    html = html + "<tr>";
                    var htmlExpandCollapseTD = ""
                    $(this).find($(this).find("th").length > 0 ? "th" : "td").each(function () {

                        if ($(this) != null && $(this).attr("class") != null && $(this).attr("class").indexOf("rtlL") != -1) {
                            htmlExpandCollapseTD = htmlExpandCollapseTD + "     ";

                        } else {
                            html = html + "<td bold=\"" + (bold ? 1 : 0) + "\">";
                            html = html + htmlExpandCollapseTD + $(this).text().trim();
                            html = html + "</td>";
                            htmlExpandCollapseTD = ""
                        }
                    });
                    html = html + "</tr>";
                });

                html = html + "</table>";

                $("#txtExportHtml").val(html);

                return true;
            } catch (ex) {
                alert("Error ocurred");
                return false
            }
        }

        function PrintReport() {
            try {
                var html = "<table  style=\"width:100%; padding:0px; border-collapse:collapse;\">";
                var i = 1;
                var j = 1;
                $('[id*=rtlProfitLoss] tr:visible').each(function () {
                    if (i == 1) {
                        html = html + "<tr style=\"font-weight:bold;background-color:lightgray; text-align:center;\">";
                    } else {
                        if ($(this).css("font-weight") == "bold") {
                            html = html + "<tr style=\"font-weight:bold;\">";
                        } else {
                            html = html + "<tr>";
                        }
                    }

                    var htmlExpandCollapseTD = ""
                    $(this).find($(this).find("th").length > 0 ? "th" : "td").each(function () {

                        if (i == 1) {
                            html = html + "<td style=\"border:1px solid #000;\">";
                            html = html + $(this).text().trim();
                            html = html + "</td>";
                        } else {
                            if ($(this) != null && $(this).attr("class") != null && $(this).attr("class").indexOf("rtlL") != -1) {
                                htmlExpandCollapseTD = htmlExpandCollapseTD + "&nbsp;&nbsp;";
                            } else {
                                html = html + "<td style=\"border:1px solid #000;" + (j == 1 ? "width:50%;" : "text-align:right;") + "\">";
                                html = html + htmlExpandCollapseTD + $(this).text().trim();
                                html = html + "</td>";
                                htmlExpandCollapseTD = ""
                                j = j + 1;
                            }
                        }
                    });
                    html = html + "</tr>";

                    i = 0;
                    j = 1;
                });

                html = html + "</table>";


                var headstr = "<html><head><title></title></head><body><div style=\"width:100%; text-align:center; font-weight:bold; font-size:20px;\">";
                if ($("[id$=hdnCustomerName]").val() != "") {
                    headstr = headstr + $("[id$=hdnCustomerName]").val() + "<br/>";
                } else {
                    headstr = headstr + $("#hdnDomainName").val() + "<br/>";
                }
                headstr = headstr + "Profit & Loss Accounts <br/>";
                var radcombobox = $find('<%=radCmbCompany.ClientID%>');
                if (radcombobox.get_selectedItem() != null) {
                    headstr = headstr + radcombobox.get_selectedItem().get_text() + "<br/>";
                }
                headstr = headstr + $("#hdnDateRange").val() + "<br/>";
                headstr = headstr + "</div>";
                var footstr = "</body>";
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + html + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                document.location.reload();
                return false;
            } catch (ex) {
                alert("error occured");
                return false;
            }
        }

        function PDFReport() {
            try {
                $("#txtExportHtml").val("");
                var html = "<table  style=\"margin:0 auto; padding:0px; border-collapse:collapse;\">";
                var i = 1;
                var j = 1;
                $('[id*=rtlProfitLoss] tr:visible').each(function () {
                    var tr = $(this);
                    if (i == 1) {
                        html = html + "<tr style=\"font-weight:bold;background-color:lightgray; text-align:center;height:28px\">";
                    } else if ($(this).is(":last-child")) {
                        html = html + "<tr style=\"font-weight:bold;background-color:lightgray; text-align:center;height:28px\">";
                    } else {
                        if ($(this).css("font-weight") == "bold") {
                            html = html + "<tr style=\"font-weight:bold;page-break-inside:avoid;height:28px\">";
                        } else {
                            html = html + "<tr style=\"page-break-inside: avoid;height:28px\">";
                        }
                    }

                    var htmlExpandCollapseTD = "";
                    var htmlExpandCollapseAmount = 2;
                    $(this).find($(this).find("th").length > 0 ? "th" : "td").each(function () {
                        if (i == 1) {
                            html = html + "<td>";
                            html = html + $(this).text().trim();
                            html = html + "</td>";
                        } else {
                            if ($(this) != null && $(this).attr("class") != null && $(this).attr("class").indexOf("rtlL") != -1) {
                                htmlExpandCollapseTD = htmlExpandCollapseTD + "&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else {
                                if (tr.css("font-weight") == "bold" && j != 1) {
                                    if (tr.is(":last-child")) {
                                        html = html + "<td style=\"text-align:right;\">";
                                    } else {
                                        html = html + "<td style=\"text-align:right;border-bottom:1px solid #000;border-top:1px solid #000;\">";
                                    }
                                } else if (j == 1) {
                                    html = html + "<td style='padding:right:20px;'>";
                                } else {
                                    html = html + "<td style=\"text-align:right;padding-right:" + (htmlExpandCollapseAmount * 10 * 2) + "px;\">";
                                }

                                if (j == 1) {
                                    html = html + htmlExpandCollapseTD + $(this).text().trim()
                                } else {
                                    if (tr.css("font-weight") == "bold") {
                                        html = html + $(this).text().trim()
                                    } else {
                                        html = html + $(this).text().trim();
                                    }
                                }
                                html = html + "</td>";
                                htmlExpandCollapseTD = "";
                                j = j + 1;
                            }
                        }
                    });
                    html = html + "</tr>";

                    i = 0;
                    j = 1;
                    htmlExpandCollapseAmount = 2;
                });

                html = html + "</table>";

                $("#txtExportHtml").val("<html><head></head><body><table style=\"width:100%;padding:0px; border-collapse:collapse;font-family: Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;\"><tr><td style=\"text-align:center;\">" + html + "</td></tr></table></body></html>");
                return true;
            } catch (ex) {
                alert("Error ocurred");
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <asp:UpdatePanel ID="UpdatePanelMain" runat="server" ChildrenAsTriggers="true" class="row padbottom10">
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="pull-left">
                    <div class="form-inline">
                        <div class="form-group" runat="server" id="pnlAccountingClass">
                            <label>Class:</label>
                            <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="form-control" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>View:</label>
                            <asp:DropDownList ID="ddlColumnType" runat="server" AutoPostBack="true" CssClass="form-control">
                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Quarter" Value="Quarter"></asp:ListItem>
                                <asp:ListItem Text="Year" Value="Year"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label></label>
                            <asp:DropDownList ID="ddlDateRange" runat="server" AutoPostBack="true" CssClass="form-control">
                                <asp:ListItem Text="-- Select --" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Current Year" Value="CurYear"></asp:ListItem>
                                <asp:ListItem Text="Previous Year" Value="PreYear"></asp:ListItem>
                                <asp:ListItem Text="Previous and Current Year" Value="CurPreYear"></asp:ListItem>
                                <asp:ListItem Text="Current Quarter" Value="CuQur"></asp:ListItem>
                                <asp:ListItem Text="Previous Quarter" Value="PreQur"></asp:ListItem>
                                <asp:ListItem Text="Previous and Current Quarter" Value="CurPreQur"></asp:ListItem>
                                <asp:ListItem Text="Current Month" Value="ThisMonth"></asp:ListItem>
                                <asp:ListItem Text="Previous Month" Value="LastMonth"></asp:ListItem>
                                <asp:ListItem Text="Previous and Current Month" Value="CurPreMonth"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label></label>
                            <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization" AutoPostBack="true"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true" EmptyMessage="-- Select Customer --"
                                Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>From</label>
                            <telerik:RadDatePicker runat="server" ID="calFrom" Width="90" />
                        </div>
                        <div class="form-group">
                            <label>To</label>
                            <telerik:RadDatePicker ID="calTo" runat="server" Width="90" />
                        </div>
                        <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
                        <asp:LinkButton ID="imgBtnExportExcel" CssClass="btn btn-primary" runat="server" OnClientClick="return GenerateExportHTML()" ToolTip="Export to excel"><i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        <asp:LinkButton ID="imgBtnPrint" CssClass="btn btn-primary" runat="server" OnClientClick="return PrintReport()" ToolTip="Print"><i class="fa fa-print"></i></asp:LinkButton>
                        <asp:LinkButton ID="imgBtnExportPDF" CssClass="btn btn-primary" runat="server" OnClientClick="return PDFReport()" ToolTip="Export to pdf"><i class="fa fa-file-pdf-o"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
            <asp:TextBox ID="txtExportHtml" runat="server" Style="display: none;"></asp:TextBox>
            <asp:HiddenField ID="hdnDomainName" runat="server" />
            <asp:HiddenField ID="hdnCustomerName" runat="server" />
            <asp:HiddenField ID="hdnFromDate" runat="server" />
            <asp:HiddenField ID="hdnToDate" runat="server" />
            <asp:HiddenField ID="hdnDateRange" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="imgBtnExportExcel" />
            <asp:PostBackTrigger ControlID="imgBtnPrint" />
            <asp:PostBackTrigger ControlID="imgBtnExportPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    <asp:UpdatePanel ID="UpdatePanelTitle" runat="server" UpdateMode="Always">
        <ContentTemplate>
            Profit & Loss Accounts
            <asp:Label ID="lblPeriod" runat="server"></asp:Label>
            &nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmprofitlossreport.aspx')"><label class="badge bg-yellow">?</label></a>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdatePanel ID="UpdatePanelGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" class="row">
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <telerik:RadTreeList ID="rtlProfitLoss" runat="server" AutoGenerateColumns="false" ParentDataKeyNames="ParentId" DataKeyNames="vcCompundParentKey"
                        OnItemCommand="rtlProfitLoss_ItemCommand" OnItemDataBound="rtlProfitLoss_ItemDataBound" Width="100%" Height="100%">
                        <Columns>
                            <telerik:TreeListTemplateColumn HeaderText="Account" UniqueName="vcAccountType" HeaderStyle-Width="250" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <span onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','0','<%#Eval("vcAccountCode")%>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountType").ToString()) %>','<%# Eval("Type")%>');">
                                        <%#IIf(Eval("Type") > 1, "<b>" & Eval("vcAccountType").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountType") & "</span>")%>
                                    </span>
                                    <span class="GLReportSpan">
                                        <img id="imgGL" alt='General Ledger' height='16px' width='16px' onclick='return OpenGLReport(<%#Eval("ParentId") %>,<%#Eval("numAccountID") %>)' title='General Ledger' src='../images/GLReport.png' style='display: none;'></span>
                                </ItemTemplate>
                            </telerik:TreeListTemplateColumn>
                        </Columns>
                        <ClientSettings Selecting-AllowItemSelection="true">
                            <Scrolling AllowScroll="true" />
                        </ClientSettings>
                    </telerik:RadTreeList>
                </div>
            </div>
            <asp:HiddenField ID="hdnMaxLevel" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
