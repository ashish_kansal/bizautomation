﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports System.IO
Imports System.Text
Imports ClosedXML.Excel
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports BACRM.BusinessLogic.Admin

Public Class frmIncomeExpenseNew
    Inherits BACRMPage

#Region "Member Variables"
    Private mobjProfitLoss As ProfitLoss
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Add(ddlColumnType.ID, ddlColumnType.SelectedValue)
            PersistTable.Add(ddlDateRange.ID, ddlDateRange.SelectedValue)
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack Then
                GetUserRightsForPage(35, 91)
                BindUserLevelClassTracking()

                Dim lobjGeneralLedger As New GeneralLedger
                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now) ' DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)

                If GetQueryStringVal("FromDate") <> "" And GetQueryStringVal("ToDate") <> "" Then

                    calFrom.SelectedDate = CDate(Replace(GetQueryStringVal("FromDate"), "%27", ""))
                    calTo.SelectedDate = CDate(Replace(GetQueryStringVal("ToDate"), "%27", ""))
                Else
                    If PersistTable.Count > 0 Then
                        ddlColumnType.SelectedValue = PersistTable(ddlColumnType.ID)
                        ddlDateRange.SelectedValue = PersistTable(ddlDateRange.ID)
                        ddlUserLevelClass.SelectedValue = PersistTable(ddlUserLevelClass.ID)

                        Try
                            calFrom.SelectedDate = PersistTable(calFrom.ID)
                            calTo.SelectedDate = PersistTable(calTo.ID)

                            If calFrom.SelectedDate Is Nothing Or calTo.SelectedDate Is Nothing Then
                                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now)
                            End If
                        Catch ex As Exception
                            'Do not throw error when date format which is stored in persist table and Current date formats are different
                            calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now)
                        End Try

                        'Clears date range selection if value selected in date range drop down
                        If ddlDateRange.SelectedValue <> "0" Then
                            calFrom.SelectedDate = Nothing
                            calTo.SelectedDate = Nothing
                        End If
                    End If
                End If

                hdnDomainName.Value = CCommon.ToString(Session("DomainName"))

                If ddlDateRange.SelectedValue <> "0" Then
                    Dim fromDate As Date
                    Dim toToDate As Date

                    Select Case ddlDateRange.SelectedValue
                        Case "CurYear"
                            fromDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "PreYear"
                            fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "CurPreYear"
                            fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "CuQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth")))
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                        Case "PreQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddDays(-1)
                        Case "CurPreQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                        Case "ThisMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                            toToDate = fromDate.AddMonths(1).AddDays(-1)
                        Case "LastMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                            toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
                        Case "CurPreMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                            toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1)
                    End Select

                    SetDateRange(fromDate, toToDate)
                Else
                    SetDateRange(calFrom.SelectedDate, calTo.SelectedDate)
                End If

                BindReport()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindReport()
        Try
            If rtlIncomeExpense.Columns.Count > 1 Then
                For i As Int32 = 1 To rtlIncomeExpense.Columns.Count - 1
                    rtlIncomeExpense.Columns.RemoveAt(1)
                Next
            End If

            rtlIncomeExpense.DataSource = GetReportData(False)
            rtlIncomeExpense.DataBind()
            rtlIncomeExpense.ExpandAllItems()

            If ddlColumnType.SelectedValue = "Year" Then
                rtlIncomeExpense.Columns(0).HeaderStyle.Width = New Unit(260, UnitType.Pixel)
            Else
                rtlIncomeExpense.Columns(0).HeaderStyle.Width = New Unit(100, UnitType.Percentage)
            End If

            lblPeriod.Text = "(" & hdnDateRange.Value & ")"

            UpdatePanelGrid.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function GetReportData(ByVal isAddTotalRow As Boolean) As DataTable
        Try
            Dim type As String = Nothing

            If ddlColumnType.SelectedValue = "Quarter" Then
                type = "Quarter"
            ElseIf ddlColumnType.SelectedValue = "Year" Then
                type = "Year"
            End If

            If mobjProfitLoss Is Nothing Then mobjProfitLoss = New ProfitLoss
            mobjProfitLoss.DomainID = Session("DomainID")

            If pnlAccountingClass.Visible AndAlso ddlUserLevelClass.Items.Count > 0 Then
                mobjProfitLoss.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
            Else
                mobjProfitLoss.AccountClass = 0
            End If

            mobjProfitLoss.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)

            Dim dateFilter As String
            If ddlDateRange.SelectedValue <> "0" Then
                dateFilter = ddlDateRange.SelectedValue
            Else
                dateFilter = "Custom"
                mobjProfitLoss.FromDate = calFrom.SelectedDate
                mobjProfitLoss.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            End If
            mobjProfitLoss.IsAddTotalRow = isAddTotalRow
            Dim dtIncomeExpense As DataTable = mobjProfitLoss.GetIncomeExpensesNew(dateFilter, type)

            Dim index As Int32 = 1
            Dim drNetIncome As DataRow = dtIncomeExpense.NewRow()
            drNetIncome("ParentId") = ""
            drNetIncome("vcCompundParentKey") = -3
            drNetIncome("numAccountTypeID") = -3
            drNetIncome("vcAccountType") = "Net Income"
            drNetIncome("vcAccountCode") = DBNull.Value
            drNetIncome("LEVEL") = 0
            drNetIncome("numAccountId") = DBNull.Value
            drNetIncome("Struc") = "#-3#"
            drNetIncome("Type") = 2
            drNetIncome("bitTotal") = 1
            drNetIncome("bitIsSubAccount") = 0

            For Each column As DataColumn In dtIncomeExpense.Columns
                If column.ColumnName <> "ParentId" And
                    column.ColumnName <> "vcCompundParentKey" And
                    column.ColumnName <> "numAccountTypeID" And
                    column.ColumnName <> "vcAccountType" And
                    column.ColumnName <> "vcAccountCode" And
                    column.ColumnName <> "LEVEL" And
                    column.ColumnName <> "numAccountId" And
                    column.ColumnName <> "Struc" And
                    column.ColumnName <> "Type" And
                    column.ColumnName <> "bitTotal" And
                    column.ColumnName <> "bitIsSubAccount" Then
                    Dim boundColumn As New TreeListBoundColumn
                    boundColumn.UniqueName = column.ColumnName
                    boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    boundColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                    boundColumn.DataFormatString = "{0:" & Session("Currency") & "#,##0.00;(" & Session("Currency") & "#,##0.00)}"
                    boundColumn.HeaderStyle.Width = New Unit("110")
                    boundColumn.DataField = column.ColumnName
                    boundColumn.HeaderText = column.ColumnName
                    rtlIncomeExpense.Columns.Insert(index, boundColumn)

                    Dim ordinaryIncome As Double = 0
                    Dim otherIncome As Double = 0

                    If dtIncomeExpense.Select("numAccountTypeID = -1").Length > 0 Then
                        ordinaryIncome = CCommon.ToDouble(dtIncomeExpense.Select("numAccountTypeID = -1")(0)(column))
                    End If

                    If dtIncomeExpense.Select("numAccountTypeID = -2").Length > 0 Then
                        otherIncome = CCommon.ToDouble(dtIncomeExpense.Select("numAccountTypeID = -2")(0)(column))
                    End If

                    drNetIncome(column) = ordinaryIncome + otherIncome

                    'Calculate Gross Profit
                    If dtIncomeExpense.Select("vcAccountType='Gross Profit' AND numAccountTypeID=-4 AND vcCompundParentKey=-4").Length > 0 Then
                        Dim drGrossProfit As DataRow = dtIncomeExpense.Select("vcAccountType='Gross Profit' AND numAccountTypeID=-4 AND vcCompundParentKey=-4")(0)

                        Dim income As Double = dtIncomeExpense.Select("vcAccountCode='0103'")(0)(column)
                        Dim Cogs As Double = dtIncomeExpense.Select("vcAccountCode='0106'")(0)(column)

                        drGrossProfit(column) = income - Cogs
                    End If

                    index = index + 1
                End If
            Next

            dtIncomeExpense.Rows.Add(drNetIncome)

            Return dtIncomeExpense
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub BindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False

            If CCommon.ToInteger(HttpContext.Current.Session("DefaultClassType")) > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()

                    Dim objItem As New System.Web.UI.WebControls.ListItem
                    objItem.Text = "-- Select One --"
                    objItem.Value = "0"

                    ddlUserLevelClass.Items.Insert(0, objItem)

                    pnlAccountingClass.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SetDateRange(ByVal fromDate As Date, ByVal toDate As Date)
        Try
            hdnDateRange.Value = "From " & FormattedDateFromDate(fromDate, Session("DateFormat")) & " to " & FormattedDateFromDate(toDate, Session("DateFormat"))
            hdnFromDate.Value = fromDate
            hdnToDate.Value = toDate
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function GetStartOfFinancialQtr(monthFinancialYearStartsOn As Integer) As DateTime
        Try
            Dim tempDate As Date = DateTime.Now.Date
            Dim actualMonth = tempDate.Month
            Dim financialYear = tempDate.Year
            Dim difference = actualMonth - monthFinancialYearStartsOn
            If difference < 0 Then
                financialYear -= 1
                difference += 12
            End If
            Dim quarter = Math.Floor(difference / 3)

            Return New DateTime(financialYear, monthFinancialYearStartsOn, 1).AddMonths(quarter * 3)
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

#Region "Event Handlers"

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Try
            If Not calFrom.SelectedDate Is Nothing AndAlso Not calTo.SelectedDate Is Nothing Then

                Dim FromDate As Date = calFrom.SelectedDate
                Dim ToDate As Date = calTo.SelectedDate

                If FromDate > ToDate Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "script", "alert('From date must be smaller than To Date');", True)
                    Exit Sub
                End If

                SetDateRange(calFrom.SelectedDate, calTo.SelectedDate)
                ddlDateRange.ClearSelection()
                lblPeriod.Text = "(" & hdnDateRange.Value & ")"

                BindReport()
            Else
                ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "script", "alert('From date and To Date is required.');", True)
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rtlIncomeExpense_ItemCommand(sender As Object, e As TreeListCommandEventArgs)
        Try
            If e.CommandName = RadTreeList.ExpandCollapseCommandName Then
                'WE ARE HANDLING EXPAND COLLAPSE CLIENT SIDE SO CODE IS NOT REQUIRED
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rtlIncomeExpense_ItemDataBound(sender As Object, e As TreeListItemDataBoundEventArgs)
        Try
            If e.Item.ItemType = TreeListItemType.Item Or e.Item.ItemType = TreeListItemType.AlternatingItem Then

                Dim btn As Button = TryCast(e.Item.FindControl("ExpandCollapseButton"), Button)
                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("Struc")

                If btn IsNot Nothing Then
                    btn.Attributes.Add("UniqueID", uniqueID)
                    btn.Attributes.Add("onclick", "return ExpandCollapse(" + btn.ClientID + ");")
                End If

                DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).Attributes.Add("UniqueID", uniqueID)

                If DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("Type") > 1 Then
                    e.Item.Font.Bold = True
                Else
                    e.Item.Font.Bold = False
                End If

                'Dim level As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("LEVEL")

                'If level <> 0 AndAlso hdnMaxLevel.Value = level Then
                '    e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
                'Else
                '    If level = 0 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#B0B0B0")
                '    ElseIf level = 1 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0")
                '    ElseIf level = 2 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#C8C8C8")
                '    ElseIf level = 3 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#D0D0D0")
                '    ElseIf level = 4 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#D8D8D8")
                '    ElseIf level = 5 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#E0E0E0")
                '    ElseIf level = 6 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("E8E8E8")
                '    ElseIf level = 7 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F0F0")
                '    ElseIf level = 8 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#F8F8F8")
                '    Else
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
                '    End If
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub imgBtnExportExcel_Click(sender As Object, e As EventArgs) Handles imgBtnExportExcel.Click
        Try
            Dim columnCount As Integer = 0
            Dim dtProfitLoss As DataTable = GetReportData(True)
            Dim netOperatingIncomeStartIndex As Integer = dtProfitLoss.Rows.IndexOf(dtProfitLoss.Select("numAccountTypeID = -1")(0)) + 1
            Dim netOtherIncomeStartIndex As Integer = dtProfitLoss.Rows.IndexOf(dtProfitLoss.Select("numAccountTypeID = -2")(0)) + 1
            dtProfitLoss.Rows.Remove(dtProfitLoss.Select("numAccountTypeID = -1")(0))
            dtProfitLoss.Rows.Remove(dtProfitLoss.Select("numAccountTypeID = -2")(0))
            dtProfitLoss.AcceptChanges()

            For Each column As DataColumn In dtProfitLoss.Columns
                If column.ColumnName <> "ParentId" AndAlso
                    column.ColumnName <> "vcCompundParentKey" AndAlso
                    column.ColumnName <> "numAccountTypeID" AndAlso
                    column.ColumnName <> "vcAccountCode" AndAlso
                    column.ColumnName <> "LEVEL" AndAlso
                    column.ColumnName <> "numAccountId" AndAlso
                    column.ColumnName <> "Struc" AndAlso
                    column.ColumnName <> "Type" AndAlso
                    column.ColumnName <> "bitTotal" And
                    column.ColumnName <> "bitIsSubAccount" Then
                    columnCount = columnCount + 1
                End If
            Next

            Dim fs As New MemoryStream()
            Dim workbook As New XLWorkbook
            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Income Expense Statement")

            'ADD HEADER
            If Not String.IsNullOrEmpty(hdnCustomerName.Value) Then
                workSheet.Cell(1, 1).Value = hdnCustomerName.Value
            Else
                workSheet.Cell(1, 1).Value = CCommon.ToString(Session("DomainName"))
            End If
            workSheet.Cell(1, 1).Style.Font.Bold = True
            workSheet.Cell(1, 1).Style.Font.FontSize = 14
            workSheet.Cell(1, 1).Style.Font.FontName = "Arial"
            workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            workSheet.Row(1).AdjustToContents()
            workSheet.Range(workSheet.Cell(1, 1), workSheet.Cell(1, columnCount)).Merge()

            workSheet.Cell(2, 1).Value = "Income Expense Statement"
            workSheet.Cell(2, 1).Style.Font.Bold = True
            workSheet.Cell(2, 1).Style.Font.FontSize = 14
            workSheet.Cell(2, 1).Style.Font.FontName = "Arial"
            workSheet.Cell(2, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            workSheet.Row(2).AdjustToContents()
            workSheet.Range(workSheet.Cell(2, 1), workSheet.Cell(2, columnCount)).Merge()

            workSheet.Cell(3, 1).Value = hdnDateRange.Value
            workSheet.Cell(3, 1).Style.Font.Bold = True
            workSheet.Cell(3, 1).Style.Font.FontSize = 10
            workSheet.Cell(3, 1).Style.Font.FontName = "Arial"
            workSheet.Cell(3, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            workSheet.Row(3).AdjustToContents()
            workSheet.Range(workSheet.Cell(3, 1), workSheet.Cell(3, columnCount)).Merge()

            Dim i As Int32 = 4
            Dim j As Int32 = 1

            For Each column As DataColumn In dtProfitLoss.Columns
                If column.ColumnName <> "ParentId" AndAlso
                    column.ColumnName <> "vcCompundParentKey" AndAlso
                    column.ColumnName <> "numAccountTypeID" AndAlso
                    column.ColumnName <> "vcAccountCode" AndAlso
                    column.ColumnName <> "LEVEL" AndAlso
                    column.ColumnName <> "numAccountId" AndAlso
                    column.ColumnName <> "Struc" AndAlso
                    column.ColumnName <> "Type" AndAlso
                    column.ColumnName <> "bitTotal" AndAlso
                    column.ColumnName <> "bitIsSubAccount" Then

                    If column.ColumnName <> "vcAccountType" Then
                        workSheet.Cell(i, j).SetValue(column.ColumnName)
                        workSheet.Cell(i, j).Style.Border.BottomBorder = XLBorderStyleValues.Thin
                    Else
                        workSheet.Cell(i, j).SetValue("")
                    End If

                    workSheet.Cell(i, j).Style.Font.Bold = True
                    workSheet.Cell(i, j).Style.Font.FontSize = 9
                    workSheet.Cell(i, j).Style.Font.FontName = "Arial"
                    workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                    j = j + 1
                End If
            Next

            Dim dataRowStartIndex As Integer = i
            Dim netIncomeFormula As String = "Column" & (dtProfitLoss.Rows.IndexOf(dtProfitLoss.Select("numAccountTypeID='-1'")(0)) + dataRowStartIndex + 1) & "+" & "Column" & (dtProfitLoss.Rows.IndexOf(dtProfitLoss.Select("numAccountTypeID='-2'")(0)) + dataRowStartIndex + 1)

            i = i + 1
            j = 1

            Dim level As Short = 0
            Dim positiveFormatTotal As String = "$* #,##0.00"
            Dim negativeFormatTotal As String = "$* (#,##0.00)"
            Dim zeroFormatTotal As String = "$* -_)"
            Dim accountingNumberFormatTotal As String = positiveFormatTotal + ";" + negativeFormatTotal + ";" + zeroFormatTotal

            Dim positiveFormat As String = "#,##0.00"
            Dim negativeFormat As String = "(#,##0.00)"
            Dim zeroFormat As String = "-_)"
            Dim accountingNumberFormat As String = positiveFormat + ";" + negativeFormat + ";" + zeroFormat

            Dim sumFormula As String = ""
            Dim accountTypeLevel As Integer
            Dim startIndex As String
            Dim endIndex As String

            For Each dr As DataRow In dtProfitLoss.Rows
                level = CCommon.ToShort(dr("LEVEL"))
                level = level - 1

                For Each column As DataColumn In dtProfitLoss.Columns
                    If column.ColumnName <> "ParentId" AndAlso
                        column.ColumnName <> "vcCompundParentKey" AndAlso
                        column.ColumnName <> "numAccountTypeID" AndAlso
                        column.ColumnName <> "vcAccountCode" AndAlso
                        column.ColumnName <> "LEVEL" AndAlso
                        column.ColumnName <> "numAccountId" AndAlso
                        column.ColumnName <> "Struc" AndAlso
                        column.ColumnName <> "Type" AndAlso
                        column.ColumnName <> "bitTotal" AndAlso
                        column.ColumnName <> "bitIsSubAccount" Then

                        If column.ColumnName = "vcAccountType" Then
                            While level > 0
                                dr(column.ColumnName) = "    " & dr(column.ColumnName)
                                level = level - 1
                            End While

                            workSheet.Cell(i, j).SetValue(CCommon.ToString(dr(column.ColumnName)))
                            workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left

                        Else
                            If CCommon.ToShort(dr("Type")) = 2 AndAlso Not CCommon.ToBool(dr("bitTotal")) AndAlso CCommon.ToLong(dr("numAccountId")) = 0 Then
                                workSheet.Cell(i, j).SetValue("")
                            Else
                                workSheet.Cell(i, j).SetValue(CCommon.ToDouble(dr(column.ColumnName)))
                                workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right
                                workSheet.Cell(i, j).SetDataType(XLCellValues.Number)
                                If CCommon.ToShort(dr("Type")) = 2 Then
                                    workSheet.Cell(i, j).Style.NumberFormat.Format = accountingNumberFormatTotal

                                    If CCommon.ToBool(dr("bitTotal")) Then
                                        workSheet.Cell(i, j).Style.Border.TopBorder = XLBorderStyleValues.Thin

                                        sumFormula = ""

                                        accountTypeLevel = CCommon.ToShort(dr("Level"))
                                        If CCommon.ToShort(dr("numAccountTypeID")) = -4 Then
                                            startIndex = netOperatingIncomeStartIndex
                                        ElseIf CCommon.ToShort(dr("numAccountTypeID")) = -1 Then
                                            startIndex = netOperatingIncomeStartIndex
                                        ElseIf CCommon.ToShort(dr("numAccountTypeID")) = -2 Then
                                            startIndex = netOtherIncomeStartIndex
                                        Else
                                            If dtProfitLoss.Select("Struc='" & CCommon.ToString(dr("Struc")).Replace("Total#", "") & "'").Length > 0 Then
                                                startIndex = dtProfitLoss.Rows.IndexOf(dtProfitLoss.Select("Struc='" & CCommon.ToString(dr("Struc")).Replace("Total#", "") & "'")(0))
                                            Else
                                                startIndex = -1
                                                workSheet.Cell(i, j).Style.Border.TopBorder = XLBorderStyleValues.None
                                            End If
                                        End If


                                        endIndex = dtProfitLoss.Rows.IndexOf(dr)

                                        If startIndex <> -1 Then
                                            If CCommon.ToLong(dr("numAccountID")) > 0 Then
                                                For index As Integer = startIndex To endIndex - 1
                                                    If (CCommon.ToShort(dtProfitLoss.Rows(index)("Level")) = (accountTypeLevel + 1) Or CCommon.ToShort(dtProfitLoss.Rows(index)("Level")) = accountTypeLevel) AndAlso Not (CCommon.ToShort(dtProfitLoss.Rows(index)("Type")) = 2 AndAlso Not CCommon.ToBool(dtProfitLoss.Rows(index)("bitTotal"))) Then
                                                        If dtProfitLoss.Select("Struc = '" & CCommon.ToString(dtProfitLoss.Rows(index)("Struc")) & "Total#' AND Struc <> '" & CCommon.ToString(dr("Struc")) & "'").Length = 0 Then
                                                            sumFormula = sumFormula + If(sumFormula.Length > 0, "+", "") + workSheet.Cell(dataRowStartIndex + index + 1, j).Address.ColumnLetter & workSheet.Cell(dataRowStartIndex + index + 1, j).Address.RowNumber
                                                        End If
                                                    End If
                                                Next
                                            Else
                                                For index As Integer = startIndex To endIndex - 1
                                                    If CCommon.ToShort(dtProfitLoss.Rows(index)("Level")) = (accountTypeLevel + 1) AndAlso Not (CCommon.ToShort(dtProfitLoss.Rows(index)("Type")) = 2 AndAlso Not CCommon.ToBool(dtProfitLoss.Rows(index)("bitTotal"))) Then
                                                        If CCommon.ToShort(dtProfitLoss.Rows(index)("numAccountId")) > 0 Then
                                                            If dtProfitLoss.Select("Struc = '" & CCommon.ToString(dtProfitLoss.Rows(index)("Struc")) & "Total#' AND Struc <> '" & CCommon.ToString(dr("Struc")) & "'").Length = 0 Then
                                                                sumFormula = sumFormula + If(sumFormula.Length > 0, "+", "") + workSheet.Cell(dataRowStartIndex + index + 1, j).Address.ColumnLetter & workSheet.Cell(dataRowStartIndex + index + 1, j).Address.RowNumber
                                                            End If
                                                        Else
                                                            sumFormula = sumFormula + If(sumFormula.Length > 0, "+", "") + workSheet.Cell(dataRowStartIndex + index + 1, j).Address.ColumnLetter & workSheet.Cell(dataRowStartIndex + index + 1, j).Address.RowNumber
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            

                                            If CCommon.ToLong(dr("numAccountTypeID")) = "-1" Then
                                                sumFormula = sumFormula.Replace("+", "-")
                                            ElseIf CCommon.ToLong(dr("numAccountTypeID")) = "-2" Then
                                                sumFormula = sumFormula.Replace("+", "-")
                                            ElseIf CCommon.ToLong(dr("numAccountTypeID")) = "-4" Then
                                                sumFormula = sumFormula.Replace("+", "-")
                                            End If

                                            If CCommon.ToLong(dr("numAccountTypeID")) = "-3" Then
                                                workSheet.Cell(i, j).FormulaA1 = "=" & netIncomeFormula.Replace("Column", workSheet.Cell(i, j).Address.ColumnLetter)
                                            Else
                                                workSheet.Cell(i, j).FormulaA1 = "=" & sumFormula
                                            End If
                                        End If
                                    End If
                                Else
                                    workSheet.Cell(i, j).Style.NumberFormat.Format = accountingNumberFormat
                                End If
                            End If
                        End If

                        workSheet.Cell(i, j).Style.Font.FontSize = 8
                        workSheet.Cell(i, j).Style.Font.FontName = "Arial"

                        If CCommon.ToShort(dr("Type")) = 2 Or column.ColumnName = "vcAccountType" Then
                            workSheet.Cell(i, j).Style.Font.Bold = True
                        End If

                        If CCommon.ToString(dr("vcCompundParentKey")) = "-3" AndAlso column.ColumnName <> "vcAccountType" Then
                            workSheet.Cell(i, j).Style.Border.TopBorder = XLBorderStyleValues.Thin
                            workSheet.Cell(i, j).Style.Border.BottomBorder = XLBorderStyleValues.Double
                        End If

                        j = j + 1
                    End If
                Next

                i = i + 1
                j = 1
            Next
            workSheet.Columns.AdjustToContents()

            Dim httpResponse As HttpResponse = Response
            httpResponse.Clear()
            httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            httpResponse.AddHeader("content-disposition", "attachment;filename=IncomeExpense.xlsx")


            Using MemoryStream As New MemoryStream
                workbook.SaveAs(MemoryStream)
                MemoryStream.WriteTo(httpResponse.OutputStream)
                MemoryStream.Close()
            End Using

            httpResponse.Flush()
            httpResponse.End()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlDateRange_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDateRange.SelectedIndexChanged
        Try
            If ddlDateRange.SelectedValue <> "0" Then
                Dim fromDate As Date
                Dim toToDate As Date

                Select Case ddlDateRange.SelectedValue
                    Case "CurYear"
                        fromDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "PreYear"
                        fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "CurPreYear"
                        fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "CuQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth")))
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                    Case "PreQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddDays(-1)
                    Case "CurPreQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                    Case "ThisMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                        toToDate = fromDate.AddMonths(1).AddDays(-1)
                    Case "LastMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                        toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
                    Case "CurPreMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                        toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1)
                End Select

                SetDateRange(fromDate, toToDate)
                BindReport()
            End If

            calFrom.SelectedDate = Nothing
            calTo.SelectedDate = Nothing
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlColumnType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlColumnType.SelectedIndexChanged
        Try
            PersistTable.Add(ddlColumnType.ID, ddlColumnType.SelectedValue)
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If Not radCmbCompany.SelectedItem Is Nothing Then
                hdnCustomerName.Value = radCmbCompany.SelectedItem.Text
            Else
                hdnCustomerName.Value = ""
            End If
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

    Protected Sub imgBtnExportPDF_Click(sender As Object, e As EventArgs)
        Try
            Dim dtIncomeExpense As DataTable = GetReportData(True)

            If Not dtIncomeExpense Is Nothing AndAlso dtIncomeExpense.Rows.Count > 0 Then
                Dim columnCount As Integer = 0

                For Each column As DataColumn In dtIncomeExpense.Columns
                    If column.ColumnName <> "ParentId" AndAlso
                        column.ColumnName <> "vcCompundParentKey" AndAlso
                        column.ColumnName <> "numAccountTypeID" AndAlso
                        column.ColumnName <> "vcAccountCode" AndAlso
                        column.ColumnName <> "LEVEL" AndAlso
                        column.ColumnName <> "numAccountId" AndAlso
                        column.ColumnName <> "Struc" AndAlso
                        column.ColumnName <> "Type" AndAlso
                        column.ColumnName <> "bitTotal" AndAlso
                    column.ColumnName <> "bitIsSubAccount" Then
                        columnCount = columnCount + 1
                    End If
                Next

                Dim sb As New System.Text.StringBuilder
                sb.Append("<html><head><style type='text/css'>table {width:100%;border-collapse:collapse;} table tr td {white-space:nowrap;} .font16 { font-family:Arial; font-size:16px; } .font14 { font-family:Arial; font-size:14px;} .font12 { font-family:Arial; font-size:12px;} .font10 { font-family:Arial; font-size:10px;} .fontbold {font-weight:bold;} .textcenter {text-align:center;}.textright {text-align:right;}</style></head><body><table>")
                If Not String.IsNullOrEmpty(hdnCustomerName.Value) Then
                    sb.Append("<tr><td class='font16 fontbold textcenter' colspan='" & columnCount & "'>" & hdnCustomerName.Value & "</td></tr>")
                Else
                    sb.Append("<tr><td class='font16 fontbold textcenter' colspan='" & columnCount & "'>" & CCommon.ToString(Session("DomainName")) & "</td></tr>")
                End If

                sb.Append("<tr><td class='font16 fontbold textcenter' colspan='" & columnCount & "'>Income Expense Statement</td></tr>")
                sb.Append("<tr><td class='font14 fontbold textcenter' colspan='" & columnCount & "'>" & hdnDateRange.Value & "</td></tr>")
                sb.Append("<tr><td class='font14 fontbold textcenter' colspan='" & columnCount & "'> </td></tr>")

                sb.Append("<tr>")
                For Each column As DataColumn In dtIncomeExpense.Columns
                    If column.ColumnName <> "ParentId" AndAlso
                        column.ColumnName <> "vcCompundParentKey" AndAlso
                        column.ColumnName <> "numAccountTypeID" AndAlso
                        column.ColumnName <> "vcAccountCode" AndAlso
                        column.ColumnName <> "LEVEL" AndAlso
                        column.ColumnName <> "numAccountId" AndAlso
                        column.ColumnName <> "Struc" AndAlso
                        column.ColumnName <> "Type" AndAlso
                        column.ColumnName <> "bitTotal" AndAlso
                    column.ColumnName <> "bitIsSubAccount" Then

                        If column.ColumnName <> "vcAccountType" Then
                            sb.Append("<td style='border-bottom-width:1px;border-bottom-style:solid' class='font12 fontbold textcenter'>" & column.ColumnName & "</td>")
                        Else
                            sb.Append("<td></td>")
                        End If
                    End If
                Next
                sb.Append("</tr>")

                Dim level As Short = 0
                Dim accountingNumberFormatTotal As String = "{0:$ #,##0.00;($ #,##0.00);-}"
                Dim accountingNumberFormat As String = "{0:#,##0.00;(#,##0.00);-}"

                For Each dr As DataRow In dtIncomeExpense.Rows
                    sb.Append("<tr>")

                    level = CCommon.ToShort(dr("LEVEL"))

                    For Each column As DataColumn In dtIncomeExpense.Columns
                        If column.ColumnName <> "ParentId" AndAlso
                            column.ColumnName <> "vcCompundParentKey" AndAlso
                            column.ColumnName <> "numAccountTypeID" AndAlso
                            column.ColumnName <> "vcAccountCode" AndAlso
                            column.ColumnName <> "LEVEL" AndAlso
                            column.ColumnName <> "numAccountId" AndAlso
                            column.ColumnName <> "Struc" AndAlso
                            column.ColumnName <> "Type" AndAlso
                            column.ColumnName <> "bitTotal" AndAlso
                            column.ColumnName <> "bitIsSubAccount" Then

                            If column.ColumnName = "vcAccountType" Then
                                While level > 0
                                    dr(column.ColumnName) = "&nbsp;&nbsp;&nbsp;&nbsp;" & dr(column.ColumnName)
                                    level = level - 1
                                End While

                                sb.Append("<td class='font10 fontbold'>" & CCommon.ToString(dr(column.ColumnName)) & "</td>")
                            Else
                                If CCommon.ToShort(dr("Type")) = 2 AndAlso Not CCommon.ToBool(dr("bitTotal")) AndAlso CCommon.ToLong(dr("numAccountId")) = 0 Then
                                    sb.Append("<td></td>")
                                Else
                                    If CCommon.ToShort(dr("Type")) = 2 Then
                                        sb.Append("<td style='border-top-width:1px;border-top-style:solid; " & If(CCommon.ToString(dr("vcCompundParentKey")) = "-3", "border-bottom-width:3px;border-bottom-style: double;", "") & "' class='font10 fontbold textright'>" & String.Format(accountingNumberFormatTotal, CCommon.ToDouble(dr(column.ColumnName))) & "</td>")
                                    Else
                                        sb.Append("<td class='font10 textright'>" & String.Format(accountingNumberFormat, CCommon.ToDouble(dr(column.ColumnName))) & "</td>")
                                    End If
                                End If
                            End If
                        End If
                    Next

                    sb.Append("</tr>")
                Next

                sb.Append("</table></body></html>")

                Dim fileName As String = "IncomeExpense" & Session("DomainID") & Format(Now, "ddmmyyyyhhmmss") & ".pdf"

                Dim objHtmlToPDF As New HTMLToPDF
                Dim objBytes As Byte() = objHtmlToPDF.ConvertHtmlToPDF(sb.ToString())

                Dim strFilePhysicalLocation As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & fileName

                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & fileName)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(objBytes)
                Response.End()
                Response.Flush()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class