﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMonthlySummary.aspx.vb"
    Inherits=".frmMonthlySummary" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Monthly Summary</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>From</label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>To</label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary" ClientIDMode="Static"></asp:Button>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Back"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Monthly Summary
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <div class="form-inline" style="padding-top: 6px;">
        <div class="form-group">
            <label>Account Name:</label>
            <asp:Label ID="lblAccountName" runat="server"></asp:Label>
        </div>
        &nbsp;&nbsp;&nbsp;
        <div class="form-group">
            <label>Opening Balance:</label>
            <asp:Label ID="lblOpening" runat="server"></asp:Label>
        </div>
        &nbsp;&nbsp;&nbsp;
        <div class="form-group">
            <label>Debit:</label>
            <asp:Label ID="lblDebit" runat="server"></asp:Label>
        </div>
        &nbsp;&nbsp;&nbsp;
        <div class="form-group">
            <label>Credit:</label>
            <asp:Label ID="lblCredit" runat="server"></asp:Label>
        </div>
        &nbsp;&nbsp;&nbsp;
        <div class="form-group">
            <label>Closing Balance:</label>
            <asp:Label ID="lblClosing" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Repeater ID="RepSummary" runat="server">
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="table table-striped table-bordered">
                            <tr>
                                <th width="20%">Month
                                </th>
                                <th width="20%">Opening
                                </th>
                                <th width="20%">Debit
                                </th>
                                <th width="20%">Credit
                                </th>
                                <th width="20%">Closing
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td width="20%">
                                <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit"><%#Eval("Month")%></asp:LinkButton>
                                <asp:Label ID="lblAccountTypeID" runat="server" Visible="false" Text='<%# Eval("numAccountTypeID") %>'></asp:Label>
                                <asp:Label ID="lblMonthNumber" runat="server" Visible="false" Text='<%# Eval("MonthNumber") %>'></asp:Label>
                                <asp:Label ID="lblYearNumber" runat="server" Visible="false" Text='<%# Eval("YEAR") %>'></asp:Label>
                            </td>
                            <td align="right" width="20%">
                                <%#ReturnMoney(Eval("Opening"))%>
                            </td>
                            <td align="right" width="20%">
                                <%#ReturnMoney(Eval("Debit"))%>
                            </td>
                            <td align="right" width="20%">
                                <%#ReturnMoney(Eval("Credit"))%>
                            </td>
                            <td align="right" width="20%">
                                <%#ReturnMoney(Eval("Closing"))%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnAccountClass" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
