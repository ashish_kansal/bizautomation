﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class TransactionInfo
    Inherits BACRMUserControl

    Public ReferenceType As Short 'Will be passed from page
    Public ReferenceID As Long 'Will be passed from page
    Public TransactionID As Long
    Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking


    Public ReadOnly Property JournalID As Long
        Get
            Return CCommon.ToLong(hdnJournalID.Value)
        End Get
    End Property

    Public ReadOnly Property HeaderTransactionID As Long
        Get
            Return CCommon.ToLong(hdnHeaderTransactionID.Value)
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.Master.Master Is Nothing AndAlso Not DirectCast(Page.Master.Master, BizMaster) Is Nothing Then
                'HIDE ERROR DIV
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            End If
            

            If Not IsPostBack Then
                If TransactionID > 0 And ReferenceID > 0 Then
                    hdnTransactionID.Value = TransactionID
                    objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                    objEBanking.TransactionID = TransactionID
                    Dim dtTransactionDetail As DataTable = objEBanking.GetTransactionDetails()
                    If dtTransactionDetail.Rows.Count > 0 Then
                        lnkbtnTransMatchDetails.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("TransMatchDetails"))
                        lnkbtnTransMatchDetails.Attributes.Add("onclick", "javascript:return TransactionDetail(" & TransactionID & ");")
                        pnlMatchedTrans.Visible = True
                    End If
                Else
                    pnlMatchedTrans.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            If Not Page.Master.Master Is Nothing AndAlso Not DirectCast(Page.Master.Master, BizMaster) Is Nothing Then
                DisplayError(CCommon.ToString(ex))
            Else
                Response.Write(ex)
            End If
        End Try
    End Sub

    Public Sub getTransactionInfo()
        Try
            Dim objJEHeader As New JournalEntryHeader

            objJEHeader.DomainID = Session("DomainID")
            objJEHeader.ReferenceType = ReferenceType
            objJEHeader.ReferenceID = ReferenceID

            Dim dtDetails As New DataTable

            dtDetails = objJEHeader.GetAccountTransactionInfo()

            If dtDetails.Rows.Count > 0 Then
                hdnIsReconciled.Value = IIf(CCommon.ToBool(dtDetails(0)("bitReconcile")), 1, 0)
                hdnJournalID.Value = CCommon.ToLong(dtDetails(0)("numJournal_Id"))
                hdnHeaderTransactionID.Value = CCommon.ToLong(dtDetails(0)("numTransactionId"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnUnMatch_Click(sender As Object, e As System.EventArgs) Handles btnUnMatch.Click
        Try
            If CCommon.ToLong(hdnTransactionID.Value) > 0 Then
                objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                objEBanking.UnMatchBankStatementTransactions()
                pnlMatchedTrans.Visible = False
                Response.Redirect("../Accounting/frmDownloadedTransactions.aspx")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            If Not Page.Master.Master Is Nothing AndAlso Not DirectCast(Page.Master.Master, BizMaster) Is Nothing Then
                DisplayError(CCommon.ToString(ex))
            Else
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = Exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

End Class
