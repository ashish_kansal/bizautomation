<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCashFlowStatement.aspx.vb"
    Inherits=".frmCashFlowStatement" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Cash Flow statement</title>
    <style type="text/css">
        table#tblExport tr td {
            border: none;
        }
    </style>
    <style type="text/css">
        .radtreelist .rtltable {
            border: none !important;
            border-collapse: collapse;
        }

        .RadTreeList_Default {
            border-color: #cbcbcb !important;
        }

            .RadTreeList_Default .rtlHeader {
                color: #000 !important;
                background: #e5e5e5 !important;
                height: 30px;
            }

                .radtreelist_default .rtlheader, .radtreelist_default .rtlheader th {
                    border-collapse: collapse;
                }

                    .radtreelist_default .rtlheader th {
                        border: 1px solid #cbcbcb !important;
                        border-collapse: collapse;
                    }

        .RadTreeList .rtlHeader th {
            font-weight: 900 !important;
            border-color: #d9d9d9 !important;
            border-width: 0px 0px 3px 1px !important;
        }

        .rtlR, .rtlA {
            height: 30px;
        }

        .rtlR {
            background-color: #f2f2f2 !important;
        }

        .rtlA {
            background-color: #fff !important;
        }

        .linkColor {
            color: Green;
            cursor: pointer;
        }

        .rcbInput {
            height: 19px !important;
        }

        .RadTreeList_Default tr:not(.rtlHeader):hover {
            color: #000 !important;
            background: #FFFFDE !important;
        }

        .RadTreeList_Default .rtlRSel {
            color: #000 !important;
            background: #FFFFDE !important;
        }

            .RadTreeList_Default .rtlRSel td {
                border-color: #d9d9d9 !important;
            }

        .RadInput .riTextBox, html body .RadInputMgr {
            border-width: 1px;
            border-style: solid;
            padding: 2px 1px 3px;
            vertical-align: middle;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_endRequest(function () {
                $(".rtlR").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () {
                        $(this).find("#imgGL").hide();
                    }
                );

                $(".rtlA").hover(
                        function () {
                            if ($(this).find(".linkColor").length > 0) {
                                $(this).find("#imgGL").show();
                            }
                        },
                        function () {
                            $(this).find("#imgGL").hide();
                        }
                );
            });

            $(".rtlR").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () { $(this).find("#imgGL").hide(); }

            );

            $(".rtlA").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () { $(this).find("#imgGL").hide(); }

            );
        });

        function ExpandCollapse(id) {
            try {
                var uniqueCode = id.getAttribute("UniqueID")
                var className = id.className;
                var newClassName;

                if (className.indexOf("rtlCollapse") >= 0) {
                    newClassName = className.replace("rtlCollapse", "rtlExpand");
                    id.className = newClassName;
                } else if (className.indexOf("rtlExpand") >= 0) {
                    newClassName = className.replace("rtlExpand", "rtlCollapse");
                    id.className = newClassName;
                }


                if (id != null) {
                    $('[id*=rtlCashFlow] > table > tbody > tr').each(function () {
                        if ($(this).attr("UniqueID") != uniqueCode && $(this).attr("UniqueID").indexOf(uniqueCode) >= 0) {
                            var btn = $(this).find("[id*=ExpandCollapseButton]")

                            if (className.indexOf("rtlCollapse") >= 0) {
                                $(this).hide();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlCollapse", "rtlExpand");
                                }

                            } else if (className.indexOf("rtlExpand") >= 0) {
                                $(this).show();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlExpand", "rtlCollapse");
                                }
                            }
                        }
                    });
                }
            }
            catch (ex) {
            }
            finally {
                return false;
            }
        }

        function OpenMonthySummary(a, b, c, d, e) {
            var fromDate = $("#hdnFromDate").val();
            var toDate = $("#hdnToDate").val();
            var accountClassID = 0;

            if ($("#ddlUserLevelClass").length > 0) {
                accountClassID = $("#ddlUserLevelClass").val();
            }

            if (e == 1 || e == 3) {
                window.location.href = '../Accounting/frmMonthlySummary.aspx?ChartAcntId=' + a + '&Opening=' + b + '&Code=' + c + '&frm=frmCashFlowStatement' + '&Name=' + d + "&From=" + fromDate + "&To=" + toDate + "&AccountClassID=" + accountClassID;
            }
        }

        function OpenGLReport(a, b) {
            var fromDate = $("#hdnFromDate").val();
            var toDate = $("#hdnToDate").val();
            var accountClassID = 0;

            if ($("#ddlUserLevelClass").length > 0) {
                accountClassID = $("#ddlUserLevelClass").val();
            }

            window.location.href = "../Accounting/frmGeneralLedger.aspx?Mode=4&AcntTypeID=" + a + "&Month=0&AccountID=" + b + "&From=" + fromDate + "&To=" + toDate + "&AccountClassID=" + accountClassID;
            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="updatepanelexcel" runat="server" ChildrenAsTriggers="true" class="row padbottom10">
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="pull-left">
                    <div class="form-inline">
                        <div class="form-group">
                            <asp:Panel runat="server" ID="pnlAccountingClass">
                                <label>Class</label>
                                <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="signup form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </asp:Panel>
                        </div>
                        &nbsp;&nbsp;
                    <div class="form-group">
                        <label>View:</label>
                        <asp:DropDownList ID="ddlColumnType" runat="server" AutoPostBack="true" CssClass="form-control">
                            <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Quarter" Value="Quarter"></asp:ListItem>
                            <asp:ListItem Text="Year" Value="Year"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                        <div class="form-group">
                            <label></label>
                            <asp:DropDownList ID="ddlDateRange" runat="server" AutoPostBack="true" CssClass="form-control">
                                <asp:ListItem Text="-- Select --" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Current Year" Value="CurYear"></asp:ListItem>
                                <asp:ListItem Text="Previous Year" Value="PreYear"></asp:ListItem>
                                <asp:ListItem Text="Previous and Current Year" Value="CurPreYear"></asp:ListItem>
                                <asp:ListItem Text="Current Quarter" Value="CuQur"></asp:ListItem>
                                <asp:ListItem Text="Previous Quarter" Value="PreQur"></asp:ListItem>
                                <asp:ListItem Text="Previous and Current Quarter" Value="CurPreQur"></asp:ListItem>
                                <asp:ListItem Text="Current Month" Value="ThisMonth"></asp:ListItem>
                                <asp:ListItem Text="Previous Month" Value="LastMonth"></asp:ListItem>
                                <asp:ListItem Text="Previous and Current Month" Value="CurPreMonth"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                    </div>
                </div>
                <div class="pull-right">
                    <div class="form-inline">
                    <div class="form-group">
                        <label>From</label>
                        <telerik:RadDatePicker runat="server" ID="calFrom" />
                    </div>
                    <div class="form-group">
                        <label>To</label>
                        <telerik:RadDatePicker ID="calTo" runat="server" />
                    </div>
                    <asp:LinkButton ID="btnGo" runat="server" CssClass="button btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    <asp:LinkButton ID="btnExportExcel" runat="server" CssClass="button btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="imgBtnExportPDF" CssClass="btn btn-primary" runat="server" OnClick="imgBtnExportPDF_Click"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF</asp:LinkButton>
                        </div>
                    <asp:HiddenField ID="hdnDateRange" runat="server" />
                    <asp:HiddenField ID="hdnDomainName" runat="server" />
                    <asp:HiddenField ID="hdnFromDate" runat="server" />
                    <asp:HiddenField ID="hdnToDate" runat="server" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
            <asp:PostBackTrigger ControlID="imgBtnExportPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Cash Flow Statement &nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmcashflowstatement.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanelGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" class="row">
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="table-responsive">                  

                    <telerik:RadTreeList ID="rtlCashFlow" runat="server" AutoGenerateColumns="false" ParentDataKeyNames="ParentId" DataKeyNames="vcCompundParentKey"
                        OnItemCommand="rtlCashFlow_ItemCommand" OnItemDataBound="rtlCashFlow_ItemDataBound" Width="100%" Height="100%">
                        <Columns>
                            <telerik:TreeListTemplateColumn HeaderText="Account" UniqueName="vcAccountType" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <span onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','0','<%#Eval("vcAccountCode")%>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountType").ToString()) %>','<%# Eval("Type")%>');">
                                        <%#IIf(Eval("Type") > 1, "<b>" & Eval("vcAccountType").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountType") & "</span>")%>
                                    </span>
                                    <span class="GLReportSpan">
                                        <img id="imgGL" alt='General Ledger' height='16px' width='16px' onclick='return OpenGLReport(<%#Eval("ParentId") %>,<%#Eval("numAccountID") %>)' title='General Ledger' src='../images/GLReport.png' style='display: none;'></span>
                                </ItemTemplate>
                            </telerik:TreeListTemplateColumn>
                        </Columns>
                        <ClientSettings Selecting-AllowItemSelection="true">
                            <Scrolling AllowScroll="true" />
                        </ClientSettings>
                    </telerik:RadTreeList>
                </div>
            </div>
            <asp:HiddenField ID="hdnMaxLevel" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>

