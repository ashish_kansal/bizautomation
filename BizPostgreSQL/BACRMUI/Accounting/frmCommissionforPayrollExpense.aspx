<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCommissionforPayrollExpense.aspx.vb"
    Inherits=".frmCommissionforPayrollExpense" MasterPageFile="~/common/Popup.Master"
    ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Commission List</title>
    <script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenReturnBizDoc(a, b, c) {
            window.open('../opportunity/frmMirrorBizdoc.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RefID=' + a + '&RefType=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?OpID=" + a + "&OppType=1";
            window.opener.location.href = str;
            return false;
        }

        function OpenReturn(a, b) {
            var str;
            str = "../opportunity/frmReturnDetail.aspx?type=" + b + "&ReturnID=" + a;
            window.opener.location.href = str;
            return false;
        }

        function OpenProject(a) {
            var str;
            str = "../projects/frmProjects.aspx?ProId=" + a;
            window.opener.location.href = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table>
                    <tr>
                        <td><b>Commission Rule</b></td>
                        <td>
                            <asp:DropDownList ID="ddlCommissionRule" runat="server" AutoPostBack="true" Width="250"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="text-align: right;">                
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text="Commission List"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <telerik:RadGrid ID="dgBizDocs" ShowStatusBar="true" runat="server" AutoGenerateColumns="False"
        AllowMultiRowSelection="False" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is"
        HeaderStyle-CssClass="hs" Skin="windows" EnableEmbeddedSkins="false" Width="1280">
        <MasterTableView DataKeyNames="numOppBizDocsId" ItemStyle-HorizontalAlign="Center"
            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" Name="ParentGrid" ShowFooter="true">
            <DetailTables>
                <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"
                    AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Name="ChildGrid">
                    <ParentTableRelation>
                        <telerik:GridRelationFields DetailKeyField="numOppBizDocsId" MasterKeyField="numOppBizDocsId" />
                    </ParentTableRelation>
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="Item" DataField="vcItemName" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="left">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Units" DataField="numUnitHour" HeaderStyle-Wrap="false" DataFormatString="{0:#}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Vendor Cost" DataField="VendorCost" DataFormatString="{0:##,#00.00}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Average Cost" DataField="monAvgCost" DataFormatString="{0:##,#00.00}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Unit Price" DataField="monPrice" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Total Amount" DataField="monTotAmount" DataFormatString="{0:##,#00.00}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission Amount" DataField="CommissionAmt" DataFormatString="{0:##,#00.00}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission" DataField="decCommission" DataFormatString="{0:##,#00.00}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Based On" DataField="BasedOn" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission Type" DataField="CommissionType" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="vcItemName"></telerik:GridSortExpression>
                    </SortExpressions>
                </telerik:GridTableView>
            </DetailTables>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Organization Name" DataField="vcCompanyName" />
                <telerik:GridTemplateColumn HeaderText="Deal/Order" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a onclick="return OpenOpp('<%# DataBinder.Eval(Container.DataItem, "numOppId") %>');">
                            <%# DataBinder.Eval(Container.DataItem, "Name") %></a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="BizDoc">
                    <ItemTemplate>
                        <a onclick="return OpenBizInvoice('<%# DataBinder.Eval(Container.DataItem, "numOppId") %>','<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>');">
                            <%# DataBinder.Eval(Container.DataItem, "vcBizDocID") %></a>
                        <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
                        </asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Deal Status" DataField="OppStatus">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Invoice Grand Total / Sub Total">
                    <ItemTemplate>
                        <%# String.Format("{0:##,#00.00}",DataBinder.Eval(Container.DataItem, "DealAmount"))%>
                        /
                        <b><%# String.Format("{0:##,#00.00}", DataBinder.Eval(Container.DataItem, "monSubTotAMount"))%></b>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Total Amount Paid" DataField="monAmountPaid" DataFormatString="{0:##,#00.00}" ItemStyle-HorizontalAlign="Right">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn>
                    <HeaderTemplate>
                        <%# IIf(Convert.ToInt16(GetQueryStringVal("Mode")) = 0, "Payment Date", "Creation Date")%>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "bintCreatedDate")%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Employee Role" DataField="EmpRole">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Total Commission" ItemStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataField="decTotalCommission" UniqueName="TemplateTotalCommission">
                    <ItemTemplate>
                        <asp:Label ID="lblTotal" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "decTotalCommission", "{0:##,#00.0000}") %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="Server" ID="lblGrandTotal">
                        </asp:Label>
                    </FooterTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="bintCreatedDate"></telerik:GridSortExpression>
            </SortExpressions>
        </MasterTableView>
    </telerik:RadGrid>

    <asp:GridView ID="gvProjectCommission" AutoGenerateColumns="False" runat="server" Width="1200px"
        CssClass="tbl" DataKeyNames="numProId">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <Columns>
            <asp:TemplateField ItemStyle-Width="150px">
                <HeaderTemplate>
                    Project Name
                </HeaderTemplate>
                <ItemTemplate>
                    <a href="javascript:OpenProject('<%#Eval("numProId")%>')">
                        <%# Eval("vcProjectID")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="dtCompletionDate" HeaderText="Completion Date"></asp:BoundField>
            <asp:BoundField DataField="monProTotalIncome" HeaderText="Income" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
            <asp:BoundField DataField="monProTotalExpense" HeaderText="Expense" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
            <asp:BoundField DataField="monProTotalGrossProfit" HeaderText="Gross Profit" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
            <asp:BoundField DataField="EmpRole" HeaderText="Employee Role" HtmlEncode="false"></asp:BoundField>
            <asp:BoundField DataField="decCommission" HeaderText="Commission %" HtmlEncode="false"></asp:BoundField>
            <asp:BoundField DataField="decTotalCommission" HeaderText="Total Commission" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
    </asp:GridView>

    <telerik:RadGrid ID="gvCreditMemoAndRefunds" ShowStatusBar="true" runat="server" AutoGenerateColumns="False"
        AllowMultiRowSelection="False" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is"
        HeaderStyle-CssClass="hs" Skin="windows" EnableEmbeddedSkins="false" Width="1280">
        <MasterTableView DataKeyNames="numReturnHeaderID" ItemStyle-HorizontalAlign="Center"
            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" Name="ParentGrid" ShowFooter="true">
            <Columns>
                <telerik:GridBoundColumn DataField="vcCompanyName" HeaderText="Organization Name"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Return" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a onclick="return OpenReturn('<%# DataBinder.Eval(Container.DataItem, "numReturnHeaderID")%>','<%# Eval("tintReturnType") %>');">
                            <%# DataBinder.Eval(Container.DataItem, "vcRMA")%></a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="BizDoc">
                    <ItemTemplate>
                        <a onclick="return OpenReturnBizDoc(<%# DataBinder.Eval(Container.DataItem, "numReturnHeaderID")%>,<%# If(DataBinder.Eval(Container.DataItem, "tintReceiveType") = 1, 9, 7)%>,0);">
                            <%# DataBinder.Eval(Container.DataItem, "vcBizDocName")%></a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="monBizDocAmount" HeaderText="Return Amount" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Total Commission" ItemStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataField="decTotalCommission" UniqueName="TemplateTotalCommission">
                    <ItemTemplate>
                        <asp:Label ID="lblTotal" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "decCommission", "{0:##,#00.0000}")%>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="Server" ID="lblGrandTotal">
                        </asp:Label>
                    </FooterTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Content>
