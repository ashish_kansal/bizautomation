<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountsPayable.aspx.vb"
    Inherits="BACRM.UserInterface.Accounting.frmAccountsPayable" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>A/P Aging Report</title>
    <link rel="stylesheet" href="../CSS/jscal2.css" type="text/css" />
    <link rel="stylesheet" href="../CSS/border-radius.css" type="text/css" />
    <script src="../JavaScript/jscal2.js" type="text/javascript"></script>
    <script src="../JavaScript/en.js" type="text/javascript"></script>
    <style>
         .divTableWithFloatingHeader{
  height: calc(100vh - 250px);
  overflow: auto
        }
        #dgAP tr th{
            position: -webkit-sticky;
  position: sticky;
  top: 0;
        }
         .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
        .hs {
            background-color: #ebebeb;
            border: 1px solid #e4e4e4;
            border-bottom-width: 2px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function receivePayment(message) {
        }
        function OpenContact(a, b) {


            window.location.href = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&r@we54f6r=t4yhbf3e&CntId=" + a

            return false;
        }

        function OpemEmail(URL) {
            window.open(URL, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenCompany(a, b, c) {
            if (b == 0) {
                 window.open('../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&DivID=' + a,'_blank');
                //window.location.href = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&DivID=" + a + ",_blank"
            }

            else if (b == 1) {
                 window.open('../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&DivID=' + a,'_blank');
                //window.location.href = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&DivID=" + a + ",_blank"
            }
            else if (b == 2) {
                 window.open('../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&klds+7kldf=fjk-las&DivId=' + a,'_blank');
               // window.location.href = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsPayable&klds+7kldf=fjk-las&DivId=" + a + ",_blank"
            }

            return false;
        }
        function OpemDetails(a, b, c) {
            if (a == '' || b == '' || c == '') {
                return false;
            }

            var dateFormat = '<%= Session("DateFormat")%>';
            var fromDate = $("[id$=calFrom_txtDate]").val();
            var toDate = $("[id$=calTo_txtDate]").val();
            if (dateFormat != "") {
                fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
                toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

                fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
                toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
            }

            window.open('frmAccountsPayableDetails.aspx?DomainId=' + parseInt(a) + '&DivisionID=' + b + '&Flag=' + c + '&accountClass=' + $("[id$=ddlUserLevelClass]").val() + "&fromDate=" + fromDate + "&toDate=" + toDate, '', 'toolbar=no,titlebar=no,top=300,left=220,width=1000,height=300,scrollbars=yes,resizable=yes')
            return false;
        }
        function reDirectPage(a) {
            parent.parent.frames['mainframe'].location.href = a;
        }
        function OpemAccountInvoice() {
            var dateFormat = '<%= Session("DateFormat")%>';
            var fromDate = $("[id$=calFrom_txtDate]").val();
            var toDate = $("[id$=calTo_txtDate]").val();
            if (dateFormat != "") {
                fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
                toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

                fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
                toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
            }

            window.open('frmAccountsPayableInvoice.aspx?accountClass=' + $("[id$=ddlUserLevelClass]").val() + "&fromDate=" + fromDate + "&toDate=" + toDate, '', 'toolbar=no,titlebar=no,top=10,left=10,width=1000,height=300,scrollbars=yes,resizable=yes')
            return false;
        }


        $(document).ready(function () {
            $("table#dgAP").each(function () {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("display", "none");

                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });
            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });


        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function () {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();
                if ((scrollTop > offset.top) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("display", "block");
                    floatingHeaderRow.css("top", Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()) + "px");

                    // Copy cell widths from original header
                    $("td", floatingHeaderRow).each(function (index) {
                        var cellWidth = $("td", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("display", "none");
                    floatingHeaderRow.css("top", "0px");
                }
            });
        }

        function Go() {
            //if (document.form1.ddlType.value == 0) {
            //    alert("Please Select Account Type");
            //    document.form1.ddlType.focus();
            //    return false;
            //}
            //if (document.form1.hdnAccounts.value.length == 0 && document.form1.ddlCOA.value == 0) {
            //    alert("Please Select a Accounts");
            //    document.form1.ddlCOA.focus();
            //    return false;
            //}
        }

        function ConfirmCommissionPaid() {
            if (confirm("All commssion entries within selected date range will be marked as paid and commission amount will be removed from past due column(s) in your organization record. Do you want to proceed?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group" runat="server" id="pnlAccountingClass">
                        <label>Class</label>
                        <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            From
                        </label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>
                            To
                        </label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>
                            Customer/Vendor</label>
                        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                            ClientIDMode="Static"
                            ShowMoreResultsBox="true"
                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
                    </div>
                    <asp:LinkButton ID="btnGo" runat="server" CssClass="btn btn-primary" OnClientClick="return Go();"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    <div class="form-group">
                        <asp:UpdatePanel ID="updateTopBtnEvnt" runat="server">
                            <ContentTemplate>
                                &nbsp;<asp:LinkButton runat="server" ID="ibExportExcel" CssClass="btn btn-primary" AlternateText="Export to Excel"
                                    ToolTip="Export to Excel"><i class="fa fa-file-excel-o"></i></asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ibExportExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                &nbsp;<asp:LinkButton runat="server" ID="ibSendEmail" OnClientClick="return ConfirmMail();" CssClass="btn btn-primary" AlternateText="Email as PDF" ToolTip="Email as PDF" CommandName="Email"><i class="fa fa-envelope-o"></i></asp:LinkButton>
                                <a href="#" class="help">&nbsp;</a>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ibSendEmail" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group">
                        <a href="#" onclick="javascript:OpemAccountInvoice();" class="hyperlink btn btn-default"><i class="fa fa-table"></i>&nbsp;&nbsp;A/P Aging Detail
            Report </a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%--<div class="col-md-12">
        <div class="form-inline">
            <div class="form-group" runat="server" ID="">
                <label>Class</label>
                <asp:DropDownList ID="" runat="server" CssClass="form-control" AutoPostBack="true">
                            </asp:DropDownList>
            </div>
            <div class="form-group">
                 <label>
                            From
                        </label>
                <BizCalendar:Calendar ID="" runat="server" ClientIDMode="AutoID" />
            </div>
            <div class="form-group">
                <label>
                            To
                        </label>
                <BizCalendar:Calendar ID="" runat="server" ClientIDMode="AutoID" />
            </div>
            <div class="form-group">
                <label>
                            Customer/Vendor</label>
                <telerik:RadComboBox AccessKey="C" ID="" Width="195px" DropDownWidth="600px"
                            OnClientItemsRequested="" 
                            ClientIDMode="Static" 
                            ShowMoreResultsBox="true"
                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
            </div>
            <div class="form-group">
                &nbsp;<asp:LinkButton ID="" runat="server" CssClass="button btn btn-primary" OnClientClick="return Go();"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
            </div>
            <div class="form-group">
                <asp:UpdatePanel ID="updatepanelExcelClieck" runat="server">
                    <ContentTemplate>
                            <asp:LinkButton runat="server" ID=""  CssClass="btn btn-primary" style="margin-top: 20px;" AlternateText="Export to Excel"
                            ToolTip="Export to Excel"><i class="fa fa-file-excel-o"></i></asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="ibExportExcel" />
                    </Triggers>
                </asp:UpdatePanel>
                
                <a href="#" class="help">&nbsp;</a>
            </div>
            <div class="form-group">
                
            </div>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12" style="margin-top: 10px; margin-bottom: 5px;">
        <div class="pull-right">
            <div class="form-inline">
                
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    A/P Aging&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmaccountspayable.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel ID="updatePanelPastDue" runat="server">
        <ContentTemplate>
            <asp:CheckBox ID="chkHidePastDue" runat="server" Text="Hide Past Due column" AutoPostBack="true" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="chkHidePastDue" />
        </Triggers>
    </asp:UpdatePanel>
    &nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblGrandTotal" CssClass="btn btn-default" style="border-radius:0px;"  runat="server" Text="Grand Total we owe our vendors:"> </asp:Label>
                <asp:Label ID="lblGrandTotalAmt" CssClass="btn btn-success" style="border-radius:0px;"  runat="server" Text="" Font-Bold="true"> </asp:Label>
    &nbsp;&nbsp;<asp:Button ID="btnMarkCommissionAsPaid" runat="server" CssClass="btn btn-primary" Text="Mark Commission as Paid" OnClientClick="return ConfirmCommissionPaid();" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">


    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive divTableWithFloatingHeader">
                 <asp:DataGrid ID="dgAP" runat="server" UseAccessibleHeader="true" AllowSorting="true" CssClass="table table-striped table-bordered" Width="100%"
        AutoGenerateColumns="False" EnableViewState="true" ShowFooter="true" FooterStyle-Font-Bold="true">
        <Columns>
            <asp:BoundColumn DataField="numDivisionId" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
            <%-- <asp:TemplateColumn HeaderText="Customer" SortExpression="vcCompanyName" HeaderStyle-Width="10%">
                <ItemTemplate>
                    <asp:Label ID="lblCompanyName" runat="server" Text='<%# Eval("vcCompanyName") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    Total
                </FooterTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText="Vendor" SortExpression="vcCompanyName" HeaderStyle-Width="11%">
                <ItemTemplate>
                    <asp:HyperLink ID="hplCompany" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "vcCompanyName") %>'>
                    </asp:HyperLink>
                    <asp:Label ID="lblCompany" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcCompanyName") %>'></asp:Label><br />
                    <asp:Label runat="server" ID="lblCustPhone" Text='<%# DataBinder.Eval(Container.DataItem, "vcCustPhone") %>'></asp:Label>
                </ItemTemplate>
                 <FooterTemplate>
                    Total
                </FooterTemplate>
            </asp:TemplateColumn>
            <%--<asp:TemplateColumn HeaderText="Accounts Payable, Phone, Email " SortExpression="vcApName"
                HeaderStyle-Width="12%">
                <HeaderTemplate>
                    Accounts Payable&nbsp;<asp:Label ID="lblToolTip" Text="[?]" CssClass="tip"
                        runat="server" ToolTip="Contact Postion type Account Payable or Primary Contact" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="hplActName" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "vcApName") %>'> 
                    </asp:HyperLink><br />
                    <asp:Label runat="server" ID="lblActPhone" Text='<%# DataBinder.Eval(Container.DataItem, "vcPhone") %>'></asp:Label><br />
                    <asp:HyperLink ID="hplActEmail1" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText="Current" SortExpression="numCurrentDays" HeaderStyle-Width="8%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0+30');">
                        <%#IIf(Integer.Parse(Eval("intCurrentDaysCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numCurrentDays")) + "</b></font>", ReturnMoney(Eval("numCurrentDays")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numCurrentDaysPaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalCurrentDays" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
           <%-- <asp:TemplateColumn HeaderText="Today-30" SortExpression="numThirtyDays" HeaderStyle-Width="8%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0+30');">
                        <%#IIf(Integer.Parse(Eval("intThirtyDaysCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numThirtyDays")) + "</b></font>", ReturnMoney(Eval("numThirtyDays")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numThirtyDaysPaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalThirtyDays" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="31-60" SortExpression="numSixtyDays" HeaderStyle-Width="8%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','30+60');">
                        <%#IIf(Integer.Parse(Eval("intSixtyDaysCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numSixtyDays")) + "</b></font>", ReturnMoney(Eval("numSixtyDays")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numSixtyDaysPaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalSixtyDays" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="61-90" SortExpression="numNinetyDays" HeaderStyle-Width="8%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','60+90');">
                        <%#IIf(Integer.Parse(Eval("intNinetyDaysCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numNinetyDays")) + "</b></font>", ReturnMoney(Eval("numNinetyDays")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numNinetyDaysPaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalNinetyDays" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Over 91" SortExpression="numOverNinetyDays" HeaderStyle-Width="8%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','90+');">
                        <%#IIf(Integer.Parse(Eval("intOverNinetyDaysCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numOverNinetyDays")) + "</b></font>", ReturnMoney(Eval("numOverNinetyDays")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numOverNinetyDaysPaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalOverNinetyDays" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText="1-30 days past due" SortExpression="numThirtyDaysOverDue"
                HeaderStyle-Width="10%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0-30');">
                        <%#IIf(Integer.Parse(Eval("intThirtyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numThirtyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numThirtyDaysOverDue")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numThirtyDaysOverDuePaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalThirtyDaysOverDue" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="31-60 days past due" SortExpression="numSixtyDaysOverDue"
                HeaderStyle-Width="9%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','30-60');">
                        <%# IIf(Integer.Parse(Eval("intSixtyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numSixtyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numSixtyDaysOverDue")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numSixtyDaysOverDuePaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalSixtyDaysOverDue" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="61-90 days past due" SortExpression="numNinetyDaysOverDue"
                HeaderStyle-Width="9%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','60-90');">
                        <%#IIf(Integer.Parse(Eval("intNinetyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numNinetyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numNinetyDaysOverDue")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numNinetyDaysOverDuePaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalNinetyDaysOverDue" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Over 90 days past due" SortExpression="numOverNinetyDaysOverDue"
                HeaderStyle-Width="9%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','90-');">
                        <%#IIf(Integer.Parse(Eval("intOverNinetyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numOverNinetyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numOverNinetyDaysOverDue")))%>
                    </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numOverNinetyDaysOverDuePaid"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalOverNinetyDaysOverDue" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Credits" SortExpression="monUnAppliedAmount"
                HeaderStyle-Width="8%">
                <ItemTemplate>
                    <font color='green'><%# ReturnMoney(Eval("monUnAppliedAmount"))%></font>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalUnAppliedAmount" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Total" SortExpression="numTotal" HeaderStyle-Width="9%">
                <ItemTemplate>
                    <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0');">
                        <%#ReturnMoney(Eval("numTotal"))%>
                    </a>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                </FooterTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
                </div>
            </div>
        </div>
    <asp:HiddenField runat="server" ID="hdnHTML" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
