﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddBill.aspx.vb" Inherits=".frmAddBill"
    MasterPageFile="~/common/PopUp.Master" ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register Src="PaymentHistory.ascx" TagName="PaymentHistory" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Add Bill</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" language="javascript">
        function OpenPayBill() {
            window.open('../Accounting/frmAddBill.aspx?I=1', '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }


        $(document).ready(function () {
            //            var duedateChage = window.validateDate;
            //            window.validateDate = function () {
            //                duedateChage('MMM-dd-yyyy');
            //            }
            ShowHide();
            InitializeValidation();

            $('#Content_calBillDate_txtDate').blur(function () {
                //if (cal.indexOf("calBillDate") > 0) {
                duedateChage(document.getElementById('hfDateFormat').value);
                //}
            });
            $(".rcbInput").addClass("{required:'#radBill:checked', messages:{required:'Select organization!'}}");
            $('#litMessage').fadeIn().delay(5000).fadeOut();
            //$('#Content_calBillDate_txtDate').val('<%=Date.Now%>');
            $('#Content_calBillDate_txtDate').blur();

            CalculateTotal();
        });

        function duedateChage(format) {

            if (!isDate($('#Content_calBillDate_txtDate').val(), format)) {
                alert("Enter Valid Bill date");
                $('#Content_calBillDate_txtDate').focus();
                return false;
            }

            var SDate = new Date(getDateFromFormat($('#Content_calBillDate_txtDate').val(), format));
            //var startDate = formatDate(d, 'dd-MM-yyyy');

            var strValue = "";

            if ($('#ddlTerms option:selected').length > 0) {
                strValue = $('#ddlTerms option:selected').val();
            }

            var intDueDays = 0;

            if (strValue != null && strValue != "") {
                intDueDays = parseInt(strValue.split("~")[1]);
            }

            startDate = new Date(SDate.getFullYear(), SDate.getMonth(), parseInt(SDate.getDate()) + intDueDays);
            console.log(startDate);
            $('#Content_calDueDate_txtDate').val(formatDate(startDate, format));
        }

        function ShowHide() {
            if (document.getElementById('rdbLstOption_0').checked == true) {
                $('#radCmbCompany').show();
                $('#ddlLiabilityAccount').hide();

                $('#lblNameFor').text('Organization');
            }
            else if (document.getElementById('rdbLstOption_1').checked == true) {
                $('#radCmbCompany').hide();
                $('#ddlLiabilityAccount').show();

                $('#lblNameFor').text('Liabilities');
            }
        }

        var IsAddClicked = false;
        function Save() {
            if (IsAddClicked == true) {
                return false;
            }

            var isValid;
            isValid = true;

            if (document.getElementById('rdbLstOption_0').checked == true) {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Organization first.")
                    return false;
                }
            }
            else if (document.getElementById('rdbLstOption_1').checked == true) {
                if (document.getElementById('ddlLiabilityAccount').value == 0) {
                    alert("Please Select Liability Account");
                    document.getElementById('ddlLiabilityAccount').focus();
                    return false;
                }

            }

            if ($('[id$=calBillDate_txtDate]').val() == "") {
                alert("Enter Bill Date")
                $('[id$=calBillDate_txtDate]').focus();
                return false;

            }

            if ($('[id$=calDueDate_txtDate]').val() == "") {
                alert("Enter Due Date")
                $('[id$=calDueDate_txtDate]').focus();
                return false;

            }

            if (CalculateTotal(true) != true) {
                return false;
            }

            if (CheckIsReconciled()) {
                IsAddClicked = true;
                return true;
            }
            else
                return false;

            //return isValid;
        }

        function CalculateTotal(TotalValidate) {
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var CrValue;
            var CrTotValue;
            var err = 0;

            $('#gvBillItems tr').not(':first,:last').each(function () {
                var txtAmount = $(this).find("input[id*='txtAmount']").val();
                ddlAccounts = $(this).find("[id*='ddlExpenseAccount']");

                if (txtAmount != undefined && txtAmount != '') {
                    if ($(ddlAccounts).val() == 0) {
                        err = 1;
                        return false;
                    }

                    itotal += parseFloat(txtAmount);

                    if (parseFloat(txtAmount) < 0) {
                        err = 2;
                        return false;
                    }
                }
            });

            if (err == 1) {
                alert('You must select an account for each split line with an amount');
                return false;
            }
            else if (err == 2) {
                alert("Each amount must be greater than Zero.");
                return false;
            }

            if (TotalValidate == true && itotal == 0) {
                alert("Please enter alteast one line with an amount and account");
                return false;
            }

            formatDigit = formatCurrency(itotal);
            $('#lblTotal').text(formatDigit);
            $('#lblTotal').val(formatDigit);
            $('#hdnTotal').val(formatDigit);
            $('#lblAmountDue').text(formatDigit);
            $('#lblAmountDue').val(formatDigit);

            return true;
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

        function OnCalenderSelect(cal) {
            if (cal.indexOf("calBillDate") > 0) {
                duedateChage(document.getElementById('hfDateFormat').value);
            }
        }

        function OrderSelectionChanged(ddlSalesOrders) {
            try {
                $(ddlSalesOrders).closest("tr").find("[id$=ddlSalesOrderItems]").empty().append('<option selected="selected" value="0">-- Select One --</option>');

                if (parseInt($(ddlSalesOrders).val()) > 0) {
                    $("[id$=UpdateProgressBill]").show();

                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetOrderExpenseItemsAddBill',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "oppID": parseInt($(ddlSalesOrders).val())
                        }),
                        beforeSend: function () {

                        },
                        complete: function () {
                            $("[id$=UpdateProgressBill]").hide();
                        },
                        success: function (data) {
                            var obj = $.parseJSON(data.GetOrderExpenseItemsAddBillResult);
                            try {
                                if (obj != null) {
                                    obj.forEach(function (n) {
                                        $(ddlSalesOrders).closest("tr").find("[id$=ddlSalesOrderItems]").append("<option value='" + n.numoppitemtCode + "'>" + (n.vcItemName || "") + "</option>");
                                    });
                                }
                            } catch (err) {
                                $("[id$=UpdateProgressBill]").hide();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                }
            } catch (e) {
                $("[id$=UpdateProgressBill]").hide();
                alert("Unknown error ocurred");
            }
        }

        function OrderItemSelectionChanged(ddlSalesOrderItems) {
            if (parseInt($(ddlSalesOrderItems).val()) > 0) {
                $(ddlSalesOrderItems).closest("td").find("[id$=ddlProject]").val("0");
                $(ddlSalesOrderItems).closest("td").find("[id$=ddlProject]").attr("disabled", "disabled");
            } else {
                $(ddlSalesOrderItems).closest("td").find("[id$=ddlProject]").removeAttr("disabled");
            }

            $(ddlSalesOrderItems).closest("td").find("[id$=hdnOppItemID]").val($(ddlSalesOrderItems).val());
        }
    </script>
    <style type="text/css">
        .required! {
        }

        .rcbInput {
            height: 19px !important;
        }

        .cont2 {
            background: #fff;
            border: 1px solid #C7D2E4;
            padding: 0px;
        }

        .cont1 {
            background: #E8ECF9;
            border: 1px solid #C7D2E4;
        }

        .tableLayout {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }

            .tableLayout > tbody > tr {
                height: 35px;
            }

                .tableLayout > tbody > tr > td:nth-child(1) {
                    min-width: 100px;
                    white-space: nowrap;
                    font-weight: bold;
                    text-align: right;
                }

                .tableLayout > tbody > tr > td:nth-child(2) {
                    padding-left: 5px;
                    min-width: 280px;
                    max-width: 280px;
                }

                .tableLayout > tbody > tr > td:nth-child(3) {
                    min-width: 145px;
                    white-space: nowrap;
                    font-weight: bold;
                    text-align: right;
                }

                .tableLayout > tbody > tr > td:nth-child(4) {
                    padding-left: 5px;
                    min-width: 180px;
                    max-width: 180px;
                }

        .gridHeader th {
            background: #345296;
            color: white;
            font-weight: bold;
            padding: 6px;
            border: 1px solid #CACED9;
            text-align: center;
        }

        .gridFooter td {
            color: white;
            font-weight: bold;
            border: 1px solid #CACED9;
        }

        .gridItem td {
            border: 1px solid #CACED9;
            background: #F0F7FF;
        }

        .gridAlternateItem td {
            background-color: #FFF;
            border: 1px solid #CACED9;
        }

        fieldset {
            border: none !important;
        }

        tr.hs td {
            background: #345296 !important;
            color: white;
            font-weight: bold;
            padding: 6px;
            border: 1px solid #CACED9;
            text-align: center;
        }

        table.dg .hs:hover td {
            background: #345296 !important;
            /*color: white;
            font-weight: bold;
            padding: 6px;
            border: 1px solid #CACED9;
            text-align: center;*/
        }

        .is td {
            border: 1px solid #CACED9;
            background: #F0F7FF;
        }

        table.dg .is:hover td {
            background: #F0F7FF !important;
        }

        .ais td {
            background-color: #FFF;
            border: 1px solid #CACED9;
        }

        table.dg .ais:hover td {
            background: #FFF !important;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div>
        <div style="float: right;">
            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="window.close()" Text="Close" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label Text="Enter Bill" runat="server" ID="lblTitle" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmaddbill.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgressBill" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <table width="850" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
        <tr>
            <td class="cont2">
                <uc2:PaymentHistory ID="PaymentHistory1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="cont1">
                <table width="100%" cellpadding="0" cellspacing="0" class="tableLayout">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rdbLstOption" RepeatDirection="Horizontal" runat="server">
                                <asp:ListItem Text="Organization" Value="0" Selected="True" onclick="ShowHide()"></asp:ListItem>
                                <asp:ListItem Text="Liability" Value="1" onclick="ShowHide()"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <asp:Panel ID="pnlCurrency" runat="server" Style="float: right;" Visible="false">
                            <td>Currency</td>
                            <td>
                                <asp:DropDownList ID="ddlCurrency" runat="server" Width="150"></asp:DropDownList>
                            </td>
                        </asp:Panel>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="cont2">
                <table width="100%" cellpadding="0" cellspacing="0" class="tableLayout">
                    <tr>
                        <td>
                            <label id="lblNameFor">
                            </label>
                            <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True"
                                ClientIDMode="Static">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                            <asp:DropDownList ID="ddlLiabilityAccount" runat="server">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnCampaign" runat="server" />
                        </td>
                        <td>Reference # <font color="white" style="visibility: hidden">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtReference" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblStageBudget" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td></td>
                        <td>Bill Date <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <BizCalendar:Calendar ID="calBillDate" runat="server" IsRequired="false" DateName="Bill Date"
                                ClientIDMode="Predictable" />
                            <asp:HiddenField ID="hfDateFormat" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Amount Paid
                        </td>
                        <td>
                            <asp:Label ID="lblAmountPaid" runat="server" Font-Bold="true" Text="0.00"></asp:Label>
                        </td>
                        <td>Amount Due
                        </td>
                        <td>
                            <asp:Label ID="lblAmountDue" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Terms <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTerms" runat="server" Width="160px">
                            </asp:DropDownList>
                        </td>
                        <td>Due Date <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <BizCalendar:Calendar ID="calDueDate" runat="server" IsRequired="false" DateName="Due Date"
                                ClientIDMode="Predictable" />
                        </td>
                    </tr>
                    <tr>
                        <td>Memo<font color="white" style="visibility: hidden">*</font>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtMemo" runat="server" CssClass="signup" Width="550px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td>
                            <b>
                                <asp:Label ID="lblTotal" runat="server" ForeColor="#4E9E19" Font-Size="Large"></asp:Label></b>
                            <asp:HiddenField runat="server" ID="hdnTotal" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="cont1">
                <asp:GridView ID="gvBillItems" CssClass="tblPrimary" runat="server" ShowFooter="true" AutoGenerateColumns="false" Width="100%">
                    <HeaderStyle CssClass="gridHeader" Height="30" />
                    <FooterStyle CssClass="gridFooter" />
                    <RowStyle CssClass="gridItem" Height="25" />
                    <AlternatingRowStyle CssClass="gridAlternateItem" Height="25" />
                    <Columns>
                        <asp:TemplateField HeaderText="&nbsp;#&nbsp;" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expense Account<font color=#ff0000>*</font>">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdnBillDetailID" Value='<%# Eval("numBillDetailID") %>' />
                                <asp:DropDownList ID="ddlExpenseAccount" runat="server" CssClass="{required:false,messages:{required:'Select Expense Account!'}}">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add New Row" OnClick="btnAdd_Click" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount<font color=#ff0000>*</font>">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" Width="50px" AutoComplete="OFF" Text='<%# ReturnMoney(Eval("monAmount"))%>'
                                    CssClass="required_decimal {required:false,number:true, messages:{required:'Enter Amount! ',number:'provide valid value!'}}"
                                    onchange="return CalculateTotal(true)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" CssClass="signup" TextMode="MultiLine"
                                    Rows="1" Width="300px" Height="18px" Text='<%# Eval("vcDescription") %>'></asp:TextBox>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sales Order">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlSalesOrders" runat="server" Width="100px" CssClass="signup" onchange="OrderSelectionChanged(this);">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlSalesOrderItems" runat="server" Width="100px" CssClass="signup" onchange="OrderItemSelectionChanged(this);">
                                    <asp:ListItem Value="0" Text="-- Select One --"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnOppItemID" runat="server" />
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlProject" runat="server" Width="100px" CssClass="signup">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlClass" runat="server" CssClass="signup" Width="100px">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Campaign">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="signup" Width="100px">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />

                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteRow" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteRow"
                                    CommandArgument="<%# Container.DataItemIndex %>"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="cont2">
                <br />
                <table width="100%" cellpadding="0" cellspacing="0" class="tableLayout">
                    <tr>
                        <td style="padding-left: 10px;">
                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="Delete" OnClientClick="return confirm('Are you sure, you want to delete bill?')" Style="float:left;background-color:#dd4b39;border-color: #d73925;color: #fff;" />
                        </td>
                        <td align="right" style="padding-right: 10px;">
                            <asp:Button ID="btnSaveClone" runat="server" CssClass="btn btn-primary" Text="Clone and Close"   OnClientClick="return Save();" Visible="false" />
                            <asp:Button ID="btnSaveNew" runat="server" CssClass="btn btn-primary" Text="Save and New" OnClientClick="return Save();" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save and Close" OnClientClick="return Save();" />
                            <asp:HiddenField ID="hfBillID" runat="server" Value="0" />
                            <asp:HiddenField ID="hfProjectID" runat="server" Value="0" />
                            <asp:HiddenField ID="hfStageID" runat="server" Value="0" />
                            <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
                            <asp:HiddenField ID="hfDivisionID" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
