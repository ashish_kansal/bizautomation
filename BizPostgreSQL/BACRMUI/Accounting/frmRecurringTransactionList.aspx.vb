﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmRecurringTransactionList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindGrid()
                
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim recEvent As New RecurringEvents
            Dim dtRec As DataTable
            recEvent.DomainId = Session("DomainID")
            recEvent.OpportunityId = CCommon.ToInteger(GetQueryStringVal("OpID"))
            recEvent.RecurringType = CCommon.ToInteger(GetQueryStringVal("RecType"))
            dtRec = recEvent.GetOpportunityRecurringList()
            If (dtRec.Rows.Count > 0) Then
                dgRec.DataSource = dtRec
                dgRec.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class