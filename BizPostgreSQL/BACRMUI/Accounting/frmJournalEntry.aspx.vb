' ''Created By Siva
'Imports BACRM.BusinessLogic.Common
'Imports BACRM.BusinessLogic.Accounting
'Partial Public Class frmJournalEntry
'    Inherits BACRMPage

'#Region "Variables"
'    Dim mintChartAcntId As Integer
'    Dim mintAcntTypeId As Integer

'    Dim objJournalEntry As JournalEntry
'    Dim lobjReceivePayment As New ReceivePayment
'    Dim lngDivisionID As Long
'    Dim intMode As Int32
'    Dim strDateFrom As String
'    Dim strDateTo As String
'    Dim lngDebitTotal As Double
'    Dim lngCreditTotal As Double
'    Dim dtFYClosingDate As Date
'#End Region

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            mintChartAcntId = CCommon.ToInteger(GetQueryStringVal("ChartAcntId"))
'            lngDivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
'            intMode = CCommon.ToInteger(GetQueryStringVal("ModeID"))
'            strDateFrom = GetQueryStringVal("FromDate")
'            strDateTo = GetQueryStringVal("ToDate")
'            'To Set Permission
'            GetUserRightsForPage(35, 77)


'            'Get COA Name Added by Sojan 02 March 2010
'            Call GetCOAName()
'            If Not IsPostBack Then

'                BindGrid()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub
'    Private Sub BindGrid()
'        Try
'            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

'            Dim dt As DataTable
'            Dim objRow As DataRow
'            objJournalEntry.ChartAcntId = mintChartAcntId
'            objJournalEntry.DomainID = Session("DomainId")
'            objJournalEntry.DivisionId = lngDivisionID
'            If calFrom.SelectedDate <> "" Then
'                objJournalEntry.FilterbyEntry_Date = calFrom.SelectedDate
'            End If

'            'Get closing date of financial year to Lock transaction from being updated

'            Dim objCOA As New ChartOfAccounting
'            objCOA.FinancialYearID = 0
'            objCOA.DomainID = Session("DomainID")
'            objCOA.Mode = 4
'            Dim ds As DataSet = objCOA.GetFinancialYear()
'            If ds.Tables(0).Rows.Count > 0 Then
'                If ds.Tables(0).Rows(0)("FYClosingDate").ToString <> "" Then
'                    dtFYClosingDate = DateFromFormattedDate(ds.Tables(0).Rows(0)("FYClosingDate").ToString, Session("DateFormat"))
'                End If
'            End If

'            FormatDataGrid(mintChartAcntId)


'            If intMode = 0 Then
'                'webmenu1.Visible = True
'                dt = objJournalEntry.GetJournalEntryList
'                mintAcntTypeId = objJournalEntry.GetChartOfAcntsTypeId()
'                lngDebitTotal = CCommon.ToDouble(dt.Compute("sum(Deposit)", ""))
'                lngCreditTotal = CCommon.ToDouble(dt.Compute("sum(Payment)", ""))
'            ElseIf intMode = 1 Then
'                'webmenu1.Visible = False
'                dt = objJournalEntry.GetJournalEntryListMultiCOA
'                lngDebitTotal = CCommon.ToDouble(dt.Compute("sum(Deposit)", ""))
'                lngCreditTotal = CCommon.ToDouble(dt.Compute("sum(Payment)", ""))
'                mintAcntTypeId = 0
'            ElseIf intMode = 2 Then
'                'webmenu1.Visible = True
'                dt = GetCOGSReport()
'                lngDebitTotal = CCommon.ToDouble(dt.Compute("sum(Deposit)", ""))
'            End If


'            objRow = dt.NewRow
'            objRow.Item("JournalId") = 0
'            objRow.Item("numTransactionId") = 0
'            objRow.Item("CheckId") = 0
'            objRow.Item("CashCreditCardId") = 0
'            objRow.Item("numChartAcntId") = 0
'            objRow.Item("numOppId") = 0
'            objRow.Item("numOppBizDocsId") = 0
'            objRow.Item("numDepositId") = 0
'            objRow.Item("numBizDocsPaymentDetId") = 0
'            objRow.Item("numCategoryHDRID") = 0
'            objRow.Item("tintTEType") = 0
'            objRow.Item("numCategory") = 0
'            objRow.Item("numUserCntID") = 0
'            objRow.Item("TransactionType") = ""
'            objRow.Item("CompanyName") = ""
'            objRow.Item("Memo") = "<b> Total </b>"
'            objRow.Item("Deposit") = lngDebitTotal
'            objRow.Item("Payment") = lngCreditTotal
'            dt.Rows.Add(objRow)
'            dt.AcceptChanges()

'            dgJournalEntry.DataSource = dt
'            dgJournalEntry.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Function GetCOGSReport() As DataTable
'        Try
'            Dim objProfitLoss As New ProfitLoss
'            Dim objDT As New DataTable

'            calFrom.SelectedDate = strDateFrom

'            objProfitLoss.DomainID = Session("DomainID")
'            objProfitLoss.FromDate = CDate(strDateFrom)
'            objProfitLoss.ToDate = CDate(strDateTo)
'            objDT = objProfitLoss.GetCOGSReport

'            Return objDT

'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Function
'    Private Sub GetCOAName()
'        Try
'            Dim objAccounting As New ChartOfAccounting

'            Dim objDt As New DataTable

'            With objAccounting
'                .DomainID = Session("DomainID")
'                .AccountId = mintChartAcntId
'                objDt = .GetChartDetails
'            End With
'            If objDt.Rows.Count > 0 Then
'                lblJournalEntryTitle.Text = "GL Transaction - " & objDt.Rows(0).Item("vcAccountName")
'                lblOpeningBalance.Text = ReturnMoney(objDt.Rows(0).Item("numOriginalOpeningBal"))
'                FormatDataGrid(objDt.Rows(0).Item("vcAccountCode"))
'            End If
'            If lngDivisionID = 0 Then Exit Sub
'            With objCommon
'                .DomainID = Session("DomainID")
'                .DivisionID = lngDivisionID
'                lblJournalEntryTitle.Text = "GL Transaction - " & .GetCompanyName
'            End With

'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub
'    Private Sub FormatDataGrid(ByVal p_AcntTypeId As String)
'        Try
'            If intMode = 2 Then
'                lblJournalEntryTitle.Text = "COGS"
'                dgJournalEntry.Columns(16).HeaderText = "BizDoc Name"
'                dgJournalEntry.Columns(17).HeaderText = "Item"
'                dgJournalEntry.Columns(18).HeaderText = "Model Id"
'                dgJournalEntry.Columns(19).HeaderText = "COGS Value"
'                dgJournalEntry.Columns(20).Visible = False
'                dgJournalEntry.Columns(21).Visible = False
'                dgJournalEntry.Columns(22).Visible = False
'                dgJournalEntry.Columns(23).Visible = False
'                Exit Sub
'            End If



'            If p_AcntTypeId = "0" Or p_AcntTypeId = "" Then
'                'webmenu1.Visible = False
'                btnBack.Visible = False
'                btnGo.Visible = False
'                JournalDate.Visible = False
'                calFrom.Visible = False
'                lblJournalEntryTitle.Text = "GL Transaction (Fiscal YTD)"
'                dgJournalEntry.Columns(19).HeaderText = "Debit"
'                dgJournalEntry.Columns(20).HeaderText = "Credit"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(21).Visible = False
'                dgJournalEntry.Columns(22).Visible = False
'                dgJournalEntry.Columns(23).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 8) = "01010101" Or Mid(p_AcntTypeId, 1, 8) = "01010102" Or Mid(p_AcntTypeId, 1, 8) = "01010103" Then
'                'lblJournalEntryTitle.Text = "Credit Card Register"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(19).HeaderText = "Receipt"
'                dgJournalEntry.Columns(20).HeaderText = "Payment"
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 8) = "01010105" Then
'                'lblJournalEntryTitle.Text = "Credit Card Register"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(19).HeaderText = "Billed\Charged"
'                dgJournalEntry.Columns(20).HeaderText = "Receipt"
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 8) = "01010104" Then
'                'lblJournalEntryTitle.Text = "Credit Card Register"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(19).HeaderText = "Stock In"
'                dgJournalEntry.Columns(20).HeaderText = "Stock Out"
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 6) = "010102" Then
'                'lblJournalEntryTitle.Text = "A/R Register"
'                dgJournalEntry.Columns(19).HeaderText = "Debit"
'                dgJournalEntry.Columns(20).HeaderText = "Credit"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 6) = "010201" Then
'                'lblJournalEntryTitle.Text = "A/P Register"
'                dgJournalEntry.Columns(19).HeaderText = "Payment"
'                dgJournalEntry.Columns(20).HeaderText = "Billed\Charged"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 6) = "010102" Then
'                'lblJournalEntryTitle.Text = "A/P Register"
'                dgJournalEntry.Columns(19).HeaderText = "Credit"
'                dgJournalEntry.Columns(20).HeaderText = "Debit"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 4) = "0103" Or Mid(p_AcntTypeId, 1, 4) = "0104" Then
'                'lblJournalEntryTitle.Text = "A/P Register"
'                dgJournalEntry.Columns(19).HeaderText = "Expenses"
'                dgJournalEntry.Columns(20).HeaderText = "Income"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(21).Visible = False
'            ElseIf Mid(p_AcntTypeId, 1, 4) = "0105" Then
'                'lblJournalEntryTitle.Text = "A/P Register"
'                dgJournalEntry.Columns(19).HeaderText = "Increase"
'                dgJournalEntry.Columns(20).HeaderText = "Decrease"
'                dgJournalEntry.Columns(0).Visible = False
'                dgJournalEntry.Columns(21).Visible = False

'                'ElseIf p_AcntTypeId = 817 Or p_AcntTypeId = 818 Or p_AcntTypeId = 819 Then
'                '    lblJournalEntryTitle.Text = "Asset Register"
'                '    dgJournalEntry.Columns(19).HeaderText = "Increase"
'                '    dgJournalEntry.Columns(20).HeaderText = "Decrease"
'                'ElseIf p_AcntTypeId = 820 Or p_AcntTypeId = 827 Then
'                '    lblJournalEntryTitle.Text = "Liability Register"
'                '    dgJournalEntry.Columns(19).HeaderText = "Increase"
'                '    dgJournalEntry.Columns(20).HeaderText = "Decrease"
'                'ElseIf p_AcntTypeId = 821 Then
'                '    lblJournalEntryTitle.Text = "Equity Register"
'                '    dgJournalEntry.Columns(19).HeaderText = "Increase"
'                '    dgJournalEntry.Columns(20).HeaderText = "Decrease"
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgJournalEntry.EditCommand
'        Try
'            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry
'            Dim dt As DataTable

'            dgJournalEntry.EditItemIndex = e.Item.ItemIndex
'            If calFrom.SelectedDate <> "" Then objJournalEntry.Entry_Date = calFrom.SelectedDate

'            objJournalEntry.ChartAcntId = mintChartAcntId
'            objJournalEntry.DomainID = Session("DomainId")
'            dt = objJournalEntry.GetJournalEntryList
'            dgJournalEntry.DataSource = dt
'            mintAcntTypeId = objJournalEntry.GetChartOfAcntsTypeId()
'            FormatDataGrid(mintAcntTypeId)
'            dgJournalEntry.DataBind()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgJournalEntry.ItemCommand
'        Try
'            Dim lblTransId As New Label
'            Dim ltxtPayment As New TextBox
'            Dim ltxtDeposit As New TextBox
'            Dim ltxtMemo As New TextBox
'            Dim lddlCompany As New DropDownList
'            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

'            Dim lintJournalIdCount As Integer
'            Dim lblJournalId As Label
'            Dim ltxtJournalDate As BACRM.Include.calandar
'            Dim lblCheckId As Label
'            Dim lblCashCreditCardId As Label
'            Dim lblChartAcntId As Label
'            Dim lblBizDocsPaymentDetId As Label
'            Dim lintAcntTypeId As Integer

'            Dim linJournalId As Integer
'            Dim lintCheckId As Integer
'            Dim lintCashCreditCardId As Integer
'            Dim lintOppId As Integer
'            Dim lintOppBizDocsId As Integer
'            Dim lintDepositId As Integer
'            Dim lintBizDocsPaymentDetId As Integer

'            linJournalId = IIf(e.Item.Cells(1).Text = "&nbsp;" OrElse e.Item.Cells(1).Text = "", 0, e.Item.Cells(1).Text)
'            lintCheckId = IIf(e.Item.Cells(3).Text = "&nbsp;" OrElse e.Item.Cells(3).Text = "", 0, e.Item.Cells(3).Text)
'            lintCashCreditCardId = IIf(e.Item.Cells(4).Text = "&nbsp;" OrElse e.Item.Cells(4).Text = "", 0, e.Item.Cells(4).Text)
'            lintOppId = IIf(e.Item.Cells(6).Text = "&nbsp;" OrElse e.Item.Cells(6).Text = "", 0, e.Item.Cells(6).Text)
'            lintOppBizDocsId = IIf(e.Item.Cells(7).Text = "&nbsp;" OrElse e.Item.Cells(7).Text = "", 0, e.Item.Cells(7).Text)
'            lintDepositId = IIf(e.Item.Cells(8).Text = "&nbsp;" OrElse e.Item.Cells(8).Text = "", 0, e.Item.Cells(8).Text)
'            lintBizDocsPaymentDetId = IIf(e.Item.Cells(9).Text = "&nbsp;" OrElse e.Item.Cells(9).Text = "", 0, e.Item.Cells(9).Text)

'            If e.CommandName = "EditJournalEntry" Then
'                If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppId = 0 And lintOppBizDocsId = 0 And lintDepositId = 0 Then
'                    ''lblJournalId = CType(e.Item.FindControl("lblJournalId"), Label)
'                    Response.Redirect("../Accounting/frmNewJournalEntry.aspx?JournalId=" & linJournalId & "&ChartAcntId=" & mintChartAcntId)
'                ElseIf lintCheckId <> 0 Then
'                    Response.Redirect("../Accounting/frmChecks.aspx?frm=BankRegister&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "&ChartAcntId=" & mintChartAcntId)
'                ElseIf lintCashCreditCardId <> 0 Then
'                    Response.Redirect("../Accounting/frmCash.aspx?frm=BankRegister&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "&ChartAcntId=" & mintChartAcntId)
'                    ''ElseIf lintOppId <> 0 And lintOppBizDocsId <> 0 Then
'                    ''    Dim lstr As String
'                    ''    lstr = "<script language=javascript>"
'                    ''    lstr += "window.open('../opportunity/frmBizInvoice.aspx?OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "','','toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');"
'                    ''    lstr += "</script>"
'                    ''    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "BizInvoice", lstr)
'                ElseIf lintDepositId <> 0 Then
'                    Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=BankRegister&JournalId=" & linJournalId & "&DepositId=" & lintDepositId & "&ChartAcntId=" & mintChartAcntId)
'                End If

'            ElseIf e.CommandName = "Update" Then
'                lblTransId = CType(e.Item.FindControl("lblTransactionId"), Label)
'                ltxtPayment = CType(e.Item.FindControl("txtPayment"), TextBox)
'                ltxtDeposit = CType(e.Item.FindControl("txtDeposit"), TextBox)
'                lddlCompany = CType(e.Item.FindControl("ddlCompany"), DropDownList)
'                ltxtMemo = CType(e.Item.FindControl("txtEDesc"), TextBox)
'                lblJournalId = CType(e.Item.FindControl("lblJournalId"), Label)
'                ltxtJournalDate = CType(e.Item.FindControl("txtJournalDateDisplay"), BACRM.Include.calandar)
'                lblCheckId = CType(e.Item.FindControl("lblCheckId"), Label)

'                lblCashCreditCardId = CType(e.Item.FindControl("lblCashCreditCardId"), Label)

'                lblChartAcntId = CType(e.Item.FindControl("lblChartAcntId"), Label)
'                objJournalEntry.ChartAcntId = lblChartAcntId.Text

'                objJournalEntry.DomainID = Session("DomainId")
'                objJournalEntry.TransactionId = IIf(lblTransId.Text = "", 0, lblTransId.Text)
'                objJournalEntry.Description = ltxtMemo.Text
'                objJournalEntry.JournalId = IIf(lblJournalId.Text = "", 0, lblJournalId.Text)
'                objJournalEntry.Entry_Date = ltxtJournalDate.SelectedDate

'                lintAcntTypeId = objJournalEntry.GetChartOfAcntsTypeId()
'                If (lintAcntTypeId = 816 Or lintAcntTypeId = 817 Or lintAcntTypeId = 820 Or lintAcntTypeId = 821 Or lintAcntTypeId = 827) And objJournalEntry.JournalId <> 0 Then
'                    objJournalEntry.DebitAmt = IIf(ltxtPayment.Text = "", 0, Replace(ltxtPayment.Text, ",", ""))
'                    objJournalEntry.CreditAmt = IIf(ltxtDeposit.Text = "", 0, Replace(ltxtDeposit.Text, ",", ""))
'                Else
'                    objJournalEntry.DebitAmt = IIf(ltxtDeposit.Text = "", 0, Replace(ltxtDeposit.Text, ",", ""))
'                    objJournalEntry.CreditAmt = IIf(ltxtPayment.Text = "", 0, Replace(ltxtPayment.Text, ",", ""))
'                End If
'                If lblJournalId.Text <> "" Then
'                    If lddlCompany.SelectedIndex <> -1 Then
'                        objJournalEntry.CustomerId = IIf(IsNothing(lddlCompany.SelectedItem.Value), 0, lddlCompany.SelectedItem.Value)
'                    End If
'                End If

'                If objJournalEntry.JournalId <> 0 Then
'                    If objJournalEntry.CreditAmt <> 0 Then
'                        objJournalEntry.Balance = -objJournalEntry.CreditAmt
'                    ElseIf objJournalEntry.DebitAmt <> 0 Then
'                        objJournalEntry.Balance = objJournalEntry.DebitAmt
'                    End If
'                    lintJournalIdCount = objJournalEntry.GetJournalEntryDetailsCount()
'                    objJournalEntry.CheckId = IIf(lblCheckId.Text = "", 0, lblCheckId.Text)
'                    objJournalEntry.CashCreditCardId = IIf(lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)
'                    objJournalEntry.DomainID = Session("DomainId")
'                    If lintJournalIdCount < 3 Then
'                        objJournalEntry.UpdateType = 1
'                        objJournalEntry.UpdateJournalDetails()
'                    Else
'                        objJournalEntry.UpdateType = 0
'                        objJournalEntry.UpdateJournalDetails()
'                    End If
'                    ''Else
'                    ''    If objJournalEntry.CreditAmt <> 0 Then
'                    ''        If mintChartAcntId = 15 Then
'                    ''            objJournalEntry.OpeningBalance = objJournalEntry.CreditAmt
'                    ''        Else
'                    ''            objJournalEntry.OpeningBalance = -objJournalEntry.CreditAmt
'                    ''        End If

'                    ''    ElseIf objJournalEntry.DebitAmt <> 0 Then
'                    ''        objJournalEntry.OpeningBalance = objJournalEntry.DebitAmt
'                    ''    End If
'                    ''    ''objJournalEntry.ChartAcntId = lblChartAcntId.Text

'                    ''    objJournalEntry.UpdateChartAcntOpenBal()
'                End If
'                dgJournalEntry.EditItemIndex = -1
'                Call Fillgrid()
'            ElseIf e.CommandName = "Cancel" Then
'                dgJournalEntry.EditItemIndex = e.Item.ItemIndex
'                dgJournalEntry.EditItemIndex = -1
'                Call Fillgrid()
'            ElseIf e.CommandName = "Go" Then
'                Dim ddlCompany As DropDownList
'                Dim txtCompanyName As TextBox

'                ddlCompany = CType(e.Item.FindControl("ddlCompany"), DropDownList)
'                txtCompanyName = CType(e.Item.FindControl("txtCompanyName"), TextBox)
'                If Not IsNothing(ddlCompany) Then
'                    If txtCompanyName.Text <> "" Then LoadItem(ddlCompany, txtCompanyName.Text)
'                End If
'            ElseIf e.CommandName = "Delete" Then
'                Dim lintTransactionId As Integer
'                Dim lintChartAcntId As Integer


'                lintTransactionId = IIf(e.Item.Cells(2).Text = "&nbsp;", 0, e.Item.Cells(2).Text) ''e.Item.Cells(2).Text
'                lintChartAcntId = IIf(e.Item.Cells(5).Text = "&nbsp;", 0, e.Item.Cells(5).Text)

'                If linJournalId <> 0 Then
'                    objJournalEntry.JournalId = linJournalId ''lblJournalId.Text
'                    objJournalEntry.CheckId = lintCheckId
'                    objJournalEntry.CashCreditCardId = lintCashCreditCardId
'                    objJournalEntry.DepositId = lintDepositId
'                    objJournalEntry.DomainID = Session("DomainId")
'                    Try
'                        objJournalEntry.DeleteJournalEntryDetails()
'                    Catch ex As Exception
'                        If ex.Message = "BILL_PAID" Then
'                            litMessage.Text = "Paid bill can not be deleted, your option is to remove vendor payment journal(s) first and try again."
'                            Exit Sub
'                        Else
'                            Throw ex
'                        End If
'                    End Try

'                    ' This is to reverse the transaction from account section
'                    ' Created by Rajaram on 31/05/2008

'                    Dim lobjReceivePayment As New ReceivePayment
'                    If lobjReceivePayment Is Nothing Then lobjReceivePayment = New ReceivePayment

'                    lobjReceivePayment.BizDocsPaymentDetId = lintBizDocsPaymentDetId
'                    lobjReceivePayment.BizDocsId = lintOppBizDocsId
'                    lobjReceivePayment.OppId = lintOppId

'                    lobjReceivePayment.UpdateOpportunityBizDocsIntegratedToAcnt()
'                    lobjReceivePayment.DeletePaymentDetails()
'                    Call Fillgrid()
'                    'Else
'                    'objJournalEntry.ChartAcntId = lintChartAcntId ''lintTransactionId
'                    'objJournalEntry.DomainId = Session("DomainId")
'                    'objJournalEntry.DeleteOpeningBalanceDetails()
'                    'objJournalEntry.DeleteOpeningBalanceDetails()
'                    'Call Fillgrid()
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub LoadItem(ByVal ddlitems As DropDownList, ByVal p_WhereCondition As String)
'        Try
'            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry
'            objJournalEntry.DomainID = Session("DomainID")
'            objJournalEntry.strWhereCondition = p_WhereCondition
'            ddlitems.DataSource = objJournalEntry.GetCompanyDetails
'            ddlitems.DataTextField = "vcCompanyName"
'            ddlitems.DataValueField = "numDivisionID"
'            ddlitems.DataBind()
'            ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Function Fillgrid()
'        Try
'            Dim dt As DataTable
'            Dim lintAcntTypeId As Integer
'            '''txtFromDateDisplay.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
'            If calFrom.SelectedDate <> "" Then objJournalEntry.FilterbyEntry_Date = calFrom.SelectedDate

'            objJournalEntry.ChartAcntId = mintChartAcntId
'            objJournalEntry.DomainID = Session("DomainId")
'            dt = objJournalEntry.GetJournalEntryList
'            dgJournalEntry.DataSource = dt
'            lintAcntTypeId = objJournalEntry.GetChartOfAcntsTypeId()
'            FormatDataGrid(lintAcntTypeId)
'            dgJournalEntry.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Function ReturnDate(ByVal CloseDate) As String
'        Try
'            If Not IsDBNull(CloseDate) Then Return (FormattedDateFromDate(CloseDate, Session("DateFormat")))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Function FormatDate(ByVal EntryDate) As String
'        Try
'            Return FormattedDateFromDate(EntryDate, Session("DateFormat"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
'        Try
'            BindGrid()
'            'Dim dt As DataTable
'            'If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

'            'Dim lintAcntTypeId As Integer
'            'If calFrom.SelectedDate <> "" Then
'            '    objJournalEntry.FilterbyEntry_Date = calFrom.SelectedDate
'            '    objJournalEntry.ChartAcntId = mintChartAcntId
'            '    objJournalEntry.DomainId = Session("DomainId")
'            '    objJournalEntry.DivisionId = lngDivisionID
'            '    dt = objJournalEntry.GetJournalEntryList
'            '    dgJournalEntry.DataSource = dt
'            '    'lintAcntTypeId = objJournalEntry.GetChartOfAcntsTypeId()
'            '    'FormatDataGrid(lintAcntTypeId)
'            '    dgJournalEntry.DataBind()
'            'Else
'            '    objJournalEntry.ChartAcntId = mintChartAcntId
'            '    objJournalEntry.DomainId = Session("DomainId")
'            '    objJournalEntry.DivisionId = lngDivisionID
'            '    dt = objJournalEntry.GetJournalEntryList
'            '    dgJournalEntry.DataSource = dt
'            '    'lintAcntTypeId = objJournalEntry.GetChartOfAcntsTypeId()
'            '    'FormatDataGrid(lintAcntTypeId)
'            '    dgJournalEntry.DataBind()
'            'End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgJournalEntry.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.EditItem Then
'                If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

'                Dim ddlChartAccounts As DropDownList
'                Dim txtEPayment As TextBox
'                Dim txtEDate As BACRM.Include.calandar
'                Dim txtEDeposit As TextBox
'                Dim lintJournalIdCount As Integer
'                Dim lblJournal As Label
'                Dim lblPaymentAmt As Label
'                Dim lblDepositAmt As Label
'                Dim lblJournalId As Label
'                Dim txtEDesc As TextBox
'                Dim lblEditMemo As Label
'                Dim lnkbtnEditJournal As LinkButton
'                Dim lblEditDate As Label
'                Dim txtCompanyName As TextBox
'                Dim btnCompanyName As Button
'                Dim lblChartAcntId As Label
'                Dim lintAcntTypeId As Integer
'                Dim lblCheckId As Label
'                Dim lblCompany As Label

'                txtCompanyName = e.Item.FindControl("txtCompanyName")
'                btnCompanyName = e.Item.FindControl("btnCompanyName")
'                txtEDate = e.Item.FindControl("txtJournalDateDisplay")
'                lblEditDate = e.Item.FindControl("lblEditDate")

'                txtEPayment = e.Item.FindControl("txtPayment")
'                txtEDeposit = e.Item.FindControl("txtDeposit")

'                lblJournalId = e.Item.FindControl("lblJournalId")
'                txtEDesc = e.Item.FindControl("txtEDesc")
'                lnkbtnEditJournal = e.Item.FindControl("lnkbtnEditJournal")
'                txtEPayment.Attributes.Add("onkeypress", "CheckNumber(1)")
'                txtEDeposit.Attributes.Add("onkeypress", "CheckNumber(1)")
'                lblJournal = e.Item.FindControl("lblJournalId")
'                lblPaymentAmt = e.Item.FindControl("lblPaymentAmt")
'                lblDepositAmt = e.Item.FindControl("lblDepositAmt")
'                lblEditMemo = e.Item.FindControl("lblEditMemo")
'                lblChartAcntId = e.Item.FindControl("lblChartAcntId")
'                lintAcntTypeId = lblChartAcntId.Text
'                objJournalEntry.JournalId = IIf(lblJournal.Text = "", 0, lblJournal.Text)
'                objJournalEntry.DomainID = Session("DomainId")
'                lblCheckId = e.Item.FindControl("lblCheckId")
'                lblCompany = e.Item.FindControl("lblCompany")

'                lintJournalIdCount = objJournalEntry.GetJournalEntryDetailsCount()
'                If lintJournalIdCount > 2 Then
'                    txtEPayment.Visible = False
'                    txtEDeposit.Visible = False
'                    lblDepositAmt.Visible = True
'                    lblPaymentAmt.Visible = True
'                ElseIf txtEPayment.Text = "" Then
'                    txtEPayment.Visible = False
'                    lblDepositAmt.Visible = False
'                ElseIf txtEDeposit.Text = "" Then
'                    txtEDeposit.Visible = False
'                    lblPaymentAmt.Visible = False
'                End If
'                ddlChartAccounts = e.Item.FindControl("ddlCompany")

'                If lblCheckId.Text <> "" Then
'                    txtCompanyName.Visible = False
'                    ddlChartAccounts.Visible = False
'                    txtCompanyName.Visible = False
'                    lblCompany.Visible = True
'                    btnCompanyName.Visible = False
'                Else
'                    ddlChartAccounts.CssClass = "signup"
'                    ddlChartAccounts.Width = Unit.Pixel(150)
'                    txtCompanyName.Visible = True
'                    btnCompanyName.Visible = True
'                    lblCompany.Visible = False
'                    If CType(e.Item.FindControl("lblDivisionId"), Label).Text <> "" Then
'                        LoadCompanyDropDown(ddlChartAccounts, CType(e.Item.FindControl("lblDivisionId"), Label).Text)
'                    End If
'                    If CType(e.Item.FindControl("lblDivisionId"), Label).Text <> "" Then
'                        ddlChartAccounts.Items.FindByValue(CType(e.Item.FindControl("lblDivisionId"), Label).Text).Selected = True
'                    End If
'                End If

'                If lblJournalId.Text = "" Then
'                    ddlChartAccounts.Visible = False
'                    txtEDesc.Visible = False
'                    lblEditMemo.Visible = True
'                    txtEDate.Visible = False
'                    lblEditDate.Visible = True
'                    btnCompanyName.Visible = False
'                    txtCompanyName.Visible = False
'                ElseIf lblJournalId.Text <> "" Then
'                    lblEditDate.Visible = False
'                    lblEditMemo.Visible = False
'                End If
'            End If
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim lintOppId As Integer
'                Dim lintOppBizDocsId As Integer
'                Dim lintCategoryHDRID As Integer
'                Dim lintCategory As Integer
'                Dim lintTEType As Integer
'                Dim lintUserCntID As Integer
'                Dim lnkbtnEditJournal As LinkButton
'                Dim lblJDate As Label
'                Dim lblPayment As Label
'                Dim lblCheckId As Label
'                Dim lblCheckNO As Label
'                Dim lblMemo As Label

'                lblPayment = e.Item.FindControl("lblPayment")
'                lblCheckId = e.Item.FindControl("lblCheckId")
'                lblCheckNO = e.Item.FindControl("lblCheckNo")
'                lblMemo = e.Item.FindControl("lblMemo")
'                lnkbtnEditJournal = e.Item.FindControl("lnkbtnEditJournal")
'                lintOppId = IIf(e.Item.Cells(6).Text = "&nbsp;" OrElse e.Item.Cells(6).Text = "", 0, e.Item.Cells(6).Text)
'                lintOppBizDocsId = IIf(e.Item.Cells(7).Text = "&nbsp;" OrElse e.Item.Cells(7).Text = "", 0, e.Item.Cells(7).Text)
'                lintCategoryHDRID = IIf(e.Item.Cells(10).Text = "&nbsp;" OrElse e.Item.Cells(10).Text = "", 0, e.Item.Cells(10).Text)


'                lintTEType = IIf(e.Item.Cells(11).Text = "&nbsp;" OrElse e.Item.Cells(11).Text = "", 0, e.Item.Cells(11).Text)
'                lintCategory = IIf(e.Item.Cells(12).Text = "&nbsp;" OrElse e.Item.Cells(12).Text = "", 0, e.Item.Cells(12).Text)
'                lintUserCntID = IIf(e.Item.Cells(13).Text = "&nbsp;" OrElse e.Item.Cells(13).Text = "", 0, e.Item.Cells(13).Text)


'                If lblCheckId.Text <> "" And lblCheckId.Text <> "0" Then
'                    lblPayment.Text = ReturnMoney(e.Item.DataItem.Row("Payment"))
'                    lblMemo.Text = lblMemo.Text + " Chek No: " + lblCheckNO.Text
'                Else
'                    lblPayment.Text = ReturnMoney(e.Item.DataItem.Row("Payment"))
'                End If

'                If lintOppId <> 0 And lintOppBizDocsId <> 0 Then
'                    lnkbtnEditJournal.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
'                ElseIf lintCategoryHDRID <> 0 Then
'                    Select Case lintTEType
'                        Case 0
'                            Dim strFromDate As String = ""
'                            If e.Item.Cells(14).Text <> "&nbsp;" Then
'                                strFromDate = CDate(e.Item.Cells(14).Text)
'                            End If
'                            lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRegister&CatID=" & lintCategoryHDRID & "&CntID=" & lintUserCntID & "&Date=" & strFromDate & "')")
'                        Case 1
'                            If lintCategory = 1 Then
'                                lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?frm=TE&ProStageID=0&Proid=0&DivId=&Date=&CatID=&CntID=" & lintUserCntID & "&CatHdrId=" & lintCategoryHDRID & "')")
'                            Else
'                                lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRegister&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lintUserCntID & "')")
'                            End If
'                        Case 2
'                            If lintCategory = 1 Then
'                                lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRegister&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lintUserCntID & "')")
'                            Else
'                                lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRegister&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lintUserCntID & "')")
'                            End If
'                        Case 3
'                            If lintCategory = 1 Then
'                                lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRegister&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lintUserCntID & "')")
'                            Else
'                                lnkbtnEditJournal.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRegister&CatHDRID=" & lintCategoryHDRID & "&CntID=" & lintUserCntID & "')")
'                            End If
'                    End Select
'                End If

'                Dim btnDeleteAction As Button
'                Dim lnkDelete As LinkButton
'                Dim lnkbtnEdit As LinkButton

'                btnDeleteAction = e.Item.FindControl("btnDeleteAction")
'                lnkDelete = e.Item.FindControl("lnkDeleteAction")
'                lnkbtnEdit = e.Item.FindControl("lnkbtnEdt")

'                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then lnkbtnEdit.Visible = False

'                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
'                    btnDeleteAction.Visible = False
'                    lnkDelete.Visible = True
'                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
'                Else
'                    btnDeleteAction.Attributes.Add("onclick", "return DeleteRecord();")
'                    If mintAcntTypeId <> 814 AndAlso mintAcntTypeId <> 815 Then
'                        Dim lnkDeleteAction As LinkButton
'                        Dim lnkbtnEdt As LinkButton

'                        lnkDeleteAction = e.Item.FindControl("lnkDeleteAction")
'                        lnkbtnEditJournal = e.Item.FindControl("lnkbtnEditJournal")
'                        lnkbtnEdt = e.Item.FindControl("lnkbtnEdt")

'                        '' lblJournalId = e.Item.FindControl("lblJournalId")
'                        If e.Item.Cells(1).Text = "&nbsp;" Then lnkbtnEditJournal.Visible = False
'                        If e.Item.Cells(6).Text <> "&nbsp;" Or e.Item.Cells(8).Text <> "&nbsp;" Or e.Item.Cells(9).Text <> "&nbsp;" Or e.Item.Cells(10).Text <> 0 Then
'                            lnkbtnEdt.Visible = False
'                        End If
'                    Else
'                        dgJournalEntry.Columns(0).Visible = False
'                        dgJournalEntry.Columns(16).Visible = True
'                        dgJournalEntry.Columns(17).Visible = True
'                    End If
'                    If e.Item.Cells(6).Text <> "&nbsp;" Or e.Item.Cells(9).Text <> "&nbsp;" Or e.Item.Cells(10).Text <> 0 Then btnDeleteAction.Visible = True
'                End If

'                'Added by chintan- do not show edit and delete option for closed FY
'                If Not dtFYClosingDate = Nothing Then
'                    lblJDate = e.Item.FindControl("lblJDate")
'                    If lblJDate.Text <> "" Then
'                        If dtFYClosingDate >= DateFromFormattedDate(lblJDate.Text, Session("DateFormat")) Then
'                            lnkbtnEditJournal.Visible = False
'                            e.Item.FindControl("btnDeleteAction").Visible = False
'                            e.Item.FindControl("lnkbtnEdt").Visible = False
'                            e.Item.FindControl("lblFYClosed").Visible = True
'                        End If
'                    End If
'                End If
'                If e.Item.Cells(1).Text = "0" Then 'Hide buttons for total row 
'                    lnkbtnEditJournal.Visible = False
'                    e.Item.FindControl("btnDeleteAction").Visible = False
'                    e.Item.FindControl("lnkbtnEdt").Visible = False
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub LoadCompanyDropDown(ByVal ddlitems As DropDownList, ByVal p_DivisionId As Integer)
'        Try
'            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry
'            objJournalEntry.DomainID = Session("DomainID")
'            objJournalEntry.DivisionId = p_DivisionId
'            ddlitems.DataSource = objJournalEntry.GetCompanyDetForDivisionId
'            ddlitems.DataTextField = "vcCompanyName"
'            ddlitems.DataValueField = "numDivisionID"
'            ddlitems.DataBind()
'            ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
'        Try
'            If GetQueryStringVal("frm") Is Nothing Then
'                Response.Redirect("../Accounting/frmChartofAccountsDetail.aspx?ChartAcntId=" & mintChartAcntId.ToString())
'            ElseIf GetQueryStringVal("frm") = "COA" Then
'                Response.Redirect("../Accounting/frmChartofAccounts.aspx?DomainID=" & Session("DomainID"))
'            ElseIf GetQueryStringVal("frm") = "frmProfitLoss" Then
'                Response.Redirect("../Accounting/frmProfitLoss.aspx?FromDate='" & strDateFrom & "'&ToDate='" & strDateTo & "'")
'            End If

'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Function ReturnMoney(ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Function ReturnMoneyNeg(ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then Return Replace(String.Format("{0:#,###.00}", Money), "-", "")
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'End Class