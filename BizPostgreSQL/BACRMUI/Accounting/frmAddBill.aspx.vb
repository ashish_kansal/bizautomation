﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.TimeAndExpense

Public Class frmAddBill
    Inherits BACRMPage

    Dim lngDivisionID As Long
    Dim lngBillID As Long
    Dim dtProject As DataTable
    Dim dtClass As DataTable
    Dim dtChartAcntDetails As DataTable
    Dim dtChartAcnt As DataTable
    Dim dtItems As New DataTable

    Dim objCommon As New CCommon
    Dim objOppInvoice As New OppInvoice
    Dim lngDefaultExpenseAccountID As Long = 0

#Region "PAGE EVENTS"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
            lngBillID = CCommon.ToLong(GetQueryStringVal("BillID"))

            If GetQueryStringVal("StageName") <> "" Then
                txtReference.Text = "Stage: " & GetQueryStringVal("StageName")
            End If

            If Not IsPostBack Then
                radCmbCompany.Focus()
                hfProjectID.Value = CCommon.ToLong(GetQueryStringVal("ProID"))
                hfStageID.Value = CCommon.ToLong(GetQueryStringVal("StageID"))
                FillNetDays()
                BindMultiCurrency()

                If GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then
                    If GetQueryStringVal("uihTR") <> "" Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                    End If
                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany As String
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                    radCmbCompany_SelectedIndexChanged(Nothing, Nothing)

                    lngDefaultExpenseAccountID = objCommon.DefaultExpenseAccountID
                    For Each lstItem As ListItem In ddlTerms.Items
                        If CCommon.ToString(lstItem.Value).Contains(objCommon.intBillingDays) Then
                            lstItem.Selected = True
                            Exit For
                        End If
                    Next

                End If
                calBillDate.SelectedDate = Date.Now

                If lngDivisionID > 0 Then
                    objCommon.DivisionID = lngDivisionID
                    radCmbCompany.Text = objCommon.GetCompanyName
                    radCmbCompany.SelectedValue = lngDivisionID
                    radCmbCompany_SelectedIndexChanged(Nothing, Nothing)
                End If

                BindExpencesAndLiability()
                LoadBillDetails(boolPostback:=True)

                If CCommon.ToLong(hfStageID.Value) > 0 Then
                    Dim objAdmin As New CAdmin
                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.SalesProsID = CCommon.ToLong(hfStageID.Value)
                    objAdmin.Mode = 2
                    Dim dtStageDetail As DataTable = objAdmin.StageItemDetails()

                    If dtStageDetail.Rows.Count = 1 Then
                        lblTitle.Text = "Enter Bill for Stage (" & dtStageDetail.Rows(0).Item("vcStageName").ToString() & ")"
                        lblStageBudget.Text = "Total stage expense budget " & ReturnMoney(CCommon.ToDecimal(dtStageDetail.Rows(0).Item("monExpenseBudget").ToString())) & ""
                    End If
                End If

                If lngBillID > 0 Then
                    PaymentHistory1.BillID = CCommon.ToLong(lngBillID)
                    PaymentHistory1.BindGrid()

                    btnSaveClone.Visible = True
                Else
                    btnDelete.Visible = False
                End If
                If CCommon.ToInteger(GetQueryStringVal("flag")) = 1 Then
                    litMessage.Visible = True
                    litMessage.Attributes.Add("class", "successInfo")
                    litMessage.Text = "You bill is saved sucessfully, Enter New Bill."
                End If
            End If

            'ClientScript.RegisterStartupScript(Me.GetType, "var duedateChage = window.validateDate;window.validateDate = function () {duedateChage('" & CCommon.GetValidationDateFormat() & "');}", True)
            calBillDate.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            calBillDate.Attributes.Add("onclick", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            ddlTerms.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            hfDateFormat.Value = CCommon.GetValidationDateFormat() 'Session("DateFormat").ToString().ToLower().Replace("m", "M").Replace("Month", "MMM")
            objCommon.ManageGridAsPerConfig(gvBillItems)
            'ClientScript.RegisterClientScriptBlock(Me.GetType, "DateChange", "$('#Content_calBillDate_txtDate').blur();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "PUBLIC/PRIVATE FUNCTIONS"

    Private Sub FillNetDays()
        Try
            Dim objOpp As New OppotunitiesIP
            Dim dtTerms As New DataTable
            With objOpp
                .DomainID = Session("DomainID")
                .TermsID = 0
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                .ApplyTo = 2
                dtTerms = .GetTerms()
            End With

            If dtTerms IsNot Nothing Then
                ddlTerms.DataSource = dtTerms
                ddlTerms.DataTextField = "vcTerms"
                ddlTerms.DataValueField = "vcValue"
                ddlTerms.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindMultiCurrency()
        Try
            If Session("MultiCurrency") = True Then
                pnlCurrency.Visible = True
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                ddlCurrency.DataTextField = "vcCurrencyDesc"
                ddlCurrency.DataValueField = "numCurrencyID"
                ddlCurrency.DataBind()
                ddlCurrency.Items.Insert(0, "--Select One--")
                ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                    ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindProject(ByVal ddlProject As DropDownList)
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = objProject.GetOpenProject()
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindClass(ByRef ddlClass As DropDownList)
        Try
            If dtClass Is Nothing Then
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()
            End If
            ddlClass.DataTextField = "ClassName"
            ddlClass.DataValueField = "numChildClassID"
            ddlClass.DataSource = dtClass
            ddlClass.DataBind()
            ddlClass.Items.Insert(0, "--Select One--")
            ddlClass.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindExpencesAndLiability()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            Dim item As ListItem
            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlLiabilityAccount.Items.Add(item)
            Next
            ddlLiabilityAccount.Items.Insert(0, New ListItem("--Select One--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadBillDetails(Optional deleteRowIndex As Integer = -1, Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Dim j As Integer
        Dim drRow As DataRow
        Try
            Dim dsBillDetails As New DataSet
            dtItems.Rows.Clear()
            dtItems.Columns.Clear()

            If objOppInvoice Is Nothing Then objOppInvoice = New OppInvoice

            objOppInvoice.DomainID = Session("DomainID")
            objOppInvoice.BillID = CCommon.ToLong(lngBillID)

            dsBillDetails = objOppInvoice.FetchBillDetails()

            If dsBillDetails IsNot Nothing AndAlso dsBillDetails.Tables.Count > 1 Then

                Dim dtBillHeader As New DataTable
                dtBillHeader = dsBillDetails.Tables(0)

                If dtBillHeader.Rows.Count > 0 Then
                    With objOppInvoice

                        If CCommon.ToLong(dtBillHeader.Rows(0)("numDivisionID")) > 0 Then
                            rdbLstOption.SelectedValue = 0
                        Else
                            rdbLstOption.SelectedValue = 1
                        End If

                        radCmbCompany.Text = CCommon.ToString(dtBillHeader.Rows(0)("vcCompanyName"))
                        radCmbCompany.SelectedValue = CCommon.ToLong(dtBillHeader.Rows(0)("numDivisionID"))
                        hfDivisionID.Value = CCommon.ToLong(dtBillHeader.Rows(0)("numDivisionID"))
                        ddlLiabilityAccount.SelectedValue = CCommon.ToLong(dtBillHeader.Rows(0)("numAccountID"))
                        txtReference.Text = CCommon.ToString(dtBillHeader.Rows(0)("vcReference"))
                        calBillDate.SelectedDate = dtBillHeader.Rows(0)("dtBillDate")
                        lblAmountDue.Text = CCommon.ToDouble(dtBillHeader.Rows(0)("monAmountDue"))
                        lblAmountPaid.Text = ReturnMoney(CCommon.ToDecimal(dtBillHeader.Rows(0)("monAmtPaid")))

                        If ddlTerms.Items.FindByText(CCommon.ToString(dtBillHeader.Rows(0)("vcTermName"))) IsNot Nothing Then
                            ddlTerms.Items.FindByText(CCommon.ToString(dtBillHeader.Rows(0)("vcTermName"))).Selected = True
                        End If

                        lblTotal.Text = CCommon.ToString(dtBillHeader.Rows(0)("monAmtPaid"))
                        calDueDate.SelectedDate = dtBillHeader.Rows(0)("dtDueDate")
                        txtMemo.Text = CCommon.ToString(dtBillHeader.Rows(0)("vcMemo"))

                        hfProjectID.Value = CCommon.ToLong(dtBillHeader.Rows(0)("numProjectID"))
                        hfStageID.Value = CCommon.ToLong(dtBillHeader.Rows(0)("numStageID"))

                        ddlCurrency.SelectedValue = CCommon.ToLong(dtBillHeader.Rows(0)("numCurrencyID"))

                        TransactionInfo1.ReferenceType = enmReferenceType.BillHeader
                        TransactionInfo1.ReferenceID = CCommon.ToLong(lngBillID)
                        TransactionInfo1.getTransactionInfo()

                    End With
                End If

                dtItems = dsBillDetails.Tables(1)

                If boolPostback = False Then
                    dtItems.Rows.Clear()

                    Dim gvRow As GridViewRow
                    Dim dtrow As DataRow

                    For i As Integer = 0 To gvBillItems.Rows.Count - 1
                        If i <> deleteRowIndex Then
                            dtrow = dtItems.NewRow
                            gvRow = gvBillItems.Rows(i)

                            dtrow.Item("numBillDetailID") = CCommon.ToLong(CType(gvRow.FindControl("hdnBillDetailID"), HiddenField).Value)
                            dtrow.Item("numExpenseAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue)
                            dtrow.Item("monAmount") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)
                            dtrow.Item("vcDescription") = CType(gvRow.FindControl("txtDescription"), TextBox).Text
                            dtrow.Item("numOppID") = CCommon.ToLong(CType(gvRow.FindControl("ddlSalesOrders"), DropDownList).SelectedValue)
                            dtrow.Item("numOppItemID") = CCommon.ToLong(CType(gvRow.FindControl("hdnOppItemID"), HiddenField).Value)

                            If CCommon.ToLong(CCommon.ToLong(CType(gvRow.FindControl("hdnOppItemID"), HiddenField).Value)) > 0 Then
                                dtrow.Item("numProjectID") = 0
                            Else
                                dtrow.Item("numProjectID") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)
                            End If

                            dtrow.Item("numClassID") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                            dtrow.Item("numCampaignID") = CCommon.ToLong(CType(gvRow.FindControl("ddlCampaign"), DropDownList).SelectedValue)
                            If CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue) > 0 AndAlso CCommon.ToDouble(CType(gvRow.FindControl("txtAmount"), TextBox).Text) > 0 Then
                                dtItems.Rows.Add(dtrow)
                            End If
                        End If
                    Next
                End If

                If dtItems.Rows.Count < 2 Then
                    Dim lintRowcount As Int16 = 2 - dtItems.Rows.Count
                    For j = 0 To lintRowcount - 1
                        drRow = dtItems.NewRow

                        If CCommon.ToLong(hfProjectID.Value) > 0 Then
                            drRow.Item("numProjectID") = CCommon.ToLong(hfProjectID.Value)
                        End If

                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.UserCntID = Session("UserContactID")
                        objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                        drRow.Item("numClassID") = objCommon.GetAccountingClass()

                        dtItems.Rows.Add(drRow)
                    Next
                End If

                If boolAddNew Then
                    For j = 0 To 1
                        drRow = dtItems.NewRow

                        If CCommon.ToLong(hfProjectID.Value) > 0 Then
                            drRow.Item("numProjectID") = CCommon.ToLong(hfProjectID.Value)
                        End If

                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.UserCntID = Session("UserContactID")
                        objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                        drRow.Item("numClassID") = objCommon.GetAccountingClass()

                        dtItems.Rows.Add(drRow)
                    Next
                End If

                gvBillItems.DataSource = dtItems
                gvBillItems.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function SaveBill(Optional ByVal isClone As Boolean = False) As Boolean
        Try
            If CCommon.ToDecimal(lblAmountPaid.Text) > 0 AndAlso CCommon.ToDecimal(lblAmountPaid.Text) > CCommon.ToDecimal(hdnTotal.Value) Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", String.Format("alert('Total bill amount {0} must more than amount paid {1}.');", CCommon.ToDecimal(hdnTotal.Value), CCommon.ToDecimal(lblAmountPaid.Text)), True)
                Return False
            End If

            If Not isClone AndAlso lngBillID > 0 AndAlso CCommon.ToDecimal(lblAmountPaid.Text) > 0 AndAlso rdbLstOption.SelectedValue = 0 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) <> CCommon.ToLong(hfDivisionID.Value) Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", String.Format("alert('This bill is aleady paid,you are not allowed change Organization. If you want to change it, you must edit the bill payment it appears on and remove it first.');", CCommon.ToDecimal(hdnTotal.Value), CCommon.ToDecimal(lblAmountPaid.Text)), True)
                Return False
            End If

            If rdbLstOption.SelectedValue = 0 Then
                If CCommon.ToLong(hfProjectID.Value) > 0 And CCommon.ToLong(hfStageID.Value) > 0 Then
                    Dim objTimeExp As New TimeExpenseLeave
                    Dim dtTimeDetails As DataTable

                    objTimeExp.DomainID = Session("DomainID")
                    objTimeExp.ProID = CCommon.ToLong(hfProjectID.Value)
                    objTimeExp.StageId = CCommon.ToLong(hfStageID.Value)

                    dtTimeDetails = objTimeExp.GetTimeAndExpense_BudgetTotal()

                    If dtTimeDetails.Rows.Count <> 0 Then
                        If dtTimeDetails.Rows(0).Item("bitExpenseBudget") = True Then
                            If (CCommon.ToDouble(hdnTotal.Value) > dtTimeDetails.Rows(0).Item("TotalExpense")) Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You’ve exceeded the amount budgeted for this stage.');", True)
                                Return False
                            End If
                            'Else
                            '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please set Expense Budget for this stage.' );", True)
                            '    bError = True
                            '    Exit Sub
                        End If
                    End If
                End If
            End If

            If objOppInvoice Is Nothing Then objOppInvoice = New OppInvoice
            Dim dtBillDetails As New DataTable

            Dim ds As New DataSet
            Dim gvRow As GridViewRow
            Dim dtrow As DataRow

            CCommon.AddColumnsToDataTable(dtBillDetails, "numBillDetailID,numExpenseAccountID,monAmount,vcDescription,numOppItemID,numOppID,numProjectID,numClassID,numCampaignID")

            For i As Integer = 0 To gvBillItems.Rows.Count - 1
                dtrow = dtBillDetails.NewRow
                gvRow = gvBillItems.Rows(i)

                If isClone Then
                    dtrow.Item("numBillDetailID") = 0
                Else
                    dtrow.Item("numBillDetailID") = CCommon.ToLong(CType(gvRow.FindControl("hdnBillDetailID"), HiddenField).Value)
                End If

                dtrow.Item("numExpenseAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue)
                dtrow.Item("monAmount") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)
                dtrow.Item("vcDescription") = CType(gvRow.FindControl("txtDescription"), TextBox).Text
                dtrow.Item("numOppID") = CCommon.ToLong(CType(gvRow.FindControl("ddlSalesOrders"), DropDownList).SelectedValue)
                dtrow.Item("numOppItemID") = CCommon.ToLong(CType(gvRow.FindControl("hdnOppItemID"), HiddenField).Value)
                dtrow.Item("numProjectID") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)
                dtrow.Item("numClassID") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                dtrow.Item("numCampaignID") = CCommon.ToLong(CType(gvRow.FindControl("ddlCampaign"), DropDownList).SelectedValue)

                If CCommon.ToLong(hfProjectID.Value) > 0 AndAlso CCommon.ToLong(hfProjectID.Value) <> dtrow.Item("numProjectID") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This bill is for particular project,you are not allowed to select different project.');", True)
                    Return False
                End If

                If CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue) > 0 AndAlso CCommon.ToDouble(CType(gvRow.FindControl("txtAmount"), TextBox).Text) > 0 Then
                    dtBillDetails.Rows.Add(dtrow)
                End If
            Next

            If dtBillDetails.Rows.Count <= 0 Then
                litMessage.Visible = True
                litMessage.Attributes.Add("class", "errorInfo")
                litMessage.Text = "Please provide bill details."
                Return False
            End If

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                With objOppInvoice
                    .BillID = If(isClone, 0, lngBillID)
                    If rdbLstOption.SelectedValue = 0 Then
                        .DivisionID = radCmbCompany.SelectedValue
                        .AccountID = 0
                    Else
                        .DivisionID = 0
                        .AccountID = ddlLiabilityAccount.SelectedValue
                    End If

                    .Reference = txtReference.Text
                    .BillDate = calBillDate.SelectedDate
                    .AmountDue = CCommon.ToDouble(hdnTotal.Value)

                    If Not ddlTerms.SelectedItem Is Nothing Then
                        .Terms = CCommon.ToString(ddlTerms.SelectedValue).Split("~")(0)
                    End If
                    .DueDate = calDueDate.SelectedDate
                    .Memo = txtMemo.Text
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")

                    If Session("MultiCurrency") = True Then
                        .CurrencyID = ddlCurrency.SelectedValue
                    Else
                        .CurrencyID = Session("BaseCurrencyID")
                    End If

                    Dim dsBill As New DataSet
                    dsBill.Tables.Add(dtBillDetails)
                    .StrItems = dsBill.GetXml()

                    lngBillID = .ManageBillHeader()

                    If lngBillID > 0 Then
                        'Link Bill with project if added from project
                        If CCommon.ToLong(hfProjectID.Value) > 0 Then 'And lngStageID > 0  commented by chintan reason: added new project dropdown in page
                            Dim objProject As New Project
                            objProject.DomainID = Session("DomainID")
                            objProject.ProjectID = CCommon.ToLong(hfProjectID.Value)
                            objProject.OppBizDocID = 0
                            objProject.BillID = lngBillID
                            objProject.StageID = CCommon.ToLong(hfStageID.Value)
                            objProject.SaveProjectOpportunities()
                        End If

                        Dim lngJournalId As Long
                        lngJournalId = SaveDataToHeader(TransactionInfo1.JournalID)
                        SaveDataToGeneralJournalDetails(lngJournalId)

                        radCmbCompany.Items.Clear()
                        LoadBillDetails()
                        litMessage.Visible = True
                        litMessage.Attributes.Add("class", "successInfo")
                        litMessage.Text = "Bill is Created."

                        objTransactionScope.Complete()
                        Return True
                    Else
                        litMessage.Visible = True
                        litMessage.Attributes.Add("class", "errorInfo")
                        litMessage.Text = "Error occurred while creating Bill."

                        Return False
                    End If
                End With
            End Using
        Catch ex As Exception
            If ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Return False
            Else
                Throw ex
            End If
        End Try
    End Function


    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader(ByVal lngJournalId As Long) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lngJournalId
                .RecurringId = 0
                .EntryDate = CDate(calBillDate.SelectedDate & " 12:00:00")
                .Description = "Bill:" & txtMemo.Text
                .Amount = CCommon.ToDecimal(hdnTotal.Value)
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = lngBillID
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Long)
        Try
            If objOppInvoice Is Nothing Then objOppInvoice = New OppInvoice
            Dim dsBillDetails As New DataSet

            objOppInvoice.DomainID = Session("DomainID")
            objOppInvoice.BillID = CCommon.ToLong(lngBillID)
            dsBillDetails = objOppInvoice.FetchBillDetails()

            Dim dtBillHeader As New DataTable
            dtBillHeader = dsBillDetails.Tables(0)


            Dim objJEList As New JournalEntryCollection
            Dim objJE As JournalEntryNew


            Dim dtDetails As New DataTable
            dtDetails = dsBillDetails.Tables(1)

            Dim i As Int32 = 0
            For Each dr As DataRow In dtDetails.Rows
                'CEDIT
                objJE = New JournalEntryNew()
                objJE.TransactionId = If(i >= dtBillHeader.Rows.Count, 0, CCommon.ToLong(dtBillHeader.Rows(i)("numTransactionId")))
                objJE.DebitAmt = 0
                objJE.CreditAmt = CCommon.ToDecimal(dr("monAmount") * dtBillHeader(0)("fltExchangeRate"))

                If CCommon.ToLong(dtBillHeader(0)("numDivisionID")) = 0 And CCommon.ToLong(dtBillHeader(0)("numAccountID")) > 0 Then
                    objJE.ChartAcntId = CCommon.ToLong(dtBillHeader(0)("numAccountID"))
                    If objJE.ChartAcntId = 0 Then
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                    End If
                    objJE.CustomerId = 0
                ElseIf CCommon.ToLong(dtBillHeader(0)("numDivisionID")) > 0 Then
                    objJE.ChartAcntId = GetVendorAP_AccountID(CCommon.ToLong(dtBillHeader(0)("numDivisionID")))
                    objJE.CustomerId = dtBillHeader(0)("numDivisionID")
                End If

                objJE.Description = "Bill"
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP"
                objJE.Reference = txtReference.Text
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = dtBillHeader(0)("numCurrencyID")
                objJE.FltExchangeRate = dtBillHeader(0)("fltExchangeRate")
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = dr("numProjectID")
                objJE.ClassID = dr("numClassID")
                objJE.CampaignID = dr("numCampaignID")
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.BillHeader
                objJE.ReferenceID = lngBillID

                objJEList.Add(objJE)

                'DEBIT
                objJE = New JournalEntryNew()

                objJE.TransactionId = dr("numTransactionId")
                objJE.DebitAmt = CCommon.ToDecimal(CCommon.ToDecimal(dr("monAmount")) * dtBillHeader(0)("fltExchangeRate"))
                objJE.CreditAmt = 0
                objJE.ChartAcntId = dr("numExpenseAccountID")
                objJE.Description = dr("vcDescription")
                objJE.CustomerId = CCommon.ToLong(dtBillHeader(0)("numDivisionID"))
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "S"
                objJE.Reference = txtReference.Text
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = dtBillHeader(0)("numCurrencyID")
                objJE.FltExchangeRate = dtBillHeader(0)("fltExchangeRate")
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = dr("numProjectID")
                objJE.ClassID = dr("numClassID")
                objJE.CampaignID = dr("numCampaignID")
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.BillDetail
                objJE.ReferenceID = CCommon.ToLong(dr("numBillDetailID"))

                objJEList.Add(objJE)

                i = i + 1
            Next

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetVendorAP_AccountID(ByVal lngDivisionID As Long) As Long
        Try
            Dim AccountID As Long
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .COARelationshipID = 0
                .DivisionID = lngDivisionID
                .DomainID = Session("DomainID")
                ds = .GetCOARelationship()
            End With
            If ds.Tables(0).Rows.Count > 0 Then
                AccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAPAccountId"))
            End If

            If AccountID = 0 Then
                '' For Account Payable to be Debited
                AccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
            End If
            Return AccountID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub UpdateClass()
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UserCntID = Session("UserContactID")
            objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            Dim lngAccountClassID As Long = objCommon.GetAccountingClass()

            For Each row As GridViewRow In gvBillItems.Rows
                Dim ddlClass As DropDownList = DirectCast(row.FindControl("ddlClass"), DropDownList)

                If Not ddlClass Is Nothing AndAlso Not ddlClass.Items.FindByValue(lngAccountClassID) Is Nothing Then
                    ddlClass.Items.FindByValue(lngAccountClassID).Selected = True
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "GRIDVIEW EVENTS"

    Private Sub gvBillItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBillItems.RowCommand
        Try
            If e.CommandName = "DeleteRow" Then
                Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                LoadBillDetails(deleteRowIndex:=row.DataItemIndex)

                'If Not ClientScript.IsStartupScriptRegistered("CalculateTotal") Then ClientScript.RegisterStartupScript(Me.GetType, "CalculateTotal", "CalculateTotal();", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvBillItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBillItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtAmount As TextBox = DirectCast(e.Row.FindControl("txtAmount"), TextBox)
                Dim ddlExpenseAccount As DropDownList = DirectCast(e.Row.FindControl("ddlExpenseAccount"), DropDownList)
                Dim ddlProject As DropDownList = DirectCast(e.Row.FindControl("ddlProject"), DropDownList)
                Dim ddlSalesOrders As DropDownList = DirectCast(e.Row.FindControl("ddlSalesOrders"), DropDownList)
                Dim ddlSalesOrderItems As DropDownList = DirectCast(e.Row.FindControl("ddlSalesOrderItems"), DropDownList)
                Dim hdnOppItemID As HiddenField = DirectCast(e.Row.FindControl("hdnOppItemID"), HiddenField)
                Dim ddlClass As DropDownList = DirectCast(e.Row.FindControl("ddlClass"), DropDownList)
                Dim ddlCampaign As DropDownList = DirectCast(e.Row.FindControl("ddlCampaign"), DropDownList)

                If dtChartAcntDetails Is Nothing Then
                    Dim objCOA As New ChartOfAccounting
                    objCOA.DomainID = Session("DomainID")
                    objCOA.AccountCode = "0104" 'Expense
                    dtChartAcntDetails = objCOA.GetParentCategory()

                    Dim dt1 As DataTable
                    objCOA.DomainID = Session("DomainID")
                    objCOA.AccountCode = "0106" ' COGS
                    dt1 = objCOA.GetParentCategory()
                    dtChartAcntDetails.Merge(dt1)

                    objCOA.AccountCode = "0103" ' Income
                    objCOA.DomainID = Session("DomainID")
                    dt1 = objCOA.GetParentCategory()
                    dtChartAcntDetails.Merge(dt1)

                    objCOA.AccountCode = "0101" ' Asset
                    objCOA.DomainID = Session("DomainID")
                    dt1 = objCOA.GetParentCategory()
                    dtChartAcntDetails.Merge(dt1)

                    objCOA.AccountCode = "0102" ' Liability
                    objCOA.DomainID = Session("DomainID")
                    dt1 = objCOA.GetParentCategory()
                    dtChartAcntDetails.Merge(dt1)
                End If

                If Not ddlSalesOrders Is Nothing Then
                    If objCommon Is Nothing Then
                        objCommon = New CCommon
                    End If
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objCommon.OppID = CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppID"))
                    ddlSalesOrders.DataSource = objCommon.GetOpenOrdersAddBill()
                    ddlSalesOrders.DataValueField = "numOppId"
                    ddlSalesOrders.DataTextField = "vcPOppName"
                    ddlSalesOrders.DataBind()
                    ddlSalesOrders.Items.Insert(0, New ListItem("-- Select One --", 0))
                End If

                If ddlProject IsNot Nothing Then
                    BindProject(ddlProject)
                End If

                If ddlClass IsNot Nothing Then
                    CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))
                End If

                If ddlCampaign IsNot Nothing Then
                    If objCommon Is Nothing Then
                        objCommon = New CCommon
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 18, CCommon.ToLong(Session("DomainID")))

                    If CCommon.ToLong(hdnCampaign.Value) > 0 AndAlso Not ddlCampaign.Items.FindByValue(hdnCampaign.Value) Is Nothing Then
                        ddlCampaign.Items.FindByValue(hdnCampaign.Value).Selected = True
                    End If
                End If

                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlExpenseAccount.Items.Add(item)
                Next
                ddlExpenseAccount.Items.Insert(0, New ListItem("--Select One --", "0"))

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numExpenseAccountID")) > 0 Then
                    If Not ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numExpenseAccountID"))) Is Nothing Then
                        ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numExpenseAccountID"))).Selected = True
                    End If
                Else
                    If Not ddlExpenseAccount.Items.FindByValue(lngDefaultExpenseAccountID) Is Nothing Then
                        ddlExpenseAccount.Items.FindByValue(lngDefaultExpenseAccountID).Selected = True
                    End If
                End If

                hdnOppItemID.Value = CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppItemID"))
                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppID")) > 0 Then
                    If Not ddlSalesOrders.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppID"))) Is Nothing Then
                        ddlSalesOrders.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppID"))).Selected = True
                    End If

                    If objCommon Is Nothing Then
                        objCommon = New CCommon
                    End If
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCommon.OppID = CCommon.ToLong(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppID")))
                    objCommon.OppItemID = CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppItemID"))
                    ddlSalesOrderItems.DataSource = objCommon.GetOrderExpenseItemsAddBill()
                    ddlSalesOrderItems.DataTextField = "vcItemName"
                    ddlSalesOrderItems.DataValueField = "numoppitemtCode"
                    ddlSalesOrderItems.DataBind()
                    ddlSalesOrderItems.Items.Insert(0, New ListItem("--Select One --", "0"))
                    If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppItemID")) > 0 Then
                        If Not ddlSalesOrderItems.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppItemID"))) Is Nothing Then
                            ddlSalesOrderItems.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numOppItemID"))).Selected = True
                        End If
                    End If
                End If

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numProjectID")) > 0 Then
                    If Not ddlProject.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numProjectID"))) Is Nothing Then
                        ddlProject.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numProjectID"))).Selected = True
                    End If
                End If

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numClassID")) > 0 Then
                    If Not ddlClass.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numClassID"))) Is Nothing Then
                        ddlClass.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numClassID"))).Selected = True
                    End If
                End If

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numCampaignID")) > 0 Then
                    If Not ddlCampaign.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numCampaignID"))) Is Nothing Then
                        ddlCampaign.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numCampaignID"))).Selected = True
                    End If
                End If

                Dim btnDeleteRow As Button = DirectCast(e.Row.FindControl("btnDeleteRow"), Button)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "BUTTON EVENTS"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            LoadBillDetails(boolAddNew:=True)

            'If Not ClientScript.IsStartupScriptRegistered("CalculateTotal") Then ClientScript.RegisterStartupScript(Me.GetType, "CalculateTotal", "CalculateTotal();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CCommon.ToLong(Session("MultiCurrency")) AndAlso Not CCommon.ToLong(ddlCurrency.SelectedValue) > 0 Then
                litMessage.Text = "Select currency"
                ddlCurrency.Focus()
                Exit Sub
            End If

            If SaveBill(False) Then
                'If CCommon.ToLong(hfStageID.Value) > 0 Then
                '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> if (window.opener != null) {window.opener.location.reload(true);Close();}</script>")
                'Else
                If CCommon.ToLong(GetQueryStringVal("BillID")) > 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> if (window.opener != null) {window.opener.location.reload(true);Close();}</script>")
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close1", "Close();", True)
                End If

                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "RAD COMBO EVENTS"

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If CCommon.ToInteger(Session("DefaultClassType")) = 2 Then 'If Account class is set on company then we have to change class when compay selection changed
                UpdateClass()
            End If

            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim objProspects As New CProspects
                Dim dtComInfo As DataTable
                objProspects.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtComInfo = objProspects.GetCompanyInfoForEdit

                If Not IsDBNull(dtComInfo.Rows(0).Item("numBillingDays")) Then
                    If Not ddlTerms.Items.FindByText(CCommon.ToString(dtComInfo.Rows(0).Item("numBillingDaysName"))) Is Nothing Then
                        ddlTerms.Items.FindByText(CCommon.ToString(dtComInfo.Rows(0).Item("numBillingDaysName"))).Selected = True
                    End If
                End If

                If Not dtComInfo Is Nothing AndAlso dtComInfo.Rows.Count > 0 AndAlso CCommon.ToLong(dtComInfo.Rows(0)("numDefaultExpenseAccountID")) Then
                    For Each row As GridViewRow In gvBillItems.Rows
                        Dim ddlExpenseAccount As DropDownList = DirectCast(row.FindControl("ddlExpenseAccount"), DropDownList)

                        If Not ddlExpenseAccount Is Nothing AndAlso Not ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(dtComInfo.Rows(0)("numDefaultExpenseAccountID"))) Is Nothing Then
                            ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(dtComInfo.Rows(0)("numDefaultExpenseAccountID"))).Selected = True
                        End If
                    Next
                End If

                If CCommon.ToLong(dtComInfo.Rows(0)("numCompanyType")) = 47 Then
                    hdnCampaign.Value = CCommon.ToLong(dtComInfo.Rows(0)("vcHow"))

                    If CCommon.ToLong(hdnCampaign.Value) > 0 Then
                        For Each dataRow As GridViewRow In gvBillItems.Rows
                            If Not DirectCast(dataRow.FindControl("ddlCampaign"), DropDownList).Items.FindByValue(hdnCampaign.Value) Is Nothing Then
                                DirectCast(dataRow.FindControl("ddlCampaign"), DropDownList).Items.FindByValue(hdnCampaign.Value).Selected = True
                            End If
                        Next
                    End If
                End If
            End If

            LoadBillDetails(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

    Private Sub btnSaveNew_Click(sender As Object, e As EventArgs) Handles btnSaveNew.Click
        Try
            If CCommon.ToLong(Session("MultiCurrency")) AndAlso Not CCommon.ToLong(ddlCurrency.SelectedValue) > 0 Then
                litMessage.Text = "Select currency"
                ddlCurrency.Focus()
                Exit Sub
            End If

            If SaveBill(False) Then
                Response.Redirect("frmAddBill.aspx?flag=1", False)
            Else
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "DoubleClickPrevent", "IsAddClicked = false;", True) 'added to avoid multiple click of add button before completing postback- by chintan

                litMessage.Visible = True
                litMessage.Attributes.Add("class", "errorInfo")
                litMessage.Text = "Error occurred while creating Bill."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClone_Click(sender As Object, e As EventArgs) Handles btnSaveClone.Click
        Try
            If CCommon.ToLong(Session("MultiCurrency")) AndAlso Not CCommon.ToLong(ddlCurrency.SelectedValue) > 0 Then
                litMessage.Text = "Select currency"
                ddlCurrency.Focus()
                Exit Sub
            End If

            If SaveBill(False) Then
                If CCommon.ToLong(GetQueryStringVal("BillID")) > 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> if (window.opener != null) {window.opener.location.reload(true);Close();}</script>")
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close1", "Close();", True)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If CCommon.ToLong(GetQueryStringVal("BillID")) > 0 Then
                Dim objJournalEntry As New JournalEntry
                objJournalEntry.JournalId = 0
                objJournalEntry.BillPaymentID = 0
                objJournalEntry.DepositId = 0
                objJournalEntry.CheckHeaderID = 0
                objJournalEntry.BillID = CCommon.ToLong(GetQueryStringVal("BillID"))
                objJournalEntry.CategoryHDRID = 0
                objJournalEntry.ReturnID = 0
                objJournalEntry.DomainID = Session("DomainId")
                objJournalEntry.UserCntID = Session("UserContactID")

                Try
                    objJournalEntry.DeleteJournalEntryDetails()
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('bill deleted sucessfully.');if (window.opener != null) {window.opener.location.reload(true);Close();}self.close();", True)
                Catch ex As Exception
                    If ex.Message = "BILL_PAID" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class