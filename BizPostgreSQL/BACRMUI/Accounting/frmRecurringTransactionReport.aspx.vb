﻿Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmRecurringTransactionReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Not IsNumeric(Session("PagingRows")) Then

                    txtCurrrentPageDeals.Text = 1

                End If
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgRep_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRep.ItemCommand
        If e.CommandName = "RecTemp" Then
            Response.Redirect("../Accounting/frmRecurringEvents.aspx?RecurringId=" & e.Item.Cells(3).Text)
        End If
    End Sub
    Private Sub BindGrid()


        Dim recEvent As New RecurringEvents

        Dim dtRep As DataTable
        recEvent.DomainId = Session("DomainId")
        recEvent.PageSize = Session("PagingRows")

        dtRep = recEvent.GetRecurringTransactionReport(txtCurrrentPageDeals.Text)


        lblRecordsDeals.Text = recEvent.TotalRecords
        dgRep.DataSource = dtRep
        If recEvent.TotalRecords Mod Session("PagingRows") = 0 Then
            lblTotal.Text = CInt(recEvent.TotalRecords / Session("PagingRows"))
        Else
            lblTotal.Text = CInt(recEvent.TotalRecords / Session("PagingRows")) + 1
        End If

        dgRep.DataBind()
    End Sub

    Protected Sub lnkNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkNext.Click
        If CInt(txtCurrrentPageDeals.Text) >= CInt(lblTotal.Text) Then
            Exit Sub
        Else
            txtCurrrentPageDeals.Text = txtCurrrentPageDeals.Text + 1
        End If
        BindGrid()
    End Sub

    Protected Sub lnkPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkPrevious.Click
        If CInt(txtCurrrentPageDeals.Text) <= 1 Then
            Exit Sub
        Else
            txtCurrrentPageDeals.Text = txtCurrrentPageDeals.Text - 1
        End If
        BindGrid()
    End Sub

    Protected Sub lnkFirst_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkFirst.Click
        txtCurrrentPageDeals.Text = 1
        BindGrid()
    End Sub

    Protected Sub lnkLast_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkLast.Click
        txtCurrrentPageDeals.Text = lblTotal.Text
        BindGrid()
    End Sub

    Protected Sub txtCurrrentPageDeals_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCurrrentPageDeals.TextChanged
        BindGrid()
    End Sub
End Class