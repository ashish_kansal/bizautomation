<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringTransaction.aspx.vb" Inherits=".frmRecurringTransaction" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Recurring Transactions </title>
    <script language="javascript" type="text/javascript" >
     function DeleteRecord()
		{
			var str;
				str='Are you sure, you want to delete the selected record?'
			if(confirm(str))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
     function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
    </script>
</head>
<body>
    <form id="form1" runat="server" method ="post">
       <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
    <br />
   <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom" >
						<table class="TabStyle">
							<tr>
								<td  >&nbsp;&nbsp;&nbsp;Recurring Transactions &nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align ="right" class="normal1"  >
					<asp:Label id="lblAccountType"  Text ="Filter By" runat ="server" ></asp:Label>
					<asp:DropDownList ID="ddlTransaction" Width ="150px" runat ="server" AutoPostBack ="true" CssClass="signup">
					<asp:ListItem Text="All" Value ="0"></asp:ListItem>
					<asp:ListItem Text ="Money Out" Value ="1"></asp:ListItem>
					<asp:ListItem Text ="Money In" Value ="2"></asp:ListItem>
					<asp:ListItem Text ="Interval" Value ="3"></asp:ListItem>
					<asp:ListItem Text ="Transaction Type" Value ="4"></asp:ListItem>
					</asp:DropDownList></td>
                    <td align ="right"class="normal1" >
					<asp:Label id="lblTransaction" Width="80px" Text ="" runat ="server" Visible ="false"  ></asp:Label>
					<asp:DropDownList ID="ddlInterval" Width ="150px" runat ="server" AutoPostBack ="true"  Visible ="false" CssClass="signup" ></asp:DropDownList>
					<asp:DropDownList ID="ddlAcntType" Width ="150px" runat ="server" AutoPostBack ="true"  Visible ="false" CssClass="signup" ></asp:DropDownList>
					
					</td>
					<td align ="right" class ="normal1">
					<asp:Label id="lblCustomerSearch"   Text ="	Filter By Customer or Vendor" runat ="server"></asp:Label>&nbsp;
					</td>
					<td align ="left" class ="normal1">
					<asp:TextBox ID="txtCustomerSearch" runat ="server" MaxLength ="50"></asp:TextBox>
					</td>
					<td align="right">
					<asp:button id="btnGo" Runat="server" Text="Go" CssClass="button"></asp:button>
					
					</td>
						
				</tr>
				 
			</TABLE>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None" Height="384px">
				<asp:tableRow ID="TableRow1" VerticalAlign="Top" runat="server">
					<asp:tableCell ID="TableCell1" runat="server">
							 <asp:datagrid id="dgRecurringTransaction" AllowSorting="true" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
							    <asp:BoundColumn DataField ="RecurringId" Visible ="false"  ></asp:BoundColumn>
							    <asp:BoundColumn DataField ="CashCreditId" Visible ="false" ></asp:BoundColumn>
							    <asp:BoundColumn DataField ="CheckId" Visible ="false" ></asp:BoundColumn>
							    <asp:BoundColumn DataField ="JournalId" Visible ="false" ></asp:BoundColumn>
							    <asp:BoundColumn DataField ="DepositId" Visible ="false" ></asp:BoundColumn>
							    <asp:BoundColumn DataField ="OppId" Visible ="false" ></asp:BoundColumn>
							    
							    <asp:TemplateColumn HeaderText="<font color=white>Created By, On</font>" HeaderStyle-Width="12%" >
							    <ItemTemplate>
							        <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "CreatedBy"), DataBinder.Eval(Container.DataItem, "CreatedDate"))%>
							    </ItemTemplate>
							    </asp:TemplateColumn>
							    <asp:ButtonColumn DataTextField ="TransactionType" HeaderText = "<font color=white>Transaction Type</font>" CommandName ="Edit" HeaderStyle-Width="10%"></asp:ButtonColumn> 
							  
							    <asp:TemplateColumn HeaderText="<font color=white>Money In</font>" HeaderStyle-Width="5%">
							    <ItemTemplate >
							    <asp:CheckBox ID="chkMoneyin" runat ="server" Enabled ="false" Checked ='<%# DataBinder.Eval(Container.DataItem, "MoneyIn")%>' />
							    </ItemTemplate>
							    </asp:TemplateColumn>
							    <asp:TemplateColumn HeaderText="<font color=white>Money Out</font>" HeaderStyle-Width="6%">
							    <ItemTemplate >
							    <asp:CheckBox ID="chkMoneyOut" runat ="server" Enabled ="false" Checked ='<%# DataBinder.Eval(Container.DataItem, "MoneyOut")%>'/>
							    </ItemTemplate>
							    </asp:TemplateColumn>
							    
							   	<asp:BoundColumn DataField="CustomerName" Headertext="<font color=white>Pay-To or Receive-From</font>" HeaderStyle-Width="12%"  ></asp:BoundColumn>								
								<asp:BoundColumn DataField="AccntType" Headertext="<font color=white>Affected Account Ledgers</font>" HeaderStyle-Width="27%" ></asp:BoundColumn>								
								<asp:BoundColumn DataField="TemplateName" Headertext="<font color=white>Recurring Template</font>" HeaderStyle-Width="10%"></asp:BoundColumn>	
								<asp:TemplateColumn HeaderText = "<font color=white>Interval, Amount</font>" HeaderStyle-Width="8%">
								<ItemTemplate>
								<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "IntervalType"),DataBinder.Eval(Container.DataItem, "monAmount"))%>
								</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn   HeaderText="<font color=white>Start Date</font>" HeaderStyle-Width="5%">
								<ItemTemplate>
								<%# ReturnDate(DataBinder.Eval(Container.DataItem, "StartDate")) %>
								</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn   HeaderText="<font color=white>End Date</font>" HeaderStyle-Width="5%">
								<ItemTemplate>
								<%# ReturnDate(DataBinder.Eval(Container.DataItem, "EndDate")) %>
								</ItemTemplate>
								</asp:TemplateColumn>
								 
								<asp:TemplateColumn HeaderStyle-Width="2%">
								<ItemTemplate>
								<asp:LinkButton ID="lnkdelete" Runat="server" Visible="false">
								 <font color="#730000">*</font></asp:LinkButton>
								 
								<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
								</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>	 				
					</asp:tableCell>
				</asp:tableRow>
				</asp:table>
    </form>
</body>
</html>--%>
