﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFinancialReportWizard.aspx.vb"
    Inherits=".frmFinancialReportWizard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
    <script type="text/javascript">
        function UpdateReportName() {
            //            if (Number(document.getElementById('hdnFRID').value) == 0) {
            var ddlFinancialView = document.getElementById("ddlFinancialView");
            var Text = ddlFinancialView.options[ddlFinancialView.selectedIndex].text;
            var Value = ddlFinancialView.options[ddlFinancialView.selectedIndex].value;
            if (Value != "0") {
                document.getElementById('txtReportName').value = Text;
            }
            var ddlDimension = document.getElementById("ddlDimension");
            if (ddlDimension.selectedIndex > 0) {
                Text = ddlDimension.options[ddlDimension.selectedIndex].text;
                Value = ddlDimension.options[ddlDimension.selectedIndex].value;
                if (Value != "0") {
                    document.getElementById('txtReportName').value = document.getElementById('txtReportName').value + ' ' + Text;
                }
            }
            //            }
        }
        function Save() {
            var ddlFinancialView = document.getElementById("ddlFinancialView");
            if (ddlFinancialView.options[ddlFinancialView.selectedIndex].value == "0") {
                document.getElementById('ddlFinancialView').focus();
                alert('Please Select Financial View');
                return false;
            }
            var ddlDimension = document.getElementById("ddlDimension");
            if (ddlDimension.options[ddlDimension.selectedIndex].value == "0") {
                document.getElementById('ddlDimension').focus();
                alert('Please Select Financial Dimension');
                return false;
            }
            if (document.getElementById("txtReportName").value == "") {
                document.getElementById('txtReportName').focus();
                alert('Please enter report name');
                return false;
            }
            return true;
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Financial Report Wizard&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return Save()" />
                <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close"
                    OnClientClick="return Save()" />
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Table ID="Table3" Width="100%" runat="server" BorderWidth="1" Height="350" GridLines="None"
                    CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <br />
                            <table width="100%" border="0">
                                <tr>
                                    <td align="left" colspan="3">
                                        <br />
                                        <span class="StepClass"><b>Step 1:</b> Select Financial View</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                    </td>
                                    <td class="normal3" colspan="3" align="left">
                                        <asp:DropDownList ID="ddlFinancialView" runat="server" CssClass="signup" onchange="UpdateReportName();setTimeout('__doPostBack(\'ddlFinancialView\',\'\')', 0)">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="StepClass" align="left" colspan="4">
                                        <b>Step 2:</b> Select Dimension
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                    </td>
                                    <td class="normal3" colspan="3" align="left">
                                        <asp:DropDownList runat="server" ID="ddlDimension" CssClass="signup" AutoPostBack="true"
                                            onchange="UpdateReportName();setTimeout('__doPostBack(\'ddlDimension\',\'\')', 0);">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <asp:Panel runat="server" ID="pnlDetail">
                                    <tr>
                                        <td align="right">
                                        </td>
                                        <td class="normal3" colspan="3" align="left">
                                            <asp:Label Text="" runat="server" ID="lblValue1" />
                                            <asp:DropDownList runat="server" ID="ddlValue1" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:Label Text="" runat="server" ID="lblValue2" />
                                            <asp:DropDownList runat="server" ID="ddlValue2" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:Label Text="" runat="server" ID="lblTextValue1" />
                                            <asp:TextBox runat="server" ID="txtValue1" CssClass="signup" />
                                            <telerik:RadComboBox ID="radItem" runat="server" Width="150" DropDownWidth="200px"
                                                AutoPostBack="True" EnableScreenBoundaryDetection="true" EnableLoadOnDemand="true"
                                                AllowCustomText="True">
                                                <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                            </telerik:RadComboBox>
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                        </td>
                                        <td class="normal3" colspan="3" align="left">
                                            <asp:DataGrid ID="dgFinReportDetail" AllowSorting="false" runat="server" CssClass="dg"
                                                AutoGenerateColumns="False"  Width="400px">
                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                <ItemStyle CssClass="is"></ItemStyle>
                                                <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn Visible="False" DataField="numFRDtlID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Value1" HeaderText="<font color=white>Value1</font>">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Value2" HeaderText="<font color=white>Value2</font>">
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                                            </asp:Button>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td class="StepClass" align="left" colspan="4">
                                        <br />
                                        <b>Step 3:</b> Set Date Range
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                    </td>
                                    <td class="normal3" colspan="3" align="left">
                                        <asp:DropDownList runat="server" ID="ddlDateRange" CssClass="signup">
                                            <asp:ListItem Text="Last 365 days (Year)" Value="365" />
                                            <asp:ListItem Text="Last 90 days (Quarter)" Value="90" />
                                            <asp:ListItem Text="Last 30 days (Month)" Value="30" />
                                            <asp:ListItem Text="Last 15 days " Value="15" />
                                            <asp:ListItem Text="Last 7 days (Week)" Value="7" />
                                            <asp:ListItem Text="Today" Value="1" />
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td align="right">
                                    </td>
                                    <td class="normal3" colspan="3">
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td align="right">
                                    </td>
                                    <td class="normal1" colspan="3">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="StepClass" align="left" colspan="4">
                                        <br />
                                        <b>Step 4:</b> Report Name
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                    </td>
                                    <td class="normal3" colspan="3" align="left">
                                        <asp:TextBox runat="server" ID="txtReportName" CssClass="signup" Width="300px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="hdnFRID" />
    </form>
</body>
</html>
