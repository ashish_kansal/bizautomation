﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class frmPayrollTracking
    Inherits BACRMPage

    Dim intMode As Short
    Dim lngPayrollHeaderID, lngUserID, lngPayrollDetailID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intMode = CCommon.ToLong(GetQueryStringVal("Mode"))
            lngPayrollHeaderID = CCommon.ToLong(GetQueryStringVal("PayrollID"))
            lngUserID = CCommon.ToLong(GetQueryStringVal("UserID"))
            lngPayrollDetailID = CCommon.ToLong(GetQueryStringVal("PayrollDetailID"))

            If Not IsPostBack Then
                Select Case Val(intMode)
                    Case 1
                        lblTitle.Text = "Regular Hrs"
                        gvPayrollData.Columns(3).Visible = False
                        gvCommission.Visible = False
                        gvProjectCommission.Visible = False

                        'Case 2 : lblTitle.Text = "Overtime Hrs"
                    Case 3
                        lblTitle.Text = "Paid Leave Hrs"
                        gvPayrollData.Columns(3).Visible = False
                        gvCommission.Visible = False
                        gvProjectCommission.Visible = False

                    Case 4
                        lblTitle.Text = "Commission Paid Invoice"
                        gvPayrollData.Visible = False
                        gvProjectCommission.Visible = False

                    Case 5
                        lblTitle.Text = "Commission UnPaid Invoice"
                        gvPayrollData.Visible = False
                        gvProjectCommission.Visible = False

                    Case 6
                        lblTitle.Text = "Expenses"
                        gvPayrollData.Columns(2).Visible = False
                        gvCommission.Visible = False
                        gvProjectCommission.Visible = False

                    Case 7
                        lblTitle.Text = "Reimbursable Expenses"
                        gvPayrollData.Columns(2).Visible = False
                        gvCommission.Visible = False
                        gvProjectCommission.Visible = False

                    Case 8
                        lblTitle.Text = "Commission from Project"
                        gvCommission.Visible = False
                        gvPayrollData.Visible = False

                End Select

                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.UserCntID = lngUserID
            objPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objPayrollExpenses.PayrollHeaderID = lngPayrollHeaderID
            objPayrollExpenses.PayrollDetailID = lngPayrollDetailID
            objPayrollExpenses.Mode = intMode

            Dim dtPayroll As DataTable = objPayrollExpenses.GetPayrollDetailTracking()

            If intMode = 4 Or intMode = 5 Then
                gvCommission.DataSource = dtPayroll
                gvCommission.DataBind()
            ElseIf intMode = 8 Then
                gvProjectCommission.DataSource = dtPayroll
                gvProjectCommission.DataBind()
            Else
                gvPayrollData.DataSource = dtPayroll
                gvPayrollData.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtDetails As New DataTable
            Dim dtrow As DataRow

            CCommon.AddColumnsToDataTable(dtDetails, "numCategoryHDRID,numComissionID,monCommissionAmt")

            If intMode = 4 Or intMode = 5 Then
                For Each gvRow As GridViewRow In gvCommission.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked Then
                        dtrow = dtDetails.NewRow

                        dtrow.Item("numCategoryHDRID") = 0
                        dtrow.Item("numComissionID") = CCommon.ToLong(gvCommission.DataKeys(gvRow.DataItemIndex)("numComissionID"))
                        dtrow.Item("monCommissionAmt") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)

                        dtDetails.Rows.Add(dtrow)
                    End If
                Next
            ElseIf intMode = 8 Then
                For Each gvRow As GridViewRow In gvProjectCommission.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked Then
                        dtrow = dtDetails.NewRow

                        dtrow.Item("numCategoryHDRID") = 0
                        dtrow.Item("numComissionID") = CCommon.ToLong(gvProjectCommission.DataKeys(gvRow.DataItemIndex)("numComissionID"))
                        dtrow.Item("monCommissionAmt") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)

                        dtDetails.Rows.Add(dtrow)
                    End If
                Next
            Else
                For Each gvRow As GridViewRow In gvPayrollData.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked Then
                        dtrow = dtDetails.NewRow

                        dtrow.Item("numCategoryHDRID") = CCommon.ToLong(gvPayrollData.DataKeys(gvRow.DataItemIndex)("numCategoryHDRID"))
                        dtrow.Item("numComissionID") = 0
                        dtrow.Item("monCommissionAmt") = 0

                        dtDetails.Rows.Add(dtrow)
                    End If
                Next
            End If

            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.PayrollHeaderID = lngPayrollHeaderID
            objPayrollExpenses.UserCntID = lngUserID

            Dim ds As New DataSet
            ds.Tables.Add(dtDetails)
            objPayrollExpenses.strItems = ds.GetXml()

            objPayrollExpenses.Mode = intMode

            objPayrollExpenses.ManagePayrollTracking()

            Response.Write("<script language=javascript>")
            Response.Write("window.opener.document.location.reload();")
            Response.Write("window.close();")
            Response.Write("</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class