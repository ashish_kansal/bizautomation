''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports System.Configuration
Imports Infragistics.WebUI.UltraWebNavigator
Imports BACRM.BusinessLogic.Common
Partial Public Class frmDefineBudget
    Inherits BACRMPage
#Region "Variables"
    Dim mobjChartAccounting As ChartOfAccounting
#End Region

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not IsPostBack Then
    '            
    '            LoadTreeView()
    '            Dim i As Integer
    '            Dim larr As Array

    '            btnClose.Attributes.Add("onclick", "return Close()")
    '            btnSaveClose.Attributes.Add("onclick", "return SaveClose()")

    '            If GetQueryStringVal( "frm") = "OperationBudget" Then
    '                btnSaveClose.Visible = False
    '            Else
    '                ''To Default Check the nodes
    '                larr = GetQueryStringVal( "chartAcntId").ToString.Split(",")
    '                For i = 0 To larr.Length - 2
    '                    SetCheckedValues(larr(i).ToString, ultratreeChartAcnt.Nodes(0))
    '                Next
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub SetCheckedValues(ByVal a As String, ByVal inode As Infragistics.WebUI.UltraWebNavigator.Node)
    '    Try
    '        Dim j As Integer
    '        For j = 0 To inode.Nodes.Count - 1
    '            Dim arr As String()
    '            arr = inode.Nodes(j).TargetFrame.Split(" ")
    '            If arr(0) = a Then inode.Nodes(j).Checked = True
    '            SetCheckedValues(a, inode.Nodes(j))
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    '''    Protected Sub trvChartAccounts_NodeCheckChanged(ByVal sender As Object, ByVal e As Syncfusion.Web.UI.WebControls.Tools.TreeViewNodeEventArgs)
    '''        Dim tv As New Syncfusion.Web.UI.WebControls.Tools.TreeViewNode
    '''        tv = e.Node.ParentNode
    '''        '' tv.Checked = True
    '''Lab:
    '''        If Not IsNothing(tv) Then
    '''            tv.Checked = True
    '''            tv = tv.ParentNode
    '''            GoTo Lab
    '''        End If

    '''        ''For Each tv In e.Node.Items

    '''        ''    tv.Checked = e.Node.Checked
    '''        ''    tv.ParentItem.Checked = True
    '''        ''Next
    '''    End Sub

    'Private Function LoadTreeView()
    '    Try
    '        ultratreeChartAcnt.Nodes.Clear()
    '        If mobjChartAccounting Is Nothing Then mobjChartAccounting = New ChartOfAccounting
    '        mobjChartAccounting.DomainId = Session("DomainID")
    '        mobjChartAccounting.UserCntId = Session("UserContactId")
    '        LoadTreeStructure(mobjChartAccounting.ChartofAccounts)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Function LoadTreeStructure(ByVal dtTreeDetails As DataTable)
    '    Dim i As New Integer
    '    Dim lstrColor As String
    '    Try
    '        Dim tv As Infragistics.WebUI.UltraWebNavigator.Node

    '        tv = New Infragistics.WebUI.UltraWebNavigator.Node
    '        tv.Text = dtTreeDetails.Rows(0).Item("vcCatgyName")
    '        tv.Tag = dtTreeDetails.Rows(0).Item("numAccountID").ToString
    '        ultratreeChartAcnt.Nodes.Add(tv)
    '        tv.Expanded = True
    '        For i = 1 To dtTreeDetails.Rows.Count - 1
    '            If Not IsDBNull(dtTreeDetails.Rows(i).Item("numOpeningBal")) Then
    '                If dtTreeDetails.Rows(i).Item("numOpeningBal") < 0 Then
    '                    lstrColor = "<font color=Red>[" & ReturnMoney(dtTreeDetails.Rows(i).Item("numOpeningBal")) & "]</font>"
    '                Else
    '                    lstrColor = "<font color=Green>[" & ReturnMoney(dtTreeDetails.Rows(i).Item("numOpeningBal")) & "]</font>"
    '                End If
    '            End If

    '            tv = New Infragistics.WebUI.UltraWebNavigator.Node
    '            tv.Tag = dtTreeDetails.Rows(i).Item("numAccountID").ToString
    '            tv.Text = dtTreeDetails.Rows(i)("vcCatgyName") & " - " & "<font color=#546083>" & dtTreeDetails.Rows(i).Item("vcData") & "</font>" & "  " & IIf(IsDBNull(dtTreeDetails.Rows(i).Item("numOpeningBal")), "", lstrColor)
    '            tv.TargetFrame = dtTreeDetails.Rows(i)("numAccountID") & " " & dtTreeDetails.Rows(i)("numAcntType")
    '            ultratreeChartAcnt.Find(dtTreeDetails.Rows(i)("numParntAcntId").ToString).Nodes.Add(tv)
    '            tv.Expanded = True
    '            If GetQueryStringVal( "frm") = "OperationBudget" Then
    '                If dtTreeDetails.Rows(i)("numAcntType") = 814 Or dtTreeDetails.Rows(i)("numAcntType") = 822 Or dtTreeDetails.Rows(i)("numAcntType") = 825 Then
    '                    tv.Checked = True
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Function ReturnMoney(ByVal Money)
    '    Try
    '        If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

End Class