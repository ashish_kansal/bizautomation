﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmPayrollManage.aspx.vb" Inherits=".frmPayrollManage" ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Manage Payroll</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style>
        .step
        {
            font-size: large;
            font-family: Arial;
            margin-right: 10px;
        }
        
        .GraySmallText
        {
            font-size: xx-small;
            font-weight: bold;
            color: gray;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
            UpdateTotal();

            $("#chkCommissionPaidInvoice").change(function () {
                $('#gvPayrollList tr').find("[id*='chkCommissionUnPaidInvoice']").prop('checked', false);
                $('#gvPayrollList tr').find("[id*='chkCommissionBoth']").prop('checked', false);

                UpdateCommissionInvoice();
            });

            $("#chkCommissionUnPaidInvoice").change(function () {
                $('#gvPayrollList tr').find("[id*='chkCommissionPaidInvoice']").prop('checked', false);
                $('#gvPayrollList tr').find("[id*='chkCommissionBoth']").prop('checked', false);

                UpdateCommissionInvoice();
            });

            $("#chkCommissionBoth").change(function () {
                $('#gvPayrollList tr').find("[id*='chkCommissionPaidInvoice']").prop('checked', false);
                $('#gvPayrollList tr').find("[id*='chkCommissionUnPaidInvoice']").prop('checked', false);

                UpdateCommissionInvoice();
            });


            $("#gvPayrollList tr input[id*='txtDeductions']").change(function () {
                SelectPayroll();
                UpdateTotal();
            });

        });


        function UpdateCommissionInvoice() {
            $("#gvPayrollList tr").not(".hs,.fs").each(function () {
                var chkCommissionPaidInvoice = $('#gvPayrollList tr').find("[id*='chkCommissionPaidInvoice']");
                var chkCommissionUnPaidInvoice = $('#gvPayrollList tr').find("[id*='chkCommissionUnPaidInvoice']");
                var chkCommissionBoth = $('#gvPayrollList tr').find("[id*='chkCommissionBoth']");

                var Total = 0, Paid = 0, UnPaid = 0;

                if ($(chkCommissionPaidInvoice).is(':checked')) {
                    Paid = Number($(this).find("#hlCommPaidInvoice").text());
                }
                else if ($(chkCommissionUnPaidInvoice).is(':checked')) {
                    UnPaid = Number($(this).find("#hlCommUNPaidInvoice").text());
                }
                else if ($(chkCommissionBoth).is(':checked')) {
                    Paid = Number($(this).find("#hlCommPaidInvoice").text());
                    UnPaid = Number($(this).find("#hlCommUNPaidInvoice").text());
                }
                else {
                    Paid = Number($(this).find("#hdnTotalCommission").val());
                }

                Total = Paid + UnPaid;
                $(this).find("#lblTotalCommission").text(Total.toFixed(2));
                $(this).find("#hdnTotalCommissionAmount").text(Total.toFixed(2));

            });

            UpdateTotal();
        }

        function UpdateTotal() {
            $("#gvPayrollList tr").not(".hs,.fs").each(function () {
                lblActualRegularHrs = Number($(this).find("#lblActualRegularHrs").text())
                lblActualOvertimeHrs = Number($(this).find("#lblActualOvertimeHrs").text())
                lblActualPaidLeaveHrs = Number($(this).find("#lblActualPaidLeaveHrs").text())
                lblTotalCommission = Number($(this).find("#lblTotalCommission").text())
                lblActualExpense = Number($(this).find("#lblActualExpense").text())
                lblActualReimburse = Number($(this).find("#lblActualReimburse").text())
                txtDeductions = Number($(this).find("#txtDeductions").val())
                hdnHourlyRate = Number($(this).find("#hdnHourlyRate").val())
                hdnOverTimeRate = Number($(this).find("#hdnOverTimeRate").val())

                var Total = (lblActualRegularHrs * hdnHourlyRate) + (lblActualOvertimeHrs * hdnOverTimeRate)
                    + (lblActualPaidLeaveHrs * hdnHourlyRate) + lblTotalCommission - lblActualExpense + lblActualReimburse - txtDeductions;

                $(this).find("#lblTotalAmtDue").text(Total.toFixed(2));
                $(this).find("#hdnTotalAmtDue").val(Total.toFixed(2));
            });
        }

        //select all 
        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('#' + headerCheckBox).is(':checked'))
            });
        }

        //        function Save() {
        //            if (document.getElementById('ctl00_TabsPlaceHolder_calPayDate_txtDate').value == 0) {
        //                alert("Enter Pay Date")
        //                document.getElementById('ctl00_TabsPlaceHolder_calPayDate_txtDate').focus();
        //                return false;
        //            }
        //            if (document.getElementById('txtPayrolllReferenceNo').value == "") {
        //                alert("Enter Entry #");
        //                document.getElementById('txtPayrolllReferenceNo').focus();
        //                return false;
        //            }
        //        }

        function OpenUserDetails(a) {
            window.open("../Accounting/frmEmpPayrollExpenses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1000,height=643,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenPayrollTracking(a, b, c, d) {
            window.open("../Accounting/frmPayrollTracking.aspx?Mode=" + a + "&PayrollID=" + b + "&UserID=" + c + "&PayrollDetailID=" + d, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1000,height=643,scrollbars=yes,resizable=yes')
            return false;
        }

        function Save() {
            if ($('#Content_calPayDate_txtDate').val().length == 0) {
                alert("Please enter Pay Date first.");
                $('#Content_calPayDate_txtDate').focus();
                return false;
            }
            if ($('#Content_fromDate_txtDate').is(':visible')) {
                if ($('#Content_fromDate_txtDate').val().length == 0) {
                    alert("Please enter Payroll From Period.");
                    $('#Content_fromDate_txtDate').focus();
                    return false;
                }
                if ($('#Content_toDate_txtDate').val().length == 0) {
                    alert("Please enter Payroll To Period.");
                    $('#Content_toDate_txtDate').focus();
                    return false;
                }
                if ($('#Content_toDate_txtDate').val() < $('#Content_fromDate_txtDate').val())
                {
                    alert("Please enter Payroll From Date must be greater/equal To Period.");
                    $('#Content_toDate_txtDate').focus();
                    return false;
                }
            }
            if ($('#txtPayrolllReferenceNo').val().length == 0) {
                alert("Please provide Entry #.");
                $('#txtPayrolllReferenceNo').focus();
                return false;
            }
            return true;
        }

        function SelectPayroll() {
            $("#gvPayrollList tr").each(function () {
                if ($(this).find(":input[name*='txtDeductions']") != 'undefined' && $(this).find(":input[name*='txtDeductions']").val() > 0) {
                    console.log($(this).find("#chkSelect"));
                    $(this).find("#chkSelect").prop("checked", true);
                    $(this).find("#chkSelect").attr("enabled", false);
                }
            });
        }
        

    </script>
    <style>
        .form-control {
        max-width:90%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <center>
        <span style="color: Red">
            <asp:Literal runat="server" ID="litMessage"></asp:Literal></span></center>
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            &nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                Text="Close" CausesValidation="false" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Manage Payroll
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div style="background-color: #fff">
        <br />
        <table width="100%" cellpadding="0" cellspacing="2">
            <tbody>
                <tr>
                    <td align="right" class="step" width="60px">
                        Step 1:
                    </td>
                    <td align="right" width="110px">
                        Select Pay Period<font color="#ff0000">*</font>
                    </td>
                    <td class="normal1" align="left" width="300px">
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="signup" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="signup" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlDate" runat="server" CssClass="signup">
                        </asp:DropDownList>
                        <BizCalendar:Calendar ID="fromDate" Visible="false" runat="server" ClientIDMode="Predictable" />
                        <BizCalendar:Calendar ID="toDate" Visible="false" runat="server" ClientIDMode="Predictable" />

                    </td>
                </tr>
                <tr>
                    <td align="right" class="step">
                        Step 2:
                    </td>
                    <td class="alt" align="right">
                        Enter Pay Date<font color="#ff0000">*</font>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <BizCalendar:Calendar ID="calPayDate" runat="server" ClientIDMode="Predictable" />
                                </td>
                                <td>
                                    <asp:Label ID="lblCalenderTip" Text="[?]" runat="server" CssClass="tip" ToolTip="This is Date on which you make payment. Accounting Entries will be posted on selected date."></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="step">
                        Step 3:
                    </td>
                    <td colspan="" class="alt" align="right">
                        Entry # <font color="#ff0000">*</font>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPayrolllReferenceNo" runat="server" MaxLength="15" AutoComplete="OFF"
                            CssClass="required_integer {required:true ,number:true,maxlength:15, messages:{required:'Please provide Entry #!',number:'Please provide valid Entry # (Max Length 15)!'}}" />
                    </td>
                </tr>
                <tr>
                    <td align="right" class="step">
                        Step 4:
                    </td>
                    <td colspan="" class="alt" align="right">
                        Payroll Status
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlPayrollStatus">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnSavePayrollHeader" runat="server" CssClass="button" Text="Continue">
                        </asp:Button>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <asp:GridView ID="gvPayrollList" runat="server" CssClass="tbl" BorderWidth="0" AutoGenerateColumns="False"
            Width="100%" DataKeyNames="numUserCntID">
            <RowStyle CssClass="is" />
            <HeaderStyle CssClass="hs" />
            <RowStyle VerticalAlign="Top" />
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                    ItemStyle-Width="10px">
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll', 'chkSelect');" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <a href="#" onclick='return OpenUserDetails(<%# Eval("numUserID")%>)'>
                            <%# Eval("vcUserName")%></a>
                        <br />
                        <span class="GraySmallText">Hourly Rate:
                            <%# ReturnMoney(Eval("monHourlyRate"))%><br />
                            Overtime Rate:
                            <%# ReturnMoney(Eval("monOverTimeRate"))%></span>
                        <asp:HiddenField ID="hdnUserCntID" runat="server" Value='<%# Eval("numUserCntID")%>' />
                        <asp:HiddenField ID="hdnHourlyRate" runat="server" Value='<%# Eval("monHourlyRate")%>' />
                        <asp:HiddenField ID="hdnOverTimeRate" runat="server" Value='<%# Eval("monOverTimeRate")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="15%">
                    <HeaderTemplate>
                        Regular Hrs<br />
                        <hr />
                        Estimated | Actual | Total ($)
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hlEstimatedRegularHrs" runat="server"></asp:HyperLink>
                        |
                        <asp:Label ID="lblActualRegularHrs" runat="server"></asp:Label>
                        |
                        <asp:Label ID="lblTotalRegularHrs" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="15%">
                    <HeaderTemplate>
                        Overtime Hrs<br />
                        <hr />
                        Estimated | Actual | Total ($)
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblEstimatedOvertimeHrs" runat="server"></asp:Label>
                        |
                        <asp:Label ID="lblActualOvertimeHrs" runat="server"></asp:Label>
                        |
                        <asp:Label ID="lblTotalOvertimeHrs" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="15%">
                    <HeaderTemplate>
                        Paid Leave Hrs<br />
                        <hr />
                        Estimated | Actual | Total ($)
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hlEstimatedPaidLeaveHrs" runat="server"></asp:HyperLink>
                        |
                        <asp:Label ID="lblActualPaidLeaveHrs" runat="server"></asp:Label>
                        |
                        <asp:Label ID="lblTotalPaidLeaveHrs" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="15%">
                    <HeaderTemplate>
                        Total Hrs<br />
                        <hr />
                        Estimated | Actual
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblEstimatedTotalHrs" runat="server"></asp:Label>
                        |
                        <asp:Label ID="lblActualTotalHrs" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="20%">
                    <HeaderTemplate>
                        Commission Amt<br />
                        <hr />
                        <asp:CheckBox ID="chkCommissionPaidInvoice" runat="server" Text="Paid Invoice" />
                        |
                        <asp:CheckBox ID="chkCommissionUnPaidInvoice" runat="server" Text="UnPaid Invoice" />
                        |
                        <asp:CheckBox ID="chkCommissionBoth" runat="server" Text="Both" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hlCommPaidInvoice" runat="server"></asp:HyperLink>
                        |
                        <asp:HyperLink ID="hlCommUNPaidInvoice" runat="server"></asp:HyperLink>
                        |
                        <asp:HiddenField ID="hdnTotalCommission" runat="server" />
                        <asp:HiddenField ID="hdnTotalCommissionAmount" runat="server" />
                        <asp:Label ID="lblTotalCommission" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="10%">
                    <HeaderTemplate>
                        Total Expenses<br />
                        <hr />
                        Estimated | Actual
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hlEstimatedExpense" runat="server"></asp:HyperLink>
                        |
                        <asp:Label ID="lblActualExpense" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="10%">
                    <HeaderTemplate>
                        Reimbursable Expenses<br />
                        <hr />
                        Estimated | Actual
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hlEstimatedReimburse" runat="server"></asp:HyperLink>
                        |
                        <asp:Label ID="lblActualReimburse" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Deductions" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:TextBox ID="txtDeductions" Width="60" runat="server" MaxLength="10" Text='<%# ReturnMoney(Eval("monDeductions"))%>'
                            AUTOCOMPLETE="OFF" CssClass="required_money {required:false ,number:true,maxlength:15, messages:{number:'Please provide valid Deduction(Max Length 10)!'}}"
                            Style="text-align: right" onblur="SelectPayroll"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Amt Due" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdnTotalAmtDue" runat="server" />
                        <asp:Label ID="lblTotalAmtDue" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
