''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Partial Public Class frmCommissionforPayrollExpense
    Inherits BACRMPage
#Region "Variables"
    Dim lngUserCntId As Long
    Dim lngComPayPeriodID As Long
    Dim lngUserId As Long
    Dim lngDivisionID As Long
    Dim Mode As Short
    Dim dtStartDate As Date
    Dim dtEndDate As Date
    Dim numTotalDiscount As Double = 0
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngUserCntId = GetQueryStringVal("NameU")
            lngUserId = GetQueryStringVal("PersonalId")
            Mode = CCommon.ToShort(GetQueryStringVal("Mode"))
            lngComPayPeriodID = CCommon.ToLong(GetQueryStringVal("ComPayPeriodID"))
            lngDivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))

            btnCancel.Attributes.Add("onclick", "return Close()")
            If Not IsPostBack Then
                ' Option of Project Gross Profit is now removed so no need for this code
                'If Session("CommissionType") = 3 Then 'If for Project Gross Profit
                '    lblTitle.Text = "Commission from Project"
                '    BindProjectCommission()
                'Else
                If Mode = 0 Then
                    lblTitle.Text = "Commission from fully paid invoices"
                ElseIf Mode = 1 Then
                    lblTitle.Text = "Commission from non-fully paid invoices"
                ElseIf Mode = 4 Then
                    lblTitle.Text = "CMs & Refunds"
                End If

                BindCommissionRules()
                dgBizDocs.MasterTableView.HierarchyLoadMode = GridChildLoadMode.Client

                If Mode = 4 Then
                    LoadSalesReturn()
                    gvCreditMemoAndRefunds.Visible = True
                    dgBizDocs.Visible = False
                Else
                    LoadCommissionListGrid()
                    gvCreditMemoAndRefunds.Visible = False
                    dgBizDocs.Visible = True
                End If


                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindProjectCommission()
        Try
            Dim lobjPayrollExpenses As New PayrollExpenses
            lobjPayrollExpenses.DomainID = Session("DomainId")
            lobjPayrollExpenses.UserCntID = lngUserCntId
            lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            lobjPayrollExpenses.StartDate = dtStartDate
            lobjPayrollExpenses.EndDate = dtEndDate
            lobjPayrollExpenses.OppBizDocsId = 0
            lobjPayrollExpenses.Mode = 1

            gvProjectCommission.DataSource = lobjPayrollExpenses.GetProjectCommission()
            gvProjectCommission.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSalesReturn()
        Try
            Dim lobjPayrollExpenses As New PayrollExpenses
            lobjPayrollExpenses.DomainID = Session("DomainId")
            lobjPayrollExpenses.UserCntID = lngUserCntId
            lobjPayrollExpenses.UserId = lngUserId
            lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            lobjPayrollExpenses.PayPeriod = lngComPayPeriodID
            lobjPayrollExpenses.DivisionID = lngDivisionID
            If ddlCommissionRule.SelectedValue <> "0" Then
                lobjPayrollExpenses.ComRuleID = CCommon.ToLong(ddlCommissionRule.SelectedValue)
            End If

            gvCreditMemoAndRefunds.DataSource = lobjPayrollExpenses.GetCommissionSalesReturn()
            gvCreditMemoAndRefunds.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Sub LoadCommissionListGrid()
        Try
            Dim lobjPayrollExpenses As New PayrollExpenses
            lobjPayrollExpenses.DomainID = Session("DomainId")
            lobjPayrollExpenses.UserCntID = lngUserCntId
            lobjPayrollExpenses.UserId = lngUserId
            lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            lobjPayrollExpenses.PayPeriod = lngComPayPeriodID
            lobjPayrollExpenses.OppBizDocsId = 0
            lobjPayrollExpenses.DivisionID = lngDivisionID
            If ddlCommissionRule.SelectedValue <> "0" Then
                lobjPayrollExpenses.ComRuleID = CCommon.ToLong(ddlCommissionRule.SelectedValue)
            End If

            dgBizDocs.DataSource = lobjPayrollExpenses.GetOpportuntityCommission(Mode)
            dgBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return String.Empty
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgBizDocs_DetailTableDataBind(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles dgBizDocs.DetailTableDataBind

        Dim parentItem As GridDataItem = _
    CType(e.DetailTableView.ParentItem, GridDataItem)

        Dim lobjPayrollExpenses As New PayrollExpenses
        lobjPayrollExpenses.DomainID = Session("DomainId")
        lobjPayrollExpenses.UserCntID = lngUserCntId
        lobjPayrollExpenses.UserId = lngUserId
        lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        lobjPayrollExpenses.PayPeriod = lngComPayPeriodID
        lobjPayrollExpenses.OppBizDocsId = Convert.ToInt32(parentItem.GetDataKeyValue("numOppBizDocsId").ToString())
        lobjPayrollExpenses.DivisionID = lngDivisionID
        If ddlCommissionRule.SelectedValue <> "0" Then
            lobjPayrollExpenses.ComRuleID = CCommon.ToLong(ddlCommissionRule.SelectedValue)
        End If

        e.DetailTableView.DataSource = lobjPayrollExpenses.GetOpportuntityCommission(IIf(Mode = 0, 2, 3))
    End Sub

    Private Sub dgBizDocs_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles dgBizDocs.ItemDataBound
        Try
            If (TypeOf e.Item Is GridDataItem Or TypeOf e.Item Is GridFooterItem) AndAlso e.Item.OwnerTableView.Name = "ParentGrid" Then
                If TypeOf e.Item Is GridDataItem Then
                    Dim dataItem As GridDataItem = DirectCast((e.Item), GridDataItem)
                    numTotalDiscount = (numTotalDiscount + Double.Parse((DirectCast((dataItem("TemplateTotalCommission").FindControl("lblTotal")), Label)).Text))
                ElseIf TypeOf e.Item Is GridFooterItem Then
                    Dim footer As GridFooterItem = DirectCast((e.Item), GridFooterItem)
                    DirectCast((footer("TemplateTotalCommission").FindControl("lblGrandTotal")), Label).Text = String.Format("{0:##,#00.0000}", numTotalDiscount)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvCreditMemoAndRefunds_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvCreditMemoAndRefunds.ItemDataBound
        Try
            If (TypeOf e.Item Is GridDataItem Or TypeOf e.Item Is GridFooterItem) AndAlso e.Item.OwnerTableView.Name = "ParentGrid" Then
                If TypeOf e.Item Is GridDataItem Then
                    Dim dataItem As GridDataItem = DirectCast((e.Item), GridDataItem)
                    numTotalDiscount = (numTotalDiscount + Double.Parse((DirectCast((dataItem("TemplateTotalCommission").FindControl("lblTotal")), Label)).Text))
                ElseIf TypeOf e.Item Is GridFooterItem Then
                    Dim footer As GridFooterItem = DirectCast((e.Item), GridFooterItem)
                    DirectCast((footer("TemplateTotalCommission").FindControl("lblGrandTotal")), Label).Text = String.Format("{0:##,#00.0000}", numTotalDiscount)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub BindCommissionRules()
        Try
            Dim listItem As New ListItem
            listItem.Text = "--- Select One ---"
            listItem.Value = 0
            ddlCommissionRule.Items.Insert(0, listItem)

            Dim objCommission As New CommissionRule
            objCommission.DomainID = CCommon.ToLong(Session("DomainID"))
            objCommission.byteMode = 1
            Dim dt As DataTable = objCommission.GetCommissionRule()

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    listItem = New ListItem
                    listItem.Text = dr("vcCommissionName")
                    listItem.Value = dr("numComRuleID")
                    ddlCommissionRule.Items.Add(listItem)
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ddlCommissionRule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCommissionRule.SelectedIndexChanged
        Try
            If Mode = 4 Then
                LoadSalesReturn()
            Else
                LoadCommissionListGrid()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class