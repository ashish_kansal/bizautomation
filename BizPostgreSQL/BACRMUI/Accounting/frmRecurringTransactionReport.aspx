﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringTransactionReport.aspx.vb"
    Inherits=".frmRecurringTransactionReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Recurring Transaction Report</title>
    <style type="text/css">
        .style1
        {
            width: 336px;
        }
        .style2
        {
            width: 699px;
        }
    </style>
    <script type="text/javascript">
        function OpenOpp(a, b, c) {
            if (c == 1) {
                var str;
                str = "../pagelayout/frmOppurtunityDtl.aspx?frm=RecurringTransactionReport&OpID=" + a;
                document.location.href = str;
            }
            if (c == 2) {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td class="style3" nowrap align="right">
            </td>
            <td class="style2">
                No of Records:
                <asp:Label ID="lblRecordsDeals" runat="server"></asp:Label>
            </td>
            <td id="hidenav" nowrap align="right" runat="server" class="style1">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblNextDeals" runat="server" CssClass="Text_bold">Next:</asp:Label>
                        </td>
                        <td class="normal1">
                            &nbsp;
                        </td>
                        <td class="normal1">
                            &nbsp;
                        </td>
                        <td class="normal1">
                            &nbsp;
                        </td>
                        <td class="normal1">
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkFirst" runat="server">
											 <div class="LinkArrow"><<</div>
                            </asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkPrevious" runat="server">
											 <div class="LinkArrow"><</div>
                            </asp:LinkButton>
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblPageDeals" runat="server">Page</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCurrrentPageDeals" runat="server" CssClass="signup" Width="28px"
                                AutoPostBack="True" MaxLength="5" Text="1"></asp:TextBox>
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblOfDeals" runat="server">of</asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow">
											 <div class="LinkArrow">></div>
                            </asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkLast" runat="server">
											 <div class="LinkArrow">>></div>
                            </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" style="width: 100%">
        <tr>
            <td valign="bottom" class="style5" height="23">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Recurring Transaction Report &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" align="right" height="23">
                &nbsp<asp:Button ID="btnClose" runat="Server" CssClass="button" OnClientClick="javascript:CloseMe();"
                    Text="Close"></asp:Button>&nbsp;
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" runat="server" Width="100%" Height="350" GridLines="None" BorderColor="black"
        CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgRep" runat="server" AllowSorting="false" CssClass="dg" Width="100%"
                    BorderColor="white" AllowCustomPaging="true" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numRecTranRepId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numRecTranSeedId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numRecTranOppID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numRecurringId"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TranType" SortExpression="TranType" HeaderText="<font color=white>Event Type</font>">
                        </asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="varRecurringTemplateName" HeaderText="<font color=white>Recurring Template Name</font>"
                            SortExpression="varRecurringTemplateName" CommandName="RecTemp"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="TotalCostAmount" SortExpression="TotalCostAmount" HeaderText="<font color=white>Calculated Amount</font>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="<font color=white>Created Opportunity or Biz Doc</font>">
                            <ItemTemplate>
                                <a href="#" onclick="OpenOpp('<%# Eval("numRecTranOppID") %>','<%# Eval("numOppBizDocsId") %>','<%# Eval("tintRecType") %>')">
                                    <%#Eval("RecurringName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="dteOppCreationDate" SortExpression="dteOppCreationDate"
                            HeaderText="<font color=white>Opportunity creation date</font>"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
