<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTimeAndExpensesDetails.aspx.vb"
    Inherits=".frmTimeAndExpensesDetails" MasterPageFile="~/common/PopupBootstrap.Master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Close">
        </asp:Button>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Time & Expense Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <h4>Time & Expense</h4>
    <asp:DataGrid ID="dgTimeAndExpenses" AllowSorting="true" runat="server" Width="100%"
        CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true" >
        <Columns>
            <asp:BoundColumn DataField="numCategoryHDRID" Visible="false"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="From Date">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "dtFromDate")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="To Date">
                <ItemTemplate>
                    <%#DataBinder.Eval(Container.DataItem, "dtToDate")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Hrs/Expense"  ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <asp:LinkButton ID="hplTypeHrs" runat="server" CssClass="hyperlink" CommandName="Edit"><%# DataBinder.Eval(Container.DataItem,"Detail")%> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="ClientCharge" HeaderText="Employee Cost">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="RecType" HeaderText="Type" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Type" HeaderText="Type of Hrs/Expense" >
            </asp:BoundColumn>
            <asp:BoundColumn DataField="chrFrom" HeaderText="From" >
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Description" HeaderText="Notes">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vcUserName" HeaderText="Created By">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="ApprovedBy" HeaderText="Approved By">
            </asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <br />
    <h4>Business Process Time & Expense</h4>
    <asp:DataGrid ID="gvBPTask" AllowSorting="true" runat="server" Width="100%"
        CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true" >
        <Columns>
            <asp:BoundColumn DataField="vcPOppName" HeaderText="Order"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcProjectName" HeaderText="Project"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcUserName" HeaderText="Created By"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcMileStoneName" HeaderText="Milestone"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcStageName" HeaderText="Stage"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcTaskName" HeaderText="Task"></asp:BoundColumn>
            <asp:BoundColumn DataField="dtCreatedDate" HeaderText="Date"></asp:BoundColumn>
            <asp:BoundColumn DataField="numHours" HeaderText="Hours Worked"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
