﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PrintCheckEditor

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnSavePreferences control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSavePreferences As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trUpload As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''txtFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFile As Global.System.Web.UI.HtmlControls.HtmlInputFile

    '''<summary>
    '''btnUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpload As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ddlCheckDateFontfamily control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCheckDateFontfamily As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlCheckDateFontSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCheckDateFontSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlCheckDateFontStyle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCheckDateFontStyle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlPayeeNameFontfamily control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPayeeNameFontfamily As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlPayeeNameFontSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPayeeNameFontSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlPayeeNameFontStyle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPayeeNameFontStyle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAmountFontFamily control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAmountFontFamily As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAmountFontSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAmountFontSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAmountFontStyle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAmountFontStyle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAmountInWordsFontFamily control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAmountInWordsFontFamily As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAmountInWordsFontSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAmountInWordsFontSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAmountInWordsFontStyle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAmountInWordsFontStyle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkEmployerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEmployerName As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ddlEmployerNameFontFamily control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEmployerNameFontFamily As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlEmployerNameFontSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEmployerNameFontSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlEmployerNameFontStyle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEmployerNameFontStyle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkAddress As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ddlAddressFontFamily control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAddressFontFamily As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAddressFontSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAddressFontSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAddressFontStyle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAddressFontStyle As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtCheckDateLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCheckDateLeft As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCheckDateTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCheckDateTop As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPayeeNameLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayeeNameLeft As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPayeeNameTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayeeNameTop As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAmountLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmountLeft As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAmountTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmountTop As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAmountInWordsLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmountInWordsLeft As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAmountInWordsTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmountInWordsTop As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtEmployerNameLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmployerNameLeft As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtEmployerNameTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmployerNameTop As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddressLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddressLeft As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddressTop control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddressTop As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtStub1Left control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStub1Left As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtStub1Top control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStub1Top As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtStub2Left control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStub2Left As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtStub2Top control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStub2Top As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ifCheckStubPdF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifCheckStubPdF As Global.System.Web.UI.HtmlControls.HtmlIframe

    '''<summary>
    '''btnPreview control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPreview As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''iFramePrintChecks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iFramePrintChecks As Global.System.Web.UI.HtmlControls.HtmlIframe
End Class
