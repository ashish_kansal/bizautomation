﻿
' ''Created By Ajit Singh
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Common

'Imports BACRM.BusinessLogic.Account
'Imports BACRM.BusinessLogic.Contacts
'Imports BACRM.BusinessLogic.Admin

'Imports BACRM.UserInterface

'Partial Public Class frmCustomerVendorTransactions : Inherits BACRMPage

'    Dim objCommon As CCommon

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            btnGo.Attributes.Add("onclick", "return fn_CheckSearchCondition()")
'            If Not IsPostBack Then

'                radCmbCompany.Focus()
'                'objCommon = New CCommon
'                'Dim objAdmin As New Tickler
'                'Dim strCompany, strDivision As String
'                'objAdmin.DivisionID = objCommon.DivisionID
'                'strCompany = objAdmin.GetCompanyName
'                'radCmbCompany.Text = strCompany
'                'radCmbCompany.Value = objCommon.DivisionID
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    'Sub LoadItem()
'    '    Try
'    '        Dim lobjChecks As New Checks
'    '        lobjChecks.DomainId = Session("DomainID")
'    '        ddlByName.DataSource = lobjChecks.GetCompanyDetails
'    '        ddlByName.DataTextField = "vcCompanyName"
'    '        ddlByName.DataValueField = "numDivisionID"
'    '        ddlByName.DataBind()
'    '        ddlByName.Items.Insert(0, New ListItem("--Select One --", "0"))
'    '    Catch ex As Exception
'    '        Throw ex
'    '    End Try
'    'End Sub

'    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
'        Try
'            Dim _searchCriteria As String = String.Empty

'            If rdByName.Checked Then
'                _searchCriteria = "rdByName"
'            ElseIf rdByCustomer.Checked Then
'                _searchCriteria = "rdByCustomer"
'            ElseIf rdByVendor.Checked Then
'                _searchCriteria = "rdByVendor"
'            End If
'            '_searchCriteria = _searchCriteria & "|" & calFrom.SelectedDate & "|" & calTo.SelectedDate
'            Me.BindDatagrid(_searchCriteria, Convert.ToDateTime(calFrom.SelectedDate), Convert.ToDateTime(calTo.SelectedDate))

'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub BindDatagrid(ByVal sTransactionBy As String, ByVal dtFromDate As DateTime, ByVal dtToDate As DateTime)
'        Try
'            Dim objJournalEntry As New JournalEntry()
'            objJournalEntry.DomainId = Session("DomainId")
'            objJournalEntry.Entry_Date = dtFromDate
'            Dim dt As DataTable = objJournalEntry.GetCustomerVendorEntryList(CInt(Session("UserID")), sTransactionBy, dtToDate)
'            gvSearch.DataSource = dt
'            gvSearch.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Public Function _GetBizDoc(ByVal sTransactionType As String) As String
'        Select Case sTransactionType
'            Case "BizDocs Invoice" : Return "Invoice"
'            Case "Receive Amt" : Return "Invoice"
'            Case "Deposit" : Return String.Empty
'            Case "Cash" : Return String.Empty
'            Case "Charge" : Return String.Empty
'            Case "Journal" : Return String.Empty
'            Case "Checks" : Return String.Empty
'            Case "Vendor Amt" : Return "Purchase"
'            Case "BizDocs Purchase" : Return "Purchase"
'        End Select
'        Return String.Empty
'    End Function

'    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.WebControls.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
'        Try

'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub
'End Class