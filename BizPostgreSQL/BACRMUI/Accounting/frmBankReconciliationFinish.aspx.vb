﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class frmBankReconciliationFinish
    Inherits BACRMPage

    Dim objJournalEntry As JournalEntry
    Dim lngReconcileID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            lngReconcileID = CCommon.ToLong(GetQueryStringVal("ReconcileID"))
            If Not IsPostBack Then
                If lngReconcileID > 0 Then
                    LoadBankReconcile()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadBankReconcile()
        Try
            Dim monPayment, monDeposit, monEndBalance, monBeginBalance, monDifference As Decimal
            monPayment = monDeposit = monEndBalance = monBeginBalance = monDifference = 0

            Dim objBankReconcile As New BankReconcile
            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 1
            objBankReconcile.ReconcileID = lngReconcileID

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()
            Dim dtReconcile As DataTable = ds.Tables(0)

            monPayment = CCommon.ToDecimal(dtReconcile.Rows(0)("Payment"))
            monDeposit = CCommon.ToDecimal(dtReconcile.Rows(0)("Deposit"))
            monBeginBalance = CCommon.ToDecimal(dtReconcile.Rows(0)("monBeginBalance"))
            monEndBalance = CCommon.ToDecimal(dtReconcile.Rows(0)("monEndBalance"))

            hfServiceChargeAmount.Value = CCommon.ToDecimal(dtReconcile.Rows(0)("monServiceChargeAmount"))
            hfInterestEarnedAmount.Value = CCommon.ToDecimal(dtReconcile.Rows(0)("monInterestEarnedAmount"))
            hfChartAcntId.Value = CCommon.ToLong(dtReconcile.Rows(0)("numChartAcntId"))
            hfStatementDate.Value = dtReconcile.Rows(0)("dtStatementDate")

            'objBankReconcile.Mode = 4
            'objBankReconcile.ReconcileComplete = True
            'objBankReconcile.AcntId = dtReconcile.Rows(0)("numChartAcntId")

            'ds = objBankReconcile.ManageBankReconcileMaster()

            'dtReconcile = ds.Tables(0)

            'If dtReconcile.Rows.Count > 0 Then
            '    monBeginBalance = CCommon.ToDecimal(dtReconcile.Rows(0)("monAmount"))
            'End If

            monDifference = monEndBalance - (monBeginBalance + monDeposit - monPayment)

            If Math.Truncate(monDifference * 100) / 100 = 0 Then
                Using ObjTransctionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    objBankReconcile = New BankReconcile
                    objBankReconcile.DomainID = Session("DomainId")
                    objBankReconcile.ReconcileID = lngReconcileID
                    objBankReconcile.DeleteOldAdjustment()

                    objBankReconcile = New BankReconcile
                    objBankReconcile.DomainID = Session("DomainId")
                    objBankReconcile.UserCntID = Session("UserContactID")
                    objBankReconcile.Mode = 5
                    objBankReconcile.ReconcileComplete = False
                    objBankReconcile.ReconcileID = lngReconcileID

                    objBankReconcile.ManageBankReconcileMaster()
                    ObjTransctionScope.Complete()
                End Using

                Response.Redirect("~/Accounting/frmBankReconcileList.aspx", False)
            Else
                lblDiff.Text = ReturnMoney(monDifference)
                hfDifference.Value = monDifference
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect("~/Accounting/frmBankReconciliation.aspx?ReconcileID=" & lngReconcileID, False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnAdjustment_Click(sender As Object, e As System.EventArgs) Handles btnAdjustment.Click
        Try
            Dim objJournal As New JournalEntry
            Dim lngRCAccountId As Long
            lngRCAccountId = ChartOfAccounting.GetDefaultAccount("RC", Session("DomainID"))

            If lngRCAccountId = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reconciliation Discrepancies from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                Exit Sub
            End If

            Dim lngJournalId As Long

            objCommon.DomainID = Session("DomainID")
            objCommon.Str = lngReconcileID
            objCommon.Mode = 28

            lngJournalId = CCommon.ToLong(objCommon.GetSingleFieldValue())

            Using ObjTransctionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                lngJournalId = SaveDataToHeader(lngJournalId)
                SaveDataToGeneralJournalDetails(lngJournalId, lngRCAccountId)

                Dim objBankReconcile As New BankReconcile
                objBankReconcile.DomainID = Session("DomainId")
                objBankReconcile.UserCntID = Session("UserContactID")
                objBankReconcile.Mode = 5
                objBankReconcile.ReconcileComplete = False
                objBankReconcile.ReconcileID = lngReconcileID

                objBankReconcile.ManageBankReconcileMaster()

                ObjTransctionScope.Complete()
            End Using
            Response.Redirect("~/Accounting/frmBankReconcileList.aspx", False)
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader(ByVal lngJournalId As Long) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lngJournalId
                .RecurringId = 0
                .EntryDate = hfStatementDate.Value
                .Description = "Bank Reconcile : Adjustment Statement Date " & hfStatementDate.Value
                .Amount = CCommon.ToDecimal(hfInterestEarnedAmount.Value) + CCommon.ToDecimal(hfServiceChargeAmount.Value) + Math.Abs(CCommon.ToDecimal(hfDifference.Value))
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = CCommon.ToLong(hfChartAcntId.Value)
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .ReconcileID = lngReconcileID
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Integer, lngRCAccountId As Long)
        Dim monDifference As Decimal
        monDifference = CCommon.ToDecimal(hfDifference.Value)
        Try
            Dim objJEList As New JournalEntryCollection


            Dim objJE As New JournalEntryNew()
            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = Math.Abs(monDifference)
            objJE.ChartAcntId = If(monDifference > 0, lngRCAccountId, hfChartAcntId.Value)
            objJE.Description = "Bank Reconcile : Adjustment"
            objJE.CustomerId = 0
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = True
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = lngReconcileID
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = Math.Abs(monDifference)
            objJE.CreditAmt = 0
            objJE.ChartAcntId = If(monDifference > 0, hfChartAcntId.Value, lngRCAccountId)
            objJE.Description = "Bank Reconcile : Adjustment"
            objJE.CustomerId = 0
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = True
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = lngReconcileID
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master, BizMaster).DisplayError(exception)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

End Class