﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountsPayableInvoice.aspx.vb"
    Inherits=".frmAccountsPayableInvoice" MasterPageFile="~/common/Popup.Master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function OpenOpp(a, b, c) {
            if (c > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                window.opener.reDirectPage(str);
            }
        }
        function OpenBizInvoice(a, b, c, d) {
            if (d == -1) {
                if (parseInt(c) > 0) {
                    window.open('../Accounting/frmBillPayment.aspx?BizDocsPaymentDetId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
                    return false;
                }
            }
            else {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                return false;
            }
        }
        function OpenAmtPaid(a, b, c, d) {
            if (d > 0) {
                var BalanceAmt = c;
                RPPopup = window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, 'ReceivePayment', 'toolbar=no,titlebar=no,top=50,width=1000,height=500,scrollbars=yes,resizable=no');
                return false;
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table border="0" width="980px">
        <tr>
            <td>
                Range:
                <asp:DropDownList ID="ddlRange" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="">All</asp:ListItem>
                    <asp:ListItem Value="0+30">Today - 30</asp:ListItem>
                    <asp:ListItem Value="30+60">31 - 60</asp:ListItem>
                    <asp:ListItem Value="60+90">61 - 90</asp:ListItem>
                    <asp:ListItem Value="90+">Over 91</asp:ListItem>
                    <asp:ListItem Value="0-30">Past Due Today - 30</asp:ListItem>
                    <asp:ListItem Value="30-60">Past Due 31 - 60</asp:ListItem>
                    <asp:ListItem Value="60-90">Past Due 61 - 90</asp:ListItem>
                    <asp:ListItem Value="90-">Past Due Over 91</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <div>
        
            A/P Aging Detail Report
            <asp:ImageButton runat="server" ID="ibExportExcel" AlternateText="Export to Excel"
                ToolTip="Export to Excel" ImageUrl="~/images/Excel.png" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvARDetails" runat="server" AutoGenerateColumns="False" Style="margin-right: 0px"
        CssClass="dg" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <Columns>
            <asp:BoundField HeaderText="Company Name" DataField="vcCompanyName" ItemStyle-HorizontalAlign="Left">
                <ItemStyle HorizontalAlign="Left" Width="130px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="Invoice / Bill Id" SortExpression="vcbizdocid">
                <ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>','<%# Eval("numBizDocsPaymentDetId") %>','<%# Eval("tintOppType") %>');">
                        <%#Eval("vcBizDocID")%>
                    </a>&nbsp;&nbsp;(<%#Eval("vcPOppName")%>)
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Due Date" DataField="DueDate" ItemStyle-HorizontalAlign="Center">
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Amt Paid">
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%--<a href="javascript:void(0)" onclick="return OpenAmtPaid('<%# Eval("numoppbizdocsid") %>','<%# Eval("numOppId") %>','<%# Eval("BalanceDue") %>','<%# Eval("tintOppType") %>');">--%>
                        <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")),Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")))%>
                    <%--</a>--%>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblAmountPaid" runat="server"></asp:Label>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Amt Due">
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue")) + "</b></font>",Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue"))) %>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField HeaderText="Order Contact" DataField="vcContactName" ItemStyle-HorizontalAlign="Left">
                <ItemStyle Width="140px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Phone & Ext" DataField="vcCustPhone" ItemStyle-HorizontalAlign="Left"
                ItemStyle-Width="120px"></asp:BoundField>
            <asp:BoundField HeaderText="Order Owner" DataField="RecordOwner" ItemStyle-HorizontalAlign="Left">
                <ItemStyle Width="120px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Order Assignee" DataField="AssignedTo" ItemStyle-HorizontalAlign="Left">
                <ItemStyle Width="140px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Days Late" DataField="DaysLate" ItemStyle-HorizontalAlign="Center">
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Invoice #" DataField="vcRefOrderNo" ItemStyle-HorizontalAlign="Left">
                <ItemStyle Width="120px" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            No Record Found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
