''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmEmpPayrollExpense
    Inherits BACRMPage
#Region "Variables"
    Dim lngUserID As Long
   
    Dim objCommon As CCommon
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngUserID = GetQueryStringVal( "NameU")    'Take the user id from the querystring
            If Not IsPostBack Then
                
                objCommon = New CCommon
                GetUserRightsForPage(35, 88)
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnSaveClose.Visible = False

                CalHired.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                CalReleased.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                LoadSaveInformation()
                txtHourlyRate.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtDailyHours.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtLimDailHrs.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtOverTimeRate.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtOverTimeRate.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                'txtManagerOfOwner.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                'txtOwnerCommission.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                'txtAssigneeCommission.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtPaidLeaveHrs.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtAmtWithheld.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtEmployeeRating.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtNetPay.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                btnCancel.Attributes.Add("onclick", "return Close()")
                'lnkRoles.Attributes.Add("onclick", "return OpenRoles(" & lngUserID & ")")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadSaveInformation()
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            Dim dtUserAccessDetails As DataTable
            Dim strStartTime As DateTime
            objPayrollExpenses.UserId = lngUserID
            objPayrollExpenses.DomainId = Session("DomainID")
            dtUserAccessDetails = objPayrollExpenses.GetUserAccessDetails
            If dtUserAccessDetails.Rows.Count > 0 Then
                lblEmpName.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcUserName")), "", dtUserAccessDetails.Rows(0).Item("vcUserName"))
                lblDepartment.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcDepartment")), "", dtUserAccessDetails.Rows(0).Item("vcDepartment"))
                lblEmailAddress.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcAsstEmail")), "", dtUserAccessDetails.Rows(0).Item("vcAsstEmail"))
                lblCellPhone.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("CellNo")), "", dtUserAccessDetails.Rows(0).Item("CellNo"))
                lblOtherPhone.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("OtherPhone")), "", dtUserAccessDetails.Rows(0).Item("OtherPhone"))
                lblHomePhone.Text = dtUserAccessDetails.Rows(0).Item("numHomePhone")
                txtNotes.Text = dtUserAccessDetails.Rows(0).Item("txtNotes")
                lblHomeAddress.Text = dtUserAccessDetails.Rows(0).Item("HomeAddress")

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitHourlyRate")) Then
                    If dtUserAccessDetails.Rows(0).Item("bitHourlyRate") = True Then
                        ''  chkHourlyRate.Checked = True
                        txtHourlyRate.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("monHourlyRate"))
                        ddltime.ClearSelection()
                        If Not ddltime.Items.FindByText(Format(dtUserAccessDetails.Rows(0).Item("dtSalaryDateTime"), "h:mm")) Is Nothing Then
                            ddltime.Items.FindByText(Format(dtUserAccessDetails.Rows(0).Item("dtSalaryDateTime"), "h:mm")).Selected = True
                        End If

                        If Format(dtUserAccessDetails.Rows(0).Item("dtSalaryDateTime"), "tt") = "AM" Then
                            chkAM.Checked = True
                        Else : chkPM.Checked = True
                        End If
                    End If
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitSalary")) Then
                    If dtUserAccessDetails.Rows(0).Item("bitSalary") = True Then
                        chkSalary.Checked = True
                        txtDailyHours.Text = dtUserAccessDetails.Rows(0).Item("numDailyHours")
                    End If
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitPaidLeave")) Then
                    If dtUserAccessDetails.Rows(0).Item("bitPaidLeave") = True Then
                        chkPaidLeave.Checked = True
                        txtPaidLeaveHrs.Text = dtUserAccessDetails.Rows(0).Item("fltPaidLeaveHrs")
                        objPayrollExpenses.UserCntID = dtUserAccessDetails.Rows(0).Item("numUserCntId")
                        '' txtPaidLeaveBalance.Text = dtUserAccessDetails.Rows(0).Item("fltPaidLeaveEmpHrs")
                        objPayrollExpenses.GetBalancePaidLeave()
                        lblPaidLeaveBalance.Text = IIf(objPayrollExpenses.CurrentBalancePaidLeaveHrs < 0, "<font color=Red>" & objPayrollExpenses.CurrentBalancePaidLeaveHrs & "</font>", objPayrollExpenses.CurrentBalancePaidLeaveHrs)
                    End If
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitOverTime")) Then
                    If dtUserAccessDetails.Rows(0).Item("bitOverTime") = True Then
                        chkOverTime.Checked = True
                        If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitOverTimeHrsDailyOrWeekly")) Then
                            If dtUserAccessDetails.Rows(0).Item("bitOverTimeHrsDailyOrWeekly") = True Then
                                rdbOverTimeDaily.Checked = True
                                txtLimDailHrs.Text = dtUserAccessDetails.Rows(0).Item("numLimDailHrs")
                                txtOverTimeRate.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("monOverTimeRate"))
                            Else
                                rdbOverTimeWeekly.Checked = True
                                txtLimWeeklyHrs.Text = dtUserAccessDetails.Rows(0).Item("numLimDailHrs")
                                txtOverTimeWeeklyRate.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("monOverTimeRate"))
                            End If
                        End If
                    End If
                End If

                'If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitManagerOfOwner")) Then
                '    If dtUserAccessDetails.Rows(0).Item("bitManagerOfOwner") = True Then
                '        chkManagerOfOwner.Checked = True
                '        txtManagerOfOwner.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("fltManagerOfOwner"))
                '    End If
                'End If

                'If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitCommOwner")) Then
                '    If dtUserAccessDetails.Rows(0).Item("bitCommOwner") = True Then
                '        chkOwnerCommission.Checked = True
                '        txtOwnerCommission.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("fltCommOwner"))
                '    End If
                'End If

                'If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitCommAssignee")) Then
                '    If dtUserAccessDetails.Rows(0).Item("bitCommAssignee") = True Then
                '        chkAssigneeCommission.Checked = True
                '        txtAssigneeCommission.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("fltCommAssignee"))
                '    End If
                'End If

                'If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitRoleComm")) Then
                '    If dtUserAccessDetails.Rows(0).Item("bitRoleComm") = True Then chkRoleCommission.Checked = True
                'End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitEmpNetAccount")) Then
                    If dtUserAccessDetails.Rows(0).Item("bitEmpNetAccount") = True Then chkEmpCOGS.Checked = True
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitPayroll")) Then
                    If dtUserAccessDetails.Rows(0).Item("bitPayroll") = True Then chkPayroll.Checked = True
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("dtHired")) Then
                    CalHired.SelectedDate = dtUserAccessDetails.Rows(0).Item("dtHired")
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("dtReleased")) Then
                    CalReleased.SelectedDate = dtUserAccessDetails.Rows(0).Item("dtReleased")
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("fltEmpRating")) Then
                    txtEmployeeRating.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("fltEmpRating"))
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("dtEmpRating")) Then
                    calEmpRating.SelectedDate = dtUserAccessDetails.Rows(0).Item("dtEmpRating")
                End If

                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("monNetPay")) Then
                    txtNetPay.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("monNetPay"))
                End If
                If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("monAmtWithheld")) Then
                    txtAmtWithheld.Text = String.Format("{0:#,##0.00}", dtUserAccessDetails.Rows(0).Item("monAmtWithheld"))
                End If

                lblTotalPay.Text = String.Format("{0:#,##0.00}", IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("monNetPay")), 0, dtUserAccessDetails.Rows(0).Item("monNetPay")) + IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("monAmtWithheld")), 0, dtUserAccessDetails.Rows(0).Item("monAmtWithheld")))

                'lblRoles.Text = dtUserAccessDetails.Rows(0).Item("Roles")
                txtEmergencyConInfo.Text = dtUserAccessDetails.Rows(0).Item("vcEmergencyContact")
                txtEmployeeId.Text = dtUserAccessDetails.Rows(0).Item("vcEmployeeId")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim ObjPayrollExpenses As New PayrollExpenses
            Dim strStartTime As String
            ObjPayrollExpenses.UserId = lngUserID
            ObjPayrollExpenses.DomainID = Session("DomainID")
            ObjPayrollExpenses.UserCntID = Session("UserContactID")
          
            ObjPayrollExpenses.bitHourlyRate = 1
            ObjPayrollExpenses.HourlyRate = IIf(txtHourlyRate.Text.Trim = "", 0, txtHourlyRate.Text.Trim)

            If chkSalary.Checked = True Then
                ObjPayrollExpenses.bitSalary = 1
                ObjPayrollExpenses.DailyHours = IIf(txtDailyHours.Text.Trim = "", 0, txtDailyHours.Text.Trim)

                strStartTime = Now.Date & " " & ddltime.SelectedItem.Text.Trim & ":00"

                Dim strSplitStartTimeDate As Date
                If chkPM.Checked = True Then
                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddHours(12)        'Convert the Start Date and Time to UTC
                Else : strSplitStartTimeDate = CType(strStartTime, DateTime)
                End If
                ObjPayrollExpenses.dtSalaryDateTime = strSplitStartTimeDate
            End If

            If chkPaidLeave.Checked = True Then
                ObjPayrollExpenses.bitPaidLeave = 1
                ObjPayrollExpenses.PaidLeaveHrs = IIf(txtPaidLeaveHrs.Text.Trim = "", 0, txtPaidLeaveHrs.Text.Trim)
                '' ObjPayrollExpenses.PaidLeaveEmpHrs = IIf(txtPaidLeaveBalance.Text.Trim = "", 0, txtPaidLeaveBalance.Text.Trim)
            End If

            If chkOverTime.Checked = True Then
                ObjPayrollExpenses.bitOverTime = 1
                If rdbOverTimeDaily.Checked = True Then
                    ObjPayrollExpenses.bitOverTimeHrsDailyOrWeekly = True
                    ObjPayrollExpenses.LimDailyHrs = IIf(txtLimDailHrs.Text.Trim = "", 0, txtLimDailHrs.Text.Trim)
                    ObjPayrollExpenses.OverTimeRate = IIf(txtOverTimeRate.Text.Trim = "", 0, txtOverTimeRate.Text.Trim)
               
                ElseIf rdbOverTimeWeekly.Checked = True Then
                    ObjPayrollExpenses.bitOverTimeHrsDailyOrWeekly = False
                    ''ObjPayrollExpenses.LimWeeklyHrs = IIf(txtLimWeeklyHrs.Text.Trim = "", 0, txtLimWeeklyHrs.Text.Trim)
                    ''ObjPayrollExpenses.OverTimeWeeklyRate = IIf(txtOverTimeWeeklyRate.Text.Trim = "", 0, txtOverTimeWeeklyRate.Text.Trim)
                    ObjPayrollExpenses.LimDailyHrs = IIf(txtLimWeeklyHrs.Text.Trim = "", 0, txtLimWeeklyHrs.Text.Trim)
                    ObjPayrollExpenses.OverTimeRate = IIf(txtOverTimeWeeklyRate.Text.Trim = "", 0, txtOverTimeWeeklyRate.Text.Trim)
                End If
            Else
                ObjPayrollExpenses.bitOverTime = 0
                ObjPayrollExpenses.bitOverTimeHrsDailyOrWeekly = False
                ObjPayrollExpenses.LimDailyHrs = 0
                ObjPayrollExpenses.OverTimeRate = 0

            End If
            'If chkManagerOfOwner.Checked = True Then
            '    ObjPayrollExpenses.bitManagerOfOwner = 1
            '    ObjPayrollExpenses.ManagerOfOwner = IIf(txtManagerOfOwner.Text.Trim = "", 0, txtManagerOfOwner.Text.Trim)
            'End If

            'If chkOwnerCommission.Checked = True Then
            '    ObjPayrollExpenses.bitCommOwner = 1
            '    ObjPayrollExpenses.CommOwner = IIf(txtOwnerCommission.Text.Trim = "", 0, txtOwnerCommission.Text.Trim)
            'End If

            'If chkAssigneeCommission.Checked = True Then
            '    ObjPayrollExpenses.bitCommAssignee = 1
            '    ObjPayrollExpenses.CommAssignee = IIf(txtAssigneeCommission.Text.Trim = "", 0, txtAssigneeCommission.Text.Trim)
            'End If

            ''If chkMainComm.Checked = True Then
            ''    ObjPayrollExpenses.bitMainComm = 1
            ''    ObjPayrollExpenses.MainCommPer = IIf(txtMainCommPer.Text.Trim = "", 0, txtMainCommPer.Text.Trim)
            ''End If
            'If chkRoleCommission.Checked = True Then ObjPayrollExpenses.bitRoleComm = 1
            If chkEmpCOGS.Checked = True Then ObjPayrollExpenses.bitEmpNetAccount = True
            If chkPayroll.Checked = True Then ObjPayrollExpenses.bitPayroll = True
            If CalHired.SelectedDate <> "" Then ObjPayrollExpenses.dtHired = CalHired.SelectedDate
            If CalReleased.SelectedDate <> "" Then ObjPayrollExpenses.dtReleased = CalReleased.SelectedDate

            ObjPayrollExpenses.EmployeeId = txtEmployeeId.Text.Trim
            ObjPayrollExpenses.EmergencyContactInfo = txtEmergencyConInfo.Text.Trim

            ObjPayrollExpenses.EmpRating = IIf(txtEmployeeRating.Text = "", 0, txtEmployeeRating.Text)
            If calEmpRating.SelectedDate <> "" Then ObjPayrollExpenses.EmpRatingDate = calEmpRating.SelectedDate

            ObjPayrollExpenses.NetPay = IIf(txtNetPay.Text = "", 0, txtNetPay.Text)
            ObjPayrollExpenses.AmtWithHeld = IIf(txtAmtWithheld.Text = "", 0, txtAmtWithheld.Text)
            ObjPayrollExpenses.UpdateUserAccessDetailsForPE()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseAndReload", "CloseAndReploadParent();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class