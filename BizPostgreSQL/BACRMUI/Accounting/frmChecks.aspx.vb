' ''Created By Siva
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Admin
'Imports BACRM.BusinessLogic.Common
'Partial Public Class frmChecks
'    Inherits BACRMPage

'#Region "Variables"
'    Dim mintJournalId As Integer
'    Dim mintCheckId As Integer
'    Dim mintChartAcntId As Integer

'    Dim dtChartAcnt As DataTable
'    Dim dtItems As New DataTable
'    Dim lobjChecks As Checks
'#End Region

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks

'            Dim j As Integer
'            Dim drRow As DataRow
'            Dim lintRowcount As Integer
'            txtAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
'            btnSave.Attributes.Add("onclick", "return Save()")
'            btnRecalculate.Attributes.Add("onclick", "return Recalculate()")
'            btnAddtoList.Attributes.Add("onclick", "return AddList()")
'            mintCheckId = CCommon.ToInteger(GetQueryStringVal("CheckId"))
'            mintJournalId = CCommon.ToInteger(GetQueryStringVal("JournalId"))
'            mintChartAcntId = CCommon.ToInteger(GetQueryStringVal("ChartAcntId"))
'            txtRouting.Attributes.Add("onkeypress", "CheckNumber(1,event)")
'            txtCheckNo.Attributes.Add("onkeypress", "CheckNumber(1,event)")
'            txtBankAcc.Attributes.Add("onkeypress", "CheckNumber(1,event)")

'            imgCompanyLogo.Attributes.Add("onclick", "return OpenLogoPage('CheckCompany')")
'            imgBankLogo.Attributes.Add("onclick", "return OpenLogoPage('CheckBank')")

'            If Not IsPostBack Then

'                GetUserRightsForPage(35, 80)
'                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
'                    btnSave.Visible = False
'                End If

'                Dim lobjUserAccess As New UserAccess
'                lobjUserAccess.DomainID = Session("DomainId")
'                lobjUserAccess.GetImagePath()
'                imgCompanyLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & lobjUserAccess.CompanyImagePath
'                imgBankLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & lobjUserAccess.BankImagePath

'                If mintCheckId <> 0 Then
'                    lobjChecks.DomainID = Session("DomainID")
'                    lobjChecks.CheckId = mintCheckId
'                    mintJournalId = lobjChecks.GetJournalId()
'                    lobjChecks.JournalId = mintJournalId
'                    dtItems = lobjChecks.GetCheckJournalDetails()
'                End If
'                calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
'                LoadChartType(ddlBankAcnt)
'                'LoadRecurringTemplate()
'                LoadChartAcnt()
'                LoadBankDetails()
'                If dtItems.Rows.Count = 0 And dtItems.Columns.Count = 0 Then
'                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

'                    LoadDefaultColumns()
'                    For j = 0 To 1
'                        drRow = dtItems.NewRow
'                        dtItems.Rows.Add(drRow)
'                    Next

'                    dgJournalEntry.DataSource = dtItems
'                    dgJournalEntry.DataBind()

'                    BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value))
'                Else
'                    LoadSavedInformation()

'                    If dtItems.Rows.Count < 2 Then
'                        lintRowcount = 2 - dtItems.Rows.Count
'                        For j = 0 To lintRowcount - 1
'                            drRow = dtItems.NewRow
'                            dtItems.Rows.Add(drRow)
'                        Next

'                    End If
'                    dgJournalEntry.DataSource = dtItems
'                    dgJournalEntry.DataBind()
'                End If

'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try

'    End Sub

'    Sub LoadDefaultColumns()
'        Try
'            dtItems.Columns.Add("TransactionId") '' Primary key
'            dtItems.Columns.Add("numJournalId")
'            dtItems.Columns.Add("numDebitAmt")
'            dtItems.Columns.Add("numCreditAmt")
'            dtItems.Columns.Add("numChartAcntId")
'            dtItems.Columns.Add("varDescription")
'            dtItems.Columns.Add("numCustomerId")
'            dtItems.Columns.Add("numDomainId")
'            dtItems.Columns.Add("bitMainCheck")
'            dtItems.Columns.Add("bitReconcile")
'            dtItems.Columns.Add("varRelation")
'            dtItems.Columns.Add("numAcntType")
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartAcnt()
'        Try
'            'Dim objJournalEntry As New JournalEntry
'            'objJournalEntry.ParentRootNode = 1
'            'objJournalEntry.DomainId = Session("DomainId")
'            'dtChartAcnt = objJournalEntry.GetChartOfAccountsDetails()
'            Dim objCOA As New ChartOfAccounting
'            objCOA.DomainId = Session("DomainId")
'            objCOA.AccountCode = ""
'            dtChartAcnt = objCOA.GetParentCategory()
'            'Commented by chintan, reason: doesn't show against which AP account journal entry was made. so will show all entry but disable re saving it.
'            'Remove AR and AP values ..As those will create problem in AR aging summary report, So better not to allow to create journal entry against it
'            'Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")

'            'For index As Integer = 0 To dr.Length - 1
'            '    dtChartAcnt.Rows.Remove(dr(index))
'            'Next
'            'dtChartAcnt.AcceptChanges()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    'Private Sub LoadRecurringTemplate()
'    '    Try
'    '        If lobjChecks Is Nothing Then lobjChecks = New Checks
'    '        lobjChecks.DomainId = Session("DomainID")
'    '        ddlMakeRecurring.DataSource = lobjChecks.GetRecurringDetails
'    '        ddlMakeRecurring.DataTextField = "varRecurringTemplateName"
'    '        ddlMakeRecurring.DataValueField = "numRecurringId"
'    '        ddlMakeRecurring.DataBind()
'    '        ddlMakeRecurring.Items.Insert(0, New ListItem("--Select One --", "0"))
'    '    Catch ex As Exception
'    '        Throw ex
'    '    End Try
'    'End Sub

'    Sub LoadBankDetails()

'        Dim dtBankDet As DataTable
'        Dim lintCheckNo As Integer
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            lobjChecks.DomainId = Session("DomainID")
'            dtBankDet = lobjChecks.GetCheckNumber()
'            If dtBankDet.Rows.Count > 0 Then
'                txtRouting.Text = IIf(IsDBNull(dtBankDet.Rows(0).Item("numBankRoutingNo")), "", dtBankDet.Rows(0).Item("numBankRoutingNo"))
'                txtBankAcc.Text = IIf(IsDBNull(dtBankDet.Rows(0).Item("numBankAccountNo")), "", dtBankDet.Rows(0).Item("numBankAccountNo"))
'                lintCheckNo = dtBankDet.Rows(0).Item("numCheckNo")
'                txtCheckNo.Text = IIf(lintCheckNo = 0, 1, lintCheckNo)
'                CheckNo.Text = txtCheckNo.Text
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadSavedInformation()
'        Dim dtCheckDetails As DataTable
'        Dim lstrColor As String
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            lobjChecks.CheckId = mintCheckId
'            lobjChecks.DomainId = Session("DomainID")
'            dtCheckDetails = lobjChecks.GetCheckDetails()
'            If Not IsDBNull(dtCheckDetails.Rows(0).Item("datCheckDate")) Then
'                calFrom.SelectedDate = dtCheckDetails.Rows(0).Item("datCheckDate")
'            End If

'            If Not IsDBNull(dtCheckDetails.Rows(0).Item("numAcntTypeId")) Then
'                If Not ddlBankAcnt.Items.FindByValue(dtCheckDetails.Rows(0).Item("numAcntTypeId")) Is Nothing Then
'                    ddlBankAcnt.Items.FindByValue(dtCheckDetails.Rows(0).Item("numAcntTypeId")).Selected = True
'                End If
'            End If

'            'If Not IsDBNull(dtCheckDetails.Rows(0).Item("numRecurringId")) Then
'            '    If dtCheckDetails.Rows(0).Item("numRecurringId") <> 0 Then
'            '        If Not ddlMakeRecurring.Items.FindByValue(dtCheckDetails.Rows(0).Item("numRecurringId")) Is Nothing Then
'            '            ddlMakeRecurring.Items.FindByValue(dtCheckDetails.Rows(0).Item("numRecurringId")).Selected = True
'            '            If Not IsDBNull(dtCheckDetails.Rows(0).Item("LastRecurringDate")) Then
'            '                ddlMakeRecurring.Enabled = False
'            '            End If
'            '        End If
'            '    End If
'            'End If

'            txtMemo.Text = dtCheckDetails.Rows(0).Item("varMemo")
'            txtAmount.Text = ReturnMoney(dtCheckDetails.Rows(0).Item("monCheckAmt"))
'            TotalAmt.Value = dtCheckDetails.Rows(0).Item("monCheckAmt")

'            If Not IsDBNull(dtCheckDetails.Rows(0).Item("vcCompanyName")) Then
'                lblCompany.Text = dtCheckDetails.Rows(0).Item("vcCompanyName")
'            End If

'            If Not IsDBNull(dtCheckDetails.Rows(0).Item("numDivisionID")) Then
'                CustomerId.Value = dtCheckDetails.Rows(0).Item("numDivisionID")
'            Else : CustomerId.Value = 0
'            End If

'            CheckNo.Text = dtCheckDetails.Rows(0).Item("numCheckNo")
'            txtCheckNo.Text = dtCheckDetails.Rows(0).Item("numCheckNo")
'            txtRouting.Text = dtCheckDetails.Rows(0).Item("numBankRoutingNo")
'            txtBankAcc.Text = dtCheckDetails.Rows(0).Item("numBankAccountNo")
'            lobjChecks.CheckId = mintCheckId

'            ViewState("TransactionId") = lobjChecks.GetMainCheckDetailsId()

'            'Convert NumericValues To Word
'            Dim lNumericToWord As New NumericToWord
'            lblMoneyDescription.Text = lNumericToWord.SpellNumber(Replace(txtAmount.Text, ",", ""))

'            lobjChecks.AccountId = dtCheckDetails.Rows(0).Item("numAcntTypeId")
'            lobjChecks.DomainId = Session("DomainId")
'            lblBalance.Text = "Balance: " & Session("Currency")
'            Dim decOpeningBal As Decimal
'            decOpeningBal = lobjChecks.GetCheckOpeningBalance()
'            lstrColor = "<font color=red>" & ReturnMoney(decOpeningBal) & "</font>"
'            lblOpeningBalance.Text = IIf(decOpeningBal < 0, lstrColor, ReturnMoney(decOpeningBal))
'            btnPrint.Visible = True

'            If Not IsDBNull(dtCheckDetails.Rows(0).Item("numCheckCompanyId")) Then
'                CheckCompanyId.Value = dtCheckDetails.Rows(0).Item("numCheckCompanyId")
'            Else : CheckCompanyId.Value = 0
'            End If

'            BindCompanyList(CheckCompanyId.Value)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadItem(ByVal ddlCompanyDet As DropDownList, ByVal p_WhereCondition As String)
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            lobjChecks.DomainId = Session("DomainID")
'            lobjChecks.strWhereCondition = p_WhereCondition
'            ddlCompanyDet.DataSource = lobjChecks.GetCompanyDetails
'            ddlCompanyDet.DataTextField = "vcCompanyName"
'            ddlCompanyDet.DataValueField = "numDivisionID"
'            ddlCompanyDet.DataBind()
'            ddlCompanyDet.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartType(ByVal ddlBankAccounts As DropDownList)
'        Try
'            '01010101 bank accounts
'            If dtChartAcnt Is Nothing Then
'                Dim objCOA As New ChartOfAccounting
'                objCOA.DomainId = Session("DomainId")
'                objCOA.AccountCode = "01010101"
'                dtChartAcnt = objCOA.GetParentCategory()
'            End If

'            Dim item As ListItem
'            For Each dr As DataRow In dtChartAcnt.Rows
'                item = New ListItem()
'                item.Text = dr("vcAccountName")
'                item.Value = dr("numAccountID")
'                item.Attributes("OptionGroup") = dr("vcAccountType")
'                ddlBankAccounts.Items.Add(item)
'            Next
'            ddlBankAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Dim i As Integer
'        Dim dtgriditem As DataGridItem
'        Dim dtrow As DataRow
'        Try
'            LoadDefaultColumns()
'            For i = 0 To dgJournalEntry.Items.Count - 1
'                dtrow = dtItems.NewRow
'                dtgriditem = dgJournalEntry.Items(i)
'                dtrow.Item("numJournalId") = CType(dtgriditem.FindControl("lblJournalId"), Label).Text
'                dtrow.Item("TransactionId") = CType(dtgriditem.FindControl("lblTransactionId"), Label).Text
'                dtrow.Item("numDebitAmt") = CType(dtgriditem.FindControl("txtDebit"), TextBox).Text ''IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, CType(dtgriditem.FindControl("txtDebit"), TextBox).Text)
'                dtrow.Item("numChartAcntId") = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                dtrow.Item("varDescription") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                dtrow.Item("numCustomerId") = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                dtrow.Item("varRelation") = CType(dtgriditem.FindControl("lblRelationship"), Label).Text
'                dtrow.Item("numAcntType") = CType(dtgriditem.FindControl("txtAccountTypeId"), TextBox).Text
'                dtItems.Rows.Add(dtrow)
'            Next

'            For i = 0 To 1
'                dtrow = dtItems.NewRow
'                dtItems.Rows.Add(dtrow)
'            Next
'            LoadChartAcnt()
'            dgJournalEntry.DataSource = dtItems
'            dgJournalEntry.DataBind()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try

'    End Sub

'    Private Sub btnWords_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWords.Click
'        Try
'            Dim lNumericToWord As New NumericToWord
'            lblMoneyDescription.Text = lNumericToWord.SpellNumber(Replace(txtAmount.Text, ",", ""))
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgJournalEntry.ItemCommand
'        Try
'            If e.CommandName = "Go" Then
'                Dim ddlName As DropDownList
'                Dim txtCompanyName As TextBox
'                ddlName = CType(e.Item.FindControl("ddlName"), DropDownList)
'                txtCompanyName = CType(e.Item.FindControl("txtCompanyName"), TextBox)
'                If Not IsNothing(ddlName) Then
'                    '' If txtCompanyName.Text <> "" Then
'                    LoadItem(ddlName, txtCompanyName.Text)
'                    ''End If
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try

'    End Sub

'    Private Sub dgJournalEntry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgJournalEntry.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim ddl As DropDownList
'                Dim ddlAccounts As DropDownList
'                Dim lblAccounts As Label
'                Dim lblDebitAmount As Label
'                Dim lblCreditAmount As Label
'                Dim lblMemo As Label
'                Dim lblName As Label
'                Dim txtDebitAmount As TextBox
'                Dim txtMemo As TextBox
'                Dim lblTransactionDetailsId As Label

'                ddl = CType(e.Item.FindControl("ddlName"), DropDownList)
'                txtDebitAmount = CType(e.Item.FindControl("txtDebit"), TextBox)
'                txtMemo = CType(e.Item.FindControl("txtMemo"), TextBox)
'                txtDebitAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")

'                If Not IsNothing(ddl) Then
'                    lblDebitAmount = CType(e.Item.FindControl("lblDebitAmt"), Label)
'                    lblCreditAmount = CType(e.Item.FindControl("lblCreditAmt"), Label)

'                    lblMemo = CType(e.Item.FindControl("lblMemo"), Label)
'                    lblName = CType(e.Item.FindControl("lblName"), Label)
'                    LoadCompanyDropDown(ddl, IIf(lblName.Text = "", 0, lblName.Text))

'                    lblTransactionDetailsId = CType(e.Item.FindControl("lblTransactionId"), Label)
'                    If lblTransactionDetailsId.Text <> "" Then
'                        CType(e.Item.FindControl("lblTransactionId"), Label).Text = lblTransactionDetailsId.Text
'                    End If
'                    If lblDebitAmount.Text <> "" AndAlso lblDebitAmount.Text <> 0 Then
'                        txtDebitAmount.Text = lblDebitAmount.Text
'                    End If
'                    If lblCreditAmount.Text <> "" AndAlso lblCreditAmount.Text <> 0 Then
'                        txtDebitAmount.Text = lblCreditAmount.Text
'                    End If

'                    If lblMemo.Text <> "" Then txtMemo.Text = lblMemo.Text
'                    If Not ddl.Items.FindByValue(lblName.Text) Is Nothing Then
'                        ddl.Items.FindByValue(lblName.Text).Selected = True
'                    End If
'                End If

'                ddlAccounts = CType(e.Item.FindControl("ddlAccounts"), DropDownList)
'                If Not IsNothing(ddlAccounts) Then
'                    LoadChartType(ddlAccounts)
'                    lblAccounts = CType(e.Item.FindControl("lblAccountID"), Label)
'                    If Not ddlAccounts.Items.FindByValue(lblAccounts.Text) Is Nothing Then
'                        ddlAccounts.Items.FindByValue(lblAccounts.Text).Selected = True
'                    End If
'                End If
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    'Sub LoadChartType(ByVal ddlAccounts As DropDownList)
'    '    Try
'    '        'ddlAccounts.DataSource = dtChartAcnt
'    '        'ddlAccounts.DataTextField = "vcCategoryName"
'    '        'ddlAccounts.DataValueField = "numChartOfAcntID"
'    '        'ddlAccounts.DataBind()
'    '        'ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))

'    '    Catch ex As Exception
'    '        Throw ex
'    '    End Try

'    'End Sub

'    Sub LoadCompanyDropDown(ByVal ddlitems As DropDownList, ByVal p_DivisionId As Integer)
'        Try
'            If p_DivisionId <> 0 Then
'                Dim objJournalEntry As New JournalEntry
'                objJournalEntry.DomainId = Session("DomainID")
'                objJournalEntry.DivisionId = p_DivisionId
'                ddlitems.DataSource = objJournalEntry.GetCompanyDetForDivisionId()
'                ddlitems.DataTextField = "vcCompanyName"
'                ddlitems.DataValueField = "numDivisionID"
'                ddlitems.DataBind()
'                ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'            Else : ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            Dim JournalId As Integer
'            Dim CheckId As Integer
'            If CheckDuplicate() = True Then Exit Sub
'            CheckId = SaveDataToCheckHeader()
'            JournalId = SaveDataToHeader(CheckId)
'            SaveDataToGeneralJournalDetails(JournalId)
'            PageRedirect()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Function SaveDataToCheckHeader() As Integer
'        Dim lntCheckId As Integer
'        Try
'            Dim lobjJournalEntry As New JournalEntry

'            With lobjJournalEntry
'                .Entry_Date = calFrom.SelectedDate '' DateFromFormattedDate(txtFromDateDisplay.Text, Session("DateFormat"))
'                .Amount = Replace(txtAmount.Text, ",", "")
'                .DomainId = Session("DomainId")
'                .CustomerId = CustomerId.Value 'ddlCompany.SelectedItem.Value
'                .Memo = txtMemo.Text
'                .AcntType = ddlBankAcnt.SelectedItem.Value
'                '.RecurringId = ddlMakeRecurring.SelectedItem.Value
'                .CheckNo = txtCheckNo.Text.Trim
'                .BankRoutingNo = txtRouting.Text.Trim
'                .BankAccountNo = txtBankAcc.Text.Trim
'                .CheckCompanyId = CheckCompanyId.Value
'                If Not IsNothing(mintCheckId) And mintCheckId <> 0 Then
'                    .CheckId = mintCheckId
'                    .SaveDataToCheckDetailsEdit()
'                    lntCheckId = mintCheckId
'                Else
'                    .UserCntID = Session("UserContactID")
'                    .CheckId = 0
'                    lntCheckId = .SaveDataToCheckDetails()
'                End If
'                Return lntCheckId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Function CheckDuplicate()
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            lobjChecks.DomainId = Session("DomainId")
'            lobjChecks.CheckId = mintCheckId
'            lobjChecks.CheckNo = txtCheckNo.Text.Trim
'            If lobjChecks.CheckDuplicateCheckNo() > 0 Then
'                litMessage.Text = "Duplicate Check No"
'                Return True
'            Else : Return False
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Function SaveDataToHeader(ByVal p_CheckId As Integer) As Integer
'        Dim lntJournalId As Integer
'        Try
'            Dim lobjChecks As New JournalEntry

'            With lobjChecks
'                .Entry_Date = CDate(calFrom.SelectedDate & " 12:00:00") ''DateFromFormattedDate(txtFromDateDisplay.Text, Session("DateFormat"))
'                .Amount = Replace(txtAmount.Text, ",", "")
'                .DomainId = Session("DomainId")
'                .ChartAcntId = ddlBankAcnt.SelectedItem.Value
'                .RecurringId = 0
'                .UserCntID = Session("UserContactID")
'                .JournalId = mintJournalId
'                .CheckId = p_CheckId
'                lntJournalId = .SaveDataToJournalEntryHeader()
'                Return lntJournalId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub SaveDataToGeneralJournalDetails(ByVal lintJournalId As Integer)
'        Dim i As Integer
'        Dim lstr As String
'        Dim ds As New DataSet

'        Dim ddlAccounts As DropDownList
'        Dim lintAccountId() As String
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            LoadDefaultColumns()
'            Dim dtgriditem As DataGridItem
'            Dim dtrow As DataRow

'            For i = 0 To dgJournalEntry.Items.Count - 1
'                dtrow = dtItems.NewRow
'                dtgriditem = dgJournalEntry.Items(i)
'                If CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString <> "0" And CType(dtgriditem.FindControl("txtDebit"), TextBox).Text <> "" Then ''And CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value.ToString <> "0" Then
'                    dtrow.Item("TransactionId") = IIf(CType(dtgriditem.FindControl("lblTransactionId"), Label).Text = "", 0, CType(dtgriditem.FindControl("lblTransactionId"), Label).Text)
'                    dtrow.Item("numJournalId") = lintJournalId
'                    dtrow.Item("numDebitAmt") = IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text, ",", ""))
'                    dtrow.Item("numCreditAmt") = 0
'                    ddlAccounts = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList)
'                    lintAccountId = ddlAccounts.SelectedItem.Value.Split("~")
'                    dtrow.Item("numChartAcntId") = lintAccountId(0) ''CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                    dtrow.Item("varDescription") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                    dtrow.Item("numCustomerId") = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                    dtrow.Item("numDomainId") = Session("DomainId")
'                    dtrow.Item("bitMainCheck") = 0
'                    dtrow.Item("bitReconcile") = 0

'                    lobjChecks.DomainId = Session("DomainId")
'                    dtItems.Rows.Add(dtrow)
'                End If
'            Next

'            'To Add the Credit Amt in JournalDetails table [i.e] For Bank Account Selected

'            dtrow = dtItems.NewRow
'            dtrow.Item("TransactionId") = IIf(ViewState("TransactionId") = Nothing, 0, ViewState("TransactionId"))
'            dtrow.Item("numJournalId") = lintJournalId
'            dtrow.Item("numDebitAmt") = 0
'            dtrow.Item("numCreditAmt") = Replace(txtAmount.Text, ",", "")
'            dtrow.Item("numChartAcntId") = ddlBankAcnt.SelectedItem.Value
'            dtrow.Item("varDescription") = txtMemo.Text
'            dtrow.Item("numCustomerId") = CustomerId.Value ''ddlCompany.SelectedItem.Value
'            dtrow.Item("numDomainId") = Session("DomainId")
'            dtrow.Item("bitMainCheck") = 1
'            dtrow.Item("bitReconcile") = 0

'            dtItems.Rows.Add(dtrow)

'            ds.Tables.Add(dtItems)
'            lstr = ds.GetXml()
'            lobjChecks.JournalId = lintJournalId

'            If Not IsNothing(mintCheckId) And mintCheckId <> 0 Then
'                lobjChecks.Mode = 1
'            Else : lobjChecks.Mode = 0
'            End If

'            lobjChecks.JournalDetails = lstr
'            lobjChecks.RecurringMode = 0
'            lobjChecks.SaveDataToJournalDetailsForChecks()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub ddlBankAcnt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankAcnt.SelectedIndexChanged
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            Dim lstrColor As String
'            Dim ldecOpeningBalance As Decimal
'            If ddlBankAcnt.SelectedItem.Value <> 0 Then
'                lobjChecks.AccountId = ddlBankAcnt.SelectedItem.Value
'                lobjChecks.DomainId = Session("DomainId")
'                lblBalance.Text = "Balance: " & Session("Currency")
'                ldecOpeningBalance = lobjChecks.GetCheckOpeningBalance()
'                lstrColor = "<font color=red>" & ReturnMoney(ldecOpeningBalance) & "</font>"
'                lblOpeningBalance.Text = IIf(ldecOpeningBalance < 0, lstrColor, ReturnMoney(ldecOpeningBalance))

'            Else
'                lblOpeningBalance.Text = ""
'                lblBalance.Text = ""
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Function ReturnMoney(ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
'        'Try
'        If mintCheckId <> 0 Then Server.Transfer("../Accounting/frmCheckPrint.aspx?CheckId=" & mintCheckId)
'        'Catch ex As Exception
'        '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'        '    Response.Write(ex)
'        'End Try
'    End Sub

'    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
'        Try
'            PageRedirect()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub PageRedirect()
'        Try
'            If GetQueryStringVal( "frm") = "ChartAcnt" Then
'                Response.Redirect("../Accounting/frmChartofAccounts.aspx")
'            ElseIf GetQueryStringVal( "frm") = "BankRegister" Then
'                Response.Redirect("../Accounting/frmJournalEntry.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal( "frm") = "RecurringTransaction" Then
'                Response.Redirect("../Accounting/frmRecurringTransaction.aspx")
'            ElseIf GetQueryStringVal( "frm") = "GeneralLedger" Then
'                Response.Redirect("../Accounting/frmGeneralLedger.aspx")
'            ElseIf GetQueryStringVal( "frm") = "Transaction" Then
'                Response.Redirect("../Accounting/frmTransactions.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal( "frm") = "QuickReport" Then
'                Response.Redirect("../Accounting/frmQuickAccountReport.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal("frm") = "BankRecon" Then
'                Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & CCommon.ToLong(GetQueryStringVal("ReconcileID")))
'            Else
'                Response.Redirect("../Accounting/frmChartofAccounts.aspx")
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub btnCompanyName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCompanyName.Click
'        Try
'            LoadItem(ddlCompanyName, txtCompanyName.Text.Trim)
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub btnAddtoList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoList.Click
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            With lobjChecks
'                .CustomerId = ddlCompanyName.SelectedItem.Value
'                .DomainId = Session("DomainId")
'                .UserCntID = Session("UserContactID")
'                .SaveCheckCompanyDetails()
'            End With
'            BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value))
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub BindCompanyList(ByVal p_CheckCompanyId As Long, Optional ByVal p_byteMode As Int16 = 0)
'        Try
'            If lobjChecks Is Nothing Then lobjChecks = New Checks
'            lobjChecks.DomainId = Session("DomainId")
'            lobjChecks.CheckId = mintCheckId
'            lobjChecks.CheckCompanyId = p_CheckCompanyId
'            lobjChecks.byteMode = p_byteMode
'            dgCompanyList.DataSource = lobjChecks.GetCompanyCheckDetails
'            dgCompanyList.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub dgCompanyList_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCompanyList.EditCommand
'        Try
'            'Dim dtCompanylist As New DataTable
'            ' '' dtCompanylist = ViewState("CompanyList")
'            dgCompanyList.EditItemIndex = e.Item.ItemIndex
'            BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value))
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgCompanyList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCompanyList.ItemCommand
'        Try
'            If e.CommandName = "Cancel" Then
'                dgCompanyList.EditItemIndex = e.Item.ItemIndex
'                dgCompanyList.EditItemIndex = -1
'                BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value))
'            ElseIf e.CommandName = "Update" Then

'                Dim lblCompanyId As Label
'                If lobjChecks Is Nothing Then lobjChecks = New Checks
'                Dim ddlCompany As DropDownList
'                Dim txtEAmount As TextBox
'                Dim txtEMemo As TextBox
'                Dim lblCheckCompanyId As Label
'                lblCheckCompanyId = CType(e.Item.FindControl("lblCheckCompanyId"), Label)
'                ddlCompany = CType(e.Item.FindControl("ddlcompanyName1"), DropDownList)
'                txtEAmount = CType(e.Item.FindControl("txtEAmount"), TextBox)
'                txtEMemo = CType(e.Item.FindControl("txtEDesc"), TextBox)
'                If ddlCompany.SelectedItem.Value <> 0 Then
'                    lobjChecks.CheckCompanyId = lblCheckCompanyId.Text
'                    lobjChecks.CustomerId = ddlCompany.SelectedItem.Value
'                    lobjChecks.DomainId = Session("DomainId")
'                    lobjChecks.UserCntID = Session("UserContactID")
'                    lobjChecks.Amount = txtEAmount.Text
'                    lobjChecks.Memo = txtEMemo.Text
'                    lobjChecks.SaveCheckCompanyDetails()
'                    dgCompanyList.EditItemIndex = -1
'                    BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value))
'                Else : litMessage.Text = "Select the Company"
'                End If

'            ElseIf e.CommandName = "Go" Then
'                Dim ddlCompany As DropDownList
'                Dim txtCompanyName As TextBox
'                ddlCompany = CType(e.Item.FindControl("ddlCompanyName1"), DropDownList)
'                txtCompanyName = CType(e.Item.FindControl("txtCompanyName1"), TextBox)
'                If Not IsNothing(ddlCompany) Then
'                    '' If txtCompanyName.Text <> "" Then
'                    LoadItem(ddlCompany, txtCompanyName.Text)
'                    ''End If
'                End If
'            ElseIf e.CommandName = "AddToCheck" Then
'                Dim lblCompanyId As Label
'                Dim lblCompanyName As Label
'                Dim lblAmount As Label
'                Dim lblMemo As Label
'                Dim lblCheckCompanyId As Label
'                If lobjChecks Is Nothing Then lobjChecks = New Checks

'                lblCompanyId = CType(e.Item.FindControl("lblCompanyId"), Label)
'                lblCompanyName = CType(e.Item.FindControl("lblCompanyName"), Label)
'                lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
'                lblMemo = CType(e.Item.FindControl("lblMemo"), Label)
'                lblCheckCompanyId = CType(e.Item.FindControl("lblCheckCompanyId"), Label)
'                CustomerId.Value = lblCompanyId.Text

'                lblCompany.Text = lblCompanyName.Text
'                txtAmount.Text = ReturnMoney(lblAmount.Text)

'                txtMemo.Text = lblMemo.Text
'                CheckCompanyId.Value = lblCheckCompanyId.Text

'                Dim lNumericToWord As New NumericToWord
'                lblMoneyDescription.Text = lNumericToWord.SpellNumber(Replace(txtAmount.Text, ",", ""))
'                BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value), 1)
'            ElseIf e.CommandName = "Delete" Then
'                If lobjChecks Is Nothing Then lobjChecks = New Checks

'                Dim lblCheckCompanyId As Label
'                lblCheckCompanyId = CType(e.Item.FindControl("lblCheckCompanyId"), Label)
'                lobjChecks.CheckCompanyId = lblCheckCompanyId.Text
'                lobjChecks.DomainId = Session("DomainId")
'                If lobjChecks.DeleteCheckCompanyDetails() = False Then
'                    litMessage.Text = "Dependent Records Exists. Cannot Be deleted"
'                Else : BindCompanyList(IIf(CheckCompanyId.Value = "", 0, CheckCompanyId.Value))
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgCompanyList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCompanyList.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.EditItem Then
'                Dim ddlCompanyName1 As DropDownList
'                Dim txtEAmount As TextBox
'                ddlCompanyName1 = e.Item.FindControl("ddlCompanyName1")
'                ddlCompanyName1.CssClass = "signup"
'                ddlCompanyName1.Width = Unit.Pixel(150)
'                txtEAmount = e.Item.FindControl("txtEAmount")
'                txtEAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
'                If CType(e.Item.FindControl("lblCompanyId"), Label).Text <> "" Then
'                    LoadCompanyDropDown(ddlCompanyName1, CType(e.Item.FindControl("lblCompanyId"), Label).Text)
'                End If
'                If CType(e.Item.FindControl("lblCompanyId"), Label).Text <> "" Then
'                    ddlCompanyName1.Items.FindByValue(CType(e.Item.FindControl("lblCompanyId"), Label).Text).Selected = True
'                End If
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'End Class