''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports System
'Imports System.Diagnostics
'Imports System.Drawing
'Imports ExpertPdf.HtmlToPdf
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Text
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmVendorPayment
    Inherits BACRMPage

    '#Region "Variables"

    Dim lngBillPaymentID As Long
    Dim strIds As String = ""
    Dim lngJournalID As Long
    Dim objBillPayment As BillPayment
    Dim intBillPayCount As Integer = 0
    Dim decTotalAmountPerOrganization As Decimal = 0
    Dim lngDivisionID As Long = 0

    '#End Region

    Private listTempSelectedBills As System.Collections.Generic.List(Of SelectedBill)
    Public Property UserSelectedBills As String
        Get
            Return CCommon.ToString(hdnSelectedBills.Value)
        End Get
        Set(value As String)
            hdnSelectedBills.Value = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            lngBillPaymentID = CCommon.ToLong(GetQueryStringVal("BillPaymentID"))
            'To Set Permission
            GetUserRightsForPage(35, 82)

            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
                btnExportCheck.Visible = False
            End If


            If Not IsPostBack Then
                PersistTable.Load(boolOnlyURL:=True)
                txtCurrrentPage.Text = CCommon.ToLong(PersistTable(PersistKey.CurrentPage))


                calPaymentDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                objCommon.sb_FillComboFromDBwithSel(ddlPaymentMethod, 31, Session("DomainID"))
                If ddlPaymentMethod.Items.Count > 0 AndAlso ddlPaymentMethod.Items.FindByValue("0") IsNot Nothing Then
                    ddlPaymentMethod.Items.Remove(ddlPaymentMethod.Items.FindByValue("0"))
                End If

                'As per Requirement defined by Carl, Need to select Check as default payment method.
                If ddlPaymentMethod.Items.Count > 0 AndAlso ddlPaymentMethod.Items.FindByValue("5") IsNot Nothing Then
                    ddlPaymentMethod.Items.FindByValue("5").Selected = True
                End If

                bindUserLevelClassTracking()
                BindPaymentFromDropdown()
                BindMultiCurrency()
                LoadBillPayment()
                BindBillsGrid("", "", False)
                btnSave.Attributes.Add("onclick", "javascript:return Save();")
                btnExportCheck.Attributes.Add("onclick", "javascript:return Save();")
                If lngBillPaymentID = 0 Then btnDelete.Visible = False

                If CCommon.ToInteger(GetQueryStringVal("flag")) = 2 AndAlso Session("Bills") IsNot Nothing Then
                    strIds = Session("Bills")
                    strIds = strIds.Trim(",")
                    strIds = strIds.Trim(" ")
                    If strIds.Length > 0 Then
                        ShowMessage(strIds & " Bill(s) is/are already paid. Other bills are paid successfully.")
                        Session("Bills") = Nothing
                    Else
                        ShowMessage("Bill(s) paid sucessfully")
                        Session("Bills") = Nothing
                    End If
                ElseIf CCommon.ToInteger(GetQueryStringVal("flag")) = 1 Then
                    ShowMessage("Bill payment deleted sucessfully")

                ElseIf CCommon.ToInteger(GetQueryStringVal("flag")) = 2 Then
                    ShowMessage("Bill(s) paid sucessfully")

                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub LoadBillPayment()
        Try
            If objBillPayment Is Nothing Then objBillPayment = New BillPayment
            objBillPayment.BillPaymentID = lngBillPaymentID
            objBillPayment.DomainID = Session("DomainId")
            objBillPayment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objBillPayment.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objBillPayment.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objBillPayment.SortColumn = ""
            objBillPayment.columnSortOrder = ""
            Dim ds As DataSet = objBillPayment.GetBillPaymentDetail()

            'Dim dtBills As DataTable = ds.Tables(1)
            Dim dtBillHeader As DataTable = ds.Tables(0)

            If lngBillPaymentID > 0 And dtBillHeader.Rows.Count > 0 Then
                If CCommon.ToLong(dtBillHeader.Rows(0).Item("numReturnHeaderID")) = 0 Then
                    hfCheckHeaderID.Value = CCommon.ToLong(dtBillHeader.Rows(0)("numCheckHeaderID").ToString)

                    For Each Item As ListItem In ddlPaymentFrom.Items
                        If Item.Value.StartsWith(dtBillHeader.Rows(0)("numAccountID").ToString) Then
                            ddlPaymentFrom.ClearSelection()
                            Item.Selected = True
                            ddlPaymentFrom_SelectedIndexChanged()
                            Exit For
                        End If
                    Next

                    If ddlPaymentMethod.Items.FindByValue(dtBillHeader.Rows(0)("numPaymentMethod").ToString) IsNot Nothing Then
                        ddlPaymentMethod.ClearSelection()
                        ddlPaymentMethod.Items.FindByValue(dtBillHeader.Rows(0)("numPaymentMethod").ToString).Selected = True
                    End If
                    calPaymentDate.SelectedDate = dtBillHeader.Rows(0)("dtPaymentDate").ToString
                    lblPaymentTotalAmount.Text = ReturnMoney(dtBillHeader.Rows(0)("monPaymentAmount")) 'ReturnMoney(dtBillHeader.Compute("sum(monAmountPaid)", ""))
                    hdnPaymentAmount.Value = ReturnMoney(dtBillHeader.Rows(0)("monPaymentAmount")) 'ReturnMoney(dtBillHeader.Compute("sum(monAmountPaid)", ""))

                    hfCheckNo.Value = CCommon.ToLong(dtBillHeader.Rows(0)("numCheckNo"))

                    If CCommon.ToLong(dtBillHeader.Rows(0)("numCheckNo")) > 0 Then
                        txtCheckNo.Text = dtBillHeader.Rows(0)("numCheckNo")
                        rbHandCheck.Checked = True
                        rbPrintedCheck.Checked = False
                    Else
                        txtCheckNo.Text = ""
                        rbHandCheck.Checked = False
                        rbPrintedCheck.Checked = True
                    End If

                    If Session("MultiCurrency") = True Then
                        If Not IsDBNull(dtBillHeader.Rows(0).Item("numCurrencyID")) Then
                            If Not ddlCurrency.Items.FindByValue(dtBillHeader.Rows(0).Item("numCurrencyID")) Is Nothing Then
                                ddlCurrency.ClearSelection()
                                ddlCurrency.Items.FindByValue(dtBillHeader.Rows(0).Item("numCurrencyID")).Selected = True
                                ddlCurrency_SelectedIndexChanged()
                            End If
                        End If
                        txtExchangeRate.Text = CCommon.ToDouble(dtBillHeader.Rows(0).Item("fltExchangeRateBillPayment"))
                        lblBaseCurr.Text = CCommon.ToString(dtBillHeader.Rows(0).Item("BaseCurrencySymbol")).Trim()
                        lblForeignCurr.Text = CCommon.ToString(dtBillHeader.Rows(0).Item("varCurrSymbol")).Trim()

                    End If
                End If

                If ddlUserLevelClass.Items.FindByValue(dtBillHeader.Rows(0).Item("numAccountClass")) IsNot Nothing Then
                    ddlUserLevelClass.ClearSelection()
                    ddlUserLevelClass.Items.FindByValue(dtBillHeader.Rows(0).Item("numAccountClass")).Selected = True
                End If

                If lngBillPaymentID > 0 Then
                    radCmbCompany.SelectedValue = CCommon.ToLong(dtBillHeader.Rows(0)("numDivisionID").ToString)
                    objCommon.DivisionID = CCommon.ToLong(dtBillHeader.Rows(0)("numDivisionID").ToString)
                    radCmbCompany.Text = objCommon.GetCompanyName()
                    BindCreditgrid()
                End If

                hdnReturnHeaderID.Value = CCommon.ToLong(dtBillHeader.Rows(0).Item("numReturnHeaderID"))
                hdnRefundAmount.Value = CCommon.ToDecimal(dtBillHeader.Rows(0).Item("monRefundAmount"))

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            'txtCurrrentPage.Text = 1
            BindBillsGrid("", "", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub PersistFormSettings()
        Try
            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add(ddlPaymentFrom.ID, ddlPaymentFrom.SelectedValue.Split("~")(0))
            PersistTable.Add(PersistKey.CurrentPage, IIf(txtCurrrentPage.Text > 0, txtCurrrentPage.Text, "1"))
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindBillsGrid(ByVal SortColumn As String, ByVal SortOrder As String, ByVal isGridFilter As Boolean)
        Try
            If objBillPayment Is Nothing Then objBillPayment = New BillPayment
            objBillPayment.BillPaymentID = lngBillPaymentID
            objBillPayment.DomainID = Session("DomainId")
            objBillPayment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            If CCommon.ToInteger(GetQueryStringVal("VendorId")) > 0 Then
                objBillPayment.DivisionID = CCommon.ToInteger(GetQueryStringVal("VendorId"))
            Else
                objBillPayment.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            End If
            objBillPayment.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objBillPayment.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
            objBillPayment.SortColumn = SortColumn
            objBillPayment.columnSortOrder = SortOrder

            If CCommon.ToLong(txtCurrrentPage.Text.Trim) = 0 Then txtCurrrentPage.Text = 1
            objBillPayment.CurrentPage = txtCurrrentPage.Text.Trim()
            objBillPayment.PageSize = Session("PagingRows")
            objBillPayment.TotalRecords = 0

            Dim strCondition As String = "1=1 "
            If Not String.IsNullOrEmpty(hdnVendorFliter.Value) Then
                strCondition = strCondition & " AND vcCompanyName ilike '%" & CCommon.ToString(hdnVendorFliter.Value) & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnBillFliter.Value)) Then
                strCondition = strCondition & " AND Name ilike '%" & CCommon.ToString(hdnBillFliter.Value) & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnReferenceFliter.Value)) Then
                strCondition = strCondition & " AND Reference ilike '%" & CCommon.ToString(hdnReferenceFliter.Value) & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnBillAmountFliter.Value)) Then
                strCondition = strCondition & " AND monOriginalAmount ilike '%" & hdnBillAmountFliter.Value.Trim() & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnAmtDueFliter.Value)) Then
                strCondition = strCondition & " AND monAmountDue ilike '%" & hdnAmtDueFliter.Value.Trim() & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnAmttoPayFliter.Value)) Then
                strCondition = strCondition & " AND monAmountPaid ilike '%" & hdnAmttoPayFliter.Value.Trim() & "%'"
            End If
            Dim txtFromDate As String
            Dim txtToDate As String
            If GetQueryStringVal("FilterFromTickler") = "1" Then
                txtFromDate = GetQueryStringVal("FromDate")
                txtToDate = GetQueryStringVal("ToDate")
            Else
                If (dgBills.Visible = True And dgBills.Items.Count > 0) Then
                    txtFromDate = CType(dgBills.Controls(0).Controls(0).FindControl("txtFromDate"), TextBox).Text
                    txtToDate = CType(dgBills.Controls(0).Controls(0).FindControl("txtToDate"), TextBox).Text
                End If
            End If
            If (Not String.IsNullOrEmpty(CCommon.ToString(txtFromDate)) And Not (String.IsNullOrEmpty(CCommon.ToString(txtToDate)))) Then
                Dim FromDate As DateTime = CType(txtFromDate, Date)
                Dim ToDate As DateTime = CType(txtToDate, Date)
                strCondition = strCondition & " AND ((dtDueDate >= '" & FromDate & "') AND (dtDueDate <= '" & ToDate & "'))"
            End If
            objBillPayment.strCondition = strCondition

            PersistFormSettings()

            Dim dsBills As DataSet = objBillPayment.GetBillPaymentDetail()

            Dim dvBills As DataView = dsBills.Tables(1).DefaultView
            'dvBills.RowFilter = strCondition

            dgBills.DataSource = dvBills
            dgBills.DataBind()

            If Not isGridFilter Then
                UserSelectedBills = ""
            End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objBillPayment.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            hdnBillPaymentID.Value = lngBillPaymentID
            TransactionInfo1.ReferenceType = enmReferenceType.BillPaymentHeader
            TransactionInfo1.ReferenceID = lngBillPaymentID
            TransactionInfo1.getTransactionInfo()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by: Sachin Sadhu||Date:21stApril2014
    'Purpose :To Display bizDoc status i.e. approved ,rejected,or pending
    'Assigned By :Carl

    Private Sub dgBills_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgBills.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                If Not String.IsNullOrEmpty(UserSelectedBills.Trim()) Then
                    txtAmount.Text = "0"
                    listTempSelectedBills = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedBill))(UserSelectedBills.Trim())
                Else
                    listTempSelectedBills = Nothing
                End If

                Dim txtVendorFliter As TextBox = CType(e.Item.FindControl("txtVendorFliter"), TextBox)
                Dim txtBillFliter As TextBox = CType(e.Item.FindControl("txtBillFliter"), TextBox)
                Dim txtBizDocStatusFliter As TextBox = CType(e.Item.FindControl("txtBizDocStatusFliter"), TextBox)
                Dim txtReferenceFliter As TextBox = CType(e.Item.FindControl("txtReferenceFliter"), TextBox)
                Dim txtBillAmountFliter As TextBox = CType(e.Item.FindControl("txtBillAmountFliter"), TextBox)
                Dim txtAmtDueFliter As TextBox = CType(e.Item.FindControl("txtAmtDueFliter"), TextBox)
                Dim txtAmttoPayFliter As TextBox = CType(e.Item.FindControl("txtAmttoPayFliter"), TextBox)
                Dim txtFromDate As TextBox = CType(e.Item.FindControl("txtFromDate"), TextBox)
                Dim txtToDate As TextBox = CType(e.Item.FindControl("txtToDate"), TextBox)

                txtVendorFliter.Text = hdnVendorFliter.Value
                txtBillFliter.Text = hdnBillFliter.Value
                txtBizDocStatusFliter.Text = hdnBizDocStatusFliter.Value
                txtReferenceFliter.Text = hdnReferenceFliter.Value
                txtBillAmountFliter.Text = hdnBillAmountFliter.Value
                txtAmtDueFliter.Text = hdnAmtDueFliter.Value
                txtAmttoPayFliter.Text = hdnAmttoPayFliter.Value

                If Not CCommon.ToString(hdnFromDateFilter.Value) = "" Then
                    txtFromDate.Text = hdnFromDateFilter.Value
                End If
                If Not CCommon.ToString(hdnToDateFilter.Value) = "" Then
                    txtFromDate.Text = hdnFromDateFilter.Value
                    txtToDate.Text = hdnToDateFilter.Value
                End If
            ElseIf e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                'Dim lblBillingDate As Label
                'lblBillingDate = DirectCast(e.Item.FindControl("lblBillingDate"), Label)

                Dim hdnOppBizDocID As New HiddenField
                hdnOppBizDocID = DirectCast(e.Item.FindControl("hdnOppBizDocID"), HiddenField)
                Dim OppBizDocID As Long = 0
                If hdnOppBizDocID IsNot Nothing Then
                    OppBizDocID = CCommon.ToLong(hdnOppBizDocID.Value)
                End If

                Dim hdnOppID As New HiddenField
                hdnOppID = DirectCast(e.Item.FindControl("hdnOppID"), HiddenField)
                Dim OppID As Long = 0 ' CCommon.ToLong(e.CommandArgument)
                If hdnOppID IsNot Nothing Then
                    OppID = CCommon.ToLong(hdnOppID.Value)
                End If
                Dim lblBizDocStatus As New Label
                lblBizDocStatus = DirectCast(e.Item.FindControl("lblBizDocStatus"), Label)

                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs

                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.OppId = OppID
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub
                If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Pending")) <> 644 Then
                    If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Pending")) > 0 Then
                        lblBizDocStatus.Text = "Pending"
                        'hplApproval.Visible = True
                        'hplApproval.Attributes.Add("onclick", "return openApp(" & lngOppBizDocID & ",'B'," & lngOppID & ",'" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID")) & "');")
                        'btnSaveClose.Visible = False
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Visible = False
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = True
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Red
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True

                    ElseIf CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Declined")) > 0 Then
                        lblBizDocStatus.Text = "Declined"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Visible = False
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = True
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Red
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                    ElseIf CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Approved")) > 0 Then
                        lblBizDocStatus.Text = "Approved by All"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled = True
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = False
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Green
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                    ElseIf CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("AppReq")) > 0 Then
                        lblBizDocStatus.Text = "Requested for Approval"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled = False
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = True
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Blue
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                        ' AppReq
                    Else
                        lblBizDocStatus.Text = "-"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled = True
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = False
                    End If

                End If

                If Not listTempSelectedBills Is Nothing Then
                    If DirectCast(e.Item.FindControl("chk"), CheckBox).Visible AndAlso DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled Then
                        If listTempSelectedBills.Where(Function(x) x.numBillID = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numBillID")) AndAlso x.numOppBizDocsID = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID"))).Count > 0 Then
                            DirectCast(e.Item.FindControl("chk"), CheckBox).Checked = True
                            DirectCast(e.Item.FindControl("txtAmountToPay"), TextBox).Text = listTempSelectedBills.First(Function(x) x.numBillID = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numBillID")) AndAlso x.numOppBizDocsID = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID"))).monAmountToPay
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    'end of Code
    Sub BindPaymentFromDropdown()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "010101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("vcStartingCheckNumber")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlPaymentFrom.Items.Add(item)
            Next
            ddlPaymentFrom.Items.Insert(0, New ListItem("--Select One --", "0"))

            PersistTable.Load(boolOnlyURL:=True)
            If PersistTable.Count > 0 Then
                'If ddlPaymentFrom.Items.FindByValue(CCommon.ToString(PersistTable(ddlPaymentFrom.ID))) IsNot Nothing Then
                '    ddlPaymentFrom.ClearSelection()
                '    ddlPaymentFrom.Items.FindByValue(CCommon.ToString(PersistTable(ddlPaymentFrom.ID))).Selected = True
                '    ddlPaymentFrom_SelectedIndexChanged()
                'End If

                For Each item In ddlPaymentFrom.Items
                    If item.Value.StartsWith(CCommon.ToString(PersistTable(ddlPaymentFrom.ID))) Then
                        ddlPaymentFrom.ClearSelection()
                        item.Selected = True
                        ddlPaymentFrom_SelectedIndexChanged()
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlPaymentFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaymentFrom.SelectedIndexChanged
        Try
            SaveSelectedBills()
            ddlPaymentFrom_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlPaymentFrom_SelectedIndexChanged()
        Try
            Dim lobjCashCreditCard As New CashCreditCard
            Dim lstrColor As String
            Dim ldecOpeningBalance As Decimal
            If ddlPaymentFrom.SelectedItem.Value <> "0" Then
                lobjCashCreditCard.AccountId = ddlPaymentFrom.SelectedValue.Split("~")(0)
                lobjCashCreditCard.DomainID = Session("DomainId")
                lblBalance.Text = "Balance: " & Session("Currency").ToString.Trim
                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
                lstrColor = "<h4 style=""display: inline;""><span class=""label label-danger"">" & ReturnMoney(CCommon.GetDecimalFormat(ldecOpeningBalance)) & "</span></h4>"
                lblBalanceAmount.Text = IIf(ldecOpeningBalance < 0, lstrColor, "<h4 style=""display: inline;""><span class=""label label-success"">" & ReturnMoney(CCommon.GetDecimalFormat(ldecOpeningBalance)) & "</span></h4>")
                'lblPaymentTotalAmount.Text = ReturnMoney(CDec(DepositTotalAmt.Value))
            Else
                lblBalanceAmount.Text = ""
                lblBalance.Text = ""
                'lblDepositTotalAmount.Text = ReturnMoney(CDec(DepositTotalAmt.Value))
            End If

            PersistFormSettings()


            If ddlPaymentFrom.SelectedValue.Contains("~") Then
                txtCheckNo.Text = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(1))
                hfCheckNo.Value = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(1))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnAllowCheckNO_Click(sender As Object, e As System.EventArgs) Handles btnAllowCheckNO.Click
        Try
            If SaveBillPayment() Then
                'Response.Redirect("frmVendorPayment.aspx")
                PageRedirect()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If PerformValidations() Then

                If rbHandCheck.Checked And CCommon.ToLong(txtCheckNo.Text.Trim()) > 0 Then
                    Dim ds As New DataSet
                    Dim dt As DataTable = GetCredits()
                    Dim drArray() As DataRow
                    Dim dv As New DataView(dt)
                    Dim dtDistictDivisions As DataTable = dv.ToTable(True, "numDivisionID")
                    Dim lngCheckNo As Long = CCommon.ToLong(txtCheckNo.Text)

                    Dim strchkNoList As New ArrayList

                    If dtDistictDivisions.Rows.Count > 0 Then
                        For i As Integer = 0 To dtDistictDivisions.Rows.Count - 1
                            strchkNoList.Add(lngCheckNo)
                            lngCheckNo = lngCheckNo + 1
                        Next
                    ElseIf dtDistictDivisions.Rows.Count = 0 And lngCheckNo > 0 Then
                        strchkNoList.Add(lngCheckNo)
                        lngCheckNo = lngCheckNo + 1
                    End If


                    Dim objCOA As New ChartOfAccounting
                    objCOA.AccountId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
                    objCOA.CheckHeaderID = 0
                    objCOA.DomainID = Session("DomainID")

                    Dim dtCheckNo As DataTable = objCOA.ValidateCheckNo(String.Join(",", strchkNoList.ToArray()))

                    If dtCheckNo.Rows.Count > 0 Then
                        strchkNoList = New ArrayList

                        For Each dr As DataRow In dtCheckNo.Rows
                            strchkNoList.Add(dr("numCheckNo"))
                        Next
                        ShowMessage("You must specify a different Check #. Check # " & String.Join(", ", strchkNoList.ToArray()) & " has already been used. Save & Print it anyway?")
                        btnAllowCheckNO.Visible = True
                        Return
                    End If
                End If

                If SaveBillPayment() Then
                    'Response.Redirect("frmVendorPayment.aspx", False)
                    PageRedirect()
                End If
            End If
        Catch ex As Exception
            If ex.Message = "IM_BALANCE" Then
                ShowMessage("The Credit and Debit amounts you�re entering must be of the same amount")
            ElseIf ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub
    Private Sub PageRedirect()
        Try
            If GetQueryStringVal("frm") = "BankRegister" Then
                Response.Redirect("../Accounting/frmJournalEntry.aspx?ChartAcntId=" & CCommon.ToLong(GetQueryStringVal("ChartAcntId")))
            ElseIf GetQueryStringVal("frm") = "RecurringTransaction" Then
                Response.Redirect("../Accounting/frmRecurringTransaction.aspx")
            ElseIf GetQueryStringVal("frm") = "GeneralLedger" Then
                Response.Redirect("../Accounting/frmGeneralLedger.aspx")
            ElseIf GetQueryStringVal("frm") = "Transaction" Then
                Response.Redirect("../Accounting/frmTransactions.aspx?ChartAcntId=" & CCommon.ToLong(GetQueryStringVal("ChartAcntId")))
            ElseIf GetQueryStringVal("frm") = "BankRecon" Then
                Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & CCommon.ToLong(GetQueryStringVal("ReconcileID")))
            ElseIf CCommon.ToString(strIds).Length > 0 Then
                Session("Bills") = strIds
                Response.Redirect("../Accounting/frmVendorPayment.aspx?flag=2")
            Else
                Response.Redirect("../Accounting/frmVendorPayment.aspx?flag=2")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnExportCheck_Click(sender As Object, e As System.EventArgs) Handles btnExportCheck.Click
        Try
            rbHandCheck.Checked = False
            rbPrintedCheck.Checked = True
            If SaveBillPayment() Then
                Response.Redirect("frmPrintChecksList.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Function SaveBillPayment() As Boolean
        Dim objJEHeader As New JournalEntryHeader

        Try
            Dim ds As New DataSet
            Dim dt As DataTable = GetCredits()

            If objBillPayment Is Nothing Then objBillPayment = New BillPayment
            objBillPayment.PaymentDate = calPaymentDate.SelectedDate
            objBillPayment.AccountID = ddlPaymentFrom.SelectedValue.Split("~")(0)
            objBillPayment.PaymentMethod = ddlPaymentMethod.SelectedValue
            objBillPayment.UserCntID = Session("UserContactID")
            objBillPayment.DomainID = Session("DomainID")



            Dim lngCheckNo As Long = CCommon.ToLong(txtCheckNo.Text)
            Dim listSelectedBills As System.Collections.Generic.List(Of SelectedBill)

            If Not String.IsNullOrEmpty(UserSelectedBills.Trim()) Then
                listSelectedBills = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedBill))(UserSelectedBills.Trim())
            End If

            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                If CCommon.ToDecimal(txtAmount.Text) > 0 Then
                    ds.Tables.Clear()
                    ds.Tables.Add(dt)
                    ds.Tables(0).TableName = "Item"

                    Using objTran As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        lngDivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                        decTotalAmountPerOrganization = CCommon.ToDecimal(txtAmount.Text) * IIf(CCommon.ToDouble(txtExchangeRate.Text) = 0.0, 1, CCommon.ToDouble(txtExchangeRate.Text))

                        objBillPayment.strXml = ds.GetXml()
                        objBillPayment.BillPaymentID = IIf(CCommon.ToLong(hdnReturnHeaderID.Value) > 0, 0, lngBillPaymentID)
                        objBillPayment.Mode = 1
                        objBillPayment.PaymentAmount = CCommon.ToDecimal(txtAmount.Text)
                        objBillPayment.DivisionID = lngDivisionID
                        objBillPayment.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                        objBillPayment.ExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                        objBillPayment.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                        objBillPayment.ManageBillPayment(Nothing) 'Save Bill Payment Entry
                        lngBillPaymentID = objBillPayment.BillPaymentID

                        If lngBillPaymentID > 0 Then
                            'Save check entry
                            If decTotalAmountPerOrganization > 0 And ddlPaymentMethod.SelectedValue = 5 And lngDivisionID > 0 And lngCheckNo > 0 Then 'For bill against liability do not create check entry
                                'SaveCheckHeader(lngCheckNo)
                                Dim objChecks As New Checks()

                                objChecks.DomainID = Session("DomainID")
                                objChecks.AccountId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
                                objChecks.CheckHeaderID = hfCheckHeaderID.Value
                                objChecks.DivisionId = lngDivisionID
                                objChecks.UserCntID = Session("UserContactID")
                                objChecks.Amount = decTotalAmountPerOrganization
                                objChecks.FromDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
                                objChecks.CheckNo = IIf(rbHandCheck.Checked, lngCheckNo, 0)
                                objChecks.Memo = "Check against bill payment(ID=" & lngBillPaymentID & ")"

                                objChecks.ReferenceID = lngBillPaymentID
                                objChecks.Type = enmReferenceType.BillPaymentHeader
                                objChecks.Mode = 2 'Insert/Edit
                                objChecks.IsPrint = rbHandCheck.Checked
                                objChecks.ManageCheckHeader(Nothing)

                                lngCheckNo = lngCheckNo + 1
                            End If

                            '''Save journal Entry
                            ''SaveDataToHeader()
                            If objJEHeader Is Nothing Then objJEHeader = New JournalEntryHeader
                            With objJEHeader
                                .JournalId = TransactionInfo1.JournalID
                                .RecurringId = 0
                                .EntryDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
                                .Description = "Bill Payment" '"Bill Payment from account: " & ddlPaymentFrom.SelectedItem.Text
                                .Amount = decTotalAmountPerOrganization
                                .CheckId = 0
                                .CashCreditCardId = 0
                                .ChartAcntId = ddlPaymentFrom.SelectedValue.Split("~")(0)
                                .OppId = 0
                                .OppBizDocsId = 0
                                .DepositId = 0
                                .BizDocsPaymentDetId = 0
                                .IsOpeningBalance = 0
                                .LastRecurringDate = Date.Now
                                .NoTransactions = 0
                                .CategoryHDRID = 0
                                .ReturnID = 0
                                .CheckHeaderID = 0
                                .BillID = 0
                                .BillPaymentID = lngBillPaymentID
                                .UserCntID = Session("UserContactID")
                                .DomainID = Session("DomainID")
                            End With
                            lngJournalID = objJEHeader.Save()

                            ''SaveDataToGeneralJournalDetails()
                            If lngJournalID > 0 Then

                                Dim dtrow As DataRow
                                Dim objJE As JournalEntryNew
                                Dim objJEList As New JournalEntryCollection
                                Dim FltExchangeRate As Double
                                Dim TotalAmountAtExchangeRateOfOrder As Decimal
                                Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0

                                'Determine Exchange Rate
                                If CCommon.ToLong(ddlCurrency.SelectedValue) = 0 Then
                                    FltExchangeRate = 1
                                ElseIf CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) Then
                                    FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                Else
                                    FltExchangeRate = 1
                                End If
                                'i.e foreign currency payment
                                If FltExchangeRate <> 1 Then
                                    TotalAmountAtExchangeRateOfOrder = 0
                                    For Each item As DataGridItem In dgBills.Items
                                        If CType(item.FindControl("chk"), CheckBox).Checked Then
                                            If Not listSelectedBills Is Nothing AndAlso listSelectedBills.Where(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                                                listSelectedBills.RemoveAll(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                                            End If

                                            TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text) * CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                                        End If
                                    Next

                                    If Not listSelectedBills Is Nothing AndAlso listSelectedBills.Count > 0 Then
                                        For Each objSelectedBill In listSelectedBills
                                            TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + objSelectedBill.monAmountToPay * objSelectedBill.numExchangeRateOfOrder
                                        Next
                                    End If

                                    If TotalAmountAtExchangeRateOfOrder > 0 Then
                                        TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfOrder - decTotalAmountPerOrganization
                                        If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                                            TotalAmountVariationDueToCurrencyExRate = 0
                                        End If
                                    End If
                                End If

                                objJE = New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = IIf(TotalAmountAtExchangeRateOfOrder > 0, TotalAmountAtExchangeRateOfOrder, decTotalAmountPerOrganization)
                                objJE.CreditAmt = 0

                                If lngDivisionID > 0 Then
                                    objJE.ChartAcntId = GetVendorAP_AccountID(lngDivisionID)
                                End If

                                objJE.Description = "Bill Payment" 'CCommon.ToString("Vendor Bill Payment From " + ddlPaymentFrom.SelectedItem.Text) ' CCommon.ToString(dr("vcMemo"))
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = 0
                                objJE.MainCheck = 0
                                objJE.MainCashCredit = 0
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = ""
                                objJE.Reference = ""
                                objJE.PaymentMethod = ddlPaymentMethod.SelectedValue
                                objJE.Reconcile = False
                                objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = 0
                                objJE.ReferenceType = enmReferenceType.BillPaymentDetail
                                objJE.ReferenceID = 0
                                objJEList.Add(objJE)

                                objJE = New JournalEntryNew()
                                objJE.TransactionId = TransactionInfo1.HeaderTransactionID
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decTotalAmountPerOrganization
                                objJE.ChartAcntId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
                                objJE.Description = "Bill Payment" 'CCommon.ToString("Vendor Bill Payment from " + ddlPaymentFrom.SelectedItem.Text)
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = 0
                                objJE.MainCheck = 0
                                objJE.MainCashCredit = 0
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = ""
                                objJE.Reference = ""
                                objJE.PaymentMethod = ddlPaymentMethod.SelectedValue
                                objJE.Reconcile = False
                                objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = 0
                                objJE.ReferenceType = enmReferenceType.BillPaymentHeader
                                objJE.ReferenceID = lngBillPaymentID
                                objJEList.Add(objJE)

                                If CCommon.ToLong(ddlCurrency.SelectedValue) > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                                    If CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And CCommon.ToDouble(txtExchangeRate.Text) > 0.0 Then 'Foreign Currency Transaction

                                        Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                                        'Credit: Customer A/R With (Amount)
                                        objJE = New JournalEntryNew()
                                        objJE.TransactionId = 0

                                        ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                                        If TotalAmountVariationDueToCurrencyExRate < 0 Then
                                            objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                            objJE.CreditAmt = 0
                                        ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                                            objJE.DebitAmt = 0
                                            objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                        End If

                                        objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                                        objJE.Description = "Exchange Gain/Loss"
                                        objJE.CustomerId = lngDivisionID
                                        objJE.MainDeposit = 0
                                        objJE.MainCheck = 0
                                        objJE.MainCashCredit = 0
                                        objJE.OppitemtCode = 0
                                        objJE.BizDocItems = ""
                                        objJE.Reference = ""
                                        objJE.PaymentMethod = ddlPaymentMethod.SelectedValue
                                        objJE.Reconcile = False
                                        objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                        objJE.FltExchangeRate = FltExchangeRate
                                        objJE.TaxItemID = 0
                                        objJE.BizDocsPaymentDetailsId = 0
                                        objJE.ContactID = 0
                                        objJE.ItemID = 0
                                        objJE.ProjectID = 0
                                        objJE.ClassID = 0
                                        objJE.CommissionID = 0
                                        objJE.ReconcileID = 0
                                        objJE.Cleared = 0
                                        objJE.ReferenceType = enmReferenceType.BillPaymentDetail
                                        objJE.ReferenceID = 0

                                        objJEList.Add(objJE)
                                    End If
                                End If

                                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"), Nothing)
                            Else
                                ShowMessage("ERROR:2 Error occurred while processing payment.") 'Error while saving Deposite Journal Header Entry data)
                                Throw New Exception("ROLLBACK")
                            End If

                            objTran.Complete()
                        Else
                            ShowMessage("ERROR:1 Error occurred while processing payment.") 'Error while saving Deposite Entry data
                            Throw New Exception("ROLLBACK")
                        End If
                    End Using
                End If

                lngBillPaymentID = 0
                lngDivisionID = 0
            Else
                Dim drArray() As DataRow
                Dim dv As New DataView(dt)
                Dim dtDistictDivisions As DataTable = dv.ToTable(True, "numDivisionID")

                Dim objJE As JournalEntryNew
                Dim objJEList As New JournalEntryCollection
                Dim FltExchangeRate As Double
                Dim TotalAmountAtExchangeRateOfOrder As Decimal
                Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0

                For i As Integer = 0 To dtDistictDivisions.Rows.Count - 1

                    drArray = dt.Select("numDivisionID='" & dtDistictDivisions.Rows(i)("numDivisionID").ToString & "'", Nothing)
                    If drArray.Length > 0 Then
                        ds.Tables.Clear()
                        ds.Tables.Add(drArray.CopyToDataTable())
                        ds.Tables(0).TableName = "Item"

                        Using objTran As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            lngDivisionID = CCommon.ToLong(CType(drArray(0), DataRow)("numDivisionID"))
                            decTotalAmountPerOrganization = Math.Abs(CCommon.ToDecimal(ds.Tables(0).Compute("sum(monAmount)", ""))) * IIf(CCommon.ToDouble(txtExchangeRate.Text) = 0.0, 1, CCommon.ToDouble(txtExchangeRate.Text))

                            'ds.Tables.Add(dtCredits)
                            'ds.Tables(1).TableName = "Credits"

                            objBillPayment.strXml = ds.GetXml()
                            objBillPayment.BillPaymentID = lngBillPaymentID
                            objBillPayment.Mode = 1
                            objBillPayment.PaymentAmount = Math.Abs(CCommon.ToDecimal(ds.Tables(0).Compute("sum(monAmount)", "")))
                            objBillPayment.DivisionID = lngDivisionID
                            objBillPayment.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                            objBillPayment.ExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                            objBillPayment.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                            objBillPayment.ManageBillPayment(Nothing) 'Save Bill Payment Entry
                            lngBillPaymentID = objBillPayment.BillPaymentID

                            If lngBillPaymentID > 0 Then
                                'Save check entry
                                If decTotalAmountPerOrganization > 0 And ddlPaymentMethod.SelectedValue = 5 And lngDivisionID > 0 And lngCheckNo > 0 Then 'For bill against liability do not create check entry
                                    'SaveCheckHeader(lngCheckNo)
                                    Dim objChecks As New Checks()
                                    objChecks.DomainID = Session("DomainID")
                                    objChecks.AccountId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
                                    objChecks.CheckHeaderID = hfCheckHeaderID.Value
                                    objChecks.DivisionId = lngDivisionID
                                    objChecks.UserCntID = Session("UserContactID")
                                    objChecks.Amount = decTotalAmountPerOrganization
                                    objChecks.FromDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
                                    objChecks.CheckNo = IIf(rbHandCheck.Checked, lngCheckNo, 0)
                                    objChecks.Memo = "Check against bill payment(ID=" & lngBillPaymentID & ")"

                                    objChecks.ReferenceID = lngBillPaymentID
                                    objChecks.Type = enmReferenceType.BillPaymentHeader
                                    objChecks.Mode = 2 'Insert/Edit
                                    objChecks.IsPrint = rbHandCheck.Checked
                                    objChecks.ManageCheckHeader(Nothing)

                                    lngCheckNo = lngCheckNo + 1
                                End If
                                '''Save journal Entry
                                ''SaveDataToHeader()
                                ''SaveDataToGeneralJournalDetails()
                                If objJEHeader Is Nothing Then objJEHeader = New JournalEntryHeader
                                With objJEHeader
                                    .JournalId = TransactionInfo1.JournalID
                                    .RecurringId = 0
                                    .EntryDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
                                    .Description = "Bill Payment" '"Bill Payment from account: " & ddlPaymentFrom.SelectedItem.Text
                                    .Amount = decTotalAmountPerOrganization
                                    .CheckId = 0
                                    .CashCreditCardId = 0
                                    .ChartAcntId = ddlPaymentFrom.SelectedValue.Split("~")(0)
                                    .OppId = 0
                                    .OppBizDocsId = 0
                                    .DepositId = 0
                                    .BizDocsPaymentDetId = 0
                                    .IsOpeningBalance = 0
                                    .LastRecurringDate = Date.Now
                                    .NoTransactions = 0
                                    .CategoryHDRID = 0
                                    .ReturnID = 0
                                    .CheckHeaderID = 0
                                    .BillID = 0
                                    .BillPaymentID = lngBillPaymentID
                                    .UserCntID = Session("UserContactID")
                                    .DomainID = Session("DomainID")
                                End With
                                lngJournalID = objJEHeader.Save()

                                ''SaveDataToGeneralJournalDetails()
                                If lngJournalID > 0 Then

                                    Dim dtrow As DataRow
                                    objJEList = New JournalEntryCollection
                                    FltExchangeRate = 0
                                    TotalAmountAtExchangeRateOfOrder = 0
                                    TotalAmountVariationDueToCurrencyExRate = 0

                                    'Determine Exchange Rate
                                    If CCommon.ToLong(ddlCurrency.SelectedValue) = 0 Then
                                        FltExchangeRate = 1
                                    ElseIf CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) Then
                                        FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                    Else
                                        FltExchangeRate = 1
                                    End If
                                    'i.e foreign currency payment
                                    If FltExchangeRate <> 1 Then
                                        TotalAmountAtExchangeRateOfOrder = 0
                                        For Each item As DataGridItem In dgBills.Items
                                            If CType(item.FindControl("chk"), CheckBox).Checked AndAlso CType(item.FindControl("hdnDivisionID"), HiddenField).Value = CCommon.ToString(dtDistictDivisions.Rows(i)("numDivisionID")) Then
                                                If Not listSelectedBills Is Nothing AndAlso listSelectedBills.Where(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                                                    listSelectedBills.RemoveAll(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                                                End If

                                                TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text) * CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                                            End If
                                        Next

                                        If Not listSelectedBills Is Nothing AndAlso listSelectedBills.Count > 0 Then
                                            For Each objSelectedBill In listSelectedBills
                                                If objSelectedBill.numDivisionID = CCommon.ToString(dtDistictDivisions.Rows(i)("numDivisionID")) Then
                                                    TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + objSelectedBill.monAmountToPay * objSelectedBill.numExchangeRateOfOrder
                                                End If
                                            Next
                                        End If

                                        If TotalAmountAtExchangeRateOfOrder > 0 Then
                                            TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfOrder - decTotalAmountPerOrganization
                                            If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                                                TotalAmountVariationDueToCurrencyExRate = 0
                                            End If
                                        End If
                                    End If

                                    If objJE Is Nothing Then objJE = New JournalEntryNew
                                    objJE.TransactionId = 0
                                    objJE.DebitAmt = IIf(TotalAmountAtExchangeRateOfOrder > 0, TotalAmountAtExchangeRateOfOrder, decTotalAmountPerOrganization)
                                    objJE.CreditAmt = 0

                                    If lngDivisionID > 0 Then
                                        objJE.ChartAcntId = GetVendorAP_AccountID(lngDivisionID)
                                    End If

                                    objJE.Description = "Bill Payment" 'CCommon.ToString("Vendor Bill Payment From " + ddlPaymentFrom.SelectedItem.Text) ' CCommon.ToString(dr("vcMemo"))
                                    objJE.CustomerId = lngDivisionID
                                    objJE.MainDeposit = 0
                                    objJE.MainCheck = 0
                                    objJE.MainCashCredit = 0
                                    objJE.OppitemtCode = 0
                                    objJE.BizDocItems = ""
                                    objJE.Reference = ""
                                    objJE.PaymentMethod = ddlPaymentMethod.SelectedValue
                                    objJE.Reconcile = False
                                    objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                    objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                    objJE.TaxItemID = 0
                                    objJE.BizDocsPaymentDetailsId = 0
                                    objJE.ContactID = 0
                                    objJE.ItemID = 0
                                    objJE.ProjectID = 0
                                    objJE.ClassID = 0
                                    objJE.CommissionID = 0
                                    objJE.ReconcileID = 0
                                    objJE.Cleared = 0
                                    objJE.ReferenceType = enmReferenceType.BillPaymentDetail
                                    objJE.ReferenceID = 0
                                    objJEList.Add(objJE)

                                    objJE = New JournalEntryNew()
                                    objJE.TransactionId = TransactionInfo1.HeaderTransactionID
                                    objJE.DebitAmt = 0
                                    objJE.CreditAmt = decTotalAmountPerOrganization
                                    objJE.ChartAcntId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
                                    objJE.Description = "Bill Payment" 'CCommon.ToString("Vendor Bill Payment from " + ddlPaymentFrom.SelectedItem.Text)
                                    objJE.CustomerId = lngDivisionID
                                    objJE.MainDeposit = 0
                                    objJE.MainCheck = 0
                                    objJE.MainCashCredit = 0
                                    objJE.OppitemtCode = 0
                                    objJE.BizDocItems = ""
                                    objJE.Reference = ""
                                    objJE.PaymentMethod = ddlPaymentMethod.SelectedValue
                                    objJE.Reconcile = False
                                    objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                    objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                    objJE.TaxItemID = 0
                                    objJE.BizDocsPaymentDetailsId = 0
                                    objJE.ContactID = 0
                                    objJE.ItemID = 0
                                    objJE.ProjectID = 0
                                    objJE.ClassID = 0
                                    objJE.CommissionID = 0
                                    objJE.ReconcileID = 0
                                    objJE.Cleared = 0
                                    objJE.ReferenceType = enmReferenceType.BillPaymentHeader
                                    objJE.ReferenceID = lngBillPaymentID
                                    objJEList.Add(objJE)

                                    If CCommon.ToLong(ddlCurrency.SelectedValue) > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                                        If CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And CCommon.ToDouble(txtExchangeRate.Text) > 0.0 Then 'Foreign Currency Transaction

                                            Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                                            'Credit: Customer A/R With (Amount)
                                            objJE = New JournalEntryNew()
                                            objJE.TransactionId = 0

                                            ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                                            If TotalAmountVariationDueToCurrencyExRate < 0 Then
                                                objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                                objJE.CreditAmt = 0
                                            ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                                                objJE.DebitAmt = 0
                                                objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                            End If

                                            objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                                            objJE.Description = "Exchange Gain/Loss"
                                            objJE.CustomerId = lngDivisionID
                                            objJE.MainDeposit = 0
                                            objJE.MainCheck = 0
                                            objJE.MainCashCredit = 0
                                            objJE.OppitemtCode = 0
                                            objJE.BizDocItems = ""
                                            objJE.Reference = ""
                                            objJE.PaymentMethod = ddlPaymentMethod.SelectedValue
                                            objJE.Reconcile = False
                                            objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                            objJE.FltExchangeRate = FltExchangeRate
                                            objJE.TaxItemID = 0
                                            objJE.BizDocsPaymentDetailsId = 0
                                            objJE.ContactID = 0
                                            objJE.ItemID = 0
                                            objJE.ProjectID = 0
                                            objJE.ClassID = 0
                                            objJE.CommissionID = 0
                                            objJE.ReconcileID = 0
                                            objJE.Cleared = 0
                                            objJE.ReferenceType = enmReferenceType.BillPaymentDetail
                                            objJE.ReferenceID = 0

                                            objJEList.Add(objJE)
                                        End If
                                    End If

                                    objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"), Nothing)
                                Else
                                    ShowMessage("ERROR:2 Error occurred while processing payment.")
                                    Throw New Exception("ROLLBACK")
                                End If

                                lngBillPaymentID = 0
                                lngDivisionID = 0

                                objTran.Complete()
                            Else
                                ShowMessage("ERROR:1 Error occurred while processing payment.")
                                Throw New Exception("ROLLBACK")
                            End If
                        End Using
                    End If
                    intBillPayCount = intBillPayCount + 1
                Next
            End If

            'TODO:Update Starting check # field

            ShowMessage(CCommon.ToString(intBillPayCount) + " bill(s) paid.")
            lngBillPaymentID = 0
            hfCheckHeaderID.Value = 0
            lngJournalID = 0
            intBillPayCount = 0
            lngCheckNo = 0

            BindBillsGrid("", "", False)
            Return True
        Catch ex As Exception
            If ex.Message <> "ROLLBACK" Then
                Throw ex
            End If
        End Try
    End Function

    Function GetCredits() As DataTable
        Try
            If objBillPayment Is Nothing Then objBillPayment = New BillPayment
            Dim dsBillDetails As New DataSet
            Dim dtBills As New DataTable
            CCommon.AddColumnsToDataTable(dtBills, "numDivisionID,tintBillType,numOppBizDocsID,numBillID")
            dtBills.Columns.Add("monAmount", Type.GetType("System.Decimal"))

            Dim dtCredits As New DataTable
            CCommon.AddColumnsToDataTable(dtCredits, "numDivisionID,tintBillType,numOppBizDocsID,numBillID")
            dtCredits.Columns.Add("monAmount", Type.GetType("System.Decimal"))

            Dim listSelectedBills As System.Collections.Generic.List(Of SelectedBill)

            If Not String.IsNullOrEmpty(UserSelectedBills.Trim()) Then
                listSelectedBills = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedBill))(UserSelectedBills.Trim())
            End If

            'Dim intCnt As Integer = 0
            For Each item As DataGridItem In dgBills.Items
                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    If Not listSelectedBills Is Nothing AndAlso listSelectedBills.Where(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                        listSelectedBills.RemoveAll(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                    End If

                    objBillPayment.DivisionID = CCommon.ToLong(CType(item.FindControl("hdnDivisionID"), HiddenField).Value)
                    objBillPayment.DomainID = Session("DomainID")
                    objBillPayment.BillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value)
                    objBillPayment.OppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                    objBillPayment.Amount = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                    dsBillDetails = objBillPayment.CheckBizDocAmountsForPayBill()

                    If dsBillDetails IsNot Nothing AndAlso dsBillDetails.Tables.Count > 0 AndAlso dsBillDetails.Tables(0).Rows.Count > 0 Then
                        If CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 1 Then
                            strIds = strIds & "," & CCommon.ToString(CType(item.FindControl("lblBizDocName"), Label).Text)
                        ElseIf CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 4 Then

                        ElseIf CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 2 OrElse CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 3 Then
                            Dim dr As DataRow = dtBills.NewRow
                            dr("numDivisionID") = CType(item.FindControl("hdnDivisionID"), HiddenField).Value
                            dr("numOppBizDocsID") = CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value
                            dr("numBillID") = CType(item.FindControl("hdnBillID"), HiddenField).Value
                            dr("tintBillType") = CType(item.FindControl("hdnBillType"), HiddenField).Value
                            dr("monAmount") = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                            dtBills.Rows.Add(dr)
                        End If
                        'intCnt = intCnt + 1
                    Else
                        Dim dr As DataRow = dtBills.NewRow
                        dr("numDivisionID") = CType(item.FindControl("hdnDivisionID"), HiddenField).Value
                        dr("numOppBizDocsID") = CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value
                        dr("numBillID") = CType(item.FindControl("hdnBillID"), HiddenField).Value
                        dr("tintBillType") = CType(item.FindControl("hdnBillType"), HiddenField).Value
                        dr("monAmount") = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                        dtBills.Rows.Add(dr)
                    End If

                End If
            Next

            If Not listSelectedBills Is Nothing AndAlso listSelectedBills.Count > 0 Then
                For Each objSelectedBill In listSelectedBills
                    objBillPayment = New BillPayment
                    objBillPayment.DivisionID = objSelectedBill.numDivisionID
                    objBillPayment.DomainID = Session("DomainID")
                    objBillPayment.BillID = objSelectedBill.numBillID
                    objBillPayment.OppBizDocsID = objSelectedBill.numOppBizDocsID
                    objBillPayment.Amount = objSelectedBill.monAmountToPay
                    dsBillDetails = objBillPayment.CheckBizDocAmountsForPayBill()

                    If dsBillDetails IsNot Nothing AndAlso dsBillDetails.Tables.Count > 0 AndAlso dsBillDetails.Tables(0).Rows.Count > 0 Then
                        If CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 1 Then
                            strIds = strIds & "," & objSelectedBill.vcBizDocName
                        ElseIf CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 4 Then

                        ElseIf CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 2 OrElse CCommon.ToShort(dsBillDetails.Tables(0).Rows(0)("IsPaidInFull")) = 3 Then
                            Dim dr As DataRow = dtBills.NewRow
                            dr("numDivisionID") = objSelectedBill.numDivisionID
                            dr("numOppBizDocsID") = objSelectedBill.numOppBizDocsID
                            dr("numBillID") = objSelectedBill.numBillID
                            dr("tintBillType") = objSelectedBill.tintBillType
                            dr("monAmount") = objSelectedBill.monAmountToPay
                            dtBills.Rows.Add(dr)
                        End If
                        'intCnt = intCnt + 1
                    Else
                        Dim dr As DataRow = dtBills.NewRow
                        dr("numDivisionID") = objSelectedBill.numDivisionID
                        dr("numOppBizDocsID") = objSelectedBill.numOppBizDocsID
                        dr("numBillID") = objSelectedBill.numBillID
                        dr("tintBillType") = objSelectedBill.tintBillType
                        dr("monAmount") = objSelectedBill.monAmountToPay
                        dtBills.Rows.Add(dr)
                    End If
                Next
            End If

            If (CCommon.ToLong(radCmbCompany.SelectedValue) > 0) Then
                Dim dr As DataRow

                Dim decBillsAmount As Decimal = 0
                decBillsAmount = CCommon.ToDecimal(dtBills.Compute("Sum(monAmount)", ""))

                If decBillsAmount > 0 Then
                    For Each gvr As GridViewRow In gvCredits.Rows
                        dtCredits.Rows.Clear()

                        If CCommon.ToLong(hdnBillPaymentID.Value) > 0 AndAlso CCommon.ToLong(hdnReturnHeaderID.Value) > 0 _
                            AndAlso CCommon.ToShort(CType(gvr.FindControl("lblRefType"), Label).Text) = 1 _
                            AndAlso CCommon.ToLong(CType(gvr.FindControl("lblReferenceID"), Label).Text) = CCommon.ToLong(hdnBillPaymentID.Value) _
                            AndAlso CType(gvr.FindControl("chk"), CheckBox).Checked = False Then

                            With objBillPayment
                                .PaymentDate = CDate(Date.UtcNow.Date & " 12:00:00")
                                .UserCntID = Session("UserContactID")
                                .DomainID = Session("DomainID")

                                .strXml = ""
                                .BillPaymentID = CCommon.ToLong(CType(gvr.FindControl("lblReferenceID"), Label).Text)
                                .Mode = 2
                                .DivisionID = lngDivisionID
                                .AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                                .ManageBillPayment() 'Save Bill Payment Entry
                            End With
                        End If

                        If CType(gvr.FindControl("chk"), CheckBox).Checked Then
                            Dim decCreditAmount As Decimal = CCommon.ToDecimal(CType(gvr.FindControl("txtCreditAmount"), TextBox).Text)

                            If decCreditAmount > 0 Then
                                For Each drMain As DataRow In dtBills.Rows
                                    If decCreditAmount > 0 Then
                                        Dim monAmountPaid As Decimal = CCommon.ToDecimal(drMain("monAmount"))

                                        If monAmountPaid > 0 Then
                                            dr = dtCredits.NewRow

                                            dr("numDivisionID") = drMain("numDivisionID")
                                            dr("numOppBizDocsID") = drMain("numOppBizDocsID")
                                            dr("numBillID") = drMain("numBillID")
                                            dr("tintBillType") = drMain("tintBillType")

                                            If decCreditAmount > monAmountPaid Then
                                                dr("monAmount") = monAmountPaid
                                                decCreditAmount = decCreditAmount - monAmountPaid
                                            Else
                                                dr("monAmount") = decCreditAmount
                                                decCreditAmount = 0
                                            End If

                                            drMain("monAmount") = monAmountPaid - dr("monAmount")
                                            dtCredits.Rows.Add(dr)
                                        End If
                                    Else
                                        Exit For
                                    End If
                                Next

                                dtBills.AcceptChanges()

                                If dtCredits.Rows.Count > 0 Then
                                    If objBillPayment Is Nothing Then objBillPayment = New BillPayment

                                    Dim ds As New DataSet
                                    Dim str As String = ""

                                    If dtCredits.Rows.Count > 0 Then
                                        ds.Tables.Add(dtCredits.Copy)
                                        ds.Tables(0).TableName = "Item"
                                        str = ds.GetXml()
                                    End If

                                    objBillPayment.PaymentDate = CDate(Date.UtcNow.Date & " 12:00:00")
                                    objBillPayment.strXml = str
                                    objBillPayment.BillPaymentID = CCommon.ToLong(CType(gvr.FindControl("lblReferenceID"), Label).Text)
                                    objBillPayment.UserCntID = Session("UserContactID")
                                    objBillPayment.DomainID = Session("DomainID")
                                    objBillPayment.Mode = 2
                                    objBillPayment.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                                    objBillPayment.ManageBillPayment() 'Save Bill Payment Entry
                                End If
                            End If
                        End If
                    Next
                End If
            End If

            Dim drRows() As DataRow = dtBills.Select("monAmount>0")
            If drRows.Length > 0 Then
                Return drRows.CopyToDataTable
            Else
                Return dtBills.Clone()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UserCntID = Session("UserContactID")
            objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            Dim lngAccountClassID As Long = objCommon.GetAccountingClass()

            If Not ddlUserLevelClass.Items.FindByValue(lngAccountClassID) Is Nothing Then
                ddlUserLevelClass.ClearSelection()
                ddlUserLevelClass.Items.FindByValue(lngAccountClassID).Selected = True
            End If

            txtCurrrentPage.Text = 1
            BindBillsGrid("", "", False)
            BindCreditgrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub BindCreditgrid()
        Try
            'If CCommon.ToDecimal(hdnAmountToCredit.Value) > 0.0 Then
            Dim objReturns As New Returns
            With objReturns
                .DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
                .DomainID = Session("DomainID")
                .ReferenceID = lngBillPaymentID
                .Mode = 2
                .CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                .AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                Dim dt As DataTable = .GetCustomerCredits
                gvCredits.DataSource = dt
                gvCredits.DataBind()

                trCredits.Visible = IIf(dt.Rows.Count > 0, True, False)

            End With

            trAmount.Visible = IIf(CCommon.ToLong(radCmbCompany.SelectedValue) > 0, True, False)
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCheckHeader(ByVal lngCheckNo As Long)
        Try
            Dim objChecks As New Checks()

            objChecks.DomainID = Session("DomainID")
            objChecks.AccountId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
            objChecks.CheckHeaderID = hfCheckHeaderID.Value
            objChecks.DivisionId = lngDivisionID
            objChecks.UserCntID = Session("UserContactID")
            objChecks.Amount = decTotalAmountPerOrganization
            objChecks.FromDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
            objChecks.CheckNo = IIf(rbHandCheck.Checked, lngCheckNo, 0)
            objChecks.Memo = "Check against bill payment(ID=" & lngBillPaymentID & ")"

            objChecks.ReferenceID = lngBillPaymentID
            objChecks.Type = enmReferenceType.BillPaymentHeader
            objChecks.Mode = 2 'Insert/Edit
            objChecks.IsPrint = rbHandCheck.Checked

            objChecks.ManageCheckHeader()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetVendorAP_AccountID(lngDivisionID As Long) As Long
        Try
            Dim AccountID As Long
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .COARelationshipID = 0
                .DivisionID = lngDivisionID
                .DomainID = Session("DomainID")
                ds = .GetCOARelationship()
            End With
            If ds.Tables(0).Rows.Count > 0 Then
                AccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAPAccountId"))
            End If

            If AccountID = 0 Then
                '' For Account Payable to be Debited
                AccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
            End If
            Return AccountID
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub BindMultiCurrency()
        Try
            If Session("MultiCurrency") = True Then
                pnlCurrency.Visible = True
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                ddlCurrency.DataTextField = "vcCurrencyDesc"
                ddlCurrency.DataValueField = "numCurrencyID"
                ddlCurrency.DataBind()
                'ddlCurrency.Items.Insert(0, "--Select One--")
                'ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                    ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    ddlCurrency_SelectedIndexChanged()
                End If
            Else
                pnlCurrency.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function PerformValidations() As Boolean
        Try
            If Session("MultiCurrency") = True Then
                Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                If lngDefaultForeignExchangeAccountID = 0 Then
                    ShowMessage("Please Map Default Foreign Exchange Gain/Loss Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" .")
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub ddlCurrency_SelectedIndexChanged()
        Try
            Dim objCurrency As New CurrencyRates
            objCurrency.DomainID = Session("DomainID")
            objCurrency.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objCurrency.GetAll = 0
            Dim dt As DataTable = objCurrency.GetCurrencyWithRates()
            If dt.Rows.Count > 0 Then
                lblBaseCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                lblForeignCurr.Text = CCommon.ToString(dt.Rows(0)("varCurrSymbol")).Trim()
                txtExchangeRate.Text = CCommon.ToString(dt.Rows(0)("fltExchangeRate"))
            Else
                txtExchangeRate.Text = "1"
                lblForeignCurr.Text = CCommon.ToString(Session("Currency")).Trim()
            End If

            If Session("BaseCurrencyID") = CCommon.ToString(ddlCurrency.SelectedValue) Then
                txtExchangeRate.Text = 1
                txtExchangeRate.Enabled = False
            Else
                txtExchangeRate.Enabled = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ddlCurrency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCurrency.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            ddlCurrency_SelectedIndexChanged()
            BindBillsGrid("", "", False)
            BindCreditgrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If lngBillPaymentID > 0 Then
                Dim objJournalEntry As New JournalEntry
                objJournalEntry.JournalId = 0
                objJournalEntry.BillPaymentID = lngBillPaymentID
                objJournalEntry.DepositId = 0
                objJournalEntry.CheckHeaderID = 0
                objJournalEntry.BillID = 0
                objJournalEntry.CategoryHDRID = 0
                objJournalEntry.ReturnID = 0
                objJournalEntry.DomainID = Session("DomainId")
                objJournalEntry.UserCntID = Session("UserContactID")

                Try
                    objJournalEntry.DeleteJournalEntryDetails()
                    Response.Redirect("../Accounting/frmVendorPayment.aspx?flag=1", False)
                Catch ex As Exception
                    If ex.Message = "BILL_PAID" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                        Exit Sub
                    ElseIf ex.Message = "Refund_Payment" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been used in Refund. If you want to change or delete it, you must edit the Refund and remove it first.');", True)
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()
                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindBillsGrid("", "", False)
            BindCreditgrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            SaveSelectedBills()

            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub SaveSelectedBills()
        Try
            Dim listSelectedBills As System.Collections.Generic.List(Of SelectedBill)

            If Not String.IsNullOrEmpty(UserSelectedBills.Trim()) Then
                listSelectedBills = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedBill))(UserSelectedBills.Trim())
            Else
                listSelectedBills = New Generic.List(Of SelectedBill)
            End If

            Dim objSelectedBill As SelectedBill
            For Each item As DataGridItem In dgBills.Items
                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    If listSelectedBills.Where(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                        objSelectedBill = listSelectedBills.First(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                        objSelectedBill.monAmountToPay = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                    Else
                        objSelectedBill = New SelectedBill
                        objSelectedBill.numDivisionID = CCommon.ToLong(CType(item.FindControl("hdnDivisionID"), HiddenField).Value)
                        objSelectedBill.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value)
                        objSelectedBill.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                        objSelectedBill.monAmountToPay = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                        objSelectedBill.tintBillType = CCommon.ToShort(CType(item.FindControl("hdnBillType"), HiddenField).Value)
                        objSelectedBill.numExchangeRateOfOrder = CCommon.ToDecimal(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                        objSelectedBill.vcBizDocName = CCommon.ToString(CType(item.FindControl("lblBizDocName"), Label).Text)

                        listSelectedBills.Add(objSelectedBill)
                    End If
                ElseIf listSelectedBills.Where(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count > 0 Then
                    listSelectedBills.Remove(listSelectedBills.First(Function(x) x.numBillID = CCommon.ToLong(CType(item.FindControl("hdnBillID"), HiddenField).Value) AndAlso x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)))
                End If
            Next

            If listSelectedBills.Count > 0 Then
                UserSelectedBills = Newtonsoft.Json.JsonConvert.SerializeObject(listSelectedBills)
            Else
                UserSelectedBills = ""
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        lblError.Text = ex
        divError.Style.Add("display", "")
        divError.Focus()
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnGo1_Click1(sender As Object, e As EventArgs)
        Try
            ClearGridFilters()
            BindBillsGrid("", "", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub dgBills_SortCommand(source As Object, e As DataGridSortCommandEventArgs)
        Dim sortData As String()
        If (Session("sortdirection") IsNot Nothing) Then
            sortData = Session("sortdirection").ToString().Trim().Split("_")

            If (e.SortExpression = sortData(0).ToString) Then
                If (sortData(1) = "ASC") Then

                    BindBillsGrid(e.SortExpression, "DESC", True)
                    Session("sortdirection") = e.SortExpression + " " + "DESC"
                Else
                    BindBillsGrid(e.SortExpression, "ASC", True)
                    Session("sortdirection") = e.SortExpression + " " + "ASC"

                End If
            Else
                BindBillsGrid(e.SortExpression, "ASC", True)
                Session("sortdirection") = e.SortExpression + "_" + "ASC"
            End If
        Else
            BindBillsGrid(e.SortExpression, "ASC", True)
            Session("sortdirection") = e.SortExpression + "_" + "ASC"
        End If

    End Sub

    Protected Sub txtVendorFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtVendorFliter As TextBox
            txtVendorFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtVendorFliter"), TextBox)
            hdnVendorFliter.Value = txtVendorFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtBizDocStatusFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtBizDocStatusFliter As TextBox
            txtBizDocStatusFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtBizDocStatusFliter"), TextBox)
            hdnBizDocStatusFliter.Value = txtBizDocStatusFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtReferenceFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtReferenceFliter As TextBox
            txtReferenceFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtReferenceFliter"), TextBox)
            hdnReferenceFliter.Value = txtReferenceFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtBillAmountFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtBillAmountFliter As TextBox
            txtBillAmountFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtBillAmountFliter"), TextBox)
            hdnBillAmountFliter.Value = txtBillAmountFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtAmtDueFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtAmtDueFliter As TextBox
            txtAmtDueFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtAmtDueFliter"), TextBox)
            hdnAmtDueFliter.Value = txtAmtDueFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtAmttoPayFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtAmttoPayFliter As TextBox
            txtAmttoPayFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtAmttoPayFliter"), TextBox)
            hdnAmttoPayFliter.Value = txtAmttoPayFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtBillFliter_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim txtBillFliter As TextBox
            txtBillFliter = CType(dgBills.Controls(0).Controls(0).FindControl("txtBillFliter"), TextBox)
            hdnBillFliter.Value = txtBillFliter.Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtFromDate_TextChanged(sender As Object, e As EventArgs)
        Try
            hdnFromDateFilter.Value = CType(dgBills.Controls(0).Controls(0).FindControl("txtFromDate"), TextBox).Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtToDate_TextChanged(sender As Object, e As EventArgs)
        Try
            hdnToDateFilter.Value = CType(dgBills.Controls(0).Controls(0).FindControl("txtToDate"), TextBox).Text
            BindBillsGrid("", "", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ClearGridFilters()
        Dim txtVendorFliter As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtVendorFliter"), TextBox)
        Dim txtFromDate As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtFromDate"), TextBox)
        Dim txtToDate As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtToDate"), TextBox)
        Dim txtReferenceFliter As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtReferenceFliter"), TextBox)
        Dim txtBillAmountFliter As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtBillAmountFliter"), TextBox)
        Dim txtAmtDueFliter As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtAmtDueFliter"), TextBox)
        Dim txtAmttoPayFliter As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtAmttoPayFliter"), TextBox)
        Dim txtBillFliter As TextBox = CType(dgBills.Controls(0).Controls(0).FindControl("txtBillFliter"), TextBox)

        txtVendorFliter.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        txtReferenceFliter.Text = ""
        txtBillAmountFliter.Text = ""
        txtAmtDueFliter.Text = ""
        txtAmttoPayFliter.Text = ""
        txtBillFliter.Text = ""
        hdnVendorFliter.Value = ""
        hdnFromDateFilter.Value = ""
        hdnToDateFilter.Value = ""
        hdnReferenceFliter.Value = ""
        hdnBillAmountFliter.Value = ""
        hdnAmtDueFliter.Value = ""
        hdnAmttoPayFliter.Value = ""
        hdnBillFliter.Value = ""

    End Sub

    Private Class SelectedBill
        Public Property numDivisionID As Long
        Public Property numBillID As Long
        Public Property numOppBizDocsID As Long
        Public Property monAmountToPay As Decimal
        Public Property tintBillType As Short
        Public Property numExchangeRateOfOrder As Decimal
        Public Property vcBizDocName As String
    End Class
End Class