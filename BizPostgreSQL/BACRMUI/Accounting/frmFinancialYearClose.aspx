﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFinancialYearClose.aspx.vb"
    Inherits=".frmFinancialYearClose" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <style>
         .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <script language="Javascript" type="text/javascript">
        function Save() {
//            if (document.form1.txtDesc.value == "") {
//                alert("Enter Description");
//                document.form1.txtDesc.focus();
//                return false;
//            }
//            if (document.form1.calFrom_txtDate.value == "") {
//                alert("Enter From Period");
//                document.form1.calFrom_txtDate.focus();
//                return false;
//            }
//            if (document.form1.calTo_txtDate.value == "") {
//                alert("Enter To Period");
//                document.form1.calTo_txtDate.focus();
//                return false;
//            }

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <div class="form-inline">
            <asp:LinkButton ID="btnSave" runat="server" CssClass="button btn btn-primary" Text="" OnClientClick="return Save();">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close       </asp:LinkButton>
            <asp:LinkButton ID="btnBack" runat="server" CssClass="button btn btn-primary" Text="Back">
                <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
            </asp:LinkButton>
            <a href="#" class="help">&nbsp;</a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12">
        <center>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
        </center>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Close Financial Year
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6">
        <div class="form-group">
            <asp:Label runat="server" ID="lblDesc"></asp:Label>
        </div>
        <div class="form-group">
            <label class="col-md-4"> Transfer Closing To</label>
            <div class="col-md-8">
                <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="signup form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4">
                Close Type
            </label>
            <div class="col-md-8">
                 <asp:RadioButtonList ID="rblCloseType" runat="server" CssClass="signup" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Text="Trial Close" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Audit Close" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnCurrentFinYearID" runat="server" />
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
