' ''Created By Siva
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Common
'Partial Public Class frmRecurringTransaction
'    Inherits BACRMPage

'#Region "Variables"

'    Dim objCommon As CCommon
'    Dim objRecurringEvents As RecurringEvents
'#End Region

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            'To Set Permission
'            objCommon = New CCommon
'            GetUserRightsForPage(35, 94)
'            If Not IsPostBack Then

'                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
'                LoadGridDetails()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub LoadGridDetails()
'        Dim dtTempDetails As DataTable
'        Try
'            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
'            Select Case ddlInterval.SelectedIndex
'                Case 0
'                    objRecurringEvents.IntervalType = "A"
'                Case 1
'                    objRecurringEvents.IntervalType = "D"
'                Case 2
'                    objRecurringEvents.IntervalType = "M"
'                Case 3
'                    objRecurringEvents.IntervalType = "Y"
'            End Select
'            objRecurringEvents.CustomerOrVendorSearch = txtCustomerSearch.Text
'            objRecurringEvents.ChecksOrCashOrJournal = ddlTransaction.SelectedItem.Value
'            objRecurringEvents.DomainId = Session("DomainId")
'            objRecurringEvents.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

'            dtTempDetails = objRecurringEvents.GetRecurringTemplateTransactionDetails()
'            dgRecurringTransaction.DataSource = dtTempDetails
'            dgRecurringTransaction.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub dgRecurringTransaction_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRecurringTransaction.ItemCommand
'        Try
'            Dim linJournalId As Integer
'            Dim lintCheckId As Integer
'            Dim lintCashCreditCardId As Integer
'            Dim lintDepositId As Integer
'            Dim lintOppID As Integer
'            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents

'            lintCashCreditCardId = IIf(e.Item.Cells(1).Text = "&nbsp;" OrElse e.Item.Cells(1).Text = "", 0, e.Item.Cells(1).Text)
'            lintCheckId = IIf(e.Item.Cells(2).Text = "&nbsp;" OrElse e.Item.Cells(2).Text = "", 0, e.Item.Cells(2).Text)
'            linJournalId = IIf(e.Item.Cells(3).Text = "&nbsp;" OrElse e.Item.Cells(3).Text = "", 0, e.Item.Cells(3).Text)
'            lintDepositId = IIf(e.Item.Cells(4).Text = "&nbsp;" OrElse e.Item.Cells(4).Text = "", 0, e.Item.Cells(4).Text)
'            lintOppID = IIf(e.Item.Cells(5).Text = "&nbsp;" OrElse e.Item.Cells(5).Text = "", 0, e.Item.Cells(5).Text)
'            If e.CommandName = "Edit" Then
'                If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintDepositId = 0 And lintOppID = 0 Then
'                    Response.Redirect("../Accounting/frmNewJournalEntry.aspx?frm=RecurringTransaction&JournalId=" & linJournalId)
'                ElseIf lintCheckId <> 0 Then
'                    Response.Redirect("../Accounting/frmChecks.aspx?frm=RecurringTransaction&CheckId=" & lintCheckId & "&JournalId=" & linJournalId)
'                ElseIf lintCashCreditCardId <> 0 Then
'                    Response.Redirect("../Accounting/frmCash.aspx?frm=RecurringTransaction&CashCreditCardId=" & lintCashCreditCardId & "&JournalId=" & linJournalId)
'                ElseIf lintDepositId <> 0 Then
'                    Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=RecurringTransaction&JournalId=" & linJournalId & "&DepositId=" & lintDepositId)
'                Else
'                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=RecurringTransaction&Opid=" & lintOppID)
'                End If

'            ElseIf e.CommandName = "Delete" Then
'                If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppID = 0 Then
'                    objRecurringEvents.AccountTypeMode = 1 '' For Journal Entry
'                    objRecurringEvents.PrimaryKeyId = linJournalId
'                    objRecurringEvents.DomainId = Session("DomainId")
'                    objRecurringEvents.UpdateRecurringTemplateForRecurringTransaction()
'                ElseIf lintCheckId <> 0 Then
'                    objRecurringEvents.PrimaryKeyId = lintCheckId
'                    objRecurringEvents.AccountTypeMode = 2 '' For Journal Checks
'                    objRecurringEvents.DomainId = Session("DomainId")
'                    objRecurringEvents.UpdateRecurringTemplateForRecurringTransaction()
'                ElseIf lintCashCreditCardId <> 0 Then
'                    objRecurringEvents.PrimaryKeyId = lintCashCreditCardId
'                    objRecurringEvents.AccountTypeMode = 3 '' For  Cash Or Credit Card Details
'                    objRecurringEvents.DomainId = Session("DomainId")
'                    objRecurringEvents.UpdateRecurringTemplateForRecurringTransaction()
'                ElseIf lintOppID <> 0 Then
'                    objRecurringEvents.PrimaryKeyId = lintOppID
'                    objRecurringEvents.AccountTypeMode = 4 '' For OpportunityMaster
'                    objRecurringEvents.DomainId = Session("DomainId")
'                    objRecurringEvents.UpdateRecurringTemplateForRecurringTransaction()
'                End If
'                LoadGridDetails()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgRecurringTransaction_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRecurringTransaction.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim btnDelete As New Button
'                Dim lnkDelete As LinkButton
'                btnDelete = CType(e.Item.FindControl("btnDelete"), Button)
'                lnkDelete = e.Item.FindControl("lnkdelete")

'                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
'                    btnDelete.Visible = False
'                    lnkDelete.Visible = True
'                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
'                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
'                End If
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Function ReturnDate(ByVal CloseDate) As String
'        Try
'            Dim strTargetResolveDate As String = ""
'            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
'            Return strTargetResolveDate
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Function ReturnDateTime(ByVal USerCntName, ByVal CloseDate) As String
'        Try
'            Dim strTargetResolveDate As String = ""
'            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
'            Return USerCntName & "  " & strTargetResolveDate
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub ddlTransaction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTransaction.SelectedIndexChanged
'        Try
'            If ddlTransaction.SelectedItem.Value = 3 Then
'                ddlInterval.Items.Clear()
'                ddlInterval.Items.Insert(0, "All")
'                ddlInterval.Items.FindByText("All").Value = "0"

'                ddlInterval.Items.Insert(1, "Daily")
'                ddlInterval.Items.Insert(2, "Monthly")
'                ddlInterval.Items.Insert(3, "Yearly")
'                lblTransaction.Visible = True
'                ddlInterval.Visible = True
'                ddlAcntType.Visible = False
'                lblTransaction.Text = "Interval"
'                LoadGridDetails()

'            ElseIf ddlTransaction.SelectedItem.Value = 4 Then
'                lblTransaction.Visible = True
'                ddlAcntType.Visible = True
'                ddlInterval.Visible = False
'                lblTransaction.Text = "Account Types"
'                LoadChartType(ddlAcntType)
'                If ddlInterval.SelectedIndex <> -1 Then ddlInterval.SelectedIndex = 0

'                ddlAcntType.SelectedIndex = 0
'                LoadGridDetails()
'                'ElseIf ddlTransaction.SelectedItem.Value <> 2 Then
'                '    lblTransaction.Visible = False
'                '    ddlAcntType.Visible = False
'                '    ddlInterval.Visible = False
'                '    LoadGridDetails()
'            Else
'                lblTransaction.Visible = False
'                ddlAcntType.Visible = False
'                ddlInterval.Visible = False
'                LoadGridDetails()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub LoadChartType(ByVal ddlAccounts As DropDownList)
'        Try
'            Dim objJournalEntry As New JournalEntry
'            objJournalEntry.ParentRootNode = 1
'            objJournalEntry.DomainId = Session("DomainId")
'            ddlAccounts.DataSource = objJournalEntry.GetChartOfAccountsDetails()
'            ddlAccounts.DataTextField = "vcCategoryName"
'            ddlAccounts.DataValueField = "numChartOfAcntID"
'            ddlAccounts.DataBind()
'            ddlAccounts.Items.Insert(0, New ListItem("--Select All--", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub ddlAcntType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcntType.SelectedIndexChanged
'        Try
'            If Not IsDBNull(ddlAcntType.SelectedItem.Value) Then
'                If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
'                objRecurringEvents.IntervalType = "A"
'                If ddlAcntType.SelectedItem.Value.ToString <> "0" Then
'                    objRecurringEvents.AcntTypeId = ddlAcntType.SelectedItem.Value.Split("~")(0)
'                Else : objRecurringEvents.AcntTypeId = 0
'                End If

'                objRecurringEvents.ChecksOrCashOrJournal = ddlTransaction.SelectedItem.Value
'                objRecurringEvents.DomainId = Session("DomainId")
'                dgRecurringTransaction.DataSource = objRecurringEvents.GetRecurringTemplateTransactionDetails()
'                dgRecurringTransaction.DataBind()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub ddlInterval_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInterval.SelectedIndexChanged
'        Try
'            If Not IsDBNull(ddlInterval.SelectedIndex) Then
'                Dim dtTempDetails As DataTable
'                If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
'                Select Case ddlInterval.SelectedIndex
'                    Case 0
'                        objRecurringEvents.IntervalType = "A"
'                    Case 1
'                        objRecurringEvents.IntervalType = "D"
'                    Case 2
'                        objRecurringEvents.IntervalType = "M"
'                    Case 3
'                        objRecurringEvents.IntervalType = "Y"
'                End Select

'                ''objRecurringEvents.AcntTypeId = ddlAcntType.SelectedItem.Value
'                objRecurringEvents.ChecksOrCashOrJournal = ddlTransaction.SelectedItem.Value
'                objRecurringEvents.DomainId = Session("DomainId")
'                dtTempDetails = objRecurringEvents.GetRecurringTemplateTransactionDetails()
'                dgRecurringTransaction.DataSource = dtTempDetails
'                dgRecurringTransaction.DataBind()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
'        Try
'            LoadGridDetails()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Function ReturnMoney(ByVal IntervalType, ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then Return IntervalType & "  " & String.Format("{0:#,###.00}", Money)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'End Class
