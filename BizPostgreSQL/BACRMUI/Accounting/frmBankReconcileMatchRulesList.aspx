﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmBankReconcileMatchRulesList.aspx.vb" Inherits=".frmBankReconcileMatchRulesList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <script type="text/javascript">
        function AddMatchRuleCondition() {


            if ($("[id$=txtText]").val() == "") {
                alert("Bank reconcile matching text is required.");
                $("[id$=txtText]").focus();
                return false;
            }

            var radCmbCompany = $find('<%= radCmbCompany.ClientID %>');
            if (radCmbCompany.get_value() == null || radCmbCompany.get_value().length == 0) {
                alert("Select organization.");
                return false;
            }

            return true;
        }

        function validateReconcileRuleSave() {
            if ($("[id$=txtRuleName]").val() == "") {
                alert("Rule name is required.");
                $("[id$=txtRuleName]").focus();
                return false;
            }

            var Combo = $find("<%= radBankAccounts.ClientID%>");
            if (Combo.get_checkedItems().length == 0) {
                alert("Select bank account(s).");
                return false;
            }


            if ($("#gvConditions tr").length > 1 && $("#gvConditions tr")[1].innerText == 'No Data Available') {
                alert("Add atleast one condition for rule.");
                return false;
            }

            return true;
        }

        function SelectAllBankAccounts(chkAll) {
            var Combo = $find("<%= radBankAccounts.ClientID%>");
            var items = Combo.get_items();
            var text = ''

            for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                if ($(chkAll).is(':checked')) {
                    items._array[x].check();
                    text = text + (text == "" ? items._array[x]._element.textContent : ", " + items._array[x]._element.textContent);
                }
                else
                    items._array[x].uncheck();
            }

            Combo.set_text(text);

            return true;
        }
    </script>
    <style type="text/css">
        .RadComboBoxDropDown_Default .rcbHeader, .RadComboBoxDropDown_Default .rcbFooter, .RadComboBoxDropDown_Default .rcbMoreResults, .RadComboBoxDropDown_Default .rcbMoreResults a {
            background-image: none !important;
        }

        .rcbItem, .rcbHovered {
            height: 30px;
        }

        .RadComboBoxDropDown .rcbCheckBox {
            vertical-align: auto !important;
            margin-right: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12 text-right">
            <asp:LinkButton ID="lkbUpdatePriority" class="btn btn-primary" runat="server">Update Priority</asp:LinkButton>
            <asp:LinkButton ID="lkbAddRule" class="btn btn-primary" runat="server">Add Rule</asp:LinkButton>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Bank Reconcile Matching Rules
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvRules" runat="server" CssClass="table table-responsive table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:BoundField DataField="vcName" HeaderText="Rule" />
                        <asp:BoundField DataField="vcBankAccounts" HeaderText="Bank Accounts" />
                        <asp:BoundField DataField="vcMatchAll" HeaderText="Match All Conditions" ItemStyle-Width="180" />
                        <asp:TemplateField HeaderText="Conditions" ItemStyle-Width="80">
                            <ItemTemplate>
                                <a href="#" data-toggle="modal" data-target='<%# "#divRule_" & Convert.ToString(Eval("ID"))%>' style="cursor: hand">Conditions</a>
                                <div id='<%# "divRule_" & Convert.ToString(Eval("ID"))%>' class="modal fade" role="dialog">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" style="font-size: 27px">&times;</button>
                                                <h4 class="modal-title">Conditions</h4>
                                            </div>
                                            <div class="modal-body">
                                                <%#Eval("vcConditions")%>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority" ItemStyle-Width="50">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnRuleID" runat="server" Value='<%# Convert.ToString(Eval("ID"))%>' />
                                <telerik:RadNumericTextBox runat="server" ID="radPriority" Width="48" Value='<%# Convert.ToString(Eval("tintOrder"))%>' NumberFormat-DecimalDigits="0" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkbDelete" ClientIDMode="AutoID" CssClass="btn btn-xs btn-danger" CommandName="Delete" CommandArgument='<%# Eval("ID") %>' runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No data available
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div id="divRules" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="modal-content" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Modal content-->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" style="font-size: 27px">&times;</button>
                        <h4 class="modal-title">Bank Reconcile Match Rules</h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        <div class="row padbottom10">
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label>Rule Name</label>
                                    <asp:TextBox ID="txtRuleName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label>Bank Accounts</label>
                                    <div>
                                        <telerik:RadComboBox ID="radBankAccounts" Width="100%" runat="server" CheckBoxes="true" EnableCheckAllItemsCheckBox="true">
                                            <FooterTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" Text="&nbsp;&nbsp;Check All" onclick="return SelectAllBankAccounts(this);" />
                                            </FooterTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label>Match All Conditions</label>
                                    <div>
                                        <asp:CheckBox ID="chkMatchAll" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row padbottom10">
                            <div class="col-xs-12">
                                <div class="form-inline">
                                    <label>when</label>
                                    <asp:DropDownList ID="ddlStatementFields" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Payee" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Description" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Reference" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                        <asp:DropDownList ID="ddlMathCondition" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="contains" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="equals" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="starts with" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="ends with" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    &nbsp;
                                        <asp:TextBox ID="txtText" runat="server" CssClass="form-control" placeholder="enter text to match" Width="250"></asp:TextBox>
                                    &nbsp;<b>than select organization</b>&nbsp;
                                        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="200" DropDownWidth="600px"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                            ClientIDMode="Static" TabIndex="1">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>
                                    <asp:LinkButton ID="lkbAdd" runat="server" CssClass="btn btn-primary" OnClientClick="return AddMatchRuleCondition();">Add</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvConditions" AutoGenerateColumns="false" runat="server" CssClass="table table-responsive table-bordered" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true">
                                        <Columns>
                                            <asp:BoundField DataField="vcCondition" HeaderText="Rule Match Conditions" HtmlEncode="false" />
                                            <asp:TemplateField ItemStyle-Width="25">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lkbDelete" ClientIDMode="AutoID" CssClass="btn btn-xs btn-danger" CommandName="Delete" CommandArgument='<%# Eval("numConditionID") %>' runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Data Available
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default pull-left" Text="Close" />
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save & Close" OnClientClick="return validateReconcileRuleSave()" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
