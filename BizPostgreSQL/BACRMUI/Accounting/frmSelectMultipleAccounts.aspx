﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSelectMultipleAccounts.aspx.vb"
    Inherits=".frmSelectMultipleAccounts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <script>
        function Close() {
            window.close();
            return false;
        }
        function Select(Select){
            for (var n=0; n < document.forms[0].length; n++) if (document.forms[0].elements[n].type=='checkbox')
            document.forms[0].elements[n].checked=Select; return false; }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td class="normal1">
                Select <a href="#" onclick="javascript:Select(true)">All</a> | <a href="#" onclick="javascript:Select(false)">
                    None</a>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBoxList ID="cblAccounts" runat="server" CssClass="signup">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr align="center">
            <td class="normal4" valign="middle">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
