﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewFinancialYear.aspx.vb"
    Inherits=".frmNewFinancialYear" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" src="../JavaScript/dateFormat.js"></script>

    <script type="text/javascript">
        function Save(format) {
            if (document.form1.txtDesc.value == "") {
                alert("Enter Period Name");
                document.form1.txtDesc.focus();
                return false;
            }
            if (document.form1.ctl00_ctl00_MainContent_GridPlaceHolder_calFrom_txtDate.value == "") {
                alert("Enter From Period");
                document.form1.calFrom_txtDate.focus();
                return false;
            }
            if (document.form1.ctl00_ctl00_MainContent_GridPlaceHolder_calTo_txtDate.value == "") {
                alert("Enter To Period");
                document.form1.calTo_txtDate.focus();
                return false;
            }
            if (compareDates(document.form1.ctl00_ctl00_MainContent_GridPlaceHolder_calFrom_txtDate.value, format, document.form1.ctl00_ctl00_MainContent_GridPlaceHolder_calTo_txtDate.value, format) == 1) {
                alert("To Period must be greater than or equal to From Period");
                return false;
            }

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnFYClosing" runat="server" CssClass="button btn btn-primary" Text="Financial Year Closing"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Manage Accounting Periods&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmnewfinancialyear.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Period Name:</label>
                    <asp:TextBox runat="server" ID="txtDesc" CssClass="form-control"></asp:TextBox>
                    <label style="color: gray"><small>i.e. FY 2011</small></label>
                </div>
                &nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <label>From Date:</label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                </div>
                &nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <label>To Date:</label>
                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                </div>
                &nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <label>Is Current Financial Year?</label>
                    <asp:RadioButtonList ID="rblFY" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                        <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="pull-right">
                    <asp:Button ID="btnSave" runat="server" CssClass="button btn btn-primary" Text="Save Financial Year"></asp:Button>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgFinYear" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" runat="server" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn DataField="vcFinYearDesc" HeaderText="Period Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtPeriodFrom" HeaderText="From Date"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtPeriodTo" HeaderText="To Date"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CurrentYear" HeaderText="Is Current Financial Year" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcCloseStatus" HeaderText="Is Trial Closed" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcAuditStatus" HeaderText="Is Audit Closed" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="65">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CssClass="btn btn-info btn-xs" CommandName="Edit" ID="lnkbtnEdt" CommandArgument='<%# Eval("numFinYearId") %>'><i class="fa fa-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete" CommandArgument='<%# Eval("numFinYearId") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hdnNextFYFromDate" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>
