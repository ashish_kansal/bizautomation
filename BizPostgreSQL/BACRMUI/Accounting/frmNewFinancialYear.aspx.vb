﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmNewFinancialYear
    Inherits BACRMPage
    Dim lngFinYearID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(35, 109)
            If CCommon.ToLong(Session("DomainID")) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If Not IsPostBack Then
                BindData()

            End If
            If Not ViewState("FinYearID") Is Nothing Then
                lngFinYearID = ViewState("FinYearID")
            End If
            btnSave.Attributes.Add("onclick", "return Save('" & CCommon.GetValidationDateFormat() & "')")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .FinancialYearID = 0
                .DomainId = Session("DomainID")
                ds = .GetFinancialYear()
            End With
            dgFinYear.DataSource = ds
            dgFinYear.DataBind()

            hdnNextFYFromDate.Value = ""
            If ds.Tables(0).Rows.Count > 0 Then
                objCOA.FinancialYearID = 0
                objCOA.DomainId = Session("DomainID")
                objCOA.Mode = 3
                ds = objCOA.GetFinancialYear()
                If ds.Tables(0).Rows.Count > 0 Then
                    calFrom.SelectedDate = ds.Tables(0).Rows(0)("NextFinYearFromDate").ToString
                    hdnNextFYFromDate.Value = ds.Tables(0).Rows(0)("NextFinYearFromDate").ToString
                    ClientScript.RegisterStartupScript(Me.GetType, "disable", "document.form1.ctl00_ctl00_MainContent_GridPlaceHolder_calFrom_txtDate.disabled=true;document.form1.ctl00_ctl00_MainContent_GridPlaceHolder_calFrom_imgDate.style.visibility=""hidden"";", True)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub LoadSavedInfo(ByVal FinYearID As Long)
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .FinancialYearID = FinYearID
                .DomainId = Session("DomainID")
                ds = .GetFinancialYear()
            End With

            If ds.Tables(0).Rows.Count > 0 Then
                calFrom.SelectedDate = ds.Tables(0).Rows(0).Item("dtPeriodFrom")
                calTo.SelectedDate = ds.Tables(0).Rows(0).Item("dtPeriodTo")
                txtDesc.Text = ds.Tables(0).Rows(0).Item("vcFinYearDesc")
                rblFY.SelectedValue = CInt(ds.Tables(0).Rows(0).Item("bitCurrentYear"))
                ViewState("FinYearID") = ds.Tables(0).Rows(0).Item("numFinYearId")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objCOA As New ChartOfAccounting
            With objCOA
                .FinancialYearID = lngFinYearID
                .FinDescription = txtDesc.Text
                .FromDate = IIf(calFrom.SelectedDate = "", hdnNextFYFromDate.Value, calFrom.SelectedDate)
                .ToDate = CDate(calTo.SelectedDate & " 23:59:59")
                .bitCurrentYear = rblFY.SelectedValue
                .DomainId = Session("DomainID")
                lngFinYearID = .ManageFinancialYear()

                lngFinYearID = 0
                txtDesc.Text = ""
                calFrom.SelectedDate = ""
                calTo.SelectedDate = ""
                rblFY.SelectedValue = 0
                ViewState("FinYearID") = "0"
            End With
            BindData()
            calTo.SelectedDate = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgFinYear_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFinYear.ItemCommand
        Try
            Dim objCOA As New ChartOfAccounting
            If e.CommandName = "Edit" Then
                LoadSavedInfo(e.CommandArgument)
            ElseIf e.CommandName = "Delete" Then
                Try
                    objCOA.FinancialYearID = e.CommandArgument
                    objCOA.DomainId = Session("DomainID")
                    objCOA.DeleteFinancialYear()
                Catch ex As Exception
                    If ex.Message = "IS_CURRENT_YEAR" Then
                        litMessage.Text = "Can not delete Current Financial Year"
                    Else
                        Throw ex
                    End If
                End Try
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgFinYear_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFinYear.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnFYClosing_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFYClosing.Click
        Try
            Response.Redirect("../Accounting/frmFinancialYearClose.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class