﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSortAccounts.aspx.vb" Inherits=".frmSortAccounts"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Sort Chart of Accounts</title>
</head>
<body>
    <form id="form1" runat="server">
    <br />
        <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td align="right">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
                </td>
            </tr>
        </table>
        &nbsp;
        <table id="Table2" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
                        <asp:TableRow>
                            <asp:TableCell>
                                <br />
                                <table cellspacing="2" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="normal1">
                                        <asp:RadioButtonList ID="rdSortType" runat="server">
                                            <asp:ListItem Value="ID">Sort tree by numbered prefixes.</asp:ListItem>
                                           <asp:ListItem Value="DESCRIPTION">Sort tree based on alphabetical.</asp:ListItem>
                                        </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
