﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports System.Collections.Generic

Public Class frmBankReconcileMatchRulesList
    Inherits BACRMPage
#Region "Member Variables"
    Private dtRuleCondition As DataTable
#End Region

#Region "Page Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lblError.Text = ""

            If Not Page.IsPostBack Then
                Dim dt As New DataTable("Condition")
                dt.Columns.Add("numConditionID")
                dt.Columns.Add("vcCondition")
                dt.Columns.Add("tintColumn")
                dt.Columns.Add("tintConditionOperator")
                dt.Columns.Add("vcTextToMatch")
                dt.Columns.Add("numDivisionID")
                dt.Columns.Add("vcCompanyName")
                ViewState("RuleCondition") = dt
                dtRuleCondition = dt

                BindRules()
                'binding empty list on initial load
                BindConditionData()
                BindBankAccounts()
            End If

            dtRuleCondition = ViewState("RuleCondition")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindRules()
        Try
            Dim objBankRecocileMatchRule As New BankReconcileMatchRule
            objBankRecocileMatchRule.DomainID = CCommon.ToLong(Session("DomainID"))
            gvRules.DataSource = objBankRecocileMatchRule.GetAll()
            gvRules.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindConditionData()
        Try
            gvConditions.DataSource = dtRuleCondition
            gvConditions.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindBankAccounts()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim dt, dt1 As DataTable
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "01010101" 'bank accounts
            dt = objCOA.GetParentCategory()

            objCOA.AccountCode = "01020103" 'For credit card liability
            dt1 = objCOA.GetParentCategory()
            dt.Merge(dt1)

            Dim item As RadComboBoxItem
            For Each dr As DataRow In dt.Rows
                item = New RadComboBoxItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                radBankAccounts.Items.Add(item)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ClearFields()
        Try
            txtRuleName.Text = ""
            radBankAccounts.ClearCheckedItems()
            chkMatchAll.Checked = False
            txtText.Text = ""
            radCmbCompany.Text = ""
            radCmbCompany.ClearSelection()
            dtRuleCondition.Rows.Clear()
            ViewState("RuleCondition") = dtRuleCondition
            BindConditionData()
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Private Sub lkbAdd_Click(sender As Object, e As EventArgs) Handles lkbAdd.Click
        Try
            If txtText.Text = "" Then
                lblError.Text = "Bank reconcile matching text is required."
                Exit Sub
            End If

            If radCmbCompany.SelectedValue = "" Then
                lblError.Text = "Select organization."
                Exit Sub
            End If

            Dim dr As DataRow = dtRuleCondition.NewRow()
            dr("numConditionID") = dtRuleCondition.Rows.Count + 1
            dr("vcCondition") = "When <b>" & ddlStatementFields.SelectedItem.Text & "</b> " & ddlMathCondition.SelectedItem.Text & " <b>" & txtText.Text & "</b> than select organization <b>" & radCmbCompany.Text & "</b>"
            dr("tintColumn") = ddlStatementFields.SelectedValue
            dr("tintConditionOperator") = ddlMathCondition.SelectedValue
            dr("vcTextToMatch") = txtText.Text.Trim()
            dr("numDivisionID") = radCmbCompany.SelectedValue
            dr("vcCompanyName") = radCmbCompany.Text
            dtRuleCondition.Rows.Add(dr)

            ViewState("RuleCondition") = dtRuleCondition

            BindConditionData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If txtRuleName.Text = "" Then
                lblError.Text = "Rule name is required."
                Exit Sub
            End If

            If radBankAccounts.CheckedItems.Count = 0 Then
                lblError.Text = "Select bank account(s)."
                Exit Sub
            End If

            If dtRuleCondition.Rows.Count = 0 Then
                lblError.Text = "Add atleast one condition for rule."
                Exit Sub
            End If

            'TODO: Save Rule
            Dim objBankRecocileMatchRule As New BankReconcileMatchRule
            objBankRecocileMatchRule.DomainID = CCommon.ToLong(Session("DomainID"))
            objBankRecocileMatchRule.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objBankRecocileMatchRule.Name = txtRuleName.Text
            For Each item As RadComboBoxItem In radBankAccounts.CheckedItems
                objBankRecocileMatchRule.BankAccounts = objBankRecocileMatchRule.BankAccounts & If(String.IsNullOrEmpty(objBankRecocileMatchRule.BankAccounts), item.Value, "," & item.Value)
            Next
            objBankRecocileMatchRule.IsMatchAllConditions = chkMatchAll.Checked

            Dim ds As New DataSet
            Dim dt As New DataTable("Condition")
            dt = dtRuleCondition.Copy()
            ds.Tables.Add(dt)

            objBankRecocileMatchRule.RuleConditions = ds.GetXml()
            objBankRecocileMatchRule.Save()

            BindRules()
            ClearFields()
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "HideModal", "$('#divRules').modal('hide'); document.location.href=document.location.href;", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            ClearFields()
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "HideModal", "$('#divRules').modal('hide'); ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub

    Private Sub lkbAddRule_Click(sender As Object, e As EventArgs) Handles lkbAddRule.Click
        Try
            ClearFields()
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, UpdatePanel1.GetType(), "HideModal", "$('#divRules').modal('show'); ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbUpdatePriority_Click(sender As Object, e As EventArgs) Handles lkbUpdatePriority.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable("Rule")
            dt.Columns.Add("numRuleID")
            dt.Columns.Add("tintOrder")

            For Each gridViewRow As GridViewRow In gvRules.Rows
                Dim ruleID As Long = CCommon.ToLong(DirectCast(gridViewRow.FindControl("hdnRuleID"), HiddenField).Value)
                Dim tintOrder As Integer = CCommon.ToInteger(DirectCast(gridViewRow.FindControl("radPriority"), RadNumericTextBox).Value)

                If tintOrder = 0 Then
                    DisplayError("Priority must be greater than 0.")
                    Exit Sub
                ElseIf dt.Select("tintOrder=" & tintOrder).Length > 0 Then
                    DisplayError("Duplicate priority value specified.")
                    Exit Sub
                End If

                Dim dr As DataRow = dt.NewRow()
                dr("numRuleID") = ruleID
                dr("tintOrder") = tintOrder
                dt.Rows.Add(dr)
            Next

            ds.Tables.Add(dt)

            Dim objBankRecocileMatchRule As New BankReconcileMatchRule
            objBankRecocileMatchRule.DomainID = CCommon.ToLong(Session("DomainID"))
            objBankRecocileMatchRule.UpdatePriority(ds.GetXml())

            BindRules()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvRules_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvRules.RowCommand
        Try
            Dim objBankRecocileMatchRule As New BankReconcileMatchRule
            objBankRecocileMatchRule.ID = CCommon.ToLong(e.CommandArgument)
            objBankRecocileMatchRule.DomainID = CCommon.ToLong(Session("DomainID"))
            objBankRecocileMatchRule.Delete()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub gvRules_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvRules.RowDeleting
        Try
            BindRules()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub gvConditions_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvConditions.RowCommand
        Try
            Dim arrRows As DataRow() = dtRuleCondition.Select("numConditionID=" & e.CommandArgument)
            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                dtRuleCondition.Rows.Remove(arrRows(0))
                ViewState("RuleCondition") = dtRuleCondition
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub gvConditions_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvConditions.RowDeleting
        Try
            BindConditionData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Try
    End Sub

#End Region

    
    
   

    
    
End Class