﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PaymentHistory.ascx.vb"
    Inherits=".PaymentHistory" %>
<script type="text/javascript">
    function OpenPaymentAmtPaid(a, b, c, d, e) {
        var str;
        if (a == 1) {
            str = '../Accounting/frmVendorPayment.aspx?BillPaymentID=' + b;
            window.opener.open(str)
        }
        else if (a == 2) {            
            str = '../Opportunity/frmAmtPaid.aspx?DepositeID=' + b + '&PaymentDate=' + c + '&PaymentTime=' + d + '&PaymentReciver=' + e;

            document.RPPopup = window.open(str, 'ReceivePayment', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
            console.log(doc.RPPopup);
        }
        return false;
    }
</script>
<div id="divPaymentsHistory" runat="server" visible="false" class="box box-primary">
    <div class="box-header with-border" style="color:#fff">
        <asp:Literal ID="lblPaymentTitle" runat="server"></asp:Literal>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <asp:GridView ID="dgPaymentDetailSalesOrder"  runat="server" CssClass="dg table table-bordered table-striped" Width="100%" AutoGenerateColumns="False" Visible="false">
                <Columns>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <a href="javascript:void(0)" onclick="return OpenPaymentAmtPaid('<%# Eval("tintRefType") %>','<%# Eval("numReferenceID") %>');">
                                <%# Eval("dtPaymentDate")%>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment #">
                        <ItemTemplate>
                            <%# Eval("vcReference")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amt Received" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <%# ReturnMoney(Eval("monAmountPaid"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Credit Amount Applied" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <%# HttpUtility.HtmlDecode(Eval("vcCredit"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paid against Invoice" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <%# ReturnMoney(Eval("monTotalAmount"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:DataGrid ID="dgPaymentDetails" runat="server" CssClass="dg table table-bordered table-striped" Width="100%" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Received on, by">
                        <ItemTemplate>
                            <table border="0">
                                <tr>
                                    <td>
                                        <a href="javascript:void(0)" onclick="return OpenPaymentAmtPaid('<%# Eval("tintRefType") %>','<%# Eval("numReferenceID") %>',' <%# Eval("dtPaymentDate") %>','<%# Eval("dtPaymentTime") %>','<%# Eval("vcPaymentReceiver") %>');">
                                            <%# Eval("dtPaymentDate") + ","%>
                                        </a>
                                    </td>
                                    <td>
                                         <asp:Label ID="lbldtPaymentTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "dtPaymentTime") + ","%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblpaymentreceiver" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcPaymentReceiver")%>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Payment #">
                        <ItemTemplate>
                            <%# Eval("vcReference")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Payment Amount" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <%# ReturnMoney(Eval("monPaymentTotal"))%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Amount Applied" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <%# ReturnMoney(Eval("monApplied"))%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
