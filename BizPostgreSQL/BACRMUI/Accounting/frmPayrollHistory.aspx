﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmPayrollHistory.aspx.vb" Inherits=".frmPayrollHistory" ValidateRequest="false"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Payroll History</title>
    <script language="javascript" type="text/javascript">
        //select all 
        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('#' + headerCheckBox).is(':checked'))
            });
        }

        function reDirectPage(a) {
            //parent.parent.frames['mainframe'].location.href = a;
            parent.location = a;
        }

        function OpenUserDetails(a) {
            window.open("../Accounting/frmEmpPayrollExpenses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1000,height=643,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenPayroll(a) {
            window.open("../Accounting/frmPayrollManage.aspx?PayrollId=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1024,height=700,scrollbars=yes,resizable=yes')
            return false;
            //window.location.href = "../Accounting/frmPayrollManage.aspx?PayrollId=" + a;
            //return false;
        }

        function pageLoaded() {
            $("#txtCheckNo").focus(function () {
                $("#rbHandCheck").prop("checked", true);
            });

            $('#rbHandCheck').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtCheckNo').val($('#hfCheckNo').val());
                }
            });

            $('#rbPrintedCheck').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtCheckNo').val('');
                }
            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);

        });

        function OpenAddPayroll() {
            window.open('../Accounting/frmPayrollManage.aspx', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
            return false;
        }

        function GrabHTML() {
            document.getElementById('hdnGridHTML').value = '' + document.getElementById("divGrid").innerHTML.replace(/https/gi, "http") + ''; //case in-sensetive replace
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Pay Period: From</label>
                    <BizCalendar:Calendar ID="calFromPayDate" runat="server" ClientIDMode="Predictable" />
                    <label>To</label>
                    <BizCalendar:Calendar ID="calToPayDate" runat="server" ClientIDMode="Predictable" />
                    <label>Employee Name</label>
                    <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    <label>Check Status</label>
                    <asp:DropDownList ID="ddlCheckStatus" runat="server" CssClass="form-control">
                        <asp:ListItem Value="3" Text="All" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Non Approved"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Approved"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Paid"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:LinkButton runat="server" ID="btnGo" Text="" CssClass=" btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                </div>
            </div>
            <div class="pull-right">
                <asp:HyperLink ID="hrfAddPayroll" NavigateUrl="#" CssClass="btn btn-primary" runat="server"
                    onclick="OpenAddPayroll();"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add Payroll</asp:HyperLink>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Payroll History
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:UpdatePanel ID="btnUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="btnApprove" runat="server" class="btn btn-md btn-success" Text=""><i class="fa fa-thumbs-up"></i>&nbsp;&nbsp;Approve</asp:LinkButton>&nbsp;&nbsp;
            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-md btn-danger" Text=""><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>&nbsp;&nbsp;
            <asp:LinkButton ID="btnExportExcel" runat="server" CssClass="btn btn-md btn-primary" OnClientClick="GrabHTML();"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnApprove" />
            <asp:PostBackTrigger ControlID="btnDelete" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div id="divGrid" class="table-responsive">
                <asp:GridView ID="gvPayrollList" runat="server" CssClass="table table-striped table-bordered" BorderWidth="0" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="numPayrollHeaderID,numPayrollDetailID" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Entry#">
                            <ItemTemplate>
                                <a href="#" onclick='return OpenPayroll(<%# Eval("numPayrollHeaderID")%>)'>
                                    <%# Eval("numPayrolllReferenceNo")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <a href="#" onclick='return OpenUserDetails(<%# Eval("numUserID")%>)'>
                                    <%# Eval("vcUserName")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee ID">
                            <ItemTemplate>
                                <%# Eval("vcEmployeeId")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pay Period">
                            <ItemTemplate>
                                <%# Eval("dtFromDate")%>
                                            -
                                            <%# Eval("dtToDate")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pay Date">
                            <ItemTemplate>
                                <%# Eval("dtPaydate")%>
                                <asp:HiddenField ID="hdnPaydate" runat="server" Value='<%# Eval("dtPaydate")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Payroll Amount">
                            <ItemTemplate>
                                <%# Eval("monTotalAmt")%>
                                <asp:HiddenField ID="hdnTotalAmt" runat="server" Value='<%# Eval("monTotalAmt")%>' />
                                <asp:HiddenField ID="hdnDeductions" runat="server" Value='<%# Eval("monDeductions")%>' />
                                <asp:HiddenField ID="hdnDivisionID" runat="server" Value='<%# Eval("numDivisionID")%>' />
                                <asp:HiddenField ID="hdnUserCntID" runat="server" Value='<%# Eval("numUserCntID")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Check Status">
                            <ItemTemplate>
                                <%# Eval("vcCheckStatus")%>
                                <asp:Label ID="lblCheckNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Width="10px" HeaderText="Approve/Delete">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll', 'chkSelect');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Width="10px" HeaderText="Write Check">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAllWriteCheck" runat="server" onclick="SelectAll('chkAllWriteCheck', 'chkWriteCheck');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkWriteCheck" CssClass="chkWriteCheck" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:HiddenField ID="hdnGridHTML" runat="server" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="form-inline">
                    <label>
                        Payment Account</label>
                    <asp:DropDownList ID="ddlPaymentFrom" runat="server" CssClass="form-control" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hfCheckNo" runat="server" Value="0" />

                </div>
                <div class="clearfix"></div>
                <div class="form-inline">
                    <label>
                        <asp:Label ID="lblBalance" runat="server"> </asp:Label></label><label><asp:Label ID="lblBalanceAmount"
                            runat="server" ForeColor="Green"></asp:Label></label>
                </div>
                <div class="form-inline">
                    <asp:RadioButton ID="rbPrintedCheck" Text="Printed check" runat="server" GroupName="Check"
                        Checked="true" />
                    <asp:RadioButton ID="rbHandCheck" Text="Hand written check" runat="server" GroupName="Check" />
                    [?]
                </div>
                <div class="clearfix"></div>
                <div class="form-inline">
                    <label>Starting Check #</label>
                    <asp:TextBox runat="server" ID="txtCheckNo" CssClass="form-control" onkeypress="CheckNumber(2,event)" />
                </div>
                <div class="clearfix"></div>
                <div class="form-inline">
                    <asp:Button ID="btnWriteCheck" runat="server" class="btn btn btn-warning" Text="Write Check"></asp:Button>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
