﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities

Public Class frmTaxReports
    Inherits BACRMPage

    Dim decSalesTaxTotal, decPurchasesTaxTotal As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())
                    End Try
                End If

                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub BindData()
        Try
            Dim objReport As New FinancialReport

            objReport.DomainID = Session("DomainID")
            objReport.FromDate = CDate(calFrom.SelectedDate)
            objReport.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            objReport.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            Dim dtReports As DataTable = objReport.GetTaxReports()

            If dtReports.Rows.Count > 0 Then
                decSalesTaxTotal = dtReports.Compute("Sum(SalesTax)", String.Empty)
                decPurchasesTaxTotal = dtReports.Compute("Sum(PurchaseTax)", String.Empty)
            End If

            gvTaxReport.DataSource = dtReports
            gvTaxReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            Dim FromDate As Date = DateFromFormattedDate(calFrom.SelectedDate, Session("DateFormat"))
            Dim ToDate As Date = DateFromFormattedDate(calTo.SelectedDate, Session("DateFormat"))
            If FromDate > ToDate Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "script", "alert('From date must be smaller than To Date');", True)
                Exit Sub
            End If
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        ExportToExcel.DataGridToExcel(gvTaxReport, Response)
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function ReturnMoney(ByVal Money As Decimal) As String
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvTaxReport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvTaxReport.RowDataBound
        Try
            If e.Row.RowType = ListItemType.Footer Then
                CType(e.Row.FindControl("lblSalesTaxTotal"), Label).Text = ReturnMoney(decSalesTaxTotal)
                CType(e.Row.FindControl("lblPurchasesTaxTotal"), Label).Text = ReturnMoney(decPurchasesTaxTotal)
                CType(e.Row.FindControl("lblTaxTotal"), Label).Text = ReturnMoney(decSalesTaxTotal - decPurchasesTaxTotal)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class