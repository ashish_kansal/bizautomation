<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBudgetImpact.aspx.vb" Inherits=".frmBudgetImpact" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>
<%@ Register Assembly="Syncfusion.Tools.Web, Version=4.402.0.51, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89"
    Namespace="Syncfusion.Web.UI.WebControls.Tools" TagPrefix="cc1" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>
    <title>Budget Impact</title>
    <script language ="javascript" type ="text/javascript" >
   function OpenDefineChartOfAccounts()
     {
         var nen;
         nen = window.open("../Accounting/frmDefineBudget.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=OperationBudget",'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=400,scrollbars=yes,resizable=yes');
         nen.focus();
         
     }       
    function SaveImpact()
 	{
 	   if (document.getElementById("rdbOperationBudget").checked==true) 
 	   {
 	       if(document.getElementById("ddlOperationBudget").value==0)
 	       {
 	         alert("Select Operation Budget");
 	         return false;
 	       }
 	       if(document.getElementById("ddlOperationBudgetAnalyze").value==0)
 	       {
 	         alert("Select Operation Budget Item to analyze");
 	         return false;
 	       }
 	       
 	   }
 	   
 	   if (document.getElementById("rdbMarketingBudget").checked==true) 
 	   {
 	       if(document.getElementById("ddlMarketBudget").value==0)
 	       {
 	         alert("Select Market Budget");
 	         return false;
 	       }
 	       if(document.getElementById("ddlMarketBudgetAnalyze").value==0)
 	       {
 	         alert("Select Market Budget Item to analyze");
 	         return false;
 	       }
 	       
 	   }
 	   
 	    if (document.getElementById("rdbProcurementBudget").checked==true) 
 	   {
 	       if(document.getElementById("ddlProcurementBudget").value==0)
 	       {
 	         alert("Select Procurement Budget");
 	         return false;
 	       }
 	       if(document.getElementById("ddlProcurementBudgetAnalyze").value==0)
 	       {
 	         alert("Select Procurement Budget Item to analyze");
 	         return false;
 	       }
 	       
 	   }
 	   
 	
 	}
    </script>
</head>
<body>
    <form id="form1" runat="server">
               <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
            <br />
    <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					
					<td valign="bottom" width ="400px">
						<table class="TabStyle" >
							<tr>
					    		<td >&nbsp;&nbsp;&nbsp;Budget Impact On Cash &nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
						</td> 
						<td class="normal1" align="right">
	                  <asp:button id="btnBudgetImpactSave"  Runat="server" Width="60" CssClass="button" Text="Save"></asp:button> 
    				  <asp:button id="btnBudgetImpactClose" Runat="server"  CssClass="button" Text="Cancel" ></asp:button></td>
						 
				</tr>
			</TABLE>
 
								<table id="Table12" runat="server" width="100%"  border="0" cellpadding ="0" cellspacing  ="0">
									<tr>
							         <td class="normal1" valign="top" align="left">	
								      	<asp:Table id="Table13"  BorderWidth="1" Runat="server" Width="100%" CssClass="aspTableDTL" Height="542px"
									    BorderColor="black" GridLines="none">
									    <asp:TableRow VerticalAlign ="top"  >
									    <asp:TableCell >	
    					                              <table id="TblOperationBudget" width="100%"   runat ="server" border ="0" >
    					                              <tr>
    					                              <td style ="width:170px" valign="top">
    					                                <asp:RadioButton id="rdbOperationBudget" runat ="server" Text ="Operations Budget" GroupName="Budget" Checked ="true" AutoPostBack="true" />
    					                               </td>
    					                               <td   align ="left" style="width:260px" >
    					                               <asp:DropDownList ID ="ddlOperationBudget" runat="server" Width ="250px" AutoPostBack ="true"  CssClass="signup"></asp:DropDownList>
    					                               </td>
    					                               <td   align ="left">
    					                               <asp:Label ID="lblOperationBudget" runat ="Server" Text ="Budget item to analyze"  ></asp:Label>
    					                               </td>
    					                              <td   align="left" >
    								                  <asp:DropDownList ID ="ddlOperationBudgetAnalyze" runat ="server" Width ="250px" CssClass="signup" AutoPostBack ="true" ></asp:DropDownList>
    								                  </td> 
    								                  </tr>
    								                  <tr>
    					                              <td style ="width:170px">
    					                                <asp:RadioButton ID="rdbMarketingBudget" runat ="server" Text ="Marketing Budget Year" GroupName="Budget" AutoPostBack="true" />
    								                  </td>
                    								   
    								                  <td   align ="left" style="width:260px" >
    								                  <asp:DropDownList ID ="ddlMarketBudget" runat ="server" Width ="250px" CssClass="signup" Autopostback="true">
    								                  <asp:ListItem text =" --Select One-- " Value ="0"></asp:ListItem>
    								                  <asp:ListItem Text =" This Year" Value ="1"></asp:ListItem> 
    								                  <asp:ListItem Text =" Last Year" Value ="2"></asp:ListItem> 
    								                  <asp:ListItem Text =" Next Year" Value ="3"></asp:ListItem>     								  
    								                  </asp:DropDownList>
    								                  </td>
    								                  <td align ="left">
    					                               <asp:Label ID="lblMarketBudget" runat ="Server" Text ="Budget item to analyze"></asp:Label>
    					                               </td>
    					                                <td align ="left">
    								                  <asp:DropDownList ID ="ddlMarketBudgetAnalyze" runat ="server" Width ="250px" CssClass="signup" Autopostback="true"></asp:DropDownList>
    								                  </td> 
                    								
    								                  </tr>
    								                  <tr>
    					                              <td style ="width:170px">
    					                                <asp:RadioButton ID="rdbProcurementBudget" runat ="server" Text ="Procurement Budget Year" GroupName="Budget" AutoPostBack="true" />
    					                              </td>
    					                              <td align ="left" style="width:260px" >
    					                              <asp:DropDownList ID ="ddlProcurementBudget" runat ="server" Width ="250px" CssClass="signup" Autopostback="true" >
    					                               <asp:ListItem text =" --Select One-- " Value ="0"></asp:ListItem>
    								                  <asp:ListItem Text =" This Year" Value ="1"></asp:ListItem> 
    								                  <asp:ListItem Text =" Last Year" Value ="2"></asp:ListItem> 
    								                  <asp:ListItem Text =" Next Year" Value ="3"></asp:ListItem>    
    					                              </asp:DropDownList>
    					                              </td>
    					                              <td align ="left" >
    					                               <asp:Label ID="lblProcurementBudget" runat ="Server" Text ="Budget item to analyze"></asp:Label>
    					                               </td>
    					                                <td align ="left" >
    								                  <asp:DropDownList ID ="ddlProcurementBudgetAnalyze" runat ="server" Width ="250px" CssClass="signup" Autopostback="true"></asp:DropDownList>
    								                  </td>
    								                </tr>
    					                              <tr>
                    					               <td colspan ="4" align ="right" class="normal1" ></td>
    					                              <td colspan ="6" align ="right" class="normal1" >
    					                              <asp:Label ID="lblTotalBalance" runat ="server" Font-Bold ="true" Text =""></asp:Label>&nbsp;&nbsp;
    					                                <asp:Label ID="lblTotalBalanceAmt" runat ="server" ></asp:Label>
    					                              </td>
    					                              </tr>
    					                              <tr>
    					                            
    					                             <td colspan ="4" align ="right" class="normal1" ></td>
    					                              <td colspan ="6" align ="right" class="normal1" >
    					                              <asp:Label ID="lblTodayBalance" runat ="server" Text ="(All bank account balances as of today)"></asp:Label>
    					                              </td>
    					                              </tr>
    					                              <tr>
    					                              
    					                              <td colspan ="3" align ="right" class="normal1" >
    					                     <asp:radiobuttonlist id="rdlForecastType" CssClass="normal1" Runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
							                RepeatLayout="Table">
					    	                <asp:ListItem Value="0" Selected="True">Probabale</asp:ListItem>
							                <asp:ListItem Value="1">Forecast</asp:ListItem>
						                </asp:radiobuttonlist>
    					                              </td>
    					                              <td colspan ="7" align ="right" class="normal1" >
    					                             <asp:Button ID="btnCalculate" runat ="Server" Text ="Calculate" CssClass="button" />
    					                             
    					                              </td>
    					                              </tr>
    					                             
    					       </table>
    					   <%--    <igtbl:UltraWebGrid ID="ulgImpactBudget" runat ="server"  Width ="" Height="10px" DisplayLayout-AutoGenerateColumns="false" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  Browser="Xml"  >
    					                                <DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single" AutoGenerateColumns="true"  
					                                              ViewType="Flat" SelectTypeCellDefault="NotSet" ColFootersVisibleDefault="no"  BorderCollapseDefault="Separate" AllowColSizingDefault="Fixed" 
					                                                    Name="ulgImpactBudget" EnableClientSideRenumbering="true"  TableLayout="Fixed"  SelectTypeColDefault="Extended" AllowUpdateDefault="Yes" >
                                                                   <ClientSideEvents  />
                                                      <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                                        <Padding Left="5px" Right="5px"></Padding>
                                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                                    </HeaderStyleDefault>
                                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                                    <FrameStyle Width="100%"   Cursor="Default" BorderWidth="0" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                                    <FooterStyleDefault  BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                                    </FooterStyleDefault>
                                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                                        <Padding Left="5px" Right="5px"></Padding>
                                                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                                    </RowStyleDefault>
                                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
                                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
                                                          </DisplayLayout> 
                                                           <Bands>
                												                    
												                 <igtbl:UltraGridBand  AllowDelete="no" BaseTableName="OperationBudgetDetails" Key="OperationBudgetDetails">
												                     <Columns>
								                                            <igtbl:UltraGridColumn headerText="numBudgetDetId" AllowUpdate="No" IsBound="true" Hidden ="true"  BaseColumnName ="" Key ="numBudgetDetId"></igtbl:UltraGridColumn>
                                                                            <igtbl:UltraGridColumn headerText="monOriginalAmt" AllowUpdate="No" IsBound="true" Hidden ="true"  BaseColumnName ="" Key ="monOriginalAmt"></igtbl:UltraGridColumn>
                                                                            <igtbl:UltraGridColumn    HeaderText="Month"    AllowUpdate="No" IsBound="true" Hidden="true" BaseColumnName="" Key="vcmonth">
												                            </igtbl:UltraGridColumn>
												                            <igtbl:UltraGridColumn    HeaderText="Month" Width ="15%"  AllowUpdate="No" IsBound="true"  BaseColumnName="" Key="vcmonthName">
												                            </igtbl:UltraGridColumn>
												                            <igtbl:UltraGridColumn HeaderText="Budget Allocated"  Width ="12%" AllowUpdate="No" IsBound="true"  DataType="Decimal"  BaseColumnName="" Key="monAmount">
												                            </igtbl:UltraGridColumn>
												                            <igtbl:UltraGridColumn HeaderText="Cash Inflows" Width ="8%" AllowUpdate="No" IsBound="true"   Type="HyperLink"  BaseColumnName="" Key="monCashInflow">
												                            </igtbl:UltraGridColumn>
												                            <igtbl:UltraGridColumn HeaderText="Sales Forecast (unclosed deals)" Width ="22%" AllowUpdate="No"  IsBound="true"  BaseColumnName="" Key="monSalesForecast">
												                            </igtbl:UltraGridColumn>
												                            <igtbl:UltraGridColumn HeaderText="Total Budget Balance" Width ="22%" AllowUpdate="No" IsBound="true"    BaseColumnName="" Key="monTotalBudgetBalance">
												                            </igtbl:UltraGridColumn>
												                            <igtbl:UltraGridColumn HeaderText="Projected Bank Balance" Width ="21%" AllowUpdate="No" IsBound="true"  BaseColumnName="" Key="monProjectedBankBalance">
												                            </igtbl:UltraGridColumn>
                												       </Columns>
												                   </igtbl:UltraGridBand>
                												                    
									                </Bands>
    					                              </igtbl:UltraWebGrid>--%>
    					    </asp:TableCell>
						</asp:TableRow>
					</asp:Table> 
    			</td>    
    		</tr>
    	</table>
    </form>
</body>
</html>
