﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.UserInterface
Imports System.Data.SqlClient
Partial Public Class frmAccountsPayableDetails
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                LoadAccountPayableDetailGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub LoadAccountPayableDetailGrid()
        Dim objVendorPayment As New VendorPayment
        Dim dtARAging As DataTable
        Try
            objVendorPayment.DomainID = CCommon.ToLong(GetQueryStringVal("DomainId"))
            objVendorPayment.DivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
            objVendorPayment.Flag = GetQueryStringVal("Flag")
            objVendorPayment.AccountClass = CCommon.ToLong(GetQueryStringVal("accountClass"))
            objVendorPayment.dtFromDate = Convert.ToDateTime(GetQueryStringVal("fromDate"))
            objVendorPayment.dtTodate = Convert.ToDateTime(GetQueryStringVal("toDate"))
            objVendorPayment.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            dtARAging = objVendorPayment.GetAccountPayableAgingDetails

            If (dtARAging.Rows.Count > 0) Then
                'Dim foundRows As DataRow() = dtARAging.Select(" BalanceDue > 0 ")
                'If foundRows.Length > 0 Then
                Dim dt As DataTable = dtARAging '.Select(" BalanceDue > 0 ").CopyToDataTable()
                For Each dr As DataRow In dt.Rows
                    If dr("vcPOppName") = "Journal" Then
                        dr("BalanceDue") = 0
                    End If
                Next
                gvARDetails.DataSource = dt
                gvARDetails.DataBind()
                'End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class