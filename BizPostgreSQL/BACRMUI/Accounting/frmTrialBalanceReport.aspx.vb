﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports System.IO
Imports System.Text
Imports ClosedXML.Excel
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports BACRM.BusinessLogic.Admin

Public Class frmTrialBalanceReport
    Inherits BACRMPage
#Region "Member Variables"
    Private mobjTrailBalance As TrailBalance
#End Region
#Region "Page Events"
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Add(ddlColumnType.ID, ddlColumnType.SelectedValue)
            PersistTable.Add(ddlDateRange.ID, ddlDateRange.SelectedValue)
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack Then
                GetUserRightsForPage(35, 92)
                BindUserLevelClassTracking()

                Dim lobjGeneralLedger As New GeneralLedger
                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now) ' DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)

                If GetQueryStringVal("FromDate") <> "" And GetQueryStringVal("ToDate") <> "" Then

                    calFrom.SelectedDate = CDate(Replace(GetQueryStringVal("FromDate"), "%27", ""))
                    calTo.SelectedDate = CDate(Replace(GetQueryStringVal("ToDate"), "%27", ""))
                Else
                    If PersistTable.Count > 0 Then
                        ddlColumnType.SelectedValue = PersistTable(ddlColumnType.ID)
                        ddlDateRange.SelectedValue = PersistTable(ddlDateRange.ID)
                        ddlUserLevelClass.SelectedValue = PersistTable(ddlUserLevelClass.ID)

                        Try
                            calFrom.SelectedDate = PersistTable(calFrom.ID)
                            calTo.SelectedDate = PersistTable(calTo.ID)

                            If calFrom.SelectedDate Is Nothing Or calTo.SelectedDate Is Nothing Then
                                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now)
                            End If
                        Catch ex As Exception
                            'Do not throw error when date format which is stored in persist table and Current date formats are different
                            calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now)
                        End Try

                        'Clears date range selection if value selected in date range drop down
                        If ddlDateRange.SelectedValue <> "0" Then
                            calFrom.SelectedDate = Nothing
                            calTo.SelectedDate = Nothing
                        End If
                    End If
                End If

                hdnDomainName.Value = CCommon.ToString(Session("DomainName"))

                If ddlDateRange.SelectedValue <> "0" Then
                    Dim fromDate As Date
                    Dim toToDate As Date

                    Select Case ddlDateRange.SelectedValue
                        Case "CurYear"
                            fromDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "PreYear"
                            fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "CurPreYear"
                            fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "CuQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth")))
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                        Case "PreQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddDays(-1)
                        Case "CurPreQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                        Case "ThisMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                            toToDate = fromDate.AddMonths(1).AddDays(-1)
                        Case "LastMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                            toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
                        Case "CurPreMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                            toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1)
                    End Select

                    SetDateRange(fromDate, toToDate)
                Else
                    SetDateRange(calFrom.SelectedDate, calTo.SelectedDate)
                End If

                BindReport()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region
#Region "Private Methods"
    Private Sub BindReport()
        Try
            If rtlTrialBalance.Columns.Count > 1 Then
                For i As Int32 = 1 To rtlTrialBalance.Columns.Count - 1
                    rtlTrialBalance.Columns.RemoveAt(1)
                Next
            End If

            Dim type As String = Nothing

            If ddlColumnType.SelectedValue = "Quarter" Then
                type = "Quarter"
            ElseIf ddlColumnType.SelectedValue = "Year" Then
                type = "Year"
            End If

            If mobjTrailBalance Is Nothing Then mobjTrailBalance = New TrailBalance
            mobjTrailBalance.DomainID = Session("DomainID")

            If pnlAccountingClass.Visible AndAlso ddlUserLevelClass.Items.Count > 0 Then
                mobjTrailBalance.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
            Else
                mobjTrailBalance.AccountClass = 0
            End If

            Dim dateFilter As String
            If ddlDateRange.SelectedValue <> "0" Then
                dateFilter = ddlDateRange.SelectedValue
            Else
                dateFilter = "Custom"
                mobjTrailBalance.FromDate = calFrom.SelectedDate
                mobjTrailBalance.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            End If

            Dim ds As DataSet = mobjTrailBalance.GetTrialBalance(dateFilter, type)
            Dim dtTrialBalance As DataTable = ds.Tables(0)

            Dim index As Int32 = 1
            For Each column As DataColumn In dtTrialBalance.Columns
                If column.ColumnName <> "ParentId" And
                    column.ColumnName <> "vcCompundParentKey" And
                    column.ColumnName <> "numAccountTypeID" And
                    column.ColumnName <> "vcAccountType" And
                    column.ColumnName <> "vcAccountCode" And
                    column.ColumnName <> "LEVEL" And
                    column.ColumnName <> "numAccountId" And
                    column.ColumnName <> "Struc" And
                    column.ColumnName <> "Type" And
                    column.ColumnName <> "bitTotal" Then
                    Dim boundColumn As New TreeListBoundColumn
                    boundColumn.UniqueName = column.ColumnName
                    boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    boundColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                    boundColumn.DataFormatString = "{0:" & Session("Currency") & "#,##0.00;(" & Session("Currency") & "#,##0.00)}"
                    boundColumn.HeaderStyle.Width = New Unit("150")
                    boundColumn.DataField = column.ColumnName
                    boundColumn.HeaderText = column.ColumnName
                    rtlTrialBalance.Columns.Insert(index, boundColumn)

                    index = index + 1
                End If
            Next


            rtlTrialBalance.DataSource = dtTrialBalance
            rtlTrialBalance.DataBind()
            rtlTrialBalance.ExpandAllItems()

            If ddlColumnType.SelectedValue = "Year" Then
                rtlTrialBalance.Columns(0).HeaderStyle.Width = New Unit(260, UnitType.Pixel)
            Else
                rtlTrialBalance.Columns(0).HeaderStyle.Width = New Unit(100, UnitType.Percentage)
            End If

            lblPeriod.Text = "(" & hdnDateRange.Value & ")"

            UpdatePanelGrid.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False

            If CCommon.ToInteger(HttpContext.Current.Session("DefaultClassType")) > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()

                    Dim objItem As New System.Web.UI.WebControls.ListItem
                    objItem.Text = "-- Select One --"
                    objItem.Value = "0"

                    ddlUserLevelClass.Items.Insert(0, objItem)

                    pnlAccountingClass.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetDateRange(ByVal fromDate As Date, ByVal toDate As Date)
        Try
            hdnDateRange.Value = "From " & FormattedDateFromDate(fromDate, Session("DateFormat")) & " to " & FormattedDateFromDate(toDate, Session("DateFormat"))
            hdnFromDate.Value = fromDate
            hdnToDate.Value = toDate
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function GetStartOfFinancialQtr(monthFinancialYearStartsOn As Integer) As DateTime
        Try
            Dim tempDate As Date = DateTime.Now.Date
            Dim actualMonth = tempDate.Month
            Dim financialYear = tempDate.Year
            Dim difference = actualMonth - monthFinancialYearStartsOn
            If difference < 0 Then
                financialYear -= 1
                difference += 12
            End If
            Dim quarter = Math.Floor(difference / 3)

            Return New DateTime(financialYear, monthFinancialYearStartsOn, 1).AddMonths(quarter * 3)
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region
#Region "Event Handlers"
    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Try
            If Not calFrom.SelectedDate Is Nothing AndAlso Not calTo.SelectedDate Is Nothing Then

                Dim FromDate As Date = calFrom.SelectedDate
                Dim ToDate As Date = calTo.SelectedDate

                If FromDate > ToDate Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "script", "alert('From date must be smaller than To Date');", True)
                    Exit Sub
                End If

                SetDateRange(calFrom.SelectedDate, calTo.SelectedDate)
                ddlDateRange.ClearSelection()
                lblPeriod.Text = "(" & hdnDateRange.Value & ")"

                BindReport()
            Else
                ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "script", "alert('From date and To Date is required.');", True)
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub rtlTrialBalance_ItemCommand(sender As Object, e As Telerik.Web.UI.TreeListCommandEventArgs)
        Try
            If e.CommandName = RadTreeList.ExpandCollapseCommandName Then
                'WE ARE HANDLING EXPAND COLLAPSE CLIENT SIDE SO CODE IS NOT REQUIRED
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub rtlTrialBalance_ItemDataBound(sender As Object, e As Telerik.Web.UI.TreeListItemDataBoundEventArgs)
        Try
            If e.Item.ItemType = TreeListItemType.Item Or e.Item.ItemType = TreeListItemType.AlternatingItem Then

                Dim btn As Button = TryCast(e.Item.FindControl("ExpandCollapseButton"), Button)
                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("Struc")

                If btn IsNot Nothing Then
                    btn.Attributes.Add("UniqueID", uniqueID)
                    btn.Attributes.Add("onclick", "return ExpandCollapse(" + btn.ClientID + ");")
                End If

                DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).Attributes.Add("UniqueID", uniqueID)

                If DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("Type") > 1 Then
                    e.Item.Font.Bold = True
                Else
                    e.Item.Font.Bold = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub imgBtnExportExcel_Click(sender As Object, e As EventArgs) Handles imgBtnExportExcel.Click
        Try
            If Not String.IsNullOrWhiteSpace(txtExportHtml.Text) Then

                Dim fs As New MemoryStream()
                Dim workbook As New XLWorkbook
                Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Trial Balance")

                Dim doc As New HtmlAgilityPack.HtmlDocument
                doc.LoadHtml(txtExportHtml.Text)

                'ADD HEADER
                workSheet.Cell(1, 1).Value = String.Format(Session("DomainName") & "{0}" & " Trial Balance " & "{1} " & hdnDateRange.Value, Environment.NewLine, Environment.NewLine)
                workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                workSheet.Cell(1, 1).Style.Font.Bold = True
                workSheet.Cell(1, 1).Style.Font.FontSize = 14
                workSheet.Row(1).AdjustToContents()
                workSheet.Range(workSheet.Cell(1, 1), workSheet.Cell(1, doc.DocumentNode.SelectNodes("//table//tr[1]//td").Count)).Merge()

                Dim i As Int32 = 2
                Dim j As Int32 = 1

                For Each row As HtmlAgilityPack.HtmlNode In doc.DocumentNode.SelectNodes("//table//tr")
                    For Each col As HtmlAgilityPack.HtmlNode In row.SelectNodes("td")


                        If i = 2 Then
                            If col.InnerText.StartsWith("(") AndAlso col.InnerText.EndsWith(")") Then
                                workSheet.Cell(i, j).SetValue("-" & col.InnerText.Replace("(", "").Replace(")", "").Replace("USD", "").Replace("$", "").Trim()).SetDataType(XLCellValues.Text)
                            Else
                                workSheet.Cell(i, j).SetValue(col.InnerText.Replace("USD", "").Replace("$", "").Trim()).SetDataType(XLCellValues.Text)
                            End If

                            workSheet.Cell(i, j).Style.Font.Bold = True
                            workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                            workSheet.Cell(i, j).Style.Fill.BackgroundColor = XLColor.LightGray
                            workSheet.Cell(i, j).DataType = XLCellValues.Text
                        Else
                            If col.InnerText.StartsWith("(") AndAlso col.InnerText.EndsWith(")") Then
                                workSheet.Cell(i, j).Value = "-" & col.InnerText.Replace("(", "").Replace(")", "").Replace("USD", "").Replace("$", "").Trim()
                            Else
                                workSheet.Cell(i, j).Value = col.InnerText.Replace("USD", "").Replace("$", "").Trim()
                            End If

                            If col.Attributes.Count > 0 AndAlso col.Attributes.Contains("bold") AndAlso col.Attributes("bold").Value = "1" Then
                                workSheet.Row(i).Style.Font.Bold = True
                            End If
                        End If

                        workSheet.Cell(i, j).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        workSheet.Cell(i, j).Style.Border.SetOutsideBorderColor(XLColor.Black)


                        j = j + 1
                    Next
                    i = i + 1
                    j = 1
                Next
                workSheet.Columns.AdjustToContents()

                Dim httpResponse As HttpResponse = Response
                httpResponse.Clear()
                httpResponse.ClearContent()
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                httpResponse.AddHeader("content-disposition", "attachment;filename=TrialBalance.xlsx")


                Using MemoryStream As New MemoryStream
                    workbook.SaveAs(MemoryStream)
                    MemoryStream.WriteTo(httpResponse.OutputStream)
                    MemoryStream.Close()
                End Using

                httpResponse.Flush()
                httpResponse.End()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub ddlDateRange_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDateRange.SelectedIndexChanged
        Try
            If ddlDateRange.SelectedValue <> "0" Then
                Dim fromDate As Date
                Dim toToDate As Date

                Select Case ddlDateRange.SelectedValue
                    Case "CurYear"
                        fromDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "PreYear"
                        fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "CurPreYear"
                        fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "CuQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth")))
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                    Case "PreQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddDays(-1)
                    Case "CurPreQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                    Case "ThisMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                        toToDate = fromDate.AddMonths(1).AddDays(-1)
                    Case "LastMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                        toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
                    Case "CurPreMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                        toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1)
                End Select

                SetDateRange(fromDate, toToDate)
                BindReport()
            End If

            calFrom.SelectedDate = Nothing
            calTo.SelectedDate = Nothing
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub ddlColumnType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlColumnType.SelectedIndexChanged
        Try
            PersistTable.Add(ddlColumnType.ID, ddlColumnType.SelectedValue)
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

    
End Class