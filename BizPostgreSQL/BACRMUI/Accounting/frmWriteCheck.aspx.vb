﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Projects

Public Class frmWriteCheck
    Inherits BACRMPage
    Dim lngTransactionID As Long

    Dim dtChartAcnt As DataTable
    Dim objChecks As Checks
    Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            GetUserRightsForPage(35, 80)
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
                btnSaveNew.Visible = False
                btnSavePrint.Visible = False
                btnSplitSave.Visible = False
            End If
            If Not IsPostBack Then
                radCmbCompany.Focus()
                lngTransactionID = CCommon.ToLong(GetQueryStringVal("TransactionID"))
                calPaymentDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                If lngTransactionID > 0 Then
                    hdnTransactionID.Value = lngTransactionID
                End If

                hfCheckHeaderID.Value = CCommon.ToLong(GetQueryStringVal("CheckHeaderID"))
                FillDepositTocombo()

                LoadCheckHeader()
                LoadCheckDetails(boolPostback:=True)

                btnSave.Attributes.Add("onclick", "return Save()")

                txtCheckNo.Attributes.Add("onkeypress", "CheckNumber(2,event)")

                Dim SplitFlag As Integer = CCommon.ToInteger(GetQueryStringVal("SplitFlag"))
                If SplitFlag > 0 Then
                    SplitTransaction()
                    ClientScript.RegisterStartupScript(Me.GetType(), "Resize", "windowResize();", True)
                    btnSplitSave.Attributes.Add("onclick", "return Save()")
                End If


                If CCommon.ToLong(hfCheckHeaderID.Value) = 0 Then btnDelete.Visible = False
                If CCommon.ToInteger(GetQueryStringVal("flag")) = 1 Then ShowMessage("You check is saved sucessfully.")
                If CCommon.ToInteger(GetQueryStringVal("flag")) = 2 Then ShowMessage("You check is saved sucessfully, Write new check.")
                If CCommon.ToInteger(GetQueryStringVal("flag")) = 3 Then ShowMessage("Check deleted sucessfully")
            End If
            'If Not ClientScript.IsStartupScriptRegistered("CalculateTotal") Then ClientScript.RegisterStartupScript(Me.GetType, "CalculateTotal", "CalculateTotal();", True)
            objCommon.ManageGridAsPerConfig(gvCheckDetails)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub FillDepositTocombo()
        Try
            Dim dtChartAcnt As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "01010101" 'bank accounts
            dtChartAcnt = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("vcStartingCheckNumber")
                'item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlDepositTo.Items.Add(item)
            Next
            ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))

            PersistTable.Load(boolOnlyURL:=True)
            If PersistTable.Count > 0 Then
                For Each item In ddlDepositTo.Items
                    If item.Value.StartsWith(CCommon.ToString(PersistTable(ddlDepositTo.ID))) Then
                        ddlDepositTo.ClearSelection()
                        item.Selected = True
                        ChangeDepositTo()
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlDepositTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepositTo.SelectedIndexChanged
        Try
            ChangeDepositTo()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub ChangeDepositTo()
        Try
            Dim lobjCashCreditCard As New CashCreditCard
            Dim ldecOpeningBalance As Decimal = 0
            If ddlDepositTo.SelectedItem.Value <> "0" Then
                lobjCashCreditCard.AccountId = ddlDepositTo.SelectedValue.Split("~")(0)
                lobjCashCreditCard.DomainID = Session("DomainId")
                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()

                lblBalanceAmount.Text = String.Format("<h4><label class=""{0}"">{1} {2}</label></h4>",
                                                        IIf(ldecOpeningBalance < 0, "label label-danger", "label label-success"),
                                                        Session("Currency").ToString.Trim,
                                                        ReturnMoney(ldecOpeningBalance))

                'If txtCheckNo.Text.Trim.Length = 0 Then
                txtCheckNo.Text = CCommon.ToLong(ddlDepositTo.SelectedValue.Split("~")(1))
                hfCheckNo.Value = CCommon.ToLong(ddlDepositTo.SelectedValue.Split("~")(1))
                'End If

                PersistTable.Clear()
                PersistTable.Add(ddlDepositTo.ID, ddlDepositTo.SelectedValue.Split("~")(0))
                PersistTable.Save(boolOnlyURL:=True)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LoadCheckHeader()
        Try
            If CCommon.ToLong(hfCheckHeaderID.Value) > 0 Then
                If objChecks Is Nothing Then objChecks = New Checks

                objChecks.DomainID = Session("DomainId")
                objChecks.Mode = 1
                objChecks.CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)

                Dim ds As DataSet = objChecks.ManageCheckHeader()

                Dim dtCheckHeader As DataTable = ds.Tables(0)

                If dtCheckHeader.Rows.Count > 0 Then
                    If CCommon.ToLong(dtCheckHeader.Rows(0)("numDivisionID")) > 0 Then
                        objCommon.UserCntID = Session("UserContactID")
                        objCommon.DivisionID = CCommon.ToLong(dtCheckHeader.Rows(0)("numDivisionID"))
                        objCommon.charModule = "D"
                        Dim strCompany, strContactID As String
                        strCompany = objCommon.GetCompanyName
                        strContactID = objCommon.ContactID
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                    End If

                    calPaymentDate.SelectedDate = dtCheckHeader.Rows(0)("dtCheckDate")
                    'If ddlDepositTo.Items.FindByValue(dtCheckHeader.Rows(0)("numChartAcntId")) IsNot Nothing Then
                    '    ddlDepositTo.ClearSelection()
                    '    ddlDepositTo.Items.FindByValue(dtCheckHeader.Rows(0)("numChartAcntId")).Selected = True

                    '    ChangeDepositTo()
                    'End If
                    For Each Item As ListItem In ddlDepositTo.Items
                        If Item.Value.StartsWith(dtCheckHeader.Rows(0)("numChartAcntId")) Then
                            ddlDepositTo.ClearSelection()
                            Item.Selected = True
                            ChangeDepositTo()
                            Exit For
                        End If
                    Next

                    hfCheckNo.Value = CCommon.ToLong(dtCheckHeader.Rows(0)("numCheckNo"))

                    If CCommon.ToLong(dtCheckHeader.Rows(0)("numCheckNo")) > 0 Then
                        txtCheckNo.Text = dtCheckHeader.Rows(0)("numCheckNo")
                        chkPrintCheck.Checked = False
                    Else
                        txtCheckNo.Text = ""
                        chkPrintCheck.Checked = True
                    End If

                    txtMemo.Text = CCommon.ToString(dtCheckHeader.Rows(0)("vcMemo"))
                    hfTotal.Value = dtCheckHeader.Rows(0)("monAmount")

                    hfHeaderTransactionId.Value = CCommon.ToLong(dtCheckHeader.Rows(0)("numTransactionId"))
                End If
            End If

            TransactionInfo1.ReferenceType = enmReferenceType.CheckHeader
            TransactionInfo1.ReferenceID = CCommon.ToLong(hfCheckHeaderID.Value)
            TransactionInfo1.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
            TransactionInfo1.getTransactionInfo()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadCheckDetails(Optional deleteRowIndex As Integer = -1, Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Dim j As Integer
        Dim drRow As DataRow
        Dim dtItems As New DataTable

        Try
            If objChecks Is Nothing Then objChecks = New Checks()

            objChecks.DomainID = Session("DomainID")
            objChecks.CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)

            dtItems = objChecks.FetchCheckDetails()

            If boolPostback = False Then
                dtItems.Rows.Clear()

                Dim i As Integer
                Dim gvRow As GridViewRow
                Dim dtrow As DataRow
                For i = 0 To gvCheckDetails.Rows.Count - 1
                    If i <> deleteRowIndex Then
                        dtrow = dtItems.NewRow
                        gvRow = gvCheckDetails.Rows(i)

                        dtrow.Item("numCheckDetailID") = CCommon.ToLong(CType(gvRow.FindControl("lblCheckDetailID"), Label).Text)
                        dtrow.Item("numChartAcntId") = CType(gvRow.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.Split("~")(0)
                        dtrow.Item("monAmount") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)
                        dtrow.Item("numClassId") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                        dtrow.Item("numProjectId") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)
                        dtrow.Item("vcDescription") = CType(gvRow.FindControl("txtDescription"), TextBox).Text
                        dtrow.Item("numCustomerId") = CCommon.ToLong(radCmbCompany.SelectedValue)
                        dtrow.Item("numTransactionId") = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)

                        dtItems.Rows.Add(dtrow)
                    End If
                Next
            End If

            If dtItems.Rows.Count < 2 Then
                Dim lintRowcount As Int16 = 2 - dtItems.Rows.Count
                For j = 0 To lintRowcount - 1
                    drRow = dtItems.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    drRow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtItems.Rows.Add(drRow)
                Next
            End If

            If boolAddNew Then
                For j = 0 To 1
                    drRow = dtItems.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    drRow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtItems.Rows.Add(drRow)
                Next
            End If

            LoadChartAcnt()

            gvCheckDetails.DataSource = dtItems
            gvCheckDetails.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Dim dtProject As DataTable
    Private Sub BindProject(ByRef ddlProject As DropDownList)
        Try
            If dtProject Is Nothing Then
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                dtProject = objProject.GetOpenProject()
            End If

            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtProject
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim dtClass As DataTable
    Private Sub BindClass(ByRef ddlClass As DropDownList)
        Try
            If dtClass Is Nothing Then
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()
            End If
            ddlClass.DataTextField = "ClassName"
            ddlClass.DataValueField = "numChildClassID"
            ddlClass.DataSource = dtClass
            ddlClass.DataBind()
            ddlClass.Items.Insert(0, "--Select One--")
            ddlClass.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadChartAcnt()
        Try
            Dim dt1 As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainID")
            objCOA.AccountCode = "0102" ' Liability
            dtChartAcnt = objCOA.GetParentCategory()
            ''Exclude AP
            'Dim drArray() As DataRow = dtChartAcnt.Select("vcAccountTypeCode <>'01020102'")
            'If drArray.Length > 0 Then
            '    dtChartAcnt = drArray.CopyToDataTable()
            'End If

            objCOA.AccountCode = "0101" ' Asset
            dt1 = objCOA.GetParentCategory()
            ''Exclude AR 
            'drArray = dt1.Select("vcAccountTypeCode <> '01010105'")
            'If drArray.Length > 0 Then
            '    dt1 = drArray.CopyToDataTable()
            'End If
            dtChartAcnt.Merge(dt1)

            objCOA.AccountCode = "0104" 'Expense
            dt1 = objCOA.GetParentCategory()
            dtChartAcnt.Merge(dt1)

            objCOA.AccountCode = "0106" ' COGS
            dt1 = objCOA.GetParentCategory()
            dtChartAcnt.Merge(dt1)

            objCOA.AccountCode = "0103" ' Income
            dt1 = objCOA.GetParentCategory()
            dtChartAcnt.Merge(dt1)



        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadChartType(ByVal ddlAccounts As DropDownList)
        Try
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("vcAccountTypeCode") 'added parent account type code , bug 949
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccounts.Items.Add(item)
            Next
            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvCheckDetails_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCheckDetails.RowCommand
        Try
            If e.CommandName = "DeleteRow" Then
                Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                LoadCheckDetails(deleteRowIndex:=row.DataItemIndex)

                'If Not ClientScript.IsStartupScriptRegistered("CalculateTotal") Then ClientScript.RegisterStartupScript(Me.GetType, "CalculateTotal", "CalculateTotal();", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvCheckDetails_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCheckDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlAccounts As DropDownList = CType(e.Row.FindControl("ddlAccounts"), DropDownList)
                Dim txtAmount As TextBox = CType(e.Row.FindControl("txtAmount"), TextBox)
                Dim txtDescription As TextBox = CType(e.Row.FindControl("txtDescription"), TextBox)
                Dim ddlClass As DropDownList = CType(e.Row.FindControl("ddlClass"), DropDownList)
                Dim ddlProject As DropDownList = CType(e.Row.FindControl("ddlProject"), DropDownList)
                'Dim radCustomer As RadComboBox = CType(e.Row.FindControl("radCustomer"), RadComboBox)

                txtAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")

                BindClass(ddlClass)
                BindProject(ddlProject)

                If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numClassID")) > 0 Then
                    If ddlClass.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numClassID")) IsNot Nothing Then
                        ddlClass.ClearSelection()
                        ddlClass.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numClassID")).Selected = True
                    End If
                End If

                If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numProjectID")) > 0 Then
                    If ddlProject.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numProjectID")) IsNot Nothing Then
                        ddlProject.ClearSelection()
                        ddlProject.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numProjectID")).Selected = True
                    End If
                End If


                'If Not IsNothing(radCustomer) Then
                '    Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)

                '    If CCommon.ToLong(lblName.Text) > 0 Then
                '        objCommon.UserCntID = Session("UserContactID")
                '        objCommon.DivisionID = IIf(lblName.Text = "", 0, lblName.Text)
                '        objCommon.charModule = "D"
                '        Dim strCompany As String
                '        strCompany = objCommon.GetCompanyName
                '        radCustomer.Text = strCompany
                '        radCustomer.SelectedValue = objCommon.DivisionID
                '    End If
                'End If

                If Not IsNothing(ddlAccounts) Then
                    LoadChartType(ddlAccounts)
                    Dim lblAccountID As Label = CType(e.Row.FindControl("lblAccountID"), Label)

                    For Each Item As ListItem In ddlAccounts.Items
                        If lblAccountID.Text <> "" And lblAccountID.Text <> "0" And Item.Value.StartsWith(lblAccountID.Text) Then
                            ddlAccounts.ClearSelection()
                            Item.Selected = True
                        End If
                    Next
                End If

                Dim btnDelete As LinkButton = CType(e.Row.FindControl("btnDelete"), LinkButton)
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            LoadCheckDetails(boolAddNew:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnAllowCheckNO_Click(sender As Object, e As System.EventArgs) Handles btnAllowCheckNO.Click
        Try
            save()

            ShowMessage("Record added successfully.")
            hfCheckHeaderID.Value = 0

            LoadCheckHeader()
            LoadCheckDetails(boolPostback:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try

            If CCommon.ToLong(txtCheckNo.Text.Trim()) > 0 Then
                Dim lngCheckNo As Long = CCommon.ToLong(txtCheckNo.Text)
                Dim strchkNoList As New ArrayList

                Dim objCOA As New ChartOfAccounting
                objCOA.AccountId = ddlDepositTo.SelectedValue.Split("~")(0)
                objCOA.CheckHeaderID = 0
                objCOA.DomainID = Session("DomainID")

                Dim dtCheckNo As DataTable = objCOA.ValidateCheckNo(lngCheckNo)

                If dtCheckNo.Rows.Count > 0 Then
                    strchkNoList = New ArrayList

                    For Each dr As DataRow In dtCheckNo.Rows
                        strchkNoList.Add(dr("numCheckNo"))
                    Next
                    ShowMessage("You must specify a different Check #. Check # " & String.Join(", ", strchkNoList.ToArray()) & " has already been used. Save & Print it anyway?")
                    btnAllowCheckNO.Visible = True
                    Return
                End If
            End If

            save()

            'hfCheckHeaderID.Value = 0
            'LoadCheckHeader()
            'LoadCheckDetails(boolPostback:=True)
            'ChangeDepositTo()
            Response.Redirect("../Accounting/frmWriteCheck.aspx?flag=1&CheckHeaderID=" & CCommon.ToLong(hfCheckHeaderID.Value).ToString)
        Catch ex As Exception
            If ex.Message = "IM_BALANCE" Then
                ShowMessage("The Credit and Debit amounts you’re entering must be of the same amount")
            ElseIf ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Private Sub btnSplitSave_Click(sender As Object, e As System.EventArgs) Handles btnSplitSave.Click
        Try
            If CCommon.ToDecimal(hfTotal.Value) = CCommon.ToDecimal(lblSplitTotal.Text) Then
                save()

                If Not CCommon.ToLong(hdnTransactionID.Value) = 0 Then
                    objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                    objEBanking.TransMappingId = 0
                    objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                    objEBanking.ReferenceType = enmReferenceType.CheckHeader
                    objEBanking.ReferenceID = CCommon.ToLong(hfCheckHeaderID.Value)
                    objEBanking.IsActive = 1
                    objEBanking.MapBankStatementTransactions()
                End If

                ClientScript.RegisterClientScriptBlock(Page.GetType, "close", "SplitCheckEnds();", True)

            Else
                ShowMessage("Amount does not match with transaction amount, you need to make sure total amount of Check(s) is equal to " & lblSplitTotal.Text)
            End If

            'Response.Redirect("frmWriteCheck.aspx")
        Catch ex As Exception
            If ex.Message = "IM_BALANCE" Then
                ShowMessage("The Credit and Debit amounts you’re entering must be of the same amount")
            ElseIf ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Private Sub btnSavePrint_Click(sender As Object, e As System.EventArgs) Handles btnSavePrint.Click
        Try
            chkPrintCheck.Checked = True
            save()

            Response.Redirect("frmPrintChecksList.aspx?CheckHeaderID=" & hfCheckHeaderID.Value)
        Catch ex As Exception
            If ex.Message = "IM_BALANCE" Then
                ShowMessage("The Credit and Debit amounts you’re entering must be of the same amount")
            ElseIf ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Sub save()
        Try
            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                SaveCheckHeader()
                SaveCheckDetails()

                Dim lngJournalId As Long

                lngJournalId = SaveDataToHeader(TransactionInfo1.JournalID)
                SaveDataToGeneralJournalDetails(lngJournalId)
                ChangeDepositTo()

                objTransactionScope.Complete()
            End Using

            btnAllowCheckNO.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader(lngJournalId As Long) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lngJournalId
                .RecurringId = 0
                .EntryDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
                .Description = "Write Check:" & txtMemo.Text
                .Amount = CCommon.ToDecimal(hfTotal.Value)
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = ddlDepositTo.SelectedValue.Split("~")(0)
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails(lngJournalId As Long)
        Try
            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()

            objJE.TransactionId = CCommon.ToLong(hfHeaderTransactionId.Value)
            objJE.DebitAmt = 0
            objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(hfTotal.Value))
            objJE.ChartAcntId = ddlDepositTo.SelectedValue.Split("~")(0)
            objJE.Description = "Write Check"
            objJE.CustomerId = CCommon.ToLong(radCmbCompany.SelectedValue)
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.CheckHeader 'Write Check Header
            objJE.ReferenceID = CCommon.ToLong(hfCheckHeaderID.Value)

            objJEList.Add(objJE)

            Dim dtDetails As New DataTable

            If objChecks Is Nothing Then objChecks = New Checks()

            objChecks.DomainID = Session("DomainID")
            objChecks.CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)

            dtDetails = objChecks.FetchCheckDetails()

            For Each dr As DataRow In dtDetails.Rows
                objJE = New JournalEntryNew()

                objJE.TransactionId = dr("numTransactionId")
                objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dr("monAmount")))
                objJE.CreditAmt = 0
                objJE.ChartAcntId = dr("numChartAcntId")
                objJE.Description = dr("vcDescription")
                objJE.CustomerId = dr("numCustomerId")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = dr("numProjectID")
                objJE.ClassID = dr("numClassID")
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.CheckDetail  'Write Check Detail
                objJE.ReferenceID = CCommon.ToLong(dr("numCheckDetailID"))

                objJEList.Add(objJE)
            Next

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCheckHeader()
        Try
            If objChecks Is Nothing Then objChecks = New Checks()

            objChecks.DomainID = Session("DomainID")
            objChecks.AccountId = CCommon.ToLong(ddlDepositTo.SelectedValue.Split("~")(0))
            objChecks.CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)
            objChecks.DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
            objChecks.UserCntID = Session("UserContactID")
            objChecks.Amount = CCommon.ToDecimal(hfTotal.Value)
            objChecks.FromDate = CDate(calPaymentDate.SelectedDate & " 12:00:00")
            objChecks.CheckNo = IIf(chkPrintCheck.Checked, 0, CCommon.ToLong(txtCheckNo.Text))
            objChecks.Memo = txtMemo.Text
            objChecks.IsPrint = IIf(chkPrintCheck.Checked, False, True)

            objChecks.Type = enmReferenceType.CheckHeader  'Write Check
            objChecks.Mode = 2 'Insert/Edit

            Dim ds As DataSet = objChecks.ManageCheckHeader()

            Dim dtCheckHeader As DataTable = ds.Tables(0)

            If dtCheckHeader.Rows.Count > 0 Then
                hfCheckHeaderID.Value = dtCheckHeader.Rows(0)("numCheckHeaderID")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCheckDetails()
        Try
            Dim ds As New DataSet
            Dim ddlAccounts As DropDownList
            Dim lintAccountId() As String
            Dim dtrow As DataRow

            Dim dt As New DataTable

            CCommon.AddColumnsToDataTable(dt, "numCheckDetailID,numChartAcntId,monAmount,numProjectID,numClassID,vcDescription,numCustomerId,numTransactionId")

            For Each gvRow As GridViewRow In gvCheckDetails.Rows
                If CType(gvRow.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString <> "0" And CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text) > 0 Then ''And CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value.ToString <> "0" Then
                    dtrow = dt.NewRow

                    dtrow.Item("numCheckDetailID") = CCommon.ToLong(CType(gvRow.FindControl("lblCheckDetailID"), Label).Text)

                    ddlAccounts = CType(gvRow.FindControl("ddlAccounts"), DropDownList)
                    lintAccountId = ddlAccounts.SelectedItem.Value.Split("~")

                    dtrow.Item("numChartAcntId") = lintAccountId(0)
                    dtrow.Item("monAmount") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)
                    dtrow.Item("numClassId") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                    dtrow.Item("numProjectId") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)
                    dtrow.Item("vcDescription") = CType(gvRow.FindControl("txtDescription"), TextBox).Text
                    dtrow.Item("numCustomerId") = CCommon.ToLong(radCmbCompany.SelectedValue)

                    dtrow.Item("numTransactionId") = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)

                    dt.Rows.Add(dtrow)
                End If
            Next

            ds.Tables.Add(dt)

            If objChecks Is Nothing Then objChecks = New Checks()

            objChecks.DomainID = Session("DomainID")
            objChecks.CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)
            objChecks.JournalDetails = ds.GetXml()

            objChecks.SaveCheckDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SplitTransaction()
        Try
            Dim ldecCheckTotalAmount As Decimal

            Dim obj As GridMasterRegular = Page.Master
            obj.HideWebMenuUserControl = True

            If CCommon.ToLong(hdnTransactionID.Value) > 0 Then
                objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                Dim dtTransactionDetail As DataTable = objEBanking.GetTransactionDetails()

                If (dtTransactionDetail.Rows.Count > 0) Then
                    ' lblDepositTotalAmount.Text = CCommon.ToString(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))))
                    'hdnDepositTotalAmt.Value = CCommon.ToString(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))))
                    ldecCheckTotalAmount = Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount")))

                    lblSplitTotal.Text = CCommon.ToString((ReturnMoney(ldecCheckTotalAmount)))
                    lblSplitTotal.Visible = True
                    lblTotal.Visible = False

                    btnSave.Visible = False
                    btnSavePrint.Visible = False
                    btnSplitSave.Visible = True
                    Dim dtDatePosted As DateTime = Convert.ToDateTime(dtTransactionDetail.Rows(0)("dtDatePosted"))
                    calPaymentDate.SelectedDate = dtDatePosted
                    Dim CheckNumber As String = CCommon.ToString(dtTransactionDetail.Rows(0)("vcCheckNumber"))
                    If Not CheckNumber = "" Then
                        txtCheckNo.Text = CheckNumber
                    Else
                        chkPrintCheck.Checked = True
                    End If
                    Dim COAccountID As Long = CCommon.ToLong(dtTransactionDetail.Rows(0)("numAccountID"))

                    For Each item As ListItem In ddlDepositTo.Items
                        Dim val As String() = item.Value.Split("~")
                        If val(0) = COAccountID Then
                            ddlDepositTo.ClearSelection()
                            ddlDepositTo.Items.FindByValue(item.Value).Selected = True
                            ddlDepositTo.Enabled = False
                            Exit For

                        End If
                    Next

                End If

            End If

        Catch ex As Exception
            Throw ex

        End Try

    End Sub


    Private Sub btnSaveNew_Click(sender As Object, e As EventArgs) Handles btnSaveNew.Click
        Try
            save()
            Response.Redirect("../Accounting/frmWriteCheck.aspx?flag=2", False)
        Catch ex As Exception
            If ex.Message = "IM_BALANCE" Then
                ShowMessage("The Credit and Debit amounts you’re entering must be of the same amount")
            ElseIf ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If CCommon.ToLong(hfCheckHeaderID.Value) > 0 Then
                Dim objJournalEntry As New JournalEntry
                objJournalEntry.JournalId = 0
                objJournalEntry.BillPaymentID = 0
                objJournalEntry.DepositId = 0
                objJournalEntry.CheckHeaderID = CCommon.ToLong(hfCheckHeaderID.Value)
                objJournalEntry.BillID = 0
                objJournalEntry.CategoryHDRID = 0
                objJournalEntry.ReturnID = 0
                objJournalEntry.DomainID = Session("DomainId")
                objJournalEntry.UserCntID = Session("UserContactID")

                Try
                    objJournalEntry.DeleteJournalEntryDetails()
                    Response.Redirect("../Accounting/frmWriteCheck.aspx?flag=3", False)
                Catch ex As Exception
                    If ex.Message = "Undeposited_Account" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If CCommon.ToInteger(Session("DefaultClassType")) = 2 Then 'If Account class is set on company then we have to change class when compay selection changed
                UpdateClass()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub UpdateClass()
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UserCntID = Session("UserContactID")
            objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            Dim lngAccountClassID As Long = objCommon.GetAccountingClass()

            For Each row As GridViewRow In gvCheckDetails.Rows
                Dim ddlClass As DropDownList = DirectCast(row.FindControl("ddlClass"), DropDownList)

                If Not ddlClass Is Nothing AndAlso Not ddlClass.Items.FindByValue(lngAccountClassID) Is Nothing Then
                    ddlClass.Items.FindByValue(lngAccountClassID).Selected = True
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class