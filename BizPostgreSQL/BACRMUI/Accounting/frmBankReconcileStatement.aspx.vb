Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmBankReconcileStatement
    Inherits BACRMPage

    Dim objJournalEntry As JournalEntry
    Dim lngReconcileID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngReconcileID = CCommon.ToLong(GetQueryStringVal("ReconcileID"))
            If Not IsPostBack Then
                If lngReconcileID > 0 Then
                    LoadBankReconcile()

                    bindJournalEntries()
                End If
            End If
            ibtnPrint.Attributes.Add("onclick", "return PrintIt();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Sub LoadBankReconcile()
        Try
            Dim monPayment, monDeposit, monEndBalance, monBeginBalance, monDifference As Decimal
            monPayment = monDeposit = monEndBalance = monBeginBalance = monDifference = 0

            Dim objBankReconcile As New BankReconcile
            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 1
            objBankReconcile.ReconcileID = lngReconcileID

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()
            Dim dtReconcile As DataTable = ds.Tables(0)

            lblAccount.Text = dtReconcile.Rows(0)("vcAccountName")
            lblStatementDate.Text = String.Format("{0:MM/dd/yyyy}", dtReconcile.Rows(0)("dtStatementDate"))
            lblReconciledOn.Text = String.Format("{0:MM/dd/yyyy}", dtReconcile.Rows(0)("dtReconcileDate"))
            lblReconciledby.Text = dtReconcile.Rows(0)("vcReconciledby")
            hfAccountID.Value = CCommon.ToLong(dtReconcile.Rows(0)("numChartAcntId"))

            monPayment = CCommon.ToDecimal(dtReconcile.Rows(0)("Payment"))
            monDeposit = CCommon.ToDecimal(dtReconcile.Rows(0)("Deposit"))
            monBeginBalance = CCommon.ToDecimal(dtReconcile.Rows(0)("monBeginBalance"))
            monEndBalance = CCommon.ToDecimal(dtReconcile.Rows(0)("monEndBalance"))

            lblServiceChargeAmt.Text = ReturnMoney(CCommon.ToDecimal(dtReconcile.Rows(0)("monServiceChargeAmount")))
            lblInterestEarnedAmt.Text = ReturnMoney(CCommon.ToDecimal(dtReconcile.Rows(0)("monInterestEarnedAmount")))

            lblBeginningBalance.Text = ReturnMoney(monBeginBalance)
            lblEndingBalAmt.Text = ReturnMoney(monEndBalance)
            lblPayment.Text = ReturnMoney(monPayment)
            lblDeposit.Text = ReturnMoney(monDeposit)
            lblClearedBalAmt.Text = ReturnMoney(monBeginBalance + monDeposit - monPayment)

            monDifference = monEndBalance - (monBeginBalance + monDeposit - monPayment)

            lblDifferenceAmt.Text = ReturnMoney(Math.Truncate(monDifference * 100) / 100)

            If (Math.Truncate(monDifference * 100) / 100) <> 0 Then
                lblDifferenceAmt.ForeColor = Color.Red
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub bindJournalEntries()
        Try
            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

            Dim dt As DataTable
            objJournalEntry.ChartAcntId = hfAccountID.Value
            objJournalEntry.DomainID = Session("DomainId")
            objJournalEntry.ReconcileID = lngReconcileID

            Dim ds As DataSet
            ds = objJournalEntry.GetJournalEntryListForBankReconciliation(0, False, 1)

            dt = ds.Tables(0)

            dgJournalEntry.DataSource = dt
            dgJournalEntry.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            If Not IsDBNull(CloseDate) Then Return (FormattedDateFromDate(CloseDate, Session("DateFormat")))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class