﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGeneralLedger.aspx.vb"
    Inherits=".frmGeneralLedger" MasterPageFile="~/common/GridMasterRegular.Master"
    EnableViewState="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>General Ledger</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
     <style>
        #RepeaterTable{
  height: calc(100vh - 240px);
  overflow: auto
        }
        .td1{
            position: -webkit-sticky;
  position: sticky;
  top: 0;
        }
    </style>
    <script type="text/javascript" src="../JavaScript/jquery-ui.min.js"></script>

    <script language="javascript" type="text/javascript">

        function OpenJournalDetailsPage(url) {
            window.location.href = url;
            return false;
        }
        function reDirect(a) {
            document.location.href = a;
            return false;
        }
        function OpenTimeExpense(url) {

            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, "BizInvoiceGL", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenMultiSelect() {
            if (document.form1.ddlCOA.selectedIndex == 1) {
                window.open("../Accounting/frmSelectMultipleAccounts.aspx?AcntCode=@" + document.form1.ddlType.options[document.form1.ddlType.selectedIndex].value, "MultiSelect", 'toolbar=no,titlebar=no,left=200, top=300,width=450,height=300,scrollbars=no,resizable=yes');
                return true;
            } else {
                SearchRecords(true);
            }
        }

        function OpenBill(a) {
            window.open('../Accounting/frmAddBill.aspx?BillID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }

        function Go() {
            if (document.form1.ddlType.value == 0) {
                alert("Please Select Account Type");
                document.form1.ddlType.focus();
                return false;
            }
            if (document.form1.hdnAccounts.value.length == 0 && document.form1.ddlCOA.value == 0) {
                alert("Please Select a Accounts");
                document.form1.ddlCOA.focus();
                return false;
            }

            return true;
        }

        function pageLoaded() {
            var pageIndex = 0;
            var pageCount;
            var AccountList;
            var LoadComplete = 0;
            CallWait = 0;

            List1 = { items: [{ index: "-1", AccountID: "0", ItemCode: "0", PrevBalance: "0", Pageindex: "0", PageIndexCalled: "0", Loaded: "0", LoadComplete: '0' }] };

            $("[id$='txtHeaderFrom']").datepicker({
                dateFormat: 'mm/dd/yy', onSelect: function (dateText, inst) {
                    $("[id$=hdnFromDate]").val(dateText);
                    SearchRecords(true);
                }
            });
            $("[id$='txtHeaderTo']").datepicker({
                dateFormat: 'mm/dd/yy', onSelect: function (dateText, inst) {
                    $("[id$=hdnToDate]").val(dateText);
                    SearchRecords(true);
                }
            });


            FillDropDown();
            $('.ReconStatus').change(function () {
                var TransactionId = $(this).parent().find('#lblTransactionId').text();

                if (confirm('Are you sure want to change Bank Recon Status?')) {
                    //console.log(TransactionId);
                    //console.log($(this).val());
                    $(this).parent().find('#lblReconStatus').text($(this).val());
                    $.ajax({
                        type: "POST",
                        url: "../common/Common.asmx/UpdateBankReconStatus",
                        data: "{TransactionId:'" + TransactionId + "',Status:'" + $(this).val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            // Insert the returned HTML into the <div>.
                            //UpdateMsgStatus(msg.d);
                        }
                    });
                }
                else {
                    var ReconStatus = $(this).parent().find('#lblReconStatus').text();
                    $(this).val(ReconStatus);
                }
            });

            $("#RepeaterTable").scroll(function () {
                if ($("#RepeaterTable").prop('scrollHeight') - $("#RepeaterTable").scrollTop() == $("#RepeaterTable").outerHeight()) {
                    GetAccountTrans();
                }
            });

            $("[id$=ddlHeaderTransactionType]").change(function () {
                SearchRecords(true);
            });

            $("#ddlReconStatusFilter").change(function () {
                $("#hdnReconStatus").val($("#ddlReconStatusFilter").val());
                SearchRecords(true);
            });

            $("#hplClearGridCondition").click(function () {
                $("[id$=ddlHeaderTransactionType]").val("0");
                $("[id$=txtHeaderName]").val("");
                $("[id$=txtHeaderDescription]").val("");
                $("[id$=txtHeaderSplit]").val("");
                $("[id$=txtHeaderAmount]").val("");
                $("[id$=txtHeaderNarration]").val("");
                $("#btnReloadGL").click();
            });


            //var strAccountList = $("#hdnAccounts").val();
            PushAccountList();
            $("[id*=RepeaterTable] tr").eq(1).hide();
        }

        function SearchRecords(isSelectChange) {
            if (isSelectChange) {
                $("#btnReloadGL").click();
            } else if(event != null && event.keyCode == 13){
                $("#btnReloadGL").click();
            } else {
                return true;
            }
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }

        function FillDropDown() {
            var i = 0;
            var RepGeneralLedger = $('#RepeaterTable tr');
            RepGeneralLedger.each(function () {
                //alert('enters each Repeater Row');
                $(this).find('#ddlReconStatus').each(function () {
                    $(this).html($('#ddlReconStatus1').html());
                    var ReconStatus = $(this).parent().find('#lblReconStatus').text();
                    $(this).val(ReconStatus);
                    i += 1;
                });
            });
            // alert('Total Count : ' + i);
        }

        function PushAccountList(strAccountList) {
            //console.log('Populates Account Id list. \r\n');
            var pageIndex1 = 0;

            var AccIds = $("#hdnAccounts").val();
            var ItemCode = $("#hdnItemCode").val();

            if (AccIds != "0" && AccIds != "") {

                var arrAccountIds = AccIds.split(',');
                // console.log('ItemCode  : |' + ItemCode + '|. \r\n');

                $.each(arrAccountIds, function (index, value) {
                    // alert('enters Each Account Id');
                    //console.log('Account ID : |' + value + '|. \r\n');
                    if (value.trim() != '') {
                        List1.items.push({ "index": index, "AccountID": value, "ItemCode": ItemCode, "PrevBalance": "0", "Pageindex": "0", "PageIndexCalled": "0", "Loaded": "0", "LoadComplete": "0" });
                        //console.log('Account ID : ' + value + ', ItemCode : ' + ItemCode+' is added to array to Import Transactions.' + '.\r\n ');
                    }
                });
            }

            console.log(List1);
            GetAccountTrans()
        }

        function GetTransactionDetails(CommandName, TransactionID) {
            //  console.log('enters GetTransactionDetails');
            //console.log('CommandName : ' + CommandName + ', TransactionID : ' + TransactionID);
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',TransactionID:'" + TransactionID + "'}";
            // console.log('dataParam : ' + dataParam);

            $.ajax({
                type: "POST",
                url: "../Accounting/frmGeneralLedger.aspx/WebMethodGetTransactionDetail",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    OnGetTransactionDetailsSuccess(Jresponse, CommandName, TransactionID);
                },
                beforeSend: function () {
                    $("[id$=UpdateProgress]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgress]").hide();
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }
            });
        }

        function OnGetTransactionDetailsSuccess(response, CommandName, TransactionID) {
            var linJournalId;
            var lintCheckId;
            var lintCashCreditCardId;
            var lintOppId;
            var lintOppBizDocsId;
            var lintDepositId;
            var lintCategoryHDRID;
            var lintChartAcntId;
            var lintBillPaymentID;
            var lintBillID;
            var lintReturnID;
            var lintLandedCostOppId;

            //  console.log('CommandName : ' + CommandName);
            var TransactionDetails = response.Table0;
            // console.log('TransactionDetails : ' + TransactionDetails);

            $.each(TransactionDetails, function (index, Transaction) {
                linJournalId = Transaction.JournalId;
                lintCheckId = Transaction.CheckId;
                lintCashCreditCardId = Transaction.CashCreditCardId;
                lintOppId = Transaction.numOppId;
                lintOppBizDocsId = Transaction.numOppBizDocsId;
                lintDepositId = Transaction.numDepositId;
                lintCategoryHDRID = Transaction.numCategoryHDRID;
                lintChartAcntId = Transaction.numAccountId;
                lintBillPaymentID = Transaction.numBillPaymentID;
                lintBillID = Transaction.numBillID;
                lintReturnID = Transaction.numReturnID;
                lintLandedCostOppId = Transaction.numLandedCostOppId;
            });
            // console.log('linJournalId : ' + linJournalId + ', lintCheckId : ' + lintCheckId + ', lintCashCreditCardId : ' +
            //     lintCashCreditCardId + ', lintOppId : ' + lintOppId + ', lintOppBizDocsId : ' + lintOppBizDocsId + ', lintDepositId : ' + lintDepositId
            //       + ', lintCategoryHDRID : ' + lintCategoryHDRID + ', lintChartAcntId : ' + lintChartAcntId + ', lintBillPaymentID : ' + lintBillPaymentID
            //       + ', lintBillID : ' + lintBillID + ', lintReturnID : ' + lintReturnID);

            if (CommandName == 'Edit') {
                // console.log('Command is Edit');
                if (linJournalId != 0 && lintCheckId == 0 && lintCashCreditCardId == 0 && lintOppId == 0 && lintOppBizDocsId == 0 && lintDepositId == 0 && lintCategoryHDRID == 0 && lintBillID == 0 && lintBillPaymentID == 0 && lintReturnID == 0) {
                    window.location.href = "../Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=" + linJournalId;
                    //console.log('Navigate to ' + '../Accounting/frmJournalEntryList.aspx?frm=GeneralLedger&JournalId=' + linJournalId);
                }
                else if (lintCheckId != 0) {
                    window.location.href = "../Accounting/frmWriteCheck.aspx?frm=GeneralLedger&JournalId=" + linJournalId + "&CheckHeaderID=" + lintCheckId;
                }
                else if (lintCashCreditCardId != 0) {
                    window.location.href = "../Accounting/frmCash.aspx?frm=GeneralLedger&JournalId=" + linJournalId + "&CashCreditCardId=" + lintCashCreditCardId;
                }
                else if (lintOppId != 0 && lintOppBizDocsId != 0) {
                    window.open("../opportunity/frmBizInvoice.aspx?frm=GeneralLedger&OpID=" + lintOppId + "&OppBizId=" + lintOppBizDocsId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes');
                }
                else if (lintOppId != 0 && lintOppBizDocsId == 0) {
                    window.location.href = "../opportunity/frmOpportunities.aspx?opid=" + lintOppId;
                }
                else if (lintBillID != 0) {
                    if (lintLandedCostOppId > 0)
                        window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + lintLandedCostOppId, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
                    else
                        window.open("../Accounting/frmAddBill.aspx?BillID=" + lintBillID, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes');
                }
                else if (lintBillPaymentID != 0) {
                    window.location.href = "../Accounting/frmVendorPayment.aspx?frm=GeneralLedger&BillPaymentID=" + lintBillPaymentID;
                }
                else if (lintReturnID != 0) {
                    window.location.href = "../opportunity/frmReturnDetail.aspx?ReturnID=" + lintReturnID;
                }
                else if (lintDepositId != 0) {
                    //console.log('Command is Edit : and Deposit is  greater than Zero');

                    var DomainID = '<%= Session("DomainId")%>';
                            var Param = "{DomainID:'" + DomainID + "',lintDepositId:'" + lintDepositId + "'}";

                            $.ajax({
                                type: "POST",
                                url: "../Accounting/frmGeneralLedger.aspx/WebMethodGetDepositDetails",
                                data: Param,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    // console.log('Command is Edit : and Deposit is  greater than Zero and getting Deposit Details Success');

                                    var JDepositDetails = $.parseJSON(response.d);
                                    var DepositDetails = JDepositDetails.Table0;
                                    var tintDepositePage;
                                    var lintCheckId;

                                    $.each(DepositDetails, function (index, Deposit) {
                                        tintDepositePage = Deposit.tintDepositePage;
                                        // console.log('tintDepositePage : ' + tintDepositePage);
                                    });
                                    if (tintDepositePage == 2) {
                                        // console.log("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + lintDepositId);
                                        //window.location.href = "../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + lintDepositId;
                                        RPPopup = window.open("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + lintDepositId, 'ReceivePayment', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
                                        console.log(RPPopup);
                                    }
                                    else if (tintDepositePage == 1) {
                                        // console.log("../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" + lintDepositId);
                                        window.location.href = "../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" + linJournalId + "&ChartAcntId=" + lintChartAcntId + "&DepositId=" + lintDepositId;
                                    }
                                    // console.log('Deposit Details : ' + DepositDetails);
                                },
                                failure: function (response) {
                                    var errMessage = 'Sorry!.,Getting Deposit details failed!.,' + 'Please contact the BizAutomation support.';
                                    //console.log("errMessage : " + errMessage);
                                    $("#lblErrMessage").text(errMessage.toString());
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    // console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                                    // console.log("textStatus : " + textStatus);
                                    // console.log("errorThrown : " + errorThrown);
                                    var errMessage = 'Sorry!.,Getting Deposit details failed!.,' + errorThrown + '., Please contact the BizAutomation support.';
                                    // console.log("errMessage : " + errMessage);
                                    $("#lblErrMessage").text(errMessage.toString());
                                }, complete: function () {
                                }
                            });
                        }
}
else if (CommandName == 'Detail') {
    //console.log('Command is Detail');
    window.location.href = "../Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=" + linJournalId;
}
else if (CommandName == 'Delete') {
    var IsValidDelete = false;
    //console.log('Command is Delete');

    if (lintBillID != 0 && lintLandedCostOppId > 0) {
        alert('Landed Cost transaction can not be deleted from General Ledger. Please open Landed Cost then try to delete.');
        return;
    }

    var ReconStatus;

    $("[id*=RepeaterTable] tr").each(function () {
        var TransID = $(this).find('#lblTransactionId').text();
        if (TransactionID == TransID) {
            ReconStatus = $(this).find('#lblReconStatus').text();
        }
    });

    // console.log('parent : ' + ReconStatus);
    if (ReconStatus == 'R') {
        if (confirm('This transaction is reconciled, Are you sure, you want to delete the selected transaction?')) {
            IsValidDelete = true;
        }
        else {
            event.preventDefault();
            IsValidDelete = false;
        }
    }
    else {
        if (confirm('Are you sure, you want to delete the selected transaction?')) {
            IsValidDelete = true;
        }
        else {
            event.preventDefault();
            IsValidDelete = false;
        }
    }
    if (IsValidDelete == true) {
        if (linJournalId != 0) {
            if (lintOppBizDocsId > 0 && lintBillID == 0 && lintDepositId == 0 && lintBillPaymentID == 0) {
                alert('This transaction can not be deleted. If you want to change or delete it, you must edit or delete selected bizdoc from order details.');
            }
            else {
                // console.log('Execute Delete code here!.,');

                var DomainID = '<%= Session("DomainId")%>';
                var UserContactID = '<%= Session("UserContactID")%>';

                var deleteParam = "{DomainID:'" + DomainID + "',UserContactID:'" + UserContactID + "',linJournalId:'" + linJournalId +
                     "',lintBillPaymentID:'" + lintBillPaymentID + "',lintDepositId:'" + lintDepositId +
                      "',lintCheckId:'" + lintCheckId + "',lintBillID:'" + lintBillID +
                    "',lintCategoryHDRID:'" + lintCategoryHDRID + "',lintReturnID:'" + lintReturnID + "'}";

                $.ajax({
                    type: "POST",
                    url: "../Accounting/frmGeneralLedger.aspx/WebMethodDeleteTransaction",
                    data: deleteParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var SuccessMessage = 'Transaction details deleted Successfully!.,';
                        $("#lblErrMessage").text(SuccessMessage.toString());
                        $("[id*=RepeaterTable] tr").each(function () {
                            var TransID = $(this).find('#lblTransactionId').text();
                            if (TransactionID == TransID) {
                                $(this).remove();
                            }
                        });

                        // console.log('Delete Transaction Success');
                        //  console.log('response : ' + response + 'response D : ' + response.d);
                    },
                    failure: function (response) {
                        var errMessage = 'Sorry!.,unable to delete the Transaction!.,';
                        // console.log("errMessage : " + errMessage);
                        $("#lblErrMessage").text(errMessage.toString());
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                        //  console.log("textStatus : " + textStatus);
                        //  console.log("errorThrown : " + errorThrown);
                        var errMessage = 'Sorry!.,unable to delete the Transaction!.,';
                        //console.log("errMessage : " + errMessage);
                        $("#lblErrMessage").text(errMessage.toString());
                    }, complete: function () {
                    }
                });
            }
        }
    }
}
}

function GetAccountTrans() {
    //var AccIds = strAccountList.toString();
    var pageIndex1;
    var AccountID;
    var ItemCode;
    var reqForward = 0;
    var PrevBalance = 0;
    if (CallWait == 0) {
        $.each(List1.items, function () {
            var item = this;
            // console.log('Enters each List Array Item. Account ID : ' + item.AccountID);
            if (item.index > -1) {
                if (item.LoadComplete == 0) {
                    if (item.Loaded > 0) {
                        pageIndex1 = item.Pageindex + 1;
                    }
                    else {
                        pageIndex1 = 1;
                    }
                    AccountID = item.AccountID;
                    ItemCode = item.ItemCode;
                    PrevBalance = item.PrevBalance;
                    if (AccountID != '' && AccountID != '0') {
                        if (item.PageIndexCalled < pageIndex1) {
                            GetTransactions(AccountID, pageIndex1, PrevBalance, ItemCode);
                            item.PageIndexCalled = pageIndex1;
                            reqForward = 1;
                            return false;
                        }
                    }
                }
            }
        });
    }
}

function GetTransactions(AccountID, pageIndex1, PrevBalance, ItemCode) {
    $.each(List1.items, function () {
        var item = this;
        if (item.index > -1) {
            if (item.LoadComplete == 0) {
                if (AccountID == item.AccountID) {
                    if (pageIndex1 > item.Pageindex) {
                        CallWait = 1;
                        $("[id$=UpdateProgress]").show();
                        var DomainID = '<%= Session("DomainId")%>';
                        var PagingRows = 0;
                        var ReconStatus = $('#hdnReconStatus').val();
                        var DivisionId = $('#hdnDivisionId').val();
                        $.ajax({
                            type: "POST",
                            url: "../Accounting/frmGeneralLedger.aspx/WebMethodLoadGeneralDetails",
                            data: JSON.stringify({
                                "DomainID": DomainID
                                ,"pageIndex": pageIndex1
                                ,"PrevBalance": PrevBalance
                                ,"strDateFormat": "MM/dd/yyyy"
                                , "FromDate": $("[id$='txtHeaderFrom']").val()
                                , "ToDate": $("[id$='txtHeaderTo']").val()
                                ,"DivisionId": DivisionId
                                ,"AccountID": AccountID
                                ,"ItemCode": ItemCode
                                ,"ReconStatus": ReconStatus
                                ,"AccountClassID": $("#hdnAccountClassID").val()
                                ,"filterTransactionType": parseInt($("[id$=ddlHeaderTransactionType]").val())
                                ,"fiterName": $("[id$=txtHeaderName]").val()
                                ,"fiterDescription": $("[id$=txtHeaderDescription]").val()
                                ,"fiterSplit": $("[id$=txtHeaderSplit]").val()
                                ,"fiterAmount": $("[id$=txtHeaderAmount]").val()
                                ,"fiterNarration": $("[id$=txtHeaderNarration]").val()
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: OnSuccess,
                            failure: function (response) {
                                //  Console.Log('Sorry!.,Getting records failed!.,');
                                $("[id$=UpdateProgress]").hide();
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                //   console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                                //   console.log("textStatus : " + textStatus);
                                //   console.log("errorThrown : " + errorThrown);
                                $("[id$=UpdateProgress]").hide();
                                var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                                //  console.log("errMessage : " + errMessage);
                                $("#lblErrMessage").text(errMessage.toString());
                                alert($("#lblErrMessage").text());
                            }, complete: function () {
                                CallWait = 0;
                                $("[id$=UpdateProgress]").hide();
                            }
                        });
                    }
                    else {
                        return false;
                    }
                }
            }
        }

    });


}

function OnSuccess(response) {
    try {
        var Jresponse = $.parseJSON(response.d);
        var AccountId = '';
        var ItemID = '';
        var TransLoaded = '';
        var PrevBalance = 0;
        var ImportedTransactionCount = 0;
        var TransactionDetails = Jresponse.Table1;

        var row = $("[id*=RepeaterTable] tr").eq(1).clone(true);
        $("[id*=RepeaterTable] tr").eq(1).hide();
        AccountId = Jresponse.AccountDetails[0].AccountID;
        TransLoaded = Jresponse.AccountDetails[0].TransactionLoaded;
        PrevBalance = Jresponse.AccountDetails[0].PrevBalance;
        ItemID = Jresponse.AccountDetails[0].ItemID;
        var i = 0;
        if (ItemID != '0') {
            var headerRow = $("[id*=RepeaterTable] tr").eq(0);
            headerRow.find("td:eq(6)").remove();
            headerRow.find("td:eq(7)").remove();
            headerRow.find("td:eq(8)").remove();
        }

        $.each(TransactionDetails, function (index, Transaction) {
            i++;
            var LogMessage = '';
            LogMessage += 'Date : ' + Transaction.Date + ', ';
            row.find('#lblJDate').html(Transaction.Date);

            LogMessage += 'TransactionType : ' + Transaction.TransactionType + ', ';
            row.find('#lblTransactionType').text(Transaction.TransactionType);
            row.find('#lblType').text(Transaction.TransactionType);

            LogMessage += 'numTransactionId : ' + Transaction.numTransactionId + ', ';
            row.find('#lblTransactionId').text(Transaction.numTransactionId);

            LogMessage += 'CompanyName : ' + Transaction.CompanyName + ', ';
            row.find('#lblCompanyName').html(Transaction.CompanyName);

            LogMessage += 'TranDesc : ' + Transaction.TranDesc + ', ';
            row.find('#lblTranDesc').html(Transaction.TranDesc);

            LogMessage += 'vcSplitAccountName : ' + Transaction.vcSplitAccountName + ', ';
            row.find('#lblvcAccountName').html(Transaction.vcSplitAccountName);

            LogMessage += 'numDebitAmt : ' + Transaction.numDebitAmt + ', ';
            row.find('#lblnumDebitAmt').html(Transaction.numDebitAmt);

            LogMessage += 'balance : ' + Transaction.balance + ', ';
            row.find('#lblBalance').html(Transaction.balance);

            LogMessage += 'Narration : ' + Transaction.Narration + ', ';
            row.find('#lblNarration').text(Transaction.Narration);

            LogMessage += 'TranRef : ' + Transaction.TranRef + ', ';
            row.find('#lblTranRef').text(Transaction.TranRef);

            LogMessage += 'ddlReconStatus HTML : ' + $('#ddlReconStatus1').html() + '.';
            row.find('#ddlReconStatus').html($('#ddlReconStatus1').html());

            if (Transaction.bitReconcile == "1") {
                row.find('#lblReconStatus').text("R");

                LogMessage += 'Transaction Reconciled , ';
                row.find('#ddlReconStatus option').each(function () {
                    if ($(this).val() == 'R') {
                        $(this).attr("selected", true);
                    }
                });
            }
            else if (Transaction.bitCleared == "1") {
                row.find('#lblReconStatus').text("C");
                LogMessage += 'Transaction Cleared , ';
                row.find('#ddlReconStatus option').each(function () {
                    if ($(this).val() == 'C') {
                        $(this).attr("selected", true);
                    }
                });
            }
            else {
                LogMessage += 'Transaction neither Cleared nor Reconsiled, ';
            }
            if (ItemID != '0') {
                row.find('#btnDeleteAction').remove();
                row.find('#lnkDelete').parent().remove();
                row.find('#lblNarration').parent().remove();
                row.find('#lblBalance').parent().remove();
                row.find('#ddlReconStatus').parent().remove();
            }
            if (Transaction.numTransactionId == '-1') {
                row.find('#ddlReconStatus').remove();
                row.find('#btnDeleteAction').remove();
                row.find('#lnkDelete').remove();
                row.find('#lnkDetail').remove();

                row.show();
                $("[id*=RepeaterTable] tbody").append(row);
                row = $("[id*=RepeaterTable] tr").eq(1).clone(true);
                LogMessage += 'Account Name : ' + Transaction.Date;
                LogMessage += ', So ddlReconStatus and btnDeleteAction Controls Removed, ';
            }
            else {
                ImportedTransactionCount++;
                row.find('#ddlReconStatus').show();
                row.find('#btnDeleteAction').show();

                if (Transaction.numLandedCostOppId > 0)
                    row.find('#btnDeleteAction').hide();
                else
                    row.find('#btnDeleteAction').show();

                row.find('#lnkDelete').show();
                row.find('#lnkDetail').show();
                row.find('#lnkType').attr("onclick", "return CallEditCommand('" + Transaction.numTransactionId + "');");
                row.find('#btnDeleteAction').attr("onclick", "return CallDeleteCommand('" + Transaction.numTransactionId + "');");
                row.find('#lnkDetail').attr("onclick", "return CallDetailCommand('" + Transaction.numTransactionId + "');");
                row.show();
                $("[id*=RepeaterTable] tbody").append(row);
                row = $("[id*=RepeaterTable] tr:last-child").clone(true);

            }
            //console.log('Row count : ' + i);
            //console.log(LogMessage);
        });
        // console.log('Imported Transaction Count for Account ID : ' + AccountId + ', is : ' + ImportedTransactionCount + '.\r\n ');
        $.each(List1.items, function () {
            var item = this;
            var calPgIndex = 0;
            if (item.index > -1) {
                if (item.AccountID == AccountId) {
                    item.Loaded = TransLoaded;
                    item.PrevBalance = PrevBalance;
                    var calPgIndex = Math.floor(item.Loaded / 100);
                    var rem = item.Loaded % 100;
                    if (rem > 0) {
                        calPgIndex++;
                    }
                    item.Pageindex = calPgIndex;
                    if (ImportedTransactionCount < 100) {
                        item.LoadComplete = "1";

                        if ($("#RepeaterTable > table").outerHeight() < $("#RepeaterTable").outerHeight()) {
                            CallWait = 0;
                            GetAccountTrans();
                        }
                    }
                   
                    return false;
                }
            }
        });
        $("[id$=UpdateProgress]").hide();
        CallWait = 0;
        //if ($(window).height() >= $("body").height()) {
        //    // console.log('has no ScrollBar');
        //    GetAccountTrans();
        //}
    }
    catch (e) {
        var errMessage = e.message + '. ' + e.description + '., Please contact the BizAutomation support.';
        $("#lblErrMessage").text(errMessage);
        console.log('error : ' + e.responseText);
    }

}

function CallEditCommand(TransactionID) {
    var HiddenValue = 'Edit,' + TransactionID;
    GetTransactionDetails('Edit', TransactionID);
    return false;
}

function CallDeleteCommand(TransactionID) {
    var HiddenValue = 'Delete,' + TransactionID;
    GetTransactionDetails('Delete', TransactionID);
    return false;
}

function CallDetailCommand(TransactionID) {
    var HiddenValue = 'Detail,' + TransactionID;
    GetTransactionDetails('Detail', TransactionID);
    return false;
}


    </script>
    <style type="text/css">
        .VisibleNone {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left" runat="server" id="trAccounts">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            Account Type:
                        </label>
                        <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server" Width="225" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:DropDownList CssClass="VisibleNone" ID="ddlReconStatus1" runat="server" EnableViewState="false">
                            <asp:ListItem Value=" "></asp:ListItem>
                            <asp:ListItem Value="C">C</asp:ListItem>
                            <asp:ListItem Value="R">R</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>
                            Select Accounts:
                        </label>
                        <asp:DropDownList ID="ddlCOA" runat="server" CssClass="form-control" Width="225" onchange="OpenMultiSelect();" />
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="form-group">
                        <ContentTemplate>
                            <asp:LinkButton runat="server" ID="ibExportExcel" AlternateText="Export to Excel" ToolTip="Export to Excel" class="btn btn-success"><i class="fa fa-file-excel-o"></i></asp:LinkButton>
                            <asp:LinkButton ID="imgBtnExportPDF" CssClass="btn btn-primary" runat="server"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ibExportExcel" />
                            <asp:PostBackTrigger ControlID="imgBtnExportPDF" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblErrMessage" Visible="true" Style="color: Crimson;" runat="server" EnableViewState="False"></asp:Label>
    <asp:Literal ID="litMessage" Visible="true" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="General Ledger" runat="server" ID="lblGridTitle" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmgeneralledger.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Button ID="btnReloadGL" runat="server" CssClass="btn btn-primary" OnClientClick="return Go();" style="display:none;"></asp:Button>
                <table cellpadding="0" width="100%" cellspacing="0" border="0" bgcolor="white" runat="server"
                    id="tblExport">
                    <tr valign="top">
                        <td valign="top">
                            <asp:UpdatePanel ID="uppnlGLTransList" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                </Triggers>
                                <ContentTemplate>

                                    <div id="RepeaterTable">
                                        <asp:Repeater ID="RepGeneralLedger" runat="server">
                                            <HeaderTemplate>
                                                <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered" border="0" width="100%">
                                                    <tr class="hs">
                                                        <th class="td1" align="center" style="white-space:nowrap">
                                                            <b>Date</b>
                                                            <br />
                                                            <ul class="list-inline">
                                                                <li>
                                                                    <asp:TextBox style="width:105px" class="form-control" id="txtHeaderFrom" runat="server" placeholder="mm/dd/yyyy" />
                                                                </li>
                                                                <li>
                                                                    <asp:TextBox style="width:105px" class="form-control" id="txtHeaderTo" runat="server" placeholder="mm/dd/yyyy" />
                                                                </li>
                                                            </ul>
                                                        </th>
                                                        <th class="td1" align="center" style="white-space:nowrap">
                                                            <b>Transaction Type</b>
                                                            <br />
                                                            <asp:DropDownList runat="server" ID="ddlHeaderTransactionType" CssClass="form-control">
                                                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Bill" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="BizDocs Invoice" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="BizDocs Purchase" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Checks" Value="4"></asp:ListItem>
                                                                <asp:ListItem Text="Credit Memo" Value="5"></asp:ListItem>
                                                                <asp:ListItem Text="Deposit" Value="6"></asp:ListItem>
                                                                <asp:ListItem Text="Journal" Value="7"></asp:ListItem>
                                                                <asp:ListItem Text="Landed Cost" Value="8"></asp:ListItem>
                                                                <asp:ListItem Text="Pay Bill" Value="9"></asp:ListItem>
                                                                <asp:ListItem Text="PO Fulfillment" Value="10"></asp:ListItem>
                                                                <asp:ListItem Text="Purchase Return Credit Memo" Value="11"></asp:ListItem>
                                                                <asp:ListItem Text="Receved Pmt" Value="12"></asp:ListItem>
                                                                <asp:ListItem Text="Refund Against Credit Memo" Value="13"></asp:ListItem>
                                                                <asp:ListItem Text="Sales Return Credit Memo" Value="14"></asp:ListItem>
                                                                <asp:ListItem Text="Sales Return Refund" Value="15"></asp:ListItem>
                                                                <asp:ListItem Text="Time And Expenses" Value="16"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Name</b>
                                                            <br />
                                                            <asp:TextBox ID="txtHeaderName" runat="server" CssClass="form-control" onkeydown='return SearchRecords(false);'></asp:TextBox>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Memo/Description</b>
                                                            <br />
                                                            <asp:TextBox ID="txtHeaderDescription" runat="server" CssClass="form-control" onkeydown='return SearchRecords(false);'></asp:TextBox>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Split</b>
                                                            <br />
                                                            <asp:TextBox ID="txtHeaderSplit" runat="server" CssClass="form-control" onkeydown='return SearchRecords(false);'></asp:TextBox>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Amount</b>
                                                            <br />
                                                            <asp:TextBox ID="txtHeaderAmount" runat="server" CssClass="form-control" onkeydown='return SearchRecords(false);'></asp:TextBox>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Balance</b>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Narration</b>
                                                            <br />
                                                            <asp:TextBox ID="txtHeaderNarration" runat="server" CssClass="form-control" onkeydown='return SearchRecords(false);'></asp:TextBox>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <asp:DropDownList ID="ddlReconStatusFilter" runat="server" ClientIDMode="Static" EnableViewState="false">
                                                                <asp:ListItem Value="A">All</asp:ListItem>
                                                                <asp:ListItem Value="C">C</asp:ListItem>
                                                                <asp:ListItem Value="R">R</asp:ListItem>
                                                                <asp:ListItem Value="N">Not C/R</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </th>
                                                        <th class="td1" align="center"></th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class=' <%# IIf(Container.ItemIndex Mod 2 = 0, "tr1", "normal1")%>'>
                                                    <td>
                                                        <asp:Label ID="lblJDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Date")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit">
                                                            <asp:Label ID="lblTransactionType" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TransactionType")%>'></asp:Label>

                                                        </asp:LinkButton>
                                                        <asp:Label ID="lblType" runat="server" Visible="false"><%#DataBinder.Eval(Container.DataItem, "TransactionType")%></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCompanyName" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CompanyName")%>'></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTranDesc" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TranDesc")%>'></asp:Label>


                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblvcAccountName" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "vcSplitAccountName")%>'></asp:Label>


                                                    </td>
                                                    <td align="right" class="money" style="white-space:nowrap;">
                                                        <asp:Label ID="lblnumDebitAmt" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numDebitAmt")%>'></asp:Label>


                                                    </td>
                                                    <td style="width: auto; white-space:nowrap;" align="right" class="money">
                                                        <asp:Label ID="lblBalance" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "balance")%>'></asp:Label>

                                                    </td>
                                                    <td style="width: auto;">
                                                        <asp:Label ID="lblTranRef" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TranRef")%>'></asp:Label>
                                                        <asp:Label ID="lblNarration" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Narration")%>'></asp:Label>
                                                    </td>
                                                    <td style="width: auto;" align="center">
                                                        <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
                                                        <asp:Label ID="lblReconStatus" EnableViewState="false" runat="server" Style="display: none"></asp:Label>
                                                        <asp:DropDownList ID="ddlReconStatus" runat="server" CssClass="ReconStatus" EnableViewState="false"></asp:DropDownList>
                                                    </td>
                                                    <td style="width:50px; white-space:nowrap;">
                                                        <asp:LinkButton ID="btnDeleteAction" runat="server" CssClass="button Delete btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkDeleteAction" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                                                        <asp:Label ID="lblFYClosed" Text="Closed" runat="server" Visible="false" CssClass="normal4" />
                                                        <asp:LinkButton ID="lnkDetail" runat="server" CssClass="hyperlink" CommandName="Detail" ToolTip="Journal detail">
                                                 <img src="../images/Text.gif" />
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnAccounts" runat="server" />
    <asp:HiddenField ID="hdnItemCode" runat="server" />
    <asp:HiddenField ID="hdnDivisionId" runat="server" />
    <asp:HiddenField ID="hdnAccountClassID" runat="server" />
    <asp:HiddenField ID="hdnReconStatus" runat="server" ClientIDMode="Static" Value="A" />
    <asp:HiddenField runat="server" ID="hdnTransactionID" />
    <asp:Button ID="btnPostRepCommand" runat="server" Style="display: none"></asp:Button>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField ID="hdnFromDate" runat="server" />
    <asp:HiddenField ID="hdnToDate" runat="server" />
    <asp:HiddenField ID="hdnHeaderName" runat="server" />
    <asp:HiddenField ID="hdnHeaderTransactionType" runat="server" />
    <asp:HiddenField ID="hdnHeaderDescription" runat="server" />
    <asp:HiddenField ID="hdnHeaderSplit" runat="server" />
    <asp:HiddenField ID="hdnHeaderAmount" runat="server" />
    <asp:HiddenField ID="hdnHeaderNarration" runat="server" />

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>
