﻿Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmMonthlySummary
    Inherits BACRMPage

    Dim strChartAcntIds As String
    Dim dblOpening As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            strChartAcntIds = GetQueryStringVal("ChartAcntId")
            'dblOpening = GetQueryStringVal( "Opening") 'Do not pass opening , it will be automatically taken from SP 

            If Not IsPostBack Then

                Dim mobjGeneralLedger As New GeneralLedger
                mobjGeneralLedger.DomainID = Session("DomainId")
                mobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())

                hdnAccountClass.Value = CCommon.ToLong(GetQueryStringVal("AccountClassID"))

                If GetQueryStringVal("From") <> "" Then
                    calFrom.SelectedDate = GetQueryStringVal("From")
                    calTo.SelectedDate = GetQueryStringVal("To")
                Else
                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        Try
                            calFrom.SelectedDate = PersistTable(calFrom.ID)
                            calTo.SelectedDate = PersistTable(calTo.ID)
                        Catch ex As Exception
                            'Do not throw error when date format which is stored in persist table and Current date formats are different
                            calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                        End Try
                    End If
                End If


                LoadMonthlySummary()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub LoadMonthlySummary()
        Dim dtSummary As DataTable
        Dim objCOA As New ChartOfAccounting
        Try
            With objCOA
                .DomainID = Session("DomainID")
                .FromDate = calFrom.SelectedDate
                .ToDate = CDate(calTo.SelectedDate & " 23:59:59")
                .ChartAcntIds = strChartAcntIds
                .AccountClass = CCommon.ToLong(hdnAccountClass.Value)
                dtSummary = .GetMonthlySummary()
            End With

            Dim Closing As Double
            Dim dblCutOffOpening As Double = 0
            If dtSummary.Rows.Count > 0 Then

                Dim dr As DataRow
                Dim dr1 As DataRow
                For i As Integer = 0 To dtSummary.Rows.Count - 1
                    dr = dtSummary.Rows(i)
                    If i >= 1 Then
                        dr1 = dtSummary.Rows(i - 1)
                    Else
                        dr1 = Nothing
                    End If

                    'Opening will come from SP itself
                    'If i = 0 Then 
                    'dr("Opening") = dblOpening

                    'Added by Manish Anjara : To show exact opening as per Stored Procudere, on page it was creating difference as this above 2 lines were commented.
                    If i = 0 Then
                        dblOpening = dr("Opening")
                    End If

                    If i <> 0 And dr("Opening") <> -0.0001 Then
                        dr("Opening") = Closing

                    ElseIf dr1 IsNot Nothing AndAlso CCommon.ToBool(dr("IsFinancialMonth")) = True Then
                        dr("Opening") = Closing - (dr1("Opening") + dr1("Debit") - dr1("Credit"))
                        dblCutOffOpening = (dr1("Opening") + dr1("Debit") - dr1("Credit"))

                    End If

                    Closing = dr("Opening") + dr("Debit") - dr("Credit")
                    dr("Closing") = Closing
                Next

                RepSummary.DataSource = dtSummary
                RepSummary.DataBind()

                'lblAccountCode.Text = GetQueryStringVal( "Code")
                lblAccountName.Text = HttpUtility.UrlDecode(GetQueryStringVal("Name"))
                lblOpening.Text = ReturnMoney(dblOpening)
                lblDebit.Text = ReturnMoney(dtSummary.Compute("sum(Debit)", ""))
                lblCredit.Text = ReturnMoney(dtSummary.Compute("sum(Credit)", "") + dblCutOffOpening)
                lblClosing.Text = ReturnMoney(dblOpening + CCommon.ToDouble(lblDebit.Text) - CCommon.ToDouble(lblCredit.Text))

            Else
                RepSummary.DataSource = Nothing
                RepSummary.DataBind()

                lblAccountName.Text = HttpUtility.UrlDecode(GetQueryStringVal("Name"))
                lblOpening.Text = ReturnMoney(dblOpening)
                lblClosing.Text = ReturnMoney(dblOpening + CCommon.ToDouble(lblDebit.Text) - CCommon.ToDouble(lblCredit.Text))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub RepTransactionReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles RepSummary.ItemCommand
        'Dim lblJournalId As Label
        'Dim lblCheckId As Label
        'Dim lblCashCreditCardId As Label
        'Dim lblTransactionId As Label
        'Dim lblOppId As Label
        'Dim lblOppBizDocsId As Label
        'Dim lblDepositId As Label
        'Dim lblCategoryHDRID As Label

        'Dim linJournalId As Integer
        'Dim lintCheckId As Integer
        'Dim lintCashCreditCardId As Integer
        'Dim lintOppId As Integer
        'Dim lintOppBizDocsId As Integer
        'Dim lintDepositId As Integer
        'Dim lintCategoryHDRID As Integer
        Try
            If e.CommandName = "Edit" Then
                Dim lngAccountTypeID As Long = CType(e.Item.FindControl("lblAccountTypeID"), Label).Text.Trim()
                Dim strMonthNumber As String = CType(e.Item.FindControl("lblMonthNumber"), Label).Text.Trim()
                Dim strYearNumber As String = CType(e.Item.FindControl("lblYearNumber"), Label).Text.Trim()

                Response.Redirect("../Accounting/frmGeneralLedger.aspx?Mode=1&AcntTypeID=" & lngAccountTypeID & "&Month=" & strMonthNumber & "&Year=" & strYearNumber & "&AccountID=" & strChartAcntIds & "&From=" & calFrom.SelectedDate & "&To=" & calTo.SelectedDate, False)
            End If

            '    lblJournalId = e.Item.FindControl("lblJournalId")
            '    lblCheckId = e.Item.FindControl("lblCheckId")
            '    lblCashCreditCardId = e.Item.FindControl("lblCashCreditCardId")
            '    lblTransactionId = e.Item.FindControl("lblTransactionId")
            '    lblOppId = e.Item.FindControl("lblOppId")
            '    lblOppBizDocsId = e.Item.FindControl("lblOppBizDocsId")
            '    lblDepositId = e.Item.FindControl("lblDepositId")
            '    lblCategoryHDRID = e.Item.FindControl("lblCategoryHDRID")

            '    linJournalId = IIf(lblJournalId.Text = "&nbsp;" OrElse lblJournalId.Text = "", 0, lblJournalId.Text)
            '    lintCheckId = IIf(lblCheckId.Text = "&nbsp;" OrElse lblCheckId.Text = "", 0, lblCheckId.Text)
            '    lintCashCreditCardId = IIf(lblCashCreditCardId.Text = "&nbsp;" OrElse lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)
            '    lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
            '    lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
            '    lintDepositId = IIf(lblDepositId.Text = "&nbsp;" OrElse lblDepositId.Text = "", 0, lblDepositId.Text)
            '    lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)
            '    If e.CommandName = "Edit" Then
            '        If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppId = 0 And lintOppBizDocsId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 Then
            '            Response.Redirect("../Accounting/frmJournalEntryList.aspx?frm=Transaction&JournalId=" & linJournalId & "&ChartAcntId=" & mintChartAcntId)
            '        ElseIf lintCheckId <> 0 Then
            '            Response.Redirect("../Accounting/frmChecks.aspx?frm=Transaction&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "&ChartAcntId=" & mintChartAcntId)
            '        ElseIf lintCashCreditCardId <> 0 Then
            '            Response.Redirect("../Accounting/frmCash.aspx?frm=Transaction&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "&ChartAcntId=" & mintChartAcntId)
            '            ''ElseIf lintOppId <> 0 And lintOppBizDocsId <> 0 Then
            '            ''    Dim lstr As String
            '            ''    lstr = "<script language=javascript>"
            '            ''    lstr += "window.open('../opportunity/frmBizInvoice.aspx?frm=Transaction&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "','','toolbar=no,titlebar=no,left=200, top=300,width=800,height=550,scrollbars=no,resizable=yes');"
            '            ''    lstr += "</script>"
            '            ''    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "BizInvoice", lstr)
            '        ElseIf lintDepositId <> 0 Then
            '            Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=Transaction&JournalId=" & linJournalId & "&DepositId=" & lintDepositId & "&ChartAcntId=" & mintChartAcntId)
            '        End If
            '    End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadMonthlySummary()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If GetQueryStringVal("frm") = "frmTrialBalance" Then
                Response.Redirect("../Accounting/frmTrialBalanceReport.aspx", False)
            ElseIf GetQueryStringVal("frm") = "frmProfitLoss" Then
                Response.Redirect("../Accounting/frmProfitLossReport.aspx", False)
            ElseIf GetQueryStringVal("frm") = "frmBalanceSheet" Then
                Response.Redirect("../Accounting/frmBalanceSheetReport.aspx", False)
            Else : Response.Redirect("../Accounting/frmChartofAccounts.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub RepTransactionReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepSummary.ItemDataBound
        Try

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class