﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Admin

Public Class frmFinancialReportWizard
    Inherits BACRMPage
    Dim lngFWizardID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(35, 107)
            lngFWizardID = CCommon.ToLong(GetQueryStringVal("FRID"))
            If Not IsPostBack Then

                objCommon.sb_FillComboFromDBwithSel(ddlFinancialView, 386, Session("DomainID"))
                hdnFRID.Value = lngFWizardID
                If lngFWizardID > 0 Then
                    LoadDetails()
                Else
                    pnlDetail.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindDimension()
        Try

            If ddlFinancialView.SelectedItem.Text.Contains("A/R") Then
                Dim dtData As DataTable
                objCommon.Mode = 3
                objCommon.DomainID = 1 ' hardcoded as we will have to setup Dropdown relationship for domain 1 and it will work for all other domain
                objCommon.PrimaryListItemID = ddlFinancialView.SelectedValue
                objCommon.SecondaryListID = 387
                dtData = objCommon.GetFieldRelationships.Tables(0)
                ddlDimension.DataTextField = "SecondaryListItem"
                ddlDimension.DataValueField = "numListItemID"
                ddlDimension.DataSource = dtData
                ddlDimension.DataBind()
                ddlDimension.Items.Insert(0, "--Select One--")
                ddlDimension.Items.FindByText("--Select One--").Value = "0"
            Else
                objCommon.sb_FillComboFromDBwithSel(ddlDimension, 387, Session("DomainID"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ddlFinancialView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFinancialView.SelectedIndexChanged
        Try
            BindDimension()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "RedirectClose", "window.opener.location.reload(true); window.close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If Save() Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "RedirectClose", "window.opener.location.reload(true); window.close();", True)
            Else
                litMessage.Text = "Can not save, Please try again later"
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function Save() As Boolean
        Try
            Dim objReport As New FinancialReport
            objReport.FRID = lngFWizardID
            objReport.FinViewID = ddlFinancialView.SelectedValue
            objReport.DimensionID = ddlDimension.SelectedValue
            objReport.ReportName = txtReportName.Text.Trim
            objReport.DateRange = ddlDateRange.SelectedValue
            objReport.DomainID = Session("DomainID")
            objReport.UserCntID = Session("UserContactID")

            objReport.ManageFinancialReport()
            lngFWizardID = objReport.FRID
            Return lngFWizardID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub LoadDetails()
        Try
            Dim objReport As New FinancialReport
            objReport.FRID = lngFWizardID
            objReport.DomainID = Session("DomainID")
            Dim dtTable As DataTable = objReport.GetFinancialReports()
            If dtTable.Rows.Count > 0 Then

                If Not ddlFinancialView.Items.FindByValue(dtTable.Rows(0).Item("numFinancialViewID")) Is Nothing Then
                    ddlFinancialView.Items.FindByValue(dtTable.Rows(0).Item("numFinancialViewID")).Selected = True
                End If
                BindDimension()
                If Not ddlDimension.Items.FindByValue(dtTable.Rows(0).Item("numFinancialDimensionID")) Is Nothing Then
                    ddlDimension.Items.FindByValue(dtTable.Rows(0).Item("numFinancialDimensionID")).Selected = True
                End If
                If Not ddlDateRange.Items.FindByValue(dtTable.Rows(0).Item("intDateRange")) Is Nothing Then
                    ddlDateRange.Items.FindByValue(dtTable.Rows(0).Item("intDateRange")).Selected = True
                End If
                txtReportName.Text = CCommon.ToString(dtTable.Rows(0).Item("vcReportName"))
                ddlDimension_SelectedIndexChanged()
                BindDetailGrid()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "RedirectClose", "window.opener.location.reload(true); window.close();", True)
    End Sub

    Private Sub ddlDimension_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDimension.SelectedIndexChanged
        Try
            ddlDimension_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub ddlDimension_SelectedIndexChanged()
        Try
            ddlValue1.ClearSelection()
            ddlValue1.Items.Clear()
            ddlValue2.ClearSelection()
            ddlValue2.Items.Clear()
            lblValue1.Text = ""
            lblValue2.Text = ""
            lblTextValue1.Text = ""
            dgFinReportDetail.Columns(1).Visible = True
            dgFinReportDetail.Columns(2).Visible = True
            Select Case True
                Case ddlDimension.SelectedItem.Text.ToLower = "by project" ' Project
                    BindProject(ddlValue1)
                    ShowFields(1)
                    lblValue1.Text = "Project"
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Project</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("class")
                    BindClass(ddlValue1)
                    ShowFields(1)
                    lblValue1.Text = "Class"
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Class</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("project type") 'project type

                    objCommon.sb_FillComboFromDBwithSel(ddlValue1, 380, Session("DomainID"))
                    ShowFields(1)
                    lblValue1.Text = "Project Type"
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Project Type</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("country")  'Country

                    objCommon.sb_FillComboFromDBwithSel(ddlValue1, 40, Session("DomainID"))
                    ShowFields(1)
                    lblValue1.Text = "Country"
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Country</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("employee")  'Employee

                    objCommon.sb_FillConEmpFromDBSel(ddlValue1, Session("DomainID"), 0, 0)
                    ShowFields(1)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Employee</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower = "by item"  'Item
                    radItem.Visible = True
                    ShowFields(4)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Item</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("item class")  'Item Class

                    objCommon.sb_FillComboFromDBwithSel(ddlValue1, 36, Session("DomainID"))
                    lblValue1.Text = "Item Classification"
                    ShowFields(1)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Item Classification</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("lot")  'Lot No
                    ShowFields(3)
                    lblTextValue1.Text = "Lot #"
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Lot #</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("month")  'Month
                    GetMyMonthList(ddlValue1, True)
                    lblValue1.Text = "Month"
                    ShowFields(1)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Month</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                    'Case 23414 'Organization custom field
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("quarter")  'Quarter
                    GetMyQuarterList(ddlValue1)
                    lblValue1.Text = "Quarter"
                    ShowFields(1)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Quarter</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("relationship")  'Relationship / Profile

                    objCommon.sb_FillComboFromDBwithSel(ddlValue1, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlValue2, 21, Session("DomainID"))
                    lblValue1.Text = "Relationship"
                    lblValue2.Text = "Profile"
                    ShowFields(2)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Relationship</font>"
                    dgFinReportDetail.Columns(2).HeaderText = "<font color=white>Profile</font>"
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("state")  'State

                    objCommon.sb_FillComboFromDBwithSel(ddlValue1, 40, Session("DomainID"))
                    ddlValue1.AutoPostBack = True
                    lblValue1.Text = "Country"
                    lblValue2.Text = "State"
                    ShowFields(2)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>State</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("team")  'Team
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = Session("DomainID")
                    objUserAccess.UserCntID = Session("UserContactID")
                    ddlValue1.DataSource = objUserAccess.GetTeamForUsers
                    ddlValue1.DataTextField = "vcData"
                    ddlValue1.DataValueField = "numlistitemid"
                    ddlValue1.DataBind()
                    lblValue1.Text = "Team"
                    ShowFields(1)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Team</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case ddlDimension.SelectedItem.Text.ToLower.Contains("territory")  'Territory
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = Session("DomainID")
                    objUserAccess.UserCntID = Session("UserContactID")
                    ddlValue1.DataSource = objUserAccess.GetTerritoryForUsers
                    ddlValue1.DataTextField = "vcTerName"
                    ddlValue1.DataValueField = "numTerritoryID"
                    ddlValue1.DataBind()
                    lblValue1.Text = "Territory"
                    ShowFields(1)
                    dgFinReportDetail.Columns(1).HeaderText = "<font color=white>Territory</font>"
                    dgFinReportDetail.Columns(2).Visible = False
                Case Else
                    Return
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub ShowFields(ByVal ControlNo As Int16)
        Try
            radItem.Visible = False
            If ControlNo = 1 Then
                radItem.Visible = False
                ddlValue1.Visible = True
                ddlValue2.Visible = False
                lblValue1.Visible = True
                lblValue2.Visible = False
                lblTextValue1.Visible = False
                txtValue1.Visible = False
            ElseIf ControlNo = 2 Then
                radItem.Visible = False
                ddlValue1.Visible = True
                ddlValue2.Visible = True
                lblValue1.Visible = True
                lblValue2.Visible = True
                lblTextValue1.Visible = False
                txtValue1.Visible = False
                radItem.Visible = False
            ElseIf ControlNo = 3 Then
                ddlValue1.Visible = False
                ddlValue2.Visible = False
                lblValue1.Visible = False
                lblValue2.Visible = False
                lblTextValue1.Visible = True
                txtValue1.Visible = True
            ElseIf ControlNo = 4 Then
                ddlValue1.Visible = False
                ddlValue2.Visible = False
                lblValue1.Visible = False
                lblValue2.Visible = False
                lblTextValue1.Visible = False
                txtValue1.Visible = False
                radItem.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim dtValue1 As DataTable
    Private Sub BindProject(ByRef ddlProject As DropDownList)
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            dtValue1 = objProject.GetOpenProject()

            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtValue1
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim dtValue2 As DataTable
    Private Sub BindClass(ByRef ddlClass As DropDownList)
        Try

            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.Mode = 1
            dtValue2 = objAdmin.GetClass()

            ddlClass.DataTextField = "ClassName"
            ddlClass.DataValueField = "numChildClassID"
            ddlClass.DataSource = dtValue2
            ddlClass.DataBind()
            ddlClass.Items.Insert(0, "--Select One--")
            ddlClass.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlValue1.SelectedIndexChanged
        Try
            If ddlDimension.SelectedItem.Text.ToLower.Contains("state") Then
                FillState(ddlValue2, ddlValue1.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objReport As New FinancialReport
            objReport.FRID = lngFWizardID
            If CCommon.ToLong(radItem.SelectedValue) > 0 Then
                objReport.Value1 = CCommon.ToLong(radItem.SelectedValue)
            Else
                objReport.Value1 = CCommon.ToLong(ddlValue1.SelectedValue)
            End If
            objReport.Value2 = CCommon.ToLong(ddlValue2.SelectedValue)
            objReport.ValueText = txtValue1.Text.Trim
            objReport.DomainID = Session("DomainID")
            If objReport.ManageFinancialReportDetail() Then
                litMessage.Text = "Value added sucessfully."
            Else
                litMessage.Text = "Problem in adding record, please try again later."
            End If
            BindDetailGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDetailGrid()
        Try
            Dim objReport As New FinancialReport
            objReport.DomainID = Session("DomainID")
            objReport.FRID = lngFWizardID
            dgFinReportDetail.DataSource = objReport.GetFinancialReportsDetail
            dgFinReportDetail.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgFinReportDetail_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFinReportDetail.DeleteCommand
        Try
            Dim objReport As New FinancialReport
            objReport.FRDetailID = e.Item.Cells(0).Text
            objReport.DomainID = Session("DomainID")
            objReport.DeleteFinancialReportDetail()
            objReport.FRDetailID = 0
            BindDetailGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgFinReportDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFinReportDetail.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                'If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                '    btnDelete.Visible = False
                '    lnkDelete.Visible = True
                '    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                'End If
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub GetMyMonthList(ByVal MyddlMonthList As DropDownList, ByVal SetCurruntMonth As Boolean)
        Try
            Dim month As DateTime = Convert.ToDateTime("1/1/2000")
            For i As Integer = 0 To 11

                Dim NextMont As DateTime = month.AddMonths(i)
                Dim list As New ListItem()
                list.Text = NextMont.ToString("MMMM")
                list.Value = NextMont.Month.ToString()
                MyddlMonthList.Items.Add(list)
            Next
            If SetCurruntMonth = True Then
                MyddlMonthList.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub GetMyQuarterList(ByVal MyddlMonthList As DropDownList)
        Try
            For i As Integer = 1 To 4
                Dim list As New ListItem()
                list.Text = "Q" + i + " " + Date.Now.Year.ToString
                list.Value = i
                MyddlMonthList.Items.Add(list)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class