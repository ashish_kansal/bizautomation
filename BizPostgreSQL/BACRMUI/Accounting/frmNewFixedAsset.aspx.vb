''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmNewFixedAsset
    Inherits BACRMPage
#Region "Variables"
    Dim lobjNewFixedAsset As New NewFixedAsset
    Dim lintAccountId As Integer
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lintAccountId = GetQueryStringVal( "AcntId")
            If Not IsPostBack Then
                
                FillParentCategoryDBSel(ddlParentCategory, 818)
                CalOriginalCost.SelectedDate = Now()
                ' chkActive.Checked = True 
                If lintAccountId <> 0 Then
                    ''  LoadSavedInformation()
                End If
            End If
            btnSave.Attributes.Add("onclick", "return Save()")
            txtDepreciationCost.Attributes.Add("onkeypress", "CheckNumber(1)")
            txtOriginalCost.Attributes.Add("onkeypress", "CheckNumber(1)")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Sub FillParentCategoryDBSel(ByRef objCombo As DropDownList, ByVal lngAcntTypeId As Long)
        Try
            lobjNewFixedAsset.AccountType = lngAcntTypeId
            lobjNewFixedAsset.AccountId = lintAccountId
            objCombo.DataSource = lobjNewFixedAsset.GetParentCategoryForSelType()
            objCombo.DataTextField = "vcCatgyName"
            objCombo.DataValueField = "numAccountId"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            With lobjNewFixedAsset
                If lintAccountId <> 0 Then
                    .AccountId = lintAccountId
                Else
                    .OpeningDate = CalOriginalCost.SelectedDate
                    .BitFixed = 0
                    If rdlFixedAsset.SelectedValue = 0 Then
                        .BitDepreciation = 1
                        .OpeningDate = CalOriginalCost.SelectedDate
                        .OriginalCostDate = CalOriginalCost.SelectedDate
                        .OriginalCost = txtOriginalCost.Text
                        .DepreciationCostDate = CalDepreciatonCost.SelectedDate
                        .DepreciationCost = txtDepreciationCost.Text
                    Else
                        .BitDepreciation = 0
                        .OpeningDate = CalOriginalCost.SelectedDate
                        .OriginalCostDate = CalOriginalCost.SelectedDate
                        .OriginalCost = txtOriginalCost.Text
                    End If
                End If
                .AccountType = 818
                .CategoryName = txtName.Text
                .Active = 1
                .CategoryDescription = txtCategoryDescription.Text
                .DomainId = Session("DomainId")
                .ParentAccountId = IIf(ddlParentCategory.SelectedItem.Value.ToString = "0", 1, ddlParentCategory.SelectedItem.Value)
                .InsertFixedAssetInChartAcnt()

                Response.Write("<script>opener.LoadFixedAssetDetails(); self.close();</script>")
            End With

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
 
End Class