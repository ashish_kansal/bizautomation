﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountsPayableDetails.aspx.vb"
    Inherits="frmAccountsPayableDetails" MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function OpenOpp(a, b, c, d) {
            if (b == -1 && a > 0) {
                var str;
                str = "../Accounting/frmNewJournalEntry.aspx?JournalId=" + a;
                //window.opener.reDirectPage(str);
                window.open(str, '_blank');
            }
            else if (c > 0) {
                if (a > 0) {
                    window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + a, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
                }
                else
                    window.open('../Accounting/frmAddBill.aspx?BillId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            }
            else if (b > 0 && a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                //window.opener.reDirectPage(str);
                window.opener.location.href = str;
                //opener.top.frames['mainframe'].location.href = str;
            }
            else if (d > 0) {
                var str;
                str = "../Accounting/frmWriteCheck.aspx?CheckHeaderID=" + d;
                //window.opener.reDirectPage(str);
                //opener.top.frames['mainframe'].location.href = str;
                window.opener.location.href = str;
            }

            //opener.top.frames['mainframe'].location.href = str;
        }
        function OpenBizInvoice(a, b, c, d, e) {
            if (d == -1) {
                if (parseInt(c) > 0) {
                    window.open('../Accounting/frmBillPayment.aspx?BizDocsPaymentDetId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
                    return false;
                }
            }
            else if (e > 0) {
                if (a > 0) {
                    window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + a, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
                }
                else
                    window.open('../Accounting/frmAddBill.aspx?BillId=' + e, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            }
            else {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                return false;
            }
        }
        //        function OpenAmtPaid(a, b, c, d, e) {
        //            if (d > 0 && e == 0) {
        //                var BalanceAmt = c;
        //                window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, '', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
        //                return false;
        //            }
        //            return false;
        //        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    A/P Detail
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvARDetails" runat="server" BorderWidth="0" CssClass="tbl" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <Columns>
            <asp:TemplateField HeaderText="Order ID" SortExpression="vcPOppName" HeaderStyle-ForeColor="">
                 <ItemStyle Width="300px" />
                <ItemTemplate>
                    <a href="javascript:void(0);" onclick="OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>','<%# Eval("numBillID") %>','<%# Eval("numCheckHeaderID") %>');">
                        <%# Eval("vcPOppName") %>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Authoritative BizDoc / Bill" SortExpression="vcbizdocid"
                HeaderStyle-ForeColor="">
                <ItemStyle Width="300px" />
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>','<%# Eval("numBizDocsPaymentDetId") %>','<%# Eval("tintOppType") %>','<%# Eval("numBillID") %>');">
                        <%#Eval("vcBizDocID")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Amount">
                  <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("TotalAmount"))%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Amount Paid">
                  <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")),Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")))%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Balance Due">
                  <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue")) + "</b></font>",Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue"))) %>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField HeaderText="Due Date" DataField="DueDate" ItemStyle-HorizontalAlign="Center">
                 <ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField HeaderText="Invoice #" DataField="vcRefOrderNo" ItemStyle-HorizontalAlign="Left">
                <ItemStyle Width="120px"></ItemStyle>
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            <center style="width: 600px">No A/P details found.</center>
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
