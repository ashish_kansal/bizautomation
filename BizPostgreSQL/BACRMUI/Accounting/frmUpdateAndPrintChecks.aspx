﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmUpdateAndPrintChecks.aspx.vb"
    Inherits=".frmUpdateAndPrintChecks" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Print Checks</title>
    <script type="text/javascript">
        function WindowClose() {
            window.close();
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Print Checks
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellpadding="2" cellspacing="2" border="0" width="750px">
        <tr>
            <td>To print properly, you must first set print options.Which options should I set?
                Then to print, either a) right-click the form below and choose Print, or b) click
                the Print icon if you see one below. 
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="iFramePrintChecks" id="iFramePrintChecks" width="750" height="600"
                    runat="server" ></iframe>
            </td>
        </tr>
        <tr>
            <td align="right">Did your checks look good ?
                <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="gbutton" OnClientClick="PrintAndRemove();" />
                <asp:Button ID="btnNo" runat="server" Text="No" OnClientClick="return WindowClose();"
                    CssClass="button" />
            </td>
        </tr>
    </table>
</asp:Content>
