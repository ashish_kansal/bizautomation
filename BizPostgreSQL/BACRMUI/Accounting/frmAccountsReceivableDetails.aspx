﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountsReceivableDetails.aspx.vb"
    Inherits="frmAccountsReceivableDetails" MasterPageFile="~/common/PopUp.Master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function OpenOpp(a, b,c,d,e) {
            var str;

            if (a > 0) {
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=1";
                window.open(str, '_blank');
            }
            else if (b > 0) {
                str = "../Accounting/frmNewJournalEntry.aspx?JournalId=" + b;
                window.open(str, '_blank');
            }
            else if (c > 0) {
                if (d == 3) {
                    str = "../opportunity/frmReturnDetail.aspx?ReturnID=" + e;
                    window.open(str, '_blank');
                }
                else {
                    str = "../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + c;
                    RPPopup = window.open(str, 'ReceivePayment', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
                    console.log(RPPopup);
                }
            }

            return;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            if (a > 0) {
                //RPPopup = window.open('../opportunity/frmAmtPaid.aspx?frm=AR&pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizId=' + a + '&OppId=' + b + '&DivId=' + c, 'ReceivePayment', 'toolbar=no,titlebar=no,top=50,width=1000,height=500,scrollbars=yes,resizable=no');
                //opener.document.RPPopup = RPPopup;
                //self.close();
                window.location = '../opportunity/frmAmtPaid.aspx?frm=AR&pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizId=' + a + '&OppId=' + b + '&DivId=' + c;
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    A/R Detail
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvARDetails" runat="server" AutoGenerateColumns="False" Style="margin-right: 0px"
        CssClass="dg" Width="100%" ShowFooter="true">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="hs" />
        <Columns>
            <asp:TemplateField HeaderText="Order ID" SortExpression="vcPOppName">
                <ItemStyle Width="300px" />
                <ItemTemplate>
                    <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("numJournal_Id")%>','<%# Eval("numDepositId")%>','<%# Eval("tintDepositePage")%>','<%# Eval("numReturnHeaderID")%>')">
                        <%# Eval("vcPOppName") %>
                    </a>
                </ItemTemplate>
                <FooterTemplate>
                   Total
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Authoritative BizDoc" SortExpression="vcbizdocid">
                <ItemStyle Width="300px" />
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>');">
                        <%#Eval("vcBizDocID")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Amount">
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("TotalAmount"))%>
                </ItemTemplate>
                 <FooterTemplate>
                    <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Amount Paid">
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="return OpenAmtPaid('<%# Eval("numoppbizdocsid") %>','<%# Eval("numOppId") %>','<%# Eval("numDivisionId") %>');">
                        <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")),Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")))%>
                    </a>
                </ItemTemplate>
                 <FooterTemplate>
                    <asp:Label ID="lblAmountPaid" runat="server"></asp:Label>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Balance Due">
                <ItemStyle Width="100px" />
                <ItemTemplate>
                    <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue")) + "</b></font>",Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue"))) %>
                </ItemTemplate>
                 <FooterTemplate>
                    <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Right" />
                <FooterStyle HorizontalAlign="Right" />
            </asp:TemplateField>
            <asp:BoundField HeaderText="Record Date" DataField="dtDate" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField HeaderText="Due Date" DataField="DueDate" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField HeaderText="Customer P.O. #" DataField="vcRefOrderNo" ItemStyle-HorizontalAlign="Left">
                <ItemStyle Width="120px"></ItemStyle>
            </asp:BoundField>
        </Columns>
    </asp:GridView>
</asp:Content>
