﻿'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Opportunities
'Imports BACRM.BusinessLogic.Common
'Partial Public Class frmMoneyTrail
'    Inherits BACRMPage

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            If Not IsPostBack Then
'                FillDetails(1)

'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'        'End Sub


'    Sub FillDetails(ByVal Mode As Int32)
'        Try
'            Dim objChecks As New Checks
'            objChecks.DomainID = Session("DomainId")
'            If Mode = 1 Then
'                objChecks.AccountType = 813 '' Bank Account Type
'                ddlDepositTo.DataSource = objChecks.GetCheckChartAcntDetails()
'                ddlDepositTo.DataTextField = "vcCategoryName"
'                ddlDepositTo.DataValueField = "numChartOfAcntID"
'                ddlDepositTo.DataBind()
'                ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
'            End If


'            If radCmbCompany.SelectedValue <> "" Then
'                objChecks.DivisionId = radCmbCompany.SelectedValue
'            End If
'            objChecks.AccountId = ddlDepositTo.SelectedValue

'            If calFrom.SelectedDate <> "" Then
'                objChecks.FromDate = calFrom.SelectedDate
'            End If

'            If calTo.SelectedDate <> "" Then
'                objChecks.ToDate = calTo.SelectedDate
'            End If

'            If radMoneyIn.Checked = True Then
'                objChecks.byteMode = 1
'            Else
'                objChecks.byteMode = 2
'            End If
'            objChecks.strSearch = txtSearch.Text

'            dgSearch.DataSource = objChecks.GetMoneyTrail
'            dgSearch.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    'Private Sub radCmbCompany_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbCompany.ItemsRequested
'    '    Try
'    '       If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then
'    '            Dim fillCombo As New COpportunities
'    '            With fillCombo
'    '                .DomainID = Session("DomainID")
'    '                .CompFilter = Trim(e.Text) & "%"
'    '                .UserCntID = Session("UserContactID")
'    '                radCmbCompany.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
'    '                radCmbCompany.DataTextField = "vcCompanyname"
'    '                radCmbCompany.DataValueField = "numDivisionID"
'    '                radCmbCompany.DataBind()
'    '            End With
'    '        End If

'    '    Catch ex As Exception
'    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'    '        Response.Write(ex)
'    '    End Try
'    'End Sub


'    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
'        Try
'            FillDetails(0)
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
'        Try
'            FillDetails(0)
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub ddlDepositTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepositTo.SelectedIndexChanged
'        Try
'            FillDetails(0)
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub


'    Private Sub dgSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearch.ItemCommand
'        Try

'            Dim objJournalEntry As New JournalEntry
'            objJournalEntry.JournalId = IIf(IsNumeric(e.Item.Cells(1).Text), e.Item.Cells(1).Text, 0)
'            objJournalEntry.CheckId = IIf(IsNumeric(e.Item.Cells(6).Text), e.Item.Cells(6).Text, 0)
'            objJournalEntry.CashCreditCardId = IIf(IsNumeric(e.Item.Cells(5).Text), e.Item.Cells(5).Text, 0)
'            objJournalEntry.DepositId = IIf(IsNumeric(e.Item.Cells(4).Text), e.Item.Cells(4).Text, 0)
'            objJournalEntry.DomainID = Session("DomainId")
'            objJournalEntry.DeleteJournalEntryDetails()

'            ' This is to reverse the transaction from account section
'            ' Created by Rajaram on 31/05/2008

'            Dim lobjReceivePayment As New ReceivePayment
'            If lobjReceivePayment Is Nothing Then lobjReceivePayment = New ReceivePayment

'            lobjReceivePayment.BizDocsPaymentDetId = IIf(IsNumeric(e.Item.Cells(3).Text), e.Item.Cells(3).Text, 0)
'            lobjReceivePayment.BizDocsId = IIf(IsNumeric(e.Item.Cells(8).Text), e.Item.Cells(8).Text, 0)
'            lobjReceivePayment.OppId = IIf(IsNumeric(e.Item.Cells(7).Text), e.Item.Cells(7).Text, 0)

'            lobjReceivePayment.UpdateOpportunityBizDocsIntegratedToAcnt()
'            lobjReceivePayment.DeletePaymentDetails()

'            FillDetails(0)
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSearch.ItemDataBound
'        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
'            Dim hplBizdcoc As HyperLink
'            hplBizdcoc = e.Item.FindControl("hplBizdcoc")
'            If IsNumeric(e.Item.Cells(8).Text) = True Then
'                hplBizdcoc.Attributes.Add("onclick", "return OpenBizInvoice('" & e.Item.Cells(7).Text & "','" & e.Item.Cells(8).Text & "')")
'            End If

'        End If
'    End Sub
'End Class