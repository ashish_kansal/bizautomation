''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports System.Configuration
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmChartofAccounts : Inherits BACRMPage

#Region "Variables"
    Dim mobjChartAccounting As ChartOfAccounting

    Dim intDomainID As Integer
    Dim treeNode As RadTreeNode
    Dim blnIsShowBalance As Boolean
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            btnChartAcntAddNew.Attributes.Add("onclick", "return OpenNewAccount()")
            intDomainID = CCommon.ToInteger(GetQueryStringVal("DomainID"))
            If intDomainID = 0 Then
                intDomainID = Session("DomainID")
            End If
            If Not IsPostBack Then
                LoadPersistTable()
                'To Set Permission
                GetUserRightsForPage(35, 73)

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnChartAcntAddNew.Visible = False
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnChartAcntEdit.Visible = False
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnChartAcntDelete.Visible = False


                Dim objUserAccess As New UserAccess
                Dim dtTable As New DataTable
                objUserAccess.DomainID = Session("DomainID")
                dtTable = objUserAccess.GetDomainDetails()
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    blnIsShowBalance = CCommon.ToBool(dtTable.Rows(0).Item("bitIsShowBalance"))
                End If

                LoadTreeView()
                LoadTreeViewCOA()
            End If
            btnChartAcntEdit.Attributes.Add("onclick", "return EditSelected()")
            'btnChartAcntDelete.Attributes.Add("onclick", "return DeleteSelected()")
            '' Code Commented by Ajit Singh on 03-Sep-2008
            'btnChartAcntReorganizeTree.Attributes.Add("onclick", "return UltraWebTree1_SortTree()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Function LoadTreeViewCOA()
        Dim lstrRootNode As String
        Try
            If mobjChartAccounting Is Nothing Then mobjChartAccounting = New ChartOfAccounting

            Dim sSorttype As String '= mobjChartAccounting.GetDomainWiseSort(intDomainID, "COA")
            If chkSortTreeView.Checked = True Then
                sSorttype = "DESCRIPTION"
            Else
                sSorttype = "ID"
            End If

            mobjChartAccounting.DomainID = intDomainID 'Session("DomainID")
            mobjChartAccounting.UserCntID = Session("UserContactId")
            If (sSorttype <> "") Then
                LoadTreeStructureRecursiveCOA(mobjChartAccounting.ChartofChildAccounts(sSorttype))
            Else : LoadTreeStructureRecursiveCOA(mobjChartAccounting.ChartofChildAccounts("ID"))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' Created on : 25-July-2008
    ''' Author : Ajit Singh
    ''' Description : This Private Module is use for fetching the Child Account records against Parent Account Id.

    Private Sub LoadTreeStructureRecursiveCOA(ByVal dsAccounts As DataSet)
        Dim lstrColor As String
        'Dim arrChildNodes As ArrayList = New ArrayList
        Try
            If dsAccounts.Tables.Count > 1 Then
                If dsAccounts.Tables(1).Rows.Count = 0 Then
                    Exit Sub
                End If
            End If

            Dim dtLevelData As DataTable
            Dim dvLevels As DataView
            Dim intMaxLevel As Integer
            intMaxLevel = CCommon.ToInteger(dsAccounts.Tables(1).Rows(0)("intMaxLevel"))

            For intCnt As Integer = 0 To intMaxLevel

                dvLevels = dsAccounts.Tables(1).DefaultView
                dvLevels.RowFilter = "intLevel = " & intCnt
                dtLevelData = dvLevels.ToTable()

                For Each row As DataRow In dtLevelData.Rows
                    CreateNodeCOA(row)
                Next
            Next

            'For Each row As DataRow In dsAccounts.Tables(1).Rows
            '    CreateNodeCOA(row)
            '    'If Not IsNothing(ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString)) Then
            '    '    ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString).Nodes.Add(Me.CreateNodeCOA(lstrColor, row))
            '    'End If

            'Next

            ''' These loop for set All Sorted Nodes in Treeview.
            'For Each row As DataRow In dsAccounts.Tables(0).Rows
            '    ultratreeChartAcnt.Nodes.Add(Me.CreateNodeCOA(lstrColor, row))
            'Next

            ''' These loop for set all Sorted Child Nodes in Treeview respect of ParentId.
            'For Each row As DataRow In dsAccounts.Tables(1).Rows
            '    If Not IsNothing(ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString)) Then
            '        ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString).Nodes.Add(Me.CreateNodeCOA(lstrColor, row))
            '    Else : arrChildNodes.Add(row)
            '    End If
            'Next
            ''' These loop for set all Sorted Child Nodes in Treeview respect of ParentId.
            'For Each row As DataRow In arrChildNodes
            '    If Not IsNothing(ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString)) Then
            '        ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString).Nodes.Add(Me.CreateNodeCOA(lstrColor, row))
            '        ultratreeChartAcnt.Find(row("numParntAcntTypeId").ToString).Nodes.Sort(False, False)
            '    End If
            'Next
            'arrChildNodes.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '' End of the Code.

    ''' Created on : 26-July-2008
    ''' Author : Ajit Singh
    ''' Description : This Private Function is use for create Child Nodes dynamically.
    'Private Function CreateNodeCOA(ByRef lstrColor As String, ByVal row As DataRow) As UltraWebNavigator.Node
    '    Try
    '        If Not IsDBNull(row("numOpeningBal")) Then
    '            If CInt(row("numOpeningBal")) < 0 Then
    '                lstrColor = "<font color=Red>[" & ReturnMoney(row("numOpeningBal")) & "]</font>"
    '            Else : lstrColor = "<font color=Green>[" & ReturnMoney(row("numOpeningBal")) & "]</font>"
    '            End If
    '        End If

    '        Dim childNode As UltraWebNavigator.Node = New UltraWebNavigator.Node
    '        Dim strGLPath As String
    '        strGLPath = "../Accounting/frmJournalEntry.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=COA&ChartAcntId=" & row("numAccountID").ToString

    '        childNode.Tag = row("numAccountID").ToString
    '        childNode.Text = "<a href=" & strGLPath & ">" & row("vcAccountName").ToString & "</a>" & " - " & "<font color=#546083>" & row("vcAccountType").ToString & "</font>" & "  " & IIf(IsDBNull(row("numOpeningBal")), "", lstrColor)
    '        childNode.TargetFrame = row("numAccountID").ToString & " " & row("numParntAcntTypeId").ToString & " 1"
    '        childNode.Expanded = True

    '        Return (childNode)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Sub LoadPersistTable()
        Try
            PersistTable.Load()
            If PersistTable.Count > 0 Then
                chkSortTreeView.Checked = CCommon.ToBool(PersistTable(chkSortTreeView.ID))
                chkShowActive.Checked = CCommon.ToBool(PersistTable(chkShowActive.ID))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SavePersistTable()
        Try
            PersistTable.Clear()
            PersistTable.Add(chkSortTreeView.ID, chkSortTreeView.Checked)
            PersistTable.Add(chkShowActive.ID, chkShowActive.Checked)
            PersistTable.Save()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim strGLPath As String
    Dim strMonthlySummaryLink As String
    Dim strChartOfAccLink As String
    Dim strOpeningBalance As String
    Dim strDeleteLink As String
    Dim strRefresh As String

    Private Sub CreateNodeCOA(ByVal row As DataRow)
        Try
            If chkShowActive.Checked = True And CCommon.ToBool(row("bitActive")) = False Then Exit Sub


            'strEditLink = "window.open('../Accounting/frmNewAccounts.aspx?Name=' + document.getElementById('hdnAccountId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=350,scrollbars=no,resizable=yes');"
            strChartOfAccLink = "<img alt=""Chart Of Account"" height=""16px"" width=""16px"" onclick=""return OpenCOAReport(" & CCommon.ToString(row("numAccountID")) & ")"" title=""Balance summary by Organization"" src=""../images/COA.png"" />"
            strGLPath = "<img alt=""GL Report"" height=""16px"" width=""16px""  onclick=""return OpenGLReport(" & CCommon.ToString(row("numAccountTypeID")) & "," & CCommon.ToString(row("numAccountID")) & ")"" title=""GL Summary Report"" src=""../images/GLReport.png"" />"
            strMonthlySummaryLink = "<img alt=""Monthly Summary Report"" height=""16px"" width=""16px""  onclick=""return OpenMSReport(" & CCommon.ToString(row("numAccountID")) & "," _
                                                                                                                                         & CCommon.ToDouble(row("numOpeningBal")) & ",'" _
                                                                                                                                         & CCommon.ToString(row("vcAccountCode")) & "','" _
                                                                                                                                         & "frmChartofAccounts" & "','" _
                                                                                                                                         & CCommon.ToString(row("vcAccountName1")) & "')"" title=""Monthly Summary Report"" src=""../images/MSReport.gif"" />"

            strRefresh = "<img alt=""Download latest transactions from bank"" height=""16px"" width=""16px"" onclick=""return RefreshBank(" & CCommon.ToString(row("numBankDetailID")) & ");"" title=""Download latest transactions from bank"" src=""../images/refresh.png"" />"
            strRefresh = If(CCommon.ToBool(row("IsConnected")) = True, strRefresh, "")

            strDeleteLink = "<img alt=""Delete"" height=""16px"" width=""16px""  onclick=""return SetAccountValue(" & CCommon.ToString(row("numAccountID")) & ")"" title=""Delete Selected Records"" src=""../images/DeleteGray.gif"" />"
            If Not IsDBNull(row("numOpeningBal")) Then
                If CCommon.ToDouble(row("numOpeningBal")) < 0 Then
                    strOpeningBalance = "<font color=Red>[" & ReturnMoney(row("numOpeningBal")) & "]</font>"
                Else : strOpeningBalance = "<font color=Green>[" & ReturnMoney(row("numOpeningBal")) & "]</font>"
                End If
            End If

            treeNode = New RadTreeNode
            If blnIsShowBalance = False AndAlso _
               (CCommon.ToString(row("vcAccountCode")).StartsWith("0103") = True OrElse _
                CCommon.ToString(row("vcAccountCode")).StartsWith("0104") = True OrElse _
                CCommon.ToString(row("vcAccountCode")).StartsWith("0106") = True) Then
                'treeNode.Text = "<a href=" & strGLPath & ">" & CCommon.ToString(row("vcAccountName1")) & "</a><font color=gray> ~ " & CCommon.ToString(row("vcAccountCode")) & "</font>" & "" & " - " & "<font color=#546083>" & CCommon.ToString(row("vcAccountType")) & "</font>"
                treeNode.Text = "<a onclick=""return EditSelected(" & CCommon.ToString(row("numAccountID")) & ")"">" & CCommon.ToString(row("vcAccountName1")) & "</a>" & "<font color=#FCFDFE>-</font>" & "<font color=#546083></font>" & "&nbsp;&nbsp;" & strChartOfAccLink & "&nbsp;&nbsp;" & strGLPath & "&nbsp;&nbsp;" & strMonthlySummaryLink & "&nbsp;&nbsp;" & strDeleteLink & "&nbsp;&nbsp;" & strRefresh
            Else
                'treeNode.Text = "<a href=" & strGLPath & ">" & CCommon.ToString(row("vcAccountName1")) & "</a><font color=gray> ~ " & CCommon.ToString(row("vcAccountCode")) & "</font>" & "" & " - " & "<font color=#546083>" & CCommon.ToString(row("vcAccountType")) & "</font>" & "  " & IIf(IsDBNull(row("numOpeningBal")), "", strOpeningBalance)
                treeNode.Text = "<a onclick=""return EditSelected(" & CCommon.ToString(row("numAccountID")) & ")"">" & CCommon.ToString(row("vcAccountName1")) & "</a>" & "<font color=#FCFDFE>-</font>" & "  " & IIf(IsDBNull(row("numOpeningBal")), "", strOpeningBalance) & "&nbsp;&nbsp;" & strChartOfAccLink & "&nbsp;&nbsp;" & strGLPath & "&nbsp;&nbsp;" & strMonthlySummaryLink & "&nbsp;&nbsp;" & strDeleteLink & "&nbsp;&nbsp;" & strRefresh
            End If

            treeNode.Value = CCommon.ToString(row("numAccountID")) & "~" & CCommon.ToString(row("numAccountTypeID"))
            treeNode.Expanded = True

            If Not RadTreeView1.FindNodeByValue(CCommon.ToString(row("numParntAcntTypeId"))) Is Nothing AndAlso CCommon.ToLong(row("numParentAccId")) = 0 Then
                RadTreeView1.FindNodeByValue(CCommon.ToString(row("numParntAcntTypeId"))).Nodes.Add(treeNode)
            ElseIf Not RadTreeView1.FindNodeByValue(CCommon.ToString(row("numParentAccId")) & "~" & CCommon.ToString(row("numAccountTypeID"))) Is Nothing Then
                RadTreeView1.FindNodeByValue(CCommon.ToString(row("numParentAccId")) & "~" & CCommon.ToString(row("numAccountTypeID"))).Nodes.Add(treeNode)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CreateNode(ByVal row As DataRow)
        Try
            treeNode = New RadTreeNode
            treeNode.Text = "<b>" & row("vcAccountType1").ToString & "</b>" & "<font color='#F3F4F5'> ~ " & row("vcAccountCode").ToString & "</font>"
            treeNode.Value = row("numAccountTypeID").ToString
            treeNode.Checkable = False
            'treeNode.Enabled = False
            treeNode.Expanded = True
            If Not RadTreeView1.FindNodeByValue(row("numParentID").ToString) Is Nothing Then
                RadTreeView1.FindNodeByValue(row("numParentID").ToString).Nodes.Add(treeNode)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnTreeView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTreeView.Click
        Try
            RadTreeView1.Nodes.Clear()
            LoadTreeView()
            LoadTreeViewCOA()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub btnChartAcntDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChartAcntDelete.Click
        Dim lintCount As Integer
        Dim lintCountJournalEntry As Integer
        Try
            If mobjChartAccounting Is Nothing Then mobjChartAccounting = New ChartOfAccounting
            mobjChartAccounting.ParentAccountId = hdnAccountId.Value
            mobjChartAccounting.DomainID = intDomainID 'Session("DomainID")
            mobjChartAccounting.GetCountOfParentCategory()
            lintCount = mobjChartAccounting.ChildCount
            lintCountJournalEntry = mobjChartAccounting.JournalEntryCount
            If lintCount = 0 Then
                If lintCountJournalEntry = 0 Then
                    mobjChartAccounting.AccountId = hdnAccountId.Value
                    Try
                        mobjChartAccounting.DeleteChartCategory()
                    Catch ex As Exception
                        If ex.Message = "LAST_PL_ACCOUNT" Then
                            litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Can not delete seleted Profit and Loss Account,Your option is to create new PL account and then try deleting given account again.</span>"
                            Exit Sub
                        ElseIf ex.Message = "TaxItems_Depend" Then
                            litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Dependant record exist, your option is to remove associated tax type from Administration->e-Commerce->Tax Details and try again</span>"
                            Exit Sub
                        ElseIf ex.Message = "CHILD_AVAILABLE" Then
                            litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Child accounts exists. Account cannot be deleted.</span>"
                            Exit Sub
                        ElseIf ex.Message = "DEFAULT_ACCOUNT" Then
                            litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Account is used as default account in Global Settings -> Accounting -> Default Accounts. Your option is to replace another account as default account.</span>"
                            Exit Sub
                        Else : litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Dependent Records Exists. Cannot Be deleted</span>"
                            Exit Sub

                        End If
                    End Try
                Else : litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;The item you selected has Journal Entries, so it cannot be deleted</span>"
                End If
            Else : litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;The item you selected has sub-accounts.</span>"
            End If
            RadTreeView1.Nodes.Clear()
            LoadTreeView()
            LoadTreeViewCOA()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub chkSortTreeView_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkSortTreeView.CheckedChanged
        Try
            BindDataToTreeView()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub BindDataToTreeView()
        Try
            RadTreeView1.Nodes.Clear()
            LoadTreeView()
            LoadTreeViewCOA()
            SavePersistTable()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadTreeView()
        Try
            If mobjChartAccounting Is Nothing Then mobjChartAccounting = New ChartOfAccounting
            Dim sSorttype As String '= mobjChartAccounting.GetDomainWiseSort(intDomainID, "COA")
            If chkSortTreeView.Checked = True Then
                sSorttype = "DESCRIPTION"
            Else
                sSorttype = "ID"
            End If

            'Dim sSorttype As String = mobjChartAccounting.GetDomainWiseSort(intDomainID, "COA")
            'If (sSorttype = String.Empty) Then sSorttype = "ID"
            RadTreeView1.Nodes.Clear()
            mobjChartAccounting.DomainID = intDomainID 'Session("DomainID")
            mobjChartAccounting.UserCntID = Session("UserContactId")
            mobjChartAccounting.GetAccountTypes()

            If (sSorttype <> "") Then
                AddAccountTypes(mobjChartAccounting.GetAccountTypes())
            Else : AddAccountTypes(mobjChartAccounting.GetAccountTypes())
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AddAccountTypes(ByVal dsAccounts As DataSet)
        Dim lstrColor As String
        Dim arrChildNodes As ArrayList = New ArrayList
        Try
            '' These loop for set All Sorted Nodes in Treeview.
            For Each row As DataRow In dsAccounts.Tables(0).Rows
                If CCommon.ToBool(row("bitActive")) Then
                    treeNode = New RadTreeNode
                    treeNode.Text = "<b>" & row("vcAccountType1").ToString & "</b>" & "<font color='#F3F4F5'> ~ " & row("vcAccountCode").ToString & "</font>"
                    treeNode.Value = row("numAccountTypeID").ToString
                    treeNode.Checkable = False
                    'treeNode.Enabled = False
                    treeNode.Expanded = True
                    RadTreeView1.Nodes.Add(treeNode)
                End If
            Next
            For Each row As DataRow In dsAccounts.Tables(1).Rows
                If CCommon.ToBool(row("bitActive")) Then
                    CreateNode(row)
                End If
            Next

            '' These loop for set all Sorted Child Nodes in Treeview respect of ParentId.
            'For Each row As DataRow In dsAccounts.Tables(1).Rows
            '    If Not IsNothing(ultratreeChartAcnt.Find(row("numParentID").ToString)) Then
            '        ultratreeChartAcnt.Find(row("numParentID").ToString).Nodes.Add(Me.CreateNode(lstrColor, row))
            '    Else : arrChildNodes.Add(row)
            '    End If
            'Next
            '' These loop for set all Sorted Child Nodes in Treeview respect of ParentId.
            'For Each row As DataRow In arrChildNodes
            '    If Not IsNothing(ultratreeChartAcnt.Find(row("numParentID").ToString)) Then
            '        ultratreeChartAcnt.Find(row("numParentID").ToString).Nodes.Add(Me.CreateNode(lstrColor, row))
            '        ultratreeChartAcnt.Find(row("numParentID").ToString).Nodes.Sort(False, False)
            '    End If
            'Next
            'arrChildNodes.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub RadTreeView1_NodeClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTreeNodeEventArgs) Handles RadTreeView1.NodeClick
    '    Try

    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnChartAcntAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChartAcntAddNew.Click
        Try
            'For Each itemNode As RadTreeNode In RadTreeView1.CheckedNodes
            '    If itemNode.Checked = True Then
            '        btnChartAcntAddNew.Attributes.Add("onclick", "return OpenNewAccount(" & itemNode.Value & ")")
            '    End If
            'Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try

    End Sub

    Private Sub btnRefreshBank_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefreshBank.Click
        Try
            Dim strMessage As String = ""
            Dim lngBankDetailID As Long = CCommon.ToLong(hdnBankDetailID.Value)
            If lngBankDetailID > 0 Then
                Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                objEBanking.TintMode = 0
                objEBanking.BankDetailID = lngBankDetailID
                Dim dt As DataTable = objEBanking.GetBankDetails()
                If dt.Rows.Count > 0 Then
                    If CCommon.ToString(dt.Rows(0)("vcAccountType")) = "4" Then
                        objEBanking.AsyncGetCreditCardStatement(dt.Rows(0))
                    Else
                        objEBanking.AsyncGetBankStatement(dt.Rows(0))

                    End If
                    strMessage = "We are connecting to your Account.. Please visit again in few Minutes to see your Current Balance."
                    ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)

                Else
                    litMessage.Visible = True
                    litMessage.Text = "Sorry!., The Account Doesnot seem having active Connection with Bank"
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub chkShowActive_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowActive.CheckedChanged
        Try
            BindDataToTreeView()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class