''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmTransactions
    Inherits BACRMPage

#Region "Variables"
    Dim mobjGeneralLedger As GeneralLedger
    Dim ldecTotalBalanceAmt As Decimal
    Dim dtChartAcntTransactionDetails As DataTable
    Dim dsTransaction As DataSet = New DataSet
    Dim dtTransaction As DataTable = dsTransaction.Tables.Add("Transaction")
    Dim mintChartAcntId As Integer
    Dim mintAcntTypeId As Integer
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            mintChartAcntId = CCommon.ToInteger(GetQueryStringVal("ChartAcntId"))
            mintAcntTypeId = CCommon.ToInteger(GetQueryStringVal("AcntTypeId"))
            If Not IsPostBack Then

                If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                mobjGeneralLedger.DomainID = Session("DomainId")
                mobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                ''calFrom.SelectedDate = DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())
                LoadGeneralDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadGeneralDetails()
        Dim dtChartAcntDetails As New DataTable
        Dim dtChartOfAccountDetails As DataTable
        Dim i As Integer
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            dtTransaction.Columns.Add("Date", Type.GetType("System.String"))
            dtTransaction.Columns.Add("Type", Type.GetType("System.String"))
            dtTransaction.Columns.Add("Name", Type.GetType("System.String"))
            dtTransaction.Columns.Add("Memo/Description", Type.GetType("System.String"))
            dtTransaction.Columns.Add("Split", Type.GetType("System.String"))
            dtTransaction.Columns.Add("Amount", Type.GetType("System.String"))
            dtTransaction.Columns.Add("Balance", Type.GetType("System.String"))
            dtTransaction.Columns.Add("JournalId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("CheckId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("CashCreditCardId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numChartAcntId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numTransactionId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numOppId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numOppBizDocsId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numDepositId", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numCategoryHDRID", Type.GetType("System.String"))
            dtTransaction.Columns.Add("tintTEType", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numCategory", Type.GetType("System.String"))
            dtTransaction.Columns.Add("numUserCntID", Type.GetType("System.String"))
            dtTransaction.Columns.Add("dtFromDate", Type.GetType("System.String"))

            mobjGeneralLedger.DomainID = Session("DomainID")
            mobjGeneralLedger.FromDate = calFrom.SelectedDate
            mobjGeneralLedger.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

            If mintAcntTypeId <> 0 Then
                mobjGeneralLedger.AccountTypeId = mintAcntTypeId
                dtChartOfAccountDetails = mobjGeneralLedger.GetChartOfAcntDetails()
                For i = 0 To dtChartOfAccountDetails.Rows.Count - 1
                    mobjGeneralLedger.ChartAcntId = dtChartOfAccountDetails.Rows(i)("numAccountId")
                    dtChartAcntDetails = mobjGeneralLedger.GetChartAcntDetailsForTransaction()
                    LoadTreeGrid(dtChartAcntDetails)
                Next
            ElseIf mintChartAcntId <> 0 Then
                mobjGeneralLedger.ChartAcntId = mintChartAcntId
                dtChartAcntDetails = mobjGeneralLedger.GetChartAcntDetailsForTransaction()
                LoadTreeGrid(dtChartAcntDetails)
            End If

            RepTransactionReport.DataSource = dsTransaction
            RepTransactionReport.DataMember = "Transaction"
            RepTransactionReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadTreeGrid(ByVal dtChartAcntDetails As DataTable)
        Dim i As Integer
        Dim j As Integer
        Dim ldecTotalAmt As Decimal
        Dim dtChartAcntBeginingBalance As DataTable
        Dim ldecOpeningBalance As Decimal
        Dim dtChildChartAcntDetails As DataTable
        Dim lobjJournalEntry As New JournalEntry
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            For i = 0 To dtChartAcntDetails.Rows.Count - 1
                ldecTotalAmt = 0
                Dim drChartAcntDetails As DataRow = dtTransaction.NewRow

                lobjJournalEntry.ChartAcntId = dtChartAcntDetails.Rows(i)("numAccountId")
                lobjJournalEntry.DomainID = Session("DomainId")

                lobjJournalEntry.FromDate = calFrom.SelectedDate
                lobjJournalEntry.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

                dtChartAcntTransactionDetails = lobjJournalEntry.GetGenernalLedgerList
                dtChartAcntBeginingBalance = lobjJournalEntry.GetBeginingBalanceGeneralLedgerList

                mobjGeneralLedger.ChartAcntId = dtChartAcntDetails.Rows(i)("numAccountId")
                mobjGeneralLedger.DomainID = Session("DomainId")
                mobjGeneralLedger.FromDate = calFrom.SelectedDate
                mobjGeneralLedger.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                ldecOpeningBalance = mobjGeneralLedger.GetCurrentOpeningBalanceForGeneralLedgerDetails

                If dtChartAcntTransactionDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then
                    drChartAcntDetails(0) = "<font color=Black><b>" & dtChartAcntDetails.Rows(i)("CategoryName") & "</b></font>"
                    dtTransaction.Rows.Add(drChartAcntDetails)
                End If

                If dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drTransactionDetails1 As DataRow = dtTransaction.NewRow
                    drTransactionDetails1(0) = "&nbsp;&nbsp;&nbsp;&nbsp;  Beginning Balance"
                    drTransactionDetails1(5) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    drTransactionDetails1(6) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    dtTransaction.Rows.Add(drTransactionDetails1)
                    ldecTotalAmt = ldecTotalAmt + dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance")
                End If

                For j = 0 To dtChartAcntTransactionDetails.Rows.Count - 1
                    Dim drTransactionDetails As DataRow = dtTransaction.NewRow
                    drTransactionDetails(0) = "&nbsp;&nbsp;&nbsp;&nbsp; " & ReturnDate(dtChartAcntTransactionDetails.Rows(j)("EntryDate"))
                    drTransactionDetails(1) = dtChartAcntTransactionDetails.Rows(j)("TransactionType")
                    drTransactionDetails(2) = dtChartAcntTransactionDetails.Rows(j)("CompanyName")
                    drTransactionDetails(3) = dtChartAcntTransactionDetails.Rows(j)("Memo")
                    drTransactionDetails(4) = dtChartAcntTransactionDetails.Rows(j)("AcntTypeDescription")
                    drTransactionDetails(5) = IIf(IsDBNull(dtChartAcntTransactionDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntTransactionDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntTransactionDetails.Rows(j)("Deposit")))
                    drTransactionDetails(6) = ReturnMoney(dtChartAcntTransactionDetails.Rows(j)("numBalance"))
                    drTransactionDetails(7) = dtChartAcntTransactionDetails.Rows(j)("JournalId")
                    drTransactionDetails(8) = IIf(IsDBNull(dtChartAcntTransactionDetails.Rows(j)("CheckId")), 0, dtChartAcntTransactionDetails.Rows(j)("CheckId"))
                    drTransactionDetails(9) = IIf(IsDBNull(dtChartAcntTransactionDetails.Rows(j)("CashCreditCardId")), 0, dtChartAcntTransactionDetails.Rows(j)("CashCreditCardId"))
                    drTransactionDetails(10) = dtChartAcntTransactionDetails.Rows(j)("numChartAcntId")
                    drTransactionDetails(11) = dtChartAcntTransactionDetails.Rows(j)("numTransactionId")
                    drTransactionDetails(12) = dtChartAcntTransactionDetails.Rows(j)("numOppId")
                    drTransactionDetails(13) = dtChartAcntTransactionDetails.Rows(j)("numOppBizDocsId")
                    drTransactionDetails(14) = dtChartAcntTransactionDetails.Rows(j)("numDepositId")
                    drTransactionDetails(15) = dtChartAcntTransactionDetails.Rows(j)("numCategoryHDRID")
                    drTransactionDetails(16) = dtChartAcntTransactionDetails.Rows(j)("tintTEType")
                    drTransactionDetails(17) = dtChartAcntTransactionDetails.Rows(j)("numCategory")
                    drTransactionDetails(18) = dtChartAcntTransactionDetails.Rows(j)("numUserCntID")
                    drTransactionDetails(19) = dtChartAcntTransactionDetails.Rows(j)("dtFromDate")

                    dtTransaction.Rows.Add(drTransactionDetails)

                    ldecTotalAmt = ldecTotalAmt + IIf(IsDBNull(dtChartAcntTransactionDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntTransactionDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntTransactionDetails.Rows(j)("Deposit")))
                Next

                If dtChartAcntTransactionDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drDummyRow As DataRow = dtTransaction.NewRow
                    drDummyRow(0) = "<B> Total for " & dtChartAcntDetails.Rows(i)("CategoryName") & "</b>"
                    drDummyRow(5) = "<B> " & ReturnMoney(ldecTotalAmt) & "</b>"
                    dtTransaction.Rows.Add(drDummyRow)
                    ldecTotalBalanceAmt = ldecTotalBalanceAmt + ldecTotalAmt
                End If

                dtChildChartAcntDetails = GetChildCategory(dtChartAcntDetails.Rows(i)("numAccountId"))
                LoadTreeGrid(dtChildChartAcntDetails)

                If dtChildChartAcntDetails.Rows.Count > 0 Then
                    If dtChartAcntTransactionDetails.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then

                        Dim drDummyRowForSubAcntTot As DataRow = dtTransaction.NewRow
                        drDummyRowForSubAcntTot(0) = "<B> Total for " & dtChartAcntDetails.Rows(i)("CategoryName") & "  with sub-accounts </b>"
                        drDummyRowForSubAcntTot(5) = "<B> " & ReturnMoney(ldecOpeningBalance) & "</b>"
                        dtTransaction.Rows.Add(drDummyRowForSubAcntTot)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetChildCategory(ByVal ParentID As String) As DataTable
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.ParentAccountId = ParentID
            mobjGeneralLedger.DomainID = Session("DomainId")
            GetChildCategory = mobjGeneralLedger.GetChildCategory()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub RepTransactionReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles RepTransactionReport.ItemCommand
        Dim lblJournalId As Label
        Dim lblCheckId As Label
        Dim lblCashCreditCardId As Label
        Dim lblTransactionId As Label
        Dim lblOppId As Label
        Dim lblOppBizDocsId As Label
        Dim lblDepositId As Label
        Dim lblCategoryHDRID As Label

        Dim linJournalId As Integer
        Dim lintCheckId As Integer
        Dim lintCashCreditCardId As Integer
        Dim lintOppId As Integer
        Dim lintOppBizDocsId As Integer
        Dim lintDepositId As Integer
        Dim lintCategoryHDRID As Integer
        Try
            lblJournalId = e.Item.FindControl("lblJournalId")
            lblCheckId = e.Item.FindControl("lblCheckId")
            lblCashCreditCardId = e.Item.FindControl("lblCashCreditCardId")
            lblTransactionId = e.Item.FindControl("lblTransactionId")
            lblOppId = e.Item.FindControl("lblOppId")
            lblOppBizDocsId = e.Item.FindControl("lblOppBizDocsId")
            lblDepositId = e.Item.FindControl("lblDepositId")
            lblCategoryHDRID = e.Item.FindControl("lblCategoryHDRID")

            linJournalId = IIf(lblJournalId.Text = "&nbsp;" OrElse lblJournalId.Text = "", 0, lblJournalId.Text)
            lintCheckId = IIf(lblCheckId.Text = "&nbsp;" OrElse lblCheckId.Text = "", 0, lblCheckId.Text)
            lintCashCreditCardId = IIf(lblCashCreditCardId.Text = "&nbsp;" OrElse lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)
            lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
            lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
            lintDepositId = IIf(lblDepositId.Text = "&nbsp;" OrElse lblDepositId.Text = "", 0, lblDepositId.Text)
            lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)
            If e.CommandName = "Edit" Then
                If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppId = 0 And lintOppBizDocsId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 Then
                    Response.Redirect("../Accounting/frmNewJournalEntry.aspx?frm=Transaction&JournalId=" & linJournalId & "&ChartAcntId=" & mintChartAcntId)
                ElseIf lintCheckId <> 0 Then
                    Response.Redirect("../Accounting/frmChecks.aspx?frm=Transaction&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "&ChartAcntId=" & mintChartAcntId)
                ElseIf lintCashCreditCardId <> 0 Then
                    Response.Redirect("../Accounting/frmCash.aspx?frm=Transaction&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "&ChartAcntId=" & mintChartAcntId)
                    ''ElseIf lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                    ''    Dim lstr As String
                    ''    lstr = "<script language=javascript>"
                    ''    lstr += "window.open('../opportunity/frmBizInvoice.aspx?frm=Transaction&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "','','toolbar=no,titlebar=no,left=200, top=300,width=800,height=550,scrollbars=no,resizable=yes');"
                    ''    lstr += "</script>"
                    ''    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "BizInvoice", lstr)
                ElseIf lintDepositId <> 0 Then
                    Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=Transaction&JournalId=" & linJournalId & "&DepositId=" & lintDepositId & "&ChartAcntId=" & mintChartAcntId)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If GetQueryStringVal("frm") = "frmTrialBalance" Then
                Response.Redirect("../Accounting/frmTrailBalance.aspx")
            ElseIf GetQueryStringVal("frm") = "frmProfitLoss" Then
                Response.Redirect("../Accounting/frmProfitLoss.aspx")
            ElseIf GetQueryStringVal("frm") = "frmBalanceSheet" Then
                Response.Redirect("../Accounting/frmBalanceSheet.aspx")
            Else : Response.Redirect("../Accounting/frmChartofAccounts.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub RepTransactionReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepTransactionReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblOppId As Label
                Dim lblOppBizDocsId As Label
                Dim lintOppId As Integer
                Dim lintOppBizDocsId As Integer
                Dim lintCategoryHDRID As Integer
                Dim lnkType As LinkButton
                Dim lblCategoryHDRID As Label
                Dim lblTEType As Label
                Dim lblCategory As Label
                Dim lblUserCntID As Label
                Dim lblFromDate As Label
                lnkType = e.Item.FindControl("lnkType")
                lblCategoryHDRID = e.Item.FindControl("lblCategoryHDRID")
                lblOppId = e.Item.FindControl("lblOppId")
                lblOppBizDocsId = e.Item.FindControl("lblOppBizDocsId")
                lblCategory = e.Item.FindControl("lblCategory")
                lblTEType = e.Item.FindControl("lblTEType")
                lblUserCntID = e.Item.FindControl("lblUserCntID")
                lblFromDate = e.Item.FindControl("lblFromDate")
                lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
                lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
                lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)

                If lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                    lnkType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                ElseIf lintCategoryHDRID <> 0 Then
                    Select Case lblTEType.Text
                        Case 0
                            lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "&Date=" & CDate(lblFromDate.Text) & "')")
                        Case 1
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else : lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 2
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else : lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 3
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else : lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=TransactionDet&ChartAcntId=" & GetQueryStringVal("ChartAcntId") & "&AcntTypeId=" & GetQueryStringVal("AcntTypeId") & "&CatHDRID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                    End Select
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class