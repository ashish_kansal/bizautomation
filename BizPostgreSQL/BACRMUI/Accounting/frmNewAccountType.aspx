﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewAccountType.aspx.vb"
    Inherits=".frmNewAccountType" MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>New Account Type</title>
    <script type="text/javascript">
        function Save() {
            if (document.getElementById("ddlParentType").value == 0) {
                alert("Please Select Type");
                document.getElementById("ddlParentType").focus();
                return false;
            }
            if (document.getElementById("txtAccountType").value == "") {
                alert("Enter Account Type");
                document.getElementById("txtAccountType").focus();
                return false;
            }
        }
        function close1() {
            window.opener.location.href = window.opener.location.href;
            self.close();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close"
                OnClientClick="return Save();"></asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50"
                OnClientClick="return close1();"></asp:Button>
        </div>
    </div>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr align="center">
            <td class="normal4" valign="middle">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Account Type
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="2" cellpadding="0" width="650" border="0">
        <tr>
            <td class="normal1" align="right">
                Name<font color="red">*</font>&nbsp;
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtAccountType" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Parent Type&nbsp;
            </td>
            <td colspan="3">
                <asp:DropDownList CssClass="signup" ID="ddlParentType" runat="server" Width="200">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Is Active
            </td>
            <td colspan="3">
                <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
            </td>
        </tr>
    </table>
</asp:Content>
