﻿Public Partial Class frmSortAccounts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim strSortType As String = "ID"
            If ((rdSortType.SelectedValue <> "") Or (rdSortType.SelectedValue <> String.Empty)) Then
                strSortType = rdSortType.SelectedValue
            End If
            Response.Write("<script>opener.SortTreeViewDetails('" & strSortType & "'); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class