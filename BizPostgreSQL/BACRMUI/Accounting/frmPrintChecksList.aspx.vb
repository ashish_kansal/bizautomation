﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Opportunities
Imports System.Collections.Generic
Imports System.Linq

Public Class frmPrintChecksList
    Inherits BACRMPage

    Dim lngCheckHeaderID As Long
    Dim objChecks As Checks

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""
            'lngCheckHeaderID = CCommon.ToLong(GetQueryStringVal("CheckHeaderID"))
            GetUserRightsForPage(35, 106)
            DomainID = Session("DomainID")
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                FillDepositTocombo()
                LoadChecksList()

                txtCheckNo.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                btnPrint.Attributes.Add("onclick", "return Save()")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub FillDepositTocombo()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "01010101" 'bank accounts
            dtChartAcntDetails = objCOA.GetParentCategory()
            ddlDepositTo.Items.Clear()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("vcStartingCheckNumber")
                'item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlDepositTo.Items.Add(item)
            Next
            'ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))

            PersistTable.Load()
            If PersistTable.Count > 0 Then
                For Each item In ddlDepositTo.Items
                    If item.Value.StartsWith(CCommon.ToString(PersistTable(ddlDepositTo.ID))) Then
                        ddlDepositTo.ClearSelection()
                        item.Selected = True
                        ChangeDepositTo()
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadChecksList()
        Try
            If objChecks Is Nothing Then objChecks = New Checks

            objChecks.DomainID = Session("DomainId")
            objChecks.Mode = 3
            'objChecks.CheckHeaderID = lngCheckHeaderID
            objChecks.AccountId = CCommon.ToInteger(ddlDepositTo.SelectedValue.Split("~")(0))
            objChecks.Type = CCommon.ToShort(ddlChecksType.SelectedValue)

            Dim ds As DataSet = objChecks.ManageCheckHeader()

            gvChecksList.DataSource = ds.Tables(0)
            gvChecksList.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlDepositTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepositTo.SelectedIndexChanged
        Try
            ChangeDepositTo()
            LoadChecksList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub ChangeDepositTo()
        Try
            Dim lobjCashCreditCard As New CashCreditCard
            Dim ldecOpeningBalance As Decimal = 0
            If ddlDepositTo.SelectedItem.Value <> "0" Then
                lobjCashCreditCard.AccountId = ddlDepositTo.SelectedValue.Split("~")(0)
                lobjCashCreditCard.DomainID = Session("DomainId")
                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
            End If

            lblBalanceAmount.Text = String.Format("<font color={0}>{1} {2}</font>",
                                                    IIf(ldecOpeningBalance < 0, Color.Red, Color.Green),
                                                    Session("Currency").ToString.Trim,
                                                    ReturnMoney(ldecOpeningBalance))

            If ddlDepositTo.SelectedValue.Contains("~") Then
                txtCheckNo.Text = CCommon.ToLong(ddlDepositTo.SelectedValue.Split("~")(1))
            End If


            PersistTable.Clear()
            PersistTable.Add(ddlDepositTo.ID, ddlDepositTo.SelectedValue.Split("~")(0))
            PersistTable.Save()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlChecksType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlChecksType.SelectedIndexChanged
        Try
            LoadChecksList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            If Not IsDBNull(CloseDate) Then Return (FormattedDateFromDate(CloseDate, Session("DateFormat")))

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
        Try
            Dim lngCheckNo As Long = CCommon.ToLong(txtCheckNo.Text)
            Dim strchkNoList As New ArrayList

            For Each gvr As GridViewRow In gvChecksList.Rows
                'If selected for printing then get the check information
                Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                If chk.Checked Then
                    strchkNoList.Add(lngCheckNo)
                    lngCheckNo = lngCheckNo + 1
                End If
            Next

            Dim objCOA As New ChartOfAccounting
            objCOA.AccountId = ddlDepositTo.SelectedValue.Split("~")(0)
            objCOA.CheckHeaderID = 0
            objCOA.DomainID = Session("DomainID")

            Dim dtCheckNo As DataTable = objCOA.ValidateCheckNo(String.Join(",", strchkNoList.ToArray()))

            If dtCheckNo.Rows.Count > 0 Then
                strchkNoList = New ArrayList

                For Each dr As DataRow In dtCheckNo.Rows
                    strchkNoList.Add(dr("numCheckNo"))
                Next
                ShowMessage("You must specify a different Check #. Check # " & String.Join(", ", strchkNoList.ToArray()) & " has already been used. Save & Print it anyway?")
                btnAllowCheckNO.Visible = True
                Return
            Else
                PrintChecksList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnAllowCheckNO_Click(sender As Object, e As System.EventArgs) Handles btnAllowCheckNO.Click
        Try
            PrintChecksList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub PrintChecksList()
        Try
            'Array to store all the vendors
            Dim alVendors As New ArrayList

            'Convert NumericValues To Word
            Dim lNumericToWord As New NumericToWord
            Dim lngDivisionID As Long
            Dim lngCheckNo As Long = CCommon.ToLong(txtCheckNo.Text)

            Dim dt As New DataTable
            Dim dtrow As DataRow

            CCommon.AddColumnsToDataTable(dt, "numCheckHeaderID,numCheckNo,CompanyName,Amount,Date,AmountInWords,EmployerName,BillToAddress,Employer,EmployerAddress")

            For Each gvr As GridViewRow In gvChecksList.Rows
                'If selected for printing then get the check information
                Dim chk As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                If chk.Checked Then
                    lngDivisionID = CCommon.ToString(CType(gvr.FindControl("hdnDivisionID"), HiddenField).Value)
                    Dim isVendorExists As Boolean = False

                    For Each item As ListDictionary In alVendors
                        If item("DivisionID") = lngDivisionID Then
                            item("Amount") = CCommon.ToDouble(item("Amount")) + CCommon.ToDouble(IIf(CType(gvr.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(gvr.FindControl("lblAmount"), Label).Text, ",", "")))
                            item("AmountInWords") = lNumericToWord.SpellNumber(item("Amount"))
                            item("totalAmount") = ReturnMoney(item("Amount"))
                            isVendorExists = True
                        End If
                    Next

                    If Not isVendorExists Then
                        Dim ldVendorItem As New ListDictionary

                        ldVendorItem("CompanyID") = lngDivisionID
                        ldVendorItem("Date") = FormattedDateFromDate(Today, Session("DateFormat"))
                        ldVendorItem("CompanyName") = CCommon.ToString(CType(gvr.FindControl("lblCompanyName"), Label).Text) 'dtgriditem.Cells(10).Text
                        ldVendorItem("Amount") = IIf(CType(gvr.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(gvr.FindControl("lblAmount"), Label).Text, ",", ""))
                        ldVendorItem("AmountInWords") = lNumericToWord.SpellNumber(IIf(CType(gvr.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(gvr.FindControl("lblAmount"), Label).Text, ",", "")))
                        ldVendorItem("PurchaseOrderNo") = String.Join("<br>", CType(gvr.FindControl("vcRefOrderNo"), Label).Text.Split(","))
                        ldVendorItem("EmployerName") = CCommon.ToString(CType(gvr.FindControl("EmployerName"), Label).Text)
                        ldVendorItem("BillToAddress") = GetBillToAddress(lngDivisionID)
                        ldVendorItem("ReferenceType") = CCommon.ToShort(CType(gvr.FindControl("hdnReferenceType"), HiddenField).Value)
                        ldVendorItem("AccountID") = CCommon.ToLong(CType(gvr.FindControl("hdnAccountID"), HiddenField).Value)
                        ldVendorItem("DivisionID") = CCommon.ToLong(CType(gvr.FindControl("hdnDivisionID"), HiddenField).Value)

                        ldVendorItem("Employer") = CCommon.ToString(System.Web.HttpContext.Current.Session("CompanyName"))
                        ldVendorItem("EmployerAddress") = GetBillToAddress(CCommon.ToLong(System.Web.HttpContext.Current.Session("UserDivisionID")), True)

                        ldVendorItem("totalAmount") = ReturnMoney(IIf(CType(gvr.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(gvr.FindControl("lblAmount"), Label).Text, ",", ""))) '"$TotalAmount$"
                        alVendors.Add(ldVendorItem)
                    End If

                    dtrow = dt.NewRow

                    dtrow.Item("numCheckHeaderID") = gvChecksList.DataKeys(gvr.DataItemIndex).Value
                    dtrow.Item("numCheckNo") = lngCheckNo
                    dtrow.Item("Date") = FormattedDateFromDate(Today, Session("DateFormat"))
                    dtrow.Item("CompanyName") = CCommon.ToString(CType(gvr.FindControl("lblCompanyName"), Label).Text) 'dtgriditem.Cells(10).Text
                    dtrow.Item("Amount") = IIf(CType(gvr.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(gvr.FindControl("lblAmount"), Label).Text, ",", ""))
                    dtrow.Item("AmountInWords") = lNumericToWord.SpellNumber(IIf(CType(gvr.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(gvr.FindControl("lblAmount"), Label).Text, ",", "")))
                    dtrow.Item("EmployerName") = CCommon.ToString(CType(gvr.FindControl("EmployerName"), Label).Text)
                    dtrow.Item("BillToAddress") = GetBillToAddress(lngDivisionID)
                    dtrow.Item("Employer") = CCommon.ToString(System.Web.HttpContext.Current.Session("CompanyName"))
                    dtrow.Item("EmployerAddress") = GetBillToAddress(CCommon.ToLong(System.Web.HttpContext.Current.Session("UserDivisionID")), True)

                    dt.Rows.Add(dtrow)

                    lngCheckNo = lngCheckNo + 1
                End If
            Next

            'Update Check Header with bitIsPrint=1 and CheckNo
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            'Modified By Manish Anjara : 29th Oct,2012
            Session("PrintCheckDS") = ds

            'Print Check No
            Dim objHTMLToPdf As New HTMLToPDF
            'Dim strFile As String = objHTMLToPdf.ExportChecksToPdf(alVendors)
            Dim strFile As String = objHTMLToPdf.ExportChecksToPdfUsingITextSharp(alVendors, Session("DomainID"), dt)

            If strFile.Length > 0 Then
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "download", "DownloadFile('" & strFile & "' );", True)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "PrintPopup", "OpenPrintCheck('" & strFile & "'," & ddlDepositTo.SelectedValue.Split("~")(0) & "," & lngCheckNo & ");", True)
            End If

            'If objChecks Is Nothing Then objChecks = New Checks()

            'objChecks.DomainID = Session("DomainID")
            'objChecks.JournalDetails = ds.GetXml()
            'objChecks.UpdateCheckHeadePrintCheck()

            ''Update COA Starting Check Number
            'Dim objCOA As New ChartOfAccounting

            'objCOA.DomainID = Session("DomainId")
            'objCOA.AccountId = ddlDepositTo.SelectedValue.Split("~")(0)
            'objCOA.StartCheckNumber = lngCheckNo
            'objCOA.UpdateCOACheckNumber()

            'FillDepositTocombo()
            'LoadChecksList()
            'litMessage.Text = "Check printed successfully."
            btnAllowCheckNO.Visible = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetBillToAddress(ByVal lngDivisionID As Long, Optional ByVal isExludeCompnayName As Boolean = False) As String
        Try
            Dim objOpp As New COpportunities
            Dim dtAddress As DataTable
            objOpp.DomainID = Session("DomainID")
            dtAddress = objOpp.GetBizDocExistingAddress(0, lngDivisionID, "Bill", "No")
            Dim strAddress As String = ""
            If dtAddress.Rows.Count > 0 Then
                If Not isExludeCompnayName Then
                    strAddress += CCommon.ToString(dtAddress.Rows(0).Item("vcCompanyName")) + " ,<br>"
                End If
                strAddress += CCommon.ToString(dtAddress.Rows(0).Item("Street")) + " <br>"
                strAddress += CCommon.ToString(dtAddress.Rows(0).Item("City")) + " ,"
                strAddress += CCommon.ToString(dtAddress.Rows(0).Item("vcState")) + " "
                strAddress += CCommon.ToString(dtAddress.Rows(0).Item("PostCode")) + " <br>"
                strAddress += CCommon.ToString(dtAddress.Rows(0).Item("vcCountry"))
            End If
            Return strAddress
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvChecksList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvChecksList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lnkType As LinkButton = e.Row.FindControl("lnkType")

                If DataBinder.Eval(e.Row.DataItem, "tintreferenceType") = 1 Then 'Write Check
                    lnkType.Attributes.Add("onclick", "return OpenWriteCheck('" & DataBinder.Eval(e.Row.DataItem, "numCheckHeaderID") & "')")

                ElseIf DataBinder.Eval(e.Row.DataItem, "tintreferenceType") = 8 Then 'Bill Payment
                    lnkType.Attributes.Add("onclick", "return OpenBillPayment('" & DataBinder.Eval(e.Row.DataItem, "numReferenceID") & "')")

                ElseIf DataBinder.Eval(e.Row.DataItem, "tintreferenceType") = 10 Then 'RMA
                    lnkType.Attributes.Add("onclick", "return OpenOpp('" & DataBinder.Eval(e.Row.DataItem, "numReferenceID") & "','1')")

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub


    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try
            FillDepositTocombo()
            LoadChecksList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub

    Private Sub gvChecksList_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvChecksList.RowEditing
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class