﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmSelectMultipleAccounts
    Inherits BACRMPage
    Dim strAccountTypeCode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strAccountTypeCode = GetQueryStringVal("AcntCode").Replace("@", "")
            If Not IsPostBack Then
                BindAccounts()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindAccounts()
        Try
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = strAccountTypeCode
            cblAccounts.DataSource = objCOA.GetParentCategory()
            cblAccounts.DataTextField = "vcAccountName1"
            cblAccounts.DataValueField = "numAccountId"
            cblAccounts.DataBind()
            'cblAccounts.Items.Insert(0, "--Select Multiple--")
            'cblAccounts.Items.FindByText("--Select Multiple--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strAccounts As String = String.Empty
            For i As Integer = 0 To cblAccounts.Items.Count - 1
                If cblAccounts.Items(i).Selected Then
                    strAccounts = strAccounts & cblAccounts.Items(i).Value & ","
                End If
            Next
            If Len(strAccounts) > 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Acnt", "window.opener.document.form1.hdnAccounts.value='" & strAccounts & "';window.opener.SearchRecords(true); Close();", True)
            Else
                litMessage.Text = "Please select atleast one account"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class