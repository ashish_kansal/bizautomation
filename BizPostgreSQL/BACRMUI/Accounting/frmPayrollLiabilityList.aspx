<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPayrollLiabilityList.aspx.vb"
    Inherits=".frmPayrollLiabilityList" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" ValidateRequest="false" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Payroll Expense</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style type="text/css">
        .GraySmallText {
            font-size: small;
            font-weight: bold;
            color: gray;
        }

        table.dg .hs td, table.dg th {
            /*padding: 0px !important;*/
            vertical-align: middle;
        }

        table.dg > tbody > tr > td {
            padding-left: 2px;
            padding-right: 2px;
        }

        input[type='checkbox'] {
            vertical-align: middle;
        }

        span > label {
            vertical-align: middle;
        }

        #ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate, #ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate {
            width: 95px;
        }

        #tblPayroll ul {
            margin-bottom: 0px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $("[id$=txtPayrollAmt]").keydown(function (e) {
                if (e.keyCode == 13 || e.keyCode == 9) {
                    e.preventDefault();
                    var tr = $(this).closest("tr");
                    $(tr).find("[id$=txtPayrollAdditionalAmount]").focus();
                }
            });

            $("[id$=txtPayrollAdditionalAmount]").keydown(function (e) {
                if (e.keyCode == 13 || e.keyCode == 9) {
                    if ($(this).closest("tr").next('tr').length > 0) {
                        e.preventDefault();
                        $(this).closest("tr").next('tr').find("[id$=txtPayrollAmt]").focus();
                    }
                    else {
                        if ($("#tblPayroll").find(' tbody tr:first').length > 0)
                        {
                            e.preventDefault();
                            $("#tblPayroll").find('tbody tr:first').find("[id$=txtReimburseExpense]").focus();
                        }
                    }
                }
            });

            $("[id$=txtReimburseExpense]").keydown(function (e) {
                if (e.keyCode == 13 || e.keyCode == 9) {
                    if ($(this).closest("tr").next('tr').length > 0) {
                        e.preventDefault();
                        $(this).closest("tr").next('tr').find("[id$=txtReimburseExpense]").focus();
                    }
                }
            });

            $("#tblPayroll #chkSelectAll").change(function () {
                $("#tblPayroll #chkSelect").prop("checked", $(this).prop("checked"));
            });
        }

        function OpenUserDetails(a) {
            window.open("../Accounting/frmEmpPayrollExpenses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1000,height=643,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTimeAndExpenses(a, b) {
            var h = screen.height;
            var w = screen.width;

            window.open("../Accounting/frmTimeAndExpensesDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a + "&ComPayPeriodID=" + b, '', 'toolbar=no,titlebar=no,left=50, top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes, target=_blank')
            return false;
        }
        function OpenCommissionDetails(a, b, c, d, e) {
            window.open("../Accounting/frmCommissionforPayrollExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a + "&PersonalId=" + b + "&Mode=" + c + "&ComPayPeriodID=" + d + "&DivisionID=" + e, '', 'toolbar=no,titlebar=no,left=100, top=200,width=1300,height=425,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenCommissionEarnedDetails(a, b, c) {
            var h = screen.height;
            var w = screen.width;

            window.open('../Accounting/frmCommissionEarnedDetails.aspx?UserCntID=' + a + '&ComPayPeriodID=' + b + '&DivisionID=' + c, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function LoadPayrollDetails() {
            document.form1.btnLoad.click()
            return false;
        }
        function OpenOrganization(divisionID, tintCRMType) {
            if (tintCRMType = 2) {
                var win = window.open('../Account/frmAccounts.aspx?DivID=' + divisionID, '_blank');
                win.focus();
            } else if (tintCRMType = 1) {
                var win = window.open('../prospects/frmProspects.aspx?DivID=' + divisionID, '_blank');
                win.focus();
            } else {
                var win = window.open('../Leads/frmLeads.aspx?DivID=' + divisionID, '_blank');
                win.focus();
            }

            return false;
        }
        function reDirectPage(a) {
            parent.parent.frames['mainframe'].location.href = a;
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function validate(TB) {
            var hdnTotalAmountDue = TB.replace("txtDeductions", "hdnTotalAmountDue");

            if (parseFloat(document.getElementById(TB).value) > parseFloat(document.getElementById(hdnTotalAmountDue).value)) {
                document.getElementById(TB).value = "0";
                alert('Deductions is not be greater than Total Amount.');
                return false;
            }
            return true;
        }

        function ValidateCustomDateRange() {
            if ($('[id*=calFrom_txtDate]').val().length == 0) {
                alert("Please enter from date.");
                $('[id*=calFrom_txtDate]').focus();
                return false;
            }

            if ($('[id*=calTo_txtDate]').val().length == 0) {
                alert("Please enter to date.");
                $('[id*=calTo_txtDate]').focus();
                return false;
            }
        }

        function GenerateExportHTML() {
            $("#txtExportHtml").val($("[id$=tblPayroll]").html());
            return true;
        }

        function CalculateCommission() {
            $("[id$=btnGenerateCommission]").click();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanelCondition" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" class="row">
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="pull-left">
                    <div class="form-inline">
                        <asp:Label ID="lblCurrent" runat="server" Font-Bold="true">Current Pay Period</asp:Label>
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="signup form-control" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="signup form-control" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlDate" runat="server" Style="max-width: 180px;" CssClass="signup form-control" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:Label ID="lblDepartment" runat="server" Font-Bold="true">Department</asp:Label>&nbsp;
                        <asp:DropDownList ID="ddlDepartment" Style="max-width: 150px;" runat="server" CssClass="signup form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="pull-right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="form-inline">
                        <ContentTemplate>
                            <asp:CheckBox ID="chkDate" runat="server" Text="Use date range" Font-Bold="true" />
                            <label>From</label>
                            <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                            <label>To</label>
                            <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                            <asp:LinkButton ID="btnGo" runat="server" CssClass="ImageButton btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                            <asp:LinkButton ID="imgBtnExportExcel" CssClass="btn btn-primary" runat="server" OnClientClick="return GenerateExportHTML()"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to CSV</asp:LinkButton>
                            <asp:Button ID="btnPay" runat="server" CssClass="btn btn-primary" Text="Pay Amounts Remaining" OnClick="btnPay_Click" />
                            <asp:TextBox ID="txtExportHtml" runat="server" Style="display: none;"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="imgBtnExportExcel" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:HiddenField ID="hdnPayPeriod" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:Label ID="lblException" runat="server" Text="" ForeColor="Red"></asp:Label>
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Payroll Expense&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmpayrollliabilitylist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanelCommission" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnGenerateCommission" runat="server" Text="Recalculate Commission" CssClass="ImageButton btn btn-primary" style="display:none" />
            <asp:HiddenField ID="hdnComPayPeriodID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanelGrid" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataList ID="dtlPayrollLiability" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" OnItemCommand="dtlPayrollLiability_ItemCommand">
                            <HeaderTemplate>
                                <table id="tblPayroll" class="table table-striped table-bordered" style="overflow-x: auto;white-space: nowrap;">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Name</th>
                                            <th rowspan="2">T&E Registry</th>
                                            <th rowspan="2">
                                                <asp:Button Id="btnUpdatePayrollAmt" CssClass="btn btn-primary" runat="server" CommandName="PayTime" Text="Update Amt Paid" style="margin-bottom:10px" />
                                                <br />
                                                Hrs Entered + Additional $
                                            </th>
                                            <th colspan="5">Commission Amt&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnRecalculateCommission" runat="server" Text="Recalculate Commission" CssClass="btn btn-primary" OnClientClick="return CalculateCommission();" /></th>
                                            <th rowspan="2" style="width: 150px;">
                                                <asp:Button Id="btnUpdateReimbursableAmt" CssClass="btn btn-primary" runat="server" CommandName="PayReimbursableExpense" Text="Update Amt Paid" style="margin-bottom:10px" />
                                                <br />
                                                Reimbursable<br />Expenses
                                            </th>
                                            <th rowspan="2"><asp:LinkButton ID="lbAmountDueSort" runat="server" OnClick="lbAmountDueSort_Click">Amt<br />Due<%# If(Convert.ToString(ViewState("SortColumn")) = "AmountDue", If(Convert.ToString(ViewState("SortOrder")) = "DESC", " <i class='fa fa-caret-down'></i>", " <i class='fa fa-caret-up'></i>"), "")%></asp:LinkButton></th>
                                            <th rowspan="2">Amt<br />Paid</th>
                                            <th rowspan="2"><asp:LinkButton ID="lbAmountRemainingSort" runat="server" OnClick="lbAmountRemainingSort_Click">Amt<br />Remaining<%# If(Convert.ToString(ViewState("SortColumn")) = "AmountRemaining", If(Convert.ToString(ViewState("SortOrder")) = "DESC", " <i class='fa fa-caret-down'></i>", " <i class='fa fa-caret-up'></i>"), "")%></asp:LinkButton></th>
                                            <th rowspan="2">
                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkAll" />
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Paid Invoices <br/>
                                                (<img alt="" src="../images/password.png" /> Deposited/bank)
                                            </th>
                                            <th>Un-paid Invoices<br />
                                                * CMs & Refunds</th>
                                            <th>Invoice<br />Sub-Totals</th>
                                            <th>Order<br />Sub-Totals</th>
                                            <th>Commission Earned <br />(Paid)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="pull-left"><asp:Label ID="lblName" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"vcUserName")%>'></asp:Label>&nbsp;(<%# Session("Currency")%><asp:Label ID="lblHourlyRate" runat="server" Text='<%# ReturnMoney(Eval("monHourlyRate"))%>'></asp:Label>)</div>
                                            </div>
                                        </div>
                                        <i style="color:#aaa;"><b>Tax ID:</b> <%# DataBinder.Eval(Container.DataItem,"vcTaxID")%></i>
                                        <asp:TextBox ID="txtTax" runat="server" Visible="false" CssClass="form-control" Width="150" Text='<%# DataBinder.Eval(Container.DataItem,"vcTaxID")%>'></asp:TextBox>
                                        <asp:HiddenField ID="hdnIsOnPayroll" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitPayroll")%>' />
                                        <asp:HiddenField ID="hdnUserID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numUserID")%>' />
                                        <asp:HiddenField ID="hdnUserCntID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numUserCntID")%>' />
                                        <asp:HiddenField ID="hdnDivisionID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numDivisionID")%>' />
                                        <asp:HiddenField ID="hdnCommissionContact" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitCommissionContact")%>' />
                                    </td>
                                    <td style="text-align:center">
                                        <asp:LinkButton runat="server" Text="Details" ID="lnkbtnDetails" CommandName="Details"><img alt="" src="../images/TimeRegistry.png" /></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:Label ID="lblHours" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "decRegularHrs")%>'></asp:Label> (hrs) = <%# ReturnMoney(Convert.ToDecimal(Eval("monHourlyRate")) * Convert.ToDecimal(Eval("decRegularHrs")))%>
                                        <br />
                                        <asp:HiddenField ID="hdnTimePaid" runat="server" Value="0" />
                                        <ul class="list-inline">
                                            <li><b><asp:Label ID="lblHoursWorkedPaid" runat="server" Text='<%# If(Convert.ToDecimal(Eval("monRegularHrsPaid")) > 0, "Amt Paid", "")%>'></asp:Label></b></li>
                                            <li><asp:TextBox runat="server" ID="txtPayrollAmt" CssClass="form-control" Width="80" Text='<%# If(Convert.ToDecimal(Eval("monRegularHrsPaid")) > 0, Convert.ToDecimal(Eval("monRegularHrsPaid")), Convert.ToDecimal(Eval("monHourlyRate")) * Convert.ToDecimal(Eval("decRegularHrs")))%>' /></li>
                                            <li>&nbsp;+&nbsp;</li>
                                            <li><asp:TextBox runat="server" CssClass="form-control" ID="txtPayrollAdditionalAmount" Width="80" Text='<%# Convert.ToDecimal(Eval("monAdditionalAmountPaid")) %>' /></li>
                                        </ul>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:HyperLink ID="hplBizDocPaidCommAmt" runat="server" Target="_blank" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem,"monCommPaidInvoice")) %>'></asp:HyperLink>
                                        <br />
                                        <ul class="list-inline" style="margin-bottom: 0px;">
                                            <li style="vertical-align: middle; padding: 0px">(&nbsp;<img alt="" src="../images/password.png" />&nbsp;&nbsp;<asp:Label runat="server" ID="lblDeposited" Text='<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monCommPaidInvoiceDeposited"))%>'></asp:Label>&nbsp;)
                                            </li>
                                        </ul>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:HyperLink ID="hplBizDocNonPaidCommAmt" runat="server" Target="_blank" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem,"monCommUNPaidInvoice")) %>' ForeColor="Red"></asp:HyperLink>
                                        <br />
                                        * (<asp:HyperLink ID="hplSalesReturn" runat="server" Target="_blank" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "monCommPaidCreditMemoOrRefund"))%>' />)
                                    </td>
                                    <td style="text-align: right">
                                        <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monInvoiceSubTotal"))%>
                                    </td>
                                    <td style="text-align: right">
                                        <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monOrderSubTotal"))%>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:HyperLink ID="hplCommissionEarned" runat="server" Target="_blank" Text='<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monCommissionEarned"))%>'></asp:HyperLink>
                                        <br />
                                        (<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monCommissionPaid"))%>)
                                        <br />
                                        <asp:Label ID="lblOverpayment" runat="server" Visible='<%# If(Convert.ToDecimal(Eval("monOverPayment")) <> 0, True, False)%>' ForeColor="Red" Text='<%# "Overpaid: " & ReturnMoney(Convert.ToDecimal(Eval("monOverPayment")) * -1)%>'></asp:Label>
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:HiddenField ID="hdnReimbursableExpensePaid" runat="server" Value="0" />
                                        <asp:Label runat="server" ID="lblReimburse" Text='<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monReimburse"))%>'></asp:Label>
                                        <br />
                                        <ul class="list-inline">
                                            <li><b><asp:Label ID="lblRXPaid" runat="server" Text='<%# If(Convert.ToDecimal(Eval("monReimbursePaid")) > 0, "Amt Paid", "")%>'></asp:Label></b></li>
                                            <li><asp:TextBox ID="txtReimburseExpense" runat="server" CssClass="form-control" Width="120" Text='<%# If(Convert.ToDecimal(Eval("monReimbursePaid")) > 0, Convert.ToDecimal(Eval("monReimbursePaid")), Convert.ToDecimal(Eval("monReimburse")))%>'></asp:TextBox></li>
                                        </ul>
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:Label runat="server" ID="lblTotalAmountDue" Text='<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monTotalAmountDue"))%>'></asp:Label>
                                    </td>
                                    <td><%#ReturnMoney(DataBinder.Eval(Container.DataItem, "monAmountPaid"))%></td>
                                    <td><%# If(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "monTotalAmountDue")).ToString("N2") = Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "monAmountPaid")).ToString("N2") AndAlso Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "monAmountPaid")) <> 0, "<img src='../images/icons/amountPaid.png' align='BASELINE' style='float:none;'>", ReturnMoney(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "monTotalAmountDue")) - Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "monAmountPaid"))))%></td>                                    
                                    <td>
                                        <asp:HiddenField ID="hdnTimeAmountToPay" runat="server" Value='<%# Convert.ToDecimal(Eval("monHourlyRate")) * Convert.ToDecimal(Eval("decRegularHrs"))%>' />
                                        <asp:HiddenField ID="hdnSalesReturn" runat="server" Value='<%#Convert.ToDecimal(Eval("monCommPaidCreditMemoOrRefund"))%>' />
                                        <asp:HiddenField ID="hdnOverPayment" runat="server" Value='<%#Convert.ToDecimal(Eval("monOverPayment"))%>' />
                                        <asp:HiddenField ID="hdnReimbursableExpenseToPay" runat="server" Value='<%#Convert.ToDecimal(Eval("monReimburse"))%>' />
                                        <asp:HiddenField ID="hdnCommissionToPay" runat="server" Value='<%#Convert.ToDecimal(Eval("monCommissionEarned"))%>' />
                                        <asp:CheckBox ID="chkSelect" runat="server" CssClass="chk" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:DataList>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnLoad" runat="server" Style="display: none"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
