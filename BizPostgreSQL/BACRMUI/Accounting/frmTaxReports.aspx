﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmTaxReports.aspx.vb" Inherits=".frmTaxReports" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>


<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style type="text/css">
        .rgAltRow1, tr.rgAltRow1 td {
            background-color: #f2f2f2 !important;
        }

        .rgRow1, tr.rgRow1 td {
            background-color: #fff !important;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .hs {
            border: 1px solid #e4e4e4;
            border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            text-align: center;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            From
                        </label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>
                            To</label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <asp:LinkButton ID="btnGo" runat="server" CssClass="button btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateExcel" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="btnExportExcel" runat="server" CssClass="button btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcel" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    <asp:Label Text="Tax Reports (All Customers & Vendors)" runat="server" ID="lblReportName" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <asp:GridView ID="gvTaxReport" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
            Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
            <AlternatingRowStyle CssClass="ais" />
            <RowStyle CssClass="is" />
            <HeaderStyle CssClass="hs" />
            <FooterStyle CssClass="rgAltRow1" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblnumOppBizDocsId" Text='<%# Eval("numOppBizDocsId")%>' />
                        <asp:Label runat="server" ID="lblnumOppId" Text='<%# Eval("numOppId")%>' />/>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField HeaderText="Organization" DataField="Organization" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField HeaderText="INV Created Date" DataField="INVCreatedDate" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField HeaderText="BizDoc" DataField="vcBizDocID" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField HeaderText="State" DataField="vcShipState" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:BoundField HeaderText="Tax Name" DataField="vcTaxName" Visible="false" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                <asp:TemplateField HeaderText="Rate" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# String.Format("{0:N2}", Eval("fltPercentage")) & " " & If(Eval("tintTaxType") = 2, "(Flat Amount)", "(%)")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Tax Collected (on Sales)" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# ReturnMoney(Eval("SalesTax"))%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblSalesTaxTotal" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Tax Paid (on Purchases)" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# ReturnMoney(Eval("PurchaseTax"))%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblPurchasesTaxTotal" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Net Tax Payable" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# ReturnMoney(CCommon.ToDecimal(Eval("SalesTax")) - CCommon.ToDecimal(Eval("PurchaseTax")))%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTaxTotal" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sub Total" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblSubTotal" Text='<%# String.Format("{0:N2}", (Eval("SubTotal")))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grand Total" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblGrandTotal" Text='<%# String.Format("{0:N2}", (Eval("GrandTotal")))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No records found.
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
