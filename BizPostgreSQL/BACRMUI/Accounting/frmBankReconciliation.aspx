﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBankReconciliation.aspx.vb"
    Inherits=".frmBankReconciliation" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" href="../CSS/jscal2.css" type="text/css" />
    <link rel="stylesheet" href="../CSS/border-radius.css" type="text/css" />
    <script src="../JavaScript/jscal2.js" type="text/javascript"></script>
    <script src="../JavaScript/en.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/Common.js"></script>
    <title></title>
    <script type="text/javascript">
        function $Ele(a) {
            return document.getElementById(a);
        }

        function SelectAllchk() {
            SelectAll('SelectAllCheckBox', 'chkSelected');
            CheckThis()
        }
        function CheckThis() {
            var ReconBalance = 0, DepositTotal = 0, PaymentTotal = 0, PaymentCount = 0, DepositCount = 0;

            $("[id$=dgJournalEntry] tr").each(function () {
                var chk = $(this).find("input[id='chk']").is(':checked');
                var ReconbalValue = parseFloat($(this).find('#lblReconbalValue').text().replace(",", ""));

                if (chk == true) {
                    if (ReconbalValue > 0) {
                        DepositTotal = DepositTotal + ReconbalValue;
                        DepositCount = DepositCount + 1;
                    }
                    else {
                        PaymentTotal = PaymentTotal + (ReconbalValue * -1);
                        PaymentCount = PaymentCount + 1;
                    }
                }
            });

            $('#lblDeposit').text(formatCurrency(DepositTotal));
            $('#lblPayment').text(formatCurrency(PaymentTotal));

            var BeginningBalance = parseFloat($('#lblBeginningBalance').text().replace(",", ""));

            var ClearedBalAmt = BeginningBalance + DepositTotal - PaymentTotal;
            $('#lblClearedBalAmt').text(formatCurrency(ClearedBalAmt));

            var EndingBalAmt = parseFloat($('#lblEndingBalAmt').text().replace(",", ""));
            var DifferenceAmt = EndingBalAmt - ClearedBalAmt;
            $('#lblDifferenceAmt').text(formatCurrency(DifferenceAmt));

            if (DifferenceAmt != 0)
                $('#lblDifferenceAmt').css("color", "red");
            else
                $('#lblDifferenceAmt').css("color", "");

            $('#lblPaymentCount').text(PaymentCount);
            $('#lblDepositCount').text(DepositCount);
        }


        function CheckAllStatementData() {
            $("[id$=dgStatementData] tr").each(function () {
                var chk = $(this).find("input[id='chk']");
                chk.prop("checked", true);
            });
        }

        function DeleteRecord() {
            var str;
            str = 'Are you sure you want to cancel reconcile?'
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }

        function EditRecon(a) {
            window.location.href = "../Accounting/frmBankReconcile.aspx?ReconcileID=" + a;
            return false;
        }

        function OpenTimeExpense(url) {

            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, "BizInvoiceGL", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }

        function OpenBill(a) {
            window.open('../Accounting/frmAddBill.aspx?BillID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenLandedCost(a) {
            window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + b, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenAmountPaid(lngReconcileID, lintDepositId) {
            window.open("../opportunity/frmAmtPaid.aspx?frm=BankRecon&ReconcileID=" + lngReconcileID + "&DepositeID=" + lintDepositId, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
    <style>
        .contentBox .row {
            margin-left: 0px !important;
            margin-right: 0px !important;
            line-height: 27px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnSaveReconcileBalance" runat="server" Text="Finish Now" CssClass="btn btn-primary" />
                <asp:Button ID="btnFinishLater" runat="server" Text="Finish Later" CssClass="btn btn-primary" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <asp:HyperLink ID="hplEditStatement" runat="server" Text="Edit Statement" ForeColor="White"></asp:HyperLink>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Bank Reconciliation (<asp:Label ID="lblAccount" runat="server"> </asp:Label>
    -
    <asp:Label ID="lblStatementDate" runat="server"> </asp:Label>)
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12 text-center">
            <asp:Label ID="litMessage" EnableViewState="false" runat="server" CssClass="text-aqua text-bold"></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 contentBox">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-body" style="display: block;">
                            <asp:RadioButtonList ID="rblRegisterFilter" runat="server" AutoPostBack="true" CssClass="signup">
                                <asp:ListItem Text="<b>All Posting</b>" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<b>Money In </b><i>(e.g Deposits)</i>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<b>Money Out</b><i>(e.g Bill Payments)</i>" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            <asp:CheckBox ID="chkDate" runat="server" Text="Hide transactions after the statement's end date"
                                AutoPostBack="true" Checked="true" />
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-body" style="display: block;">
                            <div class="row">
                                <div class="form-inline">
                                    <asp:HyperLink ID="hlServiceCharge" runat="server" Text="Service Charge"></asp:HyperLink>:
                      <asp:Label ID="lblServiceChargeAmt" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <asp:HyperLink ID="hlInterestEarned" runat="server" Text="Interest Earned"></asp:HyperLink>:
                        <asp:Label ID="lblInterestEarnedAmt" runat="server" CssClass="btn-sm btn-primary" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-body" style="display: block;">
                            <div class="row">
                                <div class="form-inline">
                                    <asp:HyperLink ID="hlBeginningBalance" runat="server" Text="Beginning Balance"></asp:HyperLink>:
                        <asp:Label ID="lblBeginningBalance" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <asp:Label ID="lblPaymentCount" runat="server" Text="0"> </asp:Label>&nbsp;&nbsp;&nbsp;
                                        Checks and Payments:
                        <asp:Label ID="lblPayment" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <asp:Label ID="lblDepositCount" runat="server" Text="0"> </asp:Label>&nbsp;&nbsp;&nbsp;
                                        Deposits and Other Credits:
                         <asp:Label ID="lblDeposit" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    Statement Ending Balance:
                         <asp:Label ID="lblEndingBalAmt" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    Cleared Balance:
                         <asp:Label ID="lblClearedBalAmt" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    Difference:
                        <asp:Label ID="lblDifferenceAmt" CssClass="btn-sm btn-primary" runat="server" Text="0.00"> </asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10" id="divColorCoding" runat="server" visible="false">
        <div class="col-sm-12 col-md-3">
            <table>
                <tr>
                    <td>
                        <div style="width: 20px; height: 20px; border: 1px solid #000; background-color: #effff1"></div>
                    </td>
                    <td style="padding-left: 10px;font-weight:bold">Matches Date, Payee/Description & Amount (100% match)</td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12 col-md-3">
            <table>
                <tr>
                    <td>
                        <div style="width: 20px; height: 20px; border: 1px solid #000; background-color: #ffeec9"></div>
                    </td>
                    <td style="padding-left: 10px;font-weight:bold">Matches Amount only without duplicate Amounts elsewhere</td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12 col-md-3">
            <table>
                <tr>
                    <td>
                        <div style="width: 20px; height: 20px; border: 1px solid #000; background-color: #fcdef7"></div>
                    </td>
                    <td style="padding-left: 10px;font-weight:bold">Matches Amount only but with duplicates Amounts found elsewhere</td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12 col-md-3">
            <table>
                <tr>
                    <td>
                        <div style="width: 20px; height: 20px; border: 1px solid #000; background-color: #ffffff"></div>
                    </td>
                    <td style="padding-left: 10px;font-weight:bold">No Amount matches found (manual mapping needed)</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6" id="divStatment" runat="server" visible="false">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Statement Data</h3>

                    <div class="box-tools pull-right">
                        <ul class="list-inline">
                            <li>
                                <div class="form-inline">
                                    <label>Total Records:</label>
                                    <asp:Label ID="lblTotalRecords" runat="server" Font-Size="13" CssClass="badge bg-light-blue"></asp:Label>
                                </div>
                            </li>
                            <li>
                                <div class="form-inline">
                                    <label>Duplicate Records:</label>
                                    <asp:Label ID="lblDuplicateRecords" runat="server" Font-Size="13" CssClass="badge bg-red"></asp:Label>
                                </div>
                            </li>
                            <li>
                                <div class="form-inline">
                                    <label>Matched Records:</label>
                                    <asp:Label ID="lblMatchedRecords" runat="server" Font-Size="13" CssClass="badge bg-green"></asp:Label>
                                </div>
                            </li>
                            <li>
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive" style="height: 600px; overflow: auto">
                        <asp:GridView ID="dgStatementData" runat="server" CssClass="table table-striped table-bordered" DataKeyNames="ID" Width="100%" ShowFooter="true" AutoGenerateColumns="False" UseAccessibleHeader="true">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="SelectAllCheckBox" runat="server" onclick="CheckAllStatementData()" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" Checked='<%# CCommon.ToBool(Eval("bitMatched"))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="dtEntryDate" HeaderText="Entry Date" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="fltAmount" HeaderText="Amount" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="vcReference" HeaderText="Reference" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="vcPayee" HeaderText="Payee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="vcDescription" HeaderText="Description"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-sm-12" id="divJournalEntry" runat="server">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Journal Entries</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive" style="height: 600px; overflow: auto">
                        <asp:GridView ID="dgJournalEntry" runat="server" CssClass="table table-striped table-bordered" Width="100%" ShowFooter="true" AutoGenerateColumns="False" UseAccessibleHeader="true" AllowSorting="true" OnSorting="dgJournalEntry_Sorting" OnRowCommand="dgJournalEntry_RowCommand" OnRowDataBound="dgJournalEntry_RowDataBound" OnRowCreated="dgJournalEntry_RowCreated">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="SelectAllCheckBox" runat="server" onclick="SelectAllchk()" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" onclick="CheckThis()" />
                                        <asp:HiddenField ID="hdnTransactionId" runat="server" Value='<%# Eval("numTransactionId") %>' />
                                        <asp:Label ID="lblReconbalValue" runat="server" Text="" Style="display: none"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="JournalId" Visible="false"></asp:BoundField>
                                <asp:TemplateField HeaderText="Entry Date" ItemStyle-Wrap="false" SortExpression="EntryDate" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblJDate" runat="server" Text='<%# ReturnDate(Eval("EntryDate")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" SortExpression="TransactionType" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="OpenRecord"><%#DataBinder.Eval(Container.DataItem, "TransactionType")%></asp:LinkButton>
                                        <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"JournalId")%>'></asp:Label>
                                        <asp:Label ID="lblCheckId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCheckHeaderID")%>'></asp:Label>
                                        <asp:Label ID="lblCashCreditCardId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCashCreditCardId")%>'></asp:Label>
                                        <asp:Label ID="lblChartAcntId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numChartAcntId")%>'></asp:Label>
                                        <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
                                        <asp:Label ID="lblOppId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppId")%>'></asp:Label>
                                        <asp:Label ID="lblOppBizDocsId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppBizDocsId")%>'>'</asp:Label>
                                        <asp:Label ID="lblDepositId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numDepositId")%>'></asp:Label>
                                        <asp:Label ID="lblCategoryHDRID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategoryHDRID")%>'></asp:Label>
                                        <asp:Label ID="lblTEType" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"tintTEType")%>'></asp:Label>
                                        <asp:Label ID="lblCategory" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategory")%>'></asp:Label>
                                        <asp:Label ID="lblUserCntID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numUserCntID")%>'></asp:Label>
                                        <asp:Label ID="lblFromDate" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"dtFromDate")%>'></asp:Label>
                                        <asp:Label ID="lblBillPaymentID" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numBillPaymentID")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Organization" SortExpression="Organization" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <%# ReturnMoney(Eval("CompanyName"))%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <b>Total:</b>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Deposit (Biz)" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" SortExpression="Deposit" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <%# ReturnMoney(Eval("Deposit")) %>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label Text="" runat="server" ID="lblTotalDeposite" Font-Bold="true" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment (Biz)" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"  SortExpression="Payment" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <%#ReturnMoney(Eval("Payment"))%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label Text="" runat="server" ID="lblTotalPayment" Font-Bold="true" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="vcCheckNo" HeaderText="Check No" SortExpression="vcCheckNo" HeaderStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="Memo" HeaderText="Memo" HeaderStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="vcReference" HeaderText="Reference #" HeaderStyle-Wrap="false"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfAccountID" runat="server" Value="0" />
    <asp:HiddenField ID="hfReconcileComplete" runat="server" Value="0" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
