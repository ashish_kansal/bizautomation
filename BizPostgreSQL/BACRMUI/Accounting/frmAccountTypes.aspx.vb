﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
'Imports Infragistics.WebUI
Imports Telerik.Web.UI
Partial Public Class frmAccountTypes
    Inherits BACRMPage
    Dim objCOA As ChartOfAccounting
    Dim treeNode As RadTreeNode
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(35, 108)
            If Not IsPostBack Then

                btnAddNew.Attributes.Add("onclick", "OpenNewAccountType();")
                btnDelete.Attributes.Add("onclick", "return DeleteSelected()")
                btnEdit.Attributes.Add("onclick", "return EditSelected()")
                LoadTreeView()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub LoadTreeView()
        Try
            If objCOA Is Nothing Then objCOA = New ChartOfAccounting
            'Dim sSorttype As String = objCOA.GetDomainWiseSort(CInt(Session("DomainID")), "COA")
            'If (sSorttype = String.Empty) Then sSorttype = "ID"
            RadTreeView1.Nodes.Clear()
            objCOA.DomainId = Session("DomainID")
            objCOA.UserCntId = Session("UserContactId")

            'If (sSorttype <> "") Then
            LoadTreeStructureRecursive(objCOA.GetAccountTypes())
            'Else : LoadTreeStructureRecursive(objCOA.GetAccountTypes())
            'End If
            RadTreeView1.Nodes(0).AllowDrag = False
            RadTreeView1.Nodes(0).AllowDrop = False
            RadTreeView1.Nodes(0).AllowEdit = False

            For Each node As RadTreeNode In RadTreeView1.GetAllNodes().Where(Function(x) x.Level = 1).AsEnumerable()
                node.AllowDrag = False
                node.AllowDrop = False
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' Description : This Private method is use for fetching the Child Account records against Parent Account Id.
    Private Sub LoadTreeStructureRecursive(ByVal dsAccounts As DataSet)
        Try
            '' These loop for set All Sorted Nodes in Treeview.
            For Each row As DataRow In dsAccounts.Tables(0).Rows
                treeNode = New RadTreeNode
                If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                    treeNode.Text = row("vcAccountType").ToString

                    If Not CCommon.ToBool(row("bitActive")) Then
                        treeNode.Text = treeNode.Text & " (Inactive)"
                        treeNode.ForeColor = Color.Red
                    End If
                Else
                    treeNode.Text = row("vcAccountType1").ToString

                    If Not CCommon.ToBool(row("bitActive")) Then
                        treeNode.Text = treeNode.Text & " (Inactive)"
                        treeNode.ForeColor = Color.Red
                    End If
                End If

                treeNode.Value = row("numAccountTypeID").ToString
                treeNode.Expanded = True
                RadTreeView1.Nodes.Add(treeNode)
            Next

            '' These loop for set all Sorted Child Nodes in Treeview respect of ParentId.
            For Each row As DataRow In dsAccounts.Tables(1).Rows
                'If Not IsNothing(ultratreeTypeOfAcnt.Find(row("numParentID").ToString)) Then
                CreateNode(row)
                'ultratreeTypeOfAcnt.Find(row("numParentID").ToString).Nodes.Add(Me.CreateNode(lstrColor, row))
                'Else : arrChildNodes.Add(row)
                'End If
            Next
            '' These loop for set all Sorted Child Nodes in Treeview respect of ParentId.
            'For Each row As DataRow In arrChildNodes
            '    If Not IsNothing(ultratreeTypeOfAcnt.Find(row("numParentID").ToString)) Then
            '        ultratreeTypeOfAcnt.Find(row("numParentID").ToString).Nodes.Add(Me.CreateNode(lstrColor, row))
            '        ultratreeTypeOfAcnt.Find(row("numParentID").ToString).Nodes.Sort(False, False)
            '    End If
            'Next
            'arrChildNodes.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '' End of the Code.


    Private Sub CreateNode(ByVal row As DataRow)
        Try
            treeNode = New RadTreeNode
            If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                treeNode.Text = row("vcAccountType").ToString
            Else
                treeNode.Text = row("vcAccountType1").ToString
            End If

            If Not CCommon.ToBool(row("bitActive")) Then
                treeNode.Text = treeNode.Text & " (Inactive)"
                treeNode.ForeColor = Color.Red
            End If

            treeNode.Value = row("numAccountTypeID").ToString
            treeNode.Expanded = True
            If Not RadTreeView1.FindNodeByValue(row("numParentID").ToString) Is Nothing Then
                RadTreeView1.FindNodeByValue(row("numParentID").ToString).Nodes.Add(treeNode)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If objCOA Is Nothing Then objCOA = New ChartOfAccounting
            objCOA.DomainId = Session("DomainID")
            objCOA.AccountTypeID = AccountTypeId.Value
            objCOA.Mode = 3 'delete
            Try
                objCOA.ManageAccountType()
            Catch ex As Exception
                If ex.Message = "SYSTEM" Then
                    litMessage.Text = "Can not delete default account type. Your are only allowed to rename default account type name, to avoid conflict in account selection while setting up default accounts."
                ElseIf ex.Message = "CHILD_ACCOUNT" Then
                    litMessage.Text = "Can not delete record,Child Account Found. Your option is to remove all child accounts from Chart of Accounts and try again."
                ElseIf ex.Message = "CHILD" Then
                    litMessage.Text = "Child Records Found.Can not delete selected record."
                ElseIf ex.Message = "AR/AP" Then
                    litMessage.Text = "Can not delete Accounts Receivable/Accounts Payable."
                Else
                    Throw ex
                End If
            End Try
            objCOA.AccountTypeID = 0
            objCOA.Mode = 0
            LoadTreeView()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            RadTreeView1.Nodes.Clear()
            LoadTreeView()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Protected Sub RadTreeView1_NodeDrop(sender As Object, e As RadTreeNodeDragDropEventArgs)
        Try
            If e.SourceDragNode.Level <> e.DestDragNode.Level Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidNodeDropped", "alert('Only nodes from the same level can be dropped here.')", True)
            Else
                If e.DropPosition = RadTreeViewDropPosition.Over Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidNodeDropped", "alert('Other nodes can not be dropped here.')", True)
                ElseIf e.DropPosition = RadTreeViewDropPosition.Above Then
                    For Each node As RadTreeNode In e.DraggedNodes
                        node.Owner.Nodes.Remove(node)
                        e.DestDragNode.InsertBefore(node)
                    Next
                ElseIf e.DropPosition = RadTreeViewDropPosition.Below Then
                    For Each node As RadTreeNode In e.DraggedNodes
                        node.Owner.Nodes.Remove(node)
                        e.DestDragNode.InsertAfter(node)
                    Next
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnSaveHierarchy_Click(sender As Object, e As EventArgs)
        Try
            Dim listNodes As System.Collections.Generic.List(Of RadTreeNode) = RadTreeView1.GetAllNodes()

            Dim dt As New DataTable("AccountType")
            dt.Columns.Add("numAccountTypeID")
            dt.Columns.Add("tintSortOrder")
            Dim dr As DataRow

            For Each node As RadTreeNode In listNodes
                Dim nodeIndex As Integer = 0

                If node.Level <> 0 AndAlso node.Level <> 1 Then
                    nodeIndex = listNodes.IndexOf(node) - listNodes.IndexOf(node.Parent)
                End If

                dr = dt.NewRow()
                dr("numAccountTypeID") = CCommon.ToLong(node.Value)
                dr("tintSortOrder") = nodeIndex
                dt.Rows.Add(dr)
            Next

            Dim ds As New DataSet
            ds.Tables.Add(dt)

            Dim objChartOfAccounting As New ChartOfAccounting
            objChartOfAccounting.DomainID = CCommon.ToLong(Session("DomainID"))
            objChartOfAccounting.SaveAccountTypeOrder(ds.GetXml())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class