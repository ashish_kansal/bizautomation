<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMakeDeposit.aspx.vb"
    Inherits=".frmMakeDeposit" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Make Deposit</title>
    <script type="text/javascript" src="../JavaScript/Common.js"></script>
    <style type="text/css">
        .PositiveAmount {
            text-align: right;
            white-space: nowrap;
            padding-right: 3px;
            font-size: 18px;
            color: #4E9E19;
        }

        .AmountLabel {
            font-weight: bold;
            text-align: right;
            font-size: 14px;
        }
    </style>
    <script type="text/javascript">
        var IsSplitMode = <%=CCommon.ToInteger(GetQueryStringVal("SplitFlag")) %>

        $(document).ready(function () {

            $(function () {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(initiate);
                prm.add_endRequest(UpdateTotal);
                prm.add_endRequest(SetHiddenValues);

                initiate();
            });
            if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initiate);
            }
            else {
                //Sys.Application.add_init(initiate);
            }

            //$('#txtReceivedFromFliter').change(
            //    function () {                    
            //        $('#hdnReceivedFrom').val($("#txtReceivedFromFliter").val());                   
                
            //    });

            //$("#txtMemoFliter").change(
            //    function () {
            //        $('#hdnMemo').val($("#txtMemoFliter").val());
            //    });

            //$("#txtReferenceFliter").change(
            //   function () {
            //       $('#hdnReference').val($("#txtReferenceFliter").val());
            //   });

            //$("#RadCalDate").datepicker({
            //    dateFormat: "yy-mm-dd",
            //    onSelect: function () {
            //        var selected = $(this).val();
            //        $('#hdnDate').val($(selected).val());
            //    }
            //});
        });

        function initiate() {
            $(".btnAmtPaid").click(function () {
                var dHref = $(this).attr("href");
                //alert(dHref);
                window.open(dHref, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
                return false;
            })
            $("#dgNewDeposits #ddlPaymentFilter").change(function () {
                UpdateTotal();
            });
            if (Number(IsSplitMode) == 0) { $("#lblDepositAmtDiffLabel,#lblDifferenceAmount").hide(); }


            $('#ddlPaymentFilter').val($('#hdnPaymentMethodID').val());

            $("#ddlCardTypeFilter option[value='" + $('#hdnCardType').val() + "']").attr("selected", "selected");

            $("#ddlSourceFilter option[value='" + $('#hdnSource').val() + "']").attr("selected", "selected");

            $(".Delete").click(function (event) {
                if (confirm('Are you sure, you want to delete the selected record?')) {
                    return true;
                }
                else {
                    event.preventDefault();
                    return false;
                }
            });


            //InitializeValidation();
            $("#chkAll").change(function () {
                //console.log($("#chkAll").is(':checked'));
                $("#dgUndepositedFunds tr input[type = 'checkbox']").not("#chkAll").prop("checked", $("#chkAll").is(':checked'))
                UpdateTotal();

            });
            $("#dgUndepositedFunds tr input[type = 'checkbox']").not("#chkAll").change(function () {
                UpdateTotal();
            });
            $("#dgNewDeposits #txtCredit").change(function () {
                UpdateTotal();
            });
            $("#btnDepositSaveNew").click(function (event) {
                if ($(this).id == "btnDepositSaveNew") { $("#__EVENTTARGET").val("btnDepositSaveNew"); } 

                if (Number($("#ddlDepositTo").val()) == 0) {
                    alert("Please select deposit to account")
                    $("#ddlDepositTo").focus()
                    event.preventDefault();
                    return false;
                }
                if ($("#txtDate").val() == "") {
                    alert("Please select deposit date")
                    $("#txtDate").focus()
                    event.preventDefault();
                    return false;
                }

                var itotal = 0.0;
                var iRow;
                var Amt;
                var err = 0;
                iRow = 0;

                $("#dgNewDeposits .is,#dgNewDeposits .ais").not(".hs,.fs").each(function () {

                    iRow = iRow + 1;

                    if (Number($("#lblDepositTotalAmount").text()) == 0.0 && Number($(this).find("#ddlAccounts").val()) == 0 && Number($(this).find("#txtCredit").val()) == 0) {
                        alert("Please enter alteast one line with an account and amount.");
                        event.preventDefault();
                        return false;
                    }
                    if (Number($("#lblDepositTotalAmount").text()) > 0.0 && Number($(this).find("#ddlAccounts").val()) == 0 && Number($(this).find("#txtCredit").val()) > 0) {
                        alert("Please select a valid account for row " + iRow);
                        event.preventDefault();
                        return false;
                    }
                    if (Number($(this).find("#ddlAccounts").val()) > 0 && Number($(this).find("#txtCredit").val()) == 0) {
                        alert("Please specify a valid amount for row " + iRow);
                        event.preventDefault();
                        return false;
                    }

                });

                if (CheckIsReconciled())
                    return true;
                else
                    return false;
            });

            UpdateTotal();
            $("#btnSplitSave").click(function (event) {

                if (Number($("#ddlDepositTo").val()) == 0) {
                    alert("Please select deposit to account")
                    $("#ddlDepositTo").focus()
                    event.preventDefault();
                    return false;
                }
                if ($("#txtDate").val() == "") {
                    alert("Please select deposit date")
                    $("#txtDate").focus()
                    event.preventDefault();
                    return false;
                }


                if (Number($("#lblSplitTotalAmount").text()) != Number($("#hdnDepositTotAmt").val())) {
                    alert("Amount does not match with transaction amount, you need to make sure total amount of deposit is equal to " + $("#lblSplitTotalAmount").text())
                    event.preventDefault();
                    return false;
                }

                var itotal = 0.0;
                var iRow;
                var Amt;
                var err = 0;
                iRow = 0;

                $("#dgNewDeposits .is,#dgNewDeposits .ais").not(".hs,.fs").each(function () {

                    iRow = iRow + 1;

                    if (Number($("#lblSplitTotalAmount").text()) == 0.0 && Number($(this).find("#ddlAccounts").val()) == 0 && Number($(this).find("#txtCredit").val()) == 0) {
                        alert("Please enter alteast one line with an account and amount.");
                        event.preventDefault();
                        return false;
                    }
                    if (Number($("#lblSplitTotalAmount").text()) > 0.0 && Number($(this).find("#ddlAccounts").val()) == 0 && Number($(this).find("#txtCredit").val()) > 0) {
                        alert("Please select a valid account for row " + iRow);
                        event.preventDefault();
                        return false;
                    }
                    if (Number($(this).find("#ddlAccounts").val()) > 0 && Number($(this).find("#txtCredit").val()) == 0) {
                        alert("Please specify a valid amount for row " + iRow);
                        event.preventDefault();
                        return false;
                    }

                });


                if (CheckIsReconciled())
                    return true;
                else
                    return false;
            });
        }



        function UpdateTotal() {

            var Total = 0.00;
            var TotalPaymentSelected = 0.00;
            var NewDepositsTotal = 0.00;
            var DifferenceAmount = 0.00;
            $("#dgUndepositedFunds tr").not(".hs,.fs").each(function () {

                Total = Total + Number(currencyToDecimal($(this).find("#lblPayment").text()))
                if ($(this).find("input[type = 'checkbox']").is(':checked')) {
                    TotalPaymentSelected = TotalPaymentSelected + Number(currencyToDecimal($(this).find("#lblPayment").text()))
                }
            });
            $("#lblTotalPayment").text(Total.toFixed(2))
            $("#lblTotalPaymentSelected").text(TotalPaymentSelected.toFixed(2))

            //console.log(TotalPaymentSelected)

            $("#dgNewDeposits .is,#dgNewDeposits .ais").not(".hs,.fs").each(function () {
                //console.log(Number($(this).find("#txtCredit").val()))
                NewDepositsTotal = NewDepositsTotal + Number($(this).find("#txtCredit").val())

            });
            TotalPaymentSelected = TotalPaymentSelected + NewDepositsTotal;
            DifferenceAmount = Number($("#lblSplitTotalAmount").text()) - NewDepositsTotal

            $("#lblDepositTotalAmount").text(parseFloat(TotalPaymentSelected).toFixed(2))
            $("#lblNewDepositsTotal").text(parseFloat(NewDepositsTotal).toFixed(2))
            $("#hdnDepositTotalAmt").val($("#lblDepositTotalAmount").text())
            $("#hdnDepositTotAmt").val($("#lblNewDepositsTotal").text())

            $("#lblDifferenceAmount").text(parseFloat(DifferenceAmount).toFixed(2))

        }

        function SetHiddenValues(a) {
            $('#hdnPaymentMethodID').val($('#ddlPaymentFilter').val());           

            if ($('#ddlCardTypeFilter option:selected').val() > 0) {
                $('#hdnCardType').val($('#ddlCardTypeFilter option:selected').text());
            }
            else {
                $('#hdnCardType').val('');
            }

            if ($('#ddlSourceFilter option:selected').val() == 0) {
                $('#hdnSource').val('');
            }
            else {
                $('#hdnSource').val($('#ddlSourceFilter option:selected').text());
            }           
        }

        function SplitDepositEnds() {
            window.opener.document.getElementById('btnHiddenSave').click();
            self.close();
        }

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 80, height + 200);
        }

        

        function FilterGrid() {

            document.getElementById('btnGo1').click().focus();
        }

        function OnClientItemsRequesting(sender, eventArgs) {
            var context = eventArgs.get_context();

            if ($("#ddlUserLevelClass").length > 0) {
                context["ClassID"] = $("#ddlUserLevelClass").val();
            } else {
                context["ClassID"] = 0;
            }
        }     

    </script>
    <style>
        .hs {
            background: #ebebeb;
            font-weight: bold;
            border: 1px solid #e4e4e4;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Make Deposit&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmmakedeposit.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static"> 
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
   
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Deposit To<font color="#ff0000">*</font></label>
                <div>
                    <asp:DropDownList ID="ddlDepositTo" runat="server" CssClass="form-control" AutoPostBack="true">
                    </asp:DropDownList>&nbsp;&nbsp;
                    <asp:Label ID="lblBalance" runat="server" Text=""> </asp:Label>
                    &nbsp;&nbsp;
                   <asp:Label ID="lblBalanceAmount" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Deposit Date</label>
                <BizCalendar:Calendar ID="calMakeDeposit" runat="server" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4" runat="server" id="pnlAccountingClass">
            <div class="form-group">
                <label>Class</label>
                <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="form-control" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row padbottom10" id="trMultiCurrency" runat="server">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Currency</label>
                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Exchange Rate</label>
                <div class="form-inline">
                    1&nbsp;
                <asp:Label runat="server" ID="lblForeignCurr"></asp:Label>&nbsp;=&nbsp;
                <asp:TextBox ID="txtExchangeRate" runat="server" CssClass="form-control" onkeypress="CheckNumber(1,event);"></asp:TextBox>&nbsp;&nbsp;<asp:Label
                    runat="server" ID="lblBaseCurr" Text="USD"></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <table style="width:100%">
        <tr>
            <td style="float:right;">
                 <a href="#" id="hplClearGridCondition" onclick="FilterGrid()" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
            </td>
        </tr>
        <tr>
            <td>
                <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Payments</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                
                <asp:DataGrid ID="dgUndepositedFunds" Visible="false" runat="server" CssClass="table table-striped table-bordered" EnableViewState="true"
                    Width="100%" AutoGenerateColumns="False" ShowFooter="true" UseAccessibleHeader="true" AllowSorting="true" OnSortCommand="dgUndepositedFunds_SortCommand">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is" ></ItemStyle>
                    <FooterStyle CssClass="fs"></FooterStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll','chkSelct');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" CssClass="chkSelct" runat="server"
                                    Checked='<%# Eval("bitDepositedToAcnt") %>' />
                                <asp:HiddenField runat="server" ID="hdnAccountID" Value='<%# Eval("numChartAcntId") %>' />
                                <asp:HiddenField runat="server" ID="hdnDepositeID" Value='<%# Eval("numDepositID")%>' />
                                <asp:HiddenField runat="server" ID="hdnDivisionID" Value='<%# Eval("numDivisionID")%>' />
                                <asp:HiddenField runat="server" ID="hdnDepositeDetailID" Value='<%# Eval("numDepositeDetailID")%>' />
                                <asp:HiddenField runat="server" ID="hdnExchangeRateOfReceivedPayment" Value='<%# Eval("fltExchangeRateReceivedPayment")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>                      
                       
                        <asp:TemplateColumn SortExpression="ReceivedFrom" HeaderText="Received From" HeaderStyle-Width="10%" >
                            <HeaderTemplate>
                                 <asp:LinkButton id="lnkReceivedFromFliter" runat="server" Text="Received From" CommandName="Sort" CommandArgument="ReceivedFrom"></asp:LinkButton>
                               
                                <asp:TextBox ID="txtReceivedFromFliter" runat="server" AutoPostBack="true" OnTextChanged="txtReceivedFromFliter_TextChanged" />                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlReceivedFrom" CssClass="btnAmtPaid" runat="server" Text='<%# Eval("ReceivedFrom")%>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="" HeaderStyle-Width="10%" SortExpression="dtDepositDate">
                            <HeaderTemplate>  
                                  <asp:LinkButton id="lnkDateFliter" runat="server" Text="Date" CommandName="Sort" CommandArgument="DepositDate"></asp:LinkButton>
                                <table style="background-color: #ebebeb">
                                    <tr>
                                        <td >
                                            From
                                            <br />
                                            <asp:TextBox ID="txtFromDate" Width="80px" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged" runat="server"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="ajaxcalFromDate" TargetControlID="txtFromDate"  runat="server"></ajaxToolkit:CalendarExtender>   
                                          
                                            <%--<telerik:RadDatePicker ID="RadCalDate" Width="80px" runat="server" AutoPostBack="true" OnSelectedDateChanged="RadCalDate_SelectedDateChanged"></telerik:RadDatePicker> --%>
                                        </td>
                                        <td>
                                            &nbsp; 
                                        </td>
                                        <td >
                                            To
                                            <br />
                                            <asp:TextBox ID="txtToDate" Width="80px" runat="server" AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="ajaxCalToDate" TargetControlID="txtToDate"  runat="server"></ajaxToolkit:CalendarExtender>
                                            <%--<telerik:RadDatePicker ID="RadCalToDate" runat="server" AutoPostBack="true" OnSelectedDateChanged="RadCalDate_SelectedDateChanged"></telerik:RadDatePicker> --%>
                                        </td>
                                    </tr>
                                </table>
                                 
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("dtDepositDate")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="" HeaderStyle-Width="20%" >
                            <HeaderTemplate>
                                 <asp:LinkButton id="lnkPaymentMethodFilter" runat="server" Text="Payment Method" CommandName="Sort" CommandArgument="numPaymentMethod"></asp:LinkButton>
                               
                                <asp:DropDownList ID="ddlPaymentFilter" CssClass="form-control" runat="server" AutoPostBack="true" onchange="SetHiddenValues(1);"
                                    OnSelectedIndexChanged="ddlPaymentFilter_SelectedIndexChanged">
                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlPayment" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Card Type" HeaderStyle-Width="10%" SortExpression="vcCardType">
                            <HeaderTemplate>
                                <asp:LinkButton id="lnkCardTypeFilter" runat="server" Text="Card type" CommandName="Sort" CommandArgument="vcCardType"></asp:LinkButton>
                                <asp:DropDownList ID="ddlCardTypeFilter" CssClass="form-control" runat="server" AutoPostBack="true" onchange="SetHiddenValues(2);"
                                    OnSelectedIndexChanged="ddlCardTypeFilter_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcCardType")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Source" HeaderStyle-Width="10%" SortExpression="vcSource">
                            <HeaderTemplate>
                                <asp:LinkButton id="lnkSourceFilter" runat="server" Text="Source" CommandName="Sort" CommandArgument="vcSource"></asp:LinkButton>
                                <asp:DropDownList ID="ddlSourceFilter" CssClass="form-control" runat="server" AutoPostBack="true" onchange="SetHiddenValues(3);"
                                    OnSelectedIndexChanged="ddlSourceFilter_SelectedIndexChanged">
                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcSource")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn  HeaderStyle-Width="10%" SortExpression="vcMemo">
                            <HeaderTemplate>
                                 <asp:LinkButton id="lnkMemoFliter" runat="server" Text="Memo" CommandName="Sort" CommandArgument="vcMemo"></asp:LinkButton>
                                <asp:TextBox ID="txtMemoFliter" runat="server" AutoPostBack="true" OnTextChanged="txtMemoFliter_TextChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMemo" CssClass="form-control" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcMemo") %>'
                                    Width="100%">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="" HeaderStyle-Width="10%" SortExpression="vcReference">
                            <HeaderTemplate>
                                  <asp:LinkButton id="lnkReferenceFliter" runat="server" Text=" Reference #" CommandName="Sort" CommandArgument="vcReference"></asp:LinkButton>                              
                                <asp:TextBox ID="txtReferenceFliter" runat="server" AutoPostBack="true" OnTextChanged="txtReferenceFliter_TextChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtReference" CssClass="form-control" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcReference")%>'>
                                </asp:TextBox>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                            <FooterTemplate>
                                <b>Total<br />
                                    Selected Payments Total</b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Amount" SortExpression="monDepositAmount" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol") + " " %><asp:Label runat="server" ID="lblPayment" Text='<%# ReturnMoney(DataBinder.Eval(Container, "DataItem.monDepositAmount"))%>' />
                                <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monDepositAmount") * Eval("fltExchangeRateReceivedPayment")) + ")</span>", "")%>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom" Width="110px" />
                            <FooterTemplate>
                                <b>

                                    <asp:Label Text="" runat="server" ID="lblTotalPayment" /><br />
                                    <asp:Label Text="" runat="server" ID="lblTotalPaymentSelected" />
                                </b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="5%" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger Delete btn-xs" CommandName="Delete"
                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.numDepositID")%>'><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
                
            </td>
        </tr>
    </table>
    

    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgNewDeposits" runat="server" CssClass="table table-striped table-bordered" Width="100%"
                    AutoGenerateColumns="False" ShowFooter="true" UseAccessibleHeader="true">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <FooterStyle CssClass="fs"></FooterStyle>
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="68px" />
                            <HeaderTemplate>
                                #
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Container.ItemIndex + 1 %>
                                <asp:HiddenField runat="server" ID="hdnDepositeDetailID" Value='<%# Eval("numDepositeDetailID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Received From">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container, "DataItem.numReceivedFrom")%>'></asp:Label>
                                <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="195px" DropDownWidth="300px"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True"
                                    ClientIDMode="AutoID" OnClientItemsRequesting="OnClientItemsRequesting">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompaniesWithClass" />
                                </telerik:RadComboBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" Width="200px" />
                            <FooterTemplate>
                                <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" OnClick="btnAdd_Click" Style="float: left"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add more row(s)</asp:LinkButton>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Accounts <font color='#ff0000'>*</font>">
                            <ItemTemplate>
                                <%--<asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numJournalId") %>'></asp:Label>
                                <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionId") %>'></asp:Label>--%>
                                <asp:DropDownList ID="ddlAccounts" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                                <%--<asp:TextBox ID="txtAccountTypeId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numAcntType") %>'></asp:TextBox>--%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Payment Method">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlPayment" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Memo">
                            <ItemTemplate>
                                <asp:TextBox ID="txtMemo" TextMode="SingleLine" CssClass="form-control" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcMemo") %>'></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Reference #">
                            <ItemTemplate>
                                <asp:TextBox ID="txtReference" TextMode="SingleLine" CssClass="form-control" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcReference") %>'></asp:TextBox>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                            <FooterTemplate>
                                <b>New Deposits Total: </b>
                                <br />
                                <b>
                                    <asp:Label Text="Deposit Amount Difference:" runat="server" ID="lblDepositAmtDiffLabel" /></b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Project<span style='display:none'>PRJT<span>" ItemStyle-Width="160px">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlProject" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Amount <font color='#ff0000'>*</font>">
                            <ItemTemplate>
                                <asp:Label ID="lblCreditAmt" runat="server" Style="display: none" Text='<%# ReturnMoney(DataBinder.Eval(Container, "DataItem.monAmountPaid"))%>'></asp:Label>
                                <asp:TextBox ID="txtCredit" Style="text-align: right;" TextMode="SingleLine" CssClass="form-control"
                                    runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container, "DataItem.monAmountPaid"))%>'
                                    autocomplete="OFF" onkeypress="CheckNumber(1,event)"></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" Width="110px" />
                            <FooterStyle HorizontalAlign="Right" />
                            <FooterTemplate>
                                <b>
                                    <asp:Label Text="" runat="server" ID="lblNewDepositsTotal" />
                                </b>
                                <br />
                                <b>
                                    <asp:Label Text="" runat="server" ID="lblDifferenceAmount" />
                                </b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger button Delete btn-xs" CommandName="DeleteRow"
                                    CommandArgument=""><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <b>
                    <asp:Label ID="lblDepositTotal" runat="server" Text="" CssClass="AmountLabel"> </asp:Label></b>
                <asp:Label ID="lblDepositTotalAmount" runat="server" Text="0.00" CssClass="PositiveAmount"></asp:Label>
                <asp:Label ID="lblSplitTotalAmount" runat="server" Text="0.00" CssClass="PositiveAmount"
                    Visible="false"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure, you want to delete deposit entry?')"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnDepositSaveNew" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save Deposit</asp:LinkButton>
                <asp:LinkButton ID="btnSplitSave" runat="server" CssClass="btn btn-primary" Text="" Visible="false"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnDepositTotalAmt" runat="server" />
    <asp:HiddenField ID="hdnTransactionID" runat="server" />
    <asp:HiddenField ID="hdnDepositTotAmt" runat="server" />
    <asp:HiddenField ID="hdnPaymentMethodID" runat="server" />
    <asp:HiddenField ID="hdnReceivedFrom" runat="server" />
    <asp:HiddenField ID="hdnCardType" runat="server" />
    <asp:HiddenField ID="hdnMemo" runat="server" />
    <asp:HiddenField ID="hdnReference" runat="server" />
    <asp:HiddenField ID="hdnSource" runat="server" />
    <asp:HiddenField ID="hdnDate" runat="server" />
    <asp:HiddenField ID="hdnFromDate" runat="server" />
    <asp:HiddenField ID="hdnToDate" runat="server" />
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" OnClick="btnGo1_Click1" Style="display:none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>   
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
