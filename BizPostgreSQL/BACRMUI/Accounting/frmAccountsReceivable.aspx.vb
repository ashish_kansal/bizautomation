''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.Text
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Packaging
Imports System.Collections.Generic

Partial Public Class frmAccountsReceivable
    Inherits BACRMPage
    Dim objCommon As CCommon
    Dim mobjGeneralLedger As New GeneralLedger
#Region "Variables"

#End Region
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(35, 83)
            If Not IsPostBack Then
                If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then ibExportExcel.Visible = False

                bindUserLevelClassTracking()

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    Try
                        If PersistTable.ContainsKey(chkHidePastDue.ID) Then
                            chkHidePastDue.Checked = PersistTable(chkHidePastDue.ID)
                        End If

                        If PersistTable.ContainsKey(calFrom.ID) Then
                            calFrom.SelectedDate = PersistTable(calFrom.ID)
                        End If

                        If PersistTable.ContainsKey(calTo.ID) Then
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) 'PersistTable(calTo.ID)
                        End If

                        If PersistTable.ContainsKey(rbInvoiceDate.ID) Then
                            If CCommon.ToBool(PersistTable(rbInvoiceDate.ID)) Then
                                rbDueDate.Checked = False
                                rbInvoiceDate.Checked = True
                            End If
                        End If

                        If PersistTable.ContainsKey(ddlUserLevelClass.ID) Then
                            If Not ddlUserLevelClass.Items.FindByValue(CCommon.ToLong(PersistTable(ddlUserLevelClass.ID))) Is Nothing Then
                                ddlUserLevelClass.ClearSelection()
                                ddlUserLevelClass.Items.FindByValue(CCommon.ToLong(PersistTable(ddlUserLevelClass.ID))).Selected = True
                            End If
                        End If

                    Catch ex As Exception
                        chkHidePastDue.Checked = False
                    End Try
                Else
                    If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                    mobjGeneralLedger.DomainID = CCommon.ToLong(Session("DomainID"))
                    mobjGeneralLedger.Year = CInt(Now.Year)

                    calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                End If


                LoadAccountReceivableGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()

                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            LoadAccountReceivableGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadAccountReceivableGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Dim decCurrentDays, decThirtyDays, decSixtyDays, decNinetyDays, decOverNinetyDays, decThirtyDaysOverDue, decSixtyDaysOverDue, decNinetyDaysOverDue, decOverNinetyDaysOverDue, decTotal, decUnAppliedAmount As Decimal

    Private Sub LoadAccountReceivableGrid()
        Dim lobjVendorPayment As New VendorPayment
        Dim dtARAging As DataTable
        Dim ldecGrantTotal As Decimal
        Try

            lobjVendorPayment.DomainID = Session("DomainId")
            lobjVendorPayment.CompanyID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
            lobjVendorPayment.UserCntID = Session("UserContactID")
            lobjVendorPayment.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
            lobjVendorPayment.dtFromDate = calFrom.SelectedDate
            lobjVendorPayment.dtTodate = CDate(calTo.SelectedDate & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            lobjVendorPayment.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            lobjVendorPayment.BasedOn = If(rbInvoiceDate.Checked, 2, 1)
            dtARAging = lobjVendorPayment.GetAccountReceivableAging

            If dtARAging.Rows.Count > 0 Then

                decCurrentDays = dtARAging.Compute("Sum(numCurrentDays)", String.Empty)
                'decThirtyDays = dtARAging.Compute("Sum(numThirtyDays)", String.Empty)
                ' decSixtyDays = dtARAging.Compute("Sum(numSixtyDays)", String.Empty)
                'decNinetyDays = dtARAging.Compute("Sum(numNinetyDays)", String.Empty)
                'decOverNinetyDays = dtARAging.Compute("Sum(numOverNinetyDays)", String.Empty)
                decThirtyDaysOverDue = dtARAging.Compute("Sum(numThirtyDaysOverDue)", String.Empty)
                decSixtyDaysOverDue = dtARAging.Compute("Sum(numSixtyDaysOverDue)", String.Empty)
                decNinetyDaysOverDue = dtARAging.Compute("Sum(numNinetyDaysOverDue)", String.Empty)
                decOverNinetyDaysOverDue = dtARAging.Compute("Sum(numOverNinetyDaysOverDue)", String.Empty)
                decTotal = dtARAging.Compute("Sum(numTotal)", String.Empty)

                decUnAppliedAmount = dtARAging.Compute("Sum(monUnAppliedAmount)", String.Empty)

                lblGrandTotalAmt.Text = Session("Currency") & ReturnMoney(decTotal)
            Else : lblGrandTotalAmt.Text = Session("Currency") & ReturnMoney(0)
            End If
            dgAR.DataSource = dtARAging
            dgAR.DataBind()

            dgAR.Columns(8).Visible = Not chkHidePastDue.Checked
            dgAR.Columns(9).Visible = Not chkHidePastDue.Checked
            dgAR.Columns(10).Visible = Not chkHidePastDue.Checked
            dgAR.Columns(11).Visible = Not chkHidePastDue.Checked

            PersistTable.Clear()
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            PersistTable.Add(chkHidePastDue.ID, chkHidePastDue.Checked)
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Add(rbDueDate.ID, rbDueDate.Checked)
            PersistTable.Add(rbInvoiceDate.ID, rbInvoiceDate.Checked)
            PersistTable.Save()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgAR_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAR.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                Dim hplCompany As HyperLink
                Dim lblCompany As Label
                hplCompany = e.Item.FindControl("hplCompany")
                hplCompany.NavigateUrl = "#"
                lblCompany = e.Item.FindControl("lblCompany")

                If e.Item.Cells(0).Text <> "&nbsp;" Then
                    hplCompany.Attributes.Add("onclick", "return OpenCompany('" & e.Item.Cells(0).Text & "','" & e.Item.Cells(2).Text & "','" & Session("EnableIntMedPage") & "')")
                    lblCompany.Visible = False
                    lblCompany.Text = ""
                Else
                    lblCompany.Visible = True
                    hplCompany.Visible = False
                    hplCompany.Text = ""
                End If

                Dim imgCustomerStatement As ImageButton
                imgCustomerStatement = DirectCast(e.Item.FindControl("imgCustomerStatement"), ImageButton)
                If imgCustomerStatement IsNot Nothing Then
                    imgCustomerStatement.Attributes.Add("onclick", "return OpenCustomerStatement(" & e.Item.Cells(0).Text & ");")
                End If

            ElseIf e.Item.ItemType = ListItemType.Footer Then
                CType(e.Item.FindControl("lblTotalCurrentDays"), Label).Text = ReturnMoney(decCurrentDays)
                ' CType(e.Item.FindControl("lblTotalThirtyDays"), Label).Text = ReturnMoney(decThirtyDays)
                'CType(e.Item.FindControl("lblTotalSixtyDays"), Label).Text = ReturnMoney(decSixtyDays)
                'CType(e.Item.FindControl("lblTotalNinetyDays"), Label).Text = ReturnMoney(decNinetyDays)
                'CType(e.Item.FindControl("lblTotalOverNinetyDays"), Label).Text = ReturnMoney(decOverNinetyDays)
                CType(e.Item.FindControl("lblTotalThirtyDaysOverDue"), Label).Text = ReturnMoney(decThirtyDaysOverDue)
                CType(e.Item.FindControl("lblTotalSixtyDaysOverDue"), Label).Text = ReturnMoney(decSixtyDaysOverDue)
                CType(e.Item.FindControl("lblTotalNinetyDaysOverDue"), Label).Text = ReturnMoney(decNinetyDaysOverDue)
                CType(e.Item.FindControl("lblTotalOverNinetyDaysOverDue"), Label).Text = ReturnMoney(decOverNinetyDaysOverDue)
                CType(e.Item.FindControl("lblTotalUnAppliedAmount"), Label).Text = ReturnMoney(decUnAppliedAmount)
                CType(e.Item.FindControl("lblTotal"), Label).Text = ReturnMoney(decTotal)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ibExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ibExportExcel.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable("AR Aging")
            dt.Columns.Add("Customer", GetType(String))
            dt.Columns.Add("Current", GetType(Double))
            dt.Columns.Add("1-30 days past due", GetType(Double))
            dt.Columns.Add("31-60 days past due", GetType(Double))
            dt.Columns.Add("61-90 days past due", GetType(Double))
            dt.Columns.Add("Over 90 days past due", GetType(Double))
            dt.Columns.Add("Credits", GetType(Double))
            dt.Columns.Add("Total", GetType(Double))

            Dim dr As DataRow
            For Each drItem As DataGridItem In dgAR.Items
                dr = dt.NewRow()
                dr("Customer") = CCommon.ToString(DirectCast(drItem.FindControl("hdnCompanyName"), HiddenField).Value)
                dr("Current") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdnCurrentDays"), HiddenField).Value)
                dr("1-30 days past due") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdn1To30"), HiddenField).Value)
                dr("31-60 days past due") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdn31To60"), HiddenField).Value)
                dr("61-90 days past due") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdn61To90"), HiddenField).Value)
                dr("Over 90 days past due") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdnOver90"), HiddenField).Value)
                dr("Credits") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdnCredits"), HiddenField).Value)
                dr("Total") = CCommon.ToDouble(DirectCast(drItem.FindControl("hdnTotal"), HiddenField).Value)
                dt.Rows.Add(dr)
            Next

            ds.Tables.Add(dt)

            Dim fileName As String = "ARAging" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".xlsx"

            Dim objCSVExport As New CSVExport
            objCSVExport.DataSetToExcel(ds, fileName, CCommon.ToLong(Session("DomainID")))

            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.ClearContent()
            response.Clear()
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment; filename=" & fileName & ";")
            response.TransmitFile(BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session("DomainID"))) & fileName)
            response.Flush()

            Try
                System.IO.File.Delete(BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session("DomainID"))) & fileName)
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try

            response.End()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub ClearControls(ByVal control As Control)
        Try
            Dim i As Integer
            For i = control.Controls.Count - 1 To 0 Step -1
                ClearControls(control.Controls(i))
            Next i

            If TypeOf control Is System.Web.UI.WebControls.Image Then control.Parent.Controls.Remove(control)
            If (Not TypeOf control Is TableCell) Then
                If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    Try
                        literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                    Catch
                    End Try
                    control.Parent.Controls.Remove(control)
                Else
                    If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                        Dim literal As New LiteralControl
                        control.Parent.Controls.Add(literal)
                        literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                        literal.Text = Replace(literal.Text, "white", "black")
                        control.Parent.Controls.Remove(control)
                    End If
                End If
            End If
            Return
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            'If radCmbCompany.Value <> "" Then
            LoadAccountReceivableGrid()
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ibSendMail_Click(sender As Object, e As System.EventArgs) Handles ibSendEmail.Click
        Try
            Dim chkSelect As CheckBox
            Dim DivIDs As String = ""

            For Each dgRow As DataGridItem In dgAR.Items
                chkSelect = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)
                If chkSelect IsNot Nothing AndAlso chkSelect.Checked = True Then
                    DivIDs = DivIDs & IIf(DivIDs.Length > 0, ",", "") & CCommon.ToLong(DirectCast(dgRow.FindControl("hdnDivisionId"), HiddenField).Value)
                End If
            Next

            Session("AREmailCustomers") = DivIDs
            Session("ARStatementDate") = calTo.SelectedDate
            Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=false&SearchID=0&ID=0&SAll=true&frm=ARAging", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Private Sub chkHidePastDue_CheckedChanged(sender As Object, e As EventArgs) Handles chkHidePastDue.CheckedChanged
        Try
            'If radCmbCompany.Value <> "" Then
            LoadAccountReceivableGrid()
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub ExportDSToExcel(ByVal ds As DataSet, ByVal destination As String)
        Using workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook)
            Dim workbookPart = workbook.AddWorkbookPart()
            workbook.WorkbookPart.Workbook = New DocumentFormat.OpenXml.Spreadsheet.Workbook()
            workbook.WorkbookPart.Workbook.Sheets = New DocumentFormat.OpenXml.Spreadsheet.Sheets()
            Dim sheetId As UInteger = 1

            For Each table As DataTable In ds.Tables
                Dim sheetPart = workbook.WorkbookPart.AddNewPart(Of WorksheetPart)()
                Dim sheetData = New DocumentFormat.OpenXml.Spreadsheet.SheetData()
                sheetPart.Worksheet = New DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData)
                Dim sheets As DocumentFormat.OpenXml.Spreadsheet.Sheets = workbook.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)()
                Dim relationshipId As String = workbook.WorkbookPart.GetIdOfPart(sheetPart)

                If sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Count() > 0 Then
                    sheetId = sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().[Select](Function(s) s.SheetId.Value).Max() + 1
                End If

                Dim sheet As DocumentFormat.OpenXml.Spreadsheet.Sheet = New DocumentFormat.OpenXml.Spreadsheet.Sheet() With {
                    .Id = relationshipId,
                    .SheetId = sheetId,
                    .Name = table.TableName
                }
                sheets.Append(sheet)
                Dim headerRow As DocumentFormat.OpenXml.Spreadsheet.Row = New DocumentFormat.OpenXml.Spreadsheet.Row()
                Dim columns As List(Of String) = New List(Of String)()

                For Each column As DataColumn In table.Columns
                    columns.Add(column.ColumnName)
                    Dim cell As DocumentFormat.OpenXml.Spreadsheet.Cell = New DocumentFormat.OpenXml.Spreadsheet.Cell()
                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                    cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName)
                    headerRow.AppendChild(cell)
                Next

                sheetData.AppendChild(headerRow)

                For Each dsrow As DataRow In table.Rows
                    Dim newRow As DocumentFormat.OpenXml.Spreadsheet.Row = New DocumentFormat.OpenXml.Spreadsheet.Row()

                    For Each col As String In columns
                        Dim cell As DocumentFormat.OpenXml.Spreadsheet.Cell = New DocumentFormat.OpenXml.Spreadsheet.Cell()
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                        cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow(col).ToString())
                        newRow.AppendChild(cell)
                    Next

                    sheetData.AppendChild(newRow)
                Next
            Next
        End Using
    End Sub

    Protected Sub rbDueDate_CheckedChanged(sender As Object, e As EventArgs)
        Try
            LoadAccountReceivableGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub rbInvoiceDate_CheckedChanged(sender As Object, e As EventArgs)
        Try
            LoadAccountReceivableGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class