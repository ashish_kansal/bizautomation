﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Partial Public Class frmNewAccountType
    Inherits BACRMPage
    Dim objCOA As ChartOfAccounting
    Dim lngAcntTypeId As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngAcntTypeId = CCommon.ToLong(GetQueryStringVal("AcntTypeId"))
            If Not IsPostBack Then
                
                BindAccountType()
                If lngAcntTypeId > 0 Then
                    LoadSavedInformation()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub LoadSavedInformation()
        Try
            If objCOA Is Nothing Then objCOA = New ChartOfAccounting
            objCOA.DomainId = Session("DomainID")
            objCOA.AccountTypeID = lngAcntTypeId
            Dim ds As DataSet = objCOA.GetAccountTypes()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAccountType.Text = ds.Tables(0).Rows(0)("vcAccountType")
                ddlParentType.SelectedValue = ds.Tables(0).Rows(0)("numParentID")
                chkActive.Checked = CCommon.ToBool(ds.Tables(0).Rows(0)("bitActive"))
                ddlParentType.Enabled = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindAccountType()
        Try
            If objCOA Is Nothing Then objCOA = New ChartOfAccounting
            Dim ds As DataSet
            Dim dt As DataTable
            objCOA.DomainId = Session("DomainID")
            ds = objCOA.GetAccountTypes()
            dt = ds.Tables(1)
            ddlParentType.DataSource = ds.Tables(1)
            If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                ddlParentType.DataTextField = "vcAccountType"
            Else
                ddlParentType.DataTextField = "vcAccountType1"
            End If
            ddlParentType.DataValueField = "numAccountTypeID"
            ddlParentType.DataBind()
            'Eliminate Self Parental Account type , Bug ID 388 
            If Not ddlParentType.Items.FindByValue(lngAcntTypeId.ToString) Is Nothing Then
                ddlParentType.Items.Remove(ddlParentType.Items.FindByValue(lngAcntTypeId.ToString))
            End If

            ddlParentType.Items.Insert(0, New ListItem("-- Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objCOA Is Nothing Then objCOA = New ChartOfAccounting
            objCOA.DomainId = Session("DomainID")
            objCOA.AccountTypeName = txtAccountType.Text.Trim
            objCOA.AccountTypeCode = ""
            objCOA.ParentAccountTypeID = ddlParentType.SelectedValue
            objCOA.AccountTypeID = lngAcntTypeId
            objCOA.Mode = IIf(lngAcntTypeId > 0, 2, 1)
            objCOA.IsActive = chkActive.Checked
            objCOA.ManageAccountType()
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Refresh", " close1();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class