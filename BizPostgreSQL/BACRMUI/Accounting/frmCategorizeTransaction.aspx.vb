﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class frmCategorizeTransaction
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then


                BindCOADropdown()

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindCOADropdown()
        Try
            Dim dtChartAcnt As New DataTable
            If dtChartAcnt.Rows.Count = 0 Then
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = ""
                dtChartAcnt = objCOA.GetParentCategory()
                'Remove AR and AP values ..As those will create problem in AR aging summary report, So better not to allow to create journal entry against it
                Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")

                For index As Integer = 0 To dr.Length - 1
                    dtChartAcnt.Rows.Remove(dr(index))
                Next
                dtChartAcnt.AcceptChanges()
            End If

            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccounts.Items.Add(item)
            Next
            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim COAccountID As Long
            COAccountID = ddlAccounts.SelectedValue
            ClientScript.RegisterClientScriptBlock(Page.GetType, "SetbankID", "SetCOAccountId(" & COAccountID & ");", True)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class