﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class frmFinancialReportList
    Inherits BACRMPage

    Dim objReport As FinancialReport
   



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            'GetUserRightsForPage( 13, 18)
            'If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            '    Response.Redirect("../admin/authentication.aspx?mesg=AS")
            'Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnNew.Visible = False
            'End If
            If Not IsPostBack Then

                objReport = New FinancialReport
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            If objReport Is Nothing Then
                objReport = New FinancialReport
            End If
            objReport.DomainID = Session("DomainID")
            dgFinReport.DataSource = objReport.GetFinancialReports
            dgFinReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgFinReport_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFinReport.DeleteCommand
        Try
            objReport = New FinancialReport
            objReport.FRID = e.Item.Cells(0).Text
            objReport.DomainID = Session("DomainID")
            objReport.DeleteFinancialReport()
            objReport.FRID = 0
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgFinReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFinReport.ItemCommand
        Try
            If e.CommandName = "FRID" Then
                'If m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                Response.Redirect("frmFinancialReportWizard.aspx?FRID=" & e.Item.Cells(0).Text, False)
                'End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub dgFinReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFinReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class