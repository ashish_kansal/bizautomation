﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmChartOfAccountsDetail
    Inherits BACRMPage
    Public strChartAcntIds As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            strChartAcntIds = GetQueryStringVal("ChartAcntId")
            If Not IsPostBack Then

                LoadCOADetail()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub LoadCOADetail()
        Dim dtSummary As DataTable
        Dim objCOA As New ChartOfAccounting
        Try
            With objCOA
                .DomainID = Session("DomainID")
                '.FromDate = calFrom.SelectedDate
                .ToDate = Date.Now.UtcNow 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                .ChartAcntIds = strChartAcntIds
                dtSummary = .GetChartOfAccountsDetail()
            End With

            RepSummary.DataSource = dtSummary
            RepSummary.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Sub RepTransactionReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles RepSummary.ItemCommand
    '    Try
    '        If e.CommandName = "Edit" Then
    '            Dim lngDivisionID As String = CType(e.Item.FindControl("lblDivisionID"), Label).Text.Trim()
    '            Response.Redirect("../Accounting/frmGeneralLedger.aspx?AccountID=" & strChartAcntIds & "&DivisionID=" & lngDivisionID)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    'Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
    '    Try
    '        LoadCOADetail()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            'If GetQueryStringVal( "frm") = "chartAcnt" Then
            '    Response.Redirect("../Accounting/frmChartofAccounts.aspx")
            '    'ElseIf GetQueryStringVal( "frm") = "frmProfitLoss" Then
            '    '    Response.Redirect("../Accounting/frmProfitLoss.aspx")
            '    'ElseIf GetQueryStringVal( "frm") = "frmBalanceSheet" Then
            '    '    Response.Redirect("../Accounting/frmBalanceSheet.aspx")
            'Else
            Response.Redirect("../Accounting/frmChartofAccounts.aspx")
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class