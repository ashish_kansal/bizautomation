﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.UserInterface
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Opportunities

Public Class frmCustomerStatement
    Inherits BACRMPage

    Dim lngDivId As Long
    Dim lngContactId As Long
    Dim decTotalAmount, decAmountPaid, decBalanceDue As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivId = CCommon.ToLong(GetQueryStringVal("DivId"))
            lngContactId = CCommon.ToLong(GetQueryStringVal("ContactID"))

            If Not IsPostBack Then
                LoadAccountReceivableDetailGrid(lngContactId)
            End If

            'ibExportExcel.Attributes.Add("onclick", "return GrabHTML()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadAccountReceivableDetailGrid(Optional ByVal ContactID As Long = 0)
        Dim objVendorPayment As New VendorPayment
        Dim dtARAging As DataTable
        Dim dtARAgingInvoice As DataTable
        'dtARAging = lobjVendorPayment.GetAccountReceivableAging
        Try
            Dim mobjGeneralLedger As New GeneralLedger
            mobjGeneralLedger.DomainID = CCommon.ToLong(Session("DomainID"))
            mobjGeneralLedger.Year = CInt(Now.Year)

            objVendorPayment.DomainID = Session("DomainID")
            objVendorPayment.CompanyID = lngDivId
            objVendorPayment.UserCntID = If(ContactID > 0, ContactID, Session("UserContactID"))
            objVendorPayment.Flag = 0 'ddlRange.SelectedValue
            objVendorPayment.dtFromDate = "1990-01-01" '"mobjGeneralLedger.GetFiscalDate()"
            objVendorPayment.dtTodate = CDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)) 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            objVendorPayment.IsCustomerStatement = True
            dtARAging = objVendorPayment.GetAccountReceivableAging
            dtARAgingInvoice = objVendorPayment.GetAccountReceivableAging_Invoice
            'If (dtARAging.Rows.Count > 0) Then

            If dtARAging.Rows.Count > 0 Then
                lblCompanyNameAddress.Text = CCommon.ToString(dtARAging.Rows(0)("vcCompanyName"))

                Dim oCOpportunities As New COpportunities
                oCOpportunities.DomainID = Session("DomainID")

                Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), lngDivId)
                lblBillingAddress.Text = CCommon.ToString(dtAddress(0)("vcBillAddress"))


                Dim dtVendorAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(Session("UserDivisionID")))
                lblVendorCompanyNameAddress.Text = CCommon.ToString(dtVendorAddress.Rows(0)("vcCompanyName"))

                lblVendorBillingAddress.Text = CCommon.ToString(dtVendorAddress(0)("vcBillAddress"))

                lblStatementDate.Text = FormattedDateFromDate(Date.UtcNow, Session("DateFormat"))
                hdnContactId.Value = CCommon.ToLong(dtARAging.Rows(0)("numContactId"))
                txtConEmail.Text = CCommon.ToString(dtARAging.Rows(0)("vcEmail"))

                lblthirtyField1.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numThirtyDays"))
                lblSixtyField1.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numSixtyDays"))
                lblNintyField1.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numNinetyDays"))
                lblTwentyField1.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numOverNinetyDays"))

                lblthirtyField2.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numThirtyDaysOverDue"))
                lblSixtyField2.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numSixtyDaysOverDue"))
                lblNintyField2.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numNinetyDaysOverDue"))
                lblTwentyField2.Text = CCommon.ToDecimal(dtARAging.Rows(0)("numOverNinetyDaysOverDue"))

            End If

            decTotalAmount = 0
            decAmountPaid = 0
            decBalanceDue = 0

            If dtARAgingInvoice.Rows.Count > 0 Then

                decTotalAmount = dtARAgingInvoice.Compute("Sum(TotalAmount)", String.Empty)
                decAmountPaid = dtARAgingInvoice.Compute("Sum(AmountPaid)", String.Empty)
                decBalanceDue = dtARAgingInvoice.Compute("Sum(BalanceDue)", String.Empty)
            End If

            gvARDetails.DataSource = dtARAgingInvoice
            gvARDetails.DataBind()

            'If lngDivId > 0 Then
            'gvARDetails.Columns(0).Visible = False

            'Dim row As New GridViewRow(0, -1, DataControlRowType.Header, DataControlRowState.Normal)
            'Dim th As TableCell = New TableHeaderCell()

            'th.HorizontalAlign = HorizontalAlign.Center
            'th.ColumnSpan = gvARDetails.Columns.Count - 1
            'th.HorizontalAlign = HorizontalAlign.Center
            'th.Text = dtARAging.Rows(0)("vcCompanyName")
            'row.Cells.Add(th)

            'DirectCast(gvARDetails.Controls(0), Table).Rows.AddAt(0, row)
            'End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ibtnSendEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSendEmail.Click
        Try
            CreateHTMLBizDocs("Email", lngDivId, txtConEmail.Text, CCommon.ToLong(hdnContactId.Value), CCommon.ToLong(txtBizDocTemplate.Text))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try

    End Sub

    Sub CreateHTMLBizDocs(ByVal CommandType As String, ByVal lngDivID As Long, ByVal strConEmail As String, ByVal lngConID As Long, ByVal lngBizDocTemplate As Long)
        Try
            'Dim sw As New System.IO.StringWriter()
            'Server.Execute("frmCustomerStatement.aspx" & QueryEncryption.EncryptQueryString("DivID" & lngDivId), sw)
            Dim strHTML As String = "<body><link rel='stylesheet' href='" & Server.MapPath("~/CSS/master.css") & "' type='text/css' />" + hdnHTML.Value + "</body>"
            Dim htmlCodeToConvert As String = CCommon.ToString(strHTML)

            If CommandType = "Email" Then
                Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments

                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""
                Dim objHTMLToPDF As New HTMLToPDF
                strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)

                Dim str As String
                str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(strConEmail) & "&pqwRT=" & lngDivID & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
            ElseIf CommandType = "PDF" Then
                Dim objBizDocs As New OppBizDocs
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""

                Dim objHTMLToPDF As New HTMLToPDF
                strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                Response.Clear()
                Response.ClearContent()
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment; filename=" + strFileName)
                'Response.Write(Session("Attachements"))

                Response.WriteFile(strFilePhysicalLocation)

                Response.End()
                'ElseIf CommandType = "Print" Then
                '    Dim strPrint As String
                '    'strPrint = "win = window.open();self.focus();win.document.open();"
                '    ''strPrint += "win.document.write('" & htmlCodeToConvert & "');"

                '    'strPrint += "if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {"
                '    'strPrint += "win.print();"
                '    'strPrint += "}"
                '    'strPrint += "else {"
                '    'strPrint += "win.document.close();"
                '    'strPrint += "win.print();"
                '    'strPrint += "win.close();"
                '    'strPrint += "}"

                '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Print", strPrint, True)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvARDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvARDetails.RowDataBound
        Try
            If e.Row.RowType = ListItemType.Footer Then
                CType(e.Row.FindControl("lblTotalAmount"), Label).Text = ReturnMoney(decTotalAmount)
                'CType(e.Row.FindControl("lblAmountPaid"), Label).Text = ReturnMoney(decAmountPaid)
                CType(e.Row.FindControl("lblBalanceDue"), Label).Text = ReturnMoney(decBalanceDue)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class