''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmNewAccounts
    Inherits BACRMPage
#Region "Variables"
    Dim lobjAccounting As ChartOfAccounting
    Dim lintAccountId As Integer
    Dim lngAccountID As Long
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lintAccountId = CCommon.ToInteger(GetQueryStringVal("Name"))
            lngAccountID = CCommon.ToLong(GetQueryStringVal("AccountID"))
            'If lintAccountId = 0 Then btnTypeChange.Visible = False

            If Not IsPostBack Then

                BindAccountType()
                If lngAccountID > 0 Then
                    PopulateDropDowns()
                End If

                'objCommon.sb_FillComboFromDBwithSel(ddlType, 52, Session("DomainID")) ''Rating
                cal.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                CalDepreciationCost.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                lblDepreciation.Visible = False
                lblDepTrack.Visible = False
                pnlDepriciation.Visible = False
                'pnlLblOpen.Visible = False
                pnlRadio.Visible = False
                pnlFixedAssetDetails.Visible = False
                pnlOpenBalance.Visible = False
                ' chkActive.Checked = True 
                If lintAccountId <> 0 Then
                    LoadSavedInformation()
                Else
                    LoadAccounts(lngAccountID, ddlParentCategory.SelectedValue)
                    'Show Start Check Number Textbox when Bank Accounts is selected from Parent Category
                    tdBank.Visible = False
                End If

                If lobjAccounting Is Nothing Then lobjAccounting = New ChartOfAccounting
                Dim ds As DataSet
                lobjAccounting.DomainID = Session("DomainID")
                lobjAccounting.Mode = 2 '
                lobjAccounting.AccountTypeID = CCommon.ToLong(ddlParentCategory.SelectedValue)
                ds = lobjAccounting.GetAccountTypes()

                If ds.Tables(0).Select("vcAccountCode = '01010101'").Length > 0 Then
                    trCheckNo.Visible = True
                Else
                    trCheckNo.Visible = False
                End If

                'If ddlParentCategory.SelectedItem.Text.IndexOf("-01010101") > 0 Then
                '    trCheckNo.Visible = True
                'Else
                '    trCheckNo.Visible = False
                'End If

            End If
            btnSave.Attributes.Add("onclick", "return Save()")
            txtOpeningBalance.Attributes.Add("onkeypress", "CheckNumber(1,event)")
            txtDepreciationCost.Attributes.Add("onkeypress", "CheckNumber(1,event)")
            hrfLinkBankAccount.Attributes.Add("onclick", "OpenBankDetail(" & lintAccountId & ",'" & txtNumber.Text.Trim() & "','')")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub PopulateDropDowns()
        Try
            lobjAccounting = New ChartOfAccounting
            lobjAccounting.AccountId = lngAccountID
            lobjAccounting.DomainID = Session("DomainID")

            Dim dtAccDetail As New DataTable
            dtAccDetail = lobjAccounting.GetChartDetails()
            If dtAccDetail IsNot Nothing AndAlso dtAccDetail.Rows.Count > 0 Then
                If ddlType.Items.FindByValue(dtAccDetail.Rows(0)("CandidateKey")) IsNot Nothing Then
                    ddlType.Items.FindByValue(dtAccDetail.Rows(0)("CandidateKey")).Selected = True
                End If

                BindParentAccountType()
                If ddlParentCategory.Items.FindByValue(dtAccDetail.Rows(0)("numParntAcntTypeId")) IsNot Nothing Then
                    ddlParentCategory.Items.FindByValue(dtAccDetail.Rows(0)("numParntAcntTypeId")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EnableUpdate(ByVal isTrue As Boolean)
        Try
            ddlParentCategory.Enabled = isTrue
            ddlType.Enabled = isTrue
            txtName.Enabled = isTrue
            'txtNumber.Enabled = isTrue
            ddlParentAccount.Enabled = isTrue
            chkIsSubAccount.Enabled = isTrue

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSavedInformation()
        Try
            'ClientScript.RegisterStartupScript(Page.GetType, "Show", "ShowParentAccount(" & chkIsSubAccount.GetType.ToString() & ")", True)
            Dim dtChartDetails As DataTable
            Dim lintCount As Integer
            If lobjAccounting Is Nothing Then lobjAccounting = New ChartOfAccounting

            lobjAccounting.AccountId = lintAccountId
            lobjAccounting.DomainID = Session("DomainId")
            dtChartDetails = lobjAccounting.GetChartDetails()

            chkIsBankAccount.Checked = CCommon.ToBool(dtChartDetails.Rows(0).Item("IsBankAccount"))
            chkActive.Checked = CCommon.ToBool(dtChartDetails.Rows(0).Item("bitActive"))
            hdnIsConnected.Value = CCommon.ToBool(dtChartDetails.Rows(0).Item("IsConnected"))
            hdnBankDetailID.Value = CCommon.ToLong(dtChartDetails.Rows(0).Item("numBankDetailID"))
            If CCommon.ToLong(hdnBankDetailID.Value) > 0 Then
                chkIsBankAccount.Enabled = False
                txtNumber.Enabled = False
            Else
                chkIsBankAccount.Enabled = True
                txtNumber.Enabled = True
            End If
            'txtBankRoutingNumber.Text = CCommon.ToString(dtChartDetails.Rows(0).Item("vcBankRountingNumber"))
            txtStartCheckNumber.Text = CCommon.ToString(dtChartDetails.Rows(0).Item("vcStartingCheckNumber"))
            lblBankName.Text = CCommon.ToString(dtChartDetails.Rows(0).Item("vcBankName"))
            lblAccountNumber.Text = CCommon.ToString(dtChartDetails.Rows(0).Item("vcBankAccountNumber"))

            If CCommon.ToBool(dtChartDetails.Rows(0).Item("IsConnected")) = False Then
                lblBankBalance.Visible = False
                lblBank.Visible = False
            Else
                lblBankBalance.Visible = True
                lblBank.Visible = True
            End If
            'lblBankBalance.Text = CCommon.ToDouble(dtChartDetails.Rows(0).Item("monBankBalance"))
            'lblCompBalance.Text = CCommon.ToDouble(dtChartDetails.Rows(0).Item("monCompanyBalance"))

            If CCommon.ToDouble(dtChartDetails.Rows(0).Item("monCompanyBalance")) < 0 Then
                lblCompBalance.Text = "<font color=Red>" & ReturnMoney(CCommon.ToDouble(dtChartDetails.Rows(0).Item("monCompanyBalance"))) & "</font>"
            Else : lblCompBalance.Text = "<font color=Green>" & ReturnMoney(CCommon.ToDouble(dtChartDetails.Rows(0).Item("monCompanyBalance"))) & "</font>"
            End If

            If CCommon.ToDouble(dtChartDetails.Rows(0).Item("monCompanyBalance")) < 0 Then
                lblBankBalance.Text = "<font color=Red>" & ReturnMoney(CCommon.ToDouble(dtChartDetails.Rows(0).Item("monBankBalance"))) & "</font>"
            Else : lblBankBalance.Text = "<font color=Green>" & ReturnMoney(CCommon.ToDouble(dtChartDetails.Rows(0).Item("monBankBalance"))) & "</font>"
            End If

            txtName.Text = CCommon.ToString(dtChartDetails.Rows(0).Item("vcAccountName"))
            txtCategoryDescription.Text = dtChartDetails.Rows(0).Item("vcAccountDescription")
            If Not IsDBNull(dtChartDetails.Rows(0).Item("dtOpeningDate")) Then
                cal.SelectedDate = dtChartDetails.Rows(0).Item("dtOpeningDate")  ''IIf(IsDBNull(dtChartDetails.Rows(0).Item("dtOpeningDate")), " ", FormattedDateFromDate(dtChartDetails.Rows(0).Item("dtOpeningDate"), Session("DateFormat")))
            End If
            txtOpeningBalance.Text = ReturnMoney(dtChartDetails.Rows(0).Item("numOriginalOpeningBal")) 'changed by chintan.
            If CCommon.ToDecimal(dtChartDetails.Rows(0).Item("numOriginalOpeningBal")) < 0 Then
                txtOpeningBalance.Text = ReturnMoney(dtChartDetails.Rows(0).Item("numOriginalOpeningBal") * -1) 'changed by chintan.
                ddlBalanceType.SelectedValue = 2
            Else
                ddlBalanceType.SelectedValue = 1
            End If
            'pnlLblOpen.Visible = True
            'pnlOpenBalance.Visible = True
            'lblBalanceAmt.Text = IIf(IsDBNull(dtChartDetails.Rows(0).Item("numOpeningBal")), 0, ReturnMoney(dtChartDetails.Rows(0).Item("numOpeningBal")))

            HdnumListItemId.Value = IIf(IsDBNull(dtChartDetails.Rows(0).Item("numListItemID")), 0, dtChartDetails.Rows(0).Item("numListItemID"))
            ''ddlType.SelectedItem.Value = IIf(IsDBNull(dtChartDetails.Rows(0).Item("numAcntType")), 0, dtChartDetails.Rows(0).Item("numAcntType"))

            If Not IsDBNull(dtChartDetails.Rows(0).Item("numAcntTypeId1")) Then
                If Not ddlType.Items.FindByValue(dtChartDetails.Rows(0).Item("numAcntTypeId1")) Is Nothing Then
                    ddlType.Items.FindByValue(dtChartDetails.Rows(0).Item("numAcntTypeId1")).Selected = True
                    BindParentAccountType()
                End If
            End If

            Dim strCode As String = ddlType.SelectedValue.Split("~")(0)
            If strCode = "0105" Then
                pnlEquity.Visible = True
            Else
                pnlEquity.Visible = False
            End If

            If strCode = "0101" Or strCode = "0102" Or strCode = "0105" Then
                pnlOpenBalance.Visible = True
            Else
                pnlOpenBalance.Visible = False
            End If
            ''  ddlParentCategory.SelectedItem.Value = IIf(IsDBNull(dtChartDetails.Rows(0).Item("numParntAcntTypeId")), 0, dtChartDetails.Rows(0).Item("numParntAcntTypeId"))

            If CCommon.ToString(dtChartDetails.Rows(0).Item("vcAccountCode")).StartsWith("01010101") = True Then
                tdBank.Visible = True
                chkIsBankAccount.Visible = True
                'ClientScript.RegisterClientScriptBlock(Page.GetType, "ShowBank", "ShowBankSegment();", True)
            Else
                chkIsBankAccount.Visible = False
                tdBank.Visible = False
            End If

            txtNumber.Text = dtChartDetails.Rows(0).Item("vcNumber")
            hrfLinkBankAccount.Attributes.Add("onclick", "OpenBankDetail(" & lintAccountId & "," & txtNumber.Text.Trim() & ",'')")

            chkIsSubAccount.Checked = dtChartDetails.Rows(0).Item("bitIsSubAccount")
            If chkIsSubAccount.Checked = True Then
                ClientScript.RegisterClientScriptBlock(Page.GetType, "Show", "ShowParentAccount();", True)
                trParentAccount.Visible = True
            End If

            If Not IsDBNull(dtChartDetails.Rows(0).Item("numParntAcntTypeID")) Then
                LoadAccounts(lintAccountId, dtChartDetails.Rows(0).Item("numParntAcntTypeID"))

                If Not ddlParentCategory.Items.FindByValue(dtChartDetails.Rows(0).Item("numParntAcntTypeID")) Is Nothing Then
                    ddlParentCategory.Items.FindByValue(dtChartDetails.Rows(0).Item("numParntAcntTypeID")).Selected = True
                End If
            End If

            If dtChartDetails.Rows(0).Item("bitDepreciation") = True Then
                pnlFixedAssetDetails.Visible = True
                If ddlParentCategory.SelectedIndex > 0 Then
                    If ddlParentCategory.SelectedItem.Text.IndexOf("010102") > 0 Then
                        txtDepreciationCost.Text = ReturnMoney(dtChartDetails.Rows(0).Item("monDepreciationCost"))
                        If Not IsDBNull(dtChartDetails.Rows(0).Item("dtDepreciationCostDate")) Then
                            CalDepreciationCost.SelectedDate = dtChartDetails.Rows(0).Item("dtDepreciationCostDate")
                        End If
                    End If
                End If
            End If

            If Not IsDBNull(dtChartDetails.Rows(0).Item("numParentAccID")) Then
                If Not ddlParentAccount.Items.FindByValue(dtChartDetails.Rows(0).Item("numParentAccID")) Is Nothing Then
                    ddlParentAccount.Items.FindByValue(dtChartDetails.Rows(0).Item("numParentAccID")).Selected = True
                End If
            End If


            lobjAccounting.ParentAccountId = lintAccountId
            lintCount = lobjAccounting.GetJournalEntryCount
            If lintCount > 0 Or txtOpeningBalance.Text <> 0 Or dtChartDetails.Rows(0).Item("bitFixed") = True Then
                '' Code Commented and Added By Ajit Singh on 25-July-2008
                'ddlType.Enabled = False
                'ddlParentCategory.Enabled = False

                ddlType.Enabled = True
                ddlParentCategory.Enabled = True
                '' End of the Code.
            End If

            If CCommon.ToBool(dtChartDetails.Rows(0).Item("bitProfitLoss")) = True Then
                pnlEquity.Visible = True
                chkProfitLoss.Checked = True
                chkProfitLoss.Enabled = False
            End If

            If CCommon.ToLong(dtChartDetails.Rows(0)("ChildCount")) > 0 Then
                EnableUpdate(False)
            Else
                EnableUpdate(True)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Save() Then

                If lintAccountId <> 0 Then
                    PageRedirect()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function Save() As Boolean
        Try
            ''' Code added by Ajit Singh on 31st July 2008
            'If Not IsNothing(Cache("ChangedData")) Then
            '    For Each oAccount As ChartOfAccounting In CType(Cache("ChangedData"), ArrayList)
            '        oAccount.RenameAcntType()
            '    Next
            '    Cache.Remove("ChangedData")
            'End If
            '''End of the code.

            If lobjAccounting Is Nothing Then lobjAccounting = New ChartOfAccounting

            With lobjAccounting
                'If lintAccountId <> 0 Then
                ''  If validation() = False Then Exit Sub
                lobjAccounting.AccountId = lintAccountId
                'Else
                If validateDateFields() = False Then Exit Function
                'If txtOpeningBalance.Text <> "" Then
                ''If ddlType.SelectedItem.Value = 816 Or ddlType.SelectedItem.Value = 817 Or ddlType.SelectedItem.Value = 819 Or ddlType.SelectedItem.Value = 821 Or ddlType.SelectedItem.Value = 822 Then

                If ddlParentCategory.SelectedItem.Text.IndexOf("010102") > 0 Then
                    'If rdlFixedAsset.SelectedValue = 0 Then
                    .BitDepreciation = 1
                    .DepreciationCostDate = CalDepreciationCost.SelectedDate
                    .DepreciationCost = CCommon.ToDecimal(txtDepreciationCost.Text)
                Else : .BitDepreciation = 0
                End If
                ''ElseIf ddlType.SelectedItem.Value = 814 Or ddlType.SelectedItem.Value = 815 Or ddlType.SelectedItem.Value = 818 Or ddlType.SelectedItem.Value = 820 Then
                ''End If
                'End If

                .OpeningDate = cal.SelectedDate
                .BitFixed = 0
                'End If

                .decOpeningBalance = 0 'IIf(ddlBalanceType.SelectedValue = 1, Replace(txtOpeningBalance.Text, ",", ""), Replace(txtOpeningBalance.Text, ",", "") * -1)
                .decOriginalOpeningBal = IIf(ddlBalanceType.SelectedValue = 1, Replace(txtOpeningBalance.Text, ",", ""), Replace(txtOpeningBalance.Text, ",", "") * -1)
                lobjAccounting.AccountType = ddlType.SelectedValue.Split("~")(1)
                .CategoryDescription = txtCategoryDescription.Text
                .OpeningDate = cal.SelectedDate
                .CategoryName = txtName.Text
                .DomainID = Session("DomainId")
                .UserCntID = Session("UserContactId")
                .numListItemID = IIf(HdnumListItemId.Value = "", 0, HdnumListItemId.Value)
                .AccountTypeID = ddlType.SelectedValue.Split("~")(1)
                .ParentAccountId = ddlParentCategory.SelectedItem.Value
                .IsProfitLossAcnt = chkProfitLoss.Checked
                .strNumber = txtNumber.Text
                .IsSubAccount = If(chkIsSubAccount.Checked, 1, 0)
                .lngParentAccountId = If(chkIsSubAccount.Checked, ddlParentAccount.SelectedValue, 0)

                .IsBankAccount = chkIsBankAccount.Checked
                .StartCheckNumber = txtStartCheckNumber.Text.Trim()
                .BankRoutingNumber = "" 'txtBankRoutingNumber.Text.Trim()
                .BankDetailID = CCommon.ToLong(hdnBankDetailID.Value)
                .IsConnected = If(.BankDetailID > 0, CCommon.ToBool(hdnIsConnected.Value), False)
                .Active = CCommon.ToBool(If(chkActive.Checked, 1, 0))
                Try
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        lintAccountId = .CreateRecordAddAccountChartInfo()

                        If lintAccountId > 0 Then
                            '.UserCntID = Session("UserContactId")
                            '.DomainID = Session("DomainID")
                            '.decOriginalOpeningBal = .decOriginalOpeningBal
                            '.CategoryName = txtName.Text
                            .MakeChartOfAccountOpeningBalanceJournalEntry(lintAccountId, .OpeningDate)
                        End If

                        objTransactionScope.Complete()
                    End Using
                Catch ex As Exception
                    If ex.Message = "NoProfitLossACC" Then
                        litMessage.Text = "No Profit / Loss Account found,your option is to add account of account type Equity with profit/loss enabled and try again"
                        Return False
                    ElseIf ex.Message = "NoCurrentFinancialYearSet" Then
                        'litMessage.Text = "Please set Current Financial Year from ""Accounting->Financial Year""  to create new Chart of Account"
                        litMessage.Text = "Please set Current Financial Year from ""Accounting->Financial Year"" to save record"
                        Return False
                    ElseIf ex.Message.ToString().Contains("NOT_AVAILABLE") Then
                        litMessage.Text = "You cannot modify current account while it has child accounts available."
                        Return False
                    ElseIf ex.Message.ToString().Contains("DuplicateNumber") Then
                        litMessage.Text = "Number is already assigned to other Chart of Account,Select other number."
                        Return False
                    ElseIf ex.Message.ToString().Contains("JournalEntryExists") Then
                        litMessage.Text = "Journal Entry are exists,You are not allowed to change Account Type & Parent Account Type."
                        Return False
                    Else
                        Throw ex
                        Return False
                    End If
                End Try
                'End If

                Return True
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''Private Function validation() As Boolean
    ''    Dim lintAcntTypeId As Integer
    ''    Dim lintParentCategoryId As Integer
    ''    Dim ldtChildCategory As New DataTable
    ''    lobjAccounting.ParentAccountId = lintAccountId
    ''    lobjAccounting.DomainId = Session("DomainId")
    ''    ldtChildCategory = lobjAccounting.GetChildCategory()
    ''    lobjAccounting.AccountId = lintAccountId
    ''    lintAcntTypeId = lobjAccounting.GetAcntType()
    ''    If ldtChildCategory.Rows.Count > 0 Then
    ''        '' Response.Write("SP")
    ''        If lintAcntTypeId <> ddlType.SelectedItem.Value Then
    ''            litMessage.Text = "A parent account must have the same account type"
    ''            Return False
    ''        Else
    ''            Return True
    ''        End If
    ''    Else
    ''        Return True
    ''    End If
    ''End Function

    Private Function validateDateFields() As Boolean
        Try
            If IsNothing(cal.SelectedDate) Then
                litMessage.Text = "Select Opening Balance Date"
                Return False
            End If
            If ChartOfAccounting.GetDefaultAccount("OE", Session("DomainID")) = 0 Then
                litMessage.Text = "Please Map Default Opening Balance Equity Account for Your Company from Administration->Global Settings->Accounting->Default Accounts, then try again"
                Return False
            End If
            'If ddlType.SelectedValue.Split("~")(1) = 818 Then
            '    If rdlFixedAsset.SelectedItem.Value = 0 Then
            If pnlFixedAssetDetails.Visible = True Then
                If IsNothing(CalDepreciationCost.SelectedDate) Then
                    litMessage.Text = "Select Depreciation Date"
                    Return False
                End If
            End If
            'End If
            'End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Try
            If Not ddlType.SelectedValue = "0" Then
                'FillParentCategoryDBSel(ddlParentCategory, ddlType.SelectedItem.Value)
                BindParentAccountType()
                Dim strCode As String = ddlType.SelectedValue.Split("~")(0)
                If strCode = "0101" Or strCode = "0102" Or strCode = "0105" Then
                    pnlOpenBalance.Visible = True
                Else
                    pnlOpenBalance.Visible = False
                End If
                If strCode = "0105" Then
                    pnlEquity.Visible = True
                Else
                    pnlEquity.Visible = False
                    chkProfitLoss.Checked = False
                End If

                Dim AcntTypeId As Long = ddlType.SelectedValue.Split("~")(1)
                'If lintAccountId <> 0 Then
                '    pnlOpenBalance.Visible = True
                '    'pnlLblOpen.Visible = True
                '    lblDepreciation.Visible = False
                '    lblDepTrack.Visible = False
                'ElseIf AcntTypeId = 813 Or AcntTypeId = 816 Then
                '    'pnlLblOpen.Visible = False
                '    pnlOpenBalance.Visible = True
                '    pnlRadio.Visible = False
                '    pnlFixedAssetDetails.Visible = False
                '    lblDepreciation.Visible = False
                '    lblDepTrack.Visible = False
                'ElseIf AcntTypeId = 818 Then
                '    lblDepreciation.Visible = True
                '    pnlRadio.Visible = True
                '    'pnlLblOpen.Visible = False
                '    pnlOpenBalance.Visible = True
                '    pnlFixedAssetDetails.Visible = True
                '    lblDepreciation.Visible = True
                '    lblDepTrack.Visible = True
                '    'Else
                '    'lblDepreciation.Visible = False
                '    'pnlOpenBalance.Visible = False
                '    'pnlLblOpen.Visible = False
                '    'pnlRadio.Visible = False
                '    'pnlFixedAssetDetails.Visible = False
                '    'lblDepreciation.Visible = False
                '    'lblDepTrack.Visible = False
                'End If
            Else
                lblDepreciation.Visible = False
                pnlOpenBalance.Visible = False
                'pnlLblOpen.Visible = False
                pnlRadio.Visible = False
                pnlFixedAssetDetails.Visible = False
                lblDepreciation.Visible = False
                lblDepTrack.Visible = False
                pnlDepriciation.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Public Sub FillParentCategoryDBSel(ByRef objCombo As DropDownList, ByVal lngAcntTypeId As Long)
    '    Try
    '        If lobjAccounting Is Nothing Then lobjAccounting = New ChartOfAccounting
    '        lobjAccounting.AccountType = lngAcntTypeId
    '        lobjAccounting.AccountId = lintAccountId
    '        lobjAccounting.DomainId = Session("DomainId")
    '        objCombo.DataSource = lobjAccounting.GetParentCategoryForSelType()
    '        objCombo.DataTextField = "vcCatgyName"
    '        objCombo.DataValueField = "numAccountId"
    '        objCombo.DataBind()
    '        objCombo.Items.Insert(0, "--Select One--")
    '        objCombo.Items.FindByText("--Select One--").Value = "0"
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''Sub FillParentCategoryDBSel(ByRef objCombo As DropDownList)
    ''    '' lobjAccounting.AccountType = lngAcntTypeId
    ''    lobjAccounting.AccountId = lintAccountId
    ''    objCombo.DataSource = lobjAccounting.GetParentCategoryForSelType()
    ''    objCombo.DataTextField = "vcCatgyName"
    ''    objCombo.DataValueField = "numAccountId"
    ''    objCombo.DataBind()
    ''    objCombo.Items.Insert(0, "--Select One--")do
    ''    objCombo.Items.FindByText("--Select One--").Value = "0"
    ''End Sub

    Sub PageRedirect()
        Try
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Refresh", "window.opener.location.href = window.opener.location.href; self.close();", True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Cache.Remove("ChangedData")
            Response.Write("<script>window.opener.location.href = window.opener.location.href; self.close();</script>")
            'Response.Write("<script>self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub btnTypeChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTypeChange.Click
    '    Try
    '        Dim sddlValue As String = ddlType.SelectedValue.Split("~")(1)
    '        Dim sddlText As String = ddlType.SelectedItem.Text
    '        Dim objCommon As CCommon
    '        Dim bool As Boolean = False

    '        If IsNothing(ViewState("btnType")) Then ViewState("btnType") = "Change Type"

    '        If (ViewState("btnType").ToString() = "Change Type") Then
    '            txtType.Visible = True
    '            ViewState("btnType") = "Save Type"
    '            btnTypeChange.Text = ViewState("btnType").ToString()
    '            txtType.Text = sddlText
    '        ElseIf (ViewState("btnType").ToString() = "Save Type") Then
    '            ViewState("btnType") = "Change Type"
    '            Dim oAccounting As New ChartOfAccounting()
    '            oAccounting.DomainId = Convert.ToInt32(Session("DomainId").ToString())
    '            oAccounting.AccountType = Convert.ToInt32(sddlValue)
    '            oAccounting.CategoryName = txtType.Text.Trim()
    '            oAccounting.RenameAcntType()

    '            txtType.Visible = False
    '            btnTypeChange.Text = ViewState("btnType").ToString()
    '            ddlType.Items.FindByValue(sddlValue).Text = txtType.Text.Trim()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub BindAccountType()
        Try
            If lobjAccounting Is Nothing Then lobjAccounting = New ChartOfAccounting
            Dim ds As DataSet
            lobjAccounting.DomainID = Session("DomainID")
            lobjAccounting.Mode = 1 'get all account type with code length = 4
            ds = lobjAccounting.GetAccountTypes()
            ddlType.DataSource = ds.Tables(0)
            If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                ddlType.DataTextField = "vcAccountType"
            Else
                ddlType.DataTextField = "vcAccountType1"
            End If
            ddlType.DataValueField = "CandidateKey"
            ddlType.DataBind()
            ddlType.Items.Insert(0, New ListItem("-- Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindParentAccountType()
        Try
            If lobjAccounting Is Nothing Then lobjAccounting = New ChartOfAccounting
            Dim ds As DataSet
            lobjAccounting.DomainID = Session("DomainID")
            lobjAccounting.Mode = 2 '
            lobjAccounting.AccountTypeID = ddlType.SelectedValue.Split("~")(1)
            ds = lobjAccounting.GetAccountTypes()
            ddlParentCategory.DataSource = ds.Tables(0)
            If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
                ddlParentCategory.DataTextField = "vcAccountType"
            Else
                ddlParentCategory.DataTextField = "vcAccountType1"
            End If
            ddlParentCategory.DataValueField = "numAccountTypeID"
            ddlParentCategory.DataBind()
            ddlParentCategory.Items.Insert(0, New ListItem("-- Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadAccounts(ByVal intAccountId As Long, ByVal intParntAcntTypeId As Long)
        Try
            If intParntAcntTypeId > 0 Then
                Dim objCOA As New ChartOfAccounting
                Dim item As ListItem
                Dim dtChartAcnt As New DataTable

                objCOA.DomainID = Session("DomainId")
                objCOA.AccountId = intAccountId
                objCOA.ParentAccountTypeID = intParntAcntTypeId
                'objCOA.AccountCode = ""
                'dtChartAcnt = objCOA.GetParentCategory()
                dtChartAcnt = objCOA.GetAccountsForDropDown()

                ddlParentAccount.Items.Clear()
                ddlParentAccount.DataSource = dtChartAcnt
                ddlParentAccount.DataTextField = "vcAccountName"
                ddlParentAccount.DataValueField = "numAccountId"
                ddlParentAccount.DataBind()
                ddlParentAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlParentCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlParentCategory.SelectedIndexChanged
        Try
            Dim strCode As String = ddlType.SelectedValue.Split("~")(0)
            If strCode = "0101" And ddlParentCategory.SelectedItem.Text.IndexOf("-010102") > 0 Then 'fixed assets
                pnlFixedAssetDetails.Visible = True
            Else
                pnlFixedAssetDetails.Visible = False
            End If

            'Show Start Check Number Textbox when Bank Accounts is selected from Parent Category
            If ddlParentCategory.SelectedItem.Text.IndexOf("-01010101") > 0 Then
                trCheckNo.Visible = True
            Else
                trCheckNo.Visible = False
            End If

            LoadAccounts(0, ddlParentCategory.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDeleteBankAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteBankAccount.Click
        Try
            If hdnBankDetailID.Value IsNot Nothing AndAlso CCommon.ToLong(hdnBankDetailID.Value) > 0 Then
                Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.BankDetailID = CCommon.ToLong(hdnBankDetailID.Value)
                If objEBanking.DeleteBankDetailConnection() = True Then
                    litMessage.Text = "Bank connection deleted successfully."
                Else
                    litMessage.Text = "Error occurred while deleting Bank connection."
                End If

                LoadSavedInformation()
                chkIsBankAccount.Enabled = True
                txtNumber.Enabled = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnHiddenSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHiddenSave.Click
        Try
            If Save() Then
                LoadSavedInformation()
            End If
            'chkIsBankAccount.Enabled = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class