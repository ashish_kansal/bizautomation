﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOLAPReports.aspx.vb"
    Inherits=".frmOLAPReports" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v10.2, Version=10.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxPivotGrid ID="ASPxPivotGrid1" runat="server" ClientIDMode="AutoID" OLAPConnectionString="provider=MSOLAP;data source=BIZAUTO;initial catalog=KPI;cube name=&quot;AWDW Cube&quot;">
            <Fields>
                <dx:PivotGridField ID="fieldClass1" AreaIndex="0" 
                    FieldName="[Dim Product].[Class].[Class]" Caption="Class">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldColor1" AreaIndex="1" 
                    FieldName="[Dim Product].[Color].[Color]" Caption="Color">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDimProduct1" AreaIndex="2" 
                    FieldName="[Dim Product].[Dim Product].[Dim Product]" Caption="Dim Product">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishProductName1" AreaIndex="3" 
                    FieldName="[Dim Product].[English Product Name].[English Product Name]" 
                    Caption="English Product Name">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldStatus1" AreaIndex="4" 
                    FieldName="[Dim Product].[Hierarchy].[Status]" Caption="Status" GroupIndex="0" 
                    InnerGroupIndex="0">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldClass2" AreaIndex="5" 
                    FieldName="[Dim Product].[Hierarchy].[Class]" Caption="Class" GroupIndex="0" 
                    InnerGroupIndex="1">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldColor2" AreaIndex="6" 
                    FieldName="[Dim Product].[Hierarchy].[Color]" Caption="Color" GroupIndex="0" 
                    InnerGroupIndex="2">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishProductName2" AreaIndex="7" 
                    FieldName="[Dim Product].[Hierarchy].[English Product Name]" 
                    Caption="English Product Name" GroupIndex="0" InnerGroupIndex="3">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldStatus2" AreaIndex="8" 
                    FieldName="[Dim Product].[Status].[Status]" Caption="Status">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarQuarter1" AreaIndex="9" 
                    Caption="Calendar Quarter" 
                    FieldName="[Due Date].[Calendar Quarter].[Calendar Quarter]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarYear1" AreaIndex="10" 
                    Caption="Calendar Year" 
                    FieldName="[Due Date].[Calendar Quarter - Month Number Of Year].[Calendar Year]" 
                    GroupIndex="1" InnerGroupIndex="0">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarQuarter2" AreaIndex="11" 
                    Caption="Calendar Quarter" 
                    FieldName="[Due Date].[Calendar Quarter - Month Number Of Year].[Calendar Quarter]" 
                    GroupIndex="1" InnerGroupIndex="1">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishMonthName1" AreaIndex="12" 
                    Caption="English Month Name" 
                    FieldName="[Due Date].[Calendar Quarter - Month Number Of Year].[English Month Name]" 
                    GroupIndex="1" InnerGroupIndex="2">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarSemester1" AreaIndex="13" 
                    Caption="Calendar Semester" 
                    FieldName="[Due Date].[Calendar Semester].[Calendar Semester]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarYear2" AreaIndex="14" 
                    Caption="Calendar Year" FieldName="[Due Date].[Calendar Year].[Calendar Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfMonth1" AreaIndex="15" 
                    Caption="Day Number Of Month" 
                    FieldName="[Due Date].[Day Number Of Month].[Day Number Of Month]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfWeek1" AreaIndex="16" 
                    Caption="Day Number Of Week" 
                    FieldName="[Due Date].[Day Number Of Week].[Day Number Of Week]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfYear1" AreaIndex="17" 
                    Caption="Day Number Of Year" 
                    FieldName="[Due Date].[Day Number Of Year].[Day Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDimTime1" AreaIndex="18" Caption="Dim Time" 
                    FieldName="[Due Date].[Dim Time].[Dim Time]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishMonthName2" AreaIndex="19" 
                    Caption="English Month Name" 
                    FieldName="[Due Date].[English Month Name].[English Month Name]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldFullDateAlternateKey1" AreaIndex="20" 
                    Caption="Full Date Alternate Key" 
                    FieldName="[Due Date].[Full Date Alternate Key].[Full Date Alternate Key]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldMonthNumberOfYear1" AreaIndex="21" 
                    Caption="Month Number Of Year" 
                    FieldName="[Due Date].[Month Number Of Year].[Month Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldWeekNumberOfYear1" AreaIndex="22" 
                    Caption="Week Number Of Year" 
                    FieldName="[Due Date].[Week Number Of Year].[Week Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarQuarter3" AreaIndex="23" 
                    Caption="Calendar Quarter" 
                    FieldName="[Order Date].[Calendar Quarter].[Calendar Quarter]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarYear3" AreaIndex="24" 
                    Caption="Calendar Year" 
                    FieldName="[Order Date].[Calendar Quarter - Month Number Of Year].[Calendar Year]" 
                    GroupIndex="2" InnerGroupIndex="0">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarQuarter4" AreaIndex="25" 
                    Caption="Calendar Quarter" 
                    FieldName="[Order Date].[Calendar Quarter - Month Number Of Year].[Calendar Quarter]" 
                    GroupIndex="2" InnerGroupIndex="1">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishMonthName3" AreaIndex="26" 
                    Caption="English Month Name" 
                    FieldName="[Order Date].[Calendar Quarter - Month Number Of Year].[English Month Name]" 
                    GroupIndex="2" InnerGroupIndex="2">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarSemester2" AreaIndex="27" 
                    Caption="Calendar Semester" 
                    FieldName="[Order Date].[Calendar Semester].[Calendar Semester]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarYear4" AreaIndex="28" 
                    Caption="Calendar Year" 
                    FieldName="[Order Date].[Calendar Year].[Calendar Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfMonth2" AreaIndex="29" 
                    Caption="Day Number Of Month" 
                    FieldName="[Order Date].[Day Number Of Month].[Day Number Of Month]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfWeek2" AreaIndex="30" 
                    Caption="Day Number Of Week" 
                    FieldName="[Order Date].[Day Number Of Week].[Day Number Of Week]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfYear2" AreaIndex="31" 
                    Caption="Day Number Of Year" 
                    FieldName="[Order Date].[Day Number Of Year].[Day Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDimTime2" AreaIndex="32" Caption="Dim Time" 
                    FieldName="[Order Date].[Dim Time].[Dim Time]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishMonthName4" AreaIndex="33" 
                    Caption="English Month Name" 
                    FieldName="[Order Date].[English Month Name].[English Month Name]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldFullDateAlternateKey2" AreaIndex="34" 
                    Caption="Full Date Alternate Key" 
                    FieldName="[Order Date].[Full Date Alternate Key].[Full Date Alternate Key]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldMonthNumberOfYear2" AreaIndex="35" 
                    Caption="Month Number Of Year" 
                    FieldName="[Order Date].[Month Number Of Year].[Month Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldWeekNumberOfYear2" AreaIndex="36" 
                    Caption="Week Number Of Year" 
                    FieldName="[Order Date].[Week Number Of Year].[Week Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarQuarter5" AreaIndex="37" 
                    Caption="Calendar Quarter" 
                    FieldName="[Ship Date].[Calendar Quarter].[Calendar Quarter]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarYear5" AreaIndex="38" 
                    Caption="Calendar Year" 
                    FieldName="[Ship Date].[Calendar Quarter - Month Number Of Year].[Calendar Year]" 
                    GroupIndex="3" InnerGroupIndex="0">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarQuarter6" AreaIndex="39" 
                    Caption="Calendar Quarter" 
                    FieldName="[Ship Date].[Calendar Quarter - Month Number Of Year].[Calendar Quarter]" 
                    GroupIndex="3" InnerGroupIndex="1">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishMonthName5" AreaIndex="40" 
                    Caption="English Month Name" 
                    FieldName="[Ship Date].[Calendar Quarter - Month Number Of Year].[English Month Name]" 
                    GroupIndex="3" InnerGroupIndex="2">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarSemester3" AreaIndex="41" 
                    Caption="Calendar Semester" 
                    FieldName="[Ship Date].[Calendar Semester].[Calendar Semester]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldCalendarYear6" AreaIndex="42" 
                    Caption="Calendar Year" FieldName="[Ship Date].[Calendar Year].[Calendar Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfMonth3" AreaIndex="43" 
                    Caption="Day Number Of Month" 
                    FieldName="[Ship Date].[Day Number Of Month].[Day Number Of Month]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfWeek3" AreaIndex="44" 
                    Caption="Day Number Of Week" 
                    FieldName="[Ship Date].[Day Number Of Week].[Day Number Of Week]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDayNumberOfYear3" AreaIndex="45" 
                    Caption="Day Number Of Year" 
                    FieldName="[Ship Date].[Day Number Of Year].[Day Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDimTime3" AreaIndex="46" Caption="Dim Time" 
                    FieldName="[Ship Date].[Dim Time].[Dim Time]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldEnglishMonthName6" AreaIndex="47" 
                    Caption="English Month Name" 
                    FieldName="[Ship Date].[English Month Name].[English Month Name]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldFullDateAlternateKey3" AreaIndex="48" 
                    Caption="Full Date Alternate Key" 
                    FieldName="[Ship Date].[Full Date Alternate Key].[Full Date Alternate Key]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldMonthNumberOfYear3" AreaIndex="49" 
                    Caption="Month Number Of Year" 
                    FieldName="[Ship Date].[Month Number Of Year].[Month Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldWeekNumberOfYear3" AreaIndex="50" 
                    Caption="Week Number Of Year" 
                    FieldName="[Ship Date].[Week Number Of Year].[Week Number Of Year]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldRevisionNumber1" Area="DataArea" AreaIndex="0" 
                    Caption="Revision Number" FieldName="[Measures].[Revision Number]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldOrderQuantity1" Area="DataArea" AreaIndex="1" 
                    Caption="Order Quantity" FieldName="[Measures].[Order Quantity]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldUnitPrice1" Area="DataArea" AreaIndex="2" 
                    Caption="Unit Price" FieldName="[Measures].[Unit Price]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldExtendedAmount1" Area="DataArea" AreaIndex="3" 
                    Caption="Extended Amount" FieldName="[Measures].[Extended Amount]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldUnitPriceDiscountPct1" Area="DataArea" 
                    AreaIndex="4" Caption="Unit Price Discount Pct" 
                    FieldName="[Measures].[Unit Price Discount Pct]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldDiscountAmount1" Area="DataArea" AreaIndex="5" 
                    Caption="Discount Amount" FieldName="[Measures].[Discount Amount]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldProductStandardCost1" Area="DataArea" AreaIndex="6" 
                    Caption="Product Standard Cost" FieldName="[Measures].[Product Standard Cost]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldTotalProductCost1" Area="DataArea" AreaIndex="7" 
                    Caption="Total Product Cost" FieldName="[Measures].[Total Product Cost]">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldSalesAmount1" Area="DataArea" AreaIndex="8" 
                    Caption="Sales Amount" FieldName="[Measures].[Sales Amount]">
                </dx:PivotGridField>
            </Fields>
            <FieldValueTemplate>
            </FieldValueTemplate>
            <Groups>
                <dx:PivotGridWebGroup Caption="Hierarchy" 
                    Hierarchy="[Dim Product].[Hierarchy]" />
                <dx:PivotGridWebGroup Caption="Due Date.Calendar Quarter - Month Number Of Year" 
                    Hierarchy="[Due Date].[Calendar Quarter - Month Number Of Year]" />
                <dx:PivotGridWebGroup Caption="Order Date.Calendar Quarter - Month Number Of Year" 
                    Hierarchy="[Order Date].[Calendar Quarter - Month Number Of Year]" />
                <dx:PivotGridWebGroup Caption="Ship Date.Calendar Quarter - Month Number Of Year" 
                    Hierarchy="[Ship Date].[Calendar Quarter - Month Number Of Year]" />
            </Groups>
        </dx:ASPxPivotGrid>
    </div>
    </form>
</body>
</html>
