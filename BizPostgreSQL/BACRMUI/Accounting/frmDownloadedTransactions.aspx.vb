﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI

Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts

Imports System.Text
Imports System.Collections.Generic

Imports System.Web.Services
Imports System.Web.Script.Serialization
Imports BACRM.BusinessLogic.Workflow

Public Class DownloadedTransactions
    Inherits BACRMPage

    Dim dtChartAcnt As DataTable
    Dim lintJournalId As Integer
    Dim objJournalEntry As JournalEntry

    Dim lngJournalID As Long
    Dim lngDepositeID As Long
    Dim lngChartAcntId As Long

    Dim objMakeDeposit As MakeDeposit
    Dim objChecks As Checks

    Dim dtItems As New DataTable
    Dim dtCOAItemDetail As DataTable

    Dim blnfiredPagingAcc As Boolean = False
    Dim blnfiredPagingUnAcc As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                pnlNoBankAccounts.Visible = False
                radTransactions.SelectedIndex = 0
                radTransactions.MultiPage.FindPageViewByID(radTransactions.SelectedTab.PageViewID).Selected = True
                BindDropDowns()

                GetCOAccountsListToLoadDropDown()
               
                If GetQueryStringVal("COAccountID") <> "" Then
                    Dim COAccountID As Long = CCommon.ToLong(GetQueryStringVal("COAccountID"))
                    If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(COAccountID)) Is Nothing Then
                        radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(COAccountID)).Selected = True
                        BindTransactions(COAccountID)
                        BindAcceptedTransactions(COAccountID)
                    End If
                Else
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))) Is Nothing Then
                            radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))).Selected = True
                            BindTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                            BindAcceptedTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                        End If
                    End If

                End If
                radcmbCOAccounts.Attributes.Add("onselectedindexchanged", "FillDropDowns();")

            End If

            'If radTransactions.SelectedTab.PageViewID = "radPageView_UnacceptedTransactions" Then
            '    ClientScript.RegisterClientScriptBlock(Me.GetType, "Paging3", "DoPaging();", True)
            '    'ClientScript.RegisterClientScriptBlock(Me.GetType, "Show1", "ShowHide(0);", True)
            'ElseIf radTransactions.SelectedTab.PageViewID = "radPageView_AcceptedTransactions" Then
            '    ClientScript.RegisterClientScriptBlock(Me.GetType, "Paging6", "DoPaging();", True)
            '    'ClientScript.RegisterClientScriptBlock(Me.GetType, "Show2", "ShowHide(1);", True)

            'End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindTransactions(ByVal COAId As Long)
        Try
            Dim dtTransactions As New DataTable
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainId")
            objEBanking.AccountID = COAId
            objEBanking.TotRecs = 0
            objEBanking.PageSize = CCommon.ToInteger(Session("PagingRows"))
            If txtCurrentPageCorr.Text.Trim = "" Then
                txtCurrentPageCorr.Text = 1
                'Else
                '    txtCurrentPageCorr.Text = txtCurrentPageCorr1.Text
            End If

            objEBanking.CurrentPage = CCommon.ToInteger(txtCurrentPageCorr.Text)

            dtTransactions = objEBanking.GetAllTransactionsOfCOAId()
            'LoadChartAcnt()
            'If dtTransactions.Rows.Count > 0 Then
            gvReconcile.Visible = True
            gvReconcile.DataSource = dtTransactions


            'If Cache.Item("__dtTransactions_Cache_Key") Is Nothing Then
            '    Cache.Insert("__dtTransactions_Cache_Key", dtTransactions)
            'End If

            Cache.Insert("__dtTransactions_Cache_Key", dtTransactions)

            bizPager.PageSize = CCommon.ToInteger(Session("PagingRows"))
            bizPager.RecordCount = objEBanking.TotRecs
            bizPager.CurrentPageIndex = txtCurrentPageCorr.Text

            gvReconcile.DataBind()

            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindAcceptedTransactions(ByVal COAId As Long)
        Try
            Dim dtTransactions As New DataTable
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainId")
            objEBanking.AccountID = COAId
            objEBanking.TotRecs = 0

            objEBanking.PageSize = CCommon.ToInteger(Session("PagingRows"))
            If txtCurrentPageCorr1.Text.Trim = "" Then txtCurrentPageCorr1.Text = 1
            objEBanking.CurrentPage = CCommon.ToInteger(txtCurrentPageCorr1.Text)


            dtTransactions = objEBanking.GetMatchedTransactionsOfCOAId()

            gvAcceptedTrans.Visible = True
            gvAcceptedTrans.DataSource = dtTransactions

            bizPager1.PageSize = CCommon.ToInteger(Session("PagingRows"))
            bizPager1.RecordCount = objEBanking.TotRecs
            bizPager1.CurrentPageIndex = txtCurrentPageCorr1.Text

            gvAcceptedTrans.DataBind()

            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnHiddenSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHiddenSave.Click
        Try
            PersistTable.Load()
            'If PersistTable.Count > 0 Then
            '    If Not ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))) Is Nothing Then
            '        ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))).Selected = True
            '        BindTransactions(CCommon.ToLong(PersistTable(ddlCOAccounts.ID)))
            '        BindAcceptedTransactions(CCommon.ToLong(PersistTable(ddlCOAccounts.ID)))
            '    End If
            'End If
            If PersistTable.Count > 0 Then
                If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))) Is Nothing Then
                    radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))).Selected = True
                    BindTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                    BindAcceptedTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub BindTransactionsFromCache(ByVal TransactionId As Long)
        Try
            If Not Cache("__dtTransactions_Cache_Key") Is Nothing Then
                Dim dt As DataTable = DirectCast(Cache("__dtTransactions_Cache_Key"), DataTable)
                Dim dtTransactionCopy As DataTable
                dtTransactionCopy = dt.Copy()
                For i As Integer = 0 To dtTransactionCopy.Rows.Count - 1
                    If dtTransactionCopy.Rows(i)("numTransactionId") = TransactionId Then
                        dt.Rows.RemoveAt(i)

                    End If
                Next

                gvReconcile.DataSource = dt
                gvReconcile.DataBind()

                Cache.Insert("__dtTransactions_Cache_Key", dt)

            Else
                If CCommon.ToLong(hdnFldCOAccountId.Value) > 0 Then
                    BindTransactions(CCommon.ToLong(hdnFldCOAccountId.Value))
                End If
            End If

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub btnHiddenCategorizeSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnHiddenCategorizeSelect.Click
        Try
            If CCommon.ToLong(hdnFldCategorizeAccId.Value) > 0 Then

                For Each gvRow As GridViewRow In gvReconcile.Rows
                    Dim chkSelect As CheckBox = CType(gvRow.FindControl("chk"), CheckBox)
                    Dim ddlAccounts As DropDownList = CType(gvRow.FindControl("ddlAccounts"), DropDownList)

                    If chkSelect.Checked = True Then
                        If Not ddlAccounts.Items.FindByValue(CCommon.ToLong(hdnFldCategorizeAccId.Value)) Is Nothing Then
                            ddlAccounts.Items.FindByValue(CCommon.ToLong(hdnFldCategorizeAccId.Value)).Selected = True
                        End If
                    End If

                Next

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub btnExclude_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExclude.Click
        Try
            ' Dim gvRow As GridViewRow
            Dim TransactionId As String = 0
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
            objEBanking.DomainID = Session("DomainID")

            For Each gvRow As GridViewRow In gvReconcile.Rows
                Dim chkSelected As CheckBox = CType(gvRow.FindControl("chk"), CheckBox)
                If Not IsNothing(chkSelected) Then
                    If chkSelected.Checked = True Then
                        TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                        objEBanking.TransactionID = TransactionId
                        objEBanking.ExcludeBankStatementTransactions()

                    End If

                End If

            Next

            BindTransactionsFromCache(TransactionId)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
            'PersistTable.Load()
            ''If PersistTable.Count > 0 Then
            ''    If Not ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))) Is Nothing Then
            ''        ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))).Selected = True
            ''        BindTransactions(CCommon.ToLong(PersistTable(ddlCOAccounts.ID)))
            ''        BindAcceptedTransactions(CCommon.ToLong(PersistTable(ddlCOAccounts.ID)))
            ''    End If
            ''End If
            'If PersistTable.Count > 0 Then
            '    If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))) Is Nothing Then
            '        radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))).Selected = True
            '        BindTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
            '        BindAcceptedTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
            '    End If
            'End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
        End Try

    End Sub

    Private Sub AcceptTransaction(ByVal RowIndex As Integer)
        Try
            Dim strMessage As String = ""
            Dim objWriteCheck As Checks
            Dim gvRow As GridViewRow
            gvRow = gvReconcile.Rows(RowIndex)
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dtBillDetails As New DataTable

            Dim TransactionId As Long = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
            Dim COAccountId As Integer = radcmbCOAccounts.SelectedValue
            Dim COAccountName As String = radcmbCOAccounts.SelectedItem.Text
            Dim rdbAssign As RadioButton = CType(gvRow.FindControl("rdbAssign"), RadioButton)
            Dim rdbMatch As RadioButton = CType(gvRow.FindControl("rdbMatch"), RadioButton)

            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
            objEBanking.DomainID = Session("DomainID")
            objEBanking.TransactionID = TransactionId
            objEBanking.TransMappingId = 0
            objEBanking.IsActive = 1

            If Not rdbAssign Is Nothing AndAlso Not rdbMatch Is Nothing Then

                If rdbAssign.Checked = True Then
                    Dim Amount As Decimal = CCommon.ToDecimal(CType(gvRow.FindControl("lblAmount"), Label).Text)
                    Dim EntryDate As DateTime = CType((CType(gvRow.FindControl("lblDatePosted"), Label).Text), DateTime)
                    Dim Memo As String = CType(gvRow.FindControl("lblPayeeName"), Label).Text & " " & CType(gvRow.FindControl("lblMemo"), Label).Text

                    Dim ReceivedFrom As Long

                    If Not CCommon.ToLong(CType(gvRow.FindControl("radCmbCompany"), RadComboBox).SelectedValue) > 0 Then
                        Dim strCustomerName As String = CCommon.ToString(CType(gvRow.FindControl("radCmbCompany"), RadComboBox).Text)
                        ReceivedFrom = RegisterAsNewCustomer(strCustomerName, Amount)
                    Else
                        ReceivedFrom = CCommon.ToLong(CType(gvRow.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    End If

                    Dim TransID As Long = 0
                    Dim AccountID As Long = 0
                    Dim ProjectID As Long = 0
                    Dim ClassID As Long = 0
                    Dim AcceptedTrans As String
                  
                    AcceptedTrans = hdnFldAcceptedTrans.Value

                    Dim rowValue As String()
                    rowValue = AcceptedTrans.Split("|")

                    For Each ColValue As String In rowValue
                        Dim d As String() = ColValue.Split(",")
                        If d.Length > 1 Then
                            TransID = CCommon.ToLong(d(0))

                            If TransactionId = TransID Then
                                AccountID = CCommon.ToLong(d(1))
                                ProjectID = CCommon.ToLong(d(2))
                                ClassID = CCommon.ToLong(d(3))

                            End If
                        End If
                    Next

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        If Amount > 0 Then

                            objMakeDeposit = New MakeDeposit
                            objMakeDeposit.DomainID = Session("DomainID")
                            objMakeDeposit.UserCntID = Session("UserContactID")

                            CCommon.AddColumnsToDataTable(dt, "numDepositeDetailID,numChildDepositID,monAmountPaid,numPaymentMethod,vcMemo,vcReference,numAccountID,numClassID,numProjectID,numReceivedFrom")
                            Dim Description As String = "Deposit: " & COAccountName
                            Dim dr As DataRow = dt.NewRow
                            dr("numDepositeDetailID") = 0
                            dr("numChildDepositID") = 0
                            dr("monAmountPaid") = ReturnMoney(Amount)
                            dr("numPaymentMethod") = CCommon.ToString(4)
                            dr("vcMemo") = CType(gvRow.FindControl("lblPayeeName"), Label).Text & " " & CType(gvRow.FindControl("lblMemo"), Label).Text
                            dr("vcReference") = CType(gvRow.FindControl("lblFITransactionID"), Label).Text
                            dr("numAccountID") = AccountID
                            dr("numClassID") = ProjectID
                            dr("numProjectID") = ClassID

                            'dr("numAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlAccounts"), DropDownList).SelectedValue)
                            'dr("numClassID") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                            'dr("numProjectID") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)

                            dr("numReceivedFrom") = ReceivedFrom

                            dt.Rows.Add(dr)
                            ds.Tables.Add(dt.Copy)
                            ds.Tables(0).TableName = "Item"
                            Dim strItems As String = ds.GetXml()
                            lngDepositeID = objMakeDeposit.SaveDeposite(EntryDate, COAccountId, Amount, strItems)
                            'lngJournalID = SaveDataToHeader(lngDepositeID, 0, Amount, COAccountId, COAccountName)
                            lngJournalID = objMakeDeposit.SaveDataToHeader(EntryDate, Description, Amount, COAccountId, COAccountName, lngDepositeID)

                            objMakeDeposit.SaveDataToGeneralJournalDetails(lngDepositeID, lngJournalID, COAccountId, COAccountName)

                            'Save the Transaction to BizDatabase and Add Reference Type and Reference ID to BankStatementTransaction
                            objEBanking.ReferenceID = lngDepositeID
                            objEBanking.ReferenceType = enmReferenceType.DepositHeader
                            objEBanking.MapBankStatementTransactions()
                        Else
                            objWriteCheck = New Checks
                            objWriteCheck.DomainID = Session("DomainID")
                            objWriteCheck.UserCntID = Session("UserContactID")


                            Dim Description As String = "Write Check: " & CType(gvRow.FindControl("radCmbCompany"), RadComboBox).Text
                            Amount = Decimal.Negate(Amount)
                            Dim IsPrint As Boolean = False
                            Dim CheckNo As String = CCommon.ToLong(CType(gvRow.FindControl("lblCheckNumber"), Label).Text)

                            CCommon.AddColumnsToDataTable(dt, "numCheckDetailID,numChartAcntId,monAmount,numProjectID,numClassID,vcDescription,numCustomerId,numTransactionId")
                            Dim dr As DataRow = dt.NewRow
                            dr("numCheckDetailID") = 0
                            dr("numChartAcntId") = AccountID
                            dr("monAmount") = Amount
                            dr("numProjectId") = ClassID
                            dr("numClassId") = ProjectID

                            'dr("numChartAcntId") = CCommon.ToLong(CType(gvRow.FindControl("ddlAccounts"), DropDownList).SelectedValue)
                            'dr("numClassId") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                            'dr("numProjectId") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)

                            dr("vcDescription") = CType(gvRow.FindControl("lblFITransactionID"), Label).Text
                            dr("numCustomerId") = ReceivedFrom
                            dr("numTransactionId") = "0"
                            dt.Rows.Add(dr)
                            ds.Tables.Add(dt.Copy)
                            ds.Tables(0).TableName = "Table1"
                            Dim strItems As String = ds.GetXml()

                            Dim CheckHeaderID As Long = objWriteCheck.SaveCheckHeader(COAccountId, ReceivedFrom, Amount, EntryDate, CheckNo, Memo, IsPrint, strItems)

                            lngJournalID = objWriteCheck.SaveCheckDataToHeader(EntryDate, Description, Amount, COAccountId, CheckHeaderID)
                            objWriteCheck.SaveCheckDataToGeneralJournalDetails(EntryDate, Description, Amount, COAccountId, CheckHeaderID, ReceivedFrom, lngJournalID)

                            'Save the Transaction to BizDatabase and Add Reference Type and Reference ID to BankStatementTransaction
                            objEBanking.ReferenceID = CheckHeaderID
                            objEBanking.ReferenceType = enmReferenceType.CheckHeader
                            objEBanking.MapBankStatementTransactions()

                        End If

                        objTransactionScope.Complete()
                    End Using
                Else
                    If rdbMatch.Checked = True Then

                        Dim Amount As Decimal = CCommon.ToDecimal(CType(gvRow.FindControl("lblAmount"), Label).Text)
                        Dim ReferenceID As Long = CCommon.ToLong(CType(gvRow.FindControl("ddlMatchTrans"), DropDownList).SelectedValue)
                        If ReferenceID > 0 Then
                            If Amount > 0 Then
                                objEBanking.BitCheck = 0
                                objEBanking.BitDeposit = 1
                                objEBanking.JournalUpdateId = ReferenceID
                                objEBanking.MapTransactionJournalUpdate()
                                objEBanking.ReferenceID = ReferenceID
                                objEBanking.ReferenceType = enmReferenceType.DepositHeader
                                objEBanking.MapBankStatementTransactions()
                            Else
                                objEBanking.BitCheck = 1
                                objEBanking.BitDeposit = 0
                                objEBanking.JournalUpdateId = ReferenceID
                                objEBanking.MapTransactionJournalUpdate()
                                objEBanking.ReferenceID = ReferenceID
                                objEBanking.ReferenceType = enmReferenceType.CheckHeader
                                objEBanking.MapBankStatementTransactions()
                            End If
                        Else
                            strMessage = "Please select a Matching Transaction"
                            ' ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)
                            litMessage1.Text = strMessage
                        End If

                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub btnAcceptSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAcceptSelected.Click
        Try
            Dim strMessage As String = ""
            Dim dt As New DataTable
            If Not Cache("__dtTransactions_Cache_Key") Is Nothing Then
                dt = CType(Cache("__dtTransactions_Cache_Key"), DataTable)
            End If
            Dim dtTransactionCopy As DataTable

            For Each gvRow As GridViewRow In gvReconcile.Rows

                Dim chkSelected As CheckBox = CType(gvRow.FindControl("chk"), CheckBox)
                dtTransactionCopy = dt.Copy()
                If Not IsNothing(chkSelected) Then
                    If chkSelected.Checked = True Then
                        Dim TransactionId As Long = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                        AcceptTransaction(gvRow.RowIndex)
                        For i As Integer = 0 To dtTransactionCopy.Rows.Count - 1
                            If dtTransactionCopy.Rows(i)("numTransactionId") = TransactionId Then
                                dt.Rows.RemoveAt(i)

                            End If
                        Next

                    End If

                End If

            Next
            gvReconcile.DataSource = dt
            gvReconcile.DataBind()
            Cache.Insert("__dtTransactions_Cache_Key", dt)

            strMessage = "Transactions accepted successfully!.,"
            'ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)
            litMessage1.Text = strMessage
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
            'PersistTable.Load()
            'If PersistTable.Count > 0 Then

            '    If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))) Is Nothing Then
            '        radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))).Selected = True
            '        BindTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
            '        BindAcceptedTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
            '        ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
            ''        ClientScript.RegisterClientScriptBlock(Me.GetType, "FillDropDowns", "FillDropDowns();", True)
            ''        ClientScript.RegisterStartupScript(Page.GetType, "FillDropDowns", "FillDropDowns();", True)
            '    End If

            'End If

        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage1.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
        End Try

    End Sub

    Sub radcmbCOAccounts_ItemDatabound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radcmbCOAccounts.ItemDataBound
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcAccountName").ToString()
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numAccountId").ToString()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Protected Sub radcmbCOAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbCOAccounts.SelectedIndexChanged
        Dim lst As Telerik.Web.UI.RadComboBox

        Try
            lst = sender
            If Not lst.SelectedValue = "" AndAlso Not lst.SelectedValue = "0" Then
                PersistTable.Clear()
                PersistTable.Add(radcmbCOAccounts.ID, radcmbCOAccounts.SelectedValue)
                PersistTable.Save()

                hdnFldCOAccountId.Value = radcmbCOAccounts.SelectedValue
                BindTransactions(CCommon.ToLong(radcmbCOAccounts.SelectedValue))
                BindAcceptedTransactions(CCommon.ToLong(radcmbCOAccounts.SelectedValue))
                ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)

            Else
                gvReconcile.DataSource = Nothing
                gvReconcile.Visible = False
                gvAcceptedTrans.DataSource = Nothing
                gvAcceptedTrans.Visible = False

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
        End Try


    End Sub

    Private Function RegisterAsNewCustomer(ByVal strCustomerName As String, ByVal Amount As Decimal) As Long
        Try
            Dim objContacts As New CContacts
            Dim objLeads As New CLeads
            Dim lngDivId As Long
            Dim lngCntID As Long = 0

            objLeads.DomainID = CCommon.ToLong(Session("DomainID"))
            objLeads.Email = ""
            objLeads.CompanyName = strCustomerName
            Dim a As Integer
            objLeads.CustName = strCustomerName
            a = strCustomerName.IndexOf(".")
            If a = -1 Then
                a = strCustomerName.IndexOf(" ")
            End If
            If a = -1 Then
                objLeads.FirstName = strCustomerName
                objLeads.LastName = strCustomerName
            Else
                objLeads.FirstName = strCustomerName.Substring(0, a)
                objLeads.LastName = strCustomerName.Substring(a + 1, strCustomerName.Length - (a + 1))
            End If
            objLeads.UserCntID = 0
            objLeads.ContactType = 70
            objLeads.PrimaryContact = True
            objLeads.UpdateDefaultTax = False
            objLeads.NoTax = False
            objLeads.CRMType = 1
            If Amount > 0 Then
                objLeads.CompanyType = 46
            Else
                objLeads.CompanyType = 47
            End If
            objLeads.DivisionName = "-"
            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo()
            lngDivId = objLeads.CreateRecordDivisionsInfo()
            objLeads.ContactID = 0
            objLeads.DivisionID = lngDivId
            lngCntID = objLeads.CreateRecordAddContactInfo() 'Creates Contact Info
            'Added By Sachin Sadhu||Date:22ndMay12014
            'Purpose :To Added Organization data in work Flow queue based on created Rules
            '          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = lngDivId
            objWfA.SaveWFOrganizationQueue()
            'end of code

            'Added By Sachin Sadhu||Date:24thJuly2014
            'Purpose :To Added Contact data in work Flow queue based on created Rules
            '         Using Change tracking
            Dim objWF As New Workflow()
            objWF.DomainID = Session("DomainID")
            objWF.UserCntID = Session("UserContactID")
            objWF.RecordID = lngCntID
            objWF.SaveWFContactQueue()
            ' ss//end of code

            Return lngDivId

        Catch ex As Exception
            Throw ex

        End Try


    End Function

    Protected Sub gvReconcile_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvReconcile.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim rdbAssign As RadioButton = CType(e.Row.FindControl("rdbAssign"), RadioButton)
                Dim rdbMatch As RadioButton = CType(e.Row.FindControl("rdbMatch"), RadioButton)
                Dim tblAssign As HtmlTable = CType(e.Row.FindControl("tblAssign"), HtmlTable)

                Dim trAssign As HtmlTable = CType(e.Row.FindControl("trAssign"), HtmlTable)
                Dim trMatch As HtmlTable = CType(e.Row.FindControl("trMatch"), HtmlTable)

                Dim ddlAccounts As DropDownList = CType(e.Row.FindControl("ddlAccounts"), DropDownList)
                Dim ddlClass As DropDownList = CType(e.Row.FindControl("ddlClass"), DropDownList)
                Dim ddlProject As DropDownList = CType(e.Row.FindControl("ddlProject"), DropDownList)
                Dim radCmbCompany As RadComboBox = CType(e.Row.FindControl("radCmbCompany"), RadComboBox)

                Dim ddlMatchTrans As DropDownList = CType(e.Row.FindControl("ddlMatchTrans"), DropDownList)
                Dim lnkbtnLookMatchingTrans As LinkButton = CType(e.Row.FindControl("lnkbtnLookMatchingTrans"), LinkButton)

                ddlClass.Visible = IIf(CCommon.ToInteger(HttpContext.Current.Session("DefaultClassType")) > 0, True, False)
                ddlProject.Visible = CCommon.ToBool(Session("IsEnableProjectTracking"))

                'BindClass(ddlClass)
                'BindProject(ddlProject)
                'BindCOADropdown(ddlAccounts)
                'ddlAccounts.Items.Insert(1, New ListItem("Add", "1"))
                'ddlAccounts.Items.Insert(1, New ListItem("Split", "2"))

                Dim btnAccept As Button = CType(e.Row.FindControl("btnAccept"), Button)
                Dim sArgument As String = CType(e.Row.DataItem, DataRowView).Row.Item("numTransactionID").ToString
                Dim CompanyName As String = CType(e.Row.DataItem, DataRowView).Row.Item("CompanyName").ToString

                If Not IsNothing(btnAccept) Then
                    setFlagCommands(btnAccept, "btnAccept", sArgument)
                End If

                'Dim lblCompany As Label = CType(e.Row.FindControl("lblCompany"), Label)
                Dim lblReferenceTypeID As Label = CType(e.Row.FindControl("lblReferenceTypeID"), Label)
                Dim lblReferenceID As Label = CType(e.Row.FindControl("lblReferenceID"), Label)

                'If Matching Transaction Found
                If CCommon.ToLong(lblReferenceID.Text) > 0 Then

                    Select Case (CType(CCommon.ToLong(lblReferenceTypeID.Text), enmReferenceType))
                        Case enmReferenceType.CheckHeader

                            If objChecks Is Nothing Then objChecks = New Checks()
                            objChecks.DomainID = Session("DomainID")
                            objChecks.CheckHeaderID = CCommon.ToLong(CCommon.ToLong(lblReferenceID.Text))
                            dtItems = objChecks.FetchCheckDetails()

                            If dtItems.Rows.Count > 0 Then
                                Dim builder As New StringBuilder

                                builder.Append(CCommon.ToString(dtItems.Rows(0)("dtCheckDate")))
                                builder.Append(" Check To ")
                                builder.Append(CCommon.ToString(dtItems.Rows(0)("CheckTo")))
                                builder.Append(CCommon.ToString(ReturnMoney(CCommon.ToDecimal((dtItems.Rows(0)("monAmount"))))))

                                Dim item As ListItem

                                item = New ListItem()
                                item.Text = builder.ToString
                                item.Value = dtItems.Rows(0)("numCheckHeaderID")
                                ddlMatchTrans.Items.Add(item)
                                ddlMatchTrans.Visible = True
                                ddlMatchTrans.Items.Insert(1, New ListItem("Look for matching Transactions", 2))
                                lnkbtnLookMatchingTrans.Visible = False

                                If Not IsNothing(rdbMatch) Then
                                    rdbMatch.Checked = True

                                End If
                            End If
                        Case enmReferenceType.DepositHeader

                            If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
                            objMakeDeposit.DomainID = Session("DomainID")
                            objMakeDeposit.DepositId = CCommon.ToLong(CCommon.ToLong(lblReferenceID.Text))
                            objMakeDeposit.Mode = 1
                            dtItems = objMakeDeposit.GetDepositDetails().Tables(1)

                            If dtItems.Rows.Count > 0 Then

                                Dim builder As New StringBuilder

                                builder.Append(CCommon.ToString(dtItems.Rows(0)("dtDepositDate")))
                                builder.Append(" Deposit from ")
                                builder.Append(CCommon.ToString(dtItems.Rows(0)("ReceivedFrom")))
                                builder.Append(CCommon.ToString(dtItems.Rows(0)("monAmountPaid")))

                                Dim item As ListItem

                                item = New ListItem()
                                item.Text = builder.ToString
                                item.Value = dtItems.Rows(0)("numDepositId")
                                ddlMatchTrans.Items.Add(item)
                                ddlMatchTrans.Visible = True
                                ddlMatchTrans.Items.Insert(1, New ListItem("Look for matching Transactions", 2))
                                lnkbtnLookMatchingTrans.Visible = False
                                If Not IsNothing(rdbMatch) Then
                                    rdbMatch.Checked = True
                                End If
                            End If

                    End Select


                Else
                    'No Reference Id found.
                    'Get UnCategorised income"/"expense" from chart_of_accounts and Map it to ddlAccounts
                    'If Not IsNothing(ddlAccounts) Then

                    '    If CCommon.ToLong(lblReferenceTypeID.Text) > 0 Then

                    '        Select Case (CType(CCommon.ToLong(lblReferenceTypeID.Text), enmReferenceType))

                    '            Case enmReferenceType.CheckHeader
                    '                If ddlAccounts.Items.FindByText("UnCategorized Expense") IsNot Nothing Then
                    '                    ddlAccounts.ClearSelection()
                    '                    ddlAccounts.Items.FindByText("UnCategorized Expense").Selected = True

                    '                End If

                    '            Case enmReferenceType.DepositHeader
                    '                If ddlAccounts.Items.FindByText("UnCategorized Income") IsNot Nothing Then
                    '                    ddlAccounts.ClearSelection()
                    '                    ddlAccounts.Items.FindByText("UnCategorized Income").Selected = True

                    '                End If

                    '        End Select

                    '    End If

                    'End If
                    If Not CompanyName = "" Then
                        Dim CompanyDet As String()
                        CompanyDet = CompanyName.Split("~")
                        If Not IsNothing(radCmbCompany) Then
                            Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)
                            lblName.Text = CompanyDet(1)
                        End If

                    End If

                End If

                If Not IsNothing(radCmbCompany) Then
                    Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)

                    If CCommon.ToLong(lblName.Text) > 0 Then
                        objCommon.UserCntID = Session("UserContactID")
                        objCommon.DivisionID = IIf(lblName.Text = "", 0, lblName.Text)
                        objCommon.charModule = "D"
                        Dim strCompany As String
                        strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID

                    End If

                End If

                'If Not IsNothing(radcmbAccounts) Then
                '    Dim dtChartAcntDetails As DataTable
                '    Dim objCOA As New ChartOfAccounting
                '    objCOA.DomainID = Session("DomainId")
                '    objCOA.AccountCode = "010101"
                '    dtChartAcntDetails = objCOA.GetParentCategory()
                '    radcmbAccounts.DataSource = dtChartAcntDetails
                '    radcmbAccounts.DataTextField = "vcAccountName"
                '    radcmbAccounts.DataValueField = "numAccountID"
                '    radcmbAccounts.ShowDropDownOnTextboxClick = True
                '    radcmbAccounts.EmptyMessage = "-- Select Account --"
                '    radcmbAccounts.Width = 175
                '    radcmbAccounts.EnableTextSelection = True
                '    radcmbAccounts.DataBind()

                'End If

                'If Not IsNothing(radcmbAccounts) Then
                '    Dim btnAdd As Button = CType(radcmbAccounts.Header.FindControl("btnAdd"), Button)

                '    Dim btnSplit As Button = CType(radcmbAccounts.Header.FindControl("btnSplit"), Button)
                '    If Not IsNothing(btnSplit) Then
                '        If CCommon.ToLong(lblReferenceTypeID.Text) > 0 Then
                '            Dim SplitFlag As String = "1"
                '            'setFlagCommands(btnSplit, "btnSplit", sArgument)
                '            Select Case (CType(CCommon.ToLong(lblReferenceTypeID.Text), enmReferenceType))

                '                Case enmReferenceType.CheckHeader
                '                    Dim OpenWriteCheck As String
                '                    OpenWriteCheck = String.Format("return OpenWriteCheck('" & sArgument & "','" & SplitFlag & "');")

                '                    ' btnSplit.Attributes.Add("onclick", "~/Accounting/frmWriteCheck.aspx?TransactionID=" & sArgument)
                '                    btnSplit.Attributes.Add("onclick", OpenWriteCheck)

                '                Case enmReferenceType.DepositHeader
                '                    Dim OpenMakeDeposit As String

                '                    OpenMakeDeposit = String.Format("return OpenMakeDeposit('" & sArgument & "','" & SplitFlag & "');")

                '                    'btnSplit.Attributes.Add("onclick", "~/Accounting/frmMakeDeposit.aspx?TransactionID=" & sArgument)
                '                    btnSplit.Attributes.Add("onclick", OpenMakeDeposit)

                '            End Select
                '        End If
                '        'btnAdd.Attributes.Add("postbackurl", "~/Accounting/frmWriteCheck.aspx?TransactionID=" & sArgument)
                '    End If
                'End If

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Protected Sub gvReconcile_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gvReconcile.RowCommand

        Try
            Dim strMessage As String = ""
            Dim _gridView As GridView = CType(sender, GridView)
            _gridView.EditIndex = -1

            ' Get the selected index and the command name
            ' Dim _selectedIndex As Integer = Integer.Parse(e.CommandArgument.ToString)
            Dim _commandName As String = e.CommandName
            Dim _eventArgument As String = Request.Form("__EVENTARGUMENT")
            Select Case (_commandName)

                Case "DoubleClick"
                    Dim gvRow As GridViewRow
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                    Dim TransactionId As String = 0
                    TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                    Response.Redirect("../EBanking/frmTransactionDetails.aspx?TransactionID=" & TransactionId, False)

                Case "btnAccept"

                    Dim gvRow As GridViewRow
                    Dim TransactionId As String = 0
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                    AcceptTransaction(gvRow.RowIndex)
                    TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                    strMessage = "Transaction accepted successfully!.,"
                    litMessage1.Text = strMessage
                    'ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)
                    BindTransactionsFromCache(TransactionId)
                    ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)

                    'PersistTable.Load()
                    'If PersistTable.Count > 0 Then
                    '    If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))) Is Nothing Then
                    '        radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))).Selected = True
                    '        BindTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                    '        BindAcceptedTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                    '        ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
                    '        ClientScript.RegisterClientScriptBlock(Me.GetType, "FillDropDowns", "FillDropDowns();", True)
                    '        ClientScript.RegisterStartupScript(Page.GetType, "FillDropDowns", "FillDropDowns();", True)
                    '    End If

                    'End If

                Case "btnCOAccAdd"

                    Dim gvRow As GridViewRow
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                    Dim TransactionId As String = 0
                    TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                    Response.Redirect("../Accounting/frmWriteCheck.aspx?TransactionID=" + TransactionId, False)

                Case "btnSplit"

                    Dim gvRow As GridViewRow
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                    Dim TransactionId As String = 0
                    TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                    Response.Redirect("../Accounting/frmWriteCheck.aspx?TransactionID=" + TransactionId, False)

                Case "GetMatchedTrans"

                    Dim gvRow As GridViewRow
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                    Dim ReferenceTypeId As String = "0"
                    Dim TransactionId As String = "0"
                    TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                    If CCommon.ToDecimal(CType(gvRow.FindControl("lblAmount"), Label).Text) > 0 Then
                        ReferenceTypeId = CCommon.ToString(CType(enmReferenceType.DepositHeader, Integer))
                    Else
                        ReferenceTypeId = CCommon.ToString(CType(enmReferenceType.CheckHeader, Integer))
                    End If
                    Response.Redirect("../EBanking/frmFindMatchingTransaction.aspx?TransactionID=" & TransactionId & "&ReferenceTypeId=" & ReferenceTypeId, False)

            End Select
        Catch ex As Exception

            If ex.Message = "FY_CLOSED" Then
                litMessage1.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)

        End Try
    End Sub

    Protected Sub gvAcceptedTrans_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAcceptedTrans.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then


                'If ((e.Row.RowState = DataControlRowState.Edit) _
                '            OrElse (e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate))) Then
                '    e.Row.Attributes.Clear()
                'Else
                '    Dim cellIndex As Integer = 2
                '    Do While (cellIndex < e.Row.Cells.Count)

                '        ' Get the LinkButton control in the second cell
                '        Dim _doubleClickButton As LinkButton = CType(e.Row.Cells(1).Controls(0), LinkButton)
                '        ' Get the javascript which is assigned to this LinkButton
                '        Dim _jsDouble As String = ClientScript.GetPostBackClientHyperlink(_doubleClickButton, "")
                '        ' Add the cell index as the event argument parameter
                '        _jsDouble = _jsDouble.Insert((_jsDouble.Length - 2), cellIndex.ToString)
                '        ' Add this javascript to the ondblclick Attribute of the row
                '        e.Row.Cells(cellIndex).Attributes("ondblclick") = _jsDouble
                '        cellIndex = (cellIndex + 1)
                '    Loop
                'End If

                Dim lnkbtnTransMatchDetails As LinkButton = CType(e.Row.FindControl("lnkbtnTransMatchDetails"), LinkButton)
                Dim sArgument As String = CType(e.Row.DataItem, DataRowView).Row.Item("numTransactionID").ToString

                ' Dim sArgument As String = e.Row.RowIndex.ToString()
                If Not IsNothing(lnkbtnTransMatchDetails) Then
                    lnkbtnTransMatchDetails.CommandName = "lnkbtnTransMatchDetails"
                    lnkbtnTransMatchDetails.CommandArgument = sArgument
                End If

            End If
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub gvAcceptedTrans_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gvAcceptedTrans.RowCommand
        Try

            Dim _gridView As GridView = CType(sender, GridView)
            _gridView.EditIndex = -1

            Dim gvRow As GridViewRow
            gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

            ' Get the selected index and the command name
            Dim _selectedIndex As Integer = Integer.Parse(e.CommandArgument.ToString)
            Dim _commandName As String = e.CommandName
            Dim _eventArgument As String = Request.Form("__EVENTARGUMENT")
            Select Case (_commandName)
                Case "lnkbtnTransMatchDetails"

                    Dim ReferenceTypeId As Integer = CCommon.ToInteger(CType(gvRow.FindControl("lblATReferenceTypeID"), Label).Text)
                    Dim ReferenceId As Long = CCommon.ToLong(CType(gvRow.FindControl("lblATReferenceID"), Label).Text)
                    Dim TransactionId As Integer = CCommon.ToLong(CType(gvRow.FindControl("lblATTransactionID"), Label).Text)

                    If CType(ReferenceTypeId, enmReferenceType) = enmReferenceType.DepositHeader Then
                        Response.Redirect("../Accounting/frmMakeDeposit.aspx?DepositId=" & ReferenceId & "&TransactionID=" & TransactionId, False)

                    ElseIf CType(ReferenceTypeId, enmReferenceType) = enmReferenceType.CheckHeader Then
                        Response.Redirect("../Accounting/frmWriteCheck.aspx?CheckHeaderID=" & ReferenceId & "&TransactionID=" & TransactionId, False)
                    Else



                    End If


            End Select

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub setFlagCommands(ByVal bFlag As Button, ByVal sCommand As String, ByVal sArgument As String)
        Try
            bFlag.CommandName = sCommand
            bFlag.CommandArgument = sArgument

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDropDowns()
        Try
            Dim dr As DataRow
            CreateTransDataTable()
            ' sb_FillCOAFromDBSel(ddlCOAccounts)
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainID")
            objEBanking.UserContactID = Session("UserContactId")
            dtCOAItemDetail = objEBanking.GetConnectedCOABankAccounts()
            If dtCOAItemDetail.Rows.Count = 0 Then
                pnlNoBankAccounts.Visible = True
            End If

            dr = dtCOAItemDetail.NewRow
            dr("numAccountId") = "0"
            '  dr("numBankDetailID") = 0
            dr("vcAccountName") = "-- Select One --"
            ' dr("numOpeningBal") = 0
            dr("dtOpeningDate") = ""
            dr("vcFIName") = ""
            dtCOAItemDetail.Rows.InsertAt(dr, 0)
            radcmbCOAccounts.DataSource = dtCOAItemDetail
            radcmbCOAccounts.DataBind()

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub CreateTransDataTable()
        Try
            dtCOAItemDetail = New DataTable()
            dtCOAItemDetail.Columns.Add("numAccountId")
            dtCOAItemDetail.Columns.Add("numBankDetailID")
            dtCOAItemDetail.Columns.Add("vcAccountName")
            dtCOAItemDetail.Columns.Add("numOpeningBal")
            dtCOAItemDetail.Columns.Add("dtOpeningDate")
            dtCOAItemDetail.Columns.Add("vcFIName")

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Public Sub sb_FillCOAFromDBSel(ByRef objCombo As DropDownList)
        Try
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainID")
            objEBanking.UserContactID = Session("UserContactId")
            objCombo.DataSource = objEBanking.GetConnectedCOABankAccounts()
            objCombo.DataTextField = "vcAccountName"
            objCombo.DataValueField = "numAccountId"
            objCombo.Attributes("OptionGroup") = "Connected Bank Accounts"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub sb_FillRadCOAFromDBSel(ByRef objCombo As DropDownList)
        Try
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainID")
            objEBanking.UserContactID = Session("UserContactId")
            objCombo.DataSource = objEBanking.GetConnectedCOABankAccounts()

            objCombo.Attributes("OptionGroup") = "Connected Bank Accounts"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Dim dtProject As DataTable
    Private Sub BindProject(ByRef ddlProject As DropDownList)
        Try
            If dtProject Is Nothing Then
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                dtProject = objProject.GetOpenProject()
            End If

            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtProject
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select Project--")
            ddlProject.Items.FindByText("--Select Project--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim dtClass As DataTable
    Private Sub BindClass(ByRef ddlClass As DropDownList)
        Try
            If dtClass Is Nothing Then
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()
            End If
            ddlClass.DataTextField = "ClassName"
            ddlClass.DataValueField = "numChildClassID"
            ddlClass.DataSource = dtClass
            ddlClass.DataBind()
            ddlClass.Items.Insert(0, "--Select Class--")
            ddlClass.Items.FindByText("--Select Class--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetCOAccountsListToLoadDropDown()

        Try
            'If Not dtChartAcnt Is Nothing & dtChartAcnt.Rows.Count = 0 Then
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = ""
            dtChartAcnt = objCOA.GetParentCategory()
            Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")
            For index As Integer = 0 To dr.Length - 1
                dtChartAcnt.Rows.Remove(dr(index))
            Next
            dtChartAcnt.AcceptChanges()
            'End If

            Dim item As ListItem

            For Each dr1 As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr1("vcAccountName")
                item.Value = dr1("numAccountID")
                item.Attributes("OptionGroup") = dr1("vcAccountType")
                ddlCOAccounts1.Items.Add(item)
                'lstCOAccnt.Items.Add(item)
            Next
            ddlCOAccounts1.Items.Insert(0, New ListItem("Split", "2"))
            ddlCOAccounts1.Items.Insert(0, New ListItem("-- Select Account --", "0"))

            If dtProject Is Nothing Then
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                dtProject = objProject.GetOpenProject()
            End If

            ddlProject1.DataTextField = "vcProjectName"
            ddlProject1.DataValueField = "numProId"
            ddlProject1.DataSource = dtProject
            ddlProject1.DataBind()
            ddlProject1.Items.Insert(0, "--Select Project--")
            ddlProject1.Items.FindByText("--Select Project--").Value = "0"

            If dtClass Is Nothing Then
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()
            End If
            ddlClass1.DataTextField = "ClassName"
            ddlClass1.DataValueField = "numChildClassID"
            ddlClass1.DataSource = dtClass
            ddlClass1.DataBind()
            ddlClass1.Items.Insert(0, "--Select Class--")
            ddlClass1.Items.FindByText("--Select Class--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Sub BindCOADropdown(ByVal ddlAccounts As DropDownList)
        Try
            If dtChartAcnt.Rows.Count = 0 Then
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = ""
                dtChartAcnt = objCOA.GetParentCategory()
                'Remove AR and AP values ..As those will create problem in AR aging summary report, So better not to allow to create journal entry against it
                Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")

                For index As Integer = 0 To dr.Length - 1
                    dtChartAcnt.Rows.Remove(dr(index))
                Next
                dtChartAcnt.AcceptChanges()
            End If



            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccounts.Items.Add(item)
            Next
            ddlAccounts.Items.Insert(0, New ListItem("--Select Account --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadChartAcnt()
        Try
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = ""
            dtChartAcnt = objCOA.GetParentCategory()
            'Commented by chintan- reason bug 949 
            'Remove AR and AP values ..As those will create problem in AR aging summary report, So better not to allow to create journal entry against it
            'Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")

            'For index As Integer = 0 To dr.Length - 1
            '    dtChartAcnt.Rows.Remove(dr(index))
            'Next
            'dtChartAcnt.AcceptChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnCorresGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo1.Click
        Try
            BindAcceptedTransactions(CCommon.ToLong(radcmbCOAccounts.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
        Try
            BindTransactions(CCommon.ToLong(radcmbCOAccounts.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Public Class DropDownCollection

        Private _Id As Integer
        Public Property Id() As Integer
            Get
                Return _Id
            End Get
            Set(ByVal value As Integer)
                _Id = value
            End Set
        End Property

        Private _Text As String
        Public Property Text() As String
            Get
                Return _Text
            End Get
            Set(ByVal value As String)
                _Text = value
            End Set
        End Property

        Private _Type As String
        Public Property Type() As String
            Get
                Return _Type
            End Get
            Set(ByVal value As String)
                _Type = value
            End Set
        End Property

    End Class

    Protected Sub btnRefreshTransaction_Click(sender As Object, e As EventArgs) Handles btnRefreshTransaction.Click
        Try
            PersistTable.Load()
            If PersistTable.Count > 0 Then
                If Not radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))) Is Nothing Then
                    radcmbCOAccounts.Items.FindItemByValue(CCommon.ToString(PersistTable(radcmbCOAccounts.ID))).Selected = True
                    BindTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                    BindAcceptedTransactions(CCommon.ToLong(PersistTable(radcmbCOAccounts.ID)))
                End If
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "FillDropDowns", "FillDropDowns();", True)
        End Try

    End Sub

End Class