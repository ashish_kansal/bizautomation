﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmBankReconciliation
    Inherits BACRMPage
    Dim objBankReconcile As BankReconcile
    'Dim dtChartAcnt As DataTable

    Dim objJournalEntry As JournalEntry
    Dim lngReconcileID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(35, 85)

            lngReconcileID = CCommon.ToLong(GetQueryStringVal("ReconcileID"))
            If Not IsPostBack Then

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnSaveReconcileBalance.Visible = False

                If lngReconcileID > 0 Then
                    LoadBankReconcile()
                    bindJournalEntries()
                End If

                'btnCancel.Attributes.Add("onclick", "return DeleteRecord()")


            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub LoadBankReconcile()
        Try
            Dim objBankReconcile As New BankReconcile
            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 1
            objBankReconcile.ReconcileID = lngReconcileID

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()
            Dim dtReconcile As DataTable = ds.Tables(0)

            lblAccount.Text = dtReconcile.Rows(0)("vcAccountName")
            lblStatementDate.Text = String.Format("{0:MM/dd/yyyy}", dtReconcile.Rows(0)("dtStatementDate"))
            lblServiceChargeAmt.Text = ReturnMoney(dtReconcile.Rows(0)("monServiceChargeAmount"))
            lblInterestEarnedAmt.Text = ReturnMoney(dtReconcile.Rows(0)("monInterestEarnedAmount"))
            lblBeginningBalance.Text = ReturnMoney(dtReconcile.Rows(0)("monBeginBalance"))
            lblEndingBalAmt.Text = ReturnMoney(dtReconcile.Rows(0)("monEndBalance"))

            hfAccountID.Value = dtReconcile.Rows(0)("numChartAcntId")
            hfReconcileComplete.Value = CCommon.ToBool(dtReconcile.Rows(0)("bitReconcileComplete"))
            'objBankReconcile.Mode = 4
            'objBankReconcile.ReconcileComplete = True
            'objBankReconcile.AcntId = hfAccountID.Value

            'ds = objBankReconcile.ManageBankReconcileMaster()

            'dtReconcile = ds.Tables(0)

            'If dtReconcile.Rows.Count > 0 Then
            '    lblBeginningBalance.Text = ReturnMoney(CCommon.ToDecimal(dtReconcile.Rows(0)("monAmount")))
            'End If
            objBankReconcile.StatementDate = dtReconcile.Rows(0)("dtStatementDate")
            If objBankReconcile.CheckFinancialyear() = False Then
                litMessage.Text = "You can reconcile statements only from current financial year. your option is to change statement date."
                btnFinishLater.Visible = False
                btnSaveReconcileBalance.Visible = False
                btnCancel.Visible = False
                hplEditStatement.Visible = False
            Else
                hlServiceCharge.Attributes.Add("onclick", "return EditRecon(" & lngReconcileID & ")")
                hlInterestEarned.Attributes.Add("onclick", "return EditRecon(" & lngReconcileID & ")")
                hlBeginningBalance.Attributes.Add("onclick", "return EditRecon(" & lngReconcileID & ")")
                hplEditStatement.Attributes.Add("onclick", "return EditRecon(" & lngReconcileID & ")")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub bindJournalEntries()
        Try
            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

            Dim dt As DataTable
            objJournalEntry.ChartAcntId = hfAccountID.Value
            objJournalEntry.DomainID = Session("DomainId")
            objJournalEntry.ReconcileID = lngReconcileID

            Dim ds As DataSet
            ds = objJournalEntry.GetJournalEntryListForBankReconciliation(rblRegisterFilter.SelectedValue, chkDate.Checked, IIf(CCommon.ToBool(hfReconcileComplete.Value), 2, 0))

            dt = ds.Tables(0)
            ViewState("Source") = dt
            dgJournalEntry.DataSource = dt
            dgJournalEntry.DataBind()

            If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                dgStatementData.DataSource = ds.Tables(1)
                dgStatementData.DataBind()

                If ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    lblTotalRecords.Text = ds.Tables(2).Rows(0)("TotalRecords")
                    lblDuplicateRecords.Text = ds.Tables(2).Rows(0)("DuplicateRecords")
                    lblMatchedRecords.Text = ds.Tables(2).Rows(0)("MatchedRecords")
                End If

                divStatment.Visible = True
                divJournalEntry.Attributes.Add("class", "col-xs-12 col-md-6")
                divColorCoding.Visible = True
            End If

            If Not Page.ClientScript.IsStartupScriptRegistered("Checkthis") Then ClientScript.RegisterStartupScript(Me.GetType, "Checkthis", "CheckThis();", True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub rblRegisterFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblRegisterFilter.SelectedIndexChanged
        Try
            bindJournalEntries()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub chkDate_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkDate.CheckedChanged
        Try
            bindJournalEntries()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Function ReturnDate(ByVal CloseDate) As String
        Try
            If Not IsDBNull(CloseDate) Then Return (FormattedDateFromDate(CloseDate, Session("DateFormat")))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function FormatDate(ByVal EntryDate) As String
        Try
            Return FormattedDateFromDate(EntryDate, Session("DateFormat"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSaveReconcileBalance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReconcileBalance.Click
        Try
            ReconcileCleared()
            Response.Redirect("frmBankReconciliationFinish.aspx?ReconcileID=" & lngReconcileID)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub ReconcileCleared()
        Try
            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                If divStatment.Visible AndAlso dgStatementData.Rows.Count > 0 Then
                    'Clear Bank Statement Rows
                    Dim dsStatementData As New DataSet
                    Dim dtStatementData As New DataTable
                    dtStatementData.Columns.Add("ID")
                    dtStatementData.Columns.Add("bitCleared")
                    dtStatementData.Columns.Add("numTransactionId")

                    For Each row As GridViewRow In dgStatementData.Rows
                        Dim drStatement As DataRow = dtStatementData.NewRow
                        drStatement("ID") = dgStatementData.DataKeys(row.RowIndex)("ID")
                        drStatement("bitCleared") = CType(row.FindControl("chk"), CheckBox).Checked
                        drStatement("numTransactionId") = 0

                        dtStatementData.Rows.Add(drStatement)
                    Next

                    dsStatementData.Tables.Add(dtStatementData)

                    If objBankReconcile Is Nothing Then objBankReconcile = New BankReconcile
                    objBankReconcile.DomainID = CCommon.ToLong(Session("DomainID"))
                    objBankReconcile.ReconcileID = lngReconcileID
                    objBankReconcile.UpdateBankReconcileFileDataStatus(dsStatementData.GetXml())
                End If


                'Clear Journal Entry
                If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

                objJournalEntry.ReconcileID = lngReconcileID

                Dim dtRecon As New DataTable
                Dim dr As DataRow
                Dim ds As New DataSet
                dtRecon.Columns.Add("numTransactionId")
                dtRecon.Columns.Add("bitReconcile")
                dtRecon.Columns.Add("bitCleared")

                For Each item As GridViewRow In dgJournalEntry.Rows
                    If Not item.FindControl("hdnTransactionId") Is Nothing AndAlso CCommon.ToLong(DirectCast(item.FindControl("hdnTransactionId"), HiddenField).Value) <> 0 Then
                        dr = dtRecon.NewRow()
                        dr("numTransactionId") = CCommon.ToLong(DirectCast(item.FindControl("hdnTransactionId"), HiddenField).Value)
                        dr("bitReconcile") = 0
                        dr("bitCleared") = CType(item.FindControl("chk"), CheckBox).Checked
                        dtRecon.Rows.Add(dr)
                    End If
                Next


                ds.Tables.Add(dtRecon.Copy)
                ds.Tables(0).TableName = "recon"
                objJournalEntry.StrText = ds.GetXml
                objJournalEntry.SaveUpdatedReconcileBalance()

                objTransactionScope.Complete()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnFinishLater_Click(sender As Object, e As System.EventArgs) Handles btnFinishLater.Click
        Try
            ReconcileCleared()

            Response.Redirect("frmBankReconcileList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            'If objBankReconcile Is Nothing Then objBankReconcile = New BankReconcile

            'objBankReconcile.DomainID = Session("DomainId")
            'objBankReconcile.UserCntID = Session("UserContactID")
            'objBankReconcile.Mode = 3
            'objBankReconcile.ReconcileID = lngReconcileID

            'objBankReconcile.ManageBankReconcileMaster()

            Response.Redirect("frmBankReconcileList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub dgStatementData_DataBound(sender As Object, e As EventArgs) Handles dgStatementData.DataBound
        
    End Sub

    Private Sub dgStatementData_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles dgStatementData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitCleared")) Or CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitMatched")) Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#b5e8d0")
                End If

                If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitCleared")) Or (CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitMatched")) AndAlso CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintType")) = 3) Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#effff1")
                ElseIf CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitMatched")) AndAlso CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintType")) = 2 Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffeec9")
                ElseIf CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintType")) = 1 Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#fcdef7")
                Else
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub dgJournalEntry_Sorting(sender As Object, e As GridViewSortEventArgs)
        Try
            Dim dt As DataTable = CType(ViewState("Source"), DataTable)

            Dim dv As DataView = New DataView(dt)

            If CCommon.ToString(ViewState("SortExpression")) = e.SortExpression Then
                If CCommon.ToString(ViewState("SortDirection")) = "ASC" Then
                    dv.Sort = e.SortExpression & " DESC"
                    ViewState("SortDirection") = "DESC"
                Else
                    dv.Sort = e.SortExpression & " ASC"
                    ViewState("SortDirection") = "ASC"
                End If
            Else
                dv.Sort = e.SortExpression & " ASC"
                ViewState("SortExpression") = e.SortExpression
                ViewState("SortDirection") = "ASC"
            End If

            dgJournalEntry.DataSource = dv
            dgJournalEntry.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub dgJournalEntry_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            If e.CommandName = "OpenRecord" Then
                '' Dim hplType As HyperLink
                Dim lblJournalId As Label
                Dim lblCheckId As Label
                Dim lblCashCreditCardId As Label
                Dim lblTransactionId As Label
                Dim lblOppId As Label
                Dim lblOppBizDocsId As Label
                Dim lblDepositId As Label
                Dim lblChartAcntId As Label
                Dim lblBillPaymentID As Label

                Dim linJournalId As Integer
                Dim lintCheckId As Integer
                Dim lintCashCreditCardId As Integer
                Dim lintOppId As Integer
                Dim lintOppBizDocsId As Integer
                Dim lintDepositId As Integer
                Dim lintCategoryHDRID As Integer
                Dim lintChartAcntId As Integer
                Dim lintBillPaymentID As Integer


                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Control).NamingContainer, GridViewRow)

                lblJournalId = row.FindControl("lblJournalId")
                lblCheckId = row.FindControl("lblCheckId")
                lblCashCreditCardId = row.FindControl("lblCashCreditCardId")
                lblTransactionId = row.FindControl("lblTransactionId")
                lblOppId = row.FindControl("lblOppId")
                lblOppBizDocsId = row.FindControl("lblOppBizDocsId")
                lblDepositId = row.FindControl("lblDepositId")
                lblChartAcntId = row.FindControl("lblChartAcntId")
                lblBillPaymentID = row.FindControl("lblBillPaymentID")

                ''hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage()")
                linJournalId = IIf(lblJournalId.Text = "&nbsp;" OrElse lblJournalId.Text = "", 0, lblJournalId.Text)
                lintCheckId = IIf(lblCheckId.Text = "&nbsp;" OrElse lblCheckId.Text = "", 0, lblCheckId.Text)
                lintCashCreditCardId = IIf(lblCashCreditCardId.Text = "&nbsp;" OrElse lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)
                lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
                lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
                lintDepositId = IIf(lblDepositId.Text = "&nbsp;" OrElse lblDepositId.Text = "", 0, lblDepositId.Text)
                lintChartAcntId = IIf(lblChartAcntId.Text = "&nbsp;" OrElse lblChartAcntId.Text = "", 0, lblChartAcntId.Text)
                lintBillPaymentID = IIf(lblBillPaymentID.Text = "&nbsp;" OrElse lblBillPaymentID.Text = "", 0, lblBillPaymentID.Text)

                If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppId = 0 And lintOppBizDocsId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 And lintBillPaymentID = 0 Then
                    ''hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmJournalEntryList.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "')")
                    Response.Redirect("../Accounting/frmNewJournalEntry.aspx?frm=BankRecon&ReconcileID=" & lngReconcileID & "&JournalId=" & linJournalId, False)
                ElseIf lintCheckId <> 0 Then
                    '' hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmChecks.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "')")
                    Response.Redirect("../Accounting/frmWriteCheck.aspx?frm=BankRecon&ReconcileID=" & lngReconcileID & "&JournalId=" & linJournalId & "&CheckHeaderID=" & lintCheckId, False)
                ElseIf lintCashCreditCardId <> 0 Then
                    '' hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmCash.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "')")
                    Response.Redirect("../Accounting/frmCash.aspx?frm=BankRecon&ReconcileID=" & lngReconcileID & "&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId, False)

                    ''ElseIf lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                    ''    Dim lstr As String
                    ''    lstr = "<script language=javascript>"
                    ''    lstr += "window.open('../opportunity/frmBizInvoice.aspx?frm=GeneralLedger&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "','','toolbar=no,titlebar=no,left=200, top=300,width=800,height=550,scrollbars=no,resizable=yes');"
                    ''    lstr += "</script>"
                    ''    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "BizInvoice", lstr)
                    ''    ''hplType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                ElseIf lintBillPaymentID <> 0 Then
                    Response.Redirect("../Accounting/frmVendorPayment.aspx?frm=BankRecon&ReconcileID=" & lngReconcileID & "&BillPaymentID=" & lintBillPaymentID, False)
                ElseIf lintDepositId <> 0 Then
                    Dim objMakeDeposit As New MakeDeposit
                    objMakeDeposit.DepositId = lintDepositId
                    objMakeDeposit.DomainID = Session("DomainID")
                    objMakeDeposit.Mode = 0
                    Dim dtMakeDeposit As DataTable = objMakeDeposit.GetDepositDetails().Tables(0)

                    If dtMakeDeposit.Rows.Count > 0 Then
                        If CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 2 Then
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "OpenAmountPaid", "OpenAmountPaid(" & lngReconcileID & "," & lintDepositId & ")", True)
                            'Response.Redirect("../opportunity/frmAmtPaid.aspx?frm=BankRecon&ReconcileID=" & lngReconcileID & "&DepositeID=" & lintDepositId, False)
                        ElseIf CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 1 Then
                            Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=BankRecon&ReconcileID=" & lngReconcileID & "&JournalId=" & linJournalId & "&ChartAcntId=" & lintChartAcntId & "&DepositId=" & lintDepositId, False)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Dim decTotDeposite As Decimal = 0
    Dim decTotPayment As Decimal = 0

    Protected Sub dgJournalEntry_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                decTotDeposite = 0
                decTotPayment = 0
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                decTotDeposite += CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Deposit"))
                decTotPayment += CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Payment"))
                If CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Deposit")) > 0 Then
                    CType(e.Row.FindControl("lblReconbalValue"), Label).Text = ReturnMoney(CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Deposit")))
                Else
                    CType(e.Row.FindControl("lblReconbalValue"), Label).Text = -ReturnMoney(CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Payment")))
                End If

                Dim chk As CheckBox = e.Row.FindControl("chk")
                chk.Checked = IIf(DataBinder.Eval(e.Row.DataItem, "bitReconcile") = True, True, DataBinder.Eval(e.Row.DataItem, "bitCleared") Or DataBinder.Eval(e.Row.DataItem, "bitMatched"))

                If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitCleared")) Or (CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitMatched")) AndAlso CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintType")) = 3) Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#effff1")
                ElseIf CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitMatched")) AndAlso CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintType")) = 2 Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffeec9")
                ElseIf CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintType")) = 1 Then
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#fcdef7")
                Else
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff")
                End If

                Dim lblOppId As Label
                Dim lblOppBizDocsId As Label
                Dim lintOppId As Integer
                Dim lintOppBizDocsId As Integer
                Dim lintCategoryHDRID As Integer
                Dim lnkType As LinkButton
                Dim lblCategoryHDRID As Label
                Dim lblTEType As Label
                Dim lblCategory As Label
                Dim lblUserCntID As Label
                Dim lblFromDate As Label
                lnkType = e.Row.FindControl("lnkType")
                lblCategoryHDRID = e.Row.FindControl("lblCategoryHDRID")
                lblOppId = e.Row.FindControl("lblOppId")
                lblOppBizDocsId = e.Row.FindControl("lblOppBizDocsId")
                lblCategory = e.Row.FindControl("lblCategory")
                lblTEType = e.Row.FindControl("lblTEType")
                lblUserCntID = e.Row.FindControl("lblUserCntID")
                lblFromDate = e.Row.FindControl("lblFromDate")
                lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
                lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
                lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)

                If lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                    lnkType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                ElseIf CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numBillID")) <> 0 Then
                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numLandedCostOppId")) <> 0 Then
                        lnkType.Attributes.Add("onclick", "return OpenLandedCost('" & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numLandedCostOppId")) & "')")
                    Else
                        lnkType.Attributes.Add("onclick", "return OpenBill('" & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numBillID")) & "')")
                    End If
                ElseIf lintCategoryHDRID <> 0 Then

                    Select Case lblTEType.Text
                        Case 0
                            If lblFromDate.Text <> "" Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "&Date=" & CDate(lblFromDate.Text) & "')")
                            Else
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 1
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?frm=TE&ProStageID=0&Proid=0&DivId=&Date=&CatID=&CntID=" & lblUserCntID.Text & "&CatHdrId=" & lintCategoryHDRID & "')")
                            Else
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 2
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 3
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BankRecon&ReconcileID=" & lngReconcileID & "&CatHDRID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                    End Select
                End If
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                CType(e.Row.FindControl("lblTotalDeposite"), Label).Text = ReturnMoney(decTotDeposite)
                CType(e.Row.FindControl("lblTotalPayment"), Label).Text = ReturnMoney(decTotPayment)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub dgJournalEntry_RowCreated(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim columnIndex As Integer = 0

                For Each dcf As DataControlField In dgJournalEntry.Columns
                    If dcf.SortExpression <> "" AndAlso dcf.SortExpression = CCommon.ToString(ViewState("SortExpression")) Then
                        If CCommon.ToString(ViewState("SortDirection")) = "DESC" Then
                            e.Row.Cells(columnIndex).Controls.Add(New LiteralControl(" <i class='fa fa-caret-down'></i>"))
                        Else
                            e.Row.Cells(columnIndex).Controls.Add(New LiteralControl(" <i class='fa fa-caret-up'></i>"))
                        End If
                    End If

                    columnIndex += 1
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class