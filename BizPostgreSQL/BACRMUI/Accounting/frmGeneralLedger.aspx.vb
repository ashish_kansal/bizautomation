''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Imports System.IO.Compression
Imports System.Web.Services
Imports Newtonsoft.Json
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmGeneralLedger
    Inherits BACRMPage

#Region "Variables"
    Dim mobjGeneralLedger As GeneralLedger
    Dim ldecTotalBalanceAmt As Decimal
    Dim dtChartAcntGeneralLedgerDetails As DataTable
    Dim dsGeneralLedger As DataSet = New DataSet
    Dim dtGeneralLedger As DataTable = dsGeneralLedger.Tables.Add("GeneralLedger")

    Dim dtChartAcntDetails As DataTable
    Dim Month, Year As Int16

    Dim strAccountTypeID As String
    Dim strAccountID As String
    Dim DisplayMode As Short
    Dim intDomainID As Integer
    Dim dtFYClosingDate As DateTime
    Protected lngDivisionID As Long
    Dim lngItemCode As Long
    Dim lngAccountClassID As Long
    Dim boolLoadPersistance As Boolean = True
    Dim boolExcludeAccountPersistance As Boolean = False

    ' Public Shared TransCountForAccount As Integer
    'Public Shared AccTransactionCount As New Dictionary(Of Long, Long)

    Public Shared AccTransactionCount As New Dictionary(Of Long, Integer)()
    Public Shared AccClosingBalance As New Dictionary(Of Long, Double)()
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngItemCode = CCommon.ToInteger(GetQueryStringVal("ItemCode"))
            DisplayMode = CCommon.ToInteger(GetQueryStringVal("Mode"))
            intDomainID = CCommon.ToInteger(GetQueryStringVal("DomainID"))
            lngDivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
            hdnAccountClassID.Value = CCommon.ToLong(GetQueryStringVal("AccountClassID"))

            hdnDivisionId.Value = lngDivisionID
            If intDomainID = 0 Then
                intDomainID = Session("DomainID")
            End If

            GetUserRightsForPage(35, 89)

            If Not IsPostBack Then
                If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                mobjGeneralLedger.DomainID = intDomainID
                mobjGeneralLedger.Year = CInt(Now.Year)



                BindAccountType()

                Select Case DisplayMode
                    Case 1 'From Monthly Summary
                        strAccountTypeID = GetQueryStringVal("AcntTypeID")
                        If Not strAccountTypeID Is Nothing Then
                            Month = CType(GetQueryStringVal("Month"), Short)
                            Year = CType(GetQueryStringVal("Year"), Short)

                            strAccountID = GetQueryStringVal("AccountID")
                            hdnAccounts.Value = strAccountID
                            hdnItemCode.Value = "0"

                            If Month > 0 Then
                                hdnFromDate.Value = New Date(Year, Month, 1).ToString("MM/dd/yyyy")
                                hdnToDate.Value = LastDayOfMonthFromDateTime(New Date(Year, Month, 1)).ToString("MM/dd/yyyy")
                            End If
                            Dim AccountTypeCode As String = GetAccountTypeCode(strAccountTypeID)
                            If Not ddlType.Items.FindByValue(AccountTypeCode) Is Nothing Then
                                ddlType.Items.FindByValue(AccountTypeCode).Selected = True
                            End If
                            BindCOA()
                            If strAccountID.Split(",").Length > 1 Then ddlCOA.SelectedValue = 1
                            If Not ddlCOA.Items.FindByValue(strAccountID) Is Nothing Then
                                ddlCOA.Items.FindByValue(strAccountID).Selected = True
                            End If
                            boolLoadPersistance = False
                        End If
                    Case 2 'Show GL by selected customer from frmAccounts.aspx
                        Dim obj As GridMasterRegular = Page.Master
                        obj.HideWebMenuUserControl = True
                        objCommon.DivisionID = lngDivisionID
                        lblGridTitle.Text = "General Ledger Transactions Of " + objCommon.GetCompanyName()
                        ddlType.ClearSelection()
                        ddlType.SelectedValue = 0
                        BindCOA()
                        ddlCOA.SelectedValue = 1 'Select multiple
                        Dim objCOA As New ChartOfAccounting
                        objCOA.DomainID = Session("DomainId")
                        objCOA.AccountCode = ""
                        Dim dtAccounts As DataTable = objCOA.GetParentCategory()
                        Dim strAccounts As String = String.Empty
                        For i As Integer = 0 To dtAccounts.Rows.Count - 1
                            strAccounts = strAccounts & dtAccounts.Rows(i)("numAccountId").ToString & ","
                        Next
                        trAccounts.Visible = False
                        hdnAccounts.Value = strAccounts
                        hdnItemCode.Value = "0"

                        boolExcludeAccountPersistance = True
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)
                    Case 3 ' Balance summary by organization and Chart of account , from frmChartofAccountsDetail.aspx
                        strAccountID = GetQueryStringVal("AccountID")
                        strAccountTypeID = GetQueryStringVal("AcntTypeID")
                        hdnAccounts.Value = strAccountID
                        hdnItemCode.Value = "0"

                        Dim AccountTypeCode As String = GetAccountTypeCode(strAccountTypeID)
                        If Not ddlType.Items.FindByValue(AccountTypeCode) Is Nothing Then
                            ddlType.Items.FindByValue(AccountTypeCode).Selected = True
                        End If

                        BindCOA()
                        If Not ddlCOA.Items.FindByValue(strAccountID) Is Nothing Then
                            ddlCOA.Items.FindByValue(strAccountID).Selected = True
                        End If
                        boolLoadPersistance = False
                        hdnFromDate.Value = mobjGeneralLedger.GetFiscalDate().ToString("MM/dd/yyyy")
                        hdnToDate.Value = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).ToString("MM/dd/yyyy")
                    Case 4 ' From Profit & Loss reports with from and to date
                        strAccountTypeID = GetQueryStringVal("AcntTypeID")
                        If Not strAccountTypeID Is Nothing Then
                            strAccountID = GetQueryStringVal("AccountID")
                            hdnAccounts.Value = strAccountID
                            hdnItemCode.Value = "0"

                            hdnFromDate.Value = Convert.ToDateTime(GetQueryStringVal("From")).ToString("MM/dd/yyyy")
                            hdnToDate.Value = Convert.ToDateTime(GetQueryStringVal("To")).ToString("MM/dd/yyyy")

                            Dim AccountTypeCode As String = GetAccountTypeCode(strAccountTypeID)
                            If Not ddlType.Items.FindByValue(AccountTypeCode) Is Nothing Then
                                ddlType.Items.FindByValue(AccountTypeCode).Selected = True
                            End If
                            BindCOA()
                            If strAccountID.Split(",").Length > 1 Then ddlCOA.SelectedValue = 1
                            If Not ddlCOA.Items.FindByValue(strAccountID) Is Nothing Then
                                ddlCOA.Items.FindByValue(strAccountID).Selected = True
                            End If
                        End If
                        boolLoadPersistance = False
                    Case 5 'from Chart of accounts tree
                        strAccountTypeID = GetQueryStringVal("AcntTypeID")
                        If Not strAccountTypeID Is Nothing Then
                            strAccountID = GetQueryStringVal("AccountID")
                            hdnAccounts.Value = strAccountID
                            hdnItemCode.Value = "0"

                            Dim AccountTypeCode As String = GetAccountTypeCode(strAccountTypeID)
                            If Not ddlType.Items.FindByValue(AccountTypeCode) Is Nothing Then
                                ddlType.Items.FindByValue(AccountTypeCode).Selected = True
                            End If
                            BindCOA()
                            If strAccountID.Split(",").Length > 1 Then ddlCOA.SelectedValue = 1
                            If Not ddlCOA.Items.FindByValue(strAccountID) Is Nothing Then
                                ddlCOA.Items.FindByValue(strAccountID).Selected = True
                            End If

                            hdnFromDate.Value = mobjGeneralLedger.GetFiscalDate().ToString("MM/dd/yyyy")
                            hdnToDate.Value = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).ToString("MM/dd/yyyy")
                        End If
                        boolLoadPersistance = True
                        boolExcludeAccountPersistance = True
                    Case 6 'from Item Details
                        Dim obj As GridMasterRegular = Page.Master
                        obj.HideWebMenuUserControl = True
                        Dim objItems As New BACRM.BusinessLogic.Item.CItems
                        objItems.ItemCode = lngItemCode
                        objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")

                        Dim dtItemDetails As DataTable
                        dtItemDetails = objItems.ItemDetails()
                        Dim ItemName As String = CCommon.Truncate(CCommon.ToString(dtItemDetails.Rows(0)("vcItemName")), 50)
                        lblGridTitle.Text = "General Ledger Transactions Of " + ItemName

                        Dim objGeneralLedger As New GeneralLedger
                        objGeneralLedger.DomainID = intDomainID
                        objGeneralLedger.ItemCode = lngItemCode
                        objGeneralLedger.byteMode = 0
                        Dim dtItemsCOA As DataTable = objGeneralLedger.GetCOAforItemsJournalEntries()
                        Dim strAccounts As String = String.Empty
                        For i As Integer = 0 To dtItemsCOA.Rows.Count - 1
                            strAccounts = strAccounts & dtItemsCOA.Rows(i)("numAccountId").ToString & ","
                        Next

                        BindCOA()
                        ddlCOA.SelectedValue = 1
                        trAccounts.Visible = False
                        hdnAccounts.Value = strAccounts
                        hdnItemCode.Value = CCommon.ToString(lngItemCode)
                        boolLoadPersistance = True
                        boolExcludeAccountPersistance = True
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)

                    Case Else 'Direct Form Tree
                        If ddlType.Items.Count > 1 Then ddlType.SelectedIndex = 1
                        hdnItemCode.Value = "0"
                        BindCOA()
                        'ddlCOA.SelectedIndex = 1

                End Select

                If boolLoadPersistance Then
                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        If boolExcludeAccountPersistance = False Then
                            If ddlType.Items.FindByValue(CCommon.ToString(PersistTable(ddlType.ID))) IsNot Nothing Then
                                ddlType.ClearSelection()
                                ddlType.Items.FindByValue(CCommon.ToString(PersistTable(ddlType.ID))).Selected = True
                                BindCOA()
                            End If
                            If ddlCOA.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOA.ID))) IsNot Nothing Then
                                ddlCOA.ClearSelection()
                                ddlCOA.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOA.ID))).Selected = True
                            End If
                        End If

                        hdnHeaderTransactionType.Value = CCommon.ToString(PersistTable("FilterTransactionType"))
                        hdnHeaderName.Value = CCommon.ToString(PersistTable("FiterName"))
                        hdnHeaderDescription.Value = CCommon.ToString(PersistTable("FiterDescription"))
                        hdnHeaderSplit.Value = CCommon.ToString(PersistTable("FiterSplit"))
                        hdnHeaderAmount.Value = CCommon.ToString(PersistTable("FiterAmount"))
                        hdnHeaderNarration.Value = CCommon.ToString(PersistTable("FiterNarration"))

                        If DisplayMode <> 1 AndAlso DisplayMode <> 2 AndAlso DisplayMode <> 3 AndAlso DisplayMode <> 4 AndAlso DisplayMode <> 5 AndAlso DisplayMode <> 6 Then
                            If ddlCOA.SelectedValue = "1" Then
                                hdnAccounts.Value = CCommon.ToString(PersistTable("AccountIDs"))
                            End If
                        End If

                        Try
                            Dim fromDate As DateTime
                            Dim toDate As DateTime
                            If PersistTable("FromDate") <> "" AndAlso DateTime.TryParse(PersistTable("FromDate"), fromDate) AndAlso PersistTable("ToDate") <> "" AndAlso DateTime.TryParse(PersistTable("ToDate"), toDate) Then
                                hdnFromDate.Value = fromDate.ToString("MM/dd/yyyy")
                                hdnToDate.Value = toDate.ToString("MM/dd/yyyy")
                            Else
                                hdnFromDate.Value = mobjGeneralLedger.GetFiscalDate().ToString("MM/dd/yyyy")
                                hdnToDate.Value = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).ToString("MM/dd/yyyy")
                            End If
                        Catch ex As Exception
                            'Do not throw error when date format which is stored in persist table and Current date formats are different
                            hdnFromDate.Value = mobjGeneralLedger.GetFiscalDate().ToString("MM/dd/yyyy")
                            hdnToDate.Value = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).ToString("MM/dd/yyyy")
                        End Try
                    Else
                        hdnFromDate.Value = mobjGeneralLedger.GetFiscalDate().ToString("MM/dd/yyyy")
                        hdnToDate.Value = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).ToString("MM/dd/yyyy")
                    End If
                    End If



                'LoadGeneralDetails()

                LoadGeneralDetailswithPaging()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Sub LoadClosingYear()
        Try
            'Get closing date of financial year to Lock transaction from being updated
            Dim objCOA As New ChartOfAccounting
            objCOA.FinancialYearID = 0
            objCOA.DomainID = Session("DomainID")
            objCOA.Mode = 4
            Dim ds As DataSet = objCOA.GetFinancialYear()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("FYClosingDate").ToString <> "" Then
                    dtFYClosingDate = DateFromFormattedDate(ds.Tables(0).Rows(0)("FYClosingDate").ToString, Session("DateFormat"))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCOA()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim dt As DataTable
            objCOA.DomainID = intDomainID
            objCOA.AccountCode = ddlType.SelectedValue
            dt = objCOA.GetParentCategory()

            ddlCOA.DataSource = dt
            ddlCOA.DataTextField = "vcAccountName1"
            ddlCOA.DataValueField = "numAccountId"
            ddlCOA.DataBind()
            ddlCOA.Items.Insert(0, New ListItem("--Select One--", "0"))
            ddlCOA.Items.Insert(1, New ListItem("--Select Multiple--", "1"))

            If DisplayMode = 0 Then
                For Each dr As DataRow In dt.Rows
                    strAccountID = strAccountID + "," + dr("numAccountId").ToString()
                Next
                hdnAccounts.Value = strAccountID
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindAccountType()
        Try
            Dim lobjAccounting As New ChartOfAccounting
            Dim ds As DataSet
            lobjAccounting.DomainID = intDomainID
            lobjAccounting.Mode = 3 'Select all account type which has atleast one account 
            ds = lobjAccounting.GetAccountTypes()
            ddlType.DataSource = ds.Tables(0)
            ddlType.DataTextField = "vcAccountType"
            ddlType.DataValueField = "vcAccountCode"
            ddlType.DataBind()
            ddlType.Items.Insert(0, New ListItem("-- Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetAccountTypeCode(ByVal lngAccountTypeID As Long) As String
        Try
            Dim lobjAccounting As New ChartOfAccounting
            Dim ds As DataSet
            lobjAccounting.DomainID = intDomainID
            lobjAccounting.AccountTypeID = lngAccountTypeID
            'lobjAccounting.Mode = 3 
            ds = lobjAccounting.GetAccountTypes()
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("vcAccountCode").ToString()
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub LoadTreeGrid(ByVal dtChartAcntDet As DataView)
        Dim i As Integer
        Dim j As Integer
        Dim lintCount As Integer
        Dim ldecTotalAmt As Decimal
        Dim dtChartAcntBeginingBalance As DataTable
        Dim ldecOpeningBalance As Decimal
        Dim lobjJournalEntry As New JournalEntry
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger

            For i = 0 To dtChartAcntDet.Count - 1
                ldecTotalAmt = 0
                Dim drChartAcntDetails As DataRow = dtGeneralLedger.NewRow
                lobjJournalEntry.ChartAcntId = dtChartAcntDet.Item(i)("numAccountId")
                lobjJournalEntry.DomainID = intDomainID

                lobjJournalEntry.FromDate = CDate(hdnFromDate.Value)
                lobjJournalEntry.ToDate = DateAdd(DateInterval.Day, 1, CDate(hdnToDate.Value)) ''calTo.SelectedDate
                dtChartAcntGeneralLedgerDetails = lobjJournalEntry.GetGenernalLedgerList
                lintCount = dtChartAcntGeneralLedgerDetails.Rows.Count - 1
                dtChartAcntBeginingBalance = lobjJournalEntry.GetBeginingBalanceGeneralLedgerList

                mobjGeneralLedger.ChartAcntId = dtChartAcntDet.Item(i)("numAccountId")
                mobjGeneralLedger.DomainID = intDomainID
                mobjGeneralLedger.FromDate = hdnFromDate.Value
                mobjGeneralLedger.ToDate = DateAdd(DateInterval.Day, 1, CDate(hdnToDate.Value))
                ldecOpeningBalance = mobjGeneralLedger.GetCurrentOpeningBalanceForGeneralLedgerDetails()

                If dtChartAcntGeneralLedgerDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then
                    drChartAcntDetails(0) = "<font color=Black><b>" & dtChartAcntDet.Item(i)("CategoryName") & "</b></font>"
                    dtGeneralLedger.Rows.Add(drChartAcntDetails)
                End If

                If dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drGeneralLedgerDetails1 As DataRow = dtGeneralLedger.NewRow
                    drGeneralLedgerDetails1(0) = "&nbsp;&nbsp;&nbsp;&nbsp;  Beginning Balance"
                    drGeneralLedgerDetails1(5) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    drGeneralLedgerDetails1(6) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    dtGeneralLedger.Rows.Add(drGeneralLedgerDetails1)
                    ldecTotalAmt = ldecTotalAmt + dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance")
                End If

                For j = 0 To dtChartAcntGeneralLedgerDetails.Rows.Count - 1
                    Dim drGeneralLedgerDetails As DataRow = dtGeneralLedger.NewRow

                    drGeneralLedgerDetails(0) = "&nbsp;&nbsp;&nbsp;&nbsp; " & dtChartAcntGeneralLedgerDetails.Rows(j)("EntryDate")
                    drGeneralLedgerDetails(1) = dtChartAcntGeneralLedgerDetails.Rows(j)("TransactionType")
                    drGeneralLedgerDetails(2) = dtChartAcntGeneralLedgerDetails.Rows(j)("CompanyName")
                    drGeneralLedgerDetails(3) = dtChartAcntGeneralLedgerDetails.Rows(j)("Memo")
                    drGeneralLedgerDetails(4) = dtChartAcntGeneralLedgerDetails.Rows(j)("AcntTypeDescription")
                    drGeneralLedgerDetails(5) = IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")))
                    drGeneralLedgerDetails(6) = ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("numBalance"))
                    drGeneralLedgerDetails(7) = dtChartAcntGeneralLedgerDetails.Rows(j)("JournalId")
                    drGeneralLedgerDetails(8) = IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("CheckId")), 0, dtChartAcntGeneralLedgerDetails.Rows(j)("CheckId"))
                    drGeneralLedgerDetails(9) = IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("CashCreditCardId")), 0, dtChartAcntGeneralLedgerDetails.Rows(j)("CashCreditCardId"))
                    drGeneralLedgerDetails(10) = dtChartAcntGeneralLedgerDetails.Rows(j)("numChartAcntId")
                    drGeneralLedgerDetails(11) = dtChartAcntGeneralLedgerDetails.Rows(j)("numTransactionId")
                    drGeneralLedgerDetails(12) = dtChartAcntGeneralLedgerDetails.Rows(j)("numOppId")
                    drGeneralLedgerDetails(13) = dtChartAcntGeneralLedgerDetails.Rows(j)("numOppBizDocsId")
                    drGeneralLedgerDetails(14) = dtChartAcntGeneralLedgerDetails.Rows(j)("numDepositId")
                    drGeneralLedgerDetails(15) = dtChartAcntGeneralLedgerDetails.Rows(j)("numCategoryHDRID")
                    drGeneralLedgerDetails(16) = dtChartAcntGeneralLedgerDetails.Rows(j)("tintTEType")
                    drGeneralLedgerDetails(17) = dtChartAcntGeneralLedgerDetails.Rows(j)("numCategory")
                    drGeneralLedgerDetails(18) = dtChartAcntGeneralLedgerDetails.Rows(j)("numUserCntID")
                    drGeneralLedgerDetails(19) = dtChartAcntGeneralLedgerDetails.Rows(j)("dtFromDate")

                    dtGeneralLedger.Rows.Add(drGeneralLedgerDetails)
                    ldecTotalAmt = ldecTotalAmt + IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")))
                Next

                If dtChartAcntGeneralLedgerDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drDummyRow As DataRow = dtGeneralLedger.NewRow
                    drDummyRow(0) = "<B> Total for " & dtChartAcntDet.Item(i)("CategoryName") & "</b>"
                    drDummyRow(5) = "<B> " & ReturnMoney(ldecTotalAmt) & "</b>"
                    dtGeneralLedger.Rows.Add(drDummyRow)
                    ldecTotalBalanceAmt = ldecTotalBalanceAmt + ldecTotalAmt
                End If

                ''dtChildChartAcntDetails = GetChildCategory(dtChartAcntDetails.Item(i)("numAccountId"))

                ''LoadTreeGrid(dtChildChartAcntDetails)

                Dim dvChildnode As New DataView(dtChartAcntDetails)          'create a dataview object
                dvChildnode.RowFilter = "numParntAcntId=" & dtChartAcntDet.Item(i)("numAccountId")
                ldecTotalBalanceAmt = ldecTotalBalanceAmt + ldecTotalAmt
                LoadTreeGrid(dvChildnode)

                If dvChildnode.Count > 0 Then
                    If dtChartAcntGeneralLedgerDetails.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then

                        Dim drDummyRowForSubAcntTot As DataRow = dtGeneralLedger.NewRow
                        drDummyRowForSubAcntTot(0) = "<B> Total for " & dtChartAcntDet.Item(i)("CategoryName") & "  with sub-accounts </b>"
                        drDummyRowForSubAcntTot(5) = "<B> " & ReturnMoney(ldecOpeningBalance) & "</b>"
                        dtGeneralLedger.Rows.Add(drDummyRowForSubAcntTot)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetChildCategory(ByVal ParentID As String) As DataTable
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.ParentAccountId = ParentID
            mobjGeneralLedger.DomainID = intDomainID
            GetChildCategory = mobjGeneralLedger.GetChildCategory()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            End If
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''Private Sub RepGeneralLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepGeneralLedger.ItemDataBound
    ''    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

    ''        Dim hplType As HyperLink
    ''        Dim lblJournalId As Label
    ''        Dim lblCheckId As Label
    ''        Dim lblCashCreditCardId As Label
    ''        Dim lblTransactionId As Label
    ''        Dim linJournalId As Integer
    ''        Dim lintCheckId As Integer
    ''        Dim lintCashCreditCardId As Integer

    ''        hplType = e.Item.FindControl("hplType")
    ''        lblJournalId = e.Item.FindControl("lblJournalId")
    ''        lblCheckId = e.Item.FindControl("lblCheckId")
    ''        lblCashCreditCardId = e.Item.FindControl("lblCashCreditCardId")
    ''        lblTransactionId = e.Item.FindControl("lblTransactionId")

    ''        hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage()")
    ''        linJournalId = IIf(lblJournalId.Text = "&nbsp;" OrElse lblJournalId.Text = "", 0, lblJournalId.Text)
    ''        lintCheckId = IIf(lblCheckId.Text = "&nbsp;" OrElse lblCheckId.Text = "", 0, lblCheckId.Text)
    ''        lintCashCreditCardId = IIf(lblCashCreditCardId.Text = "&nbsp;" OrElse lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)

    ''        If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 Then
    ''            hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmJournalEntryList.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "')")
    ''        ElseIf lintCheckId <> 0 Then
    ''            hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmChecks.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "')")
    ''        ElseIf lintCashCreditCardId <> 0 Then
    ''            hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmCash.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "')")
    ''        End If
    ''    End If
    ''End Sub

    Private Sub btnReloadGL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReLoadGL.Click
        Try
            Dim vcFromDate As String = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderFrom"), TextBox).Text
            Dim vcToDate As String = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderTo"), TextBox).Text
            Dim fromDate As DateTime
            Dim toDate As DateTime
            If vcFromDate <> "" AndAlso DateTime.TryParse(vcFromDate, fromDate) AndAlso vcToDate <> "" AndAlso DateTime.TryParse(vcToDate, toDate) Then
                hdnFromDate.Value = fromDate.ToString("MM/dd/yyyy")
                hdnToDate.Value = toDate.ToString("MM/dd/yyyy")
            Else
                hdnFromDate.Value = mobjGeneralLedger.GetFiscalDate().ToString("MM/dd/yyyy")
                hdnToDate.Value = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).ToString("MM/dd/yyyy")
            End If

            hdnHeaderTransactionType.Value = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("ddlHeaderTransactionType"), DropDownList).SelectedValue
            hdnHeaderName.Value = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderName"), TextBox).Text
            hdnHeaderDescription.Value = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderDescription"), TextBox).Text
            hdnHeaderSplit.Value = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderSplit"), TextBox).Text
            hdnHeaderAmount.Value = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderAmount"), TextBox).Text
            hdnHeaderNarration.Value = DirectCast(RepGeneralLedger.Controls(0).Controls(0).FindControl("txtHeaderNarration"), TextBox).Text

            LoadGeneralDetailswithPaging()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub RepGeneralLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepGeneralLedger.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                DirectCast(e.Item.FindControl("txtHeaderFrom"), TextBox).Text = hdnFromDate.Value
                DirectCast(e.Item.FindControl("txtHeaderTo"), TextBox).Text = hdnToDate.Value
                DirectCast(e.Item.FindControl("txtHeaderName"), TextBox).Text = hdnHeaderName.Value
                If Not DirectCast(e.Item.FindControl("ddlHeaderTransactionType"), DropDownList).Items.FindByValue(hdnHeaderTransactionType.Value) Is Nothing Then
                    DirectCast(e.Item.FindControl("ddlHeaderTransactionType"), DropDownList).Items.FindByValue(hdnHeaderTransactionType.Value).Selected = True
                End If
                DirectCast(e.Item.FindControl("txtHeaderDescription"), TextBox).Text = hdnHeaderDescription.Value
                DirectCast(e.Item.FindControl("txtHeaderSplit"), TextBox).Text = hdnHeaderSplit.Value
                DirectCast(e.Item.FindControl("txtHeaderAmount"), TextBox).Text = hdnHeaderAmount.Value
                DirectCast(e.Item.FindControl("txtHeaderNarration"), TextBox).Text = hdnHeaderNarration.Value
            ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim lintOppId As Integer
                Dim lintOppBizDocsId As Integer
                Dim lintCategoryHDRID As Integer
                Dim lnkType As LinkButton = e.Item.FindControl("lnkType")
                Dim lnkDetail As LinkButton = e.Item.FindControl("lnkDetail")

                Dim sArgument As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numTransactionID"))
                Dim ddlReconStatus As DropDownList = e.Item.FindControl("ddlReconStatus")

                If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numTransactionId")) = -1 Or ddlType.SelectedValue <> "01010101" Then
                    ddlReconStatus.Visible = False
                Else
                    CType(e.Item.FindControl("lblReconStatus"), Label).Text = IIf(DataBinder.Eval(e.Item.DataItem, "bitReconcile"), "R", IIf(DataBinder.Eval(e.Item.DataItem, "bitCleared"), "C", " "))
                End If

                Dim btnDeleteAction As LinkButton = e.Item.FindControl("btnDeleteAction")
                Dim lnkDelete As LinkButton = e.Item.FindControl("lnkDeleteAction")

                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    CType(e.Item.FindControl("lblType"), Label).Visible = True
                    lnkType.Visible = False
                    lnkDetail.Visible = False
                End If

                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDeleteAction.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                End If

                If Not dtFYClosingDate = Nothing And CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numTransactionId")) > 0 Then
                    Dim lblJDate As Label = e.Item.FindControl("lblJDate")
                    If lblJDate.Text <> "" Then
                        If dtFYClosingDate >= DateFromFormattedDate(lblJDate.Text, Session("DateFormat")) Then
                            btnDeleteAction.Visible = False
                            lnkDelete.Visible = False
                            CType(e.Item.FindControl("lblFYClosed"), Label).Visible = True
                        End If
                    End If
                End If

                If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numTransactionId")) < 0 Then 'Hide buttons for total row 
                    btnDeleteAction.Visible = False
                    lnkDelete.Visible = False
                    lnkDetail.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Try
            BindCOA()
            'If DisplayMode = 0 Then
            '    'ddlCOA.SelectedValue = 1
            'LoadGeneralDetails()
            LoadGeneralDetailswithPaging()
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Function LastDayOfMonthFromDateTime(ByVal dateTime As Date) As Date
        Dim firstDayOfTheMonth As New Date(dateTime.Year, dateTime.Month, 1)
        Return firstDayOfTheMonth.AddMonths(1).AddDays(-1)
    End Function

    'Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
    '    ExportToExcel.RepeaterToExcel(RepGeneralLedger, Response)
    'End Sub

    Private Sub LoadGeneralDetailswithPaging()
        Try
            LoadClosingYear()
            Dim TransCountForAccount As Integer
            Dim perLoadCount As Integer = 0

            Dim dsGL As DataSet
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.DomainID = intDomainID

            mobjGeneralLedger.byteMode = 0
            mobjGeneralLedger.CurrentPage = 1
            mobjGeneralLedger.PageSize = 1
            mobjGeneralLedger.TotalRecords = 0
            mobjGeneralLedger.ItemCode = CCommon.ToLong(hdnItemCode.Value)
            mobjGeneralLedger.FromDate = CDate(hdnFromDate.Value)
            mobjGeneralLedger.ToDate = CDate(hdnToDate.Value & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            Dim strAccountID As String = hdnAccounts.Value
            mobjGeneralLedger.AccountIds = IIf(ddlCOA.SelectedValue = 1, strAccountID, ddlCOA.SelectedValue)
            mobjGeneralLedger.DivisionID = lngDivisionID
            mobjGeneralLedger.ReconStatus = CCommon.ToString(hdnReconStatus.Value)
            mobjGeneralLedger.AccountClass = lngAccountClassID
            mobjGeneralLedger.FilterTransactionType = CCommon.ToShort(hdnHeaderTransactionType.Value)
            mobjGeneralLedger.FilterName = hdnHeaderName.Value
            mobjGeneralLedger.FilterDescription = hdnHeaderDescription.Value
            mobjGeneralLedger.FilterSplit = hdnHeaderSplit.Value
            mobjGeneralLedger.FilterAmount = hdnHeaderAmount.Value
            mobjGeneralLedger.FilterNarration = hdnHeaderNarration.Value
            dsGL = mobjGeneralLedger.GetGeneralLedger_VirtualLoad() 'GetChartAcntDetailsForGL()
            dtChartAcntDetails = dsGL.Tables(1).Clone()

            Dim strAccIds As String = ""

            For Each dr As DataRow In dsGL.Tables(0).Rows
                strAccIds += CCommon.ToString(dr("numAccountId")) + ","

            Next
            hdnAccounts.Value = strAccIds

            Dim drow As DataRow
            Dim TotTransCountForAccount As Integer = 0
            Dim AccountId As Long

            drow = dtChartAcntDetails.NewRow
            drow("bitReconcile") = 0
            drow("bitCleared") = 0
            drow("numTransactionId") = 1
            dtChartAcntDetails.Rows.Add(drow)

            RepGeneralLedger.DataSource = dtChartAcntDetails
            RepGeneralLedger.DataBind()

            PersistTable.Clear()
            PersistTable.Add(ddlType.ID, ddlType.SelectedValue)
            PersistTable.Add(ddlCOA.ID, ddlCOA.SelectedValue)
            PersistTable.Add("AccountIDs", If(ddlCOA.SelectedValue = "1", hdnAccounts.Value, ""))
            PersistTable.Add("FromDate", hdnFromDate.Value)
            PersistTable.Add("ToDate", hdnToDate.Value)
            PersistTable.Add("FilterTransactionType", hdnHeaderTransactionType.Value)
            PersistTable.Add("FiterName", hdnHeaderName.Value)
            PersistTable.Add("FiterDescription", hdnHeaderDescription.Value)
            PersistTable.Add("FiterSplit", hdnHeaderSplit.Value)
            PersistTable.Add("FiterAmount", hdnHeaderAmount.Value)
            PersistTable.Add("FiterNarration", hdnHeaderNarration.Value)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    <WebMethod()>
    Public Shared Function WebMethodLoadGeneralDetails(ByVal DomainID As Integer, ByVal pageIndex As Integer, ByVal PrevBalance As Decimal, ByVal strDateFormat As String, ByVal FromDate As String,
                                                       ByVal ToDate As String, ByVal DivisionId As Long, ByVal AccountID As String, ByVal ItemCode As String, ByVal ReconStatus As String, ByVal AccountClassID As Long,
                                                       ByVal filterTransactionType As Short, ByVal fiterName As String, ByVal fiterDescription As String, ByVal fiterSplit As String, ByVal fiterAmount As String, ByVal fiterNarration As String) As String
        Try
            Dim mobjGeneralLedger1 As New GeneralLedger
            Dim dtChartAcntDetails As DataTable
            Dim dtFYClosingDate1 As DateTime
            Dim DisplayMode As Integer = 1
            'Dim strDateFormat As String = "DD/MM/YYYY"
            Dim FromDate1 As New Date
            Dim ToDate1 As New Date
            'Dim AccountID As String = "1213"
            'Dim ddlCOA As String = "1"
            'Dim pageIndex As Integer = 2
            'Dim ReconStatus As String = "A"
            Dim TotTransCountForAccount As Integer
            Dim perLoadCount As Integer = 0

            Dim dt1 As New DateTime
            FromDate1 = DateFromFormattedDate(FromDate, strDateFormat)
            ToDate1 = DateFromFormattedDate(ToDate, strDateFormat)

            Dim objCOA As New ChartOfAccounting
            objCOA.FinancialYearID = 0
            objCOA.DomainID = DomainID
            objCOA.Mode = 4
            Dim ds As DataSet = objCOA.GetFinancialYear()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("FYClosingDate").ToString <> "" Then
                    dtFYClosingDate1 = DateFromFormattedDate(ds.Tables(0).Rows(0)("FYClosingDate").ToString, strDateFormat)
                End If
            End If

            Dim dsGL As DataSet
            If mobjGeneralLedger1 Is Nothing Then mobjGeneralLedger1 = New GeneralLedger

            mobjGeneralLedger1.DomainID = DomainID
            mobjGeneralLedger1.FromDate = FromDate1
            mobjGeneralLedger1.ToDate = CDate(ToDate1 & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            Dim strAccountID As String = AccountID
            mobjGeneralLedger1.AccountIds = strAccountID 'IIf(ddlCOA = 1, strAccountID, AccountID)
            mobjGeneralLedger1.DivisionID = DivisionId

            mobjGeneralLedger1.byteMode = 1
            mobjGeneralLedger1.CurrentPage = pageIndex
            mobjGeneralLedger1.PageSize = 100
            mobjGeneralLedger1.TotalRecords = 0
            mobjGeneralLedger1.AccountClass = AccountClassID
            mobjGeneralLedger1.ItemCode = CCommon.ToLong(ItemCode)

            Dim TotPages As Integer

            mobjGeneralLedger1.ReconStatus = ReconStatus
            mobjGeneralLedger1.FilterTransactionType = filterTransactionType
            mobjGeneralLedger1.FilterName = fiterName
            mobjGeneralLedger1.FilterDescription = fiterDescription
            mobjGeneralLedger1.FilterSplit = fiterSplit
            mobjGeneralLedger1.FilterAmount = fiterAmount
            mobjGeneralLedger1.FilterNarration = fiterNarration
            dsGL = mobjGeneralLedger1.GetGeneralLedger_VirtualLoad() 'GetChartAcntDetailsForGL()
            dtChartAcntDetails = dsGL.Tables(1).Clone()

            'Dim dtTransCount As DataTable
            'dtTransCount = dsGL.Tables(2)

            'TotPages = mobjGeneralLedger1.TotalRecords / mobjGeneralLedger1.PageSize
            'If mobjGeneralLedger1.TotalRecords Mod mobjGeneralLedger1.PageSize > 0 Then
            '    TotPages += 1

            'End If

            'Dim COA_AccountId As Long
            Dim TransCountForAccount As Integer

            Dim drow As DataRow

            Dim dtAccountDetails As New DataTable("AccountDetails")
            Dim drAccountDetails As DataRow
            dtAccountDetails.Columns.Add("AccountID")
            dtAccountDetails.Columns.Add("ItemID")
            dtAccountDetails.Columns.Add("TransactionLoaded")
            dtAccountDetails.Columns.Add("PrevBalance")
            If CCommon.ToLong(ItemCode) = 0 Then

                For Each dr As DataRow In dsGL.Tables(0).Rows
                    Dim drChild() As DataRow = dsGL.Tables(1).Select("numAccountID=" & dr("numAccountID"), "")
                    'Dim PrevBalance As Double
                    TransCountForAccount = 0
                    TotTransCountForAccount = 0
                    If pageIndex <= 1 Then
                        drow = dtChartAcntDetails.NewRow
                        drow("numDomainID") = dr("numDomainID")
                        drow("numAccountId") = dr("numAccountId")
                        drow("TransactionType") = ""
                        drow("CompanyName") = ""
                        drow("Date") = "<b>" & dr("vcAccountName") & "</b>"
                        drow("varDescription") = ""
                        drow("Narration") = ""
                        'drow("BizDocID") = ""
                        drow("TranRef") = ""
                        drow("TranDesc") = ""
                        drow("numDebitAmt") = ""
                        drow("numCreditAmt") = ""
                        drow("vcAccountName") = "" 'dr("vcAccountName")
                        drow("balance") = "" '"<b>" & dr("mnOpeningBalance") & "</b>"
                        drow("bitReconcile") = 0
                        drow("bitCleared") = 0
                        drow("numTransactionId") = -1

                        If DisplayMode = 2 Then
                            If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
                        Else
                            dtChartAcntDetails.Rows.Add(drow)
                        End If

                        drow = dtChartAcntDetails.NewRow
                        drow("numDomainID") = dr("numDomainID")
                        drow("numAccountId") = dr("numAccountId")
                        drow("TransactionType") = ""
                        drow("CompanyName") = ""
                        drow("Date") = "Opening Balance"
                        drow("varDescription") = ""
                        drow("Narration") = ""
                        'drow("BizDocID") = ""
                        drow("TranRef") = ""
                        drow("TranDesc") = ""
                        drow("numDebitAmt") = "<b>" & String.Format("{0:#,##0.00}", dr("mnOpeningBalance")) & "</b>"
                        drow("numCreditAmt") = ""
                        drow("vcAccountName") = "" 'dr("vcAccountName")
                        drow("balance") = "<b>" & String.Format("{0:#,##0.00}", dr("mnOpeningBalance")) & "</b>"
                        drow("bitReconcile") = 0
                        drow("bitCleared") = 0
                        drow("numTransactionId") = -1
                        PrevBalance = dr("mnOpeningBalance")
                        If DisplayMode = 2 Then
                            If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
                        Else
                            dtChartAcntDetails.Rows.Add(drow)
                        End If

                    End If

                    'PrevBalance = AccClosingBalance(COA_AccountId)

                    Dim i As Integer = 0
                    Dim boolIsCreditTransaction As Boolean = False
                    For Each childRow As DataRow In drChild
                        If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                            childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numCreditAmt")) * -1))
                            boolIsCreditTransaction = True
                        Else
                            boolIsCreditTransaction = False
                        End If
                        If i = 0 Then
                            'PrevBalance = dr("mnOpeningBalance")
                            childRow("balance") = PrevBalance + childRow("numDebitAmt")
                            PrevBalance = childRow("balance")
                        Else
                            childRow("balance") = PrevBalance + childRow("numDebitAmt")
                            PrevBalance = childRow("balance")
                        End If
                        'added by chintan
                        If boolIsCreditTransaction = True Then
                            childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numDebitAmt"))))
                            childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
                        Else
                            childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numDebitAmt"))))
                            childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
                        End If

                        childRow("balance") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("balance"))))

                        dtChartAcntDetails.ImportRow(childRow)
                        i += 1
                        TransCountForAccount += 1
                        perLoadCount += 1

                    Next

                    If TransCountForAccount < 100 Then
                        drow = dtChartAcntDetails.NewRow
                        drow("numDomainID") = dr("numDomainID")
                        drow("numAccountId") = dr("numAccountId")
                        drow("TransactionType") = ""
                        drow("CompanyName") = ""
                        drow("Date") = "Closing Balance"
                        drow("varDescription") = ""
                        drow("Narration") = ""
                        'drow("BizDocID") = ""
                        drow("TranRef") = ""
                        drow("TranDesc") = ""
                        drow("numDebitAmt") = "" '"<b>" & PrevBalance & "</b>"
                        drow("numCreditAmt") = ""
                        drow("vcAccountName") = "" 'dr("vcAccountName")
                        drow("balance") = "<b>" & String.Format("{0:#,##0.00}", (PrevBalance)) & "</b>"
                        drow("bitReconcile") = 0
                        drow("bitCleared") = 0
                        drow("numTransactionId") = -1

                        If DisplayMode = 2 Then
                            If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
                        Else
                            dtChartAcntDetails.Rows.Add(drow)
                        End If

                    End If
                    TotPages = ((pageIndex - 1) * 100) + TransCountForAccount
                    drAccountDetails = dtAccountDetails.NewRow
                    drAccountDetails("AccountID") = dr("numAccountId")
                    drAccountDetails("TransactionLoaded") = TotPages
                    drAccountDetails("PrevBalance") = PrevBalance
                    drAccountDetails("ItemID") = "0"

                    dtAccountDetails.Rows.Add(drAccountDetails)
                Next
            Else
                Dim dtTransactions As DataTable
                dtChartAcntDetails = dsGL.Tables(1)

                For Each childRow As DataRow In dtChartAcntDetails.Rows
                    If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                        childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numCreditAmt")) * -1))
                        childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
                    Else
                        childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numDebitAmt"))))
                        childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
                    End If
                    TransCountForAccount += 1

                Next

                TotPages = ((pageIndex - 1) * 100) + TransCountForAccount
                drAccountDetails = dtAccountDetails.NewRow
                drAccountDetails("AccountID") = AccountID
                drAccountDetails("TransactionLoaded") = TotPages
                drAccountDetails("PrevBalance") = 0
                drAccountDetails("ItemID") = ItemCode

                dtAccountDetails.Rows.Add(drAccountDetails)

            End If
            Dim dt123 As DataTable
            dt123 = dtChartAcntDetails.Copy()
            Dim dsChartAcntDetails As New DataSet
            dsChartAcntDetails.Tables.Add(dt123)
            Dim dtCopyAccDet As DataTable
            dtCopyAccDet = dtAccountDetails.Copy()

            dsChartAcntDetails.Tables.Add(dtCopyAccDet)

            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsChartAcntDetails, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try

    End Function

    <WebMethod()>
    Public Shared Function WebMethodGetTransactionDetail(ByVal DomainID As Integer, ByVal TransactionID As Integer) As String

        Dim mobjGeneralLedger As New GeneralLedger
        Try

            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.DomainID = DomainID
            mobjGeneralLedger.TransactionID = TransactionID

            Dim ds As DataSet = mobjGeneralLedger.GetGLTransactionDetailById() 'GetChartAcntDetailsForGL()
            Dim dt As DataTable = ds.Tables(0).Copy()

            Dim dsTransactionDetail As New DataSet
            dsTransactionDetail.Tables.Add(dt)
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsTransactionDetail, Formatting.None)
            Return json

        Catch ex As Exception
            Dim strError As String = ""
            strError = ex.Message
            Throw New Exception(strError)

        End Try

    End Function

    <WebMethod()>
    Public Shared Function WebMethodGetDepositDetails(ByVal DomainID As Integer, ByVal lintDepositId As Integer) As String
        Try

            Dim objMakeDeposit As New MakeDeposit
            objMakeDeposit.DepositId = lintDepositId
            objMakeDeposit.DomainID = DomainID
            objMakeDeposit.Mode = 0
            Dim dtMakeDeposit As DataTable = objMakeDeposit.GetDepositDetails().Tables(0)

            Dim dt As DataTable = dtMakeDeposit.Copy()
            Dim dsDepositDetails As New DataSet
            dsDepositDetails.Tables.Add(dt)
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsDepositDetails, Formatting.None)
            Return json

            'If dtMakeDeposit.Rows.Count > 0 Then
            '    If CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 2 Then
            '        Response.Redirect("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" & lintDepositId)
            '    ElseIf CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 1 Then
            '        Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&ChartAcntId=" & lintChartAcntId & "&DepositId=" & lintDepositId)
            '    End If
            'End If

        Catch ex As Exception
            Dim strError As String = ""
            strError = ex.Message
            Throw New Exception(strError)

        End Try

    End Function

    <WebMethod()>
    Public Shared Function WebMethodDeleteTransaction(ByVal DomainID As Integer, ByVal UserContactID As Integer, ByVal linJournalId As Integer,
                                                     ByVal lintBillPaymentID As Integer, ByVal lintDepositId As Integer, ByVal lintCheckId As Integer,
                                                       ByVal lintBillID As Integer, ByVal lintCategoryHDRID As Integer, ByVal lintReturnID As Integer) As String
        Try
            Dim objJournalEntry As New JournalEntry
            objJournalEntry.JournalId = linJournalId
            objJournalEntry.BillPaymentID = lintBillPaymentID
            objJournalEntry.DepositId = lintDepositId
            objJournalEntry.CheckHeaderID = lintCheckId
            objJournalEntry.BillID = lintBillID
            objJournalEntry.CategoryHDRID = lintCategoryHDRID
            objJournalEntry.ReturnID = lintReturnID
            objJournalEntry.DomainID = DomainID
            objJournalEntry.UserCntID = UserContactID
            objJournalEntry.DeleteJournalEntryDetails()

        Catch ex As Exception
            Dim strError As String = ""
            'strError = ex.Message

            If ex.Message = "BILL_PAID" Then
                ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                strError = "This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first."

            ElseIf ex.Message = "Undeposited_Account" Then
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                strError = "This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first."

            ElseIf ex.Message = "CreditMemo_PAID" Then
                ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Credit Memo applied.');", True)
                strError = "Credit Memo applied."

            ElseIf ex.Message = "InventoryAdjustment" Then
                ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You can not delete inventory adjustment for item, since it is automatically managed by Biz as inventory count goes up or down.');", True)
                strError = "You can not delete inventory adjustment for item, since it is automatically managed by Biz as inventory count goes up or down.."

            ElseIf ex.Message = "OpeningBalance" Then
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You can not delete Opening balance journal of Chart of Account, Your option is to update opening balance of respective chart of account to zero.');", True)
                strError = "You can not delete Opening balance journal of Chart of Account, Your option is to update opening balance of respective chart of account to zero."
            Else
                strError = ex.Message

            End If

            Throw New Exception(strError)

        End Try
        Return "Transaction details deleted Successfully!.,"

    End Function

    Private Sub ibExportExcel_Click(sender As Object, e As System.EventArgs) Handles ibExportExcel.Click
        Try
            Dim dsGL As DataSet
            Dim mobjGeneralLedger As New GeneralLedger

            mobjGeneralLedger.DomainID = intDomainID
            mobjGeneralLedger.FromDate = CDate(hdnFromDate.Value)
            mobjGeneralLedger.ToDate = CDate(hdnToDate.Value & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            mobjGeneralLedger.byteMode = 1
            mobjGeneralLedger.CurrentPage = 1
            mobjGeneralLedger.PageSize = 100000000
            mobjGeneralLedger.TotalRecords = 0
            mobjGeneralLedger.ItemCode = CCommon.ToLong(hdnItemCode.Value)
            Dim strAccountID As String = hdnAccounts.Value
            mobjGeneralLedger.AccountIds = IIf(ddlCOA.SelectedValue = 1, strAccountID, ddlCOA.SelectedValue)
            mobjGeneralLedger.DivisionID = lngDivisionID
            mobjGeneralLedger.ReconStatus = CCommon.ToString(hdnReconStatus.Value)
            mobjGeneralLedger.AccountClass = lngAccountClassID
            mobjGeneralLedger.FilterTransactionType = CCommon.ToShort(hdnHeaderTransactionType.Value)
            mobjGeneralLedger.FilterName = hdnHeaderName.Value
            mobjGeneralLedger.FilterDescription = hdnHeaderDescription.Value
            mobjGeneralLedger.FilterSplit = hdnHeaderSplit.Value
            mobjGeneralLedger.FilterAmount = hdnHeaderAmount.Value
            mobjGeneralLedger.FilterNarration = hdnHeaderNarration.Value
            dsGL = mobjGeneralLedger.GetGeneralLedger_VirtualLoad()

            Dim dtChartAcntDetails As New DataTable
            CCommon.AddColumnsToDataTable(dtChartAcntDetails, "Date,TransactionType,Name,MemoDescription,Split,Amount,Balance,Narration,Reconcile")

            Dim drow As DataRow
            For Each dr As DataRow In dsGL.Tables(0).Rows
                Dim drChild() As DataRow = dsGL.Tables(1).Select("numAccountID=" & dr("numAccountID"), "")

                drow = dtChartAcntDetails.NewRow

                drow("Date") = dr("vcAccountName")
                drow("TransactionType") = ""
                drow("Name") = ""
                drow("Amount") = ""
                drow("Balance") = "" '"<b>" & dr("mnOpeningBalance") & "</b>"
                drow("MemoDescription") = ""
                drow("Split") = ""
                drow("Narration") = ""
                drow("Reconcile") = ""

                If DisplayMode = 2 Then
                    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
                Else
                    dtChartAcntDetails.Rows.Add(drow)
                End If


                drow = dtChartAcntDetails.NewRow

                drow("Date") = "Opening Balance"
                drow("TransactionType") = ""
                drow("Name") = ""
                drow("Amount") = ReturnMoney(dr("mnOpeningBalance"))
                drow("Balance") = ReturnMoney(dr("mnOpeningBalance"))
                drow("MemoDescription") = ""
                drow("Split") = ""
                drow("Narration") = ""
                drow("Reconcile") = ""

                If DisplayMode = 2 Then
                    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
                Else
                    dtChartAcntDetails.Rows.Add(drow)
                End If


                Dim PrevBalance As Double
                Dim i As Integer = 0
                Dim boolIsCreditTransaction As Boolean = False
                For Each childRow As DataRow In drChild
                    If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                        childRow("numDebitAmt") = ReturnMoney(CCommon.ToDouble(childRow("numCreditAmt")) * -1)
                        boolIsCreditTransaction = True
                    Else
                        boolIsCreditTransaction = False
                    End If
                    If i = 0 Then
                        PrevBalance = dr("mnOpeningBalance")
                        childRow("balance") = PrevBalance + childRow("numDebitAmt")
                        PrevBalance = childRow("balance")
                    Else
                        childRow("balance") = PrevBalance + childRow("numDebitAmt")
                        PrevBalance = childRow("balance")
                    End If
                    'added by chintan
                    If boolIsCreditTransaction = True Then
                        childRow("numDebitAmt") = childRow("numDebitAmt")
                    Else
                        childRow("numDebitAmt") = childRow("numDebitAmt")
                    End If

                    childRow("balance") = ReturnMoney(CCommon.ToDouble(childRow("balance")))
                    dtChartAcntDetails.ImportRow(childRow)

                    drow = dtChartAcntDetails.NewRow

                    drow("Date") = childRow("Date")
                    drow("TransactionType") = childRow("TransactionType")
                    drow("Name") = childRow("CompanyName")
                    drow("Amount") = childRow("numDebitAmt")
                    drow("Balance") = childRow("balance")
                    drow("MemoDescription") = childRow("TranDesc")
                    drow("Split") = childRow("vcSplitAccountName")
                    drow("Narration") = childRow("TranRef") + " " + childRow("Narration")
                    drow("Reconcile") = IIf(childRow("bitReconcile"), "R", IIf(childRow("bitCleared"), "C", " "))
                    dtChartAcntDetails.Rows.Add(drow)

                    i += 1
                Next
                drow = dtChartAcntDetails.NewRow

                drow("Date") = "Closing Balance"
                drow("TransactionType") = ""
                drow("Name") = ""
                drow("Amount") = ""
                drow("Balance") = ReturnMoney(PrevBalance)
                drow("MemoDescription") = ""
                drow("Split") = ""
                drow("Narration") = ""
                drow("Reconcile") = ""

                If DisplayMode = 2 Then
                    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
                Else
                    dtChartAcntDetails.Rows.Add(drow)
                End If
            Next

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=GeneralLedger" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".csv")
            Response.Charset = ""
            Response.ContentType = "text/csv"
            Dim stringWrite As New System.IO.StringWriter
            CSVExport.ProduceCSV(dtChartAcntDetails, stringWrite, True)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub imgBtnExportPDF_Click(sender As Object, e As EventArgs) Handles imgBtnExportPDF.Click
        Try
            Dim dsGL As DataSet
            Dim mobjGeneralLedger As New GeneralLedger

            mobjGeneralLedger.DomainID = intDomainID
            mobjGeneralLedger.FromDate = CDate(hdnFromDate.Value)
            mobjGeneralLedger.ToDate = CDate(hdnToDate.Value & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            mobjGeneralLedger.byteMode = 1
            mobjGeneralLedger.CurrentPage = 1
            mobjGeneralLedger.PageSize = 100000000
            mobjGeneralLedger.TotalRecords = 0
            mobjGeneralLedger.ItemCode = CCommon.ToLong(hdnItemCode.Value)
            Dim strAccountID As String = hdnAccounts.Value
            mobjGeneralLedger.AccountIds = IIf(ddlCOA.SelectedValue = 1, strAccountID, ddlCOA.SelectedValue)
            mobjGeneralLedger.DivisionID = lngDivisionID
            mobjGeneralLedger.ReconStatus = CCommon.ToString(hdnReconStatus.Value)
            mobjGeneralLedger.AccountClass = lngAccountClassID
            mobjGeneralLedger.FilterTransactionType = CCommon.ToShort(hdnHeaderTransactionType.Value)
            mobjGeneralLedger.FilterName = hdnHeaderName.Value
            mobjGeneralLedger.FilterDescription = hdnHeaderDescription.Value
            mobjGeneralLedger.FilterSplit = hdnHeaderSplit.Value
            mobjGeneralLedger.FilterAmount = hdnHeaderAmount.Value
            mobjGeneralLedger.FilterNarration = hdnHeaderNarration.Value
            dsGL = mobjGeneralLedger.GetGeneralLedger_VirtualLoad()

            

            If Not dsGL Is Nothing AndAlso dsGL.Tables.Count > 1 AndAlso dsGL.Tables(0).Rows.Count > 0 Then
                Dim html As String
                html = "<html><head><style type='text/css'>table tr th {font-weight: bold;background-color: #ebebeb;text-align: center;border: 1px solid #e4e4e4;border-bottom-width: 2px;padding: 8px;line-height: 1.42857143;} table tr td {padding: 8px;border: 1px solid #e4e4e4;} table tr {page-break-inside: avoid;} .text {text-align:center;} .amount {text-align:right;}</style></head><body><table style=""width:100%; padding:0px; border-collapse:collapse;""><tr><th>Date</th><th>Transaction Type</th><th>Name</th><th>Memo/Description</th><th>Amount</th><th>Balance</th></tr>"

                Dim drow As DataRow
                For Each dr As DataRow In dsGL.Tables(0).Rows
                    Dim drChild() As DataRow = dsGL.Tables(1).Select("numAccountID=" & dr("numAccountID"), "")

                    html += "<tr><td colspan='6' style='font-weight:bold;background-color:#f7f7f7'>" & dr("vcAccountName") & "</td></tr>"
                    html += "<tr><td style='font-weight:bold;'>Opening Balance</td><td></td><td></td><td></td><td class='amount' style='font-weight:bold;'>" & ReturnMoney(dr("mnOpeningBalance")) & "</td><td class='amount' style='font-weight:bold;'>" & ReturnMoney(dr("mnOpeningBalance")) & "</td></tr>"


                    Dim PrevBalance As Double
                    Dim i As Integer = 0
                    Dim boolIsCreditTransaction As Boolean = False
                    For Each childRow As DataRow In drChild
                        If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                            childRow("numDebitAmt") = ReturnMoney(CCommon.ToDouble(childRow("numCreditAmt")) * -1)
                            boolIsCreditTransaction = True
                        Else
                            boolIsCreditTransaction = False
                        End If
                        If i = 0 Then
                            PrevBalance = dr("mnOpeningBalance")
                            childRow("balance") = PrevBalance + childRow("numDebitAmt")
                            PrevBalance = childRow("balance")
                        Else
                            childRow("balance") = PrevBalance + childRow("numDebitAmt")
                            PrevBalance = childRow("balance")
                        End If
                        'added by chintan
                        If boolIsCreditTransaction = True Then
                            childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
                        Else
                            childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
                        End If

                        childRow("balance") = ReturnMoney(CCommon.ToDouble(childRow("balance")))

                        html += "<tr><td class='text'>" & childRow("Date") & "</td><td class='text'>" & childRow("TransactionType") & "</td><td class='text'>" & childRow("CompanyName") & "</td><td class='text'>" & childRow("TranDesc") & "</td><td class='amount'>" & childRow("numDebitAmt") & "</td><td class='amount'>" & childRow("balance") & "</td></tr>"

                        i += 1
                    Next

                    html += "<tr><td style='font-weight:bold;'>Closing Balance</td><td></td><td></td><td></td><td></td><td style='font-weight:bold' class='amount'>" & ReturnMoney(PrevBalance) & "</td></tr>"
                Next

                html += "</table></body></html>"

                Dim fileName As String = "GeneralLedger" & Session("DomainID") & Format(Now, "ddmmyyyyhhmmss") & ".pdf"

                Dim objHtmlToPDF As New HTMLToPDF
                objHtmlToPDF.ConvertHTML2PDF(Session("DomainID"), "<html><head></head><body><div  style='width:100%; text-align:center; font-weight:bold; font-size:20px;'><b>" & CCommon.ToString(Session("DomainName")) & "</b><br/>" & "General Ledger" & "<br/>" & "From " & FormattedDateFromDate(CDate(hdnFromDate.Value), Session("DateFormat")) & " to " & FormattedDateFromDate(CDate(hdnToDate.Value), Session("DateFormat")) & "</div></body></html>", html, "", fileName)

                Dim strFilePhysicalLocation As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & fileName


                Response.Clear()
                Response.ClearContent()
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment; filename=" & fileName)
                'Response.Write(Session("Attachements"))

                Response.WriteFile(strFilePhysicalLocation)

                Response.End()
            End If

            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class