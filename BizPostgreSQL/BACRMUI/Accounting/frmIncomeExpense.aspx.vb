﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports System.IO
Imports System.Text
Imports ClosedXML.Excel
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports BACRM.BusinessLogic.Admin


Public Class frmIncomeExpense
    Inherits BACRMPage

#Region "Member Variables"
    Private mobjProfitLoss As ProfitLoss
#End Region

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Add(ddlColumnType.ID, ddlColumnType.SelectedValue)
            PersistTable.Add(ddlDateRange.ID, ddlDateRange.SelectedValue)
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack Then
                GetUserRightsForPage(35, 91)
                BindUserLevelClassTracking()

                Dim lobjGeneralLedger As New GeneralLedger
                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now) ' DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)

                If GetQueryStringVal("FromDate") <> "" And GetQueryStringVal("ToDate") <> "" Then

                    calFrom.SelectedDate = CDate(Replace(GetQueryStringVal("FromDate"), "%27", ""))
                    calTo.SelectedDate = CDate(Replace(GetQueryStringVal("ToDate"), "%27", ""))
                Else
                    If PersistTable.Count > 0 Then
                        ddlColumnType.SelectedValue = PersistTable(ddlColumnType.ID)
                        ddlDateRange.SelectedValue = PersistTable(ddlDateRange.ID)
                        ddlUserLevelClass.SelectedValue = PersistTable(ddlUserLevelClass.ID)

                        Try
                            calFrom.SelectedDate = PersistTable(calFrom.ID)
                            calTo.SelectedDate = PersistTable(calTo.ID)

                            If calFrom.SelectedDate Is Nothing Or calTo.SelectedDate Is Nothing Then
                                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now)
                            End If
                        Catch ex As Exception
                            'Do not throw error when date format which is stored in persist table and Current date formats are different
                            calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.Now)
                        End Try

                        'Clears date range selection if value selected in date range drop down
                        If ddlDateRange.SelectedValue <> "0" Then
                            calFrom.SelectedDate = Nothing
                            calTo.SelectedDate = Nothing
                        End If
                    End If
                End If

                hdnDomainName.Value = CCommon.ToString(Session("DomainName"))

                If ddlDateRange.SelectedValue <> "0" Then
                    Dim fromDate As Date
                    Dim toToDate As Date

                    Select Case ddlDateRange.SelectedValue
                        Case "CurYear"
                            fromDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "PreYear"
                            fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "CurPreYear"
                            fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                            toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                        Case "CuQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth")))
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                        Case "PreQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddDays(-1)
                        Case "CurPreQur"
                            fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                            toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                        Case "ThisMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                            toToDate = fromDate.AddMonths(1).AddDays(-1)
                        Case "LastMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                            toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
                        Case "CurPreMonth"
                            fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                            toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1)
                    End Select

                    SetDateRange(fromDate, toToDate)
                Else
                    SetDateRange(calFrom.SelectedDate, calTo.SelectedDate)
                End If

                BindReport()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindReport()
        Try
            If rtlIncomeExpense.Columns.Count > 1 Then
                For i As Int32 = 1 To rtlIncomeExpense.Columns.Count - 1
                    rtlIncomeExpense.Columns.RemoveAt(1)
                Next
            End If

            Dim type As String = Nothing

            If ddlColumnType.SelectedValue = "Quarter" Then
                type = "Quarter"
            ElseIf ddlColumnType.SelectedValue = "Year" Then
                type = "Year"
            End If

            If mobjProfitLoss Is Nothing Then mobjProfitLoss = New ProfitLoss
            mobjProfitLoss.DomainID = Session("DomainID")

            If pnlAccountingClass.Visible AndAlso ddlUserLevelClass.Items.Count > 0 Then
                mobjProfitLoss.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
            Else
                mobjProfitLoss.AccountClass = 0
            End If

            mobjProfitLoss.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)

            Dim dateFilter As String
            If ddlDateRange.SelectedValue <> "0" Then
                dateFilter = ddlDateRange.SelectedValue
            Else
                dateFilter = "Custom"
                mobjProfitLoss.FromDate = calFrom.SelectedDate
                mobjProfitLoss.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            End If

            Dim dtProfitLoss As DataTable = mobjProfitLoss.GetIncomeExpensesNew(dateFilter, type)





            Dim index As Int32 = 1
            Dim drNetIncome As DataRow = dtProfitLoss.NewRow()
            drNetIncome("ParentId") = ""
            drNetIncome("vcCompundParentKey") = -3
            drNetIncome("numAccountTypeID") = -3
            drNetIncome("vcAccountType") = "Net Income"
            drNetIncome("vcAccountCode") = DBNull.Value
            drNetIncome("LEVEL") = 0
            drNetIncome("numAccountId") = DBNull.Value
            drNetIncome("Struc") = "#-3#"
            drNetIncome("Type") = 2
            drNetIncome("bitTotal") = 1
            drNetIncome("bitIsSubAccount") = 0

            For Each column As DataColumn In dtProfitLoss.Columns
                If column.ColumnName <> "ParentId" And
                    column.ColumnName <> "vcCompundParentKey" And
                    column.ColumnName <> "numAccountTypeID" And
                    column.ColumnName <> "vcAccountType" And
                    column.ColumnName <> "vcAccountCode" And
                    column.ColumnName <> "LEVEL" And
                    column.ColumnName <> "numAccountId" And
                    column.ColumnName <> "Struc" And
                    column.ColumnName <> "Type" AndAlso
                    column.ColumnName <> "bitTotal" AndAlso
                    column.ColumnName <> "bitIsSubAccount" Then
                    Dim boundColumn As New TreeListBoundColumn
                    boundColumn.UniqueName = column.ColumnName
                    boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    boundColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                    boundColumn.DataFormatString = "{0:" & Session("Currency") & "#,##0.00;(" & Session("Currency") & "#,##0.00)}"
                    boundColumn.HeaderStyle.Width = New Unit("110")
                    boundColumn.DataField = column.ColumnName
                    boundColumn.HeaderText = column.ColumnName
                    rtlIncomeExpense.Columns.Insert(index, boundColumn)

                    Dim ordinaryIncome As Double = 0
                    Dim otherIncome As Double = 0

                    If dtProfitLoss.Select("numAccountTypeID = -1").Length > 0 Then
                        ordinaryIncome = CCommon.ToDouble(dtProfitLoss.Select("numAccountTypeID = -1")(0)(column))
                    End If

                    If dtProfitLoss.Select("numAccountTypeID = -2").Length > 0 Then
                        otherIncome = CCommon.ToDouble(dtProfitLoss.Select("numAccountTypeID = -2")(0)(column))
                    End If

                    drNetIncome(column) = ordinaryIncome + otherIncome

                    'Calculate Gross Profit
                    If dtProfitLoss.Select("vcAccountType='Gross Profit' AND numAccountTypeID=-4 AND vcCompundParentKey=-4").Length > 0 Then
                        Dim drGrossProfit As DataRow = dtProfitLoss.Select("vcAccountType='Gross Profit' AND numAccountTypeID=-4 AND vcCompundParentKey=-4")(0)

                        Dim income As Double = dtProfitLoss.Select("vcAccountCode='0103'")(0)(column)
                        Dim Cogs As Double = dtProfitLoss.Select("vcAccountCode='0106'")(0)(column)

                        drGrossProfit(column) = income - Cogs
                    End If

                    index = index + 1
                End If
            Next

            dtProfitLoss.Rows.Add(drNetIncome)

            'hdnMaxLevel.Value = CCommon.ToInteger(dtProfitLoss.Compute("max(LEVEL)", String.Empty))
            rtlIncomeExpense.DataSource = dtProfitLoss
            rtlIncomeExpense.DataBind()
            rtlIncomeExpense.ExpandAllItems()

            If ddlColumnType.SelectedValue = "Year" Then
                rtlIncomeExpense.Columns(0).HeaderStyle.Width = New Unit(260, UnitType.Pixel)
            Else
                rtlIncomeExpense.Columns(0).HeaderStyle.Width = New Unit(100, UnitType.Percentage)
            End If

            lblPeriod.Text = "(" & hdnDateRange.Value & ")"

            UpdatePanelGrid.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False

            If CCommon.ToInteger(HttpContext.Current.Session("DefaultClassType")) > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()

                    Dim objItem As New System.Web.UI.WebControls.ListItem
                    objItem.Text = "-- Select One --"
                    objItem.Value = "0"

                    ddlUserLevelClass.Items.Insert(0, objItem)

                    pnlAccountingClass.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetDateRange(ByVal fromDate As Date, ByVal toDate As Date)
        Try
            hdnDateRange.Value = "From " & FormattedDateFromDate(fromDate, Session("DateFormat")) & " to " & FormattedDateFromDate(toDate, Session("DateFormat"))
            hdnFromDate.Value = fromDate
            hdnToDate.Value = toDate
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function GetStartOfFinancialQtr(monthFinancialYearStartsOn As Integer) As DateTime
        Try
            Dim tempDate As Date = DateTime.Now.Date
            Dim actualMonth = tempDate.Month
            Dim financialYear = tempDate.Year
            Dim difference = actualMonth - monthFinancialYearStartsOn
            If difference < 0 Then
                financialYear -= 1
                difference += 12
            End If
            Dim quarter = Math.Floor(difference / 3)

            Return New DateTime(financialYear, monthFinancialYearStartsOn, 1).AddMonths(quarter * 3)
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

#Region "Event Handlers"

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Try
            If Not calFrom.SelectedDate Is Nothing AndAlso Not calTo.SelectedDate Is Nothing Then

                Dim FromDate As Date = calFrom.SelectedDate
                Dim ToDate As Date = calTo.SelectedDate

                If FromDate > ToDate Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "script", "alert('From date must be smaller than To Date');", True)
                    Exit Sub
                End If

                SetDateRange(calFrom.SelectedDate, calTo.SelectedDate)
                ddlDateRange.ClearSelection()
                lblPeriod.Text = "(" & hdnDateRange.Value & ")"

                BindReport()
            Else
                ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "script", "alert('From date and To Date is required.');", True)
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rtlIncomeExpense_ItemCommand(sender As Object, e As TreeListCommandEventArgs)
        Try
            If e.CommandName = RadTreeList.ExpandCollapseCommandName Then
                'WE ARE HANDLING EXPAND COLLAPSE CLIENT SIDE SO CODE IS NOT REQUIRED
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rtlIncomeExpense_ItemDataBound(sender As Object, e As TreeListItemDataBoundEventArgs)
        Try
            If e.Item.ItemType = TreeListItemType.Item Or e.Item.ItemType = TreeListItemType.AlternatingItem Then

                Dim btn As Button = TryCast(e.Item.FindControl("ExpandCollapseButton"), Button)
                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("Struc")

                If btn IsNot Nothing Then
                    btn.Attributes.Add("UniqueID", uniqueID)
                    btn.Attributes.Add("onclick", "return ExpandCollapse(" + btn.ClientID + ");")
                End If

                DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).Attributes.Add("UniqueID", uniqueID)

                If DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("Type") > 1 Then
                    e.Item.Font.Bold = True
                Else
                    e.Item.Font.Bold = False
                End If

                'Dim level As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("LEVEL")

                'If level <> 0 AndAlso hdnMaxLevel.Value = level Then
                '    e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
                'Else
                '    If level = 0 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#B0B0B0")
                '    ElseIf level = 1 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#C0C0C0")
                '    ElseIf level = 2 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#C8C8C8")
                '    ElseIf level = 3 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#D0D0D0")
                '    ElseIf level = 4 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#D8D8D8")
                '    ElseIf level = 5 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#E0E0E0")
                '    ElseIf level = 6 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("E8E8E8")
                '    ElseIf level = 7 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F0F0")
                '    ElseIf level = 8 Then
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#F8F8F8")
                '    Else
                '        e.Item.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF")
                '    End If
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub imgBtnExportExcel_Click(sender As Object, e As EventArgs) Handles imgBtnExportExcel.Click
        Try
            If Not String.IsNullOrWhiteSpace(txtExportHtml.Text) Then

                Dim fs As New MemoryStream()
                Dim workbook As New XLWorkbook
                Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Income Expense Statement")

                Dim doc As New HtmlAgilityPack.HtmlDocument
                doc.LoadHtml(txtExportHtml.Text)

                'ADD HEADER
                If Not String.IsNullOrEmpty(hdnCustomerName.Value) Then
                    workSheet.Cell(1, 1).Value = String.Format(hdnCustomerName.Value & "{0}" & " Income Expense Statement " & "{1} " & hdnDateRange.Value, Environment.NewLine, Environment.NewLine)
                Else
                    workSheet.Cell(1, 1).Value = String.Format(Session("DomainName") & "{0}" & " Income Expense Statement " & "{1} " & hdnDateRange.Value, Environment.NewLine, Environment.NewLine)
                End If

                workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                workSheet.Cell(1, 1).Style.Font.Bold = True
                workSheet.Cell(1, 1).Style.Font.FontSize = 14
                workSheet.Row(1).AdjustToContents()
                workSheet.Range(workSheet.Cell(1, 1), workSheet.Cell(1, doc.DocumentNode.SelectNodes("//table//tr[1]//td").Count)).Merge()

                Dim i As Int32 = 2
                Dim j As Int32 = 1

                For Each row As HtmlAgilityPack.HtmlNode In doc.DocumentNode.SelectNodes("//table//tr")
                    For Each col As HtmlAgilityPack.HtmlNode In row.SelectNodes("td")


                        If i = 2 Then
                            If col.InnerText.StartsWith("(") AndAlso col.InnerText.EndsWith(")") Then
                                workSheet.Cell(i, j).SetValue("-" & col.InnerText.Replace("(", "").Replace(")", "").Replace("USD", "").Replace("$", "").Trim()).SetDataType(XLCellValues.Text)
                            Else
                                workSheet.Cell(i, j).SetValue(col.InnerText.Replace("USD", "").Replace("$", "").Trim()).SetDataType(XLCellValues.Text)
                            End If

                            workSheet.Cell(i, j).Style.Font.Bold = True
                            workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                            workSheet.Cell(i, j).Style.Fill.BackgroundColor = XLColor.LightGray
                            workSheet.Cell(i, j).DataType = XLCellValues.Text
                        Else
                            If col.InnerText.StartsWith("(") AndAlso col.InnerText.EndsWith(")") Then
                                workSheet.Cell(i, j).Value = "-" & col.InnerText.Replace("(", "").Replace(")", "").Replace("USD", "").Replace("$", "").Trim()
                            Else
                                workSheet.Cell(i, j).Value = col.InnerText.Replace("USD", "").Replace("$", "").Trim()
                            End If
                            If col.Attributes.Count > 0 AndAlso col.Attributes.Contains("bold") AndAlso col.Attributes("bold").Value = "1" Then
                                workSheet.Row(i).Style.Font.Bold = True
                            End If
                        End If

                        workSheet.Cell(i, j).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        workSheet.Cell(i, j).Style.Border.SetOutsideBorderColor(XLColor.Black)


                        j = j + 1
                    Next
                    i = i + 1
                    j = 1
                Next
                workSheet.Columns.AdjustToContents()

                Dim httpResponse As HttpResponse = Response
                httpResponse.Clear()
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                httpResponse.AddHeader("content-disposition", "attachment;filename=IncomeExpense.xlsx")


                Using MemoryStream As New MemoryStream
                    workbook.SaveAs(MemoryStream)
                    MemoryStream.WriteTo(httpResponse.OutputStream)
                    MemoryStream.Close()
                End Using

                httpResponse.Flush()
                httpResponse.End()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlDateRange_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDateRange.SelectedIndexChanged
        Try
            If ddlDateRange.SelectedValue <> "0" Then
                Dim fromDate As Date
                Dim toToDate As Date

                Select Case ddlDateRange.SelectedValue
                    Case "CurYear"
                        fromDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "PreYear"
                        fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "CurPreYear"
                        fromDate = New Date(DateTime.Now.Year - 1, CCommon.ToInteger(Session("FiscalMonth")), 1)
                        toToDate = New Date(DateTime.Now.Year, CCommon.ToInteger(Session("FiscalMonth")), 1).AddYears(1).AddDays(-1)
                    Case "CuQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth")))
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                    Case "PreQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddDays(-1)
                    Case "CurPreQur"
                        fromDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(-3)
                        toToDate = GetStartOfFinancialQtr(CCommon.ToInteger(Session("FiscalMonth"))).AddMonths(3).AddDays(-1)
                    Case "ThisMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1)
                        toToDate = fromDate.AddMonths(1).AddDays(-1)
                    Case "LastMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                        toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
                    Case "CurPreMonth"
                        fromDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1)
                        toToDate = New Date(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1)
                End Select

                SetDateRange(fromDate, toToDate)
                BindReport()
            End If

            calFrom.SelectedDate = Nothing
            calTo.SelectedDate = Nothing
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlColumnType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlColumnType.SelectedIndexChanged
        Try
            PersistTable.Add(ddlColumnType.ID, ddlColumnType.SelectedValue)
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            PersistTable.Add(ddlUserLevelClass.ID, ddlUserLevelClass.SelectedValue)
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If Not radCmbCompany.SelectedItem Is Nothing Then
                hdnCustomerName.Value = radCmbCompany.SelectedItem.Text
            Else
                hdnCustomerName.Value = ""
            End If
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

    'EXPORT TO PDF FUNCTION IS NOT USED RIGHT NOW
    'Private Sub imgBtnExportPDF_Click(sender As Object, e As ImageClickEventArgs) Handles imgBtnExportPDF.Click
    '    Try
    '        If Not String.IsNullOrWhiteSpace(txtExportHtml.Text) Then

    '            Dim doc As New HtmlAgilityPack.HtmlDocument
    '            doc.LoadHtml(txtExportHtml.Text)

    '            Dim columnCount As Int32 = doc.DocumentNode.SelectNodes("//table//tr[1]//td").Count


    '            'Dim sw As New StringWriter()
    '            'Dim hw As New HtmlTextWriter(sw)
    '            'Dim sr As New StringReader(txtExportHtml.Text)
    '            Dim pdfDoc As New Document(PageSize.A4, 0.0F, 0.0F, 0.0F, 0.0F)
    '            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate())
    '            'Dim htmlparser As New HTMLWorker(pdfDoc)
    '            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '            pdfDoc.Open()



    '            Dim header1 As New Paragraph(CCommon.ToString(Session("DomainName")))
    '            header1.Alignment = Element.ALIGN_CENTER

    '            Dim header2 As New Paragraph("Income Expense Statement")
    '            header2.Alignment = Element.ALIGN_CENTER

    '            Dim header3 As New Paragraph("From " & FormattedDateFromDate(calFrom.SelectedDate, Session("DateFormat")) & " to " & FormattedDateFromDate(calTo.SelectedDate, Session("DateFormat")))
    '            header3.Alignment = Element.ALIGN_CENTER

    '            pdfDoc.Add(header1)
    '            pdfDoc.Add(header2)
    '            pdfDoc.Add(header3)


    '            pdfDoc.Add(New Paragraph(" "))


    '            Dim table As New PdfPTable(columnCount) With { _
    '                .WidthPercentage = 98.0F
    '            }

    '            Dim i As Int32 = 1
    '            Dim j As Int32 = 1
    '            Dim cell As PdfPCell
    '            For Each row As HtmlAgilityPack.HtmlNode In doc.DocumentNode.SelectNodes("//table//tr")
    '                For Each col As HtmlAgilityPack.HtmlNode In row.SelectNodes("td")
    '                    If j = 1 Then
    '                        cell = New PdfPCell(New Phrase(col.InnerText))
    '                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER
    '                        cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
    '                        cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY
    '                        table.AddCell(cell)
    '                    Else
    '                        cell = New PdfPCell(New Phrase(col.InnerText))
    '                        cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE

    '                        If i <> 1 Then
    '                            cell.HorizontalAlignment = 2
    '                        Else
    '                            cell.NoWrap = True
    '                        End If

    '                        table.AddCell(cell)
    '                    End If

    '                    i = i + 1
    '                Next
    '                j = j + 1
    '                i = 1
    '            Next


    '            pdfDoc.Add(table)
    '            Response.ContentType = "application/pdf"
    '            Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf")
    '            Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '            'htmlparser.Parse(sr)
    '            pdfDoc.Close()
    '            Response.Write(pdfDoc)
    '            Response.Flush()
    '            Response.Close()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(CCommon.ToString(ex))
    '    End Try
    'End Sub

    
End Class