<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCash.aspx.vb" Inherits=".frmCash" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Cash or Credit Card</title>

    <script language="javascript" type="text/javascript">

        function Save() {
            if (document.form1.ddlMoneyOut.value == 0) {
                alert("Please Select Money Out");
                document.form1.ddlMoneyOut.focus();
                return false;
            }

            if (document.form1.ddlMoneyAsset.value == 0) {
                alert("Please Select MoneyAsset");
                document.form1.ddlMoneyAsset.focus();
                return false;
            }
            if (document.form1.ddlPurchase.value == 0) {
                alert("Please Select Vendor");
                document.form1.ddlPurchase.focus();
                return false;
            }
            if (document.form1.txtAmount.value == "" || document.form1.txtAmount.value == 0.00) {
                alert("Enter the Amount");
                document.form1.txtAmount.focus();
                return false;
            }
            var txtAmount;
            var txtTotalAmt;
            txtAmount = document.form1.txtAmount.value;
            txtTotalAmt = formatCurrencyValues(document.form1.TotalAmt.value);
            //alert(txtAmount);
            txtAmount = txtAmount.replace(/,/g, "");
            txtTotalAmt = txtTotalAmt.replace(/,/g, "");
            if (txtAmount != txtTotalAmt) {
                alert("The sum of the Amounts on the detail lines is less than the total Amount by. \n Either click Recalculate to adjust the total Amount, \n or edit the Amounts on the detail lines so they add up to the total Amount");
                return false;
            }

            var idRoot = 'dgJournalEntry';
            var idtxtDr = 'txtDebit';
            var idtxtDebit;
            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtDebit = '_ctl0'
                }
                else {
                    idtxtDebit = '_ctl'
                }

                if (document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtDr).value != "") {
                    if (document.getElementById(idRoot + idtxtDebit + i + '_ddlAccounts').value == 0) {
                        alert('You must select an account for each split line with an amount');
                        return false;
                    }

                    //alert(document.getElementById(idRoot+idtxtDebit+i+'_lblAccountTypeId').value);
                    //if (document.getElementById(idRoot+idtxtDebit+i+'_ddlName').value==0)
                    if ((document.getElementById(idRoot + idtxtDebit + i + '_txtAccountTypeId').value == 814 || document.getElementById(idRoot + idtxtDebit + i + '_txtAccountTypeId').value == 815) && document.getElementById(idRoot + idtxtDebit + i + '_ddlName').value == 0) {
                        alert('You must select an Company name for each split line with an amount');
                        return false;
                    }

                }
            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function formatCurrency(obj) {
            //alert(obj);
            var num;
            num = document.getElementById(obj.id).value;
            //alert(num);
            // document.getElementById('TotalAmt').value=0;            
            //alert(num);
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
        num.substring(num.length - (4 * i + 3));
            document.getElementById(obj.id).value = (((sign) ? '' : '-') + num + '.' + cents);

            return false;
        }


        function Calculate(obj) {
            var idRoot = 'dgJournalEntry';
            var idtxtTail = 'txtDebit';
            var irowCnt;
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var idtxtDebitvalue;
            var CrValue;
            var CrTotValue;
            var txtTotalAmount;
            if (document.getElementById(obj.id).value != "") {
                document.getElementById(obj.id).value = formatCurrencyValues(document.getElementById(obj.id).value);
            }
            txtTotalAmount = document.getElementById("txtAmount").value;
            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtDebit = '_ctl0'
                }
                else {
                    idtxtDebit = '_ctl'
                }

                if (document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtTail).value != "") {
                    txtdebitvalue = document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtTail).value;
                    txtdebitvalue = txtdebitvalue.replace(/,/g, "");
                    itotal += parseFloat(txtdebitvalue);
                }

            }
            //alert(itotal);
            document.getElementById('TotalAmt').value = parseFloat(itotal);

            if (document.getElementById('txtAmount').value == "") {
                document.getElementById('txtAmount').value = parseFloat(itotal);
                formatCurrency(document.getElementById('txtAmount'));
            }
        }



        function SetCreditAmount(obj) {
            //alert(obj.id);
            var h = obj.id;
            var i;
            var idRoot = 'dgJournalEntry';
            var idtxtTail = 'txtDebit';
            var diff;
            var difference;
            var totalAmount;
            var totaltxtAmount;
            var lblTotalAmt;
            i = h.substring(18, 20);
            // alert(i);     
            var AcntTypeId = document.getElementById(idRoot + '_ctl' + i + '_ddlAccounts').value.split("~")
            document.getElementById(idRoot + '_ctl' + i + '_txtAccountTypeId').value = AcntTypeId[1];
            totalAmount = document.getElementById('TotalAmt').value;
            totalAmount = totalAmount.replace(/,/g, "");
            totaltxtAmount = document.getElementById('txtAmount').value;
            totaltxtAmount = totaltxtAmount.replace(/,/g, "");
            //        alert("Total Amt " +totalAmount);
            //        alert("Total TxtAmt " +totaltxtAmount);
            if (totalAmount != totaltxtAmount) {
                if (isNaN(parseFloat(totalAmount))) {
                    totalAmount = 0;
                }

                if (isNaN(parseFloat(totaltxtAmount))) {
                    totaltxtAmount = 0;
                }


                if (parseFloat(totalAmount) != parseFloat(totaltxtAmount)) {
                    if (document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value == "") {
                        diff = parseFloat(totaltxtAmount) - parseFloat(totalAmount);
                        document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value = formatCurrencyValues(parseFloat(diff));
                        lblTotalAmt = parseFloat(totalAmount) + parseFloat(diff);
                        // alert(lblTotalAmt);
                        document.getElementById('TotalAmt').value = parseFloat(lblTotalAmt);
                    }
                }

                //            if (parseFloat(totalAmount) > parseFloat(totaltxtAmount))
                //            {              
                //               difference=parseFloat(totalAmount)-parseFloat(totaltxtAmount);
                //               document.getElementById(idRoot+'_ctl'+i+'_txtDebit').value=formatCurrencyValues(parseFloat(difference));
                //               lblTotalAmt=parseFloat(totaltxtAmount)+parseFloat(difference);
                //               alert(lblTotalAmt);
                //               document.getElementById('TotalAmt').value=parseFloat(lblTotalAmt);
                //            }
            }

        }

        function Recalculate() {
            document.getElementById('txtAmount').value = formatCurrencyValues(document.getElementById('TotalAmt').value);
            return false;
        }

        function formatCurrencyValues(num) {

            num = num.toString().replace(/\$|\,/g, '');

            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
         
    </script>

</head>
<body>
    <form id="form1" runat="server" method="post">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <br />
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Paying out from Cash or Credit Card Account &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="normal1">
                <asp:Label ID="lblBankAcnt" Width="80px" Text="Money Out" runat="server"></asp:Label>
                <asp:DropDownList ID="ddlMoneyOut" Width="200px" runat="server" AutoPostBack="true"
                    CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                    <asp:ListItem Value="1">Cash or Bank Account</asp:ListItem>
                    <asp:ListItem Value="2">Company Credit Card</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add"></asp:Button>
                <asp:Button ID="btnRecalculate" runat="server" CssClass="button" Text="Recalculate">
                </asp:Button>
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                </asp:Button>
                <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
                    Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <br />
                            <table id="tblDetails" runat="server" width="100%" border="0">
                                <tr>
                                    <td class="normal1" align="right">
                                        Money Assets
                                    </td>
                                    <td class="normal1">
                                        <asp:DropDownList ID="ddlMoneyAsset" runat="server" Width="200px" AutoPostBack="true"
                                            CssClass="signup">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;<asp:Label ID="lblBalance" runat="server"></asp:Label>
                                        <asp:Label ID="lblOpeningBalance" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1" align="right">
                                        Reference
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtReference" runat="server" Width="150" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Date
                                    </td>
                                    <td id="tdsales2" runat="server" visible="true">
                                        <BizCalendar:Calendar ID="calFrom" runat="server" />
                                    </td>
                                    <td class="normal1" align="right" id="td1" runat="server" visible="true">
                                        Amount
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAmount" runat="server" Width="150" CssClass="signup" onchange="formatCurrency(this);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Purchase From
                                    </td>
                                    <td cssclass="signup">
                                        <asp:DropDownList ID="ddlPurchase" runat="server" Width="200px" CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="normal1" align="right" id="td2" runat="server" visible="true">
                                        Recurring Template
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlMakeRecurring" Width="150px" runat="server" CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Memo
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtMemo" runat="server" Width="350" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
      
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:DataGrid ID="dgJournalEntry" runat="server" CssClass="dg" Width="100%"
                                AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Accounts">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numJournalId") %>'></asp:Label>
                                            <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionId") %>'></asp:Label>
                                            <asp:Label ID="lblAccountID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numChartAcntId") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlAccounts" Width="180px" runat="server" CssClass="signup"
                                                onchange="SetCreditAmount(this)">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtAccountTypeId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numAcntType") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDebitAmt" runat="server" Style="display: none" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.numDebitAmt")) %>'></asp:Label>
                                            <asp:TextBox ID="txtDebit" TextMode="SingleLine" Width="100" runat="server" CssClass="signup"
                                                onchange="Calculate(this);"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Memo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemo" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.varDescription") %>'></asp:Label>
                                            <asp:TextBox ID="txtMemo" TextMode="SingleLine" Width="100" runat="server" CssClass="signup"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numCustomerId") %>'></asp:Label>
                                            <asp:TextBox ID="txtCompanyName" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                                            <asp:Button ID="btnCompanyName" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                                            <asp:DropDownList ID="ddlName" Width="200px" runat="server" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Relationship">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRelationship" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.varRelation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="TotalAmt" runat="server" />
    </form>
</body>
</html>--%>
