''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Accounting
    Public Class frmCheckPrint
        Inherits BACRMPage
#Region "Variables"
        Dim mintCheckId As Integer
#End Region

        Private Sub frmCheckPrint_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                mintCheckId = Request.QueryString("CheckId")
                LoadRecord()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadRecord()
            Try
                Dim lobjChecks As New Checks
                Dim dtCheckDetails As DataTable

                lobjChecks.CheckId = mintCheckId
                lobjChecks.DomainId = Session("DomainID")
                dtCheckDetails = lobjChecks.GetCheckDetails()
                If dtCheckDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtCheckDetails.Rows(0).Item("datCheckDate")) Then
                        lblDate.Text = dtCheckDetails.Rows(0).Item("datCheckDate")
                    End If

                    If Not IsDBNull(dtCheckDetails.Rows(0).Item("numCustomerId")) Then
                        lobjChecks.DomainId = Session("DomainID")
                        lobjChecks.DivisionId = dtCheckDetails.Rows(0).Item("numCustomerId")
                        lblCompany.Text = lobjChecks.GetCompanyName
                    End If

                    lblMemo.Text = dtCheckDetails.Rows(0).Item("varMemo")
                    lblAmount.Text = ReturnMoney(dtCheckDetails.Rows(0).Item("monCheckAmt"))
                    lblBankRouteNo.Text = dtCheckDetails.Rows(0).Item("numBankRoutingNo")
                    lblBankAccNo.Text = dtCheckDetails.Rows(0).Item("numBankAccountNo")
                    lblCheckNo.Text = dtCheckDetails.Rows(0).Item("numCheckNo")
                    CheckNo.Text = dtCheckDetails.Rows(0).Item("numCheckNo")

                    'Convert NumericValues To Word
                    Dim lNumericToWord As New NumericToWord
                    lblMoneyDescription.Text = lNumericToWord.SpellNumber(Replace(lblAmount.Text, ",", ""))

                    Dim lobjUserAccess As New UserAccess
                    lobjUserAccess.DomainID = Session("DomainId")
                    lobjUserAccess.GetImagePath()
                    imgCompanyLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & lobjUserAccess.CompanyImagePath
                    lobjUserAccess.byteMode = 2
                    imgBankLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & lobjUserAccess.BankImagePath

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
     
        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace