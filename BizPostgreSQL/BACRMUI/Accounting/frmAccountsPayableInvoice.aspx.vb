﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.UserInterface
Imports System.Data.SqlClient

Public Class frmAccountsPayableInvoice
    Inherits BACRMPage

    Dim decAmountPaid, decBalanceDue As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                LoadAccountPayableDetailGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub LoadAccountPayableDetailGrid()
        Dim objVendorPayment As New VendorPayment
        Dim dtARAging As DataTable
        Try
            objVendorPayment.DomainID = Session("DomainID")
            objVendorPayment.Flag = ddlRange.SelectedValue
            objVendorPayment.AccountClass = CCommon.ToLong(GetQueryStringVal("accountClass"))
            objVendorPayment.dtFromDate = Convert.ToDateTime(GetQueryStringVal("fromDate"))
            objVendorPayment.dtTodate = Convert.ToDateTime(GetQueryStringVal("toDate"))
            objVendorPayment.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            dtARAging = objVendorPayment.GetAccountPayableAging_Invoice

            decAmountPaid = 0
            decBalanceDue = 0

            If dtARAging.Rows.Count > 0 Then
                decAmountPaid = dtARAging.Compute("Sum(AmountPaid)", String.Empty)
                decBalanceDue = dtARAging.Compute("Sum(BalanceDue)", String.Empty)
            End If

            'If (dtARAging.Rows.Count > 0) Then
            gvARDetails.DataSource = dtARAging
            gvARDetails.DataBind()
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ibExportExcel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ibExportExcel.Click
        ExportToExcel.DataGridToExcel(gvARDetails, Response)
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Private Sub ddlRange_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlRange.SelectedIndexChanged
        Try
            LoadAccountPayableDetailGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvARDetails_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvARDetails.RowDataBound
        Try
            If e.Row.RowType = ListItemType.Footer Then
                CType(e.Row.FindControl("lblAmountPaid"), Label).Text = ReturnMoney(decAmountPaid)
                CType(e.Row.FindControl("lblBalanceDue"), Label).Text = ReturnMoney(decBalanceDue)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class