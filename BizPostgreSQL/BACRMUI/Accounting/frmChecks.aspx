<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmChecks.aspx.vb" Inherits=".frmChecks"
    MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Write Checks</title>
    <script language="javascript" type="text/javascript">

        function Save() {
            if (document.form1.ddlBankAcnt.value == 0) {
                alert("Select Bank Account Type");
                document.form1.ddlBankAcnt.focus();
                return false;
            }

            if (document.form1.CustomerId.value == 0) {
                alert("Select the Company Name");
                return false;
            }
            if (document.form1.txtAmount.value == "" || document.form1.txtAmount.value == 0.00) {
                alert("Enter the Amount");
                document.form1.txtAmount.focus();
                return false;
            }
            if (document.form1.txtRouting.value == "") {
                alert("Enter the Routing No");
                document.form1.txtRouting.focus();
                return false;
            }

            if (document.form1.txtBankAcc.value == "") {
                alert("Enter the Bank Account No");
                document.form1.txtBankAcc.focus();
                return false;
            }
            if (document.form1.txtCheckNo.value == "") {
                alert("Enter the Check No");
                document.form1.txtCheckNo.focus();
                return false;
            }
            var txtAmount;
            var txtTotalAmt;
            txtAmount = document.form1.txtAmount.value;
            txtTotalAmt = formatCurrencyValues(document.form1.TotalAmt.value);
            //alert(txtAmount);
            txtAmount = txtAmount.replace(/,/g, "");
            txtTotalAmt = txtTotalAmt.replace(/,/g, "");
            if (txtAmount != txtTotalAmt) {
                alert("The sum of the Amounts on the detail lines is less than the total Amount by. \n Either click Recalculate to adjust the total Amount, \n or edit the Amounts on the detail lines so they add up to the total Amount");
                return false;
            }


            var idRoot = 'dgJournalEntry';
            var idtxtDr = 'txtDebit';
            var idtxtDebit;
            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtDebit = '_ctl0'
                }
                else {
                    idtxtDebit = '_ctl'
                }

                if (document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtDr).value != "") {
                    if (document.getElementById(idRoot + idtxtDebit + i + '_ddlAccounts').value == 0) {
                        alert('You must select an account for each split line with an amount');
                        return false;
                    }
                    //if (document.getElementById(idRoot+idtxtDebit+i+'_ddlName').value==0)
                    if ((document.getElementById(idRoot + idtxtDebit + i + '_txtAccountTypeId').value == 814 || document.getElementById(idRoot + idtxtDebit + i + '_txtAccountTypeId').value == 815) && document.getElementById(idRoot + idtxtDebit + i + '_ddlName').value == 0) {
                        alert('You must select an Company name for each split line with an amount');
                        return false;
                    }

                }

            }
        }


        function CheckNo(obj) {
            if (document.all) {
                document.getElementById('CheckNo').innerText = obj.value;
            } else {
                document.getElementById('CheckNo').textContent = obj.value;
            }
        }

        function formatCurrency(obj) {
            //alert(obj);
            var num;
            num = document.getElementById(obj.id).value;
            //alert(num);
            // document.getElementById('TotalAmt').value=0;            
            //alert(num);
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
        num.substring(num.length - (4 * i + 3));
            document.getElementById(obj.id).value = (((sign) ? '' : '-') + num + '.' + cents);
            document.form1.btnWords.click();
            return false;
        }


        function Calculate(obj) {
            var idRoot = 'dgJournalEntry';
            var idtxtTail = 'txtDebit';
            var irowCnt;
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var idtxtDebitvalue;
            var CrValue;
            var CrTotValue;
            var txtTotalAmount;

            if (document.getElementById(obj.id).value != "") {
                document.getElementById(obj.id).value = formatCurrencyValues(document.getElementById(obj.id).value);
            }
            txtTotalAmount = document.getElementById("txtAmount").value;
            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtDebit = '_ctl0'
                }
                else {
                    idtxtDebit = '_ctl'
                }

                if (document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtTail).value != "") {
                    txtdebitvalue = document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtTail).value;
                    txtdebitvalue = txtdebitvalue.replace(/,/g, "");
                    itotal += parseFloat(txtdebitvalue);
                }

            }
            //alert(itotal);
            document.getElementById('TotalAmt').value = parseFloat(itotal);
            //|| (document.getElementById('txtAmount').value<document.getElementById('TotalAmt').value)
            if ((document.getElementById('txtAmount').value == "") || (document.getElementById('txtAmount').value < document.getElementById('TotalAmt').value)) {
                document.getElementById('txtAmount').value = parseFloat(itotal);
                formatCurrency(document.getElementById('txtAmount'));
            }
        }



        function SetCreditAmount(obj) {
            //alert(obj.id);
            var h = obj.id;
            var i;
            var idRoot = 'dgJournalEntry';
            var idtxtTail = 'txtDebit';
            var diff;
            var difference;
            var totalAmount;
            var totaltxtAmount;
            var lblTotalAmt;
            i = h.substring(18, 20);
            // alert(i);     
            var AcntTypeId = document.getElementById(idRoot + '_ctl' + i + '_ddlAccounts').value.split("~")
            document.getElementById(idRoot + '_ctl' + i + '_txtAccountTypeId').value = AcntTypeId[1];
            totalAmount = document.getElementById('TotalAmt').value;
            totalAmount = totalAmount.replace(/,/g, "");
            totaltxtAmount = document.getElementById('txtAmount').value;
            totaltxtAmount = totaltxtAmount.replace(/,/g, "");
            //        alert("Total Amt " +totalAmount);
            //        alert("Total TxtAmt " +totaltxtAmount);
            if (totalAmount != totaltxtAmount) {
                if (isNaN(parseFloat(totalAmount))) {
                    totalAmount = 0;
                }

                if (isNaN(parseFloat(totaltxtAmount))) {
                    totaltxtAmount = 0;
                }


                if (parseFloat(totalAmount) != parseFloat(totaltxtAmount)) {
                    if (document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value == "") {
                        diff = parseFloat(totaltxtAmount) - parseFloat(totalAmount);
                        document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value = formatCurrencyValues(parseFloat(diff));
                        lblTotalAmt = parseFloat(totalAmount) + parseFloat(diff);
                        // alert(lblTotalAmt);
                        document.getElementById('TotalAmt').value = parseFloat(lblTotalAmt);
                    }
                }

                //            if (parseFloat(totalAmount) > parseFloat(totaltxtAmount))
                //            {              
                //               difference=parseFloat(totalAmount)-parseFloat(totaltxtAmount);
                //               document.getElementById(idRoot+'_ctl'+i+'_txtDebit').value=formatCurrencyValues(parseFloat(difference));
                //               lblTotalAmt=parseFloat(totaltxtAmount)+parseFloat(difference);
                //               alert(lblTotalAmt);
                //               document.getElementById('TotalAmt').value=parseFloat(lblTotalAmt);
                //            }
            }

        }

        function Recalculate() {
            document.getElementById('txtAmount').value = formatCurrencyValues(document.getElementById('TotalAmt').value);
            document.form1.btnWords.click();
            return false;
        }

        function formatCurrencyValues(num) {

            num = num.toString().replace(/\$|\,/g, '');

            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
        function OpenLogoPage(cstr) {
            //alert(cstr);
            window.open('../opportunity/frmLogoUpload.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=' + cstr, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function AddList() {
            if (document.form1.ddlCompanyName.value == "" || document.form1.ddlCompanyName.value == 0) {
                alert("Select the Company Name");
                document.form1.ddlCompanyName.focus();
                return false;
            }
        }
         
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <table id="Table1" cellspacing="2" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="normal1">
                <asp:Label ID="lblBankAcnt" Width="80px" Text="Bank Account" runat="server"></asp:Label>
                <asp:DropDownList ID="ddlBankAcnt" Width="150px" runat="server" AutoPostBack="true"
                    CssClass="signup">
                </asp:DropDownList>
                &nbsp;&nbsp; &nbsp;<asp:Label ID="lblBalance" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblOpeningBalance" runat="server"></asp:Label>
            </td>
            <td class="normal1">
               
            </td>
            <td >
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" Visible="false">
                </asp:Button>
                <asp:Button ID="btnRecalculate" runat="server" CssClass="button" Text="Recalculate"
                    Visible="false"></asp:Button>
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close"
                    Visible="false"></asp:Button>
               
            </td>
        </tr>
      
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" Visible="false">
                </asp:Button>
                <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back"></asp:Button>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Write Checks
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table HorizontalAlign="Center" ID="tbl" runat="server" Width="100%" GridLines="None">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <asp:Table ID="Table3" runat="server" HorizontalAlign="Center" BorderStyle="Double"
                    BorderWidth="5" BorderColor="black">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell>
                            <table width="649" cellpadding="2" cellspacing="2" align="center" height="10" border="0"
                                background="../images/Bk1.gif">
                                <tr>
                                    <td colspan="3" align="left">
                                        <asp:Image ID="imgCompanyLogo" runat="server" AlternateText="Change Company Logo">
                                        </asp:Image>
                                    </td>
                                    <td align="right">
                                        <b>
                                            <asp:Label ID="CheckNo" runat="server" Text=""></asp:Label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="normal1">
                                        <b>
                                            <asp:Label ID="lblChkDate" Text="Date  " runat="server"></asp:Label></b>
                                    </td>
                                    <td align="left">
                                        <BizCalendar:Calendar ID="calFrom" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="normal1">
                                        <b>Pay to the Order of</b>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblCompany" runat="server"></asp:Label>
                                        <asp:Label ID="lblCompanyId" runat="server"></asp:Label>
                                        <hr width="350" size="1" />
                                    </td>
                                    <td align="left" class="normal1">
                                        <b>Amount </b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAmount" runat="server" MaxLength="9" Width="150" CssClass="signup"
                                            onchange="formatCurrency(this);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" class="normal1">
                                        <asp:Label ID="lblMoneyDescription" runat="server"></asp:Label>
                                        <hr width="600" size="1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="left">
                                        <b>
                                            <asp:Label ID="lblMemo" Text="Memo" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtMemo" runat="server" Width="350" CssClass="signup" MaxLength="70"></asp:TextBox>
                                    </td>
                                    <td align="left" class="normal1">
                                        <b>Signature </b>
                                    </td>
                                    <td align="left" class="normal1">
                                        <hr width="150" size="1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="right">
                                        <asp:Image ID="imgBankLogo" runat="server" AlternateText="Change Bank Logo"></asp:Image>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" class="normal1">
                                        <asp:Table HorizontalAlign="Center" ID="table2" runat="server" BorderWidth="1" BorderColor="black">
                                            <asp:TableRow VerticalAlign="Top">
                                                <asp:TableCell CssClass="normal1">
                                                    Bank Routing Number<br>
                                                    <b>|:</b>
                                                    <asp:TextBox ID="txtRouting" runat="server" Width="130" MaxLength="9" CssClass="signup"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="normal1">
                                                    Bank Account Number<br>
                                                    <b>|:</b>
                                                    <asp:TextBox ID="txtBankAcc" runat="server" Width="130" MaxLength="9" CssClass="signup"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell CssClass="normal1">
                                                    Check Number<br>
                                                    <b>|:</b>
                                                    <asp:TextBox ID="txtCheckNo" runat="server" Width="130" MaxLength="9" CssClass="signup"
                                                        onchange="CheckNo(this)"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:DataGrid ID="dgJournalEntry" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Accounts">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numJournalId") %>'></asp:Label>
                                            <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionId") %>'></asp:Label>
                                            <asp:Label ID="lblAccountID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numAccountID") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlAccounts" Width="180px" runat="server" CssClass="signup"
                                                onchange="SetCreditAmount(this)">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtAccountTypeId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numAcntType") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDebitAmt" runat="server" Style="display: none" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.numDebitAmt")) %>'></asp:Label>
                                            <asp:Label ID="lblCreditAmt" runat="server" Style="display: none" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.numCreditAmt")) %>'></asp:Label>
                                            <asp:TextBox ID="txtDebit" TextMode="SingleLine" Width="100" runat="server" CssClass="signup"
                                                onchange="Calculate(this);"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Memo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemo" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.varDescription") %>'></asp:Label>
                                            <asp:TextBox ID="txtMemo" TextMode="SingleLine" Width="150" runat="server" CssClass="signup"
                                                MaxLength="100"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numCustomerId") %>'></asp:Label>
                                            <asp:TextBox ID="txtCompanyName" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                                            <asp:Button ID="btnCompanyName" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                                            <asp:DropDownList ID="ddlName" Width="200px" runat="server" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Relationship">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRelationship" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.varRelation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblCompany" runat="server" cellpadding="0" cellspacing="0" border="0"
                    width="100%">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtCompanyName" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                            <asp:Button ID="btnCompanyName" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                            <asp:DropDownList ID="ddlCompanyName" Width="200px" runat="server" CssClass="signup">
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnAddtoList" Width="100px" runat="server" Text="Add to list" CssClass="button" />
                        </td>
                       
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:DataGrid ID="dgCompanyList" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numCheckCompanyId" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>
                                <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Company">
                            <ItemTemplate>
                                <asp:Label ID="lblCompanyId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numDivisionID") %>'></asp:Label>
                                <asp:Label ID="lblCompanyName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcCompanyName") %>'></asp:Label>
                                <asp:Label ID="lblCheckCompanyId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numCheckCompanyId") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblCompanyId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numDivisionID") %>'></asp:Label>
                                <asp:Label ID="lblCheckCompanyId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numCheckCompanyId") %>'></asp:Label>
                                <asp:TextBox ID="txtCompanyName1" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                                <asp:Button ID="btnCompanyName1" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                                <asp:DropDownList ID="ddlCompanyName1" Width="200px" runat="server" CssClass="signup">
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monAmount")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEAmount" Width="100px" runat="server" CssClass="signup" MaxLength="9"
                                    Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monAmount")) %>'></asp:TextBox>
                                <asp:Label ID="lblEditAmount" Style="display: none" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monAmount")) %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Memo">
                            <ItemTemplate>
                                <asp:Label ID="lblMemo" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcMemo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEDesc" TextMode="MultiLine" Width="300" runat="server" CssClass="signup"
                                    MaxLength="70" Text='<%# DataBinder.Eval(Container,"DataItem.vcMemo") %>'></asp:TextBox>
                                <asp:Label ID="lblEditMemo" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcMemo") %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Add to Check">
                            <ItemTemplate>
                                <asp:Button ID="btnAddChecks" runat="server" CssClass="button" Text="Add to Check"
                                    CommandName="AddToCheck"></asp:Button>
                             
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteAction" runat="server" CssClass="button Delete" Text="X"
                                    CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkDeleteAction" runat="server" Visible="false">
			 <font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Button runat="server" ID="btnWords" Style="display: none" />
    <asp:HiddenField ID="TotalAmt" runat="server" />
    <asp:HiddenField ID="CustomerId" runat="server" />
    <asp:HiddenField ID="CheckCompanyId" runat="Server" />
</asp:Content>--%>
