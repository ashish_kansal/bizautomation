''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebGrid
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmBudgetRollups
    Inherits BACRMPage

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not IsPostBack Then
    '            
    '            
    '           
    '            GetUserRightsForPage( 35, 102)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
    '                Response.Redirect("../admin/authentication.aspx?mesg=AS")
    '            End If
    '            LoadRollUpGrid()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadRollUpGrid()
    '    Try
    '        Dim dtRollUp As DataTable
    '        Dim mobjBudget As New Budget

    '        mobjBudget.DomainId = Session("DomainId")
    '        Select Case ddlFiscal.SelectedIndex
    '            Case 0
    '                mobjBudget.FiscalYear = Now.Year
    '                mobjBudget.Type = 0
    '            Case 1
    '                mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                mobjBudget.Type = -1
    '            Case 2
    '                mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                mobjBudget.Type = 1
    '        End Select
    '        ultraRollUp.Clear()
    '        dtRollUp = mobjBudget.GetRollUpBudgetDetails()
    '        ultraRollUp.DataSource = dtRollUp
    '        ultraRollUp.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ultraRollUp_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles ultraRollUp.InitializeLayout
    '    Try
    '        Dim i As Integer
    '        ultraRollUp.Bands(0).Columns.FromKey("numChartAcntId").Hidden = True
    '        ultraRollUp.Bands(0).Columns.FromKey("numParentAcntId").Hidden = True
    '        ultraRollUp.Bands(0).Columns.FromKey("TOC").Hidden = True
    '        ultraRollUp.Bands(0).Columns.FromKey("vcCategoryName").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        ultraRollUp.Bands(0).Columns.FromKey("Total").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        ultraRollUp.Bands(0).Columns.FromKey("vcCategoryName").HeaderText = "Budget Accounts"
    '        ultraRollUp.Bands(0).Columns.FromKey("vcCategoryName").Width = Unit.Percentage(16)
    '        ultraRollUp.Bands(0).Columns.FromKey("Total").DataType = "System.Decimal"
    '        ultraRollUp.Bands(0).Columns.FromKey("Total").Format = "###,###.##"
    '        ultraRollUp.Bands(0).Columns.FromKey("Total").Width = Unit.Percentage(10)

    '        ''To Set Month Name
    '        For i = 4 To ultraRollUp.Columns.Count - 2
    '            ultraRollUp.Columns.Item(i).HeaderText = MonthName(ultraRollUp.Columns.Item(i).Key.Split("~")(0)).Substring(0, 3)
    '            ultraRollUp.Bands(0).Columns.FromKey(ultraRollUp.Columns.Item(i).Key).DataType = "System.Decimal"
    '            ultraRollUp.Bands(0).Columns.FromKey(ultraRollUp.Columns.Item(i).Key).Format = "###,###.##"
    '            ultraRollUp.Bands(0).Columns.FromKey(ultraRollUp.Columns.Item(i).Key).Width = Unit.Percentage(6)
    '            ultraRollUp.Bands(0).Columns.FromKey(ultraRollUp.Columns.Item(i).Key).AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlFiscal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFiscal.SelectedIndexChanged
    '    Try
    '        LoadRollUpGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class