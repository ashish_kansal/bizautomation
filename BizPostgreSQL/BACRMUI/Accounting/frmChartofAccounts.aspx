<%@ Page Language="vb" EnableViewState="true" AutoEventWireup="true" CodeBehind="frmChartofAccounts.aspx.vb"
    Inherits=".frmChartofAccounts" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script language="javascript" type="text/javascript">
        function onNodeClicking(sender, args) {
            UnCheckAll();
            var node = args.get_node();
            var chartAcntId;
            var AcntTypeId;

            if (node.get_value() != null) {
                val = node.get_value().split("~");
                chartAcntId = val[0];
                AcntTypeId = val[1];
                /* Open page only for COA not for COA category */
                if (AcntTypeId > 0) {
                    //window.location.href = "../Accounting/frmChartofAccountsDetail.aspx?frm=chartAcnt&ChartAcntId=" + chartAcntId;
                }
            }
        }
        function UnCheckAll() {
            var tree = $find("RadTreeView1");
            for (var i = 0; i < tree.get_allNodes().length; i++) {
                var node = tree.get_allNodes()[i];
                node.set_checked(false);
            }
        }
        function onNodeChecked(sender, args) {
            var node = args.get_node();
            if (node.get_checked() == true) {
                UnCheckAll();
                node.set_checked(true);
                node.select();
                document.getElementById('hdnAccountId').value = node.get_value().split("~")[0];
            }
            else {
                UnCheckAll();
                node.unselect();
                document.getElementById('hdnAccountId').value = 0;
            }
        }

        function OpenNewAccount() {
            // alert(document.frmChartAccounts.elements.length)
            var accountID = document.getElementById('hdnAccountId').value;
            //alert(accountID);
            window.open("../Accounting/frmNewAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Name=0&AccountID=" + accountID + "", '', 'toolbar=no,titlebar=no,left=200, top=100,width=980,height=450,scrollbars=no,resizable=yes')
            return false;
        }

        function EditSelected(a) {
            //            if (document.getElementById('hdnAccountId').value == "") {
            //                alert("Please select one Chart of Account");
            //                return false;
            //            }
            window.open('../Accounting/frmNewAccounts.aspx?Name=' + a, '', 'toolbar=no,titlebar=no,left=200, top=100,width=980,height=450,scrollbars=no,resizable=yes');
            return false;
        }

        function OpenCOAReport(a) {
            //window.open('../Accounting/frmChartofAccountsDetail.aspx?frm=chartAcnt&ChartAcntId=' + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=350,scrollbars=no,resizable=yes');
            window.location.href = "../Accounting/frmChartofAccountsDetail.aspx?frm=chartAcnt&ChartAcntId=" + a;
            return false;
        }

        function OpenGLReport(a, b) {
            window.location.href = "../Accounting/frmGeneralLedger.aspx?Mode=5&AcntTypeID=" + a + "&AccountID=" + b;
            //window.open('../Accounting/frmJournalEntry.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=COA&ChartAcntId=' + document.getElementById('hdnAccountId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=350,scrollbars=no,resizable=yes');
            return false;
        }

        function OpenMSReport(AccID, Opening, Code, FormName, Name) {
            //window.location.href = "/Accounting/frmMonthlySummary.aspx?ChartAcntId=" + a + "&Opening=" + 0.0000 + "&Code=" + "0101010101" + "&frm=" + "frmTrialBalance" + "&Name=" + "UnDepositedFunds";
            window.location.href = "../Accounting/frmMonthlySummary.aspx?ChartAcntId=" + AccID + "&Opening=" + Opening + "&Code=" + Code + "&frm=" + FormName + "&Name=" + Name;
            //window.open('../Accounting/frmJournalEntry.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=COA&ChartAcntId=' + document.getElementById('hdnAccountId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=350,scrollbars=no,resizable=yes');
            return false;
        }

        function OpenBankAccount() {
            window.location.href = "../EBanking/frmBankAccounts.aspx";
            return false;
        }

        function SetAccountValue(a) {
            document.getElementById('hdnAccountId').value = a;
            //alert(document.getElementById('hdnAccountId').value);
            //alert(a);
            if (confirm('Are you sure you want to delete selected account?') == true) {
                if (document.getElementById('hdnAccountId').value == "") {
                    alert("Please select one Chart of Account");
                    return false;
                }
                document.getElementById('btnChartAcntDelete').click();
                return true;
            }
        }

        function DeleteSelected() {
            if (confirm('Are you sure you want to delete selected account?') == true) {
                if (document.getElementById('hdnAccountId').value == "") {
                    alert("Please select one Chart of Account");
                    return false;
                }
                return true;
            }
        }
        function RefreshBank(a) {
            document.getElementById("hdnBankDetailID").value = a;
            document.getElementById('btnRefreshBank').click();
        }


        function LoadTreeViewDetails() {
            document.form1.btnTreeView.click()
            return false;
        }


        function LoadAccounts(lintAccountId) {
            window.open("../Accounting/frmNewAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Name=" + lintAccountId, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=350,scrollbars=no,resizable=yes');
            return false;
        }

        function treeExpandAllNodes() {
            var treeView = $find("RadTreeView1");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }

        function treeCollapseAllNodes() {
            var treeView = $find("RadTreeView1");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }

    </script>
    <style type="text/css">
        .RadTreeView_Default .rtIn {
        }

        .RadTreeView_Default .rtSelected .rtIn {
        }

        .rtTop rtSelected {
        }

        .RadTreeView_Default .rtSelected .rtIn {
            background-color: white !important;
            background-image: none !important;
            border-color: White !important;
            color: Black !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="form-inline">
                    <asp:CheckBox ID="chkShowActive" runat="server" Text="Show only active accounts"
                        CssClass="normal1" AutoPostBack="True" Checked="false" />&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="chkSortTreeView" runat="server" Text="Sort tree alphanumerically"
                        CssClass="normal1" AutoPostBack="True" Checked="true" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="btnChartAcntAddNew" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add New
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnChartAcntEdit" runat="server" CssClass="btn btn-primary" Text="" Style="display: none">
                    <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnChartAcntDelete" runat="server" CssClass="btn btn-danger"
                        Style="display: none"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                    <asp:LinkButton ID="btnRefreshBank" runat="server" CssClass="btn btn-primary" Text=""
                        Style="display: none; padding-left: 5px !important;"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Refresh</asp:LinkButton>
                    <a href="#" class="help">&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table id="table3" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td colspan="1" class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Chart Of Accounts&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmchartofaccounts.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="pull-left">
        <div class="form-inline">
            <a href="javascript: treeExpandAllNodes();" style="color: Gray;">Expand All </a>
            &nbsp; <a href="javascript: treeCollapseAllNodes();" style="color: Gray;">Collapse All
            </a>
        </div>
    </div>
    <div class="pull-right">
        <div class="pull-right">

            <a id="hrfBnkStatement" class="text_bold btn btn-primary" href="javascript:OpenBankAccount();" style="margin-right: 2px">
                <i class="fa fa-university"></i>&nbsp&nbsp; Bank Accounts</a>
        </div>
    </div>
    <div style="clear: both"></div>
    <div class="col-md-12">
        <telerik:RadTreeView ID="RadTreeView1" runat="server" AllowNodeEditing="false" Skin="Default"
            CheckBoxes="True" OnClientNodeClicking="onNodeClicking" OnClientNodeChecked="onNodeChecked"
            ClientIDMode="Static">
        </telerik:RadTreeView>
    </div>
    <asp:Button ID="btnTreeView" runat="server" Style="display: none"></asp:Button>
    <asp:HiddenField ID="hdnAccountId" runat="server" />
    <asp:HiddenField ID="hdnBankDetailID" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
