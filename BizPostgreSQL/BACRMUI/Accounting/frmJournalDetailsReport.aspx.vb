﻿Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Imports System.IO.Compression
Imports System.Web.Services
Imports Newtonsoft.Json
Imports System.Collections.Generic


Public Class frmJournalDetailsReport
    Inherits BACRMPage

    Dim mobjGeneralLedger As GeneralLedger

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            GetUserRightsForPage(35, 89)

            If Not IsPostBack Then

                If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                mobjGeneralLedger.DomainID = Session("DomainID")
                mobjGeneralLedger.Year = CInt(Now.Year)

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    End Try
                Else
                    calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                End If

                LoadGeneralJournalDetailswithPaging()

            End If

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            End If
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub LoadGeneralJournalDetailswithPaging()
        Try
            Dim dsGL As DataSet
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.DomainID = Session("DomainID")

            mobjGeneralLedger.byteMode = 0
            mobjGeneralLedger.CurrentPage = 1
            mobjGeneralLedger.PageSize = 1
            mobjGeneralLedger.TotalRecords = 0
            mobjGeneralLedger.FromDate = calFrom.SelectedDate
            mobjGeneralLedger.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            dsGL = mobjGeneralLedger.GetGeneralJournalDetailsReport()

            RepJournalEntry.DataSource = dsGL
            RepJournalEntry.DataBind()
            'dgJournals.DataSource = dsGL
            'dgJournals.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function WebMethodLoadGeneralDetails(ByVal DomainID As Integer, ByVal pageIndex As Integer, ByVal strDateFormat As String, ByVal FromDate As String, ByVal ToDate As String) As String
        Try
            Dim mobjGeneralLedger1 As New GeneralLedger
            Dim dtChartAcntDetails As DataTable
            Dim DisplayMode As Integer = 1
            Dim FromDate1 As New Date
            Dim ToDate1 As New Date
            Dim TotTransCountForAccount As Integer
            Dim perLoadCount As Integer = 0

            Dim dt1 As New DateTime
            FromDate1 = DateFromFormattedDate(FromDate, strDateFormat)
            ToDate1 = DateFromFormattedDate(ToDate, strDateFormat)

            Dim dsGL As DataSet
            If mobjGeneralLedger1 Is Nothing Then mobjGeneralLedger1 = New GeneralLedger

            mobjGeneralLedger1.DomainID = DomainID
            mobjGeneralLedger1.FromDate = FromDate1
            mobjGeneralLedger1.ToDate = CDate(ToDate1 & " 23:59:59")
            mobjGeneralLedger1.byteMode = 1
            mobjGeneralLedger1.CurrentPage = pageIndex
            mobjGeneralLedger1.PageSize = 100
            mobjGeneralLedger1.TotalRecords = 0

            Dim TotPages As Integer
            dsGL = mobjGeneralLedger1.GetGeneralJournalDetailsReport()
            dtChartAcntDetails = dsGL.Tables(0).Clone()

            Dim TransCountForAccount As Integer
            Dim drow As DataRow
            Dim dtAccountDetails As New DataTable("AccountDetails")
            Dim drAccountDetails As DataRow
            dtAccountDetails.Columns.Add("TransactionLoaded")
            Dim dtTransactions As DataTable

            TransCountForAccount = 0
            TotTransCountForAccount = 0

            dtTransactions = dsGL.Tables(0).Copy
            Dim dvCopy As DataView = dtTransactions.DefaultView
            dtTransactions = dvCopy.ToTable(True, "numJournalID", "Date")
            'dtTransactions = dvCopy.ToTable(True, "Date")

            For Each dr As DataRow In dtTransactions.Rows
                Dim drDup() As DataRow = dsGL.Tables(0).Select("numJournalId = " & CCommon.ToLong(dr("numJournalID")) & " AND Date = '" & CCommon.ToString(dr("Date")) & "'")
                'Dim drDup() As DataRow = dsGL.Tables(0).Select("Date = '" & CCommon.ToString(dr("Date")) & "'")
                If drDup IsNot Nothing Then

                    Dim drNew As DataRow = dsGL.Tables(0).NewRow
                    drNew("numJournalId") = 0
                    drNew("numTransactionId") = 0
                    drNew("numAccountID") = 0
                    drNew("vcAccountName") = ""
                    drNew("numDebitAmt") = 0
                    drNew("numCreditAmt") = 0

                    dsGL.Tables(0).Rows.InsertAt(drNew, dsGL.Tables(0).Rows.IndexOf(drDup(drDup.Count - 1)) + 1)
                    dsGL.Tables(0).AcceptChanges()
                End If
            Next
            

            For Each dr As DataRow In dsGL.Tables(0).Rows
                TransCountForAccount += 1
                perLoadCount += 1
            Next

            dtChartAcntDetails = dsGL.Tables(0)
            TotPages = ((pageIndex - 1) * 100) + TransCountForAccount
            drAccountDetails = dtAccountDetails.NewRow
            drAccountDetails("TransactionLoaded") = TotPages

            dtAccountDetails.Rows.Add(drAccountDetails)

            Dim dt123 As DataTable
            dt123 = dtChartAcntDetails.Copy()
            Dim dsChartAcntDetails As New DataSet
            dsChartAcntDetails.Tables.Add(dt123)
            Dim dtCopyAccDet As DataTable
            dtCopyAccDet = dtAccountDetails.Copy()

            dsChartAcntDetails.Tables.Add(dtCopyAccDet)

            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsChartAcntDetails, Formatting.None)
            Return json

        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try

    End Function

    Private Sub ibExportExcel_Click(sender As Object, e As System.EventArgs) Handles ibExportExcel.Click
        Try
            Dim dsGL As DataSet
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.DomainID = Session("DomainID")

            mobjGeneralLedger.byteMode = 3
            mobjGeneralLedger.CurrentPage = 1
            mobjGeneralLedger.PageSize = 1
            mobjGeneralLedger.TotalRecords = 0
            mobjGeneralLedger.FromDate = calFrom.SelectedDate
            mobjGeneralLedger.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            dsGL = mobjGeneralLedger.GetGeneralJournalDetailsReport()

            Dim dtDetails As New DataTable
            If dsGL IsNot Nothing AndAlso dsGL.Tables.Count > 0 AndAlso dsGL.Tables(0).Rows.Count > 0 Then
                dtDetails = dsGL.Tables(0).Copy()

                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=GeneralJournalDetails" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".csv")
                Response.Charset = ""
                Response.ContentType = "text/csv"
                Dim stringWrite As New System.IO.StringWriter
                CSVExport.ProduceCSV(dtDetails, stringWrite, True)
                Response.Write(stringWrite.ToString())
                Response.End()
            Else
                lblErrMessage.Text = "No General Journals details found to export."
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch err As Exception

        End Try
        
    End Sub
End Class