''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebGrid
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmBudgetImpact
    Inherits BACRMPage

#Region "Variables"
    Dim mobjBudget As New Budget
   
#End Region

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        lblTotalBalance.Text = "Total Bank Balance: " & Session("Currency")
    '        If Not IsPostBack Then
    '            
    '            
    '            GetUserRightsForPage( 35, 101)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
    '                Response.Redirect("../admin/authentication.aspx?mesg=AS")
    '            Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnBudgetImpactSave.Visible = False
    '            End If
    '            LoadBudgetImpact()
    '            GetTotalBankBalance()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub GetTotalBankBalance()
    '    Try
    '        Dim lTotBankBal As Decimal
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.GetTotalBankBalance()
    '        lTotBankBal = mobjBudget.TotalBankBalance
    '        lblTotalBalanceAmt.Text = IIf(lTotBankBal > 0, "<font color=green><b>" & ReturnMoney(lTotBankBal) & "</b></font>", "<font color=red><b>" & ReturnMoney(lTotBankBal) & "</b></font>")
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Function ReturnMoney(ByVal Money)
    '    Try
    '        If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,##0.00}", Money)
    '        Return String.Empty
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Sub LoadOperationBudget()
    '    Try
    '        mobjBudget.DomainId = Session("DomainID")
    '        ddlOperationBudget.DataSource = mobjBudget.GetOperationBudgetDetails
    '        ddlOperationBudget.DataTextField = "vcBudgetName"
    '        ddlOperationBudget.DataValueField = "numBudgetId"
    '        ddlOperationBudget.DataBind()
    '        ddlOperationBudget.Items.Insert(0, New ListItem("--Select One --", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub rdbOperationBudget_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbOperationBudget.CheckedChanged
    '    Try

    '        If rdbOperationBudget.Checked = True Then
    '            LoadOperationBudget()
    '            If ddlOperationBudget.Items.Count > 1 Then
    '                ddlOperationBudget.SelectedIndex = 1
    '            Else : ddlOperationBudget.SelectedIndex = 0
    '            End If
    '            ''LoadTree(ddlOperationBudget.SelectedItem.Value, 0)
    '            LoadOperationBudgetAnalyseDropDownList()
    '            ddlProcurementBudget.SelectedIndex = 0
    '            ddlMarketBudget.SelectedIndex = 0

    '            mobjBudget.BudgetDetId = 0
    '            mobjBudget.DomainId = Session("DomainId")
    '            mobjBudget.sintByte = 3
    '            mobjBudget.UserCntID = Session("UserContactId")

    '            ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '            ulgImpactBudget.DataBind()

    '            ddlProcurementBudgetAnalyze.Items.Clear()
    '            ddlMarketBudgetAnalyze.Items.Clear()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try

    'End Sub

    'Private Sub ddlProcurementBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcurementBudget.SelectedIndexChanged
    '    Try
    '        ''If ddlProcurementBudget.SelectedItem.Value <> 0 Then

    '        LoadProcurementBudgetAnalyseDropDownList()
    '        ''LoadTree(ddlProcurementBudget.SelectedItem.Value, 2)
    '        rdbProcurementBudget.Checked = True

    '        mobjBudget.BudgetDetId = 0
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.sintByte = 3
    '        mobjBudget.UserCntID = Session("UserContactId")
    '        ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '        ulgImpactBudget.DataBind()

    '        ddlOperationBudget.Items.Clear()
    '        ddlOperationBudgetAnalyze.Items.Clear()
    '        ddlMarketBudget.SelectedIndex = 0
    '        ddlMarketBudgetAnalyze.Items.Clear()

    '        ''  End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub LoadProcurementBudgetAnalyseDropDownList()
    '    Try
    '        mobjBudget.DomainId = Session("DomainId")
    '        Select Case ddlProcurementBudget.SelectedItem.Value
    '            Case 0 
    '                mobjBudget.DomainId = 0
    '        End Select
    '        ddlProcurementBudgetAnalyze.DataSource = mobjBudget.GetProcurementDetailsForImpact
    '        ddlProcurementBudgetAnalyze.DataTextField = "vcItemGroup"
    '        ddlProcurementBudgetAnalyze.DataValueField = "numProcurementDetId"
    '        ddlProcurementBudgetAnalyze.DataBind()
    '        ddlProcurementBudgetAnalyze.Items.Insert(0, New ListItem("--Select One--", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlMarketBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMarketBudget.SelectedIndexChanged
    '    Try
    '        ''If ddlMarketBudget.SelectedItem.Value <> 0 Then
    '        LoadMarketBudgetAnalyseDropDownList()
    '        ''LoadTree(ddlMarketBudget.SelectedItem.Value, 1)
    '        rdbProcurementBudget.Checked = False
    '        rdbMarketingBudget.Checked = True

    '        ddlOperationBudget.Items.Clear()
    '        ddlOperationBudgetAnalyze.Items.Clear()
    '        ddlProcurementBudgetAnalyze.Items.Clear()

    '        mobjBudget.BudgetDetId = 0 ''ddlOperationBudgetAnalyze.SelectedItem.Value
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.sintByte = 3
    '        mobjBudget.UserCntID = Session("UserContactId")
    '        ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '        ulgImpactBudget.DataBind()
    '        '' End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub LoadMarketBudgetAnalyseDropDownList()
    '    Try
    '        mobjBudget.DomainId = Session("DomainId")
    '        Select Case ddlMarketBudget.SelectedItem.Value
    '            Case 0
    '                mobjBudget.DomainId = 0
    '        End Select
    '        ddlMarketBudgetAnalyze.DataSource = mobjBudget.GetMarketBudgetDetailsForImpact
    '        ddlMarketBudgetAnalyze.DataTextField = "vcData"
    '        ddlMarketBudgetAnalyze.DataValueField = "numListItemID"
    '        ddlMarketBudgetAnalyze.DataBind()
    '        ddlMarketBudgetAnalyze.Items.Insert(0, New ListItem("--Select One--", "0"))
    '        ddlMarketBudgetAnalyze.SelectedIndex = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlOperationBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOperationBudget.SelectedIndexChanged
    '    ''  If ddlOperationBudget.SelectedItem.Value <> 0 Then
    '    Try
    '        ''  LoadTree(ddlOperationBudget.SelectedItem.Value, 0)
    '        LoadOperationBudgetAnalyseDropDownList()
    '        rdbOperationBudget.Checked = True

    '        ''mobjBudget.BudgetDetId = 0 ''ddlOperationBudgetAnalyze.SelectedItem.Value
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.sintByte = 3
    '        mobjBudget.UserCntID = Session("UserContactId")

    '        ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '        ulgImpactBudget.DataBind()
    '        ddlProcurementBudget.SelectedIndex = 0
    '        ddlMarketBudget.SelectedIndex = 0
    '        ddlProcurementBudgetAnalyze.Items.Clear()
    '        ddlMarketBudgetAnalyze.Items.Clear()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    '    ''   End If
    'End Sub

    'Private Sub LoadOperationBudgetAnalyseDropDownList()
    '    Try
    '        mobjBudget.BudgetId = IIf(ddlOperationBudget.SelectedItem.Value.ToString = "0", 0, ddlOperationBudget.SelectedItem.Value.Split("~")(0))
    '        mobjBudget.DomainId = Session("DomainId")
    '        ddlOperationBudgetAnalyze.DataSource = mobjBudget.GetOperationBudgetDetailsForImpact
    '        ddlOperationBudgetAnalyze.DataTextField = "ChartAcntname"
    '        ddlOperationBudgetAnalyze.DataValueField = "numChartAcntId"
    '        ddlOperationBudgetAnalyze.DataBind()
    '        ddlOperationBudgetAnalyze.Items.Insert(0, New ListItem("--Select One--", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub rdbProcurementBudget_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbProcurementBudget.CheckedChanged
    '    Try
    '        If rdbProcurementBudget.Checked = True Then
    '            '' trvChartAccounts.Items.Clear()
    '            ClearAllDataForDropDown()
    '            ddlProcurementBudget.SelectedIndex = 1
    '            LoadProcurementBudgetAnalyseDropDownList()
    '            ''LoadTree(ddlProcurementBudget.SelectedItem.Value, 2)

    '            mobjBudget.BudgetDetId = 0 ''ddlOperationBudgetAnalyze.SelectedItem.Value
    '            mobjBudget.DomainId = Session("DomainId")
    '            mobjBudget.sintByte = 3
    '            mobjBudget.UserCntID = Session("UserContactId")

    '            ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '            ulgImpactBudget.DataBind()

    '            ddlOperationBudgetAnalyze.Items.Clear()
    '            ddlMarketBudgetAnalyze.Items.Clear()

    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ClearAllDataForDropDown()
    '    Try
    '        ddlOperationBudget.Items.Clear()
    '        Dim dt As New DataTable
    '        dt.Columns.Add("numBudgetId")
    '        dt.Columns.Add("vcBudgetName")
    '        ddlOperationBudget.DataSource = dt.DefaultView
    '        ddlOperationBudget.DataTextField = "vcBudgetName"
    '        ddlOperationBudget.DataValueField = "numBudgetId"
    '        ddlOperationBudget.DataBind()
    '        ddlOperationBudget.Items.Insert(0, New ListItem("--Select One--", "0"))
    '        ddlMarketBudget.SelectedIndex = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub rdbMarketingBudget_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbMarketingBudget.CheckedChanged
    '    Try
    '        If rdbMarketingBudget.Checked = True Then
    '            ''trvChartAccounts.Items.Clear()
    '            ddlOperationBudget.Items.Clear()
    '            Dim dt As New DataTable
    '            dt.Columns.Add("numBudgetId")
    '            dt.Columns.Add("vcBudgetName")
    '            ddlOperationBudget.DataSource = dt.DefaultView
    '            ddlOperationBudget.DataTextField = "vcBudgetName"
    '            ddlOperationBudget.DataValueField = "numBudgetId"
    '            ddlOperationBudget.DataBind()
    '            ddlOperationBudget.Items.Insert(0, New ListItem("--Select One--", "0"))
    '            ddlProcurementBudget.SelectedIndex = 0
    '            ddlMarketBudget.SelectedIndex = 1
    '            LoadMarketBudgetAnalyseDropDownList()
    '            ''LoadTree(ddlMarketBudget.SelectedItem.Value, 1)

    '            mobjBudget.BudgetDetId = 0
    '            mobjBudget.DomainId = Session("DomainId")
    '            mobjBudget.sintByte = 3
    '            mobjBudget.UserCntID = Session("UserContactId")

    '            ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '            ulgImpactBudget.DataBind()

    '            ddlProcurementBudgetAnalyze.Items.Clear()
    '            ddlOperationBudgetAnalyze.Items.Clear()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadBudgetImpact()
    '    Try
    '        rdbOperationBudget.Checked = True
    '        rdbMarketingBudget.Checked = False
    '        rdbProcurementBudget.Checked = False
    '        LoadOperationBudget()
    '        If ddlOperationBudget.Items.Count > 1 Then
    '            ddlOperationBudget.SelectedIndex = 1
    '        Else : ddlOperationBudget.SelectedIndex = 0
    '        End If
    '        ''LoadTree(ddlOperationBudget.SelectedItem.Value, 0)
    '        LoadOperationBudgetAnalyseDropDownList()
    '        ddlProcurementBudget.SelectedIndex = 0
    '        ddlProcurementBudgetAnalyze.Items.Clear()
    '        ddlMarketBudget.SelectedIndex = 0
    '        ddlMarketBudgetAnalyze.Items.Clear()

    '        mobjBudget.BudgetDetId = 0
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.sintByte = 3

    '        mobjBudget.UserCntID = Session("UserContactId")
    '        ''ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '        ''ulgImpactBudget.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlOperationBudgetAnalyze_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOperationBudgetAnalyze.SelectedIndexChanged
    '    Try
    '        DisplayOperationBudgetAnalyze()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub DisplayOperationBudgetAnalyze()
    '    Try
    '        If ddlOperationBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then

    '            ''Dim lintBudgetId As Integer
    '            ''Dim Id As Integer
    '            ''If ddlOperationBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            ''    Id = ddlOperationBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            ''Else
    '            ''    Id = 0
    '            ''End If

    '            mobjBudget.BudgetId = ddlOperationBudget.SelectedItem.Value
    '            mobjBudget.Id = ddlOperationBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            mobjBudget.DomainId = Session("DomainId")
    '            mobjBudget.sintByte = 0 ''IIf(ddlOperationBudgetAnalyze.SelectedItem.Value.Split("~")(0) = 0, 3, 0)

    '            Select Case rdlForecastType.SelectedIndex
    '                Case 0
    '                    mobjBudget.sintForecast = 0
    '                Case 1
    '                    mobjBudget.sintForecast = 1
    '            End Select

    '            mobjBudget.FiscalYear = CInt(Now.Year)
    '            mobjBudget.UserCntID = Session("UserContactId")

    '            ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '            ulgImpactBudget.DataBind()
    '        Else : ulgImpactBudget.Rows.Clear()
    '        End If
    '        ''UpdateFooter()
    '        ''Else
    '        ''    dtBudgetDetails = LoadEmptyDataTable()
    '        ''    ulgImpactBudget.DataSource = dtBudgetDetails.DefaultView
    '        ''    ulgImpactBudget.DataBind()
    '        ''End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    'Private Sub ulgImpactBudget_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles ulgImpactBudget.InitializeLayout
    '    Try
    '        ulgImpactBudget.Bands(0).Columns.FromKey("numBudgetId").Hidden = True
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monOriginalAmt").Hidden = True
    '        ulgImpactBudget.Bands(0).Columns.FromKey("vcmonth").Hidden = True
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monCashInflow").Type = ColumnType.HyperLink

    '        ulgImpactBudget.Bands(0).Columns.FromKey("monCashInflow").Header.Title = "Sum of all Income Accounts & A/R"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monSalesForecast").Header.Title = "Sum of sales forecasts for each month minus total amount sold for a each month"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monTotalBudgetBalance").Header.Title = "Sum of budget allocated[operation,Procurement & Marketing budget] for each month"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monProjectedBankBalance").Header.Title = "Cash on hand plus cash inflow plus sales forecast minus Total  budget balance "

    '        ulgImpactBudget.Bands(0).Columns.FromKey("monAmount").HeaderText = "Budget Allocated"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("vcmonthName").HeaderText = "Month"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monCashInflow").HeaderText = "Cash Inflows"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monSalesForecast").HeaderText = "Sales Forecast (unclosed deals)"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monTotalBudgetBalance").HeaderText = "Total Budget Balance"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monProjectedBankBalance").HeaderText = "Projected Bank Balance"

    '        ulgImpactBudget.Bands(0).Columns.FromKey("monAmount").DataType = "System.Decimal"

    '        ulgImpactBudget.Bands(0).Columns.FromKey("monAmount").Format = "###,###.##"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monCashInflow").Format = "###,###.##"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monSalesForecast").Format = "###,###.##"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monTotalBudgetBalance").Format = "###,###.##"
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monProjectedBankBalance").Format = "###,###.##"

    '        ''ulgImpactBudget.Bands(0).Columns.FromKey("monAmount").AllowUpdate = AllowUpdate.No
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monCashInflow").AllowUpdate = AllowUpdate.No
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monSalesForecast").AllowUpdate = AllowUpdate.No
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monTotalBudgetBalance").AllowUpdate = AllowUpdate.No
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monProjectedBankBalance").AllowUpdate = AllowUpdate.No

    '        ulgImpactBudget.Bands(0).Columns.FromKey("vcmonth").Width = Unit.Percentage("15")
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monAmount").Width = Unit.Percentage("12")
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monCashInflow").Width = Unit.Percentage("8")
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monSalesForecast").Width = Unit.Percentage("22")
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monTotalBudgetBalance").Width = Unit.Percentage("22")
    '        ulgImpactBudget.Bands(0).Columns.FromKey("monProjectedBankBalance").Width = Unit.Percentage("21")
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlProcurementBudgetAnalyze_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcurementBudgetAnalyze.SelectedIndexChanged
    '    Try
    '        DisplayProcurementBudgetAnalyze()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub DisplayProcurementBudgetAnalyze()
    '    Try
    '        If ddlProcurementBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            ''Dim lintBudgetId As Integer
    '            '' ''Dim Id As Integer
    '            ''If ddlProcurementBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            ''    lintBudgetId = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '            ''    Id = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            ''Else
    '            ''    Id = 0
    '            ''End If

    '            mobjBudget.BudgetId = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '            mobjBudget.DomainId = Session("DomainId")
    '            mobjBudget.sintByte = 2 ''IIf(ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(0) = 0, 3, 2)

    '            Select Case rdlForecastType.SelectedIndex
    '                Case 0
    '                    mobjBudget.sintForecast = 0
    '                Case 1
    '                    mobjBudget.sintForecast = 1
    '            End Select

    '            Select Case ddlProcurementBudget.SelectedIndex
    '                Case 1
    '                    mobjBudget.FiscalYear = Now.Year
    '                    mobjBudget.Type = 0
    '                Case 2
    '                    mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                    mobjBudget.Type = -1
    '                Case 3
    '                    mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                    mobjBudget.Type = 1
    '            End Select
    '            mobjBudget.Id = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            mobjBudget.UserCntID = Session("UserContactId")
    '            ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '            ulgImpactBudget.DataBind()
    '        Else : ulgImpactBudget.Rows.Clear()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlMarketBudgetAnalyze_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMarketBudgetAnalyze.SelectedIndexChanged
    '    Try
    '        DisplayMarketBudgetAnalyze()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub DisplayMarketBudgetAnalyze()
    '    Try
    '        '' If ddlMarketBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then

    '        ''Dim lintBudgetId As Integer
    '        ''Dim Id As Integer
    '        ''If ddlMarketBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '        ''    lintBudgetId = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '        ''    Id = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '        ''Else
    '        ''    lintBudgetId = 0
    '        ''    Id = 0
    '        ''End If
    '        If ddlMarketBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            mobjBudget.BudgetId = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '            mobjBudget.DomainId = Session("DomainId")
    '            mobjBudget.sintByte = 1 '' IIf(ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(0) = 0, 3, 1)

    '            Select Case rdlForecastType.SelectedIndex
    '                Case 0
    '                    mobjBudget.sintForecast = 0
    '                Case 1
    '                    mobjBudget.sintForecast = 1
    '            End Select

    '            Select Case ddlMarketBudget.SelectedIndex
    '                Case 1
    '                    mobjBudget.FiscalYear = Now.Year
    '                    mobjBudget.Type = 0
    '                Case 2
    '                    mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                    mobjBudget.Type = -1
    '                Case 3
    '                    mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                    mobjBudget.Type = 1
    '            End Select

    '            mobjBudget.Id = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            mobjBudget.UserCntID = Session("UserContactId")
    '            ulgImpactBudget.DataSource = mobjBudget.GetOperationBudgetMasterForBudgetAnalyze()
    '            ulgImpactBudget.DataBind()
    '        Else : ulgImpactBudget.Rows.Clear()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
    '    Try
    '        Dim ulrow As UltraGridRow
    '        Dim originalAmt, TotalBudgetBalance, ProjectedBankBalance, decTotalBudgetBalance, decProjectedBankBalance, decMonthlyAmt As Decimal
    '        Dim totaloriginalamt, TotalAmount As Decimal
    '        totaloriginalamt = 0
    '        TotalAmount = 0
    '        For Each ulrow In ulgImpactBudget.Rows
    '            originalAmt = 0
    '            TotalBudgetBalance = 0
    '            ProjectedBankBalance = 0
    '            decMonthlyAmt = 0
    '            decTotalBudgetBalance = 0
    '            decProjectedBankBalance = 0
    '            originalAmt = IIf(ulrow.Cells.FromKey("monOriginalAmt").Value = Nothing, 0, ulrow.Cells.FromKey("monOriginalAmt").Value)
    '            totaloriginalamt = totaloriginalamt + originalAmt
    '            decMonthlyAmt = IIf(ulrow.Cells.FromKey("monAmount").Value = Nothing, 0, ulrow.Cells.FromKey("monAmount").Value)
    '            TotalAmount = TotalAmount + decMonthlyAmt
    '            TotalBudgetBalance = IIf(ulrow.Cells.FromKey("monTotalBudgetBalance").Value = Nothing, 0, ulrow.Cells.FromKey("monTotalBudgetBalance").Value)
    '            decTotalBudgetBalance = TotalBudgetBalance - originalAmt + Replace(decMonthlyAmt, ",", "")
    '            ProjectedBankBalance = IIf(ulrow.Cells.FromKey("monProjectedBankBalance").Value = Nothing, 0, ulrow.Cells.FromKey("monProjectedBankBalance").Value)
    '            decProjectedBankBalance = ProjectedBankBalance + totaloriginalamt - Replace(TotalAmount, ",", "")
    '            ulrow.Cells.FromKey("monTotalBudgetBalance").Value = decTotalBudgetBalance
    '            ulrow.Cells.FromKey("monProjectedBankBalance").Value = decProjectedBankBalance
    '            ulrow.Cells.FromKey("monOriginalAmt").Value = IIf(ulrow.Cells.FromKey("monAmount").Value = Nothing, "", ulrow.Cells.FromKey("monAmount").Value)

    '            If Left(ulrow.Cells.FromKey("monProjectedBankBalance").Value, 1) <> "-" Then
    '                ulrow.Cells.FromKey("monProjectedBankBalance").Style.ForeColor = Color.Green
    '                ulrow.Cells.FromKey("monProjectedBankBalance").Style.CustomRules = "font-weight: bold"
    '            Else
    '                ulrow.Cells.FromKey("monProjectedBankBalance").Style.ForeColor = Color.Red
    '                ulrow.Cells.FromKey("monProjectedBankBalance").Style.CustomRules = "font-weight: bold"
    '            End If
    '        Next
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnBudgetImpactSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBudgetImpactSave.Click
    '    Try
    '        If rdbOperationBudget.Checked = True AndAlso ddlOperationBudget.SelectedItem.Value.ToString <> "0" AndAlso ddlOperationBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            SaveOperationBudget()
    '        ElseIf rdbMarketingBudget.Checked = True AndAlso ddlMarketBudget.SelectedItem.Value <> 0 AndAlso ddlMarketBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            SaveMarketBudget()
    '        ElseIf rdbProcurementBudget.Checked = True AndAlso ddlProcurementBudget.SelectedItem.Value <> 0 AndAlso ddlProcurementBudgetAnalyze.SelectedItem.Value.ToString <> "0" Then
    '            SaveProcurementBudget()
    '        End If

    '        If ddlOperationBudgetAnalyze.Items.Count <> 0 Then
    '            ''LoadTree(ddlOperationBudget.SelectedItem.Value, 0)
    '            DisplayOperationBudgetAnalyze()
    '        ElseIf ddlMarketBudgetAnalyze.Items.Count <> 0 Then
    '            ''LoadTree(ddlMarketBudget.SelectedItem.Value, 1)
    '            DisplayMarketBudgetAnalyze()
    '        ElseIf ddlProcurementBudgetAnalyze.Items.Count <> 0 Then
    '            ''LoadTree(ddlProcurementBudget.SelectedItem.Value, 2)
    '            DisplayProcurementBudgetAnalyze()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub SaveMarketBudget()
    '    Try
    '        Dim dtMarketBudget As New DataTable
    '        Dim ulrow As UltraGridRow
    '        Dim dr As DataRow
    '        Dim ds As New DataSet
    '        Dim lstr As String
    '        Dim i As Integer

    '        dtMarketBudget.Columns.Add("numMarketId") '' Primary key
    '        dtMarketBudget.Columns.Add("numListItemID")
    '        dtMarketBudget.Columns.Add("tintMonth")
    '        dtMarketBudget.Columns.Add("intYear")
    '        dtMarketBudget.Columns.Add("monAmount")

    '        For Each ulrow In ulgImpactBudget.Rows

    '            dr = dtMarketBudget.NewRow
    '            dr("numMarketId") = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '            dr("numListItemID") = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            dr("tintMonth") = ulrow.Cells.FromKey("vcmonth").Value.Split("~")(0)
    '            dr("intYear") = ulrow.Cells.FromKey("vcmonth").Value.Split("~")(1)
    '            '' dr("monAmount") = ulrow.Cells.FromKey("monAmount").Value
    '            If Not IsNothing(ulrow.Cells.FromKey("monAmount").Value) Then
    '                dr("monAmount") = IIf(ulrow.Cells.FromKey("monAmount").Value.ToString = "False", 0, ulrow.Cells.FromKey("monAmount").Value)
    '            Else : dr("monAmount") = ulrow.Cells.FromKey("monAmount").Value
    '            End If
    '            dtMarketBudget.Rows.Add(dr)
    '        Next

    '        mobjBudget.ByteMode = 1
    '        Select Case ddlMarketBudget.SelectedIndex
    '            Case 1
    '                mobjBudget.FiscalYear = Now.Year
    '                mobjBudget.Type = 0
    '            Case 2
    '                mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                mobjBudget.Type = -1
    '            Case 3
    '                mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                mobjBudget.Type = 1
    '        End Select
    '        mobjBudget.MarketBudgetId = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '        mobjBudget.Id = ddlMarketBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '        ds.Tables.Add(dtMarketBudget)
    '        lstr = ds.GetXml()
    '        mobjBudget.MarketBudgetDetails = lstr
    '        mobjBudget.SaveMarketBudgetDetails()

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub SaveProcurementBudget()
    '    Try
    '        Dim dtProcurementBudget As New DataTable
    '        Dim ulrow As UltraGridRow
    '        Dim dr As DataRow
    '        Dim ds As New DataSet
    '        Dim lstr As String
    '        Dim i As Integer

    '        dtProcurementBudget.Columns.Add("numProcurementId") '' Primary key
    '        dtProcurementBudget.Columns.Add("numItemGroupId")
    '        dtProcurementBudget.Columns.Add("tintMonth")
    '        dtProcurementBudget.Columns.Add("intYear")
    '        dtProcurementBudget.Columns.Add("monAmount")

    '        For Each ulrow In ulgImpactBudget.Rows

    '            dr = dtProcurementBudget.NewRow
    '            dr("numProcurementId") = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '            dr("numItemGroupId") = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            dr("tintMonth") = ulrow.Cells.FromKey("vcmonth").Value.Split("~")(0)
    '            dr("intYear") = ulrow.Cells.FromKey("vcmonth").Value.Split("~")(1)
    '            '' dr("monAmount") = ulrow.Cells.FromKey("monAmount").Value
    '            If Not IsNothing(ulrow.Cells.FromKey("monAmount").Value) Then
    '                dr("monAmount") = IIf(ulrow.Cells.FromKey("monAmount").Value.ToString = "False", 0, ulrow.Cells.FromKey("monAmount").Value)
    '            Else : dr("monAmount") = ulrow.Cells.FromKey("monAmount").Value
    '            End If
    '            dtProcurementBudget.Rows.Add(dr)
    '        Next
    '        Select Case ddlProcurementBudget.SelectedIndex
    '            Case 1
    '                mobjBudget.FiscalYear = Now.Year
    '                mobjBudget.Type = 0
    '            Case 2
    '                mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                mobjBudget.Type = -1
    '            Case 3
    '                mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                mobjBudget.Type = 1
    '        End Select
    '        mobjBudget.ProcurementBudgetId = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '        mobjBudget.Id = ddlProcurementBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '        mobjBudget.ByteMode = 1
    '        ds.Tables.Add(dtProcurementBudget)
    '        lstr = ds.GetXml()
    '        mobjBudget.ProcurementBudgetDetails = lstr
    '        mobjBudget.SaveProcurementBudgetDetails()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub SaveOperationBudget()
    '    Try
    '        Dim dtBudget As New DataTable
    '        Dim ds As New DataSet
    '        '' Dim i As Integer
    '        Dim ulrow As UltraGridRow
    '        Dim dr As DataRow
    '        Dim lstr As String

    '        dtBudget.Columns.Add("BudgetDetId") '' Primary key
    '        dtBudget.Columns.Add("numBudgetId")
    '        dtBudget.Columns.Add("numChartAcntId")
    '        dtBudget.Columns.Add("numParentAcntId")
    '        dtBudget.Columns.Add("tintMonth")
    '        dtBudget.Columns.Add("intYear")
    '        dtBudget.Columns.Add("monAmount")

    '        For Each ulrow In ulgImpactBudget.Rows
    '            dr = dtBudget.NewRow
    '            dr("BudgetDetId") = 0
    '            dr("numBudgetId") = ddlOperationBudget.SelectedItem.Value
    '            dr("numChartAcntId") = ddlOperationBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '            dr("numParentAcntId") = ddlOperationBudgetAnalyze.SelectedItem.Value.Split("~")(1)
    '            dr("tintMonth") = ulrow.Cells.FromKey("vcmonth").Value.Split("~")(0)
    '            dr("intYear") = ulrow.Cells.FromKey("vcmonth").Value.Split("~")(1)
    '            ''dr("monAmount") = ulrow.Cells.FromKey("monAmount").Value

    '            If Not IsNothing(ulrow.Cells.FromKey("monAmount").Value) Then
    '                dr("monAmount") = IIf(ulrow.Cells.FromKey("monAmount").Value.ToString = "False", 0, ulrow.Cells.FromKey("monAmount").Value)
    '            Else : dr("monAmount") = ulrow.Cells.FromKey("monAmount").Value
    '            End If
    '            dtBudget.Rows.Add(dr)
    '        Next

    '        mobjBudget.BudgetId = ddlOperationBudget.SelectedItem.Value
    '        mobjBudget.Id = ddlOperationBudgetAnalyze.SelectedItem.Value.Split("~")(0)
    '        mobjBudget.FiscalYear = Year(Now.Date)
    '        ds.Tables.Add(dtBudget)
    '        lstr = ds.GetXml()
    '        mobjBudget.BudgetDetails = lstr
    '        mobjBudget.ByteMode = 1
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.SaveOperationBudgetDetails()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ulgImpactBudget_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles ulgImpactBudget.InitializeRow
    '    Try
    '        e.Row.Cells.FromKey("monCashInflow").TargetURL = "javascript:OpenDefineChartOfAccounts();"
    '        If Left(e.Row.Cells.FromKey("monProjectedBankBalance").Value, 1) <> "-" Then
    '            e.Row.Cells.FromKey("monProjectedBankBalance").Style.ForeColor = Color.Green
    '            e.Row.Cells.FromKey("monProjectedBankBalance").Style.CustomRules = "font-weight: bold"
    '        Else
    '            e.Row.Cells.FromKey("monProjectedBankBalance").Style.ForeColor = Color.Red
    '            e.Row.Cells.FromKey("monProjectedBankBalance").Style.CustomRules = "font-weight: bold"
    '        End If

    '        If Left(e.Row.Cells.FromKey("monCashInflow").Value, 1) <> "-" Then
    '            e.Row.Cells.FromKey("monCashInflow").Style.ForeColor = Color.Green
    '            e.Row.Cells.FromKey("monCashInflow").Style.CustomRules = "font-weight: bold"
    '        Else
    '            e.Row.Cells.FromKey("monCashInflow").Style.ForeColor = Color.Red
    '            ''  e.Row.Cells.FromKey("monProjectedBankBalance").Style.CustomRules = "font-weight: bold"
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub rdlForecastType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlForecastType.SelectedIndexChanged
    '    Try
    '        If ddlOperationBudgetAnalyze.Items.Count <> 0 Then
    '            DisplayOperationBudgetAnalyze()
    '        ElseIf ddlMarketBudgetAnalyze.Items.Count <> 0 Then
    '            DisplayMarketBudgetAnalyze()
    '        ElseIf ddlProcurementBudgetAnalyze.Items.Count <> 0 Then
    '            DisplayProcurementBudgetAnalyze()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnBudgetImpactClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBudgetImpactClose.Click
    '    Try
    '        If ddlOperationBudgetAnalyze.Items.Count <> 0 Then
    '            ddlOperationBudgetAnalyze.SelectedIndex = 0
    '            ulgImpactBudget.Rows.Clear()
    '        ElseIf ddlMarketBudgetAnalyze.Items.Count <> 0 Then
    '            ddlMarketBudgetAnalyze.SelectedIndex = 0
    '            ulgImpactBudget.Rows.Clear()
    '        ElseIf ddlProcurementBudgetAnalyze.Items.Count <> 0 Then
    '            ddlProcurementBudgetAnalyze.SelectedIndex = 0
    '            ulgImpactBudget.Rows.Clear()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class