Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Partial Public Class frmBankReconcile
    Inherits BACRMPage

    Dim objBankReconcile As BankReconcile
    Dim dtChartAcnt As DataTable
    Dim lngAcntId As Long
    Dim lngReconcileID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If Not IsPostBack Then
                lngAcntId = CCommon.ToLong(GetQueryStringVal("AcntId"))
                lngReconcileID = CCommon.ToLong(GetQueryStringVal("ReconcileID"))

                calStatementDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calInterestEarned.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calServiceChargeDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                LoadChartType()

                If lngAcntId > 0 Then
                    If ddlAccount.Items.FindByValue(lngAcntId) IsNot Nothing Then
                        ddlAccount.ClearSelection()
                        ddlAccount.Items.FindByValue(lngAcntId).Selected = True
                    End If

                End If

                If lngReconcileID > 0 Then
                    divFile.Visible = False
                    divFileColumns.Visible = False
                    LoadPendingReconcile()
                ElseIf ddlAccount.SelectedValue > 0 Then
                    CheckPendingReconcile()
                End If
                btnSave.Attributes.Add("onclick", "return Save()")

                DirectCast(Page.Master.Master.FindControl("ScriptManager1"), ScriptManager).RegisterPostBackControl(btnSave)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub LoadPendingReconcile()
        Try
            If objBankReconcile Is Nothing Then objBankReconcile = New BankReconcile

            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 1
            objBankReconcile.ReconcileID = lngReconcileID

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()

            Dim dtReconcile As DataTable = ds.Tables(0)

            If dtReconcile.Rows.Count > 0 Then
                hfReconcileID.Value = dtReconcile.Rows(0)("numReconcileID")

                'Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & lngReconcileID)

                txtBeginingBalance.Text = ReturnMoney(dtReconcile.Rows(0)("monBeginBalance"))
                txtEndingBalance.Text = ReturnMoney(dtReconcile.Rows(0)("monEndBalance"))
                calStatementDate.SelectedDate = dtReconcile.Rows(0)("dtStatementDate")
                If ddlAccount.Items.FindByValue(dtReconcile.Rows(0)("numChartAcntId")) IsNot Nothing Then
                    ddlAccount.ClearSelection()
                    ddlAccount.Items.FindByValue(dtReconcile.Rows(0)("numChartAcntId")).Selected = True
                End If

                'Service Charge
                txtServiceCharge.Text = ReturnMoney(dtReconcile.Rows(0)("monServiceChargeAmount"))
                calServiceChargeDate.SelectedDate = dtReconcile.Rows(0)("dtServiceChargeDate")
                If ddlServiceChargeAcnt.Items.FindByValue(dtReconcile.Rows(0)("numServiceChargeChartAcntId")) IsNot Nothing Then
                    ddlServiceChargeAcnt.ClearSelection()
                    ddlServiceChargeAcnt.Items.FindByValue(dtReconcile.Rows(0)("numServiceChargeChartAcntId")).Selected = True
                End If

                'Interest Earned
                txtInterestEarned.Text = ReturnMoney(dtReconcile.Rows(0)("monInterestEarnedAmount"))
                calInterestEarned.SelectedDate = dtReconcile.Rows(0)("dtInterestEarnedDate")
                If ddlInterestEarned.Items.FindByValue(dtReconcile.Rows(0)("numInterestEarnedChartAcntId")) IsNot Nothing Then
                    ddlInterestEarned.ClearSelection()
                    ddlInterestEarned.Items.FindByValue(dtReconcile.Rows(0)("numInterestEarnedChartAcntId")).Selected = True
                End If
            Else
                hfReconcileID.Value = "0"

                calStatementDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calInterestEarned.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calServiceChargeDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                txtEndingBalance.Text = ""
                txtInterestEarned.Text = "0"
                txtServiceCharge.Text = "0"

                ddlInterestEarned.SelectedValue = 0
                ddlServiceChargeAcnt.SelectedValue = 0
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub CheckPendingReconcile()
        Try
            If objBankReconcile Is Nothing Then objBankReconcile = New BankReconcile

            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 4
            objBankReconcile.ReconcileComplete = False
            objBankReconcile.AcntId = ddlAccount.SelectedValue

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()

            Dim dtReconcile As DataTable = ds.Tables(0)

            If dtReconcile.Rows.Count > 0 Then
                hfReconcileID.Value = dtReconcile.Rows(0)("numReconcileID")
                divFile.Visible = False
                divFileColumns.Visible = False
                'Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & lngReconcileID)

                txtBeginingBalance.Text = ReturnMoney(dtReconcile.Rows(0)("monBeginBalance"))
                txtEndingBalance.Text = ReturnMoney(dtReconcile.Rows(0)("monEndBalance"))
                calStatementDate.SelectedDate = dtReconcile.Rows(0)("dtStatementDate")
                If ddlAccount.Items.FindByValue(dtReconcile.Rows(0)("numChartAcntId")) IsNot Nothing Then
                    ddlAccount.ClearSelection()
                    ddlAccount.Items.FindByValue(dtReconcile.Rows(0)("numChartAcntId")).Selected = True
                End If

                'Service Charge
                txtServiceCharge.Text = ReturnMoney(dtReconcile.Rows(0)("monServiceChargeAmount"))
                calServiceChargeDate.SelectedDate = dtReconcile.Rows(0)("dtServiceChargeDate")
                If ddlServiceChargeAcnt.Items.FindByValue(dtReconcile.Rows(0)("numServiceChargeChartAcntId")) IsNot Nothing Then
                    ddlServiceChargeAcnt.ClearSelection()
                    ddlServiceChargeAcnt.Items.FindByValue(dtReconcile.Rows(0)("numServiceChargeChartAcntId")).Selected = True
                End If

                'Interest Earned
                txtInterestEarned.Text = ReturnMoney(dtReconcile.Rows(0)("monInterestEarnedAmount"))
                calInterestEarned.SelectedDate = dtReconcile.Rows(0)("dtInterestEarnedDate")
                If ddlInterestEarned.Items.FindByValue(dtReconcile.Rows(0)("numInterestEarnedChartAcntId")) IsNot Nothing Then
                    ddlInterestEarned.ClearSelection()
                    ddlInterestEarned.Items.FindByValue(dtReconcile.Rows(0)("numInterestEarnedChartAcntId")).Selected = True
                End If
            Else
                hfReconcileID.Value = "0"

                calStatementDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calInterestEarned.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calServiceChargeDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                txtEndingBalance.Text = ""
                txtInterestEarned.Text = "0"
                txtServiceCharge.Text = "0"

                ddlInterestEarned.SelectedValue = 0
                ddlServiceChargeAcnt.SelectedValue = 0

                GetBeginningBalance()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub GetBeginningBalance()
        Try
            Dim objBankReconcile As New BankReconcile
            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 6
            objBankReconcile.ReconcileComplete = True
            objBankReconcile.AcntId = ddlAccount.SelectedValue

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()

            Dim dtReconcile As DataTable = ds.Tables(0)

            If dtReconcile.Rows.Count > 0 Then
                txtBeginingBalance.Text = ReturnMoney(CCommon.ToDecimal(dtReconcile.Rows(0)("monEndBalance")))
            Else
                Dim lobjCashCreditCard As New CashCreditCard
                Dim ldecOpeningBalance As Decimal = 0
                lobjCashCreditCard.AccountId = ddlAccount.SelectedValue
                lobjCashCreditCard.DomainID = Session("DomainId")
                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()

                txtBeginingBalance.Text = ReturnMoney(ldecOpeningBalance)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LoadChartType()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim dt, dt1 As DataTable
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "01010101" 'bank accounts
            dt = objCOA.GetParentCategory()

            objCOA.AccountCode = "01020103" 'For credit card liability
            dt1 = objCOA.GetParentCategory()
            dt.Merge(dt1)

            Dim item As ListItem

            For Each dr As DataRow In dt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccount.Items.Add(item)
            Next

            ddlAccount.Items.Insert(0, New ListItem("--Select One --", "0"))


            dt.Rows.Clear()
            objCOA.AccountCode = "0103" 'Income
            dt = objCOA.GetParentCategory()

            objCOA.AccountCode = "0104" 'Expense
            dt1 = objCOA.GetParentCategory()
            dt.Merge(dt1)

            For Each dr As DataRow In dt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")

                ddlServiceChargeAcnt.Items.Add(item)
            Next

            For Each dr As DataRow In dt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")

                ddlInterestEarned.Items.Add(item)
            Next

            ddlServiceChargeAcnt.Items.Insert(0, New ListItem("--Select One --", "0"))
            ddlInterestEarned.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CCommon.ToLong(hfReconcileID.Value) = 0 AndAlso fuStatement.HasFile Then
                Dim ext As String = System.IO.Path.GetExtension(fuStatement.PostedFile.FileName)

                If ext <> ".csv" Then
                    ShowMessage("Only csv files are allowed.")
                    Exit Sub
                End If


                If fuStatement.PostedFile.ContentLength > 51200 Then
                    ShowMessage("Maximum allowed file size is 50kb.")
                    Exit Sub
                End If
            End If

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                If objBankReconcile Is Nothing Then objBankReconcile = New BankReconcile

                objBankReconcile.DomainID = Session("DomainId")
                objBankReconcile.UserCntID = Session("UserContactID")
                objBankReconcile.Mode = 2

                objBankReconcile.ReconcileID = hfReconcileID.Value
                objBankReconcile.BeginBalance = CCommon.ToDecimal(txtBeginingBalance.Text)
                objBankReconcile.EndBalance = CCommon.ToDecimal(txtEndingBalance.Text)
                objBankReconcile.StatementDate = CDate(calStatementDate.SelectedDate & " 12:00:00")
                objBankReconcile.AcntId = ddlAccount.SelectedValue

                objBankReconcile.ServiceChargeAmt = CCommon.ToDecimal(txtServiceCharge.Text)
                objBankReconcile.ServiceChargeDate = CDate(IIf(calServiceChargeDate.SelectedDate <> "", calServiceChargeDate.SelectedDate, calStatementDate.SelectedDate) & " 12:00:00")
                objBankReconcile.ServiceChargeChartAcntId = ddlServiceChargeAcnt.SelectedItem.Value

                objBankReconcile.InterestEarnedAmt = CCommon.ToDecimal(txtInterestEarned.Text)
                objBankReconcile.InterestEarnedDate = CDate(IIf(calInterestEarned.SelectedDate <> "", calInterestEarned.SelectedDate, calStatementDate.SelectedDate) & " 12:00:00")
                objBankReconcile.InterestEarnedChartAcntId = ddlInterestEarned.SelectedItem.Value

                Dim filePath As String = ""
                'New Reconcile then upload file data
                If CCommon.ToLong(hfReconcileID.Value) = 0 AndAlso fuStatement.HasFile Then
                    Dim i As Integer = 2

                    Try
                        objBankReconcile.FileName = "BR_" & DateTime.Now.ToString("ddmmyyyyhhmmss") & "_" & fuStatement.PostedFile.FileName
                        filePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & objBankReconcile.FileName
                        fuStatement.SaveAs(filePath)

                        Dim dt As New DataTable("Trasactions")
                        dt.Columns.Add("dtEntryDate", GetType(String))
                        dt.Columns.Add("vcReference", GetType(String))
                        dt.Columns.Add("fltAmount", GetType(Double))
                        dt.Columns.Add("vcPayee", GetType(String))
                        dt.Columns.Add("vcDescription", GetType(String))

                        Using csvReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(filePath)
                            csvReader.SetDelimiters(New String() {","})
                            csvReader.HasFieldsEnclosedInQuotes = True


                            Dim listCSVColumns As New System.Collections.Generic.List(Of FileColumn)
                            'read column names  
                            Dim colFields As String() = csvReader.ReadFields()
                            Dim j As Integer = 0
                            For Each column As String In colFields
                                If column.ToUpper() = "DATE" Then
                                    listCSVColumns.Add(New FileColumn With {.Name = "Date", .Value = "dtEntryDate", .Index = j})
                                ElseIf column.ToUpper() = "REFERENCE" Then
                                    listCSVColumns.Add(New FileColumn With {.Name = "Reference", .Value = "vcReference", .Index = j})
                                ElseIf column.ToUpper() = "AMOUNT" Then
                                    listCSVColumns.Add(New FileColumn With {.Name = "Amount", .Value = "fltAmount", .Index = j})
                                ElseIf column.ToUpper() = "PAYEE" Then
                                    listCSVColumns.Add(New FileColumn With {.Name = "Payee", .Value = "vcPayee", .Index = j})
                                ElseIf column.ToUpper() = "DESCRIPTION" Then
                                    listCSVColumns.Add(New FileColumn With {.Name = "Description", .Value = "vcDescription", .Index = j})
                                End If

                                j = j + 1
                            Next

                            If listCSVColumns.Where(Function(x) x.Name = "Date").Count = 0 Or
                                listCSVColumns.Where(Function(x) x.Name = "Reference").Count = 0 Or
                                    listCSVColumns.Where(Function(x) x.Name = "Amount").Count = 0 Or
                                    listCSVColumns.Where(Function(x) x.Name = "Payee").Count = 0 Or
                                    listCSVColumns.Where(Function(x) x.Name = "Description").Count = 0 Then
                                DisplayError("Uploaded CSV file must have columns Date,Reference,Amount,Payee,Description")
                                Exit Sub
                            End If


                            Dim indexDate As Integer = listCSVColumns.Where(Function(x) x.Name = "Date").First().Index
                            Dim indexReference As Integer = listCSVColumns.Where(Function(x) x.Name = "Reference").First().Index
                            Dim indexAmount As Integer = listCSVColumns.Where(Function(x) x.Name = "Amount").First().Index
                            Dim indexPayee As Integer = listCSVColumns.Where(Function(x) x.Name = "Payee").First().Index
                            Dim indexDescription As Integer = listCSVColumns.Where(Function(x) x.Name = "Description").First().Index
                            While Not csvReader.EndOfData
                                Dim dr As DataRow = dt.NewRow
                                Dim dtEntryDate As Date
                                Dim amount As Double
                                Dim fieldData As String() = csvReader.ReadFields()

                                If String.IsNullOrEmpty(fieldData(indexDate)) Then
                                    Throw New Exception("Date is required.")
                                ElseIf Not Date.TryParse(fieldData(indexDate), dtEntryDate) Then
                                    Throw New Exception("Invalid date.")
                                End If

                                If Not Double.TryParse(fieldData(indexAmount), amount) Then
                                    Throw New Exception("Invalid Amount.")
                                End If


                                dr("dtEntryDate") = dtEntryDate
                                dr("vcReference") = CCommon.ToString(fieldData(indexReference))
                                dr("fltAmount") = amount
                                dr("vcPayee") = CCommon.ToString(fieldData(indexPayee))
                                dr("vcDescription") = CCommon.ToString(fieldData(indexDescription))

                                dt.Rows.Add(dr)

                                i = i + 1
                            End While
                        End Using

                        Dim dsFile As New DataSet("BankStatement")
                        dsFile.Tables.Add(dt)

                        objBankReconcile.FileData = dsFile.GetXml()
                    Catch ex As Exception
                        DisplayError("Data in line " & i & " of csv is not correct:" & ex.Message)
                        Exit Sub
                    End Try
                End If

                Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster(True)

                If (ds.Tables.Count > 0) Then
                    hfReconcileID.Value = ds.Tables(0).Rows(0)("numReconcileID")

                    If CCommon.ToDecimal(txtServiceCharge.Text) > 0 Or CCommon.ToDecimal(txtInterestEarned.Text) > 0 Then
                        Dim lngJournalId As Long

                        objCommon.DomainID = Session("DomainID")
                        objCommon.Str = CCommon.ToLong(hfReconcileID.Value)
                        objCommon.Mode = 40

                        lngJournalId = CCommon.ToLong(objCommon.GetSingleFieldValue())

                        lngJournalId = SaveDataToHeader(lngJournalId)
                        SaveDataToGeneralJournalDetails(lngJournalId)
                    End If



                    objTransactionScope.Complete()
                    Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & hfReconcileID.Value, False)
                Else
                    objTransactionScope.Complete()
                End If
            End Using
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try

    End Sub

    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader(lngJournalId As Long) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lngJournalId
                .RecurringId = 0
                .EntryDate = CDate(calStatementDate.SelectedDate & " 12:00:00") 'bug id 1028
                .Description = "Bank Reconcile : Service Charge,Interest Earned Statement Date " & calStatementDate.SelectedDate
                .Amount = CCommon.ToDecimal(txtInterestEarned.Text) + CCommon.ToDecimal(txtServiceCharge.Text)
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = ddlAccount.SelectedValue
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .IsReconcileInterest = True
                .ReconcileID = CCommon.ToLong(hfReconcileID.Value)
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Long)
        Dim i As Integer
        Dim lstr As String
        Dim ds As New DataSet
        Dim lintAccountId() As String
        Try
            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()

            If CCommon.ToDecimal(txtServiceCharge.Text) > 0 Then
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = CCommon.ToDecimal(txtServiceCharge.Text)
                objJE.ChartAcntId = ddlAccount.SelectedValue
                objJE.Description = "Bank Reconcile : Service Charge " & IIf(calServiceChargeDate.SelectedDate <> "", calServiceChargeDate.SelectedDate, calStatementDate.SelectedDate)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = CCommon.ToDecimal(txtServiceCharge.Text)
                objJE.CreditAmt = 0
                objJE.ChartAcntId = ddlServiceChargeAcnt.SelectedValue
                objJE.Description = "Bank Reconcile : Service Charge " & IIf(calServiceChargeDate.SelectedDate <> "", calServiceChargeDate.SelectedDate, calStatementDate.SelectedDate)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            End If

            If CCommon.ToDecimal(txtInterestEarned.Text) > 0 Then
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = CCommon.ToDecimal(txtInterestEarned.Text)
                objJE.CreditAmt = 0
                objJE.ChartAcntId = ddlAccount.SelectedValue
                objJE.Description = "Bank Reconcile : Interest Earned " & IIf(calInterestEarned.SelectedDate <> "", calInterestEarned.SelectedDate, calStatementDate.SelectedDate)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = CCommon.ToDecimal(txtInterestEarned.Text)
                objJE.ChartAcntId = ddlInterestEarned.SelectedValue
                objJE.Description = "Bank Reconcile : Interest Earned " & IIf(calInterestEarned.SelectedDate <> "", calInterestEarned.SelectedDate, calStatementDate.SelectedDate)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            End If

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlAccount_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            CheckPendingReconcile()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master, BizMaster).DisplayError(exception)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub


    Private Class FileColumn
        Public Property Name As String
        Public Property Value As String
        Public Property Index As Int32
    End Class
End Class