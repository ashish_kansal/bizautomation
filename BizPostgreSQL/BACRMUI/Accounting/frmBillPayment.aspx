﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBillPayment.aspx.vb"
    Inherits=".frmBillPayment" MasterPageFile="~/common/PopUp.Master" ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Enter Bill</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" language="javascript">
        function OpenPayBill() {
            window.open('../Accounting/frmBillPayment.aspx?I=1', '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }


        $(document).ready(function () {
            //            var duedateChage = window.validateDate;
            //            window.validateDate = function () {
            //                duedateChage('MMM-dd-yyyy');
            //            }

            InitializeValidation();
            $(".rcbInput").addClass("{required:'#radBill:checked', messages:{required:'Select organization!'}}");
            $('#litMessage').fadeIn().delay(5000).fadeOut();
        });

        function duedateChage(format) {

            if (!isDate($('#Content_calBillDate_txtDate').val(), format)) {
                alert("Enter Valid Bill date");
                $('#Content_calBillDate_txtDate').focus();
                return false;
            }

            var SDate = new Date(getDateFromFormat($('#Content_calBillDate_txtDate').val(), format));
            //var startDate = formatDate(d, 'dd-MM-yyyy');

            //alert('Bill Date :' + SDate)
            var ddDueDate = $('#ddlTerms option:selected').text();
            var dateFormat = document.getElementById('hfDateFormat').value;

            //alert(SDate);
            //alert(ddDueDate);
            startDate = new Date(SDate.getFullYear(), SDate.getMonth(), parseInt(SDate.getDate()) + parseInt(ddDueDate));
            $('#Content_calDueDate_txtDate').val(formatDate(startDate, format));
        }

    </script>
    <style type="text/css">
        .required! {
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="right">
        <tr>
            <td>
                <asp:Button ID="btnSaveNew" runat="server" CssClass="button" Text="Save & New" />
            </td>
            <td>
                <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close" />
            </td>
            <td>
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                    Text="Close" />
            </td>
            <td>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label Text="Enter Bill" runat="server" ID="lblTitle" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:Table ID="tblVendorPayment" CellPadding="0" CellSpacing="0" BorderWidth="1"
        runat="server" Width="700px" CssClass="aspTable" BorderColor="black" GridLines="None"
        Height="300">
        <asp:TableRow Width="100%">
            <asp:TableCell VerticalAlign="top">
                <br />
                <table width="700">
                    <tr>
                        <td width="10%"></td>
                        <td width="25%"></td>
                        <td width="10%"></td>
                        <td width="35%"></td>
                        <td width="10%"></td>
                        <td width="10%"></td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:RadioButton ID="radBill" GroupName="rad" runat="server" Text="Organization"
                                Checked="true" />
                            <font color="#ff0000">*</font>
                        </td>
                        <td class="normal1">
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True"
                                ClientIDMode="Static">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                            <%--<rad:RadComboBox ID="radCmbCompany" ExternalCallBackPage="../include/LoadCompany.aspx"
                                Width="195px" DropDownWidth="200px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                                AllowCustomText="True" EnableLoadOnDemand="True" SkinsPath="~/RadControls/ComboBox/Skins">
                            </rad:RadComboBox>--%>
                        </td>
                        <td class="normal1" align="right">
                            <asp:RadioButton ID="radLiability" GroupName="rad" runat="server" Text="Liabilities" /><font
                                color="#ff0000">*</font>
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlLiabilityAccount" runat="server" CssClass="{required:'#radLiability:checked', messages:{required:'Select Liability Account!'}}">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Bill Date <font color="#ff0000">*</font>
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calBillDate" runat="server" IsRequired="true" DateName="Bill Date"
                                ClientIDMode="Predictable" />
                            <asp:HiddenField ID="hfDateFormat" runat="server" />
                        </td>
                        <td class="normal1" align="right">Terms <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="normal1">
                                        <asp:DropDownList ID="ddlTerms" runat="server" Width="100px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="normal1" align="right">Due Date <font color="#ff0000">*</font>
                                    </td>
                                    <td class="normal1">
                                        <BizCalendar:Calendar ID="calDueDate" runat="server" IsRequired="true" DateName="Due Date"
                                            ClientIDMode="Predictable" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Payment Method
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlPayment" runat="server" Width="130" CssClass="signup" Visible="true">
                            </asp:DropDownList>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Reference
                        </td>
                        <td class="normal1">
                            <%--<asp:TextBox ID="txtReference" runat="server" CssClass="required_decimal {required:'#radLiability:checked' ,number:true, messages:{required:'Please provide value!',number:'provide valid value!'}}">
                            </asp:TextBox>--%>
                            <asp:TextBox ID="txtReference" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                    <%-- <tr>
                        <td class="normal1" align="right">
                            Class
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlClass" Width="207" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td colspan="4">
                        </td>
                    </tr>
                    <tr id="trProject" runat="server">
                        <td class="normal1" align="right">
                            Project
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlProject" Width="207" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td colspan="4">
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="6"></td>
                    </tr>
                    <tr>
                        <td colspan="6"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvBillItems" runat="server" ShowFooter="true" AutoGenerateColumns="false"
                                CssClass="dg">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <%--<asp:BoundField DataField="RowNumber" HeaderText="&nbsp;#&nbsp;" ItemStyle-HorizontalAlign="Center" />--%>
                                    <asp:TemplateField HeaderText="&nbsp;#&nbsp;" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Expense Account<font color=#ff0000>*</font>">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlExpenseAccount" runat="server" CssClass="{required:true,messages:{required:'Select Expense Account!'}}">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount<font color=#ff0000>*</font>">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" runat="server" Width="50px" AutoComplete="OFF" Text='<%# Eval("Amount") %>'
                                                CssClass="required_decimal {required:true,number:true, messages:{required:'Enter Amount! ',number:'provide valid value at <%# Container.DataItemIndex %> !'}}"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Memo">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMemo" runat="server" CssClass="signup" TextMode="MultiLine" Rows="1"
                                                Width="300px" Height="18px" Text='<%# Eval("Memo") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlProject" runat="server" Width="100px" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Class">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlClass" runat="server" CssClass="signup" Width="100px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Campaign">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="signup" Width="100px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add New Row" OnClick="btnAdd_Click" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteRow" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteRow"
                                                CommandArgument="<%# Container.DataItemIndex %>"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <script language="javascript" type="text/javascript">
                                //                                var duedateChage = window.validateDate; window.validateDate
                                //                            = function () { return duedateChage('dd-MMM-yyyy'); }
                            </script>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
