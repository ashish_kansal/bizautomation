﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmWriteCheck.aspx.vb" Inherits=".frmWriteCheck" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Write Check</title>
    <script type="text/javascript" src="../JavaScript/Common.js"></script>
    <script language="javascript" type="text/javascript">
        //$.noConflict();
        $(document).ready(function () {

            $(function () {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(initiate);
                prm.add_endRequest(CalculateTotal);
                prm.add_endRequest(Save);
                prm.add_endRequest(SplitCheckEnds);
                //prm.add_endRequest(windowResize);
                //prm.add_endRequest(OnClientItemsRequesting);
                initiate();
                CalculateTotal();
            });
            if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initiate);
            }
            else {
                //Sys.Application.add_init(initiate);
            }
            function initiate() {
                $('#chkPrintCheck').change(function () {
                    if ($(this).is(':checked')) {
                        $('#txtCheckNo').val('');
                    }
                    else {
                        $('#txtCheckNo').val($('#hfCheckNo').val());
                    }
                });
            }
        });

        var IsAddClicked = false;
        function Save() {
            if (IsAddClicked == true) {
                return false;
            }

            if (document.getElementById('ddlDepositTo').value == 0) {
                alert("Please Select Bank Account");
                document.getElementById('ddlDepositTo').focus();
                return false;
            }

            if ($find('ctl00_GridPlaceHolder_radCmbCompany').get_value() == "") {
                alert("Select Check Pay to the Order of")
                return false;
            }

            //            if (document.getElementById('txtAmount').value == "") {
            //                alert("Enter Check Amount");
            //                document.getElementById('txtAmount').focus();
            //                return false;
            //            }
            if (document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_calPaymentDate_txtDate').value == 0) {
                alert("Enter Payment Date")
                document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_calPaymentDate_txtDate').focus();
                return false;
            }

            if (!CalculateTotal(true))
                return false;

            if (CheckIsReconciled()) {
                IsAddClicked = true;
                return true;
            }
            else
                return false;
        }

        function CalculateTotal(TotalValidate) {
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var CrValue;
            var CrTotValue;
            var err = 0;

            $('#gvCheckDetails tr').not(':first,:last').each(function () {
                var txtAmount = $(this).find("input[id*='txtAmount']").val();
                ddlAccounts = $(this).find("[id*='ddlAccounts']");

                if (txtAmount != undefined && txtAmount != '') {
                    if ($(ddlAccounts).val() == 0) {
                        err = 1;
                        return false;
                    }

                    itotal += parseFloat(txtAmount.replace(",", ""));

                    if (parseFloat(txtAmount) < 0) {
                        err = 2;
                        return false;
                    }
                }
            });

            if (err == 1) {
                alert('You must select an account for each split line with an amount');
                return false;
            }
            else if (err == 2) {
                alert("Each amount must be greater than Zero.");
                return false;
            }

            if (TotalValidate == true && itotal == 0) {
                alert("Please enter alteast one line with an amount and account");
                return false;
            }

            formatDigit = parseFloat(itotal).toFixed(2);
            $('#lblTotal').text(formatDigit);
            $('#lblAmount').text(formatDigit);
            $('#hfTotal').val(formatDigit);

            if (itotal > 0)
                $('#lblAmountWords').text(toWords(formatDigit)).css('text-decoration', 'underline');
            else
                $('#lblAmountWords').text("");

            return true;
        }

        function DeleteRecord() {
            var str;
            str = 'Are you sure, you want to delete the selected record?'
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }

        function SplitCheckEnds() {
            window.opener.document.getElementById('btnHiddenSave').click();
            self.close();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                <asp:LinkButton ID="btnAllowCheckNO" runat="server" Visible="false">Yes</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Write Check&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmwritecheck.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />

    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Bank Account</label>
                <div class="form-inline">
                    <asp:DropDownList CssClass="form-control" ID="ddlDepositTo" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                    Balance:
                    <asp:Label ID="lblBalanceAmount" runat="server" Text="0.00" Style="display: inline-flex;"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Pay to the Order of</label>
                <div>
                    <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" DropDownWidth="600px"
                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                        ClientIDMode="Static"
                        ShowMoreResultsBox="true"
                        AutoPostBack="true"
                        Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True" Width="100%">
                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                    </telerik:RadComboBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Payment Date</label>
                <BizCalendar:Calendar ID="calPaymentDate" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Check #</label>
                <div class="form-inline">
                    <asp:TextBox ID="txtCheckNo" runat="server" CssClass="form-control" MaxLength="20" AutoComplete="OFF"></asp:TextBox>
                    <asp:CheckBox ID="chkPrintCheck" runat="server" Text="To be printed" Checked="true" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Amount</label>
                <div>
                    <asp:Label ID="lblAmount" runat="server" Text="0.00"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Memo</label>
                <asp:TextBox ID="txtMemo" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 text-center">
            <asp:Label ID="lblAmountWords" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvCheckDetails" runat="server" BorderWidth="0" CssClass="dgNHover table table-striped table-bordered"
                    AutoGenerateColumns="False" Style="margin-right: 0px" Width="100%" ShowFooter="true" UseAccessibleHeader="true">
                    <RowStyle CssClass="is" Height="40px" />
                    <HeaderStyle CssClass="hs" Height="40px" VerticalAlign="Middle" />
                    <FooterStyle CssClass="hs" Height="40px" />
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Account" ItemStyle-Width="210" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblCheckDetailID" runat="server" Style="display: none" Text='<%# Eval("numCheckDetailID") %>'></asp:Label>
                                <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# Eval("numTransactionId") %>'></asp:Label>
                                <asp:Label ID="lblAccountID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numChartAcntId") %>'></asp:Label>
                                <asp:DropDownList ID="ddlAccounts" runat="server" CssClass="form-control"
                                    onchange="CalculateTotal();">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel ID="updateFooter" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button btn btn-primary" Text="Add more row(s)" OnClick="btnAdd_Click" Style="float: left" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnAdd" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" AutoComplete="OFF" CssClass="money form-control" Text='<%# ReturnMoney(Eval("monAmount"))%>'
                                    onchange="CalculateTotal();"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDescription" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server" Text='<%# Eval("vcDescription") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class<span style='display:none'>CLS<span>" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlClass" runat="server" CssClass="signup form-control">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project<span style='display:none'>PRJT<span>">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlProject" runat="server" CssClass="signup form-control">
                                </asp:DropDownList>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="DeleteRow"
                                    CommandArgument=""><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">

            <div class="pull-right">
                <b>Total :
                    <asp:Label ID="lblTotal" runat="server" ForeColor="#4E9E19" Font-Size="Large"></asp:Label>
                    <asp:Label ID="lblSplitTotal" runat="server" ForeColor="#4E9E19" Font-Size="Large"
                        Visible="false"></asp:Label>
                </b>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="button btn btn-danger" OnClientClick="return confirm('Are you sure, you want to delete check?')"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnSplitSave" runat="server" CssClass="button" Text="Save & Close"
                    Visible="false"></asp:Button>
                <asp:Button ID="btnSave" runat="server" CssClass="button btn btn-primary" Text="Save"></asp:Button>
                <asp:Button ID="btnSaveNew" runat="server" CssClass="button btn btn-primary" Text="Save and New" OnClientClick="return Save();"></asp:Button>
                <asp:Button ID="btnSavePrint" runat="server" CssClass="button btn btn-primary" Text="Save and Print" OnClientClick="return Save();"></asp:Button>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hfTotal" runat="server" Value="0" />
    <asp:HiddenField ID="hfCheckHeaderID" runat="server" Value="0" />
    <asp:HiddenField ID="hfHeaderTransactionId" runat="server" Value="0" />
    <asp:HiddenField ID="hfCheckNo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTransactionID" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
