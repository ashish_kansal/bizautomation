''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmBalanceSheet
    Inherits BACRMPage

#Region "Variables"
    Dim mobjBalanceSheet As BalanceSheet
    Dim dsBalanceSheet As DataSet = New DataSet
    Dim dtBalanceSheet As DataTable = dsBalanceSheet.Tables.Add("BalanceSheet")
    Dim ldecTotalAmt As Decimal
    Dim mdecTotalAsset As Decimal
    Dim mobjProfitLoss As New ProfitLoss
    Dim ldecNetIncome As Decimal
    Dim ldecTotalAmtForNetIncome As Decimal
    Dim dtChartAcntForBalanceSheetDetails As DataTable
    Dim dtProfitLossDetails As DataTable

#End Region
    Public DifferenceInBalance As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                Dim lobjGeneralLedger As New GeneralLedger
                'To Set Permission
                GetUserRightsForPage(35, 92)

                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) '' DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    End Try
                End If
                bindUserLevelClassTracking()
                LoadGeneralDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()

                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub LoadGeneralDetails()
        Try
            'dtBalanceSheet.Columns.Add("Type", Type.GetType("System.String"))
            'dtBalanceSheet.Columns.Add("Amount", Type.GetType("System.String"))
            'dtBalanceSheet.Columns.Add("numAccountId", Type.GetType("System.String"))
            'dtBalanceSheet.Columns.Add("numAccountTypeId", Type.GetType("System.String"))
            If mobjBalanceSheet Is Nothing Then mobjBalanceSheet = New BalanceSheet
            mobjBalanceSheet.DomainID = Session("DomainID")
            mobjBalanceSheet.FromDate = calFrom.SelectedDate
            mobjBalanceSheet.ToDate = CDate(calTo.SelectedDate & " 23:59:59") ''calTo.SelectedDate
            mobjBalanceSheet.FRID = CCommon.ToLong(GetQueryStringVal("FRID"))
            mobjBalanceSheet.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)

            Dim dtChartAcntForBalanceSheet As DataTable = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheet()

            Dim dblEquity As Double
            Dim dblLiability As Double
            Dim dblAssets As Double
            If dtChartAcntForBalanceSheet.Rows.Count > 0 Then
                If dtChartAcntForBalanceSheet.Select("vcAccountCode='0101'", "").Length > 0 Then dblAssets = dtChartAcntForBalanceSheet.Select("vcAccountCode='0101'", "")(0)("Balance")
                If dtChartAcntForBalanceSheet.Select("vcAccountCode='0102'", "").Length > 0 Then dblLiability = dtChartAcntForBalanceSheet.Select("vcAccountCode='0102'", "")(0)("Balance")
                If dtChartAcntForBalanceSheet.Select("vcAccountCode='0105'", "").Length > 0 Then dblEquity = dtChartAcntForBalanceSheet.Select("vcAccountCode='0105'", "")(0)("Balance")
                If Not dblAssets - dblLiability - dblEquity = 0 Then
                    DifferenceInBalance = "Difference In Opening Balance: " & ReturnMoney(dblAssets - dblLiability - dblEquity).ToString()
                End If
            End If
            'RepBalanceSheet.ItemTemplate = New FinancialReportTemplate(ListItemType.Item)
            'RepBalanceSheet.AlternatingItemTemplate = New FinancialReportTemplate(ListItemType.AlternatingItem)
            'RepBalanceSheet.HeaderTemplate = New FinancialReportTemplate(ListItemType.Header)
            'RepBalanceSheet.FooterTemplate = New FinancialReportTemplate(ListItemType.Footer)
            RepBalanceSheet.DataSource = dtChartAcntForBalanceSheet
            RepBalanceSheet.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub LoadBalanceSheetGrid()
    '    Dim i As Integer
    '    Dim dtChartAcntForBalanceSheet As DataTable
    '    Dim ldecTotalEquity As Decimal
    '    Dim ldecTotalLiability As Decimal
    '    Dim ldecTotalLiabilityAndEquity As Decimal

    '    Try
    '        If mobjBalanceSheet Is Nothing Then mobjBalanceSheet = New BalanceSheet
    '        mobjBalanceSheet.DomainId = Session("DomainID")
    '        mobjBalanceSheet.FromDate = calFrom.SelectedDate
    '        mobjBalanceSheet.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate

    '        For i = 0 To 2
    '            If i = 0 Then
    '                Dim drDummyRowAsset As DataRow = dtBalanceSheet.NewRow
    '                Dim drDummyRowCurrentAsset As DataRow = dtBalanceSheet.NewRow
    '                drDummyRowAsset(0) = "<B>   ASSETS  </b>"
    '                dtBalanceSheet.Rows.Add(drDummyRowAsset)
    '                drDummyRowCurrentAsset(0) = "<B> Current Asset  </b>"
    '                dtBalanceSheet.Rows.Add(drDummyRowCurrentAsset)
    '                mobjBalanceSheet.ByteMode = 1
    '                dtChartAcntForBalanceSheet = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheet()
    '                dtChartAcntForBalanceSheetDetails = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheetDetails()

    '                LoadBalanceSheetDetails(dtChartAcntForBalanceSheet)

    '                Dim drDummyRowTotalAsset As DataRow = dtBalanceSheet.NewRow
    '                drDummyRowTotalAsset(0) = "<B> TOTAL ASSET  </b>"
    '                drDummyRowTotalAsset(1) = "<B>" & ReturnMoney(mdecTotalAsset) & "</B>"
    '                dtBalanceSheet.Rows.Add(drDummyRowTotalAsset)

    '            ElseIf i = 1 Then
    '                mdecTotalAsset = 0
    '                Dim drDummyRowLiabilitiesEquity As DataRow = dtBalanceSheet.NewRow
    '                Dim drDummyRowLiabilities As DataRow = dtBalanceSheet.NewRow
    '                Dim drDummyRowCurrentLiabilities As DataRow = dtBalanceSheet.NewRow

    '                drDummyRowLiabilitiesEquity(0) = "<B>  LIABILITIES AND EQUITY  </b>"
    '                dtBalanceSheet.Rows.Add(drDummyRowLiabilitiesEquity)
    '                drDummyRowLiabilities(0) = "<B>   Liabilities </b>"
    '                dtBalanceSheet.Rows.Add(drDummyRowLiabilities)
    '                drDummyRowCurrentLiabilities(0) = "<B>  Current Liabilities </b>"
    '                dtBalanceSheet.Rows.Add(drDummyRowCurrentLiabilities)
    '                mobjBalanceSheet.ByteMode = 2
    '                dtChartAcntForBalanceSheet = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheet()
    '                dtChartAcntForBalanceSheetDetails = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheetDetails()

    '                LoadBalanceSheetDetails(dtChartAcntForBalanceSheet)

    '                Dim drDummyRowTotalLiabilities As DataRow = dtBalanceSheet.NewRow
    '                drDummyRowTotalLiabilities(0) = "<B> Total Liabilities   <B>"
    '                drDummyRowTotalLiabilities(1) = "<B>" & ReturnMoney(mdecTotalAsset) & "</B>"
    '                ldecTotalLiability = mdecTotalAsset
    '                dtBalanceSheet.Rows.Add(drDummyRowTotalLiabilities)

    '            ElseIf i = 2 Then
    '                mdecTotalAsset = 0
    '                LoadGeneralDetailsForNetIncome()  '' To Calculate Net Income From Profit & Loss Report
    '                Dim drDummyRowEquity As DataRow = dtBalanceSheet.NewRow

    '                drDummyRowEquity(0) = "<B>  Equity  </B>"
    '                dtBalanceSheet.Rows.Add(drDummyRowEquity)

    '                Dim drDummyRowNetIncome As DataRow = dtBalanceSheet.NewRow
    '                drDummyRowNetIncome(0) = "<B>  Net Income </B>"
    '                drDummyRowNetIncome(1) = "<B>" & ReturnMoney(ldecTotalAmtForNetIncome) & "</B>"
    '                dtBalanceSheet.Rows.Add(drDummyRowNetIncome)

    '                mobjBalanceSheet.ByteMode = 3
    '                dtChartAcntForBalanceSheet = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheet()
    '                dtChartAcntForBalanceSheetDetails = mobjBalanceSheet.GetChartAcntDetailsForBalanceSheetDetails()

    '                LoadBalanceSheetDetails(dtChartAcntForBalanceSheet)

    '                Dim drDummyRowTotalEquity As DataRow = dtBalanceSheet.NewRow
    '                drDummyRowTotalEquity(0) = "<B> Total Equity   <B>"
    '                ldecTotalEquity = mdecTotalAsset + ldecTotalAmtForNetIncome
    '                drDummyRowTotalEquity(1) = "<B>" & ReturnMoney(ldecTotalEquity) & "</B>"
    '                dtBalanceSheet.Rows.Add(drDummyRowTotalEquity)

    '                ldecTotalLiabilityAndEquity = ldecTotalLiability + ldecTotalEquity

    '                Dim drDummyRowTotalLiabilitiesEquity As DataRow = dtBalanceSheet.NewRow
    '                drDummyRowTotalLiabilitiesEquity(0) = "<B> TOTAL LIABILITIES AND EQUITY  </b>"
    '                drDummyRowTotalLiabilitiesEquity(1) = "<B>" & ReturnMoney(ldecTotalLiabilityAndEquity) & "</B>"
    '                dtBalanceSheet.Rows.Add(drDummyRowTotalLiabilitiesEquity)
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub LoadBalanceSheetDetails(ByVal dtChartAcntDetails As DataTable)
    '    Dim i As Integer
    '    Dim lintRecordCount As Integer
    '    Try
    '        If mobjBalanceSheet Is Nothing Then mobjBalanceSheet = New BalanceSheet
    '        For i = 0 To dtChartAcntDetails.Rows.Count - 1
    '            If dtChartAcntDetails.Rows(i)("numAcntType") <> 825 Or dtChartAcntDetails.Rows(i)("numAcntType") <> 826 Then ldecTotalAmt = 0

    '            LoadBalanceSheetDet(dtChartAcntDetails.Rows(i)("numAcntType"), dtChartAcntDetails.Rows(i)("vcData"))

    '            Dim drDummyRow As DataRow = dtBalanceSheet.NewRow
    '            lintRecordCount = mobjBalanceSheet.GetCountFromJournalDetails()

    '            If lintRecordCount > 0 And dtChartAcntDetails.Rows(i)("numAcntType") <> 821 And ldecTotalAmt <> 0 Then
    '                drDummyRow(0) = "<B> Total for " & dtChartAcntDetails.Rows(i)("vcData") & "</b>"
    '                drDummyRow(1) = "<B> " & ReturnMoney(ldecTotalAmt) & "</b>"
    '                drDummyRow(3) = dtChartAcntDetails.Rows(i)("numAcntType")
    '                dtBalanceSheet.Rows.Add(drDummyRow)
    '            End If

    '            mdecTotalAsset = mdecTotalAsset + ldecTotalAmt
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub LoadBalanceSheetDet(ByVal p_AccountTypeId As Integer, ByVal p_DataDescription As String)
    '    Dim lintRecordCount As Integer
    '    Try
    '        If mobjBalanceSheet Is Nothing Then mobjBalanceSheet = New BalanceSheet
    '        mobjBalanceSheet.AcntTypeId = p_AccountTypeId
    '        mobjBalanceSheet.DomainId = Session("DomainId")
    '        mobjBalanceSheet.FromDate = calFrom.SelectedDate
    '        mobjBalanceSheet.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate  
    '        lintRecordCount = mobjBalanceSheet.GetCountFromJournalDetails()

    '        Dim lobjChartofAccounts As New ChartOfAccounting
    '        lobjChartofAccounts.DomainId = Session("DomainID")

    '        Dim dvPL As New DataView(dtChartAcntForBalanceSheetDetails)          'create a dataview object
    '        dvPL.RowFilter = "numParntAcntId=" & lobjChartofAccounts.GetRootNode & " And " & "numAcntType =" & p_AccountTypeId

    '        If lintRecordCount > 0 And p_AccountTypeId <> 821 Then
    '            Dim drDummyRowChartAcnt As DataRow = dtBalanceSheet.NewRow
    '            drDummyRowChartAcnt(0) = "<B>" & p_DataDescription & "</b>"
    '            dtBalanceSheet.Rows.Add(drDummyRowChartAcnt)

    '        End If
    '        LoadTreeGrid(dvPL)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub LoadTreeGrid(ByVal dtChartAcntDetails As DataView)
    '    Dim i As Integer
    '    Dim ldecOpeningBalance As Decimal

    '    Try
    '        If mobjBalanceSheet Is Nothing Then mobjBalanceSheet = New BalanceSheet
    '        For i = 0 To dtChartAcntDetails.Count - 1

    '            mobjBalanceSheet.ChartAcntId = dtChartAcntDetails.Item(i)("numAccountId")
    '            mobjBalanceSheet.DomainId = Session("DomainID")
    '            mobjBalanceSheet.FromDate = calFrom.SelectedDate
    '            mobjBalanceSheet.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate

    '            ldecOpeningBalance = mobjBalanceSheet.GetCurrentOpeningBalanceForGeneralLedgerDetails()

    '            If dtChartAcntDetails.Item(i)("Amount") <> 0 Or ldecOpeningBalance <> 0 Then
    '                Dim drBalanceSheetDetails As DataRow = dtBalanceSheet.NewRow
    '                drBalanceSheetDetails(0) = "<font color=Black><b>&nbsp;&nbsp;&nbsp;&nbsp; " & dtChartAcntDetails.Item(i)("AcntTypeDescription") & "</b></font>"
    '                drBalanceSheetDetails(1) = IIf(dtChartAcntDetails.Item(i)("Amount") = 0, "", ReturnMoney(dtChartAcntDetails.Item(i)("Amount")))
    '                drBalanceSheetDetails(2) = dtChartAcntDetails.Item(i)("numAccountId")
    '                dtBalanceSheet.Rows.Add(drBalanceSheetDetails)
    '                ldecTotalAmt = ldecTotalAmt + IIf(drBalanceSheetDetails(1) = "", 0, drBalanceSheetDetails(1))
    '            End If

    '            Dim dvChildnode As New DataView(dtChartAcntForBalanceSheetDetails)          'create a dataview object
    '            dvChildnode.RowFilter = "numParntAcntId=" & dtChartAcntDetails.Item(i)("numAccountId")
    '            LoadTreeGrid(dvChildnode)

    '            If dvChildnode.Count > 0 Then
    '                ''mobjBalanceSheet.ChartAcntId = dtChartAcntDetails.Item(i)("numAccountId")
    '                ''mobjBalanceSheet.DomainId = Session("DomainID")
    '                ''ldecOpeningBalance = mobjBalanceSheet.GetCurrentOpeningBalanceForGeneralLedgerDetails()

    '                If ldecOpeningBalance <> 0 Then
    '                    Dim drDummyRow As DataRow = dtBalanceSheet.NewRow
    '                    drDummyRow(0) = "<B> Total for " & dtChartAcntDetails.Item(i)("AcntTypeDescription") & "</b>"
    '                    drDummyRow(1) = "<B> " & ReturnMoney(ldecOpeningBalance) & "</b>"
    '                    drDummyRow(2) = dtChartAcntDetails.Item(i)("numAccountId")
    '                    dtBalanceSheet.Rows.Add(drDummyRow)
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''Private Function GetChildCategory(ByVal ParentID As String) As DataTable
    ''    Try
    ''        If mobjBalanceSheet Is Nothing Then
    ''            mobjBalanceSheet = New BalanceSheet
    ''        End If
    ''        mobjBalanceSheet.ParentAccountId = ParentID
    ''        mobjBalanceSheet.DomainId = Session("DomainId")
    ''        GetChildCategory = mobjBalanceSheet.GetChildCategory()
    ''    Catch ex As Exception
    ''        Throw ex
    ''    End Try
    ''End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", CDec(Money))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '''To Calculate Net Income From Profit & Loss Report
    'Private Sub LoadGeneralDetailsForNetIncome()
    '    Dim i As Integer
    '    Dim dtChartAcntForProfitLoss As DataTable
    '    Dim ldecTotalIncomeForNI As Decimal
    '    Dim ldecTotalExpensesForNI As Decimal
    '    Dim ldecTotalOtherIncomeForNI As Decimal
    '    Dim ldecTotalOtherExpensesForNI As Decimal
    '    Dim ldecNetOperatingIncomeForNI As Decimal
    '    Dim ldecGrossProfitForNI As Decimal
    '    Dim ldecNetOtherIncomeForNI As Decimal
    '    Dim ldecTotalCosForNI As Decimal
    '    Dim lobjChartofAccounts As New ChartOfAccounting
    '    Try
    '        mobjProfitLoss.DomainId = Session("DomainID")
    '        mobjProfitLoss.FromDate = calFrom.SelectedDate
    '        mobjProfitLoss.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
    '        dtChartAcntForProfitLoss = mobjProfitLoss.GetChartAcntDetails()
    '        dtProfitLossDetails = mobjProfitLoss.GetProfitLoss_Details()

    '        For i = 0 To dtChartAcntForProfitLoss.Rows.Count - 1
    '            If dtChartAcntForProfitLoss.Rows(i)("numAcntType") <> 825 Or dtChartAcntForProfitLoss.Rows(i)("numAcntType") <> 826 Then ldecTotalAmtForNetIncome = 0

    '            lobjChartofAccounts.DomainId = Session("DomainID")
    '            Dim dvPL As New DataView(dtProfitLossDetails)          'create a dataview object
    '            dvPL.RowFilter = "numParntAcntId=" & lobjChartofAccounts.GetRootNode & " And " & "numAcntType =" & dtChartAcntForProfitLoss.Rows(i)("numAcntType")

    '            LoadTreeStructureForProfitLoss(dvPL)

    '            If dtChartAcntForProfitLoss.Rows(i)("numAcntType") = 822 Then ldecTotalIncomeForNI = ldecTotalAmtForNetIncome
    '            If dtChartAcntForProfitLoss.Rows(i)("numAcntType") = 823 Then ldecTotalCosForNI = ldecTotalAmtForNetIncome
    '            If dtChartAcntForProfitLoss.Rows(i)("numAcntType") = 824 Then ldecTotalExpensesForNI = ldecTotalAmtForNetIncome
    '            If dtChartAcntForProfitLoss.Rows(i)("numAcntType") = 825 Then ldecTotalOtherIncomeForNI = ldecTotalAmtForNetIncome
    '            If dtChartAcntForProfitLoss.Rows(i)("numAcntType") = 826 Then ldecTotalOtherExpensesForNI = ldecTotalAmtForNetIncome

    '        Next
    '        ldecGrossProfitForNI = ldecTotalIncomeForNI - ldecTotalCosForNI
    '        ldecNetOperatingIncomeForNI = ldecGrossProfitForNI - ldecTotalExpensesForNI
    '        ldecNetOtherIncomeForNI = ldecTotalOtherIncomeForNI - ldecTotalOtherExpensesForNI
    '        ldecTotalAmtForNetIncome = ldecNetOperatingIncomeForNI + ldecNetOtherIncomeForNI
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub LoadTreeStructureForProfitLoss(ByVal dtChartAcntPLDetails As DataView)
    '    Dim i As Integer
    '    Try
    '        For i = 0 To dtChartAcntPLDetails.Count - 1
    '            If dtChartAcntPLDetails.Item(i)("Amount").ToString() <> "0.00" Then
    '                ldecTotalAmtForNetIncome = ldecTotalAmtForNetIncome + IIf(dtChartAcntPLDetails.Item(i)("Amount") = "", 0, dtChartAcntPLDetails.Item(i)("Amount"))
    '            End If
    '            Dim dvChildnode As New DataView(dtProfitLossDetails)          'create a dataview object
    '            dvChildnode.RowFilter = "numParntAcntId=" & dtChartAcntPLDetails.Item(i)("numAccountId")
    '            LoadTreeStructureForProfitLoss(dvChildnode)
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Private Sub RepBalanceSheet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepBalanceSheet.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '            Dim hplType As HyperLink
    '            Dim lblChartAcntId As Label
    '            Dim lblAcntTypeId As Label
    '            Dim lblAmount As Label
    '            lblChartAcntId = e.Item.FindControl("lblChartAcntId")
    '            lblAcntTypeId = e.Item.FindControl("lblAcntTypeId")
    '            hplType = e.Item.FindControl("hplType")
    '            lblAmount = e.Item.FindControl("lblAmount")
    '            If lblChartAcntId.Text <> "" Then
    '                hplType.Attributes.Add("onclick", "return OpenTransactionDetailsPage('" & "../Accounting/frmTransactions.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=frmBalanceSheet&ChartAcntId=" & lblChartAcntId.Text & "')")
    '                lblAmount.Visible = False
    '            ElseIf lblAcntTypeId.Text <> "" Then
    '                hplType.Attributes.Add("onclick", "return OpenTransactionDetailsPage('" & "../Accounting/frmTransactions.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=frmBalanceSheet&ChartAcntId=0&AcntTypeId=" & lblAcntTypeId.Text & "')")
    '                lblAmount.Visible = False
    '            Else
    '                lblAmount.Visible = True
    '                hplType.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        ExportToExcel.aspTableToExcel(tblExport, Response)
    End Sub
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
End Class

Public Class FinancialReportTemplate
    Implements ITemplate
    Shared itemcount As Integer = 0
    Dim TemplateType As ListItemType
    Sub New(ByVal type As ListItemType)
        TemplateType = type
    End Sub

    Sub InstantiateIn(ByVal container As Control) _
          Implements ITemplate.InstantiateIn
        Dim lc As New Literal()
        Select Case TemplateType
            Case ListItemType.Header
                lc.Text = <a><![CDATA[ <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                        <tr class="normal5">
                                            <td class="td1" align="center">
                                                <b>Account Code</b>
                                            </td>
                                            <td class="td1" align="center">
                                                <b>Account Type</b>
                                            </td>
                                            <td class="td1" align="center">
                                                <b>Class1</b>
                                            </td>
                                            <td class="td1" align="center">
                                                <b>Amount</b>
                                            </td>
                                        </tr>]]></a>.Value
            Case ListItemType.Item
                lc.Text = <a><![CDATA[ <tr class="normal1">
                                        <td>
                                            <%#IIf(Eval("Type") = "2", "<b>" & Eval("AccountCode1").ToString() & "<b>", Eval("AccountCode1"))%>
                                        </td>
                                        <td>
                                            <span style="" onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','<%#Eval("Opening") %>','<%#Eval("vcAccountCode") %>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountName").ToString()) %>','<%# Eval("Type")%>');">
                                                <%#IIf(Eval("Type") = "2", "<b>" & Eval("vcAccountName1").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountName1") & "</span>")%>
                                            </span>
                                        </td>
                                        <td align="right">
                                            <%#IIf(Eval("Type") = "2", "<b>" & ReturnMoney(Eval("Balance")).ToString() & "<b>", ReturnMoney(Eval("Balance")))%>
                                        </td>
                                    </tr>]]></a>.Value
                AddHandler lc.DataBinding, AddressOf TemplateControl_DataBinding

            Case ListItemType.AlternatingItem
                lc.Text = <a><![CDATA[   <tr class="tr1">
                                        <td>
                                            <%#IIf(Eval("Type") = "2", "<b>" & Eval("AccountCode1").ToString() & "<b>", Eval("AccountCode1"))%>
                                        </td>
                                        <td>
                                            <span style="" onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','<%#Eval("Opening") %>','<%#Eval("vcAccountCode") %>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountName").ToString()) %>','<%# Eval("Type")%>');">
                                                <%#IIf(Eval("Type") = "2", "<b>" & Eval("vcAccountName1").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountName1") & "</span>")%>
                                            </span>
                                        </td>
                                        <td align="right">
                                            <%#IIf(Eval("Type") = "2", "<b>" & ReturnMoney(Eval("Balance")).ToString() & "<b>", ReturnMoney(Eval("Balance")))%>
                                        </td>
                                    </tr>]]></a>.Value
                AddHandler lc.DataBinding, AddressOf TemplateControl_DataBinding
            Case ListItemType.Footer
                lc.Text = <a><![CDATA[  <tr>
                                        <td colspan="3">
                                            <tr>
                                                <td colspan="6" class="normal1">
                                                    <hr />
                                                    <%#DifferenceInBalance%>
                                                </td>
                                            </tr>
                                        </td>
                                    </tr>
                                    </table>]]></a>.Value
                'AddHandler lc.DataBinding, AddressOf TemplateControl_DataBinding
        End Select
        container.Controls.Add(lc)
        itemcount += 1
    End Sub
    Private Sub TemplateControl_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lc As Literal
        lc = CType(sender, Literal)
        Dim container As RepeaterItem
        container = CType(lc.NamingContainer, RepeaterItem)
        lc.Text.Replace("", DataBinder.Eval(container.DataItem, "AccountCode1"))
        lc.Text.Replace("", DataBinder.Eval(container.DataItem, "AccountCode1"))
    End Sub
End Class