<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOperationBudgetDetails.aspx.vb"  MasterPageFile="~/common/DetailPage.Master"
    Inherits=".frmOperationBudgetDetails" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Operation Budget</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>

    <title>OPeration Budget</title>

    <script language="javascript" type="text/javascript">
        function LoadTreeViewDetails(obj) {
            //alert('object==='+obj);
            if (document.all) {
                document.getElementById('chartAcnt').innerText = obj;
            } else {
                document.getElementById('chartAcnt').textContent = obj;
            }

            document.form1.btnTreeView.click();
            return false;
        }
        function LoadTextBox() {
            var ddlDivText, ddlDeptText, ddlCCText;
            ddlDivText = '';
            ddlDeptText = '';
            ddlCCText = '';
            if (document.getElementById("ddlDivision").value != 0) {
                ddlDivText = document.getElementById("ddlDivision").options[document.getElementById("ddlDivision").selectedIndex].text;
            }
            if (document.getElementById("ddlDepartment").value != 0) {
                ddlDeptText = '-' + document.getElementById("ddlDepartment").options[document.getElementById("ddlDepartment").selectedIndex].text;
            }
            if (document.getElementById("ddlCostCenter").value != 0) {
                ddlCCText = '-' + document.getElementById("ddlCostCenter").options[document.getElementById("ddlCostCenter").selectedIndex].text;
            }
            document.getElementById("txtBudgetName").value = ddlDivText + ddlDeptText + ddlCCText;
        }

        function Save() {
            if (document.getElementById("txtBudgetName").value == "") {
                alert("Enter Budget Name");
                document.getElementById("txtBudgetName").focus();
                return false;
            }
            if (document.getElementById("ddlDivision").value == 0) {
                alert("Select Division");
                document.getElementById("ddlDivision").focus();
                return false;
            }
            if (document.getElementById("ddlDepartment").value == 0) {
                alert("Select Department");
                document.getElementById("ddlDepartment").focus();
                return false;
            }
            if (document.getElementById("ddlCostCenter").value == 0) {
                alert("Select Cost Center");
                document.getElementById("ddlCostCenter").focus();
                return false;
            }

        }
        function OpenDefineBudget() {
            var chartAcntId = ''
            if (document.getElementById('UltraWebGrid1') != null) {
                for (var i = 0; i < igtbl_getGridById('UltraWebGrid1').Rows.length; i++) {
                    var str = '';
                    grow = igtbl_getGridById('UltraWebGrid1').Rows.getRow(i);
                    str = igtbl_getCellById(grow.Element.cells(1).id).getValue();
                    chartAcntId = chartAcntId + str + ',';
                }
            }
            // alert(chartAcntId);         
            window.open('../Accounting/frmDefineBudget.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&chartAcntId=' + chartAcntId, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=400,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    </asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Operation Budget
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
     <asp:Button ID="btnNewOperationBudget" runat="server" CssClass="button" Text="Create New">
                </asp:Button>
                <asp:Button ID="btnDefine" runat="server" CssClass="button" Text="Define / Edit Budget Accounts">
                </asp:Button>
                <asp:Button ID="btnDuplicateOperationBudget" Visible="false" runat="server" CssClass="button"
                    Text="Create Duplicate"></asp:Button>
                <asp:Button ID="btnDelete" runat="Server" CssClass="button Delete" Text="X" Visible="false" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="table-responsive">
    <asp:Table ID="tblOppr" BorderWidth="1" runat="server" Width="100%" CssClass="aspTableDTL"
        BorderColor="black" GridLines="none" Height="600">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="top">
                <table id="Table1" runat="server" width="100%" border="0">
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblBudget" runat="server" Text="Budget"></asp:Label>
                        </td>
                        <td class="normal1" align="left">
                            <asp:DropDownList ID="ddlBudget" runat="server" Width="330px" AutoPostBack="true"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td align="left" colspan="1">
                            <asp:TextBox ID="txtBudgetName" runat="server" Width="350px" MaxLength="400" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlDivision" runat="server" Width="200px" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlDepartment" runat="server" Width="200px" CssClass="signup"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblFiscal" runat="server" Text="Fiscal Year"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlFiscal" runat="server" Width="200px" CssClass="signup" AutoPostBack="true">
                                <asp:ListItem Text="This Year" Value="0" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="Last Year" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Next Year " Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlCostCenter" runat="server" Width="200px" CssClass="signup"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
               <%-- <igtbl:UltraWebGrid ID="UltraWebGrid1" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                    runat="server" Browser="Xml" Height="" Width="" Visible="false">
                    <DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single"
                        AutoGenerateColumns="true" ViewType="Flat" SelectTypeCellDefault="NotSet" ColFootersVisibleDefault="no"
                        BorderCollapseDefault="Separate" AllowColSizingDefault="Fixed" Name="UltraWebGrid1"
                        EnableClientSideRenumbering="true" TableLayout="Fixed" SelectTypeColDefault="Extended"
                        AllowUpdateDefault="Yes" GridLinesDefault="both">
                        <ClientSideEvents />
                        <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid"
                            HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                            <Padding Left="5px" Right="5px"></Padding>
                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                            </BorderDetails>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BackColor="White">
                        </RowSelectorStyleDefault>
                        <FrameStyle Width="100%" Cursor="Default" BorderWidth="0" Font-Size="8pt" Font-Names="Arial"
                            BorderStyle="Double">
                        </FrameStyle>
                        <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                            </BorderDetails>
                        </FooterStyleDefault>
                        <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                        </EditCellStyleDefault>
                        <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                        </SelectedRowStyleDefault>
                        <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                            BorderStyle="Solid" BackColor="White">
                            <Padding Left="5px" Right="5px"></Padding>
                            <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                        </RowStyleDefault>
                        <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                        </RowExpAreaStyleDefault>
                        <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                        </RowExpAreaStyleDefault>
                    </DisplayLayout>
                    <Bands>
                        <igtbl:UltraGridBand AllowDelete="no" BaseTableName="HierChartOfActForBudget" Key="HierChartOfActForBudget">
                            <Columns>
                            </Columns>
                        </igtbl:UltraGridBand>
                    </Bands>
                </igtbl:UltraWebGrid>--%>
                <table width="100%">
                    <tr>
                        <td class="normal4" align="center">
                            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </asp:TableCell></asp:TableRow>
    </asp:Table>
    <asp:Button ID="btnTreeView" runat="server" Style="display: none"></asp:Button>
    <asp:HiddenField ID="boolDuplicate" runat="server" Value="False" />
    <asp:HiddenField ID="chartAcnt" runat="server" />
        </div>
    </asp:content>

