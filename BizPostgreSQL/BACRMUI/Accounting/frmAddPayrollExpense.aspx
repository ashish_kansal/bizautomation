﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddPayrollExpense.aspx.vb"
    Inherits=".frmAddPayrollExpense" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add Payroll Expense</title>
    <script language="javascript" type="text/javascript">
        function OpenUserDetails(a) {
            window.open("../Accounting/frmEmpPayrollExpenses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1000,height=643,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTimeAndExpenses(a, b, c) {
            //		    alert(b);
            //		    alert(c);
            window.open("../Accounting/frmTimeAndExpensesDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a + "&StartDate=" + b + "&EndDate=" + c, '', 'toolbar=no,titlebar=no,left=200, top=300,width=700,height=450,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenCommissionDetails(a, b, c) {
            var myindex = document.getElementById('ddlDate').selectedIndex;
            var SelValue = document.getElementById('ddlDate').options[myindex].value;
            window.open("../Accounting/frmCommissionforPayrollExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=" + a + "&PersonalId=" + b + "&PayPeriod=" + SelValue + "&Mode=" + c, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1100,height=425,scrollbars=yes,resizable=yes')
            return false;
        }
        function LoadPayrollDetails() {
            document.form1.btnLoad.click()
            return false;
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function validate(TB) {
            var hdnTotalAmountDue = TB.replace("txtDeductions", "hdnTotalAmountDue");

            if (parseFloat(document.getElementById(TB).value) > parseFloat(document.getElementById(hdnTotalAmountDue).value)) {
                document.getElementById(TB).value = "0";
                alert('Deductions is not be greater than Total Amount.');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table border="0" cellpadding="0" cellspacing="0" width="" align="center">
        <tr>
            <td class="normal4" align="center" colspan="3">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <%--<table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Label ID="lblCurrent" runat="server">Current Pay Period : </asp:Label>
            </td>
            <td class="normal1" align="right">
                <asp:DropDownList ID="ddlYear" runat="server" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlMonth" runat="server" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlDate" runat="server" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right">
                &nbsp;
                <asp:Label ID="lblDepartment" runat="server">Department</asp:Label>&nbsp;
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
    </table>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Add Payroll Expense
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <style>
        .step
        {
            font-size: large;
            font-family: Arial;
            margin-right: 10px;
        }
        
        .GraySmallText
        {
            font-size: xx-small;
            font-weight: bold;
            color: gray;
        }
    </style>
    <table>
        <tbody>
            <tr>
                <td>
                    <br />
                    <table width="" cellpadding="0" cellspacing="2">
                        <tbody>
                            <tr>
                                <td align="right" class="step">
                                    Step 1:
                                </td>
                                <td align="right" width="150px">
                                    Select pay period<font color="#ff0000">*</font>
                                </td>
                                <td>
                                    <select name="endDate0" onchange="submitTo(this.form, this.form.payDate0, 1)">
                                        <option value="05/02/2012 - 06/01/2012">05/02/2012 - 06/01/2012 </option>
                                        <option value="06/02/2012 - 07/01/2012">06/02/2012 - 07/01/2012 </option>
                                        <option value="07/02/2012 - 08/01/2012">07/02/2012 - 08/01/2012 </option>
                                        <option value="08/02/2012 - 09/01/2012">08/02/2012 - 09/01/2012 </option>
                                        <option value="09/02/2012 - 10/01/2012">09/02/2012 - 10/01/2012 </option>
                                        <option selected="" value="10/02/2012 - 11/01/2012">10/02/2012 - 11/01/2012 </option>
                                        <option value="11/02/2012 - 12/01/2012">11/02/2012 - 12/01/2012 </option>
                                        <option value="12/02/2012 - 01/01/2013">12/02/2012 - 01/01/2013 </option>
                                        <option value="01/02/2013 - 02/01/2013">01/02/2013 - 02/01/2013 </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="step">
                                    Step 2:
                                </td>
                                <td class="alt" align="right" width="150px">
                                    Enter Pay Date<font color="#ff0000">*</font>
                                </td>
                                <td>
                                    <BizCalendar:Calendar ID="calPayDate" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="step">
                                    Step 3:
                                </td>
                                <td colspan="" class="alt" align="right" width="150px">
                                    Entry # <font color="#ff0000">*</font>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server" Text="007" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="step">
                                    Step 4:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="alt" align="left" width="150px">
                                    Choose Employess from following list
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <table id="table3" cellspacing="2" cellpadding="2" border="0" style="background-color: White;
                        width: 100%; border-collapse: collapse;">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <table cellspacing="0" rules="all" border="1" id="dgPayrollLiability" style="width: 100%;
                                        border-collapse: collapse;">
                                        <tbody>
                                            <tr class="hs">
                                                <td valign="top" style="background-color: rgb(214, 215, 216);">
                                                    <input id="chkAll" type="checkbox" name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$chkAll"
                                                        onclick="SelectAll('chkAll', 'chkSelect');">
                                                </td>
                                                <td valign="top" style="background-color: rgb(214, 215, 216);">
                                                    <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl00','')">
                                                        Name</a>
                                                </td>
                                                <td valign="top" style="background-color: rgb(214, 215, 216); width: 160px;">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center" style="border-bottom: 1px solid;">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl03','')">
                                                                    Regular Hrs</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%; border-right: 1px solid;">
                                                                Estimated
                                                            </td>
                                                            <td style="width: 25%; border-right: 1px solid;">
                                                                Actual
                                                            </td>
                                                            <td style="width: 50%;">
                                                                Total ($)
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" style="background-color: rgb(214, 215, 216); width: 160px;">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center" style="border-bottom: 1px solid;">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl04','')">
                                                                    Overtime Hrs</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%; border-right: 1px solid;">
                                                                Estimated
                                                            </td>
                                                            <td style="width: 25%; border-right: 1px solid;">
                                                                Actual
                                                            </td>
                                                            <td style="width: 50%">
                                                                Total ($)
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" style="background-color: rgb(214, 215, 216); width: 160px;">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center" style="border-bottom: 1px solid;">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl05','')">
                                                                    Paid Leave Hrs</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%; border-right: 1px solid;">
                                                                Estimated
                                                            </td>
                                                            <td style="width: 25%; border-right: 1px solid;">
                                                                Actual
                                                            </td>
                                                            <td style="width: 50%">
                                                                Total ($)
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" style="background-color: rgb(214, 215, 216); width: 100px;">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center" style="border-bottom: 1px solid;">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl06','')">
                                                                    Total Hrs</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%; border-right: 1px solid;">
                                                                Estimated
                                                            </td>
                                                            <td style="width: 50%">
                                                                Actual
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 10%; background-color: rgb(214, 215, 216); width: 255px;" valign="top">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="border-bottom: 1px solid;" align="center" colspan="3">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl07','')">
                                                                    Commission Amt</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: ; color: Red; border-right: 1px solid;">
                                                                <asp:CheckBox ID="chk1" runat="server" Text="Paid Invoice" />
                                                            </td>
                                                            <td style="width: ; color: blue; border-right: 1px solid;">
                                                                <asp:CheckBox ID="CheckBox14" runat="server" Text="UnPaid Invoice" />
                                                            </td>
                                                            <td style="">
                                                                <asp:CheckBox ID="CheckBox15" runat="server" Text="Both" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 6%; background-color: rgb(214, 215, 216);" valign="top">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center" style="border-bottom: 1px solid;">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl08','')">
                                                                    Total Expenses</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%; border-right: 1px solid;">
                                                                Estimated
                                                            </td>
                                                            <td style="width: 50%;">
                                                                Actual
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 6%; background-color: rgb(214, 215, 216); width: 160px;" valign="top">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center" style="border-bottom: 1px solid;">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl09','')">
                                                                    Reimbursable Expenses</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%; border-right: 1px solid;">
                                                                Estimated
                                                            </td>
                                                            <td style="width: 50%;">
                                                                Actual
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 15px; background-color: rgb(214, 215, 216);" valign="top">
                                                    Deductions
                                                </td>
                                                <td valign="top" style="background-color: rgb(214, 215, 216);">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td colspan="3" align="center">
                                                                <a href="javascript:__doPostBack('ctl00$GridPlaceHolder$dgPayrollLiability$ctl01$ctl10','')">
                                                                    Total Amt Due</a>
                                                            </td>
                                                        </tr>
                                                        <%-- <tr>
                                                            <td style="width: 50%">Estimated
                                                            </td>
                                                            <td style="width: 50%">Actual
                                                            </td>
                                                        </tr>--%>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="chkSelect">
                                                        <input id="Checkbox1" type="checkbox" name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$chkSelect"></span>
                                                </td>
                                                <td>
                                                    <a id="hplUsername" onclick="return OpenUserDetails('1')" href="#" target="_blank">Carl
                                                        Zaldivar</a>
                                                    <br />
                                                    <span class="GraySmallText">Hourly Rate: $ 10</span>
                                                </td>
                                                <td>
                                                    <table border="0">
                                                        <tr>
                                                            <td style="width: 59px">
                                                                <a id="A1" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text13" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 100
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A76" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text14" type="text" style="width: 40px; text-align: right" value="2">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 20
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A77" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text15" type="text" style="width: 40px; text-align: right" value="1">
                                                            </td>
                                                            <td style="width: 56px%; text-align: right">
                                                                $ 10
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A78" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">14</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                13
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 89px; text-align: right">
                                                                <a id="hplBizDocNonPaidCommAmt" onclick="return OpenCommissionDetails('20','84',1)"
                                                                    href="#" target="_blank" style="color: Red;">5.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 105px; text-align: right">
                                                                <a id="A61" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">100.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px;">
                                                                <input id="Text97" type="text" style="width: 40px; text-align: right" value="5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A79" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px; text-align: right">
                                                                <input id="Text1" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 76px; text-align: right">
                                                                <a id="A80" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">30.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 76px; text-align: right">
                                                                <input id="Text52" type="text" style="width: 40px; text-align: right" value="30">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <input name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$txtDeductions" type="text"
                                                        value="10" id="txtDeductions" class="signup" autocomplete="OFF" style="width: 40px;
                                                        text-align: right">
                                                </td>
                                                <td style="width: 100px; text-align: right">
                                                    <table border="0" width="60px">
                                                        <tr>
                                                            <td style="text-align: right">
                                                                $ 145
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="chkSelect">
                                                        <input id="Checkbox2" type="checkbox" name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$chkSelect"></span>
                                                </td>
                                                <td>
                                                    <a id="A2" onclick="return OpenUserDetails('1')" href="#" target="_blank">Carl Zaldivar</a>
                                                    <br />
                                                    <span class="GraySmallText">Hourly Rate: $ 10</span>
                                                </td>
                                                <td>
                                                    <table border="0">
                                                        <tr>
                                                            <td style="width: 59px">
                                                                <a id="A3" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text2" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 100
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A4" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text3" type="text" style="width: 40px; text-align: right" value="2">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 20
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A5" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text4" type="text" style="width: 40px; text-align: right" value="1">
                                                            </td>
                                                            <td style="width: 56px%; text-align: right">
                                                                $ 10
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A6" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">14</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                13
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 89px; text-align: right">
                                                                <a id="A7" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Red;">5.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 105px; text-align: right">
                                                                <a id="A8" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">100.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px;">
                                                                <input id="Text5" type="text" style="width: 40px; text-align: right" value="5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A9" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px; text-align: right">
                                                                <input id="Text6" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 76px; text-align: right">
                                                                <a id="A10" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">30.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 76px; text-align: right">
                                                                <input id="Text7" type="text" style="width: 40px; text-align: right" value="30">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <input name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$txtDeductions" type="text"
                                                        value="10" id="Text8" class="signup" autocomplete="OFF" style="width: 40px; text-align: right">
                                                </td>
                                                <td style="width: 100px; text-align: right">
                                                    <table border="0" width="60px">
                                                        <tr>
                                                            <td style="text-align: right">
                                                                $ 145
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="chkSelect">
                                                        <input id="Checkbox3" type="checkbox" name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$chkSelect"></span>
                                                </td>
                                                <td>
                                                    <a id="A11" onclick="return OpenUserDetails('1')" href="#" target="_blank">Carl Zaldivar</a>
                                                    <br />
                                                    <span class="GraySmallText">Hourly Rate: $ 10</span>
                                                </td>
                                                <td>
                                                    <table border="0">
                                                        <tr>
                                                            <td style="width: 59px">
                                                                <a id="A12" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text9" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 100
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A13" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text10" type="text" style="width: 40px; text-align: right" value="2">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 20
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A14" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text11" type="text" style="width: 40px; text-align: right" value="1">
                                                            </td>
                                                            <td style="width: 56px%; text-align: right">
                                                                $ 10
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A15" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">14</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                13
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 89px; text-align: right">
                                                                <a id="A16" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Red;">5.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 105px; text-align: right">
                                                                <a id="A17" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">100.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px;">
                                                                <input id="Text12" type="text" style="width: 40px; text-align: right" value="5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A18" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px; text-align: right">
                                                                <input id="Text16" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 76px; text-align: right">
                                                                <a id="A19" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">30.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 76px; text-align: right">
                                                                <input id="Text17" type="text" style="width: 40px; text-align: right" value="30">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <input name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$txtDeductions" type="text"
                                                        value="10" id="Text18" class="signup" autocomplete="OFF" style="width: 40px;
                                                        text-align: right">
                                                </td>
                                                <td style="width: 100px; text-align: right">
                                                    <table border="0" width="60px">
                                                        <tr>
                                                            <td style="text-align: right">
                                                                $ 145
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="chkSelect">
                                                        <input id="Checkbox4" type="checkbox" name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$chkSelect"></span>
                                                </td>
                                                <td>
                                                    <a id="A20" onclick="return OpenUserDetails('1')" href="#" target="_blank">Carl Zaldivar</a>
                                                    <br />
                                                    <span class="GraySmallText">Hourly Rate: $ 10</span>
                                                </td>
                                                <td>
                                                    <table border="0">
                                                        <tr>
                                                            <td style="width: 59px">
                                                                <a id="A21" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text19" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 100
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A22" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text20" type="text" style="width: 40px; text-align: right" value="2">
                                                            </td>
                                                            <td style="width: 56px; text-align: right">
                                                                $ 20
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A23" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">2.00</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                <input id="Text21" type="text" style="width: 40px; text-align: right" value="1">
                                                            </td>
                                                            <td style="width: 56px%; text-align: right">
                                                                $ 10
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A24" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">14</a>
                                                            </td>
                                                            <td style="width: 37px; text-align: right">
                                                                13
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 89px; text-align: right">
                                                                <a id="A25" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Red;">5.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 105px; text-align: right">
                                                                <a id="A26" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">100.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px;">
                                                                <input id="Text22" type="text" style="width: 40px; text-align: right" value="5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 59px; text-align: right">
                                                                <a id="A27" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">10.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 47px; text-align: right">
                                                                <input id="Text23" type="text" style="width: 40px; text-align: right" value="10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <table border="0" width="">
                                                        <tr>
                                                            <td style="width: 76px; text-align: right">
                                                                <a id="A28" onclick="return OpenCommissionDetails('20','84',1)" href="#" target="_blank"
                                                                    style="color: Blue;">30.00</a>&nbsp;
                                                            </td>
                                                            <td style="width: 76px; text-align: right">
                                                                <input id="Text24" type="text" style="width: 40px; text-align: right" value="30">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right">
                                                    <input name="ctl00$GridPlaceHolder$dgPayrollLiability$ctl02$txtDeductions" type="text"
                                                        value="10" id="Text25" class="signup" autocomplete="OFF" style="width: 40px;
                                                        text-align: right">
                                                </td>
                                                <td style="width: 100px; text-align: right">
                                                    <table border="0" width="60px">
                                                        <tr>
                                                            <td style="text-align: right">
                                                                $ 145
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="submit" name="ctl00$GridPlaceHolder$btnLoad" value="" id="Submit1" style="display: none">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <br />
                    <input type="submit" id="btnSave" class="button" value="Save" id="Submit2">
                    <input type="submit" id="btnClose" value="Close" id="Submit2" class="button">
                </td>
            </tr>
        </tbody>
    </table>
    <asp:Button ID="btnLoad" runat="server" Style="display: none"></asp:Button>
</asp:Content>
