﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI

Public Class frmCommissionEarnedDetails
    Inherits BACRMPage

#Region "Member Variables"

    Dim monTotalCommissionEarned As Double
    Dim monTotalCommissionPaid As Double
    Dim monTotalCommissionDue As Double

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Dim m_aryRightsForUpdate() As Integer = GetUserRightsForPage_Other(35, 87)

                If m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) <> 3 Then
                    btnUpdateAmtPaid.Visible = False
                End If

                dgBizDocs.MasterTableView.HierarchyLoadMode = GridChildLoadMode.Client
                LoadCommissionListGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub LoadCommissionListGrid()
        Try
            Dim lobjPayrollExpenses As New PayrollExpenses
            lobjPayrollExpenses.DomainID = Session("DomainId")
            lobjPayrollExpenses.UserCntID = CCommon.ToLong(GetQueryStringVal("UserCntID"))
            lobjPayrollExpenses.ComPayPeriodID = CCommon.ToLong(GetQueryStringVal("ComPayPeriodID"))
            lobjPayrollExpenses.DivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
            lobjPayrollExpenses.Mode = 1
            lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            dgBizDocs.DataSource = lobjPayrollExpenses.GetCommissionEarnedDetail()
            dgBizDocs.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Private Sub dgBizDocs_DetailTableDataBind(sender As Object, e As GridDetailTableDataBindEventArgs) Handles dgBizDocs.DetailTableDataBind
        Try
            Dim parentItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)

            Dim lobjPayrollExpenses As New PayrollExpenses
            lobjPayrollExpenses.DomainID = Session("DomainId")
            lobjPayrollExpenses.UserCntID = CCommon.ToLong(GetQueryStringVal("UserCntID"))
            lobjPayrollExpenses.ComPayPeriodID = CCommon.ToLong(GetQueryStringVal("ComPayPeriodID"))
            lobjPayrollExpenses.DivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
            lobjPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            lobjPayrollExpenses.Mode = 2
            lobjPayrollExpenses.OppID = CCommon.ToLong(parentItem.GetDataKeyValue("numOppID").ToString())
            lobjPayrollExpenses.OppBizDocsId = CCommon.ToLong(parentItem.GetDataKeyValue("numOppBizDocsId").ToString())

            e.DetailTableView.DataSource = lobjPayrollExpenses.GetCommissionEarnedDetail()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgBizDocs_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles dgBizDocs.ItemDataBound
        Try
            If (TypeOf e.Item Is GridDataItem Or TypeOf e.Item Is GridFooterItem) AndAlso e.Item.OwnerTableView.Name = "ParentGrid" Then
                If TypeOf e.Item Is GridDataItem Then
                    Dim row As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                    monTotalCommissionEarned = monTotalCommissionEarned + CCommon.ToDouble(row("monTotalCommission"))
                    monTotalCommissionPaid = monTotalCommissionPaid + CCommon.ToDouble(row("monCommissionPaid"))
                    monTotalCommissionDue = monTotalCommissionDue + CCommon.ToDouble(row("monCommissionToPay"))
                ElseIf TypeOf e.Item Is GridFooterItem Then
                    Dim footer As GridFooterItem = DirectCast((e.Item), GridFooterItem)
                    DirectCast((footer.FindControl("lblTotalCommissionEarned")), Label).Text = String.Format("{0:##,#00.00###}", monTotalCommissionEarned)
                    DirectCast((footer.FindControl("lblTotalCommissionPaid")), Label).Text = String.Format("{0:##,#00.00###}", monTotalCommissionPaid)
                    DirectCast((footer.FindControl("lblTotalCommissionDue")), Label).Text = String.Format("{0:##,#00.00###}", monTotalCommissionDue)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnUpdateAmtPaid_Click(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(GetQueryStringVal("ComPayPeriodID")) > 0 Then
                Dim isErrorOccured As Boolean = False
                Dim errorMessage As String = ""
                Dim ds As New DataSet()
                Dim dt As New DataTable("Employee")
                Dim dr As DataRow
                dt.Columns.Add("numUserCntID", GetType(System.Int64))
                dt.Columns.Add("numDivisionID", GetType(System.Int64))
                dt.Columns.Add("numOppID", GetType(System.Int64))
                dt.Columns.Add("numOppBizDocID", GetType(System.Int64))
                dt.Columns.Add("monCommissionPaid", GetType(System.Double))


                For Each item As GridDataItem In dgBizDocs.Items
                    If item.OwnerTableView.Name = "ParentGrid" AndAlso (item.ItemType = GridItemType.Item Or item.ItemType = GridItemType.AlternatingItem) Then
                        If CCommon.ToDouble(DirectCast(item.FindControl("txtComissionDue"), TextBox).Text) > CCommon.ToDouble(DirectCast(item.FindControl("hdnCommissionDue"), HiddenField).Value) Then
                            isErrorOccured = True
                            errorMessage = errorMessage & "Commission due entered (" & CCommon.ToDouble(DirectCast(item.FindControl("txtComissionDue"), TextBox).Text) & ") is greater than amount needs to be paid (" & CCommon.ToDouble(DirectCast(item.FindControl("hdnCommissionDue"), HiddenField).Value) & ") for order " & CCommon.ToString(item.GetDataKeyValue("Name")) & ".<br />"
                        ElseIf CCommon.ToDouble(DirectCast(item.FindControl("txtComissionDue"), TextBox).Text) >= 0 Then
                            dr = dt.NewRow()
                            dr("numUserCntID") = CCommon.ToLong(GetQueryStringVal("UserCntID"))
                            dr("numDivisionID") = CCommon.ToLong(GetQueryStringVal("DivisionID"))
                            dr("numOppID") = CCommon.ToLong(item.GetDataKeyValue("numOppID"))
                            dr("numOppBizDocID") = CCommon.ToLong(item.GetDataKeyValue("numOppBizDocsId"))
                            dr("monCommissionPaid") = CCommon.ToDouble(DirectCast(item.FindControl("txtComissionDue"), TextBox).Text) + CCommon.ToDouble(DirectCast(item.FindControl("hdnCommissionPaid"), HiddenField).Value)
                            dt.Rows.Add(dr)
                        End If
                    End If
                Next

                If isErrorOccured Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidCommissionPayment", "alert('" & errorMessage & "');", True)
                ElseIf dt.Rows.Count > 0 Then
                    ds.Tables.Add(dt)

                    Dim objPayrollExpenses As New PayrollExpenses
                    objPayrollExpenses.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPayrollExpenses.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objPayrollExpenses.ComPayPeriodID = CCommon.ToLong(GetQueryStringVal("ComPayPeriodID"))
                    objPayrollExpenses.Mode = 3 'Pay commission earned
                    objPayrollExpenses.strItems = ds.GetXml()
                    objPayrollExpenses.UpdatePayrollAmountPaid()

                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CloseWindowAndRefereshParent", "CloseWindowAndRefereshParent();", True)
                End If
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidPayPeriod", "alert('Commission is not calculated for selected pay period. Please click calculate commission button.');", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region


    
    
End Class