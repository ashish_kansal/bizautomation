<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmpPayrollExpenses.aspx.vb"
    Inherits=".frmEmpPayrollExpense" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Employee Payroll Expenses</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
            return false;
        }
        function OpenRoles(a) {
            window.open('../TimeAndExpense/frmAddRoles.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&NameU=' + a, '', 'toolbar=no,titlebar=no,left=300,top=400,width=500,height=400,scrollbars=yes,resizable=yes');
            return false;
        }
        $(document).ready(function () {
            InitializeValidation();
            $('#litMessage').fadeIn().delay(5000).fadeOut();
        });

        function Save() {
            //		 alert('SP');
            //		 alert(document.form1.CalHired_txtDate.value);
            if (document.form1.CalHired_txtDate.value == "") {
                alert("Select Hired Date");
                return false;
            }
            if (document.form1.CalReleased_txtDate.value == "") {
                alert("Select Released Date");
                return false;
            }

        }
//        function CheckNumber(cint, e) {
//            var k;
//            document.all ? k = e.keyCode : k = e.which;
//            if (cint == 1) {
//                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
//                    if (e.preventDefault) {
//                        e.preventDefault();
//                    }
//                    else
//                        e.returnValue = false;
//                    return false;
//                }
//            }
//            if (cint == 2) {
//                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
//                    if (e.preventDefault) {
//                        e.preventDefault();
//                    }
//                    else
//                        e.returnValue = false;
//                    return false;
//                }
//            }
//        }
        function Calculate() {
            if (document.all) {
                document.getElementById("txtNetPay").innerText = formatCurrency(document.form1.txtNetPay.value);
                document.getElementById("txtAmtWithheld").innerText = formatCurrency(document.form1.txtAmtWithheld.value);
                document.getElementById("lblTotalPay").innerText = formatCurrency(parseFloat(document.form1.txtNetPay.value.replace(/,/g, "")) + parseFloat(document.form1.txtAmtWithheld.value.replace(/,/g, "")));
            } else {
                document.getElementById("txtNetPay").textContent = formatCurrency(document.form1.txtNetPay.value);
                document.getElementById("txtAmtWithheld").textContent = formatCurrency(document.form1.txtAmtWithheld.value);
                document.getElementById("lblTotalPay").textContent = formatCurrency(parseFloat(document.form1.txtNetPay.value.replace(/,/g, "")) + parseFloat(document.form1.txtAmtWithheld.value.replace(/,/g, "")));
            }
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
        num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
        function CloseAndReploadParent() {
            window.opener.LoadPayrollDetails();
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="right">
                <asp:Button ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text="Save &amp; Close">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Close">
                </asp:Button>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Employee Payroll Expense
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
                    Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <br />
                            <table id="tblDetails" runat="server" width="100%" border="0">
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Employee Name
                                    </td>
                                    <td class="normal1" style="width: 30%">
                                        <asp:Label ID="lblEmpName" runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                    <td class="normal1" align="right" style="width: 15%" nowrap>
                                        Home Phone
                                    </td>
                                    <td class="normal1" style="width: 15%">
                                        <asp:Label ID="lblHomePhone" runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                    <td class="normal1" align="right" nowrap style="width: 7%">
                                        Cell Phone
                                    </td>
                                    <td class="normal1" style="width: 20%">
                                        <asp:Label ID="lblCellPhone" runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Department
                                    </td>
                                    <td id="tdsales2" runat="server" visible="true">
                                        <asp:Label ID="lblDepartment" runat="server" ReadOnly="true" CssClass="normal1"></asp:Label>
                                    </td>
                                    <td class="normal1" align="right" id="td1" runat="server" visible="true" nowrap>
                                        Email Address
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEmailAddress" runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                    <td class="normal1" align="right" id="td2" runat="server" visible="true" nowrap>
                                        Other Phone
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOtherPhone" runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Home Address
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblHomeAddress" runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Emergency Contact Information
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtEmergencyConInfo" runat="server" Width="350" CssClass="signup"></asp:TextBox>
                                    </td>
                                    <td class="normal1" align="right">
                                        <asp:CheckBox ID="chkPayroll" runat="server" Text="Payroll" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Notes
                                    </td>
                                    <td class="normal1" colspan="5">
                                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Width="500px" CssClass="signup"
                                            ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Employee ID
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtEmployeeId" runat="server" Width="150px" CssClass="required_integer {required:false, number:true, messages:{number:'provide valid value!'}}"></asp:TextBox>
                                    </td>
                                    <td class="normal1" align="right" nowrap>
                                        Date Hired
                                    </td>
                                    <td class="normal1">
                                        <BizCalendar:Calendar ID="CalHired" runat="server" ClientIDMode="AutoID" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Hourly Rate
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtHourlyRate" runat="server" Width="75px" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"></asp:TextBox>
                                    </td>
                                    <td class="normal1" align="right">
                                        Date Released
                                    </td>
                                    <td class="normal1">
                                        <BizCalendar:Calendar ID="CalReleased" runat="server" ClientIDMode="AutoID" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Employee Rating
                                    </td>
                                    <td>
                                        <table id="tblEmp" runat="server">
                                            <tr>
                                                <td class="normal1" nowrap>
                                                    <asp:TextBox ID="txtEmployeeRating" runat="server" Width="30px" CssClass="signup"></asp:TextBox>
                                                    <asp:Label ID="lblPercentage" runat="server" Text="% as of"></asp:Label>
                                                </td>
                                                <td>
                                                    <BizCalendar:Calendar ID="calEmpRating" runat="server" ClientIDMode="AutoID" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" nowrap>
                                        Total pay this fiscal year
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblTotalPay" runat="server" Width="75px" CssClass="signup" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td class="normal1" align="right" nowrap>
                                        Net Pay
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtNetPay" runat="server" Width="75px" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                            onchange="Calculate();"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td class="normal1" align="right" nowrap>
                                        Amounts Withheld
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtAmtWithheld" runat="server" Width="75px" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                            onchange="Calculate();"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="normal1" colspan="5">
                                        <asp:CheckBox ID="chkSalary" runat="server" Text="<b>Salary</b>" />
                                        - Automatically add &nbsp;<asp:TextBox ID="txtDailyHours" runat="server" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                            Width="50"></asp:TextBox>
                                        hours for each business day (M-F) &nbsp;
                                        <asp:DropDownList ID="ddltime" runat="server" Width="55" CssClass="signup">
                                            <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                            <asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                            <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;AM<input id="chkAM" type="radio" checked value="0" name="AM" runat="server" />PM<input
                                            id="chkPM" type="radio" value="1" name="AM" runat="server" />
                                        then multiply by the hourly rate.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="normal1" colspan="5">
                                        <asp:CheckBox ID="chkPaidLeave" runat="server" Text="<b>Paid Leave Accrual </b>" />
                                        � Add 1 hour of paid leave for every &nbsp;<asp:TextBox ID="txtPaidLeaveHrs" runat="server"
                                            CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                            Width="50"></asp:TextBox>
                                        hours worked . Current paid leave balance (hours): &nbsp;<asp:Label ID="lblPaidLeaveBalance"
                                            runat="server" CssClass="signup"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="normal1" colspan="5">
                                        <asp:CheckBox ID="chkOverTime" runat="server" Text="<b>Over Time</b>" />- After
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="normal1" colspan="5">
                                        <table>
                                            <tr>
                                                <td>
                                                </td>
                                                <td class="normal1">
                                                    <asp:RadioButton ID="rdbOverTimeDaily" runat="server" Checked="true" GroupName="OverTime" />
                                                    &nbsp;<asp:TextBox ID="txtLimDailHrs" runat="server" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                                        Width="50"></asp:TextBox>
                                                    hours are entered during a pay-period, start calculating hourly rate of pay at
                                                    <asp:TextBox ID="txtOverTimeRate" runat="server" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                                        Width="50"></asp:TextBox>
                                                    for the remainder of that pay-period.<br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(This will be shown as "Overtime" in the Gross
                                                    Profit / Loss report.)
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="normal1" colspan="5" visible="false">
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdbOverTimeWeekly" runat="server" GroupName="OverTime" /><asp:TextBox
                                                        ID="txtLimWeeklyHrs" runat="server" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                                        Width="50"></asp:TextBox>
                                                    Hours entered in a week, start calculating rate at<asp:TextBox ID="txtOverTimeWeeklyRate"
                                                        runat="server" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}"
                                                        Width="50"></asp:TextBox>
                                                    for the remainder of that week.<br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(This will be shown as "Overtime" in the Gross
                                                    Profit / Loss report.)
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td>
                                    </td>
                                    <td class="normal1" colspan="5">
                                        <asp:CheckBox ID="chkEmpCOGS" runat="server" Text="<b>   Add this employee�s net income to the COGS account. </b>" />
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
