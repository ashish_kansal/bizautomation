﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmBankReconcileList.aspx.vb" Inherits=".frmBankReconcileList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Bank Reconcilation</title>
    <script type="text/javascript">
        function ValidateAccount() {
            if (document.getElementById('ddlAccount').value == 0) {
                alert("Please Select Account Type");
                document.getElementById('ddlAccount').focus();
                return false;
            }
        }

        function OpenReconReport(a) {
            window.location.href = "../Accounting/frmBankReconcileStatement.aspx?ReconcileID=" + a
            return false;
        }

        function EditRecon(a) {
            window.location.href = "../Accounting/frmBankReconciliation.aspx?ReconcileID=" + a
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Bank Reconcilation&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmbankreconcilelist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <label>Account</label>
                &nbsp;
                <asp:DropDownList CssClass="signup form-control" ID="ddlAccount" runat="server" AutoPostBack="true">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnReconcileNow" runat="server" CssClass="button btn btn-primary" Text="Reconcile Now"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvReconcile" runat="server" BorderWidth="0" CssClass="table table-striped table-bordered" AutoGenerateColumns="False"
                    Style="margin-right: 0px" Width="100%" DataKeyNames="numReconcileID" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Statement Date">
                            <ItemTemplate>
                                 <a href="#" onclick='javascript:EditRecon(<%#eval("numReconcileID")%>)'>
                                    <%#Eval("dtStatementDate", "{0:MM/dd/yyyy}")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Reconciled On" DataField="dtReconcileDate" ItemStyle-HorizontalAlign="Left"
                            HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
                        <asp:BoundField HeaderText="Begining Balance" DataField="monBeginBalance" ItemStyle-HorizontalAlign="Right"
                            HtmlEncode="false" DataFormatString="{0:##,#00.00}"></asp:BoundField>
                        <asp:BoundField HeaderText="Ending Balance" DataField="monEndBalance" ItemStyle-HorizontalAlign="Right"
                            HtmlEncode="false" DataFormatString="{0:##,#00.00}"></asp:BoundField>
                        <asp:BoundField HeaderText="Service Charge" DataField="monServiceChargeAmount" ItemStyle-HorizontalAlign="Right"
                            HtmlEncode="false" DataFormatString="{0:##,#00.00}"></asp:BoundField>
                        <asp:BoundField HeaderText="Interest Earned" DataField="monInterestEarnedAmount"
                            ItemStyle-HorizontalAlign="Right" HtmlEncode="false" DataFormatString="{0:##,#00.00}"></asp:BoundField>
                        <asp:BoundField HeaderText="Deposit" DataField="monDeposit" ItemStyle-HorizontalAlign="Right"
                            HtmlEncode="false" DataFormatString="{0:##,#00.00}"></asp:BoundField>
                        <asp:BoundField HeaderText="Payments" DataField="monPayment" ItemStyle-HorizontalAlign="Right"
                            HtmlEncode="false" DataFormatString="{0:##,#00.00}"></asp:BoundField>
                        <asp:TemplateField HeaderText="Difference" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblDifference" runat="server" Text="0.00"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkbDelete" CommandName="Delete" CommandArgument='<%#Eval("numReconcileID")%>' CssClass="btn btn-danger btn-xs" runat="server" Visible="false"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                <asp:HyperLink ID="hplReport" NavigateUrl="#" onclick='<%# String.Format("javascript:OpenReconReport({0})", Eval("numReconcileID"))%>' runat="server">Report</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No records found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
