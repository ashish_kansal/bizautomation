﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmFinancialYearClose
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                BindFinancialYears()
            End If
            btnSave.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub BindFinancialYears()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .DomainId = Session("DomainID")
                .Mode = 1
                ds = .GetFinancialYear()

                ddlFinancialYear.DataTextField = "vcFinYearDesc"
                ddlFinancialYear.DataValueField = "numFinYearId"
                ddlFinancialYear.DataSource = ds
                ddlFinancialYear.DataBind()
                ddlFinancialYear.Items.Insert(0, "--Select One--")
                ddlFinancialYear.Items.FindByText("--Select One--").Value = "0"

                .Mode = 2
                ds = .GetFinancialYear()
                If ds.Tables(0).Rows.Count > 0 Then
                    lblDesc.Text = "Current Finacial Year is " & ds.Tables(0).Rows(0).Item("vcFinYearDesc") & " From " & ds.Tables(0).Rows(0).Item("dtPeriodFrom") & "  To " & ds.Tables(0).Rows(0).Item("dtPeriodTo")
                    hdnCurrentFinYearID.Value = ds.Tables(0).Rows(0).Item("numFinYearId")
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objCOA As New ChartOfAccounting
            With objCOA
                .FinancialYearID = hdnCurrentFinYearID.Value
                .NextFinancialYearID = ddlFinancialYear.SelectedValue
                If rblCloseType.SelectedValue = 1 Then
                    .Mode = 1
                Else
                    .Mode = 2
                End If
                .DomainId = Session("DomainID")
                .CloseFinancialYear()
            End With
            Response.Redirect("../Accounting/frmNewFinancialYear.aspx", False)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            Response.Redirect("../Accounting/frmNewFinancialYear.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class