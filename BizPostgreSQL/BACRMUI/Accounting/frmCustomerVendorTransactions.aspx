<%--<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustomerVendorTransactions.aspx.vb"
    Inherits=".frmCustomerVendorTransactions" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Accounts</title>

    <script language="javascript">

        function fn_CheckSearchCondition() {
            if (document.getElementById('calFrom_txtDate').value == '' || document.getElementById('calTo_txtDate').value == '') {
                alert("From and To Date should be selected.!!!");
                return false;
            }
            else { return true; }
        }

        function fn_GoToURL(varURL) {
            var url = varURL
            if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
                var LoWindow = window.open(url, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function OpenSetting() {
            window.open('../Prospects/frmConfCompanyList.aspx?RelId=2', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function DeleteRecord() {
            var ContactId = '';
            for (var i = 2; i <= document.getElementById('gvSearch').rows.length; i++) {
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }

                if (document.getElementById('gvSearch_ctl' + str + '_chk').checked == true) {

                    if (ContactId == '') {
                        ContactId = document.getElementById('gvSearch_ctl' + str + '_lbl1').innerHTML + '~' + document.getElementById('gvSearch_ctl' + str + '_lbl2').innerHTML + '~' + document.getElementById('gvSearch_ctl' + str + '_lbl3').innerHTML
                    }
                    else {
                        ContactId = ContactId + ',' + document.getElementById('gvSearch_ctl' + str + '_lbl1').innerHTML + '~' + document.getElementById('gvSearch_ctl' + str + '_lbl2').innerHTML + '~' + document.getElementById('gvSearch_ctl' + str + '_lbl3').innerHTML
                    }
                }
            }
            //alert(ContactId)
            document.getElementById('txtDelContactIds').value = ContactId

            return true
        }
        function OpemEmail(a, b) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContact(a, b) {
            var profileid = document.getElementById('ddlProfile').value;
            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&ft6ty=oiuy&CntId=" + a + '&profileid=' + profileid;


            document.location.href = str;

        }
        function DeleteRecord1() {
            var str;
            if (document.Form1.ddlSort.value == 9) {
                str = 'Are you sure, you want to delete the selected organization from favorites?'
            }
            else {
                str = 'Are you sure, you want to delete the selected record?'
            }
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function fnSortByChar(varSortChar) {
            document.Form1.txtSortChar.value = varSortChar;
            if (document.Form1.txtCurrrentPage != null) {
                document.Form1.txtCurrrentPage.value = 1;
            }
            document.Form1.btnGo1.click();
        }
        function SortColumn(a) {
//            document.Form1.txtSortColumn.value = a;
//            document.Form1.submit();
            //            return false;
            document.Form1.txtSortColumn.value = a;
            if (document.Form1.txtSortColumn.value == a) {
                if (document.Form1.txtSortOrder.value == 'A')
                    document.Form1.txtSortOrder.value = 'D';
                else
                    document.Form1.txtSortOrder.value = 'A';
            }
            document.Form1.submit();
            return false;
        }

        function SelectAll(a) {
            var str;

            if (typeof (a) == 'string') {
                a = document.getElementById(a)
            }
            if (a.checked == true) {
                for (var i = 1; i <= document.getElementById('gvSearch').rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('gvSearch_ctl' + str + '_chk').checked = true;
                }
            }
            else if (a.checked == false) {
                for (var i = 1; i <= document.getElementById('gvSearch').rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('gvSearch_ctl' + str + '_chk').checked = false;
                }
            }

            //gvSearch_ctl02_chk
        }
        function OpenWindow(a, b, c) {
            var profileid = document.getElementById('ddlProfile').value;
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlistDivID=" + a + '&profileid=' + profileid;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&DivID=" + a + '&profileid=' + profileid;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&klds+7kldf=fjk-las&DivId=" + a + '&profileid=' + profileid;

            }

            document.location.href = str;

        }




        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?frm=accounts";
            return false;
        }
        function DeleteRecord1() {
            var str;
            if (document.Form1.ddlSort.value == 9) {
                str = 'Are you sure, you want to delete the selected organization from favorites?'
            }
            else {
                str = 'Are you sure, you want to delete the selected record?'
            }
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>

    <style type="text/css">
        .style1
        {
            width: 143px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <table align="right">
                    <tr>
                        <td class="normal1" width="0">
                            &nbsp;
                        </td>
                        <td class="normal1">
                            &nbsp;From
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                        </td>
                        <td class="normal1">
                            &nbsp;To
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calTo" runat="server" />
                        </td>
                        <td class="normal1">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnGo" CssClass="button" runat="server" Text="Go"></asp:Button>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:RadioButton ID="rdByName" runat="server" GroupName="SortingOn" Text="Transactions by name"
                    CssClass="normal1" Checked="true" />
                <tr valign="middle">
                    <td valign="middle">
                        <rad:RadComboBox ID="radCmbCompany" ExternalCallBackPage="LoadCustomerVendor.aspx"
                            Width="195px" DropDownWidth="200px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                            AllowCustomText="True" EnableLoadOnDemand="True" SkinsPath="~/RadControls/ComboBox/Skins">
                        </rad:RadComboBox>
                    </td>
                </tr>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="style1">
                            <asp:RadioButton ID="rdByCustomer" runat="server" GroupName="SortingOn" Text="Customer Transactions"
                                CssClass="normal1" />
                        </td>
                        <td class="normal1" align="left">
                            <asp:RadioButton ID="rdByVendor" runat="server" Text="Vendor Transactions" CssClass="normal1"
                                GroupName="SortingOn" />
                            &nbsp;
                        </td>
                        <td id="hidenav" nowrap align="right" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCurrrentPage" runat="server" CssClass="signup" Width="28px" AutoPostBack="true"
                                            MaxLength="5"></asp:TextBox>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblOf" runat="server">of</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkLast" runat="server"><div class="LinkArrow">>></div>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="a" href="javascript:fnSortByChar('a')">
                                <div class="A2Z">
                                    A</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="b" href="javascript:fnSortByChar('b')">
                                <div class="A2Z">
                                    B</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="c" href="javascript:fnSortByChar('c')">
                                <div class="A2Z">
                                    C</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="d" href="javascript:fnSortByChar('d')">
                                <div class="A2Z">
                                    D</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="e" href="javascript:fnSortByChar('e')">
                                <div class="A2Z">
                                    E</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="f" href="javascript:fnSortByChar('f')">
                                <div class="A2Z">
                                    F</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="g" href="javascript:fnSortByChar('g')">
                                <div class="A2Z">
                                    G</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="h" href="javascript:fnSortByChar('h')">
                                <div class="A2Z">
                                    H</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="I" href="javascript:fnSortByChar('i')">
                                <div class="A2Z">
                                    I</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="j" href="javascript:fnSortByChar('j')">
                                <div class="A2Z">
                                    J</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="k" href="javascript:fnSortByChar('k')">
                                <div class="A2Z">
                                    K</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="l" href="javascript:fnSortByChar('l')">
                                <div class="A2Z">
                                    L</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="m" href="javascript:fnSortByChar('m')">
                                <div class="A2Z">
                                    M</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="n" href="javascript:fnSortByChar('n')">
                                <div class="A2Z">
                                    N</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="o" href="javascript:fnSortByChar('o')">
                                <div class="A2Z">
                                    O</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="p" href="javascript:fnSortByChar('p')">
                                <div class="A2Z">
                                    P</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="q" href="javascript:fnSortByChar('q')">
                                <div class="A2Z">
                                    Q</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="r" href="javascript:fnSortByChar('r')">
                                <div class="A2Z">
                                    R</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="s" href="javascript:fnSortByChar('s')">
                                <div class="A2Z">
                                    S</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="t" href="javascript:fnSortByChar('t')">
                                <div class="A2Z">
                                    T</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="u" href="javascript:fnSortByChar('u')">
                                <div class="A2Z">
                                    U</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="v" href="javascript:fnSortByChar('v')">
                                <div class="A2Z">
                                    V</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="w" href="javascript:fnSortByChar('w')">
                                <div class="A2Z">
                                    W</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="x" href="javascript:fnSortByChar('x')">
                                <div class="A2Z">
                                    X</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="y" href="javascript:fnSortByChar('y')">
                                <div class="A2Z">
                                    Y</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="z" href="javascript:fnSortByChar('z')">
                                <div class="A2Z">
                                    Z</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="all" href="javascript:fnSortByChar('0')">
                                <div class="A2Z">
                                    All</div>
                            </a>
                        </td>
                    </tr>
                </table>
                <asp:Table ID="table2" Width="100%" runat="server" Height="350" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="dg" Width="100%">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <asp:BoundField DataField="" Visible="false" HeaderText="ID" />
                                    <asp:BoundField DataField="CompanyName" Visible="true" HeaderText="Customer or Vendor" />
                                    <asp:BoundField DataField="Deposit" Visible="true" HeaderText="BizDoc Payment" />
                                    <asp:BoundField DataField='<%=_GetBizDoc(Databinder.Eval(Container.DataItem,"TransactionType") %>'
                                        Visible="true" HeaderText="BizDoc" />
                                    <asp:BoundField DataField="TransactionType" Visible="true" HeaderText="Financial Account Affected" />
                                    <asp:BoundField DataField="Deposit" Visible="true" HeaderText="COA Trans. Amount" />
                                    <asp:BoundField DataField="TransactionType" Visible="true" HeaderText="From (Accounting)" />
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <table width="100%">
                    <tr>
                        <td class="normal4" align="center">
                            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="txtDelContactIds" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
                 <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
                <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>--%>
