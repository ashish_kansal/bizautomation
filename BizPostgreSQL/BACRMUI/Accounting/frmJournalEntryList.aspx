<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmJournalEntryList.aspx.vb"
    Inherits=".frmJournalEntryList" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Journal Entries</title>

    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>

    <script language="javascript" type="text/javascript">
        function Validate() {
            if (isNaN(parseFloat(document.getElementById("lblTotDebitValue").value)) || isNaN(parseFloat(document.getElementById("lblTotCreditValue").value))) {
                alert("The Credit and Debit amounts you�re entering must be of the same amount");
                return false;
            }

            if (document.getElementById("lblTotDebitValue").value != document.getElementById("lblTotCreditValue").value) {
                alert("The Credit and Debit amounts you�re entering must be of the same amount");
                return false;
            }
            ////        alert(document.getElementById("lblTotDebitValue").value);
            ////        alert(document.getElementById("lblTotCreditValue").value);
            return true;
        }

        ////       function Validate()
        ////     {
        ////       var lbltotalDebit;
        ////       var lbltotalCredit;
        ////       lbltotalDebit=document.getElementById("lblTotDebitValue1").innerText;
        ////       lbltotalCredit=document.getElementById("lblTotCreditValue1").innerText;
        ////       lbltotalDebit=lbltotalDebit.replace(/,/g,"");
        ////       lbltotalCredit=lbltotalCredit.replace(/,/g,"");
        ////////       if(isNaN(parseFloat("lbltotalDebit")) || isNaN(parseFloat("lbltotalCredit")))
        ////////       {
        ////////            alert("The Credit and Debit amounts you�re entering must be of the same amount");
        ////////            return false;
        ////////       }
        ////       
        ////       if(lbltotalDebit!=lbltotalCredit)
        ////       {
        ////            alert("The Credit and Debit amounts you�re entering must be of the same amount");
        ////            return false;
        ////       }
        ////           return true;
        ////     }


        function Validate() {
            var lbltotalDebit;
            var lbltotalCredit;
            var idRoot = 'dgJournalEntry';
            var idtxtDr = 'txtDebit';
            var idtxtCr = 'txtCredit';
            var idtxtDebit;
            if (document.all) {
                lbltotalDebit = document.getElementById("lblTotDebitValue1").innerText;
                lbltotalCredit = document.getElementById("lblTotCreditValue1").innerText;
            } else {
                lbltotalDebit = document.getElementById("lblTotDebitValue1").textContent;
                lbltotalCredit = document.getElementById("lblTotCreditValue1").textContent;
            }

            lbltotalDebit = lbltotalDebit.replace(/,/g, "");
            lbltotalCredit = lbltotalCredit.replace(/,/g, "");

            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtDebit = '_ctl0'
                }
                else {
                    idtxtDebit = '_ctl'
                }

                if (document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtDr).value != "" || document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtCr).value != "") {
                    if (document.getElementById(idRoot + idtxtDebit + i + '_ddlAccounts').value == 0) {
                        alert('You must select an account for each split line with an amount');
                        return false;
                    }
                    // if (document.getElementById(idRoot+idtxtDebit+i+'_ddlName').value==0)
                    if ((document.getElementById(idRoot + idtxtDebit + i + '_txtAccountTypeId').value == 814 || document.getElementById(idRoot + idtxtDebit + i + '_txtAccountTypeId').value == 815) && document.getElementById(idRoot + idtxtDebit + i + '_ddlName').value == 0) {
                        alert('You must select an Company name for each split line with an amount');
                        return false;
                    }
                }
            }

            if (lbltotalDebit == '' && lbltotalCredit == '') {
                alert("Please enter at least two lines.");
                return false;
            }

            if (lbltotalDebit != lbltotalCredit) {
                alert("The Credit and Debit amounts you�re entering must be of the same amount");
                return false;
            }
            return true;
        }

        function SetDebitCreditValue(obj) {
            //alert(obj.id);
            var h = obj.id;
            var i;
            var idRoot = 'dgJournalEntry';
            i = h.substring(18, 20);
            var AcntTypeId = document.getElementById(idRoot + '_ctl' + i + '_ddlAccounts').value.split("~")
            //alert(document.getElementById(idRoot+'_ctl'+i+'_ddlAccounts').value);
            document.getElementById(idRoot + '_ctl' + i + '_txtAccountTypeId').value = AcntTypeId[1];
            //alert(document.getElementById(idRoot+'_ctl'+i+'_lblAccountTypeId').);

            if (ConditionCheck()) {
                ////        alert(h.length);

                var idtxtTail = 'txtDebit';
                var diff;
                var difference;
                var totalDebit;
                var totalCredit;
                var lbltotalDebit;
                var lbltotalCredit;
                if (document.all) {
                    totalDebit = document.getElementById('lblTotDebitValue1').innerText;
                    totalDebit = totalDebit.replace(/,/g, "");
                    totalCredit = document.getElementById('lblTotCreditValue1').innerText;
                    totalCredit = totalCredit.replace(/,/g, "");
                } else {
                    totalDebit = document.getElementById('lblTotDebitValue1').textContent;
                    totalDebit = totalDebit.replace(/,/g, "");
                    totalCredit = document.getElementById('lblTotCreditValue1').textContent;
                    totalCredit = totalCredit.replace(/,/g, "");
                }


                if (isNaN(parseFloat(totalDebit))) {
                    totalDebit = 0;
                }

                if (isNaN(parseFloat(totalCredit))) {

                    totalCredit = 0;
                }



                if (parseFloat(totalDebit) < parseFloat(totalCredit)) {
                    if ((document.getElementById(idRoot + '_ctl' + i + '_txtCredit').value == "") && (document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value == "")) {
                        //alert(idRoot+'_ctl'+i+'_txtDebit');
                        diff = parseFloat(totalCredit) - parseFloat(totalDebit);
                        document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value = formatCurrency(parseFloat(diff));
                        lbltotalDebit = parseFloat(totalDebit) + parseFloat(diff);
                        if (document.all) {
                            document.getElementById('lblTotDebitValue1').innerText = formatCurrency(parseFloat(lbltotalDebit));
                            document.getElementById('TotalDrValue').innerText = formatCurrency(parseFloat(lbltotalDebit));
                            document.getElementById('lblTotalDebit1').innerText = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                            document.getElementById('TotalDrText').innerText = "Total Debit: " + ' ' + document.getElementById('Currency').value;

                        } else {
                            document.getElementById('lblTotDebitValue1').textContent = formatCurrency(parseFloat(lbltotalDebit));
                            document.getElementById('TotalDrValue').textContent = formatCurrency(parseFloat(lbltotalDebit));
                            document.getElementById('lblTotalDebit1').textContent = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                            document.getElementById('TotalDrText').textContent = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                        }
                    }
                }
                //alert(parseFloat(totalDebit));
                if (parseFloat(totalDebit) > parseFloat(totalCredit)) {
                    if ((document.getElementById(idRoot + '_ctl' + i + '_txtDebit').value == "") && (document.getElementById(idRoot + '_ctl' + i + '_txtCredit').value == "")) {
                        // alert(idRoot+'_ctl'+i+'_txtCredit');
                        difference = parseFloat(totalDebit) - parseFloat(totalCredit);
                        //alert(parseFloat(difference));
                        document.getElementById(idRoot + '_ctl' + i + '_txtCredit').value = formatCurrency(parseFloat(difference));
                        lbltotalCredit = parseFloat(totalCredit) + parseFloat(difference);
                        //alert(parseFloat(totalCredit));
                        if (document.all) {
                            document.getElementById('lblTotCreditValue1').innerText = formatCurrency(parseFloat(lbltotalCredit));
                            document.getElementById('TotalCrValue').innerText = formatCurrency(parseFloat(lbltotalCredit));
                            document.getElementById('lblTotalCredit1').innerText = "Total Credit: " + ' ' + document.getElementById('Currency').value;
                            document.getElementById('TotalCrText').innerText = "Total Credit: " + ' ' + document.getElementById('Currency').value;

                        } else {
                            document.getElementById('lblTotCreditValue1').textContent = formatCurrency(parseFloat(lbltotalCredit));
                            document.getElementById('TotalCrValue').textContent = formatCurrency(parseFloat(lbltotalCredit));
                            document.getElementById('lblTotalCredit1').textContent = "Total Credit: " + ' ' + document.getElementById('Currency').value;
                            document.getElementById('TotalCrText').textContent = "Total Credit: " + ' ' + document.getElementById('Currency').value;

                        }
                    }

                }

            }

        }
        function ConditionCheck() {
            if (document.all) {
                if (document.getElementById('lblTotDebitValue1').innerText != document.getElementById('lblTotCreditValue1').innerText)
                    return true;
                else
                    return false;
            } else {
                if (document.getElementById('lblTotDebitValue1').textContent != document.getElementById('lblTotCreditValue1').textContent)
                    return true;
                else
                    return false;
            }
        }
        function Calculate(obj) {

            var idRoot = 'dgJournalEntry';
            var idtxtTail = 'txtDebit';
            var irowCnt;
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var idtxtDebitvalue;
            var CrValue;
            var CrTotValue;
            //alert(document.getElementById('dgJournalEntry_ctl01_txtDebit').value);
            //alert(document.getElementById('dgJournalEntry').rows.length);
            document.getElementById(obj.id).value = formatCurrency(document.getElementById(obj.id).value);
            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtDebit = '_ctl0'
                }
                else {
                    idtxtDebit = '_ctl'
                }
                // alert(document.getElementById(idRoot+idtxtDebit+i+'_'+idtxtTail));
                // alert(document.getElementById(idRoot+idtxtDebit+i+'_'+idtxtTail).value);
                //alert(document.getElementById(idRoot+idtxtDebit+i+'_'+idtxtTail).value);
                if (document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtTail).value != "") {
                    CrValue = document.getElementById(idRoot + idtxtDebit + i + '_txtCredit').value;
                    CrValue = CrValue.replace(/,/g, "");

                    if (CrValue != "" && !isNaN(CrValue)) {
                        if (document.all) {
                            CrTotValue = document.getElementById('lblTotCreditValue1').innerText;
                            CrTotValue = CrTotValue.replace(/,/g, "");
                            CrTotValue -= CrValue;
                            document.getElementById('lblTotCreditValue1').innerText = formatCurrency(CrTotValue);
                            document.getElementById(idRoot + idtxtDebit + i + '_txtCredit').value = "";
                        } else {
                            CrTotValue = document.getElementById('lblTotCreditValue1').textContent;
                            CrTotValue = CrTotValue.replace(/,/g, "");
                            CrTotValue -= CrValue;
                            document.getElementById('lblTotCreditValue1').textContent = formatCurrency(CrTotValue);
                            document.getElementById(idRoot + idtxtDebit + i + '_txtCredit').value = "";
                        }

                    }


                    txtdebitvalue = document.getElementById(idRoot + idtxtDebit + i + '_' + idtxtTail).value
                    txtdebitvalue = txtdebitvalue.replace(/,/g, "");
                    //alert(txtdebitvalue);
                    itotal += parseFloat(txtdebitvalue);

                }

            }
            if (document.all) {
                document.getElementById('lblTotalDebit1').innerText = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                document.getElementById('TotalDrText').innerText = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                digits = formatCurrency(itotal);
                document.getElementById('lblTotDebitValue1').innerText = digits;
                document.getElementById('TotalDrValue').innerText = digits;

            } else {
                document.getElementById('lblTotalDebit1').textContent = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                document.getElementById('TotalDrText').textContent = "Total Debit: " + ' ' + document.getElementById('Currency').value;
                digits = formatCurrency(itotal);
                document.getElementById('lblTotDebitValue1').textContent = digits;
                document.getElementById('TotalDrValue').textContent = digits;

            }

            //  alert(itotal);
            // alert(document.getElementById('lblTotDebitValue').value);

        }

        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

        function CalculateCreditValue(obj) {
            //  alert(obj.id);
            var idRoot = 'dgJournalEntry';
            var idtxtTail = 'txtCredit';
            var idtxtCredit;
            var irowCnt;
            var itotal = 0.0;
            var formatDigit;
            var txtCreditValue;
            var idtxtCreditvalue;
            var DrValue;
            var DrTotValue;
            document.getElementById(obj.id).value = formatCurrency(document.getElementById(obj.id).value);

            // alert(document.getElementById('dgJournalEntry').rows.length);
            for (var i = 2; i < document.getElementById('dgJournalEntry').rows.length + 1; i++) {
                if (i < 10) {
                    idtxtCredit = '_ctl0'
                }
                else {
                    idtxtCredit = '_ctl'
                }

                if (document.getElementById(idRoot + idtxtCredit + i + '_' + idtxtTail).value != "") {
                    idtxtCreditvalue = document.getElementById(idRoot + idtxtCredit + i + '_' + idtxtTail).value;
                    idtxtCreditvalue = idtxtCreditvalue.replace(/,/g, "");
                    //alert(idtxtCreditvalue);

                    DrValue = document.getElementById(idRoot + idtxtCredit + i + '_txtDebit').value;
                    DrValue = DrValue.replace(/,/g, "");
                    if (DrValue != "" && !isNaN(DrValue)) {
                        if (document.all) {
                            DrTotValue = document.getElementById('lblTotDebitValue1').innerText;
                            DrTotValue = DrTotValue.replace(/,/g, "");
                            DrTotValue -= DrValue;
                            document.getElementById('lblTotDebitValue1').innerText = formatCurrency(DrTotValue);
                            document.getElementById(idRoot + idtxtCredit + i + '_txtDebit').value = "";
                        } else {
                            DrTotValue = document.getElementById('lblTotDebitValue1').textContent;
                            DrTotValue = DrTotValue.replace(/,/g, "");
                            DrTotValue -= DrValue;
                            document.getElementById('lblTotDebitValue1').textContent = formatCurrency(DrTotValue);
                            document.getElementById(idRoot + idtxtCredit + i + '_txtDebit').value = "";
                        }

                    }
                    txtCreditValue = document.getElementById(idRoot + idtxtCredit + i + '_' + idtxtTail).value;
                    txtCreditValue = txtCreditValue.replace(/,/g, "");
                    itotal += parseFloat(txtCreditValue);

                }

            }
            if (document.all) {
                document.getElementById('lblTotalCredit1').innerText = "Total Credit: " + ' ' + document.getElementById('Currency').value;
                document.getElementById('TotalCrText').innerText = "Total Credit: " + ' ' + document.getElementById('Currency').value;
                formatDigit = formatCurrency(itotal);
                document.getElementById('lblTotCreditValue1').innerText = formatDigit;
                document.getElementById('TotalCrValue').innerText = formatDigit;

            } else {
                document.getElementById('lblTotalCredit1').textContent = "Total Credit: " + ' ' + document.getElementById('Currency').value;
                document.getElementById('TotalCrText').textContent = "Total Credit: " + ' ' + document.getElementById('Currency').value;
                formatDigit = formatCurrency(itotal);
                document.getElementById('lblTotCreditValue1').textContent = formatDigit;
                document.getElementById('TotalCrValue').textContent = formatDigit;
            }

            //alert(document.getElementById('lblTotDebitValue').value);

        }


        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
    </script>

</head>
<body>
    <form id="form1" runat="server" method="post">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <br />
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp; Journal Entry &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" align="center" width="50px">
                <asp:Label ID="JournalDate" runat="server" Text="Date  "></asp:Label>
            </td>
            <td class="normal1" align="left">
                <BizCalendar:Calendar ID="calFrom" runat="server" />
            </td>
            <td class="normal1" align="right" id="td2" runat="server" visible="true">
                Recurring Template &nbsp; &nbsp;
            </td>
            <td>
                <asp:DropDownList ID="ddlMakeRecurring" CssClass="signup" Width="150px" runat="server">
                </asp:DropDownList>
            </td>
            <td align="right">
                &nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add"></asp:Button>
                <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="384px">
        <asp:TableRow ID="TableRow1" VerticalAlign="Top" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">
                <table id="Table2" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="bottom">
                            <asp:DataGrid ID="dgJournalEntry" runat="server" CssClass="dg" Width="100%"
                                AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Accounts">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numJournalId") %>'></asp:Label>
                                            <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionId") %>'></asp:Label>
                                            <asp:Label ID="lblAccountID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numChartAcntId") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlAccounts" onchange="SetDebitCreditValue(this);" Width="200px"
                                                runat="server" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtAccountTypeId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numAcntType") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Debit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDebitAmt" runat="server" Style="display: none" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.numDebitAmt")) %>'></asp:Label>
                                            <asp:TextBox ID="txtDebit" TextMode="SingleLine" Width="100" runat="server" CssClass="signup"
                                                onchange="Calculate(this);"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Credit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditAmt" runat="server" Style="display: none" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.numCreditAmt")) %>'></asp:Label>
                                            <asp:TextBox ID="txtCredit" TextMode="SingleLine" Width="100" runat="server" CssClass="signup"
                                                onchange="CalculateCreditValue(this);"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Memo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemo" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.varDescription") %>'></asp:Label>
                                            <asp:TextBox ID="txtMemo" TextMode="SingleLine" Width="100" runat="server" CssClass="signup"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numCustomerId") %>'></asp:Label>
                                            <asp:TextBox ID="txtCompanyName" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                                            <asp:Button ID="btnCompanyName" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                                            <asp:DropDownList ID="ddlName" runat="server" Width="200px" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Bottom" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Relationship">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRelationship" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.varRelation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0">
                                <tr>
                                    <td class="normal1" align="right">
                                        <asp:Label ID="lblTotalDebit1" CssClass="signup" runat="server"></asp:Label>
                                        <asp:Label CssClass="signup" ID="lblTotDebitValue1" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        <asp:Label ID="lblTotalCredit1" CssClass="signup" runat="server"></asp:Label>
                                        <asp:Label ID="lblTotCreditValue1" CssClass="signup" BorderWidth="0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td valign="top" >
                            &nbsp;Narration:<asp:TextBox runat="server" TextMode="MultiLine" Rows="2" CssClass="signup" ID="txtDescription"
                                Width="350px" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="TotalDrValue" runat="server" />
    <asp:HiddenField ID="TotalCrValue" runat="server" />
    <asp:HiddenField ID="TotalDrText" runat="server" />
    <asp:HiddenField ID="TotalCrText" runat="server" />
    <asp:HiddenField ID="Currency" runat="server" />
    </form>
</body>
</html>--%>
