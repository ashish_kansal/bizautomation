''Created By Siva
imports BACRM.BusinessLogic.Accounting 
Imports BACRM.BusinessLogic.Common
Partial Public Class frmTimeAndExpensesDetails
    Inherits BACRMPage

#Region "Variables"
    Dim lngUserCntID As Long
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngUserCntID = CCommon.ToLong(GetQueryStringVal("NameU"))   'Take the user id from the querystring
            btnCancel.Attributes.Add("onclick", "return Close()")
            If Not IsPostBack Then LoadTimeAndExpensesList()
            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadTimeAndExpensesList()
        Try
            Dim lobjPayollExpense As New PayrollExpenses
            lobjPayollExpense.DomainId = Session("DomainId")
            lobjPayollExpense.UserCntID = lngUserCntID
            lobjPayollExpense.ComPayPeriodID = CCommon.ToLong(GetQueryStringVal("ComPayPeriodID"))
            lobjPayollExpense.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            Dim ds As DataSet = lobjPayollExpense.GetTimeAndExpensesDetails
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                dgTimeAndExpenses.DataSource = ds.Tables(0)
                dgTimeAndExpenses.DataBind()

                If ds.Tables.Count > 1 Then
                    gvBPTask.DataSource = ds.Tables(1)
                    gvBPTask.DataBind()
                End If
            End If
            
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgTimeAndExpenses_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTimeAndExpenses.ItemCommand
        Try
            If e.CommandName = "Edit" Then
                Dim lngCategoryHDRID As Long = e.Item.Cells(0).Text

                Response.Redirect("../TimeAndExpense/frmEditTimeExp.aspx?type=P&CatID=" & lngCategoryHDRID)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class