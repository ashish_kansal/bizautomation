<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmQuickAccountReport.aspx.vb"
    Inherits=".frmQuickAccountReport" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quick Accounting Report</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function OpenJournalDetailsPage(url) {
            window.location.href = url;
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenTimeExpense(url) {
            window.open(url, "TimeExpenses", 'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
            return false;
        }
        function reDirect(a) {
            document.location.href = a;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <br />
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom" width="400px">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Account Quick Report &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <td width="40px" class="normal1">
                    &nbsp;&nbsp;From
                </td>
                <td width="40px" class="normal1">
                    <BizCalendar:Calendar ID="calFrom" runat="server" />
                </td>
                <td width="20px" class="normal1">
                    &nbsp;&nbsp;&nbsp;To
                </td>
                <td width="40px" class="normal1">
                    <BizCalendar:Calendar ID="calTo" runat="server" />
                </td>
                <td valign="middle">
                    &nbsp;&nbsp;&nbsp;<asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button"></asp:Button>
                </td>
                <td align="right">
                    <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back"></asp:Button>
                </td>
        </tr>
    </table>
    <asp:Table ID="tblGeneralLeger" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow Width="100%">
            <asp:TableCell VerticalAlign="top">
                <table cellpadding="0" width="100%" cellspacing="0" border="0" bgcolor="white">
                    <tr valign="top" width="100%">
                        <td valign="top">
                            <asp:Repeater ID="RepTransactionReport" runat="server">
                                <HeaderTemplate>
                                    <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                        <tr class="normal5">
                                            <td bgcolor="#52658C" align="center">
                                                <b>Date</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Transaction Type</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Name</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Memo/Description</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Split</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Amount</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Balance</b>
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="normal1">
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Date") %>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit"><%# DataBinder.Eval(Container.DataItem,"Type")%> </asp:LinkButton>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Name") %>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Memo/Description") %>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Split") %>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "Amount")%>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "Balance")%>
                                            <asp:Label ID="lblJournalId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"JournalId")%>'></asp:Label>
                                            <asp:Label ID="lblCheckId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CheckId")%>'></asp:Label>
                                            <asp:Label ID="lblCashCreditCardId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CashCreditCardId")%>'></asp:Label>
                                            <asp:Label ID="lblAcntType" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numChartAcntId")%>'></asp:Label>
                                            <asp:Label ID="lblTransactionId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
                                            <asp:Label ID="lblOppId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppId")%>'></asp:Label>
                                            <asp:Label ID="lblOppBizDocsId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppBizDocsId")%>'></asp:Label>
                                            <asp:Label ID="lblDepositId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numDepositId")%>'></asp:Label>
                                            <asp:Label ID="lblCategoryHDRID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategoryHDRID")%>'></asp:Label>
                                            <asp:Label ID="lblTEType" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"tintTEType")%>'></asp:Label>
                                            <asp:Label ID="lblCategory" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategory")%>'></asp:Label>
                                            <asp:Label ID="lblUserCntID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numUserCntID")%>'></asp:Label>
                                            <asp:Label ID="lblFromDate" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"dtFromDate")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr bgcolor="#C6D3E7" class="normal1">
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Date") %>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit"><%# DataBinder.Eval(Container.DataItem,"Type")%></asp:LinkButton>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Name") %>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Memo/Description") %>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "Split")%>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "Amount")%>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container.DataItem,"Balance") %>
                                            <asp:Label ID="lblJournalId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"JournalId")%>'></asp:Label>
                                            <asp:Label ID="lblCheckId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CheckId")%>'></asp:Label>
                                            <asp:Label ID="lblCashCreditCardId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CashCreditCardId")%>'></asp:Label>
                                            <asp:Label ID="lblAcntType" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numChartAcntId")%>'></asp:Label>
                                            <asp:Label ID="lblTransactionId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
                                            <asp:Label ID="lblOppId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppId")%>'></asp:Label>
                                            <asp:Label ID="lblOppBizDocsId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppBizDocsId")%>'></asp:Label>
                                            <asp:Label ID="lblDepositId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numDepositId")%>'></asp:Label>
                                            <asp:Label ID="lblCategoryHDRID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategoryHDRID")%>'></asp:Label>
                                            <asp:Label ID="lblTEType" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"tintTEType")%>'></asp:Label>
                                            <asp:Label ID="lblCategory" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategory")%>'></asp:Label>
                                            <asp:Label ID="lblUserCntID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numUserCntID")%>'></asp:Label>
                                            <asp:Label ID="lblFromDate" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"dtFromDate")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
