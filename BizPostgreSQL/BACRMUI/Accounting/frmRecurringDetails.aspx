﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringDetails.aspx.vb"
    Inherits=".frmRecurringDetails" MasterPageFile="~/common/PopUp.Master" ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Partial Invoicing</title>
    <script type="text/javascript">
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function Check() {
            if ($("#rbManuallRecurring").is(':checked')) {
                $('#tblAutoRecurring').find(':input').attr('disabled', true);
                $('#tblRentalBizDoc').find(':input').attr('disabled', true);
                $('#tblManuallRecurring').find(':input').attr('disabled', false);

                $('#rbAutoRecurring').attr('disabled', false);

                if ($('#rbRentalBizDoc'))
                    $('#rbRentalBizDoc').attr('disabled', false);
            }
            else if ($("#rbAutoRecurring").is(':checked')) {
                $('#tblManuallRecurring').find(':input').attr('disabled', true);
                $('#tblRentalBizDoc').find(':input').attr('disabled', true);
                $('#tblAutoRecurring').find(':input').attr('disabled', false);

                $('#rbManuallRecurring').attr('disabled', false);

                if ($('#rbRentalBizDoc'))
                    $('#rbRentalBizDoc').attr('disabled', false);
            }
            else {
                $('#tblManuallRecurring').find(':input').attr('disabled', true);
                $('#tblAutoRecurring').find(':input').attr('disabled', true);
                $('#tblRentalBizDoc').find(':input').attr('disabled', false);

                $('#rbManuallRecurring').attr('disabled', false);
                $('#rbAutoRecurring').attr('disabled', false);
            }
        }
        function Add() {
            $("#rbManuallRecurring").prop('checked', true);

            if ($("#CalIncremetalCreateDate_txtDate").val() == "") {
                alert('Please select bizdoc creation date');
                return false;
            }
            if ($("#rbOption1").is(':checked') == false && $("#rbOption2").is(':checked') == false) {
                alert('Please select atleast one option');
                return false;
            }
            if ($("#rbOption1").is(':checked') == true) {
                if (Number($("#txtBillingBreakup").val()) < 0.001) {
                    alert('Please enter Billing Breakup Percentage');
                    return false;
                }
            }

            if ($("#rbOption2").is(':checked') == true) {
                if (Number($("#txtBillingAmount").val()) == 0) {
                    alert('Please enter valid Billing Amount ');
                    return false;
                }
            }

            return true;
        }
        function Save() {
            $("#rbAutoRecurring").prop('checked', true);

            if ($("#ddlBizDocRecurringTemplate").val() < 1) {
                alert('Please select Recurring Template');
                return false;
            }
            if ($("#CalStartDate_txtDate").val() == "") {
                alert('Please select recurring start date');
                return false;
            }

            return true;
        }
        function UpdatePercentageLabel() {
            if (Number($("#txtBillingAmount").val()) == 0) {
                alert('Please enter valid Billing Amount ');
                return false;
            }
            if (Number($("#txtBillingAmount").val()) > Number($("#hdnDealAmount").val())) {
                alert('Amount can not be greater than total order amount');
                return false;
            }
            var Percentange = Number($("#txtBillingAmount").val()) * 100 / Number($("#hdnDealAmount").val());
            $("#lblMsg").text('i.e. ' + roundNumber(Percentange, 4) + '% of Total Order Value');
        }
        function UpdateAmountLabel() {
            var Amount = Number($("#hdnDealAmount").val()) * Number($("#txtBillingBreakup").val()) / 100;
            if (Amount > Number($("#hdnDealAmount").val())) {
                alert('Amount can not be greater than total order amount');
                return false;
            }
            $("#lblMsg1").text('i.e. ' + roundNumber(Amount, 4) + ' Amount of Total Order Value');
        }
        function UpdateTotal(Mode) {
            var TotalPercentage = 0;
            var Prefix = '';
            var Amount = 0;
            var TotalSplitAmount = 0;
            if (!isNaN(Number($("#hdnDealAmount").val()))) {
                if (Mode == 1) //Update Amount from Percentage
                {

                    $("#dgBizDocSchedule").find(":input[name*='txtBizdocAmount']").each(function () {

                        Prefix = $(this).attr("ID").toString().replace("txtBizdocAmount", "")
                        Amount = Number($("#hdnDealAmount").val()) * Number($("#" + Prefix + "txtBreakUpPercentage").val()) / 100;
                        //                        console.log(Amount);
                        $(this).val(roundNumber(Amount, 4));
                        TotalSplitAmount += Amount;
                        TotalPercentage += Number($("#" + Prefix + "txtBreakUpPercentage").val());
                        //                        console.log(Number($("#" + Prefix + "txtBreakUpPercentage").val()));
                    });
                }
                else if (Mode == 2) {
                    var Percentange = 0;

                    $("#dgBizDocSchedule").find(":input[name*='txtBreakUpPercentage']").each(function () {
                        Prefix = $(this).attr("ID").toString().replace("txtBreakUpPercentage", "")
                        Percentange = Number($("#" + Prefix + "txtBizdocAmount").val()) * 100 / Number($("#hdnDealAmount").val());
                        //                        console.log(Percentange);
                        $(this).val(roundNumber(Percentange, 4));
                        TotalSplitAmount += Number($("#" + Prefix + "txtBizdocAmount").val());
                        TotalPercentage += Number($(this).val());
                    });
                }
            }
            TotalPercentage = roundNumber(TotalPercentage, 4);
            $("#lblTotalPercentage").text(TotalPercentage);
            $("#lblSplitTotal").text(roundNumber(TotalSplitAmount, 4).toString());
            $("#lblTotalOrder").text(roundNumber($("#hdnDealAmount").val(), 4));
            return TotalPercentage;
        }
        function checkTotal() {

            if (UpdateTotal(1) != 100) {
                alert('Sum total of Breakup Percentage must equal to 100%');
                return false;
            }
            //            if (UpdateTotal(2) != 100) {
            //                alert('Sum total of Breakup Percentage must equal to 100%');
            //                return false;
            //            }
            if ($("#lblSplitTotal").text() != $("#lblTotalOrder").text()) {
                alert('Sum total of Split BizDoc Amount must equal to Total Order Amount');
                return false;
            }
            return true;
        }
        function roundNumber(num, dec) {
            var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
            return result;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }

//        function RentalBizDocSave(format) {

//            var TotalchkSelected = 0;

//            $('#dgRentalBizDocs tr').not(':first').each(function () {
//                if ($(this).find("input[id*='chkInclude']").is(':checked')) {

//                    if ($(this).find("input[id*='CalRentalStartDate_txtDate']").val() == 0) {
//                        alert("Enter Rental Start Date")
//                        $(this).find("input[id*='CalRentalStartDate_txtDate']").focus();
//                        return false;
//                    }

//                    if ($(this).find("input[id*='CalRentalReturnDate_txtDate']").val() == 0) {
//                        alert("Enter Rental Return Date")
//                        $(this).find("input[id*='CalRentalReturnDate_txtDate']").focus();
//                        return false;
//                    }

//                    if (!isDate($(this).find("input[id*='CalRentalStartDate_txtDate']").val(), format)) {
//                        alert("Enter Valid Rental Start Date");
//                        $(this).find("input[id*='CalRentalStartDate_txtDate']").focus();
//                        return false;
//                    }

//                    if (!isDate($(this).find("input[id*='CalRentalReturnDate_txtDate']").val(), format)) {
//                        alert("Enter Valid Rental Return Date");
//                        $(this).find("input[id*='CalRentalReturnDate_txtDate']").focus();
//                        return false;
//                    }

//                    if (compareDates($(this).find("input[id*='CalRentalStartDate_txtDate']").val(), format, $(this).find("input[id*='CalRentalReturnDate_txtDate']").val(), format) == 1) {
//                        alert("Rental Return Date must be greater than or equal to Rental Start Date");
//                        $(this).find("input[id*='CalRentalReturnDate_txtDate']").focus();
//                        return false;
//                    }

//                    TotalchkSelected += 1;
//                }
//            });

//            if (TotalchkSelected == 0) {
//                alert("No item is selected to Include in BizDoc.");
//                return false;
//            }
//            //            for (i = 1; i < document.getElementById('dgRentalBizDocs').rows.length; i++) {
//            //                if (i <= 8) {
//            //                    index = '0' + (i + 1)
//            //                }
//            //                else {
//            //                    index = (i + 1);
//            //                }

//            //                if (document.getElementById('dgRentalBizDocs_ctl' + index + '_chkInclude').checked == true) {
//            //                    if (document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').value == 0) {
//            //                        alert("Enter Rental Start Date")
//            //                        document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').focus();
//            //                        return false;
//            //                    }

//            //                    if (document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').value == 0) {
//            //                        alert("Enter Rental Return Date")
//            //                        document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').focus();
//            //                        return false;
//            //                    }

//            //                    if (!isDate(document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').value, format)) {
//            //                        alert("Enter Valid Rental Start Date");
//            //                        document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').focus();
//            //                        return false;
//            //                    }

//            //                    if (!isDate(document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').value, format)) {
//            //                        alert("Enter Valid Rental Return Date");
//            //                        document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').focus();
//            //                        return false;
//            //                    }

//            //                    if (compareDates(document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').value, format, document.getElementById('dgRentalBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').value, format) == 1) {
//            //                        alert("Rental Return Date must be greater than or equal to Rental Start Date");
//            //                        return false;
//            //                    }

//            //                    TotalchkSelected += 1;
//            //                }
//            //            }


//        }

        $(document).ready(function () {
            var chkBox = $("input[id$='chkAll']");
            chkBox.click(
          function () {
              $("#dgRentalBizDocs .ClasschkSelect INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("#dgRentalBizDocs .ClasschkSelect INPUT[type='checkbox']").click(
        function (e) {
            if (!$(this)[0].checked) {
                chkBox.prop("checked", false);
            }
        });

            $("#btnRentalBizDoc").click(function (e) {
                var TotalchkSelected = 0;

                $('#dgRentalBizDocs tr').not(':first').each(function () {
                    if ($(this).find("input[id*='chkInclude']").is(':checked')) {

                        if ($(this).find("input[id*='CalRentalStartDate_txtDate']").val() == 0) {
                            alert("Enter Rental Start Date")
                            $(this).find("input[id*='CalRentalStartDate_txtDate']").focus();
                            e.preventDefault();
                            return false;
                        }

                        if ($(this).find("input[id*='CalRentalReturnDate_txtDate']").val() == 0) {
                            alert("Enter Rental Return Date")
                            $(this).find("input[id*='CalRentalReturnDate_txtDate']").focus();
                            e.preventDefault();
                            return false;
                        }

                        if (!isDate($(this).find("input[id*='CalRentalStartDate_txtDate']").val(), $("#hdnDateTimeFormat").val())) {
                            alert("Enter Valid Rental Start Date");
                            $(this).find("input[id*='CalRentalStartDate_txtDate']").focus();
                            e.preventDefault();
                            return false;
                        }

                        if (!isDate($(this).find("input[id*='CalRentalReturnDate_txtDate']").val(), $("#hdnDateTimeFormat").val())) {
                            alert("Enter Valid Rental Return Date");
                            $(this).find("input[id*='CalRentalReturnDate_txtDate']").focus();
                            e.preventDefault();
                            return false;
                        }

                        if (compareDates($(this).find("input[id*='CalRentalStartDate_txtDate']").val(), $("#hdnDateTimeFormat").val(), $(this).find("input[id*='CalRentalReturnDate_txtDate']").val(), $("#hdnDateTimeFormat").val()) == 1) {
                            alert("Rental Return Date must be greater than or equal to Rental Start Date");
                            $(this).find("input[id*='CalRentalReturnDate_txtDate']").focus();
                            e.preventDefault();
                            return false;
                        }

                        TotalchkSelected += 1;
                    }
                });

                if (TotalchkSelected == 0) {
                    alert("No item is selected to Include in BizDoc.");
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" style="width: 99%">
                <tr>
                    <td valign="bottom" height="23">
                    </td>
                    <td align="right">
                        &nbsp<asp:Button ID="btnClose" runat="Server" CssClass="button" OnClientClick="javascript:window.close();"
                            Text="Close"></asp:Button>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Partial Invoicing
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblSelectField" Width="700px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" style="width:900px" border="0">
                    <%--<tr>
                            <td align="left" valign="middle" class="normal1" colspan="2">
                                <asp:CheckBox ID="chkTurnOnRecurringTran" runat="server" Text="Turn On Recurring Transaction"
                                    Style="text-align: left" />
                            </td>
                        </tr>--%>
                    <tr>
                        <td colspan="2">
                            <table id="tblRecurreSO" runat="server">
                                <tr>
                                    <td align="left" valign="middle" class="normal1">
                                        <asp:Literal ID="litClientMessage" runat="server" EnableViewState="False"></asp:Literal>
                                        Use this closed order as a template from which to clone 'Recurring Orders' as per
                                        the following recurring template "
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlRec" runat="server"
                                            CssClass="signup" Width="180px">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="chkAmtZero" runat="server" Text="Create order with Amount Paid as Zero" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="tblRentalBizDoc" runat="server">
                        <td colspan="2">
                            <fieldset>
                                <legend>
                                    <asp:RadioButton Text="<b>Create invoice for rental items</b>" runat="server" ID="rbRentalBizDoc"
                                        CssClass="signup" GroupName="Mode" onclick="Check();" />
                                </legend>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnRentalBizDoc" runat="Server" CssClass="button" Text="Save"></asp:Button>
                                            <asp:HiddenField ID="hdnAuthorizativeBizDocsId" runat="server" />
                                            <asp:HiddenField ID="hdnDateTimeFormat" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1">
                                            <asp:DataGrid ID="dgRentalBizDocs" runat="server" AutoGenerateColumns="false" CssClass="dg">
                                                <AlternatingItemStyle CssClass="ais" />
                                                <ItemStyle CssClass="is" />
                                                <HeaderStyle CssClass="hs" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="numOppItemtCode" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="charItemType" HeaderText="ItemType" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="QtyOrdered" HeaderText="Qty Ordered"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="vcSKU" HeaderText="SKU"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Attributes" HeaderText="Attributes"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Rental Start Date">
                                                        <ItemTemplate>
                                                            <BizCalendar:Calendar ID="CalRentalStartDate" runat="server" ShowTime="12" SelectedDate='<%#DataBinder.Eval(Container.DataItem, "dtRentalStartDate")%>'
                                                                ClientIDMode="AutoID" />
                                                            <asp:HiddenField ID="hfnumWarehouseItmsID" runat="server" Value='<%# Eval("numWarehouseItmsID") %>' />
                                                            <asp:HiddenField ID="hfnumItemCode" runat="server" Value='<%# Eval("numItemCode") %>' />
                                                            <asp:HiddenField ID="hfnumUOM" runat="server" Value='<%# Eval("numUOM") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Rental Return Date">
                                                        <ItemTemplate>
                                                            <BizCalendar:Calendar ID="CalRentalReturnDate" runat="server" ShowTime="12" SelectedDate='<%#DataBinder.Eval(Container.DataItem, "dtRentalReturnDate")%>'
                                                                ClientIDMode="AutoID" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAll" runat="server" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkInclude" runat="server" class="ClasschkSelect" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr id="tblRecurreBizDoc" runat="server">
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td id="tblAutoRecurring" runat="server">
                                        <fieldset>
                                            <legend>
                                                <asp:RadioButton Text="<b>Create recurring invoice as per template</b>" runat="server"
                                                    ID="rbAutoRecurring" CssClass="signup" GroupName="Mode" onclick="Check();" />
                                            </legend>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="normal1" colspan="3">
                                                        Create “Recurring Authoritative BizDocs” within THIS order as per the following
                                                        recurring template
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" width="200px" valign="middle">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlBizDocRecurringTemplate"
                                                            runat="server" Width="180px" CssClass="signup">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" class="normal1" width="110px">
                                                        &nbsp; Start recurring from
                                                    </td>
                                                    <td align="left" class="normal1">
                                                        <BizCalendar:Calendar ID="CalStartDate" runat="server" ClientIDMode="AutoID" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="3">
                                                        <asp:Button ID="btnSaveandClose" runat="Server" CssClass="button" Text="Save" OnClientClick="return Save();">
                                                        </asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" id="tblManuallRecurring" runat="server">
                                        <fieldset>
                                            <legend>
                                                <asp:RadioButton Text="<b>Create invoice incrementaly</b>" runat="server" ID="rbManuallRecurring"
                                                    CssClass="signup" GroupName="Mode" onclick="Check();" /></legend>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" class="normal1" style="display:none">
                                                        Billing Term, Net Days
                                                        <asp:DropDownList ID="ddlNetDays" runat="server" CssClass="signup">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" class="normal1">
                                                    </td>
                                                    <td class="normal1" align="left" colspan="2">
                                                        &nbsp; Create BizDoc on
                                                        <BizCalendar:Calendar ID="CalIncremetalCreateDate" runat="server" ClientIDMode="AutoID" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" colspan="3">
                                                        <asp:RadioButton ID="rbOption1" Text="Enter Billing Breakup Percentage" runat="server"
                                                            Checked="true" GroupName="Breakup" />
                                                        <asp:TextBox runat="server" ID="txtBillingBreakup" CssClass="signup" onkeypress="CheckNumber(1,event);"
                                                            onblur="UpdateAmountLabel();" Width="40px" />% &nbsp;<asp:Label Text="" runat="server"
                                                                ID="lblMsg1" CssClass="normal1" />
                                                    </td>
                                                    <td class="normal1">
                                                        Percentage Left to bill:
                                                        <asp:Label Text="" runat="server" ID="lblPercentageLeft" />%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="normal1" colspan="3">
                                                        <asp:RadioButton ID="rbOption2" Text="Enter Billing Amount" runat="server" GroupName="Breakup" />
                                                        <asp:TextBox runat="server" ID="txtBillingAmount" CssClass="signup" onkeypress="CheckNumber(1,event);"
                                                            Width="100px" onblur="UpdatePercentageLabel();" />
                                                        <asp:Label Text="" runat="server" ID="lblMsg" CssClass="normal1" />
                                                    </td>
                                                    <td class="normal1">
                                                        Amount Left to bill:
                                                        <asp:Label Text="" runat="server" ID="lblAmountLeft" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" valign="middle" colspan="4">
                                                        <asp:Button Text="Add" runat="server" ID="btnAdd" CssClass="button" OnClientClick="return Add();" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <b>BizDoc Recurring History</b>
                            <br />
                            <asp:DataGrid ID="dgBizDocSchedule" runat="server" CssClass="dg" Width="100%" ShowFooter="true"
                                AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn ItemStyle-Wrap="False" ItemStyle-VerticalAlign="Bottom">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text="Edit" ID="lnkbtnEdt" CommandName="Edit"></asp:LinkButton><asp:Label
                                                ID="lblSeperator" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
                                            <asp:LinkButton runat="server" Text="Bill incrementally" ID="lnkbtnCreateBizDoc"
                                                CommandName="CreateBizDoc" CommandArgument='<%# Eval("numOppRecID") %>'></asp:LinkButton>
                                            <asp:Label Text='<%# Eval("numOppRecID") %>' runat="server" ID="lblOppRecID" CssClass="signup"
                                                Style="display: none" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"
                                                CommandArgument='<%# Eval("numOppRecID") %>'></asp:LinkButton>&nbsp;
                                            <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnBillRemaining" Text="Bill remaining balance" runat="server" CssClass="button"
                                                CommandName="BillRemanining" />
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" HeaderText="BizDoc Issue" ItemStyle-HorizontalAlign="Center"
                                        ReadOnly="true"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Creation Date">
                                        <ItemTemplate>
                                            <%# Eval("dtRecurringDate")%>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <BizCalendar:Calendar ID="CalRecurranceDate" runat="server" SelectedDate='<%# Eval("dtRecurringDate")%>'
                                                ClientIDMode="AutoID" />
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BizDoc ID">
                                        <ItemTemplate>
                                            <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocid") %>');">
                                                <%#Eval("vcbizdocid")%>
                                            </a>
                                            <asp:Label ID="lblBizDocID" Text='<%# Eval("numoppbizdocid") %>' runat="server" Style="display: none;" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn DataField="Billingdays" HeaderText="Billing Terms" ItemStyle-HorizontalAlign="Center" Visible="false"
                                        ReadOnly="true"></asp:BoundColumn>--%>
                                    <asp:TemplateColumn HeaderText="Breakup Percentage">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtBreakUpPercentage" CssClass="signup" Text='<%# Eval("fltBreakupPercentage") %>'
                                                Width="40px" Enabled=' <%# iif(Eval("numoppbizdocid")>0,false,true) %>' onkeypress="CheckNumber(1,event);"
                                                onblur="UpdateTotal(1);"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            Total:<asp:Label Text="" runat="server" ID="lblTotalPercentage" ClientIDMode="Static" />
                                            %
                                            <asp:Button Text="Update Percentage" runat="server" ID="btnUpdatePercentage" CssClass="button"
                                                CommandName="UpdatePercentage" OnClientClick="return checkTotal();" />
                                        </FooterTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Split By Order Amount">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtBizdocAmount" CssClass="signup" Text="" Width="100px"
                                                Enabled=' <%# iif(Eval("numoppbizdocid")>0,false,true) %>' onkeypress="CheckNumber(1,event);"
                                                onblur="UpdateTotal(2);"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <div style="float: left">
                                                Total Order Amount:<asp:Label Text="" runat="server" ID="lblTotalOrder" ClientIDMode="Static" /><br />
                                                Total Order Amount After Split:<asp:Label Text="" runat="server" ID="lblSplitTotal"
                                                    ClientIDMode="Static" /></div>
                                        </FooterTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                                OnClientClick="return DeleteRecord();" CommandArgument='<%# Eval("numOppRecID") %>'>
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField runat="server" ID="hdnDealAmount" ClientIDMode="Static" />
    <input type="hidden" id="hdnoppId" runat="server" />
</asp:Content>
