<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmJournalEntry.aspx.vb"
    Inherits=".frmJournalEntry" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Bank Register</title>
    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function reDirect(a) {
            document.location.href = a;
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, "BizInvoice", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenTimeExpense(url) {
            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=650,height=400,scrollbars=no,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <table id="Table1" cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="normal1" valign="top">
                Opening Balance:
            </td>
            <td>
                <asp:Label Text="" runat="server" ID="lblOpeningBalance" />
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td class="normal1" valign="top">
                <asp:Label ID="JournalDate" runat="server" Text="Date: "></asp:Label>
            </td>
            <td class="normal1" valign="top">
                <BizCalendar:Calendar ID="calFrom" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="right">
        <tr>
            <td align="right" valign="top">
                &nbsp;
                <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button"></asp:Button>
                <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back"></asp:Button>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" EnableViewState="false" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblJournalEntryTitle" runat="server" Text="Bank Register"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="384px">
        <asp:TableRow ID="TableRow1" VerticalAlign="Top" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">
                <asp:DataGrid ID="dgJournalEntry" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn ItemStyle-Wrap="False" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" ID="lnkbtnEdt" CommandName="Edit"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="JournalId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numTransactionId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CashCreditCardId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numChartAcntId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numOppBizDocsId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numDepositId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numBizDocsPaymentDetId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numCategoryHDRID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tintTEType" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numCategory" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numUserCntID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtFromDate" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Entry Date">
                            <ItemTemplate>
                                <asp:Label ID="lblJDate" runat="server" Text='<%# ReturnDate(DataBinder.Eval(Container.DataItem, "EntryDate")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblEditDate" runat="server" Text='<%# ReturnDate(DataBinder.Eval(Container.DataItem, "EntryDate")) %>'></asp:Label>
                               
                                <BizCalendar:Calendar ID="txtJournalDateDisplay" runat="server" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "EntryDate") %>' />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Transaction Type">
                            <ItemTemplate>
                                <asp:Label ID="lblTransactiontype" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionType") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblTransactiontype1" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionType") %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblCompany" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CompanyName") %>'></asp:Label>
                                <asp:Label ID="lblCheckId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.CheckId") %>'></asp:Label>
                                <asp:Label ID="lblCheckNo" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numCheckNo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.JournalId") %>'></asp:Label>
                                <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numTransactionId") %>'></asp:Label>
                                <asp:Label ID="lblCheckId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.CheckId") %>'></asp:Label>
                                <asp:Label ID="lblCashCreditCardId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.CashCreditCardId") %>'></asp:Label>
                                <asp:Label ID="lblDivisionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numCustomerId") %>'></asp:Label>
                                <asp:Label ID="lblChartAcntId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numChartAcntId") %>'></asp:Label>
                                <asp:TextBox ID="txtCompanyName" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                                <asp:Button ID="btnCompanyName" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                <asp:Label ID="lblCompany" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CompanyName") %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Memo">
                            <ItemTemplate>
                                <asp:Label ID="lblMemo" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Memo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEDesc" TextMode="MultiLine" Width="300" runat="server" CssClass="signup"
                                    Text='<%# DataBinder.Eval(Container,"DataItem.Memo") %>'>
                                </asp:TextBox>
                                <asp:Label ID="lblEditMemo" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Memo") %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Deposit" ItemStyle-CssClass="money">
                            <ItemTemplate>
                                <asp:Label ID="lblDeposit" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Deposit")) %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDeposit" runat="server" CssClass="signup" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Deposit")) %>'></asp:TextBox>
                                <asp:Label ID="lblDepositAmt" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Deposit")) %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Payment" ItemStyle-VerticalAlign="Bottom" ItemStyle-CssClass="money">
                            <ItemTemplate>
                                <asp:Label ID="lblPayment" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Payment") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPayment" runat="server" CssClass="signup" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.Payment")) %>'></asp:TextBox>
                                <asp:Label ID="lblPaymentAmt" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Payment")) %>'></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Balance" ItemStyle-CssClass="money">
                            <ItemTemplate>
                                <asp:Label ID="lblBalance" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "numBalance")) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" ID="lnkbtnEditJournal" CommandName="EditJournalEntry"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteAction" runat="server" CssClass="button Delete" Text="X"
                                    CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkDeleteAction" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                                <asp:Label ID="lblFYClosed" Text="Closed" runat="server" Visible="false" CssClass="normal13" />
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>--%>
