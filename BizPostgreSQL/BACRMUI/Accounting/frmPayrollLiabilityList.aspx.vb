''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports ClosedXML.Excel


Partial Public Class frmPayrollLiabilityList
    Inherits BACRMPage
#Region "Variables"

    Dim objCommon As CCommon
    Dim m_aryRightsForUpdate() As Integer

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lblException.Text = ""
            GetUserRightsForPage(35, 87)
            If Not IsPostBack Then
                m_aryRightsForUpdate = GetUserRightsForPage_Other(35, 87)
                If m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) <> 3 Then
                    btnPay.Visible = False
                End If

                Dim lobjPayroll As New PayrollExpenses
                lobjPayroll.DomainID = Session("DomainId")
                hdnPayPeriod.Value = CCommon.ToInteger(lobjPayroll.GetPayPeriod())

                'To Set Permission
                objCommon = New CCommon


                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("~/admin/authentication.aspx?mesg=AS")
                End If
                objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
                LoadMonth(ddlMonth)
                LoadYear(ddlYear)
                LoadDateDropDownList()

                btnGo.Attributes.Add("onclick", "return ValidateCustomDateRange()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadMonth(ByVal ddlMonth As DropDownList)
        Try
            Dim Count As Integer
            For Count = 1 To 12
                ddlMonth.Items.Add(New ListItem(MonthName(Count), Count))
            Next
            ddlMonth.Items.FindByValue(Month(Now)).Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadYear(ByVal ddlYear As DropDownList)
        Try
            ddlYear.Items.Add(Year(Now()) - 2)
            ddlYear.Items.Add(Year(Now()) - 1)
            ddlYear.Items.Add(Year(Now()))
            ddlYear.Items.Add(Year(Now()) + 1)
            ddlYear.Items.Add(Year(Now()) + 2)
            ddlYear.Items.Add(Year(Now()) + 3)
            ddlYear.Items.Add(Year(Now()) + 4)
            ddlYear.Items.FindByValue(Year(Now())).Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadGrid()
        Try

            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.UserCntID = Session("UserContactID")
            objPayrollExpenses.DepartmentId = ddlDepartment.SelectedItem.Value
            objPayrollExpenses.UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
            objPayrollExpenses.UserUpdateRightType = m_aryRightsForPage(RIGHTSTYPE.UPDATE)
            objPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objPayrollExpenses.ComPayPeriodID = CCommon.ToLong(hdnComPayPeriodID.Value)
            Dim dt As DataTable = objPayrollExpenses.GetPayrollLiabilityDetails()
            dt.Columns.Add("monAmountRemaining", GetType(System.Decimal), "ISNULL(monTotalAmountDue,0) - ISNULL(monAmountPaid,0)")
            If CCommon.ToString(ViewState("SortColumn")) <> "" Then
                If CCommon.ToString(ViewState("SortColumn")) = "AmountDue" Then
                    If CCommon.ToString(ViewState("SortOrder")) = "DESC" Then
                        dt.DefaultView.Sort = "monTotalAmountDue DESC"
                    Else
                        dt.DefaultView.Sort = "monTotalAmountDue ASC"
                    End If
                ElseIf CCommon.ToString(ViewState("SortColumn")) = "AmountRemaining" Then
                    If CCommon.ToString(ViewState("SortOrder")) = "DESC" Then
                        dt.DefaultView.Sort = "monAmountRemaining DESC"
                    Else
                        dt.DefaultView.Sort = "monAmountRemaining ASC"
                    End If
                End If

                dt.AcceptChanges()
            End If

            dtlPayrollLiability.DataSource = dt
            dtlPayrollLiability.DataBind()

            UpdatePanelGrid.Update()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then
                Return String.Format("{0:#,##0.00}", Money)
            Else
                Return String.Empty
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        Try
            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
        Try
            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            chkDate.Checked = False
            LoadDateDropDownList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub LoadDateDropDownList()
        Try
            Dim lobjPayroll As New PayrollExpenses
            Dim lintPayPeriod As Integer
            Dim strStartDate As String
            Dim strEndDate As String
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim i As Integer
            dt.Columns.Add("Date", GetType(String))
            dt.Columns.Add("DateValue", GetType(String))
            lobjPayroll.DomainID = Session("DomainId")
            lintPayPeriod = CCommon.ToInteger(hdnPayPeriod.Value)
            If lintPayPeriod = 1 Then
                strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, Day(DateAdd("d", -1, DateSerial(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value + 1, 1))))
                dr = dt.NewRow
                dr("Date") = strStartDate & " to " & strEndDate
                dr("DateValue") = strStartDate & " to " & strEndDate
                dt.Rows.Add(dr)
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                '' ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 2 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 15)
                    Else
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 16)
                        strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, Day(DateAdd("d", -1, DateSerial(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value + 1, 1))))
                    End If
                    dr = dt.NewRow
                    dr("Date") = strStartDate & " to " & strEndDate
                    dr("DateValue") = strStartDate & " to " & strEndDate
                    dt.Rows.Add(dr)
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
                ''strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)

            ElseIf lintPayPeriod = 3 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Monday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 4 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Tuesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 5 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Wednesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 6 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Thursday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 7 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Friday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 8 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Saturday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 9 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Sunday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                '' ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 10 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Monday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 11 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Tuesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 12 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Wednesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 13 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Thursday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                '' ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 14 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Friday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                ''ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 15 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Saturday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
                '' ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
            ElseIf lintPayPeriod = 16 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Sunday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
                '' ddlDate.Items.Insert(0, New ListItem("--Select One --", "0"))
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
            ElseIf lintPayPeriod = 17 Then
                ddlYear.Visible = False
                ddlMonth.Visible = False
                ddlDate.Visible = False
                lblCurrent.Visible = False
                chkDate.Checked = True
                Dim objPayrollExpenses As New PayrollExpenses
                objPayrollExpenses.DomainID = Session("DomainId")

                Dim dtDate As DataTable = objPayrollExpenses.GetCommissionDatePayPeriod()
                If dtDate.Rows.Count > 0 Then
                    calFrom.SelectedDate = CDate(dtDate.Rows(0)("dtFromDate"))
                    calTo.SelectedDate = CDate(dtDate.Rows(0)("dtToDate"))
                Else
                    calFrom.SelectedDate = Date.Now
                    calTo.SelectedDate = CDate(New DateTime(Date.Now.Year, Date.Now.Month, DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month)))
                End If
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Try
            chkDate.Checked = False
            LoadDateDropDownList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        Try
            chkDate.Checked = False
            CheckIfCommissionCalculatedForPeriod()
            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblException.Text = CCommon.ToString(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Try
            If Not String.IsNullOrEmpty(calFrom.SelectedDate) AndAlso Not String.IsNullOrEmpty(calFrom.SelectedDate) Then
                chkDate.Checked = True
                CheckIfCommissionCalculatedForPeriod()
                LoadGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub CheckIfCommissionCalculatedForPeriod()
        Try
            Dim Sdate As Date
            Dim Edate As Date
            If hdnPayPeriod.Value = 17 Or chkDate.Checked Then
                Sdate = calFrom.SelectedDate
                Edate = calTo.SelectedDate
            Else
                Sdate = CDate(ddlDate.SelectedItem.Value.Split("t")(0))
                Edate = CDate(ddlDate.SelectedItem.Value.Split("t")(1).Substring(1))
            End If
            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.StartDate = Sdate
            objPayrollExpenses.EndDate = Edate

            Dim dt As DataTable = objPayrollExpenses.GetCommissionPayPeriod()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                hdnComPayPeriodID.Value = CCommon.ToString(dt.Rows(0)("numComPayPeriodID"))
                btnGenerateCommission.Text = "Recalculate Commission"
            Else
                btnGenerateCommission.Text = "Calculate Commission"
                hdnComPayPeriodID.Value = ""
            End If

            UpdatePanelCommission.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnGenerateCommission_Click(sender As Object, e As EventArgs) Handles btnGenerateCommission.Click
        Try
            If CCommon.ToInteger(hdnPayPeriod.Value) > 0 Then
                CheckIfCommissionCalculatedForPeriod()

                Dim Sdate As Date
                Dim Edate As Date
                If hdnPayPeriod.Value = 17 Or chkDate.Checked Then
                    Sdate = calFrom.SelectedDate
                    Edate = calTo.SelectedDate
                Else
                    Sdate = CDate(ddlDate.SelectedItem.Value.Split("t")(0))
                    Edate = CDate(ddlDate.SelectedItem.Value.Split("t")(1).Substring(1))
                End If
                Dim objPayrollExpenses As New PayrollExpenses
                objPayrollExpenses.DomainID = CCommon.ToLong(Session("DomainId"))
                objPayrollExpenses.ComPayPeriodID = CCommon.ToLong(hdnComPayPeriodID.Value)
                objPayrollExpenses.StartDate = CDate(Sdate)
                objPayrollExpenses.EndDate = CDate(Edate)
                objPayrollExpenses.PayPeriod = CCommon.ToInteger(hdnPayPeriod.Value)
                objPayrollExpenses.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objPayrollExpenses.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                hdnComPayPeriodID.Value = objPayrollExpenses.CalculateCommission()

                btnGenerateCommission.Text = "Recalculate Commission"
                LoadGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub dtlPayrollLiability_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dtlPayrollLiability.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                m_aryRightsForUpdate = GetUserRightsForPage_Other(35, 87)

                If m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) = 0 Then
                    CType(e.Item.FindControl("btnRecalculateCommission"), Button).Visible = False
                End If

                If m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) <> 3 Then
                    CType(e.Item.FindControl("btnUpdatePayrollAmt"), Button).Visible = False
                    CType(e.Item.FindControl("btnUpdateReimbursableAmt"), Button).Visible = False
                End If
            End If
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim lngUserID As Long
                Dim lngUserCntId As Long
                Dim lngDivisionID As Long
                Dim bitLinkVisible As Boolean

                lngUserID = CType(e.Item.FindControl("hdnUserID"), HiddenField).Value
                lngUserCntId = CType(e.Item.FindControl("hdnUserCntID"), HiddenField).Value
                lngDivisionID = CCommon.ToLong(CType(e.Item.FindControl("hdnDivisionID"), HiddenField).Value)
                bitLinkVisible = CCommon.ToBool(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("bitLinkVisible"))
                Dim lnkbtnDetails As LinkButton
                Dim dtStartDate As Date
                Dim dtEndDate As Date

                If chkDate.Checked Then
                    dtStartDate = calFrom.SelectedDate
                    dtEndDate = CDate(calTo.SelectedDate).AddDays(1).AddSeconds(-1)
                Else
                    dtStartDate = CDate(ddlDate.SelectedItem.Value.Split("t")(0))
                    dtEndDate = CDate(ddlDate.SelectedItem.Value.Split("t")(1).Substring(1)).AddDays(1).AddSeconds(-1)
                End If

                'If (DataBinder.Eval(e.Item.DataItem, "bContractBased") = False) Then
                lnkbtnDetails = e.Item.FindControl("lnkbtnDetails")
                If CCommon.ToBool(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("bitCommissionContact")) Then
                    lnkbtnDetails.Visible = False
                Else
                    lnkbtnDetails.Attributes.Add("onclick", "return OpenTimeAndExpenses('" & lngUserCntId & "'," & CCommon.ToLong(hdnComPayPeriodID.Value) & ")")
                End If

                Dim hplBizDocPaidCommAmt As HyperLink
                hplBizDocPaidCommAmt = e.Item.FindControl("hplBizDocPaidCommAmt")
                hplBizDocPaidCommAmt.NavigateUrl = "#"
                hplBizDocPaidCommAmt.Attributes.Add("onclick", "return OpenCommissionDetails('" & lngUserCntId & "','" & lngUserID & "',0," & CCommon.ToLong(hdnComPayPeriodID.Value) & "," & lngDivisionID & ")")

                Dim hplBizDocNonPaidCommAmt As HyperLink
                hplBizDocNonPaidCommAmt = e.Item.FindControl("hplBizDocNonPaidCommAmt")
                hplBizDocNonPaidCommAmt.NavigateUrl = "#"
                hplBizDocNonPaidCommAmt.Attributes.Add("onclick", "return OpenCommissionDetails('" & lngUserCntId & "','" & lngUserID & "',1," & CCommon.ToLong(hdnComPayPeriodID.Value) & "," & lngDivisionID & ")")

                Dim hplCommissionEarned As HyperLink
                hplCommissionEarned = e.Item.FindControl("hplCommissionEarned")
                hplCommissionEarned.NavigateUrl = "#"
                hplCommissionEarned.Attributes.Add("onclick", "return OpenCommissionEarnedDetails(" & lngUserCntId & "," & CCommon.ToLong(hdnComPayPeriodID.Value) & "," & lngDivisionID & ");")

                Dim hplSalesReturn As HyperLink
                hplSalesReturn = e.Item.FindControl("hplSalesReturn")
                hplSalesReturn.NavigateUrl = "#"
                hplSalesReturn.Attributes.Add("onclick", "return OpenCommissionDetails('" & lngUserCntId & "','" & lngUserID & "',4," & CCommon.ToLong(hdnComPayPeriodID.Value) & "," & lngDivisionID & ")")

                If m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) = 0 Then
                    CType(e.Item.FindControl("txtTax"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtPayrollAmt"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtPayrollAdditionalAmount"), TextBox).Enabled = False
                ElseIf m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) = 1 Then
                    Try
                        If CCommon.ToLong(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("numUserCntID")) <> Session("UserContactID") Then
                            CType(e.Item.FindControl("txtTax"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtPayrollAmt"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtPayrollAdditionalAmount"), TextBox).Enabled = False
                        End If
                    Catch ex As Exception
                    End Try
                ElseIf m_aryRightsForUpdate(RIGHTSTYPE.UPDATE) = 2 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If CCommon.ToString(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("vcteritorries")).Length = 0 Then
                        CType(e.Item.FindControl("txtTax"), TextBox).Enabled = False
                        CType(e.Item.FindControl("txtPayrollAmt"), TextBox).Enabled = False
                        CType(e.Item.FindControl("txtPayrollAdditionalAmount"), TextBox).Enabled = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If CCommon.ToString(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("vcteritorries")).Contains("," & dtTerritory.Rows(i).Item("numTerritoryId") & ",") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            CType(e.Item.FindControl("txtTax"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtPayrollAmt"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtPayrollAdditionalAmount"), TextBox).Enabled = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub imgBtnExportExcel_Click(sender As Object, e As EventArgs) Handles imgBtnExportExcel.Click
        Try
            Dim workbook As New XLWorkbook
            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Income Expense Statement")

            Dim doc As New HtmlAgilityPack.HtmlDocument
            doc.LoadHtml("<table>" & txtExportHtml.Text & "</table>")

            'ADD HEADER
            workSheet.Cell(1, 1).Value = " Payroll Expense " & IIf(chkDate.Checked, calFrom.SelectedDate & " to " & calTo.SelectedDate, ddlDate.SelectedItem.Text)
            workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

            workSheet.Cell(1, 1).Style.Font.Bold = True
            workSheet.Cell(1, 1).Style.Font.FontSize = 14
            workSheet.Range(workSheet.Cell(1, 1), workSheet.Cell(1, 9)).Merge()

            workSheet.Cell(2, 1).Value = "Name"
            workSheet.Cell(2, 2).Value = "Tax ID"
            workSheet.Cell(2, 3).Value = "Hourly Rate"
            workSheet.Cell(2, 4).Value = "Hours Worked"
            workSheet.Cell(2, 5).Value = "Paid Invoices (Deposited/bank)"
            workSheet.Cell(2, 6).Value = "Un-paid Invoices (* CMs & Refunds)"
            workSheet.Cell(2, 7).Value = "Reimbursable Expenses"
            workSheet.Cell(2, 8).Value = "Amounts Subject to Deductions"
            workSheet.Cell(2, 9).Value = "Amounts not subject to deductions"
            workSheet.Cell(2, 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 1).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 2).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 2).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 3).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 3).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 4).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 4).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 5).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 5).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 6).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 6).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 7).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 7).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 8).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 8).Style.Border.SetOutsideBorderColor(XLColor.Black)
            workSheet.Cell(2, 9).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Cell(2, 9).Style.Border.SetOutsideBorderColor(XLColor.Black)

            workSheet.Range("A2:I2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
            workSheet.Range("A2:I2").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center
            workSheet.Range("A2:I2").Style.Font.Bold = True
            workSheet.Range("A2:I2").Style.Fill.SetBackgroundColor(XLColor.LightGray)
            workSheet.Range("A2:I2").Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
            workSheet.Range("A2:I2").Style.Border.SetOutsideBorderColor(XLColor.Black)

            Dim i As Int32 = 3
            For Each item As DataListItem In dtlPayrollLiability.Items
                Dim totalAmountDue As Decimal = CCommon.ToDecimal(DirectCast(item.FindControl("lblTotalAmountDue"), Label).Text)
                Dim reimburseExpense As Decimal = CCommon.ToDecimal(DirectCast(item.FindControl("lblReimburse"), Label).Text)
                Dim isCommissionContact As Boolean = CCommon.ToBool(CCommon.ToInteger(DirectCast(item.FindControl("hdnCommissionContact"), HiddenField).Value))


                workSheet.Cell(i, 1).Value = DirectCast(item.FindControl("lblName"), Label).Text
                workSheet.Cell(i, 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 1).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 2).Value = DirectCast(item.FindControl("txtTax"), TextBox).Text
                workSheet.Cell(i, 2).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 2).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 3).Value = DirectCast(item.FindControl("lblHourlyRate"), Label).Text
                workSheet.Cell(i, 3).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 3).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 4).Value = DirectCast(item.FindControl("lblHours"), Label).Text
                workSheet.Cell(i, 4).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 4).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 5).Value = DirectCast(item.FindControl("hplBizDocPaidCommAmt"), HyperLink).Text & " (" & DirectCast(item.FindControl("lblDeposited"), Label).Text & ")"
                workSheet.Cell(i, 5).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 5).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 6).Value = DirectCast(item.FindControl("hplBizDocNonPaidCommAmt"), HyperLink).Text & " (" & DirectCast(item.FindControl("hplSalesReturn"), HyperLink).Text & ")"
                workSheet.Cell(i, 6).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 6).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 7).Value = reimburseExpense
                workSheet.Cell(i, 7).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 7).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 8).Value = If(isCommissionContact, 0, totalAmountDue - reimburseExpense)
                workSheet.Cell(i, 8).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 8).Style.Border.SetOutsideBorderColor(XLColor.Black)

                workSheet.Cell(i, 9).Value = If(isCommissionContact, totalAmountDue, reimburseExpense)
                workSheet.Cell(i, 9).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                workSheet.Cell(i, 9).Style.Border.SetOutsideBorderColor(XLColor.Black)


                i = i + 1
            Next

            workSheet.Columns(1).AdjustToContents()
            workSheet.Columns(2).AdjustToContents()
            workSheet.Columns(3).AdjustToContents()
            workSheet.Columns(4).AdjustToContents()
            workSheet.Columns(5).AdjustToContents()
            workSheet.Columns(6).AdjustToContents()
            workSheet.Columns(7).AdjustToContents()
            workSheet.Columns(8).AdjustToContents()
            workSheet.Columns(9).AdjustToContents()

            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment; filename=PayrollExpense.xlsx")
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Dim fs As New MemoryStream()
            workbook.SaveAs(fs)
            Response.BinaryWrite(fs.ToArray())
            Response.End()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub dtlPayrollLiability_ItemCommand(source As Object, e As DataListCommandEventArgs)
        Try
            If e.CommandName = "PayTime" Then
                If CCommon.ToLong(hdnComPayPeriodID.Value) > 0 Then
                    Dim isErrorOccured As Boolean = False
                    Dim errorMessage As String = ""
                    Dim ds As New DataSet()
                    Dim dt As New DataTable("Employee")
                    Dim dr As DataRow
                    dt.Columns.Add("numUserCntID", GetType(System.Int64))
                    dt.Columns.Add("monTimeAmtToPay", GetType(System.Double))
                    dt.Columns.Add("monAdditionalAmtToPay", GetType(System.Double))


                    For Each item As DataListItem In dtlPayrollLiability.Items
                        If CCommon.ToBool(DirectCast(item.FindControl("hdnIsOnPayroll"), HiddenField).Value) AndAlso DirectCast(item.FindControl("txtPayrollAmt"), TextBox).Enabled Then
                            If CCommon.ToDecimal(DirectCast(item.FindControl("txtPayrollAmt"), TextBox).Text) > CCommon.ToDecimal(DirectCast(item.FindControl("hdnTimeAmountToPay"), HiddenField).Value) Then
                                isErrorOccured = True
                                errorMessage = errorMessage & "Amt paid for Hrs Entered (" & CCommon.ToDecimal(DirectCast(item.FindControl("txtPayrollAmt"), TextBox).Text) & ") is greater than amount needs to be paid (" & CCommon.ToDecimal(DirectCast(e.Item.FindControl("hdnTimeAmountToPay"), HiddenField).Value) & ") for employee " & DirectCast(e.Item.FindControl("lblName"), Label).Text & ".<br />"
                            ElseIf CCommon.ToDecimal(DirectCast(item.FindControl("txtPayrollAmt"), TextBox).Text) >= 0 Then
                                dr = dt.NewRow()
                                dr("numUserCntID") = CCommon.ToLong(DirectCast(item.FindControl("hdnUserCntID"), HiddenField).Value)
                                dr("monTimeAmtToPay") = CCommon.ToDecimal(DirectCast(item.FindControl("txtPayrollAmt"), TextBox).Text)
                                dr("monAdditionalAmtToPay") = CCommon.ToDecimal(DirectCast(item.FindControl("txtPayrollAdditionalAmount"), TextBox).Text)
                                dt.Rows.Add(dr)
                            End If
                        End If
                    Next

                    If isErrorOccured Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidTimePayment", "alert('" & errorMessage & "');", True)
                    ElseIf dt.Rows.Count > 0 Then
                        ds.Tables.Add(dt)

                        Dim objPayrollExpenses As New PayrollExpenses
                        objPayrollExpenses.DomainID = CCommon.ToLong(Session("DomainID"))
                        objPayrollExpenses.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objPayrollExpenses.ComPayPeriodID = CCommon.ToLong(hdnComPayPeriodID.Value)
                        objPayrollExpenses.Mode = 1 'Pay Hours Worked
                        objPayrollExpenses.strItems = ds.GetXml()
                        objPayrollExpenses.UpdatePayrollAmountPaid()

                        LoadGrid()
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidTimePayment", "alert('Commission is not calculated for selected pay period. Please click Recalculate commission button.');", True)
                End If
            ElseIf e.CommandName = "PayReimbursableExpense" Then
                If CCommon.ToLong(hdnComPayPeriodID.Value) > 0 Then
                    Dim isErrorOccured As Boolean = False
                    Dim errorMessage As String = ""
                    Dim ds As New DataSet()
                    Dim dt As New DataTable("Employee")
                    Dim dr As DataRow
                    dt.Columns.Add("numUserCntID", GetType(System.Int64))
                    dt.Columns.Add("monReimbursableExpenseToPay", GetType(System.Double))

                    For Each item As DataListItem In dtlPayrollLiability.Items
                        If CCommon.ToBool(DirectCast(item.FindControl("hdnIsOnPayroll"), HiddenField).Value) AndAlso DirectCast(item.FindControl("txtReimburseExpense"), TextBox).Enabled Then
                            If CCommon.ToDecimal(DirectCast(item.FindControl("txtReimburseExpense"), TextBox).Text) > CCommon.ToDecimal(DirectCast(item.FindControl("hdnReimbursableExpenseToPay"), HiddenField).Value) Then
                                isErrorOccured = True
                                errorMessage = errorMessage & "Reimbursable Expense paid (" & CCommon.ToDecimal(DirectCast(item.FindControl("txtReimburseExpense"), TextBox).Text) & ") is greater than amount needs to be paid (" & CCommon.ToDecimal(DirectCast(e.Item.FindControl("hdnTimeAmountToPay"), HiddenField).Value) & ") for employee " & DirectCast(e.Item.FindControl("lblName"), Label).Text & ".<br />"
                            ElseIf CCommon.ToDecimal(DirectCast(item.FindControl("txtReimburseExpense"), TextBox).Text) >= 0 Then
                                dr = dt.NewRow()
                                dr("numUserCntID") = CCommon.ToLong(DirectCast(item.FindControl("hdnUserCntID"), HiddenField).Value)
                                dr("monReimbursableExpenseToPay") = CCommon.ToDecimal(DirectCast(item.FindControl("txtReimburseExpense"), TextBox).Text)
                                dt.Rows.Add(dr)
                            End If
                        End If
                    Next

                    If isErrorOccured Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidTimePayment", "alert('" & errorMessage & "');", True)
                    ElseIf dt.Rows.Count > 0 Then
                        ds.Tables.Add(dt)

                        Dim objPayrollExpenses As New PayrollExpenses
                        objPayrollExpenses.DomainID = CCommon.ToLong(Session("DomainID"))
                        objPayrollExpenses.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objPayrollExpenses.ComPayPeriodID = CCommon.ToLong(hdnComPayPeriodID.Value)
                        objPayrollExpenses.Mode = 2 'Pay reimbursable expense
                        objPayrollExpenses.strItems = ds.GetXml()
                        objPayrollExpenses.UpdatePayrollAmountPaid()

                        LoadGrid()
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidTimePayment", "alert('Commission is not calculated for selected pay period. Please click Recalculate commission button.');", True)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    
    Protected Sub lbAmountRemainingSort_Click(sender As Object, e As EventArgs)
        Try
            Dim lbAmountRemainingSort As LinkButton = DirectCast(sender, LinkButton)

            If CCommon.ToString(ViewState("SortColumn")) = "" Then
                ViewState("SortColumn") = "AmountRemaining"
                ViewState("SortOrder") = "ASC"
                lbAmountRemainingSort.Text = lbAmountRemainingSort.Text & "<i class='icon-caret-up'></i>"
            ElseIf CCommon.ToString(ViewState("SortColumn")) <> "AmountRemaining" Then
                ViewState("SortColumn") = "AmountRemaining"
                ViewState("SortOrder") = "ASC"
                lbAmountRemainingSort.Text = lbAmountRemainingSort.Text & "<i class='icon-caret-up'></i>"
            Else
                If ViewState("SortOrder") = "DESC" Then
                    ViewState("SortOrder") = "ASC"
                    lbAmountRemainingSort.Text = lbAmountRemainingSort.Text & "<i class='icon-caret-up'></i>"
                Else
                    ViewState("SortOrder") = "DESC"
                    lbAmountRemainingSort.Text = lbAmountRemainingSort.Text & "<i class='icon-caret-down'></i>"
                End If
            End If

            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub lbAmountDueSort_Click(sender As Object, e As EventArgs)
        Try
            Dim lbAmountDueSort As LinkButton = DirectCast(sender, LinkButton)

            If CCommon.ToString(ViewState("SortColumn")) = "" Then
                ViewState("SortColumn") = "AmountDue"
                ViewState("SortOrder") = "ASC"

                lbAmountDueSort.Text = lbAmountDueSort.Text & "<i class='icon-caret-up'></i>"
            ElseIf CCommon.ToString(ViewState("SortColumn")) <> "AmountDue" Then
                ViewState("SortColumn") = "AmountDue"
                ViewState("SortOrder") = "ASC"

                lbAmountDueSort.Text = lbAmountDueSort.Text & "<i class='icon-caret-up'></i>"
            Else
                If ViewState("SortOrder") = "DESC" Then
                    ViewState("SortOrder") = "ASC"
                    lbAmountDueSort.Text = lbAmountDueSort.Text & "<i class='icon-caret-up'></i>"
                Else
                    ViewState("SortOrder") = "DESC"
                    lbAmountDueSort.Text = lbAmountDueSort.Text & "<i class='icon-caret-down'></i>"
                End If
            End If

            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnPay_Click(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(hdnComPayPeriodID.Value) > 0 Then
                Dim ds As New DataSet()
                Dim dt As New DataTable("Employee")
                Dim dr As DataRow
                dt.Columns.Add("numUserCntID", GetType(System.Int64))
                dt.Columns.Add("numDivisionID", GetType(System.Int64))
                dt.Columns.Add("monHoursWorked", GetType(System.Decimal))
                dt.Columns.Add("monAdditionalAmt", GetType(System.Decimal))
                dt.Columns.Add("monReimbursableExpense", GetType(System.Decimal))
                dt.Columns.Add("monSalesReturn", GetType(System.Decimal))
                dt.Columns.Add("monOverpayment", GetType(System.Decimal))
                dt.Columns.Add("monCommissionAmount", GetType(System.Decimal))

                For Each item As DataListItem In dtlPayrollLiability.Items
                    If DirectCast(item.FindControl("chkSelect"), CheckBox).Checked Then
                        dr = dt.NewRow()
                        dr("numUserCntID") = CCommon.ToLong(DirectCast(item.FindControl("hdnUserCntID"), HiddenField).Value)
                        dr("numDivisionID") = CCommon.ToLong(DirectCast(item.FindControl("hdnDivisionID"), HiddenField).Value)
                        dr("monHoursWorked") = CCommon.ToDecimal(DirectCast(item.FindControl("hdnTimeAmountToPay"), HiddenField).Value)
                        dr("monAdditionalAmt") = CCommon.ToDecimal(DirectCast(item.FindControl("txtPayrollAdditionalAmount"), TextBox).Text)
                        dr("monReimbursableExpense") = CCommon.ToDecimal(DirectCast(item.FindControl("hdnReimbursableExpenseToPay"), HiddenField).Value)
                        dr("monSalesReturn") = CCommon.ToDecimal(DirectCast(item.FindControl("hdnSalesReturn"), HiddenField).Value)
                        dr("monOverpayment") = CCommon.ToDecimal(DirectCast(item.FindControl("hdnOverPayment"), HiddenField).Value)
                        dr("monCommissionAmount") = CCommon.ToDecimal(DirectCast(item.FindControl("hdnCommissionToPay"), HiddenField).Value)

                        dt.Rows.Add(dr)
                    End If
                Next

                If dt.Rows.Count > 0 Then
                    ds.Tables.Add(dt)

                    Dim objPayrollExpenses As New PayrollExpenses
                    objPayrollExpenses.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPayrollExpenses.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objPayrollExpenses.ComPayPeriodID = CCommon.ToLong(hdnComPayPeriodID.Value)
                    objPayrollExpenses.Mode = 4 'Pay remaining all
                    objPayrollExpenses.strItems = ds.GetXml()
                    objPayrollExpenses.UpdatePayrollAmountPaid()

                    LoadGrid()
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "EmptySelection", "alert('Select atleast one row.');", True)
                End If
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CommissionNotCalculated", "alert('Commission is not calculated for selected pay period. Please click Recalculate commission button.');", True)
            End If
        Catch ex As Exception
            If ex.Message.Contains("INVALID_HOURS_WORKED_PAYMENT") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidTimePayment", "alert('Invalid payment for hours worked. Recalculate commission and try again.');", True)
            ElseIf ex.Message.Contains("INVALID_REIMBURSABLE_EXPENSE_PAYMENT") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidREPayment", "alert('Invalid payment for reimbursable expense. Recalculate commission and try again.');", True)
            ElseIf ex.Message.Contains("INVALID_SALES_RETURN_PAYMENT") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidSRPayment", "alert('Invalid payment for CMs & Refunds. Recalculate commission and try again.');", True)
            ElseIf ex.Message.Contains("INVALID_OVER_PAYMENT") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidOPPayment", "alert('Invalid payment for Overpayment. Recalculate commission and try again.');", True)
            ElseIf ex.Message.Contains("INVALID_COMMISSION_PAYMENT") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvalidCPayment", "alert('Invalid payment for commission earned. Recalculate commission and try again.');", True)
            Else
                Throw
            End If
        End Try
    End Sub
End Class