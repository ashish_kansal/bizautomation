''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmProfitLoss
    Inherits BACRMPage

#Region "Variables"
    Dim mobjProfitLoss As ProfitLoss
    Dim ldecTotalBalanceAmt As Decimal
    Dim dsProfitLoss As DataSet = New DataSet
    Dim dtProfitLoss As DataTable = dsProfitLoss.Tables.Add("ProfitLoss")
    Dim dtChartAcntForProfitLoss As DataTable
    Dim ldecTotalAmt As Decimal
    Dim dv As DataView


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If GetQueryStringVal("RType") <> "" Then Session("PLType") = GetQueryStringVal("RType")


                'To Set Permission

                GetUserRightsForPage(35, 91)


                Dim lobjGeneralLedger As New GeneralLedger
                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())

                If GetQueryStringVal("FromDate") <> "" And GetQueryStringVal("ToDate") <> "" Then

                    calFrom.SelectedDate = CDate(Replace(GetQueryStringVal("FromDate"), "%27", ""))
                    calTo.SelectedDate = CDate(Replace(GetQueryStringVal("ToDate"), "%27", ""))
                Else
                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        Try
                            calFrom.SelectedDate = PersistTable(calFrom.ID)
                            calTo.SelectedDate = PersistTable(calTo.ID)
                        Catch ex As Exception
                            'Do not throw error when date format which is stored in persist table and Current date formats are different
                            calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                            calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                        End Try
                    End If
                End If

                bindUserLevelClassTracking()
                LoadGeneralDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()
                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub LoadGeneralDetails()
        Try
            If mobjProfitLoss Is Nothing Then mobjProfitLoss = New ProfitLoss

            'dtProfitLoss.Columns.Add("Type", Type.GetType("System.String"))
            'dtProfitLoss.Columns.Add("Amount", Type.GetType("System.String"))
            'dtProfitLoss.Columns.Add("numAccountId", Type.GetType("System.String"))

            mobjProfitLoss.DomainID = Session("DomainID")
            mobjProfitLoss.FromDate = calFrom.SelectedDate
            mobjProfitLoss.ToDate = CDate(calTo.SelectedDate & " 23:59:59") ''calTo.SelectedDate
            mobjProfitLoss.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)

            Dim dtProfitLoss As DataTable
            dtProfitLoss = New DataTable
            If Session("PLType") = 1 Then
                dtProfitLoss = mobjProfitLoss.GetChartAcntDetails()
                lblReportName.Text = "Profit & Loss Accounts"
                Page.Title = "Profit & Loss Accounts"
            ElseIf Session("PLType") = 2 Then
                dtProfitLoss = mobjProfitLoss.GetIncomeExpenses() 'new code
                lblReportName.Text = "Income & Expense Statement"
                Page.Title = "Income & Expense Statement"
            End If

            'dtChartAcntForProfitLoss = mobjProfitLoss.GetProfitLoss_Details()
            'LoadProfitLossGrid(dtProfitLoss)

            RepProfitLoss.DataSource = dtProfitLoss
            'RepProfitLoss.DataMember = "ProfitLoss"
            RepProfitLoss.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub LoadProfitLossGrid(ByVal dtChartAcntDetails As DataTable)
    '    Dim i As Integer
    '    Dim ldecTotalIncome As Decimal
    '    Dim ldecTotalExpenses As Decimal
    '    Dim ldecTotalOtherIncome As Decimal
    '    Dim ldecTotalOtherExpenses As Decimal
    '    Dim ldecNetOperatingIncome As Decimal
    '    Dim ldecGrossProfit As Decimal
    '    Dim ldecNetIncome As Decimal
    '    Dim ldecTotalCos As Decimal
    '    Dim ldecNetOtherIncome As Decimal

    '    Try
    '        dv = New DataView(dtChartAcntDetails)

    '        For i = 0 To dtChartAcntDetails.Rows.Count - 1
    '            If dtChartAcntDetails.Rows(i)("numAcntType") <> 825 Or dtChartAcntDetails.Rows(i)("numAcntType") <> 826 Then ldecTotalAmt = 0

    '            LoadProfitLossDetails(dtChartAcntDetails.Rows(i)("numAcntType"), dtChartAcntDetails.Rows(i)("vcData"))
    '            ''If ldecTotalAmt <> 0 Then
    '            Dim drDummyRow As DataRow = dtProfitLoss.NewRow
    '            drDummyRow(0) = "<B> Total for " & dtChartAcntDetails.Rows(i)("vcData") & "</b>"
    '            drDummyRow(1) = "<B> " & ReturnMoney(ldecTotalAmt) & "</b>"
    '            dtProfitLoss.Rows.Add(drDummyRow)
    '            If dtChartAcntDetails.Rows(i)("numAcntType") = 822 Then ldecTotalIncome = ldecTotalAmt
    '            If dtChartAcntDetails.Rows(i)("numAcntType") = 823 Then ldecTotalCos = ldecTotalAmt
    '            If dtChartAcntDetails.Rows(i)("numAcntType") = 824 Then ldecTotalExpenses = ldecTotalAmt
    '            If dtChartAcntDetails.Rows(i)("numAcntType") = 825 Then ldecTotalOtherIncome = ldecTotalAmt
    '            If dtChartAcntDetails.Rows(i)("numAcntType") = 826 Then ldecTotalOtherExpenses = ldecTotalAmt

    '            ''End If
    '            '' If i = 0 Then ldecTotalIncome = ldecTotalAmt

    '            dv.RowFilter = "numAcntType=823"
    '            If dv.Count = 1 Then
    '                If i = 1 Then
    '                    ldecGrossProfit = ldecTotalIncome - ldecTotalCos
    '                    Dim drDummyRowNetGrossProfit As DataRow = dtProfitLoss.NewRow
    '                    drDummyRowNetGrossProfit(0) = "<B> Gross Profit </b>"
    '                    drDummyRowNetGrossProfit(1) = "<B> " & ReturnMoney(ldecGrossProfit) & "</b>"
    '                    dtProfitLoss.Rows.Add(drDummyRowNetGrossProfit)
    '                End If
    '            Else
    '                dv.RowFilter = "numAcntType=822"
    '                If dv.Count = 1 Then
    '                    If i = 0 Then
    '                        ldecGrossProfit = ldecTotalIncome - ldecTotalCos
    '                        Dim drDummyRowNetGrossProfit As DataRow = dtProfitLoss.NewRow
    '                        drDummyRowNetGrossProfit(0) = "<B> Gross Profit </b>"
    '                        drDummyRowNetGrossProfit(1) = "<B> " & ReturnMoney(ldecGrossProfit) & "</b>"
    '                        dtProfitLoss.Rows.Add(drDummyRowNetGrossProfit)
    '                    End If
    '                End If
    '            End If


    '            '' To Display Net Operating Income

    '            dv.RowFilter = "numAcntType=824"
    '            If dv.Count = 1 Then
    '                dv.RowFilter = "numAcntType=823"
    '                If dv.Count = 1 Then
    '                    If i = 2 Then
    '                        ldecNetOperatingIncome = ldecGrossProfit - ldecTotalExpenses
    '                        Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                        drDummyRowNetPL(0) = "<B> Net Operating Income </b>"
    '                        drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOperatingIncome) & "</b>"
    '                        dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                    End If
    '                Else
    '                    If i = 1 Then
    '                        ldecNetOperatingIncome = ldecGrossProfit - ldecTotalExpenses
    '                        Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                        drDummyRowNetPL(0) = "<B> Net Operating Income </b>"
    '                        drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOperatingIncome) & "</b>"
    '                        dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                    End If
    '                End If
    '            End If

    '            '' To Display Net Other Income
    '            dv.RowFilter = "numAcntType=826"
    '            If dv.Count = 1 Then
    '                dv.RowFilter = "numAcntType=823"
    '                If dv.Count = 1 Then
    '                    If i = 4 Then
    '                        ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                        Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                        drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                        drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                        dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                    Else
    '                        dv.RowFilter = "numAcntType=825"
    '                        If dv.Count = 0 Then
    '                            If i = 3 Then
    '                                ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                                Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                                drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                                drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                                dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                            End If
    '                        End If
    '                    End If
    '                Else
    '                    dv.RowFilter = "numAcntType=825"
    '                    If dv.Count = 1 Then
    '                        If i = 3 Then
    '                            ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                            Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                            drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                            drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                            dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                        End If
    '                    ElseIf i = 2 Then
    '                        ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                        Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                        drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                        drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                        dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                    End If
    '                End If
    '            Else
    '                dv.RowFilter = "numAcntType=823"
    '                If dv.Count = 1 Then
    '                    If i = 3 Then
    '                        ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                        Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                        drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                        drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                        dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                        ''ElseIf i = 2 Then
    '                        ''    ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                        ''    Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                        ''    drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                        ''    drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                        ''    dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                    End If
    '                ElseIf i = 2 Then
    '                    ldecNetOtherIncome = ldecTotalOtherIncome - ldecTotalOtherExpenses
    '                    Dim drDummyRowNetPL As DataRow = dtProfitLoss.NewRow
    '                    drDummyRowNetPL(0) = "<B> Net Other Income </b>"
    '                    drDummyRowNetPL(1) = "<B> " & ReturnMoney(ldecNetOtherIncome) & "</b>"
    '                    dtProfitLoss.Rows.Add(drDummyRowNetPL)
    '                End If
    '            End If
    '        Next
    '        If dtChartAcntDetails.Rows.Count > 0 Then
    '            ldecNetIncome = ldecNetOperatingIncome + ldecNetOtherIncome
    '            Dim drDummyRowNetIncome As DataRow = dtProfitLoss.NewRow
    '            drDummyRowNetIncome(0) = "<B> Net Income </b>"
    '            drDummyRowNetIncome(1) = "<B> " & ReturnMoney(ldecNetIncome) & "</b>"
    '            dtProfitLoss.Rows.Add(drDummyRowNetIncome)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub LoadProfitLossDetails(ByVal p_AccountTypeId As Integer, ByVal p_AccountTypeDescription As String)
    '    Try
    '        Dim lobjChartofAccounts As New ChartOfAccounting
    '        lobjChartofAccounts.DomainId = Session("DomainID")
    '        Dim dvPL As New DataView(dtChartAcntForProfitLoss)          'create a dataview object
    '        dvPL.RowFilter = "numParntAcntId=" & lobjChartofAccounts.GetRootNode & " And " & "numAcntType =" & p_AccountTypeId

    '        Dim drDummyRowChartAcnt As DataRow = dtProfitLoss.NewRow
    '        drDummyRowChartAcnt(0) = "<B>" & p_AccountTypeDescription & "</b>"
    '        dtProfitLoss.Rows.Add(drDummyRowChartAcnt)
    '        LoadTreeGrid(dvPL, p_AccountTypeId)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub LoadTreeGrid(ByVal dtChartAcntDetails As DataView, ByVal p_AccountTypeId As Integer)
    '    Dim j As Integer
    '    Dim lintAcntTypeId As Integer
    '    Dim lintCount As Integer
    '    Dim ldecOpeningBalance As Decimal
    '    Try
    '        If mobjProfitLoss Is Nothing Then mobjProfitLoss = New ProfitLoss
    '        For j = 0 To dtChartAcntDetails.Count - 1
    '            mobjProfitLoss.ChartAcntId = dtChartAcntDetails.Item(j)("numAccountId")
    '            mobjProfitLoss.DomainId = Session("DomainId")
    '            ldecOpeningBalance = mobjProfitLoss.GetCurrentOpeningBalanceForGeneralLedgerDetails()

    '            If dtChartAcntDetails.Item(j)("Amount") <> 0 Or ldecOpeningBalance <> 0 Then
    '                Dim drProfitLossDetails As DataRow = dtProfitLoss.NewRow
    '                mobjProfitLoss.GetChartAcntDescription()
    '                drProfitLossDetails(0) = "<font color=Black><b>&nbsp;&nbsp;&nbsp;&nbsp; " & dtChartAcntDetails.Item(j)("AcntTypeDescription") & "</b></font>"
    '                drProfitLossDetails(1) = IIf(dtChartAcntDetails.Item(j)("Amount") = 0, "", ReturnMoney(dtChartAcntDetails.Item(j)("Amount"))) '' ReturnMoney(dtChartAcntProfitLossDetails.Rows(j)("Amount"))
    '                drProfitLossDetails(2) = dtChartAcntDetails.Item(j)("numAccountId")
    '                dtProfitLoss.Rows.Add(drProfitLossDetails)
    '                ldecTotalAmt = ldecTotalAmt + IIf(drProfitLossDetails(1) = "", 0, drProfitLossDetails(1))
    '            End If

    '            Dim dvChildnode As New DataView(dtChartAcntForProfitLoss)          'create a dataview object
    '            dvChildnode.RowFilter = "numParntAcntId=" & dtChartAcntDetails.Item(j)("numAccountId")
    '            LoadTreeGrid(dvChildnode, p_AccountTypeId)

    '            If dvChildnode.Count > 0 Then
    '                mobjProfitLoss.ChartAcntId = dtChartAcntDetails.Item(j)("numAccountId")
    '                mobjProfitLoss.DomainId = Session("DomainID")
    '                ldecOpeningBalance = mobjProfitLoss.GetCurrentOpeningBalanceForGeneralLedgerDetails()
    '                If ldecOpeningBalance <> 0 Then
    '                    Dim drDummyRow As DataRow = dtProfitLoss.NewRow
    '                    drDummyRow(0) = "<B> Total for " & dtChartAcntDetails.Item(j)("AcntTypeDescription") & "</b>"
    '                    drDummyRow(1) = "<B> " & ReturnMoney(ldecOpeningBalance) & "</b>"
    '                    drDummyRow(2) = dtChartAcntDetails.Item(j)("numAccountId")
    '                    dtProfitLoss.Rows.Add(drDummyRow)
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''Private Function GetChildCategory(ByVal ParentID As String) As DataTable
    ''    mobjProfitLoss.ParentAccountId = ParentID
    ''    mobjProfitLoss.DomainId = Session("DomainId")
    ''    GetChildCategory = mobjProfitLoss.GetChildCategory()
    ''End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then
                If IsNumeric(Money) = True Then
                    Return String.Format("{0:#,##0.00}", CDec(Money))
                Else
                    Return Money
                End If
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            Dim FromDate As Date = DateFromFormattedDate(calFrom.SelectedDate, Session("DateFormat"))
            Dim ToDate As Date = DateFromFormattedDate(calTo.SelectedDate, Session("DateFormat"))
            If FromDate > ToDate Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "script", "alert('From date must be smaller than To Date');", True)
                Exit Sub
            End If
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Private Sub RepProfitLoss_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepProfitLoss.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '            Dim hplType As HyperLink
    '            Dim lblChartAcntId As Label
    '            Dim lblType As Label
    '            lblChartAcntId = e.Item.FindControl("lblChartAcntId")
    '            hplType = e.Item.FindControl("hplType")
    '            lblType = e.Item.FindControl("lblType")
    '            If lblChartAcntId.Text <> "" Then
    '                hplType.Attributes.Add("onclick", "return OpenTransactionDetailsPage('" & "../Accounting/frmTransactions.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=frmProfitLoss&ChartAcntId=" & lblChartAcntId.Text & "')")
    '                lblType.Visible = False
    '            Else
    '                hplType.Visible = False
    '                lblType.Visible = True
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        ExportToExcel.aspTableToExcel(tblExport, Response)
    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub RepProfitLoss_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepProfitLoss.ItemCreated

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class