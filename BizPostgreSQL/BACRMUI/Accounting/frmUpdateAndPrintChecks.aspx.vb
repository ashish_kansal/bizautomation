﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting


Public Class frmUpdateAndPrintChecks
    Inherits BACRMPage

    Dim AccountID As Integer = 0
    Dim strCheckNo As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim strFilePath As String = ""
            strFilePath = CCommon.ToString(GetQueryStringVal("FilePath"))

            If Not String.IsNullOrEmpty(strFilePath) Then
                iFramePrintChecks.Attributes.Add("src", strFilePath)
            Else
                iFramePrintChecks.Visible = False
            End If

            AccountID = CCommon.ToInteger(GetQueryStringVal("AccountID"))
            strCheckNo = CCommon.ToString(GetQueryStringVal("CheckNo"))

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub btnYes_Click(sender As Object, e As System.EventArgs) Handles btnYes.Click
        Try

            'Update Check Header with bitIsPrint=1 and CheckNo
            Dim ds As New DataSet
            ds = DirectCast(Session("PrintCheckDS"), DataSet)
            If ds IsNot Nothing Then
                Dim objChecks As New Checks
                objChecks.DomainID = Session("DomainID")
                objChecks.JournalDetails = ds.GetXml()
                objChecks.UpdateCheckHeadePrintCheck()

                'Update COA Starting Check Number
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountId = AccountID
                objCOA.StartCheckNumber = strCheckNo
                objCOA.UpdateCOACheckNumber()
                ClientScript.RegisterClientScriptBlock(Me.GetType, "CloseWindow", "window.opener.document.form1.btnRefresh.click();window.close();", True)

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class