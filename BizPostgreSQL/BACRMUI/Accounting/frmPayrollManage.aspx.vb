﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Partial Public Class frmPayrollManage
    Inherits BACRMPage
    Dim lintPayPeriod As Integer
    Dim lngPayrollHeaderID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngPayrollHeaderID = CCommon.ToLong(GetQueryStringVal("PayrollID"))

            If Not IsPostBack Then
                LoadMonth()
                LoadYear()
                LoadDateDropDownList()

                objCommon.sb_FillComboFromDB(ddlPayrollStatus, 485, Session("DomainID"))
                ddlPayrollStatus.Items.Insert(0, New ListItem("-- Select One --", "0"))


                If lngPayrollHeaderID > 0 Then
                    btnSavePayrollHeader.Visible = False
                    gvPayrollList.Visible = True
                Else
                    calPayDate.SelectedDate = Date.Now

                    btnSavePayrollHeader.Visible = True
                    btnSave.Visible = False
                    gvPayrollList.Visible = False
                End If
                LoadGrid()
            End If

            btnSavePayrollHeader.Attributes.Add("onclick", "return Save()")
            btnSave.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadGrid()
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objPayrollExpenses.PayrollHeaderID = lngPayrollHeaderID

            Dim ds As DataSet = objPayrollExpenses.GetPayrollDetail()
            Dim dtHeader As DataTable = ds.Tables(0)
            Dim dtDetail As DataTable = ds.Tables(1)
            If lngPayrollHeaderID > 0 Then
                fromDate.SelectedDate = CDate(dtHeader.Rows(0)("dtFromDate"))
                toDate.SelectedDate = CDate(dtHeader.Rows(0)("dtToDate"))
            End If
            If dtHeader.Rows.Count > 0 Then
                txtPayrolllReferenceNo.Text = dtHeader.Rows(0)("numPayrolllReferenceNo")
                calPayDate.SelectedDate = dtHeader.Rows(0)("dtPaydate")
                If ddlPayrollStatus.Items.FindByValue(dtHeader.Rows(0)("numPayrollStatus")) IsNot Nothing Then
                    ddlPayrollStatus.Items.FindByValue(dtHeader.Rows(0)("numPayrollStatus")).Selected = True
                End If

                gvPayrollList.DataSource = dtDetail
                gvPayrollList.DataBind()

            Else
                Dim dtMaxPayrollRefNo As DataTable = If(ds.Tables.Count = 3, ds.Tables(2), Nothing)
                If dtMaxPayrollRefNo IsNot Nothing AndAlso dtMaxPayrollRefNo.Rows.Count > 0 Then
                    txtPayrolllReferenceNo.Text = CCommon.ToLong(dtMaxPayrollRefNo.Rows(0)("numMaxPayrolllReferenceNo"))
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadMonth()
        Try
            Dim Count As Integer
            For Count = 1 To 12
                ddlMonth.Items.Add(New ListItem(MonthName(Count), Count))
            Next
            ddlMonth.Items.FindByValue(Month(Now)).Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadYear()
        Try
            ddlYear.Items.Add(Year(Now()) - 2)
            ddlYear.Items.Add(Year(Now()) - 1)
            ddlYear.Items.Add(Year(Now()))
            ddlYear.Items.Add(Year(Now()) + 1)
            ddlYear.Items.Add(Year(Now()) + 2)
            ddlYear.Items.Add(Year(Now()) + 3)
            ddlYear.Items.Add(Year(Now()) + 4)
            ddlYear.Items.FindByValue(Year(Now())).Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            LoadDateDropDownList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Try
            LoadDateDropDownList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadDateDropDownList()
        Try
            Dim lobjPayroll As New PayrollExpenses

            Dim strStartDate As String
            Dim strEndDate As String
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim i As Integer
            dt.Columns.Add("Date", GetType(String))
            dt.Columns.Add("DateValue", GetType(String))
            lobjPayroll.DomainID = Session("DomainId")
            lintPayPeriod = lobjPayroll.GetPayPeriod()
            If lintPayPeriod = 1 Then
                strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, Day(DateAdd("d", -1, DateSerial(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value + 1, 1))))
                dr = dt.NewRow
                dr("Date") = strStartDate & " to " & strEndDate
                dr("DateValue") = strStartDate & " to " & strEndDate
                dt.Rows.Add(dr)
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 2 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 15)
                    Else
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 16)
                        strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, Day(DateAdd("d", -1, DateSerial(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value + 1, 1))))
                    End If
                    dr = dt.NewRow
                    dr("Date") = strStartDate & " to " & strEndDate
                    dr("DateValue") = strStartDate & " to " & strEndDate
                    dt.Rows.Add(dr)
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 3 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Monday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 4 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Tuesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 5 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Wednesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 6 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Thursday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 7 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Friday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 8 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Saturday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If

                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 9 Then
                For i = 0 To 3
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Sunday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 10 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Monday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 11 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Tuesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 12 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Wednesday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 13 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Thursday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 14 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Friday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 15 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Saturday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            ElseIf lintPayPeriod = 17 Then
                ddlDate.Visible = False
                ddlYear.Visible = False
                ddlMonth.Visible = False
                fromDate.Visible = True
                toDate.Visible = True
                Dim objPayrollExpenses As New PayrollExpenses
                objPayrollExpenses.DomainID = Session("DomainId")

                Dim dtDate As DataTable = objPayrollExpenses.GetCommissionDatePayPeriod()
                If dtDate.Rows.Count > 0 Then
                    fromDate.SelectedDate = CDate(dtDate.Rows(0)("dtFromDate"))
                    toDate.SelectedDate = CDate(dtDate.Rows(0)("dtToDate"))
                Else
                    fromDate.SelectedDate = Date.Now
                    toDate.SelectedDate = DateAdd(DateInterval.Day, 1, Date.Now)
                End If
                
            ElseIf lintPayPeriod = 16 Then
                For i = 0 To 1
                    If i = 0 Then
                        strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                        Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Sunday
                            strStartDate = CDate(strStartDate).AddDays(1)
                        Loop

                        strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Else
                        strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                        strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    End If
                Next
                ddlDate.DataSource = dt.DefaultView
                ddlDate.DataTextField = "DateValue"
                ddlDate.DataValueField = "Date"
                ddlDate.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSavePayrollHeader_Click(sender As Object, e As System.EventArgs) Handles btnSavePayrollHeader.Click
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            Dim sDate As Date
            Dim eDate As Date
            Dim lobjPayroll As New PayrollExpenses
            lobjPayroll.DomainID = Session("DomainId")
            lintPayPeriod = lobjPayroll.GetPayPeriod()
            If lintPayPeriod = 17 Then
                sDate = CDate(fromDate.SelectedDate)
                eDate = CDate(toDate.SelectedDate)
            Else
                sDate = CDate(ddlDate.SelectedItem.Value.Split("t")(0))
                eDate = DateAdd(DateInterval.Day, 1, CDate(ddlDate.SelectedItem.Value.Split("t")(1).Substring(1)))
            End If
            With objPayrollExpenses
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .PayrollHeaderID = lngPayrollHeaderID
                .PayrolllReferenceNo = CCommon.ToLong(txtPayrolllReferenceNo.Text)
                .dtPaydate = calPayDate.SelectedDate
                .StartDate = sDate
                .EndDate = eDate
                objPayrollExpenses.PayrollStatus = ddlPayrollStatus.SelectedValue

                lngPayrollHeaderID = .ManagePayrollHeader()
                If lngPayrollHeaderID > 0 Then
                    Response.Redirect("frmPayrollManage.aspx?PayrollID=" & lngPayrollHeaderID)
                End If

                If Not String.IsNullOrEmpty(.ExceptionMsg) Then
                    If .ExceptionMsg = "DUPLICATE" Then
                        litMessage.Text = "Entry # is already exists."
                    End If

                End If
            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvPayrollList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPayrollList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 1 Or DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 2 Then
                    e.Row.FindControl("chkSelect").Visible = False
                End If

                Dim hlEstimatedRegularHrs As HyperLink = e.Row.FindControl("hlEstimatedRegularHrs")
                Dim hlEstimatedPaidLeaveHrs As HyperLink = e.Row.FindControl("hlEstimatedPaidLeaveHrs")
                Dim hlCommPaidInvoice As HyperLink = e.Row.FindControl("hlCommPaidInvoice")
                Dim hlCommUNPaidInvoice As HyperLink = e.Row.FindControl("hlCommUNPaidInvoice")
                Dim hlEstimatedExpense As HyperLink = e.Row.FindControl("hlEstimatedExpense")
                Dim hlEstimatedReimburse As HyperLink = e.Row.FindControl("hlEstimatedReimburse")

                hlEstimatedRegularHrs.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 1, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                hlEstimatedRegularHrs.Text = DataBinder.Eval(e.Row.DataItem, "decRegularHrs")

                hlEstimatedPaidLeaveHrs.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 3, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                hlEstimatedPaidLeaveHrs.Text = DataBinder.Eval(e.Row.DataItem, "decPaidLeaveHrs")

                If Session("CommissionType") = 3 Then
                    hlCommPaidInvoice.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 8, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                    hlCommPaidInvoice.Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monCommPaidInvoice"))

                    'hlCommUNPaidInvoice.Visible = False
                    hlCommUNPaidInvoice.Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monCommUNPaidInvoice"))
                Else
                    hlCommPaidInvoice.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 4, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                    hlCommPaidInvoice.Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monCommPaidInvoice"))

                    hlCommUNPaidInvoice.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 5, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                    hlCommUNPaidInvoice.Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monCommUNPaidInvoice"))
                End If
                
                
                hlEstimatedExpense.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 6, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                hlEstimatedExpense.Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monExpense"))

                hlEstimatedReimburse.Attributes.Add("onclick", String.Format("return OpenPayrollTracking('{0}','{1}','{2}','{3}')", 7, lngPayrollHeaderID, DataBinder.Eval(e.Row.DataItem, "numUserCntID"), DataBinder.Eval(e.Row.DataItem, "numPayrollDetailID")))
                hlEstimatedReimburse.Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monReimburse"))

                If DataBinder.Eval(e.Row.DataItem, "decActualRegularHrs") > 0 OrElse _
                   DataBinder.Eval(e.Row.DataItem, "decActualOvertimeHrs") > 0 OrElse _
                   DataBinder.Eval(e.Row.DataItem, "decActualPaidLeaveHrs") > 0 OrElse _
                   ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualExpense")) > 0 OrElse _
                   ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualReimburse")) > 0 Then

                    DirectCast(e.Row.FindControl("chkSelect"), CheckBox).Checked = True
                    DirectCast(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False
                End If

                CType(e.Row.FindControl("lblActualRegularHrs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "decActualRegularHrs")
                CType(e.Row.FindControl("lblTotalRegularHrs"), Label).Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "decActualRegularHrs") * DataBinder.Eval(e.Row.DataItem, "monHourlyRate"))

                CType(e.Row.FindControl("lblEstimatedOvertimeHrs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "decOvertimeHrs")
                CType(e.Row.FindControl("lblActualOvertimeHrs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "decActualOvertimeHrs")
                CType(e.Row.FindControl("lblTotalOvertimeHrs"), Label).Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "decActualOvertimeHrs") * DataBinder.Eval(e.Row.DataItem, "monOverTimeRate"))

                CType(e.Row.FindControl("lblActualPaidLeaveHrs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "decActualPaidLeaveHrs")
                CType(e.Row.FindControl("lblTotalPaidLeaveHrs"), Label).Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "decActualPaidLeaveHrs") * DataBinder.Eval(e.Row.DataItem, "monHourlyRate"))

                CType(e.Row.FindControl("lblEstimatedTotalHrs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "decTotalHrs")
                CType(e.Row.FindControl("lblActualTotalHrs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "decActualTotalHrs")

                CType(e.Row.FindControl("lblTotalCommission"), Label).Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualCommPaidInvoice") + DataBinder.Eval(e.Row.DataItem, "monActualCommUNPaidInvoice"))
                CType(e.Row.FindControl("hdnTotalCommission"), HiddenField).Value = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualCommPaidInvoice") + DataBinder.Eval(e.Row.DataItem, "monActualCommUNPaidInvoice"))
                CType(e.Row.FindControl("hdnTotalCommissionAmount"), HiddenField).Value = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualCommPaidInvoice") + DataBinder.Eval(e.Row.DataItem, "monActualCommUNPaidInvoice"))

                CType(e.Row.FindControl("lblActualExpense"), Label).Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualExpense"))
                CType(e.Row.FindControl("lblActualReimburse"), Label).Text = ReturnMoney(DataBinder.Eval(e.Row.DataItem, "monActualReimburse"))

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try


            Dim dtDetails As New DataTable
            Dim dtrow As DataRow
            CCommon.AddColumnsToDataTable(dtDetails, "numUserCntID,decRegularHrs,decOvertimeHrs,decPaidLeaveHrs,monCommissionAmt,monExpenses,monReimbursableExpenses,monDeductions,monTotalAmt")

            If gvPayrollList.Rows.Count <= 0 Then litMessage.Text = "No records found to save." : Exit Sub
            For Each gvRow As GridViewRow In gvPayrollList.Rows
                If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked Then
                    dtrow = dtDetails.NewRow

                    dtrow.Item("numUserCntID") = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numUserCntID"))
                    dtrow.Item("decRegularHrs") = CCommon.ToDecimal(CType(gvRow.FindControl("lblActualRegularHrs"), Label).Text)
                    dtrow.Item("decOvertimeHrs") = CCommon.ToDecimal(CType(gvRow.FindControl("lblActualOvertimeHrs"), Label).Text)
                    dtrow.Item("decPaidLeaveHrs") = CCommon.ToDecimal(CType(gvRow.FindControl("lblActualPaidLeaveHrs"), Label).Text)
                    dtrow.Item("monCommissionAmt") = CCommon.ToDecimal(CType(gvRow.FindControl("hdnTotalCommissionAmount"), HiddenField).Value)
                    dtrow.Item("monExpenses") = CCommon.ToDecimal(CType(gvRow.FindControl("lblActualExpense"), Label).Text)
                    dtrow.Item("monReimbursableExpenses") = CCommon.ToDecimal(CType(gvRow.FindControl("lblActualReimburse"), Label).Text)
                    dtrow.Item("monDeductions") = CCommon.ToDecimal(CType(gvRow.FindControl("txtDeductions"), TextBox).Text)
                    dtrow.Item("monTotalAmt") = CCommon.ToDecimal(CType(gvRow.FindControl("hdnTotalAmtDue"), HiddenField).Value)

                    dtDetails.Rows.Add(dtrow)
                End If
            Next

            Dim objPayrollExpenses As New PayrollExpenses
            Dim sDate As Date
            Dim eDate As Date
            Dim lobjPayroll As New PayrollExpenses
            lobjPayroll.DomainID = Session("DomainId")
            lintPayPeriod = lobjPayroll.GetPayPeriod()
            If lintPayPeriod = 17 Then
                sDate = CDate(fromDate.SelectedDate)
                eDate = CDate(toDate.SelectedDate)
            Else
                sDate = CDate(ddlDate.SelectedItem.Value.Split("t")(0))
                eDate = DateAdd(DateInterval.Day, 1, CDate(ddlDate.SelectedItem.Value.Split("t")(1).Substring(1)))
            End If
            With objPayrollExpenses
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .PayrollHeaderID = lngPayrollHeaderID
                .PayrolllReferenceNo = CCommon.ToLong(txtPayrolllReferenceNo.Text)
                .dtPaydate = calPayDate.SelectedDate
                .StartDate = sDate
                .EndDate = eDate

                If CType(gvPayrollList.HeaderRow.FindControl("chkCommissionPaidInvoice"), CheckBox).Checked Then
                    .Mode = 1
                ElseIf CType(gvPayrollList.HeaderRow.FindControl("chkCommissionUnPaidInvoice"), CheckBox).Checked Then
                    .Mode = 2
                ElseIf CType(gvPayrollList.HeaderRow.FindControl("chkCommissionBoth"), CheckBox).Checked Then
                    .Mode = 3
                Else
                    .Mode = 4
                End If

                objPayrollExpenses.PayrollStatus = ddlPayrollStatus.SelectedValue
                Dim ds As New DataSet
                ds.Tables.Add(dtDetails)
                .strItems = ds.GetXml()
                objPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                lngPayrollHeaderID = .ManagePayrollHeader()
            End With

            'Response.Redirect("frmPayrollHistory.aspx")
            If lngPayrollHeaderID > 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Open", "opener.location.href ='../Accounting/frmPayrollHistory.aspx'; self.close();", True)
            End If

            If Not String.IsNullOrEmpty(objPayrollExpenses.ExceptionMsg) Then
                If objPayrollExpenses.ExceptionMsg = "DUPLICATE" Then
                    litMessage.Text = "Entry # is already exists."
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class