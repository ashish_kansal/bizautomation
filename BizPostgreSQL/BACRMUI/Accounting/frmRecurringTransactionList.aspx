﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringTransactionList.aspx.vb"
    Inherits=".frmRecurringTransactionList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Recurring Transaction List</title>
    <style type="text/css">
        .style1
        {
            width: 336px;
        }
        .style2
        {
            width: 699px;
        }
    </style>

    <script type="text/javascript">
        function OpenOpp(a, b, c) {
            if (c == 1) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=RecurringTransactionDetailReport&OpID=" + a;
                window.opener.location.href = str;
            }
            if (c == 2) {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:Table ID="tbl" runat="server" Width="100%" Height="350" GridLines="None" BorderColor="black"
        CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgRec" runat="server" AllowSorting="false" CssClass="dg" Width="100%"
                     AllowCustomPaging="true" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="<font color=white>Created Opportunity or Biz Doc</font>">
                            <ItemTemplate>
                                <a href="#" onclick="OpenOpp('<%# Eval("numRecTranOppID") %>','<%# Eval("numRecTranBizDocID") %>','<%# Eval("tintRecType") %>')">
                                    <%#Eval("RecurringName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TotalCostAmount" DataFormatString="{0:##,#00.00}" SortExpression="TotalCostAmount" HeaderText="<font color=white>Amount</font>">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
