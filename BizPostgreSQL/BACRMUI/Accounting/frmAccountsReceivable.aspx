<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountsReceivable.aspx.vb"
    Inherits=".frmAccountsReceivable" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
     <style>
        .divTableWithFloatingHeader{
  height: calc(100vh - 250px);
  overflow: auto
        }
        .tableFloatingHeaderOriginal th{
            position: -webkit-sticky;
  position: sticky;
  top: 0;
        }

         #dgAR {
             margin-bottom:0px;
         }

    </style>
    <title>A/R Aging Summary</title>
    <script type="text/javascript">

        function receivePayment(message) {
            $("#lblMessage").text(message);

            setTimeout(function () { $("#lblMessage").text('') }, 1000 * 60);
        }
        function OpenContact(a, b) {


            window.location.href = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&fdas89iu=098jfd&CntId=" + a

            return false;
        }

        function OpemEmail(URL) {
            window.open(URL, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenCompany(a, b, c) {
            if (b == 0) {
                 window.open('../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&DivID=' + a,'_blank');
               // window.location.href = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&DivID=" + a

            }

            else if (b == 1) {
                window.open('../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&DivID=' + a,'_blank');
               // window.location.href = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&DivID=" + a

            }
            else if (b == 2) {
                  window.open('../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&klds+7kldf=fjk-las&DivId=' + a,'_blank');
               // window.location.href = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AccountsReceivable&klds+7kldf=fjk-las&DivId=" + a

            }

            return false;
        }
        function OpemDetails(a, b, c) {
            if (a == '' || b == '' || c == '') {
                return false;
            }

            var basedOn = 1;
            if ($("[id$=rbInvoiceDate]").is(":checked")) {
                basedOn = 2;
            }
            var dateFormat = '<%= Session("DateFormat")%>';
            var fromDate = $("[id$=calFrom_txtDate]").val();
            var toDate = $("[id$=calTo_txtDate]").val();
            if (dateFormat != "") {
                fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
                toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

                fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
                toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
            }

            window.open('frmAccountsReceivableDetails.aspx?DomainId=' + parseInt(a) + '&DivisionID=' + b + '&Flag=' + c + '&accountClass=' + $("[id$=ddlUserLevelClass]").val() + "&fromDate=" + fromDate + "&toDate=" + toDate + "&BasedOn=" + basedOn, '', 'toolbar=no,titlebar=no,location=0,top=300,left=220,width=1000,height=300,scrollbars=yes,resizable=no');
            return false;
        }
        function reDirectPage(a) {
            window.location.href = a;
        }

		function OpemAccountInvoice() {
            var h = screen.height;
            var w = screen.width;

            var basedOn = 1;
            if ($("[id$=rbInvoiceDate]").is(":checked")) {
                basedOn = 2;
            }

            var dateFormat = '<%= Session("DateFormat")%>';
		    var fromDate = $("[id$=calFrom_txtDate]").val();
		    var toDate = $("[id$=calTo_txtDate]").val();
		    if (dateFormat != "") {
		        fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
		        toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

		        fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
		        toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
		    }

		    window.open('frmAccountsReceivableInvoice.aspx?accountClass=' + $("[id$=ddlUserLevelClass]").val() + "&fromDate=" + fromDate + "&toDate=" + toDate + "&BasedOn=" + basedOn, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenCustomerStatement(a) {
            window.open('frmCustomerStatement.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function pageLoaded() {
            var SelectAllCheckBox = SelectAll;
            SelectAll = function (headerCheckBox, ItemCheckboxClass) {
                $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                    $(this).prop('checked', $("#dgAR tr input[id = 'chkAll']").is(":checked"))
                });
            }

            $("table#dgAR").wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");


            var originalHeaderRow = $("table#dgAR tr:first")
            originalHeaderRow.before(originalHeaderRow.clone());
            var clonedHeaderRow = $("table#dgAR tr:first")

            clonedHeaderRow.addClass("tableFloatingHeader");
            clonedHeaderRow.css("position", "absolute");
            clonedHeaderRow.css("top", "0px");
            clonedHeaderRow.css("left", $("table#dgAR").css("margin-left"));
            clonedHeaderRow.css("display", "none");

            originalHeaderRow.addClass("tableFloatingHeaderOriginal");

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);


            var originalFooterRow = $("table#dgAR tr:last");
            var clonedFooterRow = originalFooterRow.clone();
            originalFooterRow.before(clonedFooterRow);

            clonedFooterRow.addClass("tableFloatingFooter");
            clonedFooterRow.css("position", "absolute");
            clonedFooterRow.css("display", "none");
            clonedFooterRow.css("left", $("table#dgAR").css("margin-left"));

            originalFooterRow.addClass("tableFloatingFooterOriginal");
            originalFooterRow.css("visibility", "hidden");

            UpdateTableFooters();
            $(window).scroll(UpdateTableFooters);
            $(window).resize(UpdateTableFooters);
            $("div.divTableWithFloatingHeader").scroll(UpdateTableFooters);

            if ($('div.divTableWithFloatingHeader').get(0).scrollHeight <= $('div.divTableWithFloatingHeader').height()) {
                $(".tableFloatingFooterOriginal").css("visibility", "visible");
                $(".tableFloatingFooter").css("display", "none");
            }

            $('div.divTableWithFloatingHeader').on('scroll', function () {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    $(".tableFloatingFooterOriginal").css("visibility", "visible");
                    $(".tableFloatingFooter").css("display", "none");
                }
            })
        }


        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function () {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();
                if ((scrollTop > offset.top) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("display", "block");
                    floatingHeaderRow.css("top", Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()) + "px");

                    // Copy cell widths from original header
                    $("td", floatingHeaderRow).each(function (index) {
                        var cellWidth = $("td", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("display", "none");
                    floatingHeaderRow.css("top", "0px");
                }
            });
        }

        //Commented: As Sticky Footer Logic Is Not Working As Expected
        function UpdateTableFooters() {
            if ($("div.divTableWithFloatingHeader").scrollTop() + $("div.divTableWithFloatingHeader").outerHeight() - $(".tableFloatingFooterOriginal").outerHeight() < ($(".tableFloatingFooterOriginal").offset().top - $("div.divTableWithFloatingHeader").offset().top + $("div.divTableWithFloatingHeader").scrollTop())) {
                $("div.divTableWithFloatingHeader").each(function () {
                    var originalFooterRow = $(".tableFloatingFooterOriginal", this);
                    var floatingFooterRow = $(".tableFloatingFooter", this);
                    var offset = $(this).offset();
                    var scrollTop = $(window).scrollTop();

                    floatingFooterRow.css("display", "block");

                    var top = $("div.divTableWithFloatingHeader").scrollTop() + $("div.divTableWithFloatingHeader").outerHeight() - $(".tableFloatingFooterOriginal").outerHeight();

                    if ($("table#dgAR").outerWidth() > $("div.divTableWithFloatingHeader").outerWidth()) {
                        top = top - 15;
                    }

                    floatingFooterRow.css("top", top + "px");

                    $("td", floatingFooterRow).each(function (index) {
                        var cellWidth = $("td", originalFooterRow).eq(index).outerWidth();
                        $(this).css('width', cellWidth);
                    });
                });
            }
        }

        //function UpdateTableFootersOld() {
        //    $(".tableFloatingFooter").css("top",($("div.divTableWithFloatingHeader").offset().top + $("div.divTableWithFloatingHeader").height() - $(".tableFloatingFooterOriginal").height())  + "px");
        //    $(".tableFloatingFooter").css("left", $("div.divTableWithFloatingHeader").offset().left + "px");

        //    $(".tableFloatingFooter td").each(function (index) {
        //        var cellWidth = $(".tableFloatingFooterOriginal td").eq(index).outerWidth();
        //        $(this).css('width', cellWidth);
        //    });
        //}

        function ConfirmMail() {
            var intCnt;
            intCnt = 0;
            $("#dgAR tr").not(':first').each(function () {
                chk = $(this).find("[id*='chkSelect']");
                if ($(chk).is(':checked')) {
                    intCnt = intCnt + 1
                }
            });

            if (intCnt > 0) {
                return true;
                //if (confirm("You're about to broadcast an indivudual PDF A/R Statement to within the following email template Customer Statement to the A/R customers selected. Are you sure you want to proceed ?") == true) {
                    
                //}
                //else {
                //    return false;
                //}
            }
            else {
                alert("Please select atleast one record to send mail.");
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group" runat="server" id="pnlAccountingClass">
                        <label>Class</label>
                        <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Based On:</label>
                        <asp:RadioButton runat="server" ID="rbDueDate" Text="Due Date" GroupName="BasedOn" Checked="true" AutoPostBack="true" OnCheckedChanged="rbDueDate_CheckedChanged" />
                        <asp:RadioButton runat="server" ID="rbInvoiceDate" Text="Invoice Date" GroupName="BasedOn" AutoPostBack="true" OnCheckedChanged="rbInvoiceDate_CheckedChanged" />
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>
                            From
                        </label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" Width="95" />
                    </div>
                    <div class="form-group">
                        <label>
                            To
                        </label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" Width="95" />
                    </div>
                    <div class="form-group">
                        <label>
                            Customer/Vendor</label>
                        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                            ClientIDMode="Static"
                            ShowMoreResultsBox="true"
                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
                    </div>
                    <asp:LinkButton ID="btnGo" runat="server" CssClass="btn btn-primary" OnClientClick="return Go();"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    <div class="form-group">
                        <asp:UpdatePanel ID="updateTopBtnEvnt" runat="server">
                            <ContentTemplate>
                                &nbsp;<asp:LinkButton runat="server" ID="ibExportExcel" CssClass="btn btn-primary" AlternateText="Export to Excel"
                                    ToolTip="Export to Excel"><i class="fa fa-file-excel-o"></i></asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="ibExportExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group">
                        <asp:LinkButton runat="server" ID="ibSendEmail" OnClientClick="return ConfirmMail();" CssClass="btn btn-primary" AlternateText="Email Customer Statement" ToolTip="Email Customer Statement"><i class="fa fa-envelope-o"></i></asp:LinkButton>
                        <a href="#" onclick="javascript:OpemAccountInvoice();" class="hyperlink btn btn-default"><i class="fa fa-table"></i>&nbsp;&nbsp;A/R Aging Detail Report </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12" style="margin-top: 10px; margin-bottom: 5px;">
        <div class="pull-right">
            <div class="form-inline">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <asp:Label ID="lblMessage" runat="server" ForeColor="Green" Font-Bold="true"> </asp:Label>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    A/R Aging&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmaccountsreceivable.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel ID="updatePanelPastDue" runat="server">
        <ContentTemplate>
            <asp:CheckBox ID="chkHidePastDue" runat="server" Text="Hide Past Due column" AutoPostBack="true" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="chkHidePastDue" />
        </Triggers>
    </asp:UpdatePanel>
    &nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblGrandTotal" runat="server" style="padding-top: 2px;" Text="Grand Total our customers owe us:"> </asp:Label>
    <asp:Label ID="lblGrandTotalAmt" Style="border-radius: 0px; padding-top: 2px;" runat="server" Text="" Font-Bold="true"> </asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgAR" runat="server" CssClass="table table-bordered" Width="100%" AutoGenerateColumns="False"
                    AllowSorting="true" ShowFooter="true" FooterStyle-Font-Bold="true" UseAccessibleHeader="true" FooterStyle-BackColor="#ebebeb">
                    <Columns>
                        <asp:BoundColumn DataField="numDivisionId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Customer">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplCompany" runat="server" Target="_blank" Text='<%# Eval("vcCompanyName") %>'>
                                </asp:HyperLink>
                                <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("vcCompanyName") %>'></asp:Label><%--<br />--%>
                                <asp:Label runat="server" Style="display: none" ID="lblCustPhone" Text='<%# Eval("vcCustPhone") %>'></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnDivisionId" Value='<%# Eval("numDivisionId")%>' />
                                <asp:HiddenField runat="server" ID="hdnContactId" Value='<%# Eval("numContactId") %>' />
                                <asp:HiddenField runat="server" ID="hdnCompanyName" Value='<%# Eval("vcCompanyName") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                Total
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Current" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0+30');">
                                    <%#IIf(Integer.Parse(Eval("intCurrentDaysCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numCurrentDays")) + "</b></font>", ReturnMoney(Eval("numCurrentDays")))%>
                                </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numCurrentDaysPaid"))%></font>
                                <asp:HiddenField ID="hdnCurrentDays" runat="server" Value='<%# Eval("numCurrentDays")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalCurrentDays" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="1-30" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0-30');">
                                    <%#IIf(Integer.Parse(Eval("intThirtyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numThirtyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numThirtyDaysOverDue")))%>
                                </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numThirtyDaysOverDuePaid"))%></font>
                                <asp:HiddenField ID="hdn1To30" runat="server" Value='<%# Eval("numThirtyDaysOverDue")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalThirtyDaysOverDue" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="31-60" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','30-60');">
                                    <%#IIf(Integer.Parse(Eval("intSixtyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numSixtyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numSixtyDaysOverDue")))%>
                                </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numSixtyDaysOverDuePaid"))%></font>
                                <asp:HiddenField ID="hdn31To60" runat="server" Value='<%# Eval("numSixtyDaysOverDue")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalSixtyDaysOverDue" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="61-90" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','60-90');">
                                    <%#IIf(Integer.Parse(Eval("intNinetyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numNinetyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numNinetyDaysOverDue")))%>
                                </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numNinetyDaysOverDuePaid"))%></font>
                                <asp:HiddenField ID="hdn61To90" runat="server" Value='<%# Eval("numNinetyDaysOverDue")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalNinetyDaysOverDue" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Over 90" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','90-');">
                                    <%#IIf(Integer.Parse(Eval("intOverNinetyDaysOverDueCount")) > 1, "<font color='purple'><b>" + ReturnMoney(Eval("numOverNinetyDaysOverDue")) + "</b></font>", ReturnMoney(Eval("numOverNinetyDaysOverDue")))%>
                                </a>&nbsp;<font color='green'><%# ReturnMoney(Eval("numOverNinetyDaysOverDuePaid"))%></font>
                                <asp:HiddenField ID="hdnOver90" runat="server" Value='<%# Eval("numOverNinetyDaysOverDue")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalOverNinetyDaysOverDue" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Credits" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <font color='green'><%# ReturnMoney(Eval("monUnAppliedAmount"))%></font>
                                <asp:HiddenField ID="hdnCredits" runat="server" Value='<%# Eval("monUnAppliedAmount")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalUnAppliedAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Total" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpemDetails('<%# Eval("numDomainID") %>','<%# Eval("numDivisionID") %>','0');">
                                    <%#ReturnMoney(Eval("numTotal"))%>
                                </a>
                                <asp:HiddenField ID="hdnTotal" runat="server" Value='<%# Eval("numTotal")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgCustomerStatement" src="../images/GLReport.png"
                                    ToolTip="Customer Statement" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="30" />
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll','chkSelect');" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hdnHTML" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
