<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBankReconcileStatement.aspx.vb"
    Inherits=".frmBankReconcileStatement" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function PrintIt() {
            w = window.open();
            w.document.write('<' + 'html' + '><' + 'head' + '>');
            w.document.write('<link href="../CSS/NewUiStyle.css" rel="stylesheet" type="text/css" />');
            w.document.write('<' + '/' + 'head' + '><' + 'body' + '>');
            w.document.write($('.reportPrint').html());
            w.document.write('<' + '/' + 'body' + '><' + '/' + 'html' + '>');
            w.print();
            w.close();

            return false;
        }

    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .hs {
            border: 1px solid #e4e4e4;
            border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            text-align: center;
            font-weight: bold;
        }

        .reference {
            word-break: break-all;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="GridSettingPopup" runat="server"
    ClientIDMode="Static">
    <asp:ImageButton runat="server" ID="ibtnPrint" AlternateText="Print" ToolTip="Print"
        ImageUrl="~/images/Print.png" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Bank Reconciliation Report
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="reportPrint">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-widget widget-user-2">
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked" style="border: 1px solid #f4f4f4;">
                            <li><a href="#">Account <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblAccount" runat="server"> </asp:Label></label>
                                <asp:HiddenField ID="hfAccountID" runat="server" />
                            </span></a></li>
                            <li><a href="#">Statement Date <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblStatementDate" runat="server"> </asp:Label></label>
                            </span></a></li>
                            <li><a href="#">Reconciled On <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblReconciledOn" runat="server"> </asp:Label></label></span></a></li>
                            <li><a href="#">Reconciled by <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblReconciledby" runat="server"> </asp:Label></label>
                            </span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-gray-active" style="padding: 3px 11px 1px;">
                        <h5>
                            <label>Summary</label>
                        </h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#">Beginning Balance <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblBeginningBalance" runat="server" Text="0.00"> </asp:Label></label></span></a></li>
                            <li><a href="#">Service Charge <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblServiceChargeAmt" runat="server" Text="0.00"> </asp:Label></label></span></a></li>
                            <li><a href="#">Interest Earned <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblInterestEarnedAmt" runat="server" Text="0.00"> </asp:Label></label></span></a></li>
                            <li><a href="#">Checks and Payments <span class="pull-right">
                                <label>
                                    <asp:Label ID="lblPayment" runat="server" Text="0.00"> </asp:Label></label></span></a></li>
                            <li><a href="#">Deposits and Other Credits<span class="pull-right"><label><asp:Label ID="lblDeposit" runat="server" Text="0.00"> </asp:Label></label></span> </a></li>
                            <li><a href="#">Statement Ending Balance<span class="pull-right"><label><asp:Label ID="lblEndingBalAmt" runat="server" Text="0.00"> </asp:Label></label></span> </a></li>
                            <li><a href="#">Cleared Balance<span class="pull-right"><label><asp:Label ID="lblClearedBalAmt" runat="server" Text="0.00"> </asp:Label></label></span> </a></li>
                            <li><a href="#">Difference<span class="pull-right"><label><asp:Label ID="lblDifferenceAmt" runat="server" Text="0.00"> </asp:Label></label></span> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <br />
                <h3 style="margin: 0px"><b>Details</b></h3>
                <br />
                <asp:DataGrid ID="dgJournalEntry" runat="server" CssClass="table table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundColumn DataField="numTransactionId" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="JournalId" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Entry Date" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:Label ID="lblJDate" runat="server" Text='<%# ReturnDate(Eval("EntryDate")) %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Memo" ItemStyle-Width="20%" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblMemo" runat="server" Text='<%# Eval("Memo") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TransactionType" HeaderText="Type" ItemStyle-Width="5%" ItemStyle-Wrap="true"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Reference #" ItemStyle-Width="20%" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <div style="word-break:break-all;">
                                    <%# Eval("vcReference")%>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Organization" ItemStyle-Width="5%" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <%# ReturnMoney(Eval("CompanyName"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Deposit (Biz)" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <%# ReturnMoney(Eval("Deposit")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Payment (Biz)" ItemStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <%#ReturnMoney(Eval("Payment"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
