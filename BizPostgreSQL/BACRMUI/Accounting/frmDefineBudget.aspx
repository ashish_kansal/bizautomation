<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDefineBudget.aspx.vb"
    Inherits=".frmDefineBudget" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Define/Edit Budget</title>

    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        function NodeClick(treeId, nodeId) {
            val = igtree_getNodeById(nodeId).getParent();
            //alert(val)
            if (val != null) {
                val.setChecked();
            }

        }

        function SaveClose() {
            var h = UltraWebTree1_InitializeTree()
            var ob = h.substring(1, h.length);
            opener.LoadTreeViewDetails(ob);
            self.close();
        }
        var id = '';
        function UltraWebTree1_InitializeTree() {

            var rootArray = igtree_getTreeById('ultratreeChartAcnt').getNodes();
            var val;

            //alert(rootArray.length);
            for (var i = 0; i < rootArray.length; i++) {
                var y = traverseNode(rootArray[i]);
                return y
            }


        }


        function traverseNode(node) {

            if (node.hasChildren()) {
                var childArray = node.getChildNodes();
                for (var i = 0; i < childArray.length; i++) {
                    if (igtree_getNodeById(childArray[i].Id).getChecked() == true) {
                        var obj;
                        obj = igtree_getNodeById(childArray[i].Id).getParent()
                        id = id + ',' + igtree_getNodeById(childArray[i].Id).getTag() + ' ' + obj.getTag();

                    }
                    traverseNode(childArray[i]);
                }
            }
            return id;
            //node.getElement().title = node.getText();
        }
    
    </script>

</head>
<body>
    <form id="form1" runat="server" method="post">
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Define Accounts&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="button"
                    Width="75"></asp:Button>&nbsp;
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell Height="10px"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%" height="400px" cellpadding="0" cellspacing="0">
                    <tr valign="top">
                        <td class="TabArea">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr valign="top" width="100%">
                                    <td>
                                        <%--ignav:UltraWebTree id="ultratreeChartAcnt" runat="server" Font-Names="Arial" Font-Bold="true"
                                            Font-Size="11px" Cursor="Default" BorderStyle="None" BorderWidth="0px" WebTreeTarget="ClassicTree"
                                            CheckBoxes="true">
                                            <clientsideevents nodechecked="NodeClick" />
                                            <levels>
                                         </levels>
                                        </ignav:UltraWebTree>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table id="table3" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td colspan="1" class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Button ID="btnTreeView" runat="server" Style="display: none"></asp:Button>
    </form>
</body>
</html>
