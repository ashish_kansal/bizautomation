﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmPrintChecksList.aspx.vb" Inherits=".frmPrintChecksList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Print Checks</title>
    <script language="javascript" type="text/javascript">
        function OpenWriteCheck(a) {
            window.location.href = "../Accounting/frmWriteCheck.aspx?CheckHeaderID=" + a;
            return false;
        }

        function OpenBillPayment(a) {
            window.location.href = "../Accounting/frmVendorPayment.aspx?BillPaymentID=" + a;
            return false;
        }

        function OpenOpp(a, b) {

            var str;
            str = "../opportunity/frmReturnDetail.aspx?type=" + b + "&ReturnID=" + a;
            window.location.href = str;
            return false;
        }

        function OpenPrintCheck(FilePath, AccountID, CheckNo) {
            window.open('../Accounting/frmUpdateAndPrintChecks.aspx?FilePath=' + FilePath + "&AccountID=" + AccountID + "&CheckNo=" + CheckNo, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenCheckTemplate() {
            window.open('../Accounting/PrintCheckEditor.aspx', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=1024,height=600,scrollbars=yes,resizable=yes');
            return false;
        }

        function Save() {
            if (document.getElementById('txtCheckNo').value == 0) {
                alert("Enter Starting Check #");
                document.getElementById('txtCheckNo').focus();
                return false;
            }

            var i = 0;

            $("#gvChecksList tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    i = i + 1;
                }
            });

            if (i == 0) {
                alert('Please select atleast one record!!');
                return false;
            }

            return true;
        }

        function DownloadFile(a) {
            window.open(a, 'Download'); //bug id 2024
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                <asp:LinkButton ID="btnAllowCheckNO" runat="server" Visible="false">Yes</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:DropDownList CssClass="form-control" ID="ddlChecksType" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="0">All Checks</asp:ListItem>
                    <asp:ListItem Value="1">Regular Checks</asp:ListItem>
                    <asp:ListItem Value="8">Bill Payments Checks</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnPrint" runat="server" CssClass="button btn btn-primary" Text="Print"></asp:Button>
                                <asp:Button ID="btnEditCheckTemplate" runat="server" OnClientClick="OpenCheckTemplate();" UseSubmitBehavior="false" CssClass="button btn btn-primary" Text="Change Check Preferences"></asp:Button>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnPrint" />
                                <asp:PostBackTrigger ControlID="btnEditCheckTemplate" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Print Checks&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmprintcheckslist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <div class="form-inline">
        <label>Bank Account:</label>
        <asp:DropDownList CssClass="signup form-control" ID="ddlDepositTo" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;&nbsp;&nbsp;
        <label>Balance :</label>
        <asp:Label ID="lblBalanceAmount" runat="server" ForeColor="Green" Text="0.00"></asp:Label>
        &nbsp;&nbsp;&nbsp;
        <label>Starting Check #:</label>
        <asp:TextBox ID="txtCheckNo" runat="server" CssClass="signup form-control" MaxLength="20" AutoComplete="OFF"></asp:TextBox>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvChecksList" runat="server" CssClass="table table-striped table-bordered" BorderWidth="0" AutoGenerateColumns="False"
                    Style="margin-right: 0px" Width="100%" DataKeyNames="numCheckHeaderID" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-Width="20px">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Width="20px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll', 'chkSelect');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <%# ReturnDate(Eval("dtCheckDate"))%>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit"><%#DataBinder.Eval(Container.DataItem, "vcReferenceType")%></asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payee">
                            <ItemTemplate>
                                <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("vcCompanyName")%>'></asp:Label>
                                <asp:HiddenField ID="hdnDivisionID" runat="server" Value='<%#Eval("numDivisionID")%>'></asp:HiddenField>
                                <asp:Label runat="server" ID="EmployerName" Visible="false" Text='<%# Eval("EmployerName") %>' />
                                <asp:Label runat="server" ID="vcRefOrderNo" Visible="false" Text='<%# Eval("vcRefOrderNo") %>' />
                                <asp:Label ID="lblBizDocName" runat="server" Text='<%#Eval("vcBizDoc")%>'></asp:Label>
                                <asp:HiddenField ID="hdnReferenceType" runat="server" Value='<%#Eval("tintreferenceType")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdnAccountID" runat="server" Value='<%#Eval("numChartAcntId")%>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#ReturnMoney(Eval("monAmount"))%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No checks found to print
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:Button ID="btnRefresh" runat="server" Text="" CssClass="button" Style="display: none" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
