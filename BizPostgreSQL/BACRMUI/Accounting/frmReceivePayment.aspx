<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmReceivePayment.aspx.vb"
    Inherits=".frmMakePayment" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Receive Payment</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            if (document.form1.ddlDepositTo.value == 0) {
                alert("Please Select Deposit To");
                document.form1.ddlDepositTo.focus();
                return false;
            }
            var s = 0;
            $(".dg tr").each(function () {

                if ($(this).find(".chkSelected input[type = 'checkbox']").is(':checked')) {
                    s = 1
                }

            });

            if (s == 0) {
                alert('You must select an BizDocs for Deposit');
                return false;
            }
            else {
                return true;
            }
        }
        function CalculateTotalDeposit() {
            var DepositTotal = 0;
            $(".dg tr").each(function () {

                if ($(this).find(".chkSelected input[type = 'checkbox']").is(':checked')) {

                    DepositTotal = parseFloat(DepositTotal) + parseFloat($(this).find(".lblAmount").text());
                }

            });
            //            console.log(DepositTotal);

            $("#lblDepositTotalAmount").text(formatCurrency(DepositTotal));
            $("#DepositTotalAmt").val(DepositTotal);

        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function SelectAllchk() {
            SelectAll('SelectAllCheckBox', 'chkSelected');
            CalculateTotalDeposit();
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="view-cases">
        </div>
        <div class="right-input">
            <table align="right">
                <tr>
                    <td>
                        <label>
                            <asp:Label ID="lblDepositDate" runat="server" Text="Deposit Date"></asp:Label></label>
                    </td>
                    <td>
                        <BizCalendar:Calendar ID="calReceivePayment" runat="server" />
                    </td>
                    <td>
                        <label>
                            <asp:Label ID="lblLeads" runat="server">Deposit To </asp:Label></label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepositTo" runat="server" Width="250px" CssClass="signup"
                            AutoPostBack="true" onChange="javascript: CalculateTotalDeposit();">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <label>
                            <asp:Label ID="lblBalance" runat="server" Text=""> </asp:Label></label><asp:Label
                                ID="lblBalanceAmount" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="leftBorder">
                        <label>
                            <asp:Label ID="lblDepositTotal" runat="server" Text=""> </asp:Label></label>
                    </td>
                    <td>
                        <asp:Label ID="lblDepositTotalAmount" runat="server" Text="0.00"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnDepositSave" runat="server" CssClass="button" Text="Deposit selected payments & Save">
                        </asp:Button>
                    </td>
                    <td valign="bottom">
                        <a href="#" class="help">&nbsp;</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Deposit Payments Received
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dgReceivePayment" runat="server" CssClass="dg" Width="100%"
        AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:TemplateColumn>
                <HeaderStyle HorizontalAlign="Left" />
                <HeaderTemplate>
                    <asp:CheckBox ID="SelectAllCheckBox" runat="server" ToolTip="Select All" Text="Select All"
                        onclick="javascript: SelectAllchk();"></asp:CheckBox>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelected" CssClass="chkSelected" runat="server" onclick="javascript: CalculateTotalDeposit();">
                    </asp:CheckBox>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn Visible="False" DataField="numBizDocsPaymentDetailsId"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numBizDocsPaymentDetId"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numBizDocsId"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numDivisionId"></asp:BoundColumn>
            <asp:BoundColumn Visible="false" DataField="numCurrencyID"></asp:BoundColumn>
            <asp:BoundColumn Visible="false" DataField="fltExchangeRate"></asp:BoundColumn>
            <asp:BoundColumn Visible="false" DataField="PaymentMethod"></asp:BoundColumn>
            <asp:BoundColumn Visible="false" DataField="numCardTypeID"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Invoice ID</font>">
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numBizdocsId") %>');">
                        <%# Eval("vcBizDocID")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn Visible="false" HeaderText="Amount">
                <ItemTemplate>
                    <asp:Label ID="lblOrgAmount" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.Amount")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Amount">
                <ItemTemplate>
                    <asp:Label ID="lblCurrency" runat="server" Text='<%# Session("Currency") %>'></asp:Label>
                    <asp:Label ID="lblAmount" CssClass="lblAmount" runat="server" Text='<%# String.Format("{0:#,##0.00}",Eval("ConAmt")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="PaymentMethod" HeaderText="Payment Method"></asp:BoundColumn>
            <asp:BoundColumn DataField="CreatedBy" HeaderText="Entered By, on"></asp:BoundColumn>
            <asp:BoundColumn DataField="Memo" HeaderText="Memo"></asp:BoundColumn>
            <asp:BoundColumn DataField="Reference" HeaderText="Reference #"></asp:BoundColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="btnDeleteAction" runat="server" CssClass="button Delete" Text="X"
                        CommandName="Delete"></asp:Button>
                    <asp:LinkButton ID="lnkDeleteAction" runat="server" Visible="false">
													  <font color="#730000">*</font></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <asp:HiddenField ID="DepositTotalAmt" runat="server" />
</asp:Content>
--%>