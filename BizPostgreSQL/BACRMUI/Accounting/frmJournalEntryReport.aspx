﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmJournalEntryReport.aspx.vb"
    Inherits=".frmJournalEntryReport" MasterPageFile="~/common/GridMasterRegular.Master"
    EnableViewState="true" %>


<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Journal Entries Report</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>

    <script language="javascript" type="text/javascript">

        function OpenJournalDetailsPage(url) {
            window.location.href = url;
            return false;
        }
        function reDirect(a) {
            document.location.href = a;
            return false;
        }
        function OpenTimeExpense(url) {

            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, "BizInvoiceGL", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }

        function OpenBill(a) {
            window.open('../Accounting/frmAddBill.aspx?BillID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }

        function Go() {
            //if (document.form1.ddlType.value == 0) {
            //    alert("Please Select Account Type");
            //    document.form1.ddlType.focus();
            //    return false;
            //}
            //if (document.form1.hdnAccounts.value.length == 0 && document.form1.ddlCOA.value == 0) {
            //    alert("Please Select a Accounts");
            //    document.form1.ddlCOA.focus();
            //    return false;
            //}
        }

        $(document).ready(function () {
            var pageIndex = 0;
            var pageCount;
            var AccountList;
            var LoadComplete = 0;
            CallWait = 0;

            List1 = { items: [{ index: "-1", AccountID: "0", ItemCode: "0", PrevBalance: "0", Pageindex: "0", PageIndexCalled: "0", Loaded: "0", LoadComplete: '0' }] };


            FillDropDown();
            $('.ReconStatus').change(function () {
                var TransactionId = $(this).parent().find('#lblTransactionId').text();

                if (confirm('Are you sure want to change Bank Recon Status?')) {
                    //console.log(TransactionId);
                    //console.log($(this).val());
                    $(this).parent().find('#lblReconStatus').text($(this).val());
                    $.ajax({
                        type: "POST",
                        url: "../common/Common.asmx/UpdateBankReconStatus",
                        data: "{TransactionId:'" + TransactionId + "',Status:'" + $(this).val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            // Insert the returned HTML into the <div>.
                            //UpdateMsgStatus(msg.d);
                        }
                    });
                }
                else {
                    var ReconStatus = $(this).parent().find('#lblReconStatus').text();
                    $(this).val(ReconStatus);
                }
            });
            //var hasVScroll = document.body.scrollHeight < document.body.clientHeight;
            //alert('document.body.scrollHeight : ' + document.body.scrollHeight);
            //alert('document.body.clientHeight : ' + document.body.clientHeight);

            //alert('has Scroll : ' + hasVScroll);
            //if ($(window).hasScrollBar() == false) {

            //    GetAccountTrans();
            //}
            //alert($(window).has(scroll()).html());

            $(window).scroll(function () {
                //var DocHeight = $(document).height();
                //var winHeight = $(window).height();
                //var NewScrlllHeight = DocHeight - winHeight - 200;
                //if (ScrollTop >= NewScrlllHeight) {
                //console.log($(window).scrollTop());
                //console.log($(document).height() - 50);
                if ($(window).scrollTop() >= $(document).height() - $(window).height() - 500) {
                    GetAccountTrans();
                }
                else if ($(window).scrollTop() == 0) {
                    GetAccountTrans();
                }
            });

            //verticalScrollPresent: function()
            //{
            //    return (document.documentElement.scrollHeight !== document.documentElement.clientHeight);
            //}


            //$(".Delete").click(function (event) {
            //    var ReconStatus = $(this).parent().find('#lblReconStatus').text();
            //    if (ReconStatus == 'R') {
            //        if (confirm('This transaction is reconciled, Are you sure, you want to delete the selected transaction?')) {
            //            return true;
            //        }
            //        else {
            //            event.preventDefault();
            //            return false;
            //        }
            //    }
            //    else {
            //        if (confirm('Are you sure, you want to delete the selected transaction?')) {
            //            return true;
            //        }
            //        else {
            //            event.preventDefault();
            //            return false;
            //        }
            //    }
            //});

            $("#ddlReconStatusFilter").change(function () {
                $("#hdnReconStatus").val($("#ddlReconStatusFilter").val());
                $("#btnGo").click();
            });


            //var strAccountList = $("#hdnAccounts").val();
            PushAccountList();
            $("[id*=RepeaterTable] tr").eq(1).hide();
        });

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }

        function FillDropDown() {
            var i = 0;
            var RepJournalEntry = $('#RepeaterTable tr');
            RepJournalEntry.each(function () {
                //alert('enters each Repeater Row');
                $(this).find('#ddlReconStatus').each(function () {
                    $(this).html($('#ddlReconStatus1').html());
                    var ReconStatus = $(this).parent().find('#lblReconStatus').text();
                    $(this).val(ReconStatus);
                    i += 1;
                });
            });
            // alert('Total Count : ' + i);
        }

        function PushAccountList(strAccountList) {
            //console.log('Populates Account Id list. \r\n');
            var pageIndex1 = 0;

            var AccIds = $("#hdnAccounts").val();
            var ItemCode = $("#hdnItemCode").val();
            //console.log('Account IDs : |' + AccIds + '|. \r\n');
            //console.log('ItemCode  : |' + ItemCode + '|. \r\n');

            // var AccIds = '';
            //if ($("#ddlCOA").val() == 1) {
            //    AccIds = $("#hdnAccounts").val();
            //}
            //else {
            //    AccIds = $("#ddlCOA").val();
            //}
            //  console.log('Account IDs : |' + AccIds + '|. \r\n');
            if (AccIds != "0" && AccIds != "") {

                var arrAccountIds = AccIds.split(',');
                // console.log('ItemCode  : |' + ItemCode + '|. \r\n');

                $.each(arrAccountIds, function (index, value) {
                    //alert('enters Each Account Id');
                    //console.log('Account ID : |' + value + '|. \r\n');
                    if (value.trim() != '') {
                        List1.items.push({ "index": index, "AccountID": value, "ItemCode": ItemCode, "PrevBalance": "0", "Pageindex": "0", "PageIndexCalled": "0", "Loaded": "0", "LoadComplete": "0" });
                        //console.log('Account ID : ' + value + ', ItemCode : ' + ItemCode+' is added to array to Import Transactions.' + '.\r\n ');
                    }
                });
            }
            //if (ItemCode != "0" && ItemCode != "") {
            //    List1.items.push({ "index": "0", "AccountID": "0", "ItemCode": ItemCode, "PrevBalance": "0", "Pageindex": "0", "PageIndexCalled": "0", "Loaded": "0", "LoadComplete": "0" });
            //    console.log('Item Code : ' + ItemCode + ' is added to array to Import Transactions.' + '.\r\n ');
            //}
            //console.log(List1);
            GetAccountTrans()
        }

        function GetTransactionDetails(CommandName, TransactionID) {
            //console.log('enters GetTransactionDetails');
            //console.log('CommandName : ' + CommandName + ', TransactionID : ' + TransactionID);
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',TransactionID:'" + TransactionID + "'}";
            // console.log('dataParam : ' + dataParam);

            $.ajax({
                type: "POST",
                url: "../Accounting/frmJournalEntryReport.aspx/WebMethodGetTransactionDetail",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    //console.log('Jresponse : ' + Jresponse);
                    OnGetTransactionDetailsSuccess(Jresponse, CommandName, TransactionID);
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                    // console.log("textStatus : " + textStatus);
                    // console.log("errorThrown : " + errorThrown);
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    // console.log("errMessage : " + errMessage);
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }

        function OnGetTransactionDetailsSuccess(response, CommandName, TransactionID) {
            var linJournalId;
            var lintCheckId;
            var lintCashCreditCardId;
            var lintOppId;
            var lintOppBizDocsId;
            var lintDepositId;
            var lintCategoryHDRID;
            var lintChartAcntId;
            var lintBillPaymentID;
            var lintBillID;
            var lintReturnID;
            var lintLandedCostOppId;

            //  console.log('CommandName : ' + CommandName);
            var TransactionDetails = response.Table;
            // console.log('TransactionDetails : ' + TransactionDetails);

            $.each(TransactionDetails, function (index, Transaction) {
                linJournalId = Transaction.JournalId;
                lintCheckId = Transaction.CheckId;
                lintCashCreditCardId = Transaction.CashCreditCardId;
                lintOppId = Transaction.numOppId;
                lintOppBizDocsId = Transaction.numOppBizDocsId;
                lintDepositId = Transaction.numDepositId;
                lintCategoryHDRID = Transaction.numCategoryHDRID;
                lintChartAcntId = Transaction.numAccountId;
                lintBillPaymentID = Transaction.numBillPaymentID;
                lintBillID = Transaction.numBillID;
                lintReturnID = Transaction.numReturnID;
                lintLandedCostOppId = Transaction.numLandedCostOppId;
            });
            // console.log('linJournalId : ' + linJournalId + ', lintCheckId : ' + lintCheckId + ', lintCashCreditCardId : ' +
            //     lintCashCreditCardId + ', lintOppId : ' + lintOppId + ', lintOppBizDocsId : ' + lintOppBizDocsId + ', lintDepositId : ' + lintDepositId
            //       + ', lintCategoryHDRID : ' + lintCategoryHDRID + ', lintChartAcntId : ' + lintChartAcntId + ', lintBillPaymentID : ' + lintBillPaymentID
            //       + ', lintBillID : ' + lintBillID + ', lintReturnID : ' + lintReturnID);

            if (CommandName == 'Edit') {
                // console.log('Command is Edit');
                if (linJournalId != 0 && lintCheckId == 0 && lintCashCreditCardId == 0 && lintOppId == 0 && lintOppBizDocsId == 0 && lintDepositId == 0 && lintCategoryHDRID == 0 && lintBillID == 0 && lintBillPaymentID == 0 && lintReturnID == 0) {
                    window.location.href = "../Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=" + linJournalId;
                    //console.log('Navigate to ' + '../Accounting/frmJournalEntryList.aspx?frm=GeneralLedger&JournalId=' + linJournalId);
                }
                else if (lintCheckId != 0) {
                    window.location.href = "../Accounting/frmWriteCheck.aspx?frm=GeneralLedger&JournalId=" + linJournalId + "&CheckHeaderID=" + lintCheckId;
                }
                else if (lintCashCreditCardId != 0) {
                    window.location.href = "../Accounting/frmCash.aspx?frm=GeneralLedger&JournalId=" + linJournalId + "&CashCreditCardId=" + lintCashCreditCardId;
                }
                else if (lintOppId != 0 && lintOppBizDocsId != 0) {
                    window.open("../opportunity/frmBizInvoice.aspx?frm=GeneralLedger&OpID=" + lintOppId + "&OppBizId=" + lintOppBizDocsId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes');
                }
                else if (lintOppId != 0 && lintOppBizDocsId == 0) {
                    window.location.href = "../opportunity/frmOpportunities.aspx?opid=" + lintOppId;
                }
                else if (lintBillID != 0) {
                    if (lintLandedCostOppId > 0)
                        window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + lintLandedCostOppId, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
                    else
                        window.open("../Accounting/frmAddBill.aspx?BillID=" + lintBillID, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes');
                }
                else if (lintBillPaymentID != 0) {
                    window.location.href = "../Accounting/frmVendorPayment.aspx?frm=GeneralLedger&BillPaymentID=" + lintBillPaymentID;
                }
                else if (lintReturnID != 0) {
                    window.location.href = "../opportunity/frmReturnDetail.aspx?ReturnID=" + lintReturnID;
                }
                else if (lintDepositId != 0) {
                    //console.log('Command is Edit : and Deposit is  greater than Zero');

                    var DomainID = '<%= Session("DomainId")%>';
                            var Param = "{DomainID:'" + DomainID + "',lintDepositId:'" + lintDepositId + "'}";

                            $.ajax({
                                type: "POST",
                                url: "../Accounting/frmJournalEntryReport.aspx/WebMethodGetDepositDetails",
                                data: Param,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    // console.log('Command is Edit : and Deposit is  greater than Zero and getting Deposit Details Success');

                                    var JDepositDetails = $.parseJSON(response.d);
                                    var DepositDetails = JDepositDetails.Table;
                                    var tintDepositePage;
                                    var lintCheckId;

                                    $.each(DepositDetails, function (index, Deposit) {
                                        tintDepositePage = Deposit.tintDepositePage;
                                        // console.log('tintDepositePage : ' + tintDepositePage);
                                    });
                                    if (tintDepositePage == 2) {
                                        // console.log("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + lintDepositId);
                                        //window.location.href = "../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + lintDepositId;
                                        RPPopup = window.open("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + lintDepositId, 'ReceivePayment', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
                                        console.log(RPPopup);
                                    }
                                    else if (tintDepositePage == 1) {
                                        // console.log("../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" + lintDepositId);
                                        window.location.href = "../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" + linJournalId + "&ChartAcntId=" + lintChartAcntId + "&DepositId=" + lintDepositId;
                                    }
                                    // console.log('Deposit Details : ' + DepositDetails);
                                },
                                failure: function (response) {
                                    var errMessage = 'Sorry!.,Getting Deposit details failed!.,' + 'Please contact the BizAutomation support.';
                                    //console.log("errMessage : " + errMessage);
                                    $("#lblErrMessage").text(errMessage.toString());
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    // console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                                    // console.log("textStatus : " + textStatus);
                                    // console.log("errorThrown : " + errorThrown);
                                    var errMessage = 'Sorry!.,Getting Deposit details failed!.,' + errorThrown + '., Please contact the BizAutomation support.';
                                    // console.log("errMessage : " + errMessage);
                                    $("#lblErrMessage").text(errMessage.toString());
                                }, complete: function () {
                                }
                            });
                        }
}
else if (CommandName == 'Detail') {
    //console.log('Command is Detail');
    window.location.href = "../Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=" + linJournalId;
}
else if (CommandName == 'Delete') {
    var IsValidDelete = false;
    //console.log('Command is Delete');

    if (lintBillID != 0 && lintLandedCostOppId > 0) {
        alert('Landed Cost transaction can not be deleted from General Ledger. Please open Landed Cost then try to delete.');
        return;
    }

    var ReconStatus;

    $("[id*=RepeaterTable] tr").each(function () {
        var TransID = $(this).find('#lblTransactionId').text();
        if (TransactionID == TransID) {
            ReconStatus = $(this).find('#lblReconStatus').text();
        }
    });

    // console.log('parent : ' + ReconStatus);
    if (ReconStatus == 'R') {
        if (confirm('This transaction is reconciled, Are you sure, you want to delete the selected transaction?')) {
            IsValidDelete = true;
        }
        else {
            event.preventDefault();
            IsValidDelete = false;
        }
    }
    else {
        if (confirm('Are you sure, you want to delete the selected transaction?')) {
            IsValidDelete = true;
        }
        else {
            event.preventDefault();
            IsValidDelete = false;
        }
    }
    if (IsValidDelete == true) {
        if (linJournalId != 0) {
            if (lintOppBizDocsId > 0 && lintBillID == 0 && lintDepositId == 0 && lintBillPaymentID == 0) {
                alert('This transaction can not be deleted. If you want to change or delete it, you must edit or delete selected bizdoc from order details.');
            }
            else {
                // console.log('Execute Delete code here!.,');

                var DomainID = '<%= Session("DomainId")%>';
                var UserContactID = '<%= Session("UserContactID")%>';

                var deleteParam = "{DomainID:'" + DomainID + "',UserContactID:'" + UserContactID + "',linJournalId:'" + linJournalId +
                     "',lintBillPaymentID:'" + lintBillPaymentID + "',lintDepositId:'" + lintDepositId +
                      "',lintCheckId:'" + lintCheckId + "',lintBillID:'" + lintBillID +
                    "',lintCategoryHDRID:'" + lintCategoryHDRID + "',lintReturnID:'" + lintReturnID + "'}";

                $.ajax({
                    type: "POST",
                    url: "../Accounting/frmJournalEntryReport.aspx/WebMethodDeleteTransaction",
                    data: deleteParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var SuccessMessage = 'Transaction details deleted Successfully!.,';
                        $("#lblErrMessage").text(SuccessMessage.toString());
                        $("[id*=RepeaterTable] tr").each(function () {
                            var TransID = $(this).find('#lblTransactionId').text();
                            if (TransactionID == TransID) {
                                $(this).remove();
                            }
                        });

                        // console.log('Delete Transaction Success');
                        //  console.log('response : ' + response + 'response D : ' + response.d);
                    },
                    failure: function (response) {
                        var errMessage = 'Sorry!.,unable to delete the Transaction!.,';
                        // console.log("errMessage : " + errMessage);
                        $("#lblErrMessage").text(errMessage.toString());
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                        //  console.log("textStatus : " + textStatus);
                        //  console.log("errorThrown : " + errorThrown);
                        var errMessage = 'Sorry!.,unable to delete the Transaction!.,';
                        //console.log("errMessage : " + errMessage);
                        $("#lblErrMessage").text(errMessage.toString());
                    }, complete: function () {
                    }
                });
            }
        }
    }
}
}

function GetAccountTrans() {
    //var AccIds = strAccountList.toString();
    var pageIndex1;
    var AccountID;
    var ItemCode;
    var reqForward = 0;
    var PrevBalance = 0;
    if (CallWait == 0) {
        //console.log('Len:' + List1.items.length);
        $.each(List1.items, function () {
            var item = this;
            // console.log('Enters each List Array Item. Account ID : ' + item.AccountID);
            //console.log('item.index:' + item.index);
            //console.log('item.PageIndexCalled:' + item.PageIndexCalled);
            //console.log('item.Loaded:' + item.Loaded);
            if (item.index > -1) {
                //console.log('From In :' + item.index)
                if (item.LoadComplete == 0) {
                    if (item.Loaded > 0) {
                        pageIndex1 = item.Pageindex + 1;
                        //console.log('Page Index : ' + pageIndex1 + '. \r\n');
                    }
                    else {
                        pageIndex1 = 1;
                        //console.log('Page Index : ' + pageIndex1 + '. \r\n');
                    }
                    AccountID = item.AccountID;
                    ItemCode = item.ItemCode;
                    PrevBalance = item.PrevBalance;
                    //if (AccountID != '' && AccountID != '0') {
                    //  console.log('Account ID : '+ item.AccountID + ', PageIndexCalled : ' + item.PageIndexCalled + ', Current Page Index : ' + pageIndex1);
                    if (item.PageIndexCalled < pageIndex1) {

                        //console.log('Request forward to Get Transactions for Account ID : ' + AccountID + ', ItemCode : ' + ItemCode);
                        //console.log('Array Index  : ' + item.index + ', AccountID : ' + item.AccountID + ', pageIndex1 : ' + pageIndex1 + ', Loaded : ' + item.Loaded + ', LoadComplete : ' + item.LoadComplete + '.\r\n ');

                        GetTransactions(AccountID, pageIndex1, PrevBalance, ItemCode);
                        item.PageIndexCalled = pageIndex1;
                        reqForward = 1;
                        //console.log('executed Exit loop.');
                        return false;
                        // console.log('1 : ' + item.AccountID);
                    }
                    else {
                        // console.log('Request already forward for page Index : ' + pageIndex1 + ' for Account ID : ' + item.AccountID);
                    }
                    //  console.log('2 : ' + item.AccountID);
                    //}
                    //else {

                    //    //console.log('Account ID is Not Empty : ' + item.AccountID);
                    //}
                    //  console.log('3 : ' + item.AccountID);
                }
                else {
                    // console.log('Load Completed : ' + item.AccountID);
                }
                // console.log('4 : ' + item.AccountID);
            }
            else {
                //  console.log('Invalid Account Number : ' + item.AccountID);
            }
            // console.log('5 : ' + item.AccountID);
        });
    }
}

    function GetTransactions(AccountID, pageIndex1, PrevBalance, ItemCode) {
        $.each(List1.items, function () {
            var item = this;
            if (item.index > -1) {
                if (item.LoadComplete == 0) {
                    if (AccountID == item.AccountID) {
                        if (pageIndex1 > item.Pageindex) {
                            CallWait = 1;
                            $get('UpdateProgress1').style.display = "block";
                            //  console.log('Enters Get Transaction Request for AccountID : ' + AccountID + ', Page No : ' + pageIndex1);
                            //alert('enters GetTransactions');
                            //pageIndex++;
                            // pageIndex = 2;

                            var DomainID = '<%= Session("DomainId")%>';
                            //var AccountID = $("#hdnAccounts").val();
                            var PagingRows = 0;
                                    <%= lngDivisionID%>
                            var strDateFormat = '<%= Session("DateFormat")%>';
                            var FromDate = $('#ctl00_FiltersAndViews_calFrom_txtDate').val();
                            var ToDate = $('#ctl00_FiltersAndViews_calTo_txtDate').val();
                            //var ddlCOA = $('#ddlCOA').val();
                            var ReconStatus = $('#hdnReconStatus').val();
                            var DivisionId = $('#hdnDivisionId').val();
                            // var dataParam = "{DomainID:'" + DomainID + "'}";
                            var dataParam = "{DomainID:'" + DomainID + "',pageIndex:'" + pageIndex1 + "',PrevBalance:'" + PrevBalance + "',strDateFormat:'" + strDateFormat + "',FromDate:'" + FromDate + "',ToDate:'" + ToDate + "',DivisionId:'" + DivisionId + "',AccountID:'" + AccountID + "',ItemCode:'" + ItemCode + "',ReconStatus:'" + ReconStatus + "'}";
                            //  console.log('Ajax call parameter list : ' + dataParam);
                            $.ajax({
                                type: "POST",
                                url: "../Accounting/frmJournalEntryReport.aspx/WebMethodLoadGeneralDetails",
                                data: dataParam,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: OnSuccess,
                                failure: function (response) {
                                    //  Console.Log('Sorry!.,Getting records failed!.,');
                                    $get('UpdateProgress1').style.display = "none";
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    //   console.log("Error XMLHttpRequest : " + XMLHttpRequest.responseText);
                                    //   console.log("textStatus : " + textStatus);
                                    //   console.log("errorThrown : " + errorThrown);
                                    $get('UpdateProgress1').style.display = "none";
                                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                                    //  console.log("errMessage : " + errMessage);
                                    $("#lblErrMessage").text(errMessage.toString());
                                    alert($("#lblErrMessage").text());
                                }, complete: function () {
                                    CallWait = 0;
                                    $get('UpdateProgress1').style.display = "none";
                                }
                            });
                        }
                        else {
                            return false;
                        }
                    }
                }
            }

        });


    }

    function OnSuccess(response) {
        try {
            var Jresponse = $.parseJSON(response.d);
            var AccountId = '';
            var ItemID = '';
            var TransLoaded = '';
            var PrevBalance = 0;
            var ImportedTransactionCount = 0;
            var TransactionDetails = Jresponse.Table1;

            var row = $("[id*=RepeaterTable] tr").eq(1).clone(true);
            $("[id*=RepeaterTable] tr").eq(1).hide();
            AccountId = Jresponse.AccountDetails[0].AccountID;
            TransLoaded = Jresponse.AccountDetails[0].TransactionLoaded;
            PrevBalance = Jresponse.AccountDetails[0].PrevBalance;
            ItemID = Jresponse.AccountDetails[0].ItemID;
            var i = 0;
            if (ItemID != '0') {
                var headerRow = $("[id*=RepeaterTable] tr").eq(0);
                headerRow.find("td:eq(6)").remove();
                headerRow.find("td:eq(7)").remove();
                headerRow.find("td:eq(8)").remove();
            }

            $.each(TransactionDetails, function (index, Transaction) {
                i++;
                console.log('Transaction.numAccountID:' + Transaction.numAccountID);
                var LogMessage = '';
                LogMessage += 'numAccountId : ' + Transaction.numAccountId + ', ';
                row.find('#lblAccountID').html(Transaction.numAccountId);

                LogMessage += 'Date : ' + Transaction.Date + ', ';
                row.find('#lblJDate').html(Transaction.Date);

                //LogMessage += 'TransactionType : ' + Transaction.TransactionType + ', ';
                //row.find('#lblTransactionType').text(Transaction.TransactionType);
                //row.find('#lblType').text(Transaction.TransactionType);

                LogMessage += 'numTransactionId : ' + Transaction.numTransactionId + ', ';
                row.find('#lblTransactionId').text(Transaction.numTransactionId);

                LogMessage += 'CompanyName : ' + Transaction.CompanyName + ', ';
                row.find('#lblCompanyName').html(Transaction.CompanyName);

                LogMessage += 'vcAccountName : ' + Transaction.vcAccountName + ', ';
                row.find('#lblTranDesc').html(Transaction.vcAccountName);

                LogMessage += 'vcSplitAccountName : ' + Transaction.vcSplitAccountName + ', ';
                row.find('#lblvcAccountName').html(Transaction.vcSplitAccountName);

                LogMessage += 'numDebitAmt : ' + Transaction.numDebitAmt + ', ';
                row.find('#lblnumDebitAmt').html(Transaction.numDebitAmt);

                LogMessage += 'numCreditAmt : ' + Transaction.numCreditAmt + ', ';
                row.find('#lblnumCreditAmt').html(Transaction.numCreditAmt);

                LogMessage += 'Narration : ' + Transaction.Narration + ', ';
                row.find('#lblNarration').text(Transaction.Narration);

                LogMessage += 'TranRef : ' + Transaction.TranRef + ', ';
                row.find('#lblTranRef').text(Transaction.TranRef);

                //LogMessage += 'balance : ' + Transaction.balance + ', ';
                //row.find('#lblBalance').html(Transaction.balance);

                LogMessage += 'ddlReconStatus HTML : ' + $('#ddlReconStatus1').html() + '.';
                row.find('#ddlReconStatus').html($('#ddlReconStatus1').html());

                if (Transaction.bitReconcile == "1") {
                    row.find('#lblReconStatus').text("R");

                    LogMessage += 'Transaction Reconciled , ';
                    row.find('#ddlReconStatus option').each(function () {
                        if ($(this).val() == 'R') {
                            $(this).attr("selected", true);
                        }
                    });
                }
                else if (Transaction.bitCleared == "1") {
                    row.find('#lblReconStatus').text("C");
                    LogMessage += 'Transaction Cleared , ';
                    row.find('#ddlReconStatus option').each(function () {
                        if ($(this).val() == 'C') {
                            $(this).attr("selected", true);
                        }
                    });
                }
                else {
                    LogMessage += 'Transaction neither Cleared nor Reconsiled, ';
                }
                if (ItemID != '0') {
                    row.find('#btnDeleteAction').remove();
                    row.find('#lnkDelete').parent().remove();
                    row.find('#lblNarration').parent().remove();
                    //row.find('#lblBalance').parent().remove();
                    row.find('#ddlReconStatus').parent().remove();
                }
                if (Transaction.numTransactionId == '-1') {
                    row.find('#ddlReconStatus').remove();
                    row.find('#btnDeleteAction').remove();
                    row.find('#lnkDelete').remove();
                    row.find('#lnkDetail').remove();

                    row.show();
                    $("[id*=RepeaterTable] tbody").append(row);
                    row = $("[id*=RepeaterTable] tr").eq(1).clone(true);
                    LogMessage += 'Account Name : ' + Transaction.Date;
                    LogMessage += ', So ddlReconStatus and btnDeleteAction Controls Removed, ';
                }
                else {
                    ImportedTransactionCount++;
                    row.find('#ddlReconStatus').show();
                    row.find('#btnDeleteAction').show();

                    if (Transaction.numLandedCostOppId > 0)
                        row.find('#btnDeleteAction').hide();
                    else
                        row.find('#btnDeleteAction').show();

                    row.find('#lnkDelete').show();
                    row.find('#lnkDetail').show();
                    row.find('#lnkType').attr("onclick", "return CallEditCommand('" + Transaction.numTransactionId + "');");
                    row.find('#btnDeleteAction').attr("onclick", "return CallDeleteCommand('" + Transaction.numTransactionId + "');");
                    row.find('#lnkDetail').attr("onclick", "return CallDetailCommand('" + Transaction.numTransactionId + "');");
                    row.show();
                    $("[id*=RepeaterTable] tbody").append(row);
                    row = $("[id*=RepeaterTable] tr:last-child").clone(true);

                }
                //console.log('Row count : ' + i);
                //console.log(LogMessage);
            });
            // console.log('Imported Transaction Count for Account ID : ' + AccountId + ', is : ' + ImportedTransactionCount + '.\r\n ');
            $.each(List1.items, function () {
                var item = this;
                var calPgIndex = 0;
                if (item.index > -1) {
                    if (item.AccountID == AccountId) {
                        item.Loaded = TransLoaded;
                        item.PrevBalance = PrevBalance;
                        var calPgIndex = Math.floor(item.Loaded / 100);
                        var rem = item.Loaded % 100;
                        if (rem > 0) {
                            calPgIndex++;
                        }
                        item.Pageindex = calPgIndex;
                        if (ImportedTransactionCount < 100) {
                            item.LoadComplete = "1";
                        }
                        //console.log('Imported Transaction Count for Account ID : ' + item.AccountID + ', is : ' + ImportedTransactionCount + '.\r\n ');
                        //  console.log('Array Index : ' + item.index + ', AccountID : ' + item.AccountID + ', Page Index : ' + calPgIndex + ', Loaded : ' + item.Loaded + ', LoadComplete : ' + item.LoadComplete + '.\r\n ');
                    }
                }
            });
            $get('UpdateProgress1').style.display = "none";
            CallWait = 0;
            if ($(window).height() >= $("body").height()) {
                // console.log('has no ScrollBar');
                GetAccountTrans();
            }
        }
        catch (e) {
            var errMessage = e.message + '. ' + e.description + '., Please contact the BizAutomation support.';
            $("#lblErrMessage").text(errMessage);
            console.log('error : ' + e.responseText);
        }

    }

function CallEditCommand(TransactionID) {
    var HiddenValue = 'Edit,' + TransactionID;
    GetTransactionDetails('Edit', TransactionID);
    return false;
}

function CallDeleteCommand(TransactionID) {
    var HiddenValue = 'Delete,' + TransactionID;
    GetTransactionDetails('Delete', TransactionID);
    return false;
}

function CallDetailCommand(TransactionID) {
    var HiddenValue = 'Detail,' + TransactionID;
    GetTransactionDetails('Detail', TransactionID);
    return false;
}
    </script>
    <style type="text/css">
        .VisibleNone
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table width="100%">
                <tr>
                    <td align="right">
                        <label>
                            From
                        </label>
                    </td>
                    <td width="125" class="normal1">
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </td>
                    <td width="40px" class="normal1" align="right">
                        <label>
                            To
                        </label>
                    </td>
                    <td width="125" class="normal1">
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </td>
                    <td align="right">&nbsp;<asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button" OnClientClick="return Go();"></asp:Button>

                    </td>
                    <td>
                        <asp:ImageButton runat="server" ID="ibExportExcel" AlternateText="Export to Excel"
                            ToolTip="Export to Excel" ImageUrl="~/images/Excel.png" />&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table>
                <tr align="center">
                    <td valign="middle" style="margin-left: 25%">
                        <asp:Label ID="lblErrMessage" Visible="true" Style="color: Crimson;" runat="server" EnableViewState="False"></asp:Label>
                        <asp:Literal ID="litMessage" Visible="true" runat="server" EnableViewState="False"></asp:Literal>

                    </td>
                </tr>

            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="Journal Entries Report" runat="server" ID="lblGridTitle" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">

    <asp:Table ID="tblJournalEntry" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow Width="100%">
            <asp:TableCell VerticalAlign="top">
                <table cellpadding="0" width="100%" cellspacing="0" border="0" bgcolor="white" runat="server"
                    id="tblExport">
                    <tr valign="top">
                        <td valign="top">
                            <asp:UpdatePanel ID="uppnlGLTransList" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                </Triggers>
                                <ContentTemplate>

                                    <div id="RepeaterTable">
                                        <asp:Repeater ID="RepJournalEntry" runat="server">
                                            <HeaderTemplate>
                                                <table cellpadding="0" cellspacing="0" class="dg" border="0" width="100%">
                                                    <tr class="hs">
                                                        <td class="td1" align="center">
                                                            <b>Date</b>
                                                        </td>
                                                        <td class="td1" align="center">
                                                            <b>Account ID</b>
                                                        </td>
                                                        <%--<td class="td1" align="center">
                                                            <b>Transaction Type</b>
                                                        </td>--%>
                                                        <td class="td1" align="center">
                                                            <b>Name</b>
                                                        </td>
                                                        <td class="td1" align="center">
                                                            <b>Reference</b>
                                                        </td>
                                                        <td class="td1" align="center">
                                                            <b>Trans Description</b>
                                                        </td>
                                                        <td class="td1" align="center">
                                                            <b>Debit Amt</b>
                                                        </td>
                                                        <td class="td1" align="center">
                                                            <b>Credit Amt</b>
                                                        </td>
                                                        <%--<td class="td1" align="center">
                                                            <b>Balance</b>
                                                        </td>--%>
                                                        <td class="td1" align="center">
                                                            <b>Narration</b>
                                                        </td>
                                                        <td class="td1" align="center">
                                                            <%--<asp:DropDownList ID="ddlReconStatusFilter" runat="server" ClientIDMode="Static" EnableViewState="false">
                                                                <asp:ListItem Value="A">All</asp:ListItem>
                                                                <asp:ListItem Value="C">C</asp:ListItem>
                                                                <asp:ListItem Value="R">R</asp:ListItem>
                                                                <asp:ListItem Value="N">Not C/R</asp:ListItem>
                                                            </asp:DropDownList>--%>
                                                        </td>
                                                        <td class="td1" align="center"></td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class=' <%# IIf(Container.ItemIndex Mod 2 = 0, "tr1", "normal1")%>'>
                                                    <td>
                                                        <asp:Label ID="lblJDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Date")%>'></asp:Label>
                                                    </td>
                                                    <%-- <td>
                                                        <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit">
                                                            <asp:Label ID="lblTransactionType" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TransactionType")%>'></asp:Label>

                                                        </asp:LinkButton>
                                                        <asp:Label ID="lblType" runat="server" Visible="false"><%#DataBinder.Eval(Container.DataItem, "TransactionType")%></asp:Label>

                                                    </td>--%>
                                                    <td class="td1" align="center">
                                                        <asp:Label ID="lblAccountID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numAccountId")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCompanyName" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CompanyName")%>'></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTranDesc" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "vcAccountName")%>'></asp:Label>


                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblvcAccountName" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "vcSplitAccountName")%>'></asp:Label>


                                                    </td>
                                                    <td align="right" class="money">
                                                        <asp:Label ID="lblnumDebitAmt" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numDebitAmt")%>'></asp:Label>


                                                    </td>
                                                    <td align="right" class="money">
                                                        <asp:Label ID="lblnumCreditAmt" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numCreditAmt")%>'></asp:Label>


                                                    </td>
                                                    <%--<td style="width: auto;" align="right" class="money">
                                                        <asp:Label ID="lblBalance" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "balance")%>'></asp:Label>

                                                    </td>--%>
                                                    <td style="width: auto;">
                                                        <asp:Label ID="lblTranRef" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TranRef")%>'></asp:Label>
                                                        <asp:Label ID="lblNarration" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Narration")%>'></asp:Label>
                                                    </td>
                                                    <td style="width: auto;" align="center">
                                                        <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
                                                        <asp:Label ID="lblReconStatus" EnableViewState="false" runat="server" Style="display: none"></asp:Label>
                                                        <asp:DropDownList ID="ddlReconStatus" Style="visibility: hidden" runat="server" CssClass="ReconStatus" EnableViewState="false"></asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <%--<asp:Button ID="btnDeleteAction" runat="server" CssClass="button Delete" Text="X"
                                                            CommandName="Delete"></asp:Button>
                                                        <asp:LinkButton ID="lnkDeleteAction" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                                                        <asp:Label ID="lblFYClosed" Text="Closed" runat="server" Visible="false" CssClass="normal4" />
                                                        <asp:LinkButton ID="lnkDetail" runat="server" CssClass="hyperlink" CommandName="Detail" ToolTip="Journal detail">
                                                 <img src="../images/Text.gif" />
                                                        </asp:LinkButton>--%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div style="text-align: center;">
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="uppnlGLTransList">
            <ProgressTemplate>
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hdnAccounts" runat="server" />
    <asp:HiddenField ID="hdnItemCode" runat="server" />
    <asp:HiddenField ID="hdnDivisionId" runat="server" />
    <asp:HiddenField ID="hdnReconStatus" runat="server" ClientIDMode="Static" Value="A" />
    <asp:HiddenField runat="server" ID="hdnTransactionID" />
    <asp:Button ID="btnPostRepCommand" runat="server" Style="display: none"></asp:Button>
</asp:Content>
