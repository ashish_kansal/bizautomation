''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmRecurringEvents
    Inherits BACRMPage

#Region "Variables"
    Dim objBusinessRecurringEvent As RecurringEvents
    Dim lintRecurringId As Integer
   
    Dim objCommon As CCommon
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lintRecurringId = CCommon.ToLong(GetQueryStringVal("RecurringId"))
            If Not IsPostBack Then

                'To Set Permission
                objCommon = New CCommon
                GetUserRightsForPage(35, 96)

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False
                End If

                FillNetDays()
                'objCommon.sb_FillComboFromDB(ddlNetDays, 296, Session("DomainID"))
                If lintRecurringId <> 0 Then LoadSavedInformation()
            End If
            txtDays.Attributes.Add("onkeypress", "CheckNumber(2)")
            txtMonthlyDays.Attributes.Add("onkeypress", "CheckNumber(2)")
            txtMonthlyWeekDays.Attributes.Add("onkeypress", "CheckNumber(2)")
            btnSave.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub FillNetDays()
        Try
            Dim objOpp As New BACRM.BusinessLogic.Opportunities.OppotunitiesIP
            Dim dtTerms As New DataTable
            With objOpp
                .DomainID = Session("DomainID")
                .TermsID = 0
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtTerms = .GetTerms()
            End With

            If dtTerms IsNot Nothing Then
                ddlNetDays.DataSource = dtTerms
                ddlNetDays.DataTextField = "vcTerms"
                ddlNetDays.DataValueField = "numTermsID"
                ddlNetDays.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSavedInformation()
        Dim dtRecurringDet As DataTable
        Try
            If objBusinessRecurringEvent Is Nothing Then objBusinessRecurringEvent = New RecurringEvents
            objBusinessRecurringEvent.RecurringId = lintRecurringId
            objBusinessRecurringEvent.DomainId = Session("DomainId")
            dtRecurringDet = objBusinessRecurringEvent.GetRecurringTemplateDet()
            If dtRecurringDet.Rows.Count > 0 Then
                txtTemplateName.Text = dtRecurringDet.Rows(0)("varRecurringTemplateName")
                If dtRecurringDet.Rows(0)("chrIntervalType") = "D" Then
                    rdlRecurringTemplate.Items(0).Selected = True
                    txtDays.Text = dtRecurringDet.Rows(0)("tintIntervalDays")
                ElseIf dtRecurringDet.Rows(0)("chrIntervalType") = "M" Then
                    rdlRecurringTemplate.Items(1).Selected = True
                    If dtRecurringDet.Rows(0)("tintMonthlyType") = 0 Then
                        rdbMonthlyDays.Checked = True
                        ''ddlMonthlyDays.SelectedItem.Value = dtRecurringDet.Rows(0)("tintFirstDet")
                        If Not ddlMonthlyDays.Items.FindByValue(dtRecurringDet.Rows(0)("tintFirstDet")) Is Nothing Then
                            ddlMonthlyDays.Items.FindByValue(dtRecurringDet.Rows(0)("tintFirstDet")).Selected = True
                        End If
                        txtMonthlyDays.Text = dtRecurringDet.Rows(0)("tintIntervalDays")
                        'pnlMonthly.Visible = True
                        'pnlDaily.Visible = False
                        'pnlYearly.Visible = False
                    ElseIf dtRecurringDet.Rows(0)("tintMonthlyType") = 1 Then
                        rdbMonthlyWeekDays.Checked = True
                        If Not ddlMonthlyWeekDays.Items.FindByValue(dtRecurringDet.Rows(0)("tintFirstDet")) Is Nothing Then
                            ddlMonthlyWeekDays.Items.FindByValue(dtRecurringDet.Rows(0)("tintFirstDet")).Selected = True
                        End If

                        txtMonthlyWeekDays.Text = dtRecurringDet.Rows(0)("tintIntervalDays")

                        ''ddlMonthlyWeek.SelectedItem.Value = dtRecurringDet.Rows(0)("tintWeekDays")

                        If Not ddlMonthlyWeek.Items.FindByValue(dtRecurringDet.Rows(0)("tintWeekDays")) Is Nothing Then
                            ddlMonthlyWeek.Items.FindByValue(dtRecurringDet.Rows(0)("tintWeekDays")).Selected = True
                        End If
                    End If
                ElseIf dtRecurringDet.Rows(0)("chrIntervalType") = "Y" Then
                    rdlRecurringTemplate.Items(2).Selected = True

                    ''ddlYearlyMonth.SelectedItem.Value = dtRecurringDet.Rows(0)("tintMonths")

                    If Not ddlYearlyMonth.Items.FindByValue(dtRecurringDet.Rows(0)("tintMonths")) Is Nothing Then
                        ddlYearlyMonth.Items.FindByValue(dtRecurringDet.Rows(0)("tintMonths")).Selected = True
                    End If

                    If Not ddlYearlyDays.Items.FindByValue(dtRecurringDet.Rows(0)("tintFirstDet")) Is Nothing Then
                        ddlYearlyDays.Items.FindByValue(dtRecurringDet.Rows(0)("tintFirstDet")).Selected = True
                    End If

                End If
                CalStartDate.SelectedDate = dtRecurringDet.Rows(0)("dtStartDate")

                If dtRecurringDet.Rows(0)("bitEndType") = 1 Then
                    rdlNoEndDate.Checked = True
                ElseIf dtRecurringDet.Rows(0)("bitEndType") = 0 Then
                    rdlStopAfter.Checked = True
                    If dtRecurringDet.Rows(0)("bitEndTransactionType") = 0 Then
                        rdlEndDate.Checked = True
                        CalEndDate.SelectedDate = dtRecurringDet.Rows(0)("dtEndDate")
                    Else
                        rdlNoTransaction.Checked = True
                        txtNoTransactions.Text = dtRecurringDet.Rows(0)("numNoTransaction")
                    End If
                End If

                If CCommon.ToBool(dtRecurringDet.Rows(0)("bitBizDocBreakup")) Then
                    chkEnableBizDocBreakdown.Checked = True
                    If CCommon.ToInteger(dtRecurringDet.Rows(0)("intBillingBreakup")) > 0 Then
                        Dim strPercentage() As String = CCommon.ToString(dtRecurringDet.Rows(0)("vcBreakupPercentage")).Split("~")

                        Dim dt As DataTable = GetDataTable()
                        For i As Int16 = 0 To strPercentage.Length - 1
                            Dim dr As DataRow
                            dr = dt.NewRow
                            dr("ID") = i + 1
                            dr("Percentage") = strPercentage(i)
                            dt.Rows.Add(dr)
                        Next
                        dgBillingBreakup.DataSource = dt
                        dgBillingBreakup.DataBind()
                    End If

                    CalStartDate.Enabled = False
                    CalEndDate.Enabled = False
                    txtNoTransactions.Enabled = False
                    rdlEndDate.Enabled = False
                    rdlStopAfter.Enabled = False
                    rdlNoEndDate.Enabled = False
                    rdlNoTransaction.Enabled = False
                End If

            End If
            txtNoOfBizDocs.Text = CCommon.ToInteger(dtRecurringDet.Rows(0)("intBillingBreakup"))
            chkBillingTerms.Checked = CCommon.ToBool(dtRecurringDet.Rows(0)("bitBillingTerms"))
            If Not ddlNetDays.Items.FindByValue(dtRecurringDet.Rows(0)("numBillingDays")) Is Nothing Then
                ddlNetDays.Items.FindByValue(dtRecurringDet.Rows(0)("numBillingDays")).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objBusinessRecurringEvent Is Nothing Then objBusinessRecurringEvent = New RecurringEvents
            With objBusinessRecurringEvent
                .RecurringId = lintRecurringId
                .RecurringTemplateName = txtTemplateName.Text
                .DomainId = Session("DomainId")
                If rdlRecurringTemplate.SelectedValue = 0 Then
                    .IntervalType = "D"
                    .IntervalDays = txtDays.Text
                    ''pnlDaily.Style.visibility = "hidden"
                ElseIf rdlRecurringTemplate.SelectedValue = 1 Then
                    .IntervalType = "M"
                    If rdbMonthlyDays.Checked = True Then
                        .MonthlyType = 0
                        .FirstDet = ddlMonthlyDays.SelectedItem.Value
                        .IntervalDays = txtMonthlyDays.Text
                    ElseIf rdbMonthlyWeekDays.Checked = True Then
                        .MonthlyType = 1
                        .FirstDet = ddlMonthlyWeekDays.SelectedItem.Value
                        .IntervalDays = txtMonthlyWeekDays.Text
                        .WeekDays = ddlMonthlyWeek.SelectedItem.Value
                    End If
                ElseIf rdlRecurringTemplate.SelectedValue = 2 Then
                    .IntervalType = "Y"
                    .Months = ddlYearlyMonth.SelectedItem.Value
                    .FirstDet = ddlYearlyDays.SelectedItem.Value
                End If
                If chkEnableBizDocBreakdown.Checked = True Then
                    .dtStartDate = Date.Now
                Else
                    .dtStartDate = CalStartDate.SelectedDate
                End If


                If rdlNoEndDate.Checked = True Then
                    .EndType = 1
                ElseIf rdlEndDate.Checked = True Then
                    If chkEnableBizDocBreakdown.Checked = True Then
                        .dtEndDate = Date.Now
                    Else
                        .dtEndDate = CalEndDate.SelectedDate
                    End If
                    .EndType = 0
                    .EndTransactionType = 0
                ElseIf rdlNoTransaction.Checked = True Then
                    .EndTransactionType = 1
                    .NoTransaction = txtNoTransactions.Text
                ElseIf chkEnableBizDocBreakdown.Checked Then
                    .dtEndDate = Date.Now
                End If

                .BillingBreakup = CCommon.ToInteger(txtNoOfBizDocs.Text)
                .BreakupPercentage = GetPercentageAsString()
                .IsBillingTerm = chkBillingTerms.Checked
                .BillingDays = ddlNetDays.SelectedValue
                .EnableBreakup = chkEnableBizDocBreakdown.Checked
                Try
                    .SaveRecurringTemplate()
                    Response.Redirect("../Accounting/frmRecurringTemplateList.aspx", False)
                Catch ex As Exception
                    If (ex.Message = "INUSE") Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "msg", "<script>alert('Template is used by Opportunity, Please Turn off recurring for Opportunity and try again')</script>")
                    End If
                End Try
            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function GetPercentageAsString() As String
        Try
            Dim strPercentage As String = ""

            For i As Integer = 0 To dgBillingBreakup.Items.Count - 1
                strPercentage = strPercentage + CCommon.ToDecimal(CType(dgBillingBreakup.Items(i).FindControl("txtPercentage"), TextBox).Text).ToString + "~"
            Next

            Return strPercentage.TrimEnd("~")
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub ClearAllDatas()
        Try
            txtTemplateName.Text = ""
            txtDays.Text = "1"
            txtMonthlyDays.Text = "1"
            txtMonthlyWeekDays.Text = "1"
            CalStartDate.SelectedDate = ""
            CalEndDate.SelectedDate = ""
            rdlNoEndDate.Checked = True
            rdlRecurringTemplate.Items(0).Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            Response.Redirect("../Accounting/frmRecurringTemplateList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub txtNoOfBizDocs_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNoOfBizDocs.TextChanged
        Try
            Dim dt As DataTable = GetDataTable()

            Dim intNoOfBizDocIssue As Integer = CCommon.ToInteger(txtNoOfBizDocs.Text)
            If intNoOfBizDocIssue > 0 Then
                For i As Integer = 0 To intNoOfBizDocIssue - 1
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dr("ID") = i + 1
                    dr("Percentage") = CCommon.ToDecimal(100 / intNoOfBizDocIssue)
                    dt.Rows.Add(dr)
                Next
            End If

            dgBillingBreakup.DataSource = dt
            dgBillingBreakup.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Function GetDataTable() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ID")
        dt.Columns.Add("Percentage")
        Return dt
    End Function
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class