﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Workflow

Partial Public Class frmRecurringDetails
    Inherits BACRMPage
    Dim intOppId As Integer
    Dim intShipped As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            intOppId = CCommon.ToInteger(GetQueryStringVal("oppId"))
            intShipped = CCommon.ToInteger(GetQueryStringVal("Shipped"))
            hdnoppId.Value = intOppId

            If intShipped = 1 Then 'closed SO/PO
                tblRecurreSO.Visible = True
                tblRecurreBizDoc.Visible = False
            ElseIf intShipped = 0 Then 'Open SO/PO
                tblRecurreSO.Visible = False
                tblRecurreBizDoc.Visible = True
            End If

            Dim dtTempDetails As DataTable
            Dim objRecurringEvents As RecurringEvents
            If Not Page.IsPostBack Then
                
                FillNetDays()
                'objCommon.sb_FillComboFromDB(ddlNetDays, 296, Session("DomainID"))
                BindRecurringTemplate()
                BindDealAmount()
                LoadRecurringDetails()
                LoadOppBizDocItems()

                CalStartDate.SelectedDate = Date.Now
                CalIncremetalCreateDate.SelectedDate = Date.Now
            End If
            'btnRentalBizDoc.Attributes.Add("onclick", "return RentalBizDocSave('" & CCommon.GetValidationDateFormat() & " h:m a')")
            hdnDateTimeFormat.Value = CCommon.GetValidationDateFormat() & " h:m a"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "SplitAmount", "UpdateTotal(1);", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub FillNetDays()
        Try
            Dim objOpp As New OppotunitiesIP
            Dim dtTerms As New DataTable
            With objOpp
                .DomainID = Session("DomainID")
                .TermsID = 0
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtTerms = .GetTerms()
            End With

            If dtTerms IsNot Nothing Then
                ddlNetDays.DataSource = dtTerms
                ddlNetDays.DataTextField = "vcTerms"
                ddlNetDays.DataValueField = "numTermsID"
                ddlNetDays.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadOppBizDocItems()
        Try

            Dim objOppBizDocs As New OppBizDocs

            objOppBizDocs.OppId = intOppId
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.UserCntID = Session("UserContactID")
            If objOppBizDocs.GetOpportunityType() = 1 Then
                objOppBizDocs.OppId = intOppId
                hdnAuthorizativeBizDocsId.Value = objOppBizDocs.GetAuthorizativeOpportuntiy()

                objOppBizDocs.BizDocId = hdnAuthorizativeBizDocsId.Value
                objOppBizDocs.OppBizDocId = 0
                objOppBizDocs.boolRentalBizDoc = True

                Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()
                Dim dt As DataTable

                If CCommon.ToBool(Session("bitRentalItem")) = True AndAlso CCommon.ToLong(Session("RentalItemClass")) > 0 Then
                    Dim view As DataView = New DataView
                    view.Table = dtOppItems
                    view.RowFilter = "numItemClassification=" & CCommon.ToLong(Session("RentalItemClass"))
                    dgRentalBizDocs.DataSource = view
                    dgRentalBizDocs.DataBind()

                    If view.Count = 0 Then
                        tblRentalBizDoc.Visible = False
                    Else
                        tblRentalBizDoc.Visible = True
                    End If
                Else
                    tblRentalBizDoc.Visible = False
                End If
            Else
                tblRentalBizDoc.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindDealAmount()
        Try
            Dim dtDetails As DataTable
            Dim objOpportunity As New BACRM.BusinessLogic.Opportunities.MOpportunity
            objOpportunity.OpportunityId = intOppId
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtDetails = objOpportunity.OpportunityDTL.Tables(0)
            If dtDetails.Rows.Count > 0 Then
                hdnDealAmount.Value = CCommon.ToDecimal(dtDetails.Rows(0)("monPAmount"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindRecurringTemplate()
        Try
            Dim dtTempDetails As DataTable
            Dim objRecurringEvents As RecurringEvents
            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
            objRecurringEvents.DomainId = Session("DomainId")
            objRecurringEvents.IsBillingTerm = True
            dtTempDetails = objRecurringEvents.GetTemplateDetails()
            ddlRec.DataSource = dtTempDetails
            ddlRec.DataTextField = "varRecurringTemplateName"
            ddlRec.DataValueField = "numRecurringId"
            ddlRec.DataBind()
            ddlBizDocRecurringTemplate.DataSource = dtTempDetails
            ddlBizDocRecurringTemplate.DataTextField = "varRecurringTemplateName"
            ddlBizDocRecurringTemplate.DataValueField = "numRecurringId"
            ddlBizDocRecurringTemplate.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindRecurringGrid(ByVal dtRecurringId As DataTable)
        Try
            dgBizDocSchedule.DataSource = dtRecurringId
            dgBizDocSchedule.DataBind()

            'If rbManuallRecurring.Checked = True Then
            If Not dtRecurringId Is Nothing Then
                Dim decPercntageLeft = 100 - CCommon.ToDecimal(dtRecurringId.Compute("sum(fltBreakupPercentage)", ""))
                lblAmountLeft.Text = String.Format("{0:#,###.##}", decPercntageLeft * CCommon.ToDecimal(hdnDealAmount.Value) / 100)
                lblPercentageLeft.Text = String.Format("{0:####.##}", decPercntageLeft)
            End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadRecurringDetails()
        Try
            Dim dtRecurringId As DataTable
            Dim objRecurringEvents As New RecurringEvents
            'This code will get the recurring Id for the opportunity and load the drop down and radio
            objRecurringEvents.OpportunityId = intOppId
            dtRecurringId = objRecurringEvents.GetRecurringTemplateId()
            If dtRecurringId.Rows.Count > 0 Then
                If IsNumeric(dtRecurringId.Rows(0).Item("numRecurringid")) Then
                    If dtRecurringId.Rows(0).Item("numRecurringid") <> 0 Then
                        If dtRecurringId.Rows(0).Item("tintRecurringType") = 1 Then
                            ddlRec.Items.FindByValue(dtRecurringId.Rows(0).Item("numRecurringid")).Selected = True
                        ElseIf dtRecurringId.Rows(0).Item("tintRecurringType") = 2 Then
                            ddlBizDocRecurringTemplate.Items.FindByValue(dtRecurringId.Rows(0).Item("numRecurringid")).Selected = True
                            If dtRecurringId.Rows.Count > 1 Then
                                ddlBizDocRecurringTemplate.Enabled = False
                            End If
                        End If
                    End If
                End If
                If dtRecurringId.Rows(0).Item("tintRecurringType") = 1 Then
                    chkAmtZero.Checked = dtRecurringId.Rows(0).Item("bitRecurringZeroAmt")
                    tblRecurreSO.Visible = True
                    tblRecurreBizDoc.Visible = False
                ElseIf dtRecurringId.Rows(0).Item("tintRecurringType") = 2 Then
                    btnSaveandClose.CommandName = "TurnOff"
                    btnSaveandClose.Text = "Turn Off Recurring"
                    tblManuallRecurring.Visible = False

                    chkAmtZero.Checked = False
                    rbAutoRecurring.Checked = True
                    tblRecurreSO.Visible = False
                    tblRecurreBizDoc.Visible = True
                ElseIf dtRecurringId.Rows(0).Item("tintRecurringType") = 3 Then
                    rbManuallRecurring.Checked = True
                    tblAutoRecurring.Visible = False
                End If
            Else
                tblAutoRecurring.Visible = True
                tblManuallRecurring.Visible = True
            End If

            BindRecurringGrid(dtRecurringId)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim flag As Boolean = False
    Protected Sub btnSaveandClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveandClose.Click
        Try
            If tblRecurreSO.Visible = True Then
                UpdateRecurringDetails(ddlRec.SelectedValue, 1)
            ElseIf tblRecurreBizDoc.Visible = True Then
                If rbAutoRecurring.Checked Then
                    If btnSaveandClose.CommandName = "TurnOff" Then
                        UpdateRecurringDetails(0, 2) 'remove 
                        chkAmtZero.Checked = False
                        ddlBizDocRecurringTemplate.Enabled = True
                        btnSaveandClose.CommandName = ""
                        btnSaveandClose.Text = "Save"
                    Else
                        UpdateRecurringDetails(ddlBizDocRecurringTemplate.SelectedValue, 2)
                    End If
                ElseIf rbManuallRecurring.Checked Then
                    UpdateRecurringDetails(0, 3)
                End If
            Else
                UpdateRecurringDetails(0, 0)
                chkAmtZero.Checked = False
                ddlBizDocRecurringTemplate.Enabled = True
            End If
            LoadRecurringDetails()
            'If flag Then
            '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> window.close(); </script>")
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Function UpdateRecurringDetails(ByVal recId As Integer, ByVal intRecurringType As Integer)
        Try
            Dim objRecurringEvents As RecurringEvents
            If objRecurringEvents Is Nothing Then objRecurringEvents = New RecurringEvents
            objRecurringEvents.DomainId = Session("DomainId")
            objRecurringEvents.RecurringId = recId
            objRecurringEvents.OpportunityId = hdnoppId.Value
            objRecurringEvents.dtStartDate = IIf(intRecurringType = 3, CalIncremetalCreateDate.SelectedDate, CalStartDate.SelectedDate)

            'Following 3 parameter's will be used when manually spliting bizdoc by percentage
            If intRecurringType = 3 Then
                objRecurringEvents.IsBillingTerm = True 'IIf(ddlNetDays.SelectedValue > 0, True, False)
                objRecurringEvents.BillingDays = 0 'ddlNetDays.SelectedValue
                If rbOption1.Checked Then
                    objRecurringEvents.BreakupPercentageDec = Math.Round(CCommon.ToDecimal(txtBillingBreakup.Text), 4)
                ElseIf rbOption2.Checked Then
                    objRecurringEvents.BreakupPercentageDec = Math.Round(CCommon.ToDecimal(txtBillingAmount.Text) * 100 / CCommon.ToDecimal(hdnDealAmount.Value), 4)
                End If

            End If
            Dim strMessage As String = objRecurringEvents.ManageOpportunityRecurring(chkAmtZero.Checked, intRecurringType)
            If (strMessage.Length > 2) Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "msg", "<script>alert('" & strMessage & "')</script>")
                flag = False
            Else
                flag = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CloseMe", "<script> window.close(); </script>")
    'End Sub

    Private Sub dgBizDocSchedule_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocSchedule.EditCommand
        Try
            dgBizDocSchedule.EditItemIndex = e.Item.ItemIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim dtRecurringId As DataTable
            Dim objRecurringEvents As New RecurringEvents
            objRecurringEvents.OpportunityId = intOppId
            dtRecurringId = objRecurringEvents.GetRecurringTemplateId()

            BindRecurringGrid(dtRecurringId)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgBizDocSchedule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocSchedule.ItemCommand
        Try
            If e.CommandName = "Cancel" Then
                dgBizDocSchedule.EditItemIndex = e.Item.ItemIndex
                dgBizDocSchedule.EditItemIndex = -1
                Call BindGrid()
            End If

            If e.CommandName = "Update" Then
                Dim BizCalendar As UserControl

                BizCalendar = e.Item.FindControl("CalRecurranceDate")

                Dim strRecurringDate As String
                Dim _myControlType As Type = BizCalendar.GetType()
                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                strRecurringDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)

                objCommon.Mode = 7
                objCommon.UpdateRecordID = CCommon.ToLong(e.CommandArgument.ToString)
                objCommon.Comments = strRecurringDate
                objCommon.UpdateSingleFieldValue()
                dgBizDocSchedule.EditItemIndex = -1
                Call BindGrid()
            End If
            If e.CommandName = "Delete" Then
                dgBizDocSchedule.EditItemIndex = e.Item.ItemIndex
                dgBizDocSchedule.EditItemIndex = -1

                Dim objRecurring As New RecurringEvents
                objRecurring.DomainId = Session("DomainID")
                objRecurring.RecurringId = CCommon.ToLong(e.CommandArgument)
                objRecurring.DeleteRecurringRecord()

                LoadRecurringDetails()
            End If

            If e.CommandName = "CreateBizDoc" Then
                Dim objRecurring As New RecurringEvents
                objRecurring.CreateRecurringBizDocs(CCommon.ToLong(e.CommandArgument.ToString))
                BindGrid()
            End If
            If e.CommandName = "UpdatePercentage" Then

                For Each item As DataGridItem In dgBizDocSchedule.Items
                    objCommon.Mode = 8
                    objCommon.UpdateRecordID = CCommon.ToLong(CType(item.FindControl("lblOppRecID"), Label).Text)
                    objCommon.Comments = CCommon.ToDouble(CType(item.FindControl("txtBreakUpPercentage"), TextBox).Text).ToString
                    objCommon.UpdateSingleFieldValue()
                Next
                BindGrid()
            End If
            If e.CommandName = "BillRemanining" Then
                Dim objRecurring As New RecurringEvents
                objRecurring.DomainId = Session("DomainID")
                objRecurring.OpportunityId = intOppId
                objRecurring.BillRemainingBalance()
                If objRecurring.RecurringId > 0 Then
                    objRecurring.CreateRecurringBizDocs(objRecurring.RecurringId)
                    BindGrid()
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "msg", "<script>alert('Can not bill remaining amount,contact administrator')</script>")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgBizDocSchedule_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocSchedule.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CCommon.ToLong(CType(e.Item.FindControl("lblBizDocID"), Label).Text) > 0 Then
                    CType(e.Item.FindControl("lnkbtnEdt"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lnkbtnCreateBizDoc"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lblSeperator"), Label).Visible = False
                    CType(e.Item.FindControl("btnDelete"), Button).Visible = False
                End If
                If rbAutoRecurring.Checked Then
                    CType(e.Item.FindControl("btnDelete"), Button).Visible = False
                End If
            End If
            If e.Item.ItemType = ListItemType.Footer Then
                If rbManuallRecurring.Checked Then
                    CType(e.Item.FindControl("btnUpdatePercentage"), Button).Visible = False
                ElseIf rbAutoRecurring.Checked Then
                    CType(e.Item.FindControl("btnUpdatePercentage"), Button).Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            UpdateRecurringDetails(CCommon.ToInteger(ddlRec.SelectedValue), 3)
            LoadRecurringDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRentalBizDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRentalBizDoc.Click
        Try
            Dim JournalId As Long
            Dim objOppBizDocs As New OppBizDocs
            Dim lintOpportunityType As Integer
            Dim objJournal As New JournalEntry
            Dim EditMode As Boolean
            Dim OppBizDocID As Long
            Dim bAddBizDoc As Boolean = True

            Dim i As Integer
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppId = intOppId

            If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), ViewState("DivisionID")) = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip To Save BizDoc' );", True)
                Exit Sub
            End If

            objOppBizDocs.OppBizDocId = 0
            objOppBizDocs.BizDocId = hdnAuthorizativeBizDocsId.Value
            objOppBizDocs.UserCntID = Session("UserContactID")
            lintOpportunityType = objOppBizDocs.GetOpportunityType()
            objOppBizDocs.OppType = lintOpportunityType
            objOppBizDocs.bitPartialShipment = True
            objOppBizDocs.vcPONo = "" 'txtPO.Text
            objOppBizDocs.vcComments = "" 'txtComments.Text
            'objOppBizDocs.ShipCost = 0 'IIf(IsNumeric(txtShipCost.Text), txtShipCost.Text, 0)

            objOppBizDocs.boolRentalBizDoc = True
            objOppBizDocs.strBizDocItems = GetBizDocItems()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 33
            objCommon.Str = hdnAuthorizativeBizDocsId.Value
            objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                OppBizDocID = objOppBizDocs.SaveBizDoc()

                ''--------------------------------------------------------------------------''
                Dim ds As New DataSet
                objOppBizDocs.OppBizDocId = OppBizDocID
                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                Dim dtOppBiDocItems As DataTable
                dtOppBiDocItems = ds.Tables(0)

                Dim objCalculateDealAmount As New CalculateDealAmount
                objCalculateDealAmount.CalculateDealAmount(intOppId, OppBizDocID, lintOpportunityType, Session("DomainID"), dtOppBiDocItems)

                'Create new journal id
                JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Date.UtcNow, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))

                Dim objJournalEntries As New JournalEntry
                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                    objJournalEntries.SaveJournalEntriesSales(intOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ViewState("DivisionID"), ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                Else
                    objJournalEntries.SaveJournalEntriesSalesNew(intOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ViewState("DivisionID"), ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                End If

                objTransactionScope.Complete()
            End Using

            ''Added By Sachin Sadhu||Date:1stMay2014
            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = OppBizDocID
            objWfA.SaveWFBizDocQueue()
            'end of code

            '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
            Dim objAlert As New CAlerts
            objAlert.SendBizDocAlerts(intOppId, OppBizDocID, hdnAuthorizativeBizDocsId.Value, Session("DomainID"), IIf(bAddBizDoc = False, CAlerts.enmBizDoc.IsModified, CAlerts.enmBizDoc.IsCreated))

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Reload", "window.opener.location.reload(true);self.close();", True)
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Function GetBizDocItems() As String
        Try
            Dim dtBizDocItems As New DataTable
            Dim strOppBizDocItems As String
            dtBizDocItems.TableName = "BizDocItems"
            dtBizDocItems.Columns.Add("OppItemID")
            dtBizDocItems.Columns.Add("Quantity")
            dtBizDocItems.Columns.Add("Notes")
            dtBizDocItems.Columns.Add("TrackingNo")
            dtBizDocItems.Columns.Add("vcShippingMethod")
            dtBizDocItems.Columns.Add("monShipCost")
            dtBizDocItems.Columns.Add("dtDeliveryDate")
            dtBizDocItems.Columns.Add("dtRentalStartDate")
            dtBizDocItems.Columns.Add("dtRentalReturnDate")
            dtBizDocItems.Columns.Add("monPrice")
            dtBizDocItems.Columns.Add("monTotAmount")

            Dim dr As DataRow

            Dim CalRentalStartDate As UserControl
            Dim CalRentalReturnDate As UserControl

            Dim RentalStartDate, RentalReturnDate As DateTime
            For Each dgBizGridItem As DataGridItem In dgRentalBizDocs.Items
                If CType(dgBizGridItem.FindControl("chkInclude"), CheckBox).Checked = True Then
                    dr = dtBizDocItems.NewRow
                    dr("OppItemID") = dgBizGridItem.Cells(0).Text
                    dr("Notes") = ""
                    dr("TrackingNo") = ""
                    dr("vcShippingMethod") = ""
                    dr("monShipCost") = "0"
                    dr("Quantity") = dgBizGridItem.Cells(3).Text
                    dr("dtDeliveryDate") = Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")

                    CalRentalStartDate = dgBizGridItem.FindControl("CalRentalStartDate")
                    Dim _myControlType1 As Type = CalRentalStartDate.GetType()
                    Dim _myUC_DueDate1 As PropertyInfo = _myControlType1.GetProperty("SelectedDate")
                    RentalStartDate = _myUC_DueDate1.GetValue(CalRentalStartDate, Nothing)

                    CalRentalReturnDate = dgBizGridItem.FindControl("CalRentalReturnDate")
                    Dim _myControlType2 As Type = CalRentalReturnDate.GetType()
                    Dim _myUC_DueDate2 As PropertyInfo = _myControlType2.GetProperty("SelectedDate")
                    RentalReturnDate = _myUC_DueDate2.GetValue(CalRentalReturnDate, Nothing)

                    dr("dtRentalStartDate") = RentalStartDate.ToString("yyyy-MM-dd HH:mm:ss.000")
                    dr("dtRentalReturnDate") = RentalReturnDate.ToString("yyyy-MM-dd HH:mm:ss.000")

                    Dim lngDayHour As Long

                    Dim objItems As New CItems

                    Dim dtUnit As DataTable



                    Dim decUOMConversionFactor As Decimal = 1
                    'objCommon.DomainID = Session("DomainID")
                    'objCommon.ItemCode = CType(dgBizGridItem.FindControl("hfnumItemCode"), HiddenField).Value
                    'objCommon.UnitId = CType(dgBizGridItem.FindControl("hfnumUOM"), HiddenField).Value
                    'dtUnit = objCommon.GetItemUOMConversion()
                    'decUOMConversionFactor = CCommon.ToDecimal(dtUnit(0)("UOMConversion"))

                    If CCommon.ToLong(CType(dgBizGridItem.FindControl("hfnumUOM"), HiddenField).Value) = CCommon.ToLong(Session("RentalHourlyUOM")) Then
                        lngDayHour = RentalReturnDate.Subtract(RentalStartDate).TotalHours
                        decUOMConversionFactor = IIf(Session("RentalPriceBasedOn") = 1, 1, 0.0416666666666667)
                    ElseIf CCommon.ToLong(CType(dgBizGridItem.FindControl("hfnumUOM"), HiddenField).Value) = CCommon.ToLong(Session("RentalDailyUOM")) Then
                        lngDayHour = RentalReturnDate.Subtract(RentalStartDate).TotalDays
                        decUOMConversionFactor = IIf(Session("RentalPriceBasedOn") = 1, 24, 1)
                    Else
                        lngDayHour = RentalReturnDate.Subtract(RentalStartDate).TotalDays
                        decUOMConversionFactor = IIf(Session("RentalPriceBasedOn") = 1, 24, 1)
                    End If

                    objItems.ItemCode = CType(dgBizGridItem.FindControl("hfnumItemCode"), HiddenField).Value
                    objItems.NoofUnits = lngDayHour * decUOMConversionFactor
                    objItems.OppId = intOppId
                    objItems.DivisionID = ViewState("DivisionID")
                    objItems.DomainID = Session("DomainID")
                    objItems.byteMode = 1

                    objItems.Amount = 0
                    objItems.WareHouseItemID = CCommon.ToDecimal(CType(dgBizGridItem.FindControl("hfnumWarehouseItmsID"), HiddenField).Value)

                    Dim ds As DataSet
                    Dim dtItemPricemgmt As DataTable

                    ds = objItems.GetPriceManagementList1
                    dtItemPricemgmt = ds.Tables(0)

                    If dtItemPricemgmt.Rows.Count = 0 Then
                        dtItemPricemgmt = ds.Tables(2)
                    End If

                    dtItemPricemgmt.Merge(ds.Tables(1))

                    If dtItemPricemgmt.Rows.Count > 0 Then
                        dr("monPrice") = CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice"))
                        dr("monTotAmount") = Decimal.Round(Math.Abs((dr("Quantity") * lngDayHour * decUOMConversionFactor) * dr("monPrice")), 2).ToString("f2")
                    End If
                    dtBizDocItems.Rows.Add(dr)
                End If
            Next

            Dim dsNew As New DataSet
            dsNew.Tables.Add(dtBizDocItems)
            strOppBizDocItems = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            Return strOppBizDocItems
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class