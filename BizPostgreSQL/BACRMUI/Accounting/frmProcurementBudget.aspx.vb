''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Infragistics.WebUI.UltraWebGrid

Partial Public Class frmProcurementBudget
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim mobjBudget As Budget
    Dim dsTemp As DataSet
    Dim arr As ArrayList
    Dim dtHierarchyDetails As DataTable
    Dim mintBudgetId As Integer
    Dim m_aryRightsForPage() As Integer
    Dim objCommon As CCommon
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                objCommon = New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProcurementBudget.aspx", Session("userID"), 35, 99)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnProSave.Visible = False
                End If
                LoadProcurementBudget()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    '' Procurement Budget Informations
    Private Sub UlgProcurement_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles UlgProcurement.InitializeLayout
        Try
            Dim i As Integer
            UlgProcurement.Bands(0).Columns.FromKey("numItemGroupId").Hidden = True
            UlgProcurement.Bands(0).Columns.FromKey("numDomainId").Hidden = True
            UlgProcurement.Bands(0).Columns.FromKey("vcItemGroupName").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            UlgProcurement.Bands(0).Columns.FromKey("Total").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            UlgProcurement.Bands(0).Columns.FromKey("vcItemGroupName").HeaderText = "Procurement Budget Accounts"
            UlgProcurement.Bands(0).Columns.FromKey("vcItemGroupName").Width = Unit.Percentage(15)
            UlgProcurement.Bands(0).Columns.FromKey("Total").DataType = "System.Decimal"
            UlgProcurement.Bands(0).Columns.FromKey("Total").Format = "###,###.##"
            UlgProcurement.Bands(0).Columns.FromKey("Total").Width = Unit.Percentage(5)

            ''To Set Month Name
            For i = 3 To UlgProcurement.Columns.Count - 3
                UlgProcurement.Columns.Item(i).HeaderText = MonthName(UlgProcurement.Columns.Item(i).Key.Split("~")(0)).Substring(0, 3)
                UlgProcurement.Bands(0).Columns.FromKey(UlgProcurement.Columns.Item(i).Key).DataType = "System.Decimal"
                UlgProcurement.Bands(0).Columns.FromKey(UlgProcurement.Columns.Item(i).Key).Format = "###,###.##"
                UlgProcurement.Bands(0).Columns.FromKey(UlgProcurement.Columns.Item(i).Key).Width = Unit.Percentage(5)
            Next
        Catch ex As Exception
           Throw ex
        End Try
    End Sub

    Private Sub btnProSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProSave.Click
        Try
            Dim lngProcurementBudgetId As Long
            lngProcurementBudgetId = SaveProcurementBudgetMaster()
            SaveProcurementBudgetDetails(lngProcurementBudgetId)
            LoadProcurementBudget()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function SaveProcurementBudgetMaster() As Long
        Dim lngProcureBudgetId As Long
        Try
            If mobjBudget Is Nothing Then mobjBudget = New Budget
            With mobjBudget
                .DomainId = Session("DomainId")
                .UserCntID = Session("UserContactID")

                Select Case ddlProcFiscalYear.SelectedIndex
                    Case 0
                        .FiscalYear = Now.Year
                    Case 1
                        .FiscalYear = CInt(Now.Year - 1)
                    Case 2
                        .FiscalYear = CInt(Now.Year + 1)
                End Select
                .PurchaseDeal = chkProPurchaseDeal.Checked
                lngProcureBudgetId = .SaveProcurementBudgetMaster()
                Return lngProcureBudgetId
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub SaveProcurementBudgetDetails(ByVal lngProcBudgetId As Long)
        Try
            Dim dtProcurementBudget As New DataTable
            Dim ulrow As UltraGridRow
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim lstr As String
            Dim i As Integer
            If mobjBudget Is Nothing Then mobjBudget = New Budget
            dtProcurementBudget.Columns.Add("numProcurementId") '' Primary key
            dtProcurementBudget.Columns.Add("numItemGroupId")
            dtProcurementBudget.Columns.Add("tintMonth")
            dtProcurementBudget.Columns.Add("intYear")
            dtProcurementBudget.Columns.Add("monAmount")
            dtProcurementBudget.Columns.Add("Comments")

            For Each ulrow In UlgProcurement.Rows
                For i = 3 To UlgProcurement.Columns.Count - 3
                    dr = dtProcurementBudget.NewRow
                    dr("numProcurementId") = lngProcBudgetId
                    dr("numItemGroupId") = ulrow.Cells.FromKey("numItemGroupId").Value
                    dr("tintMonth") = ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Key.Split("~")(0)
                    If Not IsNothing(ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Value) Then
                        dr("monAmount") = IIf(ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Value.ToString = "False", 0, ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Value)
                    Else : dr("monAmount") = ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Value
                    End If
                    '' dr("monAmount") = ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Value
                    dr("intYear") = ulrow.Cells.FromKey(UlgProcurement.Columns.Item(i).Key).Key.Split("~")(1)
                    dr("Comments") = ulrow.Cells.FromKey("Comments").Value
                    dtProcurementBudget.Rows.Add(dr)
                Next
            Next
            Select Case ddlProcFiscalYear.SelectedIndex
                Case 0
                    mobjBudget.FiscalYear = Now.Year
                    mobjBudget.Type = 0
                Case 1
                    mobjBudget.FiscalYear = CInt(Now.Year - 1)
                    mobjBudget.Type = -1
                Case 2
                    mobjBudget.FiscalYear = CInt(Now.Year + 1)
                    mobjBudget.Type = 1
            End Select
            mobjBudget.ProcurementBudgetId = lngProcBudgetId
            ds.Tables.Add(dtProcurementBudget)
            lstr = ds.GetXml()
            mobjBudget.ProcurementBudgetDetails = lstr
            mobjBudget.SaveProcurementBudgetDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadProcurementBudget()
        Try
            If mobjBudget Is Nothing Then mobjBudget = New Budget
            Dim dtProcurement As DataTable
            mobjBudget.DomainId = Session("DomainId")

            Select Case ddlProcFiscalYear.SelectedIndex
                Case 0
                    mobjBudget.FiscalYear = Now.Year
                    mobjBudget.Type = 0
                Case 1
                    mobjBudget.FiscalYear = CInt(Now.Year - 1)
                    mobjBudget.Type = -1
                Case 2
                    mobjBudget.FiscalYear = CInt(Now.Year + 1)
                    mobjBudget.Type = 1
            End Select
            dtProcurement = mobjBudget.GetProcurementMasterDetails()
            If dtProcurement.Rows.Count > 0 Then
                chkProPurchaseDeal.Checked = dtProcurement.Rows.Item(0)("bitPurchaseDeal")
                mobjBudget.ProcurementBudgetId = dtProcurement.Rows.Item(0)("numProcurementBudgetId")
            Else
                chkProPurchaseDeal.Checked = False
                mobjBudget.ProcurementBudgetId = 0
            End If
            UlgProcurement.Clear()
            UlgProcurement.DataSource = mobjBudget.GetProcurementDetails()
            UlgProcurement.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlProcFiscalYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcFiscalYear.SelectedIndexChanged
        Try
            If Not IsNothing(ddlProcFiscalYear.SelectedItem.Value) Then LoadProcurementBudget()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnProClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProClose.Click
        Try
            ddlProcFiscalYear.SelectedIndex = 0
            LoadProcurementBudget()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class