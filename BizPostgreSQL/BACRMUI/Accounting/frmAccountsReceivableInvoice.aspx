﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAccountsReceivableInvoice.aspx.vb"
    Inherits=".frmAccountsReceivableInvoice" ValidateRequest="false" MasterPageFile="~/common/PopupBootstrap.Master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
            window.opener.reDirectPage(str);
            //document.location.href = str;
        }
        function OpenBizInvoice(a, b) {
            if (a > 0) {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            if (b > 0) {
                var BalanceAmt = c;
                RPPopup = window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, 'ReceivePayment', 'toolbar=no,titlebar=no,top=50,width=1000,height=500,scrollbars=yes,resizable=no');
                console.log(RPPopup);

            }
            return false;
        }

        function GrabHTML() {
            document.getElementById('hdnHTML').value = document.getElementById("divHtml").innerHTML; //case in-sensetive replace
            return true;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            BindJquery();
        }

        function BindJquery() {

            $("[id$=gvARDetails] tr:first input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    on_filter();
                    return false;
                }
            });

            $('[id$=gvARDetails] tr:first select').change(function () {
                on_filter();
            });

            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('#btnGo1').trigger('click');
            });
        }

        function on_filter() {

            var filter_condition = '';

            var dropDownControls = $('[id$=gvARDetails] tr:first select');
            dropDownControls.each(function () {
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=gvARDetails] tr:first input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0 && !$(this).hasClass("rcbInput")) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('#txtGridColumnFilter').val(filter_condition);
            $('#btnGo1').trigger('click');
        }
    </script>
    <style type="text/css">
        #divHtml {
            height: calc(100vh - 150px);
            overflow: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Range:</label>
                    <asp:DropDownList ID="ddlRange" runat="server" CssClass="form-control" AutoPostBack="true">
                        <asp:ListItem Value="">All</asp:ListItem>
                        <asp:ListItem Value="0+30">Today - 30</asp:ListItem>
                        <asp:ListItem Value="30+60">31 - 60</asp:ListItem>
                        <asp:ListItem Value="60+90">61 - 90</asp:ListItem>
                        <asp:ListItem Value="90+">Over 91</asp:ListItem>
                        <asp:ListItem Value="0-30">Past Due Today - 30</asp:ListItem>
                        <asp:ListItem Value="30-60">Past Due 31 - 60</asp:ListItem>
                        <asp:ListItem Value="60-90">Past Due 61 - 90</asp:ListItem>
                        <asp:ListItem Value="90-">Past Due Over 91</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    A/R Aging Detail Report
    <asp:ImageButton runat="server" ID="ibExportExcel" AlternateText="Export to Excel" ToolTip="Export to Excel" ImageUrl="~/images/Excel.png" />
</asp:Content>
<asp:Content ContentPlaceHolderID="GridFilterTools" runat="server">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release"></asp:ScriptManager>
    <div class="row">
        <div class="col-xs-12">
            <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" class="table-responsive" ID="divHtml" ClientIDMode="Static">
                <ContentTemplate>
                    <asp:GridView ID="gvARDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" ShowFooter="true" FooterStyle-Font-Bold="true">
                        <Columns>
                            <asp:TemplateField HeaderText="Company Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <HeaderTemplate>
                                    Company Name<br />
                                    <asp:TextBox ID="txtOrganization" runat="server" CssClass="form-control"></asp:TextBox>
                                </HeaderTemplate>
                                <ItemTemplate><%# Eval("vcCompanyName")%></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Invoice Id" SortExpression="vcbizdocid" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <HeaderTemplate>
                                    Invoice ID<br />
                                    <asp:TextBox ID="txtInvoice" runat="server" CssClass="form-control"></asp:TextBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>');">
                                        <%#Eval("vcBizDocID")%>
                                    </a>&nbsp;&nbsp;(<%#Eval("vcPOppName")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Record Date" DataField="dtDate" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundField>
                            <asp:BoundField HeaderText="Due Date" DataField="DueDate" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundField>
                            <asp:TemplateField HeaderText="Total Amount" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("TotalAmount"))%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amt Paid" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")),Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("AmountPaid")))%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblAmountPaid" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amt Due" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <%#IIf(Integer.Parse(Eval("CurrencyCount")) > 1, "<font color='purple'><b>" +Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue")) + "</b></font>",Eval("varCurrSymbol").ToString() + " " + String.Format("{0:##,#00.00}", Eval("BalanceDue"))) %>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Customer P.O. #" DataField="vcRefOrderNo" ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false">
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Order Contact" DataField="vcContactName" ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false">
                                <ItemStyle Width="120px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Phone & Ext" DataField="vcCustPhone" ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false">
                                <ItemStyle Width="120px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Days Late" DataField="DaysLate" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemStyle Width="80px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="A/R Account" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <HeaderTemplate>
                                    A/R Account<br />
                                    <asp:DropDownList ID="ddlARAccount" runat="server" CssClass="form-control"></asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("vcARAccount")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No Record Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnGo1" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField runat="server" ID="hdnHTML" />
    <asp:TextBox ID="txtGridColumnFilter" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none;" OnClick="btnGo1_Click" />
</asp:Content>
