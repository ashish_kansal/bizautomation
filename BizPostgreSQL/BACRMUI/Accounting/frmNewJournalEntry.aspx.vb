''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Projects

Partial Public Class frmNewJournalEntry
    Inherits BACRMPage

#Region "Variables"
    Dim lintJournalId As Integer

    Dim objJournalEntry As JournalEntry
    Dim dtChartAcnt As DataTable
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            Currency.Value = Session("Currency")
            btnSave.Attributes.Add("onclick", "return Validate();")
            btnSaveNew.Attributes.Add("onclick", "return Validate();")
            lintJournalId = CCommon.ToLong(GetQueryStringVal("JournalId"))
            'To Set Permission
            GetUserRightsForPage(35, 75)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
                btnSaveNew.Visible = False
            End If
            txtJournalReferenceNo.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            litMessage.Text = ""

            If Not IsPostBack Then

                If lintJournalId = 0 Then
                    btnBack.Visible = False
                    btnNew.Visible = False
                End If

                If Not IsNothing(lintJournalId) And lintJournalId <> 0 Then

                    'Dim j As Integer
                    'Dim lintRowcount As Integer
                    'Dim drRow As DataRow
                    Dim dtJournalDetails As DataTable
                    'If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry()
                    'objJournalEntry.DomainID = Session("DomainId")
                    'objJournalEntry.JournalId = lintJournalId
                    'dtJournalDetails = objJournalEntry.GetJournalEntryDate()
                    If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

                    objJournalEntry.DomainID = Session("DomainId")
                    objJournalEntry.JournalId = lintJournalId
                    dtJournalDetails = objJournalEntry.GetJournalEntryDetails()
                    txtDescription.Text = CCommon.ToString(dtJournalDetails.Rows(0)("vcNarration"))
                    txtJournalReferenceNo.Text = CCommon.ToString(dtJournalDetails.Rows(0)("numJournalReferenceNo"))

                    'hdnBizDocsPaymentDetId.Value = CCommon.ToLong(dtJournalDetails.Rows(0)("numBizDocsPaymentDetId"))

                    calfrom.SelectedDate = dtJournalDetails.Rows(0)("datEntryDate")

                    If CCommon.ToString(dtJournalDetails.Rows(0)("chBizDocItems")).Trim() = "IA1" Then
                        litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;You can not modify inventory adjustment journal, since it is automatically managed by Biz as inventory count goes up or down.</span>"
                        btnSave.Visible = False
                        btnSaveNew.Visible = False
                    End If
                    If CCommon.ToString(dtJournalDetails.Rows(0)("chBizDocItems")).Trim() = "OE1" Then
                        litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;You can not modify inventory opening balance journal, since it is automatically managed by Biz.Please use inventory adjustment to update stock.</span>"
                        btnSave.Visible = False
                        btnSaveNew.Visible = False
                    End If
                    If CCommon.ToString(dtJournalDetails.Rows(0)("chBizDocItems")).Trim() = "OE" Then
                        litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;You can not modify opening balance journal, since it is automatically managed by Biz.Your option is to update opening balance from respective chart of account.</span>"
                        btnSave.Visible = False
                        btnSaveNew.Visible = False
                    End If

                    If CCommon.ToLong(dtJournalDetails.Rows(0)("numOppId")) > 0 Or CCommon.ToLong(dtJournalDetails.Rows(0)("numOppBizDocsId")) > 0 Or CCommon.ToLong(dtJournalDetails.Rows(0)("numDepositId")) > 0 _
                        Or CCommon.ToLong(dtJournalDetails.Rows(0)("numReturnID")) > 0 Or CCommon.ToLong(dtJournalDetails.Rows(0)("numCategoryHDRID")) > 0 Or CCommon.ToLong(dtJournalDetails.Rows(0)("numCheckHeaderID")) > 0 _
                        Or CCommon.ToLong(dtJournalDetails.Rows(0)("numBillID")) > 0 Or CCommon.ToLong(dtJournalDetails.Rows(0)("numBillPaymentID")) > 0 Or CCommon.ToLong(dtJournalDetails.Rows(0)("numPayrollDetailID")) > 0 Then

                        litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;You can not modify journal, since it is automatically managed by Biz.</span>"
                        btnSave.Visible = False
                        btnSaveNew.Visible = False
                        gvJournalEntry.Columns(8).Visible = False
                        'gvJournalEntry.FooterRow.FindControl("btnAdd").Visible = False
                    End If

                    TransactionInfo1.ReferenceType = enmReferenceType.DirectJournal
                    TransactionInfo1.ReferenceID = lintJournalId
                    TransactionInfo1.getTransactionInfo()
                Else
                    If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

                    objJournalEntry.DomainID = Session("DomainId")
                    Dim dtJournalDetails As DataTable
                    dtJournalDetails = objJournalEntry.GetMaxJournalReferenceNo()
                    txtJournalReferenceNo.Text = CCommon.ToString(dtJournalDetails.Rows(0)("numJournalReferenceNo"))
                    calfrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                End If

                LoadJournalEntry(boolPostback:=True)
                If CCommon.ToLong(GetQueryStringVal("flag")) = 1 Then
                    If CCommon.ToLong(GetQueryStringVal("EntryNo")) > 0 Then
                        litMessage.Text = "<span class='btn btn-success'>Journal entry #" & CCommon.ToLong(GetQueryStringVal("EntryNo")) & " created sucessfully</span>"
                    Else
                        litMessage.Text = "<span class='btn btn-success'>Journal entry created sucessfully</span>"
                    End If
                End If
            End If

            objCommon.ManageGridAsPerConfig(gvJournalEntry)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadChartAcnt()
        Try
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = ""
            dtChartAcnt = objCOA.GetParentCategory()
            'Commented by chintan- reason bug 949 
            'Remove AR and AP values ..As those will create problem in AR aging summary report, So better not to allow to create journal entry against it
            'Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")

            'For index As Integer = 0 To dr.Length - 1
            '    dtChartAcnt.Rows.Remove(dr(index))
            'Next
            'dtChartAcnt.AcceptChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadJournalEntry(Optional deleteRowIndex As Integer = -1, Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Dim j As Integer
        Dim dtItems As New DataTable

        Try
            If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry

            objJournalEntry.DomainID = Session("DomainId")
            objJournalEntry.JournalId = lintJournalId
            dtItems = objJournalEntry.GetJournalEntryDetails()
            Dim dtrow As DataRow

            If boolPostback = False Then
                dtItems.Rows.Clear()

                Dim i As Integer
                Dim gvRow As GridViewRow

                For i = 0 To gvJournalEntry.Rows.Count - 1
                    If i <> deleteRowIndex Then
                        dtrow = dtItems.NewRow
                        gvRow = gvJournalEntry.Rows(i)

                        dtrow.Item("TransactionId") = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                        dtrow.Item("numDebitAmt") = CCommon.ToDecimal(CType(gvRow.FindControl("txtDebit"), TextBox).Text)
                        dtrow.Item("numCreditAmt") = CCommon.ToDecimal(CType(gvRow.FindControl("txtCredit"), TextBox).Text)
                        dtrow.Item("numChartAcntId") = CCommon.ToLong(CType(gvRow.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.Split("~")(0))
                        dtrow.Item("varDescription") = CType(gvRow.FindControl("txtMemo"), TextBox).Text
                        dtrow.Item("numCustomerId") = CCommon.ToLong(CType(gvRow.FindControl("radCmbCompany"), RadComboBox).SelectedValue)

                        dtrow.Item("numClassId") = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                        dtrow.Item("numProjectId") = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)

                        dtItems.Rows.Add(dtrow)
                    End If
                Next
            End If

            If dtItems.Rows.Count < 2 Then
                Dim lintRowcount As Int16 = 2 - dtItems.Rows.Count
                For j = 0 To lintRowcount - 1
                    dtrow = dtItems.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = 0
                    dtrow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtItems.Rows.Add(dtrow)
                Next
            End If

            If boolAddNew Then
                For j = 0 To 1
                    dtrow = dtItems.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = 0
                    dtrow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtItems.Rows.Add(dtrow)
                Next
            End If

            LoadChartAcnt()

            gvJournalEntry.DataSource = dtItems
            gvJournalEntry.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub gvJournalEntry_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvJournalEntry.RowCommand
        Try
            If e.CommandName = "DeleteRow" Then
                Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                LoadJournalEntry(deleteRowIndex:=row.DataItemIndex)

                'If Not ClientScript.IsStartupScriptRegistered("CalculateTotal") Then ClientScript.RegisterStartupScript(Me.GetType, "CalculateTotal", "CalculateTotal();", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub gvJournalEntry_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournalEntry.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlAccounts As DropDownList = CType(e.Row.FindControl("ddlAccounts"), DropDownList)
                Dim txtDebit As TextBox = CType(e.Row.FindControl("txtDebit"), TextBox)
                Dim txtCredit As TextBox = CType(e.Row.FindControl("txtCredit"), TextBox)
                Dim txtDescription As TextBox = CType(e.Row.FindControl("txtDescription"), TextBox)
                Dim ddlClass As DropDownList = CType(e.Row.FindControl("ddlClass"), DropDownList)
                Dim ddlProject As DropDownList = CType(e.Row.FindControl("ddlProject"), DropDownList)
                Dim radCmbCompany As RadComboBox = CType(e.Row.FindControl("radCmbCompany"), RadComboBox)

                txtDebit.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtCredit.Attributes.Add("onkeypress", "CheckNumber(1,event)")

                BindClass(ddlClass)
                BindProject(ddlProject)

                If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numClassID")) > 0 Then
                    If ddlClass.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numClassID")) IsNot Nothing Then
                        ddlClass.ClearSelection()
                        ddlClass.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numClassID")).Selected = True
                    End If
                End If

                If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numProjectID")) > 0 Then
                    If ddlProject.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numProjectID")) IsNot Nothing Then
                        ddlProject.ClearSelection()
                        ddlProject.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numProjectID")).Selected = True
                    End If
                End If

                If Not IsNothing(radCmbCompany) Then
                    Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)

                    If CCommon.ToLong(lblName.Text) > 0 Then
                        objCommon.UserCntID = Session("UserContactID")
                        objCommon.DivisionID = IIf(lblName.Text = "", 0, lblName.Text)
                        objCommon.charModule = "D"
                        Dim strCompany As String
                        strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                    End If
                End If

                If Not IsNothing(ddlAccounts) Then
                    LoadChartType(ddlAccounts)
                    Dim lblAccountID As Label = CType(e.Row.FindControl("lblAccountID"), Label)

                    For Each Item As ListItem In ddlAccounts.Items
                        If lblAccountID.Text <> "" And lblAccountID.Text <> "0" And Item.Value.StartsWith(lblAccountID.Text) Then
                            ddlAccounts.ClearSelection()
                            Item.Selected = True
                        End If
                    Next
                End If

                Dim btnDelete As LinkButton = CType(e.Row.FindControl("btnDelete"), LinkButton)
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If

            If e.Row.RowType = DataControlRowType.Footer Then
                If lintJournalId > 0 Then
                    'If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry()
                    'objJournalEntry.DomainID = Session("DomainID")
                    'objJournalEntry.JournalId = lintJournalId
                    'objJournalEntry.AcntType = 1  ' For Debit Entry
                    'CType(e.Row.FindControl("lblTotDebitValue1"), Label).Text = objJournalEntry.GetSumDebitCreditValues()
                    'objJournalEntry.AcntType = 2 ' For Credit Entry
                    'CType(e.Row.FindControl("lblTotCreditValue1"), Label).Text = objJournalEntry.GetSumDebitCreditValues()

                    CType(e.Row.FindControl("lblTotalDebit1"), Label).Text = Currency.Value
                    CType(e.Row.FindControl("lblTotalCredit1"), Label).Text = Currency.Value
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Dim dtProject As DataTable
    Private Sub BindProject(ByRef ddlProject As DropDownList)
        Try
            If dtProject Is Nothing Then
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                dtProject = objProject.GetOpenProject()
            End If

            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtProject
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim dtClass As DataTable
    Private Sub BindClass(ByRef ddlClass As DropDownList)
        Try
            If dtClass Is Nothing Then
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()
            End If
            ddlClass.DataTextField = "ClassName"
            ddlClass.DataValueField = "numChildClassID"
            ddlClass.DataSource = dtClass
            ddlClass.DataBind()
            ddlClass.Items.Insert(0, "--Select One--")
            ddlClass.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadChartType(ByVal ddlAccounts As DropDownList)
        Try
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("vcAccountTypeCode") 'added parent account type code , bug 949
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccounts.Items.Add(item)
            Next
            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader() As Integer
        Dim lntJournalId As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lintJournalId
                .RecurringId = 0
                .EntryDate = CDate(calfrom.SelectedDate & " 12:00:00")
                .Description = txtDescription.Text.Trim
                .Amount = Replace(TotalDrValue.Value, ",", "")
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                '.BizDocsPaymentDetId = CCommon.ToLong(hdnBizDocsPaymentDetId.Value) 'added by chintan Bug # 1802 , used when editing bill's journal entry
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .JournalReferenceNo = CCommon.ToLong(txtJournalReferenceNo.Text)
            End With

            lntJournalId = objJEHeader.Save()
            Return lntJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''To Save details in General_Journal_Details table
    Private Sub SaveDataToGeneralJournalDetails(ByVal lintJournalId As Integer)
        Dim i As Integer
        Dim ds As New DataSet
        Dim ddlAccounts As DropDownList
        Dim lintAccountId() As String
        Dim gvRow As GridViewRow
        Try
            Dim objJEList As New JournalEntryCollection
            Dim objJE As New JournalEntryNew()

            For i = 0 To gvJournalEntry.Rows.Count - 1
                gvRow = gvJournalEntry.Rows(i)
                If CType(gvRow.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString <> "0" _
                         And (CCommon.ToDecimal(CType(gvRow.FindControl("txtDebit"), TextBox).Text) > 0 Or CCommon.ToDecimal(CType(gvRow.FindControl("txtCredit"), TextBox).Text) > 0) Then

                    ddlAccounts = CType(gvRow.FindControl("ddlAccounts"), DropDownList)
                    lintAccountId = ddlAccounts.SelectedItem.Value.Split("~")

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = CCommon.ToLong(CType(gvRow.FindControl("lblTransactionId"), Label).Text)
                    objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(CType(gvRow.FindControl("txtDebit"), TextBox).Text))
                    objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(CType(gvRow.FindControl("txtCredit"), TextBox).Text))
                    objJE.ChartAcntId = lintAccountId(0)
                    objJE.Description = CType(gvRow.FindControl("txtMemo"), TextBox).Text
                    objJE.CustomerId = CCommon.ToLong(CType(gvRow.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = ""
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = CCommon.ToLong(CType(gvRow.FindControl("ddlProject"), DropDownList).SelectedValue)
                    objJE.ClassID = CCommon.ToLong(CType(gvRow.FindControl("ddlClass"), DropDownList).SelectedValue)
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = enmReferenceType.DirectJournal   'Direct Journal
                    objJE.ReferenceID = lintJournalId 'Journal ID

                    objJEList.Add(objJE)
                End If
            Next

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lintJournalId, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            LoadJournalEntry(boolAddNew:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim JournalId As Integer = Save()

            If JournalId > 0 Then
                PageRedirect()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            Dim JournalId As Integer = Save()

            If JournalId > 0 Then
                Response.Redirect("../Accounting/frmNewJournalEntry.aspx?flag=1&EntryNo=" & txtJournalReferenceNo.Text & "&ChartAcntId=" & CCommon.ToLong(GetQueryStringVal("ChartAcntId")))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function Save() As Long
        Try
            Dim JournalId As Integer = 0
            If CCommon.ToDecimal(TotalDrValue.Value) = 0 Then
                litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Journal value must be greater than zero!</span>"
                Return 0
            End If

            If lintJournalId = 0 And CheckDuplicateRefIDForJournal(CCommon.ToLong(txtJournalReferenceNo.Text)) = True Then
                litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Journal Reference # is already used. Please specify new Journal Reference #.!</span>"
                Return 0
            End If

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                JournalId = SaveDataToHeader()
                If JournalId > 0 Then
                    SaveDataToGeneralJournalDetails(JournalId)
                End If

                objTransactionScope.Complete()
            End Using
            Return JournalId
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;This transaction can not be posted,Because transactions date belongs to closed financial year.</span>"
                Return 0
            ElseIf ex.Message = "IM_BALANCE" Then
                litMessage.Text = "<span class='btn btn-warning'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;The Credit and Debit amounts you�re entering must be of the same amount</span>"
                Return 0
            Else
                Throw ex
            End If
        End Try
    End Function

    Sub PageRedirect()
        Try
            If GetQueryStringVal("frm") = "RecurringTransaction" Then
                Response.Redirect("../Accounting/frmRecurringTransaction.aspx")
            ElseIf GetQueryStringVal("frm") = "GeneralLedger" Then
                Response.Redirect("../Accounting/frmGeneralLedger.aspx")
            ElseIf GetQueryStringVal("frm") = "BankRecon" Then
                Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & CCommon.ToLong(GetQueryStringVal("ReconcileID")))
            ElseIf GetQueryStringVal("frm") = "ItemDetail" Then
                Response.Redirect("../Items/frmKitDetails.aspx?ItemCode=" & CCommon.ToLong(GetQueryStringVal("ItemCode")))
            Else
                Response.Redirect("../Accounting/frmNewJournalEntry.aspx?flag=1&EntryNo=" & txtJournalReferenceNo.Text & "&ChartAcntId=" & CCommon.ToLong(GetQueryStringVal("ChartAcntId")))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CheckDuplicateRefIDForJournal(ByVal JournalRefNo As Long) As Boolean
        Dim blnResult As Boolean = False
        Try
            Dim objJEHeader As New JournalEntryHeader
            objJEHeader.DomainID = Session("DomainID")
            objJEHeader.JournalReferenceNo = JournalRefNo

            If objJEHeader.CheckDuplicateRefIDForJournal() > 0 Then
                blnResult = True
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return blnResult
    End Function

End Class