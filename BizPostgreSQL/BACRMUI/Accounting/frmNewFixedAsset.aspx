<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewFixedAsset.aspx.vb" Inherits=".frmNewFixedAsset" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1"  runat="server">
		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
           <title>New Chart of Accounts</title>
    <script language="Javascript" type ="text/javascript" >

function Save()
 {
   
  if (document.form1.txtName.value=="")
	{
    	alert("Enter Name");
    	document.form1.txtName.focus();
		return false;
	}

 
}

 function CheckNumber(cint)
 {
	if (cint==1)
	{
		if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
		{
					window.event.keyCode=0;
		}
	}
	if (cint==2)
	{
		if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
		{
				window.event.keyCode=0;
		}
	}
						
 }
	
	function HideTempPanel()
 {
    //alert("Siva");
    var a = null;
    var f = document.forms[0];
    var e = f.elements["rdlFixedAsset"];

    for (var i=0; i < e.length; i++)
    {
       if (e[i].checked)
        {
            a = e[i].value;
            break;
        }
    }
   // alert(a);
  if(a==0)
    {
        document.getElementById("pnlFixedAssetDetails").style.visibility='';
    }
  if(a==1)
    {
        document.getElementById("pnlFixedAssetDetails").style.visibility='hidden';
        //alert(document.getElementById("pnlFixedAssetDetails").style.visibility);        
    }
  
					
 }
 
		</script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>
</head>
<body>
    <form id="form1" runat="server" method ="post">
     <br />
  <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle" >
							<tr>
								<td >&nbsp;&nbsp;&nbsp; Fixed Asset &nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					
					<td align="right">
						<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
						
				</td>
				</tr>
			</table>
			
			<table id="Table2" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
			<td>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
						<table cellspacing="2" cellpadding="0" width="600px" border="0">
								<tr>
								<td class="normal1" align="right">Type&nbsp;
								</td>
								<td colspan="3" class="normal1">
								<b>Fixed Asset </b>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right" >Name<FONT color="red">*</FONT></td>
								<td colspan="3">
									<asp:textbox id="txtName" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:textbox></td>
							</tr>
												
							<tr>
								<td class="normal1" align="right" >Parent Category</td>
								<td colspan="3"><asp:dropdownlist cssclass="signup" id="ddlParentCategory" runat="server" Width="200">
										<asp:ListItem Value="0">--Select One--</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="normal1" align="right" >Category Description</td>
								<td colspan="3" class="normal1">
									<asp:textbox id="txtCategoryDescription" runat="server" TextMode="multiLine"  Width="150px" MaxLength="50" cssclass="signup"></asp:textbox></td>
							</tr>
								<tr>
								<td class="normal1" align="right" >Depreciation </td>
								<td colspan="3" class="normal1"> Do you want to track depreciation of this asset?
								
								</td>
							</tr>
							<tr>
							<td style="width:1000px">
							</td>
							<td class="normal1" align="left"    runat="server">
						     <asp:Panel ID="pnlRaido" runat ="server" >
							   <asp:RadioButtonList ID="rdlFixedAsset" runat="server" onclick="HideTempPanel();"  RepeatDirection="vertical">
							     <asp:ListItem Value="0" Selected="true" Text="Yes" ></asp:ListItem>
								 <asp:ListItem Value="1" Text="No"></asp:ListItem>
							 
								</asp:RadioButtonList> 	
												        						  
												  </asp:Panel>
							</td>
							</tr>	
							<tr>
							<td></td>
								<td class="normal1" align="left">Original Cost  &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;</td>
								<td class="normal1" align ="left">  <asp:textbox id="txtOriginalCost" runat="server" Width="75px" MaxLength="8" CssClass="signup"></asp:textbox>
							 	<asp:Label ID="lbloriginal" Width ="50px" Text= " as of " runat ="server" ></asp:Label></td><td align ="left"><BizCalendar:Calendar ID="CalOriginalCost" runat="server" /></td>
							</tr>	
											
							 <tr>
							 <td class="normal1" align="right" ></td>
							 <td colspan="3">
							<asp:Panel ID="pnlFixedAssetDetails" runat ="server">	
							<table cellspacing="2" width ="450px" cellpadding="0" border="0">
						 	<tr> 	
							<td class ="normal1">Depreciation Cost</td> 
							<td class ="normal1" ><asp:textbox id="txtDepreciationCost" runat="server" Width="75px" MaxLength="8" CssClass="signup"></asp:textbox>
							 	<asp:Label ID="Label1" Width ="50px" Text= " as of " runat ="server" ></asp:Label>
							</td>
							<td align ="left" ><BizCalendar:Calendar ID="CalDepreciatonCost" runat="server" /></td>
							 
	                        </tr>
							</table>
						   </asp:Panel>
							</td> 
							</tr> 
							
						</table>
					</asp:TableCell>
				    </asp:TableRow>
				</asp:table>
				</td>
				                                   </tr>
				                                   <tr align ="center"  >
				                                   <td  class="normal4" valign="middle">
				                                   <asp:literal id="litMessage" Runat="server" EnableViewState="False"></asp:literal> 
				                                   </td>
				                                   </tr>
			</table> 	                                   
		    <asp:HiddenField ID="HdnumListItemId" runat ="server" />	
    </form>
</body>
</html>
