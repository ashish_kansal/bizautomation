﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmRecurringTransactionDetailReport
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                BindGrid()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub dgRep_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRep.ItemCommand
        Try
            If e.CommandName = "RecTemp" Then
                Response.Redirect("../Accounting/frmRecurringEvents.aspx?RecurringId=" & e.Item.Cells(1).Text, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim recEvent As New RecurringEvents
            Dim dtRep As DataTable
            recEvent.DomainId = Session("DomainID")
            recEvent.PageSize = Session("PagingRows")
            dtRep = recEvent.GetRecurringTransactionReport(1)
            If (dtRep.Rows.Count > 0) Then
                dgRep.DataSource = dtRep
                dgRep.DataBind()
                'lblNoRecords.Text = dgRep.Items.Count
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class