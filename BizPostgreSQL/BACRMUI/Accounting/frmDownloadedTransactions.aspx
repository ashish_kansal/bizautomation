﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmDownloadedTransactions.aspx.vb" Inherits=".DownloadedTransactions" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Downloaded Transactions</title>
    <style type="text/css">
        .info {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }

        .VisibleNone {
            display: none;
        }
    </style>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {

            if ($(".rtsSelected").text() == 'Unaccepted Transactions') {
                DoPaging();
            }
            else if ($(".rtsSelected").text() == 'Accepted Transactions') {
                DoPaging1();
            }

            $(".rtsLink").on("click", function () {
                if ($(this).text() == 'Unaccepted Transactions') {
                    DoPaging();
                }
                else if ($(this).text() == 'Accepted Transactions') {
                    DoPaging1();
                }
            });
            FillDropDowns();
        });
        

        function ShowHide(a) {
            if (a == 0) {
                //alert(0);
                $('#bizPager').show();
                $('#bizPager1').hide();
            }
            else {
                //alert(1);
                $('#bizPager').hide();
                $('#bizPager1').show();
            }
        }

        function CategorizeTransactions()
        {
            var CategorizeAccId = $('#hdnFldCategorizeAccId').val();
            $('#<%=gvReconcile.ClientID%> tr').each(function () {
                var isChecked = $(this).find('#chk').is(':checked');
                if (isChecked == true) {
                    $(this).find('#ddlAccounts option').each(function () {
                        if ($(this).val() == CategorizeAccId){
                            $(this).attr("selected", true);
                        }
                    });
                }
            });
        }

        function FillDropDowns() {
            var Expense = 'UnCategorized Expense';
            var Income = 'UnCategorized Income';
            var id = '';

            $('#<%=gvReconcile.ClientID%> tr').each(function () {
                $(this).find('#ddlAccounts').each(function () {
                    $(this).html($('#ddlCOAccounts1').html())
                });
                $(this).find('#ddlProject').each(function () {
                    $(this).html($('#ddlProject1').html())
                });

                $(this).find('#ddlClass').each(function () {
                    $(this).html($('#ddlClass1').html())
                });
            });


            $('#<%=gvReconcile.ClientID%> tr').each(function () {
                var $ctl = $(this).find('#ddlAccounts');
                var lblReferenceTypeID = $(this).find('#lblReferenceTypeID').text();
                if ($ctl != null) {
                    if (lblReferenceTypeID > 0) {
                        if (lblReferenceTypeID == 1) {
                            $(this).find('#ddlAccounts option').each(function () {
                                if ($(this).text() == Expense) {
                                    $(this).attr("selected", true);
                                }
                            });
                        }
                        else if (lblReferenceTypeID == 6) {
                            $(this).find('#ddlAccounts option').each(function () {
                                if ($(this).text() == Income) {
                                    $(this).attr("selected", true);
                                }
                            });
                        }
                    }
                }
            });
        }
        function DoPaging() {
            ShowHide(0);
            //fn_doPage = function (pgno) {
            //    //console.log('UnacceptedTransactions Sub tab');
            //    $('#txtCurrentPageCorr').val(pgno);
            //    if ($('#txtCurrentPageCorr').val() != 0 || $('#txtCurrentPageCorr').val() > 0) {
            //        $('#btnCorresGo').trigger('click');
            //    }
            //}
            fn_doPage = function (pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {

                var currentPage;
                var postbackButton;

                if (CurrentPageWebControlClientID != '') {
                    currentPage = CurrentPageWebControlClientID;
                }
                else {
                    currentPage = 'txtCurrrentPage';
                }

                if (PostbackButtonClientID != '') {
                    postbackButton = PostbackButtonClientID;
                }
                else {
                    postbackButton = 'btnGo1';
                }

                $('#' + currentPage + '').val(pgno);
                if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                    $('#' + postbackButton + '').trigger('click');
                }
            }
        }

        function DoPaging1() {
            ShowHide(1);
            //fn_doPage = function (pgno) {
            //    //console.log('AcceptedTransactions Sub tab');
            //    $('#txtCurrentPageCorr1').val(pgno);
            //    if ($('#txtCurrentPageCorr1').val() != 0 || $('#txtCurrentPageCorr1').val() > 0) {
            //        $('#btnCorresGo1').trigger('click');
            //    }
            //}
            fn_doPage = function (pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {

                var currentPage;
                var postbackButton;

                if (CurrentPageWebControlClientID != '') {
                    currentPage = CurrentPageWebControlClientID;
                }
                else {
                    currentPage = 'txtCurrrentPage';
                }

                if (PostbackButtonClientID != '') {
                    postbackButton = PostbackButtonClientID;
                }
                else {
                    postbackButton = 'btnGo1';
                }

                $('#' + currentPage + '').val(pgno);
                if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                    $('#' + postbackButton + '').trigger('click');
                }
            }
        }

        function OpenSetting(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenTransaction(TransactionID) {
            window.open("~/EBanking/frmTransactionDetails.aspx?TransactionID=" & TransactionID, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

        function OpenWriteCheck(TransactionID, SplitFlag) {
            window.open("../Accounting/frmWriteCheck.aspx?TransactionID=" + TransactionID + "&SplitFlag=" + SplitFlag, "", "width=1100,height=600,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenMakeDeposit(TransactionID, SplitFlag) {
            window.open("../Accounting/frmMakeDeposit.aspx?TransactionID=" + TransactionID + "&SplitFlag=" + SplitFlag, "", "width=1100,height=600,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenCategorize() {
            var OpenCategorizeTransaction = false;
            $('#<%=gvReconcile.ClientID%> tr').each(function () {

                 var isChecked = $(this).find('#chk').is(':checked');
                 if (isChecked == true) {
                     OpenCategorizeTransaction = true;
                 }
             });
            if (OpenCategorizeTransaction != true) {
                alert('Please select a Transaction to Categorize');
                return false;
            }
            else {
                window.open("../Accounting/frmCategorizeTransaction.aspx", "", "width=1100,height=600,status=no,scrollbars=yes,left=155,top=160");
            }
            return false;
        }
        function LookMatchingTrans(TransactionId, ReferenceTypeId) {
            window.location.href = "../EBanking/frmFindMatchingTransaction.aspx?TransactionID=" + TransactionId + "&ReferenceTypeId=" + ReferenceTypeId;
            return false;
        }

        function pageLoad() {
            var manager = Sys.WebForms.PageRequestManager.getInstance();
            manager.add_beginRequest(OnBeginRequest);
            manager.add_endRequest(OnEndRequest);
        }
        function OnBeginRequest(sender, args) {
            $get('UpdateProgress1').style.display = "block";
            $("#table-main-part").addClass("Background");
        }
        function OnEndRequest(sender, args) {
            $get('UpdateProgress1').style.display = "none";
            $("#table-main-part").removeClass("Background");
            ShowHideAssign();
            FixOnReload();
        }

    </script>
    <style type="text/css">
        .fixpanel {
            width: auto;
            overflow: hidden;
            display: inline-block;
            white-space: nowrap;
            height: auto;
        }

        .displaynone {
            display: none;
        }

        #gvReconcile {
            border: 1px solid #CBCBCB;
        }

        #gvAcceptedTrans {
            border: 1px solid #CBCBCB;
        }

        .header {
            background: #E5E5E5 !important;
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #CBCBCB;
            border-left: 1px solid #CBCBCB;
            font-weight: bold;
            line-height: 50px;
        }

        th {
            padding-left: 5px;
            padding-right: 5px;
        }

        #gvReconcile .is, .ais {
            /*background: #E5E5E5!important;*/
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #fff;
            border-left: 1px solid #fff; /*font-weight: bold;*/
            padding: 0 0 0 10px; /*line-height: 50px;*/
        }

            #gvReconcile .is td:first-child, .ais td:first-child {
                border-left: 0px solid #CBCBCB;
            }

            #gvReconcile .is td:last-child, .ais td:last-child {
                border-right: 0px solid #CBCBCB;
            }

        #gvAcceptedTrans .is, .ais {
            /*background: #E5E5E5!important;*/
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #fff;
            border-left: 1px solid #fff; /*font-weight: bold;*/
            padding: 0 0 0 10px; /*line-height: 50px;*/
        }

            #gvAcceptedTrans .is td:first-child, .ais td:first-child {
                border-left: 0px solid #CBCBCB;
            }

            #gvAcceptedTrans .is td:last-child, .ais td:last-child {
                border-right: 0px solid #CBCBCB;
            }

        #tblAssign .NA td:first-child, td:first-child, td:last-child, td:last-child {
            border-width: 0 0 0 0;
        }

        .AddNewLabel, .SplitLabel {
            padding: 0 3px;
            color: white;
            background-color: #4E9E19;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            font-size: 0.8em;
            margin-right: 3px;
            margin-top: 1px;
            float: left;
        }
    </style>
    <script type="text/javascript">
        $(document).on("ready", function () {
            FixOnReload();
            $(".accept").on("click", function () {

                var CurrentRow = $(this).parents(".is,.ais");
                var ddlAccount = $(CurrentRow).find("#ddlAccounts");
                var rdbAssign = $(CurrentRow).find("#rdbAssign");
                var rdbMatch = $(CurrentRow).find("#rdbMatch");
                var ddlProject = $(CurrentRow).find("#ddlProject");
                var ddlClass = $(CurrentRow).find("#ddlClass");
                var lblTransactionId = $(CurrentRow).find("#lblTransactionId");

                var HiddenValue = '';
                if ($(rdbAssign).is(":checked") == false && $(rdbMatch).is(":checked") == false) {
                    alert('Please select Assign Or Match before accept Transaction');
                    $(rdbAssign).focus();
                    return false;
                }
                if ($(rdbAssign).is(":checked")) {
                    if ($(ddlAccount).val() == '0') {
                        alert('Please select a valid Chart Of Account');
                        $(ddlAccount).focus();
                        return false;
                    }
                }
                HiddenValue = $(lblTransactionId).text() + ',' + $(ddlAccount).val() + ',' + $(ddlProject).val() + ',' + $(ddlClass).val() + '|';
                $("#hdnFldAcceptedTrans").val(HiddenValue);
               // alert('hdnFldAcceptedTrans Value : ' + $("#hdnFldAcceptedTrans").val());
                return true;
            });


            
        });
        function ValidateAcceptSelected() {
            var AcceptSelected = false;
            var HiddenValue = '';
                $('#<%=gvReconcile.ClientID%> tr').each(function () {

                    var isChecked = $(this).find('#chk').is(':checked');
                    if (isChecked == true) {
                        AcceptSelected = true;
                        var ddlAccount = $(this).find("#ddlAccounts");
                        var ddlProject = $(this).find("#ddlProject");
                        var ddlClass = $(this).find("#ddlClass");
                        var lblTransactionId = $(this).find("#lblTransactionId");
                        HiddenValue += $(lblTransactionId).text() + ',' + $(ddlAccount).val() + ',' + $(ddlProject).val() + ',' + $(ddlClass).val() + '|';
                        $("#hdnFldAcceptedTrans").val(HiddenValue);
                       // alert('hdnFldAcceptedTrans Value : ' + $("#hdnFldAcceptedTrans").val());
                    }
                });
                if (AcceptSelected != true) {
                    alert('Please select a Transaction to accept');
                    return false;
                }
                return true;
        }
        function ValidateExcludeTransaction() {
            var ExcludeTransaction = false;
            $('#<%=gvReconcile.ClientID%> tr').each(function () {

                    var isChecked = $(this).find('#chk').is(':checked');
                    if (isChecked == true) {
                        ExcludeTransaction = true;
                    }
                });
            if (ExcludeTransaction != true) {
                    alert('Please select a Transaction to exclude');
                    return false;
                }
                return true;
            }
        function FixOnReload() {
            $(".Assign").on("click", function () {
                var CurrentRow = $(this).parents(".is,.ais")
                $(CurrentRow).find(".trAssign").show();
                $(CurrentRow).find(".trMatch").hide();
            });

            $(".Match").on("click", function () {
                var CurrentRow = $(this).parents(".is,.ais")
                $(CurrentRow).find(".trMatch").show();
                $(CurrentRow).find(".trAssign").hide();
            });
            $(".Assign").css({ "visibility": "hidden" });
            $(".Match").css({ "visibility": "hidden" });
            $(".trMatch").hide();
            $(".is,.ais").on("mouseover", function () {
                $(this).find(".Assign").css({ "visibility": "visible" });
                $(this).find(".Match").css({ "visibility": "visible" });
            });

            $(".is,.ais").on("mouseout", function () {
                $(this).find(".Assign").css({ "visibility": "hidden" });
                $(this).find(".Match").css({ "visibility": "hidden" });
            });
            $(".ddlAccounts option").each(function () {
                if ($(this).val() == 1) {
                    $(this).addClass("AddNewLabel");
                }
                else if ($(this).val() == 2) {
                    $(this).addClass("SplitLabel");
                }
                else
                    $(this).css('backgroundColor', '#fff4bf');
            });
            $(".ddlAccounts optgroup").css('backgroundColor', '#fff4bf');

            $(".ddlAccounts").on("change", function () {
                if ($(this).val() == 2) {
                    var CurrentRow = $(this).parents(".is,.ais");

                    $(CurrentRow).find("#lblTransactionId").text();
                    if (Number($(CurrentRow).find("#lblAmount").text()) < 0)
                        OpenWriteCheck($(CurrentRow).find("#lblTransactionId").text(), 1);
                    else
                        OpenMakeDeposit($(CurrentRow).find("#lblTransactionId").text(), 1);
                }
            });

            $(".ddlMatchTrans option").each(function () {
                if ($(this).val() == 2) {
                    $(this).addClass("SplitLabel");
                }
                else
                    $(this).css('backgroundColor', '#fff4bf');
            });
            ShowHideAssign();
            -

                        $(".ddlMatchTrans").on("change", function () {
                            if ($(this).val() == 2) {
                                var CurrentRow = $(this).parents(".is,.ais");
                                $(CurrentRow).find("#lblTransactionId").text();
                                if (Number($(CurrentRow).find("#lblAmount").text()) < 0)
                                    LookMatchingTrans($(CurrentRow).find("#lblTransactionId").text(), 1);
                                else
                                    LookMatchingTrans($(CurrentRow).find("#lblTransactionId").text(), 6);
                            }
                        });

        }
        function ShowHideAssign() {
            $(".tblassign1").each(function () {

                var rdbAssign = $(this).find("#rdbAssign");
                var rdbMatch = $(this).find("#rdbMatch");

                if ($(rdbMatch).is(":checked")) {
                    $(this).find(".trMatch").show();
                    $(this).find(".trAssign").hide();
                } else {

                    $(this).find(".trMatch").hide();
                    $(this).find(".trAssign").show();
                }

            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
        <tr align="center">
            <td class="normal4" valign="middle">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Downloaded Transactions
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;" CurrentPageWebControlClientID="txtCurrentPageCorr" PostbackButtonClientID="btnCorresGo">
    </webdiyer:AspNetPager>
    <webdiyer:AspNetPager ID="bizPager1" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;" CurrentPageWebControlClientID="txtCurrentPageCorr1" PostbackButtonClientID="btnCorresGo1">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table cellspacing="2" cellpadding="0" border="0" align="center" width="100%">
        <tr align="left">
            <td>
                <table>
                    <tr>
                        <td style="font-weight: bold;">Account :
                        </td>
                        <td align="left">
                            <telerik:RadComboBox ID="radcmbCOAccounts" Width="195px" DropDownWidth="300px" Skin="Default"
                                runat="server" AllowCustomText="False" EnableLoadOnDemand="False" AccessKey="A" AutoPostBack="true" OnSelectedIndexChanged="radcmbCOAccounts_SelectedIndexChanged"
                                 ClientIDMode="AutoID"  >
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Value="0" Text="-- Select --" />
                                </Items>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <asp:Label runat="server" ID="lblAccName" Text='<%# DataBinder.Eval(Container.DataItem, "vcAccountName")%>'></asp:Label>
                                            </td>
                                            <td></td>
                                            <td style="font-weight: bold;">
                                                <asp:Label runat="server" EnableViewState="false" ID="Label1" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.numOpeningBal")) %>'></asp:Label>
                                            </td>
                                            <td style="display: none;">
                                                <asp:Label runat="server" ID="radCOAccountID" Text='<%# DataBinder.Eval(Container.DataItem, "numAccountId")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="Label2"  EnableViewState="false" Text='<%# DataBinder.Eval(Container.DataItem, "vcFIName")%>'></asp:Label>
                                            </td>
                                            <td></td>
                                            <td>
                                                <asp:Label runat="server" ID="Label3" EnableViewState="false" Text='<%# DataBinder.Eval(Container.DataItem, "LastUpdated")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                             <asp:HiddenField runat="server" ID="hdnFldAcceptedTrans" />
                            <asp:HiddenField runat="server" ID="hdnFldCOAccountId" />
                            <asp:Button ID="btnHiddenSave" runat="server" Style="visibility: hidden"></asp:Button>
                            <asp:HiddenField runat="server" ID="hdnFldCategorizeAccId" />
                            <asp:Button ID="btnHiddenCategorizeSelect" runat="server" Style="visibility: hidden" OnClientClick="javascript:CategorizeTransactions(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="text-align: center;">
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="uppnlTransList">
            <ProgressTemplate>
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <telerik:RadTabStrip ID="radTransactions" runat="server" UnSelectChildren="True"
        EnableEmbeddedSkins="true" Skin="Default" ClickSelectedTab="true" SelectedIndex="0"
        MultiPageID="radMultiPage_Transactions" ClientIDMode="Static" AutoPostBack="true">
        <Tabs>
            <telerik:RadTab Text="<span id='spnUnAcc' onclick='DoPaging();'>Unaccepted Transactions</span>"
                Value="UnacceptedTransactions" PageViewID="radPageView_UnacceptedTransactions"
                PostBack="false">
            </telerik:RadTab>
            <telerik:RadTab Text="<span id='spnAcc' onclick='DoPaging1();'>Accepted Transactions</span>"
                Value="AcceptedTransactions" PageViewID="radPageView_AcceptedTransactions" PostBack="false">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_Transactions" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_UnacceptedTransactions" runat="server">
            <table cellspacing="2" cellpadding="0" border="0" align="center" width="100%">
                <tr>
                    <td>
                        <asp:Button ID="btnAcceptSelected" runat="server" Text="Accept Selected" CssClass="button" OnClientClick="javascript:return ValidateAcceptSelected();"  EnableViewState="false"/>
                        &nbsp;
                        <asp:Button ID="btnExclude" runat="server" OnClientClick="javascript:return ValidateExcludeTransaction();"   Text="Exclude" CssClass="button"  EnableViewState="false" />
                        &nbsp;
                        <asp:Button ID="btnCategorize" runat="server" Text="Categorize" CssClass="button"
                            OnClientClick="javascript:OpenCategorize(); return false;"  EnableViewState="false" />
                    </td>
                </tr>
                <tr>
                    <td align="right" id="hidePaging" runat="server">
                        <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
                        <asp:Button ID="btnCorresGo" Width="25" runat="server" Style="display: none" />
                        <asp:TextBox ID="txtCurrentPageCorr" runat="server" Style="display: none"></asp:TextBox>
                        <asp:DropDownList ID="ddlCOAccounts1" runat="server" Style="display: none"></asp:DropDownList>
                        <asp:DropDownList ID="ddlProject1" runat="server" Style="display: none"></asp:DropDownList>
                        <asp:DropDownList ID="ddlClass1" runat="server" Style="display: none"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="uppnlTransList" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="radcmbCOAccounts" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="btnAcceptSelected" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnHiddenCategorizeSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnExclude" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr align="center">
                                        <td class="normal4" valign="middle">
                                            <asp:Literal ID="litMessage1" runat="server" EnableViewState="False"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvReconcile" runat="server" CssClass="" AutoGenerateColumns="False"
                                    Width="100%" DataKeyNames="numTransactionID" ClientIDMode="Static">
                                    <AlternatingRowStyle CssClass="ais" />
                                    <RowStyle CssClass="is row" />
                                    <HeaderStyle CssClass="header" />
                                    <Columns>
                                        <%--<asp:ButtonField Text="SingleClick" CommandName="SingleClick" Visible="false" />
                                        <asp:ButtonField Text="DoubleClick" CommandName="DoubleClick" Visible="false" />--%>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="SelectAllCheckBox" runat="server" onclick="SelectAll('SelectAllCheckBox', 'chkSelected');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDatePosted" runat="server" Text='<%# Eval("dtDatePosted") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCheckNumber" runat="server" Text='<%# Eval("vcCheckNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payee">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPayeeName" runat="server" Text='<%# Eval("vcPayeeName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Memo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMemo" runat="server" Text='<%# Eval("vcMemo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFITransactionID" runat="server" Text='<%#Eval("vcFITransactionID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monAmount")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                            HeaderText="Assign or Match">
                                            <ItemTemplate>
                                                <table id="tblAssign" clientidmode="Static" runat="server" cellpadding="0" cellspacing="0"
                                                    height="50px" width="557px">
                                                    <tr class="NA">
                                                        <td valign="bottom">
                                                            <table class="tblassign1" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr class="NA">
                                                                    <td>
                                                                        <asp:Label ID="lblTransactionId" Style="display: none" runat="server" Text='<%# Eval("numTransactionID") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="left">
                                                                        <asp:RadioButton runat="server" ID="rdbAssign" CssClass="Assign" Text="Assign" Checked="true"
                                                                            GroupName="grpAssignMatch" />
                                                                        <asp:RadioButton runat="server" ID="rdbMatch" CssClass="Match" Text="Match" GroupName="grpAssignMatch" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="NA">
                                                                    <td colspan="4" valign="top">
                                                                        <table class="trAssign" cellpadding="2" cellspacing="2" border="0" height="25px">
                                                                            <tr class="NA">
                                                                                <td>
                                                                                    <asp:Label ID="lblName" runat="server" Style="display: none" Text=""></asp:Label>
                                                                                    <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="195px" DropDownWidth="300px"
                                                                                        Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True"
                                                                                        ClientIDMode="AutoID" Text='<%# Eval("vcPayeeName") %>'>
                                                                                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                                                    </telerik:RadComboBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlAccounts" EnableViewState="true" runat="server" Width="120px" CssClass="ddlAccounts">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlProject" EnableViewState="true" runat="server" Width="120px" CssClass="signup">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlClass" EnableViewState="true" runat="server" Width="120px" CssClass="signup">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table class="trMatch" cellpadding="0" cellspacing="0" width="100%" height="25px">
                                                                            <tr class="NA">
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlMatchTrans" CssClass="ddlMatchTrans" runat="server" Width="320px"
                                                                                        Visible="false">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton runat="server" CommandName="GetMatchedTrans" CommandArgument='<%#Eval("numTransactionID") %>'
                                                                                        ID="lnkbtnLookMatchingTrans" Text="Look for matching Transactions"></asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <%--<asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="" Visible="false">
                                            <ItemTemplate>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Accept" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                 <asp:Label ID="lblReferenceTypeID" CssClass="VisibleNone" runat="server" Text='<%#Eval("ReferenceTypeId") %>'></asp:Label>
                                                <asp:Label ID="lblReferenceID" CssClass="VisibleNone" runat="server" Text='<%# Eval("numReferenceID") %>'></asp:Label>
                                                <asp:Button ID="btnAccept" runat="server" Text="Accept" CssClass="button accept"
                                                    CommandArgument='<%#Eval("numTransactionID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                    <tr align="center">
                                        <td class="normal4" align="right">
                                             <asp:Button ID="btnRefreshTransaction" CssClass="button" OnClick="btnRefreshTransaction_Click" Width="125" runat="server" Text="Refresh Transaction"/>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </telerik:RadPageView>

        <telerik:RadPageView ID="radPageView_AcceptedTransactions" runat="server">
            <script language="javascript" type="text/javascript">
                //                $(document).ready(function () {
                //                    fn_doPage = function (pgno) {
                //                        //console.log('Accounting Sub tab');
                //                        $('#txtCurrentPageCorr1').val(pgno);
                //                        if ($('#txtCurrentPageCorr1').val() != 0 || $('#txtCurrentPageCorr1').val() > 0) {
                //                            $('#btnCorresGo1').trigger('click');
                //                        }
                //                    }
                //                });


            </script>
            <table cellspacing="2" cellpadding="0" border="0" align="center" width="100%">
                <tr>
                    <td align="right" id="Td1" runat="server">
                        <asp:TextBox ID="txtSortColumn1" runat="server" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="txtSortOrder1" runat="server" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="txtSortChar1" Text="0" Style="display: none" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtGridColumnFilter1" Style="display: none" runat="server"></asp:TextBox>
                        <asp:Button ID="btnCorresGo1" Width="25" runat="server" Style="display: none" />
                        <asp:TextBox ID="txtCurrentPageCorr1" runat="server" Style="display: none"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="upnlAcceptedTrans" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="radcmbCOAccounts" EventName="SelectedIndexChanged" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:GridView ID="gvAcceptedTrans" runat="server" AutoGenerateColumns="False" Width="100%"
                                    DataKeyNames="numTransactionID" ClientIDMode="Static">
                                    <AlternatingRowStyle CssClass="ais" />
                                    <RowStyle CssClass="is NA" Height="40px" />
                                    <HeaderStyle CssClass="header" />
                                    <Columns>
                                       
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblATTransactionID" runat="server" Text='<%# Eval("numTransactionID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblATDatePosted" runat="server" Text='<%# Eval("dtDatePosted") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lblATDescription" runat="server" Text='<%# Eval("vcDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderText="Amount">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblATAmount" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monAmount")) %>'></asp:Label>--%>
                                                <asp:Label ID="Label4" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.MAmount")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Assign or Match">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="lnkbtnTransMatchDetails" Text='<%#Eval("TransMatchDetails")%>'></asp:LinkButton>
                                                 <asp:Label ID="lblATReferenceTypeID" CssClass="VisibleNone" runat="server" Text='<%# Eval("tintReferenceType") %>'></asp:Label>
                                                 <asp:Label ID="lblATReferenceID"  CssClass="VisibleNone" runat="server" Text='<%# Eval("numReferenceID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                            <ItemTemplate>
                                               
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                            <ItemTemplate>
                                               
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>

    <asp:Panel runat="server" Visible="false" ID="pnlNoBankAccounts" CssClass="info">
        <b>No Bank Accounts Linked</b><br />
        No bank accounts found!., To add one go to Chart Of Accounts and click on 'link your bank account'
    </asp:Panel>

</asp:Content>
