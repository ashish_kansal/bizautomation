﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.UserInterface
Imports System.Data.SqlClient

Public Class frmAccountsReceivableInvoice
    Inherits BACRMPage
    Dim lngDivId As Long
    Dim decTotalAmount, decAmountPaid, decBalanceDue As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivId = CCommon.ToLong(GetQueryStringVal("DivId"))

            If Not IsPostBack Then

                LoadAccountReceivableDetailGrid()
            End If

            ibExportExcel.Attributes.Add("onclick", "return GrabHTML()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadAccountReceivableDetailGrid()
        Dim objVendorPayment As New VendorPayment
        Dim dtARAging As DataTable
        Try
            objVendorPayment.DomainID = Session("DomainID")
            objVendorPayment.CompanyID = lngDivId
            objVendorPayment.Flag = ddlRange.SelectedValue
            objVendorPayment.AccountClass = CCommon.ToLong(GetQueryStringVal("accountClass"))
            objVendorPayment.dtFromDate = Convert.ToDateTime(GetQueryStringVal("fromDate"))
            objVendorPayment.dtTodate = Convert.ToDateTime(GetQueryStringVal("toDate"))
            objVendorPayment.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objVendorPayment.RegularSearch = GridColumnSearchCriteria()
            objVendorPayment.BasedOn = If(CCommon.ToShort(GetQueryStringVal("BasedOn")) = 0, 1, CCommon.ToShort(GetQueryStringVal("BasedOn")))
            dtARAging = objVendorPayment.GetAccountReceivableAging_Invoice
            'If (dtARAging.Rows.Count > 0) Then

            decTotalAmount = 0
            decAmountPaid = 0
            decBalanceDue = 0

            If dtARAging.Rows.Count > 0 Then
                decTotalAmount = dtARAging.Compute("Sum(TotalAmount)", String.Empty)
                decAmountPaid = dtARAging.Compute("Sum(AmountPaid)", String.Empty)
                decBalanceDue = dtARAging.Compute("Sum(BalanceDue)", String.Empty)
            End If
            'dtARAging = dtARAging.Select("numOppId < 2347").CopyToDataTable()
            gvARDetails.DataSource = dtARAging
            gvARDetails.DataBind()

            If lngDivId > 0 Then
                gvARDetails.Columns(0).Visible = False

                Dim row As New GridViewRow(0, -1, DataControlRowType.Header, DataControlRowState.Normal)
                Dim th As TableCell = New TableHeaderCell()

                th.HorizontalAlign = HorizontalAlign.Center
                th.ColumnSpan = gvARDetails.Columns.Count - 1
                th.HorizontalAlign = HorizontalAlign.Center
                th.Text = dtARAging.Rows(0)("vcCompanyName")
                row.Cells.Add(th)

                DirectCast(gvARDetails.Controls(0), Table).Rows.AddAt(0, row)
            End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GridColumnSearchCriteria() As String
        Try
            Dim RegularSearch As String = ""

            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                Dim strIDValue(), strID(), strCustom As String
                Dim strRegularCondition As New ArrayList
                Dim strCustomCondition As New ArrayList


                For i As Integer = 0 To strValues.Length - 1
                    strIDValue = strValues(i).Split(":")

                    Select Case strIDValue(0)
                        Case "txtOrganization"
                            strRegularCondition.Add("vcCompanyName ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                        Case "txtInvoice"
                            strRegularCondition.Add("(vcBizDocID ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR vcPOppName ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                        Case "ddlARAccount"
                            strRegularCondition.Add("numChartAcntId=" & strIDValue(1))
                    End Select
                Next

                RegularSearch = String.Join(" AND ", strRegularCondition.ToArray())
            End If

            Return RegularSearch
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub ibExportExcel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ibExportExcel.Click
        'ExportToExcel.DataGridToExcel(gvARDetails, Response)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=FileName" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".xls")
        Response.Charset = ""
        'response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Response.Write(hdnHTML.Value)
        Response.End()
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Private Sub ddlRange_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlRange.SelectedIndexChanged
        Try
            LoadAccountReceivableDetailGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvARDetails_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvARDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                If Not e.Row.FindControl("ddlARAccount") Is Nothing Then
                    Dim ddlAR As DropDownList = DirectCast(e.Row.FindControl("ddlARAccount"), DropDownList)

                    Dim objCOA As New ChartOfAccounting
                    objCOA.DomainID = CCommon.ToLong(Session("DomainID"))
                    Dim dt As DataTable = objCOA.GetDefaultARAndChildAccounts()
                    ddlAR.DataSource = dt
                    ddlAR.DataTextField = "vcAccountName"
                    ddlAR.DataValueField = "numAccountID"
                    ddlAR.DataBind()

                    ddlAR.Items.Insert(0, New ListItem("--Select One --", "0"))
                End If

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue() As String

                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        Select Case strIDValue(0)
                            Case "txtOrganization"
                                DirectCast(e.Row.FindControl("txtOrganization"), TextBox).Text = strIDValue(1)
                            Case "txtInvoice"
                                DirectCast(e.Row.FindControl("txtInvoice"), TextBox).Text = strIDValue(1)
                            Case "ddlARAccount"
                                If Not DirectCast(e.Row.FindControl("ddlARAccount"), DropDownList).Items.FindByValue(strIDValue(1)) Is Nothing Then
                                    DirectCast(e.Row.FindControl("ddlARAccount"), DropDownList).Items.FindByValue(strIDValue(1)).Selected = True
                                End If
                        End Select
                    Next
                End If
            End If

            If e.Row.RowType = ListItemType.Footer Then
                CType(e.Row.FindControl("lblTotalAmount"), Label).Text = ReturnMoney(decTotalAmount)
                CType(e.Row.FindControl("lblAmountPaid"), Label).Text = ReturnMoney(decAmountPaid)
                CType(e.Row.FindControl("lblBalanceDue"), Label).Text = ReturnMoney(decBalanceDue)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub btnGo1_Click(sender As Object, e As EventArgs)
        Try
            LoadAccountReceivableDetailGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class