﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmAccountsReports.aspx.vb" Inherits=".frmAccountsReports" %>


<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style type="text/css">
        .rgAltRow1, tr.rgAltRow1 td
        {
            background-color: #f2f2f2 !important;
        }

        .rgRow1, tr.rgRow1 td
        {
            background-color: #fff !important;
        }
                 .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
        
   .hs {
        border: 1px solid #e4e4e4;
        border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    text-align: center;
    font-weight:bold;
        }
    </style>
    <script>
        var arrFieldConfig = new Array();
        arrFieldConfig[0] = new InlineEditValidation('426', 'False', 'False', 'False', 'False', 'False', 'False', '0', '0', 'Check #');
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
    <div class="col-md-12" style="z-index:!9">
        <div class="pull-left">
            <div class="form-inline">
                 <div class="form-group">
                      <asp:Panel runat="server" ID="pnlAccountingClass">
                     <label>Class</label>
                      <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="signup form-control" AutoPostBack="true">
                            </asp:DropDownList>
                    </asp:Panel>
                 </div>
                 <div class="form-group">
                     <label>
                            From
                        </label>
                      <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />  
                 </div>
                 <div class="form-group">
                    <label>
                            To</label>
                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                </div>
                <div class="form-group">
                     <asp:LinkButton ID="btnGo" runat="server" CssClass="button btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <asp:UpdatePanel ID="updateExcel" runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="btnExportExcel" runat="server"  CssClass="button btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    <asp:Label Text="Accounts Reports" runat="server" ID="lblReportName" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmaccountsreports.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:GridView ID="gvCheckRegister" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Check #" DataField="numCheckNo" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Date" DataField="dtCheckDate" ItemStyle-HorizontalAlign="Left"
                HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
            <asp:BoundField HeaderText="Payee" DataField="vcCompanyName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("monAmount"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Reference #" DataField="vcReference" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true"></asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvSalesJournalDetail" runat="server" BorderWidth="0" CssClass="tbl  table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numDebitAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numCreditAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvPurchaseJournal" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numDebitAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numCreditAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvInvoiceRegister" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Invoice/CM #" DataField="vcBizDocID" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Date" DataField="FromDate" ItemStyle-HorizontalAlign="Left"
                HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
            <asp:BoundField HeaderText="Quote No" DataField="vcPOppName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Name" DataField="vcCompanyName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Deal Amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("monDealAMount"))%>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="BizDoc Amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("monBizDocAmount"))%>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("monAmountPaid"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvReceiptJournalSummary" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numDebitAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numCreditAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvReceiptJournalDetail" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <%-- <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />--%>
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Reference" DataField="numReference" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Date" DataField="datEntry_Date" ItemStyle-HorizontalAlign="Left"
                HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numDebitAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numCreditAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvPaymentsJournalSummary" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numDebitAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numCreditAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvPaymentsJournalDetail" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <%-- <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />--%>
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Reference" DataField="numReference" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Date" DataField="datEntry_Date" ItemStyle-HorizontalAlign="Left"
                HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
            <asp:BoundField HeaderText="Check #" DataField="numCheckNo" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numDebitAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("numCreditAmt"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:GridView ID="gvCreditCard" runat="server" BorderWidth="0" CssClass="tbl table table-responsive table-bordered" AutoGenerateColumns="False"
        Style="margin-right: 0px" Width="100%" ShowFooter="true" FooterStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <FooterStyle CssClass="rgAltRow1" />
        <Columns>
            <asp:BoundField HeaderText="Date" DataField="dtPaymentDate" ItemStyle-HorizontalAlign="Left"
                HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
            <asp:BoundField HeaderText="Payee" DataField="vcCompanyName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:BoundField HeaderText="Account" DataField="vcAccountName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
            <asp:TemplateField HeaderText="Payment Amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("monPaymentAmount"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Applied Amount" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# ReturnMoney(Eval("monAppliedAmount"))%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
