'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmBalanceSheet

    '''<summary>
    '''pnlAccountingClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAccountingClass As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlUserLevelClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlUserLevelClass As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''calFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calFrom As Global.BACRM.Include.calandar

    '''<summary>
    '''calTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calTo As Global.BACRM.Include.calandar

    '''<summary>
    '''btnGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGo As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''updatepanelexcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updatepanelexcel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''btnExportExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportExcel As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''tblBalanceSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblBalanceSheet As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''tblExport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblExport As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''RepBalanceSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RepBalanceSheet As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''UpdateProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress
End Class
