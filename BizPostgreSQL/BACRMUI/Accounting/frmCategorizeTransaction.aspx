﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCategorizeTransaction.aspx.vb" 
 MasterPageFile="~/common/Popup.Master" Inherits=".frmCategorizeTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function SetCOAccountId(COAccountID) {
            window.opener.document.getElementById('hdnFldCategorizeAccId').value = COAccountID;
            window.opener.document.getElementById('btnHiddenCategorizeSelect').click();
            self.close();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <div class="right-input">
        <div class="input-part">
            <table>
                <tr>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="PageTitle">
    Categorize Transaction
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="Content">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div id="dvPayBill" runat="server" style="width:500px">
        <table cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
        <td colspan="3" style="font-weight:bold;">
        <asp:Label runat="server" ID="lblSelectCOAccounts" Text="Select a category for the transactions"></asp:Label>
        </td>
        </tr>
            <tr>
           
                <td colspan="2">
                    <asp:DropDownList ID="ddlAccounts" runat="server" Width="180px" CssClass="ddlAccounts">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
             
            <td>
            <asp:Button runat="server" ID="btnSave" CssClass="button" Text="Save" />
            </td> 
            <td>
            <asp:Button runat="server" ID="btnCancel" CssClass="button" Text="Cancel" OnClientClick="javascript:self.close(); return false;" />
            </td>
            </tr>
        </table>
      
    </div>
  
</asp:Content>
