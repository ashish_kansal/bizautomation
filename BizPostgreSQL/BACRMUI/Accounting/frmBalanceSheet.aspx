<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBalanceSheet.aspx.vb"
    Inherits=".frmBalanceSheet" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Balance Sheet</title>
<%--    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/jscal2.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/border-radius.css" type="text/css" />--%>

    <script src="../JavaScript/jscal2.js" type="text/javascript"></script>

    <script src="../JavaScript/en.js" type="text/javascript"></script>
    <style>
         .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function OpenMonthySummary(a, b, c, d, e) {
            if (e == 1 || e == 3) {
                var dateFormat = '<%= Session("DateFormat")%>';
                var fromDate = $("[id$=ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate]").val();
                var toDate = $("[id$=ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate]").val();
                if (dateFormat != "") {
                    fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
                    toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

                    fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
                    toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
                }

                window.location.href = '../Accounting/frmMonthlySummary.aspx?ChartAcntId=' + a + '&Opening=' + b + '&Code=' + c + '&frm=frmBalanceSheet' + '&Name=' + d + "&From=" + fromDate + "&To=" + toDate;
            }
        }
        function OpenGLReport(a, b) {
            var dateFormat = '<%= Session("DateFormat")%>';
            var fromDate = $("[id$=ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate]").val();
            var toDate = $("[id$=ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate]").val();
            if (dateFormat != "") {
                fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
                toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

                fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
                toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
            }

            window.location.href = "../Accounting/frmGeneralLedger.aspx?Mode=4&AcntTypeID=" + a + "&Month=0&AccountID=" + b + "&From=" + fromDate + "&To=" + toDate;
            return false;
        }

        $(document).ready(function () {

            $(".tblRow").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () { $(this).find("#imgGL").hide(); }

            );
        });
    </script>

    <style>
        .linkColor
        {
            color: Green;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12" style="z-index:19">
        <div class="pull-left">
            <div class="form-inline">
                <div class="form-group">
                    <asp:Panel runat="server" ID="pnlAccountingClass">
                        <label>Class</label>
                         <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="signup form-control" AutoPostBack="true">
                            </asp:DropDownList>
                        </asp:Panel>
                </div>
                <div class="form-group">
                    <label>From </label>
                     <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                </div>
                <div class="form-group">
                     <label>To </label>
                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                </div>
                <div class="form-group">
                    <asp:LinkButton ID="btnGo" runat="server" Text="" CssClass="button btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <div class="form-group">
                <asp:UpdatePanel ID="updatepanelexcel" runat="server">
                    <ContentTemplate>
            <asp:LinkButton ID="btnExportExcel" runat="server" CssClass="button btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
        </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcel" />
                    </Triggers>
                        </asp:UpdatePanel>
                        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Balance Sheet           
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tblBalanceSheet" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow Width="100%">
            <asp:TableCell VerticalAlign="top">
                <center>
                    <table cellpadding="0" width="" cellspacing="0" border="0" bgcolor="white" runat="server" id="tblExport">
                        <tr valign="top">
                            <td valign="top">
                                <asp:Repeater ID="RepBalanceSheet" runat="server">
                                    <HeaderTemplate>
                                        <table cellpadding="0" class="dg table table-responsive table-bordered" cellspacing="0" border="0" width="">
                                            <tr class="hs">
                                                <%--  <td class="td1" align="center">
                                                <b>Account Code</b>
                                            </td>--%>
                                                <th class="td1" align="center">
                                                    <b>Account</b>
                                                </th>
                                                <th class="td1" align="center">
                                                    <b>Amount</b>
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="normal1 tblRow">
                                            <%-- <td>
                                            <%#IIf(Eval("Type") = "2", "<b>" & Eval("AccountCode1").ToString() & "<b>", Eval("AccountCode1"))%>
                                        </td>--%>
                                            <td>
                                                <span style="" onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','<%#Eval("Opening") %>','<%#Eval("vcAccountCode") %>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountName").ToString()) %>','<%# Eval("Type")%>');">
                                                    <%#IIf(Eval("Type") = "2", "<b>" & Eval("vcAccountName1").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountName1") & "</span>")%>
                                                </span>
                                                 <span class="GLReportSpan">
                                                    <img id="imgGL" alt='General Ledger' height='16px' width='16px' onclick='return OpenGLReport(<%#Eval("numParntAcntTypeID") %>,<%#Eval("numAccountID") %>)' title='General Ledger' src='../images/GLReport.png' style='display: none;'></span>
                                            </td>
                                            <td align="right">
                                                <%#IIf(Eval("Type") = "2", "<b>" & ReturnMoney(Eval("Balance")).ToString() & "<b>", ReturnMoney(Eval("Balance")))%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr class="tr1 tblRow">
                                            <%-- <td>
                                            <%#IIf(Eval("Type") = "2", "<b>" & Eval("AccountCode1").ToString() & "<b>", Eval("AccountCode1"))%>
                                        </td>--%>
                                            <td>
                                                <span style="" onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','<%#Eval("Opening") %>','<%#Eval("vcAccountCode") %>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountName").ToString()) %>','<%# Eval("Type")%>');">
                                                    <%#IIf(Eval("Type") = "2", "<b>" & Eval("vcAccountName1").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountName1") & "</span>")%>
                                                </span>
                                                 <span class="GLReportSpan">
                                                    <img id="imgGL" alt='General Ledger' height='16px' width='16px' onclick='return OpenGLReport(<%#Eval("numParntAcntTypeID") %>,<%#Eval("numAccountID") %>)' title='General Ledger' src='../images/GLReport.png' style='display: none;'></span>
                                            </td>
                                            <td align="right">
                                                <%#IIf(Eval("Type") = "2", "<b>" & ReturnMoney(Eval("Balance")).ToString() & "<b>", ReturnMoney(Eval("Balance")))%>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        <tr>
                                            <td colspan="3">
                                                <tr>
                                                    <td colspan="6" class="normal1">
                                                        <hr />
                                                        <%#DifferenceInBalance%>
                                                    </td>
                                                </tr>
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </center>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
