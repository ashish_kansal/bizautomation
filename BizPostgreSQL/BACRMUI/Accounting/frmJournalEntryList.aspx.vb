' ''Created By Siva
'Imports BACRM.BusinessLogic.Common
'Imports BACRM.BusinessLogic.Accounting
'Partial Public Class frmJournalEntryList
'    Inherits BACRMPage
'#Region "Variables"
'    Dim lintJournalId As Integer
'    Dim mintChartAcntId As Integer

'    Dim dtChartAcnt As DataTable
'    Dim dtItems As New DataTable
'    Dim objJournalEntry As JournalEntry
'#End Region

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Dim j As Integer
'            Dim lintRowcount As Integer
'            Dim drRow As DataRow
'            Dim dtJournalDetails As DataTable
'            If IsNothing(objJournalEntry) Then objJournalEntry = New JournalEntry
'            Currency.Value = Session("Currency")
'            lintJournalId = CCommon.ToInteger(GetQueryStringVal("JournalId"))
'            mintChartAcntId = CCommon.ToInteger(GetQueryStringVal("ChartAcntId"))
'            If Not IsPostBack Then

'                'To Set Permission
'                GetUserRightsForPage(35, 76)


'                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnSave.Visible = False
'                If Not IsNothing(lintJournalId) And lintJournalId <> 0 Then
'                    objJournalEntry.DomainID = Session("DomainId")
'                    objJournalEntry.JournalId = lintJournalId
'                    dtJournalDetails = objJournalEntry.GetJournalEntryDate()
'                    dtItems = objJournalEntry.GetJournalEntryDetails()
'                End If
'                LoadChartAcnt()
'                LoadRecurringTemplate()
'                If dtItems.Rows.Count < 6 Then
'                    lintRowcount = 6 - dtItems.Rows.Count
'                    For j = 0 To lintRowcount - 1
'                        drRow = dtItems.NewRow
'                        dtItems.Rows.Add(drRow)
'                    Next
'                End If
'                txtDescription.Text = CCommon.ToString(dtJournalDetails.Rows(0)("varDescription"))
'                calFrom.SelectedDate = dtJournalDetails.Rows(0)("datEntry_Date")
'                If Not IsDBNull(dtJournalDetails.Rows(0).Item("numRecurringId")) Then
'                    If Not ddlMakeRecurring.Items.FindByValue(dtJournalDetails.Rows(0).Item("numRecurringId")) Is Nothing Then
'                        ddlMakeRecurring.Items.FindByValue(dtJournalDetails.Rows(0).Item("numRecurringId")).Selected = True
'                        If Not IsDBNull(dtJournalDetails.Rows(0).Item("dtLastRecurringDate")) Then
'                            ddlMakeRecurring.Enabled = False
'                        End If
'                    End If
'                End If

'                dgJournalEntry.DataSource = dtItems
'                dgJournalEntry.DataBind()
'                lblTotalDebit1.Text = "Total Debit: " & Session("Currency")
'                lblTotalCredit1.Text = "Total Credit: " & Session("Currency")
'                objJournalEntry.DomainID = Session("DomainID")
'                objJournalEntry.JournalId = lintJournalId

'                objJournalEntry.AcntType = 1  ' For Debit Entry
'                lblTotDebitValue1.Text = ReturnMoney(objJournalEntry.GetSumDebitCreditValues())
'                TotalDrText.Value = "Total Debit: " & Session("Currency")
'                TotalDrValue.Value = lblTotDebitValue1.Text

'                objJournalEntry.AcntType = 2 ' For Credit Entry
'                lblTotCreditValue1.Text = ReturnMoney(objJournalEntry.GetSumDebitCreditValues())
'                TotalCrText.Value = "Total Credit: " & Session("Currency")
'                TotalCrValue.Value = lblTotCreditValue1.Text
'            End If
'            btnSave.Attributes.Add("onclick", "return Validate();")
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub LoadDefaultColumns()
'        Try
'            dtItems.Columns.Add("TransactionId") '' Primary key
'            dtItems.Columns.Add("numJournalId")
'            dtItems.Columns.Add("numDebitAmt")
'            dtItems.Columns.Add("numCreditAmt")
'            dtItems.Columns.Add("numChartAcntId")

'            dtItems.Columns.Add("numAcntType")
'            dtItems.Columns.Add("varDescription")
'            dtItems.Columns.Add("numCustomerId")
'            dtItems.Columns.Add("numDomainId")
'            dtItems.Columns.Add("bitMainCheck")
'            dtItems.Columns.Add("bitReconcile")
'            dtItems.Columns.Add("varRelation")
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartAcnt()
'        Try
'            'If IsNothing(objJournalEntry) Then objJournalEntry = New JournalEntry
'            'objJournalEntry.ParentRootNode = 1
'            'objJournalEntry.DomainId = Session("DomainId")
'            'dtChartAcnt = objJournalEntry.GetChartOfAccountsDetails()
'            Dim objCOA As New ChartOfAccounting
'            objCOA.DomainId = Session("DomainId")
'            objCOA.AccountCode = ""
'            dtChartAcnt = objCOA.GetParentCategory()
'            'Remove AR and AP values ..As those will create problem in AR aging summary report, So better not to allow to create journal entry against it
'            Dim dr() As DataRow = dtChartAcnt.Select(" vcAccountTypeCode in('01010105','01020102') ", "")

'            For index As Integer = 0 To dr.Length - 1
'                dtChartAcnt.Rows.Remove(dr(index))
'            Next
'            dtChartAcnt.AcceptChanges()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub LoadRecurringTemplate()
'        Try
'            Dim lobjChecks As New Checks
'            lobjChecks.DomainId = Session("DomainID")
'            ddlMakeRecurring.DataSource = lobjChecks.GetRecurringDetails
'            ddlMakeRecurring.DataTextField = "varRecurringTemplateName"
'            ddlMakeRecurring.DataValueField = "numRecurringId"
'            ddlMakeRecurring.DataBind()
'            ddlMakeRecurring.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgJournalEntry.ItemCommand
'        Try
'            If e.CommandName = "Go" Then
'                Dim ddlName As DropDownList
'                Dim txtCompanyName As TextBox

'                ddlName = CType(e.Item.FindControl("ddlName"), DropDownList)
'                txtCompanyName = CType(e.Item.FindControl("txtCompanyName"), TextBox)
'                If Not IsNothing(ddlName) Then
'                    '' If txtCompanyName.Text <> "" Then
'                    LoadItem(ddlName, txtCompanyName.Text)
'                    ''End If
'                End If

'                lblTotalDebit1.Text = TotalDrText.Value
'                lblTotalCredit1.Text = TotalCrText.Value

'                lblTotDebitValue1.Text = TotalDrValue.Value
'                lblTotCreditValue1.Text = TotalCrValue.Value
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgJournalEntry.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim ddl As DropDownList
'                Dim ddlAccounts As DropDownList
'                Dim lblAccounts As Label
'                Dim lblDebitAmount As Label
'                Dim lblCreditAmount As Label
'                Dim lblMemo As Label
'                Dim lblName As Label
'                Dim txtDebitAmount As TextBox
'                Dim txtCreditAmount As TextBox
'                Dim txtMemo As TextBox
'                Dim lblTransactionDetailsId As Label

'                ddl = CType(e.Item.FindControl("ddlName"), DropDownList)
'                txtDebitAmount = CType(e.Item.FindControl("txtDebit"), TextBox)
'                txtCreditAmount = CType(e.Item.FindControl("txtCredit"), TextBox)
'                txtMemo = CType(e.Item.FindControl("txtMemo"), TextBox)
'                txtDebitAmount.Attributes.Add("onkeypress", "CheckNumber(1)")
'                txtCreditAmount.Attributes.Add("onkeypress", "CheckNumber(1)")
'                If Not IsNothing(ddl) Then
'                    ''LoadItem(ddl)
'                    lblDebitAmount = CType(e.Item.FindControl("lblDebitAmt"), Label)
'                    lblCreditAmount = CType(e.Item.FindControl("lblCreditAmt"), Label)
'                    lblMemo = CType(e.Item.FindControl("lblMemo"), Label)
'                    lblName = CType(e.Item.FindControl("lblName"), Label)

'                    LoadCompanyDropDown(ddl, IIf(lblName.Text = "", 0, lblName.Text))

'                    lblTransactionDetailsId = CType(e.Item.FindControl("lblTransactionId"), Label)

'                    If lblTransactionDetailsId.Text <> "" Then CType(e.Item.FindControl("lblTransactionId"), Label).Text = lblTransactionDetailsId.Text
'                    If lblDebitAmount.Text <> "" AndAlso lblDebitAmount.Text <> 0 Then txtDebitAmount.Text = lblDebitAmount.Text
'                    If lblCreditAmount.Text <> "" AndAlso lblCreditAmount.Text <> 0 Then txtCreditAmount.Text = lblCreditAmount.Text
'                    If lblMemo.Text <> "" Then txtMemo.Text = lblMemo.Text
'                    If Not ddl.Items.FindByValue(lblName.Text) Is Nothing Then ddl.Items.FindByValue(lblName.Text).Selected = True
'                End If
'                ddlAccounts = CType(e.Item.FindControl("ddlAccounts"), DropDownList)
'                Dim lngAccountCode As Long


'                If Not IsNothing(ddlAccounts) Then
'                    LoadChartType(ddlAccounts)
'                    lblAccounts = CType(e.Item.FindControl("lblAccountID"), Label)
'                    lngAccountCode = Val(Mid(lblAccounts.Text, 1, InStr(lblAccounts.Text, "~")))

'                    If Not ddlAccounts.Items.FindByValue(lngAccountCode.ToString) Is Nothing Then ddlAccounts.Items.FindByValue(lngAccountCode.ToString).Selected = True
'                End If
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadCompanyDropDown(ByVal ddlitems As DropDownList, ByVal p_DivisionId As Integer)
'        Try
'            If p_DivisionId <> 0 Then
'                If IsNothing(objJournalEntry) Then objJournalEntry = New JournalEntry
'                objJournalEntry.DomainId = Session("DomainID")
'                objJournalEntry.DivisionId = p_DivisionId
'                ddlitems.DataSource = objJournalEntry.GetCompanyDetForDivisionId()
'                ddlitems.DataTextField = "vcCompanyName"
'                ddlitems.DataValueField = "numDivisionID"
'                ddlitems.DataBind()
'                ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'            Else : ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadItem(ByVal ddlitems As DropDownList, ByVal p_WhereCondition As String)
'        Try
'            Dim lobjJournalEntry As New JournalEntry
'            lobjJournalEntry.DomainId = Session("DomainID")
'            lobjJournalEntry.strWhereCondition = p_WhereCondition
'            ddlitems.DataSource = lobjJournalEntry.GetCompanyDetails
'            ddlitems.DataTextField = "vcCompanyName"
'            ddlitems.DataValueField = "numDivisionID"
'            ddlitems.DataBind()
'            ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartType(ByVal ddlAccounts As DropDownList)
'        Try
'            Dim item As ListItem
'            For Each dr As DataRow In dtChartAcnt.Rows
'                item = New ListItem()
'                item.Text = dr("vcAccountName")
'                item.Value = dr("numAccountID")
'                item.Attributes("OptionGroup") = dr("vcAccountType")
'                ddlAccounts.Items.Add(item)
'            Next
'            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))

'            'ddlAccounts.DataSource = dtChartAcnt
'            'ddlAccounts.DataTextField = "vcCategoryName"
'            'ddlAccounts.DataValueField = "numChartOfAcntID"
'            'ddlAccounts.DataBind()
'            'ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            Dim JournalId As Integer
'            JournalId = SaveDataToHeader()
'            SaveDataToGeneralJournalDetails(lintJournalId)
'            PageRedirect()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub PageRedirect()
'        Try
'            If GetQueryStringVal( "frm") = "RecurringTransaction" Then
'                Response.Redirect("../Accounting/frmRecurringTransaction.aspx")
'            ElseIf GetQueryStringVal( "frm") = "GeneralLedger" Then
'                Response.Redirect("../Accounting/frmGeneralLedger.aspx")
'            ElseIf GetQueryStringVal( "frm") = "Transaction" Then
'                Response.Redirect("../Accounting/frmTransactions.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal( "frm") = "QuickReport" Then
'                Response.Redirect("../Accounting/frmQuickAccountReport.aspx?ChartAcntId=" & mintChartAcntId)
'            Else
'                Response.Redirect("../Accounting/frmJournalEntry.aspx?ChartAcntId=" & mintChartAcntId)
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Function SaveDataToHeader() As Integer
'        Dim lntJournalId As Integer
'        Try
'            If IsNothing(objJournalEntry) Then objJournalEntry = New JournalEntry
'            With objJournalEntry
'                .Entry_Date = CDate(calFrom.SelectedDate & " 12:00:00")
'                .Amount = Replace(TotalDrValue.Value, ",", "")
'                .DomainId = Session("DomainId")
'                .RecurringId = ddlMakeRecurring.SelectedItem.Value
'                .Description = txtDescription.Text.Trim
'                .JournalId = lintJournalId
'                lntJournalId = .SaveDataToJournalEntryHeader()
'                Return lntJournalId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    ''To Save Details in General_Journal_Details Table
'    Private Sub SaveDataToGeneralJournalDetails(ByVal lintJournalId As Integer)
'        Dim i As Integer
'        Dim lstr As String
'        Dim ds As New DataSet
'        Dim ddlAccounts As DropDownList
'        Dim lintAccountId() As String
'        Dim dtgriditem As DataGridItem
'        Dim dtrow As DataRow

'        Try
'            If IsNothing(objJournalEntry) Then objJournalEntry = New JournalEntry
'            LoadDefaultColumns()

'            For i = 0 To dgJournalEntry.Items.Count - 1
'                dtrow = dtItems.NewRow
'                dtgriditem = dgJournalEntry.Items(i)
'                If CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString <> "0" And (CType(dtgriditem.FindControl("txtDebit"), TextBox).Text <> "" Or CType(dtgriditem.FindControl("txtCredit"), TextBox).Text <> "") Then '' And CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value.ToString <> "0" --Commented By SP
'                    ''If CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString = "" And (CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "" Or CType(dtgriditem.FindControl("txtCredit"), TextBox).Text = "") And CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value.ToString = "0" Then
'                    ''    Exit For
'                    ''End If
'                    ''lnumBalance = 0
'                    dtrow.Item("TransactionId") = IIf(CType(dtgriditem.FindControl("lblTransactionId"), Label).Text = "", 0, CType(dtgriditem.FindControl("lblTransactionId"), Label).Text)
'                    dtrow.Item("numJournalId") = lintJournalId
'                    dtrow.Item("numDebitAmt") = IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text, ",", ""))
'                    dtrow.Item("numCreditAmt") = IIf(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text, ",", ""))
'                    ddlAccounts = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList)
'                    lintAccountId = ddlAccounts.SelectedItem.Value.Split("~")

'                    dtrow.Item("numChartAcntId") = lintAccountId(0)
'                    '' dtrow.Item("numChartAcntId") = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                    dtrow.Item("varDescription") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                    dtrow.Item("numCustomerId") = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                    dtrow.Item("numDomainId") = Session("DomainId")
'                    dtrow.Item("bitMainCheck") = 0
'                    dtrow.Item("bitReconcile") = 0
'                    'lobjJournalEntry.ChartAcntId = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                    'ldecOpeningBal = lobjJournalEntry.GetOpeningBalance
'                    'lobjJournalEntry.DomainId = Session("DomainId")
'                    ''If CType(dtgriditem.FindControl("txtCredit"), TextBox).Text <> "" Then
'                    ''    ''dtrow.Item("numBalance") = Val(ldecOpeningBal) - Val(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text)
'                    ''    lnumBalance = -Replace(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text, ",", "")
'                    ''    dtrow.Item("numBalance") = lnumBalance
'                    ''ElseIf CType(dtgriditem.FindControl("txtDebit"), TextBox).Text <> "" Then
'                    ''    '' dtrow.Item("numBalance") = Val(ldecOpeningBal) + Val(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text)
'                    ''    '' dtrow.Item("numBalance") = Replace(Val(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text), ",", "")
'                    ''    lnumBalance = Replace(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text, ",", "")
'                    ''    dtrow.Item("numBalance") = lnumBalance
'                    ''End If
'                    dtItems.Rows.Add(dtrow)
'                End If
'            Next

'            ds.Tables.Add(dtItems)
'            lstr = ds.GetXml()
'            objJournalEntry.DomainId = Session("DomainId")
'            objJournalEntry.JournalId = lintJournalId
'            objJournalEntry.Mode = 1

'            objJournalEntry.JournalDetails = lstr
'            objJournalEntry.SaveDataToJournalDetails()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Dim i As Integer
'        Dim dtgriditem As DataGridItem
'        Dim dtrow As DataRow
'        Try
'            LoadDefaultColumns()
'            For i = 0 To dgJournalEntry.Items.Count - 1
'                dtrow = dtItems.NewRow
'                dtgriditem = dgJournalEntry.Items(i)
'                dtrow.Item("numJournalId") = CType(dtgriditem.FindControl("lblJournalId"), Label).Text
'                dtrow.Item("TransactionId") = CType(dtgriditem.FindControl("lblTransactionId"), Label).Text
'                dtrow.Item("numDebitAmt") = CType(dtgriditem.FindControl("txtDebit"), TextBox).Text ''IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, CType(dtgriditem.FindControl("txtDebit"), TextBox).Text)
'                dtrow.Item("numCreditAmt") = CType(dtgriditem.FindControl("txtCredit"), TextBox).Text '' IIf(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text = "", 0, CType(dtgriditem.FindControl("txtCredit"), TextBox).Text)
'                dtrow.Item("numChartAcntId") = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                dtrow.Item("varDescription") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                dtrow.Item("numCustomerId") = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                dtrow.Item("varRelation") = CType(dtgriditem.FindControl("lblRelationship"), Label).Text
'                dtrow.Item("numAcntType") = CType(dtgriditem.FindControl("txtAccountTypeId"), TextBox).Text
'                ''If CType(dtgriditem.FindControl("txtCredit"), TextBox).Text <> "" Then
'                ''    dtrow.Item("numAccountTypeId") = -CType(dtgriditem.FindControl("txtCredit"), TextBox).Text
'                ''ElseIf CType(dtgriditem.FindControl("txtDebit"), TextBox).Text <> "" Then
'                ''    dtrow.Item("numBalance") = CType(dtgriditem.FindControl("txtDebit"), TextBox).Text
'                ''End If
'                dtItems.Rows.Add(dtrow)
'            Next

'            For i = 0 To 5
'                dtrow = dtItems.NewRow
'                dtItems.Rows.Add(dtrow)
'            Next
'            LoadChartAcnt()

'            dgJournalEntry.DataSource = dtItems
'            dgJournalEntry.DataBind()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Function ReturnMoney(ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
'        Try
'            PageRedirect()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'End Class