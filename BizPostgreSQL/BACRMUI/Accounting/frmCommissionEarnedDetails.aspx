﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmCommissionEarnedDetails.aspx.vb" Inherits=".frmCommissionEarnedDetails" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?OpID=" + a + "&OppType=1";
            if (window.parent != null) {
                window.parent.open(str, '_blank');   
            }
            return false;
        }

        function CloseWindowAndRefereshParent() {
            if (window.opener != null) {
                window.opener.LoadPayrollDetails();
            }

            window.close();
            return false;
        }

        $(document).ready(function () {
            $("[id$=txtComissionDue]").keydown(function (e) {
                if (e.keyCode == 13 || e.keyCode == 9) {
                    if ($(this).closest("tr").next("tr").next("tr").length > 0) {
                        e.preventDefault();
                        $(this).closest("tr").next("tr").next("tr").find("[id$=txtComissionDue]").focus();
                    }
                }
            });
        });
    </script>
    <style type="text/css">
        .rgFooter {
            padding: 8px !important;
            text-align: center !important;
            font-weight: bold !important;
            border: 1px solid #e4e4e4 !important;
            border-bottom-width: 2px !important;
            text-align: center !important;
            background-image: none !important;
            background-color: #ebebeb !important;
        }

        .rgMasterTable > tbody> tr.ais {
            background-color: #f9f9f9;
        }

        .rgDetailTable {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <asp:Button ID="btnUpdateAmtPaid" runat="server" CssClass="btn btn-primary" Text="Pay Commission Due & Close" OnClick="btnUpdateAmtPaid_Click"  />
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Commission Earned
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <telerik:RadGrid ID="dgBizDocs" ShowStatusBar="true" runat="server" AutoGenerateColumns="False"
        AllowMultiRowSelection="False" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is"
        HeaderStyle-CssClass="hs" Skin="windows" EnableEmbeddedSkins="false">
        <MasterTableView DataKeyNames="numOppID,numOppBizDocsId,Name" ItemStyle-HorizontalAlign="Center"
            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" Name="ParentGrid" ShowFooter="true">
            <DetailTables>
                <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"
                    AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Name="ChildGrid">
                <%--    <ParentTableRelation>
                        <telerik:GridRelationFields DetailKeyField="numOppBizDocsId" MasterKeyField="numOppBizDocsId" />
                    </ParentTableRelation>--%>
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="Item" DataField="vcItemName" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="left">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Units" DataField="numUnitHour" HeaderStyle-Wrap="false" DataFormatString="{0:#}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Vendor Cost" DataField="VendorCost" DataFormatString="{0:##,#00.00###}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Average Cost" DataField="monAvgCost" DataFormatString="{0:##,#00.00###}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Unit Price" DataField="monPrice" HeaderStyle-Wrap="false" DataFormatString="{0:##,#00.00###}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Total Amount" DataField="monTotAmount" DataFormatString="{0:##,#00.00###}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission" DataField="decCommission" DataFormatString="{0:##,#00.00###}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Based On" DataField="BasedOn" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission Type" DataField="CommissionType" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission Earned" DataField="monCommissionEarned" DataFormatString="{0:##,#00.00###}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Commission Paid" DataField="monCommissionPaid" DataFormatString="{0:##,#00.00###}" HeaderStyle-Wrap="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                </telerik:GridTableView>
            </DetailTables>
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Deal/Order" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a onclick="return OpenOpp('<%# DataBinder.Eval(Container.DataItem, "numOppId") %>');">
                            <%# DataBinder.Eval(Container.DataItem, "Name") %></a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn ItemStyle-Width="100" HeaderStyle-Width="100" DataFormatString="{0:##,#00.00###}" HeaderText="SO<br />Sub-Total" DataField="monSOSubTotal" ItemStyle-HorizontalAlign="Right"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ItemStyle-Width="110" HeaderStyle-Width="100" DataFormatString="{0:##,#00.00###}" HeaderText="Gross Profit<br />(Vendor Cost)" DataField="monGPVendorCost" ItemStyle-HorizontalAlign="Right"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ItemStyle-Width="110" HeaderStyle-Width="100" DataFormatString="{0:##,#00.00###}" HeaderText="Gross Profit<br />(Average Cost)" DataField="monGPAevrageCost" ItemStyle-HorizontalAlign="Right"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Invoice">
                    <ItemTemplate>
                        <a style='<%# If(Convert.ToInt64(DataBinder.Eval(Container.DataItem, "numOppBizDocsId")) > 0, "display:block", "display:none")%>' onclick="return OpenBizInvoice('<%# DataBinder.Eval(Container.DataItem, "numOppId") %>','<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>');">
                            <%# DataBinder.Eval(Container.DataItem, "vcBizDocID") %>
                        </a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn ItemStyle-Width="100" HeaderStyle-Width="100" HeaderText="Invoice<br>Amt" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# String.Format("{0:##,#00.00##}", DataBinder.Eval(Container.DataItem, "monInvoiceSubTotal"))%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn ItemStyle-Width="100" HeaderStyle-Width="100" HeaderText="Commission<br />Earned" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# String.Format("{0:##,#00.00##}", DataBinder.Eval(Container.DataItem, "monTotalCommission"))%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalCommissionEarned"></asp:Label>
                    </FooterTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn ItemStyle-Width="100" HeaderStyle-Width="100" HeaderText="Commission<br />Paid" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# String.Format("{0:##,#00.00##}", DataBinder.Eval(Container.DataItem, "monCommissionPaid"))%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalCommissionPaid"></asp:Label>
                    </FooterTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn ItemStyle-Width="135" HeaderStyle-Width="135" HeaderText="Commission<br />Due" ItemStyle-Font-Bold="true" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataField="decTotalCommission" UniqueName="TemplateTotalCommission">
                    <ItemTemplate>
                        <asp:TextBox ID="txtComissionDue" runat="server" Width="120" CssClass="form-control" Text='<%# Convert.ToDouble(DataBinder.Eval(Container.DataItem, "monCommissionToPay"))%>'></asp:TextBox>
                        <asp:HiddenField runat="server" ID="hdnCommissionPaid" Value='<%# Convert.ToDouble(DataBinder.Eval(Container.DataItem, "monCommissionPaid"))%>' />
                        <asp:HiddenField runat="server" ID="hdnCommissionDue" Value='<%# Convert.ToDouble(DataBinder.Eval(Container.DataItem, "monCommissionToPay"))%>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalCommissionDue"></asp:Label>
                    </FooterTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
</asp:Content>
