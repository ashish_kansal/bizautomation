''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Infragistics.WebUI.UltraWebGrid
Partial Public Class frmOperationBudgetDetails
    Inherits BACRMPage
#Region "Variables"
    Dim mobjBudget As Budget
    Dim dsTemp As DataSet
    Dim arr As Array
    Dim dtHierarchyDetails As DataTable
    Dim mintBudgetId As Integer
   
    Dim objCommon As CCommon
#End Region

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If objCommon Is Nothing Then objCommon = New CCommon
    '        GetUserRightsForPage( 35, 98)
    '        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
    '            Response.Redirect("../admin/authentication.aspx?mesg=AS")
    '        Else
    '            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
    '                btnSave.Visible = False
    '                btnNewOperationBudget.Visible = False
    '                btnDuplicateOperationBudget.Visible = False
    '            End If
    '            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
    '        End If
    '        If Not IsPostBack Then
    '            
    '            objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlCostCenter, 70, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlDivision, 71, Session("DomainID"))
    '            FillBudgetCombo()
    '            boolDuplicate.Value = False
    '            CreateDataTable(0)
    '            ''UltraWebGrid1.DataSource = Session("dtBudgetCol")
    '            ''UltraWebGrid1.DataBind()
    '            btnSave.Attributes.Add("onclick", "return Save()")
    '            ddlDivision.Attributes.Add("onChange", "return LoadTextBox();")
    '            ddlDepartment.Attributes.Add("onChange", "return LoadTextBox();")
    '            ddlCostCenter.Attributes.Add("onChange", "return LoadTextBox();")
    '        End If
    '        btnDefine.Attributes.Add("onclick", "return OpenDefineBudget();")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub FillBudgetCombo()
    '    Try
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        mobjBudget.DomainId = Session("DomainId")
    '        ddlBudget.DataSource = mobjBudget.GetBudgetDetails()
    '        ddlBudget.DataTextField = "vcBudgetName"
    '        ddlBudget.DataValueField = "numBudgetId"
    '        ddlBudget.DataBind()
    '        ddlBudget.Items.Insert(0, "--Select One--")
    '        ddlBudget.Items.FindByText("--Select One--").Value = "0"
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub CreateDataTable(ByVal p_Type As Integer)
    '    Try
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        Dim dtOperationBud As DataTable
    '        Dim i As Integer
    '        dsTemp = New DataSet
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.Type = p_Type
    '        dtOperationBud = mobjBudget.GetOperationBudget
    '        Dim dtBudget As New DataTable
    '        For i = 0 To dtOperationBud.Columns.Count - 1
    '            dtBudget.Columns.Add(dtOperationBud.Columns.Item(i).ColumnName, GetType(String))
    '        Next
    '        Session("dtBudgetCol") = dtBudget
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    '' ''Private Sub btnDefine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDefine.Click
    '' ''    ''Try

    '' ''    ''    Dim dtBudget As DataTable
    '' ''    ''    Dim ulrow As UltraGridRow
    '' ''    ''    Dim dr As DataRow
    '' ''    ''    Dim lstr As String
    '' ''    ''    Dim arr1 As New ArrayList
    '' ''    ''    Dim i As Integer
    '' ''    ''   dtBudget = Session("dtBudgetCol")
    '' ''    ''    For Each ulrow In UltraWebGrid1.Rows
    '' ''    ''        dr = dtBudget.NewRow
    '' ''    ''        For i = 0 To UltraWebGrid1.Columns.Count - 1
    '' ''    ''            dr(UltraWebGrid1.Columns.Item(i).Key) = IIf(ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value = Nothing, 0, ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value) ''ulrow.Cells.FromKey().Value
    '' ''    ''        Next
    '' ''    ''        dtBudget.Rows.Add(dr)
    '' ''    ''        arr1.Add(dr("numChartAcntId"))
    '' ''    ''    Next
    '' ''    ''    dtBudget.AcceptChanges()
    '' ''    ''    Session("larraylst") = arr1
    '' ''    ''    Session("BudgetData") = dtBudget
    '' ''    ''    lstr = "<script language=javascript>"
    '' ''    ''    lstr += "var nen=window.open('../Accounting/frmDefineBudget.aspx','','toolbar=no,titlebar=no,left=200, top=300,width=600,height=400,scrollbars=yes,resizable=yes'); nen.focus();"
    '' ''    ''    lstr += "</script>"
    '' ''    ''    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "DefineAccounts", lstr)
    '' ''    ''Catch ex As Exception
    '' ''    ''    Throw ex
    '' ''    ''End Try
    '' ''End Sub

    'Private Sub ddlBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBudget.SelectedIndexChanged
    '    Try
    '        If ddlBudget.SelectedItem.Value.ToString <> "0" And Not IsNothing(ddlBudget.SelectedItem.Value) Then
    '            UltraWebGrid1.Visible = True
    '            LoadSavedInformation()
    '            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
    '                btnDelete.Visible = False
    '            Else : btnDelete.Visible = True
    '            End If

    '            btnDuplicateOperationBudget.Visible = True
    '        Else : ClearAllData()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadSavedInformation()
    '    Try
    '        Dim dtOperationBudget As DataTable
    '        Dim dtBudget As DataTable
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.BudgetId = ddlBudget.SelectedItem.Value
    '        mintBudgetId = ddlBudget.SelectedItem.Value
    '        dtOperationBudget = mobjBudget.GetOperatingBudgetMasterDet()

    '        If Not IsDBNull(dtOperationBudget.Rows(0).Item("numDepartmentId")) Then
    '            If Not ddlDepartment.Items.FindByValue(dtOperationBudget.Rows(0).Item("numDepartmentId")) Is Nothing Then
    '                ddlDepartment.ClearSelection()
    '                ddlDepartment.Items.FindByValue(dtOperationBudget.Rows(0).Item("numDepartmentId")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtOperationBudget.Rows(0).Item("numDivisionId")) Then

    '            If Not ddlDivision.Items.FindByValue(dtOperationBudget.Rows(0).Item("numDivisionId")) Is Nothing Then
    '                ddlDivision.ClearSelection()
    '                ddlDivision.Items.FindByValue(dtOperationBudget.Rows(0).Item("numDivisionId")).Selected = True
    '            End If
    '        End If


    '        If Not IsDBNull(dtOperationBudget.Rows(0).Item("numCostCenterId")) Then
    '            If Not ddlCostCenter.Items.FindByValue(dtOperationBudget.Rows(0).Item("numCostCenterId")) Is Nothing Then
    '                ddlCostCenter.ClearSelection()
    '                ddlCostCenter.Items.FindByValue(dtOperationBudget.Rows(0).Item("numCostCenterId")).Selected = True
    '            End If
    '        End If

    '        txtBudgetName.Text = dtOperationBudget.Rows(0).Item("vcBudgetName")
    '        ddlFiscal.SelectedIndex = 0

    '        LoadBudgetDetailsGrid()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ClearAllData()
    '    Try
    '        ddlBudget.Items.Clear()
    '        FillBudgetCombo()
    '        ddlCostCenter.SelectedIndex = 0
    '        ddlDepartment.SelectedIndex = 0
    '        ddlDivision.SelectedIndex = 0
    '        ddlFiscal.SelectedIndex = 0
    '        txtBudgetName.Text = ""
    '        boolDuplicate.Value = False
    '        '' CreateDataTable(0)
    '        btnDelete.Visible = False
    '        btnDuplicateOperationBudget.Visible = False
    '        UltraWebGrid1.Clear()
    '        UltraWebGrid1.Visible = False
    '        ''UltraWebGrid1.DataSource = Session("dtBudgetCol")
    '        ''UltraWebGrid1.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnNewOperationBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewOperationBudget.Click
    '    Try
    '        ClearAllData()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnDuplicateOperationBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuplicateOperationBudget.Click
    '    Try
    '        boolDuplicate.Value = True
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnTreeView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTreeView.Click
    '    Try
    '        Dim i, j, k As Integer
    '        Dim dt As New DataTable
    '        Dim ds As New DataSet
    '        Dim lstr As String
    '        Dim drRow As DataRow
    '        Dim dtBudgetData As DataTable
    '        Dim ulrow As UltraGridRow
    '        Dim dr As DataRow
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        dtBudgetData = Session("dtBudgetCol")
    '        UltraWebGrid1.Visible = True
    '        If Not IsNothing(UltraWebGrid1) Then
    '            For Each ulrow In UltraWebGrid1.Rows
    '                dr = dtBudgetData.NewRow
    '                For i = 0 To UltraWebGrid1.Columns.Count - 1
    '                    dr(UltraWebGrid1.Columns.Item(i).Key) = IIf(ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value = Nothing, 0, ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value) ''ulrow.Cells.FromKey().Value
    '                Next
    '                dtBudgetData.Rows.Add(dr)
    '            Next
    '        End If

    '        arr = chartAcnt.Value.Split(",")

    '        If arr(0) <> "" Then
    '            dt.Columns.Add("numAcntId")
    '            dt.Columns.Add("numParentAcntId")

    '            For i = 0 To arr.Length - 1
    '                drRow = dt.NewRow
    '                Dim arr1 As String()
    '                arr1 = arr(i).Split(" ")
    '                drRow.Item("numAcntId") = arr1(0)
    '                drRow.Item("numParentAcntId") = arr1(1)
    '                dt.Rows.Add(drRow)
    '            Next
    '            ds.Tables.Add(dt)
    '            lstr = ds.GetXml()
    '            mobjBudget.DomainId = Session("DomainId")

    '            Select Case ddlFiscal.SelectedIndex
    '                Case 0
    '                    mobjBudget.FiscalYear = Now.Year
    '                    mobjBudget.Type = 0
    '                Case 1
    '                    mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                    mobjBudget.Type = -1
    '                Case 2
    '                    mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                    mobjBudget.Type = 1
    '            End Select
    '            mobjBudget.AcntIdDetails = lstr
    '            dtHierarchyDetails = mobjBudget.GetHierachyDetails()
    '            'dtBudgetData = Session("BudgetData")
    '            If Not IsNothing(dtBudgetData) Then
    '                For i = 0 To dtBudgetData.Rows.Count - 1
    '                    For j = 0 To dtHierarchyDetails.Rows.Count - 1
    '                        For k = 0 To UltraWebGrid1.Columns.Count - 1
    '                            If dtBudgetData.Rows(i)("numChartAcntId") = dtHierarchyDetails.Rows(j)("numChartAcntId") Then
    '                                dtHierarchyDetails.Rows(j)(UltraWebGrid1.Columns.Item(k).Key) = IIf(dtBudgetData.Rows(i)(UltraWebGrid1.Columns.Item(k).Key).ToString = "0", "", dtBudgetData.Rows(i)(UltraWebGrid1.Columns.Item(k).Key))
    '                            End If
    '                        Next
    '                    Next
    '                Next
    '            End If

    '            dtHierarchyDetails.AcceptChanges()
    '            UltraWebGrid1.DataSource = dtHierarchyDetails.DefaultView
    '            UltraWebGrid1.DataBind()
    '        Else : UltraWebGrid1.Rows.Clear()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub UltraWebGrid1_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles UltraWebGrid1.InitializeLayout
    '    Try
    '        Dim i As Integer
    '        UltraWebGrid1.Bands(0).Columns.FromKey("numChartAcntId").Hidden = True
    '        UltraWebGrid1.Bands(0).Columns.FromKey("TOC").Hidden = True
    '        UltraWebGrid1.Bands(0).Columns.FromKey("numParentAcntId").Hidden = True
    '        UltraWebGrid1.Bands(0).Columns.FromKey("vcCategoryName").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        UltraWebGrid1.Bands(0).Columns.FromKey("Total").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        UltraWebGrid1.Bands(0).Columns.FromKey("vcCategoryName").HeaderText = "Budget Accounts"
    '        UltraWebGrid1.Bands(0).Columns.FromKey("Total").DataType = "System.Decimal"
    '        UltraWebGrid1.Bands(0).Columns.FromKey("Total").Format = "###,###.##"
    '        UltraWebGrid1.Bands(0).Columns.FromKey("Total").Width = Unit.Percentage(5)
    '        UltraWebGrid1.Bands(0).Columns.FromKey("vcCategoryName").Width = Unit.Percentage(15)
    '        For i = 4 To UltraWebGrid1.Columns.Count - 3
    '            UltraWebGrid1.Columns.Item(i).HeaderText = MonthName(UltraWebGrid1.Columns.Item(i).Key.Split("~")(0)).Substring(0, 3)
    '            UltraWebGrid1.Bands(0).Columns.FromKey(UltraWebGrid1.Columns.Item(i).Key).DataType = "System.Decimal"
    '            UltraWebGrid1.Bands(0).Columns.FromKey(UltraWebGrid1.Columns.Item(i).Key).Format = "###,###.##"
    '            UltraWebGrid1.Bands(0).Columns.FromKey(UltraWebGrid1.Columns.Item(i).Key).Width = Unit.Percentage(5)
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Dim lngBudgetId As Long
    '        If CheckDuplicateBudgetName() = True Then Exit Sub
    '        lngBudgetId = SaveBudgetMaster()
    '        SaveBudgetDetails(lngBudgetId)
    '        ClearAllData()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Function CheckDuplicateBudgetName() As Boolean
    '    Try
    '        Dim lngCount As Long
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        With mobjBudget
    '            .BudgetId = IIf(boolDuplicate.Value = True, 0, ddlBudget.SelectedItem.Value)
    '            .BudgetName = txtBudgetName.Text
    '            .DomainId = Session("DomainId")
    '            .DepartmentId = ddlDepartment.SelectedItem.Value
    '            .DivisionId = ddlDivision.SelectedItem.Value
    '            .CostCenterId = ddlCostCenter.SelectedItem.Value
    '            lngCount = .CheckDuplicateBudgetName()
    '            If lngCount > 0 Then
    '                litMessage.Text = "The budget must be unique"
    '                Return True
    '            Else : Return False
    '            End If
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Function SaveBudgetMaster() As Long
    '    Dim lngBudgetId As Long
    '    Try
    '        With mobjBudget
    '            .BudgetName = txtBudgetName.Text
    '            .DepartmentId = ddlDepartment.SelectedItem.Value
    '            .DivisionId = ddlDivision.SelectedItem.Value
    '            .CostCenterId = ddlCostCenter.SelectedItem.Value
    '            Select Case ddlFiscal.SelectedIndex
    '                Case 0
    '                    .FiscalYear = Now.Year
    '                Case 1
    '                    .FiscalYear = CInt(Now.Year - 1)
    '                Case 2
    '                    .FiscalYear = CInt(Now.Year + 1)
    '            End Select
    '            .DomainId = Session("DomainId")
    '            .UserCntID = Session("UserContactID")

    '            .BudgetId = IIf(boolDuplicate.Value = True, 0, ddlBudget.SelectedItem.Value)
    '            lngBudgetId = .SaveOperationBudget()
    '            Return lngBudgetId
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Sub SaveBudgetDetails(ByVal lngBudgetId As Long)
    '    Try
    '        Dim i As Integer
    '        Dim dtBudget As New DataTable
    '        Dim ulrow As UltraGridRow
    '        Dim dr As DataRow
    '        Dim lstr As String
    '        Dim ds As New DataSet
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        dtBudget.Columns.Add("BudgetDetId") '' Primary key
    '        dtBudget.Columns.Add("vcCategoryName")
    '        dtBudget.Columns.Add("numBudgetId")
    '        dtBudget.Columns.Add("numChartAcntId")
    '        dtBudget.Columns.Add("numParentAcntId")
    '        dtBudget.Columns.Add("tintMonth")
    '        dtBudget.Columns.Add("monAmount")
    '        dtBudget.Columns.Add("Comments")
    '        dtBudget.Columns.Add("intYear")
    '        For Each ulrow In UltraWebGrid1.Rows
    '            For i = 0 To UltraWebGrid1.Columns.Count - 1
    '                dr = dtBudget.NewRow
    '                If UltraWebGrid1.Columns.Item(i).Key <> "BudgetDetId" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "TOC" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "vcCategoryName" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "numBudgetId" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "numChartAcntId" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "numParentAcntId" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "Total" AndAlso UltraWebGrid1.Columns.Item(i).Key <> "Comments" Then
    '                    dr("BudgetDetId") = 0
    '                    dr("vcCategoryName") = ulrow.Cells.FromKey("vcCategoryName").Value
    '                    dr("numBudgetId") = lngBudgetId
    '                    dr("numChartAcntId") = ulrow.Cells.FromKey("numChartAcntId").Value
    '                    dr("numParentAcntId") = IIf(ulrow.Cells.FromKey("numParentAcntId").Value = Nothing, 1, ulrow.Cells.FromKey("numParentAcntId").Value)
    '                    dr("tintMonth") = ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Key.Split("~")(0)
    '                    If Not IsNothing(ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value) Then
    '                        dr("monAmount") = IIf(ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value.ToString = "False", 0, ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value)
    '                    Else : dr("monAmount") = ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value
    '                    End If
    '                    '' dr("monAmount") = IIf(ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value.ToString = "False", 0, ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Value)
    '                    dr("Comments") = ulrow.Cells.FromKey("Comments").Value
    '                    dr("intYear") = ulrow.Cells.FromKey(UltraWebGrid1.Columns.Item(i).Key).Key.Split("~")(1)
    '                    dtBudget.Rows.Add(dr)
    '                End If
    '            Next
    '        Next
    '        mobjBudget.DepartmentId = ddlDepartment.SelectedItem.Value
    '        mobjBudget.DivisionId = ddlDivision.SelectedItem.Value
    '        mobjBudget.CostCenterId = ddlCostCenter.SelectedItem.Value
    '        Select Case ddlFiscal.SelectedIndex
    '            Case 0
    '                mobjBudget.FiscalYear = Now.Year
    '                mobjBudget.Type = 0
    '            Case 1
    '                mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                mobjBudget.Type = -1
    '            Case 2
    '                mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                mobjBudget.Type = 1
    '        End Select
    '        mobjBudget.BudgetId = lngBudgetId
    '        ds.Tables.Add(dtBudget)
    '        lstr = ds.GetXml()
    '        mobjBudget.BudgetDetails = lstr
    '        mobjBudget.SaveOperationBudgetDetails()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Function ReturnMoney(ByVal Money)
    '    Try
    '        If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.##}", Money)
    '        Return String.Empty
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Sub ddlFiscal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFiscal.SelectedIndexChanged
    '    Try
    '        '' To load Budget Details to Grid
    '        If Not IsNothing(UltraWebGrid1) Then LoadBudgetDetailsGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadBudgetDetailsGrid()
    '    Try
    '        Dim i As Integer
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        With mobjBudget
    '            .DomainId = Session("DomainId")
    '            .BudgetId = ddlBudget.SelectedItem.Value
    '            Select Case ddlFiscal.SelectedIndex
    '                Case 0
    '                    mobjBudget.FiscalYear = Now.Year
    '                    mobjBudget.Type = 0
    '                Case 1
    '                    mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                    mobjBudget.Type = -1
    '                Case 2
    '                    mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                    mobjBudget.Type = 1
    '            End Select
    '            CreateDataTable(mobjBudget.Type)
    '            ''.DivisionId = ddlDivision.SelectedItem.Value
    '            ''.DepartmentId = ddlDepartment.SelectedItem.Value
    '            ''.CostCenterId = ddlCostCenter.SelectedItem.Value

    '            UltraWebGrid1.Clear()
    '            UltraWebGrid1.DataSource = mobjBudget.GetOperatingBudgetDetails
    '            UltraWebGrid1.DataBind()
    '            ''FormatGrid()
    '            Dim dtBudget As New DataTable
    '            For i = 0 To UltraWebGrid1.Columns.Count - 1
    '                dtBudget.Columns.Add(UltraWebGrid1.Columns.Item(i).Key, GetType(String))
    '            Next
    '            Session("dtBudgetCol") = dtBudget
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    '''Sub formatgrid()
    '''    Dim i As Integer
    '''    For i = 4 To UltraWebGrid1.Columns.Count - 3
    '''        UltraWebGrid1.Columns.Item(i).HeaderText = MonthName(UltraWebGrid1.Columns.Item(i).Key.Split("~")(0)).Substring(0, 3)
    '''        ''UltraWebGrid1.Columns.Item(i).Format = "###,##0.00"
    '''        '' UltraWebGrid1.Bands(0).Columns.FromKey(UltraWebGrid1.Columns.Item(i).Key).Format = "###,###.##"
    '''    Next
    '''End Sub
    '''Private Sub ddlDivision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDivision.SelectedIndexChanged
    '''    ''  LoadBudgetDetailsGrid()
    '''    txtBudgetName.Text = IIf(ddlDivision.SelectedItem.Value = 0, "", ddlDivision.SelectedItem.Text) & " " & IIf(ddlDepartment.SelectedItem.Value = 0, "", "-" & ddlDepartment.SelectedItem.Text) & " " & IIf(ddlCostCenter.SelectedItem.Value = 0, "", "-" & ddlCostCenter.SelectedItem.Text)
    '''End Sub

    '''Private Sub ddlDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
    '''    ''    LoadBudgetDetailsGrid()
    '''    txtBudgetName.Text = IIf(ddlDivision.SelectedItem.Value = 0, "", ddlDivision.SelectedItem.Text) & " " & IIf(ddlDepartment.SelectedItem.Value = 0, "", "-" & ddlDepartment.SelectedItem.Text) & " " & IIf(ddlCostCenter.SelectedItem.Value = 0, "", "-" & ddlCostCenter.SelectedItem.Text)
    '''End Sub

    '''Private Sub ddlCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCenter.SelectedIndexChanged
    '''    '' LoadBudgetDetailsGrid()
    '''    txtBudgetName.Text = IIf(ddlDivision.SelectedItem.Value = 0, "", ddlDivision.SelectedItem.Text) & " " & IIf(ddlDepartment.SelectedItem.Value = 0, "", "-" & ddlDepartment.SelectedItem.Text) & " " & IIf(ddlCostCenter.SelectedItem.Value = 0, "", "-" & ddlCostCenter.SelectedItem.Text)
    '''End Sub

    'Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    Try
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        mobjBudget.BudgetId = ddlBudget.SelectedItem.Value
    '        mobjBudget.DomainId = Session("DomainId")
    '        mobjBudget.DeleteOperationBudget()
    '        ClearAllData()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class