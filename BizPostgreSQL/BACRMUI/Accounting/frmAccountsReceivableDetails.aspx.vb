﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.UserInterface
Imports System.Data.SqlClient

Partial Public Class frmAccountsReceivableDetails
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadAccountReceivableDetailGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Dim decTotalAmount, decAmountPaid, decBalanceDue As Decimal

    Private Sub LoadAccountReceivableDetailGrid()
        Dim objVendorPayment As New VendorPayment
        Dim dtARAging As DataTable
        Try
            objVendorPayment.DomainID = CCommon.ToLong(GetQueryStringVal("DomainId"))
            objVendorPayment.DivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
            objVendorPayment.Flag = GetQueryStringVal( "Flag")
            objVendorPayment.AccountClass = CCommon.ToLong(GetQueryStringVal("accountClass"))
            objVendorPayment.dtFromDate = Convert.ToDateTime(GetQueryStringVal("fromDate"))
            objVendorPayment.dtTodate = Convert.ToDateTime(GetQueryStringVal("toDate"))
            objVendorPayment.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objVendorPayment.BasedOn = If(CCommon.ToShort(GetQueryStringVal("BasedOn")) = 0, 1, CCommon.ToShort(GetQueryStringVal("BasedOn")))
            dtARAging = objVendorPayment.GetAccountReceivableAgingDetails
            If (dtARAging.Rows.Count > 0) Then
                decTotalAmount = dtARAging.Compute("Sum(TotalAmount)", String.Empty)
                decAmountPaid = dtARAging.Compute("Sum(AmountPaid)", String.Empty)
                decBalanceDue = dtARAging.Compute("Sum(BalanceDue)", String.Empty)

                gvARDetails.DataSource = dtARAging
                gvARDetails.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvARDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvARDetails.RowDataBound
        Try
            If e.Row.RowType = ListItemType.Footer Then
                CType(e.Row.FindControl("lblTotalAmount"), Label).Text = ReturnMoney(decTotalAmount)
                CType(e.Row.FindControl("lblAmountPaid"), Label).Text = ReturnMoney(decAmountPaid)
                CType(e.Row.FindControl("lblBalanceDue"), Label).Text = ReturnMoney(decBalanceDue)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class