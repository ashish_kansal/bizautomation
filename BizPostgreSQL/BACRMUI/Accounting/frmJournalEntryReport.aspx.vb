''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Imports System.IO.Compression
Imports System.Web.Services
Imports Newtonsoft.Json
Imports System.Collections.Generic

Partial Public Class frmJournalEntryReport
    Inherits BACRMPage

#Region "Variables"
    Dim mobjGeneralLedger As GeneralLedger
    Dim ldecTotalBalanceAmt As Decimal
    Dim dtChartAcntGeneralLedgerDetails As DataTable
    Dim dsGeneralLedger As DataSet = New DataSet
    Dim dtGeneralLedger As DataTable = dsGeneralLedger.Tables.Add("GeneralLedger")

    Dim dtChartAcntDetails As DataTable
    Dim Month, Year As Int16

    Dim strAccountTypeID As String
    Dim strAccountID As String
    Dim DisplayMode As Short
    Dim intDomainID As Integer
    Dim dtFYClosingDate As DateTime
    Protected lngDivisionID As Long
    Dim lngItemCode As Long
    'Dim boolLoadPersistance As Boolean = True
    Dim boolExcludeAccountPersistance As Boolean = False

    ' Public Shared TransCountForAccount As Integer
    'Public Shared AccTransactionCount As New Dictionary(Of Long, Long)

    Public Shared AccTransactionCount As New Dictionary(Of Long, Integer)()
    Public Shared AccClosingBalance As New Dictionary(Of Long, Double)()
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = 0
            intDomainID = CCommon.ToInteger(GetQueryStringVal("DomainID"))
            lngDivisionID = 0
            hdnDivisionId.Value = lngDivisionID
            If intDomainID = 0 Then
                intDomainID = Session("DomainID")
            End If

            GetUserRightsForPage(35, 89)
            If Not IsPostBack Then

                Dim dtChartAcnt As New DataTable
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = intDomainID
                objCOA.AccountCode = ""
                dtChartAcnt = objCOA.GetParentCategory()

                Dim strAccounts As String = String.Empty
                For i As Integer = 0 To dtChartAcnt.Rows.Count - 1
                    strAccounts = strAccounts & dtChartAcnt.Rows(i)("numAccountId").ToString & ","
                Next
                hdnAccounts.Value = strAccounts

                If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                mobjGeneralLedger.DomainID = intDomainID
                mobjGeneralLedger.Year = CInt(Now.Year)

                'boolLoadPersistance = False
                'calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                'calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                'If boolLoadPersistance Then
                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    End Try
                Else
                    calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                End If
                'End If
                LoadGeneralDetailswithPaging()

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadClosingYear()
        Try
            'Get closing date of financial year to Lock transaction from being updated
            Dim objCOA As New ChartOfAccounting
            objCOA.FinancialYearID = 0
            objCOA.DomainID = Session("DomainID")
            objCOA.Mode = 4
            Dim ds As DataSet = objCOA.GetFinancialYear()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("FYClosingDate").ToString <> "" Then
                    dtFYClosingDate = DateFromFormattedDate(ds.Tables(0).Rows(0)("FYClosingDate").ToString, Session("DateFormat"))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadTreeGrid(ByVal dtChartAcntDet As DataView)
        Dim i As Integer
        Dim j As Integer
        Dim lintCount As Integer
        Dim ldecTotalAmt As Decimal
        Dim dtChartAcntBeginingBalance As DataTable
        Dim ldecOpeningBalance As Decimal
        Dim lobjJournalEntry As New JournalEntry
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger

            For i = 0 To dtChartAcntDet.Count - 1
                ldecTotalAmt = 0
                Dim drChartAcntDetails As DataRow = dtGeneralLedger.NewRow
                lobjJournalEntry.ChartAcntId = dtChartAcntDet.Item(i)("numAccountId")
                lobjJournalEntry.DomainID = intDomainID

                lobjJournalEntry.FromDate = calFrom.SelectedDate
                lobjJournalEntry.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
                dtChartAcntGeneralLedgerDetails = lobjJournalEntry.GetGenernalLedgerList
                lintCount = dtChartAcntGeneralLedgerDetails.Rows.Count - 1
                dtChartAcntBeginingBalance = lobjJournalEntry.GetBeginingBalanceGeneralLedgerList

                mobjGeneralLedger.ChartAcntId = dtChartAcntDet.Item(i)("numAccountId")
                mobjGeneralLedger.DomainID = intDomainID
                mobjGeneralLedger.FromDate = calFrom.SelectedDate
                mobjGeneralLedger.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                ldecOpeningBalance = mobjGeneralLedger.GetCurrentOpeningBalanceForGeneralLedgerDetails()

                If dtChartAcntGeneralLedgerDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then
                    drChartAcntDetails(0) = "<font color=Black><b>" & dtChartAcntDet.Item(i)("CategoryName") & "</b></font>"
                    dtGeneralLedger.Rows.Add(drChartAcntDetails)
                End If

                If dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drGeneralLedgerDetails1 As DataRow = dtGeneralLedger.NewRow
                    drGeneralLedgerDetails1(0) = "&nbsp;&nbsp;&nbsp;&nbsp;  Beginning Balance"
                    drGeneralLedgerDetails1(5) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    drGeneralLedgerDetails1(6) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    dtGeneralLedger.Rows.Add(drGeneralLedgerDetails1)
                    ldecTotalAmt = ldecTotalAmt + dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance")
                End If

                For j = 0 To dtChartAcntGeneralLedgerDetails.Rows.Count - 1
                    Dim drGeneralLedgerDetails As DataRow = dtGeneralLedger.NewRow

                    drGeneralLedgerDetails(0) = "&nbsp;&nbsp;&nbsp;&nbsp; " & dtChartAcntGeneralLedgerDetails.Rows(j)("EntryDate")
                    drGeneralLedgerDetails(1) = dtChartAcntGeneralLedgerDetails.Rows(j)("TransactionType")
                    drGeneralLedgerDetails(2) = dtChartAcntGeneralLedgerDetails.Rows(j)("CompanyName")
                    drGeneralLedgerDetails(3) = dtChartAcntGeneralLedgerDetails.Rows(j)("Memo")
                    drGeneralLedgerDetails(4) = dtChartAcntGeneralLedgerDetails.Rows(j)("AcntTypeDescription")
                    drGeneralLedgerDetails(5) = IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")))
                    drGeneralLedgerDetails(6) = ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("numBalance"))
                    drGeneralLedgerDetails(7) = dtChartAcntGeneralLedgerDetails.Rows(j)("JournalId")
                    drGeneralLedgerDetails(8) = IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("CheckId")), 0, dtChartAcntGeneralLedgerDetails.Rows(j)("CheckId"))
                    drGeneralLedgerDetails(9) = IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("CashCreditCardId")), 0, dtChartAcntGeneralLedgerDetails.Rows(j)("CashCreditCardId"))
                    drGeneralLedgerDetails(10) = dtChartAcntGeneralLedgerDetails.Rows(j)("numChartAcntId")
                    drGeneralLedgerDetails(11) = dtChartAcntGeneralLedgerDetails.Rows(j)("numTransactionId")
                    drGeneralLedgerDetails(12) = dtChartAcntGeneralLedgerDetails.Rows(j)("numOppId")
                    drGeneralLedgerDetails(13) = dtChartAcntGeneralLedgerDetails.Rows(j)("numOppBizDocsId")
                    drGeneralLedgerDetails(14) = dtChartAcntGeneralLedgerDetails.Rows(j)("numDepositId")
                    drGeneralLedgerDetails(15) = dtChartAcntGeneralLedgerDetails.Rows(j)("numCategoryHDRID")
                    drGeneralLedgerDetails(16) = dtChartAcntGeneralLedgerDetails.Rows(j)("tintTEType")
                    drGeneralLedgerDetails(17) = dtChartAcntGeneralLedgerDetails.Rows(j)("numCategory")
                    drGeneralLedgerDetails(18) = dtChartAcntGeneralLedgerDetails.Rows(j)("numUserCntID")
                    drGeneralLedgerDetails(19) = dtChartAcntGeneralLedgerDetails.Rows(j)("dtFromDate")

                    dtGeneralLedger.Rows.Add(drGeneralLedgerDetails)
                    ldecTotalAmt = ldecTotalAmt + IIf(IsDBNull(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntGeneralLedgerDetails.Rows(j)("Deposit")))
                Next

                If dtChartAcntGeneralLedgerDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drDummyRow As DataRow = dtGeneralLedger.NewRow
                    drDummyRow(0) = "<B> Total for " & dtChartAcntDet.Item(i)("CategoryName") & "</b>"
                    drDummyRow(5) = "<B> " & ReturnMoney(ldecTotalAmt) & "</b>"
                    dtGeneralLedger.Rows.Add(drDummyRow)
                    ldecTotalBalanceAmt = ldecTotalBalanceAmt + ldecTotalAmt
                End If

                ''dtChildChartAcntDetails = GetChildCategory(dtChartAcntDetails.Item(i)("numAccountId"))

                ''LoadTreeGrid(dtChildChartAcntDetails)

                Dim dvChildnode As New DataView(dtChartAcntDetails)          'create a dataview object
                dvChildnode.RowFilter = "numParntAcntId=" & dtChartAcntDet.Item(i)("numAccountId")
                ldecTotalBalanceAmt = ldecTotalBalanceAmt + ldecTotalAmt
                LoadTreeGrid(dvChildnode)

                If dvChildnode.Count > 0 Then
                    If dtChartAcntGeneralLedgerDetails.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then

                        Dim drDummyRowForSubAcntTot As DataRow = dtGeneralLedger.NewRow
                        drDummyRowForSubAcntTot(0) = "<B> Total for " & dtChartAcntDet.Item(i)("CategoryName") & "  with sub-accounts </b>"
                        drDummyRowForSubAcntTot(5) = "<B> " & ReturnMoney(ldecOpeningBalance) & "</b>"
                        dtGeneralLedger.Rows.Add(drDummyRowForSubAcntTot)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetChildCategory(ByVal ParentID As String) As DataTable
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.ParentAccountId = ParentID
            mobjGeneralLedger.DomainID = intDomainID
            GetChildCategory = mobjGeneralLedger.GetChildCategory()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            End If
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''Private Sub RepGeneralLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepGeneralLedger.ItemDataBound
    ''    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

    ''        Dim hplType As HyperLink
    ''        Dim lblJournalId As Label
    ''        Dim lblCheckId As Label
    ''        Dim lblCashCreditCardId As Label
    ''        Dim lblTransactionId As Label
    ''        Dim linJournalId As Integer
    ''        Dim lintCheckId As Integer
    ''        Dim lintCashCreditCardId As Integer

    ''        hplType = e.Item.FindControl("hplType")
    ''        lblJournalId = e.Item.FindControl("lblJournalId")
    ''        lblCheckId = e.Item.FindControl("lblCheckId")
    ''        lblCashCreditCardId = e.Item.FindControl("lblCashCreditCardId")
    ''        lblTransactionId = e.Item.FindControl("lblTransactionId")

    ''        hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage()")
    ''        linJournalId = IIf(lblJournalId.Text = "&nbsp;" OrElse lblJournalId.Text = "", 0, lblJournalId.Text)
    ''        lintCheckId = IIf(lblCheckId.Text = "&nbsp;" OrElse lblCheckId.Text = "", 0, lblCheckId.Text)
    ''        lintCashCreditCardId = IIf(lblCashCreditCardId.Text = "&nbsp;" OrElse lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)

    ''        If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 Then
    ''            hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmJournalEntryList.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "')")
    ''        ElseIf lintCheckId <> 0 Then
    ''            hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmChecks.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "')")
    ''        ElseIf lintCashCreditCardId <> 0 Then
    ''            hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmCash.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "')")
    ''        End If
    ''    End If
    ''End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            'LoadGeneralDetails()
            LoadGeneralDetailswithPaging()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub RepGeneralLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepJournalEntry.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim lintOppId As Integer
                Dim lintOppBizDocsId As Integer
                Dim lintCategoryHDRID As Integer
                Dim lnkType As LinkButton = e.Item.FindControl("lnkType")
                Dim lnkDetail As LinkButton = e.Item.FindControl("lnkDetail")

                Dim sArgument As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numTransactionID"))

                'Dim lblOppId As Label = e.Item.FindControl("lblOppId")
                'Dim lblOppBizDocsId As Label = e.Item.FindControl("lblOppBizDocsId")


                'Dim lblOppId As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numOppId"))
                'Dim lblOppBizDocsId As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numOppBizDocsId"))

                'Dim linJournalId As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("JournalId"))
                'Dim lintCheckId As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("CheckId"))
                'Dim lintCashCreditCardId As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("CashCreditCardId"))
                'Dim lintTransactionId As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("numTransactionId"))
                'Dim lintDepositId As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("numDepositId"))
                'Dim lintChartAcntId As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("numAccountID"))
                'Dim lintBillPaymentID As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("numBillPaymentID"))
                'Dim lintBillID As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("numBillID"))
                'Dim lintReturnID As Long = CCommon.ToLong(CType(e.Item.DataItem, DataRowView).Row.Item("numReturnID"))

                'Dim lblCategoryHDRID As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numCategoryHDRID"))
                'Dim lblTEType As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("tintTEType"))
                'Dim lblCategory As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numCategory"))
                'Dim lblUserCntID As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("numUserCntID"))
                'Dim lblFromDate As String = CCommon.ToString(CType(e.Item.DataItem, DataRowView).Row.Item("dtFromDate"))


                'Dim linJournalId As Long = CCommon.ToLong(e.Item.DataItem("JournalId"))
                'Dim lintCheckId As Long = CCommon.ToLong((e.Item.DataItem("CheckId")))
                'Dim lintCashCreditCardId As Long = CCommon.ToLong((e.Item.DataItem("CashCreditCardId")))
                'Dim lintTransactionId As Long = CCommon.ToLong((e.Item.DataItem("numTransactionId")))
                'Dim lintDepositId As Long = CCommon.ToLong((e.Item.DataItem("numDepositId")))
                'Dim lintChartAcntId As Long = CCommon.ToLong((e.Item.DataItem("numAccountID")))
                'Dim lintBillPaymentID As Long = CCommon.ToLong((e.Item.DataItem("numBillPaymentID")))
                'Dim lintBillID As Long = CCommon.ToLong((e.Item.DataItem("numBillID")))
                'Dim lintReturnID As Long = CCommon.ToLong((e.Item.DataItem("numReturnID")))

                'Dim lblCategoryHDRID As String = CCommon.ToLong((e.Item.DataItem("numCategoryHDRID")))
                'Dim lblTEType As String = CCommon.ToLong(e.Item.DataItem("tintTEType"))
                'Dim lblCategory As String = CCommon.ToLong((e.Item.DataItem("numCategory")))
                'Dim lblUserCntID As String = CCommon.ToLong((e.Item.DataItem("numUserCntID")))
                'Dim lblFromDate As String = CCommon.ToLong((e.Item.DataItem("dtFromDate")))

                'Dim lblTEType As Label = e.Item.FindControl("lblTEType")
                'Dim lblCategory As Label = e.Item.FindControl("lblCategory")
                'Dim lblUserCntID As Label = e.Item.FindControl("lblUserCntID")
                'Dim lblFromDate As Label = e.Item.FindControl("lblFromDate")

                'lintOppId = IIf(lblOppId = "&nbsp;" OrElse lblOppId = "", 0, lblOppId)
                'lintOppBizDocsId = IIf(lblOppBizDocsId = "&nbsp;" OrElse lblOppBizDocsId = "", 0, lblOppBizDocsId)
                'lintCategoryHDRID = IIf(lblCategoryHDRID = "&nbsp;" OrElse lblCategoryHDRID = "", 0, lblCategoryHDRID)

                'Dim linJournalId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblJournalId"), Label).Text)
                'Dim lintCheckId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblCheckId"), Label).Text)
                'Dim lintCashCreditCardId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblCashCreditCardId"), Label).Text)
                'Dim lintTransactionId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblTransactionId"), Label).Text)
                'Dim lintDepositId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblDepositId"), Label).Text)
                'Dim lintChartAcntId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblChartAcntId"), Label).Text)
                'Dim lintBillPaymentID As Long = CCommon.ToLong(CType(e.Item.FindControl("lblBillPaymentID"), Label).Text)
                'Dim lintBillID As Long = CCommon.ToLong(CType(e.Item.FindControl("lblBillID"), Label).Text)
                'Dim lintReturnID As Long = CCommon.ToLong(CType(e.Item.FindControl("lblReturnID"), Label).Text)

                Dim ddlReconStatus As DropDownList = e.Item.FindControl("ddlReconStatus")

                If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numTransactionId")) = -1 Then
                    ddlReconStatus.Visible = False
                Else
                    'ddlReconStatus.ClearSelection()
                    'ddlReconStatus.Items.FindByValue(IIf(DataBinder.Eval(e.Item.DataItem, "bitReconcile"), "R", IIf(DataBinder.Eval(e.Item.DataItem, "bitCleared"), "C", " "))).Selected = True

                    CType(e.Item.FindControl("lblReconStatus"), Label).Text = IIf(DataBinder.Eval(e.Item.DataItem, "bitReconcile"), "R", IIf(DataBinder.Eval(e.Item.DataItem, "bitCleared"), "C", " "))

                    'CType(e.Item.FindControl("hfReconStatus"), HiddenField).Value = ddlReconStatus.SelectedValue
                    'CType(e.Item.FindControl("lblReconStatus"), Label).Text = CCommon.ToString(ddlReconStatus.SelectedValue)
                End If

                'If lintOppId <> 0 And lintOppBizDocsId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 And lintBillPaymentID = 0 And lintReturnID = 0 Then
                '    lnkType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                'ElseIf lintOppId <> 0 And lintOppBizDocsId = 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 And lintBillPaymentID = 0 And lintReturnID = 0 Then
                '    lnkType.Attributes.Add("onclick", "return reDirect('" & "../opportunity/frmOpportunities.aspx?frm=GeneralLedger&OpID=" & lintOppId & "')")
                'ElseIf CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numBillID")) <> 0 Then
                '    lnkType.Attributes.Add("onclick", "return OpenBill('" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numBillID")) & "')")
                'ElseIf lintCategoryHDRID <> 0 Then

                '    Select Case lblTEType
                '        Case 0
                '            If lblFromDate <> "" Then
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatID=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "&Date=" & CDate(lblFromDate) & "')")
                '            Else
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatID=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "')")
                '            End If
                '        Case 1
                '            If lblCategory = 1 Then
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?frm=TE&ProStageID=0&Proid=0&DivId=&Date=&CatID=&CntID=" & CCommon.ToString(lblUserCntID) & "&CatHdrId=" & lintCategoryHDRID & "')")
                '            Else
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "')")
                '            End If
                '        Case 2
                '            If lblCategory = 1 Then
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "')")
                '            Else
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "')")
                '            End If
                '        Case 3
                '            If lblCategory = 1 Then
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "')")
                '            Else
                '                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHDRID=" & lintCategoryHDRID & "&CntID=" & CCommon.ToString(lblUserCntID) & "')")
                '            End If
                '    End Select
                'End If

                Dim btnDeleteAction As Button = e.Item.FindControl("btnDeleteAction")
                Dim lnkDelete As LinkButton = e.Item.FindControl("lnkDeleteAction")

                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    CType(e.Item.FindControl("lblType"), Label).Visible = True
                    lnkType.Visible = False
                    lnkDetail.Visible = False
                End If

                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDeleteAction.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else
                    'btnDeleteAction.Attributes.Add("onclick", "return DeleteRecord();")
                End If

                If Not dtFYClosingDate = Nothing And CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numTransactionId")) > 0 Then
                    Dim lblJDate As Label = e.Item.FindControl("lblJDate")
                    If lblJDate.Text <> "" Then
                        If dtFYClosingDate >= DateFromFormattedDate(lblJDate.Text, Session("DateFormat")) Then
                            'CType(e.Item.FindControl("lblType"), Label).Visible = True
                            'lnkType.Visible = False

                            btnDeleteAction.Visible = False
                            lnkDelete.Visible = False
                            CType(e.Item.FindControl("lblFYClosed"), Label).Visible = True
                        End If
                    End If
                End If

                If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numTransactionId")) < 0 Then 'Hide buttons for total row 
                    btnDeleteAction.Visible = False
                    lnkDelete.Visible = False
                    lnkDetail.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function LastDayOfMonthFromDateTime(ByVal dateTime As Date) As Date
        Dim firstDayOfTheMonth As New Date(dateTime.Year, dateTime.Month, 1)
        Return firstDayOfTheMonth.AddMonths(1).AddDays(-1)
    End Function

    'Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
    '    ExportToExcel.RepeaterToExcel(RepGeneralLedger, Response)
    'End Sub

    Private Sub LoadGeneralDetailswithPaging()
        Try
            LoadClosingYear()
            Dim TransCountForAccount As Integer
            Dim perLoadCount As Integer = 0

            Dim dsGL As DataSet
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.DomainID = intDomainID

            mobjGeneralLedger.byteMode = 0
            mobjGeneralLedger.CurrentPage = 1
            mobjGeneralLedger.PageSize = 1
            mobjGeneralLedger.TotalRecords = 0
            mobjGeneralLedger.ItemCode = CCommon.ToLong(hdnItemCode.Value)
            mobjGeneralLedger.FromDate = calFrom.SelectedDate
            mobjGeneralLedger.ToDate = CDate(calTo.SelectedDate & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            Dim strAccountID As String = hdnAccounts.Value
            mobjGeneralLedger.AccountIds = strAccountID
            mobjGeneralLedger.DivisionID = lngDivisionID
            mobjGeneralLedger.ReconStatus = CCommon.ToString(hdnReconStatus.Value)
            dsGL = mobjGeneralLedger.GetJournalEntriesReport_VirtualLoad() 'GetChartAcntDetailsForGL()
            dtChartAcntDetails = dsGL.Tables(1).Clone()
            'dtChartAcntDetails.Rows.Add(dtChartAcntDetails.NewRow())
            Dim strAccIds As String = ""

            For Each dr As DataRow In dsGL.Tables(0).Rows
                strAccIds += CCommon.ToString(dr("numAccountId")) + ","

            Next
            hdnAccounts.Value = strAccIds

            Dim drow As DataRow
            'Dim dtTransCount As DataTable
            'dtTransCount = dsGL.Tables(2)
            Dim TotTransCountForAccount As Integer = 0
            Dim AccountId As Long

            drow = dtChartAcntDetails.NewRow
            'drow("numDomainID") = ""
            'drow("numAccountId") = ""
            'drow("TransactionType") = ""
            'drow("CompanyName") = ""
            'drow("Date") = ""
            'drow("varDescription") = ""
            'drow("Narration") = ""
            'drow("BizDocID") = ""
            'drow("TranRef") = ""
            'drow("TranDesc") = ""
            'drow("numDebitAmt") = ""
            'drow("numCreditAmt") = ""
            'drow("vcAccountName") = "" 'dr("vcAccountName")
            'drow("balance") = "" '"<b>" & dr("mnOpeningBalance") & "</b>"
            drow("bitReconcile") = 0
            drow("bitCleared") = 0
            drow("numTransactionId") = 1
            dtChartAcntDetails.Rows.Add(drow)

            'Dim dsCopy As New DataSet
            'Dim dtCopy As DataTable
            'dtCopy = dtTransCount.Copy()
            'dsCopy.Tables.Add(dtCopy)
            'Dim strAccountList As String = dsCopy.GetXml()
            RepJournalEntry.DataSource = dtChartAcntDetails
            RepJournalEntry.DataBind()
            'Dim json As String = String.Empty
            'json = JsonConvert.SerializeObject(dsCopy, Formatting.None)

            ' ClientScript.RegisterClientScriptBlock(Page.GetType, "AccountList", "GetAccountsLists();", True)
            'ClientScript.RegisterClientScriptBlock(Page.GetType, "AccountList", "GetAccountsList(" & strAccountID & ");", True)

            'CType(RepGeneralLedger.Controls(0).Controls(0).FindControl("ddlReconStatusFilter"), DropDownList).SelectedValue = hdnReconStatus.Value

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    'Private Sub LoadGeneralDetailswithPaging_Old()
    '    Try
    '        LoadClosingYear()
    '        Dim TransCountForAccount As Integer
    '        Dim perLoadCount As Integer = 0

    '        Dim dsGL As DataSet
    '        If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
    '        mobjGeneralLedger.DomainID = intDomainID

    '        mobjGeneralLedger.byteMode = 1
    '        mobjGeneralLedger.CurrentPage = 1
    '        mobjGeneralLedger.PageSize = 100
    '        mobjGeneralLedger.TotalRecords = 0

    '        mobjGeneralLedger.FromDate = calFrom.SelectedDate
    '        mobjGeneralLedger.ToDate = CDate(calTo.SelectedDate & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
    '        Dim strAccountID As String = hdnAccounts.Value
    '        mobjGeneralLedger.AccountIds = IIf(ddlCOA.SelectedValue = 1, strAccountID, ddlCOA.SelectedValue)
    '        mobjGeneralLedger.DivisionID = lngDivisionID
    '        mobjGeneralLedger.ReconStatus = CCommon.ToString(hdnReconStatus.Value)
    '        dsGL = mobjGeneralLedger.GetJournalEntriesReport_VirtualLoad() 'GetChartAcntDetailsForGL()
    '        dtChartAcntDetails = dsGL.Tables(1).Clone()
    '        Dim drow As DataRow
    '        Dim dtTransCount As DataTable
    '        dtTransCount = dsGL.Tables(2)
    '        Dim TotTransCountForAccount As Integer = 0
    '        Dim AccountId As Long
    '        AccTransactionCount.Clear()
    '        AccClosingBalance.Clear()

    '        For Each dr As DataRow In dsGL.Tables(0).Rows
    '            TransCountForAccount = 0
    '            TotTransCountForAccount = 0
    '            Dim PrevBalance As Double

    '            Dim drChild() As DataRow = dsGL.Tables(1).Select("numAccountID=" & dr("numAccountID"), "")
    '            'TotTransCountForAccount = CCommon.ToInteger(dr("TransactionCount"))


    '            AccountId = CCommon.ToLong(dr("numAccountID"))

    '            For Each drTransactionCount As DataRow In dsGL.Tables(2).Rows
    '                If drTransactionCount("numAccountId") = AccountId Then
    '                    TotTransCountForAccount = CCommon.ToInteger(drTransactionCount("TransactionCount"))
    '                End If
    '            Next

    '            If Not AccTransactionCount.ContainsKey(AccountId) Then
    '                AccTransactionCount.Add(AccountId, 0)

    '            End If
    '            If Not AccClosingBalance.ContainsKey(AccountId) Then
    '                AccClosingBalance.Add(AccountId, CCommon.ToDouble(dr("mnOpeningBalance")))

    '            End If

    '            drow = dtChartAcntDetails.NewRow
    '            drow("numDomainID") = dr("numDomainID")
    '            drow("numAccountId") = dr("numAccountId")
    '            drow("TransactionType") = ""
    '            drow("CompanyName") = ""
    '            drow("Date") = "<b>" & dr("vcAccountName") & "</b>"
    '            drow("varDescription") = ""
    '            drow("Narration") = ""
    '            drow("BizDocID") = ""
    '            drow("TranRef") = ""
    '            drow("TranDesc") = ""
    '            drow("numDebitAmt") = ""
    '            drow("numCreditAmt") = ""
    '            drow("vcAccountName") = "" 'dr("vcAccountName")
    '            drow("balance") = "" '"<b>" & dr("mnOpeningBalance") & "</b>"
    '            drow("bitReconcile") = 0
    '            drow("bitCleared") = 0
    '            drow("numTransactionId") = -1

    '            If DisplayMode = 2 Then
    '                If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
    '            Else
    '                dtChartAcntDetails.Rows.Add(drow)
    '            End If


    '            drow = dtChartAcntDetails.NewRow
    '            drow("numDomainID") = dr("numDomainID")
    '            drow("numAccountId") = dr("numAccountId")
    '            drow("TransactionType") = ""
    '            drow("CompanyName") = ""
    '            drow("Date") = "Opening Balance"
    '            drow("varDescription") = ""
    '            drow("Narration") = ""
    '            drow("BizDocID") = ""
    '            drow("TranRef") = ""
    '            drow("TranDesc") = ""
    '            drow("numDebitAmt") = "<b>" & ReturnMoney(dr("mnOpeningBalance")) & "</b>"
    '            drow("numCreditAmt") = ""
    '            drow("vcAccountName") = "" 'dr("vcAccountName")
    '            drow("balance") = "<b>" & ReturnMoney(dr("mnOpeningBalance")) & "</b>"
    '            drow("bitReconcile") = 0
    '            drow("bitCleared") = 0
    '            drow("numTransactionId") = -1

    '            If DisplayMode = 2 Then
    '                If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
    '            Else
    '                dtChartAcntDetails.Rows.Add(drow)
    '            End If

    '            PrevBalance = AccClosingBalance(AccountId)

    '            Dim i As Integer = 0
    '            Dim boolIsCreditTransaction As Boolean = False
    '            For Each childRow As DataRow In drChild
    '                If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
    '                    childRow("numDebitAmt") = ReturnMoney(CCommon.ToDouble(childRow("numCreditAmt")) * -1)
    '                    boolIsCreditTransaction = True
    '                Else
    '                    boolIsCreditTransaction = False
    '                End If
    '                If i = 0 Then
    '                    'PrevBalance = dr("mnOpeningBalance")
    '                    childRow("balance") = PrevBalance + childRow("numDebitAmt")
    '                    PrevBalance = childRow("balance")
    '                Else
    '                    childRow("balance") = PrevBalance + childRow("numDebitAmt")
    '                    PrevBalance = childRow("balance")
    '                End If
    '                'added by chintan
    '                If boolIsCreditTransaction = True Then
    '                    childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
    '                Else
    '                    childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
    '                End If

    '                childRow("balance") = ReturnMoney(CCommon.ToDouble(childRow("balance")))
    '                dtChartAcntDetails.ImportRow(childRow)
    '                i += 1
    '                TransCountForAccount += 1
    '                perLoadCount += 1

    '            Next

    '            'For Each KeyValue As KeyValuePair(Of Long, Long) In AccTransactionCount
    '            '    If AccTransactionCount.ContainsKey(strAccountID) Then
    '            '    End If
    '            'Next

    '            If TransCountForAccount = TotTransCountForAccount Then
    '                drow = dtChartAcntDetails.NewRow
    '                drow("numDomainID") = dr("numDomainID")
    '                drow("numAccountId") = dr("numAccountId")
    '                drow("TransactionType") = ""
    '                drow("CompanyName") = ""
    '                drow("Date") = "Closing Balance"
    '                drow("varDescription") = ""
    '                drow("Narration") = ""
    '                drow("BizDocID") = ""
    '                drow("TranRef") = ""
    '                drow("TranDesc") = ""
    '                drow("numDebitAmt") = "" '"<b>" & PrevBalance & "</b>"
    '                drow("numCreditAmt") = ""
    '                drow("vcAccountName") = "" 'dr("vcAccountName")
    '                drow("balance") = "<b>" & ReturnMoney(PrevBalance) & "</b>"
    '                drow("bitReconcile") = 0
    '                drow("bitCleared") = 0
    '                drow("numTransactionId") = -1

    '                If DisplayMode = 2 Then
    '                    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
    '                Else
    '                    dtChartAcntDetails.Rows.Add(drow)
    '                End If
    '                TransCountForAccount += 1

    '            End If

    '            AccClosingBalance(AccountId) = PrevBalance
    '            AccTransactionCount(AccountId) = TransCountForAccount

    '            If perLoadCount >= mobjGeneralLedger.PageSize Then
    '                Exit For
    '            End If
    '        Next

    '        RepGeneralLedger.DataSource = dtChartAcntDetails
    '        RepGeneralLedger.DataBind()

    '        CType(RepGeneralLedger.Controls(0).Controls(0).FindControl("ddlReconStatusFilter"), DropDownList).SelectedValue = hdnReconStatus.Value

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    <WebMethod()>
    Public Shared Function WebMethodLoadGeneralDetails(ByVal DomainID As Integer, ByVal pageIndex As Integer, ByVal PrevBalance As Decimal, ByVal strDateFormat As String, ByVal FromDate As String,
                                                       ByVal ToDate As String, ByVal DivisionId As Long, ByVal AccountID As String, ByVal ItemCode As String, ByVal ReconStatus As String) As String
        Try
            Dim mobjGeneralLedger1 As New GeneralLedger
            Dim dtChartAcntDetails As DataTable
            Dim dtFYClosingDate1 As DateTime
            Dim DisplayMode As Integer = 1
            Dim FromDate1 As New Date
            Dim ToDate1 As New Date
            Dim TotTransCountForAccount As Integer
            Dim perLoadCount As Integer = 0

            Dim dt1 As New DateTime
            FromDate1 = DateFromFormattedDate(FromDate, strDateFormat)
            ToDate1 = DateFromFormattedDate(ToDate, strDateFormat)

            Dim objCOA As New ChartOfAccounting
            objCOA.FinancialYearID = 0
            objCOA.DomainID = DomainID
            objCOA.Mode = 4
            Dim ds As DataSet = objCOA.GetFinancialYear()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("FYClosingDate").ToString <> "" Then
                    dtFYClosingDate1 = DateFromFormattedDate(ds.Tables(0).Rows(0)("FYClosingDate").ToString, strDateFormat)
                End If
            End If

            Dim dsGL As DataSet
            If mobjGeneralLedger1 Is Nothing Then mobjGeneralLedger1 = New GeneralLedger

            mobjGeneralLedger1.DomainID = DomainID
            mobjGeneralLedger1.FromDate = FromDate1
            mobjGeneralLedger1.ToDate = CDate(ToDate1 & " 23:59:59")
            Dim strAccountID As String = AccountID
            mobjGeneralLedger1.AccountIds = strAccountID
            mobjGeneralLedger1.DivisionID = DivisionId

            mobjGeneralLedger1.byteMode = 1
            mobjGeneralLedger1.CurrentPage = pageIndex
            mobjGeneralLedger1.PageSize = 100
            mobjGeneralLedger1.TotalRecords = 0
            mobjGeneralLedger1.ItemCode = CCommon.ToLong(ItemCode)

            Dim TotPages As Integer

            mobjGeneralLedger1.ReconStatus = ReconStatus
            dsGL = mobjGeneralLedger1.GetJournalEntriesReport_VirtualLoad()
            dtChartAcntDetails = dsGL.Tables(1).Clone()

            Dim TransCountForAccount As Integer
            Dim drow As DataRow
            Dim dtAccountDetails As New DataTable("AccountDetails")
            Dim drAccountDetails As DataRow
            dtAccountDetails.Columns.Add("AccountID")
            dtAccountDetails.Columns.Add("ItemID")
            dtAccountDetails.Columns.Add("TransactionLoaded")
            dtAccountDetails.Columns.Add("PrevBalance")
            If CCommon.ToLong(ItemCode) = 0 Then
                For Each dr As DataRow In dsGL.Tables(0).Rows

                    TotPages = ((pageIndex - 1) * 100) + TransCountForAccount
                    drAccountDetails = dtAccountDetails.NewRow
                    drAccountDetails("AccountID") = dr("numAccountId")
                    drAccountDetails("TransactionLoaded") = TotPages
                    drAccountDetails("PrevBalance") = PrevBalance
                    drAccountDetails("ItemID") = "0"
                    dtAccountDetails.Rows.Add(drAccountDetails)

                Next

                Dim i As Integer = 0
                Dim boolIsCreditTransaction As Boolean = False
                Dim dvData As DataView
                dvData = dsGL.Tables(1).DefaultView
                dvData.Sort = "Date ASC"
                Dim dtData As DataTable = dvData.ToTable()

                For Each childRow As DataRow In dtData.Rows
                    'If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                    '    childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numCreditAmt")) * -1))
                    '    boolIsCreditTransaction = True
                    'Else
                    '    boolIsCreditTransaction = False
                    'End If
                    'If i = 0 Then
                    '    'PrevBalance = dr("mnOpeningBalance")
                    '    childRow("balance") = PrevBalance + childRow("numDebitAmt")
                    '    PrevBalance = childRow("balance")
                    'Else
                    '    childRow("balance") = PrevBalance + childRow("numDebitAmt")
                    '    PrevBalance = childRow("balance")
                    'End If
                    ''added by chintan
                    'If boolIsCreditTransaction = True Then
                    '    childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
                    'Else
                    '    childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
                    'End If

                    'childRow("vcAccountName") = childRow("vcSplitAccountName")
                    'childRow("balance") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("balance"))))
                    dtChartAcntDetails.ImportRow(childRow)
                    i += 1
                    TransCountForAccount += 1
                    perLoadCount += 1

                Next

            Else
                Dim dtTransactions As DataTable
                dtChartAcntDetails = dsGL.Tables(1)

                'For Each childRow As DataRow In dtChartAcntDetails.Rows
                '    If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                '        childRow("numDebitAmt") = String.Format("{0:#,##0.00}", (CCommon.ToDouble(childRow("numCreditAmt")) * -1))
                '        childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
                '    Else
                '        childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
                '    End If
                '    TransCountForAccount += 1
                'Next

                TotPages = ((pageIndex - 1) * 100) + TransCountForAccount
                drAccountDetails = dtAccountDetails.NewRow
                drAccountDetails("AccountID") = AccountID
                drAccountDetails("TransactionLoaded") = TotPages
                drAccountDetails("PrevBalance") = 0
                drAccountDetails("ItemID") = ItemCode

                dtAccountDetails.Rows.Add(drAccountDetails)

            End If
            Dim dt123 As DataTable
            dt123 = dtChartAcntDetails.Copy()
            Dim dsChartAcntDetails As New DataSet
            dsChartAcntDetails.Tables.Add(dt123)
            Dim dtCopyAccDet As DataTable
            dtCopyAccDet = dtAccountDetails.Copy()

            dsChartAcntDetails.Tables.Add(dtCopyAccDet)
            'Dim returnval = dsChartAcntDetails.GetXml()
            'Return returnval

            'Threading.Thread.Sleep(6000)
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsChartAcntDetails, Formatting.None)
            Return json

            'CType(RepGeneralLedger.Controls(0).Controls(0).FindControl("ddlReconStatusFilter"), DropDownList).SelectedValue = hdnReconStatus.Value

        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try

    End Function

    <WebMethod()>
    Public Shared Function WebMethodGetTransactionDetail(ByVal DomainID As Integer, ByVal TransactionID As Integer) As String

        Dim mobjGeneralLedger As New GeneralLedger
        Try

            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.DomainID = DomainID
            mobjGeneralLedger.TransactionID = TransactionID

            Dim ds As DataSet = mobjGeneralLedger.GetGLTransactionDetailById() 'GetChartAcntDetailsForGL()
            Dim dt As DataTable = ds.Tables(0).Copy()

            Dim dsTransactionDetail As New DataSet
            dsTransactionDetail.Tables.Add(dt)
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsTransactionDetail, Formatting.None)
            Return json

        Catch ex As Exception
            Dim strError As String = ""
            strError = ex.Message
            Throw New Exception(strError)

        End Try

    End Function

    <WebMethod()>
    Public Shared Function WebMethodGetDepositDetails(ByVal DomainID As Integer, ByVal lintDepositId As Integer) As String
        Try

            Dim objMakeDeposit As New MakeDeposit
            objMakeDeposit.DepositId = lintDepositId
            objMakeDeposit.DomainID = DomainID
            objMakeDeposit.Mode = 0
            Dim dtMakeDeposit As DataTable = objMakeDeposit.GetDepositDetails().Tables(0)

            Dim dt As DataTable = dtMakeDeposit.Copy()
            Dim dsDepositDetails As New DataSet
            dsDepositDetails.Tables.Add(dt)
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dsDepositDetails, Formatting.None)
            Return json

            'If dtMakeDeposit.Rows.Count > 0 Then
            '    If CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 2 Then
            '        Response.Redirect("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" & lintDepositId)
            '    ElseIf CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 1 Then
            '        Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&ChartAcntId=" & lintChartAcntId & "&DepositId=" & lintDepositId)
            '    End If
            'End If

        Catch ex As Exception
            Dim strError As String = ""
            strError = ex.Message
            Throw New Exception(strError)

        End Try

    End Function

    <WebMethod()>
    Public Shared Function WebMethodDeleteTransaction(ByVal DomainID As Integer, ByVal UserContactID As Integer, ByVal linJournalId As Integer,
                                                     ByVal lintBillPaymentID As Integer, ByVal lintDepositId As Integer, ByVal lintCheckId As Integer,
                                                       ByVal lintBillID As Integer, ByVal lintCategoryHDRID As Integer, ByVal lintReturnID As Integer) As String
        Try
            Dim objJournalEntry As New JournalEntry
            objJournalEntry.JournalId = linJournalId
            objJournalEntry.BillPaymentID = lintBillPaymentID
            objJournalEntry.DepositId = lintDepositId
            objJournalEntry.CheckHeaderID = lintCheckId
            objJournalEntry.BillID = lintBillID
            objJournalEntry.CategoryHDRID = lintCategoryHDRID
            objJournalEntry.ReturnID = lintReturnID
            objJournalEntry.DomainID = DomainID
            objJournalEntry.UserCntID = UserContactID
            objJournalEntry.DeleteJournalEntryDetails()

        Catch ex As Exception
            Dim strError As String = ""
            'strError = ex.Message

            If ex.Message = "BILL_PAID" Then
                ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                strError = "This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first."

            ElseIf ex.Message = "Undeposited_Account" Then
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                strError = "This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first."

            ElseIf ex.Message = "CreditMemo_PAID" Then
                ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Credit Memo applied.');", True)
                strError = "Credit Memo applied."

            ElseIf ex.Message = "InventoryAdjustment" Then
                ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You can not delete inventory adjustment for item, since it is automatically managed by Biz as inventory count goes up or down.');", True)
                strError = "You can not delete inventory adjustment for item, since it is automatically managed by Biz as inventory count goes up or down.."

            ElseIf ex.Message = "OpeningBalance" Then
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You can not delete Opening balance journal of Chart of Account, Your option is to update opening balance of respective chart of account to zero.');", True)
                strError = "You can not delete Opening balance journal of Chart of Account, Your option is to update opening balance of respective chart of account to zero."
            Else
                strError = ex.Message

            End If

            Throw New Exception(strError)

        End Try
        Return "Transaction details deleted Successfully!.,"

    End Function

    Private Sub ibExportExcel_Click(sender As Object, e As ImageClickEventArgs) Handles ibExportExcel.Click
        Dim dsGL As DataSet
        If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
        mobjGeneralLedger.DomainID = intDomainID
        mobjGeneralLedger.FromDate = calFrom.SelectedDate
        mobjGeneralLedger.ToDate = CDate(calTo.SelectedDate & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
        Dim strAccountID As String = hdnAccounts.Value
        mobjGeneralLedger.AccountIds = strAccountID
        mobjGeneralLedger.DivisionID = lngDivisionID
        mobjGeneralLedger.ReconStatus = CCommon.ToString(hdnReconStatus.Value)
        dsGL = mobjGeneralLedger.GetJournalEntries() 'GetChartAcntDetailsForGL()

        Dim dtChartAcntDetails As New DataTable
        CCommon.AddColumnsToDataTable(dtChartAcntDetails, "Date,TransactionType,Name,MemoDescription,Split,Amount,Balance,Narration,Reconcile")

        Dim drow As DataRow
        For Each dr As DataRow In dsGL.Tables(2).Rows
            Dim drChild() As DataRow = dsGL.Tables(1).Select("Date='" & CCommon.ToString(dr("dt")) & "'")

            'drow = dtChartAcntDetails.NewRow

            'drow("Date") = dr("vcAccountName")
            'drow("TransactionType") = ""
            'drow("Name") = ""
            'drow("Amount") = ""
            'drow("Balance") = "" '"<b>" & dr("mnOpeningBalance") & "</b>"
            'drow("MemoDescription") = ""
            'drow("Split") = ""
            'drow("Narration") = ""
            'drow("Reconcile") = ""

            'If DisplayMode = 2 Then
            '    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
            'Else
            '    dtChartAcntDetails.Rows.Add(drow)
            'End If


            'drow = dtChartAcntDetails.NewRow

            'drow("Date") = "Opening Balance"
            'drow("TransactionType") = ""
            'drow("Name") = ""
            'drow("Amount") = ReturnMoney(dr("mnOpeningBalance"))
            'drow("Balance") = ReturnMoney(dr("mnOpeningBalance"))
            'drow("MemoDescription") = ""
            'drow("Split") = ""
            'drow("Narration") = ""
            'drow("Reconcile") = ""

            'If DisplayMode = 2 Then
            '    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
            'Else
            '    dtChartAcntDetails.Rows.Add(drow)
            'End If


            Dim PrevBalance As Double
            Dim i As Integer = 0
            Dim boolIsCreditTransaction As Boolean = False
            For Each childRow As DataRow In drChild
                If CCommon.ToDouble(childRow("numDebitAmt")) = 0 Then
                    childRow("numDebitAmt") = ReturnMoney(CCommon.ToDouble(childRow("numCreditAmt")) * -1)
                    boolIsCreditTransaction = True
                Else
                    boolIsCreditTransaction = False
                End If

                'If i = 0 Then
                '    PrevBalance = dr("mnOpeningBalance")
                '    childRow("balance") = PrevBalance + childRow("numDebitAmt")
                '    PrevBalance = childRow("balance")
                'Else
                '    childRow("balance") = PrevBalance + childRow("numDebitAmt")
                '    PrevBalance = childRow("balance")
                'End If

                If boolIsCreditTransaction = True Then
                    childRow("numDebitAmt") = childRow("numDebitAmt") & " Cr"
                Else
                    childRow("numDebitAmt") = childRow("numDebitAmt") & " Dr"
                End If

                childRow("balance") = ReturnMoney(CCommon.ToDouble(childRow("balance")))
                dtChartAcntDetails.ImportRow(childRow)

                drow = dtChartAcntDetails.NewRow

                drow("Date") = childRow("Date")
                drow("TransactionType") = childRow("TransactionType")
                drow("Name") = childRow("CompanyName")
                drow("Amount") = childRow("numDebitAmt")
                drow("Balance") = childRow("balance")
                drow("MemoDescription") = childRow("vcAccountName")
                drow("Split") = childRow("vcSplitAccountName")
                drow("Narration") = childRow("TranRef") + " " + childRow("Narration")
                drow("Reconcile") = IIf(childRow("bitReconcile"), "R", IIf(childRow("bitCleared"), "C", " "))
                dtChartAcntDetails.Rows.Add(drow)

                i += 1
            Next
            'drow = dtChartAcntDetails.NewRow

            'drow("Date") = "Closing Balance"
            'drow("TransactionType") = ""
            'drow("Name") = ""
            'drow("Amount") = ""
            'drow("Balance") = ReturnMoney(PrevBalance)
            'drow("MemoDescription") = ""
            'drow("Split") = ""
            'drow("Narration") = ""
            'drow("Reconcile") = ""

            'If 2 = 2 Then
            '    If drChild.Length > 0 Then dtChartAcntDetails.Rows.Add(drow)
            'Else
            '    dtChartAcntDetails.Rows.Add(drow)
            'End If
        Next

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=JournalEntries" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".csv")
        Response.Charset = ""
        Response.ContentType = "text/csv"
        Dim stringWrite As New System.IO.StringWriter
        CSVExport.ProduceCSV(dtChartAcntDetails, stringWrite, True)
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
End Class