''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Infragistics.WebUI.UltraWebGrid

Partial Public Class frmMarketBudget
    Inherits BACRMPage
#Region "Variables"
    Dim mobjBudget As Budget
#End Region

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not IsPostBack Then
    '            
    '           
    '            
    '            GetUserRightsForPage( 35, 100)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
    '                Response.Redirect("../admin/authentication.aspx?mesg=AS")
    '            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
    '                btnMarketSave.Visible = False
    '            End If
    '            LoadMarketBudget()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadMarketBudget()
    '    Try
    '        Dim dtMarket As DataTable
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        mobjBudget.DomainId = Session("DomainId")
    '        Select Case ddlMarketFiscal.SelectedIndex
    '            Case 0
    '                mobjBudget.FiscalYear = Now.Year
    '                mobjBudget.Type = 0
    '            Case 1
    '                mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                mobjBudget.Type = -1
    '            Case 2
    '                mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                mobjBudget.Type = 1
    '        End Select
    '        dtMarket = mobjBudget.GetMarketMasterDetails()
    '        If dtMarket.Rows.Count > 0 Then
    '            chkMarketCampaignDeal.Checked = dtMarket.Rows.Item(0)("bitMarketCampaign")
    '            mobjBudget.MarketBudgetId = dtMarket.Rows.Item(0)("numMarketBudgetId")
    '        Else
    '            chkMarketCampaignDeal.Checked = False
    '            mobjBudget.MarketBudgetId = 0
    '        End If
    '        UlgMarket.Clear()

    '        UlgMarket.DataSource = mobjBudget.GetMarketBudgetDetails
    '        UlgMarket.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnMarketSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMarketSave.Click
    '    Try
    '        Dim lngMarketBudgetId As Long
    '        lngMarketBudgetId = SaveMarketBudgetMaster()
    '        SaveMarketBudgetDetails(lngMarketBudgetId)
    '        LoadMarketBudget()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Function SaveMarketBudgetMaster() As Long
    '    Dim lngMarketBudgetId As Long
    '    Try
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        With mobjBudget
    '            .DomainId = Session("DomainId")
    '            .UserCntID = Session("UserContactID")
    '            Select Case ddlMarketFiscal.SelectedIndex
    '                Case 0
    '                    .FiscalYear = Now.Year
    '                Case 1
    '                    .FiscalYear = CInt(Now.Year - 1)
    '                Case 2
    '                    .FiscalYear = CInt(Now.Year + 1)
    '            End Select
    '            .MarketCampaignBit = chkMarketCampaignDeal.Checked
    '            lngMarketBudgetId = .SaveMarketBudgetMaster()
    '            Return lngMarketBudgetId
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Sub SaveMarketBudgetDetails(ByVal lngMarketBudgetId As Long)
    '    Try
    '        Dim dtMarketBudget As New DataTable
    '        Dim ulrow As UltraGridRow
    '        Dim dr As DataRow
    '        Dim ds As New DataSet
    '        Dim lstr As String
    '        Dim i As Integer
    '        If mobjBudget Is Nothing Then mobjBudget = New Budget
    '        dtMarketBudget.Columns.Add("numMarketId") '' Primary key
    '        dtMarketBudget.Columns.Add("numListItemID")
    '        dtMarketBudget.Columns.Add("tintMonth")
    '        dtMarketBudget.Columns.Add("intYear")
    '        dtMarketBudget.Columns.Add("monAmount")
    '        dtMarketBudget.Columns.Add("vcComments")

    '        For Each ulrow In UlgMarket.Rows
    '            For i = 3 To UlgMarket.Columns.Count - 3
    '                dr = dtMarketBudget.NewRow
    '                dr("numMarketId") = lngMarketBudgetId
    '                dr("numListItemID") = ulrow.Cells.FromKey("numListItemID").Value
    '                dr("tintMonth") = ulrow.Cells.FromKey(UlgMarket.Columns.Item(i).Key).Key.Split("~")(0)
    '                If Not IsNothing(ulrow.Cells.FromKey(UlgMarket.Columns.Item(i).Key).Value) Then
    '                    dr("monAmount") = IIf(ulrow.Cells.FromKey(UlgMarket.Columns.Item(i).Key).Value.ToString = "False", 0, ulrow.Cells.FromKey(UlgMarket.Columns.Item(i).Key).Value)
    '                Else : dr("monAmount") = ulrow.Cells.FromKey(UlgMarket.Columns.Item(i).Key).Value
    '                End If
    '                dr("intYear") = ulrow.Cells.FromKey(UlgMarket.Columns.Item(i).Key).Key.Split("~")(1)
    '                dr("vcComments") = ulrow.Cells.FromKey("Comments").Value
    '                dtMarketBudget.Rows.Add(dr)
    '            Next
    '        Next
    '        Select Case ddlMarketFiscal.SelectedIndex
    '            Case 0
    '                mobjBudget.FiscalYear = Now.Year
    '                mobjBudget.Type = 0
    '            Case 1
    '                mobjBudget.FiscalYear = CInt(Now.Year - 1)
    '                mobjBudget.Type = -1
    '            Case 2
    '                mobjBudget.FiscalYear = CInt(Now.Year + 1)
    '                mobjBudget.Type = 1
    '        End Select
    '        mobjBudget.MarketBudgetId = lngMarketBudgetId
    '        ds.Tables.Add(dtMarketBudget)
    '        lstr = ds.GetXml()
    '        mobjBudget.MarketBudgetDetails = lstr
    '        mobjBudget.SaveMarketBudgetDetails()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnMarketClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMarketClose.Click
    '    Try
    '        ddlMarketFiscal.SelectedIndex = 0
    '        LoadMarketBudget()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub UlgMarket_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles UlgMarket.InitializeLayout
    '    Try
    '        Dim i As Integer
    '        UlgMarket.Bands(0).Columns.FromKey("numListItemID").Hidden = True
    '        UlgMarket.Bands(0).Columns.FromKey("numDomainId").Hidden = True
    '        UlgMarket.Bands(0).Columns.FromKey("vcData").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        UlgMarket.Bands(0).Columns.FromKey("Total").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '        UlgMarket.Bands(0).Columns.FromKey("vcData").HeaderText = "Marketing Budget Accounts"
    '        UlgMarket.Bands(0).Columns.FromKey("vcData").Width = Unit.Percentage(15)
    '        UlgMarket.Bands(0).Columns.FromKey("Total").DataType = "System.Decimal"
    '        UlgMarket.Bands(0).Columns.FromKey("Total").Format = "###,###.##"
    '        UlgMarket.Bands(0).Columns.FromKey("Total").Width = Unit.Percentage(5)

    '        ''To Set Month Name
    '        For i = 3 To UlgMarket.Columns.Count - 3
    '            UlgMarket.Columns.Item(i).HeaderText = MonthName(UlgMarket.Columns.Item(i).Key.Split("~")(0)).Substring(0, 3)
    '            UlgMarket.Bands(0).Columns.FromKey(UlgMarket.Columns.Item(i).Key).DataType = "System.Decimal"
    '            UlgMarket.Bands(0).Columns.FromKey(UlgMarket.Columns.Item(i).Key).Format = "###,###.##"
    '            UlgMarket.Bands(0).Columns.FromKey(UlgMarket.Columns.Item(i).Key).Width = Unit.Percentage(5)
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlMarketFiscal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMarketFiscal.SelectedIndexChanged
    '    Try
    '        If Not IsNothing(ddlMarketFiscal.SelectedItem.Value) Then LoadMarketBudget()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class