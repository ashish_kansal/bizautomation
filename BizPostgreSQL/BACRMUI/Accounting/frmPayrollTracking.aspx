﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmPayrollTracking.aspx.vb" Inherits=".frmPayrollTracking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#chkAllProjectCommission").change(function () {
                $("#gvProjectCommission tr input[type = 'checkbox']").not("#chkAllProjectCommission").prop("checked", $("#chkAllProjectCommission").is(':checked'));
                UpdateProjectCommission();
            });

            $("#gvProjectCommission tr input[type = 'checkbox']").not("#chkAllProjectCommission").change(function () {
                UpdateProjectCommission();
            });

            function UpdateProjectCommission() {

                var lblBalanceAmt = 0.00;
                $("#gvProjectCommission tr").not(".hs,.fs").each(function () {
                    chkSelect = $(this).find("[id*='chkSelect']");
                    txtAmount = $(this).find("[id*='txtAmount']");

                    if ($(chkSelect).is(':checked')) {
                        ComissionAmount = Number($(this).find("#hdnComissionAmount").val());
                        PaidAmount = Number($(this).find("#hdnPaidAmount").val());

                        $(txtAmount).val(Number(ComissionAmount - PaidAmount).toFixed(2));
                    }
                    else {
                        $(txtAmount).val("0");
                    }
                });
            }

            $("#chkAllOrderCommission").change(function () {
                $("#gvCommission tr input[type = 'checkbox']").not("#chkAllOrderCommission").prop("checked", $("#chkAllOrderCommission").is(':checked'));
                UpdateOrderCommission();
            });

            $("#gvCommission tr input[type = 'checkbox']").not("#chkAllOrderCommission").change(function () {
                UpdateOrderCommission();
            });

            function UpdateOrderCommission() {

                var lblBalanceAmt = 0.00;
                $("#gvCommission tr").not(".hs,.fs").each(function () {
                    chkSelect = $(this).find("[id*='chkSelect']");
                    txtAmount = $(this).find("[id*='txtAmount']");

                    if ($(chkSelect).is(':checked')) {
                        ComissionAmount = Number($(this).find("#hdnComissionAmount").val());
                        PaidAmount = Number($(this).find("#hdnPaidAmount").val());

                        $(txtAmount).val(Number(ComissionAmount - PaidAmount).toFixed(2));
                    }
                    else {
                        $(txtAmount).val("0");
                    }
                });
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="800px">
        <tr>
            <td>
                <asp:GridView ID="gvPayrollData" runat="server" CssClass="tbl" BorderWidth="0" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="numCategoryHDRID,numPayrollDetailID">
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <RowStyle VerticalAlign="Top" />
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Width="10px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll', 'chkSelect');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" Checked='<%# Eval("bitPaid")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From - To">
                            <ItemTemplate>
                                <%# Eval("dtFromDate")%>
                                -
                                <%# Eval("dtToDate")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Hrs">
                            <ItemTemplate>
                                <%# Eval("Hrs")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <%# Eval("monamount","{0:###0.00}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:GridView ID="gvCommission" runat="server" CssClass="tbl" BorderWidth="0" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="numComissionID">
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <RowStyle VerticalAlign="Top" />
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Width="10px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAllOrderCommission" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" Checked='<%# Eval("bitPaid")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Opp Name">
                            <ItemTemplate>
                                <%# Eval("vcPOppName")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Original Amount">
                            <ItemTemplate>
                                <%# Eval("numComissionAmount", "{0:###0.00}")%>
                                <asp:HiddenField ID="hdnComissionAmount" runat="server" Value='<%# Eval("numComissionAmount")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paid Amount">
                            <ItemTemplate>
                                <%# Eval("monPaidAmount", "{0:###0.00}")%>
                                <asp:HiddenField ID="hdnPaidAmount" runat="server" Value='<%# Eval("monPaidAmount")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" Width="60" runat="server" MaxLength="10" Text='<%# Eval("monUsedAmount", "{0:###0.00}")%>'
                                    AUTOCOMPLETE="OFF" CssClass="required_money {required:false ,number:true,maxlength:15, messages:{number:'Please provide valid Deduction(Max Length 10)!'}} money"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:GridView ID="gvProjectCommission" runat="server" CssClass="tbl" BorderWidth="0" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="numComissionID">
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <RowStyle VerticalAlign="Top" />
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-Width="10px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAllProjectCommission" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" Checked='<%# Eval("bitPaid")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Project Name
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("vcProjectID")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="dtCompletionDate" HeaderText="Completion Date"></asp:BoundField>
                        <asp:BoundField DataField="monProTotalIncome" HeaderText="Income" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
                        <asp:BoundField DataField="monProTotalExpense" HeaderText="Expense" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
                        <asp:BoundField DataField="monProTotalGrossProfit" HeaderText="Gross Profit" HtmlEncode="false" DataFormatString="{0:#,###.0000}"></asp:BoundField>
                        <asp:TemplateField HeaderText="Original Amount">
                            <ItemTemplate>
                                <%# Eval("numComissionAmount", "{0:###0.00}")%>
                                <asp:HiddenField ID="hdnComissionAmount" runat="server" Value='<%# Eval("numComissionAmount")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paid Amount">
                            <ItemTemplate>
                                <%# Eval("monPaidAmount", "{0:###0.00}")%>
                                <asp:HiddenField ID="hdnPaidAmount" runat="server" Value='<%# Eval("monPaidAmount")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" Width="60" runat="server" MaxLength="10" Text='<%# Eval("monUsedAmount", "{0:###0.00}")%>'
                                    AUTOCOMPLETE="OFF" CssClass="required_money {required:false ,number:true,maxlength:15, messages:{number:'Please provide valid Deduction(Max Length 10)!'}} money"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
