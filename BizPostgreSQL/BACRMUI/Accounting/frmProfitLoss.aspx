<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProfitLoss.aspx.vb"
    Inherits=".frmProfitLoss" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Profit & Loss Accounts</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function OpenMonthySummary(a, b, c, d, e) {

            if (e == 1 || e == 3) {
                var dateFormat = '<%= Session("DateFormat")%>';
                var fromDate = $("[id$=ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate]").val();
                var toDate = $("[id$=ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate]").val();
                if (dateFormat != "") {
                    fromDate = new Date(getDateFromFormat(fromDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));
                    toDate = new Date(getDateFromFormat(toDate, dateFormat.replace(/(MONTH)/g, "MMM").replace(/(MON)/g, "MMM").replace(/(D)/g, "d").replace(/(Y)/g, "y")));

                    fromDate = ((fromDate.getMonth() > 8) ? (fromDate.getMonth() + 1) : ('0' + (fromDate.getMonth() + 1))) + '/' + ((fromDate.getDate() > 9) ? fromDate.getDate() : ('0' + fromDate.getDate())) + '/' + fromDate.getFullYear();
                    toDate = ((toDate.getMonth() > 8) ? (toDate.getMonth() + 1) : ('0' + (toDate.getMonth() + 1))) + '/' + ((toDate.getDate() > 9) ? toDate.getDate() : ('0' + toDate.getDate())) + '/' + toDate.getFullYear();
                }

                if (a == -1) {
                    window.location.href = '../Accounting/frmJournalEntry.aspx?ModeID=2&FromDate=' + fromDate + '&ToDate=' + toDate + '&frm=frmProfitLoss' + "&From=" + fromDate + "&To=" + toDate;
                }
                else {
                    window.location.href = '../Accounting/frmMonthlySummary.aspx?ChartAcntId=' + a + '&Opening=' + b + '&Code=' + c + '&frm=frmProfitLoss' + '&Name=' + d + "&From=" + fromDate + "&To=" + toDate;
                }
            }
        }

        function OpenGLReport(a, b) {
            window.location.href = "../Accounting/frmGeneralLedger.aspx?Mode=4&AcntTypeID=" + a + "&Month=0&AccountID=" + b + "&From=" + $("#ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate").val() + "&To=" + $("#ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate").val();
            return false;
        }

        $(document).ready(function () {


            $(".tblRow").hover(
                    function () {
                        if ($(this).find(".linkColor").length > 0) {
                            $(this).find("#imgGL").show();
                        }
                    },
                    function () { $(this).find("#imgGL").hide(); }

            );
        });
    </script>
    <style>
        .linkColor {
            color: Green;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:Panel runat="server" ID="pnlAccountingClass">
                            <label>Class</label>
                            <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="signup form-control" AutoPostBack="true">
                            </asp:DropDownList>
                        </asp:Panel>
                    </div>
                    <div class="form-group">
                        <label>
                            From
                        </label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>
                            To</label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <asp:LinkButton ID="btnGo" runat="server" CssClass="button btn btn-primary"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <asp:UpdatePanel ID="updatepanelexcel" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnExportExcel" runat="server" CssClass="button btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnExportExcel" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="Profit & Loss Accounts" runat="server" ID="lblReportName" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6 col-md-offset-3">
        <asp:Table ID="tblGeneralLeger" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
            Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
            <asp:TableRow Width="100%">
                <asp:TableCell VerticalAlign="top">
                    <table cellpadding="0" align="center" width="100%" runat="server" id="tblExport">
                        <tr>
                            <td>
                                <asp:Repeater ID="RepProfitLoss" runat="server">
                                    <HeaderTemplate>
                                        <table cellpadding="0" class="table table-responsive table-bordered" cellspacing="0" border="0">
                                            <tr class="hs">
                                                <th class="td1" align="center">
                                                    <b>Account</b>
                                                </th>
                                                <th class="td1" align="right">
                                                    <b>Amount</b>
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="tblRow">
                                            <td>
                                                <span style="" onclick="javascript:OpenMonthySummary('<%#Eval("numAccountID") %>','<%#Eval("Opening") %>','<%#Eval("vcAccountCode") %>','<%#System.Web.HttpUtility.JavaScriptStringEncode(Eval("vcAccountName").ToString()) %>','<%# Eval("Type")%>');">
                                                    <%#IIf(Eval("Type") > 1, "<b>" & Eval("vcAccountName1").ToString() & "<b>", "<span class='linkColor'>" & Eval("vcAccountName1") & "</span>")%>
                                                </span>
                                                <span class="GLReportSpan">
                                                    <img id="imgGL" alt='General Ledger' height='16px' width='16px' onclick='return OpenGLReport(<%#Eval("numParntAcntTypeID") %>,<%#Eval("numAccountID") %>)' title='General Ledger' src='../images/GLReport.png' style='display: none;'></span>
                                            </td>
                                            <td align="right">
                                                <%#IIf(Eval("Type") > 1, "<b>" & ReturnMoney(Eval("Balance")).ToString() & "<b>", ReturnMoney(Eval("Balance")))%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
