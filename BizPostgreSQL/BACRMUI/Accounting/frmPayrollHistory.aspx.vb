﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Public Class frmPayrollHistory
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                FillEmployeeName()
                LoadGrid()
                BindPaymentToDropdown()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadGrid()
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            gvPayrollList.DataSource = objPayrollExpenses.GetPayrollHistory()
            gvPayrollList.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindPaymentToDropdown()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "010101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("vcStartingCheckNumber")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlPaymentFrom.Items.Add(item)
            Next
            ddlPaymentFrom.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FillEmployeeName()
        Try
            Dim objContacts As CContacts
            Dim dtEmployeeList As New DataTable

            objContacts = New CContacts
            objContacts.DomainID = Session("DomainID")
            dtEmployeeList = objContacts.EmployeeList

            ddlEmployee.DataSource = dtEmployeeList
            ddlEmployee.DataTextField = "vcUserName"
            ddlEmployee.DataValueField = "numContactID"
            ddlEmployee.DataBind()

            ddlEmployee.Items.Insert(0, New ListItem("All", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub ddlPaymentFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaymentFrom.SelectedIndexChanged
        Try
            ddlPaymentFrom_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlPaymentFrom_SelectedIndexChanged()
        Try
            Dim lobjCashCreditCard As New CashCreditCard
            Dim lstrColor As String
            Dim ldecOpeningBalance As Decimal
            If ddlPaymentFrom.SelectedItem.Value <> "0" Then
                lobjCashCreditCard.AccountId = ddlPaymentFrom.SelectedValue.Split("~")(0)
                lobjCashCreditCard.DomainID = Session("DomainId")
                lblBalance.Text = "Balance: " & Session("Currency").ToString.Trim
                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
                lstrColor = "<font color=red>" & ReturnMoney(ldecOpeningBalance) & "</font>"
                lblBalanceAmount.Text = IIf(ldecOpeningBalance < 0, lstrColor, ReturnMoney(ldecOpeningBalance))
                'lblPaymentTotalAmount.Text = ReturnMoney(CDec(DepositTotalAmt.Value))
            Else
                lblBalanceAmount.Text = ""
                lblBalance.Text = ""
                'lblDepositTotalAmount.Text = ReturnMoney(CDec(DepositTotalAmt.Value))
            End If
            PersistTable.Clear()
            PersistTable.Add(ddlPaymentFrom.ID, ddlPaymentFrom.SelectedValue.Split("~")(0))
            PersistTable.Save()


            If ddlPaymentFrom.SelectedValue.Contains("~") Then
                txtCheckNo.Text = ddlPaymentFrom.SelectedValue.Split("~")(1)
                hfCheckNo.Value = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(1))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Dim lngAPAccountId, lngEPAccountId, lngCPAccountId, lngDPAccountId As Long
    Dim decAmount, decDeduction As Decimal
    Dim lngDivisionID, lngUserCntID, lngPayrollDetailID As Long

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim objJournal As New JournalEntry

            lngEPAccountId = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID"))
            lngCPAccountId = ChartOfAccounting.GetDefaultAccount("CP", Session("DomainID"))
            lngDPAccountId = ChartOfAccounting.GetDefaultAccount("DP", Session("DomainID"))
            lngAPAccountId = ChartOfAccounting.GetDefaultAccount("PL", Session("DomainID"))

            If lngEPAccountId = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Employee Payroll Expense account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save');", True)
                Exit Sub
            End If

            If lngCPAccountId = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Contract Employee Payroll Expense account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save');", True)
                Exit Sub
            End If

            If lngDPAccountId = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Deductions form Employee Payroll account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save');", True)
                Exit Sub
            End If

            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainID")

            For Each gvRow As GridViewRow In gvPayrollList.Rows
                If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked Then
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        objPayrollExpenses.PayrollHeaderID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollHeaderID"))
                        objPayrollExpenses.PayrollDetailID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollDetailID"))
                        objPayrollExpenses.CheckStatus = 1
                        objPayrollExpenses.Mode = 1
                        objPayrollExpenses.ManagePayrollDetail()

                        decAmount = CCommon.ToDecimal(CType(gvRow.FindControl("hdnTotalAmt"), HiddenField).Value)
                        decDeduction = CCommon.ToDecimal(CType(gvRow.FindControl("hdnDeductions"), HiddenField).Value)

                        lngDivisionID = CCommon.ToLong(CType(gvRow.FindControl("hdnDivisionID"), HiddenField).Value)
                        lngUserCntID = CCommon.ToLong(CType(gvRow.FindControl("hdnUserCntID"), HiddenField).Value)
                        lngPayrollDetailID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollDetailID"))

                        Dim hdnPaydate As DateTime = CDate(CType(gvRow.FindControl("hdnPaydate"), HiddenField).Value)

                        Dim lngJournalID As Long = SaveDataToHeader(hdnPaydate)
                        SaveDataToGeneralJournalDetails(lngJournalID)

                        objTransactionScope.Complete()
                    End Using
                End If
            Next

            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Function SaveDataToHeader(ByVal hdnPaydate As DateTime) As Long
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = 0
                .RecurringId = 0
                .EntryDate = hdnPaydate.Date & " 12:00:00"
                .Description = "Payroll"
                .Amount = decAmount
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .PayrollHeaderID = lngPayrollDetailID
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            Dim lngJournalID As Long = objJEHeader.Save()
            Return lngJournalID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalID As Long)
        Try

            'Save Journal Entry Detail
            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()

            If decAmount > 0 Then
                ' Employee's A/c will be get credited
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = decAmount
                objJE.ChartAcntId = lngAPAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '2
                objJE.Description = ""
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "PL" 'Accoount Payable
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = Session("BaseCurrencyID")
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = lngUserCntID
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.PayrollCommission
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                ' Payroll expenses A/c will be debited and
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decAmount
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngEPAccountId 'IIf(bitContractBased = True, lngCPAccountId, lngEPAccountId)
                objJE.Description = ""
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "EP" 'IIf(bitContractBased = True, "CP", "EP")
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = Session("BaseCurrencyID")
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = lngUserCntID
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.PayrollCommission
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            End If


            If decDeduction > 0 Then
                'Accounts Payable (Employee’s A/c) will be debited
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDeduction
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngAPAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '2
                objJE.Description = "Debit to Accounts Payable"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "PL" 'Accoount Payable
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = Session("BaseCurrencyID")
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = lngUserCntID
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.PayrollCommission
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Deductions form Employee Payroll will be credited 
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = decDeduction
                objJE.ChartAcntId = lngDPAccountId
                objJE.Description = "Credit to Deductions Employee Payroll"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "DP"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = Session("BaseCurrencyID")
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = lngUserCntID
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.PayrollCommission
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            End If

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainID")

            For Each gvRow As GridViewRow In gvPayrollList.Rows
                If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked Then
                    objPayrollExpenses.PayrollHeaderID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollHeaderID"))
                    objPayrollExpenses.PayrollDetailID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollDetailID"))
                    objPayrollExpenses.Mode = 2
                    objPayrollExpenses.ManagePayrollDetail()
                End If
            Next

            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvPayrollList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPayrollList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 1 Or DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 2 Then
                    e.Row.FindControl("chkSelect").Visible = False
                End If

                If DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 0 Or DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 2 Then
                    e.Row.FindControl("chkWriteCheck").Visible = False
                End If

                If DataBinder.Eval(e.Row.DataItem, "numCheckStatus") = 2 Then
                    If DataBinder.Eval(e.Row.DataItem, "numCheckNo") > 0 Then
                        CType(e.Row.FindControl("lblCheckNo"), Label).Text = "Check No :" & DataBinder.Eval(e.Row.DataItem, "numCheckNo")
                    Else
                        CType(e.Row.FindControl("lblCheckNo"), Label).Text = "Pending Printing"
                    End If
                End If

                'Dim ddlPayrollStatus As DropDownList
                'ddlPayrollStatus = DirectCast(e.Row.FindControl("ddlPayrollStatus"), DropDownList)
                'If ddlPayrollStatus IsNot Nothing Then

                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnWriteCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWriteCheck.Click
        Try
            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainID")

            Dim lngCheckNo As Long = CCommon.ToLong(txtCheckNo.Text)

            lngAPAccountId = ChartOfAccounting.GetDefaultAccount("PL", Session("DomainID"))

            For Each gvRow As GridViewRow In gvPayrollList.Rows
                If CType(gvRow.FindControl("chkWriteCheck"), CheckBox).Checked Then
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        objPayrollExpenses.PayrollHeaderID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollHeaderID"))
                        objPayrollExpenses.PayrollDetailID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollDetailID"))
                        objPayrollExpenses.CheckStatus = 2
                        objPayrollExpenses.Mode = 1
                        objPayrollExpenses.ManagePayrollDetail()

                        decAmount = CCommon.ToDecimal(CType(gvRow.FindControl("hdnTotalAmt"), HiddenField).Value)
                        decDeduction = CCommon.ToDecimal(CType(gvRow.FindControl("hdnDeductions"), HiddenField).Value)

                        lngDivisionID = CCommon.ToLong(CType(gvRow.FindControl("hdnDivisionID"), HiddenField).Value)
                        lngUserCntID = CCommon.ToLong(CType(gvRow.FindControl("hdnUserCntID"), HiddenField).Value)
                        lngPayrollDetailID = CCommon.ToLong(gvPayrollList.DataKeys(gvRow.DataItemIndex)("numPayrollDetailID"))

                        Dim hdnPaydate As DateTime = CDate(CType(gvRow.FindControl("hdnPaydate"), HiddenField).Value)

                        Dim lngJournalID As Long = SaveDataToHeader(hdnPaydate)
                        SaveJournalCheck(lngJournalID)

                        SaveCheckHeader(lngCheckNo)

                        objTransactionScope.Complete()
                    End Using

                    lngCheckNo = lngCheckNo + 1
                End If
            Next

            LoadGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub SaveCheckHeader(ByVal lngCheckNo As Long)
        Try
            Dim objChecks As New Checks()

            objChecks.DomainID = Session("DomainID")
            objChecks.AccountId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
            objChecks.CheckHeaderID = 0
            objChecks.DivisionId = lngDivisionID
            objChecks.UserCntID = Session("UserContactID")
            objChecks.Amount = decAmount
            objChecks.FromDate = CDate(Date.UtcNow.Date & " 12:00:00")
            objChecks.CheckNo = IIf(rbHandCheck.Checked, lngCheckNo, 0)
            objChecks.Memo = "Check against Payroll"

            objChecks.ReferenceID = lngPayrollDetailID
            objChecks.Type = enmReferenceType.PayrollCommission
            objChecks.Mode = 2 'Insert/Edit
            objChecks.IsPrint = rbHandCheck.Checked

            objChecks.ManageCheckHeader()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function SaveJournalCheck(ByVal lngJournalId As Long) As Boolean
        Try
            Dim objJEList As New JournalEntryCollection
            Dim objJE As New JournalEntryNew()

            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = decAmount
            objJE.ChartAcntId = CCommon.ToLong(ddlPaymentFrom.SelectedValue.Split("~")(0))
            objJE.Description = "Refund"
            objJE.CustomerId = lngDivisionID
            objJE.MainDeposit = False
            objJE.MainCheck = False
            objJE.MainCashCredit = False
            objJE.OppitemtCode = 0
            objJE.BizDocItems = "AP"
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 1
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = False
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = decAmount
            objJE.CreditAmt = 0
            objJE.ChartAcntId = lngAPAccountId
            objJE.Description = "Refund"
            objJE.CustomerId = lngDivisionID
            objJE.MainDeposit = False
            objJE.MainCheck = False
            objJE.MainCashCredit = False
            objJE.OppitemtCode = 0
            objJE.BizDocItems = "AP"
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 1
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = False
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try

            Dim objPayrollExpenses As New PayrollExpenses
            objPayrollExpenses.DomainID = Session("DomainId")
            objPayrollExpenses.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            Dim dtPayroll As New DataTable
            Dim dvPayroll As New DataView
            dtPayroll = objPayrollExpenses.GetPayrollHistory()
            dvPayroll = dtPayroll.DefaultView

            If calFromPayDate.SelectedDate IsNot Nothing AndAlso calToPayDate.SelectedDate IsNot Nothing Then
                dvPayroll.RowFilter = " dtPayDate >=  '" & calFromPayDate.SelectedDate & _
                                 "' AND dtPayDate <= '" & calToPayDate.SelectedDate & "'"
                dtPayroll = dvPayroll.ToTable()
                dvPayroll = dtPayroll.DefaultView

            End If

            If ddlEmployee.SelectedValue <> 0 Then
                dvPayroll.RowFilter = " numUserCntID = " & ddlEmployee.SelectedValue
                dtPayroll = dvPayroll.ToTable()
                dvPayroll = dtPayroll.DefaultView

            End If

            If ddlCheckStatus.SelectedValue <> 3 Then
                dvPayroll.RowFilter = " numCheckStatus = " & CCommon.ToInteger(ddlCheckStatus.SelectedValue)
                dtPayroll = dvPayroll.ToTable()
                dvPayroll = dtPayroll.DefaultView

            End If

            gvPayrollList.DataSource = dvPayroll
            gvPayrollList.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        gvPayrollList.Columns(7).Visible = False
        gvPayrollList.Columns(8).Visible = False
        ExportToExcel.DataGridToExcel(gvPayrollList, Response)
        gvPayrollList.Columns(7).Visible = True
        gvPayrollList.Columns(8).Visible = True
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class