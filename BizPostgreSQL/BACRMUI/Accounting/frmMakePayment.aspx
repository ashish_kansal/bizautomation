<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMakePayment.aspx.vb" Inherits=".frmMakePayment" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v7.1, Version=7.1.20071.40, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Receive Payment</title>
      <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>
    <script language="javascript" type ="text/javascript" >
     function Save()
     {
        if(document.form1.ddlDepositTo.value==0)
        {
           alert("Please Select Deposit To");
           document.form1.ddlDepositTo.focus();
           return false;
        }
        
      
       var idRoot='dgReceivePayment'; 
       var idChkSelected='chkSelected';
       var idChk;
       var s=0;
       var DepositTotal=0;
       var idDepositTotal="lblAmount";
       for(var i = 2;i < document.getElementById('dgReceivePayment').rows.length+1; i++)
            {
              if(i<10)
                {
                    idChk ='_ctl0'
                }
              else
               {
                   idChk ='_ctl'
               }
             if (document.getElementById(idRoot+idChk+i+'_'+idChkSelected).checked==true)
             {   
                 s=1;
                 DepositTotal=parseFloat(DepositTotal) + parseFloat(document.getElementById(idRoot+idChk+i+'_'+idDepositTotal).innerText);
             }
          
        }
       // alert(DepositTotal);
       if(s==0)
       {
           alert('You must select an BizDocs for Deposit');
           return false;
       }
       else
       {
            return true;
       }
     }
   function CalculateTotalDeposit()
     {
        var idRoot='dgReceivePayment'; 
       var idChkSelected='chkSelected';
       var idChk;
       var s=0;
       var DepositTotal=0;
       var idDepositTotal="lblAmount";
         for(var i = 2;i < document.getElementById('dgReceivePayment').rows.length+1; i++)
            {
              if(i<10)
                {
                    idChk ='_ctl0'
                }
              else
               {
                   idChk ='_ctl'
               }
             if (document.getElementById(idRoot+idChk+i+'_'+idChkSelected).checked==true)
             {   
                 DepositTotal=parseFloat(DepositTotal) + parseFloat(document.getElementById(idRoot+idChk+i+'_'+idDepositTotal).innerText);
             }
          
        }
        document.getElementById("lblDepositTotalAmount").innerText=formatCurrency(DepositTotal);
        document.getElementById("DepositTotalAmt").innerText=formatCurrency(DepositTotal)
        
     }
     function formatCurrency(num) 
 {
    num = num.toString().replace(/\$|\,/g,'');
    if(isNaN(num))
    num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
    cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+
    num.substring(num.length-(4*i+3));
    return (((sign)?'':'-')  + num + '.' + cents);
}
	function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
    </script>
</head>
<body>
    <form id="form1" runat="server" method ="post" >
       <br />
  <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					
					<td valign="bottom" width ="200px">
						<table class="TabStyle" >
							<tr>
					    		<td >&nbsp;&nbsp;&nbsp;Deposit Payments Received &nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
					<td class="normal1" Width="60px">
			        <asp:Label ID="lblLeads" Runat="server" >Deposit To:</asp:Label>&nbsp;
			      </td> 
			      <td class="normal1" Width ="170px" ><asp:DropDownList ID="ddlDepositTo" runat="server" Width ="150px" cssclass="signup" AutoPostBack ="true" onChange="javascript: CalculateTotalDeposit();"></asp:DropDownList>			      
			      </td>
			      	<td class="normal1" Width="50px">
			        <asp:Label ID="lblBalance" Runat="server" Text =""> </asp:Label>&nbsp;
			      </td> 
			      <td class="normal1" width ="80px" align ="left" >  <asp:Label ID="lblBalanceAmount" Runat="server" Text="" ></asp:Label>&nbsp;			      
			      </td>
			      <td class="normal1" Width="80px">
			        <asp:Label ID="lblDepositTotal" Runat="server" Text ="Deposit Total :"> </asp:Label>&nbsp;
			      </td> 
			      <td class="normal1" width ="150px">  <asp:Label ID="lblDepositTotalAmount" Runat="server" Text="0.00" ></asp:Label>&nbsp;			      
			      </td>
			      <td class="normal1" > <asp:Label ID="lblDepositDate" Runat="server" Text ="Deposit Date"> </asp:Label></td>
						<td><BizCalendar:Calendar ID="calReceivePayment" runat="server" /></td>
			      	<td align="right" valign="middle" class="normal1"  >
						<asp:Button ID="btnDepoistSave" Runat="server" Width="200px" CssClass="button" Text="Deposit these payments & Save"></asp:Button>
						<asp:Button id="btnCancel" Runat="server" CssClass="button" Text="Cancel" Width="50"></asp:Button>
				    </td>
				</tr>
			</TABLE>
			
	<asp:table id="tblReceivePayment" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
						BorderColor="black" GridLines="None" Height="300"  >
		 
		<asp:TableRow  Width="100%">
		<asp:TableCell VerticalAlign="top">
		   	<table cellpadding="0" width="100%" cellspacing="0" border ="0" bgcolor="white" >
             <tr valign="top" width ="100%"><td valign ="top"> 
          <asp:datagrid id="dgReceivePayment" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
												<Columns>
												 <asp:TemplateColumn HeaderText  ="Select">											 
													<ItemTemplate>
												        <asp:CheckBox ID="chkSelected" Runat="server"  onclick ="javascript: CalculateTotalDeposit();"  ></asp:CheckBox>
												        <asp:Label ID="lblAmount" runat ="server"  style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "Amount") %>'> </asp:Label> 
												    </ItemTemplate>
													</asp:TemplateColumn>
												     <asp:BoundColumn Visible="False" DataField="numBizDocsPaymentDetId"></asp:BoundColumn>
												     <asp:BoundColumn Visible="False" DataField="numBizDocsId"></asp:BoundColumn>
												     <asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
                                                     <asp:BoundColumn Visible="False" DataField="numDivisionId"></asp:BoundColumn>												     
												     <asp:BoundColumn DataField="Name"   HeaderText="<font color=white>BizDoc ID</font>"></asp:BoundColumn>
								                     <asp:BoundColumn DataField="Amount" HeaderText="<font color=white>Amount</font>"></asp:BoundColumn>
								                     <asp:BoundColumn DataField="PaymentMethod" HeaderText="<font color=white>Payment Method</font>"></asp:BoundColumn>
                      							     <asp:BoundColumn DataField="CreatedBy" HeaderText="<font color=white>Entered By, on</font>"></asp:BoundColumn>
												     <asp:BoundColumn DataField ="Memo" headertext="<font color=white>Memo</font>"></asp:BoundColumn>
												     <asp:BoundColumn DataField ="Reference" headertext="<font color=white>Reference #</font>"></asp:BoundColumn>
												     <asp:TemplateColumn>
													<ItemTemplate>
												      <asp:Button ID="btnDeleteAction" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
												      <asp:LinkButton ID="lnkDeleteAction" Runat="server" Visible="false">
													  <font color="#730000">*</font></asp:LinkButton>
													</ItemTemplate>												 
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid>
	   </td>	</tr> </table>  			
		</asp:TableCell>
		</asp:TableRow>
		</asp:table> 
		<asp:HiddenField ID="DepositTotalAmt" runat ="server" />
    </form>
</body>
</html>
