''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Accounting
    Partial Public Class frmAccountsPayable
        Inherits BACRMPage
        Dim mobjGeneralLedger As New GeneralLedger
#Region "Variables"

        Dim objCommon As CCommon
#End Region

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then

                    'To Set Permission
                    GetUserRightsForPage(35, 84)

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        Try
                            If PersistTable.ContainsKey(chkHidePastDue.ID) Then
                                chkHidePastDue.Checked = PersistTable(chkHidePastDue.ID)
                            End If

                            If PersistTable.ContainsKey(calFrom.ID) Then
                                calFrom.SelectedDate = PersistTable(calFrom.ID)
                            End If

                            If PersistTable.ContainsKey(calTo.ID) Then
                                calTo.SelectedDate = PersistTable(calTo.ID)
                            End If

                        Catch ex As Exception
                            chkHidePastDue.Checked = False
                        End Try
                    Else
                        If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                        mobjGeneralLedger.DomainID = CCommon.ToLong(Session("DomainID"))
                        mobjGeneralLedger.Year = CInt(Now.Year)

                        calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                    End If

                    bindUserLevelClassTracking()
                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then ibExportExcel.Visible = False
                    LoadAccountPayableGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub bindUserLevelClassTracking()
            Try
                pnlAccountingClass.Visible = False
                Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

                If intDefaultClassType > 0 Then
                    Dim dtClass As DataTable
                    Dim objAdmin As New CAdmin
                    objAdmin.DomainID = Session("DomainID")
                    objAdmin.Mode = 1
                    dtClass = objAdmin.GetClass()

                    If dtClass.Rows.Count > 0 Then
                        ddlUserLevelClass.DataTextField = "ClassName"
                        ddlUserLevelClass.DataValueField = "numChildClassID"
                        ddlUserLevelClass.DataSource = dtClass
                        ddlUserLevelClass.DataBind()

                        ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                        pnlAccountingClass.Visible = True

                        If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then 'intDefaultClassType = 1 => User Level Account Class
                            ddlUserLevelClass.ClearSelection()
                            ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                        End If
                    End If

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                PersistTable.Clear()
                PersistTable.Add(chkHidePastDue.ID, chkHidePastDue.Checked)
                PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
                PersistTable.Add(calTo.ID, calTo.SelectedDate)
                PersistTable.Save()

                LoadAccountPayableGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
            Try
                LoadAccountPayableGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Dim decCurrentDays, decThirtyDays, decSixtyDays, decNinetyDays, decOverNinetyDays, decThirtyDaysOverDue, decSixtyDaysOverDue, decNinetyDaysOverDue, decOverNinetyDaysOverDue, decTotal, decUnAppliedAmount As Decimal

        Private Sub LoadAccountPayableGrid()
            Try
                Dim lobjVendorPayment As New VendorPayment
                Dim dtAPAging As DataTable

                lobjVendorPayment.DomainID = Session("DomainId")
                lobjVendorPayment.CompanyID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
                lobjVendorPayment.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                lobjVendorPayment.dtFromDate = calFrom.SelectedDate
                lobjVendorPayment.dtTodate = CDate(calTo.SelectedDate & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
                lobjVendorPayment.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                dtAPAging = lobjVendorPayment.GetAccountPayableAging

                If dtAPAging.Rows.Count > 0 Then
                    'Dim dr As DataRow = dtAPAging.NewRow
                    'dr(3) = " Total  "
                    'dr(8) = dtAPAging.Compute("Sum(numThirtyDays)", String.Empty)
                    'dr(9) = dtAPAging.Compute("Sum(numSixtyDays)", String.Empty)
                    'dr(10) = dtAPAging.Compute("Sum(numNinetyDays)", String.Empty)
                    'dr(11) = dtAPAging.Compute("Sum(numOverNinetyDays)", String.Empty)
                    'dr(12) = dtAPAging.Compute("Sum(numThirtyDaysOverDue)", String.Empty)
                    'dr(13) = dtAPAging.Compute("Sum(numSixtyDaysOverDue)", String.Empty)
                    'dr(14) = dtAPAging.Compute("Sum(numNinetyDaysOverDue)", String.Empty)
                    'dr(15) = dtAPAging.Compute("Sum(numOverNinetyDaysOverDue)", String.Empty)
                    'dr("numTotal") = dtAPAging.Compute("Sum(numTotal)", String.Empty)
                    'dr("intThirtyDaysCount") = 0
                    'dr("intThirtyDaysOverDueCount") = 0
                    'dr("intSixtyDaysCount") = 0
                    'dr("intSixtyDaysOverDueCount") = 0
                    'dr("intNinetyDaysCount") = 0
                    'dr("intNinetyDaysOverDueCount") = 0
                    'dr("intOverNinetyDaysCount") = 0
                    'dr("intOverNinetyDaysOverDueCount") = 0
                    'dtAPAging.Rows.Add(dr)
                    'lblGrandTotalAmt.Text = Session("Currency") & ReturnMoney(dr("numTotal"))

                    decCurrentDays = dtAPAging.Compute("Sum(numCurrentDays)", String.Empty)
                    'decThirtyDays = dtAPAging.Compute("Sum(numThirtyDays)", String.Empty)
                    'decSixtyDays = dtAPAging.Compute("Sum(numSixtyDays)", String.Empty)
                    'decNinetyDays = dtAPAging.Compute("Sum(numNinetyDays)", String.Empty)
                    'decOverNinetyDays = dtAPAging.Compute("Sum(numOverNinetyDays)", String.Empty)
                    decThirtyDaysOverDue = dtAPAging.Compute("Sum(numThirtyDaysOverDue)", String.Empty)
                    decSixtyDaysOverDue = dtAPAging.Compute("Sum(numSixtyDaysOverDue)", String.Empty)
                    decNinetyDaysOverDue = dtAPAging.Compute("Sum(numNinetyDaysOverDue)", String.Empty)
                    decOverNinetyDaysOverDue = dtAPAging.Compute("Sum(numOverNinetyDaysOverDue)", String.Empty)
                    decTotal = dtAPAging.Compute("Sum(numTotal)", String.Empty)
                    decUnAppliedAmount = dtAPAging.Compute("Sum(monUnAppliedAmount)", String.Empty)

                    lblGrandTotalAmt.Text = Session("Currency") & ReturnMoney(decTotal)
                Else : lblGrandTotalAmt.Text = Session("Currency") & ReturnMoney(0)
                End If
                dgAP.DataSource = dtAPAging
                dgAP.DataBind()

                'dgAP.Columns(8).Visible = Not chkHidePastDue.Checked
                'dgAP.Columns(9).Visible = Not chkHidePastDue.Checked
                'dgAP.Columns(10).Visible = Not chkHidePastDue.Checked
                'dgAP.Columns(11).Visible = Not chkHidePastDue.Checked

                dgAP.Columns(5).Visible = Not chkHidePastDue.Checked
                dgAP.Columns(6).Visible = Not chkHidePastDue.Checked
                dgAP.Columns(7).Visible = Not chkHidePastDue.Checked
                dgAP.Columns(8).Visible = Not chkHidePastDue.Checked
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgAP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAP.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplCompany As HyperLink
                    Dim lblCompany As Label
                    hplCompany = e.Item.FindControl("hplCompany")
                    hplCompany.NavigateUrl = "#"
                    lblCompany = e.Item.FindControl("lblCompany")
                    If e.Item.Cells(0).Text <> "&nbsp;" Then
                        hplCompany.Attributes.Add("onclick", "return OpenCompany('" & e.Item.Cells(0).Text & "','" & e.Item.Cells(2).Text & "','" & Session("EnableIntMedPage") & "')")
                        lblCompany.Visible = False
                        lblCompany.Text = ""
                    Else
                        lblCompany.Visible = True
                        hplCompany.Visible = False
                        hplCompany.Text = ""
                    End If

                ElseIf e.Item.ItemType = ListItemType.Footer Then
                    CType(e.Item.FindControl("lblTotalCurrentDays"), Label).Text = ReturnMoney(decCurrentDays)
                    ' CType(e.Item.FindControl("lblTotalThirtyDays"), Label).Text = ReturnMoney(decThirtyDays)
                    ' CType(e.Item.FindControl("lblTotalSixtyDays"), Label).Text = ReturnMoney(decSixtyDays)
                    ' CType(e.Item.FindControl("lblTotalNinetyDays"), Label).Text = ReturnMoney(decNinetyDays)
                    ' CType(e.Item.FindControl("lblTotalOverNinetyDays"), Label).Text = ReturnMoney(decOverNinetyDays)
                    CType(e.Item.FindControl("lblTotalThirtyDaysOverDue"), Label).Text = ReturnMoney(decThirtyDaysOverDue)
                    CType(e.Item.FindControl("lblTotalSixtyDaysOverDue"), Label).Text = ReturnMoney(decSixtyDaysOverDue)
                    CType(e.Item.FindControl("lblTotalNinetyDaysOverDue"), Label).Text = ReturnMoney(decNinetyDaysOverDue)
                    CType(e.Item.FindControl("lblTotalOverNinetyDaysOverDue"), Label).Text = ReturnMoney(decOverNinetyDaysOverDue)
                    CType(e.Item.FindControl("lblTotalUnAppliedAmount"), Label).Text = ReturnMoney(decUnAppliedAmount)
                    CType(e.Item.FindControl("lblTotal"), Label).Text = ReturnMoney(decTotal)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ibExportExcel_Click(sender As Object, e As System.EventArgs) Handles ibExportExcel.Click
            'Try
            ExportToExcel.DataGridToExcel(dgAP, Response)
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment;filename=FileName" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".xls")
            'Response.Charset = ""
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.ContentType = "application/vnd.xls"
            'Dim stringWrite As New System.IO.StringWriter
            'Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'ClearControls(dgAP)
            'dgAP.GridLines = GridLines.Both
            'dgAP.HeaderStyle.Font.Bold = True
            'dgAP.RenderControl(htmlWrite)
            'Response.Write(stringWrite.ToString())
            'Response.End()
            'Catch ex As Exception
            '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            '    DisplayError(ex.Message)
            'End Try
        End Sub

        Sub ClearControls(ByVal control As Control)
            Try
                Dim i As Integer
                For i = control.Controls.Count - 1 To 0 Step -1
                    ClearControls(control.Controls(i))
                Next i

                If TypeOf control Is System.Web.UI.WebControls.Image Then control.Parent.Controls.Remove(control)
                If (Not TypeOf control Is TableCell) Then
                    If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                        Dim literal As New LiteralControl
                        control.Parent.Controls.Add(literal)
                        Try
                            literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                        Catch
                        End Try
                        control.Parent.Controls.Remove(control)
                    Else
                        If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                            Dim literal As New LiteralControl
                            control.Parent.Controls.Add(literal)
                            literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                            literal.Text = Replace(literal.Text, "white", "black")
                            control.Parent.Controls.Remove(control)
                        End If
                    End If
                End If
                Return
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                'If radCmbCompany.Value <> "" Then
                LoadAccountPayableGrid()
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try

        End Sub

        Private Sub chkHidePastDue_CheckedChanged(sender As Object, e As EventArgs) Handles chkHidePastDue.CheckedChanged
            Try
                'If radCmbCompany.Value <> "" Then
                LoadAccountPayableGrid()
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
            Try
                PersistTable.Clear()
                PersistTable.Add(chkHidePastDue.ID, chkHidePastDue.Checked)
                PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
                PersistTable.Add(calTo.ID, calTo.SelectedDate)
                PersistTable.Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Private Sub btnMarkCommissionAsPaid_Click(sender As Object, e As EventArgs) Handles btnMarkCommissionAsPaid.Click
            Try
                If Not String.IsNullOrEmpty(calFrom.SelectedDate) AndAlso Not String.IsNullOrEmpty(calTo.SelectedDate) Then
                    Dim objPayrollExpense As New PayrollExpenses
                    objPayrollExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPayrollExpense.MarkCommissionAsPaidForDateRange(calFrom.SelectedDate, CDate(calTo.SelectedDate & " 23:59:59"))

                    LoadAccountPayableGrid()
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectDate", "alert('Select From and To Date');", True)
                End If
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class

End Namespace
