﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmpPayrollExpenseSettings.aspx.vb"
    Inherits=".frmEmpPayrollExpenseSettings" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
    <script type="text/javascript" language="javascript">
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function PriceTable(a) {
            window.open("../Items/frmPricingTable.aspx?RuleID=" + a, '', 'toolbar=no,titlebar=no,top=200,width=400,height=600,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenItem(a) {
            window.location.href = '../Items/frmKitDetails.aspx?ItemCode=' + a;
            return false;
        }
    </script>
    <style type="text/css">
        .style9
        {
            width: 54px;
            text-align: right;
        }
        .style10
        {
            width: 104px;
            text-align: right;
        }
        .style13
        {
            text-align: right;
            width: 68px;
        }
        .style16
        {
            text-align: center;
            width: 2%;
        }
        .style17
        {
            width: 109px;
            height: 26px;
        }
        .style18
        {
            width: 83px;
            height: 26px;
        }
        .style19
        {
            width: 92px;
            height: 26px;
        }
        .style20
        {
            width: 104px;
            text-align: right;
            height: 26px;
        }
        .style21
        {
            width: 54px;
            text-align: right;
            height: 26px;
        }
        .style22
        {
            text-align: right;
            width: 68px;
            height: 26px;
        }
        .style23
        {
            text-align: center;
            width: 64px;
            height: 26px;
        }
        .style25
        {
            width: 115px;
            text-align: center;
        }
        .style26
        {
            width: 115px;
            text-align: center;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Employee Payroll Settings
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table class="dg" cellspacing="2" cellpadding="2" rules="all" border="1" id="dgPayrollLiability"
        style="width: 60%; border-collapse: collapse;">
        <tbody>
            <tr class="hs">
                <td>
                    Employee Name
                </td>
                <td style="width: 10%">
                    Department
                </td>
                <td style="width: 10%">
                    Date Hired
                </td>
                <td style="width: 15%">
                    Employee Rating
                </td>
                <td style="width: 10%">
                    Hourly Rate
                </td>
                <td style="width: 10%">
                    Salary
                </td>
                <td style="width: 10%">
                    Over Time
                </td>
                <td class="style16">
                    Configure
                </td>
            </tr>
            <tr class="is">
                <td class="style6">
                    <a id="hplUsername" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('1')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"
                        target="_blank">Carl Zaldivar</a>
                </td>
                <td class="style8">
                    HR
                </td>
                <td class="style3">
                    12/10/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    100
                </td>
                <td class="style9">
                    21000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="ais">
                <td class="style6">
                    <a id="hplUsername0" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('84')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;
                        background-color: rgb(242, 242, 242);" target="_blank">Balu -</a>
                </td>
                <td class="style8">
                    Marketing
                </td>
                <td class="style3">
                    12/10/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    120
                </td>
                <td class="style9">
                    13000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="is">
                <td class="style6">
                    <a id="hplUsername1" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('161')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"
                        target="_blank">Charles Test</a>
                </td>
                <td class="style8">
                    HR
                </td>
                <td class="style3">
                    1/11/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    100
                </td>
                <td class="style9">
                    11000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="ais">
                <td class="style6">
                    <a id="hplUsername2" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('177')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;
                        background-color: rgb(242, 242, 242);" target="_blank">Ray Hivoral</a>
                </td>
                <td class="style8">
                    HR
                </td>
                <td class="style3">
                    13/1/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    110
                </td>
                <td class="style9">
                    10000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="is">
                <td class="style6">
                    <a id="hplUsername3" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('183')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;
                        background-color: rgb(255, 255, 255);" target="_blank">B2B Marketing</a>
                </td>
                <td class="style8">
                    HR
                </td>
                <td class="style3">
                    12/08/2010
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    200
                </td>
                <td class="style9">
                    9000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="ais">
                <td class="style6">
                    <a id="hplUsername4" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('196')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;
                        background-color: rgb(242, 242, 242);" target="_blank">Dave Speare</a>
                </td>
                <td class="style8">
                    Sales
                </td>
                <td class="style3">
                    12/10/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    250
                </td>
                <td class="style9">
                    10000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="is">
                <td class="style6">
                    <a id="hplUsername5" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('310')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;
                        background-color: rgb(255, 255, 255);" target="_blank">Siva Prakasam</a>
                </td>
                <td class="style8">
                    Sales
                </td>
                <td class="style3">
                    12/10/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    110
                </td>
                <td class="style9">
                    12000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="ais">
                <td class="style17">
                    <a id="hplUsername6" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('311')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"
                        target="_blank">Pratik Vasani</a>
                </td>
                <td class="style18">
                    HR
                </td>
                <td class="style19">
                    12/10/1999
                </td>
                <td class="style26">
                    5
                </td>
                <td class="style20">
                    110
                </td>
                <td class="style21">
                    12000
                </td>
                <td class="style22">
                    12.00
                </td>
                <td class="style23">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="is">
                <td class="style6">
                    <a id="hplUsername7" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('312')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"
                        target="_blank">Sanjivani S. Pant</a>
                </td>
                <td class="style8">
                    Marketing
                </td>
                <td class="style3">
                    12/10/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    112
                </td>
                <td class="style9">
                    12000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
            <tr class="ais">
                <td class="style6">
                    <a id="hplUsername8" href="http://bizauto2/bacrmui/Accounting/frmPayrollLiabilityList.aspx#"
                        onclick="return OpenUserDetails('315')" style="color: rgb(98, 109, 144); text-decoration: underline;
                        font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal;
                        font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22px;
                        orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal;
                        widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"
                        target="_blank">Fabiola Zaldivar</a>
                </td>
                <td class="style8">
                    Marketing
                </td>
                <td class="style3">
                    12/10/1999
                </td>
                <td class="style25">
                    5
                </td>
                <td class="style10">
                    120
                </td>
                <td class="style9">
                    12000
                </td>
                <td class="style13">
                    12.00
                </td>
                <td class="style16">
                    <img alt="Configure" src="../images/configure.png" onclick='return OpenPayrollSettings();' />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
