Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmMakePayment
    Inherits System.Web.UI.Page
#Region "Variables"

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            calReceivePayment.SelectedDate = Now()
            LoadReceivePaymentGrid()
            FillDepositTocombo()
        End If
        btnDepoistSave.Attributes.Add("onclick", "return Save()")

    End Sub
    Private Sub LoadReceivePaymentGrid()
        Dim lobjReceivePayment As New ReceivePayment
        Dim dtReceivePayment As New DataTable
        lobjReceivePayment.DomainId = Session("DomainId")
        dtReceivePayment = lobjReceivePayment.GetReceivePaymentDetails
        dgReceivePayment.DataSource = dtReceivePayment
        dgReceivePayment.DataBind()
    End Sub
    Sub FillDepositTocombo()
        Dim lobjCashCreditCard As New Checks
        lobjCashCreditCard.DomainId = Session("DomainId")
        lobjCashCreditCard.AccountType = 813
        ddlDepositTo.DataSource = lobjCashCreditCard.GetCheckChartAcntDetails()
        ddlDepositTo.DataTextField = "vcCategoryName"
        ddlDepositTo.DataValueField = "numChartOfAcntID"
        ddlDepositTo.DataBind()
        ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
    End Sub

    Private Sub btnDepoistSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDepoistSave.Click
        Dim JournalId As Integer
        JournalId = SaveDataToHeader()
        SaveDataToGeneralJournalDetails(JournalId)
        LoadReceivePaymentGrid()
    End Sub

    Private Function SaveDataToHeader() As Integer
        Dim lntJournalId As Integer
        Dim lobjJournalEntry As New JournalEntry
        With lobjJournalEntry
            .Entry_Date = calReceivePayment.SelectedDate
            .numAmount = lblDepositTotalAmount.Text
            .DomainId = Session("DomainId")
            .RecurringId = 0
            .JournalId = 0
            lntJournalId = .SaveDataToJournalEntryHeader()
            Return lntJournalId
        End With
    End Function

    Private Sub SaveDataToGeneralJournalDetails(ByVal lintJournalId As Integer)
        Dim dtgriditem As DataGridItem
        Dim dtrow As DataRow
        Dim dtrow1 As DataRow
        Dim i As Integer
        Dim lstr As String
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim lobjJournalEntry As New JournalEntry

        dt.Columns.Add("TransactionId") '' Primary key
        dt.Columns.Add("numJournalId")
        dt.Columns.Add("numDebitAmt")
        dt.Columns.Add("numCreditAmt")
        dt.Columns.Add("numChartAcntId")
        dt.Columns.Add("varDescription")
        dt.Columns.Add("numCustomerId")
        dt.Columns.Add("numDomainId")
        dt.Columns.Add("numBizDocsPaymentDetId")

        For i = 0 To dgReceivePayment.Items.Count - 1
            dtrow = dt.NewRow
            dtrow1 = dt.NewRow
            dtgriditem = dgReceivePayment.Items(i)
            If CType(dtgriditem.FindControl("chkSelected"), CheckBox).Checked = True Then

                dtrow.Item("TransactionId") = 0
                dtrow.Item("numJournalId") = lintJournalId
                dtrow.Item("numDebitAmt") = dtgriditem.Cells(6).Text
                dtrow.Item("numCreditAmt") = 0
                dtrow.Item("numChartAcntId") = ddlDepositTo.SelectedItem.Value
                dtrow.Item("varDescription") = "Receive Payment"
                dtrow.Item("numCustomerId") = dtgriditem.Cells(4).Text
                dtrow.Item("numDomainId") = Session("DomainId")
                dtrow.Item("numBizDocsPaymentDetId") = dtgriditem.Cells(1).Text
                dt.Rows.Add(dtrow)


                '' For Account Receivable to be Credited
                dtrow1.Item("TransactionId") = 0
                dtrow1.Item("numJournalId") = lintJournalId
                dtrow1.Item("numDebitAmt") = 0
                dtrow1.Item("numCreditAmt") = dtgriditem.Cells(6).Text
                dtrow1.Item("numChartAcntId") = 2
                dtrow1.Item("varDescription") = "Receive Payment"
                dtrow1.Item("numCustomerId") = dtgriditem.Cells(4).Text
                dtrow1.Item("numDomainId") = Session("DomainId")
                dt.Rows.Add(dtrow1)

            End If
        Next
        ds.Tables.Add(dt)
        lstr = ds.GetXml()
        lobjJournalEntry.JournalId = lintJournalId
        lobjJournalEntry.Mode = 0
        lobjJournalEntry.JournalDetails = lstr
        lobjJournalEntry.SaveDataToJournalDetails()
    End Sub

    Private Sub ddlDepositTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepositTo.SelectedIndexChanged
        Dim lobjCashCreditCard As New CashCreditCard
        Dim lstrColor As String
        If ddlDepositTo.SelectedItem.Value <> 0 Then
            lobjCashCreditCard.AccountId = ddlDepositTo.SelectedItem.Value
            lobjCashCreditCard.DomainId = Session("DomainId")
            lblBalance.Text = "Balance: "
            lstrColor = "<font color=red>" & ReturnMoney(lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()) & "</font>"
            lblBalanceAmount.Text = IIf(lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit() < 0, lstrColor, ReturnMoney(lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()))
            lblDepositTotalAmount.Text = DepositTotalAmt.Value
        Else
            lblBalanceAmount.Text = ""
            lblBalance.Text = ""
            lblDepositTotalAmount.Text = DepositTotalAmt.Value
        End If
    End Sub

    Function ReturnMoney(ByVal Money)
        If Not IsDBNull(Money) Then
            Return String.Format("{0:#,###.00}", Money)
        End If
    End Function

    Private Sub dgReceivePayment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReceivePayment.ItemCommand
        Dim lobjReceivePayment As New ReceivePayment

        If e.CommandName = "Delete" Then
            Try
                lobjReceivePayment.BizDocsPaymentDetId = e.Item.Cells(1).Text
                lobjReceivePayment.BizDocsId = e.Item.Cells(2).Text
                lobjReceivePayment.OppId = e.Item.Cells(3).Text
                lobjReceivePayment.Amount = Replace(e.Item.Cells(6).Text, ",", "")
                lobjReceivePayment.DeleteReceivePaymentDetails()
                LoadReceivePaymentGrid()
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

    Private Sub dgReceivePayment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReceivePayment.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim btnDelete As Button
            Dim lnkDelete As LinkButton
            lnkDelete = e.Item.FindControl("lnkDeleteAction")
            btnDelete = e.Item.FindControl("btnDeleteAction")
            ''If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
            ''    btnDelete.Visible = False
            ''    lnkDelete.Visible = True
            ''    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
            ''Else
            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            ''End If
        End If
    End Sub
End Class