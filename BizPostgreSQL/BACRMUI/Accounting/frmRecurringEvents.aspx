<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringEvents.aspx.vb"
    Inherits=".frmRecurringEvents" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <title>Recurring</title>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .hs {
            border: 1px solid #e4e4e4;
            border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            text-align: center;
            font-weight: bold;
        }
    </style>
    <script language="Javascript" type="text/javascript">
        $(document).ready(function () {
            // Handler for .ready() called.

            $("#chkEnableBizDocBreakdown").click(function () {

                if ($("#chkEnableBizDocBreakdown").is(':checked')) {
                    $("#CalStartDate_txtDate").attr("disabled", "disabled");
                    $("#rdlNoEndDate").attr("disabled", "disabled");
                    $("#rdlStopAfter").attr("disabled", "disabled");
                    $("#txtNoTransactions").attr("disabled", "disabled");
                    $("#rdlNoTransaction").attr("disabled", "disabled");
                    $("#rdlEndDate").attr("disabled", "disabled");
                    $("#CalEndDate_txtDate").attr("disabled", "disabled");
                }
                else {
                    $("#CalStartDate_txtDate").removeAttr("disabled");
                    $("#rdlNoEndDate").removeAttr("disabled");
                    $("#rdlStopAfter").removeAttr("disabled");
                    $("#txtNoTransactions").removeAttr("disabled");
                    $("#rdlNoTransaction").removeAttr("disabled");
                    $("#rdlEndDate").removeAttr("disabled");
                    $("#CalEndDate_txtDate").removeAttr("disabled");
                }
            });
        });

        function Save() {
            var a = null;
            var f = document.forms[0];
            var e = f.elements["rdlRecurringTemplate"];
            a = $("#<%=rdlRecurringTemplate.ClientID%>").find(":checked").val();
            //for (var i = 0; i < e.length; i++) {
            //    if (e[i].checked) {
            //        a = e[i].value;
            //        break;
            //    }
            //}

            if (document.getElementById('txtTemplateName').value == "") {
                alert("Enter a Template Name");
                document.getElementById('txtTemplateName').focus();
                return false;
            }


            if (a == 0) {
                if (document.getElementById('txtDays').value == "" || document.getElementById('txtDays').value == 0 || document.getElementById('txtDays').value > 28) {
                    alert("Enter a valid dialy schedule");
                    document.getElementById('txtDays').focus();
                    return false;
                }
            }
            if (a == 1) {
                if (document.getElementById('rdbMonthlyDays').checked == true) {
                    if (document.getElementById('txtMonthlyDays').value == "" || document.getElementById('txtMonthlyDays').value == 0 || document.getElementById('txtMonthlyDays').value > 28) {
                        alert("Enter a valid monthly schedule");
                        document.getElementById('txtMonthlyDays').focus();
                        return false;
                    }
                }
                if (document.getElementById('rdbMonthlyWeekDays').checked == true) {
                    if (document.getElementById('txtMonthlyWeekDays').value == "" || document.getElementById('txtMonthlyWeekDays').value == 0 || document.getElementById('txtMonthlyWeekDays').value > 28) {
                        alert("Enter a valid monthly schedule");
                        document.getElementById('txtMonthlyWeekDays').focus();
                        return false;
                    }
                }
            }

            if ($("#chkEnableBizDocBreakdown").is(':checked') == false) {

                if (document.getElementById('CalStartDate_txtDate').value == "") {
                    alert("Enter Start Date");
                    document.getElementById('CalStartDate_txtDate').focus();
                    return false;
                }

                if (document.getElementById('rdlStopAfter').checked == true && document.getElementById('rdlEndDate').checked == true) {
                    if (document.getElementById('CalEndDate_txtDate').value == "") {
                        alert("Enter End Date");
                        document.getElementById('CalEndDate_txtDate').focus();
                        return false;
                    }
                    //            if(document.getElementById('CalStartDate_txtDate').value<document.getElementById('CalEndDate_txtDate').value)
                    //            {
                    //                   alert("Start Date is less than End Date");
                    //                   document.getElementById('CalStartDate_txtDate.focus();
                    //                   return false; 
                    //            }
                    if (document.getElementById('CalStartDate_txtDate').value > document.getElementById('CalEndDate_txtDate').value) {
                        alert("End Date is less than Start Date");
                        document.getElementById('CalEndDate_txtDate').focus();
                        return false;
                    }
                }
                if (document.getElementById('rdlStopAfter').checked == true && document.getElementById('rdlNoTransaction').checked == true) {
                    if (document.getElementById('txtNoTransactions').value == "" || document.getElementById('txtNoTransactions').value == 0) {
                        alert("Enter Number of Transactions");
                        document.getElementById('txtNoTransactions').focus();
                        return false;
                    }
                }
            }


            if (Number($("#txtNoOfBizDocs").val()) > 0) {
                var Total = 0;
                $("#dgBillingBreakup").find(":input").each(function () {
                    Total += Number($(this).val());
                });
                if (Math.round(Total) != 100) {
                    alert('Sum total of all rows must equal to 100%');
                    return false;
                }
            }

        }

        function HideTempPanel() {
            //alert("Siva");
            var a = null;
            var f = document.forms[0];
            var e = f.elements["rdlRecurringTemplate"];
            a=$("#<%=rdlRecurringTemplate.ClientID%>").find(":checked").val();
            //for (var i = 0; i < e.length; i++) {
            //    if (e[i].checked) {
            //        a = e[i].value;
            //        break;
            //    }
            //}
            if (a == 0) {
                document.getElementById("pnlDaily").style.visibility = '';
                document.getElementById("pnlMonthly").style.visibility = 'hidden';
                document.getElementById("pnlYearly").style.visibility = 'hidden';
                //        document.getElementById("txtDays").value=1;
            }
            if (a == 1) {
                document.getElementById("pnlDaily").style.visibility = 'hidden';
                document.getElementById("pnlMonthly").style.visibility = ''
                document.getElementById("pnlYearly").style.visibility = 'hidden';
                //        document.getElementById("txtMonthlyDays").value=1;
                //        document.getElementById("txtMonthlyWeekDays").value=1;
            }
            if (a == 2) {
                document.getElementById("pnlDaily").style.visibility = 'hidden';
                document.getElementById("pnlMonthly").style.visibility = 'hidden';
                document.getElementById("pnlYearly").style.visibility = '';
            }

        }

        function HidePanel() {
            alert('SP');
            document.getElementById("pnlMonthly").style.visibility = 'hidden';
            document.getElementById("pnlYearly").style.visibility = 'hidden';
        }

        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <asp:Button ID="btnSave" runat="server" CssClass="button btn btn-primary" Text="Save & Close"></asp:Button>
        &nbsp;
                        <asp:Button ID="btnBack" runat="server" CssClass="button btn btn-primary" Text="Back"></asp:Button>
        &nbsp;
         <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Recurring Template
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
            Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="384px">
            <asp:TableRow ID="TableRow1" VerticalAlign="Top" runat="server">
                <asp:TableCell ID="TableCell1" runat="server">
                    <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="0px" runat="server"
                        Width="100%" BorderColor="Black">
                        <asp:TableRow ID="TableRow2" runat="server">
                            <asp:TableCell ID="TableCell2" runat="server">
                                <br />
                                <table id="tblDetails" runat="server" width="100%" border="0" class="table table-responsive">
                                    <tr id="Tr1" runat="server" align="left">
                                        <td></td>
                                        <td id="Td1" class="normal1" align="left" runat="server" width="120px">Template Name<font color="#ff0000">*</font>
                                        </td>
                                        <td id="Td2" class="normal1" runat="server" align="left">
                                            <asp:TextBox ID="txtTemplateName" CssClass="signup form-control" runat="server"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr2" runat="server">
                                        <td></td>
                                        <td id="Td3" class="normal1" colspan="2" align="left" runat="server" width="120px">
                                            <b>Select Interval</b>
                                        </td>
                                    </tr>
                                    <tr id="Tr3" runat="server">
                                        <td></td>
                                        <td id="Td4" class="normal1" align="left" runat="server" width="80px">
                                            <asp:Panel ID="pnlRaido" runat="server">
                                                <asp:RadioButtonList ID="rdlRecurringTemplate" runat="server" onclick="HideTempPanel();"
                                                    RepeatDirection="vertical">
                                                    <asp:ListItem Value="0" Selected="true" Text="Daily"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Monthly"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Yearly"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:Panel>
                                        </td>
                                        <td class="normal1" id="td5" runat="server" align="left">
                                            <asp:Panel ID="pnlDaily" runat="server">
                                                <div class="form-inline">
                                                    Every
                                            <asp:TextBox ID="txtDays" runat="server" CssClass="form-control" MaxLength="2"
                                                Text="1"></asp:TextBox>
                                                    Day(s)
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlMonthly" runat="server">
                                                <div class="form-inline">
                                                    <asp:RadioButton ID="rdbMonthlyDays" runat="server" GroupName="GrpMonthly" Checked="true" />
                                                    <asp:DropDownList ID="ddlMonthlyDays" runat="server" CssClass="signup form-control">
                                                        <asp:ListItem Value="1" Text="1st"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="2nd"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="3rd"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="4th"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="5th"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="6th"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="7th"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="8th"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="9th"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="10th"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="11th"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="12th"></asp:ListItem>
                                                        <asp:ListItem Value="13" Text="13th"></asp:ListItem>
                                                        <asp:ListItem Value="14" Text="14th"></asp:ListItem>
                                                        <asp:ListItem Value="15" Text="15th"></asp:ListItem>
                                                        <asp:ListItem Value="16" Text="16th"></asp:ListItem>
                                                        <asp:ListItem Value="17" Text="17th"></asp:ListItem>
                                                        <asp:ListItem Value="18" Text="18th"></asp:ListItem>
                                                        <asp:ListItem Value="19" Text="19th"></asp:ListItem>
                                                        <asp:ListItem Value="20" Text="20th"></asp:ListItem>
                                                        <asp:ListItem Value="21" Text="21th"></asp:ListItem>
                                                        <asp:ListItem Value="22" Text="22th"></asp:ListItem>
                                                        <asp:ListItem Value="23" Text="23th"></asp:ListItem>
                                                        <asp:ListItem Value="24" Text="24th"></asp:ListItem>
                                                        <asp:ListItem Value="25" Text="25th"></asp:ListItem>
                                                        <asp:ListItem Value="26" Text="26th"></asp:ListItem>
                                                        <asp:ListItem Value="27" Text="27th"></asp:ListItem>
                                                        <asp:ListItem Value="28" Text="28th"></asp:ListItem>
                                                        <%-- <asp:ListItem Value ="29" text="Last"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    of every
                                            <asp:TextBox ID="txtMonthlyDays" runat="server" CssClass="form-control" MaxLength="2"
                                                Text="1"></asp:TextBox>
                                                    month(s)
                                            <br />
                                                    <br />
                                                    <asp:RadioButton ID="rdbMonthlyWeekDays" runat="server" GroupName="GrpMonthly" />The
                                            <asp:DropDownList ID="ddlMonthlyWeekDays" runat="server" CssClass="signup form-control">
                                                <asp:ListItem Value="1" Text="First"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Second"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Third"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Fourth"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="Last"></asp:ListItem>
                                            </asp:DropDownList>
                                                    <%--Modified by chintan as per new function fn_GetNthWeekdayOfMonth--%>
                                                    <asp:DropDownList ID="ddlMonthlyWeek" runat="server" CssClass="signup form-control">
                                                        <asp:ListItem Value="7" Text="Sunday"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Monday"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Tuesday"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Wednesday"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Thursday"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="Friday"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Saturday"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    of every
                                            <asp:TextBox ID="txtMonthlyWeekDays" MaxLength="2" runat="server" Width="30px" Height="15px"
                                                Text="1"></asp:TextBox>
                                                    month(s)
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlYearly" runat="server">
                                                <div class="form-inline">
                                                    Every
                                            <asp:DropDownList ID="ddlYearlyMonth" runat="server" CssClass="signup form-control">
                                                <asp:ListItem Value="1" Text="January"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="February"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="March"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="April"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="June"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="July"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="August"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="September"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="October"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="November"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="December"></asp:ListItem>
                                            </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlYearlyDays" runat="server" CssClass="signup form-control">
                                                        <asp:ListItem Value="1" Text="1st"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="2nd"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="3rd"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="4th"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="5th"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="6th"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="7th"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="8th"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="9th"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="10th"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="11th"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="12th"></asp:ListItem>
                                                        <asp:ListItem Value="13" Text="13th"></asp:ListItem>
                                                        <asp:ListItem Value="14" Text="14th"></asp:ListItem>
                                                        <asp:ListItem Value="15" Text="15th"></asp:ListItem>
                                                        <asp:ListItem Value="16" Text="16th"></asp:ListItem>
                                                        <asp:ListItem Value="17" Text="17th"></asp:ListItem>
                                                        <asp:ListItem Value="18" Text="18th"></asp:ListItem>
                                                        <asp:ListItem Value="19" Text="19th"></asp:ListItem>
                                                        <asp:ListItem Value="20" Text="20th"></asp:ListItem>
                                                        <asp:ListItem Value="21" Text="21th"></asp:ListItem>
                                                        <asp:ListItem Value="22" Text="22th"></asp:ListItem>
                                                        <asp:ListItem Value="23" Text="23th"></asp:ListItem>
                                                        <asp:ListItem Value="24" Text="24th"></asp:ListItem>
                                                        <asp:ListItem Value="25" Text="25th"></asp:ListItem>
                                                        <asp:ListItem Value="26" Text="26th"></asp:ListItem>
                                                        <asp:ListItem Value="27" Text="27th"></asp:ListItem>
                                                        <asp:ListItem Value="28" Text="28th"></asp:ListItem>
                                                        <%--<asp:ListItem Value ="29" text="Last"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr id="Tr4" runat="server">
                                        <td></td>
                                        <td id="Td6" colspan="2" class="normal1" align="left" runat="server">
                                            <b>Select Date Range</b>
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td></td>
                                        <td align="left">Start on after<font color="#ff0000">*</font>
                                        </td>
                                        <td align="left">
                                            <BizCalendar:Calendar ID="CalStartDate" runat="server" ClientIDMode="AutoID" />
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td></td>
                                        <td align="left">
                                            <asp:RadioButton GroupName="EndDate" ID="rdlNoEndDate" runat="server" Checked="true"
                                                CssClass="normal1" Text="No End Date"></asp:RadioButton>
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td></td>
                                        <td align="left">
                                            <asp:RadioButton GroupName="EndDate" ID="rdlStopAfter" runat="server" CssClass="normal1"></asp:RadioButton>
                                            Stop after
                                        </td>
                                        <td>
                                            <table id="tblNoEndDate">
                                                <tr>
                                                    <td align="left">
                                                        <asp:RadioButton GroupName="NoEndDate" ID="rdlEndDate" runat="server" Checked="true"
                                                            CssClass="normal1" Text=""></asp:RadioButton>
                                                    </td>
                                                    <td align="left">
                                                        <BizCalendar:Calendar ID="CalEndDate" runat="server" ClientIDMode="AutoID" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td align="left">
                                            <asp:RadioButton GroupName="NoEndDate" ID="rdlNoTransaction" runat="server" CssClass="normal1"
                                                Text="No of Transactions"></asp:RadioButton>
                                            <asp:TextBox ID="txtNoTransactions" Width="30px" CssClass="signup" runat="server"
                                                MaxLength="2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td id="Td7" colspan="2" class="normal1" align="left" runat="server" width="110px">
                                            <asp:CheckBox Text="<b>Enable Billing BreakDown</b>" runat="server" ID="chkEnableBizDocBreakdown" />
                                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Lets you break total order amount into number of partial bizdocs required for your billing cycle(i.e Yearly magazine subscription can be devided into 12 partial bizdocs)" />
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td></td>
                                        <td align="left" colspan="2">
                                            <div class="form-inline">
                                                Break billing schedule into
                                        <asp:TextBox runat="server" ID="txtNoOfBizDocs" CssClass="signup form-control" AUTOCOMPLETE="OFF"
                                            AutoPostBack="true" onkeypress="CheckNumber(2, event);" />
                                                BizDocs,
                                        <asp:CheckBox ID="chkBillingTerms" runat="server" Text="Billing Terms" CssClass="signup" />
                                                &nbsp;Net
                                        <asp:DropDownList ID="ddlNetDays" runat="server" CssClass="signup form-control">
                                        </asp:DropDownList>
                                                Days
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td></td>
                                        <td align="left" colspan="2">
                                            <asp:DataGrid ID="dgBillingBreakup" runat="server" CssClass="dg table table-responsive table-bordered" AutoGenerateColumns="False">
                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                <ItemStyle CssClass="is"></ItemStyle>
                                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ID" HeaderText="BizDoc Issue" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Percentage BreakDown<br><small>(Sum total of rows must = 100%)</small>">
                                                        <ItemTemplate>
                                                            <div class="form-inline">
                                                                <asp:TextBox ID="txtPercentage" runat="server" CssClass="signup form-control" AUTOCOMPLETE="OFF"
                                                                    onkeypress="CheckNumber(1, event);" Text='<%# Eval("Percentage") %>' />%
                                                            </div>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
