'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmVendorPayment

    '''<summary>
    '''divError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divError As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''btnAllowCheckNO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAllowCheckNO As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''ddlPaymentFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPaymentFrom As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBalanceAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalanceAmount As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''bizPager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents bizPager As Global.Wuqi.Webdiyer.AspNetPager

    '''<summary>
    '''radCmbCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radCmbCompany As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''pnlAccountingClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAccountingClass As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlUserLevelClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlUserLevelClass As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''pnlCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCurrency As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCurrency As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblForeignCurr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblForeignCurr As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtExchangeRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtExchangeRate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblBaseCurr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBaseCurr As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trCredits control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trCredits As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblTotalCreditsLeft control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalCreditsLeft As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''gvCredits control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvCredits As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''dgBills control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgBills As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''btnDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''ddlPaymentMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPaymentMethod As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rbPrintedCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbPrintedCheck As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rbHandCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbHandCheck As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtCheckNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCheckNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''trAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trAmount As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''txtAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmount As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''calPaymentDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calPaymentDate As Global.BACRM.Include.calandar

    '''<summary>
    '''lblPaymentTotalAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPaymentTotalAmount As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOverPaid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOverPaid As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnExportCheck control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportCheck As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''TransactionInfo1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TransactionInfo1 As Global.TransactionInfo

    '''<summary>
    '''hdnPaymentTotalAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnPaymentTotalAmt As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfCheckNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCheckNo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfCheckHeaderID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCheckHeaderID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnBillPaymentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBillPaymentID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnReturnHeaderID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnReturnHeaderID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnPaymentAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnPaymentAmount As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''txtTotalPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalPage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTotalRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalRecords As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSortChar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSortChar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnGo1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGo1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtCurrrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCurrrentPage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''hdnRefundAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnRefundAmount As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnVendorFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnVendorFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnBizDocStatusFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBizDocStatusFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnReferenceFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnReferenceFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnBillAmountFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBillAmountFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnAmtDueFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAmtDueFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnAmttoPayFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAmttoPayFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnBillFliter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBillFliter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnFromDateFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFromDateFilter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnToDateFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnToDateFilter As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnSelectedBills control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSelectedBills As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''UpdateProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress
End Class
