﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmJournalDetailsReport.aspx.vb" Inherits=".frmJournalDetailsReport"
    MasterPageFile="~/common/GridMasterRegular.Master" ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>General Journal Details</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style>
        .GraySmallText {
            font-size: small;
            font-weight: bold;
            color: gray;
        }
    </style>

    <script type="text/javascript">
        function pageLoaded() {
            var pageIndex = 0;
            var pageCount;
            var AccountList;
            var LoadComplete = 0;
            CallWait = 0;

            List1 = { items: [{ index: "-1", Pageindex: "0", PageIndexCalled: "0", Loaded: "0", LoadComplete: '0' }] };

            $(window).scroll(function () {
                if ($(window).scrollTop() >= $(document).height() - $(window).height() - 500) {
                    GetAccountTrans();
                }
                else if ($(window).scrollTop() == 0) {
                    GetAccountTrans();
                }
            });

            PushAccountList();

            $("[id*=RepeaterTable] tr").eq(1).hide();
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }

        function PushAccountList(strAccountList) {
            //List1.items.push({ "index": -1, "Pageindex": "0", "PageIndexCalled": "0", "Loaded": "0", "LoadComplete": "0" });
            List1.items.push({ index: 0, Pageindex: "0", PageIndexCalled: "0", Loaded: "0", LoadComplete: "0" });
            var pageIndex1 = 0;
            var i;
            for (i = 0; i < 10000; i++) {
                List1.items.push({ index: i + 1, Pageindex: "0", PageIndexCalled: "0", Loaded: "1", LoadComplete: "0" });
            }
            console.log(List1);
            GetAccountTrans()
        }

        function GetAccountTrans() {
            //var AccIds = strAccountList.toString();
            var pageIndex1 = 0;
            var reqForward = 0;
            var PrevBalance = 0;
            if (CallWait == 0) {
                $.each(List1.items, function () {
                    var item = this;
                    console.log('Enters each List Array Item : ' + item.index);

                    console.log('item.index:' + item.index);
                    console.log('item.LoadComplete:' + item.LoadComplete);
                    console.log('item.Loaded:' + item.Loaded);

                    if (item.index > -1) {
                        if (item.LoadComplete == 0) {
                            if (item.Loaded > 0) {
                                console.log('item.Pageindex:' + item.Pageindex);
                                pageIndex1 = parseInt(item.Pageindex) + 1;
                                console.log('Page Index 4: ' + pageIndex1 + '. \r\n');
                            }
                            else {
                                pageIndex1 = 1;
                                console.log('Page Index 5: ' + pageIndex1 + '. \r\n');
                            }

                            //AccountID = item.AccountID;
                            //ItemCode = item.ItemCode;
                            //PrevBalance = item.PrevBalance;
                            //if (AccountID != '' && AccountID != '0') {
                            //  console.log('Account ID : '+ item.AccountID + ', PageIndexCalled : ' + item.PageIndexCalled + ', Current Page Index : ' + pageIndex1);
                            console.log('item.PageIndexCalled:' + item.PageIndexCalled);
                            if (item.PageIndexCalled < pageIndex1) {

                                //console.log('Request forward to Get Transactions for Account ID : ' + AccountID + ', ItemCode : ' + ItemCode);
                                //  console.log('Array Index  : ' + item.index + ', AccountID : ' + item.AccountID + ', pageIndex1 : ' + pageIndex1 + ', Loaded : ' + item.Loaded + ', LoadComplete : ' + item.LoadComplete + '.\r\n ');

                                GetTransactions(pageIndex1);
                                item.PageIndexCalled = pageIndex1;
                                reqForward = 1;
                                //console.log('executed Exit loop.');
                                return false;
                                // console.log('1 : ' + item.AccountID);
                            }
                            else {
                                // console.log('Request already forward for page Index : ' + pageIndex1 + ' for Account ID : ' + item.AccountID);
                            }
                            //  console.log('2 : ' + item.AccountID);
                            //}
                            //else {

                            //console.log('Account ID is Not Empty : ' + item.AccountID);
                            //  console.log('3 : ' + item.AccountID);
                        }
                        else {
                            // console.log('Load Completed : ' + item.AccountID);
                        }
                        // console.log('4 : ' + item.AccountID);
                    }
                    else {
                        //  console.log('Invalid Account Number : ' + item.AccountID);
                    }
                    // console.log('5 : ' + item.AccountID);
                });
            }
        }

        function GetTransactions(pageIndex1) {
            CallWait = 1;
            $get('UpdateProgress1').style.display = "block";

            var DomainID = '<%= Session("DomainId")%>';
            var PagingRows = 0;
            var strDateFormat = '<%= Session("DateFormat")%>';
            var FromDate = $('#ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate').val();
            var ToDate = $('#ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate').val();

            var dataParam = "{DomainID:'" + DomainID + "',pageIndex:'" + pageIndex1 + "',strDateFormat:'" + strDateFormat + "',FromDate:'" + FromDate + "',ToDate:'" + ToDate + "'}";

            $.ajax({
                type: "POST",
                url: "../Accounting/frmJournalDetailsReport.aspx/WebMethodLoadGeneralDetails",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    Console.Log('Sorry!.,Getting records failed!.,');
                    $get('UpdateProgress1').style.display = "none";
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    $get('UpdateProgress1').style.display = "none";
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                    alert($("#lblErrMessage").text());
                }, complete: function () {
                    CallWait = 0;
                    $get('UpdateProgress1').style.display = "none";
                }
            });
        }

        function OnSuccess(response) {
            try {
                //console.log(1);
                var Jresponse = $.parseJSON(response.d);
                //console.log(Jresponse);
                //console.log(Jresponse.Table);
                //var AccountId = '';
                //var ItemID = '';
                var TransLoaded = '';
                //var PrevBalance = 0;
                var ImportedTransactionCount = 0;
                var TransactionDetails = Jresponse.Table0;
                //console.log(TransactionDetails);

                var row = $("[id*=RepeaterTable] tr").eq(1).clone(true);
                //$("[id*=RepeaterTable] tr").eq(1).hide();

                //AccountId = Jresponse.AccountDetails[0].AccountID;
                TransLoaded = Jresponse.AccountDetails[0].TransactionLoaded;
                //PrevBalance = Jresponse.AccountDetails[0].PrevBalance;
                //ItemID = Jresponse.AccountDetails[0].ItemID;
                var i = 0;

                //if (ItemID != '0') {
                //    var headerRow = $("[id*=RepeaterTable] tr").eq(0);
                //    headerRow.find("td:eq(6)").remove();
                //    headerRow.find("td:eq(7)").remove();
                //    headerRow.find("td:eq(8)").remove();
                //}

                //console.log("Still no error.");
                //console.log(TransactionDetails);
                $.each(TransactionDetails, function (index, Transaction) {
                    i++;
                    //console.log(TransactionDetails);
                    //console.log(Transaction);
                    //console.log('Transaction.numAccountID:' + Transaction.numAccountID);
                    var LogMessage = '';

                    //console.log(Transaction.Date);
                    //console.log(Transaction.numAccountId);
                    //console.log(Transaction.vcAccountName);
                    //console.log(Transaction.numDebitAmt);
                    //console.log(Transaction.numCreditAmt);

                    ////if (parseFloat(Transaction.numAccountId) > 0) {

                    LogMessage += 'Date : ' + Transaction.Date + ', ';
                    row.find('#lblJDate').html(Transaction.Date);

                    if (parseFloat(Transaction.numAccountId) > 0) {
                        LogMessage += 'numAccountId : ' + Transaction.numAccountId + ', ';
                        row.find('#lblAccountID').html(Transaction.numAccountId);
                    }
                    else {
                        LogMessage += 'numAccountId : ' + '' + ', ';
                        row.find('#lblAccountID').html('');
                    }

                    if (parseFloat(Transaction.numDebitAmt) == 0) {
                        LogMessage += 'TranDesc : ' + Transaction.vcAccountName + ', ';
                        row.find('#lblTranDesc').html(Transaction.vcAccountName);
                    }
                    else {
                        LogMessage += 'TranDesc : ' + '' + ', ';
                        row.find('#lblTranDesc').html('');
                    }

                    if (parseFloat(Transaction.numCreditAmt) == 0) {
                        LogMessage += 'RefAccountName : ' + Transaction.vcAccountName + ', ';
                        row.find('#lblRefAccountName').html(Transaction.vcAccountName);
                    }
                    else {
                        LogMessage += 'RefAccountName : ' + '' + ', ';
                        row.find('#lblRefAccountName').html('');
                    }

                    if (parseFloat(Transaction.numCreditAmt) == 0 && parseFloat(Transaction.numDebitAmt) == 0) {

                        LogMessage += 'numDebitAmt : ' + '' + ', ';
                        row.find('#lblnumDebitAmt').html('<span style="color:white !important">0</span>');

                        LogMessage += 'numCreditAmt : ' + '' + ', ';
                        row.find('#lblnumCreditAmt').html('<span style="color:white !important">0</span>');
                    }
                    else {
                        LogMessage += 'numDebitAmt : ' + Transaction.numDebitAmt + ', ';
                        row.find('#lblnumDebitAmt').html(parseFloat(Transaction.numDebitAmt).toFixed(2));

                        LogMessage += 'numCreditAmt : ' + Transaction.numCreditAmt + ', ';
                        row.find('#lblnumCreditAmt').html(parseFloat(Transaction.numCreditAmt).toFixed(2));
                    }
                    ////}
                    ////else {
                    ////    LogMessage += 'Date : ' + Transaction.Date + ', ';
                    ////    row.find('#lblJDate').html('');

                    ////    LogMessage += 'numAccountId : ' + Transaction.numAccountId + ', ';
                    ////    row.find('#lblAccountID').html('');

                    ////    if (parseFloat(Transaction.numDebitAmt) == 0) {
                    ////        LogMessage += 'TranDesc : ' + Transaction.vcAccountName + ', ';
                    ////        row.find('#lblTranDesc').html('');
                    ////    }
                    ////    else {
                    ////        LogMessage += 'TranDesc : ' + '' + ', ';
                    ////        row.find('#lblTranDesc').html('');
                    ////    }

                    ////    if (parseFloat(Transaction.numCreditAmt) == 0) {
                    ////        LogMessage += 'RefAccountName : ' + Transaction.vcAccountName + ', ';
                    ////        row.find('#lblRefAccountName').html('');
                    ////    }
                    ////    else {
                    ////        LogMessage += 'RefAccountName : ' + '' + ', ';
                    ////        row.find('#lblRefAccountName').html('');
                    ////    }

                    ////    LogMessage += 'numDebitAmt : ' + Transaction.numDebitAmt + ', ';
                    ////    row.find('#lblnumDebitAmt').html('');

                    ////    LogMessage += 'numCreditAmt : ' + Transaction.numCreditAmt + ', ';
                    ////    row.find('#lblnumCreditAmt').html('');
                    ////}

                    //console.log('Row count : ' + i);
                    //console.log(LogMessage);
                    //if (ItemID != '0') {
                    //row.find('#btnDeleteAction').remove();
                    //row.find('#lnkDelete').parent().remove();
                    //row.find('#lblNarration').parent().remove();
                    //row.find('#lblBalance').parent().remove();
                    //row.find('#ddlReconStatus').parent().remove();
                    //}
                    if (Transaction.numTransactionId == '-1') {
                        //row.find('#ddlReconStatus').remove();
                        //row.find('#btnDeleteAction').remove();
                        //row.find('#lnkDelete').remove();
                        //row.find('#lnkDetail').remove();

                        row.show();
                        $("[id*=RepeaterTable] tbody").append(row);
                        row = $("[id*=RepeaterTable] tr").eq(1).clone(true);
                        //LogMessage += 'Account Name : ' + Transaction.Date;
                        //LogMessage += ', So ddlReconStatus and btnDeleteAction Controls Removed, ';
                    }
                    else {
                        ImportedTransactionCount++;
                        //row.find('#ddlReconStatus').show();
                        //row.find('#btnDeleteAction').show();

                        //if (Transaction.numLandedCostOppId > 0)
                        //    row.find('#btnDeleteAction').hide();
                        //else
                        //    row.find('#btnDeleteAction').show();

                        //row.find('#lnkDelete').show();
                        //row.find('#lnkDetail').show();
                        //row.find('#lnkType').attr("onclick", "return CallEditCommand('" + Transaction.numTransactionId + "');");
                        //row.find('#btnDeleteAction').attr("onclick", "return CallDeleteCommand('" + Transaction.numTransactionId + "');");
                        //row.find('#lnkDetail').attr("onclick", "return CallDetailCommand('" + Transaction.numTransactionId + "');");
                        row.show();
                        $("[id*=RepeaterTable] tbody").append(row);
                        row = $("[id*=RepeaterTable] tr:last-child").clone(true);
                    }
                });

                // console.log('Imported Transaction Count for Account ID : ' + AccountId + ', is : ' + ImportedTransactionCount + '.\r\n ');
                $.each(List1.items, function () {
                    var item = this;
                    var calPgIndex = 0;
                    if (item.index > -1) {
                        //if (item.AccountID == AccountId) {
                        item.Loaded = TransLoaded;
                        //item.PrevBalance = PrevBalance;
                        var calPgIndex = Math.floor(item.Loaded / 100);
                        var rem = item.Loaded % 100;
                        if (rem > 0) {
                            calPgIndex++;
                        }
                        item.Pageindex = calPgIndex;
                        if (ImportedTransactionCount < 100) {
                            item.LoadComplete = "1";
                        }
                        //console.log('Imported Transaction Count for Account ID : ' + item.AccountID + ', is : ' + ImportedTransactionCount + '.\r\n ');
                        //  console.log('Array Index : ' + item.index + ', AccountID : ' + item.AccountID + ', Page Index : ' + calPgIndex + ', Loaded : ' + item.Loaded + ', LoadComplete : ' + item.LoadComplete + '.\r\n ');
                        //}
                    }
                });

                $get('UpdateProgress1').style.display = "none";
                CallWait = 0;
                if ($(window).height() >= $("body").height()) {
                    // console.log('has no ScrollBar');
                    GetAccountTrans();
                }
            }
            catch (e) {
                var errMessage = e.message + '. ' + e.description + '., Please contact the BizAutomation support.';
                $("#lblErrMessage").text(errMessage);
                console.log('error : ' + e.responseText);
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            From
                        </label>
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <label>
                            To
                        </label>
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-group">
                        <asp:LinkButton ID="btnGo" runat="server" CssClass="button btn btn-primary" OnClientClick="return Go();"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateExcel" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="ibExportExcel" runat="server" CssClass="button btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="ibExportExcel" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table>
                <tr align="center">
                    <td valign="middle" style="margin-left: 25%">
                        <asp:Label ID="lblErrMessage" Visible="true" Style="color: Crimson;" runat="server" EnableViewState="False"></asp:Label>
                        <asp:Literal ID="litMessage" Visible="true" runat="server" EnableViewState="False"></asp:Literal>

                    </td>
                </tr>

            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    General Journal Details&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmjournaldetailsreport.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">

    <asp:Table ID="tblJournalEntry" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow Width="100%">
            <asp:TableCell VerticalAlign="top">
                <table cellpadding="0" width="100%" cellspacing="0" border="0" bgcolor="white" runat="server"
                    id="tblExport">
                    <tr valign="top">
                        <td valign="top">
                            <asp:UpdatePanel ID="uppnlGLTransList" runat="server" UpdateMode="Conditional">
                                <Triggers>
                                </Triggers>
                                <ContentTemplate>
                                    <div id="RepeaterTable">
                                        <asp:Repeater ID="RepJournalEntry" runat="server">
                                            <HeaderTemplate>
                                                <table cellpadding="0" cellspacing="0" class="dg table table-responsive table-bordered" border="0" width="100%">
                                                    <tr class="hs" style="background-color: white !important">
                                                        <th class="td1" align="center">
                                                            <b>Date</b>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Account ID</b>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Reference</b>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Trans Description</b>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Debit Amt</b>
                                                        </th>
                                                        <th class="td1" align="center">
                                                            <b>Credit Amt</b>
                                                        </th>
                                                    </tr>
                                                    <%--</table>--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<tr class='<%# IIf(Container.ItemIndex Mod 2 = 0, "tr1", "normal1")%>'>--%>
                                                <tr class="normal1" style="background-color: white !important">
                                                    <td>
                                                        <asp:Label ID="lblJDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Date")%>'></asp:Label>
                                                    </td>
                                                    <td class="td1" align="center">
                                                        <asp:Label ID="lblAccountID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "numAccountId")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTranDesc" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "vcAccountName")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRefAccountName" EnableViewState="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "vcAccountName")%>'></asp:Label>
                                                    </td>
                                                    <td align="right" class="money">
                                                        <asp:Label ID="lblnumDebitAmt" EnableViewState="false" runat="server" Text='<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "numDebitAmt"))%>'></asp:Label>
                                                    </td>
                                                    <td align="right" class="money">
                                                        <asp:Label ID="lblnumCreditAmt" EnableViewState="false" runat="server" Text='<%#ReturnMoney(DataBinder.Eval(Container.DataItem, "numCreditAmt"))%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div style="text-align: center;">
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="uppnlGLTransList">
            <ProgressTemplate>
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
