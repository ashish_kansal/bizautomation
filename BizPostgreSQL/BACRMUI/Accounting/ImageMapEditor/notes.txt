////patternObjects = eval('(' + d + ')');
//var s = 'data/patterns.js';
//var myPatterns = new Array();
//$.getJSON(s, function (d) {
//    $.each(d["Summer"], function() {
//        $.each(this, function(name, value) {
//        //alert(name + '=' + value);
//        });
//    });
//});
				
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Get image width using File API</title>
  </head>

  <body>
    <input type='file' onchange="readURL(this);" />
      
    <script>
      function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            var img = new Image;
            img.onload = function() {
              console.log("The width of the image is " + img.width + "px.");
            };
            img.src = reader.result; 
          };
          reader.readAsDataURL(input.files[0]);
        }
      }
    </script>
  </body>
</html>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" src="../stage/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
    function imageURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview_img').attr('src', e.target.result)
                 .width('auto')
                 .height('auto');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
</head>
<body>
<input type="file" class="file" onchange="imageURL(this)" />
<img id="preview_img" style="width:auto; height:auto;" />
</body>
</html>



// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

var tmp = {one: 1, two: "2"};
JSON.stringify(tmp); // '{"one":1,"two":"2"}'



//need too set global value for url here!
$("#btnXJSON").click(function () {
    var s = '/data/data1.json';
    //alert(JSON.stringify(canvas)); 
    //$('#codedata').html(canvas.toJSON());
    $.ajax({
        dataType: 'text',
        success: function(string) {
            canvas.loadFromJSON(string);
        },
        url: s
    });
    return false;
});


// "title" is the mapValue & "img" is the short path for the pattern image
function SetMapSectionPattern(title, img) {
    canvas.forEachObject(function(object){
        if(object.mapValue == title){
            loadPattern(object, img);
        }
    });
    canvas.renderAll();
    canvas.calcOffset()
    clearNodes();
}

function loadPattern(obj, url) {
    obj.pattern = url;
    var tempX = obj.scaleX;
    var tempY = obj.scaleY;
    var zfactor = (100 / obj.scaleX) * canvasScale;

    fabric.Image.fromURL(url, function(img) { 
        img.scaleToWidth(zfactor).set({
            originX: 'left',
            originY: 'top'
        });
        var patternSourceCanvas = new fabric.StaticCanvas();
        patternSourceCanvas.add(img);
        var pattern = new fabric.Pattern({
        source: function() {
            patternSourceCanvas.setDimensions({
                width: img.getWidth(),
                height: img.getHeight()
            });
            return patternSourceCanvas.getElement();
            },
            repeat: 'repeat'
        });
        fabric.util.loadImage(url, function(img) {
            // you can customize what properties get applied at this point
            obj.fill = pattern;
            canvas.renderAll();
        });

    });
}




// restore background image
//var src = "uploads/" + imageid;
//fabric.util.loadImage(src, function (img) {
//    imgMap = new fabric.Image(img);
//    imgMap.set({
//        left: imgMap.width / 2,
//        top: imgMap.height / 2
//    });
//    //////////////////////////////////////////////////////////////////////////////////// 
//    imgMap.toObject = (function(toObject) {
//        return function() {
//        return fabric.util.object.extend(toObject.call(this), {
//            mapKey: this.mapKey,
//            link: this.link,
//            alt: this.alt,
//            mapValue: this.mapValue,
//            pattern: this.pattern
//        });
//        };
//    })(imgMap.toObject);
//    imgMap.mapKey = mapKey;
//    imgMap.link = '#';
//    imgMap.alt = '';
//    imgMap.mapValue = 'background';
//    imgMap.pattern = '';
//    //////////////////////////////////////////////////////////////////////////////////// 
//    gLeft = imgMap.left;
//    gTop = imgMap.top;
//    imgMap.hasRotatingPoint = true;
//    imgMap.scaleX = imgMap.scaleY = 1.00;
//    imgMap.hasControls = false;
//    imgMap.selectable = false;
//    canvas.add(imgMap);
//    shapes.push(imgMap);
//    canvas.renderAll();
//    imgMap.sendToBack();
//    canvas.calcOffset();
//    $('#mapImage').css('visibility', 'hidden');
//});




var canvas,
poly = {
    type: "polygon",
    top: 152.37857938957478,
    left: 436.53951260288073,
    scaleX: 0.3348979766803841,
    scaleY: 0.3348979766803841,
    points: [{
        x: -941.5,
        y: -225
    }, {
        x: 340.50000000000006,
        y: -223.99999999999997
    }, {
        x: 352.50000000000006,
        y: -150.99999999999997
    }, {
        x: 714.5,
        y: -149
    }, {
        x: 760.5000000000001,
        y: -38.99999999999999
    }, {
        x: 1109.5000000000002,
        y: -38.99999999999999
    }, {
        x: 1191.5000000000002,
        y: -80.00000000000001
    }, {
        x: 742.4999999999999,
        y: 221.00000000000006
    }, {
        x: 594.4999999999998,
        y: 225
    }, {
        x: 550.5000000000001,
        y: 222
    }, {
        x: 492.4999999999999,
        y: 212.99999999999997
    }, {
        x: 442.49999999999994,
        y: 184.99999999999997
    }, {
        x: 397.50000000000017,
        y: 155.99999999999997
    }, {
        x: -130.5,
        y: -196
    }, {
        x: -411.49999999999994,
        y: -9.999999999999986
    }, {
        x: -723.5,
        y: 175.99999999999994
    }, {
        x: -741.5,
        y: 179.00000000000006
    }, {
        x: -743.5,
        y: 189.99999999999997
    }, {
        x: -1191.5,
        y: 188
    }, ]
};

$(function () {
    canvas = window.__canvas = new fabric.Canvas('canvas');
    canvas.backgroundColor = '#efefef';

    canvas.add(fabric.Polygon.fromObject(poly));
    canvas.renderAll();

    fabric.Object.NUM_FRACTION_DIGITS = 10;

    document.getElementById('save').addEventListener('click', function () {
        window.__json = JSON.stringify(canvas.toJSON());
        canvas.clear();
    });
    document.getElementById('load').addEventListener('click', function () {
        canvas.loadFromJSON(window.__json, function () {
            canvas.renderAll();
        });
    });
    document.getElementById('reload').addEventListener('click', function () {
        window.__json = JSON.stringify(canvas.toJSON());
        canvas.clear();
        canvas.loadFromJSON(window.__json, function () {
            canvas.renderAll();
        });
    });
});






