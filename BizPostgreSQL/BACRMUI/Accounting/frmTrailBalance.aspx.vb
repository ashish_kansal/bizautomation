''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmTrailBalance
    Inherits BACRMPage

#Region "Variables"
    Dim mobjTrailBalance As TrailBalance
    'Dim dsTrialBalance As DataSet = New DataSet
    Dim dtTrialBalance As DataTable '= dsTrialBalance.Tables.Add("TrailBalance")
    Dim mintTotalDebit As Decimal
    Dim mintTotalCredit As Decimal
    Dim dtChartAcntForTrialBalance As DataTable

    Dim objCommon As CCommon
#End Region
    Public OpeningTotal As Double
    Public CreditTotal As Double
    Public DebitTotal As Double
    Public BalanceTotal As Double
    Public DifferenceInBalance As String
    Public boolHideOpeningClosing As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'To Set Permission
            GetUserRightsForPage(35, 90)

            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                Dim lobjGeneralLedger As New GeneralLedger
                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ''DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)

                        If PersistTable.ContainsKey(chkHideOpeningClosing.ID) Then
                            chkHideOpeningClosing.Checked = PersistTable(chkHideOpeningClosing.ID)
                        End If
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    End Try
                End If

                bindUserLevelClassTracking()
                LoadGeneralDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()
                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Old Code Commented by chintan 25/09/2009
    Private Sub LoadGeneralDetails()
        Try
            Dim ds As DataSet
            If mobjTrailBalance Is Nothing Then mobjTrailBalance = New TrailBalance
            'dtTrialBalance.Columns.Add("Type", Type.GetType("System.String"))
            'dtTrialBalance.Columns.Add("Debit", Type.GetType("System.String"))
            'dtTrialBalance.Columns.Add("Credit", Type.GetType("System.String"))
            'dtTrialBalance.Columns.Add("numAccountId", Type.GetType("System.String"))
            mobjTrailBalance.DomainID = Session("DomainID")
            mobjTrailBalance.FromDate = calFrom.SelectedDate
            mobjTrailBalance.ToDate = CDate(calTo.SelectedDate & " 23:59:59") 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
            mobjTrailBalance.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)

            ds = mobjTrailBalance.GetChartAcntDetails()
            dtChartAcntForTrialBalance = ds.Tables(0)
            'Dim dvParentnode As New DataView(dtChartAcntForTrialBalance)          'create a dataview object
            'Dim lobjChartofAccounts As New ChartOfAccounting
            'lobjChartofAccounts.DomainId = Session("DomainID")
            'dvParentnode.RowFilter = "numParntAcntTypeId=" & lobjChartofAccounts.GetRootNode
            'LoadTreeGrid(dvParentnode)
            If ds.Tables(1).Rows.Count > 0 Then
                OpeningTotal = ds.Tables(1).Rows(0)("OpeningTotal")
                DebitTotal = ds.Tables(1).Rows(0)("DebitTotal")
                CreditTotal = ds.Tables(1).Rows(0)("CreditTotal")
                BalanceTotal = ds.Tables(1).Rows(0)("BalanceTotal")
            End If

            If OpeningTotal <> 0 Then
                DifferenceInBalance = "Difference In Opening Balance: " & (ReturnMoney(OpeningTotal - BalanceTotal)).ToString()
            End If
            'Dim drTotalDrCr As DataRow = dtChartAcntForTrialBalance.NewRow
            'If dtChartAcntForTrialBalance.Rows.Count > 0 Then
            '    drTotalDrCr("Type") = "1"
            '    drTotalDrCr("numAccountID") = 0
            '    drTotalDrCr("AccountCode1") = "<font color=Black><b> Total </b></font>"
            '    drTotalDrCr("AccountName1") = "<font color=Black><b> Total </b></font>"
            '    drTotalDrCr("Opening") = dtChartAcntForTrialBalance.Compute("sum(Opening)", "")

            'drTotalDrCr("Debit") = dtChartAcntForTrialBalance.Compute("sum(Debit)", "")
            'drTotalDrCr("Credit") = dtChartAcntForTrialBalance.Compute("sum(Credit)", "")
            'drTotalDrCr("Balance") = dtChartAcntForTrialBalance.Compute("sum(Balance)", "")
            ''drTotalDrCr("Credit") = "<font color=Black><b>" & dtChartAcntForTrialBalance.Compute("sum(Credit)", "") & "</b></font>"
            'dtChartAcntForTrialBalance.Rows.Add(drTotalDrCr)
            'End If

            'Show Cr and Dr Symbol after Opening  balance amount 
            dtChartAcntForTrialBalance.Columns.Add("OpeningSymbol")
            dtChartAcntForTrialBalance.Columns.Add("ClosingSymbol")
            For Each dr As DataRow In dtChartAcntForTrialBalance.Rows
                If CCommon.ToDecimal(dr("Opening")) < 0 Then
                    dr("OpeningSymbol") = "Cr"
                    dr("Opening") = dr("Opening") * -1 'Remove negative sign
                Else
                    dr("OpeningSymbol") = "Dr"
                End If
                If CCommon.ToDecimal(dr("Balance")) < 0 Then
                    dr("ClosingSymbol") = "Cr"
                    dr("Balance") = dr("Balance") * -1
                Else
                    dr("ClosingSymbol") = "Dr"
                End If
            Next
            dtChartAcntForTrialBalance.AcceptChanges()

            boolHideOpeningClosing = chkHideOpeningClosing.Checked

            RepTrialBalance.DataSource = dtChartAcntForTrialBalance
            RepTrialBalance.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Old Code Commented by chintan 25/09/2009
    'Private Sub LoadTreeGrid(ByVal dtChartAcntDetails As DataView)
    '    Dim j As Integer
    '    Try
    '        For j = 0 To dtChartAcntDetails.Count - 1
    '            If dtChartAcntDetails.Item(j)("Amount").ToString <> "0.00" Then
    '                If dtChartAcntDetails.Item(j)("numAcntTypeId") = 815 Or dtChartAcntDetails.Item(j)("numAcntTypeId") = 816 Or dtChartAcntDetails.Item(j)("numAcntTypeId") = 820 Or dtChartAcntDetails.Item(j)("numAcntTypeId") = 821 Or dtChartAcntDetails.Item(j)("numAcntTypeId") = 822 Or dtChartAcntDetails.Item(j)("numAcntTypeId") = 825 Or dtChartAcntDetails.Item(j)("numAcntTypeId") = 827 Then
    '                    Dim drTrialBalanceDetails As DataRow = dtTrialBalance.NewRow
    '                    drTrialBalanceDetails(0) = "<font color=Black><b> " & dtChartAcntDetails.Item(j)("AcntTypeDescription") & "</b></font>"

    '                    drTrialBalanceDetails(1) = IIf(dtChartAcntDetails.Item(j)("Amount") < 0, Replace(ReturnMoney(dtChartAcntDetails.Item(j)("Amount")), "-", ""), "")
    '                    drTrialBalanceDetails(2) = IIf(dtChartAcntDetails.Item(j)("Amount") > 0, Replace(ReturnMoney(dtChartAcntDetails.Item(j)("Amount")), "-", ""), "")
    '                    drTrialBalanceDetails(3) = dtChartAcntDetails.Item(j)("numAccountId")

    '                    dtTrialBalance.Rows.Add(drTrialBalanceDetails)
    '                    mintTotalDebit = mintTotalDebit + IIf(drTrialBalanceDetails(1) = "", 0, drTrialBalanceDetails(1))
    '                    mintTotalCredit = mintTotalCredit + IIf(drTrialBalanceDetails(2) = "", 0, drTrialBalanceDetails(2))

    '                Else
    '                    Dim drTrialBalanceDetails As DataRow = dtTrialBalance.NewRow

    '                    drTrialBalanceDetails(0) = "<font color=Black><b>" & dtChartAcntDetails.Item(j)("AcntTypeDescription") & "</b></font>"
    '                    drTrialBalanceDetails(1) = IIf(dtChartAcntDetails.Item(j)("Amount") > 0, Replace(ReturnMoney(dtChartAcntDetails.Item(j)("Amount")), "-", ""), "")
    '                    drTrialBalanceDetails(2) = IIf(dtChartAcntDetails.Item(j)("Amount") < 0, Replace(ReturnMoney(dtChartAcntDetails.Item(j)("Amount")), "-", ""), "")
    '                    drTrialBalanceDetails(3) = dtChartAcntDetails.Item(j)("numAccountId")

    '                    dtTrialBalance.Rows.Add(drTrialBalanceDetails)
    '                    mintTotalDebit = mintTotalDebit + IIf(drTrialBalanceDetails(1) = "", 0, drTrialBalanceDetails(1))
    '                    mintTotalCredit = mintTotalCredit + IIf(drTrialBalanceDetails(2) = "", 0, drTrialBalanceDetails(2))
    '                End If
    '            End If
    '            Dim dvChildnode As New DataView(dtChartAcntForTrialBalance)          'create a dataview object
    '            dvChildnode.RowFilter = "numParntAcntTypeId=" & dtChartAcntDetails.Item(j)("numAccountId")
    '            LoadTreeGrid(dvChildnode)
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''Private Function GetChildCategory(ByVal ParentID As String) As DataTable
    ''    Try
    ''        If mobjTrailBalance Is Nothing Then
    ''            mobjTrailBalance = New TrailBalance
    ''        End If
    ''        mobjTrailBalance.ParentAccountId = ParentID
    ''        mobjTrailBalance.DomainId = Session("DomainId")
    ''        GetChildCategory = mobjTrailBalance.GetChildCategory()
    ''    Catch ex As Exception
    ''        Throw ex
    ''    End Try
    ''End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", CDec(Money))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub RepTrialBalance_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepTrialBalance.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                'Dim lblName As Label
                'Dim lblChartAcntId As Label
                ''Dim lblType As Label
                'lblChartAcntId = e.Item.FindControl("lblChartAcntId")
                'lblName = e.Item.FindControl("lblName")
                'lblType = e.Item.FindControl("lblType")
                'If lblChartAcntId.Text <> "" Then
                '    'lblName.Attributes.Add("onclick", "return OpenTransactionDetailsPage('" & "../Accounting/frmTransactions.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=frmTrialBalance&ChartAcntId=" & lblChartAcntId.Text & "')")
                '    'lblType.Visible = False
                'Else
                '    'lblType.Visible = True
                '    lblName.Visible = False
                'End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Add(chkHideOpeningClosing.ID, chkHideOpeningClosing.Checked)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        ExportToExcel.aspTableToExcel(tblExport, Response)
    End Sub

    Private Sub chkHideOpeningClosing_CheckedChanged(sender As Object, e As EventArgs) Handles chkHideOpeningClosing.CheckedChanged
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class