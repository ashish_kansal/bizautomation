<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCheckPrint.aspx.vb"
    Inherits="BACRM.UserInterface.Accounting.frmCheckPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Check Print</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<body>
    <form id="form1" runat="server">
    <script language="Javascript" type="text/javascript">
        setInterval("Lfprintcheck()", 1000)
    </script>
    <asp:Table HorizontalAlign="Center" ID="tbl" runat="server" Width="649" BorderStyle="Double"
        BorderWidth="5" BorderColor="black">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <table width="649" cellpadding="1" cellspacing="1" align="center" border="0" background="../images/Bk1.gif">
                    <tr>
                        <td colspan="3" align="left">
                            <asp:Image ID="imgCompanyLogo" runat="server"></asp:Image>
                        </td>
                        <td align="right" class="normal1">
                            <b>
                                <asp:Label ID="CheckNo" runat="server"></asp:Label></b>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="normal1">
                            <b>
                                <asp:Label ID="lblChkDate" Text="Date" runat="server"></asp:Label></b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                            <hr width="120" size="0" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="normal1">
                            <b>Pay to the Order of</b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblCompany" runat="server"></asp:Label>
                            <hr width="350" size="0" />
                        </td>
                        <td align="left" class="normal1">
                            <b>Amount </b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAmount" runat="server"></asp:Label>
                            <hr width="120" size="0" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3" class="normal1">
                            <asp:Label ID="lblMoneyDescription" runat="server"></asp:Label>
                            <hr width="600" size="0" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="left">
                            <b>Memo</b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMemo" runat="server"></asp:Label>
                            <hr width="350" size="0" />
                        </td>
                        <td align="left" class="normal1">
                            <b>Signature</b>
                        </td>
                        <td align="left">
                            <hr width="150" size="0" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:Image ID="imgBankLogo" runat="server"></asp:Image>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" class="normal1">
                            <table runat="server" id="tblBankDet" border="0">
                                <tr>
                                    <td>
                                        <b>|:&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblBankRouteNo" runat="server" CssClass="signup"></asp:Label>&nbsp;&nbsp;&nbsp;</b>
                                    </td>
                                    <td>
                                        <b>|:&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblBankAccNo" runat="server" CssClass="signup"></asp:Label>&nbsp;&nbsp;&nbsp;</b>
                                    </td>
                                    <td>
                                        <b>|:&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblCheckNo" runat="server" CssClass="signup"></asp:Label>&nbsp;&nbsp;&nbsp;</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <script language="JavaScript" type="text/javascript">
        function Lfprintcheck() {
            this.print();
            history.back();
            // document.location.href = "frmChecks.aspx";
        }
    </script>
    </form>
</body>
</html>
