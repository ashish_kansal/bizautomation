﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmChartOfAccountsDetail.aspx.vb"
    Inherits=".frmChartOfAccountsDetail" MasterPageFile="~/common/GridMasterRegular.Master" EnableViewState="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <div class="form-inline">
            <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</asp:LinkButton>
            <a href="#" class="help">&nbsp;</a>
        </div>
    </div>
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Balance summary by Organization
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6 col-md-offset-2">
       <asp:Table ID="tblSummary" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow Width="100%">
            <asp:TableCell VerticalAlign="top" HorizontalAlign="Center">
                
                            <asp:Repeater ID="RepSummary" runat="server" >
                                <HeaderTemplate>
                                    <table cellpadding="0" class="table table-responsive table-bordered" cellspacing="0" border="0" align="center">
                                        <tr class="hs">
                                            <th align="center">
                                              Organization Name
                                            </th>
                                            <%-- <td bgcolor="#52658C" align="center">
                                                <b>Opening</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Debit</b>
                                            </td>
                                            <td bgcolor="#52658C" align="center">
                                                <b>Credit</b>
                                            </td>--%>
                                            <th  align="center">
                                                Balance
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="is">
                                        <td>
                                            <a href='frmGeneralLedger.aspx?Mode=3&AcntTypeID=<%#Eval("numParntAcntTypeId")%>&AccountID=<%=strChartAcntIds %>&DivisionID=<%#Eval("numDivisionID")%>'><%#Eval("CompanyName")%></a>
                                            <asp:Label ID="lblDivisionID" runat="server" Visible="false" Text='<%# Eval("numDivisionID") %>'></asp:Label>
                                        </td>
                                        <%-- <td align="right">
                                            <%#ReturnMoney(Eval("Opening"))%>
                                        </td>
                                        <td align="right">
                                            <%#ReturnMoney(Eval("TotalDebit"))%>
                                        </td>
                                        <td align="right">
                                            <%#ReturnMoney(Eval("TotalCredit"))%>
                                        </td>--%>
                                        <td align="right">
                                            <%#ReturnMoney(Eval("Closing"))%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr  class="ais">
                                        <td>
                                            <a href='frmGeneralLedger.aspx?Mode=3&AcntTypeID=<%#Eval("numParntAcntTypeId")%>&AccountID=<%=strChartAcntIds %>&DivisionID=<%#Eval("numDivisionID")%>'><%#Eval("CompanyName")%></a>
                                            <asp:Label ID="lblDivisionID" runat="server" Visible="false" Text='<%# Eval("numDivisionID") %>'></asp:Label>
                                        </td>
                                        <%--  <td align="right">
                                            <%#ReturnMoney(Eval("Opening"))%>
                                        </td>
                                        <td align="right">
                                            <%#ReturnMoney(Eval("TotalDebit"))%>
                                        </td>
                                        <td align="right">
                                            <%#ReturnMoney(Eval("TotalCredit"))%>
                                        </td>--%>
                                        <td align="right">
                                            <%#ReturnMoney(Eval("Closing"))%>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                      
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
