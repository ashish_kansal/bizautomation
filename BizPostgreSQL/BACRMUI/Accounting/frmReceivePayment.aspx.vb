'''Created By Siva
'Imports BACRM.BusinessLogic.Common
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Opportunities

'Partial Public Class frmMakePayment
'    Inherits BACRMPage

'#Region "Variables"


'    Dim objReceivePayment As ReceivePayment
'    Dim objMakeDeposit As MakeDeposit
'    Dim dtItems As New DataTable
'    Dim mintDepositId As Integer
'#End Region

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            'To Set Permission
'            objCommon = New CCommon
'            GetUserRightsForPage(35, 79)

'            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnDepositSave.Visible = False


'            lblDepositTotal.Text = "Deposit Total: " & Session("Currency")
'            If Not IsPostBack Then

'                calReceivePayment.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
'                LoadReceivePaymentGrid()
'                FillDepositTocombo()
'            End If
'            btnDepositSave.Attributes.Add("onclick", "return Save()")
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub LoadReceivePaymentGrid()
'        Try
'            If objReceivePayment Is Nothing Then objReceivePayment = New ReceivePayment
'            objReceivePayment.DomainID = Session("DomainId")
'            objReceivePayment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
'            dgReceivePayment.DataSource = objReceivePayment.GetReceivePaymentDetails
'            dgReceivePayment.DataBind()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub FillDepositTocombo()
'        Try
'            Dim dtChartAcntDetails As DataTable
'            Dim objCOA As New ChartOfAccounting
'            objCOA.DomainID = Session("DomainId")
'            objCOA.AccountCode = "010101"
'            dtChartAcntDetails = objCOA.GetParentCategory()
'            Dim item As ListItem
'            For Each dr As DataRow In dtChartAcntDetails.Rows
'                item = New ListItem()
'                item.Text = dr("vcAccountName")
'                item.Value = dr("numAccountID")
'                item.Attributes("OptionGroup") = dr("vcAccountType")
'                ddlDepositTo.Items.Add(item)
'            Next
'            ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub
'    Dim UndepositedFundAccountID As Long
'    Private Sub btnDepositSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDepositSave.Click
'        Try
'            'validation of Account Existance
'            Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
'            objAuthoritativeBizDocs.DomainID = Session("DomainID")
'            'Undeposited Fund
'            UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID"))
'            If Not UndepositedFundAccountID > 0 Then
'                litMessage.Text = "Please Map Default Undeposited Funds Account for Your Company from Administration->Domain Details->Accounting->Default Accounts."
'                Exit Sub
'            End If

'            SaveDataToGeneralJournalDetails()
'            LoadReceivePaymentGrid()

'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_BizDocsPaymentDetId As Integer, ByVal p_OppBizDocId As Integer, ByVal p_OppId As Integer) As Integer
'        Try
'            Dim objJEHeader As New JournalEntryHeader
'            Dim lngJournalId As Long
'            With objJEHeader
'                .JournalId = 0
'                .RecurringId = 0
'                .EntryDate = CDate(calReceivePayment.SelectedDate & " 12:00:00")
'                .Description = ""
'                .Amount = p_Amount
'                .CheckId = 0
'                .CashCreditCardId = 0
'                .ChartAcntId = ddlDepositTo.SelectedValue
'                .OppId = p_OppId
'                .OppBizDocsId = p_OppBizDocId
'                .DepositId = 0
'                .BizDocsPaymentDetId = p_BizDocsPaymentDetId
'                .IsOpeningBalance = 0
'                .LastRecurringDate = Date.Now
'                .NoTransactions = 0
'                .CategoryHDRID = 0
'                .ReturnID = 0
'                .CheckHeaderID = 0
'                .BillID = 0
'                .BillPaymentID = 0
'                .UserCntID = Session("UserContactID")
'                .DomainID = Session("DomainID")
'            End With
'            lngJournalId = objJEHeader.Save()
'            Return lngJournalId
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub SaveDataToGeneralJournalDetails()
'        Dim dtgriditem As DataGridItem
'        Dim dtrow As DataRow
'        Dim dtrow1 As DataRow
'        Dim i As Integer
'        Dim lstr As String
'        Dim lobjAuthoritativeBizDocs As New AuthoritativeBizDocs
'        Dim JournalId As Integer
'        Dim ldecAmount As Decimal
'        Dim lintBizDocsPaymentDetId As Integer
'        Dim lintBizDocsPaymentDetailsId As Integer
'        Dim lintOppBizDocsId As Integer
'        Dim lintOppId As Integer

'        Dim PaymentMethod As String
'        'Dim lngDivisionID As Long
'        Dim AccountID As Long
'        Dim DepositId As Integer
'        Try
'            If objReceivePayment Is Nothing Then objReceivePayment = New ReceivePayment

'            DepositId = SaveDataToDepositHeader()

'            ldecAmount = 0
'            For i = 0 To dgReceivePayment.Items.Count - 1

'                dtgriditem = dgReceivePayment.Items(i)
'                If CType(dtgriditem.FindControl("chkSelected"), CheckBox).Checked = True Then
'                    ldecAmount = ldecAmount + IIf(CType(dtgriditem.FindControl("lblAmount"), Label).Text = "", 0, Replace(CType(dtgriditem.FindControl("lblAmount"), Label).Text, ",", ""))
'                End If
'            Next

'            'Multiple payments received need to consolidate to 1 deposit to match bank transaction.  

'            Dim ds As New DataSet
'            Dim dt As New DataTable
'            dt.Columns.Add("TransactionId") '' Primary key
'            dt.Columns.Add("numJournalId")
'            dt.Columns.Add("numDebitAmt")
'            dt.Columns.Add("numCreditAmt")
'            dt.Columns.Add("numChartAcntId")
'            dt.Columns.Add("varDescription")
'            dt.Columns.Add("numCustomerId")
'            dt.Columns.Add("numDomainId")
'            dt.Columns.Add("numBizDocsPaymentDetId")
'            dt.Columns.Add("numBizDocsPaymentDetailsId")
'            dt.Columns.Add("numCurrencyID")
'            dt.Columns.Add("fltExchangeRate")
'            dt.Columns.Add("monOrgCreditAmt")
'            dt.Columns.Add("monOrgDebitAmt")

'            dt.Columns.Add("bitReconcile")
'            dtrow = dt.NewRow
'            dtrow1 = dt.NewRow

'            If CCommon.ToInteger(dtgriditem.Cells(8).Text) > 0 Then 'TransactionID>0
'                'Process Selected Journal Entries-Added By Chintan. 

'                DepositId = SaveDataToDepositHeader()
'                JournalId = SaveDataToHeader1(DepositId)
'                SaveDataToGeneralJournalDetails(JournalId)
'                Exit Sub
'            End If
'            'When we select a row in money in and when we deposit it.. it will 
'            'Debit:Selected Account
'            'Credit: Un-deposit Fund
'            PaymentMethod = dtgriditem.Cells(8).Text

'            lintBizDocsPaymentDetailsId = dtgriditem.Cells(1).Text
'            lintBizDocsPaymentDetId = dtgriditem.Cells(2).Text
'            lintOppBizDocsId = dtgriditem.Cells(3).Text
'            lintOppId = dtgriditem.Cells(4).Text
'            JournalId = SaveDataToHeader(ldecAmount, lintBizDocsPaymentDetId, lintOppBizDocsId, lintOppId)

'            dtrow.Item("TransactionId") = 0
'            dtrow.Item("numJournalId") = JournalId
'            dtrow.Item("numDebitAmt") = ldecAmount
'            dtrow.Item("numCreditAmt") = 0
'            dtrow.Item("numChartAcntId") = ddlDepositTo.SelectedItem.Value
'            dtrow.Item("varDescription") = "Receive Payment"
'            dtrow.Item("numCustomerId") = dtgriditem.Cells(5).Text
'            dtrow.Item("numDomainId") = Session("DomainId")
'            dtrow.Item("numBizDocsPaymentDetId") = lintBizDocsPaymentDetId
'            dtrow.Item("numBizDocsPaymentDetailsId") = lintBizDocsPaymentDetailsId
'            dtrow.Item("bitReconcile") = 0
'            dtrow.Item("numCurrencyID") = dtgriditem.Cells(6).Text
'            dtrow.Item("fltExchangeRate") = dtgriditem.Cells(7).Text
'            dtrow.Item("monOrgDebitAmt") = CType(dtgriditem.FindControl("lblOrgAmount"), Label).Text
'            dtrow.Item("monOrgCreditAmt") = 0
'            dt.Rows.Add(dtrow)



'            dtrow1.Item("TransactionId") = 0
'            dtrow1.Item("numJournalId") = JournalId
'            dtrow1.Item("numDebitAmt") = 0
'            dtrow1.Item("numCreditAmt") = ldecAmount
'            dtrow1.Item("numChartAcntId") = UndepositedFundAccountID
'            dtrow1.Item("varDescription") = "Receive Payment"
'            dtrow1.Item("numCustomerId") = dtgriditem.Cells(5).Text
'            dtrow1.Item("numDomainId") = Session("DomainId")
'            dtrow1.Item("bitReconcile") = 0
'            dtrow.Item("numCurrencyID") = dtgriditem.Cells(6).Text
'            dtrow.Item("fltExchangeRate") = dtgriditem.Cells(7).Text
'            dtrow.Item("monOrgDebitAmt") = 0
'            dtrow.Item("monOrgCreditAmt") = CType(dtgriditem.FindControl("lblOrgAmount"), Label).Text
'            dt.Rows.Add(dtrow1)
'            ds.Tables.Add(dt)
'            lstr = ds.GetXml()
'            objReceivePayment.JournalId = JournalId
'            objReceivePayment.Mode = 0
'            objReceivePayment.JournalDetails = lstr
'            objReceivePayment.DomainID = Session("DomainId")
'            objReceivePayment.SaveDataToJournalDetailsForReceivePayment()
'            dt = Nothing
'            ds = Nothing
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub ddlDepositTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepositTo.SelectedIndexChanged
'        Try
'            Dim lobjCashCreditCard As New CashCreditCard
'            Dim lstrColor As String
'            Dim ldecOpeningBalance As Decimal
'            If ddlDepositTo.SelectedItem.Value <> 0 Then
'                lobjCashCreditCard.AccountId = ddlDepositTo.SelectedItem.Value
'                lobjCashCreditCard.DomainID = Session("DomainId")
'                lblBalance.Text = "Balance: " & Session("Currency")
'                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
'                lstrColor = "<font color=red>" & ReturnMoney(ldecOpeningBalance) & "</font>"
'                lblBalanceAmount.Text = IIf(ldecOpeningBalance < 0, lstrColor, ReturnMoney(ldecOpeningBalance))
'                'lblDepositTotalAmount.Text = ReturnMoney(DepositTotalAmt.Value)
'            Else
'                lblBalanceAmount.Text = ""
'                lblBalance.Text = ""
'                'lblDepositTotalAmount.Text = ReturnMoney(DepositTotalAmt.Value)
'            End If
'            CalculateTotal()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub
'    Private Sub CalculateTotal()

'        Dim dblDepositeTotalAmount As Double = 0
'        Dim strAmount As String = ""
'        For i As Integer = 0 To dgReceivePayment.Items.Count - 1
'            If (CType(dgReceivePayment.Items(i).FindControl("chkSelected"), CheckBox).Checked) Then
'                strAmount = CType(dgReceivePayment.Items(i).FindControl("lblAmount"), Label).Text
'                dblDepositeTotalAmount += Double.Parse(IIf(strAmount = "", "0.0", strAmount))
'            End If
'        Next
'        lblDepositTotalAmount.Text = ReturnMoney(dblDepositeTotalAmount.ToString())
'    End Sub


'    Function ReturnMoney(ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then
'                If IsNumeric(Money) Then
'                    'If Money <> 0 Then
'                    Return String.Format("{0:#,##0.00}", Money)
'                End If
'            Else
'                Return ""
'            End If
'            Return ""
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub dgReceivePayment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReceivePayment.ItemCommand
'        Try
'            If e.CommandName = "Delete" Then
'                If objReceivePayment Is Nothing Then objReceivePayment = New ReceivePayment
'                objReceivePayment.BizDocsPaymentDetailsId = e.Item.Cells(1).Text
'                objReceivePayment.BizDocsPaymentDetId = e.Item.Cells(2).Text
'                objReceivePayment.BizDocsId = e.Item.Cells(3).Text
'                objReceivePayment.OppId = e.Item.Cells(4).Text
'                ''objReceivePayment.Amount = Replace(e.Item.Cells(6).Text, ",", "")
'                objReceivePayment.DomainID = Session("DomainID")
'                objReceivePayment.DeleteReceivePaymentDetails()
'                LoadReceivePaymentGrid()
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgReceivePayment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReceivePayment.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
'                Dim btnDelete As Button
'                Dim lnkDelete As LinkButton
'                lnkDelete = e.Item.FindControl("lnkDeleteAction")
'                btnDelete = e.Item.FindControl("btnDeleteAction")
'                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
'                    btnDelete.Visible = False
'                    lnkDelete.Visible = True
'                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
'                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
'                End If
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    '' To Save Deposit details in DepositDetails table
'    Private Function SaveDataToDepositHeader() As Integer
'        Dim lintDepositId As Integer
'        Try
'            If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
'            With objMakeDeposit
'                .Entry_Date = calReceivePayment.SelectedDate
'                ''   .numAmount = Replace(lblDepositTotalAmount.Text, ",", "")
'                .numAmount = 0
'                .DomainID = Session("DomainId")
'                .AccountId = ddlDepositTo.SelectedItem.Value
'                '.RecurringId = ddlMakeRecurring.SelectedItem.Value
'                If Not IsNothing(mintDepositId) And mintDepositId <> 0 Then

'                    .DepositId = mintDepositId
'                    .SaveDataToMakeDepositDetails()
'                    lintDepositId = mintDepositId
'                Else
'                    .UserCntID = Session("UserContactID")
'                    .DepositId = 0
'                    lintDepositId = .SaveDataToMakeDepositDetails()
'                End If
'                Return lintDepositId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    ''To Save Header Details in General_Journal_Header table
'    Private Function SaveDataToHeader1(ByVal p_DepositId As Integer) As Integer
'        Dim lntJournalId As Integer
'        Try
'            Dim objJEHeader As New JournalEntryHeader
'            Dim lngJournalId As Long

'            With objJEHeader
'                .JournalId = 0
'                .RecurringId = 0
'                .EntryDate = CDate(calReceivePayment.SelectedDate & " 12:00:00")
'                .Description = ""
'                .Amount = CCommon.ToDecimal(Replace(DepositTotalAmt.Value, ",", ""))
'                .CheckId = 0
'                .CashCreditCardId = 0
'                .ChartAcntId = ddlDepositTo.SelectedItem.Value
'                .OppId = 0
'                .OppBizDocsId = 0
'                .DepositId = p_DepositId
'                .BizDocsPaymentDetId = 0
'                .IsOpeningBalance = 0
'                .LastRecurringDate = Date.Now
'                .NoTransactions = 0
'                .CategoryHDRID = 0
'                .ReturnID = 0
'                .CheckHeaderID = 0
'                .BillID = 0
'                .BillPaymentID = 0
'                .UserCntID = Session("UserContactID")
'                .DomainID = Session("DomainID")
'            End With
'            lngJournalId = objJEHeader.Save()
'            Return lngJournalId
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    ''To Save Details in General_Journal_Details table
'    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Long)
'        Dim i As Integer
'        Dim dtgriditem As DataGridItem
'        Dim ddlAccounts As DropDownList
'        Dim lintAccountId() As String
'        Try
'            Dim objJEList As New JournalEntryCollection

'            Dim objJE As New JournalEntryNew()

'            For i = 0 To dgReceivePayment.Items.Count - 1
'                dtgriditem = dgReceivePayment.Items(i)
'                If CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString <> "0" And CType(dtgriditem.FindControl("txtCredit"), TextBox).Text <> "" Then ''And CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value.ToString <> "0" Then
'                    ddlAccounts = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList)
'                    lintAccountId = ddlAccounts.SelectedItem.Value.Split("~")

'                    objJE = New JournalEntryNew()

'                    objJE.TransactionId = IIf(CType(dtgriditem.FindControl("lblTransactionId"), Label).Text = "", 0, CType(dtgriditem.FindControl("lblTransactionId"), Label).Text)
'                    objJE.DebitAmt = 0
'                    objJE.CreditAmt = IIf(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text, ",", ""))
'                    objJE.ChartAcntId = lintAccountId(0)
'                    objJE.Description = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                    objJE.CustomerId = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                    objJE.MainDeposit = 0
'                    objJE.MainCheck = 0
'                    objJE.MainCashCredit = 0
'                    objJE.OppitemtCode = 0
'                    objJE.BizDocItems = ""
'                    objJE.Reference = CType(dtgriditem.FindControl("txtReference"), TextBox).Text
'                    objJE.PaymentMethod = CType(dtgriditem.FindControl("ddlPayment"), DropDownList).SelectedItem.Value
'                    objJE.Reconcile = False
'                    objJE.CurrencyID = 0
'                    objJE.FltExchangeRate = 0
'                    objJE.TaxItemID = 0
'                    objJE.BizDocsPaymentDetailsId = 0
'                    objJE.ContactID = 0
'                    objJE.ItemID = 0
'                    objJE.ProjectID = 0
'                    objJE.ClassID = 0
'                    objJE.CommissionID = 0
'                    objJE.ReconcileID = 0
'                    objJE.Cleared = 0
'                    objJE.ReferenceType = 0
'                    objJE.ReferenceID = 0

'                    objJEList.Add(objJE)
'                End If
'            Next

'            'To Add the Debit Amount in JournalDetails table [i.e] For Bank Account Selected
'            objJE = New JournalEntryNew()

'            objJE.TransactionId = IIf(ViewState("TransactionId") = Nothing, 0, ViewState("TransactionId"))
'            objJE.DebitAmt = Replace(DepositTotalAmt.Value, ",", "")
'            objJE.CreditAmt = 0
'            objJE.ChartAcntId = ddlDepositTo.SelectedItem.Value
'            objJE.Description = ""
'            objJE.CustomerId = 0
'            objJE.MainDeposit = 1
'            objJE.MainCheck = 0
'            objJE.MainCashCredit = 0
'            objJE.OppitemtCode = 0
'            objJE.BizDocItems = ""
'            objJE.Reference = ""
'            objJE.PaymentMethod = 0
'            objJE.Reconcile = False
'            objJE.CurrencyID = 0
'            objJE.FltExchangeRate = 0
'            objJE.TaxItemID = 0
'            objJE.BizDocsPaymentDetailsId = 0
'            objJE.ContactID = 0
'            objJE.ItemID = 0
'            objJE.ProjectID = 0
'            objJE.ClassID = 0
'            objJE.CommissionID = 0
'            objJE.ReconcileID = 0
'            objJE.Cleared = 0
'            objJE.ReferenceType = 0
'            objJE.ReferenceID = 0

'            objJEList.Add(objJE)

'            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

'            'ds.Tables.Add(dtItems)
'            'lstr = ds.GetXml()
'            'objMakeDeposit.JournalId = lintJournalId

'            'If Not IsNothing(mintDepositId) And mintDepositId <> 0 Then
'            '    objMakeDeposit.Mode = 1 '' For Update Purpose
'            'Else : objMakeDeposit.Mode = 0 '' For Insert Purpose
'            'End If

'            'objMakeDeposit.JournalDetails = lstr
'            'objMakeDeposit.RecurringMode = 0
'            'objMakeDeposit.SaveDataToJournalDetailsForDeposit()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    '' To Save Deposit details in DepositDetails table
'    Private Function SaveDataToDepositHeader(DepositTotalAmt As Decimal) As Integer
'        Dim lintDepositId As Integer
'        Try
'            If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
'            With objMakeDeposit
'                .Entry_Date = calReceivePayment.SelectedDate
'                ''   .numAmount = Replace(lblDepositTotalAmount.Text, ",", "")
'                .numAmount = DepositTotalAmt
'                .DomainID = Session("DomainId")
'                .AccountId = ddlDepositTo.SelectedItem.Value
'                .RecurringId = 0
'                If Not IsNothing(mintDepositId) And mintDepositId <> 0 Then

'                    .DepositId = mintDepositId
'                    .SaveDataToMakeDepositDetails()
'                    lintDepositId = mintDepositId
'                Else
'                    .UserCntID = Session("UserContactID")
'                    .DepositId = 0
'                    lintDepositId = .SaveDataToMakeDepositDetails()
'                End If
'                Return lintDepositId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function
'End Class