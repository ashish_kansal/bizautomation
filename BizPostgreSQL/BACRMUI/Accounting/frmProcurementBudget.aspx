<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProcurementBudget.aspx.vb" Inherits=".frmProcurementBudget" %>
<%@ Register Assembly="Syncfusion.Tools.Web, Version=4.402.0.51, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89"
    Namespace="Syncfusion.Web.UI.WebControls.Tools" TagPrefix="cc1" %>  
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>
    <title>Procurement Budget</title>
</head>
<body>
    <form id="form1" runat="server">
       <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
        <br />
    <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					
					<td valign="bottom" width ="400px">
						<table class="TabStyle" >
							<tr>
					    		<td >&nbsp;&nbsp;&nbsp;Procurement Budget &nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
						</td> 
						<td class="normal1" align="right">
						<asp:button id="btnProSave"  Runat="server" CssClass="button" Text="Save"></asp:button> 
    					<asp:button id="btnProClose" Runat="server"  CssClass="button" Text="Cancel" ></asp:button>  </td>
						 
				</tr>
			</TABLE>
     <asp:table id="Table4"  BorderWidth="1" Runat="server" Width="100%" CssClass="aspTableDTL"
									    BorderColor="black" GridLines="none"  Height="600">
									<asp:TableRow >
									<asp:TableCell VerticalAlign="Top" >
								 					
									<table id="Table6" runat="server" width="100%" border="0">
    								<tr >
    								 <td class="normal1" align="left"></td>
    					              <td class="normal1" align="right">			 
    								 <asp:CheckBox ID="chkProPurchaseDeal" runat ="server" Text ="Display the budget balance in the �New Purchase Opportunity� window as soon as item from budgeted item group is selected" /></td> 
    								  <td  class="normal1"  align ="right"><asp:Label id="lblProcFiscalYear" runat ="server" Text ="Fiscal Year"></asp:Label></td>
    						          <td>  <asp:DropDownList ID="ddlProcFiscalYear" runat ="server" Width ="200px" CssClass="signup" AutoPostBack ="true">
    						          <asp:ListItem Text ="This Year" Value ="0" Selected ="true"></asp:ListItem>
    						          <asp:ListItem Text ="Last Year" Value ="1"></asp:ListItem>
    						          <asp:ListItem Text ="Next Year " Value ="2"></asp:ListItem>
    						          </asp:DropDownList></td>
    								</tr>
    								</table>
								 <igtbl:ultrawebgrid id="UlgProcurement"   DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml" Height="" Width ="">
					                    <DisplayLayout AllowAddNewDefault="Yes" AllowDeleteDefault="Yes" AutoGenerateColumns="true" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single"
					                    ViewType="Flat" SelectTypeCellDefault="NotSet" ColFootersVisibleDefault="no"  BorderCollapseDefault="NotSet" AllowColSizingDefault="Fixed" 
					                    Name="UlgProcurement" EnableClientSideRenumbering="true"    SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
                                                   <ClientSideEvents  />
                                            <HeaderStyleDefault VerticalAlign="Middle"  Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </HeaderStyleDefault>
                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                    <FrameStyle Width="100%" Cursor="Default" BorderWidth="0" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                    <FooterStyleDefault  BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </FooterStyleDefault>
                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                    </RowStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
                                          </DisplayLayout>
                                         	                    <Bands>
												                    <igtbl:UltraGridBand  AllowDelete="no" BaseTableName="HierChartOfActForBudget" Key="HierChartOfActForBudget">
												                    <Columns>
												                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>
					</asp:TableCell>
					</asp:TableRow>
			</asp:table>
    </form>
</body>
</html>
