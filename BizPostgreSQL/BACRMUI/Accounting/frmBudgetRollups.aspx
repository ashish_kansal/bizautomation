<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBudgetRollups.aspx.vb" Inherits=".frmBudgetRollups" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Budget Roll ups</title>
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>
</head>
<body>
    <form id="form1" runat="server" method ="post" >
     <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
       <br />
 
    <TABLE id="Table4" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					
					<td valign="bottom" width ="400px">
						<table class="TabStyle" >
							<tr>
					    		<td >&nbsp;&nbsp;&nbsp;Budget Roll-up &nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
						</td> 
						  <td  class="normal1"  align ="center"><asp:Label id="lblFiscal" runat ="server" Text ="Fiscal Year"></asp:Label> &nbsp;&nbsp;
    						       
					      <asp:DropDownList ID="ddlFiscal" runat ="server" Width ="200px" CssClass="signup" AutoPostBack ="true" >
    						          <asp:ListItem Text ="This Year" Value ="0" Selected ="true"></asp:ListItem>
    						          <asp:ListItem Text ="Last Year" Value ="1"></asp:ListItem>
    						          <asp:ListItem Text ="Next Year " Value ="2"></asp:ListItem>
    						          </asp:DropDownList></td>	
						 
				</tr>
			</TABLE>
			    <asp:table id="tblOppr"  BorderWidth="1" Runat="server" Width="100%" CssClass="aspTableDTL"
									    BorderColor="black" GridLines="none" Height="600">
									<asp:TableRow>
									<asp:TableCell VerticalAlign="top">
		<%--	<igtbl:ultrawebgrid id="ultraRollUp"   DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="" Width ="" >
					                   <DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single" AutoGenerateColumns="true"  
					                              ViewType="Flat" SelectTypeCellDefault="NotSet" ColFootersVisibleDefault="no"  BorderCollapseDefault="Separate" AllowColSizingDefault="Fixed" 
					                                    Name="UltraWebGrid1" EnableClientSideRenumbering="true"  TableLayout="Fixed"  SelectTypeColDefault="Extended" AllowUpdateDefault="Yes" GridLinesDefault="both"  >
                                                   <ClientSideEvents  />
                                      <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </HeaderStyleDefault>
                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                    <FrameStyle Width="100%"   Cursor="Default" BorderWidth="0" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                    <FooterStyleDefault  BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </FooterStyleDefault>
                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                    </RowStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
                                          </DisplayLayout> 
                							                    <Bands>
												                    <igtbl:UltraGridBand  AllowDelete="no" BaseTableName="BudgetRollup" Key="HierChartOfActForBudget">
												                    <Columns>
				 								                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>--%>
										                      </asp:TableCell></asp:TableRow>
								    </asp:table>
    </form>
</body>
</html>
