﻿<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMoneyTrail.aspx.vb"
    Inherits=".frmMoneyTrail" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript">
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <table width="100%">
        <tr>
            <td>
                <table align="right">
                    <tr>
                        <td class="normal1">
                            Customer/Vendor&nbsp;
                        </td>
                        <td>
                            <table width="200px">
                                <tr>
                                    <td>
                                        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="200px"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>
                                       
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="normal1">
                            Bank Account
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDepositTo" runat="server" Width="150px" CssClass="signup"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1">
                            &nbsp;From
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                        </td>
                        <td class="normal1">
                            &nbsp;To
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calTo" runat="server" />
                        </td>
                        <td class="normal1">
                            <asp:RadioButton ID="radMoneyOut" GroupName="rad" Checked="true" runat="server" Text="Money Out" /><br />
                            <asp:RadioButton ID="radMoneyIn" GroupName="rad" runat="server" Text="Money In" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" Width="80" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnGo" CssClass="button" runat="server" Text="Go"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="bottom" width="200px">
                            <table class="TabStyle">
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;Money Trail &nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Table ID="table2" Width="100%" runat="server" Height="350" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataGrid ID="dgSearch" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                AllowSorting="True">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="numTransactionId" Visible="false" />
                                    <asp:BoundColumn DataField="numJournalId" Visible="false" />
                                    <asp:BoundColumn DataField="numBizDocsPaymentDetailsId" Visible="false" />
                                    <asp:BoundColumn DataField="numBizDocsPaymentDetId" Visible="false" />
                                    <asp:BoundColumn DataField="numDepositId" Visible="false" />
                                    <asp:BoundColumn DataField="numCashCreditCardId" Visible="false" />
                                    <asp:BoundColumn DataField="numCheckId" Visible="false" />
                                    <asp:BoundColumn DataField="numOppID" Visible="false" />
                                    <asp:BoundColumn DataField="numOppBizDocsId" Visible="false" />
                                    <asp:BoundColumn DataField="numCheckNo" Visible="true" HeaderText="Check No" />
                                    <asp:TemplateColumn HeaderText="BizDoc">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplBizdcoc" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container.DataItem, "vcBizDocID") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="monAmount" Visible="true" HeaderText="Amount" />
                                    <asp:BoundColumn DataField="dtDueDate" Visible="true" HeaderText="Date" />
                                    <asp:BoundColumn DataField="varCurrSymbol" Visible="true" HeaderText="Currency" />
                                    <asp:BoundColumn DataField="vcCatgyName" Visible="true" HeaderText="Account" />
                                    <asp:BoundColumn DataField="vcReference" Visible="true" HeaderText="Reference" />
                                    <asp:BoundColumn DataField="PaymentMethod" Visible="true" HeaderText="Payment Method" />
                                    <asp:BoundColumn DataField="vcMemo" Visible="true" HeaderText="Memo" />
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
--%>