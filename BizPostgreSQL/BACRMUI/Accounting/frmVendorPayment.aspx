<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmVendorPayment.aspx.vb"
    Inherits=".frmVendorPayment" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Vendor Payment Details</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">

        function Save() {
            if (document.form1.ddlPaymentFrom.value == 0) {
                alert("Please select Payment Account");
                document.form1.ddlPaymentFrom.focus();
                return false;
            }

            if ($("#ddlPaymentMethod").val() == "5") {
                if (parseInt($('#txtCheckNo').val()) == 0 || $('#txtCheckNo').val().length == 0) {
                    alert("Please Provide Starting Check#");
                    $('#txtCheckNo').focus();
                    return false;
                }
            }

            if (parseFloat(document.getElementById("hdnRefundAmount").value) > parseFloat(document.getElementById("txtAmount").value)) {
                alert("This transaction has been used in Refund. If you want to change amount, you must edit the Refund and remove it first.");
                return false;
            }

            return true;
        }

        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function DownloadFile(a) {
            window.open(a, 'Download'); //bug id 2024
            return false;
        }
        function OpenBizInvoice(a, b, c, d) {
            if (d == 2 || d == 4) {
                if (a > 0) {
                    window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + a, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
                    return false;
                }
                if (parseInt(c) > 0) {
                    window.open('../Accounting/frmAddBill.aspx?BillID=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
                    return false;
                }
            }
            else {
                if (parseInt(a) > 0 && parseInt(b) > 0) {
                    window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                    return false;
                }
            }
        }

        var bBillAmount = false;
        var bCreditApplied = false;
        var bBillChk = false;
        var bAmount = false;
        var bFirstTime = false;
        var flag = true;

        function UpdateTotal() {
            var TotaltxtAmountToPay = 0.00;
            var TotalBillAmount = 0.00;
            var TotalAmountDue = 0.00;

            var txtAmountToPay = 0;
            var lblBillAmount = 0;
            var lblAmountDue = 0;
            var TotalCredits = 0.00;
            var txtAmount = 0.00;

            var selectedRecords = [];
            if ($("[id$=hdnSelectedBills]").val() !== "") {
                selectedRecords = JSON.parse($("[id$=hdnSelectedBills]").val());
            }

            $("[id$=dgBills] tr:not(:first-child):not(:last-child)").each(function () {
                lblAmountDue = Number(currencyToDecimal($(this).find("#lblAmountDue").text()));
                TotalAmountDue = TotalAmountDue + lblAmountDue;
            });

            $("#lblFooterTotalDue").text(formatCurrency(TotalAmountDue.toFixed(2)));

            lblFooterTotalDue = TotalAmountDue;

            var BillAmount = 0.00;

            if (bBillAmount || bBillChk) {
                $("[id$=dgBills] tr:not(:first-child):not(:last-child)").each(function () {
                    txtAmountToPay = Number(currencyToDecimal($(this).find("#txtAmountToPay").val()));

                    chk = $(this).find("[id*='chk']");

                    if ($(chk).is(':checked')) {
                        BillAmount += Number(currencyToDecimal($(this).find("#txtAmountToPay").val()));
                    }
                });

                lblFooterTotalDue = BillAmount;
            }


            var TotalNonAppliedAmount = 0.00;
            var TotalCreditAmount = 0.00;

            var Credits = 0;
            $("[id$=gvCredits] tr:not(:first-child):not(:last-child)").each(function () {
                txtCreditAmount = $(this).find("[id*='txtCreditAmount']");
                lblNonAppliedAmount = $(this).find("[id*='lblNonAppliedAmount']");
                chk = $(this).find("[id*='chk']");

                if (lblFooterTotalDue > TotalCredits && lblFooterTotalDue > 0 && Number(currencyToDecimal($(txtCreditAmount).val())) > 0) {
                    if ($(chk).is(':checked')) {
                        Credits = Number(currencyToDecimal($(txtCreditAmount).val()));

                        if ($("#chkAutoApplyCredits").is(':checked') == false) {
                            if (TotalCredits + Credits > lblFooterTotalDue) {
                                $(txtCreditAmount).val(Number(lblFooterTotalDue - TotalCredits).toFixed(2));
                                Credits = Number(currencyToDecimal($(txtCreditAmount).val()));
                            }

                            if (Credits == 0) {
                                $(chk).prop('checked', false);
                                $(txtCreditAmount).val('');
                            }
                        }

                        TotalCredits = TotalCredits + Credits;

                        TotalCreditAmount = TotalCreditAmount + Credits;
                    }
                }
                else {
                    $(chk).prop('checked', false);
                    $(txtCreditAmount).val('');
                }

                TotalNonAppliedAmount = TotalNonAppliedAmount + Number(currencyToDecimal($(lblNonAppliedAmount).text()));
            });

            $("#lblFooterTotalNonAppliedAmount").text(TotalNonAppliedAmount.toFixed(2));
            $("#lblFooterTotalCreditAmount").text(TotalCreditAmount.toFixed(2));
            $("#lblTotalCreditsLeft").text((TotalNonAppliedAmount - TotalCreditAmount).toFixed(2));

            if ($('#txtAmount') != null)
                txtAmount = Number($('#txtAmount').val());

            if (bAmount == false) {
                if (txtAmount > 0 && lblFooterTotalDue > TotalCredits && txtAmount > TotalCredits)
                    txtAmount = txtAmount - TotalCredits;
                else
                    txtAmount = 0;
            }

            var TotalAmountReceived = TotalCredits + txtAmount;

            $("[id$=dgBills] tr:not(:first-child):not(:last-child)").each(function () {
                //console.log($(this));
                var tr = $(this);
                lblBillAmount = Number(currencyToDecimal($(this).find("#lblBillAmount").text()))
                lblAmountDue = Number(currencyToDecimal($(this).find("#lblAmountDue").text()))
                txtAmountToPay = Number(currencyToDecimal($(this).find("#txtAmountToPay").val()))
                chk = $(this).find("[id*='chk']");

                if (bBillAmount || bBillChk) {
                    if ($(chk).is(':checked')) {

                    }
                    else {
                        txtAmountToPay = 0;
                    }
                }
                else if ((bCreditApplied && $("#chkAutoApplyCredits").is(':checked') == false) || (bAmount && (chk).is(':checked'))) {
                    if (TotalAmountReceived >= lblAmountDue) {
                        $(chk).prop("checked", true)
                        $(this).find("#txtAmountToPay").val(lblAmountDue)
                        TotalAmountReceived = parseFloat(TotalAmountReceived - lblAmountDue).toFixed(2)
                    }
                    else if (TotalAmountReceived < lblAmountDue && TotalAmountReceived > 0) {
                        $(chk).prop("checked", true)
                        $(this).find("#txtAmountToPay").val(TotalAmountReceived)
                        TotalAmountReceived = 0;
                    }
                    else {
                        $(this).find("input[type = 'checkbox']").prop("checked", false);
                        $(this).find("#txtAmountToPay").val('');
                    }

                    txtAmountToPay = Number($(this).find("#txtAmountToPay").val());
                }

                var tempRecords = $.grep(selectedRecords, function (e) {
                    return !(e.numBillID == $(tr).find("#hdnBillID").val() && e.numOppBizDocsID == $(tr).find("#hdnOppBizDocID").val());
                });
                selectedRecords = tempRecords;

                TotalBillAmount = TotalBillAmount + lblBillAmount;
                TotalAmountDue = TotalAmountDue + lblAmountDue;
                TotaltxtAmountToPay = TotaltxtAmountToPay + txtAmountToPay;
            });

            if (selectedRecords != null && selectedRecords.length > 0) {
                selectedRecords.forEach(function (item) {
                    TotaltxtAmountToPay = TotaltxtAmountToPay + item.monAmountToPay;
                });
            }

            var TotalAmount = 0;

            if (TotaltxtAmountToPay > TotalCredits)
                TotalAmount = TotaltxtAmountToPay - TotalCredits;

            var OverPaid = 0;


            if ($('#txtAmount') != null) {
                if (bFirstTime)
                    txtAmount = $("#hdnPaymentAmount").val();

                if (bAmount || bFirstTime) {
                    //txtAmount = txtAmount + TotalCredits;
                    if (txtAmount > TotalAmount) {
                        OverPaid = txtAmount - TotalAmount;

                        TotalAmount = txtAmount;
                    }
                }

                if (bFirstTime)
                    $('#txtAmount').val(txtAmount);
                else
                    $('#txtAmount').val(Number(TotalAmount.toFixed(2)));

                if (OverPaid > 0)
                    $("#lblOverPaid").text(Number(OverPaid)).css('color', '#660000').css("font-weight", "bold");
                else
                    $("#lblOverPaid").text('0.00').css('color', '#282828').css("font-weight", "normal");
            }

            //console.log(Total);
            //console.log(TotalBalDue);
            //if (!isNaN(Total) && !isNaN(TotalBalDue)) {
            TotaltxtAmountToPay = TotaltxtAmountToPay.toFixed(2);
            TotalBillAmount = TotalBillAmount.toFixed(2);
            TotalAmountDue = TotalAmountDue.toFixed(2);
            //TotalAmount = TotalAmount.toFixed(2);

            $('#lblFooterTotalBillAmt').text(formatCurrency(TotalBillAmount));
            $("#lblFooterTotalPayment").text(formatCurrency(TotaltxtAmountToPay));
            $("#lblPaymentTotalAmount").text(parseFloat(TotalAmount).toFixed(2));
            $("#hdnPaymentTotalAmt").val(TotalAmount || 0);

            //}

            bBillAmount = false;
            bBillChk = false;
            bAmount = false;
            bCreditApplied = false;
            bFirstTime = false;
            return true;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(pageLoaded);

            bFirstTime = true;
            pageLoaded();

            if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoaded);
            }
        });

        function pageLoaded() {
            UpdateTotal();


            $("#chkAll").change(function () {
                //console.log($("#chkAll").is(':checked'));
                $("[id$=dgBills] tr input[type = 'checkbox']").not("#chkAll").prop("checked", $("#chkAll").is(':checked'))
                flag = false;
                $("[id$=dgBills] tr input[type = 'checkbox']").not("#chkAll").change();
                flag = true;
                UpdateTotal();

            });

            $("#chkAllCredits").change(function () {
                $("[id$=gvCredits] tr input[type = 'checkbox']").not("#chkAllCredits").prop("checked", $("#chkAllCredits").is(':checked'))
                flag = false;
                $("[id$=gvCredits] tr input[type = 'checkbox']").not("#chkAllCredits").change();
                flag = true;
                UpdateTotal();
            });

            $("#txtAmount").change(function () {
                bAmount = true;
                $("#txtAmount").val(Number($("#txtAmount").val()).toFixed(2));
                UpdateTotal();
            });

            $("[id$=dgBills] tr input[type = 'checkbox']").not("#chkAll").change(function () {

                //console.log($(CurrentRow).find('#txtAmountToPay').val());
                var CurrentRow = $(this).parents("tr").filter(':first')
                var lblBalDue = parseFloat(currencyToDecimal($(CurrentRow).find("#lblAmountDue").text())).toFixed(2)
                if ($(this).is(':checked')) {

                    $(CurrentRow).find("#txtAmountToPay").val(lblBalDue)
                } else {
                    //console.log($(CurrentRow).find("#txtAmountToPay").val())
                    $(CurrentRow).find("#txtAmountToPay").val('')
                    //console.log($(CurrentRow).find("#txtAmountToPay").val())
                }

                bBillChk = true;

                if (flag == true)
                    UpdateTotal();
                //
                //UpdateAmtToPayTextBox();
            });

            $(".txtAmountToPay").change(function () {

                var BalanceDue = 0.0
                var txtAmountToPay = 0.0

                $(this).val(Number(currencyToDecimal($(this).val())).toFixed(2));

                $("[id$=dgBills] tr:not(:first-child):not(:last-child)").each(function () {

                    txtAmountToPay = parseFloat(currencyToDecimal($(this).find("#txtAmountToPay").val()))
                    BalanceDue = parseFloat(currencyToDecimal($(this).find("#lblAmountDue").text()))
                    if (txtAmountToPay >= BalanceDue) {
                        $(this).find("input[type = 'checkbox']").prop("checked", true)
                        $(this).find("#txtAmountToPay").val(formatCurrency(BalanceDue.toFixed(2)))
                        //txtAmountToPay = parseFloat(txtAmountToPay - BalanceDue).toFixed(2)
                    }
                    else if (txtAmountToPay < BalanceDue && txtAmountToPay > 0) {
                        $(this).find("input[type = 'checkbox']").prop("checked", true)
                        $(this).find("#txtAmountToPay").val(formatCurrency(txtAmountToPay))
                        txtAmountToPay = 0;
                    }
                    else {
                        $(this).find("input[type = 'checkbox']").prop("checked", false)
                        $(this).find("#txtAmountToPay").val('')
                    }
                });

                bBillAmount = true;

                UpdateTotal();

            });

            $("[id$=gvCredits] tr input[type = 'checkbox']").not("#chkAllCredits").change(function () {
                var CurrentRow = $(this).parent().parent().parent();

                txtCreditAmount = $(CurrentRow).find("[id*='txtCreditAmount']");
                lblNonAppliedAmount = $(CurrentRow).find("[id*='lblNonAppliedAmount']");

                if ($(this).is(':checked')) {
                    $(txtCreditAmount).val(Number(currencyToDecimal($(lblNonAppliedAmount).text())).toFixed(2));
                } else {
                    $(txtCreditAmount).val('');
                }

                bCreditApplied = true;

                if (flag == true)
                    UpdateTotal();
            });

            $("[id$=gvCredits] tr input[id='txtCreditAmount']").change(function () {
                var CurrentRow = $(this).parent().parent();

                $(this).val(Number($(this).val()).toFixed(2));

                txtCreditAmount = $(CurrentRow).find("[id*='txtCreditAmount']");
                lblNonAppliedAmount = $(CurrentRow).find("[id*='lblNonAppliedAmount']");
                chk = $(CurrentRow).find("[id*='chk']");

                if ($(txtCreditAmount).val() > $(lblNonAppliedAmount).text()) {
                    $(txtCreditAmount).val(Number(currencyToDecimal($(lblNonAppliedAmount).text())).toFixed(2));
                }

                if (Number(currencyToDecimal($(txtCreditAmount).val())) > 0) {
                    $(chk).prop('checked', true);
                }
                else {
                    $(txtCreditAmount).val('');
                    $(chk).prop('checked', false);
                }

                bCreditApplied = true;

                UpdateTotal();
            });

            $("#txtCheckNo").focus(function () {
                $("#rbHandCheck").prop("checked", true);
            });

            $("#ddlPaymentMethod").change(function () {
                if ($(this).val() == 5) {
                    $(".class_check :input").attr("disabled", false);
                    $(".class_check").show("slow");
                }
                else {
                    $(".class_check :input").attr("disabled", true);
                    $(".class_check").slideUp("slow");
                }

            });

            $("#ddlPaymentMethod").change();

            $('#rbHandCheck').change(function () {
                if ($(this).is(':checked')) {
                    $('#txtCheckNo').val($('#hfCheckNo').val());
                }
            });
        }

        function OpenAmtPaid(a, b, c) {
            if (c == 1) {
                var str;
                str = "../opportunity/frmReturnDetail.aspx?ReturnID=" + b;
                document.location.href = str;
            }
            else {
                window.location.href = "../Accounting/frmVendorPayment.aspx?BillPaymentID=" + a;
            }
            return false;
        }

        function FilterGrid() {

            document.getElementById('btnGo1').click().focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                <asp:LinkButton ID="btnAllowCheckNO" runat="server" CssClass="btn btn-primary" Visible="false">Yes</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Payment Account:</label>
                        <asp:DropDownList ID="ddlPaymentFrom" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                        <label>
                            <asp:Label ID="lblBalance" runat="server"></asp:Label></label>
                        <asp:Label ID="lblBalanceAmount" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Pay Bills&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmvendorpayment.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Filters</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <div class="form-inline">
                                <label>Vendor:</label>
                                <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    ClientIDMode="Static"
                                    ShowMoreResultsBox="true"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True"
                                    AutoPostBack="true">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                                <br /><i>(for use with purchase credits)</i>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3" runat="server" id="pnlAccountingClass">
                            <div class="form-inline">
                                <label>Class:</label>
                                <asp:DropDownList ID="ddlUserLevelClass" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6" runat="server" id="pnlCurrency">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-inline">
                                        <label>Currency:</label>
                                        <asp:DropDownList Width="200px" ID="ddlCurrency" runat="server" CssClass="form-control"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-inline">
                                        <label>Exchange Rate:</label>
                                        &nbsp;1&nbsp;<asp:Label runat="server" ID="lblForeignCurr"></asp:Label>&nbsp;=&nbsp;
                        <asp:TextBox ID="txtExchangeRate" runat="server" CssClass="form-control" Width="70" onkeypress="CheckNumber(1,event);"></asp:TextBox>&nbsp;&nbsp;<asp:Label
                            runat="server" ID="lblBaseCurr" Text="USD"></asp:Label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row" id="trCredits" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Credits</h3>

                    <div class="box-tools pull-right">
                        <input id="chkAutoApplyCredits" type="checkbox" name="chkAutoApplyCredits" />
                        <label for="chkAutoApplyCredits">Manually apply</label>&nbsp;&nbsp;
                               <b>Credit balance left to apply:</b>
                        <asp:Label Text="" runat="server" ID="lblTotalCreditsLeft" />
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <asp:GridView ID="gvCredits" runat="server" BorderWidth="0" CssClass="table table-striped table-bordered" AutoGenerateColumns="False"
                        Style="margin-right: 0px" Width="100%" ShowFooter="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAllCredits" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" Checked='<%# Eval("bitIsPaid") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <a href="#" onclick='javascript:OpenAmtPaid(<%#Eval("numReferenceID") %>,<%# Eval("numReturnHeaderID")%>,<%#Eval("tintRefType")%>)'>
                                        <%#Eval("vcReference")%></a>
                                    <asp:Label ID="lblReferenceID" runat="server" Style="display: none" Text='<%# Eval("numReferenceID")%>'></asp:Label>
                                    <asp:Label ID="lblRefType" Style="display: none" runat="server" Text='<%# Eval("tintRefType")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Original Amount" ItemStyle-HorizontalAlign="Right"
                                FooterStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("varCurrSymbol") + " " %><asp:Label ID="lblAmount" runat="server" Text='<%# ReturnMoney(Eval("monAmount"))%>'></asp:Label>
                                    <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monAmount") * Eval("fltExchangeRateReceivedPayment")) + ")</span>", "")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    Totals
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Balance" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("varCurrSymbol") + " " %><asp:Label ID="lblNonAppliedAmount" runat="server"
                                        Text='<%# ReturnMoney(Eval("monNonAppliedAmount"))%>'></asp:Label>
                                    <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monNonAppliedAmount") * Eval("fltExchangeRateReceivedPayment")) + ")</span>", "")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label Text="" runat="server" ID="lblFooterTotalNonAppliedAmount" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" ItemStyle-Width="190" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <div class="form-inline">
                                        <label><%#Eval("varCurrSymbol") + " " %></label>
                                        <asp:TextBox ID="txtCreditAmount" runat="server" AutoComplete="OFF" CssClass="money form-control" onkeypress="CheckNumber(1,event);" Text='<%# ReturnMoney(Eval("monAmountPaid")) %>'></asp:TextBox>
                                        <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monAmountPaid") * Eval("fltExchangeRateReceivedPayment")) + ")</span>", "")%>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label Text="" runat="server" ID="lblFooterTotalCreditAmount" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No credits available for this Vendor.
                        </EmptyDataTemplate>
                        <FooterStyle HorizontalAlign="Right" BackColor="#ebebeb" Font-Bold="true" />
                    </asp:GridView>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
   <div style="float:right;">
    <a href="#" id="hplClearGridCondition" onclick="FilterGrid()" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    </div>  
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">

                <div class="box-header with-border">
                    <h3 class="box-title">Bill Payment Information</h3>

                    <div class="box-tools pull-right">
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:DataGrid ID="dgBills" runat="server" CssClass="table table-striped table-bordered" Width="100%"  AutoGenerateColumns="False"
                                    ShowFooter="true" UseAccessibleHeader="true" AllowSorting="true" OnSortCommand="dgBills_SortCommand" EnableViewState="true">
                                    <FooterStyle BackColor="#ebebeb" Font-Bold="true" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll','chkSelct');" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chkSelct" runat="server"
                                                    Checked='<%# Eval("bitIsPaid") %>' />
                                                <asp:Label ID="lblCheck" runat="server" Text="*" Style="padding-left: 17px;" Visible="false"></asp:Label>
                                                <asp:HiddenField runat="server" ID="hdnOppBizDocID" Value='<%# Eval("numOppBizDocsId") %>' />
                                                <asp:HiddenField runat="server" ID="hdnBillID" Value='<%# Eval("numBillID")%>' />
                                                <asp:HiddenField runat="server" ID="hdnBillType" Value='<%# Eval("tintBillType")%>' />
                                                <asp:HiddenField runat="server" ID="hdnDivisionID" Value='<%# Eval("numDivisionID")%>' />
                                                <asp:HiddenField runat="server" ID="hdnExchangeRateOfOrder" Value='<%# Eval("fltExchangeRateOfOrder")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                       
                                        <asp:TemplateColumn HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                 <asp:LinkButton id="lnkDueDateFliter" runat="server" Text="Due Date" CommandName="Sort" CommandArgument="dtDueDate"></asp:LinkButton>
                                                 <table style="background-color: #ebebeb">
                                                <tr>
                                                    <td >
                                                        From
                                                        <br />
                                                        <asp:TextBox ID="txtFromDate" Width="80px" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged" runat="server"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="ajaxcalFromDate" TargetControlID="txtFromDate"  runat="server"></ajaxToolkit:CalendarExtender> 
                                                    </td>
                                                    <td>
                                                        &nbsp; 
                                                    </td>
                                                    <td >
                                                        To
                                                        <br />
                                                        <asp:TextBox ID="txtToDate" Width="80px" runat="server" AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="ajaxCalToDate" TargetControlID="txtToDate"  runat="server"></ajaxToolkit:CalendarExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("DueDate", "{0:MM/dd/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                            <HeaderTemplate>
                                                <asp:LinkButton id="lnkVendorFliter" runat="server" Text="Vendor" CommandName="Sort" CommandArgument="vcCompanyName"></asp:LinkButton><br />
                                                <asp:TextBox ID="txtVendorFliter" runat="server" AutoPostBack="true" OnTextChanged="txtVendorFliter_TextChanged" />          
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("vcCompanyName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bill" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                            <HeaderTemplate>
                                                  <asp:LinkButton id="lnkBillFliter" runat="server" Text="Bill" CommandName="Sort" CommandArgument="Name"></asp:LinkButton><br />
                                                  <asp:TextBox ID="txtBillFliter" runat="server" AutoPostBack="true" OnTextChanged="txtBillFliter_TextChanged" />   
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocsId") %>','<%# Eval("numBillID") %>','<%# Eval("tintBillType") %>');">
                                                    <asp:Label ID="lblBizDocName" runat="server" Text='<%#Eval("Name")%>'></asp:Label>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                 <asp:LinkButton id="lnkBizDocStatusFliter" runat="server" Text="BizDoc Status" CommandName="Sort" CommandArgument="BizDocStatus"></asp:LinkButton>
                                                 <asp:TextBox ID="txtBizDocStatusFliter" runat="server" AutoPostBack="true" OnTextChanged="txtBizDocStatusFliter_TextChanged" />   
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBizDocStatus" Text="NA" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Reference #" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                              <HeaderTemplate>
                                                 <asp:LinkButton id="lnkReferenceFliter" runat="server" Text="Reference #" CommandName="Sort" CommandArgument="Reference"></asp:LinkButton>
                                                 <asp:TextBox ID="txtReferenceFliter" runat="server" AutoPostBack="true" OnTextChanged="txtReferenceFliter_TextChanged" />   
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# DataBinder.Eval(Container, "DataItem.Reference")%>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                            <FooterTemplate>
                                                Totals:
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bill Amount" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                             <HeaderTemplate>
                                                 <asp:LinkButton id="lnkBillAmountFliter" runat="server" Text="Bill Amount" CommandName="Sort" CommandArgument="monOriginalAmount"></asp:LinkButton>
                                                 <asp:TextBox ID="txtBillAmountFliter" runat="server" AutoPostBack="true" OnTextChanged="txtBillAmountFliter_TextChanged" />   
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBillAmount" CssClass="lblBillAmount" runat="server" Text='<%# ReturnMoney(Eval("monOriginalAmount")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <FooterTemplate>
                                                <asp:Label Text="" runat="server" ID="lblFooterTotalBillAmt" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:TemplateColumn HeaderText="Amt Due" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                              <HeaderTemplate>
                                                 <asp:LinkButton id="lnkAmtDueFliter" runat="server" Text="Amt Due" CommandName="Sort" CommandArgument="monAmountDue"></asp:LinkButton>
                                                 <asp:TextBox ID="txtAmtDueFliter" runat="server" AutoPostBack="true" OnTextChanged="txtAmtDueFliter_TextChanged" />   
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountDue" CssClass="lblAmountDue" runat="server" Text='<%# ReturnMoney(Eval("monAmountDue")) %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <FooterTemplate>
                                                <asp:Label Text="" runat="server" ID="lblFooterTotalDue" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                            <HeaderTemplate>
                                                <asp:LinkButton id="lnkAmttoPayFliter" runat="server" Text="Amt to Pay" CommandName="Sort" CommandArgument="monAmountPaid"></asp:LinkButton>
                                                 <asp:TextBox ID="txtAmttoPayFliter" runat="server" AutoPostBack="true" OnTextChanged="txtAmttoPayFliter_TextChanged" />   
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" Width="100%" ID="txtAmountToPay" CssClass="txtAmountToPay  form-control"
                                                    Text='<%# ReturnMoney(Eval("monAmountPaid")) %>' Style="text-align: right;" AutoComplete="OFF"
                                                    onkeypress="CheckNumber(1,event)"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right"  Width="5%" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <FooterTemplate>
                                                <asp:Label Text="" runat="server" ID="lblFooterTotalPayment" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
     
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure, you want to delete bill payment entry?')"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
            <div class="pull-right">
                <div class="table-responsive">
                    <table style="width: 360px" class="table table-bordered">
                        <tr>
                            <td style="white-space: nowrap; font-weight: bold; text-align: right">Payment Method:</td>
                            <td>
                                <asp:DropDownList ID="ddlPaymentMethod" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="class_check">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:RadioButton ID="rbPrintedCheck" Text="Printed check" runat="server" GroupName="Check" Checked="true" />
                                            <asp:RadioButton ID="rbHandCheck" Text="Hand written check" runat="server" GroupName="Check" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Starting Check #:</td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtCheckNo" CssClass="form-control" onkeypress="CheckNumber(2,event)" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trAmount" runat="server" visible="false">
                            <td style="white-space: nowrap; font-weight: bold; text-align: right">Amount:</td>
                            <td>
                                <asp:TextBox ID="txtAmount" runat="server" AutoComplete="off" onkeypress="CheckNumber(1,event);"
                                    CssClass="form-control required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap; font-weight: bold; text-align: right">Payment Date:</td>
                            <td>
                                <BizCalendar:Calendar ID="calPaymentDate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap; font-weight: bold; text-align: right">Payment Total:</td>
                            <td>
                                <asp:Label ID="lblPaymentTotalAmount" runat="server" Text="0.00" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap; font-weight: bold; text-align: right">Amount Overpaid:</td>
                            <td>
                                <asp:Label ID="lblOverPaid" runat="server" Text="0.00"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="2">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary button" Text="Pay Bills"></asp:Button>
                                <asp:Button ID="btnExportCheck" runat="server" CssClass="btn btn-primary button" Text="Pay and Print.."></asp:Button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
    <asp:HiddenField ID="hdnPaymentTotalAmt" runat="server" />
    <asp:HiddenField ID="hfCheckNo" runat="server" Value="0" />
    <asp:HiddenField ID="hfCheckHeaderID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnBillPaymentID" runat="server" />
    <asp:HiddenField ID="hdnReturnHeaderID" runat="server" />
    <asp:HiddenField ID="hdnPaymentAmount" runat="server" Value="0" />
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" OnClick="btnGo1_Click1" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none" Text="0"></asp:TextBox>
    <asp:HiddenField ID="hdnRefundAmount" runat="server" Value="0" />

     <asp:HiddenField ID="hdnVendorFliter" runat="server" />
    <asp:HiddenField ID="hdnBizDocStatusFliter" runat="server" />
    <asp:HiddenField ID="hdnReferenceFliter" runat="server" />
    <asp:HiddenField ID="hdnBillAmountFliter" runat="server" />
    <asp:HiddenField ID="hdnAmtDueFliter" runat="server" />
    <asp:HiddenField ID="hdnAmttoPayFliter" runat="server" />
    <asp:HiddenField ID="hdnBillFliter" runat="server" />
     <asp:HiddenField ID="hdnFromDateFilter" runat="server" />
    <asp:HiddenField ID="hdnToDateFilter" runat="server" />
    <asp:HiddenField ID="hdnSelectedBills" runat="server" />

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
