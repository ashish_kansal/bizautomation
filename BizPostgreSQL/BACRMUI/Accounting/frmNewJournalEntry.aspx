<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewJournalEntry.aspx.vb"
    Inherits=".frmNewJournalEntry" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" EnableViewState="true" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static"
    EnableViewState="true">
    <title>New Journal Entry</title>
    <script type="text/javascript" src="../JavaScript/Common.js"></script>
    <script type="text/javascript">
        var GridID = 'ctl00_ctl00_MainContent_GridPlaceHolder_gvJournalEntry';

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            CalculateDebitValue();
            CalculateCreditValue();
        }

        function Validate() {
            if (CalculateDebitValue() == false || CalculateCreditValue() == false)
                return false;

            if (document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_calfrom_txtDate').value == 0) {
                alert("Enter Journal Entry Date")
                document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_calfrom_txtDate').focus();
                return false;
            }

            var lbltotalDebit = $("#lblTotDebitValue1").text();
            var lbltotalCredit = $("#lblTotCreditValue1").text();

            lbltotalDebit = lbltotalDebit.replace(/,/g, "");
            lbltotalCredit = lbltotalCredit.replace(/,/g, "");
            var err = 0;

            $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvJournalEntry tr').not(':first,:last').each(function () {
                txtDebit = $(this).find("input[id*='txtDebit']").val();
                txtCredit = $(this).find("input[id*='txtCredit']").val();
                ddlAccounts = $(this).find("[id*='ddlAccounts']");

                if ((txtDebit != "" && txtDebit != undefined) || (txtCredit != "" && txtCredit != undefined)) {
                    if (parseFloat(txtDebit) > 0 || parseFloat(txtCredit) > 0) {
                        if ($(ddlAccounts).val() == 0) {
                            err = 1;
                            return false;
                        }

                        var AcntTypeId = $(ddlAccounts).val().split("~");

                        //lblAccountTypeId = $(this).find("[id*='lblAccountTypeId']");
                        //$(lblAccountTypeId).val(AcntTypeId[1]);

                        if ((AcntTypeId[1] == '01010105' || AcntTypeId[1] == '01020102')
                            && $find($(ddlAccounts).attr('id').replace('ddlAccounts', 'radCmbCompany')).get_value() == '') {

                            if (AcntTypeId[1] == '01010105')
                                err = 2;
                            else if (AcntTypeId[1] == '01020102')
                                err = 3;

                            return false;
                        }
                        else if ((AcntTypeId[1] == '0101010501' || AcntTypeId[1] == '0102010201')
                            && $find($(ddlAccounts).attr('id').replace('ddlAccounts', 'radCmbCompany')).get_value() != '') {

                            err = 4;

                            return false;
                        }
                    }
                }
            });

            if (err == 1) {
                alert('You must select an account for each split line with an amount');
                return false;
            }
            else if (err == 2) {
                alert('When you use Accounts Receivable, You must select an Customer in Name Field for each split line with an amount');
                return false;
            }
            else if (err == 3) {
                alert('When you use Accounts Payable, You must select an Vendor in Name Field for each split line with an amount');
                return false;
            }
            else if (err == 4) {
                alert("Do not select the Vendor/Customer Name in dropdown 'Name' as this transaction is for clearing of opening balance.If you select the vendor/Customer name, It will imbalance the Aging report.");
                return false;
            }

            if (lbltotalDebit == '' && lbltotalCredit == '') {
                alert("Please enter at least two lines.");
                return false;
            }

            lbltotalDebit = parseFloat(lbltotalDebit).toFixed(2);
            lbltotalCredit = parseFloat(lbltotalCredit).toFixed(2);

            if (lbltotalDebit != lbltotalCredit) {
                alert("The Credit and Debit amounts you�re entering must be of the same amount");
                return false;
            }

            if (lbltotalDebit <= 0 && lbltotalCredit <= 0) {
                alert("Journal value must be greater than zero");
                return false;
            }

            if (CheckIsReconciled())
                return true;
            else
                return false;

            return true;
        }

        function SetDebitCreditValue(obj) {
            var h = obj.id;
            var i;
            i = h.substring(40, 42);
            //console.log(GridID + '_ctl' + i + '_ddlAccounts');
            //console.log(h);
            //var AcntTypeId = $('#' + GridID + '_ctl' + i + '_ddlAccounts').val().split("~")

            //$('#' + GridID + '_ctl' + i + '_lblAccountTypeId').val(AcntTypeId[1]);
            if (ConditionCheck()) {
                var idtxtTail = 'txtDebit';
                var diff;
                var difference;
                var lbltotalDebit;
                var lbltotalCredit;

                var totalDebit = $("#lblTotDebitValue1").text();
                totalDebit = Number(totalDebit.replace(/,/g, ""));

                var totalCredit = $("#lblTotCreditValue1").text();
                totalCredit = Number(totalCredit.replace(/,/g, ""));

                if (totalDebit < totalCredit) {
                    if (($('#' + GridID + '_ctl' + i + '_txtCredit').val() == "") && ($('#' + GridID + '_ctl' + i + '_txtDebit').val() == "")) {
                        diff = parseFloat(totalCredit) - parseFloat(totalDebit);
                        $('#' + GridID + '_ctl' + i + '_txtDebit').val(parseFloat(diff).toFixed(2));

                        lbltotalDebit = parseFloat(totalDebit) + parseFloat(diff);
                        $('#lblTotDebitValue1').text(parseFloat(lbltotalDebit).toFixed(2));
                        $('#TotalDrValue').val(parseFloat(lbltotalDebit).toFixed(2));

                        $('#lblTotalDebit1').text($('#Currency').val());
                        $('#TotalDrText').val($('#Currency').val());
                    }
                }
                //alert(parseFloat(totalDebit));
                if (parseFloat(totalDebit) > parseFloat(totalCredit)) {
                    if (($('#' + GridID + '_ctl' + i + '_txtDebit').val() == "") && ($('#' + GridID + '_ctl' + i + '_txtCredit').val() == "")) {

                        difference = parseFloat(totalDebit) - parseFloat(totalCredit);
                        $('#' + GridID + '_ctl' + i + '_txtCredit').val(parseFloat(difference).toFixed(2));
                        lbltotalCredit = parseFloat(totalCredit) + parseFloat(difference);

                        $('#lblTotCreditValue1').text(parseFloat(lbltotalCredit).toFixed(2));
                        $('#TotalCrValue').val(parseFloat(lbltotalCredit).toFixed(2));

                        $('#lblTotalCredit1').text($('#Currency').val());
                        $('#TotalCrText').val($('#Currency').val());
                    }
                }

            }

        }

        function ConditionCheck() {
            if ($('#lblTotDebitValue1').text() != $('#lblTotCreditValue1').text())
                return true;
            else
                return false;
        }

        function CalculateDebitValue(obj) {
            // alert(obj.id);
            //alert($Ele('Currency').value);
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var CrValue;
            var CrTotValue;
            //alert($Ele('dgJournalEntry_ctl01_txtDebit').value);
            //alert($Ele('dgJournalEntry').rows.length);
            if (obj != null)
                $(obj).val(parseFloat($(obj).val()).toFixed(2));

            $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvJournalEntry tr').not(':first,:last').each(function () {

                var idtxtDebitvalue = $(this).find("input[id*='txtDebit']").val();

                if (idtxtDebitvalue != undefined && idtxtDebitvalue != '' && parseFloat(idtxtDebitvalue) > 0) {
                    CrValue = $(this).find("input[id*='txtCredit']").val();
                    CrValue = CrValue.replace(/,/g, "");

                    if (CrValue != "" && !isNaN(CrValue)) {
                        CrTotValue = $('#lblTotCreditValue1').text();
                        CrTotValue = CrTotValue.replace(/,/g, "");
                        CrTotValue -= CrValue;
                        $('#lblTotCreditValue1').text(parseFloat(CrTotValue).toFixed(2));
                        $(this).find("input[id*='txtCredit']").val('');
                    }

                    txtdebitvalue = $(this).find("input[id*='txtDebit']").val();
                    txtdebitvalue = txtdebitvalue.replace(/,/g, "");

                    itotal += parseFloat(txtdebitvalue);

                    if (parseFloat(txtdebitvalue) < 0) {
                        alert("Each Debit/Credit amount must be greater than Zero.");
                        return false;
                    }
                }
            });

            $('#lblTotalDebit1').text($('#Currency').val());
            $('#TotalDrText').val($('#Currency').val());

            formatDigit = parseFloat(itotal).toFixed(2);

            $('#lblTotDebitValue1').text(formatDigit);
            $('#TotalDrValue').val(formatDigit);
            //  alert(itotal);
            // alert($Ele('lblTotDebitValue').value);
            return true;
        }

        function CalculateCreditValue(obj) {
            // alert(obj.id);

            var itotal = 0.0;
            var formatDigit;
            var txtCreditValue;
            var idtxtCreditvalue;
            var DrValue;
            var DrTotValue;
            if (obj != null)
                $(obj).val(parseFloat($(obj).val()).toFixed(2));

            $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvJournalEntry tr').not(':first,:last').each(function () {

                var idtxtCreditvalue = $(this).find("input[id*='txtCredit']").val();

                if (idtxtCreditvalue != undefined && idtxtCreditvalue != '' && parseFloat(idtxtCreditvalue) > 0) {
                    idtxtCreditvalue = idtxtCreditvalue.replace(/,/g, "");

                    DrValue = $(this).find("input[id*='txtDebit']").val();
                    DrValue = DrValue.replace(/,/g, "");

                    if (DrValue != "" && !isNaN(DrValue)) {
                        DrTotValue = $('#lblTotDebitValue1').text();
                        DrTotValue = DrTotValue.replace(/,/g, "");
                        DrTotValue -= DrValue;

                        $('#lblTotDebitValue1').text(parseFloat(DrTotValue).toFixed(2));
                        $(this).find("input[id*='txtDebit']").val('');
                    }

                    txtCreditValue = $(this).find("input[id*='txtCredit']").val();
                    txtCreditValue = txtCreditValue.replace(/,/g, "");

                    itotal += parseFloat(txtCreditValue);

                    if (parseFloat(txtCreditValue) < 0) {
                        alert("Each Debit/Credit amount must be greater than Zero.");
                        return false;
                    }
                }
            });

            $('#lblTotalCredit1').text($('#Currency').val());
            $('#TotalCrText').val($('#Currency').val());

            formatDigit = parseFloat(itotal).toFixed(2);

            $('#lblTotCreditValue1').text(formatDigit);
            $('#TotalCrValue').val(formatDigit);

            return true;
            //alert($Ele('lblTotDebitValue').value);
        }

        function DeleteRecord() {
            var str;
            str = 'Are you sure, you want to delete the selected record?'
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="view-cases">
        </div>
        <div class="right-input">
            <table align="right">
                <tr>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-6 col-md-offset-3">
        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Journal Entry&nbsp;<a href="#" onclick="return OpenHelpPopUp('accounting/frmnewjournalentry.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvJournalEntry" runat="server" BorderWidth="0" CssClass="table table-striped table-bordered"
                    AutoGenerateColumns="False" Style="margin-right: 0px" Width="100%" ShowFooter="true"
                    ClientIDMode="AutoID">
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Accounts">
                            <ItemTemplate>
                                <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# Eval("TransactionId") %>'></asp:Label>
                                <asp:Label ID="lblAccountID" runat="server" Style="display: none" Text='<%# Eval("numChartAcntId") %>'></asp:Label>
                                <asp:DropDownList ID="ddlAccounts" onchange="SetDebitCreditValue(this);"
                                    runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" OnClick="btnAdd_Click" Style="float: left"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add more row(s)</asp:LinkButton>Total:
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnAdd" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </FooterTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Debit">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDebit" runat="server" onchange="CalculateDebitValue(this);"
                                    AutoComplete="OFF" CssClass="money form-control" Text='<%# ReturnMoney(Eval("numDebitAmt"))%>'></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />
                            <FooterTemplate>
                                <asp:Label ID="lblTotalDebit1" CssClass="signup" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:Label CssClass="signup" ID="lblTotDebitValue1" runat="server" ClientIDMode="Static"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCredit" runat="server" onchange="CalculateCreditValue(this);"
                                    AutoComplete="OFF" CssClass="form-control money" Text='<%# ReturnMoney(Eval("numCreditAmt"))%>'></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />
                            <FooterTemplate>
                                <asp:Label ID="lblTotalCredit1" CssClass="signup" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:Label ID="lblTotCreditValue1" CssClass="signup" runat="server" ClientIDMode="Static"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Memo">
                            <ItemTemplate>
                                <asp:Label ID="lblMemo" runat="server" Style="display: none"></asp:Label>
                                <asp:TextBox ID="txtMemo" TextMode="SingleLine" runat="server" CssClass="form-control"
                                    Text='<%# Eval("varDescription") %>'></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Style="display: none" Text='<%# Eval("numCustomerId") %>'></asp:Label>
                                <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" DropDownWidth="300px"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True"
                                    ClientIDMode="AutoID">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class<span style='display:none'>CLS<span>">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project<span style='display:none'>PRJT<span>">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlProject" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Bottom" />

                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="DeleteRow"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Narration:</label>
                    <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="txtDescription" Width="400" />
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" class="pull-right">
                <ContentTemplate>
                    <asp:LinkButton ID="btnNew" runat="server" CssClass="btn btn-primary" Text="" OnClientClick='document.location.href="frmNewJournalEntry.aspx";return false;'>
                <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Journal
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnSaveNew" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & New</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text=""><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
                    </asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnNew" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="btnSaveNew" />
                    <asp:PostBackTrigger ControlID="btnBack" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <asp:Button ID="btnTreeView" runat="server" Style="display: none"></asp:Button>
    <asp:Button ID="btnFixedAssetView" runat="server" Style="display: none" />
    <asp:HiddenField ID="TotalDrValue" runat="server" />
    <asp:HiddenField ID="TotalCrValue" runat="server" />
    <asp:HiddenField ID="TotalDrText" runat="server" />
    <asp:HiddenField ID="TotalCrText" runat="server" />
    <asp:HiddenField ID="hdnBizDocsPaymentDetId" runat="server" />
    <asp:Button runat="server" ID="btnTabs" Style="display: none" />
    <asp:Button runat="server" ID="btnBankRegister" Style="display: none" />
    <asp:HiddenField ID="Currency" runat="server" />
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ContentPlaceHolderID="GridSettingPopup" runat="server">
    <div class="form-inline">
        <label>Date:</label>
        <BizCalendar:Calendar ID="calfrom" runat="server" ClientIDMode="AutoID" />
        <label>Ref#:</label>
        <asp:TextBox ID="txtJournalReferenceNo" CssClass="form-control" runat="server" AutoComplete="OFF" MaxLength="12"></asp:TextBox>
    </div>
</asp:Content>
