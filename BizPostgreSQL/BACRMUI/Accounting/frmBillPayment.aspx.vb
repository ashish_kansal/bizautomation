﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports

Partial Public Class frmBillPayment
    Inherits BACRMPage

    Dim lngProjectID, lngStageID As Long
    Dim lngDivisionID As Long
    Dim bError As Boolean = False
    Dim lngBizDocsPaymentDetId As Long
    Dim objCommon As New CCommon

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngProjectID = CCommon.ToLong(GetQueryStringVal("ProID"))
            lngStageID = CCommon.ToLong(GetQueryStringVal("StageID"))
            lngDivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
            lngBizDocsPaymentDetId = CCommon.ToLong(GetQueryStringVal("BizDocsPaymentDetId"))
            If GetQueryStringVal("StageName") <> "" Then
                lblTitle.Text = "Enter Bill for Stage (" & GetQueryStringVal("StageName") & ")"
                txtReference.Text = "Stage: " & GetQueryStringVal("StageName")
            End If

            If Not IsPostBack Then

                If GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then
                    If GetQueryStringVal("uihTR") <> "" Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                    End If
                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany As String
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                End If

                objCommon.sb_FillComboFromDB(ddlTerms, 296, Session("DomainID"))
                objCommon.sb_FillComboFromDB(ddlPayment, 31, Session("DomainID"))
                If Not ddlPayment.Items.FindByValue(5) Is Nothing Then
                    ddlPayment.Items.FindByValue(5).Selected = True
                End If
                If lngDivisionID > 0 Then
                    objCommon.DivisionID = lngDivisionID
                    radCmbCompany.Text = objCommon.GetCompanyName
                    radCmbCompany.SelectedValue = lngDivisionID
                End If
                BindExpencesAndLiability()
                'BindProject()
                SetInitialRow()

                If lngBizDocsPaymentDetId > 0 Then
                    LoadBillDetails()
                End If
            End If

            'ClientScript.RegisterStartupScript(Me.GetType, "var duedateChage = window.validateDate;window.validateDate = function () {duedateChage('" & CCommon.GetValidationDateFormat() & "');}", True)
            calBillDate.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            calBillDate.Attributes.Add("onclick", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            ddlTerms.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            hfDateFormat.Value = Session("DateFormat").ToString().ToLower().Replace("m", "M").Replace("Month", "MMM")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindExpencesAndLiability()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            Dim item As ListItem
            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlLiabilityAccount.Items.Add(item)
            Next
            ddlLiabilityAccount.Items.Insert(0, New ListItem("--Select One--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindProject(ByVal ddlProject As DropDownList)
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = objProject.GetOpenProject()
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadBillDetails()
        Try
            Dim objOppInvoice As New OppInvoice
            objOppInvoice.DomainID = Session("DomainId")
            objOppInvoice.BizDocsPaymentDetId = lngBizDocsPaymentDetId
            Dim dtOppBizDocsDetails As DataTable = objOppInvoice.GetOpportunityBizDocsDetails()
            If dtOppBizDocsDetails.Rows.Count > 0 Then
                btnSaveClose.Visible = False
                btnSaveNew.Visible = False
                radBill.Enabled = False
                radLiability.Enabled = False
                radCmbCompany.Enabled = False
                ddlLiabilityAccount.Enabled = False
                lblTitle.Text = "View Bill"
                gvBillItems.ShowFooter = False

                gvBillItems.Columns(7).Visible = False
                ddlPayment.ClearSelection()
                If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("numPaymentMethod")) Then
                    If Not ddlPayment.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numPaymentMethod")) Is Nothing Then
                        ddlPayment.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numPaymentMethod")).Selected = True
                    End If
                End If

                If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("vcReference")) Then
                    txtReference.Text = dtOppBizDocsDetails.Rows(0).Item("vcReference")
                End If

                If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("numTermsID")) Then
                    If Not ddlTerms.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numTermsID")) Is Nothing Then
                        ddlTerms.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numTermsID")).Selected = True
                    End If
                End If

                calBillDate.SelectedDate = dtOppBizDocsDetails.Rows(0).Item("dtBillDate")
                calDueDate.SelectedDate = dtOppBizDocsDetails.Rows(0).Item("dtDueDate")

                If dtOppBizDocsDetails.Rows(0).Item("tintPaymentType") = 2 Then
                    radBill.Checked = True
                    objCommon.DivisionID = dtOppBizDocsDetails.Rows(0).Item("numDivisionID")
                    radCmbCompany.Text = objCommon.GetCompanyName
                    radCmbCompany.SelectedValue = dtOppBizDocsDetails.Rows(0).Item("numDivisionID")
                ElseIf dtOppBizDocsDetails.Rows(0).Item("tintPaymentType") = 4 Then
                    radLiability.Checked = True
                    If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("numLiabilityAccount")) Then
                        If Not ddlLiabilityAccount.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numLiabilityAccount")) Is Nothing Then
                            ddlLiabilityAccount.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numLiabilityAccount")).Selected = True
                        End If
                    End If
                End If

                'If Not dtOppBizDocsDetails.Rows(0)("numClassID") Is Nothing Then
                '    If Not ddlClass.Items.FindByValue(dtOppBizDocsDetails.Rows(0)("numClassID")) Is Nothing Then
                '        ddlClass.ClearSelection()
                '        ddlClass.Items.FindByValue(dtOppBizDocsDetails.Rows(0)("numClassID")).Selected = True
                '    End If
                'End If

                'If Not dtOppBizDocsDetails.Rows(0)("numProjectID") Is Nothing Then
                '    If Not ddlProject.Items.FindByValue(dtOppBizDocsDetails.Rows(0)("numProjectID")) Is Nothing Then
                '        ddlProject.ClearSelection()
                '        ddlProject.Items.FindByValue(dtOppBizDocsDetails.Rows(0)("numProjectID")).Selected = True
                '    End If
                'End If


                'Get Journalid from PaymentDetailID
                objCommon.DomainID = Session("DomainID")
                objCommon.Str = lngBizDocsPaymentDetId
                objCommon.Mode = 1

                'Get List Of JournalEntries
                Dim objJE As New JournalEntry
                objJE.DomainID = Session("DomainID")
                objJE.JournalId = CCommon.ToLong(objCommon.GetSingleFieldValue())
                Dim dtJournals, dt As New DataTable
                Dim drCurrentRow As DataRow
                Dim dtCurrentTable As New DataTable()

                dt = objJE.GetJournalEntryDetails()

                dtCurrentTable.Columns.Add(New DataColumn("RowNumber", GetType(String)))
                dtCurrentTable.Columns.Add(New DataColumn("ExpenseAccount", GetType(String)))
                dtCurrentTable.Columns.Add(New DataColumn("Amount", GetType(String)))
                dtCurrentTable.Columns.Add(New DataColumn("Memo", GetType(String)))
                dtCurrentTable.Columns.Add(New DataColumn("Project", GetType(Long)))
                dtCurrentTable.Columns.Add(New DataColumn("Class", GetType(Long)))
                dtCurrentTable.Columns.Add(New DataColumn("Campaign", GetType(Long)))

                If dt.Select(" numDebitAmt>0 ", "").Count > 0 Then
                    dtJournals = dt.Select("numDebitAmt>0", "").CopyToDataTable()
                End If

                For i As Integer = 0 To dtJournals.Rows.Count - 1
                    drCurrentRow = dtCurrentTable.NewRow()
                    drCurrentRow("RowNumber") = i + 1
                    drCurrentRow("ExpenseAccount") = dtJournals.Rows(i)("numChartAcntId").ToString.Split("~")(0)
                    drCurrentRow("Amount") = dtJournals.Rows(i)("numDebitAmt")
                    drCurrentRow("Memo") = dtJournals.Rows(i)("varDescription")

                    drCurrentRow("Project") = CCommon.ToLong(dtJournals.Rows(i)("numProjectID"))
                    drCurrentRow("Class") = CCommon.ToLong(dtJournals.Rows(i)("numClassID"))
                    drCurrentRow("Campaign") = CCommon.ToLong(dtJournals.Rows(i)("numCampaignID"))

                    dtCurrentTable.Rows.Add(drCurrentRow)
                Next

                ViewState("CurrentTable") = dtCurrentTable
                gvBillItems.DataSource = dtCurrentTable
                gvBillItems.DataBind()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            If bError = False Then
                If lngStageID > 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> if (window.opener != null) {window.opener.location.reload(true);Close();}</script>")
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script>Close();</script>")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function GetTotalAmount() As Decimal
        Try
            Dim decTotal As Decimal = 0
            Dim gridRow As GridViewRow
            For i As Integer = 0 To gvBillItems.Rows.Count - 1
                gridRow = gvBillItems.Rows(i)
                Dim decBillAmt As Decimal = Math.Abs(CCommon.ToDecimal(Replace(DirectCast(gridRow.FindControl("txtAmount"), TextBox).Text, ",", "")))
                If decBillAmt <= 0 Then
                    bError = True
                    litMessage.Visible = True
                    litMessage.Attributes.Add("class", "warning")
                    litMessage.Text = "Bill amount must be greater than Zero."
                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "fade", " $('#litMessage').fadeIn().delay(5000).fadeOut();", True)
                    Exit Function
                End If
                decTotal = decTotal + decBillAmt
            Next
            Return decTotal
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub Save()
        Try
            Dim dt As New DataTable

            Dim lobjAuthoritativeBizDocs As New AuthoritativeBizDocs
            Dim lngJournalID As Long
            Dim ExchangeRate As Double = 1
            Dim decAmount As Decimal = GetTotalAmount()
            Dim objOppInvoice As New OppInvoice

            If bError = True Then
                Exit Sub
            End If


            If radBill.Checked = True Then
                If lngProjectID > 0 And lngStageID > 0 Then
                    Dim objTimeExp As New TimeExpenseLeave
                    Dim dtTimeDetails As DataTable

                    objTimeExp.DomainID = Session("DomainID")
                    objTimeExp.ProID = lngProjectID
                    objTimeExp.StageId = lngStageID

                    dtTimeDetails = objTimeExp.GetTimeAndExpense_BudgetTotal()

                    If dtTimeDetails.Rows.Count <> 0 Then
                        If dtTimeDetails.Rows(0).Item("bitExpenseBudget") = True Then
                            If (decAmount > dtTimeDetails.Rows(0).Item("TotalExpense")) Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You’ve exceeded the amount budgeted for this stage.' );", True)
                                bError = True
                                Exit Sub
                            End If
                            'Else
                            '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please set Expense Budget for this stage.' );", True)
                            '    bError = True
                            '    Exit Sub
                        End If
                    End If
                End If
            End If

            objOppInvoice.AmtPaid = decAmount
            objOppInvoice.UserCntID = Session("UserContactID")
            objOppInvoice.PaymentMethod = ddlPayment.SelectedValue
            objOppInvoice.Reference = txtReference.Text
            'objOppInvoice.Memo = txtMemo.Text
            objOppInvoice.DomainID = Session("DomainId")
            'objOppInvoice.ExpenseAcount = ddlExpenseAccount.SelectedValue
            objOppInvoice.DivisionID = IIf(radBill.Checked, radCmbCompany.SelectedValue, 0)
            objOppInvoice.LiabilityAccount = ddlLiabilityAccount.SelectedValue
            If calDueDate.SelectedDate <> "" Then
                objOppInvoice.DueDate = calDueDate.SelectedDate
            Else
                objOppInvoice.DueDate = Now
            End If
            objOppInvoice.CurrencyID = Session("BaseCurrencyID")
            objOppInvoice.PaymentType = IIf(radBill.Checked, OppInvoice.BillPaymentType.Bill, OppInvoice.BillPaymentType.BillAgainstLiability)
            'objOppInvoice.ClassID = ddlClass.SelectedValue
            'objOppInvoice.ProjectID = CCommon.ToLong(ddlProject.SelectedValue)
            objOppInvoice.Terms = ddlTerms.SelectedValue
            objOppInvoice.BillDate = If(calBillDate.SelectedDate <> "", calBillDate.SelectedDate, Now)

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                ExchangeRate = objOppInvoice.AddBillDetails()
                lngJournalID = SaveDataToHeader(decAmount, objOppInvoice.BizDocsPaymentDetId)
                SaveDataToGeneralJournalDetails(lobjAuthoritativeBizDocs, lngJournalID, ExchangeRate, decAmount, objOppInvoice)

                objTransactionScope.Complete()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_BizDocsPaymentDetId As Integer) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            Dim lngJournalId As Long

            With objJEHeader
                .JournalId = lngJournalId
                .RecurringId = 0
                .EntryDate = CDate(calDueDate.SelectedDate & " 12:00:00")
                .Description = "Bill Payment"
                .Amount = p_Amount
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .BizDocsPaymentDetId = p_BizDocsPaymentDetId
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function SaveDataToGeneralJournalDetails(ByVal lobjAuthoritativeBizDocs As AuthoritativeBizDocs, ByVal lngJournalID As Long, ByVal ExchangeRate As Double, ByVal decAmount As Decimal, ByVal objOppInvoice As OppInvoice) As String
        Try
            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()

            If radBill.Checked = True Then

                'For Account Payable as Credit Entry in Journal
                Dim objOppBizDocs As New OppBizDocs
                Dim lngAPAccountId As Long = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), radCmbCompany.SelectedValue)
                If lngAPAccountId = 0 Then
                    lngAPAccountId = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                End If

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = decAmount * ExchangeRate
                objJE.ChartAcntId = lngAPAccountId
                objJE.Description = "Bill Payment"
                objJE.CustomerId = radCmbCompany.SelectedValue
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP" 'Account Payable
                objJE.Reference = ""
                objJE.PaymentMethod = ddlPayment.SelectedValue
                objJE.Reconcile = False
                objJE.CurrencyID = Session("BaseCurrencyID")
                objJE.FltExchangeRate = ExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            ElseIf radLiability.Checked = True Then


                'for more information refere bug id 814
                'Debit Expense Account
                'Credit Liability Account

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = decAmount * ExchangeRate
                objJE.ChartAcntId = ddlLiabilityAccount.SelectedValue
                objJE.Description = "Bill Payment"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP" 'Account Payable
                objJE.Reference = ""
                objJE.PaymentMethod = ddlPayment.SelectedValue
                objJE.Reconcile = False
                objJE.CurrencyID = Session("BaseCurrencyID")
                objJE.FltExchangeRate = ExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            End If

            For i As Integer = 0 To gvBillItems.Rows.Count - 1

                Dim gridRow As GridViewRow = gvBillItems.Rows(i)
                Dim ddlExpenseAccount As DropDownList = DirectCast(gridRow.Cells(1).FindControl("ddlExpenseAccount"), DropDownList)
                Dim txtAmount As TextBox = DirectCast(gridRow.FindControl("txtAmount"), TextBox)
                Dim txtMemo As TextBox = DirectCast(gridRow.FindControl("txtMemo"), TextBox)

                Dim ddlProject As DropDownList = DirectCast(gridRow.FindControl("ddlProject"), DropDownList)
                Dim ddlClass As DropDownList = DirectCast(gridRow.FindControl("ddlClass"), DropDownList)
                Dim ddlCampaign As DropDownList = DirectCast(gridRow.FindControl("ddlCampaign"), DropDownList)

                Dim decExpenseAmount As Decimal = Math.Abs(CCommon.ToDecimal(Replace(txtAmount.Text, ",", "")))

                If ddlExpenseAccount.SelectedValue > 0 And decExpenseAmount > 0 Then
                    If radBill.Checked = True Then

                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decExpenseAmount * ExchangeRate
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = ddlExpenseAccount.SelectedValue
                        objJE.Description = txtMemo.Text
                        objJE.CustomerId = CCommon.ToLong(radCmbCompany.SelectedValue)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "S"
                        objJE.Reference = ""
                        objJE.PaymentMethod = ddlPayment.SelectedValue
                        objJE.Reconcile = False
                        objJE.CurrencyID = Session("BaseCurrencyID")
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = ddlProject.SelectedValue
                        objJE.ClassID = ddlClass.SelectedValue
                        objJE.CampaignID = ddlCampaign.SelectedValue
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)

                    ElseIf radLiability.Checked = True Then
                        'for more information refere bug id 814
                        'Debit Expense Account
                        'Credit Liability Account

                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decExpenseAmount * ExchangeRate
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = ddlExpenseAccount.SelectedValue
                        objJE.Description = txtMemo.Text
                        objJE.CustomerId = 0
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "S"
                        objJE.Reference = ""
                        objJE.PaymentMethod = ddlPayment.SelectedValue
                        objJE.Reconcile = False
                        objJE.CurrencyID = Session("BaseCurrencyID")
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = ddlProject.SelectedValue
                        objJE.ClassID = ddlClass.SelectedValue
                        objJE.CampaignID = ddlCampaign.SelectedValue
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If
                End If
            Next

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            Save()
            If bError = False Then
                radCmbCompany.Items.Clear()
                SetInitialRow()
                txtReference.Text = ""
                litMessage.Visible = True
                litMessage.Attributes.Add("class", "successInfo")
                litMessage.Text = "Bill is Created."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If radCmbCompany.SelectedValue <> "" Then
                Dim objOpp As New OppBizDocs
                If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save' );", True)
                    radCmbCompany.SelectedValue = ""
                    radCmbCompany.Text = ""
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub SetInitialRow()
        Try
            Dim dt As New DataTable()
            Dim dr As DataRow = Nothing

            dt.Columns.Add(New DataColumn("RowNumber", GetType(String)))
            dt.Columns.Add(New DataColumn("ExpenseAccount", GetType(String)))
            dt.Columns.Add(New DataColumn("Amount", GetType(String)))
            dt.Columns.Add(New DataColumn("Memo", GetType(String)))

            dt.Columns.Add(New DataColumn("Project", GetType(Long)))
            dt.Columns.Add(New DataColumn("Class", GetType(Long)))
            dt.Columns.Add(New DataColumn("Campaign", GetType(Long)))

            dr = dt.NewRow()
            dr("RowNumber") = 1
            dr("ExpenseAccount") = String.Empty
            dr("Amount") = String.Empty
            dr("Memo") = String.Empty
            dr("Project") = 0
            dr("Class") = 0
            dr("Campaign") = 0

            dt.Rows.Add(dr)
            'Store the DataTable in ViewState
            ViewState("CurrentTable") = dt
            gvBillItems.DataSource = dt
            gvBillItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim dtChartAcntDetails As DataTable

    Private Sub gvBillItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBillItems.RowCommand
        Try
            If e.CommandName = "DeleteRow" Then
                If ViewState("CurrentTable") IsNot Nothing Then
                    Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
                    Dim rowIndex As Integer = 0
                    For i As Integer = 1 To dtCurrentTable.Rows.Count
                        'extract the TextBox values
                        Dim ddlExpenseAccount As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(1).FindControl("ddlExpenseAccount"), DropDownList)
                        Dim txtAmount As TextBox = DirectCast(gvBillItems.Rows(rowIndex).Cells(2).FindControl("txtAmount"), TextBox)
                        Dim txtMemo As TextBox = DirectCast(gvBillItems.Rows(rowIndex).Cells(3).FindControl("txtMemo"), TextBox)
                        Dim ddlProject As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(4).FindControl("ddlProject"), DropDownList)
                        Dim ddlClass As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(5).FindControl("ddlClass"), DropDownList)
                        Dim ddlCampaign As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(6).FindControl("ddlCampaign"), DropDownList)

                        dtCurrentTable.Rows(i - 1)("ExpenseAccount") = ddlExpenseAccount.SelectedValue
                        dtCurrentTable.Rows(i - 1)("Amount") = txtAmount.Text
                        dtCurrentTable.Rows(i - 1)("Memo") = txtMemo.Text

                        dtCurrentTable.Rows(i - 1)("Project") = ddlProject.SelectedValue
                        dtCurrentTable.Rows(i - 1)("Class") = ddlClass.SelectedValue
                        dtCurrentTable.Rows(i - 1)("Campaign") = ddlCampaign.SelectedValue

                        rowIndex += 1
                    Next

                    dtCurrentTable.Rows(e.CommandArgument).Delete()
                    dtCurrentTable.AcceptChanges()

                    ViewState("CurrentTable") = dtCurrentTable
                    gvBillItems.DataSource = dtCurrentTable
                    gvBillItems.DataBind()

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvBillItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBillItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtAmount As TextBox = DirectCast(e.Row.FindControl("txtAmount"), TextBox)
                Dim ddlExpenseAccount As DropDownList = DirectCast(e.Row.FindControl("ddlExpenseAccount"), DropDownList)
                Dim ddlProject As DropDownList = DirectCast(e.Row.FindControl("ddlProject"), DropDownList)
                Dim ddlClass As DropDownList = DirectCast(e.Row.FindControl("ddlClass"), DropDownList)
                Dim ddlCampaign As DropDownList = DirectCast(e.Row.FindControl("ddlCampaign"), DropDownList)

                If dtChartAcntDetails Is Nothing Then
                    Dim objCOA As New ChartOfAccounting
                    objCOA.DomainID = Session("DomainId")
                    objCOA.AccountCode = "0104"
                    dtChartAcntDetails = objCOA.GetParentCategory()
                End If

                If ddlProject IsNot Nothing Then
                    BindProject(ddlProject)
                End If

                If ddlClass IsNot Nothing Then
                    CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))
                End If

                If ddlCampaign IsNot Nothing Then
                    If objCommon Is Nothing Then
                        objCommon = New CCommon
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 18, CCommon.ToLong(Session("DomainID")))
                End If

                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlExpenseAccount.Items.Add(item)
                Next
                ddlExpenseAccount.Items.Insert(0, New ListItem("--Select One --", "0"))

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("ExpenseAccount")) > 0 Then
                    If Not ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("ExpenseAccount"))) Is Nothing Then
                        ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("ExpenseAccount"))).Selected = True
                    End If
                End If

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Project")) > 0 Then
                    If Not ddlProject.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Project"))) Is Nothing Then
                        ddlProject.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Project"))).Selected = True
                    End If
                End If

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Class")) > 0 Then
                    If Not ddlClass.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Class"))) Is Nothing Then
                        ddlClass.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Class"))).Selected = True
                    End If
                End If

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Campaign")) > 0 Then
                    If Not ddlCampaign.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Campaign"))) Is Nothing Then
                        ddlCampaign.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("Campaign"))).Selected = True
                    End If
                End If

                Dim btnDeleteRow As Button = DirectCast(e.Row.FindControl("btnDeleteRow"), Button)

                If btnDeleteRow IsNot Nothing Then
                    If e.Row.RowIndex = 0 Then
                        btnDeleteRow.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub AddNewRowToGrid()
        Try
            Dim rowIndex As Integer = 0
            If ViewState("CurrentTable") IsNot Nothing Then
                Dim dtCurrentTable As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
                Dim drCurrentRow As DataRow = Nothing
                If dtCurrentTable.Rows.Count > 0 Then
                    For i As Integer = 1 To dtCurrentTable.Rows.Count
                        'extract the TextBox values
                        Dim ddlExpenseAccount As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(1).FindControl("ddlExpenseAccount"), DropDownList)
                        Dim txtAmount As TextBox = DirectCast(gvBillItems.Rows(rowIndex).Cells(2).FindControl("txtAmount"), TextBox)
                        Dim txtMemo As TextBox = DirectCast(gvBillItems.Rows(rowIndex).Cells(3).FindControl("txtMemo"), TextBox)
                        Dim ddlProject As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(4).FindControl("ddlProject"), DropDownList)
                        Dim ddlClass As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(5).FindControl("ddlClass"), DropDownList)
                        Dim ddlCampaign As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(6).FindControl("ddlCampaign"), DropDownList)

                        drCurrentRow = dtCurrentTable.NewRow()
                        drCurrentRow("RowNumber") = i + 1
                        dtCurrentTable.Rows(i - 1)("ExpenseAccount") = ddlExpenseAccount.SelectedValue
                        dtCurrentTable.Rows(i - 1)("Amount") = txtAmount.Text
                        dtCurrentTable.Rows(i - 1)("Memo") = txtMemo.Text

                        dtCurrentTable.Rows(i - 1)("Project") = ddlProject.SelectedValue
                        dtCurrentTable.Rows(i - 1)("Class") = ddlClass.SelectedValue
                        dtCurrentTable.Rows(i - 1)("Campaign") = ddlCampaign.SelectedValue

                        rowIndex += 1
                    Next

                    dtCurrentTable.Rows.Add(drCurrentRow)
                    ViewState("CurrentTable") = dtCurrentTable
                    gvBillItems.DataSource = dtCurrentTable
                    gvBillItems.DataBind()
                End If
            Else
                Response.Write("ViewState is null")
            End If
            'Set Previous Data on Postbacks
            SetPreviousData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetPreviousData()
        Try
            Dim rowIndex As Integer = 0
            If ViewState("CurrentTable") IsNot Nothing Then
                Dim dt As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
                If dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        Dim ddlExpenseAccount As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(1).FindControl("ddlExpenseAccount"), DropDownList)
                        Dim txtAmount As TextBox = DirectCast(gvBillItems.Rows(rowIndex).Cells(2).FindControl("txtAmount"), TextBox)
                        Dim txtMemo As TextBox = DirectCast(gvBillItems.Rows(rowIndex).Cells(3).FindControl("txtMemo"), TextBox)
                        Dim ddlProject As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(4).FindControl("ddlProject"), DropDownList)
                        Dim ddlClass As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(5).FindControl("ddlClass"), DropDownList)
                        Dim ddlCampaign As DropDownList = DirectCast(gvBillItems.Rows(rowIndex).Cells(6).FindControl("ddlCampaign"), DropDownList)

                        ddlExpenseAccount.SelectedValue = dt.Rows(i)("ExpenseAccount").ToString()
                        txtAmount.Text = CCommon.ToString(dt.Rows(i)("Amount"))
                        txtMemo.Text = CCommon.ToString(dt.Rows(i)("Memo"))

                        If ddlProject IsNot Nothing AndAlso ddlProject.Items.FindByValue(CCommon.ToLong(dt.Rows(i)("Project"))) IsNot Nothing Then
                            ddlProject.Items.FindByValue(CCommon.ToLong(dt.Rows(i)("Project"))).Selected = True
                        End If

                        If ddlClass IsNot Nothing AndAlso ddlClass.Items.FindByValue(CCommon.ToLong(dt.Rows(i)("Class"))) IsNot Nothing Then
                            ddlClass.Items.FindByValue(CCommon.ToLong(dt.Rows(i)("Class"))).Selected = True
                        End If

                        If ddlCampaign IsNot Nothing AndAlso ddlCampaign.Items.FindByValue(CCommon.ToLong(dt.Rows(i)("Campaign"))) IsNot Nothing Then
                            ddlCampaign.Items.FindByValue(CCommon.ToLong(dt.Rows(i)("Campaign"))).Selected = True
                        End If

                        rowIndex += 1
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            AddNewRowToGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub calBillDate_CommitTransaction(ByVal sender As Object, ByVal e As System.EventArgs) Handles calBillDate.CommitTransaction
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class