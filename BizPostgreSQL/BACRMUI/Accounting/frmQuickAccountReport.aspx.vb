''Created By Siva
Imports BACRM.BusinessLogic.Common
Imports System.Configuration
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmQuickAccountReport
    Inherits BACRMPage

#Region "Variables"
    Dim mobjGeneralLedger As GeneralLedger
    Dim ldecTotalBalanceAmt As Decimal
    Dim dtChartAcntQuickReportDetails As DataTable
    Dim dsQuickReport As DataSet = New DataSet
    Dim dtQuickReport As DataTable = dsQuickReport.Tables.Add("QuickReport")
    Dim mintChartAcntId As Integer
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            mintChartAcntId = CCommon.ToInteger(GetQueryStringVal("ChartAcntId"))
            If Not IsPostBack Then
                
                If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                mobjGeneralLedger.DomainId = Session("DomainId")
                mobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()   ''DateAdd(DateInterval.Day, -7, Now())
                '' calFrom.SelectedDate = DateAdd(DateInterval.Day, -7, Now())
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ''DateAdd(DateInterval.Day, 0, Now())
                LoadGeneralDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadGeneralDetails()
        Dim dtChartAcntDetails As New DataTable
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            dtQuickReport.Columns.Add("Date", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("Type", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("Name", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("Memo/Description", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("Split", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("Amount", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("Balance", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("JournalId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("CheckId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("CashCreditCardId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numChartAcntId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numTransactionId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numOppId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numOppBizDocsId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numDepositId", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numCategoryHDRID", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("tintTEType", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numCategory", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("numUserCntID", Type.GetType("System.String"))
            dtQuickReport.Columns.Add("dtFromDate", Type.GetType("System.String"))

            mobjGeneralLedger.DomainId = Session("DomainID")
            mobjGeneralLedger.FromDate = calFrom.SelectedDate
            mobjGeneralLedger.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
            mobjGeneralLedger.ChartAcntId = mintChartAcntId
            dtChartAcntDetails = mobjGeneralLedger.GetChartAcntDetailsForTransaction()

            LoadTreeGrid(dtChartAcntDetails)
            RepTransactionReport.DataSource = dsQuickReport
            RepTransactionReport.DataMember = "QuickReport"
            RepTransactionReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadTreeGrid(ByVal dtChartAcntDetails As DataTable)
        Dim i As Integer
        Dim j As Integer
        Dim ldecTotalAmt As Decimal
        Dim dtChartAcntBeginingBalance As DataTable
        Dim ldecOpeningBalance As Decimal
        Dim dtChildChartAcntDetails As DataTable
        Try
            Dim lobjJournalEntry As New JournalEntry
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger

            For i = 0 To dtChartAcntDetails.Rows.Count - 1
                ldecTotalAmt = 0
                Dim drChartAcntDetails As DataRow = dtQuickReport.NewRow

                lobjJournalEntry.ChartAcntId = dtChartAcntDetails.Rows(i)("numAccountId")
                lobjJournalEntry.DomainId = Session("DomainId")

                lobjJournalEntry.FromDate = calFrom.SelectedDate
                lobjJournalEntry.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

                dtChartAcntQuickReportDetails = lobjJournalEntry.GetGenernalLedgerList
                dtChartAcntBeginingBalance = lobjJournalEntry.GetBeginingBalanceGeneralLedgerList

                mobjGeneralLedger.ChartAcntId = dtChartAcntDetails.Rows(i)("numAccountId")
                mobjGeneralLedger.DomainId = Session("DomainId")

                mobjGeneralLedger.FromDate = calFrom.SelectedDate
                mobjGeneralLedger.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))

                ldecOpeningBalance = mobjGeneralLedger.GetCurrentOpeningBalanceForGeneralLedgerDetails

                If dtChartAcntQuickReportDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then
                    drChartAcntDetails(0) = "<font color=Black><b>" & dtChartAcntDetails.Rows(i)("CategoryName") & "</b></font>"
                    dtQuickReport.Rows.Add(drChartAcntDetails)
                End If

                If dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drQuickReportDetails1 As DataRow = dtQuickReport.NewRow
                    drQuickReportDetails1(0) = "&nbsp;&nbsp;&nbsp;&nbsp;  Beginning Balance"
                    drQuickReportDetails1(5) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    drQuickReportDetails1(6) = ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance"))
                    dtQuickReport.Rows.Add(drQuickReportDetails1)
                    ldecTotalAmt = ldecTotalAmt + Replace(ReturnMoney(dtChartAcntBeginingBalance.Rows(dtChartAcntBeginingBalance.Rows.Count - 1)("numBalance")), ",", "")
                End If

                For j = 0 To dtChartAcntQuickReportDetails.Rows.Count - 1
                    Dim drQuickReportDetails As DataRow = dtQuickReport.NewRow
                    drQuickReportDetails(0) = "&nbsp;&nbsp;&nbsp;&nbsp; " & ReturnDate(dtChartAcntQuickReportDetails.Rows(j)("EntryDate"))
                    drQuickReportDetails(1) = dtChartAcntQuickReportDetails.Rows(j)("TransactionType")
                    drQuickReportDetails(2) = dtChartAcntQuickReportDetails.Rows(j)("CompanyName")
                    drQuickReportDetails(3) = dtChartAcntQuickReportDetails.Rows(j)("Memo")
                    drQuickReportDetails(4) = dtChartAcntQuickReportDetails.Rows(j)("AcntTypeDescription")
                    drQuickReportDetails(5) = IIf(IsDBNull(dtChartAcntQuickReportDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntQuickReportDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntQuickReportDetails.Rows(j)("Deposit")))
                    drQuickReportDetails(6) = ReturnMoney(dtChartAcntQuickReportDetails.Rows(j)("numBalance"))
                    drQuickReportDetails(7) = dtChartAcntQuickReportDetails.Rows(j)("JournalId")
                    drQuickReportDetails(8) = IIf(IsDBNull(dtChartAcntQuickReportDetails.Rows(j)("CheckId")), 0, dtChartAcntQuickReportDetails.Rows(j)("CheckId"))
                    drQuickReportDetails(9) = IIf(IsDBNull(dtChartAcntQuickReportDetails.Rows(j)("CashCreditCardId")), 0, dtChartAcntQuickReportDetails.Rows(j)("CashCreditCardId"))
                    drQuickReportDetails(10) = dtChartAcntQuickReportDetails.Rows(j)("numChartAcntId")
                    drQuickReportDetails(11) = dtChartAcntQuickReportDetails.Rows(j)("numTransactionId")
                    drQuickReportDetails(12) = dtChartAcntQuickReportDetails.Rows(j)("numOppId")
                    drQuickReportDetails(13) = dtChartAcntQuickReportDetails.Rows(j)("numOppBizDocsId")
                    drQuickReportDetails(14) = dtChartAcntQuickReportDetails.Rows(j)("numDepositId")
                    drQuickReportDetails(15) = dtChartAcntQuickReportDetails.Rows(j)("numCategoryHDRID")
                    drQuickReportDetails(16) = dtChartAcntQuickReportDetails.Rows(j)("tintTEType")
                    drQuickReportDetails(17) = dtChartAcntQuickReportDetails.Rows(j)("numCategory")
                    drQuickReportDetails(18) = dtChartAcntQuickReportDetails.Rows(j)("numUserCntID")
                    drQuickReportDetails(19) = dtChartAcntQuickReportDetails.Rows(j)("dtFromDate")

                    dtQuickReport.Rows.Add(drQuickReportDetails)

                    ldecTotalAmt = ldecTotalAmt + IIf(IsDBNull(dtChartAcntQuickReportDetails.Rows(j)("Deposit")), ReturnMoney(dtChartAcntQuickReportDetails.Rows(j)("Payment")), ReturnMoney(dtChartAcntQuickReportDetails.Rows(j)("Deposit")))
                Next

                If dtChartAcntQuickReportDetails.Rows.Count > 0 Or dtChartAcntBeginingBalance.Rows.Count > 0 Then
                    Dim drDummyRow As DataRow = dtQuickReport.NewRow
                    drDummyRow(0) = "<B> Total for " & dtChartAcntDetails.Rows(i)("CategoryName") & "</b>"
                    drDummyRow(5) = "<B> " & ReturnMoney(ldecTotalAmt) & "</b>"
                    dtQuickReport.Rows.Add(drDummyRow)
                    ldecTotalBalanceAmt = ldecTotalBalanceAmt + ldecTotalAmt
                End If

                dtChildChartAcntDetails = GetChildCategory(dtChartAcntDetails.Rows(i)("numAccountId"))

                LoadTreeGrid(dtChildChartAcntDetails)

                If dtChildChartAcntDetails.Rows.Count > 0 Then
                    If dtChartAcntQuickReportDetails.Rows.Count > 0 Or ldecOpeningBalance <> 0 Then

                        Dim drDummyRowForSubAcntTot As DataRow = dtQuickReport.NewRow
                        drDummyRowForSubAcntTot(0) = "<B> Total for " & dtChartAcntDetails.Rows(i)("CategoryName") & "  with sub-accounts </b>"
                        drDummyRowForSubAcntTot(5) = "<B> " & ReturnMoney(ldecOpeningBalance) & "</b>"
                        dtQuickReport.Rows.Add(drDummyRowForSubAcntTot)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetChildCategory(ByVal ParentID As String) As DataTable
        Try
            If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
            mobjGeneralLedger.ParentAccountId = ParentID
            mobjGeneralLedger.DomainId = Session("DomainId")
            GetChildCategory = mobjGeneralLedger.GetChildCategory()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDate(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub RepTransactionReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles RepTransactionReport.ItemCommand
        Dim lblJournalId As Label
        Dim lblCheckId As Label
        Dim lblCashCreditCardId As Label
        Dim lblTransactionId As Label
        Dim lblOppId As Label
        Dim lblOppBizDocsId As Label
        Dim lblDepositId As Label
        Dim lblCategoryHDRID As Label

        Dim linJournalId As Integer
        Dim lintCheckId As Integer
        Dim lintCashCreditCardId As Integer
        Dim lintOppId As Integer
        Dim lintOppBizDocsId As Integer
        Dim lintDepositId As Integer
        Dim lintCategoryHDRID As Integer
        Try
            lblJournalId = e.Item.FindControl("lblJournalId")
            lblCheckId = e.Item.FindControl("lblCheckId")
            lblCashCreditCardId = e.Item.FindControl("lblCashCreditCardId")
            lblTransactionId = e.Item.FindControl("lblTransactionId")
            lblOppId = e.Item.FindControl("lblOppId")
            lblOppBizDocsId = e.Item.FindControl("lblOppBizDocsId")
            lblDepositId = e.Item.FindControl("lblDepositId")
            lblCategoryHDRID = e.Item.FindControl("lblCategoryHDRID")

            ''hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage()")
            linJournalId = IIf(lblJournalId.Text = "&nbsp;" OrElse lblJournalId.Text = "", 0, lblJournalId.Text)
            lintCheckId = IIf(lblCheckId.Text = "&nbsp;" OrElse lblCheckId.Text = "", 0, lblCheckId.Text)
            lintCashCreditCardId = IIf(lblCashCreditCardId.Text = "&nbsp;" OrElse lblCashCreditCardId.Text = "", 0, lblCashCreditCardId.Text)
            lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
            lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
            lintDepositId = IIf(lblDepositId.Text = "&nbsp;" OrElse lblDepositId.Text = "", 0, lblDepositId.Text)
            lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)
            If e.CommandName = "Edit" Then
                If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppId = 0 And lintOppBizDocsId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 Then
                    ''hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmJournalEntryList.aspx?frm=Transaction&JournalId=" & linJournalId & "&ChartAcntId=" & mintChartAcntId & "')")
                    Response.Redirect("../Accounting/frmNewJournalEntry.aspx?frm=QuickReport&JournalId=" & linJournalId & "&ChartAcntId=" & mintChartAcntId)
                ElseIf lintCheckId <> 0 Then
                    '' hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmChecks.aspx?frm=Transaction&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "&ChartAcntId=" & mintChartAcntId & "')")
                    Response.Redirect("../Accounting/frmChecks.aspx?frm=QuickReport&JournalId=" & linJournalId & "&CheckId=" & lintCheckId & "&ChartAcntId=" & mintChartAcntId)
                ElseIf lintCashCreditCardId <> 0 Then
                    ''  hplType.Attributes.Add("onclick", "return OpenJournalDetailsPage('" & "../Accounting/frmCash.aspx?frm=Transaction&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "&ChartAcntId=" & mintChartAcntId & "')")
                    Response.Redirect("../Accounting/frmCash.aspx?frm=QuickReport&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId & "&ChartAcntId=" & mintChartAcntId)
                    ''ElseIf lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                    ''    Dim lstr As String
                    ''    lstr = "<script language=javascript>"
                    ''    lstr += "window.open('../opportunity/frmBizInvoice.aspx?frm=QuickReport&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "','','toolbar=no,titlebar=no,left=200, top=300,width=800,height=550,scrollbars=no,resizable=yes');"
                    ''    lstr += "</script>"
                    ''    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "BizInvoice", lstr)
                    ''    ''hplType.ttributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                ElseIf lintDepositId <> 0 Then
                    Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=QuickReport&JournalId=" & linJournalId & "&DepositId=" & lintDepositId & "&ChartAcntId=" & mintChartAcntId)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadGeneralDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If GetQueryStringVal( "frm") = "frmTrialBalance" Then
                Response.Redirect("../Accounting/frmTrailBalance.aspx")
            ElseIf GetQueryStringVal( "frm") = "frmProfitLoss" Then
                Response.Redirect("../Accounting/frmProfitLoss.aspx")
            Else : Response.Redirect("../Accounting/frmChartofAccounts.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub RepTransactionReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepTransactionReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                ''Dim lintOppId As Integer
                ''Dim lintOppBizDocsId As Integer
                ''Dim lblOppId As Label
                ''Dim lblOppBizDocsId As Label
                ''Dim hplType As LinkButton
                ''hplType = e.Item.FindControl("lnkType")
                ''lblOppId = e.Item.FindControl("lblOppId")
                ''lblOppBizDocsId = e.Item.FindControl("lblOppBizDocsId")
                ''lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)

                ''lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
                ''If lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                ''    hplType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                ''End If

                Dim lblOppId As Label
                Dim lblOppBizDocsId As Label
                Dim lintOppId As Integer
                Dim lintOppBizDocsId As Integer
                Dim lintCategoryHDRID As Integer
                Dim lnkType As LinkButton
                Dim lblCategoryHDRID As Label
                Dim lblTEType As Label
                Dim lblCategory As Label
                Dim lblUserCntID As Label
                Dim lblFromDate As Label
                lnkType = e.Item.FindControl("lnkType")
                lblCategoryHDRID = e.Item.FindControl("lblCategoryHDRID")
                lblOppId = e.Item.FindControl("lblOppId")
                lblOppBizDocsId = e.Item.FindControl("lblOppBizDocsId")
                lblCategory = e.Item.FindControl("lblCategory")
                lblTEType = e.Item.FindControl("lblTEType")
                lblUserCntID = e.Item.FindControl("lblUserCntID")
                lblFromDate = e.Item.FindControl("lblFromDate")
                lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
                lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
                lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)

                If lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                    lnkType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                ElseIf lintCategoryHDRID <> 0 Then

                    Select Case lblTEType.Text
                        Case 0
                            lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "&Date=" & CDate(lblFromDate.Text) & "')")
                        Case 1
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else : lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 2
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else : lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                        Case 3
                            If lblCategory.Text = 1 Then
                                lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            Else : lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=QuickAccount&CatHDRID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                            End If
                    End Select
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class