﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports Newtonsoft.Json.Linq
Imports iTextSharp.text
Imports iTextSharp.text.pdf

Public Class PrintCheckEditor
    Inherits BACRMPage

    Dim SavePath As String = ""
    Dim DownloadPath As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                LoadPreferences()
                ShowCheckStubPDF()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub ShowCheckStubPDF()
        Try
            Dim ImagePath As String = CCommon.ToString(Session("FileLocation"))
            If Not String.IsNullOrEmpty(Path.GetFileName(ImagePath)) AndAlso File.Exists(ImagePath) Then
                ImagePath = CCommon.GetDocumentPath(Session("DomainID")) & Path.GetFileName(ImagePath)
                ifCheckStubPdF.Src = ImagePath
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSavePreferences_Click(sender As Object, e As EventArgs) Handles btnSavePreferences.Click
        Try
            If Not String.IsNullOrEmpty(CCommon.ToString(Session("FileLocation"))) AndAlso File.Exists(CCommon.ToString(Session("FileLocation"))) Then
                If File.Exists(SavePreferences()) Then
                    litMessage.Text = "Check Preferences Saved Successfully."
                Else
                    litMessage.Text = "Error occurred while saving Check Preferences.!"
                End If
            Else
                litMessage.Text = "Upload check stub pdf file.!"
            End If

           
            LoadPreferences()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Function SavePreferences() As String
        Dim strFilePath As String = ""
        Try
            Dim dsPreferences As New DataSet
            Dim dtPreferences As New DataTable

            dtPreferences.Columns.Add("Name")
            dtPreferences.Columns.Add("FontFamily")
            dtPreferences.Columns.Add("FontSize")
            dtPreferences.Columns.Add("FontStyle")
            dtPreferences.Columns.Add("Left")
            dtPreferences.Columns.Add("Top")
            dtPreferences.Columns.Add("ImagePath")
            dtPreferences.Columns.Add("IsIncluded")

            Dim dr As DataRow
            dr = dtPreferences.NewRow
            dr("Name") = "PayeeName"
            dr("FontFamily") = ddlPayeeNameFontfamily.SelectedValue
            dr("FontSize") = ddlPayeeNameFontSize.SelectedValue
            dr("FontStyle") = ddlPayeeNameFontStyle.SelectedValue
            dr("Left") = CCommon.ToDecimal(txtPayeeNameLeft.Text)
            dr("Top") = CCommon.ToDecimal(txtPayeeNameTop.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = 1
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "CheckDate"
            dr("FontFamily") = ddlCheckDateFontfamily.SelectedValue
            dr("FontSize") = ddlCheckDateFontSize.SelectedValue
            dr("FontStyle") = ddlCheckDateFontStyle.SelectedValue
            dr("Left") = CCommon.ToDecimal(txtCheckDateLeft.Text)
            dr("Top") = CCommon.ToDecimal(txtCheckDateTop.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = 1 'CCommon.ToShort(chkCheckDate.Checked)
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "Amount"
            dr("FontFamily") = ddlAmountFontFamily.SelectedValue
            dr("FontSize") = ddlAmountFontSize.SelectedValue
            dr("FontStyle") = ddlAmountFontStyle.SelectedValue
            dr("Left") = CCommon.ToDecimal(txtAmountLeft.Text)
            dr("Top") = CCommon.ToDecimal(txtAmountTop.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = 1 'CCommon.ToShort(chkAmount.Checked)
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "AmountInWords"
            dr("FontFamily") = ddlAmountInWordsFontFamily.SelectedValue
            dr("FontSize") = ddlAmountInWordsFontSize.SelectedValue
            dr("FontStyle") = ddlAmountInWordsFontStyle.SelectedValue
            dr("Left") = CCommon.ToDecimal(txtAmountInWordsLeft.Text)
            dr("Top") = CCommon.ToDecimal(txtAmountInWordsTop.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = 1 'CCommon.ToShort(chkAmountInWords.Checked)
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "EmployerName"
            dr("FontFamily") = ddlEmployerNameFontFamily.SelectedValue
            dr("FontSize") = ddlEmployerNameFontSize.SelectedValue
            dr("FontStyle") = ddlEmployerNameFontStyle.SelectedValue
            dr("Left") = CCommon.ToDecimal(txtEmployerNameLeft.Text)
            dr("Top") = CCommon.ToDecimal(txtEmployerNameTop.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = CCommon.ToShort(chkEmployerName.Checked)
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "Address"
            dr("FontFamily") = ddlAddressFontFamily.SelectedValue
            dr("FontSize") = ddlAddressFontSize.SelectedValue
            dr("FontStyle") = ddlAddressFontStyle.SelectedValue
            dr("Left") = CCommon.ToDecimal(txtAddressLeft.Text)
            dr("Top") = CCommon.ToDecimal(txtAddressTop.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = CCommon.ToShort(chkAddress.Checked)
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "Stub1"
            dr("FontFamily") = 0
            dr("FontSize") = 0
            dr("FontStyle") = 0
            dr("Left") = CCommon.ToDecimal(txtStub1Left.Text)
            dr("Top") = CCommon.ToDecimal(txtStub1Top.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = 1
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "Stub2"
            dr("FontFamily") = 0
            dr("FontSize") = 0
            dr("FontStyle") = 0
            dr("Left") = CCommon.ToDecimal(txtStub2Left.Text)
            dr("Top") = CCommon.ToDecimal(txtStub2Top.Text)
            dr("ImagePath") = ""
            dr("IsIncluded") = 1
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "Image"
            dr("FontFamily") = ""
            dr("FontSize") = 0
            dr("FontStyle") = 0
            dr("Left") = 0
            dr("Top") = 0
            dr("ImagePath") = CCommon.ToString(Session("FileLocation"))
            dr("IsIncluded") = 1
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dr = dtPreferences.NewRow
            dr("Name") = "PagePreference"
            dr("FontFamily") = ""
            dr("FontSize") = 0
            dr("FontStyle") = 0
            dr("Left") = 0
            dr("Top") = 0
            dr("ImagePath") = ""
            dr("IsIncluded") = 1
            dtPreferences.Rows.Add(dr)
            dtPreferences.AcceptChanges()

            dsPreferences.Tables.Add(dtPreferences)
            dsPreferences.AcceptChanges()

            strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "/CheckPreferences.xml"
            dsPreferences.WriteXml(strFilePath)

        Catch ex As Exception
            Throw ex
        End Try

        Return strFilePath
    End Function

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Try
            Dim SaveLocation As String = ""
            If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)

                If System.IO.Path.GetExtension(fileName) = ".pdf" Then
                    SaveLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & fileName
                    If File.Exists(fileName) Then
                        Try
                            File.Delete(fileName)
                        Catch ex As Exception
                            litMessage.Text = "Error occurred while uploading file.!"
                        End Try
                    End If

                    Try
                        txtFile.PostedFile.SaveAs(SaveLocation)
                        Session("FileLocation") = SaveLocation
                        litMessage.Text = "The file has been uploaded"

                        SavePreferences()
                        LoadPreferences()
                    Catch ex As Exception
                        Throw ex
                    End Try
                Else
                    litMessage.Text = "Only pdf file is allowed."
                End If
            Else
                litMessage.Text = "Please select a file to upload."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub btnPreview_Click(sender As Object, e As EventArgs) Handles btnPreview.Click
        Try
            If Not String.IsNullOrEmpty(Session("FileLocation")) AndAlso File.Exists(Session("FileLocation")) Then
                CreatePDFNew()
                iFramePrintChecks.Attributes.Add("src", DownloadPath)
                SavePreferences()
            Else
                litMessage.Text = "Upload check stub pdf file.!"
            End If

            LoadPreferences()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function ConvertInchesPoints(ByVal dcValue As Decimal, Optional ByVal IsPointToInches As Boolean = False) As Decimal
        Dim dcResult As Decimal = 0
        Try
            dcResult = dcValue * 0.72
            'If IsPointToInches Then
            '    dcResult = dcValue / 72
            'Else
            '    dcResult = dcValue * 72
            'End If

        Catch ex As Exception
            dcResult = 0
        End Try

        Return dcResult
    End Function

    Private Sub LoadPreferences()
        Try
            Dim strPreferenceFile As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "/CheckPreferences.xml"
            If Not File.Exists(strPreferenceFile) Then
                strPreferenceFile = CCommon.GetDocumentPhysicalPath(0) & "/CheckPreferences.xml"
            End If

            Dim dsPref As New DataSet
            dsPref.ReadXml(strPreferenceFile)
            If dsPref IsNot Nothing AndAlso dsPref.Tables.Count > 0 AndAlso dsPref.Tables(0).Rows.Count > 0 Then

                Dim drPref() As DataRow
                drPref = dsPref.Tables(0).Select("Name = 'Image'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    Session("FileLocation") = CCommon.ToString(drPref(0)("ImagePath"))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'PayeeName'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    'txtFile.Value = CCommon.ToString(drPref(0)("ImagePath"))
                    If ddlPayeeNameFontfamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))) IsNot Nothing Then
                        ddlPayeeNameFontfamily.ClearSelection()
                        ddlPayeeNameFontfamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))).Selected = True
                    End If

                    If ddlPayeeNameFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))) IsNot Nothing Then
                        ddlPayeeNameFontSize.ClearSelection()
                        ddlPayeeNameFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))).Selected = True
                    End If

                    If ddlPayeeNameFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))) IsNot Nothing Then
                        ddlPayeeNameFontStyle.ClearSelection()
                        ddlPayeeNameFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))).Selected = True
                    End If

                    txtPayeeNameLeft.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtPayeeNameTop.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'CheckDate'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    'txtFile.Value = CCommon.ToString(drPref(0)("ImagePath"))
                    If ddlCheckDateFontfamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))) IsNot Nothing Then
                        ddlCheckDateFontfamily.ClearSelection()
                        ddlCheckDateFontfamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))).Selected = True
                    End If

                    If ddlCheckDateFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))) IsNot Nothing Then
                        ddlCheckDateFontSize.ClearSelection()
                        ddlCheckDateFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))).Selected = True
                    End If

                    If ddlCheckDateFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))) IsNot Nothing Then
                        ddlCheckDateFontStyle.ClearSelection()
                        ddlCheckDateFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))).Selected = True
                    End If

                    txtCheckDateLeft.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtCheckDateTop.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'Amount'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    'txtFile.Value = CCommon.ToString(drPref(0)("ImagePath"))
                    If ddlAmountFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))) IsNot Nothing Then
                        ddlAmountFontFamily.ClearSelection()
                        ddlAmountFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))).Selected = True
                    End If

                    If ddlAmountFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))) IsNot Nothing Then
                        ddlAmountFontSize.ClearSelection()
                        ddlAmountFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))).Selected = True
                    End If

                    If ddlAmountFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))) IsNot Nothing Then
                        ddlAmountFontStyle.ClearSelection()
                        ddlAmountFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))).Selected = True
                    End If

                    txtAmountLeft.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtAmountTop.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'AmountInWords'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    If ddlAmountInWordsFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))) IsNot Nothing Then
                        ddlAmountInWordsFontFamily.ClearSelection()
                        ddlAmountInWordsFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))).Selected = True
                    End If

                    If ddlAmountInWordsFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))) IsNot Nothing Then
                        ddlAmountInWordsFontSize.ClearSelection()
                        ddlAmountInWordsFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))).Selected = True
                    End If

                    If ddlAmountInWordsFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))) IsNot Nothing Then
                        ddlAmountInWordsFontStyle.ClearSelection()
                        ddlAmountInWordsFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))).Selected = True
                    End If

                    txtAmountInWordsLeft.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtAmountInWordsTop.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'EmployerName'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    'txtFile.Value = CCommon.ToString(drPref(0)("ImagePath"))
                    If ddlEmployerNameFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))) IsNot Nothing Then
                        ddlEmployerNameFontFamily.ClearSelection()
                        ddlEmployerNameFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))).Selected = True
                    End If

                    If ddlEmployerNameFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))) IsNot Nothing Then
                        ddlEmployerNameFontSize.ClearSelection()
                        ddlEmployerNameFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))).Selected = True
                    End If

                    If ddlEmployerNameFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))) IsNot Nothing Then
                        ddlEmployerNameFontStyle.ClearSelection()
                        ddlEmployerNameFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))).Selected = True
                    End If

                    txtEmployerNameLeft.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtEmployerNameTop.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                    chkEmployerName.Checked = CCommon.ToInteger(drPref(0)("IsIncluded"))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'Address'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    'txtFile.Value = CCommon.ToString(drPref(0)("ImagePath"))
                    If ddlAddressFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))) IsNot Nothing Then
                        ddlAddressFontFamily.ClearSelection()
                        ddlAddressFontFamily.Items.FindByValue(CCommon.ToString(drPref(0)("FontFamily"))).Selected = True
                    End If

                    If ddlAddressFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))) IsNot Nothing Then
                        ddlAddressFontSize.ClearSelection()
                        ddlAddressFontSize.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontSize"))).Selected = True
                    End If

                    If ddlAddressFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))) IsNot Nothing Then
                        ddlAddressFontStyle.ClearSelection()
                        ddlAddressFontStyle.Items.FindByValue(CCommon.ToInteger(drPref(0)("FontStyle"))).Selected = True
                    End If

                    txtAddressLeft.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtAddressTop.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                    chkAddress.Checked = CCommon.ToInteger(drPref(0)("IsIncluded"))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'Stub1'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    txtStub1Left.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtStub1Top.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                End If

                drPref = dsPref.Tables(0).Select("Name = 'Stub2'")
                If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                    txtStub2Left.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Left")))
                    txtStub2Top.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(drPref(0)("Top")))
                End If
            End If

            ShowCheckStubPDF()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetFonts(ByVal FontName As String, ByVal FontSize As Integer, ByVal FontStyle As Integer) As iTextSharp.text.Font

        Dim fontResult As iTextSharp.text.Font
        Try
            If FontName = "Arial" Then
                fontResult = FontFactory.GetFont("Arial", FontSize, FontStyle)

            ElseIf FontName = "Courier" Then
                fontResult = FontFactory.GetFont(BaseFont.COURIER, FontSize, FontStyle)

            ElseIf FontName = "Helvetica" Then
                fontResult = FontFactory.GetFont(BaseFont.HELVETICA, FontSize, FontStyle)

            ElseIf FontName = "Times New Roman" Then
                fontResult = FontFactory.GetFont(BaseFont.TIMES_ROMAN, FontSize, FontStyle)

            Else
                fontResult = FontFactory.GetFont("Arial", FontSize, FontStyle)

            End If
        Catch ex As Exception

        End Try
        Return fontResult

    End Function

    Private Sub CreatePDFNew()
        Dim reader As PdfReader = Nothing
        Dim stamper As PdfStamper = Nothing
        Try
            If Not String.IsNullOrEmpty(Session("FileLocation")) AndAlso File.Exists(Session("FileLocation")) Then
                Dim pathin As String = CCommon.ToString(Session("FileLocation"))
                Dim pathout As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & "CheckPrintPreviewFile.pdf"
                If File.Exists(pathout) Then
                    Try
                        File.Delete(pathout)
                    Catch ex As Exception

                    End Try
                End If

                reader = New PdfReader(pathin)
                'select two pages from the original document
                reader.SelectPages("1-1")
                stamper = New PdfStamper(reader, New FileStream(pathout, FileMode.Create))
                Dim pbover As PdfContentByte = stamper.GetOverContent(1)
                'add content to the page using ColumnText
                ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase("2015/02/28"), Convert.ToDouble(txtCheckDateLeft.Text), (reader.GetPageSize(1).Height - Convert.ToDouble(txtCheckDateTop.Text)), 0)
                ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase("Bizautomation.com"), Convert.ToDouble(txtPayeeNameLeft.Text), (reader.GetPageSize(1).Height - Convert.ToDouble(txtPayeeNameTop.Text)), 0)
                ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase("**10000000.00"), Convert.ToDouble(txtAmountLeft.Text), (reader.GetPageSize(1).Height - Convert.ToDouble(txtAmountTop.Text)), 0)
                ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase("One Crore Dollers Only*******************"), Convert.ToDouble(txtAmountInWordsLeft.Text), (reader.GetPageSize(1).Height - Convert.ToDouble(txtAmountInWordsTop.Text)), 0)


                If chkEmployerName.Checked = True Then
                    ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase("Bizautomation.com"), Convert.ToDouble(txtEmployerNameLeft.Text), (reader.GetPageSize(1).Height - Convert.ToDouble(txtEmployerNameTop.Text)), 0)
                End If

                Dim bizdocsTable As PdfPTable = New PdfPTable(5)
                bizdocsTable.TotalWidth = reader.GetPageSize(1).Width
                bizdocsTable.HorizontalAlignment = Element.ALIGN_LEFT
                bizdocsTable.WidthPercentage = 100
                bizdocsTable.SetWidths(New Integer() {3, 5, 3, 3, 3})

                Dim hCell1 As PdfPCell
                hCell1 = New PdfPCell(New Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                bizdocsTable.AddCell(hCell1)

                hCell1 = New PdfPCell(New Phrase("Payee Name & ID", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                bizdocsTable.AddCell(hCell1)

                hCell1 = New PdfPCell(New Phrase("P.O. ID(s)", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                bizdocsTable.AddCell(hCell1)

                hCell1 = New PdfPCell(New Phrase("Vendor Invoice ID", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                bizdocsTable.AddCell(hCell1)

                hCell1 = New PdfPCell(New Phrase("Amount", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                bizdocsTable.AddCell(hCell1)

                Dim ct As ColumnText = New ColumnText(pbover)

                If chkAddress.Checked = True Then
                    Dim addressTable As New PdfPTable(1)
                    addressTable.TotalWidth = 300
                    addressTable.HorizontalAlignment = Element.ALIGN_LEFT

                    Dim cellAddress As PdfPCell = New PdfPCell(New Phrase("404, Apollo Arcade"))
                    cellAddress.Border = 0
                    Dim ie As IElement = New Phrase("R C Technical Road,")
                    cellAddress.AddElement(ie)
                    ie = New Phrase("Ahmedabad-380061")
                    cellAddress.AddElement(ie)
                    ie = New Phrase("Gujarat,India")
                    cellAddress.AddElement(ie)
                    addressTable.AddCell(cellAddress)

                    ct.SetSimpleColumn(Convert.ToDouble(txtAddressLeft.Text), 0, Convert.ToDouble(txtAddressLeft.Text) + 300, (reader.GetPageSize(1).Height - Convert.ToDouble(txtAddressTop.Text)))
                    ct.AddElement(addressTable)
                    ct.Go()
                End If


                ct.SetSimpleColumn(10, 0, (reader.GetPageSize(1).Width - 10), (reader.GetPageSize(1).Height - Convert.ToDouble(txtStub1Top.Text)))
                ct.AddElement(bizdocsTable)
                ct.Go()

                ct.SetSimpleColumn(10, 0, (reader.GetPageSize(1).Width - 10), (reader.GetPageSize(1).Height - Convert.ToDouble(txtStub2Top.Text)))
                ct.AddElement(bizdocsTable)
                ct.Go()

                'close the stamper
                stamper.Close()

                DownloadPath = CCommon.GetDocumentPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & "CheckPrintPreviewFile.pdf"
            Else
                Throw New Exception("Upload your check pdf.")
            End If
        Catch ex As Exception
            Throw
        Finally
            stamper.Close()
            reader.Close()
            reader.Dispose()
        End Try
    End Sub
End Class