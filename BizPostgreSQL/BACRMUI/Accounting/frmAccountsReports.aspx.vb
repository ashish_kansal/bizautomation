﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Public Class frmAccountsReports
    Inherits BACRMPage

    Dim ReportType As Short
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            ReportType = CCommon.ToShort(GetQueryStringVal("RType"))

            If Not IsPostBack Then

                calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())

                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, -1, Date.UtcNow))
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow) ' DateAdd(DateInterval.Day, 0, Now())
                    End Try
                End If

                bindUserLevelClassTracking()
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()
                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub BindData()
        Try
            Dim objReport As New FinancialReport

            objReport.DomainID = Session("DomainID")
            objReport.FromDate = CDate(calFrom.SelectedDate)
            objReport.ToDate = CDate(calTo.SelectedDate & " 23:59:59")
            objReport.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)

            Dim dtReports As DataTable = objReport.GetAccountsReports(ReportType)

            Select Case ReportType
                Case 1
                    gvCheckRegister.Columns(4).FooterText = ReturnMoney(dtReports.Compute("sum(monAmount)", ""))

                    gvCheckRegister.DataSource = dtReports
                    gvCheckRegister.DataBind()

                    lblReportName.Text = "Check Register"
                Case 2
                    gvSalesJournalDetail.Columns(1).FooterText = ReturnMoney(dtReports.Compute("sum(numDebitAmt)", ""))
                    gvSalesJournalDetail.Columns(2).FooterText = ReturnMoney(dtReports.Compute("sum(numCreditAmt)", ""))

                    gvSalesJournalDetail.DataSource = dtReports
                    gvSalesJournalDetail.DataBind()

                    lblReportName.Text = "Sales Journal Detail (By GL Account)"
                Case 3
                    gvPurchaseJournal.Columns(1).FooterText = ReturnMoney(dtReports.Compute("sum(numDebitAmt)", ""))
                    gvPurchaseJournal.Columns(2).FooterText = ReturnMoney(dtReports.Compute("sum(numCreditAmt)", ""))

                    gvPurchaseJournal.DataSource = dtReports
                    gvPurchaseJournal.DataBind()

                    lblReportName.Text = "Purchase Journal Detail (By GL Account)"
                Case 4
                    gvInvoiceRegister.Columns(4).FooterText = ReturnMoney(dtReports.Compute("sum(monDealAMount)", ""))
                    gvInvoiceRegister.Columns(5).FooterText = ReturnMoney(dtReports.Compute("sum(monBizDocAmount)", ""))
                    gvInvoiceRegister.Columns(6).FooterText = ReturnMoney(dtReports.Compute("sum(monAmountPaid)", ""))

                    gvInvoiceRegister.DataSource = dtReports
                    gvInvoiceRegister.DataBind()

                    lblReportName.Text = "Invoice Register"
                Case 5
                    gvReceiptJournalSummary.Columns(1).FooterText = ReturnMoney(dtReports.Compute("sum(numDebitAmt)", ""))
                    gvReceiptJournalSummary.Columns(2).FooterText = ReturnMoney(dtReports.Compute("sum(numCreditAmt)", ""))

                    gvReceiptJournalSummary.DataSource = dtReports
                    gvReceiptJournalSummary.DataBind()

                    lblReportName.Text = "Receipt Journal (Summary)"
                Case 6
                    gvReceiptJournalDetail.Columns(4).FooterText = ReturnMoney(dtReports.Compute("sum(numDebitAmt)", ""))
                    gvReceiptJournalDetail.Columns(5).FooterText = ReturnMoney(dtReports.Compute("sum(numCreditAmt)", ""))

                    gvReceiptJournalDetail.DataSource = dtReports
                    gvReceiptJournalDetail.DataBind()

                    If dtReports.Rows.Count > 0 Then
                        GroupGridView(gvReceiptJournalDetail.Rows, 0, 2)
                        gvReceiptJournalDetail.Columns(0).Visible = False
                    End If

                    lblReportName.Text = "Receipt Journal (Detailed)"
                Case 7
                    gvPaymentsJournalSummary.Columns(1).FooterText = ReturnMoney(dtReports.Compute("sum(numDebitAmt)", ""))
                    gvPaymentsJournalSummary.Columns(2).FooterText = ReturnMoney(dtReports.Compute("sum(numCreditAmt)", ""))

                    gvPaymentsJournalSummary.DataSource = dtReports
                    gvPaymentsJournalSummary.DataBind()

                    lblReportName.Text = "Total Disbursement Journal (Summary)"
                Case 8
                    gvPaymentsJournalDetail.Columns(5).FooterText = ReturnMoney(dtReports.Compute("sum(numDebitAmt)", ""))
                    gvPaymentsJournalDetail.Columns(6).FooterText = ReturnMoney(dtReports.Compute("sum(numCreditAmt)", ""))

                    gvPaymentsJournalDetail.DataSource = dtReports
                    gvPaymentsJournalDetail.DataBind()

                    If dtReports.Rows.Count > 0 Then
                        GroupGridView(gvPaymentsJournalDetail.Rows, 0, 2)
                        gvPaymentsJournalDetail.Columns(0).Visible = False
                    End If

                    lblReportName.Text = "Total Disbursement Journal (Detailed)"

                Case 9
                    gvCreditCard.Columns(3).FooterText = ReturnMoney(dtReports.Compute("sum(monPaymentAmount)", ""))
                    gvCreditCard.Columns(4).FooterText = ReturnMoney(dtReports.Compute("sum(monAppliedAmount)", ""))

                    gvCreditCard.DataSource = dtReports
                    gvCreditCard.DataBind()

                    lblReportName.Text = "Credit Card Register"

            End Select

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            Dim FromDate As Date = DateFromFormattedDate(calFrom.SelectedDate, Session("DateFormat"))
            Dim ToDate As Date = DateFromFormattedDate(calTo.SelectedDate, Session("DateFormat"))
            If FromDate > ToDate Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "script", "alert('From date must be smaller than To Date');", True)
                Exit Sub
            End If
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        Select Case ReportType
            Case 1
                ExportToExcel.DataGridToExcel(gvCheckRegister, Response)
            Case 2
                ExportToExcel.DataGridToExcel(gvSalesJournalDetail, Response)
            Case 3
                ExportToExcel.DataGridToExcel(gvPurchaseJournal, Response)
            Case 4
                ExportToExcel.DataGridToExcel(gvInvoiceRegister, Response)
            Case 5
                ExportToExcel.DataGridToExcel(gvReceiptJournalSummary, Response)
            Case 6
                ExportToExcel.DataGridToExcel(gvReceiptJournalDetail, Response)
            Case 7
                ExportToExcel.DataGridToExcel(gvPaymentsJournalSummary, Response)
            Case 8
                ExportToExcel.DataGridToExcel(gvPaymentsJournalDetail, Response)
            Case 9
                ExportToExcel.DataGridToExcel(gvCreditCard, Response)
        End Select
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvReceiptJournalDetail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvReceiptJournalDetail.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintReferenceType")) = 6 Then
                    e.Row.CssClass = "rgAltRow1"
                Else
                    'e.Row.Cells(0).Text = ""
                    e.Row.CssClass = "rgRow1"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvPaymentsJournalDetail_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPaymentsJournalDetail.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintReferenceType")) = 1 Or CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintReferenceType")) = 8 Then
                    e.Row.CssClass = "rgAltRow1"
                Else
                    'e.Row.Cells(0).Text = ""
                    e.Row.CssClass = "rgRow1"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub GroupGridView(gvrc As GridViewRowCollection, startIndex As Integer, total As Integer)
        If total = 0 Then
            Return
        End If
        Dim i As Integer, count As Integer = 1
        Dim lst As New ArrayList()
        lst.Add(gvrc(0))
        Dim ctrl = gvrc(0).Cells(startIndex)
        For i = 1 To gvrc.Count - 1
            Dim nextCell As TableCell = gvrc(i).Cells(startIndex)
            If ctrl.Text = nextCell.Text Then
                count += 1
                nextCell.Visible = False
                lst.Add(gvrc(i))
            Else
                If count > 1 Then
                    ctrl.RowSpan = count
                    GroupGridView(New GridViewRowCollection(lst), startIndex + 1, total - 1)
                End If
                count = 1
                lst.Clear()
                ctrl = gvrc(i).Cells(startIndex)
                lst.Add(gvrc(i))
            End If
        Next
        If count > 1 Then
            ctrl.RowSpan = count
            GroupGridView(New GridViewRowCollection(lst), startIndex + 1, total - 1)
        End If
        count = 1
        lst.Clear()
    End Sub

    Private Sub gvCheckRegister_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvCheckRegister.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).Attributes.Add("id", "CheckRegister~426~False~" & CCommon.ToString(DirectCast(e.Row.DataItem, DataRowView)("numCheckHeaderID")) & "~0~0~0")
                e.Row.Cells(0).Attributes.Add("class", "click")
                'DirectCast(DirectCast(e.Row.FindControl("hdnCheckNo"), HiddenField).Parent, System.Web.UI.WebControls.DataControlFieldCell).Attributes.Add("id", "CheckRegister~0~False~" & CCommon.ToString(DirectCast(e.Row.DataItem, DataRowView)("numCheckHeaderID")) & "~0~0~0")
                'DirectCast(DirectCast(e.Row.FindControl("hdnCheckNo"), HiddenField).Parent, System.Web.UI.WebControls.DataControlFieldCell).Attributes.Add("class", "click")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class