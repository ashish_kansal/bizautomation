''Created By Siva
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmMakeDeposit
    Inherits BACRMPage

#Region "Variables"
    Dim lngJournalID As Long
    Dim lngDepositeID As Long
    Dim lngChartAcntId As Long
    Dim lngTransactionID As Long

    Dim dtChartAcnt As New DataTable
    Dim objMakeDeposit As MakeDeposit
    Dim dtItems As New DataTable
    Dim objJournalEntry As JournalEntry
    Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking
    Dim dAmount As Decimal
    Dim dtSource As DataTable
    Shared dsDeposit As DataSet

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            'To Set Permission
            GetUserRightsForPage(35, 78)
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnDepositSaveNew.Visible = False
                btnSplitSave.Visible = False
            End If

            lngDepositeID = CCommon.ToLong(GetQueryStringVal("DepositId"))
            lngJournalID = CCommon.ToLong(GetQueryStringVal("JournalId"))
            lngChartAcntId = CCommon.ToLong(GetQueryStringVal("ChartAcntId"))
            lblDepositTotal.Text = "Deposit Total: " & Session("Currency")
            lngTransactionID = CCommon.ToLong(GetQueryStringVal("TransactionID"))
            If lngTransactionID > 0 Then
                hdnTransactionID.Value = lngTransactionID
            End If

            If Not IsPostBack Then

                PersistTable.Load()
                txtCurrrentPage.Text = PersistTable(PersistKey.CurrentPage)

                bindUserLevelClassTracking()
                BindDepositToDropDown()
                BindMultiCurrency()
                LoadDepositDetails()
                BindUnDepositedPaymentGrid("", "")
                BindNewDepositsGrid(boolPostback:=True)
                'LoadRecurringTemplate()
                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    If ddlDepositTo.Items.FindByValue(CCommon.ToString(PersistTable(ddlDepositTo.ID))) IsNot Nothing Then
                        ddlDepositTo.ClearSelection()
                        ddlDepositTo.Items.FindByValue(CCommon.ToString(PersistTable(ddlDepositTo.ID))).Selected = True
                    End If
                    ddlDepositTo_SelectedIndexChanged()
                End If

                Dim SplitFlag As Integer = CCommon.ToInteger(GetQueryStringVal("SplitFlag"))
                If SplitFlag > 0 Then
                    SplitTransaction()
                    ClientScript.RegisterStartupScript(Me.GetType(), "Resize", "windowResize();", True)
                End If
                'Else
                '    BindUnDepositedPaymentGrid()
                If lngDepositeID = 0 Then btnDelete.Visible = False
                If CCommon.ToInteger(GetQueryStringVal("flag")) = 1 Then litMessage.Text = "Deposit deleted sucessfully"
                If CCommon.ToInteger(GetQueryStringVal("flag")) = 2 Then litMessage.Text = "You deposit is saved sucessfully.Make new deposit."
            End If

            CCommon.ManageGridAsPerConfig(dgNewDeposits)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub BindUnDepositedPaymentGrid(ByVal SortColumn As String, ByVal SortOrder As String)
        Try

            Dim dvDeposit As DataView
            Dim objMakeDeposit As New MakeDeposit
            objMakeDeposit.DepositId = lngDepositeID
            objMakeDeposit.DomainID = Session("DomainID")
            objMakeDeposit.Mode = 1
            objMakeDeposit.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objMakeDeposit.AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)

            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            objMakeDeposit.CurrentPage = txtCurrrentPage.Text.Trim()

            objMakeDeposit.PageSize = Session("PagingRows")
            objMakeDeposit.TotalRecords = 0
            objMakeDeposit.SortColumn = SortColumn
            objMakeDeposit.columnSortOrder = SortOrder

            Dim strCondition As String = "1=1 "
            If CCommon.ToLong(hdnPaymentMethodID.Value) > 0 Then
                strCondition = strCondition & " AND numPaymentMethod = " & CCommon.ToLong(hdnPaymentMethodID.Value)
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnCardType.Value)) Then
                strCondition = strCondition & " AND vcCardType ilike '%" & CCommon.ToString(hdnCardType.Value) & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnSource.Value)) Then
                strCondition = strCondition & " AND vcSource ilike '%" & CCommon.ToString(hdnSource.Value) & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnReceivedFrom.Value)) Then
                strCondition = strCondition & " AND ReceivedFrom ilike '%" & CCommon.ToString(hdnReceivedFrom.Value) & "%'"
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnMemo.Value)) Then
                strCondition = strCondition & " AND vcMemo ilike '%" & CCommon.ToString(hdnMemo.Value) & "%'"
                'hdnMemo.Value = ""
            End If

            If Not String.IsNullOrEmpty(CCommon.ToString(hdnReference.Value)) Then
                strCondition = strCondition & " AND vcReference ilike '%" & CCommon.ToString(hdnReference.Value) & "%'"
                'hdnReference.Value = ""
            End If
            Dim txtFromDate As String
            Dim txtToDate As String
            If (dgUndepositedFunds.Visible = True) Then
                txtFromDate = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtFromDate"), TextBox).Text

                txtToDate = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtToDate"), TextBox).Text
            End If
            'If Not String.IsNullOrEmpty(CCommon.ToString(hdnDate.Value)) Then
            If (Not String.IsNullOrEmpty(CCommon.ToString(txtFromDate)) And Not (String.IsNullOrEmpty(CCommon.ToString(txtToDate)))) Then
                strCondition = strCondition & " AND ((DepositDate >= '" & txtFromDate & "') AND (DepositDate <= '" & txtToDate & "'))"
                'hdnReference.Value = ""
            End If

            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add(PersistKey.CurrentPage, IIf(txtCurrrentPage.Text > 0, txtCurrrentPage.Text, "1"))
            PersistTable.Save()

            ' dvDeposit.RowFilter = strCondition

            objMakeDeposit.strCondition = strCondition

            dsDeposit = objMakeDeposit.GetDepositDetailsWithClass()
            dvDeposit = dsDeposit.Tables(0).DefaultView

            dgUndepositedFunds.DataSource = dvDeposit
            dgUndepositedFunds.DataBind()
            If dsDeposit.Tables(0).Rows.Count > 0 Then
                dgUndepositedFunds.Visible = True

            End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objMakeDeposit.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            If bizPager.CurrentPageIndex <> CCommon.ToInteger(txtCurrrentPage.Text) Then
                txtCurrrentPage.Text = 1
                BindUnDepositedPaymentGrid("", "")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
    '    Try
    '        ClearGridFilters()
    '        BindUnDepositedPaymentGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Private Sub BindNewDepositsGrid(Optional ByVal deleteRowIndex As Integer = -1, Optional ByVal boolAddNew As Boolean = False, Optional ByVal boolPostback As Boolean = False)
        Try
            Dim j As Integer
            Dim drRow As DataRow
            Dim lintRowcount As Integer
            Dim dtMakeDeposit As DataSet
            If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
            objMakeDeposit.DomainID = Session("DomainID")
            objMakeDeposit.DepositId = lngDepositeID
            objMakeDeposit.Mode = 1
            dtItems = objMakeDeposit.GetDepositDetails().Tables(1)

            If boolPostback = False Then
                dtItems.Rows.Clear()

                Dim dtgriditem As DataGridItem
                For i As Integer = 0 To dgNewDeposits.Items.Count - 1
                    If i <> deleteRowIndex Then
                        drRow = dtItems.NewRow
                        dtgriditem = dgNewDeposits.Items(i)
                        drRow.Item("numDepositeDetailID") = CCommon.ToLong(CType(dtgriditem.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                        drRow.Item("monAmountPaid") = CCommon.ToDecimal(CType(dtgriditem.FindControl("txtCredit"), TextBox).Text) ''IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, CType(dtgriditem.FindControl("txtDebit"), TextBox).Text)
                        drRow.Item("numAccountID") = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
                        drRow.Item("numPaymentMethod") = CType(dtgriditem.FindControl("ddlPayment"), DropDownList).SelectedItem.Value
                        drRow.Item("numReceivedFrom") = CCommon.ToLong(CType(dtgriditem.FindControl("radCmbCompany"), RadComboBox).SelectedValue) 'CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
                        drRow.Item("vcMemo") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
                        drRow.Item("vcReference") = CType(dtgriditem.FindControl("txtReference"), TextBox).Text
                        drRow.Item("numProjectId") = CType(dtgriditem.FindControl("ddlProject"), DropDownList).SelectedValue
                        dtItems.Rows.Add(drRow)
                    End If
                Next
            End If

            If dtItems.Rows.Count < 2 Then
                lintRowcount = 2 - dtItems.Rows.Count
                For j = 0 To lintRowcount - 1
                    drRow = dtItems.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = 0
                    drRow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtItems.Rows.Add(drRow)
                Next
            End If


            If boolAddNew Then
                For i As Integer = 0 To 1
                    drRow = dtItems.NewRow

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = 0
                    drRow.Item("numClassID") = objCommon.GetAccountingClass()

                    dtItems.Rows.Add(drRow)
                Next
            End If

            dgNewDeposits.DataSource = dtItems
            dgNewDeposits.DataBind()

            TransactionInfo1.ReferenceType = enmReferenceType.DepositHeader
            TransactionInfo1.ReferenceID = lngDepositeID
            TransactionInfo1.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
            TransactionInfo1.getTransactionInfo()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDepositDetails()
        Dim dtMakeDeposit As DataTable
        Dim ds As DataSet
        Try


            If lngDepositeID > 0 Then
                If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
                objMakeDeposit.DepositId = lngDepositeID
                objMakeDeposit.DomainID = Session("DomainID")
                objMakeDeposit.Mode = 0
                ds = objMakeDeposit.GetDepositDetails()
                dtMakeDeposit = ds.Tables(0)

                If dtMakeDeposit.Rows.Count > 0 Then
                    If ddlUserLevelClass.Items.FindByValue(dtMakeDeposit.Rows(0).Item("numAccountClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(dtMakeDeposit.Rows(0).Item("numAccountClass")).Selected = True
                    End If

                    If Not IsDBNull(dtMakeDeposit.Rows(0).Item("dtOrigDepositDate")) Then
                        calMakeDeposit.SelectedDate = dtMakeDeposit.Rows(0).Item("dtOrigDepositDate")
                    End If

                    If Not IsDBNull(dtMakeDeposit.Rows(0).Item("numChartAcntId")) Then
                        If Not ddlDepositTo.Items.FindByValue(dtMakeDeposit.Rows(0).Item("numChartAcntId")) Is Nothing Then
                            ddlDepositTo.Items.FindByValue(dtMakeDeposit.Rows(0).Item("numChartAcntId")).Selected = True
                        End If
                    End If
                    ddlDepositTo_SelectedIndexChanged()

                    lblDepositTotalAmount.Text = ReturnMoney(dtMakeDeposit.Rows(0).Item("monDepositAmount"))
                    hdnDepositTotalAmt.Value = dtMakeDeposit.Rows(0).Item("monDepositAmount")

                    If Session("MultiCurrency") = True Then
                        If Not IsDBNull(dtMakeDeposit.Rows(0).Item("numCurrencyID")) Then
                            If Not ddlCurrency.Items.FindByValue(dtMakeDeposit.Rows(0).Item("numCurrencyID")) Is Nothing Then
                                ddlCurrency.ClearSelection()
                                ddlCurrency.Items.FindByValue(dtMakeDeposit.Rows(0).Item("numCurrencyID")).Selected = True
                                ddlCurrency_SelectedIndexChanged()
                            End If
                        End If
                        txtExchangeRate.Text = CCommon.ToDouble(dtMakeDeposit.Rows(0).Item("fltExchangeRateReceivedPayment"))
                        lblBaseCurr.Text = CCommon.ToString(dtMakeDeposit.Rows(0).Item("BaseCurrencySymbol")).Trim()
                        lblForeignCurr.Text = CCommon.ToString(dtMakeDeposit.Rows(0).Item("varCurrSymbol")).Trim()

                    End If

                End If

            Else
                calMakeDeposit.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgNewDeposits_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgNewDeposits.ItemCommand
        Try
            If e.CommandName = "DeleteRow" Then
                BindNewDepositsGrid(deleteRowIndex:=e.Item.ItemIndex)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgNewDeposits_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgNewDeposits.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Try
                    Dim ddlAccounts As DropDownList
                    Dim lblName As Label
                    Dim ddlPayment As DropDownList
                    Dim ddlProject As DropDownList = CType(e.Item.FindControl("ddlProject"), DropDownList)
                    lblName = CType(e.Item.FindControl("lblName"), Label)

                    BindProjectDropDown(ddlProject)

                    If CCommon.ToLong(lblName.Text) > 0 Then
                        Dim radCmbCompany As RadComboBox = CType(e.Item.FindControl("radCmbCompany"), RadComboBox)
                        objCommon.DivisionID = CCommon.ToLong(lblName.Text)
                        radCmbCompany.Text = objCommon.GetCompanyName()
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                    End If

                    If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numProjectID")) > 0 Then
                        If ddlProject.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numProjectID")) IsNot Nothing Then
                            ddlProject.ClearSelection()
                            ddlProject.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numProjectID")).Selected = True
                        End If
                    End If



                    ddlAccounts = CType(e.Item.FindControl("ddlAccounts"), DropDownList)
                    If Not IsNothing(ddlAccounts) Then
                        BindCOADropdown(ddlAccounts)

                        If Not ddlAccounts.Items.FindByValue(CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numAccountID"))) Is Nothing Then
                            ddlAccounts.Items.FindByValue(CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numAccountID"))).Selected = True
                        End If
                    End If

                    ddlPayment = CType(e.Item.FindControl("ddlPayment"), DropDownList)
                    BindPaymentMethodDropdown(ddlPayment)
                    If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPaymentMethod")) > 0 Then
                        If Not ddlPayment.Items.FindByValue(CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPaymentMethod"))) Is Nothing Then
                            ddlPayment.ClearSelection()
                            ddlPayment.Items.FindByValue(CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPaymentMethod"))).Selected = True
                        End If
                    End If

                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            BindNewDepositsGrid(boolAddNew:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlDepositTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepositTo.SelectedIndexChanged
        Try
            ddlDepositTo_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlDepositTo_SelectedIndexChanged()
        Try
            Dim lobjCashCreditCard As New CashCreditCard
            Dim lstrColor As String
            Dim ldecDepositTotalAmount As Decimal
            Dim ldecOpeningBalance As Decimal

            If hdnDepositTotalAmt.Value = "" Then
                ldecDepositTotalAmount = 0
            Else : ldecDepositTotalAmount = hdnDepositTotalAmt.Value
            End If
            If ddlDepositTo.SelectedItem.Value <> 0 Then
                lobjCashCreditCard.AccountId = ddlDepositTo.SelectedItem.Value
                lobjCashCreditCard.DomainID = Session("DomainId")
                lblBalance.Text = "Balance: " & Session("Currency")
                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
                lstrColor = "<h4 class=""label label-danger"">" & ReturnMoney(ldecOpeningBalance) & "</h4>"
                lblBalanceAmount.Text = IIf(ldecOpeningBalance < 0, lstrColor, ReturnMoney(ldecOpeningBalance))
                lblDepositTotalAmount.Text = ReturnMoney(ldecDepositTotalAmount)
            Else
                lblBalanceAmount.Text = ""
                lblBalance.Text = ""
                lblDepositTotalAmount.Text = ReturnMoney(ldecDepositTotalAmount)
            End If

            PersistTable.Clear()
            PersistTable.Add(ddlDepositTo.ID, ddlDepositTo.SelectedValue)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDepositSave_ClickExtracted()
        Dim objJEHeader As New JournalEntryHeader

        Try
            If CCommon.ToDecimal(hdnDepositTotalAmt.Value) > 0 Then
                ''If PerformValidations() Then
                ''    'Store Deposite Entry with payments
                ''    SaveDeposite()
                ''    'Create Journal Entry for line item
                ''    SaveDataToHeader()
                ''    SaveDataToGeneralJournalDetails()
                ''    PageRedirect()
                ''End If

                If PerformValidations() Then
                    'Store Deposite Entry with payments
                    Using objTran As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim objMakeDeposit As New MakeDeposit
                        With objMakeDeposit
                            .DivisionId = 0
                            .Entry_Date = calMakeDeposit.SelectedDate
                            .PaymentMethod = 0
                            .DepositeToType = 1 ' Direct deposite to Bank
                            .DepositePage = 1
                            .numAmount = Replace(hdnDepositTotalAmt.Value, ",", "")
                            .AccountId = ddlDepositTo.SelectedValue
                            .RecurringId = 0
                            .DepositId = lngDepositeID
                            .UserCntID = Session("UserContactID")
                            .DomainID = Session("DomainId")
                            .StrItems = GetItems() 'Selected Deposits entries of undeposited funds to Bank Account
                            .Mode = 1
                            .CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                            .ExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                            .AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                            lngDepositeID = .SaveDataToMakeDepositDetails(Nothing)
                        End With

                        If lngDepositeID > 0 Then
                            'Create Journal Header Entry for line item
                            If objJEHeader Is Nothing Then objJEHeader = New JournalEntryHeader

                            With objJEHeader
                                .JournalId = TransactionInfo1.JournalID
                                .RecurringId = 0
                                .EntryDate = CDate(calMakeDeposit.SelectedDate & " 12:00:00")
                                .Description = "Deposit:" & ddlDepositTo.SelectedItem.Text
                                .Amount = CCommon.ToDecimal(hdnDepositTotalAmt.Value) * IIf(CCommon.ToDouble(txtExchangeRate.Text) = 0.0, 1, CCommon.ToDouble(txtExchangeRate.Text))
                                .CheckId = 0
                                .CashCreditCardId = 0
                                .ChartAcntId = ddlDepositTo.SelectedValue
                                .OppId = 0
                                .OppBizDocsId = 0
                                .DepositId = lngDepositeID
                                .BizDocsPaymentDetId = 0
                                .IsOpeningBalance = 0
                                .LastRecurringDate = Date.Now
                                .NoTransactions = 0
                                .CategoryHDRID = 0
                                .ReturnID = 0
                                .CheckHeaderID = 0
                                .BillID = 0
                                .BillPaymentID = 0
                                .UserCntID = Session("UserContactID")
                                .DomainID = Session("DomainID")
                                .ReconcileID = 0
                            End With
                            lngJournalID = objJEHeader.Save()

                            If lngJournalID > 0 Then
                                'Create Journal Detail Entry for line item
                                Dim dtrow As DataRow
                                Dim objJE As JournalEntryNew
                                Dim objJEList As New JournalEntryCollection
                                Dim FltExchangeRate As Double
                                Dim TotalAmountAtExchangeRateOfReceivedPayment As Decimal
                                Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0
                                If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit

                                objMakeDeposit.DomainID = Session("DomainID")
                                objMakeDeposit.DepositId = lngDepositeID
                                objMakeDeposit.Mode = 0
                                dtItems = objMakeDeposit.GetDepositDetails(Nothing).Tables(1)

                                'Determine Exchange Rate
                                If CCommon.ToLong(ddlCurrency.SelectedValue) = 0 Then
                                    FltExchangeRate = 1
                                ElseIf CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) Then
                                    FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                Else
                                    FltExchangeRate = 1
                                End If

                                'i.e foreign currency payment
                                If FltExchangeRate <> 1 Then
                                    TotalAmountAtExchangeRateOfReceivedPayment = 0
                                    For Each item As DataGridItem In dgUndepositedFunds.Items
                                        If CType(item.FindControl("chk"), CheckBox).Checked Then
                                            TotalAmountAtExchangeRateOfReceivedPayment = TotalAmountAtExchangeRateOfReceivedPayment + CCommon.ToDecimal(CType(item.FindControl("lblPayment"), Label).Text) * CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfReceivedPayment"), HiddenField).Value)
                                        End If
                                    Next

                                    For Each item As DataGridItem In dgNewDeposits.Items
                                        If CCommon.ToDecimal(CType(item.FindControl("txtCredit"), TextBox).Text) > 0 Then
                                            TotalAmountAtExchangeRateOfReceivedPayment = TotalAmountAtExchangeRateOfReceivedPayment + CCommon.ToDecimal(CType(item.FindControl("txtCredit"), TextBox).Text) * FltExchangeRate
                                        End If
                                    Next

                                    TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfReceivedPayment - (Math.Abs(CCommon.ToDecimal(dtItems.Compute("sum(monAmountPaid)", ""))) * FltExchangeRate)
                                    If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                                        TotalAmountVariationDueToCurrencyExRate = 0
                                    End If
                                End If


                                For Each dr As DataRow In dtItems.Rows
                                    objJE = New JournalEntryNew()
                                    objJE.TransactionId = CCommon.ToLong(dr("numTransactionID")) 'CCommon.ToLong(hfHeaderTransactionId.Value)
                                    objJE.DebitAmt = 0
                                    objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(dr("monAmountPaid")) * CCommon.ToDecimal(dr("fltExchangeRateReceivedPayment")))
                                    If CCommon.ToLong(dr("numChildDepositID")) > 0 Then
                                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID")) 'Undeposited funds to bank account
                                    Else ' New deposit
                                        objJE.ChartAcntId = CCommon.ToLong(dr("numAccountID"))
                                    End If
                                    If CCommon.ToLong(dr("numChildDepositID")) > 0 Then
                                        objJE.Description = "Amount Deposited from UnDeposited Funds"
                                    Else
                                        objJE.Description = CCommon.ToString(dr("vcMemo"))
                                    End If

                                    objJE.CustomerId = CCommon.ToLong(dr("numReceivedFrom"))
                                    objJE.MainDeposit = 0
                                    objJE.MainCheck = 0
                                    objJE.MainCashCredit = 0
                                    objJE.OppitemtCode = 0
                                    objJE.BizDocItems = ""
                                    objJE.Reference = CCommon.ToString(dr("vcReference"))
                                    objJE.PaymentMethod = CCommon.ToLong(dr("numPaymentMethod"))
                                    objJE.Reconcile = False
                                    objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                    objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                    objJE.TaxItemID = 0
                                    objJE.BizDocsPaymentDetailsId = 0
                                    objJE.ContactID = 0
                                    objJE.ItemID = 0
                                    objJE.ProjectID = CCommon.ToLong(dr("numProjectID"))
                                    objJE.ClassID = CCommon.ToLong(dr("numClassID"))
                                    objJE.CommissionID = 0
                                    objJE.ReconcileID = 0
                                    objJE.Cleared = 0
                                    objJE.ReferenceType = enmReferenceType.DepositDetail
                                    objJE.ReferenceID = CCommon.ToLong(dr("numDepositeDetailID"))
                                    objJEList.Add(objJE)
                                    'End If
                                Next

                                'To Add the Debit Amount in JournalDetails table [i.e] For Bank Account Selected
                                objJE = New JournalEntryNew()
                                objJE.TransactionId = CCommon.ToLong(dtItems.Rows(0)("numTransactionIDHeader"))
                                objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dtItems.Compute("sum(monAmountPaid)", "")) * FltExchangeRate)
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = CCommon.ToLong(ddlDepositTo.SelectedValue)
                                objJE.Description = "Amount Deposited to Bank A/c" 'CCommon.ToString("Deposit Amount to " + ddlDepositTo.SelectedItem.Text)
                                objJE.CustomerId = 0
                                objJE.MainDeposit = 1
                                objJE.MainCheck = 0
                                objJE.MainCashCredit = 0
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = ""
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = 0
                                objJE.ReferenceType = enmReferenceType.DepositHeader
                                objJE.ReferenceID = lngDepositeID
                                objJEList.Add(objJE)

                                If CCommon.ToLong(ddlCurrency.SelectedValue) > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                                    If CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And CCommon.ToDouble(txtExchangeRate.Text) > 0.0 Then 'Foreign Currency Transaction

                                        Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                                        'Credit: Customer A/R With (Amount)
                                        objJE = New JournalEntryNew()
                                        objJE.TransactionId = 0

                                        ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                                        If TotalAmountVariationDueToCurrencyExRate < 0 Then
                                            objJE.DebitAmt = 0
                                            objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                        ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                                            objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                            objJE.CreditAmt = 0
                                        End If

                                        objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                                        objJE.Description = "Exchange Gain/Loss"
                                        objJE.CustomerId = 0
                                        objJE.MainDeposit = 0
                                        objJE.MainCheck = 0
                                        objJE.MainCashCredit = 0
                                        objJE.OppitemtCode = 0
                                        objJE.BizDocItems = ""
                                        objJE.Reference = ""
                                        objJE.PaymentMethod = 0
                                        objJE.Reconcile = False
                                        objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                        objJE.FltExchangeRate = FltExchangeRate
                                        objJE.TaxItemID = 0
                                        objJE.BizDocsPaymentDetailsId = 0
                                        objJE.ContactID = 0
                                        objJE.ItemID = 0
                                        objJE.ProjectID = 0
                                        objJE.ClassID = 0
                                        objJE.CommissionID = 0
                                        objJE.ReconcileID = 0
                                        objJE.Cleared = 0
                                        objJE.ReferenceType = enmReferenceType.DepositDetail
                                        objJE.ReferenceID = 0

                                        objJEList.Add(objJE)
                                    End If
                                End If

                                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"), Nothing)

                                objTran.Complete()
                            Else
                                litMessage.Text = "ERROR:2 Error occurred while processing payment." 'Error while saving Deposite Journal Header Entry data
                                Throw New Exception("ROLLBACK")
                            End If

                        Else
                            litMessage.Text = "ERROR:1 Error occurred while processing payment." 'Error while saving Deposite Entry data
                            Throw New Exception("ROLLBACK")
                        End If
                    End Using

                    PageRedirect()
                End If

            Else
                litMessage.Text = "You must specify at least one new deposit"
            End If

        Catch ex As Exception
            If ex.Message = "IM_BALANCE" Then
                litMessage.Text = "The Credit and Debit amounts you�re entering must be of the same amount"
            ElseIf ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            ElseIf ex.Message <> "ROLLBACK" Then
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub
    Private Sub btnSplitSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSplitSave.Click
        Try

            If CCommon.ToDecimal(hdnDepositTotAmt.Value) = CCommon.ToDecimal(lblSplitTotalAmount.Text) Then
                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    'Store Deposite Entry with payments 
                    Dim lngDepositeID As Long = SaveDeposite()
                    'Create Journal Entry for line item
                    SaveDataToHeader()
                    SaveDataToGeneralJournalDetails()

                    If Not CCommon.ToLong(hdnTransactionID.Value) = 0 Then
                        objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                        objEBanking.TransMappingId = 0
                        objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                        objEBanking.ReferenceType = enmReferenceType.DepositHeader
                        objEBanking.ReferenceID = lngDepositeID
                        objEBanking.IsActive = 1
                        objEBanking.MapBankStatementTransactions()
                    End If

                    objTransactionScope.Complete()
                End Using
                ClientScript.RegisterClientScriptBlock(Page.GetType, "close", "SplitDepositEnds();", True)
            Else
                litMessage.Text = "Amount does not match with transaction amount, you need to make sure total amount of deposit is equal to " & lblSplitTotalAmount.Text
            End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Private Sub PageRedirect()
        Try
            If GetQueryStringVal("frm") = "BankRegister" Then
                Response.Redirect("../Accounting/frmJournalEntry.aspx?ChartAcntId=" & lngChartAcntId, False)
            ElseIf GetQueryStringVal("frm") = "RecurringTransaction" Then
                Response.Redirect("../Accounting/frmRecurringTransaction.aspx", False)
            ElseIf GetQueryStringVal("frm") = "GeneralLedger" Then
                Response.Redirect("../Accounting/frmGeneralLedger.aspx", False)
            ElseIf GetQueryStringVal("frm") = "Transaction" Then
                Response.Redirect("../Accounting/frmTransactions.aspx?ChartAcntId=" & lngChartAcntId, False)
            ElseIf GetQueryStringVal("frm") = "BankRecon" Then
                Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & CCommon.ToLong(GetQueryStringVal("ReconcileID")), False)
            Else
                Response.Redirect("../Accounting/frmMakeDeposit.aspx?flag=2", False)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function SaveDeposite() As Long
        Try
            Dim objMakeDeposit As New MakeDeposit
            With objMakeDeposit
                .DivisionId = 0
                .Entry_Date = calMakeDeposit.SelectedDate
                .PaymentMethod = 0
                .DepositeToType = 1 ' Direct deposite to Bank
                .DepositePage = 1
                .numAmount = Replace(hdnDepositTotalAmt.Value, ",", "")
                .AccountId = ddlDepositTo.SelectedValue
                .RecurringId = 0
                .DepositId = lngDepositeID
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainId")
                .StrItems = GetItems() 'Selected Deposits entries of undeposited funds to Bank Account
                .Mode = 1
                .CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                .ExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                .AccountClass = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                lngDepositeID = .SaveDataToMakeDepositDetails()

                Return lngDepositeID
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable

            CCommon.AddColumnsToDataTable(dt, "numDepositeDetailID,numChildDepositID,monAmountPaid,numPaymentMethod,vcMemo,vcReference,numAccountID,numClassID,numProjectID,numReceivedFrom")

            For Each item As DataGridItem In dgUndepositedFunds.Items

                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numDepositeDetailID") = CCommon.ToLong(CType(item.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                    dr("numChildDepositID") = CCommon.ToLong(CType(item.FindControl("hdnDepositeID"), HiddenField).Value)
                    dr("monAmountPaid") = CCommon.ToDecimal(CType(item.FindControl("lblPayment"), Label).Text)
                    dr("numPaymentMethod") = CCommon.ToLong(CType(item.FindControl("ddlPayment"), DropDownList).SelectedValue)
                    dr("vcMemo") = CType(item.FindControl("txtMemo"), TextBox).Text
                    dr("vcReference") = CType(item.FindControl("txtReference"), TextBox).Text
                    dr("numReceivedFrom") = CCommon.ToLong(CType(item.FindControl("hdnDivisionID"), HiddenField).Value)
                    dt.Rows.Add(dr)
                End If
            Next

            For Each item As DataGridItem In dgNewDeposits.Items

                If CCommon.ToDecimal(CType(item.FindControl("txtCredit"), TextBox).Text) > 0 Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numDepositeDetailID") = CCommon.ToLong(CType(item.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                    dr("numChildDepositID") = 0
                    dr("monAmountPaid") = CCommon.ToDecimal(CType(item.FindControl("txtCredit"), TextBox).Text)
                    dr("numPaymentMethod") = CCommon.ToLong(CType(item.FindControl("ddlPayment"), DropDownList).SelectedValue)
                    dr("vcMemo") = CType(item.FindControl("txtMemo"), TextBox).Text
                    dr("vcReference") = CType(item.FindControl("txtReference"), TextBox).Text
                    dr("numAccountID") = CCommon.ToLong(CType(item.FindControl("ddlAccounts"), DropDownList).SelectedValue)
                    dr("numClassID") = If(ddlUserLevelClass.Items.Count > 0, CCommon.ToLong(ddlUserLevelClass.SelectedValue), 0)
                    dr("numProjectID") = CCommon.ToLong(CType(item.FindControl("ddlProject"), DropDownList).SelectedValue)
                    dr("numReceivedFrom") = CCommon.ToLong(CType(item.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    dt.Rows.Add(dr)
                End If
            Next

            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader() As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = TransactionInfo1.JournalID
                .RecurringId = 0
                .EntryDate = CDate(calMakeDeposit.SelectedDate & " 12:00:00")
                .Description = "Deposit:" & ddlDepositTo.SelectedItem.Text
                .Amount = CCommon.ToDecimal(hdnDepositTotalAmt.Value) * IIf(CCommon.ToDouble(txtExchangeRate.Text) = 0.0, 1, CCommon.ToDouble(txtExchangeRate.Text))
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = ddlDepositTo.SelectedValue
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = lngDepositeID
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .ReconcileID = 0
            End With
            lngJournalID = objJEHeader.Save()
            Return lngJournalID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails()
        Dim dtrow As DataRow
        Dim objJE As JournalEntryNew
        Dim objJEList As New JournalEntryCollection
        Dim FltExchangeRate As Double
        Dim TotalAmountAtExchangeRateOfReceivedPayment As Decimal
        Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0

        Try
            If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
            objMakeDeposit.DomainID = Session("DomainID")
            objMakeDeposit.DepositId = lngDepositeID
            objMakeDeposit.Mode = 0
            dtItems = objMakeDeposit.GetDepositDetails().Tables(1)

            'Determine Exchange Rate
            If CCommon.ToLong(ddlCurrency.SelectedValue) = 0 Then
                FltExchangeRate = 1
            ElseIf CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) Then
                FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
            Else
                FltExchangeRate = 1
            End If

            'i.e foreign currency payment
            If FltExchangeRate <> 1 Then
                TotalAmountAtExchangeRateOfReceivedPayment = 0
                For Each item As DataGridItem In dgUndepositedFunds.Items
                    If CType(item.FindControl("chk"), CheckBox).Checked Then
                        TotalAmountAtExchangeRateOfReceivedPayment = TotalAmountAtExchangeRateOfReceivedPayment + CCommon.ToDecimal(CType(item.FindControl("lblPayment"), Label).Text) * CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfReceivedPayment"), HiddenField).Value)
                    End If
                Next

                For Each item As DataGridItem In dgNewDeposits.Items
                    If CCommon.ToDecimal(CType(item.FindControl("txtCredit"), TextBox).Text) > 0 Then
                        TotalAmountAtExchangeRateOfReceivedPayment = TotalAmountAtExchangeRateOfReceivedPayment + CCommon.ToDecimal(CType(item.FindControl("txtCredit"), TextBox).Text) * FltExchangeRate
                    End If
                Next

                TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfReceivedPayment - (Math.Abs(CCommon.ToDecimal(dtItems.Compute("sum(monAmountPaid)", ""))) * FltExchangeRate)
                If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                    TotalAmountVariationDueToCurrencyExRate = 0
                End If
            End If


            For Each dr As DataRow In dtItems.Rows
                objJE = New JournalEntryNew()
                objJE.TransactionId = CCommon.ToLong(dr("numTransactionID")) 'CCommon.ToLong(hfHeaderTransactionId.Value)
                objJE.DebitAmt = 0
                objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(dr("monAmountPaid")) * CCommon.ToDecimal(dr("fltExchangeRateReceivedPayment")))
                If CCommon.ToLong(dr("numChildDepositID")) > 0 Then
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID")) 'Undeposited funds to bank account
                Else ' New deposit
                    objJE.ChartAcntId = CCommon.ToLong(dr("numAccountID"))
                End If
                If CCommon.ToLong(dr("numChildDepositID")) > 0 Then
                    objJE.Description = "Amount Deposited from UnDeposited Funds"
                Else
                    objJE.Description = CCommon.ToString(dr("vcMemo"))
                End If

                objJE.CustomerId = CCommon.ToLong(dr("numReceivedFrom"))
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = CCommon.ToString(dr("vcReference"))
                objJE.PaymentMethod = CCommon.ToLong(dr("numPaymentMethod"))
                objJE.Reconcile = False
                objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = CCommon.ToLong(dr("numProjectID"))
                objJE.ClassID = CCommon.ToLong(dr("numClassID"))
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.DepositDetail
                objJE.ReferenceID = CCommon.ToLong(dr("numDepositeDetailID"))
                objJEList.Add(objJE)
                'End If
            Next



            'To Add the Debit Amount in JournalDetails table [i.e] For Bank Account Selected
            objJE = New JournalEntryNew()
            objJE.TransactionId = CCommon.ToLong(dtItems.Rows(0)("numTransactionIDHeader"))
            objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dtItems.Compute("sum(monAmountPaid)", "")) * FltExchangeRate)
            objJE.CreditAmt = 0
            objJE.ChartAcntId = CCommon.ToLong(ddlDepositTo.SelectedValue)
            objJE.Description = "Amount Deposited to Bank A/c" 'CCommon.ToString("Deposit Amount to " + ddlDepositTo.SelectedItem.Text)
            objJE.CustomerId = 0
            objJE.MainDeposit = 1
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objJE.FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.DepositHeader
            objJE.ReferenceID = lngDepositeID
            objJEList.Add(objJE)

            If CCommon.ToLong(ddlCurrency.SelectedValue) > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                If CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And CCommon.ToDouble(txtExchangeRate.Text) > 0.0 Then 'Foreign Currency Transaction

                    Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                    'Credit: Customer A/R With (Amount)
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0

                    ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                    If TotalAmountVariationDueToCurrencyExRate < 0 Then
                        objJE.DebitAmt = 0
                        objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                    ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                        objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                        objJE.CreditAmt = 0
                    End If

                    objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                    objJE.Description = "Exchange Gain/Loss"
                    objJE.CustomerId = 0
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = ""
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                    objJE.FltExchangeRate = FltExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = enmReferenceType.DepositDetail
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If
            End If


            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim dtPaymentMethod As New DataTable
    Private Sub BindPaymentMethodDropdown(ByRef ddlPayment As DropDownList)
        Try
            If dtPaymentMethod.Rows.Count = 0 Then
                dtPaymentMethod = objCommon.GetMasterListItems(31, Session("DomainID"))
            End If
            'Dim ddlPayment As DropDownList = CType(e.Item.FindControl("ddlPayment"), DropDownList)
            ddlPayment.DataTextField = "vcData"
            ddlPayment.DataValueField = "numListItemID"
            ddlPayment.DataSource = dtPaymentMethod
            ddlPayment.DataBind()
            ddlPayment.Items.Insert(0, "--Select One--")
            ddlPayment.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim dtProject As DataTable
    Private Sub BindProjectDropDown(ByRef ddlProject As DropDownList)
        Try
            If dtProject Is Nothing Then
                Dim objProject As New Project
                objProject.DomainID = Session("DomainID")
                dtProject = objProject.GetOpenProject()
            End If

            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtProject
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindCOADropdown(ByVal ddlAccounts As DropDownList)
        Try
            If dtChartAcnt.Rows.Count = 0 Then
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = "" '"0103" 'income
                dtChartAcnt = objCOA.GetParentCategory()

                'objCOA.AccountCode = "0101" ' Asset
                'Dim dt1 As DataTable = objCOA.GetParentCategory()
                ''Exclude AR 
                'Dim drArray() As DataRow = dt1.Select("vcAccountTypeCode <> '01010105'")
                'If drArray.Length > 0 Then
                '    dt1 = drArray.CopyToDataTable()
                'End If
                'dtChartAcnt.Merge(dt1)

                'objCOA.AccountCode = "0105" ' Equity -Task 240
                'dt1 = objCOA.GetParentCategory()
                'dtChartAcnt.Merge(dt1)
                'dtChartAcnt.AcceptChanges()
            End If



            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccounts.Items.Add(item)
            Next
            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDepositToDropDown()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "010101" ' current asset
            dtChartAcntDetails = objCOA.GetParentCategory()

            'Exclude AR As per discussion with jeff. since it created AR aging summary report balance off.
            Dim drArray() As DataRow = dtChartAcntDetails.Select("vcAccountTypeCode <> '01010105'")
            If drArray.Length > 0 Then
                dtChartAcntDetails = drArray.CopyToDataTable()
            End If


            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlDepositTo.Items.Add(item)
            Next
            ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgUndepositedFunds_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgUndepositedFunds.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objJournalEntry As New JournalEntry
                objJournalEntry.JournalId = 0
                objJournalEntry.BillPaymentID = 0
                objJournalEntry.DepositId = CCommon.ToLong(e.CommandArgument)
                objJournalEntry.CheckHeaderID = 0
                objJournalEntry.BillID = 0
                objJournalEntry.CategoryHDRID = 0
                objJournalEntry.DomainID = Session("DomainId")
                Try
                    objJournalEntry.DeleteJournalEntryDetails()
                    BindUnDepositedPaymentGrid("", "")
                    litMessage.Text = "Record deleted sucessfully."
                Catch ex As Exception
                    If ex.Message = "BILL_PAID" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                        Exit Sub
                    ElseIf ex.Message = "Undeposited_Account" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgUndepositedFunds_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgUndepositedFunds.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Try
                    Dim ddlPayment As DropDownList
                    ddlPayment = CType(e.Item.FindControl("ddlPayment"), DropDownList)
                    BindPaymentMethodDropdown(ddlPayment)
                    If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPaymentMethod")) > 0 Then
                        If Not ddlPayment.Items.FindByValue(CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPaymentMethod"))) Is Nothing Then
                            ddlPayment.ClearSelection()
                            ddlPayment.Items.FindByValue(CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPaymentMethod"))).Selected = True
                        End If
                    End If

                    ddlPayment.Enabled = False

                    CType(e.Item.FindControl("hlReceivedFrom"), HyperLink).NavigateUrl = String.Format("../Opportunity/frmAmtPaid.aspx?frm=Deposit&DivId={0}&DepositeID={1}&MakeDepositId={2}",
                                                                                                       CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numDivisionID")), CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numDepositID")), lngDepositeID)

                Catch ex As Exception
                    Throw ex
                End Try

            ElseIf e.Item.ItemType = ListItemType.Header Then

                Try
                    Dim ddlPaymentFilter As New DropDownList
                    ddlPaymentFilter = CType(e.Item.FindControl("ddlPaymentFilter"), DropDownList)
                    BindPaymentMethodDropdown(ddlPaymentFilter)
                    If CCommon.ToLong(hdnPaymentMethodID.Value) > 0 Then
                        If Not ddlPaymentFilter.Items.FindByValue(CCommon.ToLong(hdnPaymentMethodID.Value)) Is Nothing Then
                            ddlPaymentFilter.ClearSelection()
                            ddlPaymentFilter.Items.FindByValue(CCommon.ToLong(hdnPaymentMethodID.Value)).Selected = True
                        End If
                    End If

                    Dim ddlCardTypeFilter As New DropDownList
                    ddlCardTypeFilter = CType(e.Item.FindControl("ddlCardTypeFilter"), DropDownList)
                    objCommon.sb_FillComboFromDB(ddlCardTypeFilter, 120, Session("DomainID"))
                    ddlCardTypeFilter.Items.Insert(0, New ListItem("--Select One--", "0"))

                    If Not String.IsNullOrEmpty(CCommon.ToString(hdnCardType.Value)) Then
                        If Not ddlCardTypeFilter.Items.FindByText(CCommon.ToString(hdnCardType.Value)) Is Nothing Then
                            ddlCardTypeFilter.ClearSelection()
                            ddlCardTypeFilter.Items.FindByText(CCommon.ToString(hdnCardType.Value)).Selected = True
                        End If
                    End If

                    Dim ddlSourceFilter As New DropDownList
                    ddlSourceFilter = CType(e.Item.FindControl("ddlSourceFilter"), DropDownList)

                    If ddlSourceFilter.Items.Count = 0 Then
                        If Session("Source") IsNot Nothing AndAlso DirectCast(Session("Source"), DataTable).Rows.Count > 0 Then
                            dtSource = DirectCast(Session("Source"), DataTable)
                        Else
                            objCommon.DomainID = Session("DomainID")
                            dtSource = objCommon.GetOpportunitySource()
                            Session("Source") = dtSource
                        End If

                        ddlSourceFilter.DataTextField = "vcItemName"
                        ddlSourceFilter.DataValueField = "numItemID"
                        ddlSourceFilter.DataSource = dtSource
                        ddlSourceFilter.DataBind()
                        ddlSourceFilter.Items.Insert(0, New ListItem("--Select One--", "0"))

                        If Not String.IsNullOrEmpty(CCommon.ToString(hdnSource.Value)) Then
                            If Not ddlSourceFilter.Items.FindByText(CCommon.ToString(hdnSource.Value)) Is Nothing Then
                                ddlSourceFilter.ClearSelection()
                                ddlSourceFilter.Items.FindByText(CCommon.ToString(hdnSource.Value)).Selected = True
                            End If
                        End If
                    End If

                    Dim txtmemo As TextBox = CType(e.Item.FindControl("txtMemoFliter"), TextBox)
                    Dim txtReference As TextBox = CType(e.Item.FindControl("txtReferenceFliter"), TextBox)
                    Dim txtReceivedFromFliter As TextBox = CType(e.Item.FindControl("txtReceivedFromFliter"), TextBox)
                    'Dim RadDate As RadDatePicker = CType(e.Item.FindControl("RadCalDate"), RadDatePicker)
                    Dim txtFromDate As TextBox = CType(e.Item.FindControl("txtFromDate"), TextBox)
                    Dim txtToDate As TextBox = CType(e.Item.FindControl("txtToDate"), TextBox)
                    txtmemo.Text = hdnMemo.Value
                    txtReference.Text = hdnReference.Value
                    txtReceivedFromFliter.Text = hdnReceivedFrom.Value
                    'If Not CCommon.ToString(hdnDate.Value) = "" Then
                    '    RadDate.SelectedDate = hdnDate.Value
                    '    hdnDate.Value = ""
                    'End If
                    If Not CCommon.ToString(hdnFromDate.Value) = "" Then
                        txtFromDate.Text = hdnFromDate.Value
                    End If
                    If Not CCommon.ToString(hdnToDate.Value) = "" Then
                        txtFromDate.Text = hdnFromDate.Value
                        txtToDate.Text = hdnToDate.Value
                    End If
                Catch ex As Exception
                    Throw ex
                End Try

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SplitTransaction()
        Try
            Dim ldecDepositTotalAmount As Decimal

            Dim obj As GridMasterRegular = Page.Master
            obj.HideWebMenuUserControl = True

            If CCommon.ToLong(hdnTransactionID.Value) > 0 Then
                objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                Dim dtTransactionDetail As DataTable = objEBanking.GetTransactionDetails()

                If (dtTransactionDetail.Rows.Count > 0) Then
                    ' lblDepositTotalAmount.Text = CCommon.ToString(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))))
                    hdnDepositTotalAmt.Value = CCommon.ToString(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))))


                    Dim COAccountID As Long = CCommon.ToLong(dtTransactionDetail.Rows(0)("numAccountID"))

                    If COAccountID > 0 Then
                        If ddlDepositTo.Items.FindByValue(COAccountID) IsNot Nothing Then
                            ddlDepositTo.ClearSelection()
                            ddlDepositTo.Items.FindByValue(COAccountID).Selected = True
                            ddlDepositTo.Enabled = False
                        End If

                    End If

                    If hdnDepositTotalAmt.Value = "" Then
                        ldecDepositTotalAmount = 0
                    Else : ldecDepositTotalAmount = CCommon.ToDecimal(hdnDepositTotalAmt.Value)
                    End If
                    lblSplitTotalAmount.Visible = True
                    lblSplitTotalAmount.Text = ReturnMoney(ldecDepositTotalAmount)
                    btnDepositSaveNew.Visible = False
                    btnSplitSave.Visible = True
                    lblDepositTotalAmount.Visible = False
                    'lblDifferenceAmount.Visible = True

                    'lblDepositTotalAmount.Text = ReturnMoney(ldecDepositTotalAmount)
                    Dim dtDatePosted As DateTime = Convert.ToDateTime(dtTransactionDetail.Rows(0)("dtDatePosted"))
                    calMakeDeposit.SelectedDate = dtDatePosted

                End If


            End If


        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Protected Sub ddlPaymentFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub


    Protected Sub ddlCardTypeFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlSourceFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtMemoFliter_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim txtMemoFltr As TextBox
            txtMemoFltr = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtMemoFliter"), TextBox)
            hdnMemo.Value = txtMemoFltr.Text
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Protected Sub txtReferenceFliter_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim txtReferenceFltr As TextBox
            txtReferenceFltr = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtReferenceFliter"), TextBox)
            hdnReference.Value = txtReferenceFltr.Text
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtReceivedFromFliter_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim txtReceivedFromFliter As TextBox
            txtReceivedFromFliter = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtReceivedFromFliter"), TextBox)
            hdnReceivedFrom.Value = txtReceivedFromFliter.Text
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Protected Sub RadCalDate_SelectedDateChanged(sender As Object, e As Calendar.SelectedDateChangedEventArgs)
    '    Try
    '        Dim RadDate As RadDatePicker
    '        RadDate = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("RadCalDate"), RadDatePicker)
    '        If RadDate.SelectedDate.HasValue Then
    '            hdnDate.Value = RadDate.SelectedDate
    '        End If
    '        BindUnDepositedPaymentGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Private Sub ddlCurrency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCurrency.SelectedIndexChanged
        Try
            ddlCurrency_SelectedIndexChanged()
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub ddlCurrency_SelectedIndexChanged()
        Try
            Dim objCurrency As New CurrencyRates
            objCurrency.DomainID = Session("DomainID")
            objCurrency.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objCurrency.GetAll = 0
            Dim dt As DataTable = objCurrency.GetCurrencyWithRates()
            If dt.Rows.Count > 0 Then
                lblBaseCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                lblForeignCurr.Text = CCommon.ToString(dt.Rows(0)("varCurrSymbol")).Trim()
                txtExchangeRate.Text = CCommon.ToString(dt.Rows(0)("fltExchangeRate"))
                lblDepositTotal.Text = "Deposit Total: " & CCommon.ToString(dt.Rows(0)("varCurrSymbol")).Trim()
            Else
                txtExchangeRate.Text = "1"
                lblForeignCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                lblDepositTotal.Text = "Deposit Total: " & CCommon.ToString(Session("Currency")).Trim()
            End If
            'If Session("BaseCurrencyID") <> CCommon.ToString(ddlCurrency.SelectedValue) Then
            '    dgNewDeposits.Visible = False
            'Else
            '    dgNewDeposits.Visible = True
            'End If
            If Session("BaseCurrencyID") = CCommon.ToString(ddlCurrency.SelectedValue) Then
                txtExchangeRate.Text = 1
                txtExchangeRate.Enabled = False
            Else
                txtExchangeRate.Enabled = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindMultiCurrency()
        Try
            If Session("MultiCurrency") = True Then
                trMultiCurrency.Visible = True
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                ddlCurrency.DataTextField = "vcCurrencyDesc"
                ddlCurrency.DataValueField = "numCurrencyID"
                ddlCurrency.DataBind()
                'ddlCurrency.Items.Insert(0, "--Select One--")
                'ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                    ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    ddlCurrency_SelectedIndexChanged()
                End If
            Else
                trMultiCurrency.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function PerformValidations() As Boolean
        Try
            If Session("MultiCurrency") = True Then
                Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                If lngDefaultForeignExchangeAccountID = 0 Then
                    litMessage.Text = "Please Map Default Foreign Exchange Gain/Loss Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" ."
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If lngDepositeID > 0 Then
                Dim objJournalEntry As New JournalEntry
                objJournalEntry.JournalId = 0
                objJournalEntry.BillPaymentID = 0
                objJournalEntry.DepositId = lngDepositeID
                objJournalEntry.CheckHeaderID = 0
                objJournalEntry.BillID = 0
                objJournalEntry.CategoryHDRID = 0
                objJournalEntry.ReturnID = 0
                objJournalEntry.DomainID = Session("DomainId")
                objJournalEntry.UserCntID = Session("UserContactID")

                Try
                    objJournalEntry.DeleteJournalEntryDetails()
                    Response.Redirect("../Accounting/frmMakeDeposit.aspx?flag=1", False)
                Catch ex As Exception
                    If ex.Message = "Undeposited_Account" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnDepositSaveNew_Click(sender As Object, e As EventArgs) Handles btnDepositSaveNew.Click
        btnDepositSave_ClickExtracted()
    End Sub

    Sub bindUserLevelClassTracking()
        Try
            pnlAccountingClass.Visible = False
            Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))

            If intDefaultClassType > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlUserLevelClass.DataTextField = "ClassName"
                    ddlUserLevelClass.DataValueField = "numChildClassID"
                    ddlUserLevelClass.DataSource = dtClass
                    ddlUserLevelClass.DataBind()
                    ddlUserLevelClass.Items.Insert(0, New ListItem("-- Select One --", "0"))
                    pnlAccountingClass.Visible = True

                    If intDefaultClassType = 1 AndAlso ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then
                        ddlUserLevelClass.ClearSelection()
                        ddlUserLevelClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlUserLevelClass_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserLevelClass.SelectedIndexChanged
        Try
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Protected Sub txtFromDate_TextChanged(sender As Object, e As EventArgs)
        Try
            hdnFromDate.Value = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtFromDate"), TextBox).Text
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub txtToDate_TextChanged(sender As Object, e As EventArgs)
        Try
            hdnToDate.Value = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtToDate"), TextBox).Text
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ClearGridFilters()
        Dim txtReceivedFromFliter As TextBox = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtReceivedFromFliter"), TextBox)
        Dim txtFromDate As TextBox = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtFromDate"), TextBox)
        Dim txtToDate As TextBox = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtToDate"), TextBox)
        Dim ddlPaymentFilter As DropDownList = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("ddlPaymentFilter"), DropDownList)
        Dim ddlCardTypeFilter As DropDownList = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("ddlCardTypeFilter"), DropDownList)
        Dim ddlSourceFilter As DropDownList = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("ddlSourceFilter"), DropDownList)
        Dim txtMemoFliter As TextBox = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtMemoFliter"), TextBox)
        Dim txtReferenceFliter As TextBox = CType(dgUndepositedFunds.Controls(0).Controls(0).FindControl("txtReferenceFliter"), TextBox)

        txtReceivedFromFliter.Text = ""
        txtFromDate.Text = ""
        txtToDate.Text = ""
        ddlPaymentFilter.SelectedValue = "0"
        ddlCardTypeFilter.SelectedValue = "0"
        ddlSourceFilter.SelectedValue = "0"
        txtMemoFliter.Text = ""
        txtReferenceFliter.Text = ""
        hdnReceivedFrom.Value = ""
        hdnFromDate.Value = ""
        hdnToDate.Value = ""
        hdnFromDate.Value = ""
        hdnPaymentMethodID.Value = ""
        hdnCardType.Value = ""
        hdnSource.Value = ""
        hdnMemo.Value = ""
        hdnReference.Value = ""

    End Sub

    Protected Sub btnGo1_Click1(sender As Object, e As EventArgs)
        Try
            ClearGridFilters()
            BindUnDepositedPaymentGrid("", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub dgUndepositedFunds_SortCommand(source As Object, e As DataGridSortCommandEventArgs)

        Dim sortData As String()
        If (Session("sortdirection") IsNot Nothing) Then
            sortData = Session("sortdirection").ToString().Trim().Split("_")

            If (e.SortExpression = sortData(0).ToString) Then
                If (sortData(1) = "ASC") Then

                    BindUnDepositedPaymentGrid(e.SortExpression, "DESC")
                    Session("sortdirection") = e.SortExpression + " " + "DESC"
                Else
                    BindUnDepositedPaymentGrid(e.SortExpression, "ASC")
                    Session("sortdirection") = e.SortExpression + " " + "ASC"

                End If
            Else
                BindUnDepositedPaymentGrid(e.SortExpression, "ASC")
                Session("sortdirection") = e.SortExpression + "_" + "ASC"
            End If
        Else
            BindUnDepositedPaymentGrid(e.SortExpression, "ASC")
            Session("sortdirection") = e.SortExpression + "_" + "ASC"
        End If

    End Sub
End Class