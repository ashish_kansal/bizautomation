﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class frmBankReconcileList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                LoadChartType()
                btnReconcileNow.Attributes.Add("onclick", "return ValidateAccount()")

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    If Not ddlAccount.Items.FindByValue(CCommon.ToString(PersistTable(ddlAccount.ID))) Is Nothing Then
                        ddlAccount.Items.FindByValue(CCommon.ToString(PersistTable(ddlAccount.ID))).Selected = True

                        BindBankReconcileList()
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Sub LoadChartType()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim dt, dt1 As DataTable
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "01010101" 'bank accounts
            dt = objCOA.GetParentCategory()

            objCOA.AccountCode = "01020103" 'For credit card liability
            dt1 = objCOA.GetParentCategory()
            dt.Merge(dt1)

            Dim item As ListItem
            For Each dr As DataRow In dt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccount.Items.Add(item)
            Next
            ddlAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindBankReconcileList()
        Try
            PersistTable.Clear()
            PersistTable.Add(ddlAccount.ID, ddlAccount.SelectedValue)
            PersistTable.Save()

            Dim objBankReconcile As New BankReconcile
            objBankReconcile.DomainID = Session("DomainId")
            objBankReconcile.UserCntID = Session("UserContactID")
            objBankReconcile.Mode = 7
            objBankReconcile.AcntId = ddlAccount.SelectedValue

            Dim ds As DataSet = objBankReconcile.ManageBankReconcileMaster()

            Dim dtReconcile As DataTable = ds.Tables(0)

            gvReconcile.DataSource = dtReconcile
            gvReconcile.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlAccount_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            BindBankReconcileList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnReconcileNow_Click(sender As Object, e As System.EventArgs) Handles btnReconcileNow.Click
        Try
            Response.Redirect("../Accounting/frmBankReconcile.aspx?AcntId=" & ddlAccount.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvReconcile_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReconcile.RowCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objBankReconcile As New BankReconcile
                objBankReconcile.DomainID = Session("DomainId")
                objBankReconcile.ReconcileID = CCommon.ToLong(e.CommandArgument)
                objBankReconcile.DeleteBankReconcile()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvReconcile_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReconcile.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblDifference As Label = e.Row.FindControl("lblDifference")

                Dim decDiff As Decimal = 0
                decDiff = CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "monEndBalance")) - (CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "monBeginBalance")) + CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "monDeposit")) - CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "monPayment")))

                lblDifference.Text = String.Format("{0:##,#00.00}", Math.Truncate(decDiff * 100) / 100)

                If (Math.Truncate(decDiff * 100) / 100) <> 0 Then
                    lblDifference.ForeColor = Color.Red
                End If

                If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitReconcileComplete")) Then
                    e.Row.FindControl("hplReport").Visible = True
                    e.Row.FindControl("lkbDelete").Visible = False
                Else
                    e.Row.FindControl("hplReport").Visible = False
                    e.Row.FindControl("lkbDelete").Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Private Sub gvReconcile_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvReconcile.RowDeleting
        Try
            BindBankReconcileList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class