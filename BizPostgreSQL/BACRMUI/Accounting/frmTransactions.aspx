<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTransactions.aspx.vb" Inherits=".frmTransactions" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Transaction Report</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>    
     <script language="javascript" type="text/javascript" >
      function OpenJournalDetailsPage(url)
		{
		    window.location.href=url;
			return false; 
		}
		function OpenBizIncome(url)
		{
		// alert(url);
		  window.open(url,"BizInvoice",'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
		   return false;
		}
		
		function OpenTimeExpense(url)
		{
		// alert(url);
		  window.open(url,"TimeExoenses",'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
		   return false;
		}
		function OpenAddTimeExpense(url)
		{
		 
		  window.open(url,"TimeExoenses",'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
		   return false;
		}
		function reDirect(a)
        {
            document.location.href =a;
        }
	</script>
</head>
<body>
    <form id="form1" runat="server">
       <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
   <br />
  <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					
					<td valign="bottom" width ="400px">
						<table class="TabStyle" >
							<tr>
					    		<td >&nbsp;&nbsp;&nbsp;Transaction Report &nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
						<td width ="40px" class="normal1">&nbsp;&nbsp;From</td>
						<td width ="40px" class="normal1"><BizCalendar:Calendar ID="calFrom" runat="server" /></td>
						<td width ="20px" class="normal1">&nbsp;&nbsp;&nbsp;To</td>
						<td width ="40px" class="normal1"><BizCalendar:Calendar ID="calTo" runat="server" /></td>
						<td  valign="middle">
						&nbsp;&nbsp;&nbsp;<asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>
						</td>
						<td align="right"><asp:button id="btnBack" Runat="server" CssClass="button" Text="Back"></asp:button>
						</td>
				</tr>
			</TABLE>
			
	<asp:table id="tblGeneralLeger" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
						BorderColor="black" GridLines="None" Height="300">
		 
		<asp:TableRow  Width="100%">
		<asp:TableCell VerticalAlign="top" >
		<table cellpadding="0" width="100%" cellspacing="0" border ="0" bgcolor="white" >
             <tr valign="top" width ="100%"><td valign ="top">
		    
          <asp:Repeater id="RepTransactionReport" runat="server">
         		
         		<HeaderTemplate>
				<table cellpadding="2" cellspacing="2"  border ="0" width ="100%">
				<tr class ="normal5">
				<td bgcolor="#52658C" align ="center" ><b>Date</b></td>
				<td bgcolor="#52658C" align ="center"><b>Type</b></td>
				<td bgcolor="#52658C" align ="center"><b>Name</b></td>
				<td bgcolor="#52658C" align ="center"><b>Memo/Description</b></td>
				<td bgcolor="#52658C" align ="center"><b>Split</b></td>
				<td bgcolor="#52658C" align ="center"><b>Amount</b></td>
				<td bgcolor="#52658C" align ="center"><b>Balance</b></td>
		        </tr>
		 		</HeaderTemplate>
		 		<ItemTemplate>				
		 		<tr class="normal1">				
		  	    <td><%# DataBinder.Eval(Container.DataItem,"Date") %></td>
		 	    <td><asp:LinkButton id="lnkType" Runat="server" CssClass="hyperlink" CommandName ="Edit"><%# DataBinder.Eval(Container.DataItem,"Type")%> </asp:LinkButton></td>
				<td><%# DataBinder.Eval(Container.DataItem,"Name") %></td> 
				<td><%# DataBinder.Eval(Container.DataItem,"Memo/Description") %></td> 
				<td><%# DataBinder.Eval(Container.DataItem,"Split") %></td> 
				<td><%#DataBinder.Eval(Container.DataItem, "Amount")%></td> 
				<td><%#DataBinder.Eval(Container.DataItem, "Balance")%> 
				 <asp:Label ID="lblJournalId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"JournalId")%>'></asp:Label>
				 <asp:Label ID="lblCheckId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"CheckId")%>'></asp:Label> 
				 <asp:Label ID="lblCashCreditCardId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"CashCreditCardId")%>'></asp:Label>  
				 <asp:Label ID="lblChartAcntId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"numChartAcntId")%>'></asp:Label> 
				 <asp:Label ID="lblTransactionId" style="display:none" runat ="server"  Text ='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
				 <asp:Label ID="lblOppId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"numOppId")%>'></asp:Label> 
				 <asp:Label ID="lblOppBizDocsId" style="display:none" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"numOppBizDocsId")%>'></asp:Label>
				 <asp:Label ID="lblDepositId" style="display:none" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"numDepositId")%>'></asp:Label>  
				 <asp:Label ID="lblCategoryHDRID"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"numCategoryHDRID")%>'></asp:Label>  
				 <asp:Label ID="lblTEType" style="display:none" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"tintTEType")%>'></asp:Label>  
				 <asp:Label ID="lblCategory"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"numCategory")%>'></asp:Label>
				<asp:Label ID="lblUserCntID"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"numUserCntID")%>'></asp:Label> 
				<asp:Label ID="lblFromDate"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"dtFromDate")%>'></asp:Label> </td>
				
 
				 </tr>
			 	</ItemTemplate>
			 	<AlternatingItemTemplate >
				<tr bgcolor="#C6D3E7" class="normal1" >
			 	<td><%# DataBinder.Eval(Container.DataItem,"Date") %></td>
			    <td><asp:LinkButton id="lnkType" Runat="server" CssClass="hyperlink" CommandName ="Edit"><%# DataBinder.Eval(Container.DataItem,"Type")%></asp:LinkButton></td>
				<td><%# DataBinder.Eval(Container.DataItem,"Name") %></td> 
				<td><%# DataBinder.Eval(Container.DataItem,"Memo/Description") %></td> 
				<td><%#DataBinder.Eval(Container.DataItem, "Split")%></td> 
				<td><%#DataBinder.Eval(Container.DataItem, "Amount")%></td> 
				<td><%# DataBinder.Eval(Container.DataItem,"Balance") %> 
                <asp:Label ID="lblJournalId" style="display:none" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"JournalId")%>' ></asp:Label> 
				<asp:Label ID="lblCheckId" style="display:none"  runat ="server"  Text ='<%# DataBinder.Eval(Container.DataItem,"CheckId")%>'></asp:Label> 
				<asp:Label ID="lblCashCreditCardId" style="display:none"  runat ="server"  Text ='<%# DataBinder.Eval(Container.DataItem,"CashCreditCardId")%>'></asp:Label> 
				<asp:Label ID="lblChartAcntId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"numChartAcntId")%>'></asp:Label> 
				<asp:Label ID="lblTransactionId" style="display:none"  runat ="server"  Text ='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>		
				<asp:Label ID="lblOppId" style="display:none"  runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"numOppId")%>'></asp:Label> 
				 <asp:Label ID="lblOppBizDocsId" style="display:none" runat ="server"  Text ='<%# DataBinder.Eval(Container.DataItem,"numOppBizDocsId")%>'></asp:Label> 
				<asp:Label ID="lblDepositId" style="display:none" runat ="server"  Text ='<%# DataBinder.Eval(Container.DataItem,"numDepositId")%>'></asp:Label>  
				 <asp:Label ID="lblCategoryHDRID" runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"numCategoryHDRID")%>'></asp:Label>  
				  <asp:Label ID="lblTEType" style="display:none" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"tintTEType")%>'></asp:Label>  
				 <asp:Label ID="lblCategory"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"numCategory")%>'></asp:Label>  
				 <asp:Label ID="lblUserCntID"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"numUserCntID")%>'></asp:Label> 
				<asp:Label ID="lblFromDate"  runat ="server" style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem,"dtFromDate")%>'></asp:Label> </td>
 
				</tr>
				</AlternatingItemTemplate>
				</asp:Repeater>
           </td>	</tr> </table>  
		</asp:TableCell>
		</asp:TableRow>
		</asp:table> 
    </form>
</body>
</html>
