<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBankReconcile.aspx.vb"
    Inherits=".frmBankReconcile" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Bank Reconcilation</title>
    <script type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function Save() {
            if (document.getElementById('ddlAccount').value == 0) {
                alert("Please Select Account");
                document.getElementById('ddlAccount').focus();
                return false;
            }
            if (document.getElementById('txtBeginingBalance').value == "") {
                alert("Enter Begining Balance");
                document.getElementById('txtBeginingBalance').focus();
                return false;
            }
            if (document.getElementById('txtEndingBalance').value == "") {
                alert("Enter Ending Balance");
                document.getElementById('txtEndingBalance').focus();
                return false;
            }
            if ($("[id$=calStatementDate_txtDate]").val() == 0) {
                alert("Enter Statement Date")
                $("[id$=calStatementDate_txtDate]").focus();
                return false;
            }

            if (document.getElementById('txtServiceCharge').value != "" && parseFloat(document.getElementById('txtServiceCharge').value) != 0) {
                if (document.getElementById('ddlServiceChargeAcnt').value == 0) {
                    alert("Please Select Service Charge Account");
                    document.getElementById('ddlServiceChargeAcnt').focus();
                    return false;
                }

                if ($('[id$=calServiceChargeDate_txtDate]').val() == 0) {
                    alert("Enter Service Charge Date")
                    $('[id$=calServiceChargeDate_txtDate]').focus();
                    return false;
                }
            }


            if (document.getElementById('txtInterestEarned').value != "" && parseFloat(document.getElementById('txtInterestEarned').value) != 0) {
                if (document.getElementById('ddlInterestEarned').value == 0) {
                    alert("Please Select Interest Earned Account");
                    document.getElementById('ddlInterestEarned').focus();
                    return false;
                }

                if ($('[id$=calInterestEarned_txtDate]').val() == 0) {
                    alert("Enter Interest Earned Date")
                    $('[id$=calInterestEarned_txtDate]').focus();
                    return false;
                }
            }
        }

        function MoveUp() {
            if ($('#lstColumns :selected').length > 0) {
                $('#lstColumns :selected').each(function (i, selected) {
                    if (!$(this).prev().length) return false;
                    $(this).insertBefore($(this).prev());
                });
                $('#lstColumns').focus().blur();
            }
            else {
                alert("select any column to change order.")
            }


        }

        function MoveDown() {
            if ($('#lstColumns :selected').length > 0) {
                $($('#lstColumns :selected').get().reverse()).each(function (i, selected) {
                    if (!$(this).next().length) return false;
                    $(this).insertAfter($(this).next());
                });
                $('#lstColumns').focus().blur();
            }
            else {
                alert("select any column to change order.")
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:UpdatePanel runat="server" class="pull-right">
                <ContentTemplate>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Account</label>
                <asp:DropDownList CssClass="form-control" ID="ddlAccount" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group" id="divFile" runat="server">
                <label>Choose File<span id="lblAvgCostToolTip" title="First row of file should be header row. It header row is not added to file than first row will not be available in reconciliation" class="tip">[?]</span></label>
                <asp:FileUpload ID="fuStatement" runat="server" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group" id="divFileColumns" runat="server">
                <label>File must have following columns</label>
                <div class="form-inline">
                    Date(MM/dd/yyyy), Reference, Amount, Payee, Description
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <b>Enter the following from your statement</b>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Statement End Date</label>
                <BizCalendar:Calendar ID="calStatementDate" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Beginning Balance</label>
                <asp:TextBox ID="txtBeginingBalance" runat="server" MaxLength="20" AutoComplete="OFF"
                    CssClass="form-control required_money {required:true ,number:true,maxlength:20, messages:{required:'Please provide value!',number:'provide valid length!'}} money"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Ending Balance</label>
                <asp:TextBox ID="txtEndingBalance" runat="server" MaxLength="20" AutoComplete="OFF"
                    CssClass="form-control required_money {required:true ,number:true,maxlength:20, messages:{required:'Please provide value!',number:'provide valid length!'}} money"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <b>Enter service charges and interest earned, if any</b>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Service Charge</label>
                <asp:TextBox ID="txtServiceCharge" runat="server" MaxLength="20" AutoComplete="OFF"
                    CssClass="form-control required_decimal {required:true ,number:true,maxlength:20, messages:{required:'Please provide value!',number:'provide valid length!'}} money"
                    Text="0.00"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Date</label>
                <BizCalendar:Calendar ID="calServiceChargeDate" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Account</label>
                <asp:DropDownList CssClass="form-control" ID="ddlServiceChargeAcnt" runat="server">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Interest Earned</label>
                <asp:TextBox ID="txtInterestEarned" runat="server" MaxLength="20" AutoComplete="OFF"
                    CssClass="form-control required_decimal {required:true ,number:true,maxlength:20, messages:{required:'Please provide value!',number:'provide valid length!'}} money"
                    Text="0.00"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Date</label>
                <BizCalendar:Calendar ID="calInterestEarned" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label>Account</label>
                <asp:DropDownList CssClass="form-control" ID="ddlInterestEarned" runat="server">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfReconcileID" runat="server" Value="0" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Bank Reconcilation
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
