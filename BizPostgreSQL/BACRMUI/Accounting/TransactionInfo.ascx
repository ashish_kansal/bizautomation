﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TransactionInfo.ascx.vb"
    Inherits=".TransactionInfo" %>

<script type="text/javascript">
    function TransactionDetail(TransactionID) {
        var URL = "../EBanking/frmTransactionDetails.aspx?TransactionID=" + TransactionID
        window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
        return false;
    }

</script>

<asp:HiddenField ID="hdnIsReconciled" runat="server" Value="0" />
<asp:HiddenField ID="hdnJournalID" runat="server" Value="0" />
<asp:HiddenField ID="hdnTransactionID" runat="server" Value="0" />
<asp:HiddenField ID="hdnHeaderTransactionID" runat="server" Value="0" />


<asp:Panel runat="server" ID="pnlMatchedTrans" Visible="false" class="box box-solid box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Online Banking Status</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body text-center">
        <div class="form-inline">
            <label>Manually Added:</label>
            <asp:LinkButton runat="server" ID="lnkbtnTransMatchDetails"></asp:LinkButton>
            <asp:Button runat="server" CssClass="btn btn-sm btn-primary" ID="btnUnMatch" Text="Unmatch" />
        </div>
    </div>
    <!-- /.box-body -->
</asp:Panel>
