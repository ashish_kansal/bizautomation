﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmBankReconciliationFinish.aspx.vb" Inherits=".frmBankReconciliationFinish" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Bank Reconcilation Finish</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                 <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Create Adjustment for difference
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="row text-center">
        <div class="col-xs-12 padbottom10">
            There is a difference of
                    <asp:Label ID="lblDiff" runat="server" Font-Bold="true"></asp:Label>
            between the total of the selected transactions and the ending balance.<br />
            <br />
            Click Add Adjustment to adjust the account balance. The adjustment will be posted
                    to the Reconciliation Discrepancies account.
                    <br />
            <br />
            Or Click Cancel to return without making an adjustment.
        </div>
        <div class="col-xs-12">
            <asp:Button ID="btnAdjustment" runat="server" Text="Add Adjustment" CssClass="btn btn-primary" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
        </div>
        <asp:HiddenField ID="hfDifference" runat="server" />
        <asp:HiddenField ID="hfServiceChargeAmount" runat="server" />
        <asp:HiddenField ID="hfInterestEarnedAmount" runat="server" />
        <asp:HiddenField ID="hfChartAcntId" runat="server" />
        <asp:HiddenField ID="hfStatementDate" runat="server" />
    </div>
</asp:Content>
