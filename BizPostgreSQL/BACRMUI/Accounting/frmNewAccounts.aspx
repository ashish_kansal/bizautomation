<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewAccounts.aspx.vb"
    EnableEventValidation="false" Inherits=".frmNewAccounts" MasterPageFile="~/common/PopupBootstrap.Master"
    ClientIDMode="Static" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add / Edit Account</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="Javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
            ShowParentAccount();
            //if (document.getElementById("chkIsBankAccount").checked == true) {
            //alert("From PageLoad");
            ShowBankSegment();
            // }
        });

        function Save() {

            if (document.getElementById("ddlType").value == 0) {
                alert("Please Select Type");
                document.getElementById("ddlType").focus();
                return false;
            }
            if (document.getElementById("txtName").value == "") {
                alert("Enter Name");
                document.getElementById("txtName").focus();
                return false;
            }
            if (document.getElementById("ddlParentCategory").value == 0) {
                alert("Please Select Parent Type");
                document.getElementById("ddlParentCategory").focus();
                return false;
            }

            if ($('#chkIsSubAccount').is(':checked') == true) {
                if (document.getElementById("ddlParentAccount").value == 0) {
                    alert("Please Select Parent Account");
                    document.getElementById("ddlParentAccount").focus();
                    return false;
                }
            }

            var SelctedType = document.getElementById("ddlType").value.split('~')[0];
            if (SelctedType == '0101' || SelctedType == '0102' || SelctedType == '0105') {
                // Code Changed By Ajit Singh on 25-July-2008
                if (document.getElementById("lblBalanceAmt") == null) {
                    if (document.getElementById("txtOpeningBalance").value == "") {
                        alert("Enter Opening Balance Amt");
                        document.getElementById("txtOpeningBalance").focus();
                        return false;
                    }
                }
                // End of the Code..
            }
            //if parent type = Fixed Asset( starts with 010102
            if (document.getElementById("ddlParentCategory").options[document.getElementById("ddlParentCategory").selectedIndex].text.indexOf('010102') > 0) {
                if (document.getElementById("txtDepreciationCost").value == "") {
                    alert("Enter Depreciation Cost");
                    document.getElementById("txtDepreciationCost").focus();
                    return false;
                }
            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            console.log(k);
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function HideTempPanel() {
            //alert("Siva");
            var a = null;
            var f = document.forms[0];
            var e = f.elements["rdlFixedAsset"];

            for (var i = 0; i < e.length; i++) {
                if (e[i].checked) {
                    a = e[i].value;
                    break;
                }
            }
            // alert(a);
            if (a == 0) {
                document.getElementById("pnlFixedAssetDetails").style.visibility = '';
            }
            if (a == 1) {
                document.getElementById("pnlFixedAssetDetails").style.visibility = 'hidden';
                //alert(document.getElementById("pnlFixedAssetDetails").style.visibility);        
            }
        }

        function ShowParentAccount() {
            if ($('#chkIsSubAccount') == null) {
                $('#trParentAccount').hide();
            }
            else {
                if ($('#chkIsSubAccount').is(':checked') == true) {
                    $('#trParentAccount').show();
                }
                else {
                    $('#trParentAccount').hide();
                }
            }
        }

        function ShowBankSegment() {
            if ($('#hdnBankDetailID').val() == null || $('#hdnBankDetailID').val() == 0) {
                //alert($('#hdnBankDetailID').val());
                //alert(document.getElementById("chkIsBankAccount").value);
                if ($('#chkIsBankAccount').is(':checked') == true) {
                    $('#divLink').show();
                    $('#divConnection').hide();
                    //alert(1);
                }
                else {
                    $('#divLink').hide();
                    $('#divConnection').hide();
                    //alert(2);
                }

            }
            else {
                $('#divLink').hide();
                $('#divConnection').show();
                //alert(3);
            }
        }
        function OpenBankDetail(AccountID, BankID, BankRoutingNumber) {

            if (document.getElementById("chkIsBankAccount").checked == true && window.opener != null) {
                window.opener.location.href = "../EBanking/frmBankConnectWizard.aspx?AccountID=" + AccountID + "&AccountNo=" + BankRoutingNumber + "&BankId=" + BankID;
                self.close();
                return false;
            }
        }

    </script>
    <style>
        .tblNoBorder tr td{
            border:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td colspan="4">
                <ul id="messagebox" class="errorInfo" style="display: none">
                </ul>
            </td>
        </tr>
        <tr>
            <td class="normal4" valign="middle" colspan="4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
                        <td><label> Balance :</label>&nbsp;<label class="text-success text-md" style="font-size: 30px;font-weight: normal;">$<asp:Label runat="server" ID="lblCompBalance" Text="0.00"></asp:Label></label>
                        </td>
                        <td >
                            
                          <label>  <asp:Label runat="server" ID="lblBank" Text="Bank Balance :"></asp:Label></label><label class="text-success text-md" style="font-size: 30px;font-weight: normal;">$<asp:Label
                                runat="server" ID="lblBankBalance" Text="0.00"></asp:Label></label>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save &amp; Close"></asp:Button>
                            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="Close();"></asp:Button>&nbsp;
                            <asp:Button ID="btnHiddenSave" runat="server" Style="visibility: hidden"></asp:Button>
                        </td>
                    </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add / Edit Account
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="True" />--%>
    <table id="Table2" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
                     CssClass="table tblNoBorder" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <br>
                            <table cellspacing="2" cellpadding="0" class="table tblNoBorder" width="100%" border="0">
                                <tr>
                                    <td style="width:13%"  class="normal1" align="left"><label> Type</label>&nbsp;
                                    </td>
                                    <td colspan="4"  style="width:35%">
                                        <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server"  AutoPostBack="True">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width:10%" class="normal1"><label> Sub-Type</label><font color="red">*</font>
                                    </td>
                                    <td colspan="4" style="width:35%">
                                        <asp:DropDownList CssClass="form-control" ID="ddlParentCategory" runat="server" 
                                            AutoPostBack="true">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="hdnIsConnected" />
                                        <asp:HiddenField runat="server" ID="hdnBankDetailID" />
                                    </td>
                                    <td rowspan="4" valign="top" runat="server" id="tdBank">
                                        <table width="350px">
                                            <tr>
                                                <td>
                                                    <div id="div1">
                                                        <table>
                                                            <tr>
                                                                <td ><label>Is Bank Account</label> 
                                                                </td>
                                                                <td>:
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox ID="chkIsBankAccount" runat="server" onclick="ShowBankSegment();" />
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                    <div id="divLink" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td >
                                                                    <asp:HyperLink runat="server" ID="hrfLinkBankAccount" Text="Link your Bank Account"></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="divConnection" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td ><label>Connected To</label> 
                                                                </td>
                                                                <td>:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblBankName" runat="server" Text="XXXXX"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td ><label> Bank Account #</label> 
                                                                </td>
                                                                <td>:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblAccountNumber" runat="server" Text="XXXXX"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td >
                                                                    <asp:Button ID="btnDeleteBankAccount" UseSubmitBehavior="false" runat="server" Text="Delete Bank Connection" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtType" runat="server"  MaxLength="50" Visible="false"
                                            CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td class="normal1" ><label> Name</label><font color="red">*</font>
                                    </td>
                                    <td colspan="4">
                                        <asp:TextBox ID="txtName" runat="server"  MaxLength="50" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td class="normal1"><label>Number</label> 
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtNumber" runat="server" MaxLength="50" CssClass="form-control required_integer {required:false,number:true messages:{required:'Valid value',number:'Please provide numeric Number !'}}"></asp:TextBox>
                                        
                                    </td>
                                    <td>
                                        <label for="chkIsSubAccount">
                                            Sub-Account</label>&nbsp;<asp:CheckBox runat="server" ID="chkIsSubAccount" onclick="ShowParentAccount();" />
                                    </td>
                                </tr>

                                <tr runat="server" id="trCheckNo" visible="false">
                                    <td class="normal1" ><label> Start Check #</label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="txtStartCheckNumber" CssClass="form-control required_integer {required:false,number:true messages:{required:'Valid value',number:'Please provide numeric Start Check Number !'}}"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trParentAccount" runat="server">
                                    <td class="normal1" ><label> Parent Account</label><font color="red">*</font>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList CssClass="form-control {required:'#chkIsSubAccount:checked', messages:{required:'Select Parent Account!'}}"
                                            ID="ddlParentAccount" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" ><label> Description</label>
                                    </td>
                                    <td colspan="9" class="normal1">
                                        <asp:TextBox ID="txtCategoryDescription" runat="server" TextMode="multiLine" 
                                            MaxLength="50" CssClass="form-control" Rows="1"></asp:TextBox>
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlEquity" runat="server" Visible="false">
                                    <tr>
                                        <td class="normal1" >
                                            <label for="chkProfitLoss">
                                                Is Profit / Loss Account ?</label>
                                        </td>
                                        <td colspan="3" class="normal1">
                                        <td colspan="3" class="normal1">
                                            <asp:CheckBox ID="chkProfitLoss" runat="server" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlDepriciation" runat="server">
                                <tr>
                                    <td class="normal1" >
                                        <asp:Label ID="lblDepreciation" runat="server" Text="Depreciation"></asp:Label>
                                    </td>
                                    <td colspan="3" class="normal1">
                                        <asp:Label ID="lblDepTrack" runat="server" Text="Do you want to track depreciation of this asset?"></asp:Label>
                                    </td>
                                </tr>
                                    </asp:Panel>
                                <asp:Panel ID="pnlRadio" runat="server">
                                <tr>
                                    <td></td>
                                    <td colspan="3" class="normal1">
                                        
                                            <asp:RadioButtonList ID="rdlFixedAsset" runat="server" onclick="HideTempPanel();"
                                                RepeatDirection="vertical">
                                                <asp:ListItem Value="0" Selected="true" Text="Yes"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="No"></asp:ListItem>
                                            </asp:RadioButtonList>
                                    </td>
                                </tr>
                                        </asp:Panel>
                                <tr>
                                    
                                        
                                    <td colspan="9">
                                        
                                         <asp:Panel ID="pnlOpenBalance" runat="server" Visible="true">
                                            <div class="form-inline"><label>
                                                <asp:Label ID="lblOpeningBalance" style="margin-right:8px;" runat="server" Text="Opening Balance"></asp:Label></label>&nbsp;
                                                <asp:TextBox ID="txtOpeningBalance" runat="server" Width="75px" MaxLength="10" CssClass="form-control" Text="0"></asp:TextBox>
                                                        <asp:DropDownList ID="ddlBalanceType" runat="server" CssClass="form-control">
                                                            <asp:ListItem Text="Dr" Value="1" Selected="True" />
                                                            <asp:ListItem Text="Cr" Value="2" />
                                                        </asp:DropDownList>
                                                        <asp:Label ID="Label2" Width="30px" Text="as of" runat="server"></asp:Label>
                                                <BizCalendar:Calendar ID="cal" runat="server" />
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    
                                    <td style="text-align: right;">
                                        <label for="chkActive">Is Active?</label>
                                        
                                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" Text="" Visible="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Panel ID="pnlFixedAssetDetails" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="normal1" ><label> Depreciation Cost</label>  &nbsp;
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox ID="txtDepreciationCost" runat="server" Width="75px" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                        <asp:Label ID="Label1" Width="30px" Text=" as of " runat="server"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <BizCalendar:Calendar ID="CalDepreciationCost" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    
                                </tr>
                                <%--<asp:Panel ID="pnlLblOpen" runat="server" Visible="false">
                                    <tr>
                                        <td class="normal1" >
                                        </td>
                                        <td colspan="3" class="normal1">
                                            Balance :
                                            <asp:Label ID="lblBalanceAmt" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>--%>
                      
                            </table>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <table width="100%">
                                
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HdnumListItemId" runat="server" />
</asp:Content>
