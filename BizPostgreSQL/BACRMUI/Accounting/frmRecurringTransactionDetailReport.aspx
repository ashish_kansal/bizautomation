﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRecurringTransactionDetailReport.aspx.vb"
    Inherits=".frmRecurringTransactionDetailReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Recurring Transaction Report</title>
    <style type="text/css">
        .style1
        {
            width: 336px;
        }
        .style2
        {
            width: 699px;
        }
    </style>
    <script type="text/javascript">
        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=RecurringTransactionDetailReport&OpID=" + a;
            document.location.href = str;
        }
        function OpenOppDetail(a, c) {
            window.open('../Accounting/frmRecurringTransactionList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&RecType=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=500,height=360,scrollbars=yes,resizable=yes');
        }
        
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" style="width: 100%">
        <tr>
            <td class="normal1" align="right" height="23">
                &nbsp<asp:LinkButton ID="btnClose" runat="Server" CssClass="btn btn-primary" OnClientClick="javascript:CloseMe();"><i class="fa fa-times-circle-o"></i>&nbsp; Close</asp:LinkButton>&nbsp;
            </td>
        </tr>
    </table>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Recurring Transaction Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="tbl" runat="server" Width="100%" Height="350" GridLines="None" CssClass="aspTable"
        CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgRep" runat="server" AllowSorting="false" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%"
                    AllowCustomPaging="true" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numRecTranSeedId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numRecurringId"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Seed Order">
                            <ItemTemplate>
                                <a href="#" onclick="OpenOpp('<%# Eval("numRecTranSeedId") %>')">
                                    <%#Eval("RecurringName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Frequency" SortExpression="Frequency" HeaderText="Frequency">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="StartDate" SortExpression="StartDate" HeaderText="Start Date">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="EndDate" SortExpression="EndDate" HeaderText="End Date">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TranType" SortExpression="TranType" HeaderText="Event Type">
                        </asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="varRecurringTemplateName" HeaderText="Recurring Template Name"
                            SortExpression="varRecurringTemplateName" CommandName="RecTemp"></asp:ButtonColumn>
                        <asp:TemplateColumn HeaderText="Created Child Records">
                            <ItemTemplate>
                                <a href="#" onclick="OpenOppDetail('<%# Eval("numRecTranSeedId") %>','<%# Eval("tintRecType") %>')">
                                    <%#Eval("numNoTransactions")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ExpectedChildRecords" HeaderText="Expected Child Records to Date"
                            SortExpression="ExpectedChildRecords"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
