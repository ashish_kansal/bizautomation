﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class PaymentHistory
    Inherits System.Web.UI.UserControl

    Public BillID As Long = 0
    Public ReturnID As Long = 0
    Public OppID As Long = 0
    Public OppBizDocID As Long = 0
    Public LandedCostOppId As Long = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objJournalEntry As New JournalEntry
            objJournalEntry.BillID = BillID
            objJournalEntry.ReturnID = ReturnID
            objJournalEntry.OppId = OppID
            objJournalEntry.OppBIzDocID = OppBizDocID
            objJournalEntry.LandedCostOppId = LandedCostOppId
            objJournalEntry.DomainID = Session("DomainId")
            objJournalEntry.UserCntID = Session("UserContactID")
            objJournalEntry.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            Dim ds As DataSet = objJournalEntry.GetOppBizDocsReceivePayment


            Dim dtReceivePayment As DataTable
            dtReceivePayment = ds.Tables(0)

            Dim dtRecordPayment As DataTable = ds.Tables(1)

            'If CCommon.ToShort(dtRecordPayment(0)("tintRefType")) = 1 Then
            '    dgPaymentDetailSalesOrder.DataSource = dtReceivePayment
            '    dgPaymentDetailSalesOrder.DataBind()

            '    dgPaymentDetailSalesOrder.Visible = True
            '    dgPaymentDetails.Visible = False
            'Else
            dgPaymentDetails.DataSource = dtReceivePayment
            dgPaymentDetails.DataBind()
            'End If

            Dim decAmountTotal, decAmtPaid, decAmountDue As Decimal
            If dtRecordPayment.Rows.Count > 0 Then
                decAmountTotal = CCommon.ToDecimal(dtRecordPayment(0)("monAmountTotal"))
                decAmtPaid = CCommon.ToDecimal(dtRecordPayment(0)("monAmtPaid"))
            End If

            decAmountDue = decAmountTotal - decAmtPaid

            If BillID > 0 Or LandedCostOppId > 0 Then
                If decAmtPaid > 0 AndAlso decAmountDue = 0 Then
                    lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Paid</h3><div class=""pull-right text-green"">Full payment has been made.</div>")
                ElseIf decAmtPaid > 0 AndAlso decAmountDue > 0 Then
                    lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-aqua"">Partial payment has been made. The remaining balance is {0}.</div>", ReturnMoney(decAmountDue))
                ElseIf decAmtPaid = 0 Then
                    lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-yellow"">No payments made yet. Full amount still due.</div>", ReturnMoney(decAmountDue))
                End If
            ElseIf ReturnID > 0 Then
                If decAmtPaid > 0 AndAlso decAmountDue = 0 Then
                    lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Paid</h3><div class=""pull-right text-green"">Full refund has been taken.</div>")
                ElseIf decAmtPaid > 0 AndAlso decAmountDue > 0 Then
                    lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-aqua"">Partial refund has been taken. The remaining balance is {0}.</div>", ReturnMoney(decAmountDue))
                ElseIf decAmtPaid = 0 Then
                    lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-yellow"">No refunds made yet. Full amount still to be applied.</div>", ReturnMoney(decAmountDue))
                End If
            ElseIf OppID > 0 AndAlso OppBizDocID > 0 Then
                If dtRecordPayment.Rows.Count > 0 Then
                    If CCommon.ToShort(dtRecordPayment(0)("tintRefType")) = 1 Then 'Sales Order
                        If decAmtPaid > 0 AndAlso decAmountDue = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Paid</h3><div class=""pull-right text-green"">Full payment has been received.</div>")
                        ElseIf decAmtPaid > 0 AndAlso decAmountDue > 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-aqua"">Partial payment has been received. The remaining balance is {0}. <a href='#' onclick='return OpenAmtPaid({1},{2},{3});'>Receive Payment</a></div>", ReturnMoney(decAmountDue), OppBizDocID, OppID, CCommon.ToLong(dtRecordPayment(0)("numDivisionId")))
                        ElseIf decAmtPaid = 0 Then
                            lblPaymentTitle.Text = String.Format("<div class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-yellow"">No payments made yet. Full amount still due. <a href='#' onclick='return OpenAmtPaid({1},{2},{3});'>Receive Payment</a></div>", ReturnMoney(decAmountDue), OppBizDocID, OppID, CCommon.ToLong(dtRecordPayment(0)("numDivisionId")))
                        End If
                    ElseIf CCommon.ToShort(dtRecordPayment(0)("tintRefType")) = 2 Then 'Purchase Order
                        If decAmtPaid > 0 AndAlso decAmountDue = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Paid</h3><div class=""pull-right text-green"">Full payment has been made.</div>")
                        ElseIf decAmtPaid > 0 AndAlso decAmountDue > 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-aqua"">Partial payment has been made. The remaining balance is {0}.</div>", ReturnMoney(decAmountDue))
                        ElseIf decAmtPaid = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-yellow"">No payments made yet. Full amount still due.</div>", ReturnMoney(decAmountDue))
                        End If
                    End If
                End If
            ElseIf OppID > 0 Then
                If dtRecordPayment.Rows.Count > 0 Then
                    If CCommon.ToShort(dtRecordPayment(0)("tintRefType")) = 1 Then 'Sales Order
                        If decAmtPaid > 0 AndAlso decAmountDue = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Paid</h3><div class=""pull-right text-green"">Full payment has been received.</div>")
                        ElseIf decAmtPaid > 0 AndAlso decAmountDue > 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-aqua"">Partial payment has been received. The remaining balance is {0}.</div>", ReturnMoney(decAmountDue))
                        ElseIf decAmtPaid = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-yellow"">No payments made yet. Full amount still due.</div>", ReturnMoney(decAmountDue))
                        End If
                    ElseIf CCommon.ToShort(dtRecordPayment(0)("tintRefType")) = 2 Then 'Purchase Order
                        If decAmtPaid > 0 AndAlso decAmountDue = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Paid</h3><div class=""pull-right text-green"">Full payment has been made.</div>")
                        ElseIf decAmtPaid > 0 AndAlso decAmountDue > 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-aqua"">Partial payment has been made. The remaining balance is {0}.</div>", ReturnMoney(decAmountDue))
                        ElseIf decAmtPaid = 0 Then
                            lblPaymentTitle.Text = String.Format("<h3 class=""box-title"">Balance Due {0}</h3><div class=""pull-right text-yellow"">No payments made yet. Full amount still due.</div>", ReturnMoney(decAmountDue))
                        End If
                    End If
                End If
            End If

            divPaymentsHistory.Visible = IIf(dtRecordPayment.Rows.Count > 0, True, False)
            dgPaymentDetails.Visible = IIf(dtReceivePayment.Rows.Count > 0, True, False)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            Return String.Format("{0:#,###.##}", Math.Round(CCommon.ToDecimal(Money), 2))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class