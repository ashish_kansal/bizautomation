' ''Created By Siva
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Common
'Partial Public Class frmCash
'    Inherits BACRMPage

'#Region "Variables"
'    Dim mintJournalId As Integer
'    Dim mintCashCreditCardId As Integer
'    Dim mintChartAcntId As Integer

'    Dim dtChartAcnt As DataTable
'    Dim dtItems As New DataTable
'    Dim lobjCashCreditCard As CashCreditCard
'#End Region

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            If lobjCashCreditCard Is Nothing Then lobjCashCreditCard = New CashCreditCard
'            Dim j As Integer
'            Dim drRow As DataRow
'            Dim lintRowcount As Integer
'            txtAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
'            btnSave.Attributes.Add("onclick", "return Save()")
'            btnRecalculate.Attributes.Add("onclick", "return Recalculate()")
'            mintCashCreditCardId = CCommon.ToInteger(GetQueryStringVal("CashCreditCardId"))
'            mintJournalId = CCommon.ToInteger(GetQueryStringVal("JournalId"))
'            mintChartAcntId = CCommon.ToInteger(GetQueryStringVal("ChartAcntId"))

'            If Not IsPostBack Then

'                'To Set Permission
'                GetUserRightsForPage(35, 81)


'                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
'                    btnSave.Visible = False
'                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
'                    btnSave.Visible = False
'                End If
'                If mintCashCreditCardId <> 0 Then
'                    lobjCashCreditCard.DomainID = Session("DomainID")
'                    lobjCashCreditCard.CashCreditCardId = mintCashCreditCardId
'                    mintJournalId = lobjCashCreditCard.GetJournalId()
'                    lobjCashCreditCard.JournalId = mintJournalId
'                    dtItems = lobjCashCreditCard.GetCashCreditCardJournalDetails()
'                End If
'                calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
'                LoadItem(ddlPurchase, "")
'                LoadChartAcnt()
'                LoadRecurringTemplate()
'                If dtItems.Rows.Count = 0 And dtItems.Columns.Count = 0 Then
'                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
'                    LoadDefaultColumns()

'                    For j = 0 To 1
'                        drRow = dtItems.NewRow
'                        dtItems.Rows.Add(drRow)
'                    Next

'                    dgJournalEntry.DataSource = dtItems
'                    dgJournalEntry.DataBind()
'                Else
'                    LoadSavedInformation()
'                    If dtItems.Rows.Count < 2 Then
'                        lintRowcount = 2 - dtItems.Rows.Count
'                        For j = 0 To lintRowcount - 1
'                            drRow = dtItems.NewRow
'                            dtItems.Rows.Add(drRow)
'                        Next
'                    End If
'                    dgJournalEntry.DataSource = dtItems
'                    dgJournalEntry.DataBind()
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub LoadDefaultColumns()
'        Try
'            dtItems.Columns.Add("TransactionId") '' Primary key
'            dtItems.Columns.Add("numJournalId")
'            dtItems.Columns.Add("numDebitAmt")
'            dtItems.Columns.Add("numCreditAmt")
'            dtItems.Columns.Add("numChartAcntId")
'            dtItems.Columns.Add("varDescription")
'            dtItems.Columns.Add("numCustomerId")
'            dtItems.Columns.Add("varRelation")
'            dtItems.Columns.Add("numDomainId")
'            dtItems.Columns.Add("numAcntType")
'            dtItems.Columns.Add("bitMainCashCredit")
'            dtItems.Columns.Add("bitReconcile")
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartAcnt()
'        Try
'            Dim objJournalEntry = New JournalEntry
'            objJournalEntry.ParentRootNode = 1
'            objJournalEntry.DomainId = Session("DomainId")
'            dtChartAcnt = objJournalEntry.GetChartOfAccountsDetails()
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub LoadRecurringTemplate()
'        Try
'            Dim lobjChecks As New Checks
'            lobjChecks.DomainId = Session("DomainID")
'            ddlMakeRecurring.DataSource = lobjChecks.GetRecurringDetails
'            ddlMakeRecurring.DataTextField = "varRecurringTemplateName"
'            ddlMakeRecurring.DataValueField = "numRecurringId"
'            ddlMakeRecurring.DataBind()
'            ddlMakeRecurring.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadSavedInformation()
'        Dim dtCashCreditDetails As DataTable
'        Dim lstrColor As String
'        Dim str As String
'        Dim lintAcntType As String
'        Try
'            If lobjCashCreditCard Is Nothing Then lobjCashCreditCard = New CashCreditCard

'            lobjCashCreditCard.CashCreditCardId = mintCashCreditCardId
'            lobjCashCreditCard.DomainId = Session("DomainID")
'            dtCashCreditDetails = lobjCashCreditCard.GetCashCreditCardDetails()
'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("datEntry_Date")) Then
'                calFrom.SelectedDate = dtCashCreditDetails.Rows(0).Item("datEntry_Date")
'            End If

'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("bitMoneyOut")) Then
'                If dtCashCreditDetails.Rows(0).Item("bitMoneyOut") = False Then
'                    str = "1"
'                    ddlMoneyOut.Items.FindByValue(str).Selected = True
'                    lintAcntType = 813 '' For Bank Account Type
'                    LoadChartType(ddlMoneyAsset, lintAcntType)
'                Else
'                    str = "2"
'                    lintAcntType = 816 '' For Credit Card Account Type
'                    LoadChartType(ddlMoneyAsset, lintAcntType)
'                    ddlMoneyOut.Items.FindByValue(str).Selected = True
'                End If
'            End If

'            ''If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("bitChargeBack")) Then
'            ''    If dtCashCreditDetails.Rows(0).Item("bitChargeBack") = True Then
'            ''        chkChargeBack.Visible = True
'            ''        chkChargeBack.Checked = True
'            ''    ElseIf ddlMoneyOut.SelectedItem.Value = "2" Then
'            ''        chkChargeBack.Visible = True
'            ''        chkChargeBack.Checked = False
'            ''    Else
'            ''        chkChargeBack.Visible = False
'            ''    End If
'            ''End If

'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("numAcntTypeId")) Then
'                If Not ddlMoneyAsset.Items.FindByValue(dtCashCreditDetails.Rows(0).Item("numAcntTypeId")) Is Nothing Then
'                    ddlMoneyAsset.Items.FindByValue(dtCashCreditDetails.Rows(0).Item("numAcntTypeId")).Selected = True
'                End If
'            End If

'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("numPurchaseId")) Then
'                If Not ddlPurchase.Items.FindByValue(dtCashCreditDetails.Rows(0).Item("numPurchaseId")) Is Nothing Then
'                    ddlPurchase.Items.FindByValue(dtCashCreditDetails.Rows(0).Item("numPurchaseId")).Selected = True
'                End If
'            End If

'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("numRecurringId")) Then
'                If Not ddlMakeRecurring.Items.FindByValue(dtCashCreditDetails.Rows(0).Item("numRecurringId")) Is Nothing Then
'                    ddlMakeRecurring.Items.FindByValue(dtCashCreditDetails.Rows(0).Item("numRecurringId")).Selected = True
'                    If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("dtLastRecurringDate")) Then
'                        ddlMakeRecurring.Enabled = False
'                    End If
'                End If
'            End If
'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("vcMemo")) Then
'                txtMemo.Text = dtCashCreditDetails.Rows(0).Item("vcMemo")
'            End If
'            If Not IsDBNull(dtCashCreditDetails.Rows(0).Item("vcReference")) Then
'                txtReference.Text = dtCashCreditDetails.Rows(0).Item("vcReference")
'            End If

'            txtAmount.Text = ReturnMoney(dtCashCreditDetails.Rows(0).Item("monAmount"))
'            TotalAmt.Value = dtCashCreditDetails.Rows(0).Item("monAmount")

'            ViewState("TransactionId") = lobjCashCreditCard.GetMainCashCreditCardId()

'            lobjCashCreditCard.AccountId = dtCashCreditDetails.Rows(0).Item("numAcntTypeId")
'            lobjCashCreditCard.DomainId = Session("DomainId")
'            lblBalance.Text = "Balance: " & Session("Currency")
'            Dim ldecOpeningBalance As Decimal
'            ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
'            lstrColor = "<font color=red>" & ReturnMoney(ldecOpeningBalance) & "</font>"
'            lblOpeningBalance.Text = IIf(ldecOpeningBalance < 0, lstrColor, ReturnMoney(ldecOpeningBalance))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadItem(ByVal ddlCompanyDet As DropDownList, ByVal p_WhereCondition As String)
'        Try
'            Dim lobjChecks As New Checks
'            lobjChecks.DomainId = Session("DomainID")
'            lobjChecks.strWhereCondition = p_WhereCondition
'            ddlCompanyDet.DataSource = lobjChecks.GetCompanyDetails
'            ddlCompanyDet.DataTextField = "vcCompanyName"
'            ddlCompanyDet.DataValueField = "numDivisionID"
'            ddlCompanyDet.DataBind()
'            ddlCompanyDet.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartType(ByVal ddlMoneyAsset As DropDownList, ByVal p_AcntTypeId As Integer)
'        Try
'            Dim lobjChecks As New Checks
'            lobjChecks.DomainId = Session("DomainId")
'            lobjChecks.AccountType = p_AcntTypeId
'            ddlMoneyAsset.DataSource = lobjChecks.GetCheckChartAcntDetails()
'            ddlMoneyAsset.DataTextField = "vcCategoryName"
'            ddlMoneyAsset.DataValueField = "numChartOfAcntID"
'            ddlMoneyAsset.DataBind()
'            ddlMoneyAsset.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgJournalEntry.ItemCommand
'        Try
'            If e.CommandName = "Go" Then
'                Dim ddlName As DropDownList
'                Dim txtCompanyName As TextBox

'                ddlName = CType(e.Item.FindControl("ddlName"), DropDownList)
'                txtCompanyName = CType(e.Item.FindControl("txtCompanyName"), TextBox)
'                If Not IsNothing(ddlName) Then
'                    ''   If txtCompanyName.Text <> "" Then
'                    LoadItem(ddlName, txtCompanyName.Text)
'                    ''End If
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub dgJournalEntry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgJournalEntry.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim ddl As DropDownList
'                Dim ddlAccounts As DropDownList
'                Dim lblAccounts As Label
'                Dim lblDebitAmount As Label
'                Dim lblMemo As Label
'                Dim lblName As Label
'                Dim txtDebitAmount As TextBox
'                Dim txtMemo As TextBox
'                Dim lblTransactionDetailsId As Label

'                ddl = CType(e.Item.FindControl("ddlName"), DropDownList)
'                txtDebitAmount = CType(e.Item.FindControl("txtDebit"), TextBox)
'                txtMemo = CType(e.Item.FindControl("txtMemo"), TextBox)
'                txtDebitAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")

'                If Not IsNothing(ddl) Then
'                    lblDebitAmount = CType(e.Item.FindControl("lblDebitAmt"), Label)

'                    lblMemo = CType(e.Item.FindControl("lblMemo"), Label)
'                    lblName = CType(e.Item.FindControl("lblName"), Label)
'                    LoadCompanyDropDown(ddl, IIf(lblName.Text = "", 0, lblName.Text))

'                    lblTransactionDetailsId = CType(e.Item.FindControl("lblTransactionId"), Label)
'                    If lblTransactionDetailsId.Text <> "" Then
'                        CType(e.Item.FindControl("lblTransactionId"), Label).Text = lblTransactionDetailsId.Text
'                    End If
'                    If lblDebitAmount.Text <> "" AndAlso lblDebitAmount.Text <> 0 Then
'                        txtDebitAmount.Text = lblDebitAmount.Text
'                    End If

'                    If lblMemo.Text <> "" Then txtMemo.Text = lblMemo.Text

'                    If Not ddl.Items.FindByValue(lblName.Text) Is Nothing Then
'                        ddl.Items.FindByValue(lblName.Text).Selected = True
'                    End If
'                End If
'                ddlAccounts = CType(e.Item.FindControl("ddlAccounts"), DropDownList)
'                If Not IsNothing(ddlAccounts) Then
'                    LoadChartType(ddlAccounts)
'                    lblAccounts = CType(e.Item.FindControl("lblAccountID"), Label)
'                    If Not ddlAccounts.Items.FindByValue(lblAccounts.Text) Is Nothing Then
'                        ddlAccounts.Items.FindByValue(lblAccounts.Text).Selected = True
'                    End If
'                End If

'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadCompanyDropDown(ByVal ddlitems As DropDownList, ByVal p_DivisionId As Integer)
'        Try
'            If p_DivisionId <> 0 Then
'                Dim objJournalEntry As New JournalEntry
'                objJournalEntry.DomainId = Session("DomainID")
'                objJournalEntry.DivisionId = p_DivisionId
'                ddlitems.DataSource = objJournalEntry.GetCompanyDetForDivisionId()
'                ddlitems.DataTextField = "vcCompanyName"
'                ddlitems.DataValueField = "numDivisionID"
'                ddlitems.DataBind()
'                ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'            Else : ddlitems.Items.Insert(0, New ListItem("--Select One --", "0"))
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Sub LoadChartType(ByVal ddlAccounts As DropDownList)
'        Try
'            ddlAccounts.DataSource = dtChartAcnt
'            ddlAccounts.DataTextField = "vcCategoryName"
'            ddlAccounts.DataValueField = "numChartOfAcntID"
'            ddlAccounts.DataBind()
'            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Function ReturnMoney(ByVal Money)
'        Try
'            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Dim i As Integer
'        Dim dtgriditem As DataGridItem
'        Dim dtrow As DataRow
'        Try
'            LoadDefaultColumns()
'            For i = 0 To dgJournalEntry.Items.Count - 1
'                dtrow = dtItems.NewRow
'                dtgriditem = dgJournalEntry.Items(i)
'                dtrow.Item("numJournalId") = CType(dtgriditem.FindControl("lblJournalId"), Label).Text
'                dtrow.Item("TransactionId") = CType(dtgriditem.FindControl("lblTransactionId"), Label).Text
'                dtrow.Item("numDebitAmt") = CType(dtgriditem.FindControl("txtDebit"), TextBox).Text ''IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, CType(dtgriditem.FindControl("txtDebit"), TextBox).Text)
'                dtrow.Item("numChartAcntId") = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                dtrow.Item("varDescription") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                dtrow.Item("numCustomerId") = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                dtrow.Item("varRelation") = CType(dtgriditem.FindControl("lblRelationship"), Label).Text
'                dtrow.Item("numAcntType") = CType(dtgriditem.FindControl("txtAccountTypeId"), TextBox).Text
'                dtItems.Rows.Add(dtrow)
'            Next
'            For i = 0 To 1
'                dtrow = dtItems.NewRow
'                dtItems.Rows.Add(dtrow)
'            Next
'            LoadChartAcnt()
'            dgJournalEntry.DataSource = dtItems
'            dgJournalEntry.DataBind()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub ddlMoneyOut_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMoneyOut.SelectedIndexChanged
'        Try
'            Dim lintAcntType As Integer
'            If ddlMoneyOut.SelectedItem.Value <> 0 Then
'                If ddlMoneyOut.SelectedItem.Value = 1 Then
'                    lintAcntType = 813 '' Bank Account Type
'                    LoadChartType(ddlMoneyAsset, lintAcntType)
'                    '' chkChargeBack.Visible = False
'                ElseIf ddlMoneyOut.SelectedItem.Value = 2 Then
'                    lintAcntType = 816 '' Credit Card Account Type
'                    LoadChartType(ddlMoneyAsset, lintAcntType)
'                    '' chkChargeBack.Visible = True
'                End If
'            Else
'                LoadChartType(ddlMoneyAsset, 0)
'                ''  chkChargeBack.Visible = False
'                lblOpeningBalance.Text = ""
'                lblBalance.Text = ""
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim JournalId As Integer
'        Dim CashCreditId As Integer
'        Try
'            CashCreditId = SaveDataToCash_CreditCardDetails()
'            JournalId = SaveDataToHeader(CashCreditId)
'            SaveDataToGeneralJournalDetails(JournalId)

'            PageRedirect()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Sub PageRedirect()
'        Try
'            If GetQueryStringVal( "frm") = "BankRegister" Then
'                Response.Redirect("../Accounting/frmJournalEntry.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal( "frm") = "RecurringTransaction" Then
'                Response.Redirect("../Accounting/frmRecurringTransaction.aspx")
'            ElseIf GetQueryStringVal( "frm") = "GeneralLedger" Then
'                Response.Redirect("../Accounting/frmGeneralLedger.aspx")
'            ElseIf GetQueryStringVal( "frm") = "Transaction" Then
'                Response.Redirect("../Accounting/frmTransactions.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal( "frm") = "QuickReport" Then
'                Response.Redirect("../Accounting/frmQuickAccountReport.aspx?ChartAcntId=" & mintChartAcntId)
'            ElseIf GetQueryStringVal("frm") = "BankRecon" Then
'                Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & CCommon.ToLong(GetQueryStringVal("ReconcileID")))
'            Else
'                Response.Redirect("../Accounting/frmChartofAccounts.aspx")
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    ''To Save Cash Or Credit Card Details in CashCreditCardDetails Table
'    Private Function SaveDataToCash_CreditCardDetails() As Integer
'        Dim lintCashCreditCreditId As Integer
'        Try
'            If lobjCashCreditCard Is Nothing Then lobjCashCreditCard = New CashCreditCard

'            With lobjCashCreditCard
'                .Entry_Date = calFrom.SelectedDate
'                .numAmount = Replace(txtAmount.Text, ",", "")
'                .DomainId = Session("DomainId")
'                .PurchaseId = ddlPurchase.SelectedItem.Value
'                .Memo = txtMemo.Text
'                .AccountId = ddlMoneyAsset.SelectedItem.Value
'                .RecurringId = ddlMakeRecurring.SelectedItem.Value
'                If ddlMoneyOut.SelectedItem.Value = "1" Then
'                    .bitMoneyOut = 0 '' -- 0 For Cash
'                Else
'                    .bitMoneyOut = 1 '' -- 1 For Credit Card
'                    .bitChargeBack = 1
'                End If
'                '' If chkChargeBack.Checked = True Then

'                ''  Else
'                '' .bitChargeBack = 0
'                ''  End If
'                .ReferenceNo = txtReference.Text
'                If Not IsNothing(mintCashCreditCardId) And mintCashCreditCardId <> 0 Then
'                    .CashCreditCardId = mintCashCreditCardId
'                    .SaveDataToCashCreditCardToEdit()
'                    lintCashCreditCreditId = mintCashCreditCardId
'                Else
'                    .CashCreditCardId = 0
'                    .UserCntID = Session("UserContactID")
'                    lintCashCreditCreditId = .SaveDataToCashCreditDetails()
'                End If
'                Return lintCashCreditCreditId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    ''To Save Header Details in General_Journal_Header table
'    Private Function SaveDataToHeader(ByVal p_CashCreditCard As Integer) As Integer
'        Dim lntJournalId As Integer
'        Try
'            Dim lobjCashCreditCard As New JournalEntry

'            With lobjCashCreditCard
'                .Entry_Date = CDate(calFrom.SelectedDate & " 12:00:00")
'                .Amount = Replace(txtAmount.Text, ",", "")
'                .DomainId = Session("DomainId")
'                .ChartAcntId = ddlMoneyAsset.SelectedItem.Value
'                .RecurringId = 0
'                .JournalId = mintJournalId
'                .CashCreditCardId = p_CashCreditCard
'                .UserCntID = Session("UserContactID")
'                lntJournalId = .SaveDataToJournalEntryHeader
'                Return lntJournalId
'            End With
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function

'    ''To Save Header Details in General_Journal_Details table
'    Private Sub SaveDataToGeneralJournalDetails(ByVal lintJournalId As Integer)
'        Dim i As Integer
'        Dim lstr As String
'        Dim ds As New DataSet

'        Dim ddlAccounts As DropDownList
'        Dim lintAccountId() As String
'        Try
'            If lobjCashCreditCard Is Nothing Then lobjCashCreditCard = New CashCreditCard

'            LoadDefaultColumns()
'            Dim dtgriditem As DataGridItem
'            Dim dtrow As DataRow

'            For i = 0 To dgJournalEntry.Items.Count - 1
'                dtrow = dtItems.NewRow
'                dtgriditem = dgJournalEntry.Items(i)
'                If CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value.ToString <> "0" And CType(dtgriditem.FindControl("txtDebit"), TextBox).Text <> "" Then ''And CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value.ToString <> "0" Then
'                    dtrow.Item("TransactionId") = IIf(CType(dtgriditem.FindControl("lblTransactionId"), Label).Text = "", 0, CType(dtgriditem.FindControl("lblTransactionId"), Label).Text)
'                    dtrow.Item("numJournalId") = lintJournalId
'                    ''If ddlMoneyOut.SelectedItem.Value = 2 Then
'                    ''    If chkChargeBack.Checked = True Then
'                    ''        dtrow.Item("numDebitAmt") = IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text, ",", ""))
'                    ''        dtrow.Item("numCreditAmt") = 0
'                    ''    Else
'                    ''        dtrow.Item("numDebitAmt") = 0
'                    ''        dtrow.Item("numCreditAmt") = IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text, ",", ""))
'                    ''    End If
'                    ''Else
'                    dtrow.Item("numDebitAmt") = IIf(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text = "", 0, Replace(CType(dtgriditem.FindControl("txtDebit"), TextBox).Text, ",", ""))
'                    dtrow.Item("numCreditAmt") = 0
'                    ''End If
'                    ddlAccounts = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList)
'                    lintAccountId = ddlAccounts.SelectedItem.Value.Split("~")
'                    dtrow.Item("numChartAcntId") = lintAccountId(0)
'                    '' dtrow.Item("numChartAcntId") = CType(dtgriditem.FindControl("ddlAccounts"), DropDownList).SelectedItem.Value
'                    dtrow.Item("varDescription") = CType(dtgriditem.FindControl("txtMemo"), TextBox).Text
'                    dtrow.Item("numCustomerId") = CType(dtgriditem.FindControl("ddlName"), DropDownList).SelectedItem.Value
'                    dtrow.Item("numDomainId") = Session("DomainId")
'                    dtrow.Item("bitMainCashCredit") = 0
'                    dtrow.Item("bitReconcile") = 0

'                    lobjCashCreditCard.DomainId = Session("DomainId")
'                    dtItems.Rows.Add(dtrow)
'                End If
'            Next

'            'To Add the Credit Amount in General_Journal_Details table [i.e] For Bank Account Selected

'            dtrow = dtItems.NewRow
'            dtrow.Item("TransactionId") = IIf(ViewState("TransactionId") = Nothing, 0, ViewState("TransactionId"))
'            dtrow.Item("numJournalId") = lintJournalId
'            ''If ddlMoneyOut.SelectedItem.Value = 2 Then
'            ''    If chkChargeBack.Checked = True Then
'            ''        dtrow.Item("numDebitAmt") = 0
'            ''        dtrow.Item("numCreditAmt") = Replace(txtAmount.Text, ",", "")
'            ''        dtrow.Item("numBalance") = -Replace(Val(txtAmount.Text), ",", "")
'            ''    Else
'            ''        dtrow.Item("numDebitAmt") = Replace(txtAmount.Text, ",", "")
'            ''        dtrow.Item("numCreditAmt") = 0
'            ''        dtrow.Item("numBalance") = Replace(Val(txtAmount.Text), ",", "")

'            ''    End If
'            ''Else
'            dtrow.Item("numDebitAmt") = 0
'            dtrow.Item("numCreditAmt") = Replace(txtAmount.Text, ",", "")
'            ''  dtrow.Item("numBalance") = -Replace(Val(txtAmount.Text), ",", "")
'            '' End If

'            dtrow.Item("numChartAcntId") = ddlMoneyAsset.SelectedItem.Value
'            dtrow.Item("varDescription") = txtMemo.Text
'            dtrow.Item("numCustomerId") = ddlPurchase.SelectedItem.Value
'            dtrow.Item("numDomainId") = Session("DomainId")

'            dtrow.Item("bitMainCashCredit") = 1
'            dtrow.Item("bitReconcile") = 0
'            dtItems.Rows.Add(dtrow)

'            ds.Tables.Add(dtItems)
'            lstr = ds.GetXml()
'            lobjCashCreditCard.JournalId = lintJournalId

'            ''journalid = GetQueryStringVal("JournalId")
'            ''If Not IsNothing(journalid) And journalid <> 0 Then
'            ''    lobjCashCreditCard.Mode = 1
'            ''Else
'            ''    lobjCashCreditCard.Mode = 0
'            ''End If

'            ''journalid = GetQueryStringVal("JournalId")
'            If Not IsNothing(mintCashCreditCardId) And mintCashCreditCardId <> 0 Then
'                lobjCashCreditCard.Mode = 1 '' For Update Purpose
'            Else : lobjCashCreditCard.Mode = 0 '' For Insert Purpose
'            End If

'            lobjCashCreditCard.JournalDetails = lstr
'            lobjCashCreditCard.RecurringMode = 0
'            lobjCashCreditCard.DomainId = Session("DomainId")
'            lobjCashCreditCard.SaveDataToJournalDetailsForCashCreditCard()

'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub ddlMoneyAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMoneyAsset.SelectedIndexChanged
'        Try
'            If lobjCashCreditCard Is Nothing Then lobjCashCreditCard = New CashCreditCard

'            Dim lstrColor As String
'            Dim ldecOpeningBalance As Decimal
'            If ddlMoneyAsset.SelectedItem.Value <> 0 Then
'                lobjCashCreditCard.AccountId = ddlMoneyAsset.SelectedItem.Value
'                lobjCashCreditCard.DomainId = Session("DomainId")
'                lblBalance.Text = "Balance: " & Session("Currency")
'                ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
'                lstrColor = "<font color=red>" & ReturnMoney(ldecOpeningBalance) & "</font>"
'                lblOpeningBalance.Text = IIf(ldecOpeningBalance < 0, lstrColor, ReturnMoney(ldecOpeningBalance))
'            Else
'                lblOpeningBalance.Text = ""
'                lblBalance.Text = ""
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
'        Try
'            PageRedirect()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'End Class