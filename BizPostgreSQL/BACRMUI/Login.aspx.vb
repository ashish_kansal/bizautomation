''created by anoop jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.ServiceProcess
Imports System.Web.Services


'Imports QueryStringValues

Partial Public Class Login
    Inherits BACRMPage

    Dim lngUserID As Long
    Dim strUserName As String = String.Empty
    Dim strPassword As String = String.Empty
    Protected msUserID As String
    Protected msSessionCacheValue As String
    Dim vcDefaultNavURL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not IsPostBack Then
                If GetQueryStringVal("From") <> "Help" Then
                    If GetQueryStringVal("Token") <> "" Then
                        LoginUsingToken()
                    End If
                    Dim strClientLogin = Request.Url
                    If GetQueryStringVal("org") <> "" Then
                        Dim lobjUserAccess As New UserAccess
                        lobjUserAccess.vcLoginURL = GetQueryStringVal("org")
                        lobjUserAccess.DomainID = 0
                        Dim dt As New DataTable()
                        dt = lobjUserAccess.GetDomainDetailsByURL()
                        If dt.Rows.Count() = 1 Then
                            hdnTemplateSkin.Value = dt.Rows(0)("vcThemeClass")
                            If CCommon.ToBool(dt.Rows(0)("bitLogoAppliedToLoginBizTheme")) = True Then
                                Dim strFilePath As String
                                Dim FilePath = CCommon.GetDocumentPath(dt.Rows(0)("numDomainId"))
                                strFilePath = FilePath & CCommon.ToString(dt.Rows(0)("vcLogoForLoginBizTheme"))

                                imgLogo.ImageUrl = strFilePath
                            End If
                        End If
                    End If
                    btnLogin.Attributes.Add("onclick", "return Login()")
                    btnSendPwd.Attributes.Add("onclick", "return Forgot()")

                    If System.Web.HttpContext.Current.User.Identity.IsAuthenticated AndAlso CCommon.ToLong(Session("DomainID")) > 0 Then
                        Response.Redirect("~/Home.aspx", False)
                        Exit Sub
                    End If


                    Dim sRet As String = ""
                    sRet = DirectCast(System.Web.HttpContext.Current.Cache("SessionTimeOut" & Session("UserID")), String)
                    If sRet IsNot Nothing Then
                        System.Web.HttpContext.Current.Cache.Remove("SessionTimeOut" & Session("UserID"))
                    End If

                    txtEmaillAdd.Focus()
                    If GetQueryStringVal("Msg") <> "" Then
                        pnlLogout.Visible = True
                    End If
                    'Added by sachin sadhu||date:19thMarch2014
                    'Purpose:Prompt User If Window service is not working
                    Dim sc As New ServiceController("Bizservice")
                    If sc.Status = ServiceControllerStatus.Stopped Then
                        pnlServiceStopped.Visible = True
                    Else
                        pnlServiceStopped.Visible = False

                    End If
                    'End of code by sachin
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            SetOffset()
            strUserName = txtEmaillAdd.Text.Trim
            strPassword = txtPassword.Text.Trim

            If System.Web.HttpContext.Current.User.Identity.IsAuthenticated AndAlso CCommon.ToLong(Session("DomainID")) > 0 Then
                litMessage.Text = "<span class='btn btn-danger'>BizAutomation.com is already open in another tab of same browser and if not than clear cookies.</span>"
                Exit Sub
            End If

            'Added by chintan for subscription Management
            Dim strSubscriptionLogin() As String = CCommon.ToString(ConfigurationManager.AppSettings("SubscriptionManagement")).Split("/")
            If strSubscriptionLogin.Length = 2 Then
                If strSubscriptionLogin(0) = strUserName And strSubscriptionLogin(1) = strPassword Then
                    Session("DomainID") = 1
                    Session("UserContactID") = 1
                    Session("PagingRows") = 20
                    Session("DateFormat") = "DD/MONTH/YYYY"

                    Response.Redirect("Service/frmSerNav.htm", False)
                    Exit Sub

                End If
            End If

            If sb_GetDomainUser(False) = False Then
                System.Web.Security.FormsAuthentication.SignOut()
                Session.Abandon()
                Exit Sub
            End If
        Catch ex As Exception
            Dim exception As System.Reflection.ReflectionTypeLoadException = TryCast(ex, System.Reflection.ReflectionTypeLoadException)

            If Not exception Is Nothing Then
                Dim errroMessage As String = ""

                For Each loaderException As Exception In exception.LoaderExceptions
                    errroMessage = errroMessage & ";" & loaderException.Message
                Next

                Throw New Exception(errroMessage)
            Else
                Throw
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
                Session.Abandon()
            End If

            Exit Sub
        End Try

        If GetQueryStringVal("From") = "Help" Then
            Dim HelpURL As String = ConfigurationManager.AppSettings("HelpURL")

            HelpURL = HelpURL & "?a=" & objCommon.Encrypt(Session("AccessID")) & "&pageurl=" & CCommon.ToString(GetQueryStringVal("pageurl"))
            Response.Redirect(HelpURL, False)
            Exit Sub
        Else
            Cache.Remove("BizMenu" & Session("UserContactId"))
            If vcDefaultNavURL = "" Then
                Response.Redirect("~/Home.aspx", False)
            Else
                Response.Redirect(vcDefaultNavURL, False)
            End If

            'Response.Redirect("common/TicklerDisplay.aspx?ClientMachineUTCTimeOffset=" & txtOffset.Text.Trim)
        End If
        'Response.Redirect("include/frmMenu.aspx")
    End Sub

    Private Sub btnLinkdeinLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLinkdeinLogin.Click
        Try
            SetOffset()

            If System.Web.HttpContext.Current.User.Identity.IsAuthenticated AndAlso CCommon.ToLong(Session("DomainID")) > 0 Then
                litMessage.Text = "<span class='btn btn-danger'>BizAutomation.com is already open in another tab of same browser and if not than clear cookies.</span>"
                Exit Sub
            End If

            If sb_GetDomainUser(True) = False Then
                System.Web.Security.FormsAuthentication.SignOut()
                Session.Abandon()
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

            System.Web.Security.FormsAuthentication.SignOut()
            Session.Abandon()
            Exit Sub
        End Try

        If GetQueryStringVal("From") = "Help" Then
            Dim HelpURL As String = ConfigurationManager.AppSettings("HelpURL")

            HelpURL = HelpURL & "?a=" & objCommon.Encrypt(Session("AccessID")) & "&pageurl=" & CCommon.ToString(GetQueryStringVal("pageurl"))
            Response.Redirect(HelpURL, False)
            Exit Sub
        Else
            If vcDefaultNavURL = "" Then
                Response.Redirect("~/Home.aspx", False)
            Else
                Response.Redirect(vcDefaultNavURL, False)
            End If
        End If
        'Response.Redirect("include/frmMenu.aspx")
    End Sub

    <WebMethod()>
    Public Shared Function AuthenticateUser(ByVal Password As String, ByVal offset As String) As String()
        Try
            Dim dsUserDetails As DataSet
            Dim msUserID As String
            Dim msSessionCacheValue As String
            Dim objCommonLinkdein As New CCommon
            Dim Context As System.Web.HttpContext
            Dim lngUserID As Long
            If System.Web.HttpContext.Current.User.Identity.IsAuthenticated Then
                Return New String() {"label", "BizAutomation.com is already open in another tab of same browser and if not than clear cookies."}
                Exit Function
            End If
            dsUserDetails = objCommonLinkdein.CheckUserAtLogin("", "", Password)
            Dim dtTable As DataTable
            dtTable = dsUserDetails.Tables(0)
            If dtTable.Rows.Count <> 1 Then
                Return New String() {"label", "Please Check Email Address and Password !"}
                Exit Function
            End If

            If (dtTable.Rows(0).Item("tintLogin") = 1) Then
                Dim sUser As String = Convert.ToString(System.Web.HttpContext.Current.Cache("SessionTimeOut" & dtTable.Rows(0).Item("numUserID")))
                If sUser Is Nothing OrElse sUser = [String].Empty Then
                    msUserID = dtTable.Rows(0).Item("numUserID")
                    msSessionCacheValue = ""

                    DirectCast(Context.ApplicationInstance, [Global]).InsertSessionCacheItem(msUserID)
                    ' Test to access the cache and make sure it still exists
                    msSessionCacheValue = DirectCast(Context.ApplicationInstance, [Global]).GetSessionCacheItem(msUserID)
                Else
                    Return New String() {"label", "Please Check another user already login!"}
                    Exit Function
                End If
            End If

            System.Web.Security.FormsAuthentication.SetAuthCookie(dtTable.Rows(0).Item("vcEmailID"), False)
            'Get the Domain ID        
            System.Web.HttpContext.Current.Session("bitMarginPriceViolated") = dtTable.Rows(0).Item("bitMarginPriceViolated")
            System.Web.HttpContext.Current.Session("bitUseOnlyActionItems") = dtTable.Rows(0).Item("bitUseOnlyActionItems")
            System.Web.HttpContext.Current.Session("numAuthorizePercentage") = dtTable.Rows(0).Item("numAuthorizePercentage")
            System.Web.HttpContext.Current.Session("numDefaultPurchaseShippingDoc") = dtTable.Rows(0).Item("numDefaultPurchaseShippingDoc")
            System.Web.HttpContext.Current.Session("numDefaultSalesShippingDoc") = dtTable.Rows(0).Item("numDefaultSalesShippingDoc")
            System.Web.HttpContext.Current.Session("ProfilePic") = dtTable.Rows(0).Item("ProfilePic")
            System.Web.HttpContext.Current.Session("numCost") = dtTable.Rows(0).Item("numCost")
            System.Web.HttpContext.Current.Session("bitApprovalforTImeExpense") = dtTable.Rows(0).Item("bitApprovalforTImeExpense")
            System.Web.HttpContext.Current.Session("intTimeExpApprovalProcess") = dtTable.Rows(0).Item("intTimeExpApprovalProcess")
            System.Web.HttpContext.Current.Session("intOpportunityApprovalProcess") = dtTable.Rows(0).Item("intOpportunityApprovalProcess")
            System.Web.HttpContext.Current.Session("DomainID") = dtTable.Rows(0).Item("numDomainID") ''Domain ID
            'Session("MailName") = dtTable.Rows(0).Item("vcMailNickName") 'not being used anywhere -commented by chintan
            System.Web.HttpContext.Current.Session("UserEmail") = dtTable.Rows(0).Item("vcEmailID") ''Email ID
            System.Web.HttpContext.Current.Session("UserEmailAlias") = CCommon.ToString(dtTable.Rows(0).Item("vcEmailAlias"))
            Dim objEncrypt As New CCommon
            System.Web.HttpContext.Current.Session("UserEmailAliasPassword") = If(String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0).Item("vcEmailAliasPassword"))), "", CCommon.ToString(dtTable.Rows(0).Item("vcEmailAliasPassword")))
            System.Web.HttpContext.Current.Session("UserID") = dtTable.Rows(0).Item("numUserID")  '' User ID
            System.Web.HttpContext.Current.Session("Signature") = dtTable.Rows(0).Item("txtSignature") ''Signature
            System.Web.HttpContext.Current.Session("CompanyName") = dtTable.Rows(0).Item("vcCompanyName") ''Parent Company Name
            System.Web.HttpContext.Current.Session("ContactName") = dtTable.Rows(0).Item("ContactName")  ''Contact NAme
            lngUserID = objCommonLinkdein.UserID    'Set the session into a local variable
            System.Web.HttpContext.Current.Session("UserContactID") = dtTable.Rows(0).Item("numUserDetailId") ''User Contact ID
            System.Web.HttpContext.Current.Session("UserGroupID") = dtTable.Rows(0).Item("numGroupID")   ''Group ID
            System.Web.HttpContext.Current.Session("UserDivisionID") = dtTable.Rows(0).Item("numDivisionID") ''Division ID
            System.Web.HttpContext.Current.Session("ExchIntegration") = dtTable.Rows(0).Item("bitExchangeIntegration") '' Exchange Integration
            System.Web.HttpContext.Current.Session("ImapIntegration") = dtTable.Rows(0).Item("bitImapIntegration") '' Exchange Integration
            System.Web.HttpContext.Current.Session("DateFormat") = dtTable.Rows(0).Item("vcDateFormat") 'DateFormat
            ' Session("AddressForOutLook") = dtTable.Rows(0).Item("tintAddForOutlook") '' Address For Exporting To Outlook
            System.Web.HttpContext.Current.Session("PagingRows") = dtTable.Rows(0).Item("tintCustomPagingRows") ''Custom paging Rows
            System.Web.HttpContext.Current.Session("DefCountry") = dtTable.Rows(0).Item("numDefCountry")  ''Default country
            System.Web.HttpContext.Current.Session("CompWindow") = dtTable.Rows(0).Item("tintComposeWindow") ''  Compose Window when click on Email
            System.Web.HttpContext.Current.Session("StartDate") = dtTable.Rows(0).Item("StartDate") '' Start date
            System.Web.HttpContext.Current.Session("EndDate") = dtTable.Rows(0).Item("EndDate") '' End date
            System.Web.HttpContext.Current.Session("PopulateUserCriteria") = dtTable.Rows(0).Item("tintAssignToCriteria")     ''Populate Users Based on Certailn Criteria
            System.Web.HttpContext.Current.Session("EnableIntMedPage") = IIf(dtTable.Rows(0).Item("bitIntmedPage") = True, 1, 0) ' Status of Intermediatory page
            System.Web.HttpContext.Current.Session("FiscalMonth") = dtTable.Rows(0).Item("tintFiscalStartMonth")
            System.Web.HttpContext.Current.Session("AdminID") = dtTable.Rows(0).Item("numAdminID")
            System.Web.HttpContext.Current.Session("Team") = dtTable.Rows(0).Item("numTeam")
            System.Web.HttpContext.Current.Session("Currency") = dtTable.Rows(0).Item("vccurrency") & " " ' this is base currency symbol
            System.Web.HttpContext.Current.Session("Trial") = dtTable.Rows(0).Item("bitTrial")
            System.Web.HttpContext.Current.Session("DomainName") = dtTable.Rows(0).Item("vcDomainName")
            System.Web.HttpContext.Current.Session("SMTPServer") = dtTable.Rows(0).Item("vcSMTPServer")
            System.Web.HttpContext.Current.Session("SMTPPort") = dtTable.Rows(0).Item("numSMTPPort")
            System.Web.HttpContext.Current.Session("SMTPAuth") = dtTable.Rows(0).Item("bitSMTPAuth")
            System.Web.HttpContext.Current.Session("SMTPPassword") = dtTable.Rows(0).Item("vcSmtpPassword")
            System.Web.HttpContext.Current.Session("bitSMTPSSL") = dtTable.Rows(0).Item("bitSMTPSSL")
            System.Web.HttpContext.Current.Session("SMTPServerIntegration") = dtTable.Rows(0).Item("bitSMTPServer")
            System.Web.HttpContext.Current.Session("ChrForItemSearch") = dtTable.Rows(0).Item("tintChrForItemSearch")
            System.Web.HttpContext.Current.Session("ChrForCompSearch") = dtTable.Rows(0).Item("tintChrForComSearch")
            System.Web.HttpContext.Current.Session("UnitSystem") = dtTable.Rows(0).Item("UnitSystem")
            System.Web.HttpContext.Current.Session("NoOfDaysLeft") = dtTable.Rows(0).Item("NoOfDaysLeft")
            System.Web.HttpContext.Current.Session("BaseCurrencyID") = dtTable.Rows(0).Item("numCurrencyID")
            System.Web.HttpContext.Current.Session("MultiCurrency") = dtTable.Rows(0).Item("bitMultiCurrency")
            System.Web.HttpContext.Current.Session("SOBizDocStatus") = dtTable.Rows(0).Item("numSOBizDocStatus")
            System.Web.HttpContext.Current.Session("AutolinkUnappliedPayment") = dtTable.Rows(0).Item("bitAutolinkUnappliedPayment")
            System.Web.HttpContext.Current.Session("DiscountOnUnitPrice") = dtTable.Rows(0).Item("bitDiscountOnUnitPrice")


            System.Web.HttpContext.Current.Session("SubscriberID") = dtTable.Rows(0).Item("numSubscriberID")
            System.Web.HttpContext.Current.Session("DefaultShippingCompany") = dtTable.Rows(0).Item("numShipCompany")
            System.Web.HttpContext.Current.Session("OrganizationSearchCriteria") = dtTable.Rows(0).Item("tintOrganizationSearchCriteria")
            System.Web.HttpContext.Current.Session("DocumentRepositaryEnabled") = dtTable.Rows(0).Item("bitDocumentRepositary")

            System.Web.HttpContext.Current.Session("GtoBContact") = dtTable.Rows(0).Item("bitGtoBContact")
            System.Web.HttpContext.Current.Session("BtoGContact") = dtTable.Rows(0).Item("bitBtoGContact")
            System.Web.HttpContext.Current.Session("GtoBCalendar") = dtTable.Rows(0).Item("bitGtoBCalendar")
            System.Web.HttpContext.Current.Session("BtoGCalendar") = dtTable.Rows(0).Item("bitBtoGCalendar")
            System.Web.HttpContext.Current.Session("IsDirtyForm") = dtTable.Rows(0).Item("bitTrakDirtyForm")

            System.Web.HttpContext.Current.Session("BaseTaxOnArea") = dtTable.Rows(0).Item("tintBaseTaxOnArea")
            System.Web.HttpContext.Current.Session("TabEvent") = dtTable.Rows(0).Item("tintTabEvent")
            System.Web.HttpContext.Current.Session("AutoSerialNoAssign") = dtTable.Rows(0).Item("bitAutoSerialNoAssign")
            'No longer sutpporting exchange integration
            'If dtTable.Rows(0).Item("bitExchangeIntegration") = True Then
            '    Session("AccessMethod") = dtTable.Rows(0).Item("bitAccessExchange")
            '    Session("ExchURL") = dtTable.Rows(0).Item("vcExchPath")
            '    Session("ExchDomain") = dtTable.Rows(0)t.Iem("vcExchDomain")
            '    If IsDBNull(dtTable.Rows(0).Item("vcExchPassword")) = False Then
            '        If dtTable.Rows(0).Item("vcExchPassword") <> "" Then
            '            Session("ExchPassword") = objCommon.Decrypt(dtTable.Rows(0).Item("vcExchPassword"))
            '        End If
            '    End If
            '    If dtTable.Rows(0).Item("bitAccessExchange") = False Then
            '        Session("ExchUserName") = dtTable.Rows(0).Item("vcEmailID").ToString.Split("@")(0)
            '    Else : Session("ExchUserName") = dtTable.Rows(0).Item("vcExchUserName").ToString.Split("@")(0)
            '    End If
            'End If

            System.Web.HttpContext.Current.Session("UserTerritory") = dsUserDetails.Tables(1)
            System.Web.HttpContext.Current.Session("SiteType") = HttpContext.Current.Request.Url.Scheme + ":"
            System.Web.HttpContext.Current.Session("BaseTaxCalcOn") = dtTable.Rows(0).Item("tintBaseTax")
            System.Web.HttpContext.Current.Session("EnableEmbeddedCost") = dtTable.Rows(0).Item("bitEmbeddedCost")
            System.Web.HttpContext.Current.Session("AuthoritativeSalesBizDoc") = dtTable.Rows(0).Item("numAuthoritativeSales")
            System.Web.HttpContext.Current.Session("AuthoritativePurchaseBizDoc") = dtTable.Rows(0).Item("numAuthoritativePurchase")
            System.Web.HttpContext.Current.Session("DecimalPoints") = dtTable.Rows(0).Item("tintDecimalPoints")
            System.Web.HttpContext.Current.Session("DefaultBillToForPO") = dtTable.Rows(0).Item("tintBillToForPO")
            System.Web.HttpContext.Current.Session("DefaultShipToForPO") = dtTable.Rows(0).Item("tintShipToForPO")
            System.Web.HttpContext.Current.Session("EnableNonInventoryItemExpense") = dtTable.Rows(0).Item("bitExpenseNonInventoryItem")
            System.Web.HttpContext.Current.Session("InlineEdit") = dtTable.Rows(0).Item("bitInlineEdit")
            System.Web.HttpContext.Current.Session("DefaultClass") = dtTable.Rows(0).Item("numDefaultClass")
            System.Web.HttpContext.Current.Session("RemoveVendorPOValidation") = dtTable.Rows(0).Item("bitRemoveVendorPOValidation")
            System.Web.HttpContext.Current.Session("SearchOrderCustomerHistory") = dtTable.Rows(0).Item("bitSearchOrderCustomerHistory")

            System.Web.HttpContext.Current.Session("PortalURL") = "http://" & CCommon.ToString(dtTable.Rows(0)("vcPortalName")) & ".bizautomation.com/Login.aspx"
            System.Web.HttpContext.Current.Session("bitAmountPastDue") = CCommon.ToBool(dtTable.Rows(0).Item("bitAmountPastDue"))
            System.Web.HttpContext.Current.Session("AmountPastDue") = CCommon.ToDecimal(dtTable.Rows(0).Item("monAmountPastDue"))

            System.Web.HttpContext.Current.Session("bitRentalItem") = CCommon.ToDecimal(dtTable.Rows(0).Item("bitRentalItem"))
            System.Web.HttpContext.Current.Session("RentalItemClass") = CCommon.ToLong(dtTable.Rows(0).Item("numRentalItemClass"))
            System.Web.HttpContext.Current.Session("RentalHourlyUOM") = CCommon.ToLong(dtTable.Rows(0).Item("numRentalHourlyUOM"))
            System.Web.HttpContext.Current.Session("RentalDailyUOM") = CCommon.ToLong(dtTable.Rows(0).Item("numRentalDailyUOM"))
            System.Web.HttpContext.Current.Session("RentalPriceBasedOn") = CCommon.ToLong(dtTable.Rows(0).Item("tintRentalPriceBasedOn"))
            System.Web.HttpContext.Current.Session("CalendarTimeFormat") = CCommon.ToInteger(dtTable.Rows(0).Item("tintCalendarTimeFormat"))
            System.Web.HttpContext.Current.Session("DefaultClassType") = CCommon.ToInteger(dtTable.Rows(0).Item("tintDefaultClassType"))
            System.Web.HttpContext.Current.Session("DefaultWarehouse") = CCommon.ToLong(dtTable.Rows(0).Item("numDefaultWarehouse"))
            System.Web.HttpContext.Current.Session("PriceBookDiscount") = CCommon.ToLong(dtTable.Rows(0).Item("tintPriceBookDiscount"))

            System.Web.HttpContext.Current.Session("IsEnableProjectTracking") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableProjectTracking"))
            System.Web.HttpContext.Current.Session("IsEnableCampaignTracking") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableCampaignTracking"))
            System.Web.HttpContext.Current.Session("IsEnableResourceScheduling") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableResourceScheduling"))
            System.Web.HttpContext.Current.Session("ShippingServiceItem") = CCommon.ToLong(dtTable.Rows(0).Item("numShippingServiceItemID"))
            System.Web.HttpContext.Current.Session("DiscountServiceItem") = CCommon.ToLong(dtTable.Rows(0).Item("numDiscountServiceItemID"))
            System.Web.HttpContext.Current.Session("CommissionType") = CCommon.ToInteger(dtTable.Rows(0).Item("tintCommissionType"))

            System.Web.HttpContext.Current.Session("ShippingImageWidth") = CCommon.ToInteger(dtTable.Rows(0).Item("intShippingImageWidth"))
            System.Web.HttpContext.Current.Session("ShippingImageHeight") = CCommon.ToInteger(dtTable.Rows(0).Item("intShippingImageHeight"))
            System.Web.HttpContext.Current.Session("TotalInsuredValue") = CCommon.ToDecimal(dtTable.Rows(0).Item("numTotalInsuredValue"))
            System.Web.HttpContext.Current.Session("TotalCustomsValue") = CCommon.ToDecimal(dtTable.Rows(0).Item("numTotalCustomsValue"))
            System.Web.HttpContext.Current.Session("UseBizdocAmount") = CCommon.ToBool(dtTable.Rows(0).Item("bitUseBizdocAmount"))
            System.Web.HttpContext.Current.Session("IsDefaultRateType") = CCommon.ToBool(dtTable.Rows(0).Item("bitDefaultRateType"))
            System.Web.HttpContext.Current.Session("IsIncludeTaxAndShippingInCommission") = CCommon.ToBool(dtTable.Rows(0).Item("bitIncludeTaxAndShippingInCommission"))
            System.Web.HttpContext.Current.Session("IsLandedCost") = CCommon.ToBool(dtTable.Rows(0).Item("bitLandedCost"))
            System.Web.HttpContext.Current.Session("LanedCostDefault") = dtTable.Rows(0).Item("vcLanedCostDefault")

            'Minimum Unit Price related Properties
            System.Web.HttpContext.Current.Session("IsMinUnitPriceRule") = CCommon.ToBool(dtTable.Rows(0).Item("bitMinUnitPriceRule"))
            System.Web.HttpContext.Current.Session("UnitPriceApprover") = CCommon.ToString(dtTable.Rows(0).Item("vcUnitPriceApprover"))
            System.Web.HttpContext.Current.Session("DefaultSalesOppBizDocID") = CCommon.ToLong(dtTable.Rows(0).Item("numDefaultSalesOppBizDocID"))
            System.Web.HttpContext.Current.Session("PODropShipBizDoc") = CCommon.ToLong(dtTable.Rows(0).Item("numPODropShipBizDoc"))
            System.Web.HttpContext.Current.Session("PODropShipBizDocTemplate") = CCommon.ToLong(dtTable.Rows(0).Item("numPODropShipBizDocTemplate"))
            System.Web.HttpContext.Current.Session("IsEnableDeferredIncome") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableDeferredIncome"))
            System.Web.HttpContext.Current.Session("EnableItemLevelUOM") = CCommon.ToBool(dtTable.Rows(0).Item("bitEnableItemLevelUOM"))
            System.Web.HttpContext.Current.Session("PurchaseTaxCredit") = CCommon.ToBool(dtTable.Rows(0).Item("bitPurchaseTaxCredit"))
            System.Web.HttpContext.Current.Session("tintLeadRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintLeadRights"))
            System.Web.HttpContext.Current.Session("tintProspectRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintProspectRights"))
            System.Web.HttpContext.Current.Session("tintAccountRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintAccountRights"))
            System.Web.HttpContext.Current.Session("tintSalesOrderListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintSalesOrderListRights"))
            System.Web.HttpContext.Current.Session("tintPurchaseOrderListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintPurchaseOrderListRights"))
            System.Web.HttpContext.Current.Session("tintSalesOpportunityListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintSalesOpportunityListRights"))
            System.Web.HttpContext.Current.Session("tintPurchaseOpportunityListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintPurchaseOpportunityListRights"))
            System.Web.HttpContext.Current.Session("tintContactListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintContactListRights"))
            System.Web.HttpContext.Current.Session("tintCaseListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintCaseListRights"))
            System.Web.HttpContext.Current.Session("tintProjectListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintProjectListRights"))
            System.Web.HttpContext.Current.Session("tintActionItemRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintActionItemRights"))
            System.Web.HttpContext.Current.Session("IsAllowDuplicateLineItems") = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDuplicateLineItems"))
            System.Web.HttpContext.Current.Session("bitLogoAppliedToBizTheme") = CCommon.ToBool(dtTable.Rows(0).Item("bitLogoAppliedToBizTheme"))
            System.Web.HttpContext.Current.Session("vcLogoForBizTheme") = CCommon.ToString(dtTable.Rows(0).Item("vcLogoForBizTheme"))
            System.Web.HttpContext.Current.Session("vcThemeClass") = CCommon.ToString(dtTable.Rows(0).Item("vcThemeClass"))
            System.Web.HttpContext.Current.Session("bitLogoAppliedToLoginBizTheme") = CCommon.ToBool(dtTable.Rows(0).Item("bitLogoAppliedToLoginBizTheme"))
            System.Web.HttpContext.Current.Session("vcLogoForLoginBizTheme") = CCommon.ToString(dtTable.Rows(0).Item("vcLogoForLoginBizTheme"))
            System.Web.HttpContext.Current.Session("vcLoginURL") = CCommon.ToString(dtTable.Rows(0).Item("vcLoginURL"))
            System.Web.HttpContext.Current.Session("PayrollType") = CCommon.ToShort(dtTable.Rows(0).Item("tintPayrollType"))
            System.Web.HttpContext.Current.Session("IsUserClockedIn") = CCommon.ToBool(dtTable.Rows(0).Item("bitUserClockedIn"))
            System.Web.HttpContext.Current.Session("bitDisplayContractElement") = CCommon.ToBool(dtTable.Rows(0).Item("bitDisplayContractElement"))
            System.Web.HttpContext.Current.Session("vcEmployeeForContractTimeElement") = CCommon.ToString(dtTable.Rows(0).Item("vcEmployeeForContractTimeElement"))

            If dtTable.Rows(0).Item("numUserID") <= 0 Then
                'Return "Err01"
                Return New String() {"href", "admin/authentication.aspx?mesg=AS"}
                Exit Function
            End If

            If dtTable.Rows(0).Item("bitActivateFlag") = False Then
                'Return "Err02"
                Return New String() {"href", "admin/authentication.aspx?mesg=Deactivated"}
                Exit Function
            End If

            Dim objAdmin As New CAdmin
            Dim dtTab As DataTable
            objAdmin.UserCntID = System.Web.HttpContext.Current.Session("UserContactId")
            objAdmin.DomainID = System.Web.HttpContext.Current.Session("DomainId")
            objAdmin.DivisionID = System.Web.HttpContext.Current.Session("UserDivisionID")
            dtTab = objAdmin.GetDefaultTabName()
            System.Web.HttpContext.Current.Session("AccessID") = objAdmin.AddLoggedInDetails()
            If dtTab.Rows.Count > 0 Then System.Web.HttpContext.Current.Session("DefaultTab") = dtTab
            Dim objQueryStringValues As New QueryStringValues
            System.Web.HttpContext.Current.Session("objQSV") = objQueryStringValues

            'Add AccessID to Cookie 
            'Dim Cookie As New HttpCookie("AccessID")
            'Cookie("numAccessID") = Session("AccessID")
            'Cookie.Domain = ".bizautomation.com"
            'Cookie.Expires = DateTime.Now.AddDays(50)
            'Response.Cookies.Add(Cookie)
            Dim paged As New BACRMPage
            'If paged.GetQueryStringVal("From") = "Help" Then
            '    Dim HelpURL As String = ConfigurationManager.AppSettings("HelpURL")

            '    HelpURL = HelpURL & "?a=" & objCommonLinkdein.Encrypt(System.Web.HttpContext.Current.Session("AccessID")) & "&pageurl=" & CCommon.ToString(paged.GetQueryStringVal("pageurl"))
            '    HttpContext.Current.Response.Redirect(HelpURL)
            '    Exit Function
            'Else
            Return New String() {"href", "Home.aspx?ClientMachineUTCTimeOffset=" & offset}
            'Exit Function
            'End If

        Catch ex As Exception
            If ex.Message.Contains("SUB_RENEW") Then
                Return New String() {"label", "Your subscription is expired! please contact your administrator."}
            ElseIf ex.Message.Contains("MULTIPLE_RECORDS_WITH_SAME_EMAIL") Then
                Return New String() {"label", "Multiple records with same email exists! please contact your administrator."}
            Else
                ExceptionModule.ExceptionPublish(ex, System.Web.HttpContext.Current.Session("DomainID"), System.Web.HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Return New String() {"label", "Your subscription is expired! please contact your administrator."}
            End If
            'Throw ex
        End Try
    End Function

    Private Function sb_GetDomainUser(ByVal isLinkedInLogin As Boolean) As Boolean
        Try
            Dim dsUserDetails As DataSet
            Dim dtTable As DataTable
            Dim plainText As String
            Dim cipherText As String

            plainText = strPassword
            cipherText = objCommon.Encrypt(plainText)

            If isLinkedInLogin Then
                If hdnLinkdein.Value = "" Then
                    litMessage.Text = "<span class='btn btn-danger'>LinkedIn access is not enabled yet.</span>"
                    Return False
                    Exit Function
                End If

                dsUserDetails = objCommon.CheckUserAtLogin("", "", hdnLinkdein.Value)
                dtTable = dsUserDetails.Tables(0)

                If dtTable.Rows.Count <> 1 Then
                    litMessage.Text = "<span class='btn btn-danger'>LinkedIn access is not enabled yet.</span>"
                    Return False
                    Exit Function
                End If
            Else
                If Not String.IsNullOrEmpty(CCommon.ToString(GetQueryStringVal("org")).Trim()) Then
                    Dim lobjUserAccess As New UserAccess
                    lobjUserAccess.vcLoginURL = GetQueryStringVal("org")
                    lobjUserAccess.DomainID = 0
                    Dim dt As DataTable = lobjUserAccess.GetDomainDetailsByURL()

                    If Not dt Is Nothing AndAlso dt.Rows.Count = 1 Then
                        objCommon.DomainID = CCommon.ToLong(dt.Rows(0)("numDomainId"))
                    End If
                End If

                dsUserDetails = objCommon.CheckUserAtLogin(strUserName, cipherText, "N")
                dtTable = dsUserDetails.Tables(0)

                If dtTable.Rows.Count <> 1 Then
                    litMessage.Text = "<span class='btn btn-danger'>Please Check Email Address and Password !</span>"
                    Return False
                    Exit Function
                End If
            End If

            Dim userData As String = CCommon.ToString(dtTable.Rows(0).Item("numDomainID")) & "~" & CCommon.ToString(dtTable.Rows(0).Item("numUserDetailId"))
            Dim formUserID As String = ""


            If isLinkedInLogin Then
                formUserID = CCommon.ToString(dtTable.Rows(0).Item("vcEmailID"))
            Else
                formUserID = strUserName
            End If

            If CCommon.ToLong(dtTable.Rows(0)("numUserID")) > 0 AndAlso CCommon.ToInteger(dtTable.Rows(0)("intFullUserConcurrency")) > 0 Then
                Dim objUserAccess As New UserAccess
                objUserAccess.UserId = 0
                objUserAccess.DomainID = CCommon.ToLong(dtTable.Rows(0)("numDomainID"))
                Dim dtUsers As DataTable = objUserAccess.GetUserAccessDetails

                Dim loggedInUsers As Integer = 0

                If Not dtUsers Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                    For Each drUser As DataRow In dtUsers.Rows
                        If Not Cache("SessionTimeOut" & CCommon.ToLong(drUser("numUserID"))) Is Nothing Then
                            loggedInUsers += 1
                        End If
                    Next
                End If

                If loggedInUsers < CCommon.ToInteger(dtTable.Rows(0)("intFullUserConcurrency")) Then
                    msUserID = dtTable.Rows(0)("numUserID")
                    msSessionCacheValue = ""

                    DirectCast(Context.ApplicationInstance, [Global]).InsertSessionCacheItem(msUserID)
                    ' Test to access the cache and make sure it still exists
                    msSessionCacheValue = DirectCast(Context.ApplicationInstance, [Global]).GetSessionCacheItem(msUserID)
                Else
                    litMessage.Text = "<span class='btn btn-danger'>Please Check another user already login!</span>"
                    Return False
                    Exit Function
                End If
            End If

            Dim ticket As System.Web.Security.FormsAuthenticationTicket = New System.Web.Security.FormsAuthenticationTicket(1, formUserID, DateTime.Now, DateTime.Now.AddMinutes(120), False, userData, System.Web.Security.FormsAuthentication.FormsCookiePath)
            Dim encTicket As String = System.Web.Security.FormsAuthentication.Encrypt(ticket)
            Response.Cookies.Add(New HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, encTicket))

            vcDefaultNavURL = dtTable.Rows(0).Item("vcDefaultNavURL")
            Session("bitUseOnlyActionItems") = dtTable.Rows(0).Item("bitUseOnlyActionItems")
            Session("numAuthorizePercentage") = dtTable.Rows(0).Item("numAuthorizePercentage")
            Session("bitMarginPriceViolated") = dtTable.Rows(0).Item("bitMarginPriceViolated")
            Session("numDefaultPurchaseShippingDoc") = dtTable.Rows(0).Item("numDefaultPurchaseShippingDoc")
            Session("numDefaultSalesShippingDoc") = dtTable.Rows(0).Item("numDefaultSalesShippingDoc")
            Session("ProfilePic") = dtTable.Rows(0).Item("ProfilePic")
            Session("numCost") = dtTable.Rows(0).Item("numCost")
            Session("bitApprovalforTImeExpense") = dtTable.Rows(0).Item("bitApprovalforTImeExpense")
            Session("intTimeExpApprovalProcess") = dtTable.Rows(0).Item("intTimeExpApprovalProcess")
            Session("intOpportunityApprovalProcess") = dtTable.Rows(0).Item("intOpportunityApprovalProcess")
            'Get the Domain ID        
            Session("DomainID") = dtTable.Rows(0).Item("numDomainID") ''Domain ID
            'Session("MailName") = dtTable.Rows(0).Item("vcMailNickName") 'not being used anywhere -commented by chintan
            Session("UserEmail") = dtTable.Rows(0).Item("vcEmailID") ''Email ID
            Session("UserEmailAlias") = CCommon.ToString(dtTable.Rows(0).Item("vcEmailAlias"))
            Dim objEncrypt As New CCommon
            Session("UserEmailAliasPassword") = If(String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0).Item("vcEmailAliasPassword"))), "", CCommon.ToString(dtTable.Rows(0).Item("vcEmailAliasPassword")))
            Session("UserID") = dtTable.Rows(0).Item("numUserID")  '' User ID
            Session("Signature") = dtTable.Rows(0).Item("txtSignature") ''Signature
            Session("CompanyName") = dtTable.Rows(0).Item("vcCompanyName") ''Parent Company Name
            Session("ContactName") = dtTable.Rows(0).Item("ContactName")  ''Contact NAme
            lngUserID = objCommon.UserID    'Set the session into a local variable
            Session("UserContactID") = dtTable.Rows(0).Item("numUserDetailId") ''User Contact ID
            Session("UserGroupID") = dtTable.Rows(0).Item("numGroupID")   ''Group ID
            Session("UserDivisionID") = dtTable.Rows(0).Item("numDivisionID") ''Division ID
            Session("ExchIntegration") = dtTable.Rows(0).Item("bitExchangeIntegration") '' Exchange Integration
            Session("ImapIntegration") = dtTable.Rows(0).Item("bitImapIntegration") '' Exchange Integration
            Session("DateFormat") = dtTable.Rows(0).Item("vcDateFormat") 'DateFormat
            ' Session("AddressForOutLook") = dtTable.Rows(0).Item("tintAddForOutlook") '' Address For Exporting To Outlook
            Session("PagingRows") = dtTable.Rows(0).Item("tintCustomPagingRows") ''Custom paging Rows
            Session("DefCountry") = dtTable.Rows(0).Item("numDefCountry")  ''Default country
            Session("CompWindow") = dtTable.Rows(0).Item("tintComposeWindow") ''  Compose Window when click on Email
            Session("StartDate") = dtTable.Rows(0).Item("StartDate") '' Start date
            Session("EndDate") = dtTable.Rows(0).Item("EndDate") '' End date
            Session("PopulateUserCriteria") = dtTable.Rows(0).Item("tintAssignToCriteria")     ''Populate Users Based on Certailn Criteria
            Session("EnableIntMedPage") = IIf(dtTable.Rows(0).Item("bitIntmedPage") = True, 1, 0) ' Status of Intermediatory page
            Session("FiscalMonth") = dtTable.Rows(0).Item("tintFiscalStartMonth")
            Session("AdminID") = dtTable.Rows(0).Item("numAdminID")
            Session("Team") = dtTable.Rows(0).Item("numTeam")
            Session("Currency") = dtTable.Rows(0).Item("vccurrency") & " " ' this is base currency symbol
            Session("Trial") = dtTable.Rows(0).Item("bitTrial")
            Session("DomainName") = dtTable.Rows(0).Item("vcDomainName")
            Session("SMTPServer") = dtTable.Rows(0).Item("vcSMTPServer")
            Session("SMTPPort") = dtTable.Rows(0).Item("numSMTPPort")
            Session("SMTPAuth") = dtTable.Rows(0).Item("bitSMTPAuth")
            Session("SMTPPassword") = dtTable.Rows(0).Item("vcSmtpPassword")
            Session("bitSMTPSSL") = dtTable.Rows(0).Item("bitSMTPSSL")
            Session("SMTPServerIntegration") = dtTable.Rows(0).Item("bitSMTPServer")
            Session("ChrForItemSearch") = dtTable.Rows(0).Item("tintChrForItemSearch")
            Session("ChrForCompSearch") = dtTable.Rows(0).Item("tintChrForComSearch")
            Session("UnitSystem") = dtTable.Rows(0).Item("UnitSystem")
            Session("NoOfDaysLeft") = dtTable.Rows(0).Item("NoOfDaysLeft")
            Session("BaseCurrencyID") = dtTable.Rows(0).Item("numCurrencyID")
            Session("MultiCurrency") = dtTable.Rows(0).Item("bitMultiCurrency")
            Session("SOBizDocStatus") = dtTable.Rows(0).Item("numSOBizDocStatus")
            Session("AutolinkUnappliedPayment") = dtTable.Rows(0).Item("bitAutolinkUnappliedPayment")
            Session("DiscountOnUnitPrice") = dtTable.Rows(0).Item("bitDiscountOnUnitPrice")


            Session("SubscriberID") = dtTable.Rows(0).Item("numSubscriberID")
            Session("DefaultShippingCompany") = dtTable.Rows(0).Item("numShipCompany")
            Session("OrganizationSearchCriteria") = dtTable.Rows(0).Item("tintOrganizationSearchCriteria")
            Session("DocumentRepositaryEnabled") = dtTable.Rows(0).Item("bitDocumentRepositary")

            Session("GtoBContact") = dtTable.Rows(0).Item("bitGtoBContact")
            Session("BtoGContact") = dtTable.Rows(0).Item("bitBtoGContact")
            Session("GtoBCalendar") = dtTable.Rows(0).Item("bitGtoBCalendar")
            Session("BtoGCalendar") = dtTable.Rows(0).Item("bitBtoGCalendar")
            Session("IsDirtyForm") = dtTable.Rows(0).Item("bitTrakDirtyForm")

            Session("BaseTaxOnArea") = dtTable.Rows(0).Item("tintBaseTaxOnArea")
            Session("TabEvent") = dtTable.Rows(0).Item("tintTabEvent")
            Session("AutoSerialNoAssign") = dtTable.Rows(0).Item("bitAutoSerialNoAssign")
            'No longer sutpporting exchange integration
            'If dtTable.Rows(0).Item("bitExchangeIntegration") = True Then
            '    Session("AccessMethod") = dtTable.Rows(0).Item("bitAccessExchange")
            '    Session("ExchURL") = dtTable.Rows(0).Item("vcExchPath")
            '    Session("ExchDomain") = dtTable.Rows(0)t.Iem("vcExchDomain")
            '    If IsDBNull(dtTable.Rows(0).Item("vcExchPassword")) = False Then
            '        If dtTable.Rows(0).Item("vcExchPassword") <> "" Then
            '            Session("ExchPassword") = objCommon.Decrypt(dtTable.Rows(0).Item("vcExchPassword"))
            '        End If
            '    End If
            '    If dtTable.Rows(0).Item("bitAccessExchange") = False Then
            '        Session("ExchUserName") = dtTable.Rows(0).Item("vcEmailID").ToString.Split("@")(0)
            '    Else : Session("ExchUserName") = dtTable.Rows(0).Item("vcExchUserName").ToString.Split("@")(0)
            '    End If
            'End If

            Session("UserTerritory") = dsUserDetails.Tables(1)
            Session("SiteType") = Request.Url.Scheme + ":"
            Session("BaseTaxCalcOn") = dtTable.Rows(0).Item("tintBaseTax")
            Session("EnableEmbeddedCost") = dtTable.Rows(0).Item("bitEmbeddedCost")
            Session("AuthoritativeSalesBizDoc") = dtTable.Rows(0).Item("numAuthoritativeSales")
            Session("AuthoritativePurchaseBizDoc") = dtTable.Rows(0).Item("numAuthoritativePurchase")
            Session("DecimalPoints") = dtTable.Rows(0).Item("tintDecimalPoints")
            Session("DefaultBillToForPO") = dtTable.Rows(0).Item("tintBillToForPO")
            Session("DefaultShipToForPO") = dtTable.Rows(0).Item("tintShipToForPO")
            Session("EnableNonInventoryItemExpense") = dtTable.Rows(0).Item("bitExpenseNonInventoryItem")
            Session("InlineEdit") = dtTable.Rows(0).Item("bitInlineEdit")
            Session("DefaultClass") = dtTable.Rows(0).Item("numDefaultClass")
            Session("RemoveVendorPOValidation") = dtTable.Rows(0).Item("bitRemoveVendorPOValidation")
            Session("SearchOrderCustomerHistory") = dtTable.Rows(0).Item("bitSearchOrderCustomerHistory")

            Session("PortalURL") = "http://" & CCommon.ToString(dtTable.Rows(0)("vcPortalName")) & ".bizautomation.com/Login.aspx"
            Session("bitAmountPastDue") = CCommon.ToBool(dtTable.Rows(0).Item("bitAmountPastDue"))
            Session("AmountPastDue") = CCommon.ToDecimal(dtTable.Rows(0).Item("monAmountPastDue"))

            Session("bitRentalItem") = CCommon.ToDecimal(dtTable.Rows(0).Item("bitRentalItem"))
            Session("RentalItemClass") = CCommon.ToLong(dtTable.Rows(0).Item("numRentalItemClass"))
            Session("RentalHourlyUOM") = CCommon.ToLong(dtTable.Rows(0).Item("numRentalHourlyUOM"))
            Session("RentalDailyUOM") = CCommon.ToLong(dtTable.Rows(0).Item("numRentalDailyUOM"))
            Session("RentalPriceBasedOn") = CCommon.ToLong(dtTable.Rows(0).Item("tintRentalPriceBasedOn"))
            Session("CalendarTimeFormat") = CCommon.ToInteger(dtTable.Rows(0).Item("tintCalendarTimeFormat"))
            Session("DefaultClassType") = CCommon.ToInteger(dtTable.Rows(0).Item("tintDefaultClassType"))
            Session("DefaultWarehouse") = CCommon.ToLong(dtTable.Rows(0).Item("numDefaultWarehouse"))
            Session("PriceBookDiscount") = CCommon.ToLong(dtTable.Rows(0).Item("tintPriceBookDiscount"))

            Session("IsEnableProjectTracking") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableProjectTracking"))
            Session("IsEnableCampaignTracking") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableCampaignTracking"))
            Session("IsEnableResourceScheduling") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableResourceScheduling"))
            Session("ShippingServiceItem") = CCommon.ToLong(dtTable.Rows(0).Item("numShippingServiceItemID"))
            Session("DiscountServiceItem") = CCommon.ToLong(dtTable.Rows(0).Item("numDiscountServiceItemID"))
            Session("CommissionType") = CCommon.ToInteger(dtTable.Rows(0).Item("tintCommissionType"))

            Session("ShippingImageWidth") = CCommon.ToInteger(dtTable.Rows(0).Item("intShippingImageWidth"))
            Session("ShippingImageHeight") = CCommon.ToInteger(dtTable.Rows(0).Item("intShippingImageHeight"))
            Session("TotalInsuredValue") = CCommon.ToDecimal(dtTable.Rows(0).Item("numTotalInsuredValue"))
            Session("TotalCustomsValue") = CCommon.ToDecimal(dtTable.Rows(0).Item("numTotalCustomsValue"))
            Session("UseBizdocAmount") = CCommon.ToBool(dtTable.Rows(0).Item("bitUseBizdocAmount"))
            Session("IsDefaultRateType") = CCommon.ToBool(dtTable.Rows(0).Item("bitDefaultRateType"))
            Session("IsIncludeTaxAndShippingInCommission") = CCommon.ToBool(dtTable.Rows(0).Item("bitIncludeTaxAndShippingInCommission"))
            Session("IsLandedCost") = CCommon.ToBool(dtTable.Rows(0).Item("bitLandedCost"))
            Session("LanedCostDefault") = dtTable.Rows(0).Item("vcLanedCostDefault")

            'Minimum Unit Price related Properties
            Session("IsMinUnitPriceRule") = CCommon.ToBool(dtTable.Rows(0).Item("bitMinUnitPriceRule"))
            Session("UnitPriceApprover") = CCommon.ToString(dtTable.Rows(0).Item("vcUnitPriceApprover"))
            Session("DefaultSalesOppBizDocID") = CCommon.ToLong(dtTable.Rows(0).Item("numDefaultSalesOppBizDocID"))
            Session("PODropShipBizDoc") = CCommon.ToLong(dtTable.Rows(0).Item("numPODropShipBizDoc"))
            Session("PODropShipBizDocTemplate") = CCommon.ToLong(dtTable.Rows(0).Item("numPODropShipBizDocTemplate"))
            Session("IsEnableDeferredIncome") = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableDeferredIncome"))
            Session("EnableItemLevelUOM") = CCommon.ToBool(dtTable.Rows(0).Item("bitEnableItemLevelUOM"))
            Session("PurchaseTaxCredit") = CCommon.ToBool(dtTable.Rows(0).Item("bitPurchaseTaxCredit"))
            If dtTable.Rows(0).Item("bintCreatedDate") Is DBNull.Value Then
                Session("UserCeatedDate") = ""
            Else
                Session("UserCeatedDate") = CCommon.ToSqlDate(dtTable.Rows(0).Item("bintCreatedDate"))
            End If

            Session("tintLeadRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintLeadRights"))
            Session("tintProspectRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintProspectRights"))
            Session("tintAccountRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintAccountRights"))
            Session("tintSalesOrderListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintSalesOrderListRights"))
            Session("tintPurchaseOrderListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintPurchaseOrderListRights"))
            Session("tintSalesOpportunityListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintSalesOpportunityListRights"))
            Session("tintPurchaseOpportunityListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintPurchaseOpportunityListRights"))
            Session("tintContactListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintContactListRights"))
            Session("tintCaseListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintCaseListRights"))
            Session("tintProjectListRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintProjectListRights"))
            Session("tintActionItemRights") = CCommon.ToShort(dtTable.Rows(0).Item("tintActionItemRights"))
            Session("IsAllowDuplicateLineItems") = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDuplicateLineItems"))
            Session("bitLogoAppliedToBizTheme") = CCommon.ToBool(dtTable.Rows(0).Item("bitLogoAppliedToBizTheme"))
            Session("vcLogoForBizTheme") = CCommon.ToString(dtTable.Rows(0).Item("vcLogoForBizTheme"))
            Session("vcThemeClass") = CCommon.ToString(dtTable.Rows(0).Item("vcThemeClass"))
            Session("bitLogoAppliedToLoginBizTheme") = CCommon.ToBool(dtTable.Rows(0).Item("bitLogoAppliedToLoginBizTheme"))
            Session("vcLogoForLoginBizTheme") = CCommon.ToString(dtTable.Rows(0).Item("vcLogoForLoginBizTheme"))
            Session("vcLoginURL") = CCommon.ToString(dtTable.Rows(0).Item("vcLoginURL"))
            Session("PayrollType") = CCommon.ToShort(dtTable.Rows(0).Item("tintPayrollType"))
            Session("IsUserClockedIn") = CCommon.ToBool(dtTable.Rows(0).Item("bitUserClockedIn"))

            Session("bitDisplayContractElement") = CCommon.ToBool(dtTable.Rows(0).Item("bitDisplayContractElement"))
            Session("vcEmployeeForContractTimeElement") = CCommon.ToString(dtTable.Rows(0).Item("vcEmployeeForContractTimeElement"))

            Session("bitDisplayCustomField") = CCommon.ToBool(dtTable.Rows(0).Item("bitDisplayCustomField"))
            Session("bitFollowupAnytime") = CCommon.ToBool(dtTable.Rows(0).Item("bitFollowupAnytime"))
            Session("bitpartycalendarTitle") = CCommon.ToBool(dtTable.Rows(0).Item("bitpartycalendarTitle"))
            Session("bitpartycalendarLocation") = CCommon.ToBool(dtTable.Rows(0).Item("bitpartycalendarLocation"))
            Session("bitpartycalendarDescription") = CCommon.ToBool(dtTable.Rows(0).Item("bitpartycalendarDescription"))
            Session("bitREQPOApproval") = CCommon.ToBool(dtTable.Rows(0).Item("bitREQPOApproval"))
            Session("bitARInvoiceDue") = CCommon.ToBool(dtTable.Rows(0).Item("bitARInvoiceDue"))
            Session("bitAPBillsDue") = CCommon.ToBool(dtTable.Rows(0).Item("bitAPBillsDue"))
            Session("bitItemsToPickPackShip") = CCommon.ToBool(dtTable.Rows(0).Item("bitItemsToPickPackShip"))
            Session("bitItemsToInvoice") = CCommon.ToBool(dtTable.Rows(0).Item("bitItemsToInvoice"))
            Session("bitSalesOrderToClose") = CCommon.ToBool(dtTable.Rows(0).Item("bitSalesOrderToClose"))
            Session("bitItemsToPutAway") = CCommon.ToBool(dtTable.Rows(0).Item("bitItemsToPutAway"))
            Session("bitItemsToBill") = CCommon.ToBool(dtTable.Rows(0).Item("bitItemsToBill"))
            Session("bitPosToClose") = CCommon.ToBool(dtTable.Rows(0).Item("bitPosToClose"))
            Session("bitBOMSToPick") = CCommon.ToBool(dtTable.Rows(0).Item("bitBOMSToPick"))
            Session("vchREQPOApprovalEmp") = CCommon.ToString(dtTable.Rows(0).Item("vchREQPOApprovalEmp"))
            Session("vchARInvoiceDue") = CCommon.ToString(dtTable.Rows(0).Item("vchARInvoiceDue"))
            Session("vchAPBillsDue") = CCommon.ToString(dtTable.Rows(0).Item("vchAPBillsDue"))
            Session("vchItemsToPickPackShip") = CCommon.ToString(dtTable.Rows(0).Item("vchItemsToPickPackShip"))
            Session("vchItemsToInvoice") = CCommon.ToString(dtTable.Rows(0).Item("vchItemsToInvoice"))
            Session("vchPriceMarginApproval") = CCommon.ToString(dtTable.Rows(0).Item("vchPriceMarginApproval"))
            Session("vchSalesOrderToClose") = CCommon.ToString(dtTable.Rows(0).Item("vchSalesOrderToClose"))
            Session("vchItemsToPutAway") = CCommon.ToString(dtTable.Rows(0).Item("vchItemsToPutAway"))
            Session("vchItemsToBill") = CCommon.ToString(dtTable.Rows(0).Item("vchItemsToBill"))
            Session("vchPosToClose") = CCommon.ToString(dtTable.Rows(0).Item("vchPosToClose"))
            Session("vchBOMSToPick") = CCommon.ToString(dtTable.Rows(0).Item("vchBOMSToPick"))
            Session("decReqPOMinValue") = CCommon.ToDecimal(dtTable.Rows(0).Item("decReqPOMinValue"))
            Session("vcElectiveItemFields") = CCommon.ToString(dtTable.Rows(0).Item("vcElectiveItemFields"))
            Session("bitEnableSmartyStreets") = CCommon.ToBool(dtTable.Rows(0).Item("bitEnableSmartyStreets"))
            Session("vcSmartyStreetsAPIKeys") = CCommon.ToString(dtTable.Rows(0).Item("vcSmartyStreetsAPIKeys"))
            Session("bitAllowToEditHelpFile") = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowToEditHelpFile"))

            PersistTable.Load(strParam:="SimpleSearchType", isMasterPage:=True)
            If PersistTable.Count > 0 Then
                Session("SimpleSearchType") = CCommon.ToString(PersistTable("SimpleSearchType"))
            End If

            If dtTable.Rows(0).Item("numUserID") <= 0 AndAlso CCommon.ToLong(dtTable.Rows(0).Item("numExtranetDtlID")) <= 0 Then
                Response.Redirect("admin/authentication.aspx?mesg=AS", False)
                Exit Function
            End If

            If dtTable.Rows(0).Item("bitActivateFlag") = False Then
                Response.Redirect("admin/authentication.aspx?mesg=Deactivated", False)
                Exit Function
            End If

            Dim objAdmin As New CAdmin
            Dim dtTab As DataTable
            objAdmin.UserCntID = Session("UserContactId")
            objAdmin.DomainID = Session("DomainId")
            objAdmin.DivisionID = Session("UserDivisionID")
            dtTab = objAdmin.GetDefaultTabName()
            Session("AccessID") = objAdmin.AddLoggedInDetails()
            If dtTab.Rows.Count > 0 Then Session("DefaultTab") = dtTab
            Dim objQueryStringValues As New QueryStringValues
            Session("objQSV") = objQueryStringValues

            'CLEAR MENU CATCH
            Cache.Remove("BizMenu" & Session("UserContactId"))
            Cache.Remove("BizAdminMenu" & Session("UserContactId"))
            Cache.Remove("BizAdvanceSearchMenu" & Session("UserContactId"))
            Cache.Remove("Alert_Company_" & Session("UserContactId"))
            Cache.Remove("Alert_Case_" & Session("UserContactId"))
            Cache.Remove("Alert_Opp_" & Session("UserContactId"))
            Cache.Remove("Alert_Order_" & Session("UserContactId"))
            Cache.Remove("Alert_Project_" & Session("UserContactId"))
            Cache.Remove("Alert_Email_" & Session("UserContactId"))
            Cache.Remove("Alert_Tickler_" & Session("UserContactId"))
            Cache.Remove("Alert_Process_" & Session("UserContactId"))

            Return True
        Catch ex As Exception
            If ex.Message.Contains("SUB_RENEW") Then
                litMessage.Text = "<span class='btn btn-danger'>Your subscription is expired! please contact your administrator.</span>"
                Return False
            ElseIf ex.Message.Contains("MULTIPLE_RECORDS_WITH_SAME_EMAIL") Then
                litMessage.Text = "<span class='btn btn-danger'>Multiple records with same email exists! please contact your administrator.</span>"
                Return False
            Else
                Dim exception As System.Reflection.ReflectionTypeLoadException = TryCast(ex, System.Reflection.ReflectionTypeLoadException)

                If Not exception Is Nothing Then
                    Dim errroMessage As String = ""

                    For Each loaderException As Exception In exception.LoaderExceptions
                        errroMessage = errroMessage & ";" & loaderException.Message
                    Next

                    Throw New Exception(errroMessage)
                Else
                    Throw
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Return False
                End If
            End If
            'Throw ex
        End Try
    End Function

    Private Sub lnkForgot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkForgot.Click
        Try
            trLogin.Visible = False
            trForgotPwd.Visible = True
            trPassword.Visible = False
            trForgotPwdLink.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            trLogin.Visible = True
            trForgotPwd.Visible = False
            trPassword.Visible = True
            trForgotPwdLink.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSendPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendPwd.Click
        Try
            Dim objUserAccess As New UserAccess()
            objUserAccess.Email = txtEmailPas.Text.Trim()
            objUserAccess.boolPortal = False
            objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
            objUserAccess.SendPassoword()
            If objUserAccess.NoOfRows = 1 Then
                Dim objSendMail As New Email()

                objSendMail.SendSystemEmail("Your Password !", "Hi <br><br><br>   Your password is " & objCommon.Decrypt(objUserAccess.Password), "", ConfigurationManager.AppSettings("FromAddress"), txtEmaillAdd.Text.Trim)
                litMessage.Text = "<span class='btn btn-success'>Your Password has been sent to your registered email address !</span>"
            ElseIf objUserAccess.NoOfRows = 0 Then
                litMessage.Text = "<span class='btn btn-danger'>Check Your Email Id and try Again !</span>"
            ElseIf objUserAccess.NoOfRows > 1 Then
                litMessage.Text = "<span class='btn btn-danger'>Found Multiple Records.Contact Administrator !</span>"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub LoginUsingToken()
        Try
            SetOffset()


            Dim strToken As String = objCommon.Decrypt(HttpUtility.UrlDecode(GetQueryStringVal("Token")))
            Dim strCurrentDate As String
            Dim strArray As Array = strToken.Split(New String() {"&*%"}, StringSplitOptions.RemoveEmptyEntries)
            strUserName = strArray(0)
            strPassword = strArray(1)
            strCurrentDate = strArray(2)
            If DateDiff(DateInterval.Minute, CDate(strCurrentDate), DateTime.Now) > 20 Or DateDiff(DateInterval.Minute, CDate(strCurrentDate), DateTime.Now) < 0 Then
                System.Web.Security.FormsAuthentication.SignOut()
                Session.Abandon()
            End If

            If sb_GetDomainUser(False) = False Then
                System.Web.Security.FormsAuthentication.SignOut()
                Session.Abandon()
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

            System.Web.Security.FormsAuthentication.SignOut()
            Session.Abandon()
            Exit Sub
        End Try

        Response.Redirect("~/Home.aspx", False)
        'Response.Redirect("common/TicklerDisplay.aspx?ClientMachineUTCTimeOffset=" & txtOffset.Text.Trim)
    End Sub


    Private Sub SetOffset()
        'using new Date().getTimezoneOffset(); js function to get timezone difference
        Session("ClientMachineUTCTimeOffset") = txtOffset.Text.Trim
    End Sub


End Class
