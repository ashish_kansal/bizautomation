﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json
Imports System.Net
Imports System.IO
Imports System.Text

Public Class Office365Authentication
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim office365AppClientID As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365AppClientID"))
            Dim office365AppClientSecret As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365AppClientSecret"))
            Dim office365AppRedirectURL As String = CCommon.ToString(ConfigurationManager.AppSettings("office365AppRedirectURL"))

            If Not Page.IsPostBack Then
                If Session("GoogleEmailId") IsNot Nothing AndAlso Session("GoogleContactId") IsNot Nothing Then
                    If Not String.IsNullOrEmpty(GetQueryStringVal("code")) Then
                        btnContinue.Visible = False

                        Dim postData As String = ""
                        postData += "client_id" + "=" + HttpUtility.UrlEncode(office365AppClientID) + "&"
                        postData += "code" + "=" + HttpUtility.UrlEncode(GetQueryStringVal("code")) + "&"
                        postData += "scope" + "=" + HttpUtility.UrlEncode("https://graph.microsoft.com/Calendars.ReadWrite") + "&"
                        postData += "redirect_uri" + "=" + HttpUtility.UrlEncode(office365AppRedirectURL) + "&"
                        postData += "grant_type" + "=" + HttpUtility.UrlEncode("authorization_code") + "&"
                        postData += "client_secret" + "=" + HttpUtility.UrlEncode(office365AppClientSecret)

                        Dim request As HttpWebRequest = HttpWebRequest.Create("https://login.microsoftonline.com/common/oauth2/v2.0/token")
                        request.Method = "POST"
                        request.ContentType = "application/x-www-form-urlencoded"

                        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
                        request.ContentLength = byteArray.Length

                        Dim dataStream As Stream = request.GetRequestStream()
                        dataStream.Write(byteArray, 0, byteArray.Length)
                        dataStream.Close()

                        Try
                            Dim response As HttpWebResponse = request.GetResponse()
                            Dim responseString As String = New StreamReader(response.GetResponseStream()).ReadToEnd()
                            response.Close()

                            Dim objOffice365TokenResponse As Office365TokenResponse = JsonConvert.DeserializeObject(Of Office365TokenResponse)(responseString)

                            If Not objOffice365TokenResponse Is Nothing Then
                                Dim objUserAccess As New UserAccess
                                objUserAccess.ContactID = Session("GoogleContactId")
                                objUserAccess.DomainID = Session("DomainID")
                                objUserAccess.UpdateGoogleRefreshToken(objOffice365TokenResponse.RefreshToken)

                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CloseAuthenticationWindow", "alert('Microsoft office 365 authentication completed successfully'); self.close();", True)
                            End If
                        Catch exWeb As WebException
                            If exWeb.Response IsNot Nothing Then
                                Dim tokenResponse As String = New StreamReader(exWeb.Response.GetResponseStream()).ReadToEnd()
                                Dim objOffice365TokenErrorResponse As Office365TokenErrorResponse = JsonConvert.DeserializeObject(Of Office365TokenErrorResponse)(tokenResponse)
                                DisplayError(objOffice365TokenErrorResponse.ErrorMessage)
                            End If
                        Catch ex As Exception
                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Page.Request)
                            DisplayError(CCommon.ToString(ex))
                        End Try
                    ElseIf Not String.IsNullOrEmpty(GetQueryStringVal("error")) Then
                        Dim errorMessage As String = CCommon.ToString(GetQueryStringVal("error"))
                        Dim errorDescription As String = CCommon.ToString(GetQueryStringVal("error_description"))

                        btnContinue.Visible = False
                        DisplayError(errorMessage & ": " & errorDescription)
                    Else
                        lblInstruction.Text = "You will be redirected to Office 365 Login Page for authorization on continue click.<br /><br /> Please login using your ""<b>" & Session("GoogleEmailId") & "</b>"" email and say ""Allow"" when prompted for authorization."
                    End If
                Else
                    btnContinue.Visible = False
                    DisplayError("Configure smtp settings")
                End If
            Else
                Dim url As String = String.Format("https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id={0}&response_type=code&redirect_uri={1}&response_mode=query&scope=offline_access%20Calendars.ReadWrite", office365AppClientID, office365AppRedirectURL)
                Response.Redirect(url, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub


#Region "Private Methods"

    Private Sub DisplayError(ByVal exception As String)
        Try
            lblPageError.Text = exception
            divPageError.Style.Add("display", "")
            divPageError.Focus()
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Class Office365TokenResponse
        <JsonProperty("access_token")>
        Public AccessToken As String

        <JsonProperty("token_type")>
        Public TokenType As String

        <JsonProperty("expires_in")>
        Public ExpiresIn As Long

        <JsonProperty("scope")>
        Public Scope As String

        <JsonProperty("refresh_token")>
        Public RefreshToken As String

        <JsonProperty("id_token")>
        Public IdToken As String
    End Class

    Private Class Office365TokenErrorResponse
        <JsonProperty("error")>
        Public ErrorMessage As String

        <JsonProperty("error_description")>
        Public ErrorDescription As String

        <JsonProperty("error_codes")>
        Public ErrorCodes As System.Collections.Generic.List(Of String)

        <JsonProperty("timestamp")>
        Public TimeStamp As String

        <JsonProperty("trace_id")>
        Public TraceID As String

        <JsonProperty("correlation_id")>
        Public CorrelationId As String
    End Class

End Class