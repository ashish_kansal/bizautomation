﻿Imports Infragistics.WebUI
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class frmMultiTB
        Inherits BACRMPage

        Dim objMultiAccounting As New MultiCompany
        Dim intDomainID As Long
        Dim strMode As String
        Dim boolRollUp As Boolean
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                intDomainID = GetQueryStringVal( "DomainID")
                strMode = GetQueryStringVal( "Mode")
                boolRollUp = GetQueryStringVal( "RollUP")
                If Not IsPostBack Then
                    
                    Dim lobjGeneralLedger As New GeneralLedger
                    lobjGeneralLedger.DomainId = Session("DomainId")
                    lobjGeneralLedger.Year = CInt(Now.Year)
                    calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                    Select Case strMode
                        Case "TB"
                            MultiTB()
                        Case "PL"
                            MultiPL()
                        Case "BS"
                            MultiBS()
                    End Select
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", CDec(Money))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub MultiTB()
            Dim ds As New DataSet
            Dim intCol As Integer
            Try


                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany

                objMultiAccounting.IsRollUp = boolRollUp
                ds = objMultiAccounting.GetMultipleTB(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), Session("SubscriberID"))

                Dim dtTB As New DataTable

                dgReport.Columns.Clear()
                dgReport.DataSource = Nothing


                Dim dbc As BoundColumn


                dtTB.Columns.Add("vcAccountNamePK", Type.GetType("System.String"))
                dtTB.Columns.Add("Account Code", Type.GetType("System.String"))
                dtTB.Columns.Add("Account Type", Type.GetType("System.String"))

                dbc = New BoundColumn
                dbc.DataField = "vcAccountNamePK"
                dbc.HeaderText = "<font color=white>" & "vcAccountNamePK" & "</font>"
                dbc.Visible = False
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Account Code"
                dbc.HeaderText = "<font color=white>" & "Account Code" & "</font>"
                dbc.ItemStyle.Width = 20
                dbc.HeaderStyle.Width = 20
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Account Type"
                dbc.ItemStyle.Width = 100
                dbc.HeaderStyle.Width = 100
                dbc.HeaderText = "<font color=white>" & "Account Type" & "</font>"
                dgReport.Columns.Add(dbc)

                Dim arrBalance() As Decimal

                dtTB.AcceptChanges()
                For intCol = 0 To ds.Tables(0).Rows.Count - 1
                    dtTB.Columns.Add(ds.Tables(0).Rows(intCol).Item("vcDomainName"), Type.GetType("System.String"))
                    dtTB.AcceptChanges()

                    dbc = New BoundColumn
                    dbc.DataField = ds.Tables(0).Rows(intCol).Item("vcDomainName")
                    dbc.HeaderText = "<font color=white>" & ds.Tables(0).Rows(intCol).Item("vcDomainName") & "</font>"
                    dbc.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    dbc.ItemStyle.Width = 20
                    dbc.HeaderStyle.Width = 20
                    dgReport.Columns.Add(dbc)

                Next


                dtTB.PrimaryKey = New DataColumn() {dtTB.Columns("vcAccountNamePK")}

                For intCol = 0 To ds.Tables(1).Rows.Count - 1
                    Dim rw As DataRow = dtTB.NewRow
                    rw.Item(0) = ds.Tables(1).Rows(intCol).Item("vcAccountNamePK")
                    If Len(ds.Tables(1).Rows(intCol).Item("vcAccountCodePK")) = 6 Then
                        rw.Item(1) = "<b>" & ds.Tables(1).Rows(intCol).Item("vcAccountCode") & "</b>"
                        rw.Item(2) = "<b>" & ds.Tables(1).Rows(intCol).Item("vcAccountName") & "</b>"
                    Else
                        rw.Item(1) = ds.Tables(1).Rows(intCol).Item("vcAccountCode")
                        rw.Item(2) = ds.Tables(1).Rows(intCol).Item("vcAccountName")
                    End If
                    dtTB.Rows.Add(rw)
                    dtTB.AcceptChanges()
                Next

                Dim intRow As Integer

                ReDim arrBalance(ds.Tables(0).Rows.Count)

                For intRow = 0 To ds.Tables(2).Rows.Count - 1
                    Dim rw As DataRow = dtTB.Rows.Find(ds.Tables(2).Rows(intRow).Item("vcAccountName"))
                    For intCol = 1 To dtTB.Columns.Count - 1
                        If ds.Tables(2).Rows(intRow).Item("vcDomainName") = dtTB.Columns(intCol).ColumnName Then
                            If Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 4 Then
                                rw(dtTB.Columns(intCol).ColumnName) = " <b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b>"
                            ElseIf Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 6 Then
                                rw(dtTB.Columns(intCol).ColumnName) = "<b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b>"
                            Else
                                rw(dtTB.Columns(intCol).ColumnName) = ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance"))
                            End If

                            arrBalance(intCol - 3) = ReturnMoney(arrBalance(intCol - 3) + ds.Tables(2).Rows(intRow).Item("Balance"))

                            dtTB.AcceptChanges()
                            Exit For
                        End If
                    Next
                Next

                Dim rwFooter As DataRow = dtTB.NewRow

                rwFooter.Item("vcAccountNamePK") = "Total"
                rwFooter.Item("Account Type") = " <b> Totals </b>"

                For intCol = 0 To UBound(arrBalance) - 1
                    rwFooter.Item(intCol + 3) = " <b>" & ReturnMoney(arrBalance(intCol)) & "</b>"
                Next
                dtTB.Rows.Add(rwFooter)

                dgReport.DataSource = dtTB
                dgReport.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub MultiPL()
            Dim ds As New DataSet
            Dim intCol As Integer

            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany

                'intDomainID = CInt(IIf(Mid(e, 1, 1) = "S", "0", e))
                objMultiAccounting.IsRollUp = boolRollUp
                ds = objMultiAccounting.GetMultiplePL(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), Session("SubscriberID"))

                Dim dtTB As New DataTable

                dgReport.Columns.Clear()
                dgReport.DataSource = Nothing


                'Dim grdCol As New Infragistics.Web.UI.GridControls.BoundDataField

                'WebGridTB.Columns.Add(

                Dim dbc As BoundColumn


                dtTB.Columns.Add("vcAccountNamePK", Type.GetType("System.String"))
                dtTB.Columns.Add("Account Code", Type.GetType("System.String"))
                dtTB.Columns.Add("Account Type", Type.GetType("System.String"))

                dbc = New BoundColumn
                dbc.DataField = "vcAccountNamePK"
                dbc.HeaderText = "<font color=white>" & "vcAccountNamePK" & "</font>"
                dbc.Visible = False
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Account Code"
                dbc.HeaderText = "<font color=white>" & "Account Code" & "</font>"
                dbc.ItemStyle.Width = 20
                dbc.HeaderStyle.Width = 20
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Account Type"
                dbc.ItemStyle.Width = 100
                dbc.HeaderStyle.Width = 100
                dbc.HeaderText = "<font color=white>" & "Account Type" & "</font>"
                dgReport.Columns.Add(dbc)

                Dim arrBalance() As Decimal

                dtTB.AcceptChanges()
                For intCol = 0 To ds.Tables(0).Rows.Count - 1
                    dtTB.Columns.Add(ds.Tables(0).Rows(intCol).Item("vcDomainName"), Type.GetType("System.String"))
                    dtTB.AcceptChanges()

                    dbc = New BoundColumn
                    dbc.DataField = ds.Tables(0).Rows(intCol).Item("vcDomainName")
                    dbc.HeaderText = "<font color=white>" & ds.Tables(0).Rows(intCol).Item("vcDomainName") & "</font>"
                    dbc.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    dbc.ItemStyle.Width = 20
                    dbc.HeaderStyle.Width = 20
                    dgReport.Columns.Add(dbc)

                Next


                dtTB.PrimaryKey = New DataColumn() {dtTB.Columns("vcAccountNamePK")}

                For intCol = 0 To ds.Tables(1).Rows.Count - 1
                    Dim rw As DataRow = dtTB.NewRow
                    rw.Item(0) = ds.Tables(1).Rows(intCol).Item("vcAccountNamePK")
                    If Len(ds.Tables(1).Rows(intCol).Item("vcAccountCodePK")) = 6 Then
                        rw.Item(1) = "<b>" & ds.Tables(1).Rows(intCol).Item("vcAccountCode") & "</b>"
                        rw.Item(2) = "<b>" & ds.Tables(1).Rows(intCol).Item("vcAccountName") & "</b>"
                    Else
                        rw.Item(1) = ds.Tables(1).Rows(intCol).Item("vcAccountCode")
                        rw.Item(2) = ds.Tables(1).Rows(intCol).Item("vcAccountName")
                    End If
                    dtTB.Rows.Add(rw)
                    dtTB.AcceptChanges()
                Next

                Dim intRow As Integer

                ReDim arrBalance(ds.Tables(0).Rows.Count)

                For intRow = 0 To ds.Tables(2).Rows.Count - 1
                    Dim rw As DataRow = dtTB.Rows.Find(ds.Tables(2).Rows(intRow).Item("vcAccountName"))
                    For intCol = 1 To dtTB.Columns.Count - 1
                        If ds.Tables(2).Rows(intRow).Item("vcDomainName") = dtTB.Columns(intCol).ColumnName Then
                            If Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 4 Then
                                rw(dtTB.Columns(intCol).ColumnName) = " <b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b>"
                            ElseIf Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 6 Then
                                rw(dtTB.Columns(intCol).ColumnName) = "<b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b>"
                            Else
                                rw(dtTB.Columns(intCol).ColumnName) = ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance"))
                            End If

                            If ds.Tables(2).Rows(intRow).Item("vcAccountCode") = "0103" Or ds.Tables(2).Rows(intRow).Item("vcAccountCode") = "0104" Then
                                arrBalance(intCol - 3) = arrBalance(intCol - 3) + ds.Tables(2).Rows(intRow).Item("Balance")
                            End If

                            dtTB.AcceptChanges()
                            Exit For
                        End If
                    Next
                Next

                Dim rwFooter As DataRow = dtTB.NewRow

                rwFooter.Item("vcAccountNamePK") = "Total"
                rwFooter.Item("Account Type") = " <b> (Profit)/Loss For The Period </b>"

                For intCol = 0 To UBound(arrBalance) - 1
                    rwFooter.Item(intCol + 3) = " <b>" & ReturnMoney(arrBalance(intCol)) & "</b>"
                Next
                dtTB.Rows.Add(rwFooter)

                dgReport.DataSource = dtTB
                dgReport.DataBind()

                '' WebGridTB.Columns(0).Width = 0

                'Response.Write(ds.Tables.Count.ToString)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub MultiBS()
            Dim ds As New DataSet
            Dim intCol As Integer
            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany

                'intDomainID = CInt(IIf(Mid(e, 1, 1) = "S", "0", e))
                objMultiAccounting.IsRollUp = boolRollUp
                ds = objMultiAccounting.GetMultipleBS(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), Session("SubscriberID"))

                Dim dtTB As New DataTable

                dgReport.Columns.Clear()
                dgReport.DataSource = Nothing


                Dim dbc As BoundColumn


                dtTB.Columns.Add("vcAccountNamePK", Type.GetType("System.String"))
                dtTB.Columns.Add("Account Code", Type.GetType("System.String"))
                dtTB.Columns.Add("Account Type", Type.GetType("System.String"))

                dbc = New BoundColumn
                dbc.DataField = "vcAccountNamePK"
                dbc.HeaderText = "<font color=white>" & "vcAccountNamePK" & "</font>"
                dbc.Visible = False
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Account Code"
                dbc.HeaderText = "<font color=white>" & "Account Code" & "</font>"
                dbc.ItemStyle.Width = 20
                dbc.HeaderStyle.Width = 20
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Account Type"
                dbc.ItemStyle.Width = 100
                dbc.HeaderStyle.Width = 100
                dbc.HeaderText = "<font color=white>" & "Account Type" & "</font>"
                dgReport.Columns.Add(dbc)

                Dim arrBalance() As Decimal

                dtTB.AcceptChanges()
                For intCol = 0 To ds.Tables(0).Rows.Count - 1
                    dtTB.Columns.Add(ds.Tables(0).Rows(intCol).Item("vcDomainName"), Type.GetType("System.String"))
                    dtTB.AcceptChanges()

                    dbc = New BoundColumn
                    dbc.DataField = ds.Tables(0).Rows(intCol).Item("vcDomainName")
                    dbc.HeaderText = "<font color=white>" & ds.Tables(0).Rows(intCol).Item("vcDomainName") & "</font>"
                    dbc.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    dbc.ItemStyle.Width = 20
                    dbc.HeaderStyle.Width = 20
                    dgReport.Columns.Add(dbc)

                Next


                dtTB.PrimaryKey = New DataColumn() {dtTB.Columns("vcAccountNamePK")}

                For intCol = 0 To ds.Tables(1).Rows.Count - 1
                    Dim rw As DataRow = dtTB.NewRow
                    rw.Item(0) = ds.Tables(1).Rows(intCol).Item("vcAccountNamePK")
                    If Len(ds.Tables(1).Rows(intCol).Item("vcAccountCodePK")) = 6 Then
                        rw.Item(1) = "<b>" & ds.Tables(1).Rows(intCol).Item("vcAccountCode") & "</b>"
                        rw.Item(2) = "<b>" & ds.Tables(1).Rows(intCol).Item("vcAccountName") & "</b>"
                    Else
                        rw.Item(1) = ds.Tables(1).Rows(intCol).Item("vcAccountCode")
                        rw.Item(2) = ds.Tables(1).Rows(intCol).Item("vcAccountName")
                    End If
                    dtTB.Rows.Add(rw)
                    dtTB.AcceptChanges()
                Next

                Dim intRow As Integer

                ReDim arrBalance(ds.Tables(0).Rows.Count)

                For intRow = 0 To ds.Tables(2).Rows.Count - 1
                    Dim rw As DataRow = dtTB.Rows.Find(ds.Tables(2).Rows(intRow).Item("vcAccountName"))
                    For intCol = 1 To dtTB.Columns.Count - 1
                        If ds.Tables(2).Rows(intRow).Item("vcDomainName") = dtTB.Columns(intCol).ColumnName Then
                            If Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 4 Then
                                rw(dtTB.Columns(intCol).ColumnName) = " <b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b>"
                            ElseIf Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 6 Then
                                rw(dtTB.Columns(intCol).ColumnName) = "<b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b>"
                            Else
                                rw(dtTB.Columns(intCol).ColumnName) = ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance"))
                            End If

                            dtTB.AcceptChanges()
                            Exit For
                        End If
                    Next
                Next

                For intCol = 3 To dtTB.Columns.Count - 1
                    For intRow = 0 To ds.Tables(3).Rows.Count - 1
                        If ds.Tables(3).Rows(intRow).Item("vcDomainName") = dtTB.Columns(intCol).ColumnName Then
                            arrBalance(intCol - 3) = arrBalance(intCol - 3) + CCommon.ToDecimal(ds.Tables(3).Rows(intRow).Item("Balance"))
                        End If
                    Next
                Next

                Dim rwFooter As DataRow = dtTB.NewRow

                rwFooter.Item("vcAccountNamePK") = "Total "
                rwFooter.Item("Account Type") = "<b>Current (Profit)/Loss </b>"

                For intCol = 0 To UBound(arrBalance) - 1
                    rwFooter.Item(intCol + 3) = "<b>" & ReturnMoney(arrBalance(intCol)) & "</b>"
                Next
                dtTB.Rows.Add(rwFooter)

                dgReport.DataSource = dtTB
                dgReport.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                Select Case strMode
                    Case "TB"
                        MultiTB()
                    Case "PL"
                        MultiPL()
                    Case "BS"
                        MultiBS()
                End Select
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace