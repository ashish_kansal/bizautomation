﻿Imports Infragistics.WebUI
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports Telerik.WebControls
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class MultiDashBoard
        Inherits BACRMPage
        Dim objMultiAccounting As New MultiCompany
        Dim intDomainID As Integer
        Dim boolRollUp As Boolean
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            intDomainID = GetQueryStringVal( "DomainID")
            boolRollUp = GetQueryStringVal( "RollUP")
            Try
                If Not IsPostBack Then
                    Dim lobjGeneralLedger As New GeneralLedger
                    lobjGeneralLedger.DomainId = Session("DomainId")
                    lobjGeneralLedger.Year = CInt(Now.Year)
                    calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                    BindCharts()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindCharts()
            Dim dtAR As New DataTable
            Dim dtAP As New DataTable
            Dim dtExp As New DataTable
            Dim dtExpInc As New DataTable
            Dim dtCash As New DataTable
            Dim dtStock As New DataTable
            Dim dtProfitLoss As New DataTable

            'Dim grd As New GridTableItemStyle
            'Dim grc As New GridTableCell

            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                objMultiAccounting.IsRollUp = boolRollUp
                dtAR = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 1, Session("SubscriberID"))
                dtAP = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 2, Session("SubscriberID"))
                dtExp = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 4, Session("SubscriberID"))
                dtExpInc = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 3, Session("SubscriberID"))
                dtCash = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 5, Session("SubscriberID"))
                dtStock = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 6, Session("SubscriberID"))
                dtProfitLoss = objMultiAccounting.GetMultiSummary(intDomainID, CDate(calFrom.SelectedDate), CDate(calTo.SelectedDate), 7, Session("SubscriberID"))

                Dim dr As DataRow
                Dim dblTotal As Double

                If dtAR.Rows.Count <> 0 Then
                    chartAR.DataSource = dtAR
                    chartAR.Legend.Visible = False
                    chartAR.DataBind()

                    dblTotal = 0

                    For Each dr In dtAR.Rows
                        dblTotal = dblTotal + dr("Total")
                    Next

                    radPanAR.FindItemByValue("RadPanelItem1").Text = "A/R Total :" & ReturnMoney(dblTotal)

                End If

                If dtAP.Rows.Count > 0 Then
                    ChartAP.DataSource = dtAP
                    ChartAP.Legend.Visible = False
                    ChartAP.DataBind()

                    dblTotal = 0

                    For Each dr In dtAP.Rows
                        dblTotal = dblTotal + dr("Total")
                    Next

                    radPanAP.FindItemByValue("RadPanelItem1").Text = "A/P Total :" & ReturnMoney(dblTotal)
                End If

                If dtExp.Rows.Count > 0 Then
                    chartExpenses.DataSource = dtExp
                    chartExpenses.Legend.Visible = True
                    chartExpenses.DataBind()

                    dblTotal = 0

                    For Each dr In dtExp.Rows
                        dblTotal = dblTotal + dr("Total")
                    Next

                    radPanExpenses.FindItemByValue("RadPanelItem1").Text = "Total Expenses :" & ReturnMoney(dblTotal)
                End If

                If dtExpInc.Rows.Count > 0 Then
                    chartIncomeExp.DataSource = dtExpInc
                    chartIncomeExp.DataBind()
                    radPanIncomeExp.FindItemByValue("RadPanelItem1").Text = "Income & Expenses"
                End If


                Dim intLoop As Integer
                Dim dtCashTb As New DataTable

                dtCashTb.Columns.Add("Company", Type.GetType("System.String"))
                dtCashTb.Columns.Add("Amount", Type.GetType("System.String"))

                For intLoop = 0 To dtCash.Rows.Count - 1
                    Dim drCash As DataRow = dtCashTb.NewRow
                    drCash(0) = dtCash.Rows(intLoop).Item("vcDomainName")
                    drCash(1) = ReturnMoney(Val(dtCash.Rows(intLoop).Item("Total")))
                    dtCashTb.Rows.Add(drCash)
                Next

                Dim dbc As BoundColumn

                dbc = New BoundColumn
                dbc.DataField = "Company"
                dbc.HeaderText = "<font color=white>" & "Company" & "</font>"
                dgCash.Columns.Add(dbc)
                dgStock.Columns.Add(dbc)
                dgPL.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "Amount"
                dbc.HeaderText = "<font color=white>" & "Balance" & "</font>"
                dbc.ItemStyle.Width = 20
                dbc.DataFormatString = "{0:#,###.00}"
                dbc.HeaderStyle.Width = 20
                dbc.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                dgCash.Columns.Add(dbc)
                dgStock.Columns.Add(dbc)
                dbc.HeaderText = "<font color=white>" & "(Profit)/Loss" & "</font>"
                dgPL.Columns.Add(dbc)



                dgCash.DataSource = dtCashTb
                dgCash.DataBind()

                dgStock.DataSource = dtStock
                dgStock.DataBind()

                dgPL.DataSource = dtProfitLoss
                dgPL.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", CDec(Money))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                BindCharts()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
    End Class
End Namespace