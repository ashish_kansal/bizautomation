﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SubScriberDomain.aspx.vb"
    Inherits="BACRM.UserInterface.MultiComp.SubScriberDomain" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Multi Company Subscription</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script>
        function Close() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <asp:scriptmanager id="ScriptManager1" runat="server">
    </asp:scriptmanager>
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Multi-Company &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:button id="btnSave" runat="server" cssclass="button" text="Save &amp; Close"></asp:button>
                <asp:button id="btnDelete" runat="server"  CssClass="button Delete" Text="X"></asp:button>
                <asp:button id="btnClose" runat="server" cssclass="button" text="Close" width="50"
                    onclientclick="Close();"></asp:button>
            </td>
        </tr>
    </table>
    <table id="Table2" height="10" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="aspTable">
                <asp:table id="tbl" cellpadding="0" cellspacing="0" borderwidth="1" runat="server"
                    width="100%" cssclass="aspTable" bordercolor="black" gridlines="None" height="103px">
                    <asp:tablerow>
                        <asp:tablecell>
                            <br>
                            <table cellspacing="2" cellpadding="0" width="650" border="0">
                                <tr>
                                    <td class="normal1" align="right">
                                        New Domain&nbsp;
                                    </td>
                                    <td>
                                        <asp:dropdownlist cssclass="signup" id="ddlDomain" runat="server" width="200" autopostback="True">
                                            <asp:listitem value="0">--Select One--</asp:listitem>
                                        </asp:dropdownlist>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Parent Domain
                                    </td>
                                    <td colspan="3">
                                        <asp:dropdownlist cssclass="signup" id="ddlDomainParent" runat="server" width="200">
                                            <asp:listitem value="0">--Select One--</asp:listitem>
                                        </asp:dropdownlist>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </asp:tablecell>
                    </asp:tablerow>
                </asp:table>
                <telerik:radtreeview id="UltraDomain" runat="server" allownodeediting="false" clientidmode="Static">
                </telerik:radtreeview>
            </td>
        </tr>
        <tr align="center">
            <td class="normal1">
                &nbsp;
            </td>
        </tr>
        <tr align="center">
            <td class="normal4" valign="middle">
                <asp:literal id="litMessage" runat="server" enableviewstate="False"></asp:literal>
            </td>
        </tr>
    </table>
    <asp:hiddenfield id="HdnumListItemId" runat="server" />
    </form>
</body>
</html>
