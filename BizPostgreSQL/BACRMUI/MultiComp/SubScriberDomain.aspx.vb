﻿Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class SubScriberDomain
        Inherits BACRMPage
        Dim objMultiAccounting As New MultiCompany
        Dim SubscriberName As String
        Dim lngSubscriberID As Long

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngSubscriberID = GetQueryStringVal( "SubID")
                SubscriberName = GetQueryStringVal( "SubName") & "-Subscriber"
                If Not IsPostBack Then
                    
                    FillDomainTree()
                    Call objMultiAccounting.sb_FillComboFromDomain(ddlDomain, 0, 1, 0)
                End If
                btnSave.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub FillDomainTree()
            Try
                Dim parentNode As RadTreeNode
                UltraDomain.Nodes.Clear()
                parentNode = New RadTreeNode
                parentNode.Text = SubscriberName
                parentNode.Value = "S" & lngSubscriberID.ToString
                UltraDomain.Nodes.Add(parentNode)
                parentNode = New RadTreeNode
                parentNode = UltraDomain.Nodes(0)
                LoadTreeView(parentNode)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub ddlDomain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDomain.SelectedIndexChanged
            Try
                Call objMultiAccounting.sb_FillComboFromDomain(ddlDomainParent, ddlDomain.SelectedValue, 2, lngSubscriberID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Function LoadTreeView(ByVal node As RadTreeNode)
            Dim dsDomain As New DataTable
            Dim childNode As RadTreeNode
            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                objMultiAccounting.SubscriberID = lngSubscriberID
                objMultiAccounting.DomainID = 0
                objMultiAccounting.Mode = 3
                dsDomain = objMultiAccounting.GetSubscriberDomain()
                For Each row As DataRow In dsDomain.Rows
                    childNode = New RadTreeNode
                    childNode.Text = row("vcDomainName").ToString
                    childNode.Value = row("numDomainID").ToString
                    childNode.Target = row("numDomainID").ToString
                    node.Nodes.Add(childNode)
                    node.Expanded = True
                    LoadTreeStructureRecursive(row("vcDomainCode") & "0")
                Next


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub LoadTreeStructureRecursive(ByVal domainCode As String)
            Dim dsDomain As New DataTable

            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                objMultiAccounting.SubscriberID = lngSubscriberID
                objMultiAccounting.DomainID = 0
                objMultiAccounting.Mode = 4
                objMultiAccounting.DomainCode = domainCode
                dsDomain = objMultiAccounting.GetSubscriberDomain()
                For Each row As DataRow In dsDomain.Rows
                    If Not UltraDomain.FindNodeByValue(row("numParentDomainID").ToString, True) Is Nothing Then
                        UltraDomain.FindNodeByValue(row("numParentDomainID").ToString, True).Nodes.Add(Me.CreateNode(row))
                    End If
                    If Not UltraDomain.FindNodeByValue(row("ParentDomainName").ToString, True) Is Nothing Then
                        UltraDomain.FindNodeByValue(row("ParentDomainName").ToString, True).Nodes.Add(Me.CreateNode(row))
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Private Function CreateNode(ByVal row As DataRow) As RadTreeNode
            Try

                Dim childNode As RadTreeNode = New RadTreeNode

                childNode.Value = row("numDomainID").ToString
                childNode.Text = row("vcDomainName").ToString
                childNode.Target = row("numDomainID").ToString
                childNode.Expanded = True
                Return (childNode)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Function validateFields()
            Try
                If ddlDomain.SelectedValue = 0 Then
                    litMessage.Text = "Select New Domain"
                    Return False
                    Exit Function
                End If
                If ddlDomainParent.SelectedValue = 0 Then
                    litMessage.Text = "Select Parent Domain"
                    Return False
                    Exit Function
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

        End Sub
        Private Sub Save()
            Try
                Dim objMultiComp As New MultiCompany
                If validateFields() = True Then
                    With objMultiComp
                        .NewDomainId = ddlDomain.SelectedValue
                        .SubscriberID = lngSubscriberID
                        If InStr(ddlDomainParent.SelectedItem.Text, "-Subscriber") > 0 Then
                            .ParentDomainId = 0
                            .ParentType = 1
                        Else
                            .ParentType = 2
                            .ParentDomainId = ddlDomainParent.SelectedValue
                        End If

                        If .InsertMultiCompany = True Then

                            Dim childNode As RadTreeNode = New RadTreeNode

                            childNode.Value = ddlDomain.SelectedValue.ToString
                            childNode.Text = ddlDomain.SelectedItem.Text
                            childNode.Target = ddlDomain.SelectedValue.ToString

                            If .ParentType = 1 Then
                                UltraDomain.Nodes(0).Nodes.Add(childNode)
                                UltraDomain.Nodes(0).Expanded = True
                            Else
                                If Not UltraDomain.FindNodeByValue(Replace(ddlDomainParent.SelectedItem.Text, "-Domain", ""), True) Is Nothing Then
                                    UltraDomain.FindNodeByValue(Replace(ddlDomainParent.SelectedItem.Text, "-Domain", ""), True).Nodes.Add(childNode)
                                    UltraDomain.FindNodeByValue(Replace(ddlDomainParent.SelectedItem.Text, "-Domain", ""), True).Expanded = True
                                End If
                            End If
                            Call objMultiAccounting.sb_FillComboFromDomain(ddlDomain, 0, 1, 0)
                            ddlDomainParent.SelectedIndex = 0
                            litMessage.Text = "New Domain Added Into Multi-Company"
                        End If
                    End With
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub Delete()
            Try
                Dim objMultiComp As New MultiCompany
                If UltraDomain.SelectedNode Is Nothing Then Exit Sub
                If InStr(UltraDomain.SelectedNode.Text, "-Subscriber") > 0 Then Exit Sub
                With objMultiComp
                    .NewDomainId = UltraDomain.SelectedNode.Value
                    .SubscriberID = lngSubscriberID
                    .ParentType = 3
                    .ParentDomainId = UltraDomain.SelectedNode.Value

                    If .InsertMultiCompany = True Then
                        FillDomainTree()
                        Call objMultiAccounting.sb_FillComboFromDomain(ddlDomain, 0, 1, 0)
                        ddlDomainParent.SelectedIndex = 0
                        litMessage.Text = "Seleted Domain Un-Mapped From The Multi-Company"
                    End If
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
            Try
                Delete()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace