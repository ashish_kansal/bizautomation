﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.MultiComp

    Partial Public Class RepMultiGrid

        '''<summary>
        '''radPanAR control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radPanAR As Global.Telerik.Web.UI.RadPanelBar

        '''<summary>
        '''ChartInventory control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ChartInventory As Global.Infragistics.WebUI.UltraWebChart.UltraChart
    End Class
End Namespace
