﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Inventory.aspx.vb" Inherits="BACRM.UserInterface.MultiComp.Inventory" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/MultiComp/InventoryChart.ascx" TagName="RepMultiGrid" TagPrefix="uc1" %>
<%@ Register Src="MultiCompInventory.ascx" TagName="MultiCompInventory" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <asp:panel id="Panel1" runat="server">
        <asp:checkbox id="chkDomain" runat="server" text="Domain" />
        <br />
        <asp:checkbox id="chkMulti" runat="server" text="Multicompany" />
    </asp:panel>
    <table width="100%" align="left" class="aspTable">
        <tr>
            <td class="style2" valign="top">
                <telerik:radtreeview id="ultraTreeDomain" runat="server" allownodeediting="false"
                    clientidmode="Static">
                </telerik:radtreeview>
            </td>
            <td class="style2" valign="top">
                <uc1:repmultigrid id="InventoryChart3" runat="server" />
            </td>
            <td>
                <uc1:repmultigrid id="InventoryChart0" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <uc1:repmultigrid id="InventoryChart1" runat="server" />
            </td>
            <td>
                <uc1:repmultigrid id="InventoryChart2" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <uc2:multicompinventory id="MultiCompInventory1" runat="server" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
