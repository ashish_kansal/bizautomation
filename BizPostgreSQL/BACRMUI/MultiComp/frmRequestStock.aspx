﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRequestStock.aspx.vb"
    Inherits=".frmRequestStock" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stock Request</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Request Stock&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button">
                </asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tblSR" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table width="100%" border="1">
                    <tr>
                        <td class="text" align="right" width="180">
                            Select Branch
                        </td>
                        <td>
                            <telerik:radcombobox accesskey="C" id="radCmbCompany" width="195px" dropdownwidth="200px"
                                skin="Default" runat="server" autopostback="True" allowcustomtext="True" enableloadondemand="True">
                                <webservicesettings path="../common/Common.asmx" method="GetCompanies" />
                            </telerik:radcombobox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Shipping Address
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblShippingAddress" CssClass="text"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="normal1">
                            Sales Order
                        </td>
                        <td align="left" class="normal1">
                            <asp:DropDownList ID="ddlOpp" runat="server" CssClass="signup" Width="300" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Invoice
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlInvoice" runat="server" CssClass="signup" Width="250">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" valign="top" colspan="4">
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
