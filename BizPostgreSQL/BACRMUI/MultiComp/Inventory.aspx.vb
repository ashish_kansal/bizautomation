﻿Option Explicit On
Option Strict Off
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class Inventory
        Inherits BACRMPage
        Dim objMultiAccounting As New MultiCompany

        Protected Sub Inventory_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim parentNode As New RadTreeNode
                If Not IsPostBack Then
                    
                    ultraTreeDomain.Nodes.Clear()
                    parentNode = New RadTreeNode
                    parentNode.Text = "Multi Company"
                    parentNode.Value = ""

                    ultraTreeDomain.Nodes.Add(parentNode)

                    parentNode = New RadTreeNode
                    parentNode.Text = "BizAutomation.com"
                    parentNode.Value = "S103"

                    ultraTreeDomain.Nodes(0).Nodes.Add(parentNode)

                    parentNode = New RadTreeNode
                    parentNode = ultraTreeDomain.Nodes(0).Nodes(0)
                    LoadTreeView(parentNode, 1, 72)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub LoadTreeView(ByVal node As RadTreeNode, ByVal intType As Integer, ByVal domainId As Int16)
            Dim dsDomain As New DataTable
            Dim childNode As RadTreeNode

            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                objMultiAccounting.DomainID = domainId
                objMultiAccounting.SubscriberID = CCommon.ToInteger(Session("SubscriberID"))
                objMultiAccounting.Mode = 1
                objMultiAccounting.DomainCode = ""
                dsDomain = objMultiAccounting.GetDomain()
                childNode = New RadTreeNode


                For Each row As DataRow In dsDomain.Rows
                    childNode = New RadTreeNode
                    childNode.Text = row("vcDomainName").ToString
                    childNode.Value = row("numDomainID").ToString
                    childNode.Target = row("numDomainID").ToString
                    node.Nodes.Add(childNode)
                    node.Expanded = True
                    LoadTreeStructureRecursive(row("vcDomainCode").ToString & "0")
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub LoadTreeStructureRecursive(ByVal domainCode As String)
            Dim dsDomain As New DataTable
            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                objMultiAccounting.DomainID = CCommon.ToInteger(Session("DomainID"))
                objMultiAccounting.SubscriberID = CCommon.ToInteger(Session("SubscriberID"))
                objMultiAccounting.Mode = 2
                objMultiAccounting.DomainCode = domainCode
                dsDomain = objMultiAccounting.GetDomain()
                For Each row As DataRow In dsDomain.Rows
                    If Not ultraTreeDomain.FindNodeByValue(row("numParentDomainID").ToString, True) Is Nothing Then
                        ultraTreeDomain.FindNodeByValue(row("numParentDomainID").ToString, True).Nodes.Add(Me.CreateNode(row))
                    End If
                    If Not ultraTreeDomain.FindNodeByValue(row("ParentDomainName").ToString, True) Is Nothing Then
                        ultraTreeDomain.FindNodeByValue(row("ParentDomainName").ToString, True).Nodes.Add(Me.CreateNode(row))
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Private Function CreateNode(ByVal row As DataRow) As RadTreeNode
            Try

                Dim childNode As RadTreeNode = New RadTreeNode

                childNode.Value = row("numDomainID").ToString
                childNode.Text = row("vcDomainName").ToString
                childNode.Target = row("numDomainID").ToString
                childNode.Expanded = True
                Return (childNode)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ultraTreeDomain_NodeClick(sender As Object, e As RadTreeNodeEventArgs) Handles ultraTreeDomain.NodeClick
            Try
                If e.Node.Level = 0 Then Exit Sub
                If e.Node.Level = 1 Then
                    ShowMultiChart(0, CType(Mid(e.Node.Value.ToString, 2, 10), Int16))
                Else
                    If chkMulti.Checked = True Then ShowMultiChart(CType(e.Node.Value, Int16), CType(Mid(ultraTreeDomain.Nodes(0).Nodes(0).Value.ToString, 2, 10), Int16))
                    If chkDomain.Checked = True Then ShowDomainChart(CType(e.Node.Value, Int16))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ShowDomainChart(ByVal intDomainID As Int16)
            Try
                InventoryChart3.DomainID = intDomainID
                InventoryChart3.TopCount = 5
                InventoryChart3.ReportType = 1
                InventoryChart3.FromDate = CDate("01/Jan/2008")
                InventoryChart3.ToDate = CDate("31/Dec/2008")
                InventoryChart3.ChartCaption = "Top " & InventoryChart3.TopCount.ToString & " Most Sold"
                InventoryChart3.ShowChart()
                InventoryChart3.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart

                InventoryChart0.DomainID = intDomainID
                InventoryChart0.TopCount = 5
                InventoryChart0.ReportType = 2
                InventoryChart0.FromDate = CDate("01/Jan/2008")
                InventoryChart0.ToDate = CDate("31/Dec/2008")
                InventoryChart0.ChartCaption = "Top " & InventoryChart0.TopCount.ToString & " Average Cost"
                InventoryChart0.ShowChart()
                InventoryChart0.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart

                InventoryChart1.DomainID = intDomainID
                InventoryChart1.TopCount = 5
                InventoryChart1.ReportType = 3
                InventoryChart1.FromDate = CDate("01/Jan/2008")
                InventoryChart1.ToDate = CDate("31/Dec/2008")
                InventoryChart1.ChartCaption = "Top " & InventoryChart1.TopCount.ToString & " COGS"
                InventoryChart1.ShowChart()
                InventoryChart1.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart

                InventoryChart2.DomainID = intDomainID
                InventoryChart2.TopCount = 5
                InventoryChart2.ReportType = 4
                InventoryChart2.FromDate = CDate("01/Jan/2008")
                InventoryChart2.ToDate = CDate("31/Dec/2008")
                InventoryChart2.ChartCaption = "Top " & InventoryChart2.TopCount.ToString & " Profitable"
                InventoryChart2.ShowChart()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ShowMultiChart(ByVal intDomainID As Int16, ByVal intSubscriberId As Int16)
            Try
                MultiCompInventory1.DomainID = intDomainID
                MultiCompInventory1.TopCount = 5
                MultiCompInventory1.SubscriberID = intSubscriberId
                MultiCompInventory1.ReportType = 1
                MultiCompInventory1.FromDate = CDate("01/Jan/2008")
                MultiCompInventory1.ToDate = CDate("31/Dec/2008")
                MultiCompInventory1.ChartCaption = "Top " & MultiCompInventory1.TopCount.ToString & " Most Sold"
                MultiCompInventory1.ShowChart()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

     
    End Class
End Namespace