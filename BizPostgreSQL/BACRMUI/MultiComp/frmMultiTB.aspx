﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMultiTB.aspx.vb" Inherits="BACRM.UserInterface.MultiComp.frmMultiTB" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>    
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <br />
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
        Width="100%" CssClass="" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table>
                    <tr>
                        <td width="30px" class="normal1">
                            &nbsp;&nbsp;From
                        </td>
                        <td width="130px" class="normal1">
                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                        </td>
                        <td width="20px" class="normal1">
                            &nbsp;&nbsp;&nbsp;To
                        </td>
                        <td width="130px" class="normal1">
                            <BizCalendar:Calendar ID="calTo" runat="server" />
                        </td>
                        <td class="normal1">
                            <asp:Button runat="server" ID="btnGo" CssClass="button" Text="Go" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:DataGrid ID="dgReport" AutoGenerateColumns="false" runat="server" Width="98%"
                    CssClass="dg">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
