﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTreeNavigation.aspx.vb"
    Inherits=".frmTreeNavigation" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table>
        <tr>
            <td valign="top" align="left">
              <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
                <telerik:radtreeview id="UltraWebTree1" runat="server" allownodeediting="false" clientidmode="Static">
                </telerik:radtreeview>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
