﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI

Partial Public Class frmTreeNavigation
    Inherits BACRMPage
    Dim CurrentNode As Short
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objMultiComp As New MultiCompany
            objMultiComp.DomainID = Session("DomainID")
            If objMultiComp.ValidateMultiComp() Then
                If Not IsPostBack Then
                    LoadTree()
                End If
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType, "Auth", "parent.rightFrame.location.href='../admin/authentication.aspx?mesg=AC'", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub LoadTree()
        Try
            Dim ultraNode As RadTreeNode
            ultraNode = New RadTreeNode
            ultraNode.Text = "OminBiz"
            ultraNode.ImageUrl = "../images/OmniBiz.gif"
            ultraNode.SelectedImageUrl = "../images/OmniBiz.gif"
            ultraNode.Value = 0
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)

            ultraNode = New RadTreeNode
            ultraNode.Text = "Territorial Dashboard"
            ultraNode.Value = 1
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)
            CurrentNode = 1
            LoadTreeView(ultraNode)

            ultraNode = New RadTreeNode
            ultraNode.Text = "Charts of Account"
            ultraNode.Value = 2
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)
            CurrentNode = 2
            LoadTreeView(ultraNode)

            ultraNode = New RadTreeNode
            ultraNode.Text = "General Ledger"
            ultraNode.Value = 3
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)
            CurrentNode = 3
            LoadTreeView(ultraNode)

            ultraNode = New RadTreeNode
            ultraNode.Text = "Balance Sheets"
            ultraNode.Value = 4
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)
            CurrentNode = 4
            LoadTreeView(ultraNode)

            ultraNode = New RadTreeNode
            ultraNode.Text = "Profits and Losses A/C"
            ultraNode.Value = 5
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)
            CurrentNode = 5
            LoadTreeView(ultraNode)

            ultraNode = New RadTreeNode
            ultraNode.Text = "Trial Balance"
            ultraNode.Value = 6
            ultraNode.Font.Bold = True
            UltraWebTree1.Nodes.Add(ultraNode)
            CurrentNode = 6
            LoadTreeView(ultraNode)

            UltraWebTree1.ExpandAllNodes()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim objMultiAccounting As MultiCompany
    Private Function LoadTreeView(ByVal node As RadTreeNode)
        Dim dsDomain As New DataTable
        Dim childNode As RadTreeNode
        Try
            Dim SubscriberNode As New RadTreeNode
            Dim RollUpNode As New RadTreeNode

            If Not (CurrentNode = 2 Or CurrentNode = 3) Then
                RollUpNode.Text = "Roll-Up View"
                RollUpNode.Value = "S" & Session("SubscriberID")
                RollUpNode.Target = "rightFrame"
                SetTargetURL(0, RollUpNode, 1)
                node.Nodes.Add(RollUpNode)
                SubscriberNode = New RadTreeNode
                SubscriberNode.Text = "Seperated View" 'Session("DomainName") & "-Subscriber"
                SubscriberNode.Value = "S" & Session("SubscriberID")
                SubscriberNode.Target = "rightFrame"
                SetTargetURL(0, SubscriberNode)
                RollUpNode.Nodes.Add(SubscriberNode)
            End If

            If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
            objMultiAccounting.SubscriberID = Session("SubscriberID")
            objMultiAccounting.DomainID = 0
            objMultiAccounting.Mode = 3
            dsDomain = objMultiAccounting.GetSubscriberDomain()
            For Each row As DataRow In dsDomain.Rows
                childNode = New RadTreeNode
                childNode.Text = row("vcDomainName").ToString
                childNode.Value = row("numDomainID").ToString & "~" & CurrentNode
                childNode.Target = "rightFrame"
                SetTargetURL(row("numDomainID"), childNode)
                If Not (CurrentNode = 2 Or CurrentNode = 3) Then
                    SubscriberNode.Nodes.Add(childNode)
                    SubscriberNode.Expanded = True
                Else
                    node.Nodes.Add(childNode)
                    node.Expanded = True
                End If
                LoadTreeStructureRecursive(row("vcDomainCode") & "0")
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub LoadTreeStructureRecursive(ByVal domainCode As String)
        Dim dsDomain As New DataTable

        Try
            If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
            objMultiAccounting.SubscriberID = Session("SubscriberID")
            objMultiAccounting.DomainID = 0
            objMultiAccounting.Mode = 4
            objMultiAccounting.DomainCode = domainCode
            dsDomain = objMultiAccounting.GetSubscriberDomain()
            For Each row As DataRow In dsDomain.Rows
                If Not UltraWebTree1.Nodes.FindNodeByValue(row("numParentDomainID").ToString & "~" & CurrentNode) Is Nothing Then
                    UltraWebTree1.Nodes.FindNodeByValue(row("numParentDomainID").ToString & "~" & CurrentNode).Nodes.Add(Me.CreateNode(row))
                End If
                'If Not UltraWebTree1.Find(row("ParentDomainName").ToString & "~" & CurrentNode, True) Is Nothing Then
                '    UltraWebTree1.Find(row("ParentDomainName").ToString & "~" & CurrentNode, True).Nodes.Add(Me.CreateNode(row))
                'End If
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Function CreateNode(ByVal row As DataRow) As RadTreeNode
        Try
            Dim childNode As RadTreeNode = New RadTreeNode

            childNode.Value = row("numDomainID").ToString & "~" & CurrentNode
            childNode.Text = row("vcDomainName").ToString
            childNode.Target = "rightFrame"
            SetTargetURL(row("numDomainID"), childNode)

            childNode.Expanded = True
            Return (childNode)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub SetTargetURL(ByVal intDomainID As Integer, ByVal childNode As RadTreeNode, Optional ByVal IsRollUp As Short = 0)
        Select Case CurrentNode
            Case 1
                childNode.NavigateUrl = "../MultiComp/MultiDashBoard.aspx?DomainID=" & intDomainID & "&RollUp=" & IsRollUp
            Case 2
                childNode.NavigateUrl = "../Accounting/frmChartofAccounts.aspx?DomainID=" & intDomainID
            Case 3
                childNode.NavigateUrl = "../Accounting/frmGeneralLedger.aspx?DomainID=" & intDomainID
            Case 4
                childNode.NavigateUrl = "../MultiComp/frmMultiTB.aspx?DomainID=" & intDomainID & "&Mode=BS" & "&RollUp=" & IsRollUp
            Case 5
                childNode.NavigateUrl = "../MultiComp/frmMultiTB.aspx?DomainID=" & intDomainID & "&Mode=PL" & "&RollUp=" & IsRollUp
            Case 6
                childNode.NavigateUrl = "../MultiComp/frmMultiTB.aspx?DomainID=" & intDomainID & "&Mode=TB" & "&RollUp=" & IsRollUp
            Case Else
        End Select
    End Sub
End Class