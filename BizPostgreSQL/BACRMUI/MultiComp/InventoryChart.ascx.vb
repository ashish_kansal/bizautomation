﻿Option Explicit On
Option Strict Off
Imports Infragistics.WebUI
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.MultiComp

    Partial Public Class RepMultiGrid
        Inherits BACRMUserControl

        Dim modMultiAccounting As New MultiCompany
        Dim _DomainID As Int16
        Dim _ReportType As Int16
        Dim _FromDate As Date
        Dim _ToDate As Date
        Dim _ChartCaption As String
        Dim _TopCount As Int16
        Dim _ChartType As Infragistics.UltraChart.Shared.Styles.ChartType

        Public Property ChartType() As Infragistics.UltraChart.Shared.Styles.ChartType
            Get
                Return _ChartType
            End Get
            Set(ByVal value As Infragistics.UltraChart.Shared.Styles.ChartType)
                _ChartType = value
            End Set
        End Property
        Public Property DomainID() As Int16
            Get
                Return _DomainID
            End Get
            Set(ByVal value As Int16)
                _DomainID = value
            End Set
        End Property
        Public Property TopCount() As Int16
            Get
                Return _TopCount
            End Get
            Set(ByVal value As Int16)
                _TopCount = value
            End Set
        End Property
        Public Property ReportType() As Int16
            Get
                Return _ReportType
            End Get
            Set(ByVal value As Int16)
                _ReportType = value
            End Set
        End Property
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal value As Date)
                _FromDate = value
            End Set
        End Property
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal value As Date)
                _ToDate = value
            End Set
        End Property
        Public Property ChartCaption() As String
            Get
                Return _ChartCaption
            End Get
            Set(ByVal value As String)
                _ChartCaption = value
            End Set
        End Property
        Public Sub ShowChart()
            Dim strMSG As String
            strMSG = vbNullString
            If CType(DomainID, String) = vbNullString Then
                strMSG = "Global Settings Not Available"
                radPanAR.FindItemByValue("RadPanelItem1").Text = strMSG
                Exit Sub
            End If
            If FromDate.ToString = vbNullString Then
                strMSG = "Period From Not Available"
                radPanAR.FindItemByValue("RadPanelItem1").Text = strMSG
                Exit Sub
            End If
            If ToDate.ToString = vbNullString Then
                strMSG = "Period To Not Available"
                radPanAR.FindItemByValue("RadPanelItem1").Text = strMSG
                Exit Sub
            End If
            If CType(ReportType, String) = vbNullString Then
                strMSG = "Report Type Not Available"
                radPanAR.FindItemByValue("RadPanelItem1").Text = strMSG
                Exit Sub
            End If
            If CType(TopCount, String) = vbNullString Then
                strMSG = "Top Count Not Available"
                radPanAR.FindItemByValue("RadPanelItem1").Text = strMSG
                Exit Sub
            End If

            If modMultiAccounting Is Nothing Then modMultiAccounting = New MultiCompany
            Dim dt As New DataTable

            dt = modMultiAccounting.GetInventoryChart(DomainID, ReportType, FromDate, ToDate, TopCount)
            If dt.Rows.Count > 0 Then
                ChartInventory.DataSource = dt
                ChartInventory.DataBind()
                'ChartInventory.ChartType = ChartType
            End If
            radPanAR.FindItemByValue("RadPanelItem1").Text = ChartCaption
            ChartInventory.TitleTop.Text = "Last " & DateDiff(DateInterval.Day, FromDate, ToDate).ToString & " Days," & """" & " Inventory Items - Classification X " & """"

        End Sub
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        End Sub

        'Protected Sub webPanAR_ExpandedStateChanging(ByVal sender As Object, ByVal e As Infragistics.WebUI.Misc.WebPanel.ExpandedStateChangingEventArgs) Handles webPanAR.ExpandedStateChanging

        'End Sub
    End Class

End Namespace