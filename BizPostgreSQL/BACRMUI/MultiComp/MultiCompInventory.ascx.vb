﻿'Imports Infragistics.WebUI
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class MultiCompInventory
        Inherits BACRMUserControl
        Dim modMultiAccounting As New MultiCompany
        Dim _DomainID As Int16
        Dim _SubscriberID As Int16
        Dim _ReportType As Int16
        Dim _FromDate As Date
        Dim _ToDate As Date
        Dim _ChartCaption As String
        Dim _TopCount As Int16
        Dim _ChartType As Infragistics.UltraChart.Shared.Styles.ChartType
        Public Property SubscriberID() As Int16
            Get
                Return _SubscriberID
            End Get
            Set(ByVal value As Int16)
                _SubscriberID = value
            End Set
        End Property
        Public Property ChartType() As Infragistics.UltraChart.Shared.Styles.ChartType
            Get
                Return _ChartType
            End Get
            Set(ByVal value As Infragistics.UltraChart.Shared.Styles.ChartType)
                _ChartType = value
            End Set
        End Property
        Public Property DomainID() As Int16
            Get
                Return _DomainID
            End Get
            Set(ByVal value As Int16)
                _DomainID = value
            End Set
        End Property
        Public Property TopCount() As Int16
            Get
                Return _TopCount
            End Get
            Set(ByVal value As Int16)
                _TopCount = value
            End Set
        End Property
        Public Property ReportType() As Int16
            Get
                Return _ReportType
            End Get
            Set(ByVal value As Int16)
                _ReportType = value
            End Set
        End Property
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal value As Date)
                _FromDate = value
            End Set
        End Property
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal value As Date)
                _ToDate = value
            End Set
        End Property
        Public Property ChartCaption() As String
            Get
                Return _ChartCaption
            End Get
            Set(ByVal value As String)
                _ChartCaption = value
            End Set
        End Property
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub
        'Private Sub MultiTB(ByVal e As String)
        '    Dim dt As DataTable
        '    Dim intCol As Integer

        '    Try

        '        If modMultiAccounting Is Nothing Then modMultiAccounting = New MultiCompany
        '        dt = modMultiAccounting.GetMultiInventory(SubscriberID, DomainID, ReportType, FromDate, ToDate, TopCount)

        '        Dim dtTB As New DataTable

        '        dgReport.Columns.Clear()
        '        dgReport.DataSource = Nothing


        '        'Dim grdCol As New Infragistics.Web.UI.GridControls.BoundDataField

        '        'WebGridTB.Columns.Add(

        '        Dim dbc As BoundColumn


        '        dtTB.Columns.Add("vcAccountNamePK", Type.GetType("System.String"))
        '        dtTB.Columns.Add("Account Code", Type.GetType("System.String"))
        '        dtTB.Columns.Add("Account Type", Type.GetType("System.String"))

        '        dbc = New BoundColumn
        '        dbc.DataField = "vcAccountNamePK"
        '        dbc.HeaderText = "<font color=white>" & "vcAccountNamePK" & "</font>"
        '        dbc.Visible = False
        '        dgReport.Columns.Add(dbc)

        '        dbc = New BoundColumn
        '        dbc.DataField = "Account Code"
        '        dbc.HeaderText = "<font color=white>" & "Account Code" & "</font>"
        '        dbc.ItemStyle.Width = 20
        '        dbc.HeaderStyle.Width = 20
        '        dgReport.Columns.Add(dbc)

        '        dbc = New BoundColumn
        '        dbc.DataField = "Account Type"
        '        dbc.ItemStyle.Width = 100
        '        dbc.HeaderStyle.Width = 100
        '        dbc.HeaderText = "<font color=white>" & "Account Type" & "</font>"
        '        dgReport.Columns.Add(dbc)

        '        Dim arrBalance() As Decimal

        '        dtTB.AcceptChanges()
        '        For intCol = 0 To ds.Tables(0).Rows.Count - 1
        '            dtTB.Columns.Add(ds.Tables(0).Rows(intCol).Item("vcDomainName"), Type.GetType("System.String"))
        '            dtTB.AcceptChanges()

        '            dbc = New BoundColumn
        '            dbc.DataField = ds.Tables(0).Rows(intCol).Item("vcDomainName")
        '            dbc.HeaderText = "<font color=white>" & ds.Tables(0).Rows(intCol).Item("vcDomainName") & "</font>"
        '            dbc.ItemStyle.HorizontalAlign = HorizontalAlign.Right
        '            dbc.ItemStyle.Width = 20
        '            dbc.HeaderStyle.Width = 20
        '            dgReport.Columns.Add(dbc)

        '        Next


        '        dtTB.PrimaryKey = New DataColumn() {dtTB.Columns("vcAccountNamePK")}

        '        For intCol = 0 To ds.Tables(1).Rows.Count - 1
        '            Dim rw As DataRow = dtTB.NewRow
        '            rw.Item(0) = ds.Tables(1).Rows(intCol).Item("vcAccountNamePK")
        '            If Len(ds.Tables(1).Rows(intCol).Item("vcAccountCodePK")) = 6 Then
        '                rw.Item(1) = "<b><i>" & ds.Tables(1).Rows(intCol).Item("vcAccountCode") & "</i></b>"
        '                rw.Item(2) = "<b><i>" & ds.Tables(1).Rows(intCol).Item("vcAccountName") & "</i></b>"
        '            Else
        '                rw.Item(1) = ds.Tables(1).Rows(intCol).Item("vcAccountCode")
        '                rw.Item(2) = ds.Tables(1).Rows(intCol).Item("vcAccountName")
        '            End If
        '            dtTB.Rows.Add(rw)
        '            dtTB.AcceptChanges()
        '        Next

        '        Dim intRow As Integer

        '        ReDim arrBalance(ds.Tables(0).Rows.Count)

        '        For intRow = 0 To ds.Tables(2).Rows.Count - 1
        '            Dim rw As DataRow = dtTB.Rows.Find(ds.Tables(2).Rows(intRow).Item("vcAccountName"))
        '            For intCol = 1 To dtTB.Columns.Count - 1
        '                If ds.Tables(2).Rows(intRow).Item("vcDomainName") = dtTB.Columns(intCol).ColumnName Then
        '                    If Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 4 Then
        '                        rw(dtTB.Columns(intCol).ColumnName) = "<font size=4.5pt> <b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b></font>"
        '                    ElseIf Len(ds.Tables(2).Rows(intRow).Item("vcAccountCode")) = 6 Then
        '                        rw(dtTB.Columns(intCol).ColumnName) = "<i><b>" & ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance")) & "</b></i>"
        '                    Else
        '                        rw(dtTB.Columns(intCol).ColumnName) = ReturnMoney(ds.Tables(2).Rows(intRow).Item("Balance"))
        '                    End If

        '                    arrBalance(intCol - 3) = ReturnMoney(arrBalance(intCol - 3) + ds.Tables(2).Rows(intRow).Item("Balance"))

        '                    dtTB.AcceptChanges()
        '                    Exit For
        '                End If
        '            Next
        '        Next

        '        Dim rwFooter As DataRow = dtTB.NewRow

        '        rwFooter.Item("vcAccountNamePK") = "Total"
        '        rwFooter.Item("Account Type") = "<font size=4.5pt> <b> Totals </b></font>"

        '        For intCol = 0 To UBound(arrBalance) - 1
        '            rwFooter.Item(intCol + 3) = "<font size=4.5pt> <b>" & ReturnMoney(arrBalance(intCol)) & "</b></font>"
        '        Next
        '        dtTB.Rows.Add(rwFooter)

        '        dgReport.DataSource = dtTB
        '        dgReport.DataBind()

        '        ' WebGridTB.Columns(0).Width = 0

        '        Response.Write(ds.Tables.Count.ToString)28m


        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Public Sub ShowChart()
            'Dim obj As Infragistics.WebUI.UltraWebChart.UltraChart
            'Dim obj As Object
            'For Each obj In webPanAR.Controls
            '    'obj = New Object
            '    'obj = New Infragistics.WebUI.UltraWebChart.UltraChart
            '    'webPanAR.Controls.Add(obj)
            '    CType(obj,Web.UI.Control.
            'Next
            Dim objCell As Web.UI.WebControls.TableCell
            Dim objCellHead As Web.UI.WebControls.TableCell
            Dim objRow As Web.UI.WebControls.TableRow
            Dim obj As Infragistics.WebUI.UltraWebChart.UltraChart
            Dim objDt As New DataTable
            Dim objDtChart As New DataTable
            Dim objCompRow As DataRow

            Dim intDomainCount As Integer

            objRow = New Web.UI.WebControls.TableRow
            Table1.Rows.Add(objRow)

            If modMultiAccounting Is Nothing Then modMultiAccounting = New MultiCompany
            objDt = modMultiAccounting.GetMultiCompany(SubscriberID, DomainID)
            Dim objChartText As Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance
            intDomainCount = 1
            For Each objCompRow In objDt.Rows
                objDtChart = New DataTable
                objDtChart = modMultiAccounting.GetMultiInventory(SubscriberID, DomainID, objCompRow.Item("numDomainID"), ReportType, FromDate, ToDate, TopCount).Tables(0)
                'If objDtChart.Rows.Count > 0 Then
                objCellHead = New Web.UI.WebControls.TableCell
                objCellHead.Text = objCompRow.Item("vcDomainName")

                objCellHead.HorizontalAlign = HorizontalAlign.Center
                objCellHead.VerticalAlign = VerticalAlign.Middle

                Table1.Rows(0).Cells.Add(objCellHead)

                objCell = New Web.UI.WebControls.TableCell
                obj = New Infragistics.WebUI.UltraWebChart.UltraChart
                obj.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart
                obj.Axis.X.Labels.ItemFormatString = ""
                obj.Axis.X.Labels.ItemFormat = Infragistics.UltraChart.Shared.Styles.AxisItemLabelFormat.None
                obj.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Custom
                obj.Axis.X.Labels.SeriesLabels.OrientationAngle = 45
                obj.Axis.X.Labels.SeriesLabels.HorizontalAlign = Drawing.StringAlignment.Center
                obj.Axis.X.Labels.SeriesLabels.VerticalAlign = Drawing.StringAlignment.Center
                obj.Border.Color = Drawing.Color.Black
                obj.Border.CornerRadius = 10
                obj.Border.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dash
                obj.ColorModel.AlphaLevel = 150
                obj.ColorModel.ColorBegin = Drawing.Color.Pink
                obj.ColorModel.ColorEnd = Drawing.Color.DarkRed
                obj.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear
                obj.ColorModel.Scaling = Infragistics.UltraChart.Shared.Styles.ColorScaling.Random
                obj.BackColor = Drawing.Color.FromName("#E9EDF4")
                obj.Width = 433
                obj.CrossHairColor = Drawing.Color.Blue
                obj.TitleTop.Text = "Last " & DateDiff(DateInterval.Day, FromDate, ToDate).ToString & " Days," & """" & " Inventory Items - Classification X " & """"
                obj.EmptyChartText = ""
                If objDtChart.Rows.Count > 0 Then
                    obj.DataSource = objDtChart
                    obj.DataBind()
                End If
                Dim objChartRow As DataRow

                For Each objChartRow In objDtChart.Rows
                    objChartText = New Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance
                    objChartText.Visible = True
                    objChartText.HorizontalAlign = Drawing.StringAlignment.Center
                    objChartText.VerticalAlign = Drawing.StringAlignment.Far
                    objChartText.ItemFormatString = "<DATA_VALUE:00.00>"
                    objChartText.Row = -2
                    obj.ColumnChart.ChartText.Add(objChartText)
                Next

                objCell.Controls.Add(obj)
                Table1.Rows(1).Cells.Add(objCell)
                intDomainCount = (++1)
                radPanAR.FindItemByValue("RadPanelItem1").Text = ChartCaption
                'End If
            Next




        End Sub
    End Class
End Namespace