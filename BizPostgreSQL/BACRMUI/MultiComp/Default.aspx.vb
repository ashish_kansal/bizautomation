﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class _Default
        Inherits BACRMPage
        Dim objMultiAccounting As New MultiCompany
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    LoadTreeView()
                    
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub LoadTreeView()
            Dim dsAccounts As New DataTable
            Try
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                objMultiAccounting.DomainID = Session("DomainID")
                objMultiAccounting.SubscriberID = Session("SubscriberID")
                objMultiAccounting.Mode = 1
                objMultiAccounting.DomainCode = ""
                dsAccounts = objMultiAccounting.GetDomain()
                For Each row As DataRow In dsAccounts.Rows
                    Response.Write(row("vcDomainName"))
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub



    End Class
End Namespace
