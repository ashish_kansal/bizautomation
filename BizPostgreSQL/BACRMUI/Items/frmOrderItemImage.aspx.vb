﻿Imports System.IO
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Partial Class frmOrderItemImage
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strFileName As String
    Dim strFileTName As String
    Dim strFilePath As String
    Dim lngItemCode As Long
    Dim lngOppItemCode As Long
    Dim lngOppID As Long
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
            lngOppItemCode = CCommon.ToLong(GetQueryStringVal("OppItemCode"))
            lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
            If Not IsPostBack Then
                
                LoadPic()
                btnClose.Attributes.Add("onclick", "return Close()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadPic()
        Try
            Dim objItems As New CItems
            Dim dtItemDetails As DataTable
            If lngOppItemCode > 0 Then
                lblTitle.Text = "Change Image"
                pnlLargeImage.Visible = False
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppID
                objOpportunity.OppItemCode = lngOppItemCode
                objOpportunity.Mode = 2
                objOpportunity.DomainID = Session("DomainID")
                Dim dt As DataTable = objOpportunity.GetOrderItems().Tables(0)
                If dt.Rows.Count > 0 Then
                    If CCommon.ToString(dt.Rows(0)("vcPathForTImage")) <> "" Then
                        Image1.Visible = True
                        Image1.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dt.Rows(0)("vcPathForTImage")
                        HiddenField2.Value = dt.Rows(0)("vcPathForTImage")
                    Else : Image1.Visible = False
                    End If
                End If
            Else
                objItems.ItemCode = lngItemCode
                dtItemDetails = objItems.ItemDetails
                If dtItemDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForImage")) Then
                        If dtItemDetails.Rows(0).Item("vcPathForImage") <> "" Then
                            Image.Visible = True
                            Image.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtItemDetails.Rows(0).Item("vcPathForImage")
                            HiddenField1.Value = dtItemDetails.Rows(0).Item("vcPathForImage")
                        Else : Image.Visible = False
                        End If
                    Else : Image.Visible = False
                    End If
                    If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForTImage")) Then
                        If dtItemDetails.Rows(0).Item("vcPathForTImage") <> "" Then
                            Image1.Visible = True
                            Image1.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtItemDetails.Rows(0).Item("vcPathForTImage")
                            HiddenField2.Value = dtItemDetails.Rows(0).Item("vcPathForTImage")
                        Else : Image1.Visible = False
                        End If
                    Else : Image1.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UploadImage()
        Try
            If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If
            Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
            Dim ArrFilePath, ArrFileExt
            Dim strFileExt As String
            If lngOppItemCode = 0 Then
                If txtMagFile.PostedFile.FileName <> "" Then
                    strFilePath = FilePath
                    ArrFileExt = Split(txtMagFile.PostedFile.FileName, ".")
                    strFileExt = ArrFileExt(UBound(ArrFileExt))
                    strFileName = "Item" & lngItemCode & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".gif"
                    strFilePath = strFilePath & "\" & strFileName
                    If Not txtMagFile.PostedFile Is Nothing Then txtMagFile.PostedFile.SaveAs(strFilePath)
                    HiddenField1.Value = strFileName
                Else : strFileName = HiddenField1.Value
                End If
            End If


            If txtThumbFile.PostedFile.FileName <> "" Then
                strFilePath = FilePath
                ArrFileExt = Split(txtThumbFile.PostedFile.FileName, ".")
                strFileExt = ArrFileExt(UBound(ArrFileExt))
                strFileTName = "ThumbItem" & lngItemCode & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".gif"
                strFilePath = strFilePath & "\" & strFileTName
                If Not txtThumbFile.PostedFile Is Nothing Then txtThumbFile.PostedFile.SaveAs(strFilePath)
                HiddenField2.Value = strFileTName
            Else : strFileTName = HiddenField2.Value
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UpdateImageDetails()
        Try
            Dim objItem As New CItems
            objItem.ItemCode = lngItemCode
            objItem.PathForImage = strFileName
            objItem.PathForTImage = strFileTName
            objItem.OppItemCode = lngOppItemCode
            objItem.UploadItemImage()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            HiddenField1.Value = ""
            HiddenField2.Value = ""
            UpdateImageDetails()
            LoadPic()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            UploadImage()
            UpdateImageDetails()
            LoadPic()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            UploadImage()
            UpdateImageDetails()
            LoadPic()
            Response.Write("<script>window.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
