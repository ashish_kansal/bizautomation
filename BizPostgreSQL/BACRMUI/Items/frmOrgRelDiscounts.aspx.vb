Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Class frmOrgRelDiscounts
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                
                
                objCommon.sb_FillItemFromDB(ddlItems, Session("DomainId"))
                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID")) ''Relationship
                LoadDetails()
            End If
            If ddlDiscPro.SelectedIndex = 0 Then
                tblOrg.Visible = True
                dgOrgDisc.Visible = True
                tblRel.Visible = False
                dgRelPro.Visible = False
            Else
                tblOrg.Visible = False
                dgOrgDisc.Visible = False
                tblRel.Visible = True
                dgRelPro.Visible = True
            End If
            btnAdd.Attributes.Add("onclick", "return OrgAdd()")
            btnRelAdd.Attributes.Add("onclick", "return RelAdd()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadProfile()
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.RelID = ddlRelationship.SelectedItem.Value
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetRelProfileD
            ddlProfile.DataSource = dtTable
            ddlProfile.DataTextField = "ProName"
            ddlProfile.DataValueField = "numProfileID"
            ddlProfile.DataBind()
            ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            FillCustomer()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillCustomer()
        Try

            

            With objCommon
                .DomainID = Session("DomainID")
                .Filter = Trim(txtCompName.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
                ddlCompany.Items.Insert(0, "--Select One--")
                ddlCompany.Items.FindByText("--Select One--").Value = 0
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objItems As New CItems
            objItems.byteMode = 0
            objItems.ItemCode = ddlItems.SelectedValue
            objItems.DivisionID = ddlCompany.SelectedValue
            objItems.Disc = 0
            objItems.Applicable = True
            objItems.ManageItemDiscProfile()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
        Try
            LoadProfile()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRelAdd.Click
        Try
            Dim objItems As New CItems
            objItems.byteMode = 1
            objItems.ItemCode = ddlItems.SelectedValue
            objItems.RelID = ddlRelationship.SelectedValue
            objItems.ProfileID = ddlProfile.SelectedValue
            objItems.Disc = 0
            objItems.Applicable = True
            objItems.ManageItemDiscProfile()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objItems As New CItems
            Dim dtTable As DataTable
            If ddlDiscPro.SelectedIndex = 0 Then
                objItems.byteMode = 0
                objItems.ItemCode = ddlItems.SelectedValue
                dtTable = objItems.GetItemDiscProfile
                dgOrgDisc.DataSource = dtTable
                dgOrgDisc.DataBind()
            Else
                objItems.byteMode = 1
                objItems.ItemCode = ddlItems.SelectedValue
                dtTable = objItems.GetItemDiscProfile
                dgRelPro.DataSource = dtTable
                dgRelPro.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgOrgDisc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOrgDisc.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblApp As Label
                Dim chk As CheckBox
                lblApp = e.Item.FindControl("lblOrgApplicale")
                chk = e.Item.FindControl("chkOrgApply")
                If lblApp.Text = "True" Then
                    chk.Checked = True
                Else : chk.Checked = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgRelPro_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRelPro.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblApp As Label
                Dim chk As CheckBox
                lblApp = e.Item.FindControl("lblRelApplicale")
                chk = e.Item.FindControl("chkRelDisc")
                If lblApp.Text = "True" Then
                    chk.Checked = True
                Else : chk.Checked = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlDiscPro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDiscPro.SelectedIndexChanged
        Try
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dg As DataGrid
            Dim i As Integer
            Dim objItems As New CItems
            If ddlDiscPro.SelectedIndex = 0 Then
                dg = Page.FindControl("dgOrgDisc")
                For i = 0 To dg.Items.Count - 1
                    Dim chk As CheckBox
                    Dim txt As TextBox
                    chk = dg.Items(i).FindControl("chkOrgApply")
                    txt = dg.Items(i).FindControl("txtOrgDisc")
                    objItems.DiscProfileID = dg.Items(i).Cells(0).Text
                    objItems.Disc = IIf(txt.Text = "", 0, txt.Text)
                    objItems.Applicable = IIf(chk.Checked = True, 1, 0)
                    objItems.byteMode = 0
                    objItems.ManageItemDiscProfile()
                Next
            Else
                dg = Page.FindControl("dgRelPro")
                For i = 0 To dg.Items.Count - 1
                    Dim chk As CheckBox
                    Dim txt As TextBox
                    chk = dg.Items(i).FindControl("chkRelDisc")
                    txt = dg.Items(i).FindControl("txtRelDisc")
                    objItems.DiscProfileID = dg.Items(i).Cells(0).Text
                    objItems.Disc = IIf(txt.Text = "", 0, txt.Text)
                    objItems.Applicable = IIf(chk.Checked = True, 1, 0)
                    objItems.byteMode = 1
                    objItems.ManageItemDiscProfile()
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItems.SelectedIndexChanged
        Try
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgOrgDisc_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOrgDisc.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objItems As New CItems
                objItems.DiscProfileID = e.Item.Cells(0).Text
                objItems.DeleteItemDiscProfile()
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgRelPro_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRelPro.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objItems As New CItems
                objItems.DiscProfileID = e.Item.Cells(0).Text
                objItems.DeleteItemDiscProfile()
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
