﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmInventoryValuation.aspx.vb" Inherits="BACRM.UserInterface.Items.frmInventoryValuation" ValidateRequest="false" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>
                        As of: 
                    </label>
                    <BizCalendar:Calendar ID="calReportDate" runat="server" ClientIDMode="AutoID" />
                    <asp:Button ID="btnGo" runat="server" CssClass="btn btn-primary" Text="GO" OnClick="btnGo_Click" />
                </div>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lkbDownload" runat="server" CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp;Export to Excel</asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="lkbDownload" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Inventory Valuation&nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frminventoryvaluation.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView runat="server" ID="gvReport" CssClass="table table-bordered table-striped" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="numItemCode" HeaderText="Item Code" />
                        <asp:BoundField DataField="vcItemName" HeaderText="Item Name" />
                        <asp:BoundField DataField="vcSKU" HeaderText="SKU" />
                        <asp:BoundField DataField="numBarCodeId" HeaderText="UPC" />
                        <asp:BoundField DataField="numTotalOnHand" HeaderText="Total OnHand (OnHand + OnAllocation)" />
                        <asp:BoundField DataField="monAverageCost" HeaderText="Average Cost" DataFormatString="{0:#,##0.00000}" ItemStyle-HorizontalAlign="Right" />
                        <asp:BoundField DataField="monTotalValuation" HeaderText="Total Valuation" DataFormatString="{0:#,##0.00000}" ItemStyle-HorizontalAlign="Right" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtCurrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
