'Created by anoop jayaraj
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Telerik.Web.UI
Imports System.Linq
Namespace BACRM.UserInterface.Items
    Partial Public Class Items_frmCategories
        Inherits BACRMPage
        Dim objItems As CItems

#Region "Custom Properties"
        Public Property DeletePermission() As Boolean
            Get
                Dim o As Object = ViewState("DeletePermission")
                If o IsNot Nothing Then
                    Return CCommon.ToBool(o)
                Else
                    ViewState("DeletePermission") = False
                    Return CCommon.ToBool(ViewState("DeletePermission"))
                End If
            End Get
            Private Set(ByVal value As Boolean)
                ViewState("DeletePermission") = value
            End Set
        End Property

        Public Property UpdatePermission() As Boolean
            Get
                Dim o As Object = ViewState("UpdatePermission")
                If o IsNot Nothing Then
                    Return CCommon.ToBool(o)
                Else
                    ViewState("UpdatePermission") = False
                    Return CCommon.ToBool(ViewState("UpdatePermission"))
                End If
            End Get
            Private Set(ByVal value As Boolean)
                ViewState("UpdatePermission") = value
            End Set
        End Property
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                GetUserRightsForPage(13, 34)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                End If
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    Me.UpdatePermission = False
                Else
                    Me.UpdatePermission = True
                End If
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnConfigure.Visible = False
                    btnCategoryAdd.Visible = False
                End If
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    Me.DeletePermission = False
                Else
                    Me.DeletePermission = True
                End If

                RadGrid1.MasterTableView.FilterExpression = "numDepCategory1=0"

                PersistTable.Load()

                If Not IsPostBack Then
                    BindCategoryProfile()
                    BindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Public Sub HideExpandColumnRecursive(ByVal tableView As GridTableView)
            Try
                Dim nestedViewItems As GridItem() = tableView.GetItems(GridItemType.NestedView)
                For Each nestedViewItem As GridNestedViewItem In nestedViewItems
                    For Each nestedView As GridTableView In nestedViewItem.NestedTableViews
                        nestedView.Style("border") = "0"

                        Dim MyExpandCollapseButton As Button = DirectCast(nestedView.ParentItem.FindControl("MyExpandCollapseButton"), Button)
                        Dim btnDelete As LinkButton = DirectCast(nestedView.ParentItem.FindControl("btnDelete"), LinkButton)
                        Dim hplCategoryAdd As HyperLink = DirectCast(nestedView.ParentItem.FindControl("hplCategoryAdd"), HyperLink)

                        If Me.UpdatePermission Then
                            hplCategoryAdd.Visible = True
                        Else
                            hplCategoryAdd.Visible = False
                        End If

                        If nestedView.Items.Count = 0 Then
                            If Not MyExpandCollapseButton Is Nothing Then
                                MyExpandCollapseButton.Style("visibility") = "hidden"

                                If Me.DeletePermission Then
                                    btnDelete.Visible = True
                                Else
                                    btnDelete.Visible = False
                                End If
                            End If
                            nestedViewItem.Visible = False
                        Else
                            If Not MyExpandCollapseButton Is Nothing Then
                                MyExpandCollapseButton.Style.Remove("visibility")
                                btnDelete.Visible = False
                            End If
                        End If

                        If nestedView.HasDetailTables Then
                            HideExpandColumnRecursive(nestedView)
                        End If
                    Next
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Protected Sub RadGrid1_ColumnCreated(ByVal sender As Object, ByVal e As GridColumnCreatedEventArgs) Handles RadGrid1.ColumnCreated
            Try
                If TypeOf e.Column Is GridExpandColumn Then
                    e.Column.Visible = False
                ElseIf TypeOf e.Column Is GridBoundColumn Then
                    e.Column.HeaderStyle.Width = Unit.Pixel(100)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Protected Sub RadGrid1_ItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemCreated
            Try
                CreateExpandCollapseButton(e.Item, "tintLevel")

                If TypeOf e.Item Is GridHeaderItem AndAlso Not e.Item.OwnerTableView Is RadGrid1.MasterTableView Then
                    e.Item.Style("display") = "none"
                End If

                If TypeOf e.Item Is GridNestedViewItem Then
                    e.Item.Cells(0).Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try

        End Sub

        Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemDataBound
            Try
                If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
                    Dim hplCategoryAdd As HyperLink = DirectCast(e.Item.FindControl("hplCategoryAdd"), HyperLink)

                    If Not hplCategoryAdd Is Nothing Then
                        hplCategoryAdd.NavigateUrl = "~/Items/frmCategoryAdd.aspx?CategoryID=" & DirectCast(e.Item.DataItem, DataRowView)("numCategoryID") & "&numCategoryProfileID=" & ddlCategoryProfile.SelectedValue
                    End If
                    If CCommon.ToBool(DirectCast(e.Item.DataItem, DataRowView)("bitVisible")) Then
                        DirectCast(e.Item.FindControl("lkbHideCategory"), LinkButton).Visible = True
                        DirectCast(e.Item.FindControl("lkbShowCategory"), LinkButton).Visible = False
                    Else
                        DirectCast(e.Item.FindControl("lkbHideCategory"), LinkButton).Visible = False
                        DirectCast(e.Item.FindControl("lkbShowCategory"), LinkButton).Visible = True
                    End If

                End If
                CreateExpandCollapseButton(e.Item, "tintLevel")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try

        End Sub

        Public Sub CreateExpandCollapseButton(ByVal item As GridItem, ByVal columnUniqueName As String)
            Try
                If TypeOf item Is GridDataItem Then
                    If item.FindControl("MyExpandCollapseButton") Is Nothing Then
                        Dim button As New Button()
                        AddHandler button.Click, AddressOf button_Click_Collapse_Expand
                        button.CommandName = "ExpandCollapse"
                        button.CssClass = IIf((item.Expanded), "rgCollapse", "rgExpand")
                        button.ID = "MyExpandCollapseButton"

                        If item.OwnerTableView.HierarchyLoadMode = GridChildLoadMode.Client Then
                            Dim script As String = [String].Format("$find(""{0}"")._toggleExpand(this, event); return false;", item.Parent.Parent.ClientID)

                            button.OnClientClick = script
                        End If
                        Dim level As Integer = item.ItemIndexHierarchical.Split(":"c).Length - 1
                        'item.Cells(5).Style("padding-left") = (level * 10) & "px"

                        Dim cell As TableCell = DirectCast(item, GridDataItem)(columnUniqueName)

                        cell.Controls.Add(button)
                        cell.Controls.Add(New LiteralControl("&nbsp;"))
                        cell.Style("padding-left") = (level * 10) & "px"
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Sub button_Click_Collapse_Expand(ByVal sender As Object, ByVal e As EventArgs)
            Try
                CType(sender, Button).CssClass = IIf((CType(sender, Button).CssClass = "rgExpand"), "rgCollapse", "rgExpand")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try

        End Sub

        Private Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    If objItems Is Nothing Then objItems = New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.byteMode = 1
                    objItems.CategoryID = CType(e.Item.FindControl("lblCategoryID"), Label).Text.Trim
                    objItems.SeleDelCategory()
                ElseIf e.CommandName = "ShowCategory" Then
                    If objItems Is Nothing Then objItems = New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)
                    objItems.CategoryID = CType(e.Item.FindControl("lblCategoryID"), Label).Text.Trim
                    objItems.ManageSiteCategoriesVisibility(True)
                ElseIf e.CommandName = "HideCategory" Then
                    If objItems Is Nothing Then objItems = New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)
                    objItems.CategoryID = CType(e.Item.FindControl("lblCategoryID"), Label).Text.Trim
                    objItems.ManageSiteCategoriesVisibility(False)
                End If

                Response.Redirect("frmCategories.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub btnConfigure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfigure.Click
            Try
                Response.Redirect("../Items/frmAssignCategories.aspx?I=1&numCategoryProfileID=" & ddlCategoryProfile.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub btnCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategoryAdd.Click
            Try
                Response.Redirect("../Items/frmCategoryAdd.aspx?numCategoryProfileID=" & ddlCategoryProfile.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub radGridCategory_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemDataBound
            Try
                If e.Item.ItemType = TreeListItemType.AlternatingItem Or e.Item.ItemType = TreeListItemType.EditItem Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub RadGrid1_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles RadGrid1.PreRender
            Try
                HideExpandColumnRecursive(RadGrid1.MasterTableView)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try

        End Sub

        Private Sub BindData()
            Try
                Dim objItems As CItems
                If objItems Is Nothing Then objItems = New CItems
                objItems.byteMode = 18
                objItems.DomainID = Session("DomainID")
                objItems.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text)
                objItems.PageSize = Convert.ToInt32(Session("PagingRows"))
                objItems.SiteID = IIf(Not Session("SiteID") Is Nothing, Session("SiteID"), 0)
                objItems.CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)

                Dim dtCategory As DataTable
                dtCategory = objItems.SeleDelCategory
                RadGrid1.DataSource = dtCategory
                RadGrid1.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindCategoryProfile()
            Try
                Dim objCategoryProfile As New CategoryProfile
                objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCategoryProfile.GetAll()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    ddlCategoryProfile.DataSource = dt
                    ddlCategoryProfile.DataTextField = "vcName"
                    ddlCategoryProfile.DataValueField = "ID"
                    ddlCategoryProfile.DataBind()

                    If Not PersistTable(ddlCategoryProfile.ClientID) Is Nothing _
                        AndAlso Not ddlCategoryProfile.Items.FindByValue(PersistTable(ddlCategoryProfile.ClientID)) Is Nothing Then
                        ddlCategoryProfile.Items.FindByValue(PersistTable(ddlCategoryProfile.ClientID)).Selected = True
                    End If

                    GetCategoryProfileDetail()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub GetCategoryProfileDetail()
            Try
                If Not ddlCategoryProfile.SelectedItem Is Nothing Then
                    Dim objCategoryProfile As New CategoryProfile
                    objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCategoryProfile.ID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)
                    objCategoryProfile.GetByIDWithSites()

                    If Not objCategoryProfile.ListCategoryProfileSites Is Nothing AndAlso objCategoryProfile.ListCategoryProfileSites.Count > 0 Then
                        lblCategoryProfileSites.Text = "<b>Sites using this profile:</b> " & String.Join(", ", objCategoryProfile.ListCategoryProfileSites.Select(Function(x) x.SiteName))
                    Else
                        lblCategoryProfileSites.Text = "<b>Sites using this profile:</b> "
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ddlCategoryProfile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategoryProfile.SelectedIndexChanged
            Try
                If PersistTable.ContainsKey(ddlCategoryProfile.ClientID) Then
                    PersistTable(ddlCategoryProfile.ClientID) = ddlCategoryProfile.SelectedValue
                Else
                    PersistTable.Add(ddlCategoryProfile.ClientID, ddlCategoryProfile.SelectedValue)
                End If
                PersistTable.Save()

                GetCategoryProfileDetail()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub ibtnRemoveCategoryProfile_Click(sender As Object, e As EventArgs) Handles ibtnRemoveCategoryProfile.Click
            Try
                Dim objCategoryProfile As New CategoryProfile
                objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                objCategoryProfile.ID = ddlCategoryProfile.SelectedValue
                objCategoryProfile.Delete()

                BindCategoryProfile()
            Catch ex As Exception
                If ex.Message.Contains("ITEMS_EXITS") Then
                    ShowMessage("Remove all items associated with this category profile in order to delete the profile.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex)
                End If
            End Try
        End Sub

        Private Sub btnReload_Click(sender As Object, e As EventArgs) Handles btnReload.Click
            Try
                BindCategoryProfile()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(ex)
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As Exception)
            Try
                TryCast(Me.Master, ECommerceMenuMaster).ThrowError(exception)
            Catch ex As Exception

            End Try
        End Sub

        Private Sub RadGrid1_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
            Try
                Dim objItems As CItems
                If objItems Is Nothing Then objItems = New CItems
                objItems.byteMode = 18
                objItems.DomainID = Session("DomainID")
                objItems.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text)
                objItems.PageSize = Convert.ToInt32(Session("PagingRows"))
                objItems.SiteID = IIf(Not Session("SiteID") Is Nothing, Session("SiteID"), 0)
                objItems.CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)

                Dim dtCategory As DataTable
                dtCategory = objItems.SeleDelCategory

                RadGrid1.DataSource = dtCategory
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub
    End Class
End Namespace