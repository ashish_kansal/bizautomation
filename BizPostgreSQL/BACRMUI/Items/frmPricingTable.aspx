﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPricingTable.aspx.vb"
    Inherits=".frmPricingTable" MasterPageFile="~/common/PopupBootstrap.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Pricing Table</title>
    <script type="text/javascript">
        function Save() {

            var chkValidationDisable = document.getElementById('chkValidationDisable');
            if (chkValidationDisable != null && chkValidationDisable.checked == true) {
                return true;
            }

            var idRoot = 'ctl00_Content_dgPriceTable';
            var From = 0;
            var To = 0;
            var From1 = 0;
            var To1 = 0;
            var idtxt = '';
            var idtxt1 = '';
            for (var i = 2; i < document.getElementById(idRoot).rows.length + 1; i++) {
                if (i < 10) {
                    idtxt = '_ctl0'
                }
                else {
                    idtxt = '_ctl'
                }
                From = parseInt(document.getElementById(idRoot + idtxt + i + '_' + 'txtQtyFrom').value);
                To = parseInt(document.getElementById(idRoot + idtxt + i + '_' + 'txtQtyTo').value);
                if (From != "" && To != "") {
                    if (From >= To) {
                        alert('Qty To must be greater than Qty From (' + From + '-' + To + ')')
                        return false;
                    }
                    for (var j = 2; j < document.getElementById(idRoot).rows.length + 1; j++) {
                        if (j < 10) {
                            idtxt1 = '_ctl0'
                        }
                        else {
                            idtxt1 = '_ctl'
                        }
                        if (j != i) {
                            From1 = parseInt(document.getElementById(idRoot + idtxt1 + j + '_' + 'txtQtyFrom').value);
                            To1 = parseInt(document.getElementById(idRoot + idtxt1 + j + '_' + 'txtQtyTo').value);

                            if (From1 != "" && To1 != "") {

                                if (From1 >= From && From1 <= To) {
                                    // alert(From1 + '>=' + From + ' && ' + From1 + '<=' + To);
                                    alert('Qty value (' + From1 + '-' + To1 + ') must be unique');
                                    return false;
                                }
                                if (To1 >= From && To1 <= To) {
                                    // alert(From1 + '>=' + From + ' && ' + From1 + '<=' + To);
                                    alert('Qty value (' + From1 + '-' + To1 + ') must be unique');
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        function keyPressed(TB, e) {
            var tblGrid = document.getElementById("ctl00_Content_dgPriceTable");

            var rowcount = tblGrid.rows.length;
            var TBID = document.getElementById(TB);

            var key;
            if (window.event) { e = window.event; }
            key = e.keyCode;
            if (key == 37 || key == 38 || key == 39 || key == 40) {
                for (Index = 1; Index < rowcount; Index++) {

                    for (childIndex = 0; childIndex <
              tblGrid.rows[Index].cells.length; childIndex++) {

                        if (tblGrid.rows[Index].cells[childIndex].children[0] != null) {
                            if (tblGrid.rows[Index].cells[
                 childIndex].children[0].id == TBID.id) {

                                if (key == 40) {
                                    if (Index + 1 < rowcount) {
                                        if (tblGrid.rows[Index + 1].cells[
                     childIndex].children[0] != null) {
                                            if (tblGrid.rows[Index + 1].cells[
                       childIndex].children[0].type == 'text') {

                                                //downvalue

                                                tblGrid.rows[Index + 1].cells[
                             childIndex].children[0].focus();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                if (key == 38) {
                                    if (tblGrid.rows[Index - 1].cells[
                     childIndex].children[0] != null) {
                                        if (tblGrid.rows[Index - 1].cells[
                       childIndex].children[0].type == 'text') {
                                            //upvalue

                                            tblGrid.rows[Index - 1].cells[
                             childIndex].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 37 && (childIndex != 0)) {

                                    if ((tblGrid.rows[Index].cells[
                      childIndex - 1].children[0]) != null) {

                                        if (tblGrid.rows[Index].cells[
                       childIndex - 1].children[0].type == 'text') {
                                            //left

                                            if (tblGrid.rows[Index].cells[
                         childIndex - 1].children[0].value != '') {
                                                var cPos =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex - 1].children[0], 'left');
                                                if (cPos) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex - 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex - 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 39) {
                                    if (tblGrid.rows[Index].cells[childIndex + 1].children[0] != null) {
                                        if (tblGrid.rows[Index].cells[
                       childIndex + 1].children[0].type == 'text') {
                                            //right

                                            if (tblGrid.rows[Index].cells[
                         childIndex + 1].children[0].value != '') {
                                                var cPosR =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex + 1].children[0], 'right');
                                                if (cPosR) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex + 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex + 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function getCaretPos(control, way) {
            var movement;
            if (way == 'left') {
                movement = -1;
            }
            else {
                movement = 1;
            }
            if (control.createTextRange) {
                control.caretPos = document.selection.createRange().duplicate();
                if (control.caretPos.move("character", movement) != '') {
                    return false;
                }
                else {
                    return true;
                }
            }
        }

        $(document).ready(function () {
            CheckRuleType();

            function CheckRuleType() {
                if ($("#ddlRuleType").val() == "3") {
                    $("#ddlDiscountType").hide();
                    //$("#chkFromToAmount").parent().hide();
                }
                else {
                    $("#ddlDiscountType").show();
                    //$("#chkFromToAmount").parent().show();
                }
            }

            $("#ddlRuleType").change(function () {
                CheckRuleType();
            });


            //$("#chkValidationDisable").change(function () {
            //    if ($(this).is(":checked")) {
            //        $("#chkFromToAmount").parent().hide();
            //    } else {
            //        $("#chkFromToAmount").parent().show();
            //    }
            //});

            //$("#ddlDiscountType").change(function () {
            //    if ($(this).val() == "3") {
            //        $("#chkFromToAmount").parent().hide();
            //    } else {
            //        $("#chkFromToAmount").parent().show();
            //    }
            //});
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save & Close" OnClientClick="return Save();"></asp:Button>
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" OnClientClick="return Close();"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Pricing Table
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-inline">
                    <li>
                        <asp:DropDownList ID="ddlRuleType" runat="server" CssClass="form-control" AutoPostBack="true">
                            <asp:ListItem Text="Deduct from List price" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Add to Primary Vendor Cost" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li>
                        <asp:DropDownList runat="server" ID="ddlVendorCostType" Visible="false">
                            <asp:ListItem Text="Base on dynamic cost values" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Base on static cost values" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li>
                        <asp:DropDownList ID="ddlDiscountType" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Percentage" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Flat Amount" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Named Price" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li>
                        <asp:CheckBox ID="chkValidationDisable" runat="server" Text="Disable Qty From/To Range" CssClass="signup" Font-Bold="true" />
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <asp:DropDownList runat="server" ID="ddlCurrency" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row padbottom10" id="divLabel" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Rule Type:</label>
                    <div>
                        <asp:Label ID="lblRuleType" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Discount Type:</label>
                    <div>
                        <asp:Label ID="lblDiscountType" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <!-- IMPORTANT - COLUMN INDEX IS USED IN CODE SO TAKE CARE OF THAT WHILE ADDING COLUMNS OR CHANGING ORDER OF COLUMN -->
                    <asp:DataGrid ID="dgPriceTable" runat="server" UseAccessibleHeader="true" CssClass="table table-bordered table-striped" Width="100%" AutoGenerateColumns="False" ClientIDMode="AutoID">
                        <Columns>
                            <asp:BoundColumn DataField="numPricingID" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Price Levels" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblPriceLevelName" runat="server" Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Name" ItemStyle-Wrap="false" ItemStyle-Width="120">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("vcName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Base UOM Qty From">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQtyFrom" TextMode="SingleLine" runat="server" CssClass="form-control"
                                        Text='<%# Eval("intFromQty") %>' onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                                    <asp:Label ID="lblQtyFrom" runat="server" Text='<%# Eval("intFromQty") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Base UOM Qty To">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQtyTo" TextMode="SingleLine" runat="server" CssClass="form-control"
                                        Text='<%# Eval("intToQty") %>' onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                                    <asp:Label ID="lblQtyTo" runat="server" Text='<%# Eval("intToQty") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="+, -, Named Price">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDiscAmount" TextMode="SingleLine" Width="90%" runat="server" Text='<%# Eval("decDiscount") %>'
                                        CssClass="form-control" onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                                    <asp:Label ID="lblDiscAmount" runat="server" Text='<%# Eval("decDiscount") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
