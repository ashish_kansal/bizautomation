﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddEditInventory.aspx.vb"
    Inherits=".frmAddEditInventory" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function SaveWareHouse() {
            if (document.getElementById("ddlWareHouse").value == 0) {
                alert("Select WareHouse");
                document.getElementById("ddlWareHouse").focus();
                return false;
            }
            if (Number(document.getElementById("hfAverageCost").value) == 0) {

                return confirm('Average Cost of Item is 0.00, are you sure you want to add stock?');
            }
            return true;
        }

        function SaveSerialNo() {
            if (document.getElementById("ddlWareHouseItem").value == 0) {
                alert("Select WareHouse Item");
                document.getElementById("ddlWareHouseItem").focus();
                return false;
            }
            if (Number(document.getElementById("hfAverageCost").value) == 0) {

                return confirm('Average Cost of Item is Zero(0.00), are you sure you want to add/update stock?');
            }
            return true;
        }

        function ChangedUPCSKU(TB, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;

            if (e.ctrlKey) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }

            if (k == 13) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;

                return false;
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Manage Inventory
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:Table ID="Table2" runat="server" BorderColor="Black" Width="100%" BorderWidth="1px"
        CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" Width="50%">
                <fieldset style="padding-top: 5px">
                    <legend align="left" class="normal1">Warehouse</legend>
                    <table width="100%" class="normal1">
                        <tr>
                            <td align="right">WareHouse :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="signup" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hfWareHouseItemID" runat="server" />
                            </td>
                        </tr>
                        <tr id="trOnHand" runat="server">
                            <td align="right">OnHand :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtOnHand" CssClass="signup" runat="server" Width="100">
                                </asp:TextBox>
                                <asp:Label Text="" ID="lblOnHand" runat="server" />
                            </td>
                        </tr>
                        <tr id="trReOrder" runat="server">
                            <td align="right">ReOrder :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtReOrder" CssClass="signup" runat="server" Width="100">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Default Location :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlWarehouseLocation" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                <%--<asp:TextBox ID="txtLocation" CssClass="signup" runat="server" Width="100">
                                </asp:TextBox>--%>
                            </td>
                        </tr>
                        <tr id="trSKU" runat="server">
                            <td align="right">SKU (M) :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtSKU" CssClass="signup" runat="server" Width="100" onkeypress="javascript:ChangedUPCSKU(this,event)">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trUPC" runat="server">
                            <td align="right">UPC (M) :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUPC" CssClass="signup" runat="server" Width="100" onkeypress="javascript:ChangedUPCSKU(this,event)">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trListPrice" runat="server">
                            <td align="right">List Price (I) :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtListPrice" CssClass="signup" runat="server" Width="100">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <table id="tblWareHouseAttributes" width="100%" runat="server">
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">&nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Width="50" Text="Save"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top" Width="50%">
                <fieldset style="padding-top: 5px">
                    <legend align="left" class="normal1">Serial #/ Lot #</legend>
                    <table width="100%" class="normal1" id="tblSerialLot" runat="server" visible="false">
                        <tr>
                            <td align="right">WareHouse Item :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlWareHouseItem" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hfnumWareHouseItmsDTLID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSerialNoCaption" runat="Server"></asp:Label>
                                :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtSerialNo" CssClass="signup" runat="server" Width="100">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trLotQty" runat="server" visible="false">
                            <td align="right">Lot Qty :
                            </td>
                            <td align="left">
                                <asp:Label ID="lblLotQty" runat="server" Text="" Visible="false"></asp:Label>
                                <asp:TextBox ID="txtLotQty" CssClass="signup" runat="server" Width="100">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Comments :
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtComments" CssClass="signup" runat="server" Width="200" Height="70"
                                    TextMode="MultiLine" Wrap="true">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <table id="tblSerialNoAttributes" width="100%" runat="server">
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">&nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSaveSerialNo" runat="server" CssClass="button" Width="50" Text="Save"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="normal4" HorizontalAlign="Center" VerticalAlign="Top" ColumnSpan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" ColumnSpan="2">
                <telerik:RadGrid ID="gvInventory" runat="server" Width="100%" AutoGenerateColumns="false"
                    GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <MasterTableView DataKeyNames="numWareHouseItemID,OnHand,IsDeleteKitWarehouse,bitKitParent,bitChildItemWarehouse" HierarchyLoadMode="Client" DataMember="WareHouse">
                        <DetailTables>
                            <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                DataKeyNames="numWareHouseItmsDTLID,numWareHouseItemID,numQty" DataMember="SerializedItems"
                                CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs"
                                AutoGenerateColumns="false">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="numWareHouseItemID" MasterKeyField="numWareHouseItemID" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridButtonColumn CommandName="Edit_SerialLot" Text="Edit">
                                    </telerik:GridButtonColumn>
                                    <telerik:GridButtonColumn CommandName="Delete_SerialLot" Text="Delete">
                                    </telerik:GridButtonColumn>
                                </Columns>
                                <ItemStyle HorizontalAlign="Center" CssClass="is"></ItemStyle>
                                <AlternatingItemStyle HorizontalAlign="Center" CssClass="ais"></AlternatingItemStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="hs"></HeaderStyle>
                            </telerik:GridTableView>
                        </DetailTables>
                        <Columns>
                            <telerik:GridButtonColumn CommandName="Edit_WareHouse" Text="Edit">
                            </telerik:GridButtonColumn>
                            <telerik:GridButtonColumn CommandName="Delete_WareHouse" Text="Delete">
                            </telerik:GridButtonColumn>
                            <%--<telerik:GridBoundColumn Visible="false" DataField="IsDeleteKitWarehouse" ></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn Visible="false" DataField="bitKitParent"></telerik:GridBoundColumn>--%>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <FilterMenu EnableImageSprites="False" EnableEmbeddedSkins="False">
                    </FilterMenu>
                    <HeaderContextMenu EnableEmbeddedSkins="False" CssClass="GridContextMenu GridContextMenu_windows">
                    </HeaderContextMenu>
                </telerik:RadGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hfAverageCost" runat="server" Value="0.0" />
    <asp:HiddenField ID="hfItemName" runat="server" Value="" />
    <asp:HiddenField ID="hfAssetChartAcntID" runat="server" Value="" />

</asp:Content>
