Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart

Partial Public Class frmPaymentGateway
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            GetUserRightsForPage(13, 39)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If
            If Not IsPostBack Then
                ddlPaymentGateway.DataSource = objCommon.PaymentGateways
                ddlPaymentGateway.DataTextField = "vcGateWayName"
                ddlPaymentGateway.DataValueField = "intPaymentGateWay"
                ddlPaymentGateway.DataBind()

                BindSites()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub BindGrid()
        Dim objItems As New CItems
        objItems.DomainID = Session("DomainID")
        objItems.numSiteId = ddlSites.SelectedValue
        Dim ds As DataSet = objItems.GateWayDtls

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If Not ddlPaymentGateway.Items.FindByValue(ds.Tables(0).Rows(0)("intPaymentGateWay")) Is Nothing Then
                ddlPaymentGateway.ClearSelection()
                ddlPaymentGateway.Items.FindByValue(ds.Tables(0).Rows(0)("intPaymentGateWay")).Selected = True
            End If
        End If
        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 Then
            dgPaymentGateway.DataSource = ds.Tables(1)
            dgPaymentGateway.DataBind()
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objItems As New CItems
            Dim dtTable As New DataTable
            objItems.DomainID = Session("DomainID")
            dtTable.Columns.Add("intPaymentGateWay")
            dtTable.Columns.Add("vcFirstFldValue")
            dtTable.Columns.Add("bitTest")
            dtTable.Columns.Add("vcSecndFldValue")
            dtTable.Columns.Add("vcThirdFldValue")
            Dim dr As DataRow
            For Each dgGridItem As DataGridItem In dgPaymentGateway.Items
                dr = dtTable.NewRow
                dr("intPaymentGateWay") = dgGridItem.Cells(0).Text
                dr("vcFirstFldValue") = CType(dgGridItem.FindControl("txtFirstFldValue"), TextBox).Text
                dr("vcSecndFldValue") = CType(dgGridItem.FindControl("txtSecondFldValue"), TextBox).Text
                dr("vcThirdFldValue") = CType(dgGridItem.FindControl("txtThirdFldValue"), TextBox).Text
                dr("bitTest") = CType(dgGridItem.FindControl("chkTest"), CheckBox).Checked
                dtTable.Rows.Add(dr)
            Next
            Dim dsNew As New DataSet
            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)
            objItems.str = dsNew.GetXml
            objItems.numSiteId = ddlSites.SelectedValue
            objItems.PaymentGateWayID = CCommon.ToLong(ddlPaymentGateway.SelectedValue)
            dsNew.Tables.Remove(dsNew.Tables(0))
            objItems.ManageGateWayDtls()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "Biz Internal Payment")
            ddlSites.Items.FindByText("Biz Internal Payment").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgPaymentGateway_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaymentGateway.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim lbl As Label
            lbl = e.Item.FindControl("lblSecndFldName")
            If lbl.Text = "" Then
                lbl.Visible = False
                CType(e.Item.FindControl("txtSecondFldValue"), TextBox).Visible = False
            End If
            lbl = e.Item.FindControl("lblThirdFldName")
            If lbl.Text = "" Then
                lbl.Visible = False
                CType(e.Item.FindControl("txtThirdFldValue"), TextBox).Visible = False
            End If
        End If
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlSites_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSites.SelectedIndexChanged
        BindGrid()
    End Sub
End Class