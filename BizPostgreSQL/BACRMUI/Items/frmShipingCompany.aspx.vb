Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.ShioppingCart
Partial Public Class frmShipingCompany
    Inherits BACRMPage
    Dim ObjCommon As CCommon
    Dim objItems As CItems

    Dim objShippingRule As ShippingRule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            
            GetUserRightsForPage(13, 38)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If Not IsPostBack Then
                BindDataGrid()
                BindServiceTypes()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try

    End Sub

    Sub BindDataGrid()
        Try
            ObjCommon = New CCommon
            Dim dtTable As DataTable
            dtTable = ObjCommon.GetMasterListItems(82, Session("DomainId"))
            dgShipcmp.DataSource = dtTable
            dgShipcmp.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgShipcmp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShipcmp.ItemDataBound
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")

            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                objItem.ShippingCMPID = e.Item.Cells(0).Text
                Dim dtFields As DataTable = objItem.ShippingDtls.Tables(0)

                If dtFields.Rows.Count = 0 Then
                    e.Item.FindControl("hplEditData").Visible = False
                Else
                    e.Item.FindControl("hplEditData").Visible = True
                End If
                Dim hplEdit As HyperLink = CType(e.Item.FindControl("hplEditData"), HyperLink)
                hplEdit.Attributes.Add("onclick", "return OpenComDTL(" & e.Item.Cells(0).Text & ",'" & CType(e.Item.FindControl("hlData"), HyperLink).Text & "')")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As Exception)
        Try
            TryCast(Me.Master, ECommerceMenuMaster).ThrowError(exception)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindServiceTypes()
        Try
            Dim dtShippingMethod As New DataTable

            Dim objShippingRule As New ShippingRule
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.RuleID = 0
            objShippingRule.ServiceTypeID = 0
            objShippingRule.byteMode = 2

            dtShippingMethod = objShippingRule.GetShippingServiceType()

            Dim datarows As DataRow() = dtShippingMethod.Select("numServiceTypeID not in('101','102','103')", "") '.Select("numServiceTypeID <> 102") '.Select("numServiceTypeID <> 103")
            If datarows IsNot Nothing Then
                If datarows.Length > 0 Then
                    dtShippingMethod = datarows.CopyToDataTable()
                End If
            Else
                dtShippingMethod = New DataTable()
            End If

            dgRealTimeShippingQuotes.DataSource = dtShippingMethod
            dgRealTimeShippingQuotes.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub dgRealTimeShippingQuotes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRealTimeShippingQuotes.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblNSoftEnm As Label = e.Item.FindControl("lblNSoftEnm")
                If CCommon.ToInteger(lblNSoftEnm.Text) > 0 Then
                    CType(e.Item.FindControl("txtShippingCaption"), TextBox).Visible = False
                Else
                    CType(e.Item.FindControl("txtRate"), TextBox).Visible = True
                    CType(e.Item.FindControl("chkIsPercentage"), CheckBox).Visible = False
                    e.Item.FindControl("lblIsPercentage").Visible = False
                    CType(e.Item.FindControl("txtMarkup"), TextBox).Visible = False
                    CType(e.Item.FindControl("lblServiceName"), Label).Visible = False
                    CType(e.Item.FindControl("btnDelete"), Button).Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Try
            If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.byteMode = 2 'Update

            objShippingRule.ServiceTypeID = 2
            objShippingRule.Str = GetItemsForRealShippingQuotes()
            objShippingRule.ManageShippingServiceTypes()
            BindServiceTypes()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Function GetItemsForRealShippingQuotes() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dr As DataRow
            CCommon.AddColumnsToDataTable(dt, "numServiceTypeID,vcServiceName,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled")

            For Each gvr As DataGridItem In dgRealTimeShippingQuotes.Items
                dr = dt.NewRow()
                dr("numServiceTypeID") = gvr.Cells(0).Text.Trim
                dr("vcServiceName") = CType(gvr.FindControl("txtShippingCaption"), TextBox).Text
                dr("intFrom") = CType(gvr.FindControl("txtFrom"), TextBox).Text
                dr("intTo") = CType(gvr.FindControl("txtTo"), TextBox).Text
                dr("fltMarkup") = CType(gvr.FindControl("txtMarkup"), TextBox).Text
                dr("bitMarkupType") = CType(gvr.FindControl("chkIsPercentage"), CheckBox).Checked
                dr("monRate") = CType(gvr.FindControl("txtRate"), TextBox).Text
                dr("bitEnabled") = CType(gvr.FindControl("chk"), CheckBox).Checked
                dt.Rows.Add(dr)
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class