﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPriceBookSteps.aspx.vb"
    Inherits=".frmPriceBookSteps" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Table ID="Table3" Width="900px" runat="server" BorderWidth="1" Height="350" GridLines="None"
        CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:Panel runat="server" ID="pnlItems" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Item
                                <%--<rad:RadComboBox ID="radItem" ExternalCallBackPage="../Items/frmLoadItems.aspx" Width="150"
                                    DropDownWidth="150" Skin="WindowsXP" runat="server" EnableLoadOnDemand="True">
                                </rad:RadComboBox>--%>
                                <telerik:RadComboBox ID="radItem" runat="server" Width="150" DropDownWidth="200px"
                                    EnableLoadOnDemand="true">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                </telerik:RadComboBox>
                                &nbsp;
                                <asp:Button ID="btnAddItem" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnExportItem" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                <asp:Button ID="btnRemItem" CssClass="btn btn-primary" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnClose" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItems" AllowSorting="True" runat="server" Width="100%" CssClass="dg table table-responsive"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numPriceBookItemID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcItemName" HeaderText="Item Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="txtItemDesc" HeaderText="Description"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ItemClassification" HeaderText="Item Classification"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkItemsSelectAll" runat="server" onclick="SelectAll('chkItemsSelectAll','dgItemschk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgItemschk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlItemClassification" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Item Classification
                                <asp:DropDownList ID="ddlItemClassification" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddItemClass" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnExportItemClassification" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                <asp:Button ID="btnRemoveItemClass" CssClass="btn btn-primary" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnCloseIC" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItemClass" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numPriceBookItemID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ItemClassification" HeaderText="Item Classification"></asp:BoundColumn>
                                        <asp:BoundColumn DataFormatString="{0:##,#00}" DataField="ItemsCount" HeaderText="Items belonging to this classification"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkItemClassSelectAll" runat="server" onclick="SelectAll('chkItemClassSelectAll','dgItemClasschk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgItemClasschk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlOrganizations" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Organization
                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    ClientIDMode="Static"
                                    ShowMoreResultsBox="true"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                                <%--<rad:radcombobox id="radCmbCompany" externalcallbackpage="../include/LoadCompany.aspx"
                                    width="195px" dropdownwidth="200px" skin="WindowsXP" runat="server" allowcustomtext="True"
                                    enableloadondemand="True">
                                </rad:radcombobox>--%>
                                &nbsp;
                                <asp:Button ID="btnAddOrg" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="center">
                                <asp:Button ID="btnExportOrg" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                <asp:Button ID="btnRemoveOrg" CssClass="btn btn-primary" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnCloseOrg" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgOrg" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numPriceBookRuleDTLID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcCompanyName" HeaderText="Organization Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="PrimaryContact" HeaderText="Primary Contact (First / Last Name & Email)"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Relationship" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkOrgSelectAll" runat="server" onclick="SelectAll('chkOrgSelectAll','dgOrgchk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgOrgchk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlRelProfiles" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Relationship & Profile
                                <asp:DropDownList ID="ddlRelationship" runat="server" Width="200" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:DropDownList ID="ddlProfile" runat="server" Width="200" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddRelProfile" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnRemoveRelProfile" CssClass="btn btn-primary" runat="server" Text="Remove"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnExportRelProfile" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />
                                <asp:Button ID="btnCloseRel" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgRelationship" AllowSorting="True" runat="server" Width="100%"
                                    CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numPriceBookRuleDTLID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RelProfile" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkRelationshipSelectAll" runat="server" onclick="SelectAll('chkRelationshipSelectAll','dgRelationshipchk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgRelationshipchk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
