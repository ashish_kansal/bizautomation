﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection

Imports System.IO
Public Class frmWebAPIItemDetails
    Inherits BACRMPage
    Dim lngItemCode As Long
    Dim dtDynFields As DataTable
    Dim objGenericAdvSearch As New FormGenericAdvSearch
    Public dtGenericFormConfig As DataTable
    Public dsGenericFormConfig As DataSet
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
           
            ' LoadControls()

            GetFormFieldList()
            dtGenericFormConfig = dsGenericFormConfig.Tables(1)
            GenerateForm(dsGenericFormConfig)
            LoadControlValues()

            If Not IsPostBack Then
                FillDataToControls()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadControls()
        Try
            tblMain.Controls.Clear()
            Dim dtTableInfo As DataTable
            Dim objPageControls As New PageControls
            Dim numFormId As Integer

            numFormId = 50 'amazon fields

            Dim objWebAPIItemDetail As New WebAPIItemDetail

            dtTableInfo = GetFields()

            Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & lngItemCode.ToString() & ".xml"
            If System.IO.File.Exists(FilePath) Then
                Dim objStreamReader As New IO.StreamReader(FilePath)
                Dim x As New System.Xml.Serialization.XmlSerializer(objWebAPIItemDetail.GetType)
                objWebAPIItemDetail = CType(x.Deserialize(objStreamReader), WebAPIItemDetail)
                objStreamReader.Close()
            End If

            'GenerateForm(ds)


            Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
            Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
            Dim intCol1 As Int32 = 0
            Dim intCol2 As Int32 = 0
            Dim tblCell As TableCell

            Dim tblRow As TableRow
            Const NoOfColumns As Short = 2
            Dim ColCount As Int32 = 0


            For Each dr As DataRow In dtTableInfo.Rows


                dr("vcValue") = objWebAPIItemDetail.GetType.GetProperty(dr("vcPropertyName")).GetValue(objWebAPIItemDetail, Nothing)

                If ColCount = 0 Then
                    tblRow = New TableRow
                End If

                If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)
                    ColCount = 1
                End If

                If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                    Dim ddl As DropDownList
                    Dim dtData As New DataTable

                    If dr("numListID") = 0 Then
                        dtData = FillMagentoAttributeSets()
                        dr("bitCustomField") = 0
                    Else
                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                        dr("bitCustomField") = 1

                    End If

                    ddl = objPageControls.CreateCells(tblRow, dr, False, dtData, boolAdd:=True)

                Else
                    objPageControls.CreateCells(tblRow, dr, False, RecordID:=0, boolAdd:=True)
                End If


                If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    ColCount = 1
                End If

                If dr("intcoulmn") = 2 Then
                    If intCol1 <> intCol1Count Then
                        intCol1 = intCol1 + 1
                    End If

                    If intCol2 <> intCol2Count Then
                        intCol2 = intCol2 + 1
                    End If
                End If

                ColCount = ColCount + 1
                If NoOfColumns = ColCount Then
                    ColCount = 0
                    tblMain.Rows.Add(tblRow)
                End If
            Next

            If ColCount > 0 Then
                tblMain.Rows.Add(tblRow)
            End If



        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function GetFields() As DataTable
        Try
            Dim objPageLayout As New CPageLayout
            objPageLayout.CoType = ""
            objPageLayout.UserCntID = Session("UserContactId")
            objPageLayout.RecordId = 0
            objPageLayout.DomainID = Session("DomainID")
            objPageLayout.PageId = 4
            objPageLayout.PageType = 3
            objPageLayout.FormId = 50

            Dim ds As DataSet = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
            dtTableInfo = ds.Tables(0)

            objPageLayout.FormId = 51
            ds = objPageLayout.GetTableInfoDefault()
            dtTableInfo.Merge(ds.Tables(0).Copy())

            objPageLayout.FormId = 52
            ds = objPageLayout.GetTableInfoDefault()
            dtTableInfo.Merge(ds.Tables(0).Copy())

            objPageLayout.FormId = 53
            ds = objPageLayout.GetTableInfoDefault()
            dtTableInfo.Merge(ds.Tables(0).Copy())

            Return dtTableInfo
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub LoadControlValues()
        Dim objWebAPIItemDetail As New WebAPIItemDetail
        Try
            Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & lngItemCode.ToString() & ".xml"
            If System.IO.File.Exists(FilePath) Then
                Dim objStreamReader As New IO.StreamReader(FilePath)
                Dim x As New System.Xml.Serialization.XmlSerializer(objWebAPIItemDetail.GetType)
                objWebAPIItemDetail = CType(x.Deserialize(objStreamReader), WebAPIItemDetail)
                objStreamReader.Close()
            End If

            For Each dr As DataRow In dtGenericFormConfig.Rows
                If CCommon.ToString(dr("vcPropertyName")).Length > 0 Then
                    If Not objWebAPIItemDetail.GetType.GetProperty(dr("vcPropertyName")) Is Nothing Then
                        dr("vcValue") = objWebAPIItemDetail.GetType.GetProperty(dr("vcPropertyName")).GetValue(objWebAPIItemDetail, Nothing)
                    Else
                        dr("vcValue") = ""
                    End If
                End If
            Next

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Dim dtTableInfo As DataTable

    Sub GetFormFieldList()
        Try
            If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

            objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
            objGenericAdvSearch.FormID = 50
            objGenericAdvSearch.DomainID = Session("DomainID")
            dsGenericFormConfig = objGenericAdvSearch.GetAdvancedSearchFieldList()

            dsGenericFormConfig.Tables(0).TableName = "SectionInfo"                           'give a name to the datatable
            dsGenericFormConfig.Tables(1).TableName = "WebApiItemDetails"                           'give a name to the datatable

            dsGenericFormConfig.Tables(1).Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"), "[vcFormFieldName]") 'This columsn has to be an integer to trying to manipulate the datatype as Compute(Max) does not work on string datatypes
            dsGenericFormConfig.Tables(1).Columns.Add("vcValue", System.Type.GetType("System.String"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GenerateForm(ByVal dsGenericFormConfig As DataSet)
        Try
            GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI

            createDynamicFormControlsSectionWise(dsGenericFormConfig, tblItemDetails, 1, Session("DomainID"), Session("UserContactID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveFields()
        Try
            Dim objWebAPIItemDetail As New WebAPIItemDetail
            Dim objPageControls As New PageControls

            dtTableInfo = GetFields()
            For Each dr As DataRow In dtTableInfo.Rows
                If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And Not dr("vcPropertyName").ToString() = "MagentoProductAttributeSet" Then
                    objPageControls.SetValueForStaticFields(dr, objWebAPIItemDetail, tblMain, False)

                ElseIf Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("vcPropertyName").ToString() = "MagentoProductAttributeSet" Then

                    objPageControls.SetValueForStaticFields(dr, objWebAPIItemDetail, tblMain)

                End If
            Next


            'Serialize object to a xml file.
            If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) Then
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If


            Dim objStreamWriter As New StreamWriter(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & lngItemCode.ToString() & ".xml")
            Dim x As New System.Xml.Serialization.XmlSerializer(objWebAPIItemDetail.GetType)
            x.Serialize(objStreamWriter, objWebAPIItemDetail)
            objStreamWriter.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub SaveItemDetails()
        Try
            Dim objWebAPIItemDetail As New WebAPIItemDetail
            Dim dvConfig As DataView
            dtGenericFormConfig = dsGenericFormConfig.Tables(1)
            dvConfig = New DataView(dtGenericFormConfig)
            Dim objPropertyInfo As PropertyInfo

            If dvConfig.Count > 0 Then
                Dim i As Integer
                For i = 0 To dvConfig.Count - 1
                    Dim strControlID As String
                    Dim drState() As DataRow
                    drState = dtGenericFormConfig.Select("vcDbColumnName='" & dvConfig(i).Item("vcDbColumnName") & "'")
                    strControlID = drState(0)("numFormFieldID").ToString & "_" & drState(0)("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    If drState(0)("vcAssociatedControlType").ToString = "TextBox" Then
                        If Not CType(tblItemDetails.FindControl(strControlID), TextBox) Is Nothing Then
                            Dim txtBox As TextBox
                            txtBox = CType(tblItemDetails.FindControl(strControlID), TextBox)
                            drState(0)("vcValue") = txtBox.Text

                        End If
                    ElseIf drState(0)("vcAssociatedControlType").ToString = "TextArea" Then

                        If Not CType(tblItemDetails.FindControl(strControlID), TextBox) Is Nothing Then
                            Dim txtBox As TextBox
                            txtBox = CType(tblItemDetails.FindControl(strControlID), TextBox)
                            drState(0)("vcValue") = txtBox.Text

                        End If
                    ElseIf drState(0)("vcAssociatedControlType").ToString = "SelectBox" Then
                        If Not CType(tblItemDetails.FindControl(strControlID), DropDownList) Is Nothing Then
                            Dim ddl As DropDownList
                            ddl = CType(tblItemDetails.FindControl(strControlID), DropDownList)
                            If drState(0)("numListID") = 0 Then
                                drState(0)("vcValue") = ddl.SelectedItem.Value
                            Else
                                drState(0)("vcValue") = ddl.SelectedItem.Text
                            End If

                            'If ddl.SelectedIndex > 0 Then
                            '    drState(0)("vcValue") = ddl.SelectedItem.Text
                            'Else
                            '    drState(0)("vcValue") = 0
                            'End If
                        End If
                    End If
                    objPropertyInfo = objWebAPIItemDetail.GetType.GetProperty(drState(0)("vcPropertyName").ToString)

                    If Not objPropertyInfo Is Nothing AndAlso objPropertyInfo.PropertyType.FullName = "System.Decimal" Then
                        objPropertyInfo.SetValue(objWebAPIItemDetail, CType(drState(0)("vcValue"), Decimal), Nothing)
                    ElseIf Not objPropertyInfo Is Nothing AndAlso objPropertyInfo.PropertyType.FullName = "System.Int32" Then
                        objPropertyInfo.SetValue(objWebAPIItemDetail, CType(drState(0)("vcValue"), Int32), Nothing)
                    ElseIf Not objPropertyInfo Is Nothing AndAlso objPropertyInfo.PropertyType.FullName = "System.Int64" Then
                        objPropertyInfo.SetValue(objWebAPIItemDetail, CType(drState(0)("vcValue"), Int64), Nothing)
                    Else
                        objPropertyInfo.SetValue(objWebAPIItemDetail, drState(0)("vcValue"), Nothing)
                    End If

                    'objWebAPIItemDetail.GetType.GetProperty(drState(0)("vcPropertyName").ToString).SetValue(objWebAPIItemDetail, drState(0)("vcValue"), Nothing)
                Next
            End If

            'Serialize object to a xml file.
            If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) Then
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If


            Dim objStreamWriter As New StreamWriter(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & lngItemCode.ToString() & ".xml")
            Dim x As New System.Xml.Serialization.XmlSerializer(objWebAPIItemDetail.GetType)
            x.Serialize(objStreamWriter, objWebAPIItemDetail)
            objStreamWriter.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            'SaveFields()
            SaveItemDetails()
            objCommon = New CCommon
            objCommon.Mode = 23
            objCommon.UpdateRecordID = lngItemCode
            objCommon.UpdateSingleFieldValue()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function FillMagentoAttributeSets() As DataTable
        Dim dr As DataRow
        Dim dtData As New DataTable
        Dim Attrset As String
        Dim objCommon As New CCommon
        Dim AttrSetCollection As String = ""
        Dim arrAttributeSets() As String
        Dim SetId_Name() As String

        Try
            If dtData.Columns.Count = 0 Then
                dtData.Columns.Add("numFieldId")
                dtData.Columns.Add("vcFieldName")
            End If

            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 31
            objCommon.Str = 8
            AttrSetCollection = CCommon.ToString(objCommon.GetSingleFieldValue())

            arrAttributeSets = AttrSetCollection.Split(",")

            For Each Attrset In arrAttributeSets

                If Not String.IsNullOrEmpty(Attrset) AndAlso Attrset.Contains("|") Then
                    SetId_Name = Attrset.Split("|")
                    dr = dtData.NewRow
                    dr("numFieldId") = SetId_Name(0).Trim
                    dr("vcFieldName") = SetId_Name(1).Trim
                    dtData.Rows.Add(dr)
                End If
             
            Next
            dr = dtData.NewRow
            dr("numFieldId") = "0"
            dr("vcFieldName") = "-- Select One --"
            dtData.Rows.InsertAt(dr, 0)

        Catch ex As Exception
            Throw ex

        End Try

        Return dtData

    End Function

    Function createDynamicFormControlsSectionWise(ByVal dsFormConfig As DataSet, ByVal tblDynamicFormControls As HtmlTable, Optional ByVal lngFormID As Long = 0, Optional ByVal lngDomainID As Long = 0, Optional ByVal lngCntID As Long = 0) As Table

        Dim dtFormConfig As DataTable = dsFormConfig.Tables(1)
        Dim dtSection As DataTable = dsFormConfig.Tables(0)

        'tblDynamicFormControls.BorderColor = Color.Black                                            'Border Color is Black
        'tblDynamicFormControls.BorderWidth = Unit.Pixel(1)                                          'Border Width is 1 pixel
        tblDynamicFormControls.Width = "100%"                                        'Width of the table is 100%
        'tblDynamicFormControls.ID = "tblFormLeadBoxNonAOITable"                                     'give a name to the table
        tblDynamicFormControls.Border = 0                                        'There are no GridLines
        tblDynamicFormControls.EnableViewState = True                                               'Enable view state for the table
        tblDynamicFormControls.CellPadding = 0                                                      'Set the cell padding
        tblDynamicFormControls.CellSpacing = 0                                                      'Set the cell spacing
        'Dim boolCountryListBoxPresent As Boolean = False                                            'A flag which indicates if a country drop down is present or not

        Dim tblRow As HtmlTableRow                                                                  'declare a table row object
        Dim tblCell, tblControlCell, tblLiteralCell, tblTooltipCell, tblLinkCell As HtmlTableCell                                'declare a table cell object

        Dim dvSectionFields As DataView

        If dtFormConfig.Rows.Count > 0 Then                                                         'determine that the XML file exists and there is atleast one field
            For Each drSec As DataRow In dtSection.Rows
                dvSectionFields = New DataView(dtFormConfig)

                If drSec("Loc_Id") > 0 Then
                    dvSectionFields.RowFilter = "GRP_ID = " & drSec("Loc_Id") & " and boolAOIField = " & boolAOIField & ""
                Else
                    dvSectionFields.RowFilter = "intSectionID = " & drSec("intSectionID") & " and boolAOIField = " & boolAOIField & ""
                End If

                dvSectionFields.RowStateFilter = DataViewRowState.CurrentRows

                If dvSectionFields.Count > 0 Then
                    tblRow = New HtmlTableRow
                    tblCell = New HtmlTableCell
                    tblCell.InnerHtml = drSec("vcSectionName")
                    tblCell.Align = "left"
                    tblCell.Attributes.Add("class", "tblrowHeader")
                    tblCell.ColSpan = 7
                    tblRow.Cells.Add(tblCell)
                    tblDynamicFormControls.Rows.Add(tblRow)

                    Dim iMaxRows As Integer                                                                 'declare a variable which will contain the max nos of rows
                    iMaxRows = CInt(dvSectionFields.ToTable().Compute("Max(intRowNum)", ""))                      'Get the max value of the row number

                    Dim iMaxCols As Integer                                                                 'declare a variable which will contain the max nos of rows
                    iMaxCols = CInt(dvSectionFields.ToTable().Compute("Max(intColumnNum)", ""))                          'Get the max value of the Columns

                    Dim dvFormConfig As DataView                                                            'declare a dataview object
                    Dim iRowIndex, iColumnIndex As Integer                                                  'declare a row index variable
                    Dim iColNum, iRowNum As Integer                                                         'declare a column index variable
                    Dim litLableText As Literal                                                             'declare a literal
                    Dim litRequired As New Literal
                    Dim strControlType, strFieldName, strControlID As String
                    Dim dr As DataRowView
                    Dim intSearchCriteria As Integer
                    Dim strSearchCriteriaName As String
                    litRequired.Text = "<span class=normal4>*</span>"

                    'Dim UserSearchCriteria As New Hashtable
                    'Dim objSearch As New FormGenericAdvSearch
                    'objSearch.FormID = lngFormID
                    'objSearch.DomainID = lngDomainID
                    'objSearch.UserCntID = lngCntID
                    'objSearch.byteMode = 0
                    'Dim dt As DataTable = objSearch.ManageSearchCriteria()
                    'For Each dr1 As DataRow In dt.Rows
                    '    UserSearchCriteria.Add(dr1("vcFormFieldName"), dr1("intSearchOperator"))
                    'Next

                    For iRowIndex = 0 To iMaxRows                                                           'loop through the max rows to create rows for the table
                        iRowNum = iRowIndex                                                                 'find the row number
                        tblRow = New HtmlTableRow                                                               'instantiate a new table row
                        tblRow.Height = 23                                                    'set the height attribute
                        For iColumnIndex = 0 To iMaxCols * 2                                                'loop through the cell in the table row
                            dvFormConfig = New DataView(dvSectionFields.ToTable())                                       'store the dataview as the dataview of the form config
                            tblControlCell = New HtmlTableCell                                                  'create a new table literal cell
                            tblControlCell.Attributes.Add("class", "normal1Right")                                           'set the class attribute for teh tablecell
                            tblLiteralCell = New HtmlTableCell                                                  'create a new table control cell
                            tblLiteralCell.Attributes.Add("class", "normal1")                                          'set the class attribute for teh tablecell
                            tblLiteralCell.Align = "right"
                            tblTooltipCell = New HtmlTableCell
                            tblTooltipCell.Align = "right"
                            tblLinkCell = New HtmlTableCell
                            tblLinkCell.Align = "right"
                            tblLinkCell.Width = 0

                            'align the labels to the right
                            If (iColumnIndex Mod 2) = 0 Then                                                'Odd cells
                                iColNum = (tblRow.Cells.Count / 2) + 1                                      'the column number is dynamic
                                dvFormConfig.RowFilter = "intRowNum = " & iRowNum & " and intColumnNum = " & iColNum 'set the row filter
                                If dvFormConfig.Count > 0 Then
                                    dr = dvFormConfig(0)

                                    If dr("vcAssociatedControlType").ToString.ToLower = "checkbox" Then
                                        dr("vcAssociatedControlType") = "SelectBox"
                                        dr("vcListItemType") = "CHK"
                                        dr("numListID") = "0"
                                    End If
                                    Dim MaxLength As Integer = 50
                                    If CCommon.ToInteger(dr("intFieldMaxLength")) > 0 Then
                                        MaxLength = CCommon.ToInteger(dr("intFieldMaxLength"))
                                    End If

                                    strControlType = dr("vcAssociatedControlType").ToString.Replace(" ", "") 'replace whitespace with blank 
                                    strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_") 'remove white space
                                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                                    tblLiteralCell.InnerHtml = CStr(dr("vcNewFormFieldName")) & "&nbsp;" 'get the text as lable

                                    'intSearchCriteria = 0
                                    'strSearchCriteriaName = ""
                                    'If UserSearchCriteria.ContainsKey(strControlID) Then
                                    '    intSearchCriteria = UserSearchCriteria(strControlID)
                                    '    strSearchCriteriaName = [Enum].GetName(GetType(enmSearchCriteria), intSearchCriteria).Replace("_", " ")
                                    'End If


                                    'add the dynamic control to the table cell
                                    tblControlCell.Controls.Add(getDynamicControlAndData(Replace(Replace(Replace(dr("numFormFieldId"), "R", ""), "C", ""), "D", ""), strControlID, dr("vcListItemType"), strControlType, dr("numListID"), iRowIndex, iColNum, MaxLength))

                                    If Not IsDBNull(dr("vcToolTip")) And dr("vcToolTip").ToString.Trim.Length > 0 Then
                                        Dim lblToolTip As Label
                                        lblToolTip = New Label
                                        lblToolTip.Text = "[?]"
                                        lblToolTip.CssClass = "tip"
                                        lblToolTip.ToolTip = dr("vcToolTip").ToString.Trim
                                        tblLiteralCell.Controls.Add(lblToolTip)
                                        'tblControlCell.Controls.Add(lblToolTip)
                                        'tblTooltipCell.Controls.Add(lblToolTip)
                                    End If

                                    'If strFieldName = "vcGoogleProductCategory" Then
                                    '    Dim lblToolTip As Label
                                    '    lblToolTip = New Label
                                    '    lblToolTip.Text = "[?]"
                                    '    lblToolTip.CssClass = "tip"
                                    '    lblToolTip.ToolTip = "Click to list all Google Product Categories, select your product category, copy the Category Text and add it to the respective TextBox"
                                    '    tblLinkCell.Controls.Add(lblToolTip)
                                    '    tblLinkCell.Width = "auto"

                                    '    Dim hLink As HyperLink
                                    '    hLink = New HyperLink
                                    '    hLink.Text = "Google Product Category"
                                    '    hLink.CssClass = "normal"
                                    '    hLink.Target = "_blank"
                                    '    hLink.NavigateUrl = "http://support.google.com/merchants/bin/answer.py?hl=en&answer=1705911"
                                    '    tblLinkCell.Controls.Add(hLink)
                                    '    'tblLinkCell.ColSpan = 2
                                    'End If

                                End If
                            End If
                            tblRow.Cells.Add(tblLiteralCell)                                                'add the literal cell to the row
                            tblRow.Cells.Add(tblControlCell)
                            'tblRow.Cells.Add(tblLinkCell)
                            'tblRow.Cells.Add(tblTooltipCell)
                            'add the control cell to the row
                            iColumnIndex += 1                                                               'increment the column number as two cells are added at a time
                        Next
                        tblDynamicFormControls.Rows.Add(tblRow)                                             'add the new table row to the existing table
                    Next
                End If
            Next
        Else
            tblRow = New HtmlTableRow                                                                   'instantiate a tablerow object
            tblLiteralCell = New HtmlTableCell                                                          'create a new table control cell
            tblLiteralCell.Attributes.Add("class", "normal1")                                                     'set the class attribute for teh tablecell
            Dim litLableText As New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")                                                            'declare a literal
            tblLiteralCell.Controls.Add(litLableText)                                               'Add the literal control to the table cell
            tblRow.Cells.Add(tblLiteralCell)                                                        'Add the table cell to the table row
            tblDynamicFormControls.Rows.Add(tblRow)                                                 'Add the table row to the table
        End If
        'Return teh data table
    End Function

  
    Sub FillDataToControls()
        Try
            Dim dvConfig As DataView
            dtGenericFormConfig = dsGenericFormConfig.Tables(1)
            dvConfig = New DataView(dtGenericFormConfig)

            If dvConfig.Count > 0 Then
                Dim i As Integer
                For i = 0 To dvConfig.Count - 1
                    Dim strControlID As String
                    Dim drState() As DataRow
                    drState = dtGenericFormConfig.Select("vcDbColumnName='" & dvConfig(i).Item("vcDbColumnName") & "'")
                    strControlID = drState(0)("numFormFieldID").ToString & "_" & drState(0)("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    If drState(0)("vcAssociatedControlType").ToString = "TextBox" Then
                        If Not CType(tblItemDetails.FindControl(strControlID), TextBox) Is Nothing Then
                            Dim txtBox As TextBox
                            txtBox = CType(tblItemDetails.FindControl(strControlID), TextBox)
                            txtBox.Text = CCommon.ToString(drState(0)("vcValue"))

                        End If
                    ElseIf drState(0)("vcAssociatedControlType").ToString = "TextArea" Then

                        If Not CType(tblItemDetails.FindControl(strControlID), TextBox) Is Nothing Then
                            Dim txtBox As TextBox
                            txtBox = CType(tblItemDetails.FindControl(strControlID), TextBox)
                            txtBox.Text = CCommon.ToString(drState(0)("vcValue"))

                        End If
                    ElseIf drState(0)("vcAssociatedControlType").ToString = "SelectBox" Then
                        If Not CType(tblItemDetails.FindControl(strControlID), DropDownList) Is Nothing Then
                            Dim ddl As DropDownList
                            ddl = CType(tblItemDetails.FindControl(strControlID), DropDownList)
                            Dim dtData As DataTable

                            If drState(0)("numListID") = 0 Then
                                dtData = FillMagentoAttributeSets()

                                ddl.DataSource = dtData
                                ddl.DataTextField = "vcFieldName"
                                ddl.DataValueField = "numFieldId"
                                ddl.DataBind()
                                'ddl.Items.Insert(0, "--Select One--")
                                If Not ddl.Items.FindByValue(CCommon.ToInteger(drState(0)("vcValue"))) Is Nothing Then
                                    ddl.Items.FindByValue(CCommon.ToInteger(drState(0)("vcValue"))).Selected = True
                                End If

                            Else
                                If Not ddl.Items.FindByText(CCommon.ToString(drState(0)("vcValue"))) Is Nothing Then
                                    ddl.Items.FindByText(CCommon.ToString(drState(0)("vcValue"))).Selected = True
                                End If
                            End If
                           
                          
                        End If
                    End If

                Next
            End If

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

End Class