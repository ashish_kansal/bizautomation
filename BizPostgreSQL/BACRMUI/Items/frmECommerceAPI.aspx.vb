﻿Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmECommerceAPI
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(MODULEID.Administration, 40)

            If Not IsPostBack Then

                Dim objAPI As New WebAPI
                objAPI.WebApiId = CCommon.ToLong(GetQueryStringVal("WebApiID"))
                'objAPI.DomainID = CCommon.ToLong(Session("DomainID"))
                objAPI.Mode = 1
                dgECommerceAPI.DataSource = objAPI.GetWebApi
                dgECommerceAPI.DataBind()
                'hrfConfig.Attributes.Add("onclick", "javascript:parent.frames['mainframe'].location.href='../EBanking/frmBankConnectWizard.aspx';UnSelectTabs();")
                LoadDomainDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.ShipToPhoneNumber = txtShipToPhoneNo.Text.Trim()
            objUserAccess.UpdateShipPhoneNumber()
            LoadDomainDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadDomainDetails()
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            If dtTable.Rows.Count > 0 Then
                txtShipToPhoneNo.Text = CCommon.ToString(dtTable.Rows(0).Item("vcShipToPhoneNo"))

            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    'Private Sub dgECommerceAPI_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgECommerceAPI.ItemDataBound
    '    Try
    '        'If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
    '        '    Dim lbl As Label
    '        '    lbl = e.Item.FindControl("lblSecndFldName")
    '        '    If lbl.Text = "" Then
    '        '        lbl.Visible = False
    '        '        CType(e.Item.FindControl("txtSecondFldValue"), TextBox).Visible = False
    '        '    End If
    '        '    lbl = e.Item.FindControl("lblThirdFldName")
    '        '    If lbl.Text = "" Then
    '        '        lbl.Visible = False
    '        '        CType(e.Item.FindControl("txtThirdFldValue"), TextBox).Visible = False
    '        '    End If
    '        'End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
End Class