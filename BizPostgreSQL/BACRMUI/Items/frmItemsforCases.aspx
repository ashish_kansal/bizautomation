<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmItemsforCases.aspx.vb" Inherits="frmItemsforCases"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Item Details</title>
		<script language="javascript">
		function Close()
		{
			window.close();
			return false;
			
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right"><asp:Button ID="btnClose" CssClass="button" Runat="server" Text="Close" Width="50"></asp:Button></td>
				</tr>
				<tr>
					<td>
						<asp:datagrid id="dgItem" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Item" ItemStyle-VerticalAlign="Bottom">
									<ItemTemplate>
										<asp:Label ID=lblItem Runat=server Text= '<%# DataBinder.Eval(Container,"DataItem.ItemName") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Type">
									<ItemTemplate>
										<asp:Label ID="Label1" Runat=server Text= '<%# DataBinder.Eval(Container,"DataItem.Type") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<ItemTemplate>
										<asp:Label ID="Label2" Runat=server Text= '<%# DataBinder.Eval(Container,"DataItem.Desc") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Unit/Hours" ItemStyle-VerticalAlign="Bottom">
									<ItemTemplate>
										<asp:Label ID="Label3" Runat=server Text= '<%# DataBinder.Eval(Container,"DataItem.Unit") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn HeaderText="Price" DataFormatString="{0:#,##0.00}" DataField="Price"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="Amount" DataField="Amount" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
			<br>
		</form>
	</body>
</HTML>
