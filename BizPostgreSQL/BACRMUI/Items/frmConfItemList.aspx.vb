﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Partial Public Class frmConfItemList
    Inherits BACRMPage
    Dim objContact As CContacts

    Dim intFormID As Int16
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "Save()")
            intFormID = GetQueryStringVal("FormID")

            If Not IsPostBack Then

                Select Case intFormID
                    Case 21 'ItemList
                        GetUserRightsForPage(37, 9)
                    Case 22 ' What fields do you want to see in result when search for item in New Sales Order/Purchase order()
                        'What fields do you want to use to search result for item in New Sales Order/Purchase Order ?
                        m_aryRightsForPage = {3, 3, 3}
                    Case 23 'Sales Fulfillment
                        GetUserRightsForPage(10, 22)
                    Case 26 'Products and services
                        GetUserRightsForPage(10, 23)
                    Case 30 'BizCart Simple Search fields
                        m_aryRightsForPage = {3, 3, 3}
                        hdSiteID.Value = CCommon.ToLong(GetQueryStringVal("SiteID"))
                    Case 33 'Work Order
                        GetUserRightsForPage(10, 25)
                    Case 43 'Tickler list
                        m_aryRightsForPage = {3, 3, 3}
                    Case 44 'Inbox columns
                        GetUserRightsForPage(33, 10)
                    Case 96 'Customer display fields settings
                        m_aryRightsForPage = {3, 3, 3}
                    Case 97 'Customer search fields settings
                        m_aryRightsForPage = {3, 3, 3}
                    Case Else
                        m_aryRightsForPage = {3, 3, 3}
                End Select
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False

                If ddlSortType.Items.FindByValue(intFormID) IsNot Nothing Then
                    ddlSortType.Items.FindByValue(intFormID).Selected = True
                End If
                BindLists()
            End If

            If intFormID = 21 Then
                lblTitle.Text = "Items column settings"
                lblMaxFields.Text = "(Max 25)"
                lstSelectedfld.Attributes.Add("MaxAllowedFields", "25")
                trSort.Visible = False

            ElseIf intFormID = 22 And GetQueryStringVal("type") <> "" Then
                If GetQueryStringVal("type") = 1 Then
                    lblTitle.Text = "Item search column settings"
                End If
                trSort.Visible = False

            ElseIf intFormID = 26 Then
                lblTitle.Text = "Products and services column settings"
                lblMaxFields.Text = "(Max 20)"
                lstSelectedfld.Attributes.Add("MaxAllowedFields", "20")
                trSort.Visible = False

            ElseIf intFormID = 23 Then
                lblTitle.Text = "Sales Fulfillment column settings"
                trSort.Visible = False

            ElseIf intFormID = 30 Then
                lblTitle.Text = "BizCart Simple Item Search fields"
                trSort.Visible = False

            ElseIf intFormID = 33 Then
                lblTitle.Text = "Work Order column settings"
                trSort.Visible = False

            ElseIf intFormID = 43 Then
                lblTitle.Text = "Activity column settings "
                trSort.Visible = False

            ElseIf intFormID = 44 Then
                lblTitle.Text = "Inbox column settings"
                trSort.Visible = False

            ElseIf intFormID = 57 Then
                lblTitle.Text = "Shipping Fulfillment settings"
                trView.Visible = True
                trSort.Visible = False

            ElseIf intFormID = 84 Or intFormID = 85 Then
                lblTitle.Text = "Primary / Secondary Sort column settings"
                trSort.Visible = True

            ElseIf intFormID = 96 Then
                lblMaxFields.Text = "(Max 4)"
                lstSelectedfld.Attributes.Add("MaxAllowedFields", "4")
                lblTitle.Text = "Organization look-ahead column settings"
                trSort.Visible = False

            ElseIf intFormID = 97 Then
                lblMaxFields.Text = "(Max 3)"
                lstSelectedfld.Attributes.Add("MaxAllowedFields", "3")
                lblTitle.Text = "Organization search column settings"
                trSort.Visible = False

            ElseIf intFormID = 123 Then
                lblMaxFields.Text = "(Max 4)"
                lstSelectedfld.Attributes.Add("MaxAllowedFields", "4")
                lblTitle.Text = "Add/Edit Order - Item Grid Column Settings"
                trSort.Visible = False

            ElseIf intFormID = 125 Then
                lblTitle.Text = "Planning & Procurement Grid Columns Setting"
                trSort.Visible = False

            ElseIf intFormID = 126 Then
                lblTitle.Text = "Inventory Adjustment Grid Columns Setting"
                trSort.Visible = False

            ElseIf intFormID = 135 Then
                lblTitle.Text = "Purchase Fulfillment column settings"
                trSort.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindLists()
        Try
            Dim ds As DataSet
            objContact = New CContacts
            objContact.DomainID = Session("DomainId")
            objContact.FormId = intFormID

            If intFormID = 30 Or intFormID = 22 Then
                objContact.UserCntID = 0
            ElseIf intFormID = 32 Then
                objContact.UserCntID = GetQueryStringVal("SiteId")
            Else
                objContact.UserCntID = Session("UserContactId")
            End If

            If GetQueryStringVal("type") <> "" Then
                objContact.ContactType = GetQueryStringVal("type")
            Else
                objContact.ContactType = 0
            End If

            If intFormID = 57 Then
                objContact.ViewID = ddlView.SelectedValue
            End If

            ds = objContact.GetColumnConfiguration

            If intFormID = 22 AndAlso GetQueryStringVal("type") = 1 Then
                Dim foundRows() As Data.DataRow
                foundRows = ds.Tables(0).Select("vcDBColumnName = 'vcAttributes'")

                If foundRows.Count() > 0 Then
                    lstAvailablefld.DataSource = ds.Tables(0).Select("vcDBColumnName <> 'vcAttributes'").CopyToDataTable()
                Else
                    lstAvailablefld.DataSource = ds.Tables(0)
                End If

                lstAvailablefld.DataTextField = "vcFieldName"
                lstAvailablefld.DataValueField = "numFieldID"
                lstAvailablefld.DataBind()
            Else
                lstAvailablefld.DataSource = ds.Tables(0)
                lstAvailablefld.DataTextField = "vcFieldName"
                lstAvailablefld.DataValueField = "numFieldID"
                lstAvailablefld.DataBind()
            End If

           


            lstSelectedfld.DataSource = ds.Tables(1)
            lstSelectedfld.DataValueField = "numFieldID"
            lstSelectedfld.DataTextField = "vcFieldName"
            lstSelectedfld.DataBind()
            If Not lstAvailablefld.Items.FindByText("Opp Name") Is Nothing Then
                Dim item As ListItem
                item = lstAvailablefld.Items.FindByText("Opp Name")
                item.Text = "Sales Order Name"
            End If
            If Not lstSelectedfld.Items.FindByText("Opp Name") Is Nothing Then
                Dim item As ListItem
                item = lstSelectedfld.Items.FindByText("Opp Name")
                item.Text = "Sales Order Name"
            End If

            trSalesFulfill.Visible = False

            If objContact.FormId = 23 Or objContact.FormId = 26 Or objContact.FormId = 21 Or objContact.FormId = 33 Or objContact.FormId = 43 Or objContact.FormId = 57 Or objContact.FormId = 125 Or objContact.FormId = 126 Or objContact.FormId = 129 Or objContact.FormId = 135 Or objContact.FormId = 139 Then
                btnClose.Attributes.Add("onclick", "javascript:window.opener.location.href=window.opener.location.href;window.close();return false;")
            ElseIf objContact.FormId = 123 Then
                btnClose.Attributes.Add("onclick", "javascript:window.close(); window.opener.BindOrderGrid(); return false;")
            Else
                btnClose.Attributes.Add("onclick", "javascript:window.close();return false;")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objContact Is Nothing Then objContact = New CContacts

            Dim dsNew As New DataSet
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numFieldID")
            dtTable.Columns.Add("bitCustom")
            dtTable.Columns.Add("tintOrder")
            Dim i As Integer

            Dim dr As DataRow
            Dim str As String()
            str = hdnCol.Value.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numFieldID") = str(i).Split("~")(0)
                dr("bitCustom") = str(i).Split("~")(1)
                dr("tintOrder") = i
                dtTable.Rows.Add(dr)
            Next

            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)

            objContact.FormId = intFormID
            If GetQueryStringVal("type") <> "" Then
                objContact.ContactType = GetQueryStringVal("type")
            Else
                objContact.ContactType = 0
            End If

            objContact.DomainID = Session("DomainId")

            If intFormID = 30 Or intFormID = 22 Then
                objContact.UserCntID = 0
            ElseIf intFormID = 32 Then
                objContact.UserCntID = GetQueryStringVal("SiteId")
            Else
                objContact.UserCntID = Session("UserContactId")
            End If


            'If objContact.FormId = 30 Then 'bizcart search create index
            '    Dim ErrorMesageToLog As String = String.Empty
            '    Dim strErrorMessage As String = String.Empty
            '    If CCommon.ToLong(hdSiteID.Value) > 0 Then
            '        strErrorMessage = SearchLucene.ReIndexAllWebsiteOfDomain(CCommon.ToLong(Session("DomainID")), CCommon.ToLong(hdSiteID.Value), ErrorMesageToLog)

            '        If ErrorMesageToLog.Length > 0 Then
            '            ExceptionModule.ExceptionPublish(New Exception(ErrorMesageToLog), Session("DomainID"), Session("UserContactID"), Request)
            '        End If
            '    End If

            '    If strErrorMessage.Length > 0 Then
            '        litMessage.Text = strErrorMessage
            '        BindLists()
            '        Exit Sub
            '    End If
            'End If

            If intFormID = 57 Then
                objContact.ViewID = ddlView.SelectedValue
            End If

            objContact.strXml = dsNew.GetXml
            objContact.SaveContactColumnConfiguration()
            dsNew.Tables.Remove(dsNew.Tables(0))

            Try
                If intFormID = 22 Then
                    Dim objElasticSearchDataSync As New ElasticSearchDataSync
                    objElasticSearchDataSync.CreateItemIndexByDomain(Session("DomainId"))
                ElseIf intFormID = 96 Or intFormID = 97 Then
                    Dim objElasticSearchDataSync As New ElasticSearchDataSync
                    objElasticSearchDataSync.CreateOrganizationIndexByDomain(Session("DomainId"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                'DO NOT THROW ERROR
            End Try
           

            'If objContact.FormId = 23 Then
            '    objContact.bitDefault = False
            '    objContact.FilterID = ddlSortSalesFulFillment.SelectedValue()
            '    objContact.SaveDefaultFilter()
            '    Session("SFFilterBy") = ddlSortSalesFulFillment.SelectedIndex
            'End If

            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlView_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlView.SelectedIndexChanged
        Try
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlSortType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSortType.SelectedIndexChanged
        Try
            intFormID = CCommon.ToInteger(ddlSortType.SelectedValue)
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class