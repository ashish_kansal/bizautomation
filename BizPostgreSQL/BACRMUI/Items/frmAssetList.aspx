﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAssetList.aspx.vb"
    Inherits="BACRM.UserInterface.Items.frmAssetList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content7" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript" src="../JavaScript/prototype-1.6.0.2.compressed.js"></script>
    <script type="text/javascript" src="../JavaScript/comboClientSide.js"></script>
    <script type="text/javascript" language="javascript">
        function OpenNewAsset(a, b, c) {
            if (b == 0) {
                window.location.href = "../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + c + "&AssetCode=" + a;
            }
            else {

                window.location.href = "../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + c + "&AssetCode=" + a + "&DivisionID=" + b;
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table align="right" width="100%">
                <tr>
                    <td class="normal1" align="right">
                        <span style="float: left;">Search For</span>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="signup" Width="130"></asp:TextBox>
                        <asp:DropDownList ID="ddlSearch" runat="server" CssClass="signup">
                            <asp:ListItem Text="-Select One--" Value=""></asp:ListItem>
                            <asp:ListItem Text="Item" Value="vcItemName"></asp:ListItem>
                            <%--<asp:ListItem Text="Warehouse" Value="vcWarehouse"></asp:ListItem>--%>
                            <asp:ListItem Text="Serial No" Value="vcSKU"></asp:ListItem>
                            <asp:ListItem Text="ModelID" Value="vcModelID"></asp:ListItem>
                            <asp:ListItem Text="Vendor" Value="vcCompanyName"></asp:ListItem>
                            <asp:ListItem Text="Description" Value="txtItemDesc"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnGo" CssClass="button" Width="25" Text="Go" runat="server"></asp:Button>
                    </td>
                    <td class="normal1" align="left">
                        Filter&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlFilter" runat="server" Width="150" AutoPostBack="True" CssClass="signup">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Organization&nbsp;&nbsp;<telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px"
                           DropDownWidth="600px"
                            OnClientItemsRequested="OnClientItemsRequestedOrganization" 
                            ClientIDMode="Static" 
                            ShowMoreResultsBox="true" Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True"
                            EnableLoadOnDemand="True" >
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
                    </td>
                    <td class="normal1" align="left">
                        <asp:Button runat="server" ID="btnNew" Text="New" CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblList" runat="server"></asp:Label>Asset Items
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td>
                <telerik:RadGrid ID="gvAssetItem" runat="server" Width="100%" AutoGenerateColumns="False"
                    GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="tbl aspTable">
                    <MasterTableView DataKeyNames="numItemcode" HierarchyLoadMode="Client" DataMember="Asset">
                        <DetailTables>
                            <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                ShowFooter="true" DataKeyNames="numAssetItemId" DataMember="AssetSerial" CssClass="tbl aspTable">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="numAssetItemId" MasterKeyField="numItemcode" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSerialNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Model Id" ItemStyle-Width="10%" DataField="vcModelId">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="BarCode Id" ItemStyle-Width="10%" DataField="vcBarCodeId">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Purchase Date" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <%#ReturnDate(Eval("dtPurchase"))%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Warrante Expiration" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <%#ReturnDate(Eval("dtWarrante"))%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Location" ItemStyle-Width="10%" DataField="vcLocation">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Image" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Item Name" ItemStyle-Width="25%">
                                <ItemTemplate>
                                    <a href="javascript:void(0)" onclick="OpenNewAsset('<%#Eval("numItemCode")%>', '0', 'frmAssetList');">
                                        <%#Eval("vcitemName")%>
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Model Id" ItemStyle-Width="15%" DataField="vcModelId">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Description" ItemStyle-Width="25%" DataField="txtItemDesc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSKU">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Vendor" ItemStyle-Width="10%" DataField="Vendor">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="UPC" ItemStyle-Width="10%" DataField="numBarCodeId">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Cost" ItemStyle-Width="10%" DataField="Cost">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Department" ItemStyle-Width="10%" DataField="Department">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Purchase Date" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <%#ReturnDate(Eval("dtPurchase"))%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Warrante Expiration" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <%#ReturnDate(Eval("dtWarrentyTill"))%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtDelItemIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSaveAPIItemIds" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
