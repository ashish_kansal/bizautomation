'Created by anoop jayaraj
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Items

    Partial Public Class Items_frmAssignCategories
        Inherits BACRMPage
        Dim objItems As CItems
        Dim lngCategoryID As Long
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngCategoryID = CCommon.ToLong(GetQueryStringVal("CategoryID"))

                If Not IsPostBack Then

                    objItems = New CItems
                    objItems.byteMode = 0
                    objItems.DomainID = Session("DomainID")
                    objItems.CategoryProfileID = CCommon.ToLong(GetQueryStringVal("numCategoryProfileID"))
                    ddlCategory.DataSource = objItems.SeleDelCategory
                    ddlCategory.DataTextField = "vcCategoryName"
                    ddlCategory.DataValueField = "numCategoryID"
                    ddlCategory.DataBind()
                    ddlCategory.Items.Insert(0, "--Select One--")
                    ddlCategory.Items.FindByText("--Select One--").Value = 0
                    objItems.DomainID = Session("DomainID")
                    ddlItemGroup.DataSource = objItems.GetItemGroups.Tables(0)
                    ddlItemGroup.DataTextField = "vcItemGroup"
                    ddlItemGroup.DataValueField = "numItemGroupID"
                    ddlItemGroup.DataBind()
                    ddlItemGroup.Items.Insert(0, "--Select One--")
                    ddlItemGroup.Items.FindByText("--Select One--").Value = "0"

                    If lngCategoryID > 0 Then
                        If Not ddlCategory.Items.FindByValue(lngCategoryID) Is Nothing Then
                            ddlCategory.Items.FindByValue(lngCategoryID).Selected = True
                        End If

                        LoadItemsForCategory()
                    End If
                End If
                If txtTotalPage.Text.Trim = "" Then
                    txtTotalPage.Text = 1
                End If
                btnHier.Attributes.Add("onclick", "return OpenHier(" & GetQueryStringVal("numCategoryProfileID") & ")")
                chkAll.Attributes.Add("onclick", "return CheckAll()")
                btnAdd.Attributes.Add("onclick", "return Add()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
            Try
                lblCountAvailItems.Text = "1"
                txtCurrrentPageItems.Text = "1"
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Sub LoadItemsForCategory()
            Try
                txtCurrrentPageItems.Text = IIf(txtCurrrentPageItems.Text = "", 1, txtCurrrentPageItems.Text)
                txtCurrrentPage.Text = IIf(txtCurrrentPage.Text = "", 1, txtCurrrentPage.Text)
                If ddlCategory.SelectedIndex > 0 Then
                    If objItems Is Nothing Then objItems = New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.byteMode = 5
                    objItems.CategoryID = ddlCategory.SelectedValue

                    If txtCurrrentPage.Text.Trim <> "" Then
                        objItems.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text)
                    Else : objItems.CurrentPage = 1
                    End If
                    objItems.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue)
                    chkAddedItems.DataSource = objItems.SeleDelCategory
                    chkAddedItems.DataTextField = "vcItemName"
                    chkAddedItems.DataValueField = "numItemCode"
                    chkAddedItems.DataBind()

                    txtTotalPage.Text = objItems.TotalPages
                    lblTotal.Text = objItems.TotalPages
                    Dim chkListItem As ListItem
                    For Each chkListItem In chkAddedItems.Items
                        chkListItem.Selected = True
                    Next

                    If radALLItems.Checked = True Then
                        objItems.byteMode = 11
                        objItems.CategoryID = ddlCategory.SelectedItem.Value
                        objItems.CurrentPage = IIf(txtCurrrentPageItems.Text = "", 1, Convert.ToInt32(txtCurrrentPageItems.Text))
                        objItems.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue)
                        chkItems.DataSource = objItems.SeleDelCategory
                        lblCountAvailItems.Text = objItems.TotalPages
                        chkItems.DataTextField = "vcItemName"
                        chkItems.DataValueField = "numItemCode"
                        chkItems.DataBind()
                    ElseIf radItemFromGroups.Checked = True Then
                        If ddlItemGroup.SelectedIndex > 0 Then
                            objItems.byteMode = 12
                            objItems.CurrentPage = IIf(txtCurrrentPageItems.Text = "", 1, Convert.ToInt32(txtCurrrentPageItems.Text))
                            objItems.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue)
                            objItems.CategoryID = ddlItemGroup.SelectedItem.Value
                            chkItems.DataSource = objItems.SeleDelCategory
                            lblCountAvailItems.Text = objItems.TotalPages
                            chkItems.DataTextField = "vcItemName"
                            chkItems.DataValueField = "numItemCode"
                            chkItems.DataBind()
                        End If
                    End If
                End If
                lblTotalItems.Text = lblCountAvailItems.Text

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Sub Save()
            Try
                Dim i As Integer
                If objItems Is Nothing Then objItems = New CItems
                Dim dtTable As DataTable

                If radOneItemAtaTime.Checked = True Then
                    If radItem.SelectedValue <> "" Then
                        objItems.byteMode = 1
                        objItems.DomainID = Session("DomainID")
                        objItems.CategoryID = ddlCategory.SelectedValue
                        objItems.ItemCode = radItem.SelectedValue
                        objItems.Checked = 1
                        objItems.ManageSubCategoriesAndItems()
                    End If
                Else
                    objItems.byteMode = 4
                    objItems.DomainID = Session("DomainID")
                    objItems.CategoryID = ddlCategory.SelectedItem.Value
                    dtTable = objItems.SeleDelCategory

                    Dim chkListItem As ListItem
                    objItems.byteMode = 1

                    For Each chkListItem In chkAddedItems.Items
                        objItems.ItemCode = chkListItem.Value
                        If chkListItem.Selected = True Then
                            objItems.Checked = 1
                        Else : objItems.Checked = 0
                        End If
                        objItems.ManageSubCategoriesAndItems()
                    Next
                    For Each chkListItem In chkItems.Items
                        If chkListItem.Selected = True Then
                            objItems.Checked = 1
                            objItems.ItemCode = chkListItem.Value
                        End If
                        objItems.ManageSubCategoriesAndItems()
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                Response.Redirect("../Items/frmCategories.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                Response.Redirect("../Items/frmCategories.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub radALLItems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radALLItems.CheckedChanged
            Try
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub radItemFromGroups_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radItemFromGroups.CheckedChanged
            Try
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub ddlItemGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItemGroup.SelectedIndexChanged
            Try
                lblCountAvailItems.Text = "1"
                txtCurrrentPageItems.Text = "1"
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Save()
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkLastItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastItems.Click
            Try
                txtCurrrentPageItems.Text = lblCountAvailItems.Text

                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkPreviousItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousItems.Click
            Try
                If Val(txtCurrrentPageItems.Text) = 1 Then
                    'Exit Sub
                Else
                    txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) - 1
                End If

                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkFirstItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstItems.Click
            Try
                txtCurrrentPageItems.Text = 1

                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkNextItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextItems.Click
            Try
                If txtCurrrentPageItems.Text = lblCountAvailItems.Text Then
                    '                    Exit Sub
                Else
                    txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 1

                End If

                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk2Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Items.Click
            Try

                If Val(txtCurrrentPageItems.Text) + 1 = Val(lblCountAvailItems.Text) Or Val(txtCurrrentPageItems.Text) + 1 > Val(lblCountAvailItems.Text) Then

                Else
                    txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 2
                End If


                LoadItemsForCategory()


            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk3Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Items.Click
            Try

                If Val(txtCurrrentPageItems.Text) + 2 = Val(lblCountAvailItems.Text) Or Val(txtCurrrentPageItems.Text) + 2 > Val(lblCountAvailItems.Text) Then

                Else
                    txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 3

                End If


                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk4Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Items.Click
            Try

                If Val(txtCurrrentPageItems.Text) + 3 = Val(lblCountAvailItems.Text) Or Val(txtCurrrentPageItems.Text + 3) > Val(lblCountAvailItems.Text) Then
                    'Exit Sub
                Else : txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 4
                End If

                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk5Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Items.Click
            Try

                If Val(txtCurrrentPageItems.Text) + 4 = Val(lblCountAvailItems.Text) Or Val(txtCurrrentPageItems.Text + 4) > Val(lblCountAvailItems.Text) Then
                    'Exit Sub
                Else
                    txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 5
                End If

                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub


        Private Sub lnkLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLast.Click
            Try
                txtCurrrentPage.Text = txtTotalPage.Text
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Try
                If txtCurrrentPage.Text = 1 Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
                End If
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
            Try
                txtCurrrentPage.Text = 1
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnkNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNext.Click
            Try
                If txtCurrrentPage.Text = txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
                End If
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
            Try
                If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
                End If
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
            Try
                If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
                End If
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
            Try
                If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
                End If
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
            Try
                If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
                End If
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
            Try
                LoadItemsForCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As Exception)
            Try
                TryCast(Me.Master, ECommerceMenuMaster).ThrowError(exception)
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace