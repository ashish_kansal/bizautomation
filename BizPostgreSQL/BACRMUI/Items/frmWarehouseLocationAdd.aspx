﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmWarehouseLocationAdd.aspx.vb" Inherits=".frmWarehouseLocationAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function cartesianProductOf() {
            return Array.prototype.reduce.call(arguments, function (a, b) {
                var ret = [];
                a.forEach(function (a) {
                    b.forEach(function (b) {
                        ret.push(a.concat([b]));
                    });
                });
                return ret;
            }, [[]]);
        }

        function pageLoaded() {
            $("[id$=btnSave]").closest("div").hide();            $("[id$=btnGenerate]").click(function () {
                if ($("[id$=ddlWarehouse]").val() == 0) {
                    alert('please select warehouse');
                    return false;
                }

                var arr1 = $("[id$=txtAisle]").val().split(",");
                var arr2 = $("[id$=txtRack]").val().split(",");
                var arr3 = $("[id$=txtShelf]").val().split(",");                var arr4 = $("[id$=txtBin]").val().split(",");                var Combination = '';                if ($("[id$=txtAisle]").val().trim().length > 0) { Combination = Combination + "A"; }                if ($("[id$=txtRack]").val().trim().length > 0) { Combination = Combination + "B"; }                if ($("[id$=txtShelf]").val().trim().length > 0) { Combination = Combination + "C"; }                if ($("[id$=txtBin]").val().trim().length > 0) { Combination = Combination + "D"; }                if (Combination != "ABCD" && Combination != "ABC" && Combination != "AB" && Combination != "A") {
                    alert("Invalid combination, you must enter value of fields only in following combinations  1) Aisle  2) Aisle and Rack 3) Aisle and Rack and Shelf 4) Aisle and Rack and Shelf and Bin");                    return false;
                }                var arrProdOf3;                if (Combination.length == 1) { arrProdOf3 = cartesianProductOf(arr1); }                if (Combination.length == 2) { arrProdOf3 = cartesianProductOf(arr1, arr2); }                if (Combination.length == 3) { arrProdOf3 = cartesianProductOf(arr1, arr2, arr3); }                if (Combination.length == 4) { arrProdOf3 = cartesianProductOf(arr1, arr2, arr3, arr4); }                $(".LocationPlaceholder").html('');
                $(".LocationPlaceholder").append($("<table id='Locations'class='table table-bordered table-striped' cellspacing='0' rules='all' border='1' style='width:100%;border-collapse:collapse;'>"))                var header = "<tr><th>No</th><th>Location</th><th></th></tr>"                $("[id$=Locations]").append(header);                $.each(arrProdOf3, function (index, value) {
                    var row = "<tr><td>" + (index + 1) + "</td><td class='locationValue'>" + value.toString().replace(/,/g, ".").replace("...", "").replace("..", "") + "</td>" + "<td><input type='button' style='font-family: FontAwesome' title='Delete' class='btn btn-xs btn-danger' value='&#xf1f8;' onclick='Remove(this);'></input></td></tr>";
                    $("[id$=Locations]").append($(row));
                });                $("[id$=btnSave]").closest("div").show();                return false;
            });

            $("[id$=btnAddOne]").click(function () {

                if ($("[id$=ddlWarehouse]").val() == 0) {
                    alert('please select warehouse');
                    return false;
                }

                var arr1 = $("[id$=txtAisle]").val();
                var arr2 = $("[id$=txtRack]").val();
                var arr3 = $("[id$=txtShelf]").val();                var arr4 = $("[id$=txtBin]").val();                if (arr1.indexOf(",") >= 0) {
                    alert('Comma is not allowed when adding location one by one');
                    return false;
                }                if (arr2.indexOf(",") >= 0) {
                    alert('Comma is not allowed when adding location one by one');
                    return false;
                }                if (arr3.indexOf(",") >= 0) {
                    alert('Comma is not allowed when adding location one by one');
                    return false;
                }                if (arr4.indexOf(",") >= 0) {
                    alert('Comma is not allowed when adding location one by one');
                    return false;
                }                var Combination = '';                if ($("#txtAisle").val().trim().length > 0) { Combination = Combination + "A"; }                if ($("#txtRack").val().trim().length > 0) { Combination = Combination + "B"; }                if ($("#txtShelf").val().trim().length > 0) { Combination = Combination + "C"; }                if ($("#txtBin").val().trim().length > 0) { Combination = Combination + "D"; }                if (Combination != "ABCD" && Combination != "ABC" && Combination != "AB" && Combination != "A") {
                    alert("Invalid combination, you must enter value of fields only in following combinations  1) Aisle  2) Aisle and Rack 3) Aisle and Rack and Shelf 4) Aisle and Rack and Shelf and Bin");                    return false;
                }                if (arr2.trim() != "") { arr2 = "." + arr2; }                if (arr3.trim() != "") { arr3 = "." + arr3; }                if (arr4.trim() != "") { arr4 = "." + arr4; }                var location = arr1 + arr2 + arr3 + arr4;                if ($("[id$=Locations]").length == 0) {

                    $(".LocationPlaceholder").html('');
                    $(".LocationPlaceholder").append($("<table id='Locations'class='table table-bordered table-responsive' cellspacing='0' rules='all' style='width:100%;'>"))                    var header = "<tr><th>No</th><th>Location</th><th></th></tr>"                    $("[id$=Locations]").append(header);
                }                var index = $("[id$=Locations] tr").length - 1;
                var row = "<tr><td>" + (index + 1) + "</td><td class='locationValue'>" + location.toString() + "</td>" + "<td><input type='button' style='font-family: FontAwesome' class='btn btn-xs btn-danger' title='Delete' value='&#xf1f8;' onclick='Remove(this);'/></td></tr>";
                $("[id$=Locations]").append($(row));
                $("[id$=btnSave]").closest("div").show();                return false;
            });

            $("[id$=btnCancel]").click(function () {

                if (confirm('Are you sure, you want to remove all loctions?\n(you can still re-generate same by clicking on Generate button)') == true) {
                    $(".LocationPlaceholder").html('');
                    $("[id$=btnSave]").closest("div").hide();
                    return false;
                }
                return false;
            });

            $("[id$=btnSave]").click(function () {

                if ($("[id$=ddlWarehouse]").val() == 0) {
                    alert('please select warehouse');
                    return false;
                }
                var str = "";
                $("[id$=Locations] .locationValue").each(function () {
                    str = str + $(this).text() + ",";
                });

                $("[id$=hdnLocationString]").val(str);

            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });        function Remove(element) {
            element.closest("tr").remove();
            return false;
        }    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton runat="server" CssClass="btn btn-info" ID="btnBack"><i class="fa fa-backward"></i>&nbsp;&nbsp;Back</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Add Warehouse locations
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row padbottom10">
        <div class="col-sm-12 col-md-9">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Warehouse <span style="color: red">*</span></label>
                        <asp:DropDownList runat="server" ID="ddlWarehouse" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Aisles</label>
                        <div>
                            <asp:TextBox ID="txtAisle" CssClass="form-control" runat="server" Text="" TextMode="MultiLine">
                            </asp:TextBox>
                            <span class="text-aqua">For multiple Ailse enter comma seperate values i.e A1,A2</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Racks</label>
                        <div>
                            <asp:TextBox ID="txtRack" CssClass="form-control" runat="server" TextMode="MultiLine">
                            </asp:TextBox>
                            <span class="text-aqua">For multiple Rack enter comma seperate values i.e R1,R2,R3</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label>Shelves</label>
                        <div>
                            <asp:TextBox ID="txtShelf" CssClass="form-control" runat="server" TextMode="MultiLine">
                            </asp:TextBox>
                            <span class="text-aqua">For multiple Shelf enter comma seperate values i.e S1,S2,S3,S4</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Bins</label>
                        <div>
                            <asp:TextBox ID="txtBin" CssClass="form-control" runat="server" TextMode="MultiLine">
                            </asp:TextBox>
                            <span class="text-aqua">For multiple Bin enter comma seperate values i.e B1,B2,B3,B4,B5</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <img src="../images/LocationMgt.png" alt="" class="img-responsive" />
        </div>
        <div class="col-xs-12 text-center">
            <asp:Button Text="Generate Multiple" runat="server" CssClass="btn btn-primary" ID="btnGenerate" />
            <asp:Button Text="Add One by One" runat="server" CssClass="btn btn-primary" ID="btnAddOne" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="LocationPlaceholder table-responsive"></div>
        </div>
        <div class="col-xs-12 text-center">
            <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="btn btn-primary" />
            <asp:Button ID="btnCancel" Text="Clear" runat="server" CssClass="btn btn-primary" />
        </div>
    </div>

    <asp:HiddenField ID="hdnLocationString" runat="server" />
</asp:Content>
