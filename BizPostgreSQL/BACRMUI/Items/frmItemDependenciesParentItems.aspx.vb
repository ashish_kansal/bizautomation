﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Public Class frmItemDependenciesParentItems
    Inherits BACRMPage

    Dim lngItemCode As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
            btnClose.Attributes.Add("onclick", "return Close()")

            If Not IsPostBack Then
                bindItemDependencies()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindItemDependencies()
        Try
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.byteMode = 1

            gvItemDependencies.DataSource = objItems.GetItemOrderDependencies()
            gvItemDependencies.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class