<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmManageAssembly.aspx.vb"
    Inherits=".frmManageAssembly" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">

    <link rel="stylesheet" href='<%# ResolveUrl("~/CSS/font-awesome.min.css")%>' />
    <style>
        .rgHeader {
            background-color: #1473b4 !important;
            color: #fff;
            font-size: 13px;
            border: 0px solid #e1e1e1;
            font-weight: 600 !important;
        }

        #grdAddedItems tr td {
            padding: 6px;
            font-size: 13px !important;
        }

        .gridHeader td, .gridHeader th {
            background: #1473B4;
            color: white;
            font-weight: bold;
            padding: 6px;
            border: 1px solid #CACED9;
            text-align: center;
            font-size: 13px !important;
        }

        .gridItem td {
            border: 1px solid #CACED9;
            background-color: #FFF;
            padding: 6px;
        }

        .btn-danger {
            background-color: #b50707;
            color: #fff;
        }

        .tblItems tr td {
            padding: 5px;
        }

        .gridItem table td {
            border: none;
        }
    </style>
    <title>Manage Assembly</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CreateWorkOrder() {
            if ($("#grdAddedItems tbody tr").length <= 1) {
                alert("Please add atleast one assembly item into list");
                return false;
            }
        }
        function Save() {
            var combo = $find('<%=radCmbSearch.ClientID %>');
            var editItemCode = parseFloat($("#<%=hdnEditItemCode.ClientID%>").val());
            if (combo.get_value() == "" && editItemCode == 0) {
                alert("Please select one item");
                $("#radCmbSearch_Input").focus();
                return false;
            }
            if (document.getElementById("txtUnits").value == "") {
                alert("Enter Units")
                document.getElementById("txtUnits").focus();
                return false;
            }

            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0")
                document.getElementById("txtUnits").focus();
                return false;
            }

            if (document.getElementById("ddlAssWareHouse").value == 0) {
                alert("Select Warehouse")
                document.getElementById("ddlAssWareHouse").focus();
                return false;
            }
            if (document.getElementById('ctl00_Content_calenderStartDate_txtDate').value == 0) {
                alert("Enter Planned Start Date")
                document.getElementById('ctl00_Content_calenderStartDate_txtDate').focus();
                return false;
            }
            if (document.getElementById('ctl00_Content_calenderFinishDate_txtDate').value == 0) {
                alert("Enter Requested Finish Date")
                document.getElementById('ctl00_Content_calenderFinishDate_txtDate').focus();
                return false;
            }
            var checkbox = document.getElementById("chkWorkOrder");
            if (checkbox.checked) {
                if (document.getElementById('ctl00_Content_calCompliationDate_txtDate').value == 0) {
                    alert("Enter Expected Completion Date")
                    document.getElementById('ctl00_Content_calCompliationDate_txtDate').focus();
                    return false;
                }
            }
            if (Number(document.getElementById("hfAverageCost").value) == 0) {

                return confirm('Average Cost of Item is 0.00, are you sure you want to add stock?');
            }
        }
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $("#btnAddMore").click();
            }
        });
        $(document).ready(function () {
            $("#ctl00_Content_calenderStartDate_txtDate").attr("TabIndex", "7");
            $("#ctl00_Content_calenderFinishDate_txtDate").attr("TabIndex", "10");
            $("#radCmbSearch_Input").focus();
            $("#chkWorkOrder").change(function () {
                if ($(this).is(':checked')) {
                    document.getElementById("trWorkOrder").style.display = '';
                } else {
                    document.getElementById("trWorkOrder").style.display = 'none';
                }
            });

            if ($("#chkWorkOrder").is(":checked")) {
                document.getElementById("trWorkOrder").style.display = '';
            }

            
            $("#txtUnits").change(function () {
                var checkbox = document.getElementById("chkWorkOrder");
                if (!checkbox.checked) {
                    if (Number($(this).val()) > Number($("#lblMaxQty").text())) {
                        $(this).val(0);
                        alert('Insufficient stock of child items,please re enter value.')
                        return false;
                    }
                }
                var itemCost = Number($("[id$=hdnMaterialCost]").val()) + Number($("[id$=hdnLabourCost]").val()) + Number($("[id$=hdnOverheadCost]").val());
                var totalCost = itemCost * Number($(this).val());
                $("[id$=lblUnitTotalCost]").text("<%= Session("Currency")%>" + " " + itemCost.toString() + " / " + "<%= Session("Currency")%>" + " " + totalCost.toString());

                var startDate = new Date($("[id$=calenderStartDate_txtDate]").val());
                var finishDate = new Date($("[id$=hdnProjectedFinishDate]").val());
                var newdate = new Date(startDate);
                newdate.setDate(startDate.getDate() + (Math.round((finishDate - startDate) / (1000 * 60 * 60 * 24)) * Number($(this).val()) - 1));

                $("[id$=lblProjectFinishDate]").text(newdate.format('<%= BACRM.BusinessLogic.Common.CCommon.GetValidationDateFormat() %>'));
            });

        });

        function CloseAndRedirect() {
            alert('Work Order created Successfully.');
            if (window.opener) {
                window.opener.location.href = '../Opportunity/frmWorkOrderList.aspx';
            }
            window.close();
        }

        function NotAuthorized(url) {
            if (window.opener) {
                window.opener.location.href = url;
            }
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnBuildStock" runat="server" CssClass="btn btn-primary" OnClientClick="return CreateWorkOrder()" Text="Build to Stock" />
                <asp:Button ID="btnSave" runat="server" OnClientClick="return CreateWorkOrder()" CssClass="btn btn-primary" Text="Create Work Order(s)" />
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
        </div>
    </div>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Create Assemblies / Work Orders
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
       <div class="row assembly-item-detail">
        <div class="col-xs-12 col-sm-6">
            <div>
                <div class="pull-left">
                    <label>Assembly Item</label>
                </div>
                <div class="pull-right font-bold">
                    <i>(Max build qty: 
                    <asp:Label Text="0" runat="server" ID="lblMaxQty" />)</i>
                </div>
            </div>
            <div>
                <telerik:RadComboBox ID="radCmbSearch" runat="server" DropDownWidth="500px" Width="100%" Height="300"
                    Skin="Default" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                    ItemsPerRequest="10" EnableVirtualScrolling="true" ItemRequestTimeout="1000" DropDownCssClass="ItemListSlide"
                    OnItemsRequested="radCmbSearch_ItemsRequested" OnSelectedIndexChanged="radCmbSearch_SelectedIndexChanged" AutoPostBack="true" HighlightTemplatedItems="true" ClientIDMode="Static" ZIndex="999"
                    TabIndex="1" MaxHeight="230">
                    <HeaderTemplate>
                        <table style="width: 500px">
        <tr>
                                <th style="width: 100px; text-align: center; font-weight: bold">Item ID
                                </th>
                                <th style="width: 250px; text-align: center; font-weight: bold">Name
                                </th>
                                <th style="width: 150px; text-align: center; font-weight: bold">Model ID
                                </th>
        </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table style="width: 500px">
        <tr>
                                <td style="width: 100px">
                                    <%# DataBinder.Eval(Container, "Value")%>
            </td>
                                <td style="width: 250px;">
                                    <%# DataBinder.Eval(Container, "Text")%>
            </td>
                                <td style="width: 150px">
                                    <%# DataBinder.Eval(Container, "Attributes['vcModelID']")%>
            </td>
        </tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadComboBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Sales Order<span style="color: red"> *</span></label>
                <div>
                    <telerik:RadComboBox ID="radCmbOrder_Search" runat="server" DropDownWidth="500px" Width="100%" Height="300"
                        Skin="Default" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                        ItemsPerRequest="10" EnableVirtualScrolling="true" ItemRequestTimeout="1000" DropDownCssClass="ItemListSlide"
                        OnItemsRequested="radCmbOrder_ItemsRequested" AutoPostBack="true" HighlightTemplatedItems="true" ClientIDMode="Static" ZIndex="999"
                        TabIndex="9" MaxHeight="230">
                        <HeaderTemplate>
                            <table style="width: 500px">
        <tr>
                                    <th style="width: 250px; text-align: center; font-weight: bold">Order ID
                                    </th>
                                    <th style="width: 250px; text-align: center; font-weight: bold">Order Name
                                    </th>
                                    <th style="width: 100px; text-align: center; font-weight: bold">Customer Name
                                    </th>
        </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table style="width: 500px">
                        <tr>
                                    <td style="width: 100px">
                                        <%# DataBinder.Eval(Container, "Value")%>
                            </td>
                                    <td style="width: 250px;">
                                        <%# DataBinder.Eval(Container, "Text")%>
                            </td>
                                    <td style="width: 150px">
                                        <%# DataBinder.Eval(Container, "Attributes['vcCompanyName']")%>
                            </td>
                        </tr>
                            </table>
                        </ItemTemplate>
                    </telerik:RadComboBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="divItemDetail" runat="server">
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Units<span style="color: red"> *</span></label>
                <asp:TextBox ID="txtUnits" TabIndex="2" CssClass="form-control" runat="server" Text="1" onkeypress="CheckNumber(2,event);"></asp:TextBox>
                <asp:HiddenField ID="hdnEditSoid" Value="0" runat="server" />
                <asp:HiddenField ID="hdnEditItemCode" Value="0" runat="server" />
            </div>
            <div class="form-group">
                <label>Warehouse<span style="color: red"> *</span></label>
                <asp:DropDownList ID="ddlAssWareHouse" runat="server" TabIndex="4" CssClass="form-control" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Manager</label>
                <asp:DropDownList ID="ddlWOAssignTo" TabIndex="3" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                </asp:DropDownList>
            </div>
            <div class="form-group">
                <label>Build Process</label>
                <asp:DropDownList ID="ddlBusinessProcess" TabIndex="5" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Comments</label>
                <asp:TextBox ID="txtComments" CssClass="form-control" TabIndex="6" TextMode="MultiLine" Style="height: 108px" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Mfg Cost</label>
                <div>
                    <ul class="list-unstyled">
                        <li><i>Materials:<asp:Label runat="server" ID="lblMaterialCost"></asp:Label></i></li>
                        <li><i>Overhead:<asp:Label runat="server" ID="lblOverheadCost"></asp:Label></i></li>
                        <li><i>Projected Labour:<asp:Label runat="server" ID="lblLabourCost"></asp:Label></i></li>
                        <li><i><b>Unit Cost / Total Cost:</b><asp:Label runat="server" ID="lblUnitTotalCost"></asp:Label></i></li>
                    </ul>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hdnMaterialCost" />
            <asp:HiddenField runat="server" ID="hdnOverheadCost" />
            <asp:HiddenField runat="server" ID="hdnLabourCost" />
            <asp:HiddenField runat="server" ID="hdnProjectedFinishDate" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Planned Start<span style="color: red"> *</span></label>
                <BizCalendar:Calendar ID="calenderStartDate" TabIndex="7" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Requested Finish<span style="color: red"> *</span></label>
                <BizCalendar:Calendar ID="calenderFinishDate" TabIndex="8" runat="server" ClientIDMode="AutoID" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label>Projected Finish</label>
                <div>
                    <asp:Label ID="lblProjectFinishDate" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3" style="line-height: 60px">
            <asp:Button CssClass="btn btn-sm btn-primary" ID="btnAddMore" TabIndex="8" OnClientClick="return Save()" runat="server" Text="Add" />
        </div>
    </div>
    <div class="row padbottom10" id="divChildItems" runat="server">
        <div class="col-xs-12">
            <div class="table-responsive">
                <telerik:RadTreeList ID="rtlChildItems" runat="server" ParentDataKeyNames="numParentID" DataKeyNames="numItemDetailID" AutoGenerateColumns="false">
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />
                    <Columns>
                        <telerik:TreeListBoundColumn DataField="numItemKitID" UniqueName="numItemKitID" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numItemCode" UniqueName="numItemCode" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numItemDetailID" UniqueName="numItemDetailID" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numWarehouseItmsID" UniqueName="numWarehouseItmsID" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="RStageLevel" UniqueName="RStageLevel" Visible="false"></telerik:TreeListBoundColumn>

                        <telerik:TreeListBoundColumn DataField="vcItemName" HeaderText="Item" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="charItemType" HeaderText="Item Type" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                        <telerik:TreeListTemplateColumn HeaderText="Units per build" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numCalculatedQty") %>'></asp:Label>
                                    <%# IIf(DataBinder.Eval(Container, "DataItem.vcIDUnitName") Is Nothing, "", DataBinder.Eval(Container, "DataItem.vcIDUnitName", " / {0}"))%>
                                <asp:Label ID="lblItemType" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.ItemType") %>'
                                    Visible="false"></asp:Label>
                                <asp:Label ID="lblDescription" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.txtItemDesc") %>'
                                        Visible="false"></asp:Label>
                                <asp:Label ID="lblSequence" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.sintOrder") %>'
                                        Visible="false"></asp:Label>
                                <asp:Label ID="lblUOMId" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numIDUOMId") %>'
                                        Visible="false"></asp:Label>
                                <asp:Label ID="lblQuantity_Original" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numQtyItemsReq") %>'
                                        Visible="false"></asp:Label>
                                <asp:Label ID="lblWareHouseItemId" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numWareHouseItemId") %>'
                                        Visible="false"></asp:Label>
                                <asp:Label ID="lblUOMQuantity" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numUOMQuantity") %>'
                                        Visible="false"></asp:Label>
                                <asp:Label ID="lblOnHand" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numOnHand") %>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                        </telerik:TreeListTemplateColumn>
                        <telerik:TreeListBoundColumn DataField="vcWareHouse" HeaderText="WareHouse" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numAvailable" HeaderText="On Hand" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numOnHand" HeaderText="Available" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numOnOrder" HeaderText="On Order" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numBackOrder" HeaderText="Back Order" HeaderStyle-HorizontalAlign="Center"></telerik:TreeListBoundColumn>
                    </Columns>
                </telerik:RadTreeList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:GridView ID="grdAddedItems" runat="server" ShowHeader="true" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true" CssClass="table table-striped table-bordered tblPrimary"
                 OnRowCommand="grdAddedItems_RowCommand" OnRowDataBound="grdAddedItems_RowDataBound" OnRowEditing="grdAddedItems_RowEditing" OnRowDeleting="grdAddedItems_RowDeleting" AutoGenerateColumns="false" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Items to build">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnNumItemCode" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numItemCode") %>' />
                            <asp:Label ID="lblItemCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcItemName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="WareHouse">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnComments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.vcComments") %>' />
                            <asp:HiddenField ID="hfAssetChartAcntID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numAssetChartAcntID") %>' />
                            <asp:HiddenField ID="hfAverageCost" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numAverageCost") %>' />
                            <asp:HiddenField ID="hdnWareHouseId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numWareHouseId") %>' />
                            <asp:Label ID="lblWareHouseName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcWarehouseName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Units">
                        <ItemTemplate>
                            <asp:Label ID="lblUnits" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.numUnits") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Planned Start">
                        <ItemTemplate>
                            <BizCalendar:Calendar ID="rdpStartDate" runat="server" ClientIDMode="AutoID" ></BizCalendar:Calendar>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Finish">
                        <ItemTemplate>
                            <BizCalendar:Calendar ID="rdpFinishDate" runat="server" ClientIDMode="AutoID"></BizCalendar:Calendar>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="(SO) Release Date">
                        <ItemTemplate>
                            <asp:Label ID="lblSOName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcSalesOrderName") %>'></asp:Label>
                            <asp:HiddenField ID="hdnSOId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numSalesOrderId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Build Process">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnBusinessProcessId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numBusinessProcessId") %>' />
                            <asp:Label ID="lblBuildProcess" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcBusinessProcessName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Build Manager">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnJobManager" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numAssigneTo") %>' />
                            <asp:Label ID="lbljobManager" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcAssignToName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="61">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEdit" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "numItemCode") %>' runat="server" CssClass="btn btn-xs btn-info"><i class="fa fa-pencil"></i></asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "numItemCode") %>' CssClass="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                        </Columns>
            </asp:GridView>
        </div>
    </div>
    <table border="0" class="tblItems">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr id="trNoWarehouses" runat="server" visible="false">
            <td>
                <label style="color: red">Item does no have any warehouse(s).</label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfAverageCost" runat="server" Value="0.0" />
    <asp:HiddenField ID="hfItemName" runat="server" Value="" />
    <asp:HiddenField ID="hfAssetChartAcntID" runat="server" Value="" />
</asp:Content>
