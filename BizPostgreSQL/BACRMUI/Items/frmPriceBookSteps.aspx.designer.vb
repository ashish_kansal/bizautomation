﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmPriceBookSteps

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Table3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Table3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''pnlItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlItems As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''radItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radItem As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''btnAddItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddItem As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnExportItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportItem As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRemItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemItem As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''dgItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgItems As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''pnlItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlItemClassification As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlItemClassification As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAddItemClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddItemClass As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnExportItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportItemClassification As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRemoveItemClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveItemClass As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCloseIC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseIC As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''dgItemClass control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgItemClass As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''pnlOrganizations control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlOrganizations As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''radCmbCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radCmbCompany As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''btnAddOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddOrg As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnExportOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportOrg As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRemoveOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveOrg As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCloseOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseOrg As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''dgOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgOrg As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''pnlRelProfiles control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRelProfiles As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlRelationship control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRelationship As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProfile As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAddRelProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddRelProfile As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRemoveRelProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveRelProfile As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnExportRelProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportRelProfile As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCloseRel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseRel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''dgRelationship control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgRelationship As Global.System.Web.UI.WebControls.DataGrid
End Class
