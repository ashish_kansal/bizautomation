﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmECommerceAPI.aspx.vb"
    MasterPageFile="~/common/ECommerceMenuMaster.Master" Inherits=".frmECommerceAPI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">

        function OpenSetting(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

    </script>
    <style type="text/css">
        .paging-part{
            display:none;
        }
    </style>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" Text="Save" runat="server" CssClass="btn btn-primary"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    E-Commerce API
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a href='#' onclick="OpenSetting('../Opportunity/frmImportMarketplaceOrder.aspx');">Import Marketplace Order</a> |
                <a href='#' onclick="OpenSetting('../Items/frmImportWebApiItems.aspx');">Import Marketplace Items</a> |
                <a href='#' onclick="OpenSetting('../Service/frmReadAPILogs.aspx?Log=MarketplaceAPILog');">Read Logs</a> |
                <a href='#' onclick="OpenSetting('../Service/frmReadAPILogs.aspx?Log=TransactionLogs');">Downloaded Transactions Logs</a>
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right form-inline form-group">
                If Ship To phone number is not available in markeplace orders then use following phone number:
                                    <asp:Label ID="Label48" Text="[?]" CssClass="tip" runat="server" ToolTip="In order to generate shipping label it is mandatory to have shipper phone number, please provide a Phone Number which can be used if the Marketplace Orders doesnot contain a Shipping Address Phone Number, it is known to happen in amazon marketplace" />
                <asp:TextBox ID="txtShipToPhoneNo" CssClass="form-control" runat="server" Width="130" title="">
                </asp:TextBox>
            </div>
        </div>
    </div>
    <%--<table width="100%" style="background-color:#ececec">
        <tr>
            <td class="normal1" align="right" valign="top">
            </td>
            <td class="normal1" valign="top">
                
            </td>
        </tr>
    </table>--%>
    <asp:DataGrid ID="dgECommerceAPI" AutoGenerateColumns="false" runat="server" CssClass="table table-responsive table-bordered tblDataGrid"
        Width="100%">
        <AlternatingItemStyle CssClass="ais" />
        <ItemStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <Columns>
            <asp:BoundColumn DataField="WebApiId" Visible="false"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="API Provider">
                <ItemTemplate>
                    <a href='#' onclick="OpenSetting('<%# "frmAPISetting.aspx?WebApiID=" + Eval("WebApiId").ToString() %>');">
                        <%# Eval("vcProviderName") %></a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateColumn>
            <%--          <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblFirstFldName" runat="server" Text='<%# Eval("vcFirstFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtFirstFldValue" runat="server" Text='<%# Eval("vcFirstFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblSecndFldName" runat="server" Text='<%# Eval("vcSecondFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtSecondFldValue" runat="server" Text='<%# Eval("vcSecondFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblThirdFldName" runat="server" Text='<%# Eval("vcThirdFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtThirdFldValue" runat="server" Text='<%# Eval("vcThirdFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblFourthFldName" runat="server" Text='<%# Eval("vcFourthFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtFourthFldValue" runat="server" Text='<%# Eval("vcFourthFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblFifthFldName" runat="server" Text='<%# Eval("vcFifthFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtFifthFldValue" runat="server" Text='<%# Eval("vcFifthFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblSixthFldName" runat="server" Text='<%# Eval("vcSixthFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtSixthFldValue" runat="server" Text='<%# Eval("vcSixthFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblSeventhFldName" runat="server" Text='<%# Eval("vcSeventhFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtSeventhFldValue" runat="server" Text='<%# Eval("vcSeventhFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblEighthFldName" runat="server" Text='<%# Eval("vcEighthFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtEighthFldValue" runat="server" Text='<%# Eval("vcEighthFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblNinthFldName" runat="server" Text='<%# Eval("vcNinthFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtNinthFldValue" runat="server" Text='<%# Eval("vcNinthFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblTenthFldName" runat="server" Text='<%# Eval("vcTenthFldName") %>'> </asp:Label>
                                :
                                <asp:TextBox Width="100" ID="txtTenthFldValue" runat="server" Text='<%# Eval("vcTenthFldValue") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
            <%--  <asp:TemplateColumn HeaderText="Enable">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTest" runat="server" Checked='<%# Eval("bitEnableAPI") %>' />
                            </ItemTemplate>
                             <ItemStyle HorizontalAlign=Center />
                        </asp:TemplateColumn>--%>
        </Columns>
    </asp:DataGrid>
</asp:Content>
