Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebNavigator
Imports System.Web.Caching
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Outlook

    Partial Public Class frmItemNavigation
        Inherits BACRMPage

        Private __strTreeView_Cache_Key As String = "__str_TreeView_Cache"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim dtTab As DataTable
                Dim intModuleIID As Integer
                Dim dtTable As DataTable
                Dim objCommon As New CCommon

                'BUG ID : 308 
                'MODIFICATION : To add data to cache for retrieving purpose whenever TreeView needs.
                '--------------- START BLOCK 1 ----------------------------------
                objCommon.ModuleID = -1
                objCommon.DomainID = -1
                objCommon.GroupID = Session("UserGroupID")

                If Cache.Item(__strTreeView_Cache_Key) Is Nothing Then
                    dtTable = Nothing
                    dtTable = objCommon.GetPageNavigationDTLs

                    'Cache.Insert(__strTreeView_Cache_Key, dtTable)
                Else

                    dtTable = CType(Cache.Item(__strTreeView_Cache_Key), DataTable)

                    If dtTable.Rows.Count <= 0 Then
                        dtTable = Nothing
                        dtTable = objCommon.GetPageNavigationDTLs
                    End If
                End If
                '---------------- END BLOCK 1---------------------------------

                intModuleIID = GetQueryStringVal("ModID")

                objCommon.ModuleID = intModuleIID
                objCommon.DomainID = Session("DomainId")

                '----------------------- START BLOCK 2 -----------------------------------------
                Dim dtTableCopy As New DataTable
                Dim drRow() As DataRow
                drRow = dtTable.Select("numModuleID = " & objCommon.ModuleID)
                If drRow.Count > 0 Then
                    dtTableCopy = drRow.CopyToDataTable()
                End If

                If intModuleIID = MODULEID.Accounting OrElse intModuleIID = MODULEID.Documents Then
                    dtTable = objCommon.GetPageNavigationDTLs
                Else
                    dtTable = dtTableCopy
                End If
                '------------------------ END BLOCK 2 ----------------------------------------

                If intModuleIID = MODULEID.Relationships Then

                    objCommon.TabId = 7
                    dtTable = objCommon.GetTreeNodesForRelationship(1)
                    dtTab = Session("DefaultTab")

                    If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                        RadItemNavigation.DataTextField = "vcNodeName"
                        RadItemNavigation.DataValueField = "ID"
                        RadItemNavigation.DataNavigateUrlField = "vcNavURL"
                        RadItemNavigation.DataFieldID = "ID"
                        RadItemNavigation.DataFieldParentID = "numParentID"
                        RadItemNavigation.DataSource = dtTable
                        RadItemNavigation.DataBind()
                        RadItemNavigation.ExpandAllNodes()

                        For Each node As RadTreeNode In RadItemNavigation.GetAllNodes()
                            node.Target = "rightFrame"

                            If node.Value = "1" Then
                                node.Font.Bold = True

                            Else
                                node.Font.Italic = True
                            End If

                            If node.Level = 1 Then
                                node.ImageUrl = "../images/tf_note.gif"
                            End If

                            Select Case node.Text
                                Case "Leads" : node.Text = dtTab.Rows(0).Item("vcLeadPlural").ToString
                                Case "My Leads" : node.Text = "My " & dtTab.Rows(0).Item("vcLeadPlural").ToString
                                Case "WebLeads" : node.Text = "Web" & dtTab.Rows(0).Item("vcLeadPlural").ToString
                                Case "Public Leads" : node.Text = "Public" & dtTab.Rows(0).Item("vcLeadPlural").ToString
                                Case "Contacts" : node.Text = dtTab.Rows(0).Item("vcContactPlural").ToString
                                Case "Prospects" : node.Text = dtTab.Rows(0).Item("vcProspectPlural").ToString
                                Case "Accounts" : node.Text = dtTab.Rows(0).Item("vcAccountPlural").ToString
                            End Select
                        Next
                    End If
                Else
                    dtTable = objCommon.GetPageNavigationDTLs
                    Dim treeNodeA As RadTreeNode

                    For Each dr As DataRow In dtTable.Rows
                        'If intModuleIID = MODULEID.Items And dr("numPageNavID") = 173 Then
                        '    'Commented by :Sachin Sadhu ||Date :20thSept2014
                        '    'Purpose  :  Asset/Rental Items 
                        '    ' If CCommon.ToBool(Session("bitRentalItem")) = True AndAlso CCommon.ToLong(Session("RentalItemClass")) > 0 Then

                        '    treeNodeA = New RadTreeNode
                        '    treeNodeA.Target = "rightFrame"
                        '    treeNodeA.Text = "Rental Items"
                        '    'treeNodeA.Font.Bold = True
                        '    treeNodeA.Font.Italic = True
                        '    treeNodeA.NavigateUrl = "../Items/frmItemList.aspx?Page=Rental Items&ItemGroup=0"
                        '    RadItemNavigation.FindNodeByValue("62").Nodes.Add(treeNodeA)
                        '    '  End If
                        'End If

                'If intModuleIID = MODULEID.Items And dr("numPageNavID") = 3 Then 'Remove Assembly Items
                '    Continue For
                'End If

                treeNodeA = New RadTreeNode
                treeNodeA.Target = "rightFrame"
                treeNodeA.Value = dr("numPageNavID").ToString
                treeNodeA.NavigateUrl = dr("vcNavURL")

                If intModuleIID = MODULEID.Relationships Then ' If intModuleIID = 2 Then
                    If dr("numParentID").ToString = "0" Then
                        treeNodeA.Font.Bold = True
                    ElseIf dr("numParentID") = "6" Then
                        treeNodeA.ImageUrl = "../images/tf_note.gif"
                        'treeNodeA.Font.Bold = True
                    Else
                        'treeNodeA.Font.Bold = True
                        treeNodeA.Font.Italic = True
                    End If
                ElseIf intModuleIID = MODULEID.Documents Then

                    If dr("numParentID").ToString = "0" Then
                        treeNodeA.ImageUrl = "../images/clearpixel.gif"
                        treeNodeA.SelectedImageUrl = "../images/clearpixel.gif"
                        treeNodeA.Font.Bold = True
                        treeNodeA.Expanded = True
                    Else
                        If Not IsDBNull(dr("vcImageURL")) Then
                            treeNodeA.ImageUrl = dr("vcImageURL")
                            treeNodeA.SelectedImageUrl = dr("vcImageURL")
                            If dr("numParentID").ToString = "0" Or dr("numParentID").ToString = "133" Then
                                'treeNodeA.Font.Bold = True
                                treeNodeA.Expanded = True
                            Else
                                'treeNodeA.Font.Bold = True
                                treeNodeA.Font.Italic = True
                                treeNodeA.Expanded = False
                            End If
                        Else
                            'treeNodeA.Font.Bold = True
                            treeNodeA.Font.Italic = True
                            treeNodeA.Expanded = False
                        End If
                    End If
                Else
                    If dr("numParentID").ToString = "0" Then
                        If Not IsDBNull(dr("vcImageURL")) Then
                            treeNodeA.ImageUrl = dr("vcImageURL")
                            treeNodeA.SelectedImageUrl = dr("vcImageURL")
                            treeNodeA.Font.Bold = True
                        Else
                            treeNodeA.ImageUrl = "../images/clearpixel.gif"
                            treeNodeA.SelectedImageUrl = "../images/clearpixel.gif"
                            'treeNodeA.Font.Bold = True
                        End If
                    Else
                        If Not IsDBNull(dr("vcImageURL")) Then
                            treeNodeA.ImageUrl = dr("vcImageURL")
                            treeNodeA.SelectedImageUrl = dr("vcImageURL")
                            'treeNodeA.Font.Bold = True
                        Else
                            'treeNodeA.Font.Bold = True
                            treeNodeA.Font.Italic = True
                        End If
                    End If
                End If
                If intModuleIID = MODULEID.Relationships AndAlso dtTable.Rows.Count > 0 Then
                    'If intModuleIID = 2 Then
                    Select Case dr("vcPageNavName")
                        Case "Leads" : treeNodeA.Text = dtTab.Rows(0).Item("vcLeadPlural").ToString
                        Case "My Leads" : treeNodeA.Text = "My " & dtTab.Rows(0).Item("vcLeadPlural").ToString
                        Case "WebLeads" : treeNodeA.Text = "Web" & dtTab.Rows(0).Item("vcLeadPlural").ToString
                        Case "Public Leads" : treeNodeA.Text = "Public" & dtTab.Rows(0).Item("vcLeadPlural").ToString
                        Case "Contacts" : treeNodeA.Text = dtTab.Rows(0).Item("vcContactPlural").ToString
                        Case "Prospects" : treeNodeA.Text = dtTab.Rows(0).Item("vcProspectPlural").ToString
                        Case "Accounts" : treeNodeA.Text = dtTab.Rows(0).Item("vcAccountPlural").ToString
                        Case Else
                            treeNodeA.Text = dr("vcPageNavName")
                    End Select
                Else : treeNodeA.Text = dr("vcPageNavName")
                End If

                If dr("numParentID") = 0 Then
                    RadItemNavigation.Nodes.Add(treeNodeA)
                    If intModuleIID <> MODULEID.Documents Then
                        RadItemNavigation.ExpandAllNodes()
                    End If
                Else
                    If Not (intModuleIID = MODULEID.Accounting And dr("vcPageNavName") = "Budgeting") Then 'hide budgeting node for time being 
                        If Not RadItemNavigation.FindNodeByValue(dr("numParentID").ToString) Is Nothing Then
                            RadItemNavigation.FindNodeByValue(dr("numParentID").ToString).Nodes.Add(treeNodeA)
                        End If
                    End If
                End If
                    Next

                    'If intModuleIID = 2 Then
                    If intModuleIID = MODULEID.Relationships AndAlso dtTable.Rows.Count > 0 Then
                        Dim objTab As New Tabs
                        Dim dtTabDataMenu As DataTable
                        objTab.ListId = 5
                        objTab.DomainID = Session("DomainID")
                        objTab.mode = True
                        dtTabDataMenu = objTab.GetTabMenuItem()
                        For Each dr As DataRow In dtTabDataMenu.Rows

                            treeNodeA = New RadTreeNode
                            treeNodeA.Target = "rightFrame"
                            treeNodeA.Text = dr("vcData")
                            treeNodeA.NavigateUrl = dr("vcNavURL")
                            treeNodeA.Value = dr("numListItemID").ToString

                            If dr("numListItemID") <> 0 Then
                                treeNodeA.ImageUrl = "../images/tf_note.gif"
                                'treeNodeA.Font.Bold = True
                            Else
                                'treeNodeA.Font.Bold = True
                                treeNodeA.Font.Italic = True
                            End If

                            If Not RadItemNavigation.FindNodeByValue(dr("parentid").ToString) Is Nothing Then
                                RadItemNavigation.FindNodeByValue(dr("parentid").ToString).Nodes.Add(treeNodeA)
                            End If

                        Next

                        RadItemNavigation.ExpandAllNodes()
                        If Not RadItemNavigation.FindNodeByValue("7") Is Nothing Then
                            RadItemNavigation.FindNodeByValue("7").Expanded = True
                        End If
                    ElseIf intModuleIID = MODULEID.Accounting AndAlso dtTable.Rows.Count > 0 Then

                    ElseIf intModuleIID = MODULEID.Items AndAlso dtTable.Rows.Count > 0 AndAlso RadItemNavigation.FindNodeByValue("70") IsNot Nothing Then
                        Dim objItems As New CItems
                        Dim dtItemGroups As DataTable
                        objItems.DomainID = Session("DomainID")
                        dtItemGroups = objItems.GetItemGroups.Tables(0)
                        For Each dr As DataRow In dtItemGroups.Rows

                            treeNodeA = New RadTreeNode
                            treeNodeA.Target = "rightFrame"
                            treeNodeA.Text = dr("vcItemGroup")
                            treeNodeA.NavigateUrl = "../Items/frmItemList.aspx?Page=All Items&ItemGroup=" & dr("numItemGroupID")
                            treeNodeA.Value = dr("numItemGroupID").ToString
                            RadItemNavigation.FindNodeByValue("70").Nodes.Add(treeNodeA)
                        Next
                    ElseIf intModuleIID = MODULEID.Pre_Defined_Reports Then
                        treeNodeA = New RadTreeNode
                        treeNodeA.Target = "rightFrame"
                        treeNodeA.Text = "Old Custom Reports"
                        treeNodeA.NavigateUrl = "../reports/frmCustomRptList.aspx"
                        RadItemNavigation.FindNodeByValue("22").Nodes.Add(treeNodeA)
                    End If
                End If
                If intModuleIID = MODULEID.Opportunities Or intModuleIID = MODULEID.Marketing Or intModuleIID = MODULEID.Administration Then RadItemNavigation.ExpandAllNodes()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace