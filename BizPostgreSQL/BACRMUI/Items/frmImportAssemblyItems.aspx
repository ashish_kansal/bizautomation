﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImportAssemblyItems.aspx.vb"
    Inherits=".frmImportAssemblyItems" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Import Kit and Assembly Children</title>
    <script language="javascript">
        function setDllValues() {
            if (document.getElementById('tbldtls').rows.length < 2) {
                alert("Please Upload the file first")
                return false;
            }
            var intCnt;
            inCnt = document.Form1.txtDllValue.value;
            document.Form1.txtDllValue.value = "";
            var dllValue;
            dllValue = "";
            for (var x = 0; x <= inCnt; x++) {
                if (dllValue != "") {
                    dllValue = dllValue + "," + document.getElementById('ddlDestination' + x).value;
                }
                else {
                    dllValue = document.getElementById('ddlDestination' + x).value;
                }
            }
            document.Form1.txtDllValue.value = dllValue;
        }

        function checkFileExt() {

            if (document.Form1.txtFile.value.length == 0) {
                alert("Plz Select File");
                return false;
            }
            else {

                var data = document.Form1.txtFile.value

                var extension = data.substr(data.lastIndexOf('.'));

                if (extension == '.csv' || extension == '.CSV') {

                    return true;
                }
                else {
                    alert("Plz Select a .csv File");
                    return false;
                }

            }
        }

        function displayPBar() {

            if (document.getElementById('tbldtls').rows.length <= 2) {
                alert("please click on display records before importing")
                return false;
            }
            if (document.Form1.pgBar.style.display == "none")
                document.Form1.pgBar.style.display = "";
            else
                document.Form1.pgBar.style.display = "none";
        }
      
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Import Kit and Assembly Children&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="middle">
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </td>
        </tr>
    </table>
    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" Width="100%" runat="server"
        BorderColor="black" GridLines="None" Height="400" BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <br>
                <table width="100%" cellspacing="4">
                    <tr>
                        <td class="normal1" align="right">
                            Step&nbsp;1
                        </td>
                        <td class="normal1" colspan="2">
                            <a href="ImportAssemblyItems.csv">Download CSV</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 2
                        </td>
                        <td class="normal1" colspan="2">
                            Source&nbsp;
                            <input runat="server" id="txtFile" type="file" class="signup">
                            &nbsp;
                            <asp:Button runat="server" ID="btnUpload" Text="Upload" CssClass="button"></asp:Button>
                            &nbsp;(Upload the CSV file to view before you save it to database)
                        </td>
                    </tr>
                    <%--    <tr id="trImport" runat="server" visible="false">
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnImportForHitech" runat="server" Text="Hitech Import" CssClass="button"
                                Visible="false" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="normal1" align="right">
                            Step 3
                        </td>
                        <td colspan="2" class="normal1">
                            Mapping
                            <asp:TextBox ID="txtDllValue" runat="server" Height="16px" Style="display: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2" class="normal1">
                            <div style="height: 250px; width: 1000px; overflow: scroll;">
                                <asp:Table ID="tbldtls" runat="server" Width="100%" GridLines="none" BorderWidth="0"
                                    CellSpacing="1">
                                </asp:Table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 4
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:Button ID="btnDisplay" runat="server" Text="Display Records" CssClass="button"
                                Width="100"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 5
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:Button ID="btnImports" runat="server" Text="Import Records to database" CssClass="button"
                                Width="170px"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
