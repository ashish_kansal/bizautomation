﻿Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Accounting
Partial Public Class frmCularisSetting
    Inherits BACRM.UserInterface.BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadDetails()
                btnSave.Attributes.Add("onclick", "return Save()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadDetails()
        Try
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            ddlWareHouse.DataValueField = "numWareHouseID"
            ddlWareHouse.DataTextField = "vcWareHouse"
            ddlWareHouse.DataSource = objItems.GetWareHouses
            ddlWareHouse.DataBind()
            ddlWareHouse.Items.Insert(0, "--Select One--")
            ddlWareHouse.Items.FindByText("--Select One--").Value = 0
            Dim objUser As New UserGroups
            objUser.DomainId = Session("DomainID")

            objUser.SortCharacter = "0"
            objUser.CurrentPage = 1
            objUser.PageSize = 100
            objUser.BitStatus = 1
            ddlUsers.DataValueField = "numUserID"
            ddlUsers.DataTextField = "Name"
            ddlUsers.DataSource = objUser.GetUserList().Tables(0)
            ddlUsers.DataBind()
            ddlUsers.Items.Insert(0, "--Select One--")
            ddlUsers.Items.FindByText("--Select One--").Value = 0

            LoadChartType()

            Dim objAPI As New WebAPI
            objAPI.WebApiId = GetQueryStringVal(Request.QueryString("enc"), "WebApiID")
            Dim dtAPI As DataTable = objAPI.GetWebApi()
            If Not ddlWareHouse.Items.FindByValue(dtAPI.Rows(0).Item("vcFifthFldValue")) Is Nothing Then
                ddlWareHouse.Items.FindByValue(dtAPI.Rows(0).Item("vcFifthFldValue")).Selected = True
            End If
            If Not ddlUsers.Items.FindByValue(dtAPI.Rows(0).Item("vcSixthFldValue")) Is Nothing Then
                ddlUsers.Items.FindByValue(dtAPI.Rows(0).Item("vcSixthFldValue")).Selected = True
            End If
            If Not ddlCOGS.Items.FindByValue(dtAPI.Rows(0).Item("vcSeventhFldValue")) Is Nothing Then
                ddlCOGS.Items.FindByValue(dtAPI.Rows(0).Item("vcSeventhFldValue")).Selected = True
            End If
            If Not ddlAssetAccount.Items.FindByValue(dtAPI.Rows(0).Item("vcEighthFldValue")) Is Nothing Then
                ddlAssetAccount.Items.FindByValue(dtAPI.Rows(0).Item("vcEighthFldValue")).Selected = True
            End If
            If Not ddlIncomeAccount.Items.FindByValue(dtAPI.Rows(0).Item("vcNinthFldValue")) Is Nothing Then
                ddlIncomeAccount.Items.FindByValue(dtAPI.Rows(0).Item("vcNinthFldValue")).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objAPI As New WebAPI
            objAPI.WebApiId = 1
            objAPI.DomainId = Session("DomainID")
            objAPI.UserID = ddlUsers.SelectedValue
            objAPI.WareHouseID = ddlWareHouse.SelectedValue
            objAPI.COGSChartAcntId = ddlCOGS.SelectedValue
            objAPI.IncomeChartAcntId = ddlIncomeAccount.SelectedValue
            objAPI.AssetChartAcntId = ddlAssetAccount.SelectedValue
            objAPI.SaveCularisSettings()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadChartType()
        Try
            Dim objJournalEntry As New JournalEntry
            Dim dtChartAcntDetails As DataTable
            objJournalEntry.ParentRootNode = 1
            objJournalEntry.DomainId = Session("DomainId")
            dtChartAcntDetails = objJournalEntry.GetChartOfAccountsDetailsForItems

            ddlCOGS.DataSource = dtChartAcntDetails
            ddlCOGS.DataTextField = "vcCategoryName"
            ddlCOGS.DataValueField = "numChartOfAcntID"
            ddlCOGS.DataBind()
            ddlCOGS.Items.Insert(0, New ListItem("--Select One --", "0"))

            ddlAssetAccount.DataSource = dtChartAcntDetails
            ddlAssetAccount.DataTextField = "vcCategoryName"
            ddlAssetAccount.DataValueField = "numChartOfAcntID"
            ddlAssetAccount.DataBind()
            ddlAssetAccount.Items.Insert(0, New ListItem("--Select One --", "0"))

            ddlIncomeAccount.DataSource = dtChartAcntDetails
            ddlIncomeAccount.DataTextField = "vcCategoryName"
            ddlIncomeAccount.DataValueField = "numChartOfAcntID"
            ddlIncomeAccount.DataBind()
            ddlIncomeAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class