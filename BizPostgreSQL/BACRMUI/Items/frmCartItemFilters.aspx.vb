﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Partial Public Class frmCartItemFilters
    Inherits BACRMPage

    Dim objCartItems As New CartItemsFilters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "return Save();")
            btnClose.Attributes.Add("onclick", "javascript:window.close();return false;")
            btnDelete.Attributes.Add("onclick", "return DeleteRecord();")

            If Not IsPostBack Then
                BindDropDowns()
                BindGrid()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindDropDowns()
        Try
            Dim dtTable As DataTable
            Dim objSite As New Sites
            objSite.DomainID = CCommon.ToLong(Session("DomainID"))
            dtTable = objSite.GetSites()

            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = dtTable
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "--Select One--")
            ddlSites.Items.FindByText("--Select One--").Value = 0

            Dim objFormWizard As New FormConfigWizard
            Dim dtFields As New DataTable
            objFormWizard.DomainID = Session("DomainID")
            objFormWizard.FormID = 77
            objFormWizard.AuthenticationGroupID = 0
            objFormWizard.BizDocTemplateID = 0
            dtFields = objFormWizard.getFieldList("")
            If dtFields IsNot Nothing AndAlso dtFields.Rows.Count > 0 Then
                ddlAvailableFields.DataValueField = "vcFieldAndType"
                ddlAvailableFields.DataTextField = "vcFormFieldName"
                ddlAvailableFields.DataSource = dtFields
                ddlAvailableFields.DataBind()
            End If
            ddlAvailableFields.Items.Insert(0, "--Select One--")
            ddlAvailableFields.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindGrid()
        Try
            If objCartItems Is Nothing Then objCartItems = New CartItemsFilters
            Dim ds As DataSet
            objCartItems.FormID = 77
            objCartItems.DomainID = Session("DomainId")
            objCartItems.SiteID = CCommon.ToLong(ddlSites.SelectedValue)
            ds = objCartItems.GetDycCartFilters()
            gvCartFilters.DataSource = ds
            gvCartFilters.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objCartItems Is Nothing Then objCartItems = New CartItemsFilters

            Dim strItem As String = CCommon.ToString(ddlAvailableFields.SelectedValue)
            Dim FieldID As Long = 0
            Dim CustomField As Boolean = False
            Dim intOrder As Integer = 0

            FieldID = If(strItem.Split("~").Length > 0, CCommon.ToLong(strItem.Split("~")(0)), 0)
            CustomField = If(strItem.Split("~").Length > 1, CCommon.ToBool(CCommon.ToLong(strItem.Split("~")(1))), False)
            intOrder = If(strItem.Split("~").Length > 2, CCommon.ToInteger(strItem.Split("~")(2)), 0)

            objCartItems.FieldID = FieldID
            objCartItems.FormID = 77
            objCartItems.CustomField = CustomField
            objCartItems.FilterType = 1
            objCartItems.SiteID = CCommon.ToLong(ddlSites.SelectedValue)
            objCartItems.Order = intOrder
            objCartItems.DomainID = Session("DomainId")
            objCartItems.ManageDycCartFilters()

            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If objCartItems Is Nothing Then objCartItems = New CartItemsFilters

            objCartItems.FormID = 77
            objCartItems.DomainID = Session("DomainId")

            Dim hdnFieldID As New HiddenField
            Dim hdnSiteID As New HiddenField
            Dim chkSelect As New CheckBox
            For Each gridRow As GridViewRow In gvCartFilters.Rows

                chkSelect = gridRow.FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    hdnFieldID = gridRow.FindControl("hdnFieldID")
                    hdnSiteID = gridRow.FindControl("hdnSiteID")

                    objCartItems.FieldID = CCommon.ToLong(hdnFieldID.Value)
                    objCartItems.SiteID = CCommon.ToLong(hdnSiteID.Value)
                    objCartItems.DeleteDycCartFilters()
                End If
            Next

            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class