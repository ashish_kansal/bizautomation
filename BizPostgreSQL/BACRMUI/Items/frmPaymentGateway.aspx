<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPaymentGateway.aspx.vb"
    Inherits=".frmPaymentGateway" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Payment Gateway</title>
    <script>
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });
        function pageLoaded() {
            $("#btnSave").click(function () {
                var i = 0;
                
                $("#<%=dgPaymentGateway.ClientID%> tr").each(function () {
                    var txtFirstFldValue;
                    var txtSecondFldValue;
                    var txtThirdFldValue;
                    var rbs;
                    txtFirstFldValue = $(this).find("[id$=txtFirstFldValue]");
                    txtSecondFldValue = $(this).find("[id$=txtSecondFldValue]");
                    txtThirdFldValue = $(this).find("[id$=txtThirdFldValue]");
                    rbs = $(this).find("[id$=chktest]");
                    if (rbs.is(':checked') == true) {
                        if (txtFirstFldValue.val() == "") {
                            alert("Please Enter details for default payment gateway.");
                            i = 1;
                        }
                    }
                })

                if (i == 1) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
    </script>
    <script type = "text/javascript">
        function RadioCheck(rb) {
            var gv = document.getElementById("<%=dgPaymentGateway.ClientID%>");
         var rbs = gv.getElementsByTagName("input");

         var row = rb.parentNode.parentNode;
         for (var i = 0; i < rbs.length; i++) {
             if (rbs[i].type == "radio") {
                 if (rbs[i].checked && rbs[i] != rb) {
                     rbs[i].checked = false;
                     break;
                 }
             }
         }
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:UpdatePanel ID="updatesave" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSave" />
                    </Triggers>
                </asp:UpdatePanel>
                
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Payment Gateway&nbsp;<a href="#" onclick="return OpenHelpPopUp('Items/frmPaymentGateway.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Site:</label>
                        <asp:DropDownList ID="ddlSites" CssClass="form-control" runat="server" AutoPostBack="True" ></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Default Payment Gateway:<asp:Label ID="Label57" Text="[?]" CssClass="tip" runat="server" ToolTip="When you want to process payment for BizDoc(i.e Invoice) thorugh credit card Default payment gateway will be used for capturing amount from credit card. WebStore also uses default payment gateway to process payments for orders placed online" /></label>
                        <asp:DropDownList ID="ddlPaymentGateway" CssClass="form-control" runat="server" AutoPostBack="True" ></asp:DropDownList>
                    </div>
                </div>
                <asp:DataGrid ID="dgPaymentGateway" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-striped" Width="100%" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn DataField="intPaymentGateWay" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Gateway" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <div class="form-inline">
                                    <%--<asp:RadioButton ID="chktest" onclick = "RadioCheck(this);" runat="server" GroupName="gateway" Checked='<%# DataBinder.Eval(Container,"DataItem.bitTest") %>' />--%>
                                    <label> 
                                        <asp:Label ID="lblGateway" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcGateWayName") %>'> </asp:Label></label>
                                    <asp:Label ID="lblToolTip" Text="[?]" CssClass="tip" runat="server" ToolTip='<%# DataBinder.Eval(Container,"DataItem.vcToolTip") %>' Visible='<%# Boolean.Parse(IIf(DataBinder.Eval(Container,"DataItem.vcToolTip")="","false","true")) %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblFirstFldName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcFirstFldName")%>'> </asp:Label></label>
                                    <asp:TextBox CssClass="form-control" ID="txtFirstFldValue" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcFirstFldValue") %>'></asp:TextBox>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblSecndFldName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcSecndFldName")%>'> </asp:Label></label>
                                    <asp:TextBox CssClass="form-control" ID="txtSecondFldValue" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcSecndFldValue") %>'></asp:TextBox>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <div class="form-group">
                                    <label>
                                        <asp:Label ID="lblThirdFldName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcThirdFldName") %>'> </asp:Label></label>
                                    <asp:TextBox CssClass="form-control" ID="txtThirdFldValue" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcThirdFldValue") %>'></asp:TextBox>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Test">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTest" runat="server" Checked='<%# DataBinder.Eval(Container,"DataItem.bitTest") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
