<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmWareHouseDetails.aspx.vb"
    MasterPageFile="~/common/ECommerceMenuMaster.Master" Inherits="BACRM.UserInterface.Item.frmWareHouseDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>WareHouse Details</title>
    <script type="text/javascript">
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function Save() {

            if (document.getElementById('ddlWareHouse').value == 0) {
                alert("Select Warehouse");
                document.getElementById('ddlWareHouse').focus()
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="ltrMessage" Visible="false" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    WareHouse Mapping
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel runat="server" ID="up1" UpdateMode="Always">
        <ContentTemplate>
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Country:</label>
                            <asp:DropDownList ID="ddlCountry" Width="200" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>State:</label>
                            <asp:DropDownList ID="ddlState" Width="200" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>WareHouse: <span style="color:red">*</span></label>
                            <asp:DropDownList runat="server" ID="ddlWareHouse" CssClass="form-control" Width="200"></asp:DropDownList>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>From zipcode:</label>
                            <asp:TextBox runat="server" ID="txtFromPin" CssClass="form-control" Width="80"></asp:TextBox>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>To zipcode:</label>
                            <asp:TextBox runat="server" ID="txtToPin" CssClass="form-control" Width="80"></asp:TextBox>
                        </div>
                        <asp:LinkButton runat="server" ID="btnSave" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgwarehouse" runat="server" CssClass="table table-bordered table-striped" Width="100%" AutoGenerateColumns="False" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn DataField="numWareHouseDetailID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
                                <asp:BoundColumn DataField="state" HeaderText="State"></asp:BoundColumn>
                                <asp:BoundColumn DataField="WareHouse" HeaderText="Warehouse"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numFromPinCode" HeaderText="From zipcode"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numToPinCode" HeaderText="To zipcode"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" Text="X" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:TextBox runat="server" Visible="false" ID="txtWareHouseDetailID" Text="0"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
