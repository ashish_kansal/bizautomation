<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAssignCategories.aspx.vb"
    Inherits="BACRM.UserInterface.Items.Items_frmAssignCategories" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Assign Categories</title>
    <script type="text/javascript">
        function OpenHier(categoryProfileID) {
            window.open("../Items/frmHierCategoryItems.aspx?numCategoryProfileID=" + categoryProfileID)
            return false;
        }
        function CheckAll() {
            if (document.form1.chkAll.checked == true) {
                for (i = 0; i < document.form1.elements.length; i++) {

                    if (document.form1.elements[i].type == 'checkbox') {
                        document.form1.elements[i].checked = true;
                    }
                }
            }
            else {

                for (i = 0; i < document.form1.elements.length; i++) {

                    if (document.form1.elements[i].type == 'checkbox') {
                        document.form1.elements[i].checked = false;
                    }
                }
            }

        }
        function Add() {
            if ($("[id$=ddlCategory]").val() == 0) {
                alert("Select a Category");
                $("[id$=ddlCategory]").focus();
                return false;
            }
            if ($find('radItem').get_value() == "") {
                alert("Select Item")
                return false;
            }
        }

        function Validate() {
            if ($("[id$=ddlCategory]").val() == 0) {
                alert("Select a Category");
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnHier" runat="server" CssClass="btn btn-primary"><i class="fa fa-list-ol"></i>&nbsp;&nbsp;Open Hierarchy</asp:LinkButton>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return Validate();"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Validate();"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Configure Items And Categories
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <div class="form-inline">
        <label>Items per page:</label>
        <asp:DropDownList ID="ddlPageSize" runat="server" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="20" Value="20" Selected="True"></asp:ListItem>
            <asp:ListItem Text="50" Value="50"></asp:ListItem>
            <asp:ListItem Text="100" Value="100"></asp:ListItem>
            <asp:ListItem Text="150" Value="150"></asp:ListItem>
        </asp:DropDownList>
    </div>


</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="form-inline">
                    <label>Category:</label>
                    <asp:DropDownList ID="ddlCategory" runat="server" Width="200" AutoPostBack="true" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="form-inline">
                    <label>
                        <asp:RadioButton ID="radOneItemAtaTime" GroupName="rad1" runat="server" Text="Add One Item at a Time" />
                    </label>
                    <div class="input-group">
                        <telerik:RadComboBox ID="radItem" runat="server" Width="200" DropDownWidth="200px"
                            EnableScreenBoundaryDetection="true" EnableLoadOnDemand="true" AllowCustomText="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                        </telerik:RadComboBox>
                        <span class="input-group-btn">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="form-inline">
                    <label>
                        <asp:RadioButton ID="radItemFromGroups" AutoPostBack="true" GroupName="rad1" Text="Select Items from Item Groups" runat="server" /></label>
                    <asp:DropDownList ID="ddlItemGroup" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <div class="form-inline">
                    <label>
                        <asp:RadioButton ID="radALLItems" AutoPostBack="true" GroupName="rad1" Checked="true" Text="All Items" runat="server" />
                    </label>
                    <asp:CheckBox ID="chkAll" runat="server" Text="Check All" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="box box-default box-solid">
                <div class="box-header">
                    <h3 class="box-title">Added Items</h3>

                    <div class="pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
                            <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
                            <div class="form-inline">
                                <div style="font-weight: bold;">
                                    <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkFirst" runat="server">
											<<
                                    </asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkPrevious" runat="server">
											<
                                    </asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                                    <asp:TextBox ID="txtCurrrentPage" runat="server" CssClass="signup" Width="28px" AutoPostBack="True"
                                        MaxLength="5" Text="1"></asp:TextBox>
                                    <asp:Label ID="lblOf" runat="server">of</asp:Label>
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow">
											>
                                    </asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkLast" runat="server">
											>>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:CheckBoxList ID="chkAddedItems" CssClass="normal1" runat="server" RepeatColumns="2">
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="box box-default box-solid">
                <div class="box-header">
                    <h3 class="box-title">Available Items</h3>

                    <div class="pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:Label ID="lblCountAvailItems" runat="server" Visible="false"></asp:Label>
                            <div class="form-inline">
                                <div style="font-weight: bold;">
                                    <asp:Label ID="lblNextItems" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk2Items" runat="server">2</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk3Items" runat="server">3</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk4Items" runat="server">4</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnk5Items" runat="server">5</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkFirstItems" runat="server"><<</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkPreviousItems" runat="server"><</asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:Label ID="lblPageItems" runat="server">Page</asp:Label>
                                    <asp:TextBox ID="txtCurrrentPageItems" runat="server" CssClass="signup" Width="28px" AutoPostBack="True" MaxLength="5"></asp:TextBox>
                                    <asp:Label ID="lblOfItems" runat="server">of</asp:Label>
                                    <asp:Label ID="lblTotalItems" runat="server"></asp:Label>
                                    <asp:Label ID="lblCheckedCOunt" runat="server" Visible="false"></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkNextItems" runat="server" CssClass="LinkArrow">></asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkLastItems" runat="server">>></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:CheckBoxList ID="chkItems" CssClass="normal1" runat="server" RepeatColumns="2">
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
