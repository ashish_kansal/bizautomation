<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmECommerceSettings.aspx.vb"
    Inherits=".frmECommerceSettings" MasterPageFile="~/common/ECommerceMenuMaster.Master" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>E-Commerce Settings</title>
    <script type="text/javascript">
        function OpenEditor(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenSettings(type, FormID) {
            window.open('../Items/frmConfItemList.aspx?FormID=' + FormID + '&type=' + type + '&SiteID=' + $('[id$=ddlDefaultSite]').val(), '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenSelectFilters() {
            window.open('../Items/frmCartItemFilters.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=600,scrollbars=yes,resizable=yes')
            return false;
        }
        function manageItemVisibility() {
            if ($("[id$=ddlDefaultSite]") != null && $("[id$=ddlDefaultSite]").val() > 0) {
                window.open('../ECommerce/frmManageItemVisibility.aspx?numSiteID=' + $("[id$=ddlDefaultSite]").val(), '', 'toolbar=no,titlebar=no,top=200,left=200,width=900,height=500,scrollbars=yes,resizable=yes')
            } else {
                $("[id$=ddlDefaultSite]").focus();
                alert("Select site");
            }

            return false;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            InitializeValidation();
        }
    </script>
    <style type="text/css">
        .Feature {
            font-weight: bolder;
            font-family: Segoe UI,Arial,Tamoha;
            text-decoration: underline;
            display: block;
        }

        .margin-bottom-10 {
            margin-bottom: 10px;
        }

        .col1 label.col-md-4 {
            padding-left: 14px;
            padding-right: 1px;
        }

        .col1 a {
            text-decoration: underline;
        }
         .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    e-Commerce Settings&nbsp;<a href="#" onclick="return OpenHelpPopUp('Items/frmECommerceSettings.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row col1">
        <div class="col-md-5">
            <div class="row">
                <label class="col-md-4">Select Site&nbsp;&nbsp;<asp:Label ID="lblDefaultSite" Text="[?]" CssClass="tip" runat="server" ToolTip="Select site you want to apply settings to" /></label>
                <div class="col-md-8">
                    <asp:DropDownList ID="ddlDefaultSite" runat="server" CssClass="form-control" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="margin-bottom-10"></div>
            <div class="row">
                <div class="form-group">
                    <label class="col-md-4">Default&nbsp;<asp:Label ID="lblDynamicToolTip" Text="[?]" CssClass="tip" runat="server" ToolTip=""></asp:Label></label>
                    <div class="col-md-4" style="padding-right: 2px;">
                        <asp:DropDownList ID="ddlDefaultType" runat="server" CssClass="form-control" AutoPostBack="true">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">Warehouse</asp:ListItem>
                            <asp:ListItem Value="2">Class (New Customers from site)</asp:ListItem>
                            <asp:ListItem Value="3">Relationship (New Customers from site</asp:ListItem>
                            <asp:ListItem Value="4">Profile (New Customers from site)</asp:ListItem>
                            <asp:ListItem Value="5">Pre-login Price</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4" style="padding-left: 2px;">
                        <asp:DropDownList ID="ddlDefaultValues" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="margin-bottom-10"></div>

            <div class="row">
                <label class="col-md-4">Warehouse Availability</label>
                <div class="col-md-8">
                    <asp:DropDownList ID="ddlWarehouseAvailability" runat="server" CssClass="form-control">
                        <asp:ListItem Text="Display all warehouse names" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Display only warehouse names containing a qty of 1 or >" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Display sum of available qty of all warehouses" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="margin-bottom-10"></div>
            <div class="row">
                <label class="col-md-4">Only Authorize Credit-Card on Checkout</label>
                <div class="col-md-4" style="padding-top: 6px;">
                    <asp:CheckBox runat="server" ID="chkAuthCreditCard" />
                    &nbsp; on order status to &nbsp;
                </div>

                <div class="col-md-4" style="padding-left: 0px;">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="margin-bottom-10"></div>
            <div class="row">
                <label class="col-md-4">Thank You Rewrite URL&nbsp;<asp:Label ID="Label4" Text="[?]" CssClass="tip" runat="server" ToolTip="Make sure you have configured User Friendly URL of what you have typed in textbox in Design -> User Friendly URL"></asp:Label></label>
                <div class="col-md-8">
                    <asp:TextBox ID="txtThankYouRewriteUrl" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
            </div>
             <div class="margin-bottom-10"></div>
            <div class="row">
                <label class="col-md-4">Page Size for Shopping Cart&nbsp;<asp:Label ID="lblPageSizeinfo" Text="[?]" CssClass="tip" runat="server" ToolTip="For e.g. Page Size stands for number of records to view in single page."></asp:Label></label>
                <div class="col-md-8">
                <asp:TextBox ID="txtPageSize" CssClass="form-control" runat="server" onkeypress="CheckNumber(2,event)"></asp:TextBox>
                </div>
            </div>
            <div class="margin-bottom-10"></div>
            <div class="row">
                <label class="col-md-4">Page Size Variation Count&nbsp;&nbsp;<asp:Label ID="lblPageVariantinfo" Text="[?]" CssClass="tip" runat="server" ToolTip="For e.g. If user enters Page Size of 15 and Page Size count of 6, then values of Page Size will be 15,30,45,60,75,90. Page Size stands for number of records to view in single page."></asp:Label></label>
                <div class="col-md-8">
                    <asp:TextBox ID="txtPageVariant" CssClass="form-control" runat="server" onkeypress="CheckNumber(2,event)"></asp:TextBox>
                </div>
            </div>
            <div class="margin-bottom-10"></div>
            <div class="row">
                <label class="col-md-4"></label>
                <div class="col-md-1" style="margin-top: 15px; padding-right: 0px;">
                    <asp:CheckBox ID="chkSkipStep2" runat="server" />
                </div>
                <div class="col-md-7">
                    When cart visitor clicks the �Checkout� button, skip �Step 2� (address) and take them directly to �Step 3� (Order Summary) while Sales inquiry is only selected as payment method
                </div>
            </div>
            <div class="margin-bottom-10"></div>
            <div class="row">
                <label class="col-md-4">Configure</label>
                <div class="col-md-8">
                    <asp:HyperLink Text="Simple Search Fields" runat="server" ID="hplSimpleSearch" onclick="return OpenSettings('0','30');"></asp:HyperLink>&nbsp;&nbsp;
                <asp:HyperLink Text="Advanced Search Fields" runat="server" ID="hplAdvSearch" NavigateUrl="~/admin/frmFormConfigWizard.aspx?FormID=30"></asp:HyperLink><br />
                    <asp:HyperLink Text="Select Cart item Filters" runat="server" ID="hplCartItemFilters" onclick="return OpenSelectFilters();"></asp:HyperLink>&nbsp;&nbsp;
                    <asp:HyperLink Text="Sorting Columns" runat="server" ID="hplSortingColumns" onclick="return OpenSettings('0','85');"></asp:HyperLink><br />

                    <asp:HyperLink Text="Items to hide based on user�s Relationship/Profile Organization" runat="server" ID="hplManageItemsVisibility" Style="cursor: pointer;" onclick="return manageItemVisibility();"></asp:HyperLink>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="form-group">
                <asp:CheckBox ID="chkCreditStatus" runat="server" />&nbsp;&nbsp;Enforce Credit Limits
            </div>
            <div class="form-group">
                <asp:CheckBox runat="server" ID="chkHideAddtoCart" />&nbsp;&nbsp;Hide �Add-to Cart� until log-in 
            </div>
            <div class="form-group">
                <asp:CheckBox runat="server" ID="chkHidePrice" />&nbsp;&nbsp;Hide prices until log-in
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkEmailStatus" runat="server" />&nbsp;&nbsp;After Signup send account information mail
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkEnableSorting" runat="server" />&nbsp;&nbsp;Enable Sorting              
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkSortPriceMode" runat="server" />&nbsp;&nbsp;Sort by Retail Price instead of Sale Price 
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkDisplayQtyAvailable" runat="server" />&nbsp;&nbsp;Display Qty Available  
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkShowPriceUsingPriceLevel" runat="server" />&nbsp;&nbsp;Display Price Levels            
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkQuantOnHand" runat="server" />&nbsp;&nbsp;Display Qty On-Hand 
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkCategorySettings" runat="server" />&nbsp;&nbsp;Display category if at least 1 item in category has qty-on-hand of 1 or > 
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkItemInStock" runat="server" />&nbsp;&nbsp;If item is in-stock show item �In Stock� 
            </div>
            <%-- <div class="form-group">
                <asp:CheckBox runat="server" ID="CheckBox10" />&nbsp;&nbsp;Send Account info after Sign-Up 
            </div>--%>
            <div class="form-group">
                <asp:CheckBox ID="chkPreUpSell" runat="server" />&nbsp;&nbsp;Turn on Pre-Upsell 
            </div>
            <div class="form-group">
                <asp:CheckBox ID="chkPostSell" runat="server" />&nbsp;&nbsp;Turn on Post-Upsell 
            </div>
            <div class="form-group">
                <asp:CheckBox runat="server" ID="chkShowPromoDetailsLink" />&nbsp;&nbsp;Item Promotions should display �Click for details� until user logs in 
            </div>
            <div class="form-group">
                <asp:CheckBox runat="server" ID="chkEnableElasticSearch" />&nbsp;&nbsp;Enable Elastic Search
            </div>
        </div>
        <div class="col-md-3">
            <table class="table table-responsive tblNoBorder">
                <tr>
                    <td colspan="2">Change Tab Labels / Click to hide tabs </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtSalesOrderTabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkSalesOrderTabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtSalesQuotesTabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkSalesQuotesTabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtItemPurchaseHistoryTabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkItemPurchaseHistoryTabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtItemsFrequentlyPurchasedTabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkItemsFrequentlyPurchasedTabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtOpenCasesTabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkOpenCasesTabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtOpenRMATabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkOpenRMATabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtSupportTabs" Style="width: 200px" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkSupportTabs" runat="server"></asp:CheckBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row" style="display: none">
        <%--Old Form--%>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Default Class <i>(New Customers from site)</i>&nbsp;&nbsp;<asp:Label ID="lblDefaultClass" Text="[?]" CssClass="tip" runat="server" ToolTip="Allow you to set default accounting class for new customers get registered on selected site." /></label>
                <asp:DropDownList ID="ddlDefaultClass" runat="server" CssClass="form-control" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Default Relationship <i>(New Customers from site)</i>&nbsp;&nbsp;<asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Any relationship created from Web store will create record using default relationship set here" /></label>
                <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Default Profile <i>(New Customers from site)</i>&nbsp;&nbsp;<asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Any relationship created from Web store will create record using default profile set here" /></label>
                <asp:DropDownList ID="ddlProfile" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>

        </div>
    </div>
    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Default Warehouse&nbsp;&nbsp;<asp:Label ID="Label3" Text="[?]" CssClass="tip" runat="server" ToolTip="Any order created from Web store will use default warehouse set here, 'Auto Select' will be applicable in following situation For items belonging to multiple warehouses, if default warehouse has 0 on-hand, pull from 1st warehouse on list with sufficient qty-on-hand to satisfy order." /></label>
                <div class="form-inline">
                    <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control" Width="180">
                    </asp:DropDownList>
                    <asp:CheckBox Text="Auto Select?" runat="server" ID="chkAutoSelectWarehouse" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Enforce Credit Limits </label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>
                    Set items to hide by organization relationship & profile&nbsp;&nbsp;<asp:Label ID="Label5" Text="[?]" CssClass="tip" runat="server" ToolTip="Use this tool for hiding items subscribed to a category on your site, based on the type of user visiting the site. For example, visitors that navigate the site (which haven�t yet logged in) might not see certain item, that you expose to other users that log in.

Any items belonging to the classification(s) added for selected organization and profile will be hidden from that user tier when they visit the site" /></label>
                <div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">

                <label>Turn on Pre Up-sell page</label>
                <div>
                    <asp:TextBox ID="txtPreSellPage" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label style="display: block">
                    Warehouse Availability<div class="pull-right">
                    </div>
                </label>

            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <label>Pre-login Price Default</label>
            <div>
                <asp:DropDownList ID="ddlPriceLevel" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Default Page Size For Shopping Cart&nbsp;&nbsp;
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Page Size Variation Count</label>

            </div>
        </div>
    </div>
    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Display Price Levels</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>BizCart Adavance Search fields</label>
                <div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Select Cart Item Filters</label>
                <div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Configure Sorting Columns</label>
                <div>
                </div>
            </div>

        </div>
    </div>
    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>BizCart Simple Search fields</label>
                <div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Sort By Retail Price instead of Sale Price</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Enable Sorting For Shopping Cart</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>If item is in stock, Show Item "In Stock"</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>After signup send account information mail</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Thank You Rewrite Url&nbsp;&nbsp;

            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Hide prices for until visitors log in</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">

                <label>Turn on Post Up-Sell</label>
                <div>
                    <asp:TextBox ID="txtPostUpSell" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="min-height: 50px; display: none">
        <div class="col-xs-6 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Display Quantity On Hand</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Hide Add to Cart for until visitors log in</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-group">
                <label>All item promotions should display �Click for details� link until user logs in.</label>
                <div class="form-control" style="border: none; padding-left: 0px;">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="min-height: 50px; display: none">
        <div class="col-xs-12">
            <div class="form-inline">
                <label>
                    <asp:Label ID="Label8" Text="[?]" CssClass="tip" runat="server" ToolTip="i.e Capture Amount Later from bizdoc" />&nbsp;&nbsp;Only Authorize Credit Card On Checkout</label>

            </div>
        </div>
    </div>
    <div class="row" style="min-height: 50px; display: none">
        <div class="col-xs-12">
            <div class="form-inline">
                <label>When cart visitor clicks the �Checkout� button, skip step 2 (Address) and take them directly to Step 3 (Order Summary) while Sales Inquiry is only selected as Payment Method.</label>

            </div>
        </div>
    </div>
    <div class="row" style="min-height: 50px; display: none">
        <div class="col-xs-12">
            <div class="form-inline">

                <label>Category Settings: Only display category if any item that belongs to category has Qty-On-Hand of 1 or more.</label>
            </div>
        </div>
    </div>


    <div class="box box-default box-solid">
        <div class="box-header">
            <h3 class="box-title">Google Checkout Credential</h3>

            <div class="pull-right text-aqua">
                Note: Only valid merchant id and merchant key values will be saved.
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-inline">
                        <label>Merchant ID:</label>
                        <asp:TextBox ID="txtGoogleMerchantID" Width="190px" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-inline">
                        <label>Merchant Key:</label>
                        <asp:TextBox ID="txtGoogleMerchantKey" Width="200px" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-inline">
                        <asp:CheckBox ID="cbSandbox" runat="server" />
                        <label>Is Testing Environment:</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default box-solid">
        <div class="box-header">
            <h3 class="box-title">PayPal Configuration</h3>

            <div class="pull-right">
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-inline">
                        <label>UserName:</label>
                        <asp:TextBox ID="txtPaypalUserName" Width="190px" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-inline">
                        <label>Password:</label>
                        <asp:TextBox ID="txtPaypalPassword" Width="200px" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-inline">
                        <label>Signature:</label>
                        <asp:TextBox ID="txtPaypalSignature" Width="200px" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-inline">
                        <asp:CheckBox ID="chkPaypalSandbox" runat="server" />
                        <label>Enable Paypal Sandbox:</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default box-solid">
        <div class="box-header">
            <h3 class="box-title">Payment Method Configuration</h3>

            <div class="pull-right">
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgPaymentGateway" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" Width="100%">
                            <Columns>
                                <asp:BoundColumn DataField="numPaymentMethodId" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaymentMethod" HeaderText="Payment Method"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Enable?">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEnable" runat="server" Checked='<%# DataBinder.Eval(Container,"DataItem.bitEnable") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        BizDoc
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlSalesOrder" Width="130" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSalesOrder_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select BizDoc--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderTemplate>
                                        Bizdoc Status<asp:Label ID="lblBizdocStatus" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected Bizdoc status when given payment method is used during checkout process" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlBizdocStatus" Width="130" CssClass="form-control">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Width="210px">
                                    <HeaderTemplate>
                                        Mirror Bizdoc Type<asp:Label ID="lblSalesOrderTemplate" Text="[?]" CssClass="tip" runat="server" ToolTip="Create selected sales order when given payment method is used during checkout process" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSalesOrder" Text='<%# DataBinder.Eval(Container,"DataItem.strType") %>' runat="server"></asp:Label>
                                        <asp:DropDownList runat="server" ID="ddlMirrorBizDocTemplate" Width="160" CssClass="form-control">
                                            <asp:ListItem Text="--Select Template--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderTemplate>
                                        Success Order Status<asp:Label ID="Label5" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected Success order status when given payment method is used during checkout process" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlOrderStatus" Width="130" CssClass="form-control">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        Failed Order Status<asp:Label ID="lblFailedOrderStatus" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected Failed order status with given payment method is used during checkout process" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlFailedOrderStatus" Width="130" CssClass="form-control">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        Record Owner<asp:Label ID="Label6" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected record owner when given payment method is used during checkout process" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlRecordOwner" Width="130" CssClass="form-control">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        Assign To<asp:Label ID="Label7" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected record as assign to when given payment method is used during checkout process" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlAssignTo" Width="130" CssClass="form-control">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
