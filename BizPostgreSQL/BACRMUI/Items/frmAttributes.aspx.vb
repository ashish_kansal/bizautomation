Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports System.Collections.Generic
Imports System.Xml.Linq


Partial Public Class frmAttributes
    Inherits BACRMPage

#Region "Member Variables"
    Dim lngItemGroup As Long
    Dim lngItemCode As Long
    Dim lngWarehouseItemID As Long
    Dim IsMatrix As Boolean = False
    Private dsAttributes As DataSet
    Dim SKU As String
    Dim Barcode As String
    Dim ListPrice As Double
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
        lngItemGroup = CCommon.ToLong(GetQueryStringVal("ItemGroup"))
        lngWarehouseItemID = CCommon.ToLong(GetQueryStringVal("numWarehouseItemID"))
        IsMatrix = CCommon.ToBool(GetQueryStringVal("IsMatrix"))
        SKU = GetQueryStringVal("SKU")
        Barcode = GetQueryStringVal("Barcode")
        ListPrice = CCommon.ToDouble(GetQueryStringVal("ListPrice"))
        Try
            lblErrorMessage.Text = ""
            If Not IsPostBack Then
                txtSKU.Enabled = Not IsMatrix
                txtUPC.Enabled = Not IsMatrix

                Dim objItems = New CItems
                objItems.ItemCode = lngItemCode
                objItems.byteMode = 1
                objItems.WareHouseItemID = lngWarehouseItemID
                Dim ds As DataSet = objItems.GetItemWareHouses()

                dsAttributes = New DataSet
                dsAttributes.Tables.Add(ds.Tables(2).Copy())

                hdnAttributes.Value = System.Web.HttpUtility.HtmlEncode(dsAttributes.GetXml())
                If Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 0 Then
                    AttributesControl(dsAttributes.Tables(0))
                End If

                txtSKU.Text = SKU
                txtUPC.Text = Barcode
                txtListPrice.Text = ListPrice
            Else
                'RELOAD ATTRIBUTES CONTROL IF MATRIX ITEM
                Dim str As String = System.Web.HttpUtility.HtmlDecode(hdnAttributes.Value)
                Dim reader As New System.IO.StringReader(str)
                dsAttributes = New DataSet
                dsAttributes.ReadXml(reader)

                If Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 0 Then
                    AttributesControl(dsAttributes.Tables(0))
                End If
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub AttributesControl(ByVal dtAttributes As DataTable, Optional ByVal isEdit As Boolean = 0, Optional ByVal dataRow As DataRow = Nothing)
        Try
            If Not dtAttributes Is Nothing AndAlso dtAttributes.Rows.Count > 0 Then
                plhWareHouseAttributes.Controls.Clear()

                Dim divRow As HtmlGenericControl
                Dim divColumn As HtmlGenericControl
                Dim dr As DataRow

                Dim i As Int32 = 0
                divRow = New HtmlGenericControl("div")
                divRow.Attributes.Add("id", "divAttributes")
                divRow.Attributes.Add("class", "row")

                Dim tabIndex As Int32 = 20

                For Each dr In dtAttributes.Rows
                    divColumn = New HtmlGenericControl("div")
                    divColumn.Attributes.Add("class", "col-xs-12 col-sm-6")

                    Dim objFormGroup As New HtmlGenericControl("div")
                    objFormGroup.Attributes.Add("class", "form-group")

                    Dim label As New HtmlGenericControl("label")
                    label.InnerHtml = "<span style=""color:red"">* </span>" & dr("Fld_label")

                    objFormGroup.Controls.Add(label)


                    Dim lbl As New Label
                    lbl.ID = "lbl" & dr("Fld_id")
                    If Not dataRow Is Nothing Then
                        lbl.Text = CCommon.ToString(dataRow(dr("Fld_label") & "Value"))
                    End If


                    If dr("fld_type") = "SelectBox" Then
                        If isEdit Then
                            Dim divInner As New HtmlGenericControl("div")
                            divInner.Controls.Add(lbl)
                            objFormGroup.Controls.Add(divInner)
                        Else
                            Dim ddl As DropDownList = CreateDropdown(objFormGroup, dr("Fld_id"), CCommon.ToLong(dr("Fld_Value")), dr("numlistid"))

                            If CCommon.ToBool(IsMatrix) Then
                                ddl.Enabled = False
                            End If
                        End If

                        divColumn.Controls.Add(objFormGroup)
                        divRow.Controls.Add(divColumn)
                    ElseIf dr("fld_type") = "CheckBox" Then
                        If isEdit Then
                            Dim divInner As New HtmlGenericControl("div")
                            divInner.Controls.Add(lbl)
                            objFormGroup.Controls.Add(divInner)
                        Else
                            Dim divInner As New HtmlGenericControl("div")
                            CreateChkBox(divInner, dr("Fld_id"), 0)
                            objFormGroup.Controls.Add(divInner)
                        End If

                        divColumn.Controls.Add(objFormGroup)
                        divRow.Controls.Add(divColumn)
                    End If

                    i = i + 1

                    If i = 2 Then
                        plhWareHouseAttributes.Controls.Add(divRow)
                        divRow = New HtmlGenericControl("div")
                        divRow.Attributes.Add("class", "row")
                    End If
                Next

                If divRow.Controls.Count > 0 Then
                    plhWareHouseAttributes.Controls.Add(divRow)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try


            Dim objItems As New CItems
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")
                objItems.ItemCode = lngItemCode
                objItems.WareHouseItemID = lngWarehouseItemID
                objItems.WListPrice = CCommon.ToDecimal(txtListPrice.Text.Trim())
                objItems.WSKU = txtSKU.Text.Trim
                objItems.WBarCode = txtUPC.Text.Trim()

                'RETRIVE MATRIX ITEM ATTRIBUTES VALUE
                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("Fld_ID")
                dtCusTable.Columns.Add("Fld_Value")
                dtCusTable.TableName = "CusFlds"
                Dim drCusRow As DataRow

                If Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 0 AndAlso dsAttributes.Tables(0).Rows.Count > 0 Then
                    For Each dr In dsAttributes.Tables(0).Rows
                        drCusRow = dtCusTable.NewRow
                        drCusRow("Fld_ID") = dr("fld_id")

                        If dr("fld_type") = "SelectBox" Then
                            Dim ddl As DropDownList
                            ddl = plhWareHouseAttributes.FindControl(dr("Fld_id"))

                            If CCommon.ToLong(ddl.SelectedValue) > 0 Then
                                drCusRow("Fld_Value") = CStr(ddl.SelectedItem.Value)
                            Else
                                lblErrorMessage.Text = dr("Fld_label") & " is required."
                                Exit Sub
                            End If
                        ElseIf dr("fld_type") = "CheckBox" Then
                            Dim chk As CheckBox
                            chk = plhWareHouseAttributes.FindControl(dr("Fld_id"))
                            If chk.Checked = True Then
                                drCusRow("Fld_Value") = "1"
                            Else : drCusRow("Fld_Value") = "0"
                            End If
                        End If

                        dtCusTable.Rows.Add(drCusRow)
                        dtCusTable.AcceptChanges()
                    Next
                End If

                Dim ds1 As New DataSet
                ds1.Tables.Add(dtCusTable)
                objItems.strFieldList = ds1.GetXml

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                objItems.SaveAttributesForMatrixItems()

                objTransactionScope.Complete()

            End Using


                lblErrorMessage.Text = "Record added successfully."

            'plhWareHouseAttributes.Controls.Clear()



        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub





#End Region


End Class


