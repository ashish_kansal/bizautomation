<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmPriceBookMgmtList.aspx.vb"
    Inherits=".frmPriceBookMgmtList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Price Book Rule</title>
    <script type="text/javascript" language="javascript">
        function New(a, b) {
            window.location.href = 'frmPriceBookMgmt.aspx?ItemCode=' + a + '&Vendor=' + b;
            return false;
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function PriceTable(a) {
            var w = 1000;
            var h = screen.height;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("../Items/frmPricingTable.aspx?RuleID=" + a, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenItem(a) {
            window.location.href = '../Items/frmKitDetails.aspx?ItemCode=' + a;
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Admin-Price_Management.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline" id="tblItemVendor" runat="server" visible="false">
                    <div class="form-group">
                        <label>Item: </label>
                        <asp:Label ID="lblItem" runat="server"></asp:Label>
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Vendor: </label>
                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnNew" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Price Rule</asp:LinkButton>
                <asp:LinkButton ID="btnBackItem" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back to Item</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Price Book Rule&nbsp;<a href="#" onclick="return OpenHelpPopUp('Items/frmPriceBookMgmtList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgPriceBookRule" AllowSorting="True" runat="server" Width="100%"
                    CssClass="table table-striped table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numPricRuleID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcRuleName" CommandName="RuleID" SortExpression="vcRuleName"
                            HeaderText="Rule Name"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="Priority" HeaderText="Priority"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcRuleFor" HeaderText="Rule For"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PricingRule" HeaderText="Pricing Method"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Item it applies to" DataField="ItemList"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Customer it applies to" DataField="CustomerList"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
