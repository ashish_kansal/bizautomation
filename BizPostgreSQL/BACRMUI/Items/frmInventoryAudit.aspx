﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmInventoryAudit.aspx.vb"
    MasterPageFile="~/common/PopupBootstrap.Master" Inherits=".frmInventoryAudit" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Inventory Audit Report</title>
    <script type="text/javascript">
        function OpenOpp(a, b, c) {
            var str;

            if (c == 2) {
                str = "../items/frmWorkOrder.aspx?WOID=" + a;
                window.opener.location.href = str;
            } else if (a > 0 && b == 5) {
                str = "../opportunity/frmReturnDetail.aspx?frm=OpenBizDoc&ReturnID=" + a;
                window.opener.location.href = str;
            }
            else if (a > 0 && b > 0) {
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
                window.opener.location.href = str;
            }
        }

        //Paging
        //function fn_doPage(pgno) {
        //    $('#txtCurrrentPage').val(pgno);
        //    if ($('#txtCurrrentPage').val() != 0 || $('#txtCurrrentPage').val() > 0) {
        //        $('#btnGo1').trigger('click');
        //    }
        //}
        function fn_doPage(pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
            var currentPage;
            var postbackButton;

            if (CurrentPageWebControlClientID != '') {
                currentPage = CurrentPageWebControlClientID;
            }
            else {
                currentPage = 'txtCurrentPageCorr';
            }

            if (PostbackButtonClientID != '') {
                postbackButton = PostbackButtonClientID;
            }
            else {
                postbackButton = 'btnCorresGo';
            }

            $('#' + currentPage + '').val(pgno);
            if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                $('#' + postbackButton + '').trigger('click');
            }
        }

        function dateSelected(sender, eventArgs) {
            $("[id$=btnFilter]").click();
        }
        function OnClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnFilter]").click();
            return false;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $('.txtFilterOrder').on("keypress", function (e) {
                if (e.keyCode == 13) {
                    $("[id$=btnFilter]").click();
                    return false;
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-left">
        <asp:CheckBox ID="chkOnlyAvgCost" runat="server" Text="Biz Calculated Average Cost" AutoPostBack="true" />
    </div>
    <div class="pull-right">
        <ul class="list-inline">
                    <li>
                        <webdiyer:AspNetPager
                        ID="bizPager"
                        runat="server"
                        PagingButtonSpacing="0"
                        CssClass="bizgridpager"
                        AlwaysShow="true"
                        CurrentPageButtonClass="active"
                        PagingButtonUlLayoutClass="pagination"
                        PagingButtonLayoutType="UnorderedList"
                        FirstPageText="<<"
                        LastPageText=">>"
                        NextPageText=">"
                        PrevPageText="<"
                        Width="100%"
                        HorizontalAlign="Right"
                        NumericButtonCount="5"
                        CustomInfoSectionWidth="300"
                        LayoutType="div" UrlPaging="false" ShowMoreButtons="true" ShowPageIndexBox="Never"
                        ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
                        CustomInfoStyle="line-height:35px;margin-right:3px;text-align:right !important;font-weight:bold" CurrentPageWebControlClientID="txtCurrrentPage" PostbackButtonClientID="btnGo1">
                    </webdiyer:AspNetPager>
                    </li>
                    <li style="vertical-align: top;margin-top: 5px;">
                        <asp:LinkButton runat="server" id="lbClearGridCondition" OnClick="lbClearGridCondition_Click" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></asp:LinkButton>
                    </li>
                </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">Inventory Audit Report</asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>
    <asp:GridView ID="gvAuditReport" AutoGenerateColumns="False" runat="server" CssClass="table table-bordered table-striped" DataKeyNames="numWareHouseItemID" OnRowDataBound="gvAuditReport_RowDataBound" ShowHeaderWhenEmpty="true">
        <Columns>
            <asp:BoundField DataField="vcItemName" HeaderText="Item" HeaderStyle-Wrap="false" visible="false"></asp:BoundField>
            <asp:BoundField DataField="vcWarehouse" HeaderText="Warehouse" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" visible="false"></asp:BoundField>
            <asp:TemplateField HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    Date / Time 
                    <br />
                    <ul class="list-inline">
                        <li style="padding-right: 0px">
                            <telerik:RadDatePicker ID="radDateFrom" runat="server" Width="65" DateInput-CssClass="form-control" DateInput-AutoCompleteType="Disabled" DateInput-EmptyMessage="From" ShowPopupOnFocus="true" DatePopupButton-Visible="false">
                                 <ClientEvents OnDateSelected="dateSelected" />
                            </telerik:RadDatePicker>
                        </li>
                        <li style="padding-left: 0px">
                            <telerik:RadDatePicker ID="radDateTo" runat="server" Width="65" DateInput-CssClass="form-control" DateInput-AutoCompleteType="Disabled" DateInput-EmptyMessage="To" ShowPopupOnFocus="true" DatePopupButton-Visible="false">
                                 <ClientEvents OnDateSelected="dateSelected" />
                            </telerik:RadDatePicker>
                        </li>
                    </ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("dtCreatedDate")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Wrap="false" HeaderStyle-Width="50%" ItemStyle-Width="50%">
                <HeaderTemplate>
                    Inventory Activity
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("vcDescription")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Result">
                <ItemTemplate>
                    <%-- <%# Eval("vcResult")%>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="numOnHand" DataFormatString="{0:#,##0.#####}" HeaderText="Available" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
            <asp:TemplateField HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    Total Available
                    <asp:Label ID="lblToolTip" Text="[?]" CssClass="tip"
                        runat="server" ToolTip="Indicates sum total of OnHand qty from all warehouse(s) of item" />&nbsp;
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("numTotalOnHand", "{0:#,##0}")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="numOnOrder" DataFormatString="{0:#,##0.#####}" HeaderText="OnOrder" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
            <asp:BoundField DataField="numAllocation" DataFormatString="{0:#,##0.#####}" HeaderText="Allocation" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
            <asp:BoundField DataField="numBackOrder" DataFormatString="{0:#,##0.#####}" HeaderText="BackOrder" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
            <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    Order ID
                    <br />
                    <asp:TextBox ID="txtOrderName" runat="server" CssClass="form-control txtFilterOrder" onchange="HeaderFiltersValueChanged('order');"></asp:TextBox>
                </HeaderTemplate>
                <ItemTemplate>
                    <a href="javascript:OpenOpp('<%#Eval("numReferenceID")%>','<%# Eval("tintopptype") %>','<%# Eval("tintRefType")%>')">
                        <%# Eval("vcPOppName") %>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="monAverageCost" HeaderText="Avg Cost" HtmlEncode="false" DataFormatString="{0:#,###.0000}" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
            <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <HeaderTemplate>
                    User
                    <br />
                    <telerik:RadComboBox ID="rcbUsers" runat="server" CheckBoxes="true" onclientselectedindexchanged="OnClientSelectedIndexChanged"></telerik:RadComboBox>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("vcUserName")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
    </asp:GridView>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:Button ID="btnFilter" runat="server" Style="display:none" OnClick="btnFilter_Click" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
