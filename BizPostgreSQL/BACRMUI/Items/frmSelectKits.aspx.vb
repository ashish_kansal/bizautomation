Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Items
    Public Class frmSelectKits
        Inherits BACRMPage
        Dim objItem As New CItems
        Dim dtKits As DataTable

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Item"
                    BindGrid()
                    loadDropdown()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub loadDropdown()
            Try
                objItem.KitParent = 1
                objItem.DomainID = Session("DomainID")
                'dtKits = objItem.GetItemsOrKits
                ddlKit.DataSource = dtKits
                ddlKit.DataTextField = "vcItemName"
                ddlKit.DataValueField = "numItemCode"
                ddlKit.DataBind()
                ddlKit.Items.Insert(0, "--Select One--")
                ddlKit.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindGrid()
            Try
                objItem.ItemCode = GetQueryStringVal( "ItemCode")
                dgKitParents.DataSource = objItem.GetKitParentsForChildItem
                dgKitParents.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                objItem.KitID = ddlKit.SelectedItem.Value
                objItem.ItemCode = GetQueryStringVal( "ItemCode")
                objItem.NoofItemsReqForKit = txtNoOfChilItems.Text
                objItem.byteMode = 0
                objItem.ManageKitParentsForChildItem()
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgKitParents_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgKitParents.EditCommand
            Try
                dgKitParents.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgKitParents_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgKitParents.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.EditItem Then
                    objItem.KitParent = 1
                    objItem.DomainID = Session("DomainID")
                    Dim ddl As DropDownList
                    ddl = e.Item.FindControl("ddlKits")
                    'ddl.DataSource = objItem.GetItemsOrKits
                    ddl.DataTextField = "vcItemName"
                    ddl.DataValueField = "numItemCode"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = 0
                    ddl.Items.FindByValue(CType(e.Item.FindControl("lblKitId"), Label).Text).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgKitParents_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgKitParents.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgKitParents.EditItemIndex = e.Item.ItemIndex
                    dgKitParents.EditItemIndex = -1
                    Call BindGrid()
                End If

                If e.CommandName = "Delete" Then
                    objItem.KitID = CType(e.Item.FindControl("lblKitParentID"), Label).Text
                    objItem.ItemCode = GetQueryStringVal( "ItemCode")
                    objItem.byteMode = 1
                    objItem.ManageKitParentsForChildItem()
                    Call BindGrid()
                End If
                If e.CommandName = "Update" Then
                    objItem.KitID = CType(e.Item.FindControl("ddlKits"), DropDownList).SelectedValue
                    objItem.ItemCode = GetQueryStringVal( "ItemCode")
                    objItem.NoofItemsReqForKit = CType(e.Item.FindControl("txtQuantity"), TextBox).Text
                    objItem.byteMode = 0
                    objItem.ManageKitParentsForChildItem()
                    dgKitParents.EditItemIndex = -1
                    Call BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace