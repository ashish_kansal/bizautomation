﻿'Imports BACRM.BusinessLogic.Common
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Item
'Imports BACRM.BusinessLogic.Projects

'Public Class frmInventoryAdjustment
'    Inherits BACRMPage
'    Dim lngItemCode As Long
'    Dim objJournalEntry As JournalEntry
'    Dim dtItems As New DataTable
'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            lngItemCode = GetQueryStringVal("ItemCode")
'            If Not IsPostBack Then

'                BindWarehouse()
'                BindCOA()
'                BindProject()
'                calAdjustmentDate.SelectedDate = Date.Now

'                PersistTable.Load(boolOnlyURL:=True)
'                If PersistTable.Count > 0 Then
'                    If ddlAccount.Items.FindByValue(PersistTable(ddlAccount.ID)) IsNot Nothing Then
'                        ddlAccount.ClearSelection()
'                        ddlAccount.Items.FindByValue(PersistTable(ddlAccount.ID)).Selected = True
'                    End If

'                    If ddlProject.Items.FindByValue(PersistTable(ddlProject.ID)) IsNot Nothing Then
'                        ddlProject.ClearSelection()
'                        ddlProject.Items.FindByValue(PersistTable(ddlProject.ID)).Selected = True
'                    End If

'                    If ddlWarehouse.Items.FindByValue(PersistTable(ddlWarehouse.ID)) IsNot Nothing Then
'                        ddlWarehouse.ClearSelection()
'                        ddlWarehouse.Items.FindByValue(PersistTable(ddlWarehouse.ID)).Selected = True
'                    End If
'                    ddlWarehouse_SelectedIndexChanged()
'                End If
'            End If
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveClose.Click
'        Try
'            If CCommon.ToString(txtUnitCost.Text) = "" Then
'                litMessage.Text = "Estimated unit cost is required, if you just want to change On Hand quantity then enter ""0""."
'                Exit Sub
'            End If


'            Dim journalID As Long = SaveJournalHeader()
'            SaveDataToGeneralJournalDetails(journalID)

'            'Update Quantity
'            If radQty.Checked And Math.Abs(CCommon.ToDecimal(txtAdjustedQty.Text)) > 0 Then
'                objCommon.Mode = 13
'                objCommon.UpdateRecordID = CCommon.ToLong(ddlWarehouse.SelectedValue)
'                objCommon.UpdateValueID = Session("UserContactID")
'                objCommon.Comments = txtAdjustedQty.Text
'                objCommon.UpdateSingleFieldValue()
'            End If
'            'Only when Adj qty is not zero OR only adjust cost
'            If Math.Abs(CCommon.ToDecimal(txtAdjustedQty.Text)) > 0 Or radCost.Checked Then
'                'Update New Average cost
'                objCommon.Mode = 14
'                objCommon.UpdateRecordID = lngItemCode
'                If radCost.Checked Then
'                    objCommon.Comments = ((CCommon.ToDecimal(txtUnitCost.Text)) + (CCommon.ToDecimal(lblOnHandQty.Text) * CCommon.ToDecimal(hdnAverageCost.Value))) / CCommon.ToDecimal(lblOnHandQty.Text)
'                Else
'                    objCommon.Comments = ((CCommon.ToDecimal(txtAdjustedQty.Text) * CCommon.ToDecimal(txtUnitCost.Text)) + (CCommon.ToDecimal(lblOnHandQty.Text) * CCommon.ToDecimal(hdnAverageCost.Value))) / (CCommon.ToDecimal(lblOnHandQty.Text) + CCommon.ToDecimal(txtAdjustedQty.Text))
'                End If
'                objCommon.UpdateSingleFieldValue()
'            End If
'            litMessage.Text = "Adjustment posted sucessfully"

'            PersistTable.Clear()
'            PersistTable.Add(ddlAccount.ID, ddlAccount.SelectedValue)
'            PersistTable.Add(ddlWarehouse.ID, ddlWarehouse.SelectedValue)
'            PersistTable.Add(ddlProject.ID, ddlProject.SelectedValue)
'            PersistTable.Save(boolOnlyURL:=True)
'            'update latest warehouse values.
'            ddlWarehouse_SelectedIndexChanged()
'        Catch ex As Exception
'            If ex.Message = "FY_CLOSED" Then
'                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
'            Else
'                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'                Response.Write(ex)
'            End If
'        End Try
'    End Sub

'    Private Sub ddlWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWarehouse.SelectedIndexChanged
'        Try
'            ddlWarehouse_SelectedIndexChanged()
'        Catch ex As Exception
'            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
'            Response.Write(ex)
'        End Try
'    End Sub

'    Private Sub ddlWarehouse_SelectedIndexChanged()
'        Try
'            Dim objItems As New CItems
'            Dim ds As DataSet
'            objItems.ItemCode = lngItemCode
'            ds = objItems.GetItemWareHouses()
'            Dim dr() As DataRow = ds.Tables(0).Select("numWareHouseItemID=" & ddlWarehouse.SelectedValue)
'            If dr.Length > 0 Then
'                lblOnHandQty.Text = CCommon.ToInteger(dr(0)("OnHand"))


'                Dim dtItemDetails As DataTable
'                objItems.ItemCode = lngItemCode
'                objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
'                dtItemDetails = objItems.ItemDetails

'                If dtItemDetails.Rows.Count > 0 Then
'                    lblCurrentInvValue.Text = String.Format("{0:###0.00}", CCommon.ToInteger(dr(0)("OnHand")) * CCommon.ToDecimal(dtItemDetails.Rows(0)("monAverageCost")))
'                    txtUnitCost.Text = String.Format("{0:###0.00}", CCommon.ToDecimal(dtItemDetails.Rows(0)("monAverageCost")))
'                    hdnAverageCost.Value = CCommon.ToDecimal(dtItemDetails.Rows(0)("monAverageCost"))
'                End If

'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub
'    Private Function SaveJournalHeader() As Long
'        Try
'            Dim objJEHeader As New JournalEntryHeader
'            Dim lngJournalId As Long = 0
'            With objJEHeader
'                .JournalId = 0
'                .RecurringId = 0
'                .EntryDate = CDate(calAdjustmentDate.SelectedDate & " 12:00:00") 'bug id 1028
'                .Description = txtMemo.Text
'                If radCost.Checked Then
'                    .Amount = CCommon.ToDecimal(txtUnitCost.Text)
'                Else
'                    .Amount = CCommon.ToDecimal(txtUnitCost.Text) * CCommon.ToDecimal(txtAdjustedQty.Text)
'                End If

'                .CheckId = 0
'                .CashCreditCardId = 0
'                .ChartAcntId = 0
'                .OppId = 0
'                .OppBizDocsId = 0
'                .DepositId = 0
'                .BizDocsPaymentDetId = 0
'                .IsOpeningBalance = 0
'                .LastRecurringDate = Date.Now
'                .NoTransactions = 0
'                .CategoryHDRID = 0
'                .ReturnID = 0
'                .CheckHeaderID = 0
'                .BillID = 0
'                .BillPaymentID = 0
'                .UserCntID = Session("UserContactID")
'                .DomainID = Session("DomainID")
'            End With
'            lngJournalId = objJEHeader.Save()
'            Return lngJournalId
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Function
'    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Long)
'        Dim lstr As String
'        Dim ds As New DataSet

'        Dim dtrow As DataRow
'        Try
'            Dim objItem As New CItems
'            Dim dtItemDetails As DataTable

'            objItem.ItemCode = lngItemCode
'            objItem.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
'            dtItemDetails = objItem.ItemDetails

'            If dtItemDetails.Rows.Count > 0 Then
'                If CCommon.ToLong(dtItemDetails.Rows(0)("numAssetChartAcntId")) > 0 Then
'                    If objJournalEntry Is Nothing Then objJournalEntry = New JournalEntry
'                    Dim decDebitAmtAsset As Decimal
'                    Dim decCreditAmtAsset As Decimal
'                    Dim decDebitAmtExpense As Decimal
'                    Dim decCreditAmtExpense As Decimal

'                    If radCost.Checked Then
'                        decDebitAmtAsset = CCommon.ToDecimal(txtUnitCost.Text)
'                        decCreditAmtAsset = 0
'                        decDebitAmtExpense = 0
'                        decCreditAmtExpense = CCommon.ToDecimal(txtUnitCost.Text)
'                    Else
'                        If CCommon.ToInteger(txtAdjustedQty.Text) < 0 Then
'                            decDebitAmtAsset = CCommon.ToDecimal(txtUnitCost.Text) * CCommon.ToDecimal(txtAdjustedQty.Text)
'                            decCreditAmtAsset = 0
'                            decDebitAmtExpense = 0
'                            decCreditAmtExpense = CCommon.ToDecimal(txtUnitCost.Text) * CCommon.ToDecimal(txtAdjustedQty.Text)
'                        ElseIf CCommon.ToInteger(txtAdjustedQty.Text) > 0 Then
'                            decDebitAmtAsset = CCommon.ToDecimal(txtUnitCost.Text) * CCommon.ToDecimal(txtAdjustedQty.Text)
'                            decCreditAmtAsset = 0
'                            decDebitAmtExpense = 0
'                            decCreditAmtExpense = CCommon.ToDecimal(txtUnitCost.Text) * CCommon.ToDecimal(txtAdjustedQty.Text)
'                        End If
'                    End If

'                    Dim objJEList As New JournalEntryCollection

'                    Dim objJE As New JournalEntryNew()

'                    objJE = New JournalEntryNew()

'                    objJE.TransactionId = 0
'                    objJE.DebitAmt = decDebitAmtExpense
'                    objJE.CreditAmt = decCreditAmtExpense
'                    objJE.ChartAcntId = ddlAccount.SelectedValue 'expense account
'                    objJE.Description = ""
'                    objJE.CustomerId = 0
'                    objJE.MainDeposit = 0
'                    objJE.MainCheck = 0
'                    objJE.MainCashCredit = 0
'                    objJE.OppitemtCode = 0
'                    objJE.BizDocItems = ""
'                    objJE.Reference = ""
'                    objJE.PaymentMethod = 0
'                    objJE.Reconcile = False
'                    objJE.CurrencyID = 0
'                    objJE.FltExchangeRate = 0
'                    objJE.TaxItemID = 0
'                    objJE.BizDocsPaymentDetailsId = 0
'                    objJE.ContactID = 0
'                    objJE.ItemID = lngItemCode 'added by chintan to track adjustment
'                    objJE.ProjectID = ddlProject.SelectedValue
'                    objJE.ClassID = 0
'                    objJE.CommissionID = 0
'                    objJE.ReconcileID = 0
'                    objJE.Cleared = 0
'                    objJE.ReferenceType = 0
'                    objJE.ReferenceID = 0

'                    objJEList.Add(objJE)

'                    objJE = New JournalEntryNew()

'                    objJE.TransactionId = 0
'                    objJE.DebitAmt = decDebitAmtAsset
'                    objJE.CreditAmt = decCreditAmtAsset
'                    objJE.ChartAcntId = CCommon.ToLong(dtItemDetails.Rows(0)("numAssetChartAcntId")) 'asset account
'                    objJE.Description = ""
'                    objJE.CustomerId = 0
'                    objJE.MainDeposit = 0
'                    objJE.MainCheck = 0
'                    objJE.MainCashCredit = 0
'                    objJE.OppitemtCode = 0
'                    objJE.BizDocItems = ""
'                    objJE.Reference = ""
'                    objJE.PaymentMethod = 0
'                    objJE.Reconcile = False
'                    objJE.CurrencyID = 0
'                    objJE.FltExchangeRate = 0
'                    objJE.TaxItemID = 0
'                    objJE.BizDocsPaymentDetailsId = 0
'                    objJE.ContactID = 0
'                    objJE.ItemID = lngItemCode 'added by chintan to track adjustment
'                    objJE.ProjectID = ddlProject.SelectedValue
'                    objJE.ClassID = 0
'                    objJE.CommissionID = 0
'                    objJE.ReconcileID = 0
'                    objJE.Cleared = 0
'                    objJE.ReferenceType = 0
'                    objJE.ReferenceID = 0

'                    objJEList.Add(objJE)

'                    objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

'                End If
'            End If


'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Private Sub BindProject()
'        Try
'            Dim objProject As New Project
'            objProject.DomainID = Session("DomainID")
'            ddlProject.DataTextField = "vcProjectName"
'            ddlProject.DataValueField = "numProId"
'            ddlProject.DataSource = objProject.GetOpenProject()
'            ddlProject.DataBind()
'            ddlProject.Items.Insert(0, "--Select One--")
'            ddlProject.Items.FindByText("--Select One--").Value = "0"
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub
'    Private Sub BindWarehouse()
'        Try
'            Dim objItems As New CItems
'            Dim ds As DataSet
'            objItems.ItemCode = lngItemCode
'            ds = objItems.GetItemWareHouses()

'            ddlWarehouse.DataValueField = "numWareHouseItemID"
'            ddlWarehouse.DataTextField = "vcWareHouse"
'            ddlWarehouse.DataSource = ds.Tables(0)
'            ddlWarehouse.DataBind()
'            ddlWarehouse.Items.Insert(0, "--Select One--")
'            ddlWarehouse.Items.FindByText("--Select One--").Value = 0

'            If ddlWarehouse.Items.Count = 2 Then
'                ddlWarehouse.SelectedIndex = 1
'                ddlWarehouse_SelectedIndexChanged()
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub
'    Private Sub BindCOA()
'        Try
'            Dim objCOA As New ChartOfAccounting
'            objCOA.DomainID = Session("DomainId")
'            objCOA.AccountCode = "0104"
'            Dim dtChartAcntDetails As DataTable = objCOA.GetParentCategory()
'            objCOA.AccountCode = "0103"
'            Dim dtChartAcntDetails1 As DataTable = objCOA.GetParentCategory()
'            dtChartAcntDetails.Merge(dtChartAcntDetails1)

'            Dim item As ListItem
'            For Each dr As DataRow In dtChartAcntDetails.Rows
'                item = New ListItem()
'                item.Text = dr("vcAccountName")
'                item.Value = dr("numAccountID")
'                item.Attributes("OptionGroup") = dr("vcAccountType")
'                ddlAccount.Items.Add(item)
'            Next
'            ddlAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub


'End Class