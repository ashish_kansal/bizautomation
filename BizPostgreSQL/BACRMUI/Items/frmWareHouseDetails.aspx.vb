Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Namespace BACRM.UserInterface.Item
    Partial Public Class frmWareHouseDetails
        Inherits BACRMPage

        Dim objItem As CItems
       
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                ltrMessage.Text = ""

                GetUserRightsForPage(13, 36)
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                End If
                txtFromPin.Attributes.Add("onKeyPress", "CheckNumber(2)")
                txtToPin.Attributes.Add("onKeyPress", "CheckNumber(2)")
                btnSave.Attributes.Add("OnClick", "return Save()")
                If Not IsPostBack Then

                    objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                    objItem = New CItems
                    objItem.DomainID = Session("DomainID")
                    ddlWareHouse.DataSource = objItem.GetWareHouses
                    ddlWareHouse.DataTextField = "vcWareHouse"
                    ddlWareHouse.DataValueField = "numWareHouseID"
                    ddlWareHouse.DataBind()
                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindDataGrid()
            Try
                If objItem Is Nothing Then objItem = New CItems
                objItem.DomainID = Session("DomainId")
                dgwarehouse.DataSource = objItem.GetWareHousesDetails()
                dgwarehouse.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
            Try
                FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objItem Is Nothing Then objItem = New CItems
                objItem.WareHouseDetailID = 0
                objItem.Country = ddlCountry.SelectedValue
                objItem.State = ddlState.SelectedValue
                objItem.WarehouseID = ddlWareHouse.SelectedValue
                objItem.mode = False
                objItem.DomainID = Session("DomainId")

                If txtFromPin.Text <> "" Then objItem.FromPinCode = txtFromPin.Text
                If txtToPin.Text <> "" Then objItem.ToPinCode = txtToPin.Text

                If objItem.AddWarehouseDetails = 0 Then
                    ShowMessage("WareHouse Already Set for the State Selected")
                Else
                    ddlCountry.SelectedIndex = 0
                    ddlState.Items.Clear()
                    ddlWareHouse.SelectedIndex = 0
                    txtFromPin.Text = ""
                    txtToPin.Text = ""
                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgwarehouse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgwarehouse.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    If objItem Is Nothing Then objItem = New CItems
                    objItem.WareHouseDetailID = e.Item.Cells(0).Text
                    objItem.DomainID = Session("DomainId")
                    objItem.Country = 0
                    objItem.WarehouseID = 0
                    objItem.State = 0
                    objItem.mode = True
                    objItem.AddWarehouseDetails()
                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgwarehouse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgwarehouse.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                ltrMessage.Text = message
                divMessage.Focus()

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SlectTab", "selectTab('');", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace