<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddItem.aspx.vb" Inherits=".frmAddItem"
    MasterPageFile="~/common/PopupBootstrap.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add New Item</title>
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <link rel="stylesheet" href="../CSS/select2.css" />
    <script type="text/javascript">
        var columns;

        $(document).ready(function () {
            if ($("[id$=rbNew]").length == 0 || $("[id$=rbNew]").is(":checked")) {
                $("#divNewItem").show();
                $("#divExistingItem").hide();
            } else {
                $("#divNewItem").hide();
                $("#divExistingItem").show();
            }

            $("[id$=rbNew]").change(function () {
                if ($(this).is(":checked")) {
                    $("#divNewItem").show();
                    $("#divExistingItem").hide();
                } else {
                    $("#divNewItem").hide();
                    $("#divExistingItem").show();
                }
            });

            $("[id$=rbExisting]").change(function () {
                if ($(this).is(":checked")) {
                    $("#divNewItem").hide();
                    $("#divExistingItem").show();
                } else {
                    $("#divNewItem").show();
                    $("#divExistingItem").hide();
                }
            });

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);

                    $('#txtItem').select2({
                      placeholder: 'Select Items',
                      minimumInputLength: 1,
                      multiple: false,
                      formatResult: formatItem,
                      width: "100%",
                      dropdownCssClass: 'bigdrop',
                      dataType: "json",
                      allowClear: true,
                      ajax: {
                          quietMillis: 500,
                          url: '../common/Common.asmx/GetSearchedItems',
                          type: 'POST',
                          params: {
                              contentType: 'application/json; charset=utf-8'
                          },
                          dataType: 'json',
                          data: function (term, page) {
                              var customerPart = $("#chkAutoCheckCustomerPart").prop("checked");
                              if (customerPart == undefined) {
                                  customerPart = false;
                              }
                              return JSON.stringify({
                                  searchText: term,
                                  pageIndex: page,
                                  pageSize: 10,
                                  divisionId: 0,
                                  isGetDefaultColumn: false,
                                  warehouseID: 0,
                                  searchOrdCusHistory: 0,
                                  oppType: 1,
                                  isTransferTo: false,
                                  IsCustomerPartSearch: customerPart,
                                  searchType: "1"
                              });
                          },
                          results: function (data, page) {

                              if (data.hasOwnProperty("d")) {
                                  if (data.d == "Session Expired") {
                                      alert("Session expired.");
                                      window.opener.location.href = window.opener.location.href;
                                      window.close();
                                  } else {
                                      data = $.parseJSON(data.d)
                                  }
                              }
                              else
                                  data = $.parseJSON(data);

                              var more = (page.page * 10) < data.Total;
                              return { results: $.parseJSON(data.results), more: more };
                          }
                      }
                  });
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

          

            $('#txtItem').on("change", function (e) {
                if (e.added != null) {
                    $("#hdnItemID").val(e.added.id.toString().split("-")[0]);
                }
            })

            $('#txtItem').on("select2-removed", function (e) {
                $("#hdnItemID").val("");
            })
        });

        function formatItem(row) {
            var numOfColumns = 0;
            var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
            ui = ui + "<tbody>";
            ui = ui + "<tr>";
            ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
            if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
            } else {
                ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
            }

            ui = ui + "</td>";
            ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
            ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
            ui = ui + "<tbody>";
            ui = ui + "<tr>";
            $.each(columns, function (index, column) {
                if (numOfColumns == 4) {
                    ui = ui + "</tr><tr>";
                    numOfColumns = 0;
                }

                if (numOfColumns == 0) {
                    ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                }
                else {
                    ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                }
                numOfColumns += 2;
            });
            ui = ui + "</tr>";
            ui = ui + "</tbody>";
            ui = ui + "</table>";
            ui = ui + "</td>";
            ui = ui + "</tr>";
            ui = ui + "</tbody>";
            ui = ui + "</table>";

            return ui;
        }

        function CloseAndRefreshParent() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            window.close();
        }

        function SendToNewOrderPage() {
            if (window.opener != null) {
                var url = GetURL();
                window.opener.OpenNewItemPopup(url);
            }

            window.close();

            return false;
        }

        function SendToAssemblyKitItemDetailPage() {
            if (window.opener != null) {
                var url = GetURL();
                window.opener.OpenNewItemPopup(url);
            }

            window.close();

            return false;
        }

        function GetURL() {
            var url = ""

            if ($("[id$=radNonInventory]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=87";
            } else if ($("[id$=radInventoryItem]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=86";
            } else if ($("[id$=radService]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=87&ItemType=Services";
            } else if ($("[id$=radSerialized]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=88&ItemType=Serialized";
            } else if ($("[id$=radKit]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=86&ItemType=Kits";
            } else if ($("[id$=radAssembly]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=86&ItemType=Assembly";
            } else if ($("[id$=radLot]").is(":checked")) {
                url = "/Items/frmNewItem.aspx?FormID=88&ItemType=Lot";
            }

            return url;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnNext" runat="server" Text="Save & Close" CssClass="btn btn-primary" />
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" OnClientClick="return Close();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add Item
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="row padbottom10" runat="server" visible="false" id="divCreateOrMap">
        <div class="col-xs-12">
            <ul class="list-inline">
                <li>
                    <asp:RadioButton runat="server" ID="rbNew" Checked="true" Text="Create New Item" GroupName="CreateOrMap" />
                </li>
                <li>
                    <asp:RadioButton runat="server" ID="rbExisting" Text="Map to an existing Item" GroupName="CreateOrMap" />
                </li>
            </ul>
        </div>
    </div>
    <div class="row" id="divExistingItem" style="display: none">
        <div class="col-xs-12">
            <input id="txtItem" type="text" />
        </div>
    </div>
    <div class="row" id="divNewItem">
        <div class="col-xs-12">
            <ul class="list-unstyled">
                <li>
                    <asp:RadioButton Checked="true" ID="radNonInventory" runat="server" GroupName="rad" Text=" Non-Inventory Item" /></li>
                <li>
                    <asp:RadioButton ID="radInventoryItem" runat="server" GroupName="rad" Text=" Inventory Item" /></li>
                <li>
                    <asp:RadioButton ID="radService" runat="server" GroupName="rad" Text=" Service" /></li>
                <li>
                    <asp:RadioButton ID="radSerialized" runat="server" GroupName="rad" Text=" Serialized" /></li>
                <li>
                    <asp:RadioButton ID="radKit" runat="server" GroupName="rad" Text=" Kit" /></li>
                <li>
                    <asp:RadioButton ID="radAssembly" runat="server" GroupName="rad" Text=" Assembly" /></li>
                <li>
                    <asp:RadioButton ID="radLot" runat="server" GroupName="rad" Text=" Lot # based" /></li>
            </ul>
        </div>
    </div>

    <asp:HiddenField ID="hdnItemID" runat="server" />
</asp:Content>
