﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPricingNamesTable.aspx.vb" Inherits=".frmPricingNamesTable" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">    
    <title>Pricing Table</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save & Close"></asp:Button>&nbsp;
            <asp:Button ID="btnClose" runat="server" Width="50" Text="Close" CssClass="button"
                OnClientClick="return Close();"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <center>
        <table style="min-width: 700px">

            <tr>
                <td colspan="4">
                    <!-- IMPORTANT - COLUMN INDEX IS USED IN CODE SO TAKE CARE OF THAT WHILE ADDING COLUMNS OR CHANGING ORDER OF COLUMN -->
                    <asp:DataGrid ID="dgPriceTable" runat="server" CssClass="dg" Width="100%"
                        AutoGenerateColumns="False" ClientIDMode="AutoID">
                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                        <ItemStyle CssClass="is"></ItemStyle>
                        <HeaderStyle CssClass="hs"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Wrap="false" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <span><%# Container.DataSetIndex+1 %></span>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-Wrap="false" ItemStyle-Width="40%">
                                <ItemTemplate>                                    
                                    <asp:TextBox ID="txtPricingName" runat="server" Text='<%# Eval("vcPriceLevelName")%>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Bottom" />
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
