<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPriceBookMgmt.aspx.vb"
    Inherits=".frmPriceBookMgmt" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <style>
        .StepClass {
            color: Black;
            font-size: 12px;
            font-family: Arial; /*font-weight:bold;*/
        }
    </style>
    <script language="javascript" type="text/javascript">
        function CheckRad(rad) {
            if (rad.checked == false) {
                alert("Please select the radio button")
                return false;
            }
        }
        function PriceTable(a) {
            var w = 1000;
            var h = screen.height;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            document.getElementById('rbPricingTable').checked = true;
            window.open("../Items/frmPricingTable.aspx?RuleID=" + a, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenConfig(StepNo, StepValue, RuleID) {
            if (StepNo == 2) {
                if (StepValue == 1) {
                    document.getElementById('rbIndividualItem').checked = true;
                }
                else if (StepValue == 2) {
                    document.getElementById('rbItemClassification').checked = true;
                }
            } else if (StepNo == 3) {
                if (StepValue == 1) {
                    document.getElementById('rbIndividualOrg').checked = true;
                }
                else if (StepValue == 2) {
                    document.getElementById('rbRelProfile').checked = true;
                }
            }
            window.open("../Items/frmPriceBookSteps.aspx?Step=" + StepNo + "&StepValue=" + StepValue + "&RuleID=" + RuleID, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Save() {
            if (document.getElementById('txtRuleName').value == "") {
                alert("Enter rule name");
                document.getElementById('txtRuleName').focus();
                return false;
            }
            if (document.getElementById('rbPricingFormula').checked == true) {

                if (document.getElementById('txtDiscount').value == "") {
                    alert('Enter Discount');
                    document.getElementById('txtDiscount').focus();
                    return false;
                }
                if (document.getElementById('txtQntyItems').value == "") {
                    alert('Enter qty of items');
                    document.getElementById('txtQntyItems').focus();
                    return false;
                }
                if (document.getElementById('txtMaxPerOrAmt').value == "") {
                    alert('Enter max addition or deduction');
                    document.getElementById('txtMaxPerOrAmt').focus();
                    return false;
                }
            }
            if (document.getElementById('rbIndividualItem').checked == false && document.getElementById('rbItemClassification').checked == false && document.getElementById('rbAllItems').checked == false) {
                alert("You can�t save a rule without completing all 3 steps, Select one option from Step 2")
                return false;
            }
            if (document.getElementById('rbIndividualOrg').checked == false && document.getElementById('rbRelProfile').checked == false && document.getElementById('rbAllOrg').checked == false) {
                alert("You can�t save a rule without completing all 3 steps,Select one option from Step 3")
                return false;
            }
        }
        function OpenHelp() {
            window.open('../Help/Admin-Price_Management.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
    <style>
        .table > tbody > tr > td {
            border-top: 0px solid #ddd !important;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Save();"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" usesubmitbehaviour="false" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
                    <ProgressTemplate>
                        <div class="overlay">
                            <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                                <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                                <strong>Processing Request</strong>
                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Price Book Rule
    &nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center" style="color: red;">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <div class="row">
        <div class="col-sm-4 com-md-4">
            <div class="form-group">
                <label>Price Rule Name</label>
                <asp:TextBox ID="txtRuleName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-4 com-md-4">
            <div class="form-group">
                <label>Price Rule Description</label>
                <asp:TextBox ID="txtRuleDesc" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-4 com-md-4">
            <div class="form-group">
                <label>
                    <asp:CheckBox ID="chkRoundTo" runat="server" />
                    Round to
                </label>
                <asp:DropDownList runat="server" ID="ddlRoundTo" CssClass="form-control">
                    <asp:ListItem Text="99" Value="1" />
                    <asp:ListItem Text="95" Value="2" />
                    <asp:ListItem Text="00" Value="3" />
                </asp:DropDownList>
            </div>
        </div>

        <asp:HiddenField ID="hdnRuleFor" runat="server" Value="1" />
    </div>
    <div class="col-md-12">
        <asp:Panel ID="pnlSel" runat="server" Visible="false">
            <div class="row" style="background-color: #dcdcdc; padding: 10px;">
                <label>Step 1: Select a pricing method</label>
            </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="row">
                    <asp:RadioButton ID="rbPricingTable" runat="server" CssClass="signup" GroupName="PricingMethod" />
                    <asp:HyperLink ID="hplPricingTable" CssClass="hyperlink" Text="<label for='rbPricingTable' style='cursor:pointer;'>Use Pricing Table</label>"
                        runat="server"> </asp:HyperLink>
                </div>
                <div class="row" id="trPricingFormula" runat="server">
                    <asp:RadioButton ID="rbPricingFormula" runat="server" CssClass="signup" GroupName="PricingMethod" />
                    <label for="rbPricingFormula">
                        Use Pricing Formula</label>
                </div>
                <div class="row" id="trPricingFormulaTable" runat="server" style="line-height: 45px;">
                    <div class="form-inline">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlRuleType" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Deduct from List price" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Add to Primary Vendor Cost" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        &nbsp;
                        <div class="form-group">
                            <asp:DropDownList runat="server" ID="ddlVendorCostType" Visible="false">
                                <asp:ListItem Text="Base on dynamic cost values" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Base on static cost values" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        &nbsp;the following&nbsp;
                    <div class="form-group">
                        <asp:DropDownList ID="ddlDiscountType" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Percentage" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Flat Amount" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                        <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control"></asp:TextBox>
                        &nbsp;for every&nbsp;<asp:TextBox ID="txtQntyItems" runat="server" CssClass="form-control"></asp:TextBox>&nbsp;qty of items sold
                    &nbsp;until maximum deduction / addition of&nbsp;
                                <asp:TextBox ID="txtMaxPerOrAmt" runat="server" CssClass="form-control"></asp:TextBox>
                        &nbsp;(% or flat amount)
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row" style="background-color: #dcdcdc; padding: 10px;">
                <label>Step 2: Select items</label>
            </div>


            <div class="col-md-8 col-md-offset-1">
                <div class="row">
                    <asp:RadioButton ID="rbIndividualItem" runat="server" CssClass="signup" GroupName="Items" />
                    <asp:HyperLink ID="hplIndividualItems" CssClass="hyperlink" Text="<label for='rbIndividualItem'>Apply to items individually</label>"
                        runat="server"> </asp:HyperLink>
                </div>
                <div class="row" id="trItemClassification" runat="server">
                    <asp:RadioButton ID="rbItemClassification" runat="server" CssClass="signup" GroupName="Items" />
                    <asp:HyperLink ID="hplClassification" CssClass="hyperlink" Text="<label for='rbItemClassification'>Apply to items with the selected item classifications </label>"
                        runat="server"> </asp:HyperLink>
                </div>
                <div class="row" id="trAllItems" runat="server">
                    <asp:RadioButton ID="rbAllItems" runat="server" CssClass="signup" GroupName="Items" />
                    <label for="rblAllItems">
                        All Items</label>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row" style="background-color: #dcdcdc; padding: 10px;">
                <label>
                    Step 3:
                    <asp:Label ID="lblStep3Caption" runat="server" Text="Select Leads, Prospects and Accounts"></asp:Label></label>
            </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="row">
                    <asp:RadioButton ID="rbIndividualOrg" runat="server" CssClass="signup" GroupName="Organization" />
                    <asp:HyperLink ID="hplIndividualOrg" CssClass="hyperlink" Text="<label for='rbIndividualOrg'>Apply to customers individually</label>"
                        runat="server"> </asp:HyperLink>
                </div>
                <div class="row" id="trRelProfile" runat="server">
                    <asp:RadioButton ID="rbRelProfile" runat="server" CssClass="signup" GroupName="Organization" />
                    <asp:HyperLink ID="hplRelProfile" CssClass="hyperlink" Text="<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles </label>"
                        runat="server"> </asp:HyperLink>
                </div>
                <div class="row" id="trAllOrg" runat="server">
                    <asp:RadioButton ID="rbAllOrg" runat="server" CssClass="signup" GroupName="Organization" />
                    <label for="rbAllOrg">
                        All Customers</label>
                </div>
            </div>
        </asp:Panel>
    </div>

    <table class="table table-responsive" border="0" style="border-collapse: collapse; float: left">
        <asp:Panel ID="pnlSel1" runat="server" Visible="false">
        </asp:Panel>
    </table>
</asp:Content>
