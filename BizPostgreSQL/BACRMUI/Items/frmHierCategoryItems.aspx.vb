Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class Items_frmHierCategoryItems
    Inherits BACRMPage
    Dim objItems As CItems

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnCategoryProfileID.Value = GetQueryStringVal("numCategoryProfileID")
                PopulateRootLevel()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub PopulateRootLevel()
        Try
            If objItems Is Nothing Then objItems = New CItems
            Dim dt As DataTable
            objItems.byteMode = 6
            objItems.DomainID = Session("DomainID")
            objItems.CategoryProfileID = CCommon.ToLong(hdnCategoryProfileID.Value)
            dt = objItems.SeleDelCategory
            PopulateNodes(dt, TreeView1.Nodes)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, ByVal nodes As TreeNodeCollection)
        Try
            For Each dr As DataRow In dt.Rows
                Dim tn As New TreeNode()
                tn.Text = dr("vcCategoryName").ToString()
                tn.Value = dr("numCategoryID").ToString()
                nodes.Add(tn)
                If CInt(dr("Count")) > 0 Then
                    PopulateSubLevel(dr("numCategoryID"), tn)
                    tn.ExpandAll()
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As Integer, ByVal parentNode As TreeNode)
        Try
            If objItems Is Nothing Then objItems = New CItems
            Dim dt As DataTable
            objItems.byteMode = 7
            objItems.DomainID = Session("DomainID")
            objItems.CategoryID = parentid
            dt = objItems.SeleDelCategory
            PopulateNodes(dt, parentNode.ChildNodes)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class