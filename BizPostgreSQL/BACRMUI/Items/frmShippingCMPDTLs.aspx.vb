Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class frmShippingCMPDTLs
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                objItem.ShippingCMPID = GetQueryStringVal("ListItemID")
                lblShippingCompany.Text = GetQueryStringVal("ListItem")
                Dim dtFields As DataTable = objItem.ShippingDtls.Tables(0)
                dgShipcmp.DataSource = dtFields
                dgShipcmp.DataBind()

                Dim index As Integer = 0
                For index = 0 To dtFields.Rows.Count - 1
                    If (CType(dgShipcmp.Items(index).FindControl("ddlFedExOption"), DropDownList).Visible = True) AndAlso dtFields.Rows(index)("vcFieldName") = "Rate Type" Then
                        CType(dgShipcmp.Items(index).FindControl("ddlFedExOption"), DropDownList).SelectedValue = dtFields.Rows(index)("vcShipFieldValue")
                    End If

                    If (CType(dgShipcmp.Items(index).FindControl("ddlImageTypeFedex"), DropDownList).Visible = True) AndAlso dtFields.Rows(index)("vcFieldName") = "Shipping Label Type" Then
                        CType(dgShipcmp.Items(index).FindControl("ddlImageTypeFedex"), DropDownList).SelectedValue = dtFields.Rows(index)("vcShipFieldValue")
                    End If

                    If (CType(dgShipcmp.Items(index).FindControl("ddlImageTypeUPS"), DropDownList).Visible = True) AndAlso dtFields.Rows(index)("vcFieldName") = "Shipping Label Type" Then
                        CType(dgShipcmp.Items(index).FindControl("ddlImageTypeUPS"), DropDownList).SelectedValue = dtFields.Rows(index)("vcShipFieldValue")
                    End If

                    If (CType(dgShipcmp.Items(index).FindControl("ddlImageTypeUSPS"), DropDownList).Visible = True) AndAlso dtFields.Rows(index)("vcFieldName") = "Shipping Label Type" Then
                        If Not CType(dgShipcmp.Items(index).FindControl("ddlImageTypeUSPS"), DropDownList).Items.FindByValue(dtFields.Rows(index)("vcShipFieldValue")) Is Nothing Then
                            CType(dgShipcmp.Items(index).FindControl("ddlImageTypeUSPS"), DropDownList).SelectedValue = dtFields.Rows(index)("vcShipFieldValue")
                        End If
                    End If
                Next
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtTable As New DataTable
            dtTable.TableName = "ShipDTL"
            Dim ds As New DataSet
            dtTable.Columns.Add("intShipFieldID")
            dtTable.Columns.Add("vcShipFieldValue")
            Dim dr As DataRow
            For Each dgGridShipItem As DataGridItem In dgShipcmp.Items
                dr = dtTable.NewRow
                dr("intShipFieldID") = dgGridShipItem.Cells(0).Text
                If CType(dgGridShipItem.FindControl("txtShipFieldValue"), TextBox).Visible = True Then
                    dr("vcShipFieldValue") = CType(dgGridShipItem.FindControl("txtShipFieldValue"), TextBox).Text.Trim
                ElseIf CType(dgGridShipItem.FindControl("ddlFedExOption"), DropDownList).Visible = True Then
                    dr("vcShipFieldValue") = CType(dgGridShipItem.FindControl("ddlFedExOption"), DropDownList).SelectedValue
                ElseIf CType(dgGridShipItem.FindControl("chkIsEndicia"), CheckBox).Visible = True Then
                    dr("vcShipFieldValue") = If(CType(dgGridShipItem.FindControl("chkIsEndicia"), CheckBox).Checked, 1, 0)
                ElseIf CType(dgGridShipItem.FindControl("ddlImageTypeFedex"), DropDownList).Visible = True Then
                    dr("vcShipFieldValue") = CType(dgGridShipItem.FindControl("ddlImageTypeFedex"), DropDownList).SelectedValue
                ElseIf CType(dgGridShipItem.FindControl("ddlImageTypeUPS"), DropDownList).Visible = True Then
                    dr("vcShipFieldValue") = CType(dgGridShipItem.FindControl("ddlImageTypeUPS"), DropDownList).SelectedValue
                ElseIf CType(dgGridShipItem.FindControl("ddlImageTypeUSPS"), DropDownList).Visible = True Then
                    dr("vcShipFieldValue") = CType(dgGridShipItem.FindControl("ddlImageTypeUSPS"), DropDownList).SelectedValue
                End If
                dtTable.Rows.Add(dr)
            Next
            ds.Tables.Add(dtTable)
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            objItem.ShippingCMPID = GetQueryStringVal( "ListItemID")
            objItem.str = ds.GetXml
            objItem.ManageShipDTLs()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgShipcmp_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgShipcmp.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                If CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "numListItemID")) = 90 AndAlso CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcFieldName")) = "Is Endicia" Then
                    Dim txtShipFieldValue As TextBox
                    txtShipFieldValue = e.Item.FindControl("txtShipFieldValue")
                    If txtShipFieldValue IsNot Nothing Then
                        txtShipFieldValue.Visible = False
                    End If

                    Dim chkIsEndicia As CheckBox
                    chkIsEndicia = e.Item.FindControl("chkIsEndicia")
                    If chkIsEndicia IsNot Nothing Then
                        chkIsEndicia.Visible = True
                        chkIsEndicia.Checked = CCommon.ToBool(CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "vcShipFieldValue")))
                    End If

                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class