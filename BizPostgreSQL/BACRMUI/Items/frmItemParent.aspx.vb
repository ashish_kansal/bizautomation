﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmItemParent
    Inherits BACRMPage

#Region "Member Variables"
    Private numItemCode As Long
    Private objItem As CItems
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                numItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))

                If numItemCode > 0 Then
                    BindGrid()
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindGrid()
        Try
            objItem = New CItems
            objItem.ItemCode = numItemCode
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objItem.GetItemChildMembershipDetails()
            dtParentDetail.DataSource = dt
            dtParentDetail.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

End Class