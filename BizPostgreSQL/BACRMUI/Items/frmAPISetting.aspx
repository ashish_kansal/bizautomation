﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAPISetting.aspx.vb"
    Inherits=".frmAPISetting" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>eCommerce API Settings</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style type="text/css">
        .info1
        {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position:10px 10px;
        }
        .info2
        {
            color: #000000;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #A2D9F5), color-stop(1, #D8ECF7));
            border: 1px solid;
            margin: 10px 5px 5px 5px;
            padding: 1px 1px 5px 90px;
            background-repeat: no-repeat;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function ValidateImportItem() {

            if (document.getElementById("chkEnable").checked == false) {
                alert("First enable API communication to import items");
                document.getElementById("chkEnable").focus();
                return false;
            }
            return true;
        }

        function ValidateSave() {
            if (document.getElementById("chkEnable").checked == true) {
                if (document.getElementById("hdnShippingServiceItemMapping").value == 0) {
                    alert("Please map a Shipping Service Item first., To map one, please navigate to Administration -> Global Settings -> Accounting Tab");
                    return false;
                }
                if (document.getElementById("hdnDiscountServiceItemMapping").value == 0) {
                    alert("Please map a Discount Service Item first., To map one, please navigate to Administration -> Global Settings -> Accounting Tab");
                    return false;
                }
               
                    if (document.getElementById("ddlRelationship").value == 0) {
                        alert("Select Relationship");
                        document.getElementById("ddlRelationship").focus();
                        return false;
                    }
                    if (document.getElementById("ddlWareHouse").value == 0) {
                        alert("Select default WareHouse");
                        document.getElementById("ddlWareHouse").focus();
                        return false;
                    }
                    if (document.getElementById("ddlWareHouse").value == 0) {
                        alert("Select default WareHouse");
                        document.getElementById("ddlWareHouse").focus();
                        return false;
                    }
                    if (document.getElementById("ddlBizDoc").value == 0) {
                        alert("Select BizDoc Type");
                        document.getElementById("ddlBizDoc").focus();
                        return false;
                    }
                    if (document.getElementById("ddlBizDocStatus").value == 0) {
                        alert("Select BizDoc Status");
                        document.getElementById("ddlBizDocStatus").focus();
                        return false;
                    }
                    if (document.getElementById("ddlOrderStatus").value == 0) {
                        alert("Select default Order Status");
                        document.getElementById("ddlOrderStatus").focus();
                        return false;
                    }
                    if (document.getElementById("ddlFBABizDoc") != null) {
                        if (document.getElementById("ddlFBABizDoc").value == 0) {
                            alert("Select default BizDoc for FBA Orders");
                            document.getElementById("ddlFBABizDoc").focus();
                            return false;
                        }

                        if (document.getElementById("ddlBizDoc").value == document.getElementById("ddlFBABizDoc").value) {
                            alert("You cannot map same BizDoc Type for FBM Orders and FBA Orders");
                            document.getElementById("ddlFBABizDoc").focus();
                            return false;
                        }
                    }
                    if (document.getElementById("ddlFBABizDocStatus") != null) {
                        if (document.getElementById("ddlFBABizDocStatus").value == 0) {
                            alert("Select default BizDoc status for FBA Orders");
                            document.getElementById("ddlFBABizDocStatus").focus();
                            return false;
                        }
                        if (document.getElementById("ddlFBABizDocStatus").value == document.getElementById("ddlBizDocStatus").value) {
                            alert("You cannot map same BizDoc Status for FBM Orders and FBA Orders");
                            document.getElementById("ddlFBABizDocStatus").focus();
                            return false;
                        }
                    }

                    if (document.getElementById("ddlUnShipmentOrderStatus") != null) {
                        if (document.getElementById("ddlUnShipmentOrderStatus").value == 0) {
                            alert("Select default Shipped Order Status");
                            document.getElementById("ddlUnShipmentOrderStatus").focus();
                            return false;
                        }
                    }

                    if (document.getElementById("ddlRecordOwner").value == 0) {
                        alert("Select default Record Owner");
                        document.getElementById("ddlRecordOwner").focus();
                        return false;
                    }
                    if (document.getElementById("ddlAssignTo").value == 0) {
                        alert("Select default Record Owner");
                        document.getElementById("ddlAssignTo").focus();
                        return false;
                    }
                   
                  
                    if (document.getElementById("ddlSalesTaxItemMapping").value == 0) {
                        alert("Select default Item to map Sales Tax amount as Order line Item");
                        document.getElementById("ddlSalesTaxItemMapping").focus();
                        return false;
                    }
            }
            return true;
        }
        function AlertShippingFromMissing() {
            var Message = "Shipping from address is missing for the Organization. Please provide Shipping Address in Organization Details.";
            alert(Message);
        }
    </script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server">
   
     <table cellspacing="0" cellpadding="0" width="100%">
    <tr><td align="right"><table><tr align="right">
             <td >
                <asp:Button ID="btnImportItems" runat="server" Visible="false" CssClass="button" Text="Import Items" />
            </td>
            <td >
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
            </td>
            <td>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="window.close();" />
            </td>
        </tr></table></td></tr>
        <tr><td> <asp:Label ID="lblErrorMessage" CssClass="errorInfo" runat="server" ForeColor="Crimson" Visible="False">Errors</asp:Label></td></tr>
    </table>
            
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    

    <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        CssClass="asptable" BorderColor="black" GridLines="None" Height="350" Width="800">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblFirstFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFirstFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblSecondFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSecondFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblThirdFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtThirdFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                            &nbsp;<asp:Button ID="btnGenToken" runat="server" Text="Generate Token" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblFourthFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFourthFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblFifthFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFifthFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblSixthFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSixthFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblSeventhFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSeventhFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblEighthFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEighthFieldValue" runat="server" CssClass="signup"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblNinthFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <BizCalendar:Calendar ID="calNinthFieldValue" runat="server" ClientIDMode="AutoID"
                                ShowTime="12" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblTenthFieldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <BizCalendar:Calendar ID="calTenthFieldValue" runat="server" ClientIDMode="AutoID"
                                ShowTime="12" />
                        </td>
                    </tr>
                      <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblSixteenthFldName" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <BizCalendar:Calendar ID="calSixteenthFldValue" runat="server" ClientIDMode="AutoID"
                                ShowTime="12" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblEnableItemUpdate" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                              <asp:Label ID="Label12" Text="[?]" CssClass="tip" runat="server" ToolTip="Enable/Disable updating Item Details (like Item Name, Description etc.,) to Marketplace" />

                            <asp:CheckBox Text="" runat="server" CssClass="signup" ID="chkEnableItemUpdate" />
                        </td>
                    </tr>
                     <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblEnableInventoryUpdate" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="Label13" Text="[?]" CssClass="tip" runat="server" ToolTip="Enable/Disable updating Item Inventory to Marketplace" />

                             <asp:CheckBox Text="" runat="server" CssClass="signup" ID="chkEnableInventoryUpdate" />
                        </td>
                    </tr>
                      <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblEnableOrdersImport" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                             <asp:Label ID="Label14" Text="[?]" CssClass="tip" runat="server" ToolTip="Enable/Disable importing Orders from Marketplace" />

                             <asp:CheckBox Text="" runat="server" CssClass="signup" ID="chkEnableOrdersImport" />
                        </td>
                    </tr>

                      <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblEnableTrackingUpdate" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="left">
                             <asp:Label ID="Label15" Text="[?]" CssClass="tip" runat="server" ToolTip="Enable/Disable updating Tracking Number to Marketplace for fulfilled Orders" />

                             <asp:CheckBox Text="" runat="server" CssClass="signup" ID="chkEnableTrackingUpdate" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            Enable?
                        </td>
                        <td align="left">
                            <asp:CheckBox Text="" runat="server" CssClass="signup" ID="chkEnable" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Relationship
                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Any relationship created from current Web API  will create record using default relationship set here" />
                        <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Profile
                            <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Any relationship created from current Web API will create record using default profile set here" />
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProfile" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Warehouse
                            <asp:Label ID="Label3" Text="[?]" CssClass="tip" runat="server" ToolTip="Any order created from current Web API will use default warehouse set here" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            BizDoc
                            <asp:Label ID="Label4" Text="[?]" CssClass="tip" runat="server" ToolTip="Create selected bizdoc when current API is used to create order" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlBizDoc" CssClass="signup"  AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                      <tr>
                        <td class="normal1" align="right">
                            BizDoc Status
                            <asp:Label ID="Label11" Text="[?]" CssClass="tip" runat="server" ToolTip="Create selected bizdoc when current API is used to create order" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlBizDocStatus" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Order Status
                            <asp:Label ID="Label5" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected order status when current API is used to create order" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlOrderStatus" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server" id="trFbaBizDoc" visible="false">
                        <td class="normal1" align="right">
                            FBA BizDoc
                            <asp:Label ID="Label17" Text="[?]" CssClass="tip" runat="server" ToolTip="Create selected bizdoc for FBA Orders" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlFBABizDoc" CssClass="signup" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                      <tr runat="server" id="trFbaBizDocStatus" visible="false">
                        <td class="normal1" align="right">
                           FBA BizDoc Status
                            <asp:Label ID="Label18" Text="[?]" CssClass="tip" runat="server" ToolTip="Create selected bizdoc Status for FBA Orders" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlFBABizDocStatus" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server" id="trFbaOrderStatus" visible="false">
                        <td class="normal1" align="right">
                         FBA Order Status
                            <asp:Label ID="Label10" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected order status to Orders already Fulfilled by Marketplace providers" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlUnShipmentOrderStatus" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Record Owner
                            <asp:Label ID="Label6" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected record owner when current API is used to create order" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlRecordOwner" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Assign To
                            <asp:Label ID="Label7" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected record as assign to when current API is used to create order" />
                        <font color="red">*</font>
                             </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlAssignTo" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td class="normal1" align="right">
                            Expense Account
                            <asp:Label ID="Label8" Text="[?]" CssClass="tip" runat="server" ToolTip="set selected Expense Account as assign to when current API is used to create order" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlExpenseAccount" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                   <%-- <tr>
                        <td class="normal1" align="right">
                            Discount Item Mapping
                            <asp:Label ID="Label9" Text="[?]" CssClass="tip" runat="server" ToolTip="Maps Order Discount amount as Order line Item with the selected Item" />
                        <font color="red">*</font>
                             </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlDiscountItemMapping" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                     <tr>
                        <td class="normal1" align="right">
                            Sales Tax Item Mapping
                            <asp:Label ID="Label16" Text="[?]" CssClass="tip" runat="server" ToolTip="Maps Order Sales Tax amount as Order line Item with the selected Item" />
                         <font color="red">*</font>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddlSalesTaxItemMapping" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="hdnShippingServiceItemMapping"/>
                 <asp:HiddenField runat="server" ID="hdnDiscountServiceItemMapping"/>
            </asp:TableCell></asp:TableRow>
        <asp:TableRow runat="server" ID="AmazonNotes" Visible="false" >
            <asp:TableCell ColumnSpan="2">
                <asp:Panel runat="server" Visible="true" ID="pnlAmazonNotes" CssClass="info1">
                    <b>Notes to Configure Amazon Marketplace</b><br />
                    <ol>
                       <li>Navigate to <a href="http://developer.amazonservices.com" target="_blank">http://developer.amazonservices.com</a>. Click the <b>Sign up for MWS button</b>.</li><br />
                        <li>Log into your Amazon seller account.</li><br />
                        <li>On the MWS registration page, click the option for <b>I want to give a developer access to my Amazon seller account with MWS</b>.</li><br />
                        <li>In the Developer's Name text box, type in <b>“BizAutomation”</b> .</li><br />
                        <li>In the Developer Account Number text box, type in <b>“8220-5889-4552”</b> and Click the <b>Next</b> button.</li><br />
                        <li>Accept the Amazon MWS License Agreements and click the <b>Next</b> button.</li><br />
                        <li>Copy your account identifiers (<b>Merchant ID</b> and <b>Marketplace ID</b>) </li><br />
                        <li><b>AWS Access Key</b> and <b>Secret Key</b> fields can be left empty</li><br />
                        <li>For Marketplace, fill in the Amazon Country Code of your Seller Account., example, <b>United States: US, United Kingdom : UK </b>etc.</li><br />
                        <li><b>Merchant Token</b> can be get from your Amazon Seller Central by navigating to Settings-> Account Info</li><br />
                        <li>Fill in BizAutomation specific details like Relationship, Profile, Warehouse, BizDoc, BizDoc Status, FBA BizDoc, FBA BizDoc Status, Order Status, Record Owner, Assign To, Sales Tax Item Mapping etc. (Every fields are Mandatory).</li><br />
                        <li>Check the Enable? checkbox and click <b>Save</b> button</li><br />
                        <li>Click here for <a runat="server" href="https://docs.google.com/a/bizautomation.com/file/d/0B-YPb0ShZeFjdVZOalhPRVRqRFU/edit?usp=sharing" target="_blank">more Information</a></li>
                    </ol>
                   * mail us at support@bizautomation.com for any inquires.
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="EbayNotes" Visible="false">
            <asp:TableCell ColumnSpan="2">
                <asp:Panel runat="server" Visible="true" ID="pnlEbayNotes" CssClass="info1">
                    <b>Notes to Configure E-Bay Marketplace</b><br />
                    <ol>
                        <li>Enter the Country Code for Site Code and Marketplace. (for example : United States : US).</li><br />
                        <li>Enter Paypal Email address</li><br />
                        <li>To generate E-Bay API Merchant Token navigate to <a target="_blank" href="https://developer.ebay.com/base/membership/signin/default.aspx"> https://developer.ebay.com/base/membership/signin/default.aspx </a></li><br />
                        <li>Login in with the credential<br /> &#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;<b>User Name : </b>joseph2012<br />  &#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;<b>Password : </b>Joseph321</li><br />
                        <li>Click <b><u> Generate User Token link </u></b></li><br />
                        <li>Select Production in Select Environment dropdown</li><br />
                        <li>Select Key Set 1 in Select a Key set dropdown</li><br />
                        <li>Click <b>Continue to Generate token</b></li><br />
                        <li>Sign in to your e-Bay Merchant Account and Proceed with requested credentials.</li><br />
                        <li>Enter you E-Bay API Merchant Token in Merchant Token field</li><br />
                        <li>Fill in BizAutomation specific details like Relationship, Profile, Warehouse, BizDoc, BizDoc Status, Order Status, Record Owner, Assign To, Sales Tax Item Mapping etc. (Every fields are Mandatory).</li><br />
                         <li>Check the Enable? checkbox and click <b>Save</b> button</li><br />
                        <li>Click here for <a href="https://docs.google.com/a/bizautomation.com/file/d/0B-YPb0ShZeFjdVZOalhPRVRqRFU/edit?usp=sharing" target="_blank">more Information</a></li>
                    </ol>
                   * mail us at support@bizautomation.com for any inquires.
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="GoogleNotes" Visible="false">
            <asp:TableCell ColumnSpan="2">
                <asp:Panel runat="server" Visible="true" ID="pnlGoogleNotes" CssClass="info1">
                    <b>Note to Configure Google Marketplace</b><br />
                    <ol>
                        <li>Enter the Country Code of the Site Code. (For example: United States: US).</li><br />
                        <li>Navigate to Google Merchant Center<a href="http://www.google.com/merchants/" target="_blank">http://www.google.com/merchants/</a> and Sign in to your Google Merchant Center Account to get you Google Merchant Center ID</li><br />
                        <li>Enter your Google Merchant Center ID in Merchant Account ID field and your Merchant login Email and password in User Login Mail Id and Password field respectively.</li><br />
                        <li>The Website URL registered in Google Merchant Center must be verified and claimed in order to Add product Listing to Google Merchant Center. To do so., you can navigate to Settings -> General and under Website URL click the link Verify this URL. </li><br />
                        <li>Navigate to Google Checkout Merchant Login  <a href="http://checkout.google.com/sell/" target="_blank">http://checkout.google.com/sell/</a>  and Sign in to your Google Checkout Account</li><br />
                        <li>Navigate to <b>Settings -> Integration </b> and under <b>Account Information</b> you can find your <b>Google Checkout Merchant ID </b>and the <b>Google Checkout Merchant Key</b>.</li><br />
                        <li>Fill in your Google Checkout Merchant Id and Google Checkout Merchant Key in Respective fields.</li><br />
                        <li>Fill in BizAutomation specific details like Relationship, Profile, Warehouse, BizDoc, BizDoc Status, Order Status, Record Owner, Assign To, Sales Tax Item Mapping etc. (Every fields are Mandatory).</li><br />
                         <li>Check the Enable? checkbox and click <b>Save</b> button</li><br />
                        <li>Click here for <a href="https://docs.google.com/a/bizautomation.com/file/d/0B-YPb0ShZeFjdVZOalhPRVRqRFU/edit?usp=sharing" target="_blank">more Information</a></li>
                    </ol>
                   * mail us at support@bizautomation.com for any inquires.
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="MagentoNotes" Visible="false">
            <asp:TableCell ColumnSpan="2">
                <asp:Panel runat="server" Visible="true" ID="pnlMagentoNotes" CssClass="info1">
                    <b>Note to Configure Magento Marketplace</b><br />
                    <ol>
                        <li>Enter your Magento enabled Domain URL (for example: www.domainname.com).</li><br />
                        <li>Generate API Role. Navigate to <b>System -> Web Services -> SOAP/XML-RPC Roles</b> from your Magento Admin panel.</li><br />
                        <li>Click <b>Add New Role</b>. In <b>Role Information</b> Add a name to the Role and in <b>Role Resources</b> select Resource Access to <b>All</b> and Click <b>Save Role</b>.</li><br />
                        <li>Generate API User. Navigate to <b>System -> Web Services -> SOAP/XML-RPC Users </b>from your Magento Admin panel.</li><br />
                        <li>Click <b>Add New User</b>. In User Information fill in the User Name, First Name, Last Name, Email, API Key and Select <b>Active</b> from “This account is” dropdown. And in<b> User Role</b> select the Role created with Resources access to All and Click <b>Save User</b>. (Having a note of the credentials entered might help for future reference). </li><br />
                        <li>Fill in the User Name and the API Key in respective Textbox.</li><br />
                        <li>Fill in BizAutomation specific details like Relationship, Profile, Warehouse, BizDoc, BizDoc Status, Order Status, Record Owner, Assign To, Sales Tax Item Mapping etc. (Every fields are Mandatory).</li><br />
                         <li>Check the Enable? checkbox and click <b>Save</b> button</li><br />
                        <li>Click here for <a href="https://docs.google.com/a/bizautomation.com/file/d/0B-YPb0ShZeFjdVZOalhPRVRqRFU/edit?usp=sharing" target="_blank">more Information</a></li>
                    </ol>
                   * mail us at support@bizautomation.com for any inquires.
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
          <asp:TableRow runat="server" ID="TableRow1">
            <asp:TableCell ColumnSpan="2">
                <asp:Panel runat="server" Visible="false" ID="Panel1" CssClass="info2">
                  <br />
                    <ul>
                        <li>  &#160;&#160; Click here for <a href="https://docs.google.com/a/bizautomation.com/file/d/0B-YPb0ShZeFjdVZOalhPRVRqRFU/edit?usp=sharing" target="_blank">more Information</a></li><br />
                        <li>* mail us at support@bizautomation.com for any inquires.</li>
                    </ul>
                    
                    <br />
                     </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
 </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label runat="server" ID="lblPageTitle"></asp:Label> API Settings </asp:Content>