<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSelectKits.aspx.vb" Inherits="BACRM.UserInterface.Items.frmSelectKits"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Select Kits</title>
		<script language="javascript">
		function Close()
		{
			window.close()
			return false;
		}

		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
					<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Select Kit 
									Parents&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close"></asp:button></td>
				</tr>
			</table>
			<asp:table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None" Height="300">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table width="100%" align="center">
							<tr>
								<td class="normal1" align="right">
									Kit Parent
								</td>
								<td>
									<asp:DropDownList ID="ddlKit" Runat="server" CssClass="signup"></asp:DropDownList>
								</td>
								<td class="normal1" align="right">
									Qty of Child Items Req for kit
								</td>
								<td>
									<asp:TextBox ID="txtNoOfChilItems" Width=40 Runat="server" CssClass="signup"></asp:TextBox>
								</td>
								<td>
									<asp:Button ID="btnAdd" Runat="server" Width="50" Text="Add" CssClass="button"></asp:Button>
								</td>
							</tr>
						</table>
						<br>
						<table width="100%" CellPadding="0" CellSpacing="0">
							<tr>
								<td>
									<asp:datagrid id="dgKitParents" runat="server" CssClass="dg"  AutoGenerateColumns="False"
										AllowSorting="True" Width="100%">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
													<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:Label ID="lblKitParentID" Runat="server" Visible=False Text= '<%# DataBinder.Eval(Container,"DataItem.numItemKitID") %>'>
													</asp:Label>
													<%# Container.ItemIndex +1 %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Parent Item">
												<ItemTemplate>
													<asp:Label ID="lblKitName" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.KitParent") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblKitId" Visible=False Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.numItemKitID") %>'>
													</asp:Label>
													<asp:DropDownList ID="ddlKits" Runat="server" CssClass="signup"></asp:DropDownList>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Qty of Child Items Req for kit">
												<ItemTemplate>
													<asp:Label ID="lblNoofChildItems" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.numQtyItemsReq") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox ID="txtQuantity" Width=40 Runat="server" CssClass="signup" Text= '<%# DataBinder.Eval(Container,"DataItem.numQtyItemsReq") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
												<HeaderTemplate>
													<asp:Button ID="btnHdelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
