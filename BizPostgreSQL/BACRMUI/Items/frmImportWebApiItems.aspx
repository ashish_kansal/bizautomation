﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImportWebApiItems.aspx.vb"
    Inherits=".frmImportWebApiItems" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Import Online Marketplace Item</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function ValidateImportItem() {

            if (document.getElementById("ddlWebApi").value == 0) {
                alert("First Select a Marketplace to import items");
                document.getElementById("ddlWebApi").focus();
                return false;
            }
            if (document.getElementById("ddlListingType").value == 0) {
                alert("please Select Listing Type to import items");
                document.getElementById("ddlListingType").focus();
                return false;
            }
            
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager runat="server">

    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="right"></td>
        </tr>

    </table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <asp:UpdatePanel runat="server" ID="upnlImportItems">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnImportItems" EventName="Click" />
        </Triggers>
        <ContentTemplate>
              <table style="vertical-align:top;">
        <tr>
            <td colspan="3">
                <table>
                    <tr> <td>
                 <asp:Label runat="server" ID="lblWebAPI" Text="Market Place : "></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlWebApi"></asp:DropDownList>
            </td>
                        <td>
                <asp:DropDownList runat="server" ID="ddlListingType">
                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Complete" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td><asp:Button CssClass="button" runat="server" ID="btnImportItems" Text="Import Items"></asp:Button></td></tr>
                </table>
            </td>
           
        </tr>
          <tr>
            <td colspan="3">
                 <b> Notes :</b>  
            <ol start="1">
                <li>On clicking Import Items button, a request will be sent to respective Online Marketplace to get the Item listings </li>
                <li>Item details will be added into BizAutomation on receiving the product listing report from respective Marketplace </li>
                <li>Item Details for SKU already exists in BizAutomation will be Updated</li>
                <li>You can find the Imported item list under BizAutomation Item Classifications as "MarketplaceName_Import_ddmmmyyyy"</li>
                <li>Once Items are added/Updated in BizAutomation, an auto generated mail will be sent to the User email requested for Items Import with Imported Items list</li>
            </ol>
                   
            </td>
        </tr>
         <tr>
            <td>
             
            </td>
        </tr>
         <tr>
            <td>
            </td>
        </tr>
    </table>
    
  
        </ContentTemplate>
    </asp:UpdatePanel>
  
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label runat="server" ID="lblPageTitle"></asp:Label>Import API Items
</asp:Content>
