Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports System.Reflection
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Imports BACRM.BusinessLogic.Reports
Partial Public Class frmImportFiles
    Inherits BACRMPage

#Region "Decleration"
    Dim strFileName As String  'Variable to hold the FileName string
    Dim lngDivisionID As Long
    Dim lngContactID As Long
    Dim txtState As String
    Dim txtCountry As String
    Dim cmbAnnualRevenue As String
    Dim cmbEmployees As Long
    Dim strUpdateValues As String
    Dim Mode As Short

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Mode = GetQueryStringVal( "Mode")
            If Not IsPostBack Then
                'If GetQueryStringVal( "ShowCustom") = "true" Then
                '    trImport.Visible = True
                '    btnImportForHitech.Visible = True
                'End If
                pgBar.Style.Add("display", "none")
                 ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Import"


            End If
            If CCommon.ToLong(Session("DomainID")) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")

            If Session("FileLocation") <> "" Then
                displayDropdowns()
            End If
            If Mode = 1 Then
                caption.InnerHtml = "&nbsp;&nbsp;&nbsp;Import Wizard&nbsp;&nbsp;&nbsp;"
                btnImports.Text = "Import records to database"

                GetUserRightsForPage(37, 31)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                End If
                ValidateMandatoryColumns() 'Only validate for import

                btnConfg.Attributes.Add("onClick", "return OpenConf('2')")
            ElseIf Mode = 2 Then
                caption.InnerHtml = "&nbsp;&nbsp;&nbsp;Update Wizard&nbsp;&nbsp;&nbsp;"
                btnImports.Text = "Update records to database"
                lbDownloadCSV.Text = "Download Existing Items as CSV"

                GetUserRightsForPage(37, 32)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                End If

                btnConfg.Attributes.Add("onClick", "return OpenConf('4')")
            End If
            Page.Title = caption.InnerHtml
            btnUpload.Attributes.Add("onClick", "return checkFileExt()")
            btnDisplay.Attributes.Add("onClick", "return setDllValues()")
            btnImports.Attributes.Add("onClick", "return displayPBar()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Display Dropdowns"

    Private Sub displayDropdowns()
        Try
            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As New StreamReader(fileLocation)
            Dim intCount As Integer = 0
            Dim intCountFor As Integer = 0
            Dim intStart As Integer
            Dim strSplitValue() As String
            Dim strSplitHeading() As String
            Dim strLine As String = streamReader.ReadLine()
            Dim dataTableMap As New DataTable
            dataTableMap.Columns.Add("Destination")
            dataTableMap.Columns.Add("ID")
            Try
                intStart = 0
                'If Mode = 1 Then 'Import Items
                '    intStart = 0
                'ElseIf Mode = 2 Then 'Update Items
                '    intStart = 1
                'End If

                Do While Not strLine Is Nothing
                    Dim dataRowMap As DataRow
                    dataRowMap = dataTableMap.NewRow
                    strSplitHeading = Split(strLine, ",")
                    For intCountFor = intStart To strSplitHeading.Length - 1
                        dataRowMap = dataTableMap.NewRow
                        dataRowMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                        dataRowMap("ID") = intCountFor
                        dataTableMap.Rows.Add(dataRowMap)
                    Next
                    Exit Do
                Loop

                Dim trHeader As New TableRow
                Dim trDetail As New TableRow
                Dim tableHC As TableHeaderCell
                Dim tableDtl As TableCell
                Dim drHeading As DataRow

                intCount = 0
                dataTableHeading = Session("dataTableHeadingObj")

                Dim ddlDestination As DropDownList
                Dim txtDestination As TextBox
                Dim arryList() As String
                Dim strdllValue As String
                Dim intArryValue As Integer

                If Session("dllValue") <> "" Then
                    strdllValue = Session("dllValue")
                    arryList = Split(strdllValue, ",")
                End If

                tbldtls.Rows.Clear()
                'intCount = 2

                For Each drHeading In dataTableHeading.Rows
                    trHeader.CssClass = "hs"
                    tableHC = New TableHeaderCell
                    tableHC.ID = intCount
                    tableHC.Text = drHeading.Item(1)
                    tableHC.CssClass = "normal5"
                    trHeader.Cells.Add(tableHC)
                    tableDtl = New TableCell
                    ddlDestination = New DropDownList
                    txtDestination = New TextBox
                    ddlDestination.CssClass = "signup"
                    ddlDestination.ID = "ddlDestination" & intCount.ToString
                    ddlDestination.DataSource = dataTableMap
                    ddlDestination.DataTextField = "Destination"
                    ddlDestination.DataValueField = "ID"
                    ddlDestination.DataBind()
                    If Session("dllValue") <> "" Then
                        If arryList.Length > 9 Then
                            intArryValue = CType(arryList(intCount), Integer)
                            ddlDestination.SelectedIndex = intArryValue
                        ElseIf ddlDestination.Items.Count > intCount Then
                            ddlDestination.SelectedIndex = intCount
                        End If
                    ElseIf ddlDestination.Items.Count > intCount Then
                        ddlDestination.SelectedIndex = intCount
                    End If

                    If Mode = 1 Then 'Import Items
                        tableDtl.Controls.Add(ddlDestination)
                    ElseIf Mode = 2 Then ' Update Items
                        'tableDtl.Text = dataTableMap.Rows(intCount).Item(0)
                        'txtDestination.Text = dataTableMap.Rows(intCount).Item(0)
                        'txtDestination.ReadOnly = True
                        'txtDestination
                        'tableDtl.Controls.Add(txtDestination)
                        'ddlDestination.Visible = False
                        'ddlDestination.Enabled = False

                        tableDtl.Controls.Add(ddlDestination)
                    End If

                    trDetail.Cells.Add(tableDtl)
                    intCount += 1
                    trDetail.CssClass = "is"
                    tbldtls.Rows.Add(trDetail)
                    tbldtls.Rows.AddAt(0, trHeader)
                Next

                Session("IntCount") = intCount - 1
                If txtDllValue.Text <> "" Then Session("dllValue") = txtDllValue.Text
                txtDllValue.Text = intCount - 1

            Catch ex As Exception
                Throw ex
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayUploadFields()
        Try

            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As New StreamReader(fileLocation)
            Dim intCount As Integer = 0
            Dim intCountFor As Integer = 0
            Dim strSplitValue() As String
            Dim strSplitHeading() As String
            Dim strLine As String = streamReader.ReadLine()
            Dim dataTableMap As New DataTable

            dataTableMap.Columns.Add("Destination")
            dataTableMap.Columns.Add("ID")

            Try
                Do While Not strLine Is Nothing
                    Dim dataRowMap As DataRow
                    dataRowMap = dataTableMap.NewRow
                    strSplitHeading = Split(strLine, ",")
                    For intCountFor = 2 To strSplitHeading.Length - 1
                        dataRowMap = dataTableMap.NewRow
                        dataRowMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                        dataRowMap("ID") = intCountFor
                        dataTableMap.Rows.Add(dataRowMap)
                    Next
                    Exit Do
                Loop

                Dim trHeader As New TableRow
                Dim trDetail As New TableRow
                Dim tableHC As TableHeaderCell
                Dim tableDtl As TableCell
                Dim drHeading As DataRow

                intCount = 0
                dataTableHeading = Session("dataTableHeadingObj")

                Dim ddlDestination As DropDownList
                Dim arryList() As String
                Dim strdllValue As String
                Dim intArryValue As Integer

                If Session("dllValue") <> "" Then
                    strdllValue = Session("dllValue")
                    arryList = Split(strdllValue, ",")
                End If

                tbldtls.Rows.Clear()

                For Each drHeading In dataTableHeading.Rows
                    trHeader.CssClass = "hs"
                    tableHC = New TableHeaderCell
                    tableHC.ID = intCount
                    tableHC.Text = drHeading.Item(1)
                    tableHC.CssClass = "normal5"
                    trHeader.Cells.Add(tableHC)
                    tableDtl = New TableCell
                    tableDtl.Text = strSplitHeading(intCount + 2)
                    trDetail.Cells.Add(tableDtl)
                    intCount += 1
                    trDetail.CssClass = "is"
                    tbldtls.Rows.Add(trDetail)
                    tbldtls.Rows.AddAt(0, trHeader)
                Next

                Session("IntCount") = intCount - 1

                If txtDllValue.Text <> "" Then Session("dllValue") = txtDllValue.Text

                txtDllValue.Text = intCount - 1

            Catch ex As Exception
                Throw ex
            Finally
                streamReader.Close()
            End Try

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Display Click"
    Private Sub btnDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As StreamReader
            streamReader = File.OpenText(fileLocation)
            Dim intCount As Integer = Session("IntCount")
            Dim intCountFor As Integer = 0
            Dim strSplitValue() As String
            Dim strSplitHeading() As String
            ' Dim strLine As String = streamReader.ReadLine()
            Dim arryList() As String
            Dim strdllValue As String
            Dim intArryValue As Integer
            Dim intCountSession As Integer = Session("IntCount")
            dataTableHeading = Session("dataTableHeadingObj")
            dataTableDestination = Session("dataTableDestinationObj")
            dataTableDestination.Clear()
            Dim csv As CsvReader = New CsvReader(streamReader, True)
            Try
                csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                csv.SupportsMultiline = True

                strdllValue = Session("dllValue")
                arryList = Split(strdllValue, ",")
                'strLine = streamReader.ReadLine()
                'Do While Not strLine Is Nothing
                '    Dim dataRowDestination As DataRow
                '    dataRowDestination = dataTableDestination.NewRow
                '    strSplitValue = Split(strLine, ",")
                '    dataTableHeading = Session("dataTableHeadingObj")
                '    Dim drHead As DataRow
                '    intCount = 0
                '    For Each drHead In dataTableHeading.Rows
                '        intArryValue = CType(arryList(intCount), Integer)
                '        dataRowDestination(drHead.Item(1)) = strSplitValue(intArryValue).ToString
                '        intCount += 1
                '    Next
                '    dataTableDestination.Rows.Add(dataRowDestination)
                '    strLine = streamReader.ReadLine()
                'Loop

                'Dim headers As String() = csv.GetFieldHeaders()
                'Dim i As Integer
                Dim dataRowDestination As DataRow
                ''For i = 0 To csv.FieldCount - 1
                ''    dataTableDestination.Columns.Add(headers(i))
                ''    'Response.Write(String.Format("{0} = {1};", headers(i), csv(i)))
                ''Next
                Dim drHead As DataRow
                dataTableHeading = Session("dataTableHeadingObj")
                Dim i1000 As Integer = 0
                While (csv.ReadNextRecord())
                    dataRowDestination = dataTableDestination.NewRow
                    intCount = 0
                    For Each drHead In dataTableHeading.Rows
                        intArryValue = CType(arryList(intCount), Integer)
                        dataRowDestination(drHead.Item(1)) = csv(intArryValue).ToString
                        intCount += 1
                    Next
                    dataTableDestination.Rows.Add(dataRowDestination)
                    i1000 = i1000 + 1

                End While

                Session("dataTableDestinationObj") = dataTableDestination

                Dim drDetination As DataRow
                Dim iCount As Integer = 0
                Dim iCountRow As Integer = 1
                Dim dtCol As DataColumn
                Dim trDetail As TableRow
                Dim tableCell As TableCell
                'tbldtls.Rows.Clear()
                Dim k As Integer = 0
                For Each drDetination In dataTableDestination.Rows
                    trDetail = New TableRow
                    If k = 0 Then
                        trDetail.CssClass = "ais"
                        k = 1
                    Else
                        trDetail.CssClass = "is"
                        k = 0
                    End If
                    For iCount = 1 To dataTableDestination.Columns.Count - 1
                        tableCell = New TableCell
                        tableCell.Text = drDetination.Item(iCount)
                        tableCell.CssClass = "normal1"
                        trDetail.Cells.Add(tableCell)
                    Next
                    iCountRow += 1
                    tbldtls.Rows.AddAt(iCountRow, trDetail)
                    If iCountRow = 100 Then
                        Exit For
                    End If

                Next
            Catch ex As Exception
                If ex.Message = "An item with the same key has already been added." Then
                    streamReader.Close()
                    litMessage.Text = "Duplicate column names found in CSV file, Your option is to remove duplicate columns and upload it again."
                    Exit Sub
                End If
                Response.Write(ex)
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
#End Region

#Region "Create Table Schema"
    Private Sub createTableSchema()
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim dataTable As New DataTable
            Dim numRows As Integer
            Dim i As Integer
            dataTableHeading.Columns.Add("Id")
            dataTableHeading.Columns.Add("Destination")
            dataTableHeading.Columns.Add("FType")
            dataTableHeading.Columns.Add("cCtype")
            dataTableHeading.Columns.Add("vcAssociatedControlType")
            dataTableHeading.Columns.Add("vcDbColumnName")
            dataTableHeading.Columns.Add("numListID")
            Dim dataRowHeading As DataRow

            If Mode = 2 Then
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = "Item ID"
                dataRowHeading("Id") = 1
                dataRowHeading("FType") = 0
                dataRowHeading("cCtype") = "V"
                dataRowHeading("vcAssociatedControlType") = "TextBox"
                dataRowHeading("vcDbColumnName") = "numItemCode"
                dataRowHeading("numListID") = 0
                dataTableHeading.Rows.Add(dataRowHeading)
            End If

            Dim objImport As New ImportWizard
            objImport.DomainId = Session("DomainID")
            objImport.Relationship = 0
            If Mode = 1 Then
                objImport.ImportType = 2
            Else
                objImport.ImportType = 4
            End If
            dataTable = objImport.GetConfigurationTable()

            numRows = dataTable.Rows.Count()

            For i = 0 To numRows - 1
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = dataTable.Rows(i).Item("vcFormFieldName")
                dataRowHeading("Id") = dataTable.Rows(i).Item("numFormFieldId")
                dataRowHeading("FType") = IIf(dataTable.Rows(i).Item("bitCustomFld") = "False", "0", "1")
                dataRowHeading("cCtype") = dataTable.Rows(i).Item("cCtype")
                dataRowHeading("vcAssociatedControlType") = dataTable.Rows(i).Item("vcAssociatedControlType")
                dataRowHeading("vcDbColumnName") = dataTable.Rows(i).Item("vcDbColumnName")
                dataRowHeading("numListID") = dataTable.Rows(i).Item("numListID")
                dataTableHeading.Rows.Add(dataRowHeading)
            Next

            Session("dataTableHeadingObj") = dataTableHeading
            Dim drHead As DataRow
            dataTableDestination.Columns.Add("SlNo")
            dataTableDestination.Columns("SlNo").AutoIncrement = True
            dataTableDestination.Columns("SlNo").AutoIncrementSeed = 1
            dataTableDestination.Columns("SlNo").AutoIncrementStep = 1
            For Each drHead In dataTableHeading.Rows
                dataTableDestination.Columns.Add(drHead.Item(1))
            Next
            Session("dataTableDestinationObj") = dataTableDestination
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Upload Click"
    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                pgBar.Style.Add("display", "")
                Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)
                Dim SaveLocation As String = Server.MapPath("../Documents/Docs") & "\" & fileName
                Try
                    txtFile.PostedFile.SaveAs(SaveLocation)
                    Session("FileLocation") = SaveLocation
                    litMessage.Text = "The file has been uploaded"
                    createTableSchema()
                    displayDropdowns()
                    pgBar.Style.Add("display", "none")
                Catch ex As Exception
                    pgBar.Style.Add("display", "none")
                    Throw ex
                End Try
            Else
                Session("IntCount") = ""
                Session("FileLocation") = ""
                Session("dataTableDestinationObj") = ""
                Session("dataTableHeadingObj") = ""
                Session("dllValue") = ""
                tbldtls.Rows.Clear()
                litMessage.Text = "Please select a file to upload."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Imports Click"
    Private Sub btnImports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImports.Click
        Dim objItems As New CItems
        Dim objImpWzd As New ImportWizard
        Try
            Dim ds As New DataSet
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim lngItemID As Long
            dataTableDestination = Session("dataTableDestinationObj")
            dataTableHeading = Session("dataTableHeadingObj")
            Dim drDestination As DataRow
            Dim drHead As DataRow
            Dim intCount As Integer = 0

            Dim strItemType As String
            Dim blnAsset As Boolean
            Dim blnVendor As Boolean
            Dim blnWareHouse As Boolean
            Dim blnUpdatingCategory As Boolean

            Dim dtVendorDetails As New DataTable
            dtVendorDetails.Columns.Add("VendorID")
            dtVendorDetails.Columns.Add("PartNo")
            dtVendorDetails.Columns.Add("monCost")
            dtVendorDetails.Columns.Add("intMinQty")
            dtVendorDetails.TableName = "Table"

            Dim dtWareHouse As New DataTable
            dtWareHouse.Columns.Add("numWareHouseID")
            dtWareHouse.Columns.Add("numOnHand")
            dtWareHouse.Columns.Add("vcLocation")
            dtWareHouse.Columns.Add("monWListPrice")
            dtWareHouse.TableName = "Table"



            Dim strEditedfldlst As String = ""
            Dim dsVendor As New DataSet

            Dim drVendor As DataRow
            Dim drWareHouse As DataRow

            ds.Tables.Add(dtVendorDetails)
            strEditedfldlst = ds.GetXml()
            ds.Tables.Remove(dtVendorDetails)

            For Each drDestination In dataTableDestination.Rows
                dtVendorDetails.Clear()
                dtWareHouse.Clear()
                objItems.ItemCode = 0
                Dim vcFieldtype As Char
                Dim vcAssociatedControlType As String
                strItemType = ""

                blnAsset = False
                blnVendor = False
                blnWareHouse = False
                blnUpdatingCategory = False

                strEditedfldlst = ""
                drVendor = dtVendorDetails.NewRow
                drVendor("VendorID") = 0
                drVendor("PartNo") = ""
                drVendor("monCost") = 0
                drVendor("intMinQty") = 0
                dtVendorDetails.Rows.Add(drVendor)

                drWareHouse = dtWareHouse.NewRow
                drWareHouse("numWareHouseID") = 0
                drWareHouse("numOnHand") = 0
                drWareHouse("vcLocation") = ""
                drWareHouse("monWListPrice") = 0
                dtWareHouse.Rows.Add(drWareHouse)

                If Mode = 2 Then
                    objItems.ItemCode = CCommon.ToInteger(drDestination("Item ID"))
                End If

                strUpdateValues = "UPDATE ITEM SET [bintModifiedDate] = GETUTCDATE() ,[numModifiedBy] =" & Session("UserContactID") & " ,"
                For Each dr As DataRow In dataTableHeading.Rows
                    vcFieldtype = dr("cCtype")
                    If vcFieldtype = "R" Then
                        vcAssociatedControlType = dr("vcAssociatedControlType")
                        If CCommon.ToString(drDestination(dr("Destination").ToString)).Trim.Length > 0 Then
                            Select Case vcAssociatedControlType
                                Case "TextBox"
                                    If dr("vcDbColumnName").IndexOf("charItemType") > -1 Then
                                        strItemType = IIf(IsDBNull(drDestination(dr("Destination").ToString)), "Non-Inventory", drDestination(dr("Destination").ToString))
                                        Select Case strItemType.ToLower
                                            Case "asset"
                                                drDestination(dr("Destination").ToString) = "A"
                                            Case "inventory item"
                                                drDestination(dr("Destination").ToString) = "P"
                                            Case "service"
                                                drDestination(dr("Destination").ToString) = "S"
                                            Case Else
                                                drDestination(dr("Destination").ToString) = "N"
                                        End Select
                                    ElseIf dr("vcDbColumnName").IndexOf("vcPartNo") > -1 Then
                                        dtVendorDetails.Rows(0).Item("PartNo") = CCommon.ToString(drDestination(dr("Destination").ToString))
                                    ElseIf dr("vcDbColumnName").IndexOf("monCost") > -1 Then
                                        dtVendorDetails.Rows(0).Item("monCost") = IIf(IsNumeric(IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString))), IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString)), 0)
                                    ElseIf dr("vcDbColumnName").IndexOf("intMinQty") > -1 Then
                                        dtVendorDetails.Rows(0).Item("intMinQty") = IIf(IsNumeric(IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString))), IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString)), 0)
                                    ElseIf dr("vcDbColumnName").IndexOf("numOnHand") > -1 Then
                                        drWareHouse("numOnHand") = IIf(IsNumeric(IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString))), IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString)), 0)
                                    ElseIf dr("vcDbColumnName").IndexOf("monWListPrice") > -1 Then
                                        drWareHouse("monWListPrice") = IIf(IsNumeric(IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString))), IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString)), 0)
                                    ElseIf dr("vcDbColumnName").IndexOf("vcLocation") > -1 Then
                                        drWareHouse("vcLocation") = IIf(IsDBNull(drDestination(dr("Destination").ToString)), "0", drDestination(dr("Destination").ToString))
                                    End If
                                    AssignValuesEditBox(objItems, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                                Case "SelectBox"
                                    If dr("vcDbColumnName").IndexOf("numItemGroup") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(2, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numItemClassification") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(4, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numIncomeChartAcntId") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(5, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numAssetChartAcntId") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(5, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numCOGsChartAcntId") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(5, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numDeptId") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(8, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numDivId") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(7, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                        If strItemType = "Asset" Then
                                            blnAsset = True
                                        End If
                                    ElseIf dr("vcDbColumnName").IndexOf("numVendorID") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(7, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                        dtVendorDetails.Rows(0).Item("VendorID") = drDestination(dr("Destination").ToString)
                                        blnVendor = True
                                    ElseIf dr("vcDbColumnName").IndexOf("numWareHouseID") > -1 Then
                                        drWareHouse("numWareHouseID") = objImpWzd.GetStateAndCountry(6, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                        blnWareHouse = True
                                    ElseIf dr("vcDbColumnName").IndexOf("numCategoryID") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(9, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                        blnUpdatingCategory = True
                                    ElseIf dr("vcDbColumnName").IndexOf("numBaseUnit") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(15, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numPurchaseUnit") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(15, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numSaleUnit") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(15, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                    ElseIf dr("vcDbColumnName").IndexOf("numItemClass") > -1 Then
                                        drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(18, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"), CCommon.ToLong(dr("numListID")))
                                    End If

                                    AssignValuesSelectBox(objItems, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                            End Select
                        End If
                    End If
                Next
                lngItemID = 0
                objItems.UserCntId = Session("UserContactID")
                objItems.DomainID = Session("DomainID")

                If Mode = 1 Then
                    If Trim(objItems.ItemName) <> "" Then
                        If objItems.ItemType = "" Then objItems.ItemType = "N" 'set default to Non-Inventory item
                        objItems.ManageItemsAndKits()
                        lngItemID = objItems.ItemCode
                    End If
                    If blnAsset = True And lngItemID > 0 Then
                        objItems.type = 1
                        objItems.SaveAssetItem()
                    End If
                ElseIf Mode = 2 And objItems.ItemCode > 0 Then
                    strUpdateValues = strUpdateValues.Trim.TrimEnd(",") & " where numItemCode=" & objItems.ItemCode & " and numDomainId=" & Session("DomainID")
                    Dim objCustomReports As New CustomReports
                    objCustomReports.DynamicQuery = strUpdateValues
                    objCustomReports.UserCntID = Session("UserContactId")
                    objCustomReports.DomainID = Session("DomainId")
                    objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objCustomReports.ExecuteDynamicSql()
                    lngItemID = objItems.ItemCode 'lngItemID will be used in Update Custom field
                End If

                'Add or update vendor in Import/Update items
                If blnVendor = True And lngItemID > 0 And dtVendorDetails.Rows(0).Item("VendorID") > 0 Then
                    dtVendorDetails.AcceptChanges()
                    ds.Tables.Add(dtVendorDetails)
                    strEditedfldlst = ds.GetXml()
                    ds.Tables.Remove(dtVendorDetails)
                    objItems.DivisionID = dtVendorDetails.Rows(0).Item("VendorID")
                    If objItems.AddVendor = True Then
                        objItems.strFieldList = strEditedfldlst
                        objItems.UpdateVendor()
                    End If
                End If

                'If Mode = 1 Then 'commented by chintan, reason lake wants to update warehouse and on hand quantities
                If blnWareHouse = True And lngItemID > 0 And dtWareHouse.Rows(0).Item("numWareHouseID") > 0 Then
                    objItems.WarehouseID = dtWareHouse.Rows(0).Item("numWareHouseID")
                    objItems.WLocation = dtWareHouse.Rows(0).Item("vcLocation")
                    objItems.WListPrice = dtWareHouse.Rows(0).Item("monWListPrice") 'objItems.ListPrice 
                    objItems.WOnHand = dtWareHouse.Rows(0).Item("numOnHand")
                    objItems.DomainID = Session("DomainID")
                    Call objItems.AddWareHouseForitems()
                End If
                'End If

                If Mode = 1 Then 'insert 
                    If blnUpdatingCategory = True And objItems.CategoryID > 0 And objItems.ItemCode > 0 Then
                        objItems.byteMode = 3
                        objItems.DomainID = Session("DomainID")
                        objItems.CategoryID = objItems.CategoryID
                        objItems.Checked = 1
                        objItems.ManageSubCategoriesAndItems()
                    End If
                ElseIf Mode = 2 Then 'delete and insert
                    If blnUpdatingCategory = True And objItems.ItemCode > 0 Then
                        objItems.byteMode = 4
                        objItems.DomainID = Session("DomainID")
                        objItems.CategoryID = objItems.CategoryID
                        objItems.Checked = 1 'when category is 0 in update mode then delete item from item category
                        objItems.ManageSubCategoriesAndItems()
                    End If
                End If


                'insert/update item's extended description for shopping cart with html
                If CCommon.ToString(objItems.str).Trim.Length > 0 And objItems.ItemCode > 0 Then
                    objItems.byteMode = 1 'objItems.str contains extended description for shopping cart
                    objItems.UserCntId = Session("UserContactID")
                    objItems.ManageItemExtendedDesc()
                    objItems.str = ""
                End If

                'Insert custom field values
                For Each drHead In dataTableHeading.Rows
                    If CCommon.ToString(drDestination(drHead("Destination"))).Trim.Length > 0 Then
                        If drHead.Item(2) <> 0 Then
                            objImpWzd.RecId = lngItemID
                            If drHead.Item("vcAssociatedControlType") = "SelectBox" Then
                                objImpWzd.FldValue = objImpWzd.GetStateAndCountry(18, CCommon.ToString(drDestination(drHead("Destination"))), Session("DomainID"), CCommon.ToLong(drHead("numListID")))
                            ElseIf drHead.Item("vcAssociatedControlType") = "DateField" Then
                                If IsDate(drDestination(drHead("Destination").ToString)) = True Then
                                    objImpWzd.FldValue = FormattedDateFromDate(drDestination(drHead("Destination").ToString), Session("DateFormat"))
                                Else
                                    objImpWzd.FldValue = ""
                                End If
                            ElseIf drHead.Item("vcAssociatedControlType") = "CheckBox" Then
                                Try
                                    If UCase(drDestination(drHead("Destination").ToString)) = "TRUE" Or UCase(drDestination(drHead("Destination").ToString)) = "YES" Then
                                        objImpWzd.FldValue = 1
                                    Else : objImpWzd.FldValue = 0
                                    End If
                                Catch ex As Exception
                                    objImpWzd.FldValue = 0
                                End Try
                            Else : objImpWzd.FldValue = CCommon.ToString(drDestination(drHead("Destination"))).TrimLength(100)
                            End If
                            objImpWzd.FldId = drHead("Id")
                            objImpWzd.UpdateCustomeField() 'Function just delete(if exist) and Inserts value
                        End If
                    End If
                    intCount += 1
                Next

                intCount = 0
                lngItemID = 0
            Next

            lngItemID = 0
            litMessage.Text = "Records are sucessfully saved into database"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        Finally
            pgBar.Style.Add("display", "none")
            Session("IntCount") = ""
            Session("FileLocation") = ""
            Session("dataTableDestinationObj") = ""
            Session("dataTableHeadingObj") = ""
            Session("dllValue") = ""
            tbldtls.Rows.Clear()
        End Try
    End Sub
#End Region

    Public Sub SetProperties(ByRef objLeadBoxData As FormGenericLeadBox, ByVal sPropertyName As String, ByVal sPropertyValue As String)
        Try
            Dim theType As Type = objLeadBoxData.GetType
            Dim myProperties() As PropertyInfo = theType.GetProperties((BindingFlags.Public Or BindingFlags.Instance))
            Dim fillObject As Object = objLeadBoxData
            Dim PropertyItem As PropertyInfo
            For Each PropertyItem In myProperties                                                       'Loop thru Writeonly public properties
                With PropertyItem
                    If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                        Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                            Case "System.String" : PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)
                            Case "System.Int64", "System.Int32", "System.Int16"                         'Integer type properties
                                If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CInt(sPropertyValue), Nothing) 'Typecasting to integer before setting the value
                            Case "System.Boolean"                                                       'Boolean properties
                                If IsBoolean(sPropertyValue) Then PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing) 'Typecasting to boolean before setting the value
                        End Select
                        Exit Sub
                    End If
                End With
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function IsBoolean(ByVal sTrg As String) As Boolean
        Try
            Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
        Catch Ex As InvalidCastException
            Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
        End Try
        Return True                                                                             'Test passed, its booelan
    End Function

    Sub AssignValuesEditBox(ByVal objItems As CItems, ByVal vcValue As String, ByVal vcColumnName As String)
        Try
            vcValue = Replace(vcValue, "�", ",")
            Select Case vcColumnName
                Case "vcItemName"
                    objItems.ItemName = vcValue
                    strUpdateValues = strUpdateValues & " vcItemName='" & Replace(objItems.ItemName, "'", "''").TrimLength(300) & "',"
                Case "charItemType"
                    objItems.ItemType = vcValue
                    strUpdateValues = strUpdateValues & " charItemType='" & objItems.ItemType.TrimLength(1) & "',"
                Case "vcModelID"
                    objItems.ModelID = vcValue
                    strUpdateValues = strUpdateValues & " vcModelID='" & objItems.ModelID.TrimLength(200) & "',"
                Case "monListPrice"
                    objItems.ListPrice = IIf(IsNumeric(Replace(vcValue, ",", "")), vcValue, 0)
                    strUpdateValues = strUpdateValues & " monListPrice=" & CType(objItems.ListPrice, String) & ","
                Case "fltWeight"
                    objItems.Weight = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " fltWeight=" & CType(objItems.Weight, String) & ","
                Case "fltLength"
                    objItems.Length = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " fltLength=" & CType(objItems.Length, String) & ","
                Case "fltWidth"
                    objItems.Width = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " fltWidth=" & CType(objItems.Width, String) & ","
                Case "fltHeight"
                    objItems.Height = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " fltHeight=" & CType(objItems.Height, String) & ","
                Case "txtItemDesc" 'regular description
                    objItems.ItemDesc = vcValue
                    strUpdateValues = strUpdateValues & " txtItemDesc='" & Replace(objItems.ItemDesc, "'", "''").TrimLength(1000) & "',"
                Case "txtDesc" 'Shopping cart extended desc
                    objItems.str = vcValue
                    'strUpdateValues = strUpdateValues & " txtDesc='" & Replace(objItems.ItemDesc, "'", "''") & "',"
                Case "numBarCodeId"
                    objItems.BarCodeID = vcValue
                    strUpdateValues = strUpdateValues & " numBarCodeId='" & CType(objItems.BarCodeID, String) & "',"
                Case "numAppvalue"
                    objItems.AppreValue = IIf(IsNumeric(vcValue), vcValue, 0)
                    If objItems.AppreValue > 0 Then objItems.Appreciation = 1

                Case "numDepValue"
                    objItems.DepreValue = IIf(IsNumeric(vcValue), vcValue, 0)
                    If objItems.DepreValue > 0 Then objItems.Deppreciation = 1

                Case "dtPurchase"
                    objItems.PurchaseDate = IIf(IsDate(vcValue), vcValue, Nothing)

                Case "dtWarrentyTill"
                    objItems.WarrentyTill = IIf(IsDate(vcValue), vcValue, Nothing)

                Case "vcManufacturer"
                    objItems.Manufacturer = IIf(IsDBNull(vcValue) = False, vcValue, "")
                    strUpdateValues = strUpdateValues & " vcManufacturer='" & objItems.Manufacturer.TrimLength(250) & "',"
                Case "vcPathForTImage"
                    objItems.PathForTImage = IIf(IsDBNull(vcValue) = False, vcValue, Nothing)
                    If Not objItems.PathForTImage Is Nothing Then strUpdateValues = strUpdateValues & " vcPathForTImage='" & objItems.PathForTImage.TrimLength(300) & "',"
                Case "vcPathForImage"
                    objItems.PathForImage = IIf(IsDBNull(vcValue) = False, vcValue, Nothing)
                    If Not objItems.PathForImage Is Nothing Then strUpdateValues = strUpdateValues & " vcPathForImage='" & objItems.PathForImage.TrimLength(300) & "',"
                Case "vcSKU"
                    objItems.SKU = IIf(IsDBNull(vcValue) = False, vcValue, "")
                    strUpdateValues = strUpdateValues & " vcSKU='" & objItems.SKU.TrimLength(50) & "',"
                Case "bitTaxable"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.Taxable = True
                    Else
                        objItems.Taxable = False
                    End If
                    strUpdateValues = strUpdateValues & " bitTaxable=" & IIf(objItems.Taxable = True, 1, 0) & ","
                Case "IsArchieve"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.bitArchieve = True
                    Else
                        objItems.bitArchieve = False
                    End If
                    strUpdateValues = strUpdateValues & " IsArchieve=" & IIf(objItems.bitArchieve = True, 1, 0) & ","
                Case "bitKitParent"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.KitParent = True
                    Else
                        objItems.KitParent = False
                    End If
                    strUpdateValues = strUpdateValues & " bitKitParent=" & IIf(objItems.KitParent = True, 1, 0) & ","
                Case "bitAssembly"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.Assembly = True
                        objItems.KitParent = True
                    Else
                        objItems.Assembly = False
                    End If
                    strUpdateValues = strUpdateValues & " bitAssembly=" & IIf(objItems.Assembly = True, 1, 0) & ","

                    'If objItems.Assembly = True Then
                    '    strUpdateValues = strUpdateValues & " bitKitParent=" & IIf(objItems.Assembly = True, 1, 0) & ","
                    'End If
                Case "bitSerialized"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.bitSerialized = True
                    Else
                        objItems.bitSerialized = False
                    End If
                    strUpdateValues = strUpdateValues & " bitSerialized=" & IIf(objItems.bitSerialized = True, 1, 0) & ","

                Case "bitLotNo"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.bitLotNo = True
                    Else
                        objItems.bitLotNo = False
                    End If
                    strUpdateValues = strUpdateValues & " bitLotNo=" & IIf(objItems.bitLotNo = True, 1, 0) & ","

                Case "bitAllowBackOrder"
                    If CCommon.ToString(vcValue).ToUpper = "YES" Or CCommon.ToString(vcValue).ToUpper = "1" Or CCommon.ToString(vcValue).ToUpper = "TRUE" Then
                        objItems.AllowBackOrder = True
                    Else
                        objItems.AllowBackOrder = False
                    End If
                    strUpdateValues = strUpdateValues & " bitAllowBackOrder=" & IIf(objItems.AllowBackOrder = True, 1, 0) & ","
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub AssignValuesSelectBox(ByVal objItems As CItems, ByVal vcValue As String, ByVal vcColumnName As String)
        Try
            Select Case vcColumnName
                Case "numItemGroup"
                    objItems.ItemGroupID = CLng(vcValue)
                    strUpdateValues = strUpdateValues & " numItemGroup=" & CType(objItems.ItemGroupID, String) & ","
                Case "numItemClassification"
                    objItems.ItemClassification = CLng(vcValue)
                    strUpdateValues = strUpdateValues & " numItemClassification=" & CType(objItems.ItemClassification, String) & ","
                Case "numIncomeChartAcntId"
                    objItems.IncomeChartAcntId = CCommon.ToInteger(vcValue)
                    If objItems.IncomeChartAcntId > 0 Then
                        strUpdateValues = strUpdateValues & " numIncomeChartAcntId=" & CType(objItems.IncomeChartAcntId, String) & ","
                    End If
                Case "numAssetChartAcntId"
                    objItems.AssetChartAcntId = CCommon.ToInteger(vcValue)
                    If objItems.AssetChartAcntId > 0 Then
                        strUpdateValues = strUpdateValues & " numAssetChartAcntId=" & CType(objItems.AssetChartAcntId, String) & ","
                    End If
                Case "numCOGsChartAcntId"
                    objItems.COGSChartAcntId = CCommon.ToInteger(vcValue)
                    If objItems.COGSChartAcntId > 0 Then
                        strUpdateValues = strUpdateValues & " numCOGsChartAcntId=" & CType(objItems.COGSChartAcntId, String) & ","
                    End If
                Case "numDeptId"
                    objItems.DepartmentID = IIf(IsNumeric(vcValue), vcValue, 0)

                Case "numDivId"
                    objItems.DivisionID = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " numDivId=" & CType(objItems.DivisionID, String) & ","
                Case "numVendorID"
                    objItems.VendorID = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " numVendorID=" & CType(objItems.VendorID, String) & ","
                Case "numCategoryID"
                    objItems.CategoryID = IIf(IsNumeric(vcValue), vcValue, 0)
                Case "numBaseUnit"
                    objItems.BaseUnit = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " numBaseUnit=" & CType(objItems.BaseUnit, String) & ","
                Case "numPurchaseUnit"
                    objItems.PurchaseUnit = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " numPurchaseUnit=" & CType(objItems.PurchaseUnit, String) & ","
                Case "numSaleUnit"
                    objItems.SaleUnit = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " numSaleUnit=" & CType(objItems.SaleUnit, String) & ","
                Case "numItemClass"
                    objItems.ItemClass = IIf(IsNumeric(vcValue), vcValue, 0)
                    strUpdateValues = strUpdateValues & " numItemClass=" & CType(objItems.ItemClass, String) & ","

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'This function only works for domain 110 ,Hitech Inc, they had 21,000 item in their inventory so this is custom function to update their inventory
    'Private Sub btnImportForHitech_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportForHitech.Click
    '    Try
    '        Session("PagingRows") = "5000"
    '        Dim Filename As String = System.IO.Path.GetFileName(Session("FileLocation").ToString)
    '        Dim dtData As New DataTable '= CSV2DataTable(Session("FileLocation").ToString, vbTab) ' GetCVSFile(Session("FileLocation").ToString.Replace(Filename, ""), Filename).Tables(0)

    '        Dim dataTableDestination As New DataTable
    '        Dim dataTableHeading As New DataTable
    '        Dim fileLocation As String = Session("FileLocation")
    '        Dim streamReader As StreamReader

    '        streamReader = File.OpenText(fileLocation)

    '        Dim intCount As Integer = Session("IntCount")
    '        Dim intCountFor As Integer = 0
    '        Dim strSplitValue() As String
    '        Dim strSplitHeading() As String
    '        Dim arryList() As String
    '        Dim strdllValue As String
    '        Dim intArryValue As Integer
    '        Dim intCountSession As Integer = Session("IntCount")

    '        dataTableDestination = Session("dataTableDestinationObj")
    '        dataTableDestination.Clear()

    '        Dim csv As CsvReader = New CsvReader(streamReader, True)
    '        csv.MissingFieldAction = MissingFieldAction.ReturnPartiallyParsedValue
    '        csv.SupportsMultiline = True

    '        strdllValue = Session("dllValue")
    '        arryList = Split(strdllValue, ",")

    '        Dim dataRowDestination As DataRow
    '        Dim drHead As DataRow

    '        dataTableHeading = Session("dataTableHeadingObj")

    '        Dim i1000 As Integer = 0

    '        Dim headers As String() = csv.GetFieldHeaders()
    '        Dim i As Integer

    '        For i = 0 To csv.FieldCount - 1
    '            dtData.Columns.Add(headers(i))
    '        Next

    '        While (csv.ReadNextRecord())
    '            dataRowDestination = dtData.NewRow 'dataTableDestination.NewRow

    '            For i = 0 To csv.FieldCount - 1
    '                dataRowDestination(i) = csv(i).ToString
    '            Next
    '            dtData.Rows.Add(dataRowDestination)
    '        End While

    '        Dim objItems As New CItems
    '        For Each dr As DataRow In dtData.Rows
    '            With objItems
    '                .ItemCode = 0
    '                .ItemName = CCommon.ToString(dr("Item Name"))
    '                .ItemDesc = CCommon.ToString(dr("Item Description"))
    '                .ItemType = CCommon.ToString(dr("Item Type"))
    '                .ListPrice = 0
    '                .ItemClassification = 0
    '                .ModelID = CCommon.ToString(dr("Model ID"))
    '                .KitParent = False
    '                .bitSerialized = False
    '                .Taxable = IIf(dr("Is it taxable").ToString = "Yes", True, False)

    '                .SKU = CCommon.ToString(dr("SKU"))
    '                .Height = CCommon.ToDouble(dr("Height"))
    '                .Width = CCommon.ToDouble(dr("Width"))
    '                .Length = CCommon.ToDouble(dr("Length"))
    '                .Weight = CCommon.ToDouble(dr("Weight"))


    '                .ItemGroupID = 0
    '                .AverageCost = 0
    '                .LabourCost = 0

    '                .UserCntId = Session("UserContactID")
    '                .DomainID = Session("DomainID")

    '                If .ItemType = "S" Then
    '                    .COGSChartAcntId = 0
    '                    .AssetChartAcntId = 0
    '                Else
    '                    .COGSChartAcntId = CCommon.ToLong(dr("COGS account"))
    '                    .AssetChartAcntId = CCommon.ToLong(dr("Asset"))
    '                End If
    '                .IncomeChartAcntId = CCommon.ToLong(dr("Income"))
    '                .UnitofMeasure = CCommon.ToString(dr("Unit"))

    '                .VendorID = CCommon.ToLong(dr("VendorID"))
    '            End With

    '            Dim strError As String = objItems.ManageItemsAndKits()
    '            If strError.Length > 2 Then
    '                litMessage.Text = strError
    '                Exit Sub
    '            End If
    '            If objItems.ItemCode > 0 Then
    '                'Add Warehouse ListPrice and OnHand Qty
    '                Dim ds As New DataSet
    '                Dim table As New DataTable
    '                Dim strEditedfldlst As String

    '                table.Columns.Add("numWareHouseItemID")
    '                table.Columns.Add("numWareHouseID")
    '                table.Columns.Add("OnHand")
    '                table.Columns.Add("Reorder")
    '                table.Columns.Add("OnOrder")
    '                table.Columns.Add("Allocation")
    '                table.Columns.Add("BackOrder")
    '                table.Columns.Add("Op_Flag")
    '                table.Columns.Add("Location")
    '                table.Columns.Add("Price")
    '                table.TableName = "WareHouse"

    '                Dim row As DataRow = table.NewRow()
    '                If CCommon.ToLong(dr("WarehouseID")) > 0 And objItems.ItemCode > 0 Then
    '                    row("numWareHouseID") = CCommon.ToLong(dr("WarehouseID"))
    '                    objItems.WarehouseID = CCommon.ToLong(dr("WarehouseID"))
    '                    objItems.DomainID = Session("DomainID")
    '                    row("numWareHouseItemID") = objItems.AddWareHouseForitems
    '                    row("Price") = CCommon.ToDecimal(dr("List Price"))
    '                    row("Location") = ""
    '                    row("OnHand") = CCommon.ToDecimal(dr("Qty on Hand"))
    '                    row("Reorder") = 0
    '                    row("OnOrder") = 0
    '                    row("Allocation") = 0
    '                    row("BackOrder") = 0
    '                    row("Op_Flag") = 2
    '                    table.Rows.Add(row)
    '                End If
    '                ds.Tables.Add(table)
    '                strEditedfldlst = ds.GetXml()
    '                objItems.strFieldList = strEditedfldlst
    '                ds.Tables.Remove(table)
    '                objItems.ManageItemsAndKits()

    '                '''Saving Item Tax Types
    '                'SaveTaxTypes()
    '                'Save custom field
    '                If CCommon.ToString(dr("Manufacturer")).Trim.Length > 0 Then
    '                    Dim ObjCusfld As New CustomFields
    '                    ObjCusfld.strDetails = "<NewDataSet><Table><FldDTLID>0</FldDTLID><fld_id>369</fld_id><fld_type>Text Box</fld_type><fld_label>Manufacturer</fld_label><Value>" & CCommon.ToString(dr("Manufacturer")) & "</Value><TabId>0</TabId><vcURL /></Table></NewDataSet>"
    '                    ObjCusfld.locId = 5
    '                    ObjCusfld.RecordId = objItems.ItemCode
    '                    ObjCusfld.SaveCustomFldsByRecId()
    '                End If


    '                'Add prefered vendor details
    '                objItems.DivisionID = objItems.VendorID
    '                objItems.DomainID = Session("DomainID")
    '                objItems.AddVendor()

    '                Dim dtVendorDetails As New DataTable
    '                dtVendorDetails.Columns.Add("VendorID")
    '                dtVendorDetails.Columns.Add("PartNo")
    '                dtVendorDetails.Columns.Add("monCost")
    '                dtVendorDetails.Columns.Add("intMinQty")
    '                dtVendorDetails.TableName = "Table"

    '                Dim drVendor As DataRow
    '                drVendor = dtVendorDetails.NewRow
    '                drVendor("VendorID") = objItems.VendorID
    '                drVendor("PartNo") = CCommon.ToString(dr("PartNo"))
    '                drVendor("monCost") = CCommon.ToDecimal(dr("Cost"))
    '                drVendor("intMinQty") = 0
    '                dtVendorDetails.Rows.Add(drVendor)

    '                ds.Tables.Add(dtVendorDetails)
    '                strEditedfldlst = ds.GetXml()
    '                ds.Tables.Remove(dtVendorDetails)
    '                objItems.strFieldList = strEditedfldlst
    '                objItems.ItemCode = objItems.ItemCode
    '                objItems.UpdateVendor()

    '                'Add item to Category
    '                If CCommon.ToLong(dr("CategoryID")) > 0 Then
    '                    objItems.byteMode = 1
    '                    objItems.DomainID = Session("DomainID")
    '                    objItems.CategoryID = CCommon.ToLong(dr("CategoryID"))
    '                    objItems.Checked = 1
    '                    objItems.ManageSubCategoriesAndItems()
    '                End If

    '                'Update Imagename
    '                If CCommon.ToString(dr("ItemImageFileName")).Trim.Length > 2 Then
    '                    objItems.PathForImage = CCommon.ToString(dr("ItemImageFileName"))
    '                    objItems.PathForTImage = CCommon.ToString(dr("ItemImageFileName"))
    '                    objItems.UploadItemImage()
    '                End If
    '            End If
    '        Next

    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub lbDownloadCSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDownloadCSV.Click
        If Mode = 2 Then
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")

            Dim DataTable As DataTable = objItem.GetItemForUpdateWizard()

            If DataTable.Rows.Count = 0 Then
                litMessage.Text = "Please Configure Fields"
                Exit Sub
            End If


            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=UpdateItemTemplate." & Now.Month.ToString & "." & Now.Day.ToString & "." & Now.Year.ToString & ".csv")
            Response.Charset = ""
            Me.EnableViewState = False
            CSVExport.ProduceCSV(DataTable, Response.Output, True)
            Response.End()
            'Response.Redirect("../admin/frmExportData.aspx?frm=Import", False)
        Else
            Dim objImport As New ImportWizard
            objImport.DomainId = Session("DomainID")
            objImport.Relationship = 0
            If Mode = 1 Then
                objImport.ImportType = 2
            Else
                objImport.ImportType = 2
            End If
            Dim DataTable As DataTable = objImport.GetConfigurationTable()

            If DataTable.Rows.Count = 0 Then
                litMessage.Text = "Please Configure Fields"
                Exit Sub
            End If

            Dim dt As New DataTable
            For Each dr As DataRow In DataTable.Rows
                dt.Columns.Add(dr("vcFormFieldName").ToString.Replace(",", " "))
            Next
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=ImportItemTemplate." & Now.Month.ToString & "." & Now.Day.ToString & "." & Now.Year.ToString & ".csv")
            Response.Charset = ""
            Me.EnableViewState = False
            CSVExport.ProduceCSV(dt, Response.Output, True)
            Response.End()
        End If


    End Sub

    Function ValidateMandatoryColumns() As Boolean
        Try
            Dim objImport As New ImportWizard
            objImport.DomainId = Session("DomainID")
            objImport.Relationship = 0
            If Mode = 1 Then
                objImport.ImportType = 2
            Else
                objImport.ImportType = 2
            End If
            Dim DataTable As DataTable = objImport.GetConfigurationTable()

            If DataTable.Select(" numFormFieldId = '1199'", "").Count = 0 Then
                litMessage.Text = "Field ""Item Name"" is mandatory,your option is to add given field to Column Configuration and try again"
                btnImports.Visible = False
                Exit Function
            End If
            If DataTable.Select(" numFormFieldId = '1200'", "").Count = 0 Then
                litMessage.Text = "Field ""Item Type"" is mandatory,your option is to add given field to Column Configuration and try again"
                btnImports.Visible = False
                Exit Function
            End If
            If DataTable.Select(" numFormFieldId = '1349'", "").Count = 0 Then
                litMessage.Text = "Field ""Income Account"" is mandatory,your option is to add given field to Column Configuration and try again"
                btnImports.Visible = False
                Exit Function
            End If
            If DataTable.Select(" numFormFieldId = '1350'", "").Count = 0 Then
                litMessage.Text = "Field ""Asset Account"" is mandatory,your option is to add given field to Column Configuration and try again"
                btnImports.Visible = False
                Exit Function
            End If
            If DataTable.Select(" numFormFieldId = '1351'", "").Count = 0 Then
                litMessage.Text = "Field ""COGS Account"" is mandatory,your option is to add given field to Column Configuration and try again"
                btnImports.Visible = False
                Exit Function
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
