Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Partial Public Class frmWarehouses
    Inherits BACRMPage

    Dim m_aryRightsForAdd() As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If Not IsPostBack Then
                GetUserRightsForPage(37, 12)
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnNew.Visible = False
                LoadWareHouse()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadWareHouse()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            dgWarehouse.DataSource = objItem.GetWareHouses
            dgWarehouse.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgWarehouse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgWarehouse.ItemCommand
        Try
            If e.CommandName = "Warehouse" Then
                Response.Redirect("../Items/frmAddWareHouse.aspx?WID=" & e.Item.Cells(0).Text, False)
            ElseIf e.CommandName = "Delete" Then
                Dim objItem As New CItems
                objItem.WarehouseID = e.Item.Cells(0).Text
                objItem.byteMode = 1
                objItem.ManageWarehouse() 'Delete warehouse
                objItem.DomainID = Session("DomainID")
                objItem.byteMode = 0
                objItem.WarehouseID = 0
                dgWarehouse.DataSource = objItem.GetWareHouses
                dgWarehouse.DataBind()
            End If
        Catch ex As Exception
            If ex.Message = "Dependant" Then
                ShowMessage("Can not delete warehouse, Dependant records found.")
                Exit Sub
            End If
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub dgWarehouse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgWarehouse.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class