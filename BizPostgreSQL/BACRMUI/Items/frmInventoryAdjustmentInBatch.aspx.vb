﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Projects
Imports System.Text.RegularExpressions
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Xml.Linq

Public Class frmInventoryAdjustmentInBatch
    Inherits BACRMPage
#Region "Memeber Variables"
    Private dsAttributes As DataSet
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If ChartOfAccounting.GetDefaultAccount("IA", Session("DomainID")) = 0 Then
                ShowMessage("Please Map Default Inventory Adjustment Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.")
                btnSave.Visible = False
            ElseIf ChartOfAccounting.GetDefaultAccount("OE", Session("DomainID")) = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SerializeItemValidate", "alert('Please Map Default Opening Balance Equity Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.')", True)
                btnSaveInvetory.Visible = False
            End If

            If Not IsPostBack Then
                Dim m_aryRightsForPage As Integer() = GetUserRightsForPage(37, 132)

                ddlWareHouse.Focus()
                hdnItemCode.Value = CCommon.ToLong(GetQueryStringVal("ItemCode"))
                hdnWarehouseID.Value = CCommon.ToLong(GetQueryStringVal("WarehouseID"))
                hdnWarehouseItemID.Value = CCommon.ToLong(GetQueryStringVal("WarehouseItemID"))

                BindDropdown()
                BindProject()
                calAdjustmentDate.SelectedDate = Date.Now

                'DO NOT LOAD PERSISTANCE INFORMATION WHEN PAGE IS CALLED FROM ITEM
                If CCommon.ToLong(hdnItemCode.Value) = 0 Then
                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        btnSaveInvetory.Visible = False

                        If ddlProject.Items.FindByValue(PersistTable(ddlProject.ID)) IsNot Nothing Then
                            ddlProject.ClearSelection()
                            ddlProject.Items.FindByValue(PersistTable(ddlProject.ID)).Selected = True
                        End If

                        If ddlWareHouse.Items.FindByValue(PersistTable(ddlWareHouse.ID)) IsNot Nothing Then
                            ddlWareHouse.ClearSelection()
                            ddlWareHouse.Items.FindByValue(PersistTable(ddlWareHouse.ID)).Selected = True
                        End If

                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))

                        If ddlSearch.Items.FindByValue(CCommon.ToString(PersistTable(ddlSearch.ID))) IsNot Nothing Then
                            ddlSearch.ClearSelection()
                            ddlSearch.Items.FindByValue(CCommon.ToString(PersistTable(ddlSearch.ID))).Selected = True
                        End If

                        txtSearch.Text = PersistTable(PersistKey.SearchValue)
                    End If

                    ddlWareHouse_SelectedIndexChanged()

                ElseIf CCommon.ToLong(hdnItemCode.Value) > 0 Then
                    divSearch.Visible = False

                    Dim objItem As New CItems
                    objItem.ItemCode = CCommon.ToLong(hdnItemCode.Value)
                    objItem.GetItemDetails()

                    hdnItemCodeEdit.Value = CCommon.ToLong(hdnItemCode.Value)
                    hdnIsSerialize.Value = objItem.bitSerialized
                    hdnIsLotNo.Value = objItem.bitLotNo
                    hdnItemGroup.Value = objItem.ItemGroupID
                    hdnItemName.Value = objItem.ItemName
                    hdnIsKit.Value = objItem.KitParent
                    hdnIsAssembly.Value = objItem.Assembly
                    hdnAverageCost.Value = objItem.AverageCost
                    hdnAssetChartAcntID.Value = objItem.AssetChartAcntId
                    hdnIsMatrix.Value = objItem.IsMatrix
                    txtSKU.Enabled = Not objItem.IsMatrix
                    txtUPC.Enabled = Not objItem.IsMatrix

                    If objItem.IsMatrix Then
                        txtSKU.Text = objItem.SKU
                        txtUPC.Text = objItem.BarCodeID
                    End If

                    ShowHideElement()

                    If CCommon.ToBool(hdnIsSerialize.Value) Or CCommon.ToBool(hdnIsLotNo.Value) Or CCommon.ToBool(hdnIsKit.Value) Then
                        divAdjust.Visible = False
                    End If
                End If
            Else
                'RELOAD ATTRIBUTES CONTROL IF MATRIX ITEM
                If CCommon.ToLong(hdnItemGroup.Value) > 0 Then
                    Dim str As String = System.Web.HttpUtility.HtmlDecode(hdnAttributes.Value)
                    Dim reader As New System.IO.StringReader(str)
                    dsAttributes = New DataSet
                    dsAttributes.ReadXml(reader)

                    If Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 0 Then
                        AttributesControl(dsAttributes.Tables(0))
                    End If
                End If
            End If
            'BECAUSE OF DYNAMICALLY ADDED COLUMNS WE NEED TO BIND GRID ON EVERY POSTBACK
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub

    Private Sub BindGrid()
        Try
            Dim strValue As String = ""
            Dim dt As New DataTable
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            objItem.UserCntID = Session("UserContactID")
            objItem.WarehouseID = CCommon.ToLong(ddlWareHouse.SelectedValue)
            objItem.ItemGroupID = CCommon.ToLong(radItemGroup.SelectedValue)
            If ddlSearch.SelectedItem.Value <> "" And txtSearch.Text <> "" Then
                objItem.KeyWord = " And " + ddlSearch.SelectedItem.Value & " ilike '%" & txtSearch.Text.Trim.Replace("'", "''") & "%'"
            Else
                objItem.KeyWord = ""
            End If

            If radAisle.CheckedItems.Count > 0 Then
                For Each item As RadComboBoxItem In radAisle.CheckedItems
                    If CCommon.ToString(item.Value).Trim.Length > 0 Then
                        strValue = strValue & "'" & item.Value & "',"
                    End If
                Next
                objItem.KeyWord = objItem.KeyWord + " And vcAisle in(" + strValue.TrimEnd(",") + ") "
            End If

            If radRack.CheckedItems.Count > 0 Then
                For Each item As RadComboBoxItem In radRack.CheckedItems
                    If CCommon.ToString(item.Value).Trim.Length > 0 Then
                        strValue = strValue & "'" & item.Value & "',"
                    End If
                Next
                objItem.KeyWord = objItem.KeyWord + " And vcRack in(" + strValue.TrimEnd(",") + ") "
            End If

            If radShelf.CheckedItems.Count > 0 Then
                For Each item As RadComboBoxItem In radShelf.CheckedItems
                    If CCommon.ToString(item.Value).Trim.Length > 0 Then
                        strValue = strValue & "'" & item.Value & "',"
                    End If
                Next
                objItem.KeyWord = objItem.KeyWord + " And vcShelf in(" + strValue.TrimEnd(",") + ") "
            End If

            If radBin.CheckedItems.Count > 0 Then
                For Each item As RadComboBoxItem In radBin.CheckedItems
                    If CCommon.ToString(item.Value).Trim.Length > 0 Then
                        strValue = strValue & "'" & item.Value & "',"
                    End If
                Next
                objItem.KeyWord = objItem.KeyWord + " And vcBin in(" + strValue.TrimEnd(",") + ") "
            End If


            objItem.SortCharacter = txtSortChar.Text.Trim()

            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            objItem.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text.Trim())

            objItem.PageSize = Convert.ToInt32(Session("PagingRows"))
            objItem.TotalRecords = 0
            objItem.SortCharacter = txtSortChar.Text
            objItem.ItemCode = CCommon.ToLong(hdnItemCode.Value)
            objItem.WareHouseItemID = CCommon.ToLong(hdnWarehouseItemID.Value)
            Dim ds As DataSet = objItem.GetItemsForInventoryAdjustment()

            'If Not Page.IsPostBack Then
            Dim i As Int16 = 3

            If ds.Tables(2).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(2).Rows
                    Dim boundField As New BoundField
                    boundField.HeaderText = CCommon.ToString(dr("vcFieldName"))
                    boundField.DataField = CCommon.ToString(dr("vcDbColumnName"))
                    gvItems.Columns.Insert(i, boundField)
                    i = i + 1
                Next
            End If

            gvItems.DataSource = ds.Tables(0)
            gvItems.DataBind()

            Cache("cols") = gvItems.Columns
            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objItem.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text


            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add(PersistKey.CurrentPage, IIf(dt.Rows.Count > 0, txtCurrrentPage.Text, "1"))
            PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
            PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
            PersistTable.Add(PersistKey.SearchValue, txtSearch.Text.Trim)
            PersistTable.Add(ddlSearch.ID, ddlSearch.SelectedValue)
            PersistTable.Add(ddlWareHouse.ID, ddlWareHouse.SelectedValue)
            PersistTable.Add(ddlProject.ID, ddlProject.SelectedValue)
            PersistTable.Save(boolOnlyURL:=True)

            If objItem.TotalRecords = 0 Then
                lblRecordCountCases.Text = 0
            Else
                lblRecordCountCases.Text = String.Format("{0:#,###}", objItem.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCountCases.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCountCases.Text Mod Session("PagingRows")) = 0 Then
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCountCases.Text
            End If



            For intCol As Integer = 0 To gvItems.HeaderRow.Cells.Count - 1
                If gvItems.HeaderRow.Cells(intCol).Text.Contains("Attributes") = True And CCommon.ToLong(hdnItemGroup.Value) > 0 Then
                    gvItems.Columns(intCol).Visible = True
                ElseIf gvItems.HeaderRow.Cells(intCol).Text.Contains("Qty to Adjust") Or gvItems.HeaderRow.Cells(intCol).Text.Contains("Counted") Then
                    If CCommon.ToLong(hdnItemCode.Value) > 0 _
                        AndAlso (CCommon.ToBool(hdnIsSerialize.Value) Or CCommon.ToBool(hdnIsLotNo.Value) Or CCommon.ToBool(hdnIsKit.Value)) Then
                        gvItems.Columns(intCol).Visible = False
                    Else
                        gvItems.Columns(intCol).Visible = True
                    End If
                End If
            Next

            If CCommon.ToLong(hdnItemCode.Value) > 0 _
                       AndAlso (CCommon.ToBool(hdnIsSerialize.Value) Or CCommon.ToBool(hdnIsLotNo.Value) Or CCommon.ToBool(hdnIsKit.Value)) Then
                gvItems.Columns(0).Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindDropdown()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            ddlWareHouse.DataSource = objItem.GetWareHouses
            ddlWareHouse.DataTextField = "vcWareHouse"
            ddlWareHouse.DataValueField = "numWareHouseID"
            ddlWareHouse.DataBind()
            ddlWareHouse.Items.Insert(0, New ListItem("--Select One--", "0"))


            objItem.ItemCode = 0
            radItemGroup.DataSource = objItem.GetItemGroups.Tables(0)
            radItemGroup.DataTextField = "vcItemGroup"
            radItemGroup.DataValueField = "numItemGroupID"
            radItemGroup.DataBind()
            radItemGroup.Items.Insert(0, New RadComboBoxItem() With {.Text = "--Select One--", .Value = "0"})
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
    Private Sub BindProject()
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = objProject.GetOpenProject()
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlWareHouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWareHouse.SelectedIndexChanged
        Try
            ddlWareHouse_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlWarehouse_SelectedIndexChanged()
        Try
            If CCommon.ToLong(ddlWareHouse.SelectedValue) > 0 Then

                Dim objItem As New CItems
                objItem.WarehouseID = ddlWareHouse.SelectedValue
                objItem.DomainID = Session("DomainID")
                Dim dt As DataTable = objItem.GetWarehouseLocation()

                ddlWarehouseLocation.Items.Clear()

                ddlWarehouseLocation.DataSource = dt
                ddlWarehouseLocation.DataValueField = "numWLocationID"
                ddlWarehouseLocation.DataTextField = "vcLocation"
                ddlWarehouseLocation.DataBind()

                ddlWarehouseLocation.Items.Insert(0, New ListItem("-- Select One --", "0"))
                ddlWarehouseLocation.Items.Insert(1, New ListItem("Global", "-1"))
                ddlWarehouseLocation.SelectedIndex = 0

                radAisle.DataSource = dt.DefaultView.ToTable(True, "vcAisle")
                radAisle.DataTextField = "vcAisle"
                radAisle.DataValueField = "vcAisle"
                radAisle.DataBind()

                For Each item As RadComboBoxItem In radAisle.Items.ToList()
                    If CCommon.ToString(item.Value).Trim.Length = 0 Then
                        item.Remove()
                    End If
                Next


                radRack.DataSource = dt.DefaultView.ToTable(True, "vcRack")
                radRack.DataTextField = "vcRack"
                radRack.DataValueField = "vcRack"
                radRack.DataBind()
                For Each item As RadComboBoxItem In radRack.Items.ToList()
                    If CCommon.ToString(item.Value).Trim.Length = 0 Then
                        item.Remove()
                    End If
                Next


                radShelf.DataSource = dt.DefaultView.ToTable(True, "vcShelf")
                radShelf.DataTextField = "vcShelf"
                radShelf.DataValueField = "vcShelf"
                radShelf.DataBind()
                For Each item As RadComboBoxItem In radShelf.Items.ToList()
                    If CCommon.ToString(item.Value).Trim.Length = 0 Then
                        item.Remove()
                    End If
                Next


                radBin.DataSource = dt.DefaultView.ToTable(True, "vcBin")
                radBin.DataTextField = "vcBin"
                radBin.DataValueField = "vcBin"
                radBin.DataBind()
                For Each item As RadComboBoxItem In radBin.Items.ToList()
                    If CCommon.ToString(item.Value).Trim.Length = 0 Then
                        item.Remove()
                    End If
                Next


            End If

            BindGrid()

            ddlWarehouseLocation.Focus()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim decTotalAdjustmentAmount As Decimal
    Dim Counted, OnHandQty As Decimal
    Dim decAverageCost As Decimal
    Dim intDifference As Double
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dtSerial As New DataTable

            CCommon.AddColumnsToDataTable(dt, "numWareHouseItemID,numItemCode,intAdjust,vcItemName,monAverageCost,numAssetChartAcntId")
            CCommon.AddColumnsToDataTable(dtSerial, "numItemCode,numWareHouseID,numWareHouseItemID,vcSerialNo,byteMode")
            Dim drSerial As DataRow

            Dim sb As New System.Text.StringBuilder
            Dim flag As Boolean = False
            For Each gvr As GridViewRow In gvItems.Rows

                OnHandQty = CCommon.ToDecimal(CType(gvr.FindControl("lblOnHandQty"), Label).Text)
                Counted = CCommon.ToDecimal(CType(gvr.FindControl("txtCounted"), TextBox).Text)
                decAverageCost = CCommon.ToDecimal(CType(gvr.FindControl("lblAverageCost"), Label).Text)

                Dim bitSerialized As Boolean = CCommon.ToBool(gvItems.DataKeys(gvr.RowIndex)("bitSerialized"))
                Dim bitLotNo As Boolean = CCommon.ToBool(gvItems.DataKeys(gvr.RowIndex)("bitLotNo"))
                Dim strItemName As String = CCommon.ToString(CType(gvr.FindControl("lblItemName"), Label).Text.Trim())

                If CType(gvr.FindControl("chk"), CheckBox).Visible AndAlso CType(gvr.FindControl("chk"), CheckBox).Checked AndAlso Not (bitSerialized Or bitLotNo) Then
                    If CCommon.ToLong(CType(gvr.FindControl("lblAssetChartAcntId"), Label).Text) = 0 Then
                        sb.Append(strItemName + "<br />")
                    End If
                    If Counted <> OnHandQty Then
                        intDifference = Counted - OnHandQty
                        decTotalAdjustmentAmount += decAverageCost * intDifference

                        Dim dr As DataRow = dt.NewRow
                        dr("numWareHouseItemID") = CCommon.ToLong(CType(gvr.FindControl("lblWarehouseItemID"), Label).Text)
                        dr("numItemCode") = CCommon.ToLong(CType(gvr.FindControl("lblItemID"), Label).Text)
                        dr("intAdjust") = intDifference
                        dr("vcItemName") = strItemName
                        dr("monAverageCost") = decAverageCost
                        dr("numAssetChartAcntId") = CCommon.ToLong(CType(gvr.FindControl("lblAssetChartAcntId"), Label).Text)

                        dt.Rows.Add(dr)

                    End If
                    flag = True
                End If
            Next

            If sb.Length > 0 Then
                ShowMessage("Asset account is not set for following items <br>" + sb.ToString + "Your option is to set Asset account and try again.")
                Exit Sub
            End If

            If flag = False Then
                ShowMessage("You must select atleast one item to proceed.")
                Exit Sub
            End If



            'Update Quantity
            If dt.Rows.Count > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                objItem.UserCntID = Session("UserContactID")
                ds.Tables.Add(dt.Copy)
                ds.Tables(0).TableName = "Item"

                ds.Tables.Add(dtSerial.Copy)
                ds.Tables(1).TableName = "SerialLotNo"

                objItem.dtAdjustmentDate = CDate(calAdjustmentDate.SelectedDate & " 12:00:00")
                objItem.str = ds.GetXml()

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    objItem.UpdateInventoryAdjustments()

                    For Each dr As DataRow In dt.Rows
                        objItem.MakeItemQtyAdjustmentJournal(CCommon.ToLong(dr("numItemCode")), CCommon.ToString(dr("vcItemName")), CCommon.ToInteger(dr("intAdjust")), CCommon.ToDecimal(dr("monAverageCost")), CCommon.ToLong(dr("numAssetChartAcntId")), Session("UserContactID"), Session("DomainID"), boolUseOpeningBalanceEqityAccount:=False, dtAdjustmentDate:=calAdjustmentDate.SelectedDate)
                    Next

                    objTransactionScope.Complete()
                End Using
            End If

            ShowMessage("Adjustment posted sucessfully", True)

            BindGrid()

        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Public Sub btnGo_Click()
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            hdnIsNewAddedWarehouseEdited.Value = False


            If e.CommandName = "Edit_WareHouse" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim numItemCode As Int32 = gvItems.DataKeys(row.RowIndex)("numItemcode")
                Dim numWarehouseItemID As Int32 = gvItems.DataKeys(row.RowIndex)("numWareHouseItemID")

                Dim numItemGroup As Long = CCommon.ToInteger(gvItems.DataKeys(row.RowIndex)("ItemGroup"))
                Dim bitKitParent As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitKitParent"))
                Dim bitSerialized As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitSerialized"))
                Dim bitLotNo As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitLotNo"))
                Dim bitAssembly As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitAssembly"))
                Dim monAverageCost As Long = CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("monAverageCost"))

                If numItemCode > 0 AndAlso numWarehouseItemID > 0 Then
                    Dim objItems = New CItems
                    objItems.ItemCode = numItemCode
                    objItems.byteMode = 1
                    objItems.WareHouseItemID = numWarehouseItemID
                    Dim ds As DataSet = objItems.GetItemWareHouses()

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Dim dataRow As DataRow = DirectCast(ds.Tables(0).Rows(0), DataRow)

                        If Not dataRow Is Nothing Then
                            Dim numOnHand As Double = CCommon.ToDouble(dataRow("OnHand"))
                            Dim numReorder As Double = CCommon.ToDouble(dataRow("Reorder"))
                            Dim numOnOrder As Double = CCommon.ToDouble(dataRow("OnOrder"))
                            Dim numAllocation As Double = CCommon.ToDouble(dataRow("Allocation"))
                            Dim numBackOrder As Double = CCommon.ToDouble(dataRow("BackOrder"))
                            Dim numWarehouseID As Long = CCommon.ToLong(dataRow("numWarehouseID"))
                            Dim numWarehouseLocationID As Long = CCommon.ToLong(dataRow("numWLocationID"))
                            Dim Price As Double = CCommon.ToDouble(dataRow("Price"))
                            Dim sku As String = CCommon.ToString(dataRow("SKU"))
                            Dim barcode As String = CCommon.ToString(dataRow("BarCode"))

                            btnCancel.Visible = True
                            txtOnHand.Visible = False
                            lblOnHand.Visible = True

                            'MATRIX ITEM
                            trMatrixItem.Visible = If(numItemGroup > 0, True, False)
                            divListPrice.Visible = trMatrixItem.Visible
                            If numItemGroup > 0 Then
                                If numOnHand = 0 AndAlso numOnOrder = 0 AndAlso numReorder = 0 AndAlso numAllocation = 0 AndAlso numBackOrder = 0 And numWarehouseLocationID <> -1 Then
                                    AttributesControl(ds.Tables(2), 0, dataRow)
                                    hdnIsNewAddedWarehouseEdited.Value = True
                                Else
                                    AttributesControl(ds.Tables(2), 1, dataRow)
                                End If
                            End If

                            'ASSEMBLY & KIT ITEM
                            trInventory.Visible = Not (bitKitParent Or bitAssembly)

                            'SERIAL LOT ITEM
                            tblSerialLot.Visible = (bitSerialized Or bitLotNo)
                            trSerial.Visible = bitSerialized
                            trLot.Visible = bitLotNo
                            txtSerialLot.ReadOnly = bitLotNo

                            hfWareHouseItemID.Value = numWarehouseItemID
                            hdnAssetChartAcntID.Value = CCommon.ToString(DirectCast(row.FindControl("lblAssetChartAcntID"), Label).Text)
                            ddlWareHouse.SelectedValue = numWarehouseID
                            ddlWareHouse.Enabled = False
                            ddlWarehouseLocation.SelectedValue = numWarehouseLocationID
                            ddlWarehouseLocation.Enabled = False
                            txtListPrice.Text = Price
                            lblOnHand.Text = numOnHand
                            txtReOrder.Text = numReorder
                            txtSKU.Text = If(numItemGroup > 0, sku, "")
                            txtUPC.Text = If(numItemGroup > 0, barcode, "")

                            hdnItemCodeEdit.Value = numItemCode

                            'If USER CAME TO THIS PAGE FROM ITEMDETAILS PAGE THEN FOLLOWING HIDDEN FIELDS VALUES ARE ALREADY FILLED FROM PAGE_LOAD
                            If CCommon.ToLong(hdnItemCode.Value) = 0 Then
                                hdnIsSerialize.Value = bitSerialized
                                hdnIsLotNo.Value = bitLotNo
                                hdnIsAssembly.Value = bitAssembly
                                hdnAverageCost.Value = monAverageCost
                                hdnItemName.Value = DirectCast(row.FindControl("lblItemName"), Label).Text
                                hdnAssetChartAcntID.Value = DirectCast(row.FindControl("lblAssetChartAcntID"), Label).Text
                            End If

                            btnSaveInvetory.Text = "Update"
                            btnSaveInvetory.Visible = True

                            row.CssClass = "lightYellow"
                        End If
                    End If
                End If
            ElseIf e.CommandName = "Delete_WareHouse" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim numItemCode As Int32 = CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("numItemcode"))
                Dim numWarehouseItemID As Int32 = CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("numWareHouseItemID"))
                Dim numOnHand As Double = CCommon.ToDouble(gvItems.DataKeys(row.RowIndex)("numOnHand"))
                Dim isDeleteKitWarehouse As Boolean = CCommon.ToBool(DirectCast(row.FindControl("hdnDeleteKitWarehouse"), HiddenField).Value)
                Dim bitKitParent As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitKitParent"))
                Dim bitChildItemWarehouse As Boolean = CCommon.ToBool(DirectCast(row.FindControl("hdnChildItemWarehouse"), HiddenField).Value)
                Dim itemName As String = CCommon.ToString(DirectCast(row.FindControl("lblItemName"), Label).Text)
                Dim numAverageCost As Long = CCommon.ToLong(DirectCast(row.FindControl("lblAverageCost"), Label).Text)
                Dim numAssetChartAcntID As Long = CCommon.ToLong(DirectCast(row.FindControl("lblAssetChartAcntID"), Label).Text)

                If numItemCode > 0 AndAlso numWarehouseItemID > 0 Then
                    Dim objItems = New CItems
                    objItems.ItemCode = numItemCode
                    objItems.WareHouseItemID = numWarehouseItemID

                    'make Delete journal entry 
                    If isDeleteKitWarehouse AndAlso bitKitParent AndAlso Not bitChildItemWarehouse Then
                        objItems.DomainID = Session("DomainID")
                        objItems.UserCntID = Session("UserContactID")
                        objItems.byteMode = 3
                        objItems.AddUpdateWareHouseForItems()
                    ElseIf bitChildItemWarehouse Then
                        ShowMessage("You are not allowed to delete warehouse item, because item warehouse is used as child of other assembly or kit item(s).")
                    ElseIf numOnHand > 0 Then
                        ShowMessage("You are not allowed to delete warehouse item, Your option is to do adjustment of qty such that OnHand quantity goes to 0.")

                    Else
                        objItems.DomainID = Session("DomainID")
                        objItems.UserCntID = Session("UserContactID")
                        objItems.byteMode = 3

                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            objItems.AddUpdateWareHouseForItems()
                            objItems.MakeItemQtyAdjustmentJournal(numItemCode, itemName, -1 * numOnHand, numAverageCost, numAssetChartAcntID, Session("UserContactID"), Session("DomainID"))

                            objTransactionScope.Complete()
                        End Using
                    End If
                End If

                BindGrid()
            ElseIf e.CommandName = "TransferStock" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, ImageButton).NamingContainer, GridViewRow)
                Dim numItemCode As Int32 = CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("numItemcode"))
                Dim numWarehouseItemID As Int32 = CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("numWareHouseItemID"))
                Dim bitSerialized As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitSerialized"))
                Dim bitLotNo As Boolean = CCommon.ToBool(gvItems.DataKeys(row.RowIndex)("bitLotNo"))

                hdnTransferFromWarehouse.Value = CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("numWareHouseItemID"))
                Dim objCommon As New CCommon
                Dim dt As DataTable = objCommon.GetWarehouseAttrBasedItem(CCommon.ToLong(gvItems.DataKeys(row.RowIndex)("numItemcode")))

                Dim arrDataRow As DataRow() = dt.Select("numWareHouseItemId=" & hdnTransferFromWarehouse.Value)

                If Not arrDataRow Is Nothing AndAlso arrDataRow.Count > 0 Then
                    dt.Rows.Remove(arrDataRow(0))
                End If

                radTransferToWarehouse.DataSource = dt
                radTransferToWarehouse.DataTextField = "vcWareHouse"
                radTransferToWarehouse.DataValueField = "numWareHouseItemId"
                radTransferToWarehouse.DataBind()

                If bitSerialized Or bitLotNo Then
                    If bitSerialized Then
                        hplSerialLot.Attributes.Add("onclick", "return OpenSertailLot(0,0," & numItemCode & "," & numWarehouseItemID & ",'Serial'" & ");")
                    Else
                        hplSerialLot.Attributes.Add("onclick", "return OpenSertailLot(0,0," & numItemCode & "," & numWarehouseItemID & ",'Lot'" & ");")
                    End If

                    hplSerialLot.Visible = True
                    txtQty.Visible = False
                    hdnIsSerial.Value = bitSerialized
                    hdnIsLot.Value = bitLotNo
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');", True)
            End If
        Catch ex As Exception
            If ex.Message.Contains("OpportunityItems_Depend") Or ex.Message.Contains("OpportunityKitItems_Depend") Then
                ShowMessage("You are not allowed to delete warehouse item because order is placed for item with this warehouse.")
            ElseIf ex.Message.Contains("KitItems_Depend") Then
                ShowMessage("Item with selected warehouse location is used as child of Assembly/Kit item.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub AttributesControl(ByVal dtAttributes As DataTable, Optional ByVal isEdit As Boolean = 0, Optional ByVal dataRow As DataRow = Nothing)
        Try
            If Not dtAttributes Is Nothing AndAlso dtAttributes.Rows.Count > 0 Then
                plhWareHouseAttributes.Controls.Clear()

                Dim divRow As HtmlGenericControl
                Dim divColumn As HtmlGenericControl
                Dim dr As DataRow

                Dim i As Int32 = 0
                divRow = New HtmlGenericControl("div")
                divRow.Attributes.Add("id", "divAttributes")
                divRow.Attributes.Add("class", "row")

                Dim tabIndex As Int32 = 20

                For Each dr In dtAttributes.Rows
                    divColumn = New HtmlGenericControl("div")
                    divColumn.Attributes.Add("class", "col-xs-12 col-sm-6")

                    Dim objFormGroup As New HtmlGenericControl("div")
                    objFormGroup.Attributes.Add("class", "form-group")

                    Dim label As New HtmlGenericControl("label")
                    label.InnerHtml = "<span style=""color:red"">* </span>" & dr("Fld_label")

                    objFormGroup.Controls.Add(label)


                    Dim lbl As New Label
                    lbl.ID = "lbl" & dr("Fld_id")
                    If Not dataRow Is Nothing Then
                        lbl.Text = CCommon.ToString(dataRow(dr("Fld_label") & "Value"))
                    End If


                    If dr("fld_type") = "SelectBox" Then
                        If isEdit Then
                            Dim divInner As New HtmlGenericControl("div")
                            divInner.Controls.Add(lbl)
                            objFormGroup.Controls.Add(divInner)
                        Else
                            Dim ddl As DropDownList = CreateDropdown(objFormGroup, dr("Fld_id"), CCommon.ToLong(dr("Fld_Value")), dr("numlistid"))

                            If CCommon.ToBool(hdnIsMatrix.Value) Then
                                ddl.Enabled = False
                            End If
                        End If

                        divColumn.Controls.Add(objFormGroup)
                        divRow.Controls.Add(divColumn)
                    ElseIf dr("fld_type") = "CheckBox" Then
                        If isEdit Then
                            Dim divInner As New HtmlGenericControl("div")
                            divInner.Controls.Add(lbl)
                            objFormGroup.Controls.Add(divInner)
                        Else
                            Dim divInner As New HtmlGenericControl("div")
                            CreateChkBox(divInner, dr("Fld_id"), 0)
                            objFormGroup.Controls.Add(divInner)
                        End If

                        divColumn.Controls.Add(objFormGroup)
                        divRow.Controls.Add(divColumn)
                    End If

                    i = i + 1

                    If i = 2 Then
                        plhWareHouseAttributes.Controls.Add(divRow)
                        divRow = New HtmlGenericControl("div")
                        divRow.Attributes.Add("class", "row")
                    End If
                Next

                If divRow.Controls.Count > 0 Then
                    plhWareHouseAttributes.Controls.Add(divRow)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            If CCommon.ToLong(hdnItemCode.Value) > 0 Then
                btnSaveInvetory.Text = "Add"
                btnSaveInvetory.Visible = True
            Else
                btnSaveInvetory.Text = "Update"
                btnSaveInvetory.Visible = False
            End If

            Dim tempId As Long = CCommon.ToLong(hfWareHouseItemID.Value)

            hfWareHouseItemID.Value = ""
            hdnAssetChartAcntID.Value = ""
            ddlWareHouse.Enabled = True
            ddlWarehouseLocation.SelectedValue = "0"
            ddlWarehouseLocation.Enabled = True

            txtListPrice.Text = ""

            trInventory.Visible = True
            txtOnHand.Text = ""
            txtOnHand.Visible = True
            lblOnHand.Visible = False
            txtReOrder.Text = ""

            trMatrixItem.Visible = False
            divListPrice.Visible = False
            txtSKU.Text = ""
            txtUPC.Text = ""

            tblSerialLot.Visible = False
            txtLotNumber.Text = ""
            txtQuantity.Text = ""
            rdpExpirationDate.SelectedDate = Nothing
            txtSerialLot.Text = ""

            plhWareHouseAttributes.Controls.Clear()

            If CCommon.ToLong(hdnItemCode.Value) > 0 Then
                ShowHideElement()
            End If

            btnCancel.Visible = False
            ddlWareHouse.Focus()

            hdnIsNewAddedWarehouseEdited.Value = False

            BindGrid()

            If tempId > 0 Then
                For Each row As GridViewRow In gvItems.Rows
                    If gvItems.DataKeys(row.RowIndex)("numWareHouseItemID") = tempId Then
                        row.Focus()
                        row.CssClass = "lightYellowTr"
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim numWarehouseItemID As Int32 = gvItems.DataKeys(e.Row.RowIndex)("numWareHouseItemID")
                Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chk"), CheckBox)
                Dim txtCounted As TextBox = DirectCast(e.Row.FindControl("txtCounted"), TextBox)
                Dim txtAdjQty As TextBox = DirectCast(e.Row.FindControl("txtAdjQty"), TextBox)
                Dim ibtnManageSerial As LinkButton = DirectCast(e.Row.FindControl("ibtnManageSerial"), LinkButton)
                Dim ibtnTransferStok As ImageButton = DirectCast(e.Row.FindControl("ibtnTransferStok"), ImageButton)

                Dim numItemcode As Long = CCommon.ToLong(gvItems.DataKeys(e.Row.RowIndex)("numItemcode"))
                Dim bitSerialized As Boolean = CCommon.ToBool(gvItems.DataKeys(e.Row.RowIndex)("bitSerialized"))
                Dim bitLotNo As Boolean = CCommon.ToBool(gvItems.DataKeys(e.Row.RowIndex)("bitLotNo"))
                Dim bitKitParent As Boolean = CCommon.ToBool(gvItems.DataKeys(e.Row.RowIndex)("bitKitParent"))
                Dim bitAssembly As Boolean = CCommon.ToBool(gvItems.DataKeys(e.Row.RowIndex)("bitAssembly"))

                txtCounted.Visible = Not (bitSerialized Or bitLotNo Or bitKitParent)
                txtAdjQty.Visible = Not (bitSerialized Or bitLotNo Or bitKitParent)
                chkSelect.Visible = Not (bitSerialized Or bitLotNo Or bitKitParent)

                ibtnManageSerial.Visible = (bitSerialized Or bitLotNo)

                If bitSerialized Then
                    ibtnManageSerial.OnClientClick = "return SerialNo(" & numItemcode & ",1," & numWarehouseItemID & ")"
                    hplSerialLot.Visible = True
                    txtQty.Visible = False
                ElseIf bitLotNo Then
                    ibtnManageSerial.OnClientClick = "return SerialNo(" & numItemcode & ",0," & numWarehouseItemID & ")"
                    hplSerialLot.Visible = True
                    txtQty.Visible = False
                End If

                If CCommon.ToLong(hdnItemCode.Value) > 0 AndAlso Not bitKitParent Then
                    ibtnTransferStok.Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSaveInvetory_Click(sender As Object, e As EventArgs) Handles btnSaveInvetory.Click
        Try
            If CCommon.ToLong(hdnItemCodeEdit.Value) > 0 AndAlso CCommon.ToLong(ddlWareHouse.SelectedValue) > 0 Then
                Dim vcSerialLotNo As String
                Dim finalListSerialLot As New List(Of SerialLot)

                If CCommon.ToBool(hdnIsLotNo.Value) Or CCommon.ToBool(hdnIsSerialize.Value) Then
                    'IF INSERTING NEW WAREHOUSE RECORD SERIAL/LOT# ARE REQUIRED
                    'If CCommon.ToLong(hfWareHouseItemID.Value) = 0 AndAlso String.IsNullOrEmpty(txtSerialLot.Text.Trim()) Then
                    '    litMessage.Text = "Atleast one serial/lot# are required."
                    '    Exit Sub
                    'End If

                    If Not String.IsNullOrEmpty(txtSerialLot.Text.Trim()) Then
                        'FIRST VALIDATE SERIAL/LOT# NUMBERS ARE NOT DUPLICATED IN TEXTBOX
                        Dim rgx As New System.Text.RegularExpressions.Regex("^[^\(\)]*(\(\d+\)\([^\(\)]*\)){1,1}$")
                        Dim listSerialLot As List(Of String) = txtSerialLot.Text.Split(",").ToList()


                        If CCommon.ToBool(hdnIsLotNo.Value) Then
                            Dim tempLotNo As String
                            Dim tempQty As Int32
                            Dim dtExpirationDate As DateTime

                            For Each strSerialLot As String In listSerialLot
                                If Not rgx.IsMatch(strSerialLot) Then
                                    ShowMessage("Some Lot #s are not in valid format.")
                                    Exit Sub
                                Else
                                    tempLotNo = strSerialLot.Substring(0, strSerialLot.Trim().IndexOf("(")).Trim()

                                    If finalListSerialLot.FindIndex(Function(x) x.vcSerialLot.Equals(tempLotNo, StringComparison.OrdinalIgnoreCase)) <> -1 Then
                                        ShowMessage("Duplicate Lot #s are not allowed.")
                                        Exit Sub
                                    End If

                                    If Not Int32.TryParse(strSerialLot.Substring(strSerialLot.Trim().IndexOf("(") + 1, strSerialLot.IndexOf(")") - strSerialLot.Trim().IndexOf("(") - 1), tempQty) Then
                                        ShowMessage("Some Lot #s quantity value is invalid.")
                                        Exit Sub
                                    End If

                                    If Not DateTime.TryParse(DateFromFormattedDate(strSerialLot.Substring(strSerialLot.Trim().LastIndexOf("(") + 1, strSerialLot.LastIndexOf(")") - strSerialLot.LastIndexOf("(") - 1), Session("DateFormat")), dtExpirationDate) Then
                                        ShowMessage("Some Lot #s expiration date is invlaid.")
                                        Exit Sub
                                    End If

                                    finalListSerialLot.Add(New SerialLot With {.vcSerialLot = tempLotNo, .numQty = tempQty, .dtExpirationDate = dtExpirationDate})
                                End If
                            Next
                        ElseIf CCommon.ToBool(hdnIsSerialize.Value) Then
                            For Each strSerialLot As String In listSerialLot
                                If finalListSerialLot.FindIndex(Function(x) x.vcSerialLot.Equals(strSerialLot.Trim(), StringComparison.OrdinalIgnoreCase)) <> -1 Then
                                    ShowMessage("Duplicate serial #s are not allowed.")
                                    Exit Sub
                                Else
                                    finalListSerialLot.Add(New SerialLot With {.vcSerialLot = strSerialLot.Trim(), .numQty = 1, .dtExpirationDate = Nothing})
                                End If
                            Next
                        End If

                        'NOW CHECK IF SERIAL LOT# ARE NOT DUPLICATED ACROSS WAREHOUSES
                        'NOTE: USER CAN NOT ADD DUPLICATE SERIAL NUMBER IRESPECTIVE OF WAREHOUSE OR LOCATION
                        'NOTE: USER CAN ADD DUPLICATE LOT NUMBER IN UNIQUE WAREHOUSE AND LOCATION BUT NOT IN SAME
                        Dim objItem As New CItems
                        objItem.ItemCode = hdnItemCodeEdit.Value
                        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItem.WarehouseID = ddlWareHouse.SelectedValue
                        objItem.WareHouseItemID = CCommon.ToLong(hfWareHouseItemID.Value)
                        objItem.bitSerialized = CCommon.ToBool(hdnIsSerialize.Value)

                        Dim xEle = New XElement("SerialLots", _
                                    From strSerialLot In finalListSerialLot _
                                    Select New XElement("SerialLot", _
                                                        New XElement("vcSerialLot", strSerialLot.vcSerialLot), _
                                                        New XElement("numQty", strSerialLot.numQty), _
                                                        New XElement("dtExpirationDate", strSerialLot.dtExpirationDate)
                                                        )
                                        )

                        vcSerialLotNo = xEle.ToString()
                        objItem.SerialNo = vcSerialLotNo

                        'RETURNS TRUE IF SERIAL/LOT# ARE DUPLICATE
                        If objItem.ValidateNewSerialLotNo() Then
                            If CCommon.ToBool(hdnIsSerialize.Value) Then
                                ShowMessage("Duplicate serial #s are not allowed. Doen't matter if external & internal location is unique.")
                            Else
                                ShowMessage("Duplicate lot #s are not allowed in unique external & internal location.")
                            End If

                            Exit Sub
                        End If
                    End If
                End If



                Dim objItems As New CItems
                objItems.ItemCode = hdnItemCodeEdit.Value
                objItems.WarehouseID = ddlWareHouse.SelectedValue
                objItems.WareHouseItemID = CCommon.ToLong(hfWareHouseItemID.Value)
                objItems.WarehouseLocationID = CCommon.ToLong(ddlWarehouseLocation.SelectedValue)
                objItems.WLocation = CCommon.ToString(ddlWarehouseLocation.Text)
                objItems.WListPrice = CCommon.ToDecimal(txtListPrice.Text.Trim())
                objItems.OnHand = CCommon.ToDouble(txtOnHand.Text.Trim())
                objItems.ReOrder = CCommon.ToDouble(txtReOrder.Text.Trim())
                objItems.WSKU = txtSKU.Text.Trim
                objItems.WBarCode = txtUPC.Text.Trim
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")
                objItems.SerialNo = vcSerialLotNo
                objItems.IsCreateGlobalLocation = chkGlobalLocation.Checked
                objItems.byteMode = 1

                'RETRIVE MATRIX ITEM ATTRIBUTES VALUE
                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("Fld_ID")
                dtCusTable.Columns.Add("Fld_Value")
                dtCusTable.TableName = "CusFlds"
                Dim drCusRow As DataRow

                If (CCommon.ToLong(hfWareHouseItemID.Value) = 0 Or CCommon.ToBool(hdnIsNewAddedWarehouseEdited.Value)) AndAlso Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 0 AndAlso dsAttributes.Tables(0).Rows.Count > 0 Then
                    For Each dr In dsAttributes.Tables(0).Rows
                        drCusRow = dtCusTable.NewRow
                        drCusRow("Fld_ID") = dr("fld_id")

                        If dr("fld_type") = "SelectBox" Then
                            Dim ddl As DropDownList
                            ddl = plhWareHouseAttributes.FindControl(dr("Fld_id"))

                            If CCommon.ToLong(ddl.SelectedValue) > 0 Then
                                drCusRow("Fld_Value") = CStr(ddl.SelectedItem.Value)
                            Else
                                ShowMessage(dr("Fld_label") & " is required.")
                                Exit Sub
                            End If
                        ElseIf dr("fld_type") = "CheckBox" Then
                            Dim chk As CheckBox
                            chk = plhWareHouseAttributes.FindControl(dr("Fld_id"))
                            If chk.Checked = True Then
                                drCusRow("Fld_Value") = "1"
                            Else : drCusRow("Fld_Value") = "0"
                            End If
                        End If

                        dtCusTable.Rows.Add(drCusRow)
                        dtCusTable.AcceptChanges()
                    Next
                End If

                Dim ds1 As New DataSet
                ds1.Tables.Add(dtCusTable)
                objItems.strFieldList = ds1.GetXml

                If CCommon.ToLong(hfWareHouseItemID.Value) = 0 AndAlso dsAttributes Is Nothing AndAlso objItems.IsDuplicateWarehouseLocation() Then
                    ShowMessage("Record with same external and internal location is already added.")
                    Exit Sub
                ElseIf CCommon.ToLong(hfWareHouseItemID.Value) = 0 And dtCusTable.Rows.Count > 0 And objItems.IsDuplicateLocationAttributes() Then
                    ShowMessage("Record with same attriubtes in external and internal location is already added. Please select different values for attributes.")
                    Exit Sub
                End If

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    objItems.SaveWarehouseItems()

                    If CCommon.ToLong(hfWareHouseItemID.Value) = 0 Then
                        If Not CCommon.ToBool(hdnIsSerialize.Value) AndAlso Not CCommon.ToBool(hdnIsLotNo.Value) And Not CCommon.ToBool(hdnIsKit.Value) Then
                            objItems.MakeItemQtyAdjustmentJournal(hdnItemCodeEdit.Value, CCommon.ToString(hdnItemName.Value), CCommon.ToDouble(txtOnHand.Text), CCommon.ToDecimal(hdnAverageCost.Value), CCommon.ToLong(hdnAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        End If
                    ElseIf CCommon.ToBool(hdnIsSerialize.Value) AndAlso finalListSerialLot.Count > 0 Then
                        objItems.MakeItemQtyAdjustmentJournal(hdnItemCodeEdit.Value, CCommon.ToString(hdnItemName.Value), finalListSerialLot.Sum(Function(x) x.numQty), CCommon.ToDecimal(hdnAverageCost.Value), CCommon.ToLong(hdnAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                    ElseIf CCommon.ToBool(hdnIsLotNo.Value) AndAlso finalListSerialLot.Count > 0 Then
                        objItems.MakeItemQtyAdjustmentJournal(hdnItemCodeEdit.Value, CCommon.ToString(hdnItemName.Value), finalListSerialLot.Sum(Function(x) x.numQty), CCommon.ToDecimal(hdnAverageCost.Value), CCommon.ToLong(hdnAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                    End If

                    objTransactionScope.Complete()
                    hdnIsNewAddedWarehouseEdited.Value = False
                End Using

                If CCommon.ToLong(hdnWarehouseItemID.Value) > 0 AndAlso CCommon.ToLong(hfWareHouseItemID.Value) = 0 Then
                    ShowMessage("Record is added successfully. Go back to item details -> inventory to view it.")
                End If

                Dim tempId As Long = CCommon.ToLong(hfWareHouseItemID.Value)

                ClearFields()

                'IF USER CAME TO THIS PAGE FROM ITEM LIST PAGE THEN CLEAR HIDDEN FIELD VALUE BECAUSE EACH ITEM CAN HAVE DIFFERENT VALUES FOR FIELD
                If CCommon.ToLong(hdnItemCode.Value) = 0 Then
                    hdnItemCodeEdit.Value = ""
                    hdnIsSerialize.Value = ""
                    hdnIsLotNo.Value = ""
                    hdnIsAssembly.Value = ""
                    hdnAverageCost.Value = ""
                    hdnItemName.Value = ""
                    hdnAssetChartAcntID.Value = ""

                    hdnAttributes.Value = ""
                End If

                BindGrid()

                If tempId > 0 Then
                    For Each row As GridViewRow In gvItems.Rows
                        If gvItems.DataKeys(row.RowIndex)("numWareHouseItemID") = tempId Then
                            row.Focus()
                            row.CssClass = "lightYellowTr"
                            Exit For
                        End If
                    Next
                End If

                ddlWareHouse.Focus()
            Else
                ShowMessage("Select warehouse.")
                ddlWareHouse.Focus()
            End If
        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_SERIALLOT") Then
                If CCommon.ToBool(hdnIsSerialize.Value) Then
                    ShowMessage("Duplicate serial #s are not allowed. Doen't matter if external & internal location is unique.")
                Else
                    ShowMessage("Duplicate lot #s are not allowed in unique external & internal location.")
                End If
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub ShowHideElement()
        Try
            If String.IsNullOrEmpty(hdnAttributes.Value) AndAlso CCommon.ToLong(hdnItemGroup.Value) > 0 Then
                Dim objItems = New CItems
                objItems.ItemCode = CCommon.ToLong(hdnItemCode.Value)
                objItems.byteMode = 1
                objItems.WareHouseItemID = CCommon.ToLong(hdnWarehouseItemID.Value)
                Dim ds As DataSet = objItems.GetItemWareHouses()

                dsAttributes = New DataSet
                dsAttributes.Tables.Add(ds.Tables(2).Copy())

                hdnAttributes.Value = System.Web.HttpUtility.HtmlEncode(dsAttributes.GetXml())
            End If

            'MATRIX ITEM
            trMatrixItem.Visible = If(CCommon.ToLong(hdnItemGroup.Value) > 0, True, False)
            divListPrice.Visible = trMatrixItem.Visible
            If CCommon.ToLong(hdnItemGroup.Value) > 0 AndAlso Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 0 Then
                AttributesControl(dsAttributes.Tables(0))
            End If

            'ASSEMBLY & KIT ITEM
            Dim bitKit As Boolean = CCommon.ToBool(hdnIsKit.Value)
            Dim bitAssembly = CCommon.ToBool(hdnIsAssembly.Value)
            trInventory.Visible = Not (bitKit Or bitAssembly)

            'SERIAL LOT ITEM
            Dim bitSerial As Boolean = CCommon.ToBool(hdnIsSerialize.Value)
            Dim bitLot As Boolean = CCommon.ToBool(hdnIsLotNo.Value)
            divOnHand.Visible = Not (bitSerial Or bitLot)

            tblSerialLot.Visible = (bitSerial Or bitLot)
            trSerial.Visible = bitSerial
            trLot.Visible = bitLot
            txtSerialLot.ReadOnly = bitLot

            ddlWareHouse.SelectedValue = CCommon.ToLong(hdnWarehouseID.Value)

            If CCommon.ToLong(hdnWarehouseItemID.Value) > 0 Then
                ddlWareHouse.Enabled = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ClearFields()
        Try
            If CCommon.ToLong(hdnItemCode.Value) > 0 Then
                btnSaveInvetory.Text = "Add"
                btnSaveInvetory.Visible = True
            Else
                btnSaveInvetory.Text = "Update"
                btnSaveInvetory.Visible = False
            End If

            'Edit WarehouseItem
            If CCommon.ToLong(hfWareHouseItemID.Value) > 0 Then
                ddlWarehouseLocation.SelectedValue = "0"
                txtListPrice.Text = ""

                trInventory.Visible = True
                txtOnHand.Text = ""
                txtOnHand.Visible = True
                lblOnHand.Visible = False

                txtReOrder.Text = ""

                trMatrixItem.Visible = False
                divListPrice.Visible = False
                txtSKU.Text = ""
                txtUPC.Text = ""

                tblSerialLot.Visible = False
                txtLotNumber.Text = ""
                txtQuantity.Text = ""
                rdpExpirationDate.SelectedDate = Nothing
                txtSerialLot.Text = ""

                plhWareHouseAttributes.Controls.Clear()

                If CCommon.ToLong(hdnItemCode.Value) > 0 Then
                    ShowHideElement()
                End If
            End If

            hfWareHouseItemID.Value = ""
            ddlWareHouse.Enabled = True
            ddlWarehouseLocation.Enabled = True

            btnCancel.Visible = False
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Class SerialLot
        Public Property vcSerialLot As String
        Public Property numQty As Int32
        Public Property dtExpirationDate As DateTime?
    End Class

    Private Sub ibtnAddLot_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnAddLot.Click
        Try
            Dim vcLotNo As String
            Dim qty As Int32
            Dim expirationDate As Date

            Dim strErrorMessage As String = ""

            If String.IsNullOrEmpty(txtLotNumber.Text.Trim()) Then
                strErrorMessage = "Lot # is required. <br />"
            End If

            If Not Int32.TryParse(txtQuantity.Text, qty) Then
                strErrorMessage = strErrorMessage & "Lot # quantity is invalid. <br />"
            ElseIf Not qty > 0 Then
                strErrorMessage = strErrorMessage & "Lot # quantity is required. <br />"
            End If

            If rdpExpirationDate.SelectedDate Is Nothing Then
                strErrorMessage = strErrorMessage & "Lot # Expiration Date is required. <br />"
            ElseIf Not DateTime.TryParse(rdpExpirationDate.SelectedDate, expirationDate) Then
                strErrorMessage = strErrorMessage & "Lot # Expiration Date is invalid. <br />"
            End If

            If Not String.IsNullOrEmpty(strErrorMessage) Then
                ShowMessage(strErrorMessage)
                Exit Sub
            End If

            vcLotNo = txtLotNumber.Text

            txtSerialLot.Text = If(String.IsNullOrEmpty(txtSerialLot.Text), vcLotNo & "(" & qty & ")" & "(" & FormattedDateFromDate(expirationDate, Session("DateFormat")) & ")", txtSerialLot.Text & "," & vcLotNo & "(" & qty & ")" & "(" & FormattedDateFromDate(expirationDate, Session("DateFormat")) & ")")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radItemGroup_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radItemGroup.SelectedIndexChanged
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ibtnClear_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnClear.Click
        Try
            txtSerialLot.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String, Optional ByVal isSuccess As Boolean = False)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()

            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ChangeMessageCSS", "ChangeMessageCSS(" & If(isSuccess, 1, 0) & ")", True)

        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCloseStockTransfer_Click(sender As Object, e As EventArgs) Handles btnCloseStockTransfer.Click
        Try
            lblTransferExc.Text = ""
            hdnTransferFromWarehouse.Value = ""
            hdnIsSerial.Value = ""
            hdnIsLot.Value = ""
            txtQty.Visible = True
            hplSerialLot.Visible = False
            hplSerialLot.Text = "Added (0)"
            hdnSelectedSerialLot.Value = ""
            txtQty.Text = ""
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('hide');$('.modal-backdrop').remove();", True)
        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSaveStockTransfer_Click(sender As Object, e As EventArgs) Handles btnSaveStockTransfer.Click
        Try
            Dim qty As Int32 = 0
            Dim serialLot As String = ""

            If CCommon.ToBool(hdnIsLot.Value) Or CCommon.ToBool(hdnIsSerial.Value) Then
                If Not String.IsNullOrEmpty(hdnSelectedSerialLot.Value) Then
                    Dim arrSeriaLot As String() = hdnSelectedSerialLot.Value.Split(",")

                    For Each item As String In arrSeriaLot
                        qty += CCommon.ToInteger(item.Split("-")(1))

                        If CCommon.ToBool(hdnIsLot.Value) Then
                            serialLot = IIf(String.IsNullOrEmpty(serialLot), item.Split("-")(0) & "(" & item.Split("-")(1) & ")", serialLot & "," & item.Split("-")(0) & "(" & item.Split("-")(1) & ")")
                        Else
                            serialLot = IIf(String.IsNullOrEmpty(serialLot), item.Split("-")(0), serialLot & "," & item.Split("-")(0))
                        End If
                    Next
                End If
            Else
                Int32.TryParse(txtQty.Text, qty)
            End If

            If qty > 0 AndAlso radTransferToWarehouse.SelectedValue > 0 Then
                Try
                    If (CCommon.ToBool(hdnIsLot.Value) Or CCommon.ToBool(hdnIsSerial.Value)) AndAlso String.IsNullOrEmpty(serialLot) Then
                        ShowStockTransferError("Serial/Lot# are required.")
                        Exit Sub
                    End If

                    Dim objItem As New CItems
                    objItem.DomainID = Session("DomainID")
                    objItem.UserCntID = Session("UserContactID")
                    objItem.WareHouseItemID = CCommon.ToLong(hdnTransferFromWarehouse.Value)
                    objItem.StockTransferToWarehouseItemID = CCommon.ToLong(radTransferToWarehouse.SelectedValue)
                    objItem.NoofUnits = qty
                    objItem.SerialNo = serialLot
                    objItem.InventoryTransfer()

                    lblTransferExc.Text = ""
                    hdnSelectedSerialLot.Value = ""
                    hdnTransferFromWarehouse.Value = ""
                    hdnIsSerial.Value = ""
                    hdnIsLot.Value = ""
                    txtQty.Visible = True
                    hplSerialLot.Visible = False
                    hplSerialLot.Text = "Added (0)"
                    txtQty.Text = ""
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('hide');$('.modal-backdrop').remove(); document.location.href=document.location.href;", True)
                Catch ex As Exception
                    If ex.Message.Contains("INSUFFICIENT_ONHAND_QUANTITY") Then
                        lblTransferExc.Text = "Qty to transfer is more than on hand quantity"
                    ElseIf ex.Message.Contains("INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY") Then
                        lblTransferExc.Text = "Invalid serial/lot number or lot quantity is more than available quantity."
                    Else
                        lblTransferExc.Text = ex.Message
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');$('.modal-backdrop').remove();", True)
                End Try

            Else
                lblTransferExc.Text = "Select transfer quantity and warehouse."
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');$('.modal-backdrop').remove();", True)
            End If


        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ShowStockTransferError(ByVal msg As String)
        lblTransferExc.Text = msg
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');$('.modal-backdrop').remove();", True)
    End Sub
End Class