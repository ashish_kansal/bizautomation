﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Public Class frmWarehouseLocationAdd
    Inherits BACRMPage
    Dim lngWLocationID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                bindWarehouse()
                lngWLocationID = CCommon.ToLong(GetQueryStringVal("WLocationID"))
                If Not ddlWarehouse.Items.FindByValue(CCommon.ToString(lngWLocationID)) Is Nothing Then
                    ddlWarehouse.Items.FindByValue(CCommon.ToString(lngWLocationID)).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub bindWarehouse()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            ddlWarehouse.DataSource = objItem.GetWareHouses
            ddlWarehouse.DataTextField = "vcWareHouse"
            ddlWarehouse.DataValueField = "numWareHouseID"
            ddlWarehouse.DataBind()
            ddlWarehouse.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            objItem.WarehouseID = ddlWarehouse.SelectedValue
            objItem.WarehouseLocationID = 0
            objItem.str = GetItems()
            objItem.ManageWarehouseLocation()

            Response.Redirect("frmWarehouseLocation.aspx?WarehouseID=" & CCommon.ToLong(ddlWarehouse.SelectedValue))
            'BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            CCommon.AddColumnsToDataTable(dt, "vcAisle,vcRack,vcShelf,vcBin,vcLocation,bitDefault,bitSystem,intQty")
            Dim Locations() As String = hdnLocationString.Value.Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)

            Dim LocationSplit() As String
            For i As Integer = 0 To Locations.Length - 1
                Dim dr As DataRow = dt.NewRow
                LocationSplit = Locations(i).Split(".")

                If LocationSplit.Length >= 1 Then
                    dr("vcAisle") = LocationSplit(0)
                    If LocationSplit.Length >= 2 Then dr("vcRack") = LocationSplit(1) Else dr("vcRack") = ""
                    If LocationSplit.Length >= 3 Then dr("vcShelf") = LocationSplit(2) Else dr("vcShelf") = ""
                    If LocationSplit.Length >= 4 Then dr("vcBin") = LocationSplit(3) Else dr("vcBin") = ""

                    dr("vcLocation") = dr("vcAisle") + "." + dr("vcRack") + "." + dr("vcShelf") + "." + dr("vcBin")
                    dr("vcLocation") = dr("vcLocation").ToString.Replace("...", "").Replace("..", "").Trim(".")
                    dr("bitDefault") = 0
                    dr("bitSystem") = 0
                    dr("intQty") = 0
                    dt.Rows.Add(dr)
                End If

            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmWarehouseLocation.aspx?WarehouseID=" & CCommon.ToLong(ddlWarehouse.SelectedValue))
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
End Class