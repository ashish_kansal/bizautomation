<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmWarehouses.aspx.vb"
    Inherits=".frmWarehouses" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Warehouses</title>
    <script type="text/javascript">
        function New() {
            document.location.href = "frmAddWareHouse.aspx";
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <input type="button" runat="server" class="btn btn-primary" id="btnNew" value="New WareHouse" onclick="New()" />&nbsp;
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Warehouses&nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frmwarehouses.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgWarehouse" AllowSorting="True" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numWarehouseID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcWarehouse" SortExpression="vcWarehouse" HeaderText="Warehouse" CommandName="Warehouse"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcAddress" HeaderText="Address"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcPrintNodeAPIKey" HeaderText="PrintNode API Key"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcPrintNodePrinterID" HeaderText="PrintNode Printer ID"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="20">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CssClass="btn btn-xs btn-danger"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

</asp:Content>
