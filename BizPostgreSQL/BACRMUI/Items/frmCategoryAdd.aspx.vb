﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Telerik.Web.UI
Imports System.IO

Partial Public Class frmCategoryAdd
    Inherits BACRMPage
#Region "Global Declaration"

    Dim strFileName As String
    Dim strFileTName As String
    Dim strFilePath As String
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            GetUserRightsForPage(13, 34)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                btnSaveAndClose.Visible = False
            End If
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSaveAndClose.Visible = False
            End If

            If Not IsPostBack Then
                BindMetrixGroup()
                btnSaveAndClose.Attributes.Add("onclick", "return Save()")
                hdnCategoryProfile.Value = CCommon.ToLong(GetQueryStringVal("numCategoryProfileID"))
                FillCategory(rtvParentCategory)
                If Me.CategoryID > 0 Then
                    SelectCategory()
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
    Protected Sub btnSaveAndClose_Click(sender As Object, e As EventArgs) Handles btnSaveAndClose.Click
        Try
            If txtCategoryName.Text <> "" Then
                UploadImage()

                Dim objItems As CItems
                If objItems Is Nothing Then objItems = New CItems
                objItems.CategoryID = Me.CategoryID
                objItems.CategoryProfileID = CCommon.ToLong(hdnCategoryProfile.Value)
                objItems.CategoryName = txtCategoryName.Text.Trim
                objItems.CategoryNameURL = txtFriendURL.Text.Trim()
                objItems.CategoryDescription = txtDescription.Content.Trim
                objItems.DisplayOrder = CCommon.ToInteger(txtSortOrder.Text)
                objItems.PathForImage = HiddenField2.Value
                objItems.DomainID = Session("DomainID")
                objItems.CategoryMetaTitle = CCommon.ToString(txtMetaTitle.Text)
                objItems.CategoryMetaKeywords = CCommon.ToString(txtMetaKeywords.Text)
                objItems.CategoryMetaDescription = CCommon.ToString(txtMetaDescription.Text)
                objItems.MatrixGroups = String.Join(",", rcbMatrixGroup.CheckedItems.Select(Function(x) x.Value).ToArray())
                objItems.ParentCategoryID = GetSelectedCategory()

                objItems.ManageCategory()

                txtCategoryName.Text = ""
                txtDescription.Content = ""

                If Me.ItemID = 0 Then
                    Response.Redirect("~/Items/frmCategories.aspx?ItemCode", False)
                Else
                    Response.Redirect("~/Items/frmKitDetails.aspx?ItemCode=" & Me.ItemID, False)
                End If
            Else
                litMessage.Text = "Enter Category Name."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
    Protected Sub hlDeleteImage_Click(sender As Object, e As EventArgs) Handles lbDeleteImage.Click
        Try
            DeleteImageFromFolder()
            HiddenField2.Value = ""
            DeleteImageDetails()
            FillCategory(rtvParentCategory)
            If Me.CategoryID > 0 Then
                SelectCategory()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            If Me.ItemID = 0 Then
                Response.Redirect("~/Items/frmCategories.aspx?ItemCode", False)
            Else
                Response.Redirect("~/Items/frmKitDetails.aspx?ItemCode=" & Me.ItemID, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

#End Region

#Region "Methods"
#Region "Category"
    Private Sub SelectCategory()
        Try
            Dim objItems As CItems
            objItems = New CItems
            objItems.byteMode = 17
            objItems.DomainID = Session("DomainID")
            objItems.SiteID = IIf(Not Session("SiteID") Is Nothing, Session("SiteID"), 0)
            objItems.CategoryID = Me.CategoryID

            Dim dtCategory As DataTable
            dtCategory = objItems.SeleDelCategory

            If dtCategory.Rows.Count > 0 Then
                txtCategoryName.Text = CCommon.ToString(dtCategory.Rows(0)("vcCategoryName"))
                txtFriendURL.Text = CCommon.ToString(dtCategory.Rows(0)("vcCategoryNameURL"))
                txtSortOrder.Text = CCommon.ToString(dtCategory.Rows(0)("intDisplayOrder"))
                txtDescription.Content = CCommon.ToString(dtCategory.Rows(0)("vcDescription"))
                txtMetaTitle.Text = CCommon.ToString(dtCategory.Rows(0)("vcMetaTitle"))
                txtMetaKeywords.Text = CCommon.ToString(dtCategory.Rows(0)("vcMetaKeywords"))
                txtMetaDescription.Text = CCommon.ToString(dtCategory.Rows(0)("vcMetaDescription"))

                Dim lngParentCategoryID As Long = CCommon.ToLong(dtCategory.Rows(0)("numDepCategory"))
                If lngParentCategoryID = 0 Then
                    If rtvParentCategory.Nodes.Count > 0 Then
                        rtvParentCategory.Nodes(0).Checked = True
                    End If
                Else
                    For Each treeNode As RadTreeNode In rtvParentCategory.Nodes
                        If treeNode.Value = lngParentCategoryID Then
                            treeNode.Checked = True
                        Else
                            FindNode(treeNode, lngParentCategoryID)
                        End If

                    Next
                End If
                If CCommon.ToString(dtCategory.Rows(0)("vcPathForCategoryImage")) <> "" Then
                    lbDeleteImage.Visible = True
                    imgCategory.Visible = True
                    imgCategory.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtCategory.Rows(0).Item("vcPathForCategoryImage")
                    HiddenField2.Value = dtCategory.Rows(0).Item("vcPathForCategoryImage")
                Else
                    imgCategory.Visible = False
                    lbDeleteImage.Visible = False
                End If

                Dim objCategoryMatrixGroup As New CategoryMatrixGroup
                objCategoryMatrixGroup.DomainID = Ccommon.ToLong(Session("DomainID"))
                objCategoryMatrixGroup.CategoryID = objItems.CategoryID
                Dim dtMatrixGroups As DataTable = objCategoryMatrixGroup.GetByCategoryID()

                If Not dtMatrixGroups Is Nothing AndAlso dtMatrixGroups.Rows.Count > 0 Then
                    For Each dr As DataRow In dtMatrixGroups.Rows
                        If Not rcbMatrixGroup.Items.FindItemByValue(CCommon.ToLong(dr("numItemGroup"))) Is Nothing Then
                            rcbMatrixGroup.Items.FindItemByValue(CCommon.ToLong(dr("numItemGroup"))).Checked = True
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub FillCategory(ByVal treeView As RadTreeView)
        Try
            Dim objItems As CItems
            objItems = New CItems

            objItems.byteMode = 18
            objItems.DomainID = Session("DomainID")
            objItems.CategoryID = Me.CategoryID 'exclude current category
            objItems.CategoryProfileID = CCommon.ToLong(hdnCategoryProfile.Value)

            Dim dtCategory As DataTable
            dtCategory = objItems.SeleDelCategory

            Try
                treeView.DataTextField = "vcCategoryName"
                treeView.DataFieldID = "numCategoryID"
                treeView.DataValueField = "numCategoryID"
                treeView.DataFieldParentID = "numDepCategory"
                treeView.DataSource = dtCategory
                treeView.DataBind()
            Catch ex As Exception
                litMessage.Text = ex.Message
            End Try

            Try
                Dim treeNode As RadTreeNode
                treeNode = New RadTreeNode
                treeNode.Text = "-- No Parent Category --"
                treeNode.Value = "0"
                treeView.Nodes.Insert(0, treeNode)

                If Me.CategoryID <> 0 Then
                    RemoveCurrentCategoryNode(CCommon.ToString(Me.CategoryID))
                End If
            Catch ex As Exception
                litMessage.Text = ex.Message
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Private Sub BindMetrixGroup()
        Try
            Dim objItems As New CItems
            objItems.DomainID = CCommon.ToLong(Session("DomainID"))
            objItems.ItemCode = 0
            rcbMatrixGroup.DataSource = objItems.GetItemGroups.Tables(0)
            rcbMatrixGroup.DataTextField = "vcItemGroup"
            rcbMatrixGroup.DataValueField = "numItemGroupID"
            rcbMatrixGroup.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function GetSelectedCategory() As Long
        Try
            Dim lngCategory As Long = 0

            If rtvParentCategory.CheckedNodes.Count > 0 Then
                For Each item As RadTreeNode In rtvParentCategory.CheckedNodes
                    If item.Checked = True Then
                        'If item.Index <> 0 Then
                        lngCategory = item.Value
                        'End If
                        Return lngCategory
                    End If
                Next
            End If
            Return lngCategory
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub FindNode(treeNode As RadTreeNode, ByVal lngSelectedNodeValue As Long)
        Try
            For Each tn As RadTreeNode In treeNode.Nodes
                If tn.Value = lngSelectedNodeValue Then
                    tn.Checked = True
                End If
                FindNode(tn, lngSelectedNodeValue)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RemoveCurrentCategoryNode(ByVal strCategoryID As String)
        Try
            For Each treeNode As RadTreeNode In rtvParentCategory.Nodes
                If treeNode.Value = strCategoryID Then
                    treeNode.Remove()
                    Return
                Else
                    FindNodeAndRemove(treeNode, strCategoryID)
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub FindNodeAndRemove(treeNode As RadTreeNode, ByVal strCategoryID As String)
        Try
            For Each tn As RadTreeNode In treeNode.Nodes
                If tn.Value = strCategoryID Then
                    tn.Remove()
                    Return
                End If
                FindNodeAndRemove(tn, strCategoryID)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Image"
    Private Sub UploadImage()
        Try
            If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If
            Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
            Dim ArrFilePath, ArrFileExt
            Dim strFileExt As String

            If txtThumbFile.PostedFile.FileName <> "" Then
                If HiddenField2.Value <> "" Then
                    DeleteImageFromFolder()
                End If

                strFilePath = FilePath
                ArrFileExt = Split(txtThumbFile.PostedFile.FileName, ".")
                strFileExt = ArrFileExt(UBound(ArrFileExt))
                strFileName = "Category" & Me.CategoryID & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".gif"
                strFilePath = strFilePath & strFileName
                If Not txtThumbFile.PostedFile Is Nothing Then txtThumbFile.PostedFile.SaveAs(strFilePath)
                HiddenField2.Value = strFileName
            Else : strFileName = HiddenField2.Value
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Private Sub LoadPic()
    '    Try
    '        Dim objItems As New CItems
    '        objItems.byteMode = 16
    '        objItems.DomainID = Session("DomainID")
    '        objItems.CategoryID = Me.CategoryID
    '        Dim dtTemp As DataTable = objItems.SeleDelCategory

    '        If dtTemp.Rows.Count > 0 Then
    '            If CCommon.ToString(dtTemp.Rows(0).Item("vcPathForCategoryImage")) <> "" Then
    '                imgCategory.Visible = True
    '                imgCategory.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtTemp.Rows(0).Item("vcPathForCategoryImage")
    '                HiddenField2.Value = dtTemp.Rows(0).Item("vcPathForCategoryImage")
    '            Else : imgCategory.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub DeleteImageDetails()
        Try
            Dim objItem As New CItems
            objItem.CategoryID = Me.CategoryID
            objItem.PathForImage = HiddenField2.Value
            objItem.UploadItemImage()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub DeleteImageFromFolder()
        Try
            Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
            strFilePath = FilePath
            strFileName = HiddenField2.Value
            strFilePath = strFilePath & strFileName

            If File.Exists(strFilePath) Then
                File.Delete(strFilePath)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#End Region

#Region "Custom Properties"
    Public ReadOnly Property CategoryID() As Long
        Get
            Dim o As Object = ViewState("ItemCode")
            If o IsNot Nothing Then
                Return CCommon.ToLong(o)
            Else
                If GetQueryStringVal("CategoryID") <> "" Then
                    ViewState("CategoryID") = GetQueryStringVal("CategoryID")
                    Return CCommon.ToLong(ViewState("CategoryID"))
                Else
                    Return 0
                End If
            End If
        End Get
    End Property
    Public ReadOnly Property ItemID() As Long
        Get
            Dim o As Object = ViewState("ItemCode")
            If o IsNot Nothing Then
                Return CCommon.ToLong(o)
            Else
                If GetQueryStringVal("ItemCode") <> "" And GetQueryStringVal("ItemCode") IsNot Nothing Then
                    ViewState("ItemCode") = GetQueryStringVal("ItemCode")
                    Return CCommon.ToLong(ViewState("ItemCode"))
                Else
                    Return 0
                End If
            End If
        End Get
    End Property
#End Region


    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            DisplayError(ex)
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As Exception)
        Try
            TryCast(Me.Master, ECommerceMenuMaster).ThrowError(exception)
        Catch ex As Exception

        End Try
    End Sub

End Class
