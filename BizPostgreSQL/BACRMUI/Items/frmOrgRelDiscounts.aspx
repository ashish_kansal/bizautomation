<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOrgRelDiscounts.aspx.vb" Inherits="frmOrgRelDiscounts"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Discounts</title>
		<script>
		function OrgAdd()
		{
			if (document.Form1.ddlItems.value==0)
			{
				alert("Select Item")
				document.Form1.ddlItems.focus();
				return false;
			}
			if ((document.Form1.ddlCompany.selectedIndex==-1 )||(document.Form1.ddlCompany.value==0))
			{
				alert("Select Company")
				document.Form1.txtCompName.focus();
				return false;
			}
		}
		function RelAdd()
		{
			if (document.Form1.ddlItems.value==0)
			{
				alert("Select Item")
				document.Form1.ddlItems.focus();
				return false;
			}
			if ((document.Form1.ddlRelationship.selectedIndex==-1 )||(document.Form1.ddlRelationship.value==0))
			{
				alert("Select Relationship")
				document.Form1.ddlRelationship.focus();
				return false;
			}
			if ((document.Form1.ddlProfile.selectedIndex==-1 )||(document.Form1.ddlProfile.value==0 ))
			{
				alert("Select Profile")
				document.Form1.ddlProfile.focus();
				return false;
			}
		}
		function Close()
		{
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<br>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Discounts&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1" align="right">
						Item or Kit &nbsp;
					</td>
					<td><asp:dropdownlist id="ddlItems" CssClass="signup" AutoPostBack="True" Runat="server"></asp:dropdownlist></td>
					<td class="normal1" align="right">
						Discount Profile&nbsp;</td>
					<td><asp:dropdownlist id="ddlDiscPro" CssClass="signup" AutoPostBack="True" Runat="server">
							<asp:ListItem Value="1">Organization Discounts</asp:ListItem>
							<asp:ListItem Value="2">Relationship Profile Discounts</asp:ListItem>
						</asp:dropdownlist></td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save" Width="50"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<asp:table id="Table2" Runat="server" Width="100%" BorderColor="black" Height="400" GridLines="None" CssClass="aspTable"
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table id="tblOrg" cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
							<tr>
								<td class="normal1" align="right">Organization</td>
								<td>
									<asp:textbox id="txtCompName" Runat="server" width="100" cssclass="signup"></asp:textbox>&nbsp;
									<asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
									<asp:dropdownlist id="ddlCompany" Runat="server" Width="200" CssClass="signup"></asp:dropdownlist>
									&nbsp;<asp:Button ID="btnAdd" Runat="server" Text="Add" Width="50" CssClass="button"></asp:Button>
								</td>
							</tr>
						</table>
						<table width="600" id="tblRel" runat="server">
							<tr>
								<td class="normal1" align="right">
									Relationship</td>
								<td>
									<asp:dropdownlist id="ddlRelationship" Width="200" AutoPostBack="True"  Runat="server" CssClass="signup"></asp:dropdownlist>
								</td>
								<td class="normal1" align="right">
									Profile
								</td>
								<td>
									<asp:dropdownlist id="ddlProfile" Runat="server" Width="200" CssClass="signup"></asp:dropdownlist>
									&nbsp;&nbsp;
									<asp:button id="btnRelAdd" CssClass="button" Runat="server" Width="50" Text="Add"></asp:button>
								</td>
							</tr>
						</table>
						<asp:datagrid id="dgOrgDisc" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="numDiscProfileID"></asp:BoundColumn>
								<asp:BoundColumn DataField="Company" HeaderText="Company, Division"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Discount off list price">
									<ItemTemplate>
										<asp:Label ID="lblDivisionID" Runat="server" style="display:none" Text= '<%# DataBinder.Eval(Container,"DataItem.numDivisionID") %>'>
										</asp:Label>
										<asp:TextBox ID="txtOrgDisc" Runat="server" Width="50" CssClass="signup" Text= '<%# DataBinder.Eval(Container,"DataItem.decDisc") %>'></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Apply this discount to ALL items for THIS Organization">
									<ItemTemplate>
										<asp:Label ID="lblOrgApplicale" Runat="server" style="display:none" Text= '<%# DataBinder.Eval(Container,"DataItem.bitApplicable") %>'>
										</asp:Label>
										<asp:CheckBox ID="chkOrgApply" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button ID="btnOrgDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
						<asp:datagrid id="dgRelPro" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							>
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="numDiscProfileID"></asp:BoundColumn>
								<asp:BoundColumn DataField="RelPro" HeaderText="Relationship, Profile"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Discount off list price">
									<ItemTemplate>
										<asp:Label ID="lblProfileID" Runat="server" style="display:none" Text= '<%# DataBinder.Eval(Container,"DataItem.numProID") %>'>
										</asp:Label>
										<asp:TextBox ID="txtRelDisc" Runat="server" Width="50" CssClass="signup" Text= '<%# DataBinder.Eval(Container,"DataItem.decDisc") %>'></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Apply this discount to ALL items for THIS Organization">
									<ItemTemplate>
										<asp:Label ID="lblRelApplicale" Runat="server" style="display:none" Text= '<%# DataBinder.Eval(Container,"DataItem.bitApplicable") %>'>
										</asp:Label>
										<asp:CheckBox ID="chkRelDisc" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button ID="btnRelDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table></form>
	</body>
</HTML>
