﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="frmInventoryAdjustmentInBatch.aspx.vb" Inherits=".frmInventoryAdjustmentInBatch" MasterPageFile="~/common/BizMaster.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc2" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Manage Inventory</title>

    <script src="../JavaScript/colResizable-1.5.min.js" type="text/javascript"></script>
    <%--<link href="../images/BizSkin/RadComboBox.css" rel="stylesheet"/>--%>
    <style type="text/css">
        .tblInventory td:first-child, .tblInventory td:nth-child(3) {
            font-weight: bold;
            text-align: right;
            white-space: nowrap;
        }

        .lightYellow {
            background-color: #fffde1 !important;
        }

        .lightYellowTr {
            animation: fade 5s forwards;
            background-color: rgba(242, 245, 169, 1);
        }

        @keyframes fade {
            from {
                background-color: #fffde1;
            }

            to {
                background-color: none !important;
            }
        }

        .rcbFooter {
            background-image: none !important;
        }

            /** Columns */
            .rcbHeader ul,
            .rcbFooter ul,
            .rcbItem ul,
            .rcbHovered ul,
            .rcbDisabled ul {
                margin: 0;
                padding: 0;
                width: 100%;
                display: inline-block;
                list-style-type: none;
            }

        .rcbHeader {
            padding: 5px 27px 4px 7px;
        }

        /** Multiple rows and columns */
        .multipleRowsColumns .rcbItem,
        .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px;
        }

        .col0 {
            width: 0%;
        }

        .col1 {
            width: 275px;
        }

        .col2,
        .col3,
        .col4,
        .col5 {
            width: 80px;
        }

        .col1,
        .col2,
        .col3,
        .col4,
        .col5 {
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            float: left;
        }
    </style>
    <script type="text/javascript">
        var RPPopup;
        //select all 
        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('#' + headerCheckBox).is(':checked'))
            });
        }

        function SetSelectedSerialLot(serialLot) {
            $("[id$=hdnSelectedSerialLot]").val(serialLot);
            var qty = 0;
            var array = serialLot.split(",");
            $.each(array, function (i) {
                qty += parseInt(array[i].toString().split("-")[1]);
            });
            $("[id$=hplSerialLot]").text("Added (" + qty + ")");
        }

        function OpenSertailLot(oppID, oppItemID, itemID, warehouseItemID, itemType) {
            window.open('../opportunity/frmAddSerialLotNumbers.aspx?oppID=' + oppID + '&oppItemID=' + oppItemID + '&itemID=' + itemID + '&warehouseItemID=' + warehouseItemID + "&itemType=" + itemType + '&type=1', '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
            return false;
        }

        //Sorting
        function SortColumn(a) {
            $("[id$=txtSortColumn]").val(a);
            if ($("[id$=txtSortColumn]").val() == a) {
                if ($("[id$=txtSortOrder]").val() == 'A')
                    $("[id$=txtSortOrder]").val('D');
                else
                    $("[id$=txtSortOrder]").val('A');
            }
            $('[id$=btnGo1]').trigger('click');
            return false;
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        //Used by Update Panel
        function pageLoaded() {
            BindInlineEdit();
            BindJquery();

            if ($("[id$=divAttributes]") != NaN && $("[id$=ddlWarehouseLocation]").val() > 0 && $("[id$=btnSaveInvetory]").val() == "Add") {
                $("[id$=divGlobalAttributes]").show();
            }

            //Highlight selected row
            $(".table-bordered tr").click(function () {
                $('.table-bordered tr>td').each(function () {
                    $(this).css('background-color', '');
                });
                $(this).find('td').css('background-color', '#D6D7D8');
            });

            $("[id$=gvSearch]").colResizable({
                //fixed: false,
                liveDrag: true,
                gripInnerHtml: "<div class='grip2'></div>",
                draggingClass: "dragging",
                minWidth: 30,
                onResize: onGridColumnResized
            });

            $("[id$=txtCounted]").each(function () {
                var CurrentRow = $(this).parents("tr").filter(':first')
                var AdjQty = (Number($(CurrentRow).find("[id$=txtCounted]").val()) - Number($(CurrentRow).find("[id$=lblOnHandQty]").text())).toPrecision(8);

                var TotalQty = (Number($(CurrentRow).find("[id$=lblOnHandQty]").text()) + Number(AdjQty)).toPrecision(8);
                if (AdjQty != 0) {
                    if (AdjQty > 0) { $(CurrentRow).find("[id$=txtAdjQty]").val("+" + AdjQty.toString()).css("color", "green"); }
                    else { $(CurrentRow).find("[id$=txtAdjQty]").val(AdjQty.toString()).css("color", "red"); }
                }
                var NewInventoryValue = (Number($(CurrentRow).find("[id$=txtCounted]").val()) * Number($(CurrentRow).find("[id$=lblAverageCost]").text()))
                $(CurrentRow).find("[id$=lblNewInventoryValue]").text(parseFloat(NewInventoryValue).toFixed(2));

            })

            UpdateTotalFooter();

            $("[id$=txtCounted]").change(function () {
                UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
                UpdateTotalFooter();
            });

            $("[id$=txtCounted]").keypress(function () {
                var CurrentRow = $(this).parents("tr").filter(':first');
                var chk = CurrentRow.find("[id$=chk]");
                if (chk != null) {
                    chk.prop("checked", true);
                }
            });

            var flag = 0;
            $("[id$=txtAdjQty]").keypress(function () {
                var CurrentRow = $(this).parents("tr").filter(':first');
                var chk = CurrentRow.find("[id$=chk]");
                if (chk != null) {
                    chk.prop("checked", true);
                }


                if (CheckNumberAllowPlus(2, event)) {
                    flag = 1;
                }

            });

            $("[id$=txtAdjQty]").focusout(function () {
                if (flag = 1) {
                    var CurrentRow = $(this).parents("tr").filter(':first');
                    var CurrentOnHand = Number($(CurrentRow).find("[id$=lblOnHandQty]").text());
                    CurrentOnHand = (CurrentOnHand + Number($(CurrentRow).find("[id$=txtAdjQty]").val())).toPrecision(8);
                    if (CurrentOnHand < 0) {
                        alert('Adjustment can not be less than current on hand qty!');
                        $(CurrentRow).find("[id$=txtAdjQty]").val('');
                        return;
                    }
                    if (!isNaN(CurrentOnHand)) {
                        $(CurrentRow).find("[id$=txtCounted]").val(CurrentOnHand.toString());
                        UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
                    }
                }

            });


            $("[id$=chkAll]").change(function () {
                $("[id$=gvItems] tr input[type = 'checkbox']").not("[id$=chkAll]").prop("checked", $("[id$=chkAll]").is(':checked'))
                $("[id$=gvItems] tr input[type = 'checkbox']").not("[id$=chkAll]").change();

                UpdateTotalFooter();
            });

            $("[id$=gvItems] tr input[type = 'checkbox']").not("#chkAll").change(function () {
                UpdateTotalFooter();
            });

            $("[id$=chkAisleStatusAll]").click(function () {
                var Combo = $find("<%= radAisle.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            $("[id$=chkShelfStatusAll]").click(function () {
                var Combo = $find("<%= radShelf.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            $("[id$=chkRackStatusAll]").click(function () {
                var Combo = $find("<%= radRack.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            $("[id$=chkBinStatusAll]").click(function () {
                var Combo = $find("<%= radBin.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            $("[id$=ddlWarehouseLocation]").change(function () {
                if ($("[id$=divAttributes]") != NaN && $(this).val() > 0 && $("[id$=btnSaveInvetory]").val() == "Add") {
                    $("[id$=divGlobalAttributes]").show();
                } else {
                    $("[id$=divGlobalAttributes]").hide();
                }
            });
        }

        var onGridColumnResized = function (e) {
            var columns = $(e.currentTarget).find("th");
            var msg = "";
            columns.each(function () {
                var thId = $(this).attr("id");
                var thWidth = $(this).width();
                msg += thId + ':' + thWidth + ';';
            }
            )

            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/SaveGridColumnWidth",
                data: "{str:'" + msg + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
        };

        function BindJquery() {
            $("[id$=gvSearch] input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    console.log($('#txtGridColumnFilter').val());
                    on_filter();
                }
            });

            $('[id$=gvSearch] select').change(function () {
                on_filter();
            });

            $("[id$=hplClearGridCondition]").click(function () {
                $('[id$=txtGridColumnFilter]').val("");

                $('[id$=divDropDowns]').find('.RadComboBox').each(function () {
                    var Combo = $find($(this).attr('id'));
                    Combo.clearSelection();

                    var items = Combo.get_items();
                    for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                        items._array[x].uncheck();
                    }
                });

                $('[id$=btnGo1]').trigger('click');
            });
        }

        function on_filter() {
            var filter_condition = '';

            var dropDownControls = $('[id$=gvSearch] select');
            dropDownControls.each(function () {
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=gvSearch] input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('[id$=txtGridColumnFilter]').val(filter_condition);
            $('[id$=txtCurrrentPage]').val('1');
            $('[id$=btnGo1]').trigger('click');
        }

        //Other Common functions
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpemEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }
        function GetCheckedRowValues() {
            var RecordIDs = '';
            $("[id$=gvSearch] tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    RecordIDs = RecordIDs + $(this).find("#lbl1").text() + '~' + $(this).find("#lbl2").text() + '~' + $(this).find("#lbl3").text() + ',';
                }
            });
            RecordIDs = RecordIDs.substring(0, RecordIDs.length - 1);
            //            console.log(RecordIDs);
            return RecordIDs;
        }
        function fn_GoToURL(varURL) {
            var url = varURL
            if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
                var LoWindow = window.open(url, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function OpenShareRecord(a, b) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=" + b, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
        }
        function DeleteWarehouse() {
            if (confirm("This will adjust inventory down by the qty of the row, which will impact your asset value. Are you sure?")) {
                return true;
            } else {
                return false;
            }
        }

        function UpdateAdjustmentValue() {
            if (Number($('[id$=txtAdjustedQty]').val()) + Number($('[id$=lblOnHandQty]').text()) < 0) {
                alert('Can not accept value, it creates negative inventory.');
                $('[id$=txtAdjustedQty]').val('');
                $('[id$=lblAdjustValue]').text('');
                return false;
            }

            if (Number($('[id$=txtAdjustedQty]').val()) < 0) {
                if ((Number($('[id$=txtAdjustedQty]').val()) * -1) > Number($('[id$=lblOnHandQty]').text())) {
                    alert('Adjusted Qty can not be lesser than on hand qty');
                    $('[id$=txtAdjustedQty]').val('');
                    $('[id$=lblAdjustValue]').text('');
                    return false;
                }
            }

            var AdjustValue = roundNumber(Number($('[id$=txtAdjustedQty]').val()) * Number($('[id$=txtUnitCost]').val()), 4);
            $('[id$=lblAdjustValue]').text(' Adjustment value: ' + AdjustValue.toString());

        }

        function Save() {
            var datePicker = $telerik.findControl(document, "calAdjustmentDate");
            var date = datePicker.get_selectedDate();

            if (datePicker.isEmpty()) {
                alert('Please enter adjustment date');
                return false;
            }

            return true;
        }

        function roundNumber(num, dec) {
            var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
            return result;
        }

        function UpdateTotalFooter() {
            var sumOfValues1 = 0.0;
            var sumOfValues2 = 0.0;

            $("[id$=gvItems] tr").each(function () {

                if ($(this).find("[id$=chk]").is(':checked')) {
                    sumOfValues1 += Number($(this).find("[id$=lblCurrentInventoryValue]").text());
                    sumOfValues2 += Number($(this).find("[id$=lblNewInventoryValue]").text());
                }

            });

            $("[id$=lblCurrentInventoryValueTotal]").text(parseFloat(sumOfValues1).toFixed(2));
            $("[id$=lblNewInventoryValueTotal]").text(parseFloat(sumOfValues2).toFixed(2));

        }

        function CheckNumberAllowPlus(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!((k > 47 && k < 58) || k == 43 || k == 44 || k == 45 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!((k > 47 && k < 58) || k == 43 || k == 45 || k == 8 || k == 37 || k == 39 || k == 16 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function UpdateAverageCostForLineItem(CurrentRow) {

            var AdjQty = (Number($(CurrentRow).find("[id$=txtCounted]").val()) - Number($(CurrentRow).find("[id$=lblOnHandQty]").text())).toPrecision(8);

            var TotalQty = (Number($(CurrentRow).find("[id$=lblOnHandQty]").text()) + Number(AdjQty)).toPrecision(8);
            if (AdjQty != 0) {
                if (AdjQty > 0) { $(CurrentRow).find("[id$=txtAdjQty]").val("+" + AdjQty.toString()).css("color", "green"); }
                else { $(CurrentRow).find("[id$=txtAdjQty]").val(AdjQty.toString()).css("color", "red"); }
            }
            var NewInventoryValue = (Number($(CurrentRow).find("[id$=txtCounted]").val()) * Number($(CurrentRow).find("[id$=lblAverageCost]").text())).toPrecision(8);
            $(CurrentRow).find("[id$=lblNewInventoryValue]").text(parseFloat(NewInventoryValue).toFixed(2));

        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=126', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function SerialNo(a, b, c) {
            window.open('../Items/frmSerializeitems.aspx?ItemCode=' + a + '&Serialize=' + b + '&numWarehouseItemID=' + c, '', 'toolbar=no,titlebar=no,left=200,top=250,width=900,height=550,scrollbars=yes,resizable=no');
            return false;
        }

        function SaveInventory() {
            if (parseInt($("[id$=hfWareHouseItemID]").val()) > 0) {
                if (confirm("Selected interal location will be updated. Do you want to proceed?")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (confirm("New interal location will be added to item. Do you want to proceed?")) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        function ChangeMessageCSS(isSuccess) {
            if (isSuccess == 1) {
                $("#divMessage").find("div.alert").removeClass("alert-warning").addClass("alert-success");
                $("#divMessage").find("i.icon").removeClass("fa-warning").addClass("fa-check");
            } else {
                $("#divMessage").find("div.alert").removeClass("alert-success").addClass("alert-warning");
                $("#divMessage").find("i.icon").removeClass("fa-check").addClass("fa-warning");
            }
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>External Location:</label>
                        <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control" AutoPostBack="true" Width="150" TabIndex="1">
                        </asp:DropDownList>
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group" id="divSearch" runat="server">
                        <label>Search For</label>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="130"></asp:TextBox>
                        <asp:DropDownList ID="ddlSearch" runat="server" CssClass="form-control" Width="120" ViewStateMode="Enabled">
                            <asp:ListItem Text="-Select One--" Value=""></asp:ListItem>
                            <asp:ListItem Text="Item" Value="vcItemName"></asp:ListItem>
                            <asp:ListItem Text="SKU" Value="vcSKU"></asp:ListItem>
                            <asp:ListItem Text="ModelID" Value="vcModelID"></asp:ListItem>
                            <asp:ListItem Text="Vendor" Value="vcCompanyName"></asp:ListItem>
                            <asp:ListItem Text="Description" Value="txtItemDesc"></asp:ListItem>
                            <asp:ListItem Text="Item ID" Value="numItemCode"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnGo" CssClass="btn btn-sm btn-primary" Text="Go" runat="server" OnClick="btnGo_Click"></asp:Button>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline" id="divAdjust" runat="server">
                    <div class="form-group">
                        <label><span style="color: red">*</span> Adjustment Date:</label>
                        <telerik:RadDatePicker runat="server" ID="calAdjustmentDate" Width="100" TabIndex="102" />
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Project<asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Selecting project will be tacked in report of P&L by Project" />:</label>
                        <asp:DropDownList runat="server" ID="ddlProject" CssClass="form-control" Width="150" TabIndex="103">
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnSave" Text="Adjust Inventory" runat="server" CssClass="btn btn-primary" OnClientClick="return Save();" TabIndex="104" Style="padding-left: 5px;" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bars"></i>

                    <h3 class="box-title">Inventory Details 
                    </h3>

                    <div class="box-tools biz-tool pull-right">
                        <asp:Button ID="btnSaveInvetory" runat="server" CssClass="btn btn-md btn-primary" Text="Add" TabIndex="100" OnClientClick="return SaveInventory()"></asp:Button>
                        &nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-md btn-primary" Text="Cancel" Visible="false" TabIndex="101"></asp:Button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Internal Location&nbsp;&nbsp;<asp:Label ID="lblToolTipInternalLocation" runat="server" CssClass="tip" ToolTip="You can select an internal location when adding a new warehouse to an item, but afterwards you’ll need to use “Transfer Inventory” to change or add a new internal location">[?]</asp:Label></label>
                                        <asp:HiddenField ID="hfWareHouseItemID" runat="server" />
                                        <asp:DropDownList ID="ddlWarehouseLocation" runat="server" CssClass="form-control" TabIndex="2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group" id="divListPrice" runat="server" visible="false">
                                        <label>List Price (I)</label>
                                        <asp:TextBox ID="txtListPrice" CssClass="form-control" runat="server" TabIndex="3">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trInventory" runat="server">
                                <div class="col-xs-12 col-sm-6" id="divOnHand" runat="server">
                                    <div class="form-group">
                                        <label>
                                            <asp:Label Text="OnHand" ID="lblOnHandTitle" runat="server" />
                                        </label>
                                        <div>
                                            <asp:TextBox ID="txtOnHand" CssClass="form-control" runat="server" TabIndex="4">
                                            </asp:TextBox>
                                            <asp:Label Text="" ID="lblOnHand" runat="server" ForeColor="Blue" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>ReOrder</label>
                                        <asp:TextBox ID="txtReOrder" CssClass="form-control" runat="server" TabIndex="5">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="trMatrixItem" runat="server" visible="false">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>SKU (M)</label>
                                        <asp:TextBox ID="txtSKU" CssClass="form-control" runat="server" onkeypress="javascript:ChangedUPCSKU(this,event)" TabIndex="6">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>UPC (M)</label>
                                        <asp:TextBox ID="txtUPC" CssClass="form-control" runat="server" onkeypress="javascript:ChangedUPCSKU(this,event)" TabIndex="7">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:PlaceHolder ID="plhWareHouseAttributes" runat="server"></asp:PlaceHolder>
                            <div class="row" id="divGlobalAttributes" runat="server" style="display: none">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Create Global Location</label>
                                        <div>
                                            <asp:CheckBox ID="chkGlobalLocation" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row" id="tblSerialLot" runat="server" visible="false">
                                <div class="col-xs-12">
                                    <div class="row" id="trLot" runat="server">
                                        <div class="col-xs-12">
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label>
                                                        <asp:Label ID="lblToolTip" runat="server" Text="[?]" CssClass="tip" ToolTip="Entering inventory for Lot #s is easy. To enter a new Lot#, enter the Lot # in the field, select quantity and select an expiration date.<br/><br/>To edit or delete a Lot # just click the manage serial/lot icon in “Inventory Management” grid."></asp:Label>
                                                        <span style="color: red">* </span>Lot #</label>
                                                    <asp:TextBox ID="txtLotNumber" runat="server" TabIndex="50" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="form-group">
                                                    <label><span style="color: red">* </span>Qty</label>
                                                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" Width="50" TabIndex="51"></asp:TextBox>
                                                </div>
                                                <div class="form-group">
                                                    <label style="white-space: nowrap"><span style="color: red">* </span>Expiration Date</label>
                                                    <telerik:RadDatePicker ID="rdpExpirationDate" runat="server" Width="108" TabIndex="52"></telerik:RadDatePicker>
                                                </div>
                                                <asp:ImageButton ID="ibtnAddLot" runat="server" ImageUrl="~/images/AddRecord.png" TabIndex="53" ToolTip="Add" Height="16" />
                                                <asp:ImageButton ID="ibtnClear" runat="server" ImageUrl="~/images/clear.png" TabIndex="54" ToolTip="Clear" Height="16" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="trSerial" runat="server">
                                        <div class="col-xs-12">
                                            <asp:Label ID="lblTooltipSerial" runat="server" Text="[?]" CssClass="tip" ToolTip="Entering inventory for Serial #s is easy. Just enter a the Serial # or for multiple units enter many Serial #s separated by comma (e.g. 1234,1234,44444 would add 3 Serial #s which adds 3 units to inventory). A maximum qty of 100 total can be added at any one time.<br /><br />To delete a serial # just click the manage serial/lot icon in “Inventory Management” grid and deleting a number will adjust inventory and any related short term asset value downward."></asp:Label>
                                            <span style="color: red">* </span>Serial #
                                        <asp:Label ID="lblSerialLotExample" runat="server" Text="Example: A123,B123 adds 2 serial #s increasing stock by 2" Font-Italic="true" Style="float: right"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label></label>
                                                <asp:TextBox ID="txtSerialLot" runat="server" CssClass="form-control" TextMode="MultiLine" TabIndex="55"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bars"></i>

                    <h3 class="box-title">Inventory Management 
                    </h3>

                    <div class="box-tools biz-tool pull-right">
                        <webdiyer:AspNetPager ID="bizPager" runat="server"
                            PagingButtonSpacing="0"
                            CssClass="bizgridpager"
                            AlwaysShow="true"
                            CurrentPageButtonClass="active"
                            PagingButtonUlLayoutClass="pagination"
                            PagingButtonLayoutType="UnorderedList"
                            FirstPageText="<<"
                            LastPageText=">>"
                            NextPageText=">"
                            PrevPageText="<"
                            Width="100%"
                            UrlPaging="false"
                            NumericButtonCount="8"
                            ShowCustomInfoSection="Left"
                            ShowPageIndexBox="Never"
                            OnPageChanged="bizPager_PageChanged"
                            CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                            CustomInfoClass="bizpagercustominfo">
                        </webdiyer:AspNetPager>
                        &nbsp;&nbsp;
                        <a href="#" onclick="return OpenSetting()" title="Show Grid Settings."><i class="fa fa-lg fa-gear"></i></a>
                        &nbsp;&nbsp;
                        <a href="#" id="hplClearGridCondition" class="hyperlink" title="Clear All Filters"><i class="fa fa-lg fa-filter"></i></a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline" id="divDropDowns">
                                <div class="form-group">
                                    <label>Item Group:</label>
                                    <telerik:RadComboBox ID="radItemGroup" Skin="Default" runat="server" Width="150px" DropDownWidth="250px" AutoPostBack="true">
                                    </telerik:RadComboBox>
                                </div>
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label>Aisle:</label>
                                    <telerik:RadComboBox ID="radAisle" Skin="Default" runat="server" CheckBoxes="true" Width="150px" DropDownWidth="250px">
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkAisleStatusAll" runat="server" Text="Select All" />&nbsp;
                                        <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" OnClick="btnGo_Click"></asp:Button>
                                        </FooterTemplate>
                                    </telerik:RadComboBox>
                                </div>
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label>Rack:</label>
                                    <telerik:RadComboBox ID="radRack" Skin="Default" runat="server" CheckBoxes="true" Width="150px" DropDownWidth="250px">
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkRackStatusAll" runat="server" Text="Select All" />&nbsp;
                                        <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" OnClick="btnGo_Click"></asp:Button>
                                        </FooterTemplate>
                                    </telerik:RadComboBox>
                                </div>
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label>Shelf:</label>
                                    <telerik:RadComboBox ID="radShelf" Skin="Default" runat="server" CheckBoxes="true" Width="150px" DropDownWidth="250px">
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkShelfStatusAll" runat="server" Text="Select All" />&nbsp;
                                        <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" OnClick="btnGo_Click"></asp:Button>
                                        </FooterTemplate>
                                    </telerik:RadComboBox>
                                </div>
                                &nbsp;&nbsp;
                                <div class="form-group">
                                    <label>Bin:</label>
                                    <telerik:RadComboBox ID="radBin"
                                        Skin="Default" runat="server" CheckBoxes="true" Width="150px" DropDownWidth="250px">
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkBinStatusAll" runat="server" Text="Select All" />&nbsp;
                                        <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" OnClick="btnGo_Click"></asp:Button>
                                        </FooterTemplate>
                                    </telerik:RadComboBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvItems" runat="server" ShowFooter="true" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped" Width="100%" EnableViewState="false" DataKeyNames="numItemcode,numWareHouseItemID,numOnHand,TotalOnHand,ItemGroup,bitKitParent,bitSerialized,bitLotNo,bitAssembly,monAverageCost">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" />
                                                <asp:HiddenField ID="hdnDeleteKitWarehouse" runat="server" Value='<%#Eval("IsDeleteKitWarehouse") %>' />
                                                <asp:HiddenField ID="hdnChildItemWarehouse" runat="server" Value='<%#Eval("bitChildItemWarehouse") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="80">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit_WareHouse" ToolTip="Edit" CssClass="btn btn-xs btn-info"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete_WareHouse" CssClass="btn btn-xs btn-danger" OnClientClick="return DeleteWarehouse();" ToolTip="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                                <asp:LinkButton ID="ibtnManageSerial" runat="server" OnClientClick="return " CssClass="btn btn-xs btn-default" ToolTip="Serial/Lot"><i class="fa fa-barcode"></i></asp:LinkButton>
                                                <asp:ImageButton ID="ibtnTransferStok" Style="vertical-align: middle" runat="server" ToolTip="Inventory Transfer" Visible="false" CommandName="TransferStock" ImageUrl="~/images/StockTransfer.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:Label Text='<%#Eval("numItemCode") %>' runat="server" ID="lblItemID" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="250px">
                                            <ItemTemplate>
                                                <a href="frmKitDetails.aspx?ItemCode=<%# Eval("numItemCode") %>&frm=InvAdjust">
                                                    <asp:Label Text='<%# CCommon.Truncate(Eval("vcItemName").ToString(),45) %>' runat="server" ID="lblItemName" />
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="External/Internal Location" DataField="vcWarehouse" HeaderStyle-Wrap="false" />
                                        <asp:TemplateField HeaderText="Attributes" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="200px" Visible="false">
                                            <ItemTemplate>
                                                <%# Eval("vcAttribute").ToString().Trim(",") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="On Hand" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <%# Eval("numOnHand").ToString().Trim(",") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Inventory" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="50px" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Eval("TotalOnHand")%>' runat="server" ID="lblOnHandQty" />
                                                <asp:Label Text='<%# String.Format("{0:###00.00}",Eval("monAverageCost")) %>' runat="server" ID="lblAverageCost" Style="display: none;" />
                                                <asp:Label Text='<%# Eval("numAssetChartAcntId") %>' runat="server" ID="lblAssetChartAcntID" Style="display: none;" />
                                                <asp:Label Text='<%# Eval("numWarehouseItemID") %>' runat="server" ID="lblWarehouseItemID" Style="display: none;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- IF YOU CHANGE COLUMN NAME THEN CHANGE IN CODE ALSO --%>
                                        <asp:TemplateField HeaderText="Counted<font color=#ff0000>*</font>" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="125px">
                                            <ItemTemplate>
                                                <asp:TextBox Text='<%# Eval("TotalOnHand") %>' ID="txtCounted" CssClass="form-control" runat="server" Width="120px" AutoComplete="OFF" Style="text-align: right" onkeypress="CheckNumber(2,event);"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- IF YOU CHANGE COLUMN NAME THEN CHANGE IN CODE ALSO --%>
                                        <asp:TemplateField HeaderText="Qty to Adjust" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="125px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjQty" CssClass="form-control" runat="server" Width="120px" AutoComplete="OFF" Text="" Style="text-align: right"></asp:TextBox>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <strong>Total:</strong>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current value ($)" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:Label Text='<%# String.Format("{0:###00.00}",Eval("monCurrentValue")) %>' runat="server" ID="lblCurrentInventoryValue" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label Text='' runat="server" ID="lblCurrentInventoryValueTotal" Font-Bold="true" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted value ($)" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" HeaderStyle-Width="170px">
                                            <ItemTemplate>
                                                <asp:Label Text="" runat="server" ID="lblNewInventoryValue" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label Text='' runat="server" ID="lblNewInventoryValueTotal" Font-Bold="true" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Records Found .
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <asp:TextBox ID="txtDelCaseId" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
                    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
                    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
                    <asp:Label ID="lblRecordCountCases" Visible="false" runat="server"></asp:Label>
                </div>
            </div>

        </div>
    </div>

    <div class="modal" id="divTransferStock" runat="server">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Inventory Transfer</h4>
                </div>
                <div class="modal-body">
                    <asp:HiddenField ID="hdnTransferFromWarehouse" runat="server" />
                    <asp:HiddenField ID="hdnIsSerial" runat="server" />
                    <asp:HiddenField ID="hdnIsLot" runat="server" />
                    <asp:HiddenField ID="hdnSelectedSerialLot" runat="server" />
                    <asp:Label ID="lblTransferExc" runat="server" ForeColor="Red"></asp:Label>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <label><span class="label" style="color: red">* </span>Enter the on-hand qty you’d like transferred from this location:</label>
                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Width="80"></asp:TextBox>
                                <asp:HyperLink ID="hplSerialLot" runat="server" Visible="false">Added (0)</asp:HyperLink>
                            </div>
                        </div>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <label><span class="label" style="color: red">* </span>Select where you’d like this qty transferred:</label>
                                <telerik:RadComboBox ID="radTransferToWarehouse" Width="199" runat="server" DropDownWidth="613px" Height="100"
                                    AccessKey="W" ClientIDMode="Static" DropDownCssClass="multipleRowsColumns" EnableScreenBoundaryDetection="true" TabIndex="500">
                                    <HeaderTemplate>
                                        <ul>
                                            <li class="col1">Warehouse</li>
                                            <li class="col2">On Hand</li>
                                            <li class="col3">On Order</li>
                                            <li class="col4">On Allocation</li>
                                            <li class="col5">BackOrder</li>
                                        </ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <ul>
                                            <li style="display: none">
                                                <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                            <li class="col1">
                                                <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                            <li class="col2">
                                                <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                            <li class="col3">
                                                <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                            &nbsp;</li>
                                            <li class="col4">
                                                <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                            <li class="col5">
                                                <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                            <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                Style="display: none"></asp:Label></li>
                                        </ul>
                                    </ItemTemplate>
                                </telerik:RadComboBox>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCloseStockTransfer" CssClass="btn btn-default pull-left" runat="server" Text="Close" />
                    <asp:Button ID="btnSaveStockTransfer" CssClass="btn btn-primary" runat="server" Text="Save & Close" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <asp:HiddenField ID="hdnWarehouseID" runat="server" />
    <asp:HiddenField ID="hdnWarehouseItemID" runat="server" />
    <asp:HiddenField ID="hdnItemCode" runat="server" />
    <asp:HiddenField ID="hdnItemCodeEdit" runat="server" />
    <asp:HiddenField ID="hdnItemName" runat="server" />
    <asp:HiddenField ID="hdnIsSerialize" runat="server" />
    <asp:HiddenField ID="hdnIsLotNo" runat="server" />
    <asp:HiddenField ID="hdnItemGroup" runat="server" />
    <asp:HiddenField ID="hdnIsKit" runat="server" />
    <asp:HiddenField ID="hdnIsMatrix" runat="server" />
    <asp:HiddenField ID="hdnIsAssembly" runat="server" />
    <asp:HiddenField ID="hdnAssetChartAcntID" runat="server" />
    <asp:HiddenField ID="hdnAverageCost" runat="server" />
    <asp:HiddenField ID="hdnAttributes" runat="server" />
    <asp:HiddenField ID="hdnIsNewAddedWarehouseEdited" runat="server" />

</asp:Content>
