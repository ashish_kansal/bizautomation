﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmVendorWareHouse.aspx.vb"
    Inherits=".frmVendorWareHouse" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Vendor WareHouse</title>
    <script language="javascript" type="text/javascript">
        function OpenShipment() {
            if (document.getElementById("ddlVendor").value == 0) {
                alert("Select Vendor");
                document.getElementById("ddlVendor").focus();
                return false;
            }
            if (document.getElementById("ddlWarehouse").value == 0) {
                alert("Select Warehouse Address Name");
                document.getElementById("ddlWarehouse").focus();
                return false;
            }

            window.open('../items/frmVendorShipmentMethod.aspx?Vendor=' + document.getElementById("ddlVendor").value + '&VWarehouse=' + document.getElementById("ddlWarehouse").value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=700,height=400,scrollbars=no,resizable=yes')
            return false;
        }

        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?OpID=" + a;
            opener.frames.document.location.href = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                Text="Close" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Vendor WareHouse Detail
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table9" Width="100%" runat="server" Height="350" GridLines="None"
        BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td class="normal1">
                            <span style="float: left"><strong>Vendor :</strong></span>
                            <asp:DropDownList ID="ddlVendor" CssClass="signup" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1">
                            <span style="float: left"><strong>Warehouse Address Name :</strong></span>
                            <asp:DropDownList ID="ddlWarehouse" CssClass="signup" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1">
                            <strong>Warehouse Address :</strong><asp:Label ID="lblWarehouseAddress" runat="server"></asp:Label>
                        </td>
                        <td class="normal1" valign="top">
                            <a href="#" onclick="return OpenShipment()" class="hyperlink">Set method of shipment
                                schedule</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="normal1">
                <strong>Items currently in transit from this warehouse</strong><br />
                <asp:GridView ID="gvOrderTransit" runat="server" AutoGenerateColumns="False" CssClass="tbl"
                    Width="100%">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:BoundField DataField="vcItemName" HeaderText="Item Name"></asp:BoundField>
                        <asp:BoundField DataField="numUnitHour" HeaderText="Qty"></asp:BoundField>
                        <asp:BoundField DataField="vcUOMName" HeaderText="UOM"></asp:BoundField>
                        <%--<asp:BoundField DataField="vcItemDesc" HeaderText="Description"></asp:BoundField>--%>
                        <asp:BoundField DataField="vcModelID" HeaderText="Model #"></asp:BoundField>
                        <asp:BoundField DataField="vcManufacturer" HeaderText="Manufacturer"></asp:BoundField>
                        <asp:BoundField DataField="bintCreatedDate" HeaderText="Ordered On" HtmlEncode="false">
                        </asp:BoundField>
                        <asp:BoundField DataField="vcOrderedBy" HeaderText="Ordered By"></asp:BoundField>
                        <asp:BoundField DataField="vcShipmentMethod" HeaderText="Method of Shipment"></asp:BoundField>
                        <asp:BoundField DataField="bintExpectedDelivery" HeaderText="Expected Delivery" HtmlEncode="false">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Purchase Order">
                            <ItemTemplate>
                                <a href="javascript:void(0);" onclick="OpenOpp('<%# Eval("numOppId") %>');">
                                    <%# Eval("vcPOppName")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
