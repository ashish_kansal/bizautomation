﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmWebAPIItemDetails.aspx.vb"
    Inherits=".frmWebAPIItemDetails" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

    </script>
  
</asp:Content>
<asp:Content ID ="Content2" runat ="server" ContentPlaceHolderID ="FiltersAndViews1">
    <div class="input-part">
        <div class="right-input">
        <table >
           <tr>
              <td>
                  <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"  />        
              </td>
           </tr>
        </table>
      </div>
      </div>
</asp:Content>

<asp:Content ID = "Content3" runat ="server" ContentPlaceHolderID ="PageTitle">
       WebAPI Item Detail
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
   
    <%--<asp:Table ID="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        CssClass="aspTable" Width="300px" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">--%>
             <%--   <asp:TextBox runat="server" CssClass="{required:true, messages:{required:'Max length!'}}"
                    TextMode="SingleLine" ID="txtatr" />--%>
                <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" runat="server" Width="820px"
                    GridLines="none" border="0">
                    <asp:TableRow>
                    <asp:TableCell> 
                    <table border="0" width="100%" id="tblItemDetails" runat="server">
                    </table>
                    </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            <%--</asp:TableCell>
        </asp:TableRow>
    </asp:Table>--%>
</asp:Content>

