Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI
Imports System.Collections.Specialized
Imports System.IO
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.Collections.Generic

Partial Public Class Items_frmKitDetails
    Inherits BACRMPage
    Dim lngItemCode As Long
    Dim lngMetaID As Long
    Dim dtWarehouses As DataTable
    Dim objItems As New CItems
    Dim dtCusTable As New DataTable
    Dim objCommon As CCommon
    Dim dtItemTax As DataTable
    Dim lngAssetCode As Long
    Shared dsEDICF As DataSet

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbOrganization)
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radcmbManufacturer)
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radcmbChildPrimaryVendor)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblAdjustmentSuccess.Text = ""
            lblAdjustmentSuccess.Visible = False

            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
            hdnItemId.Value = lngItemCode
            lngAssetCode = 0

            If GetQueryStringVal("AssetCode") <> "" Then
                lngAssetCode = CCommon.ToLong(GetQueryStringVal("AssetCode"))
                If lngAssetCode > 0 Then lngItemCode = lngAssetCode
                If lngAssetCode = -1 And lngItemCode = 0 Then lngItemCode = 0
                radItemTab.FindTabByValue("AssetDetails").Visible = True
                radItemTab.FindTabByValue("ItemDetails").Visible = False
                radItemTab.FindTabByValue("Inventory").Visible = False
                radItemTab.FindTabByValue("Vendor").Visible = True
            Else
                radItemTab.FindTabByValue("AssetDetails").Visible = False
            End If
            If lngItemCode > 0 Then
                btnAddAssetSerial.Visible = True
                btnCloneItem.Visible = True
            Else
                btnAddAssetSerial.Visible = False
                btnCloneItem.Visible = False
            End If

            'lblItemID.Text = lngItemCode.ToString

            If Not IsPostBack Then
                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    'Business Portal User
                    radItemTab.FindTabByValue("Vendor").Visible = False
                End If

                BindBusinessProcess()
                BindCurrencyPrice()
                lblitemcode.Text = "(ID:" & lngItemCode.ToString() & ")"

                Dim m_aryRightsForSaveInventoryConfiguration() As Integer = GetUserRightsForPage_Other(37, 149)
                If m_aryRightsForSaveInventoryConfiguration(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSaveInventory.Visible = False
                End If

                calAdjustmentDate.SelectedDate = Date.Now

                hlAddNewCategory.NavigateUrl = "frmCategoryAdd.aspx?ItemCode=" & CCommon.ToString(lngItemCode)
                AddToRecentlyViewed(IIf(lngAssetCode = 0, RecetlyViewdRecordType.Item, RecetlyViewdRecordType.AssetItem), IIf(lngAssetCode = 0, lngItemCode, lngAssetCode))
                Dim objUnitSystem As New UnitSystem
                'objUnitSystem.AddUnitSystemItems(ddlUnitofMeasure, Session("UnitSystem"))
                If objCommon Is Nothing Then objCommon = New CCommon
                GetUserRightsForPage(37, 129)
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End If
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnActDelete.Visible = False
                End If
                ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Item"
                Session("VendorId") = Nothing
                Loaddropdowns()
                'BindUOM()
                LoadChartType()

                If GetQueryStringVal("AssetCode") <> "" Then
                    radItemTab.SelectedIndex = 0
                Else
                    radItemTab.SelectedIndex = 1
                End If
                ShowTaxesDefault()
                If lngItemCode <> 0 Then
                    LoadWareHouse()
                    ddlProduct.Enabled = False
                    gvInventory.Visible = True
                    'Istart Pinkal Patel ErrId:1387 Date:28-11-2011
                    'tblSimilarItems.Visible = True
                    radItemTab.FindTabByValue("relatedItems").Visible = True
                    'Iend Pinkal Patel ErrId:1387 Date:28-11-2011
                    BindContainers()
                    LoadDetails()
                    Dim strPage As String
                    strPage = GetQueryStringVal("frm")
                    tdAssembly.Visible = False
                    If strPage = "Kits" Or (chkKit.Checked = True And chkAssembly.Checked = False) Then
                        chkKit.Visible = True
                        lblAverageCost.Visible = False
                        lblAvgCostToolTip.Visible = False
                        txtAverageCost.Visible = False
                        chkVirtual.Visible = False
                        chkVirtual.Checked = False
                        tdAssembly.Visible = True
                        divMaxBuildQty.Visible = False
                    ElseIf strPage = "Assembly" Or (chkKit.Checked = False And chkAssembly.Checked = True) Then
                        chkAssembly.Visible = True
                        lblAverageCost.Visible = False
                        lblAvgCostToolTip.Visible = False
                        chkVirtual.Visible = False
                        chkVirtual.Checked = False
                        divMaxBuildQty.Visible = True
                        tdAssembly.Visible = True
                        divBuildProcess.Visible = True
                    ElseIf strPage = "Assembly" Or strPage = "Services" Then
                        divShipping.Visible = False
                    End If

                    hplPriceLevel.Visible = True
                    hplPriceLevel.Attributes.Add("onclick", "return OpenPriceLevel(" & lngItemCode & ")")
                Else
                    '-------------------------------------------------------------------------------
                    'Set Default Unit for New Item Only
                    If ddlBaseUOM.Items.FindByText("Units") IsNot Nothing Then
                        ddlBaseUOM.Items.FindByText("Units").Selected = True
                    End If
                    If ddPurchaseUOM.Items.FindByText("Units") IsNot Nothing Then
                        ddPurchaseUOM.Items.FindByText("Units").Selected = True
                    End If
                    If ddSalesUOM.Items.FindByText("Units") IsNot Nothing Then
                        ddSalesUOM.Items.FindByText("Units").Selected = True
                    End If

                    'Set Default Accounts as per saved for Domain for New Item Only
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = Session("DomainID")
                    Dim dtTable As DataTable = objUserAccess.GetDomainDetails()

                    If ddlAssetAccount.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numAssetAccID"))) IsNot Nothing Then
                        ddlAssetAccount.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numAssetAccID"))).Selected = True
                    End If
                    If ddlCOGS.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numCOGSAccID"))) IsNot Nothing Then
                        ddlCOGS.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numCOGSAccID"))).Selected = True
                    End If
                    If ddlIncomeAccount.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numIncomeAccID"))) IsNot Nothing Then
                        ddlIncomeAccount.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numIncomeAccID"))).Selected = True
                    End If

                    '-------------------------------------------------------------------------------

                    'UltraWebGrid1.Visible = False
                    divKitAssembltChildItems.Visible = False
                    'Istart Pinkal Patel ErrId:1387 Date:28-11-2011
                    'tblSimilarItems.Visible = False
                    radItemTab.FindTabByValue("relatedItems").Visible = False
                    radItemTab.FindTabByValue("MaintenanceRepair").Visible = False
                    radItemTab.FindTabByValue("ItemDependencies").Visible = False
                    radItemTab.FindTabByValue("ItemEcommerce").Visible = False

                    'Iend Pinkal Patel ErrId:1387 Date:28-11-2011
                    'hplOptionAcc.Visible = False
                    hplImage.Visible = False
                    hplAssetImage.Visible = False
                    hplShoppingCart.Visible = False
                    hplWebAPI.Visible = False
                    btnActDelete.Visible = False
                    'btnTrackAsset.Visible = False
                    btnAdd.Visible = False
                    Dim strPage As String
                    strPage = GetQueryStringVal("frm")
                    strPage = strPage.Replace("%20", " ")
                    If strPage = "Non-Inventory Items" Then
                        ddlProduct.SelectedIndex = 2
                        pnlAssetRental.Visible = False
                    ElseIf strPage = "Inventory Items" Then
                        ddlProduct.SelectedIndex = 1
                        pnlAssetRental.Visible = True
                    ElseIf strPage = "Services" Then
                        ddlProduct.SelectedIndex = 3
                        pnlAssetRental.Visible = False
                        divShipping.Visible = False
                    ElseIf strPage = "Serialized Items" Then
                        ddlProduct.SelectedIndex = 1
                        chkSerializedItem.Checked = True
                        pnlAssetRental.Visible = False
                    ElseIf strPage = "Asset" Then
                        ddlAssetItemType.SelectedIndex = 4
                        pnlAssetRental.Visible = False
                    ElseIf strPage = "Kits" Then
                        ddlProduct.SelectedIndex = 1
                        chkKit.Visible = True
                        chkKit.Checked = True
                        divMaxBuildQty.Visible = False
                        chkSerializedItem.Visible = False
                        chkLotNo.Visible = False
                        pnlAssetRental.Visible = False
                        tdAssembly.Visible = True
                        tdAsset1.Style.Add("display", "none")
                        tdddlAsset1.Style.Add("display", "none")
                        ddlProduct.SelectedValue = "P"
                        ddlProduct.Enabled = False
                    ElseIf strPage = "Assembly" Then
                        ddlProduct.SelectedIndex = 1
                        chkKit.Checked = False
                        chkAssembly.Checked = True
                        chkAssembly.Visible = True
                        divMaxBuildQty.Visible = True
                        pnlAssetRental.Visible = False
                        tdAssembly.Visible = True
                        divShipping.Visible = False
                    ElseIf strPage = "Lot No" Then
                        ddlProduct.SelectedIndex = 1
                        chkLotNo.Checked = True
                        pnlAssetRental.Visible = False
                    End If
                    ProductTypeChange()

                    If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                        Dim objOpportunity As New OppotunitiesIP
                        objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                        objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpportunity.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        objOpportunity.Mode = 2
                        Dim dsTemp As DataSet = objOpportunity.GetOrderItems()

                        If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                            txtItem.Text = CCommon.ToString(dsTemp.Tables(0).Rows(0)("vcItemName"))
                            txtASIN.Text = CCommon.ToString(dsTemp.Tables(0).Rows(0)("vcASIN"))
                            txtSKU.Text = CCommon.ToString(dsTemp.Tables(0).Rows(0)("vcSKUOpp"))
                            txtDescription.Text = CCommon.ToString(dsTemp.Tables(0).Rows(0)("vcItemDesc"))
                        End If
                    End If
                End If

                PersistTable.Load()

                If PersistTable.Count > 0 Then
                    radItemTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))
                End If

                PersistTable.Load(strParam:="KitAsseblyChildItemSearchType", isMasterPage:=True)
                If PersistTable.Count > 0 Then
                    hdnKitAsseblyChildItemSearchType.Value = CCommon.ToString(PersistTable("KitAsseblyChildItemSearchType"))
                End If

                IframeRelatedItems.Attributes("src") = Page.ResolveClientUrl("~/Items/frmRelatedItems.aspx") & "?ParentItemId=" & hdnItemId.Value & "&Mode=0"
            Else
                If chkMatrix.Checked AndAlso CCommon.ToLong(ddlItemGroup.SelectedValue) > 0 Then
                    objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItems.ItemCode = lngItemCode
                    objItems.ItemGroupID = CCommon.ToLong(ddlItemGroup.SelectedValue)
                    Dim dtAttributes As DataTable = objItems.GetItemAttributes()

                    If dtAttributes Is Nothing Or dtAttributes.Rows.Count = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
                    Else
                        AttributesControl(dtAttributes)

                        If dtAttributes.Select("numVariations=0").Length > 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
                        End If
                    End If
                Else
                    plhCustomControls.Controls.Clear()
                End If
            End If

            DisplayDynamicFlds()

            If Not IsPostBack Then
                If lngAssetCode <> 0 Then
                    btnSave.Attributes.Add("onclick", "return Save(" & Session("DomainID") & ",1)")
                    btnSaveClose.Attributes.Add("onclick", "return Save(" & Session("DomainID") & ",1)")
                Else
                    btnSave.Attributes.Add("onclick", "return Save(" & Session("DomainID") & ",0)")
                    btnSaveClose.Attributes.Add("onclick", "return Save(" & Session("DomainID") & ",0)")
                End If

                gvInventory.Rebind()

                'hplOptionAcc.Attributes.Add("onclick", "return OpenOptAnAcc('" & lngItemCode & "')")
                txtLstPrice.Attributes.Add("onkeypress", "return CheckNumber(1)")
                btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                'hplAdjustInv.Attributes.Add("onclick", "return openAdjust(" & lngItemCode & ")")
                hplShoppingCart.Attributes.Add("onclick", "return OpenShopCartDTL(" & lngItemCode & ")")
                hplWebAPI.Attributes.Add("onclick", "return OpenWebAPIItem(" & lngItemCode & ")")
                hplImage.Attributes.Add("onclick", "return OpenImage(" & lngItemCode & ")")
                hplAssetImage.Attributes.Add("onclick", "return OpenImage(" & lngItemCode & ")")
                btnAdd.Attributes.Add("onclick", "return Add()")
                btnAddChilItem.Attributes.Add("onclick", "return AddChildItem()")
                chkAssembly.Attributes.Add("onclick", "return KitAssembly()")

                hplItemDependencies.Attributes.Add("onclick", "return OpenItemDependenciesParentItems(" & lngItemCode & ")")
                If Not Session("OwnerId") Is Nothing Then
                    radCmbOrganization.SelectedValue = Session("OwnerId")
                    radCmbOrganization.Text = Session("OwnerName")
                End If


                If lngItemCode > 0 Then
                    BindGrid()
                End If

                If GetQueryStringVal("DivisionID") <> "" Then
                    If lngAssetCode <> 0 Then txtAssetName.Focus()
                Else
                    If lngAssetCode <> 0 Then radCmbOrganization.Focus()
                End If

                radItemTab.MultiPage.FindPageViewByID(radItemTab.SelectedTab.PageViewID).Selected = True
                If radItemTab.SelectedTab.PageViewID = "radPageView_ItemDependencies" Then
                    LoadLocationsAndInventory()
                    bindItemDependencies()
                End If
                'If radItemTab.SelectedIndex = 2 Then
                '    divLocation.Visible = True
                'Else
                '    divLocation.Visible = False
                'End If


                If chkKit.Checked = True Or chkAssembly.Checked = True Then
                    radItemTab.FindTabByValue("Vendor").Visible = False
                    radItemTab.FindTabByValue("VendorWarehouses").Visible = False
                End If
                hrfGL.Attributes.Add("onclick", "return OpenGL('" & lngItemCode & "')")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub BindCurrencyPrice()
        Try
            Dim objItemCurrencyPrice As New ItemCurrencyPrice
            objItemCurrencyPrice.DomainID = CCommon.ToLong(Session("DomainID"))
            objItemCurrencyPrice.ItemCode = lngItemCode
            gvCurrencies.DataSource = objItemCurrencyPrice.GetByItem()
            gvCurrencies.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindBusinessProcess()
        Try
            Dim objAdmin = New CAdmin()
            objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
            objAdmin.SalesProsID = 0
            objAdmin.Mode = 3
            ddlBusinessProcess.Items.Clear()
            Dim dtConfiguration = objAdmin.LoadProcessList()
            If Not dtConfiguration Is Nothing AndAlso dtConfiguration.Rows.Count > 0 Then
                ddlBusinessProcess.DataSource = dtConfiguration
                ddlBusinessProcess.DataTextField = "Slp_Name"
                ddlBusinessProcess.DataValueField = "Slp_Id"
                ddlBusinessProcess.DataBind()
            End If
            ddlBusinessProcess.Items.Insert(0, New ListItem("-Select-", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindCategoryProfile()
        Try
            Dim objCategoryProfile As New CategoryProfile
            objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dt As DataTable = objCategoryProfile.GetAll()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                ddlCategoryProfile.DataSource = dt
                ddlCategoryProfile.DataTextField = "vcName"
                ddlCategoryProfile.DataValueField = "ID"
                ddlCategoryProfile.DataBind()

                If Not PersistTable(ddlCategoryProfile.ClientID) Is Nothing _
                    AndAlso Not ddlCategoryProfile.Items.FindByValue(PersistTable(ddlCategoryProfile.ClientID)) Is Nothing Then
                    ddlCategoryProfile.Items.FindByValue(PersistTable(ddlCategoryProfile.ClientID)).Selected = True
                End If

                GetCategoryProfileDetail()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GetCategoryProfileDetail()
        Try
            If Not ddlCategoryProfile.SelectedItem Is Nothing Then
                Dim objCategoryProfile As New CategoryProfile
                objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                objCategoryProfile.ID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)
                objCategoryProfile.GetByIDWithSites()

                If Not objCategoryProfile.ListCategoryProfileSites Is Nothing AndAlso objCategoryProfile.ListCategoryProfileSites.Count > 0 Then
                    lblProfileSites.Text = "<b>Sites using this profile:</b> " & String.Join(", ", objCategoryProfile.ListCategoryProfileSites.Select(Function(x) x.SiteName))
                Else
                    lblProfileSites.Text = "<b>Sites using this profile:</b> "
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub gvInventory_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInventory.ItemDataBound
        Try
            If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
                Dim item As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                Dim header As GridHeaderItem = TryCast(gvInventory.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
                If CCommon.ToBool(Session("EnableItemLevelUOM")) Then
                    If CCommon.ToBool(Session("EnableItemLevelUOM")) AndAlso e.Item.DataSetIndex = 0 Then
                        Dim lblOnHandHeader As Label = DirectCast(header.FindControl("lblOnHandHeader"), Label)
                        Dim lblOnOrderHeader As Label = DirectCast(header.FindControl("lblOnOrderHeader"), Label)
                        Dim lblRequisitionsHeader As Label = DirectCast(header.FindControl("lblRequisitionsHeader"), Label)
                        Dim lblAllocationHeader As Label = DirectCast(header.FindControl("lblAllocationHeader"), Label)
                        Dim lblBackOrderHeader As Label = DirectCast(header.FindControl("lblBackOrderHeader"), Label)

                        lblOnHandHeader.Text = "On-Hand <br/>(" & item("vcBaseUnit") & " / " & item("vcSaleUnit") & ")"
                        lblOnOrderHeader.Text = "On-Order <br/>(" & item("vcBaseUnit") & " / " & item("vcPurchaseUnit") & ")"
                        lblRequisitionsHeader.Text = "Requisitions <br/>(" & item("vcBaseUnit") & " / " & item("vcPurchaseUnit") & ")"
                        lblAllocationHeader.Text = "Allocation <br/>(" & item("vcBaseUnit") & " / " & item("vcSaleUnit") & ")"
                        lblBackOrderHeader.Text = "Back-Order <br/>(" & item("vcBaseUnit") & " / " & item("vcSaleUnit") & ")"
                    End If

                    Dim lblOnHand As Label = DirectCast(e.Item.FindControl("lblOnHand"), Label)
                    Dim lblOnOrder As Label = DirectCast(e.Item.FindControl("lblOnOrder"), Label)
                    Dim lblRequisitions As Label = DirectCast(e.Item.FindControl("lblRequisitions"), Label)
                    Dim lblAllocation As Label = DirectCast(e.Item.FindControl("lblAllocation"), Label)
                    Dim lblBackOrder As Label = DirectCast(e.Item.FindControl("lblBackOrder"), Label)

                    lblOnHand.Text = item("OnHand") & " / " & item("OnHandUOM")
                    lblOnOrder.Text = item("OnOrder") & " / " & item("OnOrderUOM")
                    lblRequisitions.Text = item("Requisitions") & " / " & item("OnOrderUOM")
                    lblAllocation.Text = item("Allocation") & " / " & item("AllocationUOM")
                    lblBackOrder.Text = item("BackOrder") & " / " & item("BackOrderUOM")
                End If

                If e.Item.OwnerTableView.DataMember = "WareHouse" Then
                    'Added by Richa Start
                    Dim txtCounted As TextBox = DirectCast(e.Item.FindControl("txtCounted"), TextBox)
                    Dim txtAdjQty As TextBox = DirectCast(e.Item.FindControl("txtAdjQty"), TextBox)
                    txtCounted.Visible = Not (chkSerializedItem.Checked Or chkLotNo.Checked Or chkKit.Checked)
                    txtAdjQty.Visible = Not (chkSerializedItem.Checked Or chkLotNo.Checked Or chkKit.Checked)
                    'End

                    Dim ibtnAudit As ImageButton = DirectCast(e.Item.FindControl("ibtnAudit"), ImageButton)
                    Dim ibtnManageSerial As ImageButton = DirectCast(e.Item.FindControl("ibtnManageSerial"), ImageButton)
                    Dim ibtnManageMatrix As ImageButton = DirectCast(e.Item.FindControl("ibtnManageMatrix"), ImageButton)
                    Dim ibtnTransferStok As LinkButton = DirectCast(e.Item.FindControl("ibtnTransferStok"), LinkButton)
                    Dim vcExternalLocation = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numWarehouseID"))
                    Dim vcInternalLocationID = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numWLocationID"))
                    Dim numWarehouseItemID As Int32 = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numWareHouseItemID"))

                    If lngItemCode > 0 AndAlso Not chkKit.Checked Then
                        ibtnTransferStok.Visible = True
                    End If

                    If chkSerializedItem.Checked AndAlso CCommon.ToBool(Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "bitSerialLotExists"))) Then
                        ibtnManageSerial.OnClientClick = "return SerialNo(" & lngItemCode & ",1," & numWarehouseItemID & ",'" & vcExternalLocation & "'," & vcInternalLocationID & ",'')"
                    ElseIf chkLotNo.Checked AndAlso CCommon.ToBool(Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "bitSerialLotExists"))) Then
                        ibtnManageSerial.OnClientClick = "return SerialNo(" & lngItemCode & ",0," & numWarehouseItemID & ",'" & vcExternalLocation & "'," & vcInternalLocationID & ",'')"
                    Else
                        ibtnManageSerial.Visible = False
                    End If

                    'Matrix
                    If CCommon.ToLong(ddlItemGroup.SelectedValue) > 0 Then
                        ibtnManageMatrix.Visible = True
                        Dim Price As Double = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "Price"))
                        Dim sku As String = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "SKU"))
                        Dim barcode As String = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "Barcode"))
                        ibtnManageMatrix.OnClientClick = "return MatrixItem(" & CCommon.ToLong(ddlItemGroup.SelectedValue) & "," & lngItemCode & "," & Price & "," & sku & "," & barcode & "," & numWarehouseItemID & ",'" & chkMatrix.Checked & "'" & ")"
                    Else
                        ibtnManageMatrix.Visible = False
                    End If

                    ibtnAudit.OnClientClick = "return OpenInventoryReport('" + CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numWareHouseItemID")) + "')"

                    If Not e.Item.FindControl("dlInternalLocation") Is Nothing Then
                        Dim dlInternalLocation As DataList = DirectCast(e.Item.FindControl("dlInternalLocation"), DataList)
                        Dim strLocations As String = CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcInternalLocations"))

                        dlInternalLocation.DataSource = Newtonsoft.Json.JsonConvert.DeserializeObject(Of DataTable)(strLocations)
                        dlInternalLocation.DataBind()

                        For Each dlItem As DataListItem In dlInternalLocation.Items
                            If chkSerializedItem.Checked Then
                                DirectCast(dlItem.FindControl("txtInternalLocationQty"), TextBox).Enabled = False
                                DirectCast(dlItem.FindControl("ibtnManageSerial"), ImageButton).OnClientClick = "return SerialNo(" & lngItemCode & ",1," & DirectCast(dlItem.FindControl("hdnWarehouseItemID"), HiddenField).Value & ",'" & DirectCast(dlItem.FindControl("hdnWarehouse"), HiddenField).Value & "'," & DirectCast(dlItem.FindControl("hdnLocationID"), HiddenField).Value & ",'" & DirectCast(dlItem.FindControl("hdnLocation"), HiddenField).Value & "')"
                            ElseIf chkLotNo.Checked Then
                                DirectCast(dlItem.FindControl("txtInternalLocationQty"), TextBox).Enabled = False
                                DirectCast(dlItem.FindControl("ibtnManageSerial"), ImageButton).OnClientClick = "return SerialNo(" & lngItemCode & ",0," & DirectCast(dlItem.FindControl("hdnWarehouseItemID"), HiddenField).Value & ",'" & DirectCast(dlItem.FindControl("hdnWarehouse"), HiddenField).Value & "'," & DirectCast(dlItem.FindControl("hdnLocationID"), HiddenField).Value & ",'" & DirectCast(dlItem.FindControl("hdnLocation"), HiddenField).Value & "')"
                            Else
                                DirectCast(dlItem.FindControl("ibtnManageSerial"), ImageButton).Visible = False
                            End If
                        Next

                        If dlInternalLocation.Items.Count > 0 Then
                            txtCounted.Visible = False
                            txtAdjQty.Visible = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub



    Private Sub gvInventory_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvInventory.ItemCommand
        Try
            If e.CommandName = "Delete_WareHouse" Then
                Dim numItemCode As Int32 = lngItemCode

                Dim numWarehouseItemID As Int32 = CCommon.ToLong(CType(e.Item.FindControl("lblWarehouseItemID"), Label).Text)
                Dim numOnHand As Double = CCommon.ToDouble(CType(e.Item.FindControl("lblOnHand"), Label).Text)
                Dim bitKitParent As Boolean = chkKit.Checked
                Dim itemName As String = CCommon.ToString(hdnItemName.Value)
                Dim numAverageCost As Long = CCommon.ToLong(lblAverageCost.Text)
                Dim numAssetChartAcntID As Long = CCommon.ToLong(DirectCast(e.Item.FindControl("lblAssetChartAcntID"), Label).Text)

                If numItemCode > 0 AndAlso numWarehouseItemID > 0 Then
                    Dim objItems = New CItems
                    objItems.ItemCode = numItemCode
                    objItems.WareHouseItemID = numWarehouseItemID

                    If numOnHand > 0 Then
                        ShowMessage("You are not allowed to delete warehouse item, Your option is to do adjustment of qty such that OnHand quantity goes to 0.")
                    Else
                        objItems.DomainID = Session("DomainID")
                        objItems.UserCntID = Session("UserContactID")
                        objItems.byteMode = 3

                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            objItems.AddUpdateWareHouseForItems()
                            objItems.MakeItemQtyAdjustmentJournal(numItemCode, itemName, -1 * numOnHand, numAverageCost, numAssetChartAcntID, Session("UserContactID"), Session("DomainID"))

                            objTransactionScope.Complete()
                        End Using
                    End If
                End If

                gvInventory.Rebind()
            ElseIf e.CommandName = "TransferStock" Then
                Dim numItemCode As Int32 = lngItemCode
                Dim numWarehouseItemID As Int32 = CCommon.ToLong(CType(e.Item.FindControl("lblWarehouseItemID"), Label).Text)
                Dim bitSerialized As Boolean = chkSerializedItem.Checked
                Dim bitLotNo As Boolean = chkLotNo.Checked

                hdnTransferFromWarehouse.Value = numWarehouseItemID
                Dim objCommon As New CCommon
                Dim dt As DataTable = objCommon.GetWarehouseAttrBasedItem(lngItemCode)

                Dim arrDataRow As DataRow() = dt.Select("numWareHouseItemId=" & hdnTransferFromWarehouse.Value)

                If Not arrDataRow Is Nothing AndAlso arrDataRow.Count > 0 Then
                    dt.Rows.Remove(arrDataRow(0))
                End If

                radTransferToWarehouse.DataSource = dt
                radTransferToWarehouse.DataTextField = "vcWareHouse"
                radTransferToWarehouse.DataValueField = "numWareHouseItemId"
                radTransferToWarehouse.DataBind()

                If bitSerialized Or bitLotNo Then
                    If bitSerialized Then
                        hplSerialLot.Attributes.Add("onclick", "return OpenSertailLot(0,0," & numItemCode & "," & numWarehouseItemID & ",'Serial'" & ");")
                    Else
                        hplSerialLot.Attributes.Add("onclick", "return OpenSertailLot(0,0," & numItemCode & "," & numWarehouseItemID & ",'Lot'" & ");")
                    End If

                    hplSerialLot.Visible = True
                    txtQty.Visible = False
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');", True)


            End If
        Catch ex As Exception
            If ex.Message.Contains("OpportunityItems_Depend") Or ex.Message.Contains("OpportunityKitItems_Depend") Then
                ShowMessage("You are Not allowed To delete warehouse item because order Is placed For item With this warehouse.")
            ElseIf ex.Message.Contains("KitItems_Depend") Then
                ShowMessage("Item With selected warehouse location Is used As child Of Assembly/Kit item.")
            ElseIf ex.Message.Contains("WorkOrder_Depend") Then
                ShowMessage("You are Not allowed To delete warehouse item because workorder(s) are with selected warehouse.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub
    Private Sub ddlGridWareHouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGridWareHouse.SelectedIndexChanged
        bindItemsGrid()
    End Sub
    Sub bindItemsGrid()
        Try
            objItems.ItemCode = lngItemCode
            objItems.WareHouseItemID = ddlGridWareHouse.SelectedValue
            objItems.bitApplyFilter = True
            Dim dtChildItems As DataTable = objItems.GetChildItemsForKitsAss

            Dim dv As DataView = dtChildItems.DefaultView
            dv.Sort = "StageLevel ASC, sintOrder ASC"
            rtlChildItems.DataSource = dv


            rtlChildItems.DataBind()
            rtlChildItems.ExpandAllItems()

            If Not dtChildItems Is Nothing AndAlso dtChildItems.Rows.Count > 0 Then
                If dtChildItems.Select("bitKitParent=1").Length > 0 Then
                    ddlKitSelectionType.Visible = True
                Else
                    ddlKitSelectionType.Visible = False
                    btnNewKitItem.Visible = False
                End If
            End If

            If CCommon.ToLong(ddlGridWareHouse.SelectedValue) > 0 Or chkKit.Checked Then
                dtChildItems.Columns.Add("MaximumPossibleUnits", Type.GetType("System.Decimal"))

                Dim drArray As DataRow() = dtChildItems.Select("numItemKitID=0 AND numWarehouseItmsID > 0")

                If drArray.Length > 0 Then
                    For Each row As DataRow In drArray
                        row.Item("MaximumPossibleUnits") = Math.Floor(row.Item("numOnHand") / IIf(row.Item("numUOMQuantity") = 0, 1, row.Item("numUOMQuantity")))
                    Next

                    Dim dt = drArray.CopyToDataTable()

                    lblMaxBuildQty.Text = CCommon.ToString(dt.Compute("min(MaximumPossibleUnits)", ""))
                Else
                    lblMaxBuildQty.Text = 0
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub FindNode(treeNode As RadTreeNode, NodeValue As String)
        Try
            For Each tn As RadTreeNode In treeNode.Nodes
                If tn.Value = NodeValue Then
                    tn.Checked = True
                End If
                FindNode(tn, NodeValue)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindContainers()
        objItems.DomainID = Session("DomainID")
        Dim dtContainer As New DataTable
        dtContainer = objItems.ViewItemListForContainer().Tables(0)
        ddlContainer.DataSource = dtContainer
        ddlContainer.DataValueField = "numItemCode"
        ddlContainer.DataTextField = "vcItemName"
        ddlContainer.DataBind()
        ddlContainer.Items.Insert(0, New ListItem("--Select One--", "0"))
    End Sub

    Sub LoadDetails()
        Try

            Dim dtItemDetails As DataTable
            Dim dtAssetItem As New DataTable

            objItems.DomainID = CCommon.ToLong(Session("DomainID"))
            objItems.ItemCode = lngItemCode
            objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
            dtItemDetails = objItems.ItemDetails()

            If lngAssetCode > 0 Then dtAssetItem = objItems.GetAssetInvetory
            chkMatrix.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitMatrix"))

            If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitOppOrderExists")) Then
                If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitItemGroupHasAttributes")) Then
                    ddlItemGroup.Enabled = False
                End If
                chkMatrix.Enabled = False
            Else
                ddlItemGroup.Enabled = True
                chkMatrix.Enabled = True
            End If

            If CCommon.ToLong(CCommon.ToLong(dtItemDetails.Rows(0).Item("numItemGroup"))) > 0 Then
                If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitMatrix")) Then
                    objItems.ItemGroupID = CCommon.ToLong(dtItemDetails.Rows(0).Item("numItemGroup"))
                    Dim dtAttributes As DataTable = objItems.GetItemAttributes()
                    AttributesControl(dtAttributes)
                End If
            End If

            If dtItemDetails.Rows(0).Item("bitContainer") = "1" Then
                lblContainer.Visible = False
                ddlContainer.Visible = False
                txtNoOfItem.Visible = False
            End If

            If lngAssetCode > 0 Then
                If Not ddlAssetIClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClass")) Is Nothing Then
                    ddlAssetIClass.ClearSelection()
                    ddlAssetIClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClass")).Selected = True
                End If
            Else
                If Not ddlItemClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClass")) Is Nothing Then
                    ddlItemClass.ClearSelection()
                    ddlItemClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClass")).Selected = True
                End If
            End If

            If Not ddlBaseUOM.Items.FindByValue(dtItemDetails.Rows(0).Item("numBaseUnit")) Is Nothing Then
                ddlBaseUOM.Items.FindByValue(dtItemDetails.Rows(0).Item("numBaseUnit")).Selected = True

                If ddlBaseUOM.SelectedValue <> "0" Then
                    hfBaseUnit.Value = ddlBaseUOM.SelectedItem.Text
                End If
            End If
            If Not ddlContainer.Items.FindByValue(dtItemDetails.Rows(0).Item("numContainer")) Is Nothing Then
                ddlContainer.Items.FindByValue(dtItemDetails.Rows(0).Item("numContainer")).Selected = True

                If ddlContainer.SelectedValue <> "0" Then
                    txtNoOfItem.Text = dtItemDetails.Rows(0).Item("numNoItemIntoContainer")
                End If
            End If
            If Not ddSalesUOM.Items.FindByValue(dtItemDetails.Rows(0).Item("numSaleUnit")) Is Nothing Then
                ddSalesUOM.Items.FindByValue(dtItemDetails.Rows(0).Item("numSaleUnit")).Selected = True

                If ddSalesUOM.SelectedValue <> "0" Then
                    hfSaleUnit.Value = ddSalesUOM.SelectedItem.Text
                End If
            End If

            If Not ddPurchaseUOM.Items.FindByValue(dtItemDetails.Rows(0).Item("numPurchaseUnit")) Is Nothing Then
                ddPurchaseUOM.Items.FindByValue(dtItemDetails.Rows(0).Item("numPurchaseUnit")).Selected = True

                If ddPurchaseUOM.SelectedValue <> "0" Then
                    hfPurchaseUnit.Value = ddPurchaseUOM.SelectedItem.Text
                End If
            End If
            If Not ddlStandardProductIDType.Items.FindByValue(dtItemDetails.Rows(0).Item("tintStandardProductIDType")) Is Nothing Then
                ddlStandardProductIDType.ClearSelection()
                ddlStandardProductIDType.Items.FindByValue(dtItemDetails.Rows(0).Item("tintStandardProductIDType")).Selected = True
            End If

            Dim strListOfAPI As String() = CCommon.ToString(dtItemDetails.Rows(0).Item("vcExportToAPI")).Split(",")
            For k As Integer = 0 To strListOfAPI.Length - 1
                If radWebAPIList.Items.FindItemByValue(strListOfAPI(k)) IsNot Nothing Then
                    radWebAPIList.Items.FindItemByValue(strListOfAPI(k)).Checked = True
                End If
            Next

            Dim strListOfCategories As String() = CCommon.ToString(dtItemDetails.Rows(0).Item("vcItemCategories")).Split(",")
            For k As Integer = 0 To strListOfCategories.Length - 1
                For Each tn As RadTreeNode In rtvCategory.Nodes
                    If tn.Value = strListOfCategories(k) Then
                        tn.Checked = True
                    Else
                        FindNode(tn, strListOfCategories(k))
                    End If

                Next
            Next

            If Not ddlShipClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numShipClass")) Is Nothing Then
                ddlShipClass.ClearSelection()
                ddlShipClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numShipClass")).Selected = True
            End If
            If Not ddlBusinessProcess.Items.FindByValue(dtItemDetails.Rows(0).Item("numBusinessProcessId")) Is Nothing Then
                ddlBusinessProcess.ClearSelection()
                ddlBusinessProcess.Items.FindByValue(dtItemDetails.Rows(0).Item("numBusinessProcessId")).Selected = True
            End If

            lblCreatedBy.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("CreatedBy")), "", dtItemDetails.Rows(0).Item("CreatedBy"))
            lblLastModifiedBy.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("ModifiedBy")), "", dtItemDetails.Rows(0).Item("ModifiedBy"))
            txtItem.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("vcItemName")), "", dtItemDetails.Rows(0).Item("vcItemName"))
            hdnItemName.Value = txtItem.Text
            'lblItemID.Text = lngItemCode.ToString & "(" & txtItem.Text & ")"
            lblItemID.Text = txtItem.Text
            txtDescription.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("txtItemDesc")), "", dtItemDetails.Rows(0).Item("txtItemDesc"))
            txtVendorId.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("numVendorID")), "", dtItemDetails.Rows(0).Item("numVendorID"))
            Session("VendorId") = IIf(IsDBNull(dtItemDetails.Rows(0).Item("numVendorID")), 0, dtItemDetails.Rows(0).Item("numVendorID"))

            If Not IsDBNull(dtItemDetails.Rows(0).Item("monListPrice")) Then txtLstPrice.Text = CCommon.GetDecimalFormat(dtItemDetails.Rows(0).Item("monListPrice"))

            If Not IsDBNull(dtItemDetails.Rows(0).Item("numItemClassification")) Then
                If Not ddlIClassification.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClassification")) Is Nothing Then
                    ddlIClassification.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClassification")).Selected = True
                    If lngAssetCode <> 0 Then ddlAssetItemClass.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemClassification")).Selected = True

                End If
            End If
            txtASIN.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("vcASIN")), "", dtItemDetails.Rows(0).Item("vcASIN"))
            txtSKU.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("vcItemSKU")), "", dtItemDetails.Rows(0).Item("vcItemSKU"))
            txtModelID.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("vcModelID")), "", dtItemDetails.Rows(0).Item("vcModelID"))
            radcmbManufacturer.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("vcManufacturer")), "", dtItemDetails.Rows(0).Item("vcManufacturer"))
            radcmbManufacturer.SelectedValue = CCommon.ToLong(dtItemDetails.Rows(0).Item("numManufacturer"))
            txtUPC.Text = dtItemDetails.Rows(0).Item("numBarCodeId")
            lblInventoryItemType.Text = CCommon.ToString(dtItemDetails.Rows(0).Item("InventoryItemType"))
            hplChildMembership.Text = CCommon.ToString(dtItemDetails.Rows(0).Item("numChildMembershipCount"))
            lblMaxWO.Text = CCommon.ToString(dtItemDetails.Rows(0).Item("numWOQty"))
            If lngAssetCode <> 0 Then
                txtAssetName.Text = txtItem.Text
                txtAssetDescription.Text = txtDescription.Text
                txtAssetModelId.Text = txtModelID.Text
                txtAssetSerial.Text = txtSKU.Text
                txtBarCode.Text = dtItemDetails.Rows(0).Item("numBarCodeId")

                If dtAssetItem.Rows.Count > 0 Then
                    radCmbOrganization.SelectedValue = dtAssetItem.Rows(0).Item("numDivId")
                    radCmbOrganization.Text = dtAssetItem.Rows(0).Item("Company")
                    If Not ddlDepartment.Items.FindByValue(dtAssetItem.Rows(0).Item("numDeptId")) Is Nothing Then
                        ddlDepartment.Items.FindByValue(dtAssetItem.Rows(0).Item("numDeptId")).Selected = True
                    End If
                    optAppreciation.Checked = False
                    optDepreciation.Checked = False
                    If dtAssetItem.Rows(0).Item("btAppreciation") = True Then optAppreciation.Checked = True
                    If dtAssetItem.Rows(0).Item("btDepreciation") = True Then optDepreciation.Checked = True
                    txtAppreciation.Text = dtAssetItem.Rows(0).Item("numAppvalue")
                    txtDepreciation.Text = dtAssetItem.Rows(0).Item("numDepValue")
                    If Not IsDBNull(dtAssetItem.Rows(0).Item("dtPurchase")) Then calPurchaseDt.SelectedDate = dtAssetItem.Rows(0).Item("dtPurchase")
                    If Not IsDBNull(dtAssetItem.Rows(0).Item("dtWarrentyTill")) Then calExpiration.SelectedDate = dtAssetItem.Rows(0).Item("dtWarrentyTill")
                End If
            End If

            chkVirtual.Checked = dtItemDetails.Rows(0).Item("bitVirtualInventory")
            If chkVirtual.Checked Then
                txtAverageCost.Text = "0.0000"
                txtAverageCost.Enabled = False
                lblAvgCostToolTip.Visible = True
            Else
                If Not IsDBNull(dtItemDetails.Rows(0).Item("monAverageCost")) Then
                    txtAverageCost.Text = CCommon.GetDecimalFormat(dtItemDetails.Rows(0).Item("monAverageCost"))
                End If

                If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitItemIsUsedInOrder")) Then
                    txtAverageCost.Enabled = False
                    lblAvgCostToolTip.Visible = True
                End If
            End If

            If Not IsDBNull(dtItemDetails.Rows(0).Item("bitSerialized")) And Not IsDBNull(dtItemDetails.Rows(0).Item("bitLotNo")) Then
                If dtItemDetails.Rows(0).Item("bitSerialized") = True Then
                    chkSerializedItem.Checked = True
                    pnlAssetRental.Visible = False
                    chkExpense.Visible = False
                ElseIf dtItemDetails.Rows(0).Item("bitLotNo") = True Then
                    chkLotNo.Checked = True
                    pnlAssetRental.Visible = False
                    chkExpense.Visible = False
                Else
                    chkSerializedItem.Checked = False
                    chkLotNo.Checked = False
                End If
            End If
            If Not IsDBNull(dtItemDetails.Rows(0).Item("IsArchieve")) Then
                If dtItemDetails.Rows(0).Item("IsArchieve") = True Then
                    chkArchieve.Checked = True
                Else
                    chkArchieve.Checked = False
                End If
            End If

            If Not IsDBNull(dtItemDetails.Rows(0).Item("bitArchiveItem")) Then
                If dtItemDetails.Rows(0).Item("bitArchiveItem") = True Then
                    chkArchieveItemSetting.Checked = True
                Else
                    chkArchieveItemSetting.Checked = False
                End If
            End If
            If Not IsDBNull(dtItemDetails.Rows(0).Item("bitTimeContractFromSalesOrder")) Then
                If dtItemDetails.Rows(0).Item("bitTimeContractFromSalesOrder") = True Then
                    chkTimeContractFromSalesOrder.Checked = True
                Else
                    chkTimeContractFromSalesOrder.Checked = False
                End If
            End If
            If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitKitParent")) = True Or CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAssembly")) = True Then
                chkKit.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitKitParent"))
                tdAssembly.Visible = True
            End If

            If CCommon.ToLong(dtItemDetails.Rows(0).Item("numItemGroup")) > 0 Then
                If Not ddlItemGroup.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemGroup")) Is Nothing Then
                    ddlItemGroup.Items.FindByValue(dtItemDetails.Rows(0).Item("numItemGroup")).Selected = True
                End If

                If Not chkMatrix.Checked AndAlso CCommon.ToBool(dtItemDetails.Rows(0).Item("bitItemGroupHasAttributes")) Then
                    tdListPrice.Visible = False
                    tdListPrice1.Visible = False
                End If
            End If
            If dtItemDetails.Rows.Count > 0 AndAlso CCommon.ToString(dtItemDetails.Rows(0).Item("vcPathForTImage")) <> "" Then
                If CCommon.ToString(dtItemDetails.Rows(0).Item("vcPathForTImage")).StartsWith("http") Then
                    imgItem.ImageUrl = dtItemDetails.Rows(0).Item("vcPathForTImage")
                Else
                    imgItem.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtItemDetails.Rows(0).Item("vcPathForTImage")
                End If
            Else
                imgItem.ImageUrl = "../Images/DefaultProduct.png"
            End If

            If lngAssetCode <> 0 Then ddlAssetItemType.Items.FindByValue(dtItemDetails.Rows(0).Item("charItemType")).Selected = True
            If Not IsDBNull(dtItemDetails.Rows(0).Item("charItemType")) Then
                If Not ddlProduct.Items.FindByValue(dtItemDetails.Rows(0).Item("charItemType")) Is Nothing Then
                    ddlProduct.Items.FindByValue(dtItemDetails.Rows(0).Item("charItemType")).Selected = True
                    ProductTypeChange()
                    If ddlProduct.SelectedValue = "N" Then
                        If CCommon.ToBool(Session("EnableNonInventoryItemExpense")) Then
                            If Not IsDBNull(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")) Then
                                If Not ddlCOGS.Items.FindByValue(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")) Is Nothing Then
                                    ddlCOGS.Items.FindByValue(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")).Selected = True
                                End If
                            End If
                        End If

                        pnlAssetRental.Visible = False
                        divShipping.Visible = False
                    ElseIf ddlProduct.SelectedValue = "S" Then
                        ddlContainer.Visible = False
                        txtNoOfItem.Visible = False
                        lblContainer.Visible = False
                        pnlAssetRental.Visible = False

                        divShipping.Visible = False
                        chkExpense.Checked = CCommon.ToBool(dtItemDetails.Rows(0)("bitExpenseItem"))
                        If CCommon.ToBool(dtItemDetails.Rows(0)("bitExpenseItem")) Then
                            If ddlExpense.Items.FindByValue(CCommon.ToLong(dtItemDetails.Rows(0)("numExpenseChartAcntId"))) IsNot Nothing Then
                                ddlExpense.Items.FindByValue(CCommon.ToLong(dtItemDetails.Rows(0)("numExpenseChartAcntId"))).Selected = True
                            End If

                            If ddlCOGS.Items.FindByValue(CCommon.ToLong(dtItemDetails.Rows(0)("numCOGsChartAcntId"))) IsNot Nothing Then
                                ddlCOGS.Items.FindByValue(CCommon.ToLong(dtItemDetails.Rows(0)("numCOGsChartAcntId"))).Selected = True
                            End If

                            If Not ddlAssetAccount.Items.FindByValue(dtItemDetails.Rows(0).Item("numAssetChartAcntId")) Is Nothing Then
                                ddlAssetAccount.Items.FindByValue(dtItemDetails.Rows(0).Item("numAssetChartAcntId")).Selected = True
                                If lngAssetCode <> 0 Then ddlAssetAcCOA.Items.FindByValue(dtItemDetails.Rows(0).Item("numAssetChartAcntId")).Selected = True
                            End If
                        ElseIf CCommon.ToBool(Session("EnableNonInventoryItemExpense")) Then
                            If Not IsDBNull(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")) Then
                                If Not ddlCOGS.Items.FindByValue(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")) Is Nothing Then
                                    ddlCOGS.Items.FindByValue(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")).Selected = True
                                End If
                            End If
                        End If
                    ElseIf ddlProduct.SelectedValue = "P" Then 'Inventory item

                        If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitKitParent")) = True Or
                           CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAssembly")) = True Or
                           CCommon.ToBool(dtItemDetails.Rows(0).Item("bitLotNo")) = True Then
                            pnlAssetRental.Visible = False
                        Else
                            pnlAssetRental.Visible = True
                        End If

                        ' BUG NO : 1893------------------------------------------------------------------------

                        If Val(ddlIClassification.SelectedValue) > 0 Then
                            If CCommon.ToLong(ddlIClassification.SelectedValue) = CCommon.ToLong(Session("RentalItemClass")) Then
                                tdlblCogs1.Visible = False
                                tdddlCogs1.Visible = False
                            End If
                        End If

                        '--------------------------------------------------------------------------------------
                        If Not IsDBNull(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")) Then
                            If Not ddlCOGS.Items.FindByValue(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")) Is Nothing Then
                                ddlCOGS.Items.FindByValue(dtItemDetails.Rows(0).Item("numCOGsChartAcntId")).Selected = True
                            End If
                        End If

                        If Not IsDBNull(dtItemDetails.Rows(0).Item("numAssetChartAcntId")) Then
                            If Not ddlAssetAccount.Items.FindByValue(dtItemDetails.Rows(0).Item("numAssetChartAcntId")) Is Nothing Then
                                ddlAssetAccount.Items.FindByValue(dtItemDetails.Rows(0).Item("numAssetChartAcntId")).Selected = True
                                If lngAssetCode <> 0 Then ddlAssetAcCOA.Items.FindByValue(dtItemDetails.Rows(0).Item("numAssetChartAcntId")).Selected = True
                            End If
                        End If

                        'Author:Sachin Sadhu||Date:1stSept.2014
                        'Purpose :For Asset
                        If Not IsDBNull(dtItemDetails.Rows(0).Item("bitAsset")) Then
                            If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAsset")) = True Then
                                chkAsset.Checked = True
                                divAssetDepreciation.Visible = True
                                txtAssetDepreciationPeriod.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("numDepreciationPeriod"))
                                txtAssetResidual.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("monResidualValue"))
                                ddlAssetDepreciationMethod.SelectedValue = CCommon.ToShort(dtItemDetails.Rows(0).Item("tintDepreciationMethod"))

                                LoadAssetDepreciationGrid()
                            Else
                                chkAsset.Checked = False
                                divAssetDepreciation.Visible = False
                            End If
                        End If

                        If Not IsDBNull(dtItemDetails.Rows(0).Item("bitRental")) Then
                            If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitRental")) = True Then
                                chkRental.Checked = True
                                chkAsset.Checked = True
                            Else
                                chkRental.Checked = False
                            End If
                        End If

                        If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAssembly")) Then
                            hdnIsAssembly.Value = "1"
                            lblMaxWOTitle.Visible = True
                            lblMaxWO.Visible = True
                        End If

                        'end of code

                    End If
                End If
            End If
            If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForImage")) Then
                If dtItemDetails.Rows(0).Item("vcPathForImage") <> "" Then
                    hplImage.Text = "Edit Image/Document"
                Else : hplImage.Text = "Upload Image/Document"
                End If
            Else : hplImage.Text = "Upload Image/Document"
            End If
            If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitKitParent")) = True Or CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAssembly")) = True Then
                pnlAssetRental.Visible = False

                chkKit.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitKitParent"))
                If chkKit.Checked Then
                    divReorderQty.Visible = False
                    divReorderPoint.Visible = False
                End If
                divKitAssembltChildItems.Visible = True
                divPriceBasedOn.Visible = True
                divPriceBasedOnValues.Visible = True
                chkSerializedItem.Visible = False
                chkLotNo.Visible = False

                bindItemsGrid()

                chkAmtBasedonIndItems.Visible = True
                chkAssembly.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAssembly"))

                If chkAssembly.Checked = True Then
                    chkSoWorkOrder.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitSOWorkOrder"))

                    chkAssembly.Visible = True
                    tdAssembly.Visible = True
                    btnManAssem.Visible = True
                    btnManAssem.Attributes.Add("onclick", "return OpenManAssem(" & lngItemCode & ")")

                    lkbDisassemble.Visible = True
                    lkbDisassemble.Attributes.Add("onclick", "return OpenDisassembleWindow(" & lngItemCode & ")")

                    lblReorderTipAssembly.Visible = True
                    lblReorderTipAssemblyHeader.Visible = True
                    lblReorderTipOtherItems.Visible = False
                    lblReorderTipOtherItemsHeader.Visible = False
                    ddlKitSelectionType.Visible = False
                    chkSoWorkOrder.Visible = True
                    chkPreventOrphanedParents.Visible = False
                    btnNewKitItem.Visible = False
                    gvInventory.Columns.FindByUniqueNameSafe("BuildableQty").Visible = True
                    rtlChildItems.GetColumn("SKU").Visible = False
                Else
                    chkPreventOrphanedParents.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitPreventOrphanedParents"))
                    If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitKitSingleSelect")) Then
                        ddlKitSelectionType.SelectedValue = "2"
                    Else
                        ddlKitSelectionType.SelectedValue = "1"
                    End If
                    ddlKitSelectionType.Visible = True
                    chkSoWorkOrder.Visible = False
                    chkPreventOrphanedParents.Visible = True
                    btnNewKitItem.Visible = True
                End If
            Else
                divKitAssembltChildItems.Visible = False
                divPriceBasedOn.Visible = False
                ddlKitSelectionType.Visible = False
                chkSoWorkOrder.Visible = False
                chkPreventOrphanedParents.Visible = False
                btnNewKitItem.Visible = False
                divPriceBasedOnValues.Visible = False
                lblReorderTipAssembly.Visible = False
                lblReorderTipAssemblyHeader.Visible = False
                lblReorderTipOtherItems.Visible = True
                lblReorderTipOtherItemsHeader.Visible = True
            End If

            'If chkKit.Checked Or chkAssembly.Checked Then
            '    hplAdjustInv.Visible = False
            'End If

            If Not IsDBNull(dtItemDetails.Rows(0).Item("numIncomeChartAcntId")) Then
                If Not ddlIncomeAccount.Items.FindByValue(dtItemDetails.Rows(0).Item("numIncomeChartAcntId")) Is Nothing Then
                    ddlIncomeAccount.Items.FindByValue(dtItemDetails.Rows(0).Item("numIncomeChartAcntId")).Selected = True
                End If
            End If
            txtWeight.Text = dtItemDetails.Rows(0).Item("fltWeight")
            txtHeight.Text = dtItemDetails.Rows(0).Item("fltHeight")
            txtWidth.Text = dtItemDetails.Rows(0).Item("fltWidth")
            txtLength.Text = dtItemDetails.Rows(0).Item("fltLength")
            chkFreeShipping.Checked = dtItemDetails.Rows(0).Item("bitFreeShipping")
            chkAllowBackOrder.Checked = dtItemDetails.Rows(0).Item("bitAllowBackOrder")
            chkAmtBasedonIndItems.Checked = dtItemDetails.Rows(0).Item("bitCalAmtBasedonDepItems")
            ddlKitAssemblyPriceBasedOn.SelectedValue = CCommon.ToShort(dtItemDetails.Rows(0).Item("tintKitAssemblyPriceBasedOn"))

            If CCommon.ToString(dtItemDetails.Rows(0).Item("charItemType")) = "S" Then
                chkExpense.Visible = True


                If CCommon.ToBool(dtItemDetails.Rows(0).Item("bitExpenseItem")) Then
                    chkExpense.Checked = True
                    ddlExpense.Style.Add("display", "")
                    lblExpense.Style.Add("display", "")
                    tdAsset1.Style.Add("display", "")
                    tdddlAsset1.Style.Add("display", "")
                Else
                    chkExpense.Checked = False
                End If
            Else
                chkExpense.Visible = False
                ddlExpense.Style.Add("display", "none")
                lblExpense.Style.Add("display", "none")
            End If

            chkAllowDropShip.Checked = dtItemDetails.Rows(0).Item("bitAllowDropShip")
            txtReorderQty.Text = CCommon.ToDouble(dtItemDetails.Rows(0).Item("fltReorderQty"))
            txtReOrderPoint.Text = CCommon.ToDouble(dtItemDetails.Rows(0).Item("numReOrder"))
            chkAutomateReorderPoint.Checked = CCommon.ToBool(dtItemDetails.Rows(0).Item("bitAutomateReorderPoint"))

            objItems.DomainID = Session("DomainID")
            dtItemTax = objItems.ItemTax()
            Dim dr As DataRow
            dr = dtItemTax.NewRow
            dr("numTaxItemID") = 0
            dr("vcTaxName") = "Sales Tax(Default)"
            dr("bitApplicable") = dtItemDetails.Rows(0).Item("bitTaxable")
            dtItemTax.Rows.Add(dr)


            chkTaxItems.DataTextField = "vcTaxName"
            chkTaxItems.DataValueField = "numTaxItemID"
            chkTaxItems.DataSource = dtItemTax
            chkTaxItems.DataBind()

            Dim i As Integer
            For i = 0 To dtItemTax.Rows.Count - 1
                If Not IsDBNull(dtItemTax.Rows(i).Item("bitApplicable")) Then
                    If dtItemTax.Rows(i).Item("bitApplicable") = True Then
                        chkTaxItems.Items.FindByValue(dtItemTax.Rows(i).Item("numTaxItemID")).Selected = True
                    Else
                        chkTaxItems.Items.FindByValue(dtItemTax.Rows(i).Item("numTaxItemID")).Selected = False
                    End If
                End If
            Next

            'GET CRV TAX TYPES
            Dim objTaxDetails As New TaxDetails
            objTaxDetails.DomainID = Session("DomainID")
            Dim dtCRV As DataTable = objTaxDetails.GetCRVTaxTypes(lngItemCode)

            If Not dtCRV Is Nothing AndAlso dtCRV.Rows.Count > 0 Then
                ddlCRVTextTypes.DataSource = dtCRV
                ddlCRVTextTypes.DataTextField = "vcTaxName"
                ddlCRVTextTypes.DataValueField = "numTaxID"
                ddlCRVTextTypes.DataBind()

                ddlCRVTextTypes.Items.Insert(0, New ListItem("-- Select CRV Tax --", "0"))

                Dim drRow As DataRow() = dtCRV.Select("bitApplicable=1")

                If Not drRow Is Nothing AndAlso drRow.Count > 0 Then
                    If Not ddlCRVTextTypes.Items.FindByValue(drRow(0)("numTaxID")) Is Nothing Then
                        ddlCRVTextTypes.Items.FindByValue(drRow(0)("numTaxID")).Selected = True
                    End If
                End If

                ddlCRVTextTypes.Visible = True
            Else
                ddlCRVTextTypes.Visible = False
            End If


            If lngAssetCode <> 0 Then
                radItemTab.FindTabByValue("AssetDetails").Visible = True
                radItemTab.FindTabByValue("ItemDetails").Visible = False
                radItemTab.FindTabByValue("Inventory").Visible = False
                btnAddAssetSerial.Attributes.Add("onclick", "return SerialAssets(" & lngItemCode & "," & radCmbOrganization.SelectedValue & ")")
            End If
            'bindSimilarItemsGrid()



            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 16
            objCommon.Str = lngItemCode
            Dim lngTotalOrders As Long = objCommon.GetSingleFieldValue()

            If lngTotalOrders > 0 Then
                ddlProduct.Enabled = False

                chkAssembly.Enabled = False
                chkKit.Enabled = False

                chkLotNo.Enabled = False
                chkSerializedItem.Enabled = False

                chkExpense.Enabled = False
            End If

            objCommon.Mode = 41
            objCommon.Str = lngItemCode
            Dim lngAvailableQty As Double = CCommon.ToDouble(objCommon.GetSingleFieldValue())

            If lngAvailableQty > 0 Then
                chkLotNo.Enabled = False
                chkSerializedItem.Enabled = False
            End If


            If CCommon.ToInteger(dtItemDetails.Rows(0).Item("intParentKitItemCount")) = 0 Then
                divParentKitItems.Visible = False
                gvItemDependencies.Columns(6).Visible = False 'Kit Item
            Else
                divParentKitItems.Visible = True
                gvItemDependencies.Columns(6).Visible = True 'Kit Item
            End If

            If CCommon.ToBool(Session("EnableItemLevelUOM")) Then
                lblBaseUOM.Visible = False

                hplBaseUOM.Text = "Base UOM (" & CCommon.ToInteger(dtItemDetails.Rows(0).Item("tintUOMConvCount")) & ")"
                hplBaseUOM.Attributes.Add("onclick", "return OpenItemUOMConversion(" & hdnItemId.Value & ")")
                hplBaseUOM.Visible = True
            End If

            If chkKit.Checked Then
                tdAsset1.Style.Add("display", "none")
                tdddlAsset1.Style.Add("display", "none")
                ddlProduct.Enabled = False
            End If

            LoadShoppingCartExtendedDetail()
            LoadMetaTagsDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub ShowTaxesDefault()
        Try
            objItems.ItemCode = lngItemCode
            objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
            objItems.DomainID = Session("DomainID")
            dtItemTax = objItems.ItemTax()
            Dim dr As DataRow
            dr = dtItemTax.NewRow
            dr("numTaxItemID") = 0
            dr("vcTaxName") = "Sales Tax(Default)"
            dr("bitApplicable") = 0
            dtItemTax.Rows.Add(dr)


            chkTaxItems.DataTextField = "vcTaxName"
            chkTaxItems.DataValueField = "numTaxItemID"
            chkTaxItems.DataSource = dtItemTax
            chkTaxItems.DataBind()
        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Sub SaveTaxTypes()
        Try
            dtItemTax = New DataTable
            dtItemTax.Columns.Add("numTaxItemID")
            dtItemTax.Columns.Add("numTaxID")
            dtItemTax.Columns.Add("bitApplicable")
            Dim dr As DataRow
            Dim i As Integer
            For i = 0 To chkTaxItems.Items.Count - 1
                If chkTaxItems.Items(i).Selected = True Then
                    dr = dtItemTax.NewRow
                    dr("numTaxItemID") = chkTaxItems.Items(i).Value
                    dr("numTaxID") = Nothing
                    dr("bitApplicable") = 1
                    dtItemTax.Rows.Add(dr)
                End If
            Next

            If ddlCRVTextTypes.Visible AndAlso CCommon.ToLong(ddlCRVTextTypes.SelectedValue) > 0 Then
                dr = dtItemTax.NewRow
                dr("numTaxItemID") = 1
                dr("numTaxID") = CCommon.ToLong(ddlCRVTextTypes.SelectedValue)
                dr("bitApplicable") = 1
                dtItemTax.Rows.Add(dr)
            End If

            Dim ds As New DataSet
            Dim strdetails As String
            dtItemTax.TableName = "Table"
            ds.Tables.Add(dtItemTax)
            strdetails = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            objItems.strItemTaxTypes = strdetails
            objItems.ManageItemTaxTypes()
            ds.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadChartType()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "0103"
            dtChartAcntDetails = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlIncomeAccount.Items.Add(item)
            Next
            objCOA.AccountCode = "0101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAssetAccount.Items.Add(item)
                ddlAssetAcCOA.Items.Add(item)
            Next
            objCOA.AccountCode = "0106"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlCOGS.Items.Add(item)
                'ddlIncomeAccount.Items.Add(item) 'added by chintan bug id 746
            Next
            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlExpense.Items.Add(item)
            Next
            ''added by chintan bug id 746
            'objCOA.AccountCode = "0102" 'Liabilities
            'dtChartAcntDetails = objCOA.GetParentCategory()
            'For Each dr As DataRow In dtChartAcntDetails.Rows
            '    item = New ListItem()
            '    item.Text = dr("vcAccountName")
            '    item.Value = dr("numAccountID")
            '    item.Attributes("OptionGroup") = dr("vcAccountType")
            '    ddlIncomeAccount.Items.Add(item)
            'Next
            ddlIncomeAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
            ddlAssetAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
            ddlCOGS.Items.Insert(0, New ListItem("--Select One --", "0"))
            ddlExpense.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Loaddropdowns()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlIClassification, 36, Session("DomainID")) ''tem Classification
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = 0
            ddlItemGroup.DataSource = objItems.GetItemGroups.Tables(0)
            ddlItemGroup.DataTextField = "vcItemGroup"
            ddlItemGroup.DataValueField = "numItemGroupID"
            ddlItemGroup.DataBind()
            ddlItemGroup.Items.Insert(0, "--Select One--")
            ddlItemGroup.Items.FindByText("--Select One--").Value = "0"
            objItems.ItemCode = lngItemCode
            objCommon.sb_FillComboFromDBwithSel(ddlAssetItemClass, 36, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))

            'objCommon.sb_FillComboFromDBwithSel(ddlItemClass, 381, Session("DomainID"))
            CAdmin.sb_BindClassCombo(ddlItemClass, Session("DomainID"))
            CAdmin.sb_BindClassCombo(ddlAssetIClass, Session("DomainID"))

            objCommon.Mode = 25
            objCommon.DomainID = Session("DomainID")
            If CCommon.ToBool(objCommon.GetSingleFieldValue()) = False Then
                lblTipStandardProductIDType.Visible = False
                ddlStandardProductIDType.Visible = False
            Else
                Dim strProductIDTypes() As String = [Enum].GetNames(GetType(enmStandardProductIDType))
                Dim enumCount As String = 0
                For Each value As String In strProductIDTypes
                    ddlStandardProductIDType.Items.Add(New ListItem(value, enumCount))
                    enumCount = enumCount + 1
                Next
                ddlStandardProductIDType.Items.Insert(0, New ListItem("--Select One --", "0"))
            End If

            objCommon.DomainID = Session("DomainID")
            objCommon.UserCntID = Session("UserContactID")

            If GetQueryStringVal("DivisionID") <> "" Then
                objCommon.DivisionID = GetQueryStringVal("DivisionID")
            End If
            If lngItemCode = 0 And lngAssetCode <> 0 Then
                ddlAssetItemType.Items.FindByText("Asset").Selected = True
            End If

            objCommon.GetCompanySpecificValues1()
            Dim strCompany, strContactID As String
            strCompany = objCommon.GetCompanyName
            strContactID = objCommon.ContactID
            radCmbOrganization.Text = strCompany
            radCmbOrganization.SelectedValue = IIf(objCommon.DivisionID > 0, objCommon.DivisionID, "")

            If radItemTab.FindTabByValue("ItemDetails").Visible = True Then OrgSelectedIndexChanged()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UOMAll = True
            Dim dtUnit As DataTable
            dtUnit = objCommon.GetItemUOM()

            With ddlBaseUOM
                .DataSource = dtUnit
                .DataTextField = "vcUnitName"
                .DataValueField = "numUOMId"
                .DataBind()
                '.Items.Insert(0, "--Select One--")
                '.Items.FindByText("--Select One--").Value = "0"
            End With

            With ddSalesUOM
                .DataSource = dtUnit
                .DataTextField = "vcUnitName"
                .DataValueField = "numUOMId"
                .DataBind()
                '.Items.Insert(0, "--Select One--")
                '.Items.FindByText("--Select One--").Value = "0"
            End With

            With ddPurchaseUOM
                .DataSource = dtUnit
                .DataTextField = "vcUnitName"
                .DataValueField = "numUOMId"
                .DataBind()
                '.Items.Insert(0, "--Select One--")
                '.Items.FindByText("--Select One--").Value = "0"
            End With

            'With lstAvailableUnit
            '    .DataSource = dtUnit
            '    .DataTextField = "vcUnitName"
            '    .DataValueField = "numUOMId"
            '    .DataBind()
            'End With
            Dim objAPI As New BACRM.BusinessLogic.WebAPI.WebAPI

            objAPI.Mode = 1
            Dim dtAPI As DataTable = objAPI.GetWebApi()

            radWebAPIList.DataSource = dtAPI
            radWebAPIList.DataTextField = "vcProviderName"
            radWebAPIList.DataValueField = "WebAPIId"
            radWebAPIList.ShowDropDownOnTextboxClick = True
            radWebAPIList.CheckBoxes = True
            radWebAPIList.EmptyMessage = "-- Select Marketplace--"
            radWebAPIList.Width = 175
            radWebAPIList.EnableTextSelection = True
            radWebAPIList.DataBind()

            BindCategoryProfile()
            loadCategory()

            objCommon.sb_FillComboFromDBwithSel(ddlShipClass, 461, Session("DomainID"))

            'Added By Richa
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            ddlWareHouse.DataSource = objItem.GetWareHouses
            ddlWareHouse.DataTextField = "vcWareHouse"
            ddlWareHouse.DataValueField = "numWareHouseID"
            ddlWareHouse.DataBind()
            ddlWareHouse.Items.Insert(0, New ListItem("--Select One--", "0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadWareHouse()
        ddlGridWareHouse.Items.Clear()
        ddlGridWareHouse.DataSource = objCommon.LoadWarehouseBasedOnItemsWithAttr(lngItemCode)
        ddlGridWareHouse.DataTextField = "vcWareHouse"
        ddlGridWareHouse.DataValueField = "numWareHouseItemId"
        ddlGridWareHouse.DataBind()
        ddlGridWareHouse.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub

    Private Sub loadCategory()
        Try
            objItems = New CItems
            objItems.byteMode = 18
            objItems.DomainID = Session("DomainID")
            objItems.SiteID = IIf(Not Session("SiteID") Is Nothing, Session("SiteID"), 0)
            objItems.CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)

            Dim dtCategory As DataTable
            dtCategory = objItems.SeleDelCategory

            rtvCategory.DataTextField = "vcCategoryName"
            rtvCategory.DataValueField = "numCategoryID"
            rtvCategory.DataFieldID = "numCategoryID"
            rtvCategory.DataFieldParentID = "numDepCategory"
            rtvCategory.DataSource = dtCategory
            Try
                rtvCategory.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Sub BindUOM()
    '    objCommon = New CCommon

    '    Dim dtUnit As DataTable
    '    objCommon.DomainID = Session("DomainID")
    '    objCommon.ItemCode = lngItemCode
    '    objCommon.UOMAll = False
    '    dtUnit = objCommon.GetItemUOM()

    '    With lstAddUnit
    '        .DataSource = dtUnit
    '        .DataTextField = "vcUnitName"
    '        .DataValueField = "numUOMId"
    '        .DataBind()
    '    End With
    'End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If chkVirtual.Visible AndAlso chkVirtual.Checked Then
                txtAverageCost.Text = "0"
            End If

            If lngItemCode = 0 Then
                If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                    Dim objOpportunity As New OppotunitiesIP
                    objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                    objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOpportunity.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                    objOpportunity.OpportunityDetails()

                    If (objOpportunity.Source = 1 Or objOpportunity.Source = 2 Or objOpportunity.Source = 3 Or objOpportunity.Source = 4) AndAlso String.IsNullOrEmpty(txtASIN.Text.Trim()) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('Add ASIN in order to proceed.');", True)
                        Exit Sub
                    ElseIf String.IsNullOrEmpty(txtSKU.Text.Trim()) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('Add SKU in order to proceed.');", True)
                        Exit Sub
                    End If
                End If

                Dim newItemCode As Long = 0

                If radItemTab.FindTabByValue("AssetDetails").Visible = False Then
                    newItemCode = Save()
                Else
                    newItemCode = SaveAsset()
                End If

                If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItem.ItemCode = newItemCode

                    If Not objItem.IsKitWithChildKits() Then
                        Dim objOpp As New MOpportunity
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                        objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        objOpp.OppItemCode = CCommon.ToLong(GetQueryStringVal("numOppItemID"))
                        objOpp.AddMappedItem(newItemCode)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('You need to add item manually to order as selected item is kit with child kits');", True)
                    End If

                    Response.Redirect("~/opportunity/frmOpportunities.aspx?opId=" & CCommon.ToLong(GetQueryStringVal("numOppID")), False)
                Else
                    If radItemTab.FindTabByValue("AssetDetails").Visible = False Then
                        Response.Redirect("~/items/frmKitDetails.aspx?ItemCode=" & newItemCode & "&frm=" & GetQueryStringVal("frm"), False)
                    Else
                        Response.Redirect("~/items/frmKitDetails.aspx?ItemCode=" & newItemCode & "&AssetCode=-1&frm=" & GetQueryStringVal("frm"), False)
                    End If
                End If
            Else
                If lngAssetCode <> 0 Then
                    SaveAsset()
                    radCmbOrganization.SelectedValue = Session("OwnerId")
                    radCmbOrganization.Text = Session("OwnerName")
                Else
                    Save()
                End If
                'BindUOM()
                populateDataSet()
                gvInventory.Rebind()
                ProductTypeChange()

                If radItemTab.SelectedIndex = 5 Then 'Related Items
                    IframeRelatedItems.Attributes.Add("src", "../Items/frmRelatedItems.aspx?ParentItemId=" & lngItemCode & "&Mode=0")
                End If
            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_GROUP_NOT_SELECTED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select item group');", True)
            ElseIf ex.Message.Contains("ITEM_ATTRIBUTES_NOT_SELECTED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select values for all attributes before you can save this record.');", True)
            ElseIf ex.Message.Contains("ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('Same attributes combination already exists.');", True)
            ElseIf ex.Message.Contains("ITEM_GROUP_NOT_CONFIGURED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                objOpportunity.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objOpportunity.OpportunityDetails()

                If (objOpportunity.Source = 1 Or objOpportunity.Source = 2 Or objOpportunity.Source = 3 Or objOpportunity.Source = 4) AndAlso String.IsNullOrEmpty(txtASIN.Text.Trim()) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('Add ASIN in order to proceed.');", True)
                    Exit Sub
                ElseIf String.IsNullOrEmpty(txtSKU.Text.Trim()) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('Add SKU in order to proceed.');", True)
                    Exit Sub
                End If
            End If

            Dim newItemCode As Long = 0

            If radItemTab.FindTabByValue("AssetDetails").Visible = False Then
                newItemCode = Save()
            Else
                newItemCode = SaveAsset()
            End If

            If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.ItemCode = newItemCode

                If Not objItem.IsKitWithChildKits() Then
                    Dim objOpp As New MOpportunity
                    objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                    objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                    objOpp.OppItemCode = CCommon.ToLong(GetQueryStringVal("numOppItemID"))
                    objOpp.AddMappedItem(newItemCode)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('You need to add item manually to order as selected item is kit with child kits');", True)
                End If

                Response.Redirect("~/opportunity/frmOpportunities.aspx?opId=" & CCommon.ToLong(GetQueryStringVal("numOppID")), False)
            Else
                SB_PageRedirect()
            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_GROUP_NOT_SELECTED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select item group');", True)
            ElseIf ex.Message.Contains("ITEM_ATTRIBUTES_NOT_SELECTED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select values for all attributes before you can save this record.');", True)
            ElseIf ex.Message.Contains("ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('Same attributes combination already exists.');", True)
            ElseIf ex.Message.Contains("ITEM_GROUP_NOT_CONFIGURED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function SaveAsset() As Long

        Dim vcAttributes As String = ""

        If chkMatrix.Checked Then
            If CCommon.ToLong(ddlItemGroup.SelectedValue) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectItemGroup", "alert('You have to select item group.');", True)
                Exit Function
            Else
                objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                objItems.ItemCode = lngItemCode
                objItems.ItemGroupID = CCommon.ToLong(ddlItemGroup.SelectedValue)
                Dim dtAttributes As DataTable = objItems.GetItemAttributes()

                If dtAttributes Is Nothing Or dtAttributes.Rows.Count = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
                Else
                    If dtAttributes.Select("numVariations=0").Length > 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
                    Else
                        'RETRIVE MATRIX ITEM ATTRIBUTES VALUE
                        Dim dtCusTable As New DataTable
                        dtCusTable.Columns.Add("Fld_ID")
                        dtCusTable.Columns.Add("Fld_Value")
                        dtCusTable.TableName = "CusFlds"
                        Dim drCusRow As DataRow

                        For Each drAttr In dtAttributes.Rows
                            drCusRow = dtCusTable.NewRow
                            drCusRow("Fld_ID") = drAttr("fld_id")

                            If drAttr("fld_type") = "SelectBox" Then
                                Dim ddl As DropDownList
                                ddl = plhItemAttributes.FindControl(drAttr("Fld_id"))

                                If CCommon.ToLong(ddl.SelectedValue) > 0 Then
                                    drCusRow("Fld_Value") = CCommon.ToLong(ddl.SelectedItem.Value)
                                Else
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select values for all attributes before you can save this record.');", True)
                                    Exit Function
                                End If
                            End If

                            dtCusTable.Rows.Add(drCusRow)
                            dtCusTable.AcceptChanges()
                        Next

                        Dim ds1 As New DataSet
                        ds1.Tables.Add(dtCusTable)
                        vcAttributes = ds1.GetXml
                    End If
                End If
            End If
        End If


        Dim dr As DataRow
        Dim strEditedfldlst As String = ""

        With objItems
            .ItemCode = lngItemCode
            .ItemName = txtAssetName.Text.Trim
            .ItemDesc = txtAssetDescription.Text
            .ItemType = ddlAssetItemType.SelectedItem.Value
            .ListPrice = 0
            .ItemClassification = ddlAssetItemClass.SelectedItem.Value
            .ModelID = txtAssetModelId.Text
            .KitParent = 0
            .bitSerialized = 0
            .SKU = txtAssetSerial.Text
            .AverageCost = 0
            .LabourCost = 0
            ' If Not cal.SelectedDate = "" Then .DateEntered = cal.SelectedDate

            .UserCntID = Session("UserContactID")
            .DomainID = Session("DomainID")
            .AssetChartAcntId = ddlAssetAcCOA.SelectedItem.Value
            .Weight = 0
            .Height = 0
            .Length = 0
            .Width = 0
            .FreeShipping = 0
            .AllowBackOrder = 0
            .UnitofMeasure = 0
            .BarCodeID = txtBarCode.Text
            .ItemClass = ddlAssetIClass.SelectedValue
            .IsMatrix = chkMatrix.Checked
            .ItemAttributes = vcAttributes
        End With

        If lngItemCode > 0 Then
            Dim dtVendorDetails As New DataTable
            dtVendorDetails.Columns.Add("VendorID")
            dtVendorDetails.Columns.Add("PartNo")
            dtVendorDetails.Columns.Add("monCost")
            dtVendorDetails.Columns.Add("intMinQty")
            dtVendorDetails.Columns.Add("vcNotes")
            dtVendorDetails.TableName = "Table"

            For Each dgGridItem As DataGridItem In dgVendor.Items
                dr = dtVendorDetails.NewRow
                dr("VendorID") = CType(dgGridItem.FindControl("lblvendorID"), Label).Text
                dr("PartNo") = CType(dgGridItem.FindControl("txtPartNo"), TextBox).Text
                dr("monCost") = CCommon.ToDouble(CType(dgGridItem.FindControl("txtCost"), TextBox).Text)
                dr("intMinQty") = CCommon.ToDouble(CType(dgGridItem.FindControl("txtMinQty"), TextBox).Text)
                dr("vcNotes") = CType(dgGridItem.FindControl("txtNotes"), TextBox).Text

                dtVendorDetails.Rows.Add(dr)
                If CType(dgGridItem.FindControl("radPreffered"), RadioButton).Checked = True Then
                    objItems.VendorID = CType(dgGridItem.FindControl("lblvendorID"), Label).Text
                End If
            Next

            Dim ds As New DataSet
            ds.Tables.Add(dtVendorDetails)
            strEditedfldlst = ds.GetXml()
            ds.Tables.Remove(dtVendorDetails)



        End If
        Dim strError As String = objItems.ManageItemsAndKits()
        If strError.Length > 2 Then
            ShowMessage(strError)
            Exit Function
        End If

        lngItemCode = objItems.ItemCode
        If lngItemCode > 0 Then
            With objItems
                Session("OwnerId") = radCmbOrganization.SelectedValue
                Session("OwnerName") = radCmbOrganization.Text
                .DepartmentID = ddlDepartment.SelectedValue
                .DivisionID = radCmbOrganization.SelectedValue 'Session("DivisionId")
                .type = 1
                .Appreciation = IIf(optAppreciation.Checked = True, 1, 0)
                .Deppreciation = IIf(optDepreciation.Checked = True, 1, 0)
                .AppreValue = IIf(optAppreciation.Checked = True, txtAppreciation.Text, 0)
                .DepreValue = IIf(optDepreciation.Checked = True, txtDepreciation.Text, 0)
                If calPurchaseDt.SelectedDate <> "" Then .PurchaseDate = calPurchaseDt.SelectedDate
                If calExpiration.SelectedDate <> "" Then .WarrentyTill = calExpiration.SelectedDate
                .SaveAssetItem()
            End With
        End If

        If lngItemCode > 0 And strEditedfldlst <> "" Then
            objItems.strFieldList = strEditedfldlst
            objItems.ItemCode = lngItemCode
            objItems.UpdateVendor()
        End If
        Return objItems.ItemCode
    End Function
    Function Save() As Long
        Try
            Dim vcAttributes As String = ""

            If chkMatrix.Checked Then
                If CCommon.ToLong(ddlItemGroup.SelectedValue) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectItemGroup", "alert('You have to select item group.');", True)
                    Exit Function
                Else
                    objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItems.ItemCode = lngItemCode
                    objItems.ItemGroupID = CCommon.ToLong(ddlItemGroup.SelectedValue)
                    Dim dtAttributes As DataTable = objItems.GetItemAttributes()

                    If dtAttributes Is Nothing Or dtAttributes.Rows.Count = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
                    Else
                        If dtAttributes.Select("numVariations=0").Length > 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ConfigureGroup", "alert('Select an item group with at lest one attribute. To do so for this group do the following.\n1 Create a new custom field.\n2. From the wizard select ""Item Attributes"" from the ""Select Location"" drop down, and proceed to add at least one attribute value.\n3. Come back to this record, select the group and go from there');", True)
                        Else
                            'RETRIVE MATRIX ITEM ATTRIBUTES VALUE
                            Dim dtCusTable As New DataTable
                            dtCusTable.Columns.Add("Fld_ID")
                            dtCusTable.Columns.Add("Fld_Value")
                            dtCusTable.TableName = "CusFlds"
                            Dim drCusRow As DataRow

                            For Each drAttr In dtAttributes.Rows
                                drCusRow = dtCusTable.NewRow
                                drCusRow("Fld_ID") = drAttr("fld_id")

                                If drAttr("fld_type") = "SelectBox" Then
                                    If CCommon.ToBool(drAttr("bitAutocomplete")) Then
                                        Dim radcmb As RadComboBox
                                        radcmb = plhItemAttributes.FindControl(drAttr("Fld_id"))

                                        If CCommon.ToLong(radcmb.SelectedValue) > 0 Then
                                            drCusRow("Fld_Value") = CCommon.ToLong(radcmb.SelectedValue)
                                        Else
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select values for all attributes before you can save this record.');", True)
                                            Exit Function
                                        End If
                                    Else
                                        Dim ddl As DropDownList
                                        ddl = plhItemAttributes.FindControl(drAttr("Fld_id"))

                                        If CCommon.ToLong(ddl.SelectedValue) > 0 Then
                                            drCusRow("Fld_Value") = CCommon.ToLong(ddl.SelectedItem.Value)
                                        Else
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectGroup", "alert('You have to select values for all attributes before you can save this record.');", True)
                                            Exit Function
                                        End If
                                    End If
                                End If

                                dtCusTable.Rows.Add(drCusRow)
                                dtCusTable.AcceptChanges()
                            Next

                            Dim ds1 As New DataSet
                            ds1.Tables.Add(dtCusTable)
                            vcAttributes = ds1.GetXml
                        End If
                    End If
                End If
            End If

            Dim dr As DataRow
            Dim strEditedfldlst As String = ""

            With objItems
                .ItemCode = lngItemCode
                .ItemName = txtItem.Text.Trim
                .ItemDesc = txtDescription.Text
                .ItemType = ddlProduct.SelectedItem.Value
                .ListPrice = IIf(txtLstPrice.Text = "", 0, txtLstPrice.Text)
                .ItemClassification = ddlIClassification.SelectedItem.Value
                .ModelID = txtModelID.Text
                .KitParent = chkKit.Checked
                .bitSerialized = chkSerializedItem.Checked
                .bitLotNo = chkLotNo.Checked
                .bitArchieve = chkArchieve.Checked
                .Manufacturer = radcmbManufacturer.Text
                .numManufacturer = CCommon.ToLong(radcmbManufacturer.SelectedValue)
                .numContainer = CCommon.ToLong(ddlContainer.SelectedValue)
                .numNoItemIntoContainer = CCommon.ToLong(txtNoOfItem.Text)
                .BarCodeID = txtUPC.Text.Trim()
                If chkTaxItems.Items.Count > 0 Then
                    .Taxable = chkTaxItems.Items.FindByValue(0).Selected
                End If
                .SKU = txtSKU.Text
                .ItemGroupID = ddlItemGroup.SelectedItem.Value
                If chkVirtual.Visible AndAlso chkVirtual.Checked Then
                    .AverageCost = 0
                Else
                    .AverageCost = CCommon.ToDouble(txtAverageCost.Text)
                End If
                .IsVirtualInventory = chkVirtual.Checked
                .LabourCost = 0

                .BaseUnit = ddlBaseUOM.SelectedValue
                .PurchaseUnit = ddPurchaseUOM.SelectedValue
                .SaleUnit = ddSalesUOM.SelectedValue
                .vcASIN = txtASIN.Text

                Dim ds As New DataSet
                If dtCusTable.Columns.Count > 0 Then ds.Tables.Add(dtCusTable)


                .strFieldList = IIf(IsNothing(ds), "", ds.GetXml)
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")

                If .ItemType = "S" Or .ItemType = "N" Then
                    .IsExpenseItem = chkExpense.Visible = True AndAlso chkExpense.Checked
                    If chkExpense.Visible = True AndAlso chkExpense.Checked Then
                        .AssetChartAcntId = CCommon.ToLong(ddlAssetAccount.SelectedValue)
                        .COGSChartAcntId = CCommon.ToLong(ddlCOGS.SelectedValue)
                        .ExpenseChartAcntId = CCommon.ToLong(ddlExpense.SelectedValue)
                    Else
                        .AssetChartAcntId = 0
                        .COGSChartAcntId = IIf(CCommon.ToBool(Session("EnableNonInventoryItemExpense")), ddlCOGS.SelectedValue, 0)
                    End If
                Else
                    .COGSChartAcntId = ddlCOGS.SelectedItem.Value
                    .AssetChartAcntId = ddlAssetAccount.SelectedItem.Value
                End If
                .IncomeChartAcntId = ddlIncomeAccount.SelectedItem.Value
                .Weight = IIf(IsNumeric(txtWeight.Text) = False, 0, txtWeight.Text)
                .Height = IIf(IsNumeric(txtHeight.Text) = False, 0, txtHeight.Text)
                .Length = IIf(IsNumeric(txtLength.Text) = False, 0, txtLength.Text)
                .Width = IIf(IsNumeric(txtWidth.Text) = False, 0, txtWidth.Text)
                .FreeShipping = chkFreeShipping.Checked
                .AllowBackOrder = chkAllowBackOrder.Checked
                .ArchieveItemSetting = chkArchieveItemSetting.Checked
                .bitTimeContractFromSalesOrder = chkTimeContractFromSalesOrder.Checked
                '.UnitofMeasure = ddlUnitofMeasure.SelectedItem.Text

                If chkKit.Checked = True Or chkAssembly.Checked = True Then
                    If lngItemCode > 0 Then
                        ds = New DataSet

                        Dim dtChildItem As New DataTable
                        dtChildItem.Columns.Add("numItemDetailID")
                        dtChildItem.Columns.Add("ItemKitID")
                        dtChildItem.Columns.Add("ChildItemID")
                        dtChildItem.Columns.Add("QtyItemsReq")
                        dtChildItem.Columns.Add("numUOMId")
                        dtChildItem.Columns.Add("vcItemDesc")
                        dtChildItem.Columns.Add("sintOrder")
                        dtChildItem.Columns.Add("tintView")
                        dtChildItem.Columns.Add("vcFields")
                        dtChildItem.Columns.Add("bitUseInDynamicSKU")
                        dtChildItem.Columns.Add("bitOrderEditable")
                        dtChildItem.Columns.Add("bitUseInNewItemConfiguration")

                        For Each dgGridItem As TreeListDataItem In rtlChildItems.Items
                            If CType(dgGridItem.FindControl("txtQuantity"), TextBox).Visible = True Then
                                dr = dtChildItem.NewRow
                                dr("numItemDetailID") = CCommon.ToLong(dgGridItem("numItemDetailID").Text)
                                dr("ItemKitID") = lngItemCode
                                dr("ChildItemID") = CCommon.ToLong(dgGridItem("numItemCode").Text)
                                dr("vcItemDesc") = CType(dgGridItem.FindControl("txtIDescription"), TextBox).Text
                                dr("sintOrder") = CType(dgGridItem.FindControl("txtSequence"), TextBox).Text

                                If dgGridItem("ItemType").Text = "P" Then
                                    dr("numUOMId") = CType(dgGridItem.FindControl("ddlItemUOM"), DropDownList).SelectedValue.Split("~")(0)
                                    dr("QtyItemsReq") = IIf(CType(dgGridItem.FindControl("txtQuantity"), TextBox).Text.Trim.Length > 0, CType(dgGridItem.FindControl("txtQuantity"), TextBox).Text * CType(dgGridItem.FindControl("ddlItemUOM"), DropDownList).SelectedValue.Split("~")(1), 0)
                                Else
                                    dr("QtyItemsReq") = CType(dgGridItem.FindControl("txtQuantity"), TextBox).Text
                                End If

                                dr("tintView") = CCommon.ToShort(DirectCast(dgGridItem.FindControl("ddlView"), DropDownList).SelectedValue)
                                dr("vcFields") = String.Join(",", DirectCast(dgGridItem.FindControl("rcbFields"), RadComboBox).CheckedItems.Select(Function(item) item.Value).ToList())
                                dr("bitUseInDynamicSKU") = CCommon.ToShort(DirectCast(dgGridItem.FindControl("chkSKU"), CheckBox).Checked)
                                dr("bitOrderEditable") = CCommon.ToShort(DirectCast(dgGridItem.FindControl("chkOrderEditable"), CheckBox).Checked)
                                dr("bitUseInNewItemConfiguration") = CCommon.ToShort(DirectCast(dgGridItem.FindControl("chkUseInNewItemConfiguration"), CheckBox).Checked)

                                dtChildItem.Rows.Add(dr)
                            End If
                        Next
                        dtChildItem.TableName = "Table1"
                        ds.Tables.Add(dtChildItem)
                        .strChildItems = ds.GetXml
                        ds.Tables.Remove(dtChildItem)
                    End If
                    .CalPriceBasedOnIndItems = chkAmtBasedonIndItems.Checked
                    .KitAssemblyPriceBasedOn = ddlKitAssemblyPriceBasedOn.SelectedValue
                End If
                .Assembly = chkAssembly.Checked
                .BusinessProcessId = ddlBusinessProcess.SelectedValue
                .ItemClass = ddlItemClass.SelectedValue
                .StandardProductIDType = ddlStandardProductIDType.SelectedValue
                .ListOfAPI = GetSelectedAPI()
                .CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)
                .ListOfCategories = GetSelectedCategory()
                .ShippingClass = ddlShipClass.SelectedValue
                .ReorderQty = If(ddlProduct.SelectedValue.ToLower() = "p" AndAlso divReorderQty.Visible, CCommon.ToDouble(txtReorderQty.Text), 0)

                If .ItemType = "N" Or .ItemType = "P" Then
                    .AllowDropShip = chkAllowDropShip.Checked
                End If
                If .ItemType = "P" Then 'If inventory ,then there's scope for Rental or Asset
                    .bitAsset = chkAsset.Checked
                    .bitRental = chkRental.Checked
                    If chkRental.Checked Then
                        chkAsset.Checked = True
                        .bitAsset = chkAsset.Checked
                    End If

                End If
                .IsMatrix = chkMatrix.Checked
                .ItemAttributes = vcAttributes

                If .Assembly Then
                    .IsSOWorkOrder = chkSoWorkOrder.Checked
                End If
                If .KitParent Then
                    .IsKitSingleSelect = If(CCommon.ToShort(ddlKitSelectionType.SelectedValue) = 2, True, False)
                    .IsPreventOrphanedParents = chkPreventOrphanedParents.Checked
                End If
                .AssetDepreciationPeriod = Convert.ToDouble(txtAssetDepreciationPeriod.Value)
                .AssetResidual = Convert.ToDouble(txtAssetResidual.Value)
                .AssetDepreciationMethod = CCommon.ToShort(ddlAssetDepreciationMethod.SelectedValue)
            End With

            If lngItemCode > 0 Then
                Dim dtVendorDetails As New DataTable
                dtVendorDetails.Columns.Add("VendorID")
                dtVendorDetails.Columns.Add("PartNo")
                dtVendorDetails.Columns.Add("monCost")
                dtVendorDetails.Columns.Add("intMinQty")
                dtVendorDetails.Columns.Add("vcNotes")

                dtVendorDetails.TableName = "Table"

                For Each dgGridItem As DataGridItem In dgVendor.Items
                    dr = dtVendorDetails.NewRow
                    dr("VendorID") = CType(dgGridItem.FindControl("lblvendorID"), Label).Text
                    dr("PartNo") = CType(dgGridItem.FindControl("txtPartNo"), TextBox).Text
                    dr("monCost") = CCommon.ToDouble(CType(dgGridItem.FindControl("txtCost"), TextBox).Text)
                    dr("intMinQty") = CCommon.ToDouble(CType(dgGridItem.FindControl("txtMinQty"), TextBox).Text)
                    dr("vcNotes") = CType(dgGridItem.FindControl("txtNotes"), TextBox).Text

                    dtVendorDetails.Rows.Add(dr)
                    If CType(dgGridItem.FindControl("radPreffered"), RadioButton).Checked = True Then
                        objItems.VendorID = CType(dgGridItem.FindControl("lblvendorID"), Label).Text
                    End If
                Next

                Dim ds As New DataSet
                ds.Tables.Add(dtVendorDetails)

                strEditedfldlst = ds.GetXml()
                ds.Tables.Remove(dtVendorDetails)
            End If



            Dim strError As String = objItems.ManageItemsAndKits()

            If strError.Length > 2 Then
                ShowMessage(strError)
                Exit Function
            Else
                objItems.ItemCode = objItems.ItemCode
                objItems.byteMode = 1
                objItems.str = RadEditor1.Content.Trim()
                objItems.txtShortDesc = RadEditor2.Content.Trim()
                objItems.UserCntID = Session("UserContactID")
                objItems.ManageItemExtendedDesc()

                ManageMetaTag(objItems.ItemCode)
            End If
            If objItems.ItemCode > 0 Then lngItemCode = objItems.ItemCode 'To save custom field when creating item

            If lngItemCode > 0 Then
                ''Saving Item Tax Types
                SaveTaxTypes()
                SaveCusField()




                objItems.strFieldList = strEditedfldlst
                objItems.ItemCode = lngItemCode
                objItems.UpdateVendor()
            End If
            Return objItems.ItemCode
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub SaveEDIFields()
        Try
            If (dsEDICF IsNot Nothing) Then
                Dim dtEDICF As DataTable = dsEDICF.Tables(0)
                If (dtEDICF.Rows.Count > 0) Then
                    For Each row As DataRow In dtEDICF.Rows
                        Dim GrpID As Integer = 20
                        Dim FldDTLID As Long = CCommon.ToLong(row("FldDTLID"))
                        Dim Fld_ID As Long = CCommon.ToLong(row("Fld_ID"))
                        Dim RecId As Long = lngItemCode
                        Dim numModifiedBy As Long = CCommon.ToLong(row("numModifiedBy"))
                        Dim bintModifiedDate As String = row("bintModifiedDate")
                        Dim Fld_Value As String = CType(tblCustomEDIFields.FindControl("txtCustEDI" + row("Fld_id").ToString), TextBox).Text

                        objCommon = New CCommon
                        objCommon.UpdateEDICustomFields(GrpID, RecId, Fld_ID, Fld_Value, FldDTLID, numModifiedBy, bintModifiedDate)
                    Next
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadShoppingCartExtendedDetail()
        Try
            Dim objItems As New CItems
            Dim dtTable As DataTable
            objItems.ItemCode = lngItemCode
            objItems.byteMode = 2
            objItems.UserCntID = Session("UserContactID")
            Dim dt As DataTable
            dt = objItems.ViewItemExtendedDesc()
            If (dt.Rows.Count > 0) Then
                RadEditor1.Content = dt.Rows(0)("txtDesc")
                RadEditor2.Content = dt.Rows(0)("txtShortDesc")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub ManageMetaTag(ByVal lngItemCode As Long)
        Try
            Dim objSite As New Sites
            objSite.MetaID = CCommon.ToLong(hfMetaID.Value)
            objSite.PageTitle = txtPageTitle.Text.Trim()
            objSite.MetaKeywords = txtMetaKeywords.Text.Trim()
            objSite.MetaDescription = txtMetaDescription.Text.Trim()
            objSite.MetaTagFor = 2
            objSite.ReferenceID = lngItemCode
            objSite.ManageMetaTags()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadMetaTagsDetails()
        Try
            Dim objSite As New Sites
            Dim dtMeta As DataTable
            objSite.MetaID = 0
            objSite.MetaTagFor = 2 '1-page,2-Item
            objSite.ReferenceID = lngItemCode
            dtMeta = objSite.GetMetaTag()

            If dtMeta.Rows.Count > 0 Then
                txtPageTitle.Text = CCommon.ToString(dtMeta.Rows(0)("vcPageTitle"))
                txtMetaKeywords.Text = CCommon.ToString(dtMeta.Rows(0)("vcMetaKeywords"))
                txtMetaDescription.Text = CCommon.ToString(dtMeta.Rows(0)("vcMetaDescription"))
                hfMetaID.Value = CCommon.ToString(dtMeta.Rows(0)("numMetaID"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub LoadEDIFields()
    '    BindEDICFValues()
    '    tblEdiFields.Controls.Clear()

    '    objCommon = New CCommon
    '    objCommon.DomainID = Session("DomainID")
    '    Dim dsEdiFields As DataSet = objCommon.GetEDICustomFields(20, lngItemCode)

    '    If (dsEdiFields IsNot Nothing And dsEdiFields.Tables.Count > 0) Then
    '        Dim dtEDI As DataTable = dsEdiFields.Tables(0)
    '        If (dtEDI.Rows.Count > 0) Then
    '            For Each dr As DataRow In dtEDI.Rows
    '                If (CCommon.ToInteger(dr("Grp_id")) = 20) Then
    '                    Dim tblRow As New TableRow
    '                    Dim tblCell1 As New TableCell

    '                    tblCell1.Style.Add("text-align", "right")
    '                    tblCell1.Width = New Unit(50%)

    '                    Dim lblCFName As Label
    '                    lblCFName = New Label
    '                    lblCFName.Text = dr("Fld_label").ToString
    '                    tblCell1.Controls.Add(lblCFName)
    '                    tblRow.Cells.Add(tblCell1)


    '                    Dim tblCell2 As New TableCell
    '                    tblCell2.Style.Add("text-align", "right")
    '                    tblCell2.Width = New Unit(50%)

    '                    Dim txt As New TextBox
    '                    txt.CssClass = "signup"
    '                    txt.ID = "txtEDI" + dr("Fld_id").ToString
    '                    ' txt.Text = dr("Fld_Value").ToString
    '                    txt.ClientIDMode = ClientIDMode.Static

    '                    txt.Width = Unit.Pixel(180)
    '                    tblCell2.Controls.Add(txt)
    '                    tblRow.Cells.Add(tblCell2)

    '                    tblEdiFields.Rows.Add(tblRow)

    '                    Dim dtEDIFields As DataTable
    '                    dtEDIFields = dsEDICF.Tables(0)

    '                    Dim drEDI As DataRow
    '                    drEDI = dtEDIFields.NewRow

    '                    drEDI("FldDTLID") = dr("FldDTLID").ToString
    '                    drEDI("Fld_ID") = dr("Fld_id").ToString
    '                    drEDI("Fld_Value") = txt.Text
    '                    drEDI("numModifiedBy") = Session("UserContactID")
    '                    drEDI("bintModifiedDate") = Date.Today.ToString("MM/dd/yyyy")

    '                    dtEDIFields.Rows.Add(drEDI)

    '                    drEDI.AcceptChanges()
    '                    dsEDICF.AcceptChanges()

    '                    Dim txtFldValue As TextBox = CType(tblEdiFields.FindControl("txtEDI" + dr("Fld_id").ToString), TextBox)
    '                    If (txtFldValue.Text = "") Then

    '                        txtFldValue.Text = dr("Fld_Value").ToString
    '                    End If
    '                End If
    '            Next
    '        End If

    '    End If
    'End Sub

    Sub DisplayDynamicFlds()
        Try
            Dim itemType As Int16
            itemType = 0
            If ddlProduct.SelectedValue = "P" Then
                itemType = 4
            ElseIf ddlProduct.SelectedValue = "N" Then
                itemType = 5
            ElseIf ddlProduct.SelectedValue = "S" Then
                itemType = 1
            ElseIf chkSerializedItem.Checked = True Then
                itemType = 6
            ElseIf chkKit.Checked = True Then
                itemType = 2
            ElseIf chkAssembly.Checked = True Then
                itemType = 3
            ElseIf chkMatrix.Checked = True Then
                itemType = 7
            ElseIf chkAsset.Checked = True Then
                itemType = 8
            End If
            Dim strDate As String
            Dim bizCalendar As UserControl
            Dim _myUC_DueDate As PropertyInfo
            Dim PreviousRowID As Integer = 0
            Dim objRow As HtmlTableRow
            Dim objCell As HtmlTableCell
            Dim i, k As Integer
            Dim dtTable As DataTable

            Dim ObjCus As New CustomFields
            ObjCus.locId = 5
            ObjCus.RecordId = lngItemCode
            ObjCus.LocationType = itemType
            ObjCus.DomainID = Session("DomainID")
            dtTable = ObjCus.GetCustFlds.Tables(0)
            ViewState("CusFields") = dtTable

            If dtTable.Rows.Count > 0 Then
                'Main Detail Section
                k = 0
                objRow = New HtmlTableRow
                For i = 0 To dtTable.Rows.Count - 1
                    If dtTable.Rows(i).Item("TabId") = 0 Then
                        If k = 3 Then
                            k = 0
                            rblCustom.Rows.Add(objRow)
                            objRow = New HtmlTableRow
                        End If

                        objCell = New HtmlTableCell
                        objCell.Align = "Right"
                        objCell.Attributes.Add("class", "normal1")
                        objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " : "
                        objCell.Width = "8%"
                        objRow.Cells.Add(objCell)
                        If dtTable.Rows(i).Item("fld_type") = "TextBox" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "SelectBox" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBox" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "TextArea" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "DateField" Then
                            PreviousRowID = i
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            bizCalendar = LoadControl("../include/calandar.ascx")
                            bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                            objCell.Controls.Add(bizCalendar)
                            objRow.Cells.Add(objCell)
                        ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))

                            Dim href As New HtmlGenericControl("a")
                            href.Attributes.Add("href", "javascript:OpenItemCustomFieldLink('" & dtTable.Rows(i).Item("fld_id") & "')")
                            href.InnerHtml = "&nbsp;&nbsp;<i class='fa fa-external-link text-blue'></i>"
                            objCell.Controls.Add(href)
                            'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngItemCode, dtTable.Rows(i).Item("fld_label"))
                        ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBoxList" Then
                            objCell = New HtmlTableCell
                            objCell.Width = "20%"
                            objCell.Align = "left"
                            CreateCheckBoxList(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CCommon.ToString(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                        End If
                        k = k + 1
                    End If

                Next
                Dim intRowCount As Integer = 0
                If k <> 3 Then
                    For intRowCount = 0 To 2 - k
                        '' objRow = New HtmlTableRow
                        objCell = New HtmlTableCell
                        objCell.Width = "8%"
                        objRow.Cells.Add(objCell)
                        objCell = New HtmlTableCell
                        objCell.Width = "20%"
                        objRow.Cells.Add(objCell)
                        objCell.Align = "left"
                    Next
                End If
                rblCustom.Rows.Add(objRow)

            End If
            Dim dvCusFields As DataView
            dvCusFields = dtTable.DefaultView
            dvCusFields.RowFilter = "fld_type='DateField'"
            Dim iViewCount As Integer
            For iViewCount = 0 To dvCusFields.Count - 1
                If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                    bizCalendar = radItemTab.MultiPage.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                    Dim _myControlType As Type = bizCalendar.GetType()
                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                    strDate = dvCusFields(iViewCount).Item("Value")
                    If strDate = "0" Then strDate = ""
                    If strDate <> "" Then
                        'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                    End If
                End If
            Next

            '''' Display EDI Custom Fields''''
            BindEDICFValues()
            tblCustomEDIFields.Controls.Clear()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            Dim dsEdiFields As DataSet = objCommon.GetEDICustomFields(20, lngItemCode)

            Dim objEDIRow As HtmlTableRow
            Dim obEDICell As HtmlTableCell
            Dim j, h As Integer

            If (dsEdiFields IsNot Nothing And dsEdiFields.Tables.Count > 0) Then
                Dim dtEDI As DataTable = dsEdiFields.Tables(0)
                If (dtEDI.Rows.Count > 0) Then
                    h = 0
                    objEDIRow = New HtmlTableRow

                    For i = 0 To dtEDI.Rows.Count - 1
                        ' For Each dr As DataRow In dtEDI.Rows
                        If (CCommon.ToInteger(dtEDI.Rows(i).Item("Grp_id")) = 20) Then
                            If h = 3 Then
                                h = 0
                                tblCustomEDIFields.Rows.Add(objEDIRow)
                                objEDIRow = New HtmlTableRow
                            End If

                            obEDICell = New HtmlTableCell
                            obEDICell.Align = "Right"
                            obEDICell.Attributes.Add("class", "normal1")
                            obEDICell.InnerText = dtEDI.Rows(i).Item("Fld_label").ToString & " : "
                            obEDICell.Width = "8%"
                            objEDIRow.Cells.Add(obEDICell)

                            obEDICell = New HtmlTableCell
                            obEDICell.Width = "20%"
                            obEDICell.Align = "left"

                            Dim txtEDI As New TextBox
                            txtEDI.CssClass = "signup"
                            txtEDI.ID = "txtCustEDI" + dtEDI.Rows(i).Item("Fld_id").ToString
                            txtEDI.Width = Unit.Pixel(180)
                            obEDICell.Controls.Add(txtEDI)
                            objEDIRow.Cells.Add(obEDICell)

                            h = h + 1

                            tblCustomEDIFields.Rows.Add(objEDIRow)

                            Dim dtEDIFields As DataTable
                            dtEDIFields = dsEDICF.Tables(0)

                            Dim drEDI As DataRow
                            drEDI = dtEDIFields.NewRow

                            drEDI("FldDTLID") = dtEDI.Rows(i).Item("FldDTLID").ToString
                            drEDI("Fld_ID") = dtEDI.Rows(i).Item("Fld_id").ToString
                            drEDI("Fld_Value") = txtEDI.Text
                            drEDI("numModifiedBy") = Session("UserContactID")
                            drEDI("bintModifiedDate") = Date.Today.ToString("MM/dd/yyyy")

                            dtEDIFields.Rows.Add(drEDI)

                            drEDI.AcceptChanges()
                            dsEDICF.AcceptChanges()

                            Dim txtFldValue As TextBox = CType(tblCustomEDIFields.FindControl("txtCustEDI" + dtEDI.Rows(i).Item("Fld_id").ToString), TextBox)
                            If (txtFldValue.Text = "") Then
                                txtFldValue.Text = dtEDI.Rows(i).Item("Fld_Value").ToString
                            End If

                        End If
                    Next
                    Dim intEDIRowCount As Integer = 0
                    If h <> 3 Then
                        For intEDIRowCount = 0 To 2 - h
                            '' objRow = New HtmlTableRow
                            obEDICell = New HtmlTableCell
                            obEDICell.Width = "8%"
                            objEDIRow.Cells.Add(obEDICell)
                            obEDICell = New HtmlTableCell
                            obEDICell.Width = "20%"
                            objEDIRow.Cells.Add(obEDICell)
                            obEDICell.Align = "left"
                        Next
                    End If

                End If

            End If

            '''' Display EDI Custom Fields''''

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCusField()
        Try
            If Not ViewState("CusFields") Is Nothing Then
                Dim dtTable As DataTable
                Dim i As Integer
                dtTable = ViewState("CusFields")
                For i = 0 To dtTable.Rows.Count - 1
                    If dtTable.Rows(i).Item("fld_type") = "TextBox" Or dtTable.Rows(i).Item("fld_type") = "Link" Then
                        Dim txt As TextBox
                        txt = radItemTab.MultiPage.FindControl(dtTable.Rows(i).Item("fld_id"))
                        If Not txt Is Nothing Then
                            dtTable.Rows(i).Item("Value") = txt.Text
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "SelectBox" Then
                        Dim ddl As DropDownList
                        ddl = radItemTab.MultiPage.FindControl(dtTable.Rows(i).Item("fld_id"))
                        If Not ddl Is Nothing Then
                            dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBox" Then
                        Dim chk As CheckBox
                        chk = radItemTab.MultiPage.FindControl(dtTable.Rows(i).Item("fld_id"))
                        If Not chk Is Nothing Then
                            If chk.Checked = True Then
                                dtTable.Rows(i).Item("Value") = "1"
                            Else : dtTable.Rows(i).Item("Value") = "0"
                            End If
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "TextArea" Then
                        Dim txt As TextBox
                        txt = radItemTab.MultiPage.FindControl(dtTable.Rows(i).Item("fld_id"))
                        If Not txt Is Nothing Then
                            dtTable.Rows(i).Item("Value") = txt.Text
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "DateField" Then
                        Dim BizCalendar As UserControl
                        BizCalendar = radItemTab.MultiPage.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))
                        If Not BizCalendar Is Nothing Then
                            Dim strDueDate As String
                            Dim _myControlType As Type = BizCalendar.GetType()
                            Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                            strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                            If strDueDate <> "" Then
                                dtTable.Rows(i).Item("Value") = strDueDate
                            Else : dtTable.Rows(i).Item("Value") = ""
                            End If
                        End If
                    ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBoxList" Then
                        Dim chkbl As CheckBoxList
                        chkbl = CType(radItemTab.MultiPage.FindControl(dtTable.Rows(i).Item("fld_id")), CheckBoxList)
                        If Not chkbl Is Nothing Then
                            Dim selectedValue As String = ""

                            For Each listItem As ListItem In chkbl.Items
                                If listItem.Selected Then
                                    selectedValue = selectedValue & CCommon.ToString(IIf(String.IsNullOrEmpty(selectedValue), listItem.Value, "," & listItem.Value))
                                End If
                            Next

                            dtTable.Rows(i).Item("Value") = selectedValue
                        End If

                    End If
                Next

                Dim ds As New DataSet
                Dim strdetails As String
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable.Copy)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim ObjCusfld As New CustomFields
                ObjCusfld.strDetails = strdetails
                ObjCusfld.locId = 5
                ObjCusfld.RecordId = lngItemCode
                ObjCusfld.SaveCustomFldsByRecId()
            End If

            SaveEDIFields()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
        Try
            objItems.ItemCode = lngItemCode
            objItems.DomainID = Session("DomainID")
            'get item images data
            Dim dtItemImages As DataTable
            dtItemImages = objItems.ImageItemDetails()

            objItems.DeleteItems()

            'Delete Web API XML file
            Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & lngItemCode.ToString() & ".xml"
            If File.Exists(FilePath) Then
                File.Delete(FilePath)
            End If

            'Delete Images
            If dtItemImages.Rows.Count > 0 Then
                For Each row As DataRow In dtItemImages.Rows
                    If row("vcPathForImage") <> "" Then
                        If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage")) Then
                            File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage"))
                        End If
                    End If
                    If row("vcPathForTImage") <> "" Then
                        If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage")) Then
                            File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage"))
                        End If
                    End If
                Next
            End If


            If lngAssetCode > 0 Then
                Response.Redirect("../Items/frmAssetList.aspx?Page=Asset Items&ItemGroup=0&Asset=1", False)
            Else
                Response.Redirect("../Items/frmItemList.aspx", False)
            End If

        Catch ex As Exception
            If ex.Message.Contains("OpportunityItems_Depend") Then
                ShowMessage("You can't delete an item that belongs to a transaction (Sales or Purchase Opportunity or Orders) or is a child of a Kit or a Bill Of Material (BOM) within an Assembly.")
            ElseIf ex.Message.Contains("OpportunityKitItems_Depend") Then
                ShowMessage("You can't delete an item that belongs to a transaction (Sales or Purchase Opportunity or Orders) or is a child of a Kit or a Bill Of Material (BOM) within an Assembly.")
            ElseIf ex.Message.Contains("KitItems_Depend") Then
                ShowMessage("You can't delete an item that belongs to a transaction (Sales or Purchase Opportunity or Orders) or is a child of a Kit or a Bill Of Material (BOM) within an Assembly.")
            ElseIf ex.Message.Contains("WorkOrder_Depend") Then
                ShowMessage("You can't delete an item that belongs to a workorder(s).")
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub populateDataSet()
        Try
            Dim objItemWarehouse As New ItemWarehouse
            objItemWarehouse.DomainID = CCommon.ToLong(Session("DomainID"))
            objItemWarehouse.ItemCode = lngItemCode
            objItemWarehouse.WarehouseID = CCommon.ToLong(ddlWareHouse.SelectedValue)
            objItemWarehouse.InternalLocationID = CCommon.ToLong(radcmbWarehouseLocation.SelectedValue)
            dtWarehouses = objItemWarehouse.GetWarehouses()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim i As Integer = 0

    Private Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        Try
            ProductTypeChange()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ProductTypeChange()
        Try

            lblManufacturerName.Text = "Manufacturer"
            If lngItemCode = 0 Then
                chkAllowDropShip.Visible = False
            End If
            Dim listSelectedUsers As List(Of String)
            listSelectedUsers = CCommon.ToString(Session("vcElectiveItemFields")).Split(",").ToList()

            If listSelectedUsers.Contains("1") Then
                tdModelIdLabel.Visible = True
                tdModelId.Visible = True
            Else
                tdModelIdLabel.Visible = False
                tdModelId.Visible = False
            End If

            If listSelectedUsers.Contains("2") Then
                tdSKU1.Visible = True
                txtSKU.Visible = True
            Else
                tdSKU1.Visible = False
                txtSKU.Visible = False
            End If

            If listSelectedUsers.Contains("3") Then
                tdUPCLabel.Visible = True
                tdUPC.Visible = True

                tdUPCV2Label.Visible = True
                tdUPCV2.Visible = True
            Else
                tdUPCLabel.Visible = False
                tdUPC.Visible = False

                tdUPCV2Label.Visible = False
                tdUPCV2.Visible = False
            End If

            If listSelectedUsers.Contains("4") Then
                tdASIN.Visible = True
                tdASINLabel.Visible = True
            Else
                tdASIN.Visible = False
                tdASINLabel.Visible = False
            End If

            If listSelectedUsers.Contains("6") Then
                tdChildMemberShipLabel.Visible = True
                tdChildMemberShip.Visible = True
            Else
                tdChildMemberShipLabel.Visible = False
                tdChildMemberShip.Visible = False
            End If

            If listSelectedUsers.Contains("7") Then
                tdItemClassLabel.Visible = True
                tdItemClass.Visible = True
            Else
                tdItemClassLabel.Visible = False
                tdItemClass.Visible = False
            End If
            chkTimeContractFromSalesOrder.Visible = False
            lblTimeContract.Visible = False
            If ddlProduct.SelectedValue = "N" Then
                tdAssembly.Visible = True
                lblUPC.Visible = True
                txtUPC.Visible = True


                tdUPCV2Label.Visible = True
                tdUPCV2.Visible = True
                gvInventory.Visible = False

                trProducts1.Visible = False
                txtLabourCost.Visible = False
                trProducts2.Visible = False
                txtAverageCost.Visible = False

                tdlblCogs1.Visible = False
                tdddlCogs1.Visible = False
                tdAsset1.Style.Add("display", "none")
                tdddlAsset1.Style.Add("display", "none")
                lblAverageCost.Visible = False
                txtAverageCost.Visible = False

                chkFreeShipping.Visible = False
                lblAccountName.Text = "Account"
                radItemTab.FindTabByValue("Inventory").Visible = False

                chkSerializedItem.Visible = False
                chkSerializedItem.Checked = False

                chkLotNo.Visible = False
                chkLotNo.Checked = False

                tdItemGroupLabel.Visible = False
                tdItemGroup.Visible = False

                tdASINLabel.Visible = False
                tdASIN.Visible = False

                chkVirtual.Visible = False

                If CCommon.ToBool(Session("EnableNonInventoryItemExpense")) Then
                    tdlblCogs1.Visible = True
                    tdddlCogs1.Visible = True
                End If

                chkExpense.Visible = False
            ElseIf ddlProduct.SelectedValue = "S" Then
                ddlContainer.Visible = False
                lblContainer.Visible = False

                chkTimeContractFromSalesOrder.Visible = True
                lblTimeContract.Visible = True
                txtNoOfItem.Visible = False
                lblManufacturerName.Text = "Service Provider"
                'trSKU.Visible = False
                tdAssembly.Visible = False
                tdSKU1.Visible = False
                txtSKU.Visible = False
                gvInventory.Visible = False
                chkExpense.Visible = True
                tdAsset1.Style.Add("display", "none")
                tdddlAsset1.Style.Add("display", "none")
                tdlblCogs1.Visible = False
                tdddlCogs1.Visible = False
                lblAverageCost.Visible = False
                txtAverageCost.Visible = False
                'lblUPC.Visible = False
                'txtUPC.Visible = False
                'trProducts.Visible = False
                trProducts1.Visible = False
                txtLabourCost.Visible = False
                trProducts2.Visible = False
                txtAverageCost.Visible = False
                lblUPC.Visible = False
                txtUPC.Visible = False

                tdUPCV2Label.Visible = False
                tdUPCV2.Visible = False
                'trDimen.Visible = False
                trDimen1.Visible = True
                txtHeight.Visible = True
                trDimen2.Visible = True
                txtLength.Visible = True
                trDimen3.Visible = True
                txtWidth.Visible = True
                tdWeight3.Visible = False
                tdWeight2.Visible = False
                chkFreeShipping.Visible = False

                tdModelIdLabel.Visible = False
                tdModelId.Visible = False
                tdASINLabel.Visible = False
                tdASIN.Visible = False
                td1.Visible = False
                td2.Visible = False
                chkMatrix.Visible = False
                chkVirtual.Visible = False
                lblAvgCostToolTip.Visible = False
                tdItemGroupLabel.Visible = False
                tdItemGroup.Visible = False

                lblAccountName.Text = "Account"
                radItemTab.FindTabByValue("Inventory").Visible = False

                chkSerializedItem.Visible = False
                chkSerializedItem.Checked = False

                chkLotNo.Visible = False
                chkLotNo.Checked = False
                If CCommon.ToBool(Session("EnableNonInventoryItemExpense")) Then
                    tdlblCogs1.Visible = True
                    tdddlCogs1.Visible = True
                End If

                chkAllowDropShip.Visible = False

                Dim dtChartAcntDetails As DataTable
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = "0102"
                dtChartAcntDetails = objCOA.GetParentCategory()
                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlIncomeAccount.Items.Add(item)
                Next

            Else
                tdAssembly.Visible = True
                tdSKU1.Visible = True
                txtSKU.Visible = True
                chkExpense.Visible = False
                tdAsset1.Style.Add("display", "")
                tdddlAsset1.Style.Add("display", "")
                tdlblCogs1.Visible = True
                tdddlCogs1.Visible = True
                lblAverageCost.Visible = True
                txtAverageCost.Visible = True
                lblUPC.Visible = True
                txtUPC.Visible = True
                'trProducts.Visible = True
                trProducts1.Visible = True
                txtLabourCost.Visible = True
                trProducts2.Visible = True
                txtAverageCost.Visible = True
                lblUPC.Visible = True
                txtUPC.Visible = True

                tdUPCV2Label.Visible = True
                tdUPCV2.Visible = True
                'trDimen.Visible = True
                trDimen1.Visible = True
                txtHeight.Visible = True
                trDimen2.Visible = True
                txtLength.Visible = True
                trDimen3.Visible = True
                txtWidth.Visible = True
                tdWeight3.Visible = True
                tdWeight2.Visible = True

                'End If
                chkFreeShipping.Visible = True
                lblAccountName.Text = "Income Account"
                radItemTab.FindTabByValue("Inventory").Visible = True
                gvInventory.Rebind()
            End If
            If lngAssetCode <> 0 Then
                radItemTab.FindTabByValue("Inventory").Visible = False
                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    'Business Portal User
                    radItemTab.FindTabByValue("Vendor").Visible = False
                Else
                    radItemTab.FindTabByValue("Vendor").Visible = True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If radCmbCompany.SelectedValue <> "" Then
                Dim objItems As New CItems
                objItems.DivisionID = radCmbCompany.SelectedValue 'ddlCompany.SelectedItem.Value
                objItems.DomainID = Session("DomainID")
                objItems.ItemCode = lngItemCode 'GetQueryStringVal( "ItemCode")
                objItems.AddVendor()

                If dgVendor.Items.Count = 0 Then
                    Session("VendorID") = CCommon.ToLong(radCmbCompany.SelectedValue)
                End If
            End If
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim dtVendors As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            dtVendors = objItems.GetVendors
            dgVendor.DataSource = dtVendors
            dgVendor.DataBind()

            Dim dtVendorsWareHouse As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.byteMode = 1
            dtVendorsWareHouse = objItems.GetVendorsWareHouse

            gvVendorWareHouse.DataSource = dtVendorsWareHouse
            gvVendorWareHouse.DataBind()

            Dim dtMaintenanceRepair As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.ClientZoneOffsetTime = HttpContext.Current.Session("ClientMachineUTCTimeOffset")
            dtMaintenanceRepair = objItems.GetItemMaintenanceRepair

            gvMaintenanceRepair.DataSource = dtMaintenanceRepair
            gvMaintenanceRepair.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgVendor_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVendor.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                If DirectCast(e.Item.FindControl("hdnIsPrimaryVendor"), HiddenField).Value = "1" AndAlso
                    DirectCast(e.Item.FindControl("hdnUsedInAssembly"), HiddenField).Value = "1" Then
                    ShowMessage("You can not delete primary window because item is used in assembly.")
                Else
                    Dim objItems As New CItems
                    objItems.VendorID = e.Item.Cells(0).Text
                    objItems.ItemCode = lngItemCode
                    objItems.DomainID = Session("DomainID")
                    If objItems.DeleteVendors = False Then
                        ShowMessage("Dependent Records Exists. Cannot Be deleted")
                    Else : BindGrid()
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub dgVendor_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVendor.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                Dim rad As RadioButton
                Dim lbl As Label
                lbl = e.Item.FindControl("lblvendorID")
                rad = e.Item.FindControl("radPreffered")
                rad.Attributes.Add("onclick", "return CheckRadio('" & rad.ClientID & "','" & lblAsset.ClientID & "')")

                If Session("VendorId") = lbl.Text Then
                    rad.Checked = True
                    DirectCast(e.Item.FindControl("hdnIsPrimaryVendor"), HiddenField).Value = "1"

                    'USER CAN NOT DELETE PRIMARY VENDOR IF ITEM IS USED IN ASSEMBLY
                    If CCommon.ToBool(DirectCast(e.Item.DataItem, DataRowView).Row("bitUsedInAssembly")) Then
                        btnDelete.Attributes.Add("onclick", "return DeletePrimaryVendor()")
                        DirectCast(e.Item.FindControl("hdnUsedInAssembly"), HiddenField).Value = "1"
                    Else
                        DirectCast(e.Item.FindControl("hdnUsedInAssembly"), HiddenField).Value = "0"
                    End If
                Else
                    DirectCast(e.Item.FindControl("hdnIsPrimaryVendor"), HiddenField).Value = "0"
                End If

                If Not e.Item.FindControl("txtCost") Is Nothing Then
                    CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(DirectCast(e.Item.DataItem, DataRowView)("monCost")), DirectCast(e.Item.FindControl("txtCost"), TextBox))
                End If

                If Not e.Item.FindControl("txtMinQty") Is Nothing Then
                    CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(DirectCast(e.Item.DataItem, DataRowView)("intMinQty")), DirectCast(e.Item.FindControl("txtMinQty"), TextBox))
                End If
            ElseIf e.Item.ItemType = ListItemType.Header Then
                CType(e.Item.FindControl("lblCostUOM"), Label).Text = " / " & ddPurchaseUOM.SelectedItem.Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAddChilItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddChilItem.Click
        Try
            If CCommon.ToLong(hdnSelectedChildItem.Value) > 0 Then
                If lngItemCode <> CCommon.ToLong(hdnSelectedChildItem.Value) Then
                    Dim listSelectedChildItemsForAdd As New List(Of Long)

                    For Each dgKitGridItem As TreeListDataItem In rtlChildItems.Items
                        If CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Checked = True And CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Visible = True Then
                            If listSelectedChildItemsForAdd.Where(Function(x) x = CCommon.ToLong(dgKitGridItem("numItemCode").Text)).Count = 0 Then
                                listSelectedChildItemsForAdd.Add(CCommon.ToLong(dgKitGridItem("numItemCode").Text))
                            End If
                        End If
                    Next

                    If listSelectedChildItemsForAdd.Count = 0 Then
                        listSelectedChildItemsForAdd.Add(lngItemCode)
                    End If

                    Using objTrasaction As New Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                        For Each lngParentItem As Long In listSelectedChildItemsForAdd
                            objItems.KitID = lngParentItem
                            objItems.ItemCode = CCommon.ToLong(hdnSelectedChildItem.Value)
                            objItems.NoofItemsReqForKit = 1
                            objItems.byteMode = 0
                            objItems.ItemDetailID = 0
                            objItems.ManageKitParentsForChildItem()
                        Next

                        If CCommon.ToLong(hdnSelectedChildItemPrimaryVendor.Value) > 0 Then
                            Dim objItems As New CItems
                            objItems.DivisionID = CCommon.ToLong(hdnSelectedChildItemPrimaryVendor.Value)
                            objItems.DomainID = Session("DomainID")
                            objItems.ItemCode = CCommon.ToLong(hdnSelectedChildItem.Value)
                            objItems.IsPrimaryVendor = True
                            objItems.PartNo = txtVendorPart.Text
                            objItems.monCost = CCommon.ToDecimal(txtVendorCost.Text)
                            objItems.minQty = CCommon.ToInteger(txtVendorMinOrderQuantity.Text)
                            objItems.AddVendor()
                        End If

                        objTrasaction.Complete()
                    End Using

                    bindItemsGrid()
                End If
            End If
        Catch ex As Exception
            If ex.Message.Contains("CAN_NOT_ADD_PARENT_AS_CHILD") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ChildIsParent", "alert('You can't add item as its own child.');", True)
            ElseIf ex.Message.Contains("CAN_NOT_ADD_KIT_AS_ASSEMBLY_CHILD") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CANNOTADDKITCHILD", "alert('You can't add kit item as child of assembly item.');", True)
            ElseIf ex.Message.Contains("CAN_NOT_ADD_KIT_AS_KIT_IS_ALREADY_CHILD_OTHER_ITEM") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "MultilevelKit", "alert('You can't add kit item as child of another kit item which already child of another kit item.');", True)
            ElseIf ex.Message.Contains("CHILD_ITEM_ALREADY_ADDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ChildItemAdded", "alert('Selected item is already added to Kit/Assembly');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            For Each dgKitGridItem As TreeListDataItem In rtlChildItems.Items
                If CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Checked = True And CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Visible = True Then
                    objItems.ItemDetailID = CCommon.ToLong(dgKitGridItem("numItemDetailID").Text)
                    objItems.byteMode = 2
                    objItems.ManageKitParentsForChildItem()
                End If
            Next

            bindItemsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            SB_PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub SB_PageRedirect()
        Try
            Select Case GetQueryStringVal("frm")
                Case "AdvSearch"
                    Response.Redirect("../admin/frmAdvSerInvItemsRes.aspx")
                Case "InvAdjust"
                    Response.Redirect("../Items/frmInventoryAdjustmentInBatch.aspx")
                Case "frmAccounts"
                    Dim lngDivId As Long
                    If GetQueryStringVal("numDivisionID") <> "" Then
                        lngDivId = GetQueryStringVal("numDivisionID")
                    Else
                        lngDivId = IIf(radCmbOrganization.SelectedValue = "", 0, radCmbOrganization.SelectedValue)
                    End If
                    Response.Redirect("../account/frmAccounts.aspx?DivId=" & lngDivId & "&SI=8", False)
                Case Else
                    If radItemTab.FindTabByValue("AssetDetails").Visible = False Then
                        Response.Redirect("../Items/frmItemList.aspx?Page=" & GetQueryStringVal("frm"), False)
                    Else
                        Response.Redirect("../Items/frmAssetList.aspx?Page=Asset Items&ItemGroup=0&Asset=1", False)
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub radKitWareHouse_ItemDatabound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs)
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString()
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub optAppreciation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAppreciation.CheckedChanged
    '    If optAppreciation.Checked = True Then
    '        optDepreciation.Checked = False
    '        txtDepreciation.Enabled = False
    '        txtAppreciation.Enabled = True
    '    End If

    'End Sub

    'Private Sub optDepreciation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDepreciation.CheckedChanged
    '    If optDepreciation.Checked = True Then
    '        optAppreciation.Checked = False
    '        optAppreciation.Enabled = False
    '        txtDepreciation.Enabled = True
    '    End If

    'End Sub

    Private Sub radCmbOrganization_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbOrganization.SelectedIndexChanged
        Try
            OrgSelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub OrgSelectedIndexChanged()
        btnAddAssetSerial.Attributes.Add("onclick", "return SerialAssets(" & lngItemCode & "," & radCmbOrganization.SelectedValue & ")")
    End Sub

    Protected Sub DropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        Try
            Dim lst As Telerik.Web.UI.RadComboBox
            Dim cell As TableCell
            Dim item As TreeListDataItem

            lst = sender
            cell = lst.Parent
            item = cell.Parent

            Dim objItems As New CItems
            If Not (item("numItemCode").Text = "") Then
                objItems.ItemCode = item("numItemCode").Text
                objItems.NoofUnits = 1
                objItems.OppId = 0
                objItems.DivisionID = 0
                objItems.DomainID = Session("DomainID")
                objItems.byteMode = 1
                objItems.Amount = 0
                If Not lst.SelectedValue = "" Then
                    objItems.WareHouseItemID = lst.SelectedValue
                Else
                    objItems.WareHouseItemID = Nothing
                End If
                Dim dtItemPricemgmt As DataTable

                If objItems.byteMode = 1 Then
                    Dim ds As DataSet
                    ds = objItems.GetPriceManagementList1
                    dtItemPricemgmt = ds.Tables(0)
                    If dtItemPricemgmt.Rows.Count = 0 Then
                        dtItemPricemgmt = ds.Tables(2)
                        dtItemPricemgmt.Merge(ds.Tables(1))
                    Else : dtItemPricemgmt.Merge(ds.Tables(1))
                    End If
                Else : dtItemPricemgmt = objItems.GetPriceManagementList1.Tables(0)
                End If
                If dtItemPricemgmt.Rows.Count > 0 Then
                    item("UnitPrice").Text = String.Format("{0:0.00}", dtItemPricemgmt.Rows(0)("ListPrice")) & "/Unit"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub gvInventory_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvInventory.NeedDataSource
        Try
            gvInventoryColumnSetting()
            gvInventory.DataSource = dtWarehouses

            If Not dtWarehouses Is Nothing AndAlso dtWarehouses.Rows.Count > 0 Then
                lblMaxBuildQty.Text = CCommon.ToString(dtWarehouses.Compute("SUM(numBuildableQty)", ""))
            Else
                lblMaxBuildQty.Text = 0
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub



    Sub gvInventoryColumnSetting()
        Try
            If GetQueryStringVal("AssetCode") <> "" Then
                lngAssetCode = CCommon.ToLong(GetQueryStringVal("AssetCode"))
                If lngAssetCode > 0 Then lngItemCode = lngAssetCode
                If lngAssetCode = -1 And lngItemCode = 0 Then lngItemCode = 0
                radItemTab.FindTabByValue("AssetDetails").Visible = True
                radItemTab.FindTabByValue("ItemDetails").Visible = False
                radItemTab.FindTabByValue("Inventory").Visible = False
                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    'Business Portal User
                    radItemTab.FindTabByValue("Vendor").Visible = False
                Else
                    radItemTab.FindTabByValue("Vendor").Visible = True
                End If
            Else
                radItemTab.FindTabByValue("AssetDetails").Visible = False
            End If

            populateDataSet()

            If CCommon.ToLong(ddlItemGroup.SelectedValue) > 0 Then
                gvInventory.Columns.FindByUniqueNameSafe("SKU").Visible = False
                gvInventory.Columns.FindByUniqueNameSafe("BarCode").Visible = False
                gvInventory.Columns.FindByUniqueNameSafe("Attribute").Visible = True
                'gvInventory.Columns.FindByUniqueNameSafe("Price").Visible = True

            Else
                gvInventory.Columns.FindByUniqueNameSafe("SKU").Visible = False
                gvInventory.Columns.FindByUniqueNameSafe("BarCode").Visible = False
                gvInventory.Columns.FindByUniqueNameSafe("Attribute").Visible = False
            End If

            If chkSerializedItem.Checked Or chkKit.Checked Or chkLotNo.Checked Then
                gvInventory.Columns.FindByUniqueNameSafe("Qty to Adjust").Visible = False
                gvInventory.Columns.FindByUniqueNameSafe("Counted").Visible = False
                'gvInventory.Columns(0).Visible = False
            Else
                gvInventory.Columns.FindByUniqueNameSafe("Qty to Adjust").Visible = True
                gvInventory.Columns.FindByUniqueNameSafe("Counted").Visible = True
                'gvInventory.Columns(0).Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub button_Click_Collapse_Expand(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Button).CssClass = IIf((CType(sender, Button).CssClass = "rgExpand"), "rgCollapse", "rgExpand")
    End Sub

    Sub bindItemDependencies()
        Try
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.ClientZoneOffsetTime = HttpContext.Current.Session("ClientMachineUTCTimeOffset")
            objItems.byteMode = 0
            objItems.WarehouseID = radcmbLocation.SelectedValue
            Dim dtItemDependencies As DataTable
            dtItemDependencies = objItems.GetItemOrderDependencies(radIDOppType.SelectedValue)
            Session("dtItemDependencies") = dtItemDependencies

            gvItemDependencies.Columns(0).FooterText = "Total"
            gvItemDependencies.Columns(7).FooterText = dtItemDependencies.AsEnumerable().Sum(Function(row) row.Field(Of Double)("numOnOrder"))
            gvItemDependencies.Columns(8).FooterText = dtItemDependencies.AsEnumerable().Sum(Function(row) row.Field(Of Double)("numAllocation"))
            gvItemDependencies.Columns(9).FooterText = dtItemDependencies.AsEnumerable().Sum(Function(row) row.Field(Of Double)("numQtyReleasedReceived"))
            gvItemDependencies.DataSource = dtItemDependencies
            gvItemDependencies.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvItemDependencies.PageIndex = e.NewPageIndex
        Me.bindItemDependencies()
    End Sub

    Protected Sub gvItemDependencies_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        Dim sortExpression As String = e.SortExpression
        If GridViewSortDirection = SortDirection.Ascending Then
            GridViewSortDirection = SortDirection.Descending
            SortGridView(sortExpression, DESCENDING)
        Else
            GridViewSortDirection = SortDirection.Ascending
            SortGridView(sortExpression, ASCENDING)
        End If
    End Sub

    Private Sub SortGridView(ByVal sortExpression As String, ByVal direction As String)
        Dim dt As DataTable = Session("dtItemDependencies")
        Dim dv As DataView = New DataView(dt)
        dv.Sort = sortExpression & direction
        gvItemDependencies.DataSource = dv
        gvItemDependencies.DataBind()
    End Sub

    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"

    Public Property GridViewSortDirection As SortDirection
        Get
            If ViewState("sortDirection") Is Nothing Then ViewState("sortDirection") = SortDirection.Ascending
            Return CType(ViewState("sortDirection"), SortDirection)
        End Get

        Set(ByVal value As SortDirection)
            ViewState("sortDirection") = value
        End Set
    End Property

    Private Sub radItemTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radItemTab.TabClick
        Try
            If radItemTab.SelectedIndex = 7 Then
                LoadLocationsAndInventory()
                bindItemDependencies()
                ProductTypeChange()
            End If

            PersistTable.Clear()
            PersistTable.Add(PersistKey.SelectedTab, radItemTab.SelectedIndex)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Function GetSelectedAPI()
        Try
            Dim strAPI As String = ""
            If radWebAPIList.CheckedItems().Count > 0 Then
                For Each item As RadComboBoxItem In radWebAPIList.CheckedItems
                    If item.Checked = True Then
                        strAPI = strAPI + item.Value + ","
                    End If
                Next
            End If
            Return strAPI.TrimEnd(",")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetSelectedCategory()
        Try
            Dim strCategories As String = ""
            If rtvCategory.CheckedNodes.Count > 0 Then
                'rtvParentCategory.SelectedNodes()
                For Each item As RadTreeNode In rtvCategory.CheckedNodes
                    If item.Checked = True Then
                        strCategories = strCategories + item.Value + ","
                    End If
                Next
            End If
            strCategories = strCategories.TrimEnd(",")
            Return strCategories
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub radIDOppType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles radIDOppType.SelectedIndexChanged
        Try
            bindItemDependencies()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbLocation_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles radcmbLocation.SelectedIndexChanged
        Try
            bindItemDependencies()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub btnCloneItem_Click(sender As Object, e As EventArgs) Handles btnCloneItem.Click
        Try
            'Create Clone Item 
            If lngItemCode = 0 Then Exit Sub

            If objItems Is Nothing Then objItems = New CItems
            objItems.ItemCode = lngItemCode
            objItems.DomainID = Session("DomainID")
            Dim newItemCode As Long = objItems.CreateCloneItem()

            If newItemCode > 0 Then
                Dim objItem As New CItems
                objItem.ItemCode = Me.lngItemCode
                objItem.DomainID = Session("DomainId")

                Dim dtImage As DataTable
                dtImage = objItem.ImageItemDetails()

                If Not dtImage Is Nothing AndAlso dtImage.Rows.Count > 0 Then
                    For Each dr As DataRow In dtImage.Rows
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcPathForImage"))) Then
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & dr("vcPathForImage")) Then
                                Try
                                    Dim newFileName As String = CCommon.ToString(dr("vcPathForImage")).Substring(0, CCommon.ToString(dr("vcPathForImage")).IndexOf("."))
                                    Dim extension As String = CCommon.ToString(dr("vcPathForImage")).Substring(CCommon.ToString(dr("vcPathForImage")).IndexOf("."), CCommon.ToString(dr("vcPathForImage")).Length - CCommon.ToString(dr("vcPathForImage")).IndexOf("."))

                                    File.Copy(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & dr("vcPathForImage"), CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & newFileName & "_" & newItemCode & extension, True)
                                Catch ex As Exception
                                    'DO NOT THROW ERROR
                                End Try
                            End If
                        End If

                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcPathForTImage"))) Then
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & dr("vcPathForTImage")) Then
                                Try
                                    Dim newFileName As String = CCommon.ToString(dr("vcPathForTImage")).Substring(0, CCommon.ToString(dr("vcPathForTImage")).IndexOf("."))
                                    Dim extension As String = CCommon.ToString(dr("vcPathForTImage")).Substring(CCommon.ToString(dr("vcPathForTImage")).IndexOf("."), CCommon.ToString(dr("vcPathForTImage")).Length - CCommon.ToString(dr("vcPathForTImage")).IndexOf("."))

                                    File.Copy(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & dr("vcPathForTImage"), CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & newFileName & "_" & newItemCode & extension, True)
                                Catch ex As Exception
                                    'DO NOT THROW ERROR
                                End Try
                            End If
                        End If
                    Next
                End If

                objItems.ItemCode = newItemCode
                objItems.byteMode = 1
                objItems.str = RadEditor1.Content.Trim()
                objItems.txtShortDesc = RadEditor2.Content.Trim()
                objItems.UserCntID = Session("UserContactID")
                objItems.ManageItemExtendedDesc()

                ManageMetaTag(newItemCode)

                Response.Redirect("~/Items/frmKitDetails.aspx?ItemCode=" & newItemCode & "&frm='Item'", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub rtlChildItems_ItemCommand(sender As Object, e As TreeListCommandEventArgs) Handles rtlChildItems.ItemCommand
        Try
            'WE ARE USING CLIENT SIDE EXPAND_COLLAPSE SO CODE IS NOT REQUIRED BUT EVENT IS REQUIRED
            If e.CommandName = RadTreeList.ExpandCollapseCommandName Then

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub rtlChildItems_ItemDataBound(sender As Object, e As TreeListItemDataBoundEventArgs) Handles rtlChildItems.ItemDataBound
        Try
            If e.Item.ItemType = TreeListItemType.Item Or e.Item.ItemType = TreeListItemType.AlternatingItem Then
                Dim dataRow As DataRow = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row

                Dim btn As Button = TryCast(e.Item.FindControl("ExpandCollapseButton"), Button)
                Dim uniqueID As String = dataRow("ID")

                If btn IsNot Nothing Then
                    btn.Attributes.Add("UniqueID", uniqueID)
                    btn.Attributes.Add("onclick", "return ExpandCollapse(" + btn.ClientID + ");")
                End If

                DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).Attributes.Add("UniqueID", uniqueID)

                Dim chkUseInNewItemConfiguration As CheckBox
                chkUseInNewItemConfiguration = e.Item.FindControl("chkUseInNewItemConfiguration")

                Dim chkOrderEditable As CheckBox
                chkOrderEditable = e.Item.FindControl("chkOrderEditable")

                Dim ddlItemUOM As DropDownList
                ddlItemUOM = e.Item.FindControl("ddlItemUOM")

                Dim lblItemUOM As Label
                lblItemUOM = e.Item.FindControl("lblItemUOM")

                Dim ddlView As DropDownList
                ddlView = e.Item.FindControl("ddlView")

                Dim rcbFields As RadComboBox
                rcbFields = e.Item.FindControl("rcbFields")

                Dim chkSKU As CheckBox
                chkSKU = e.Item.FindControl("chkSKU")

                If CCommon.ToInteger(dataRow("StageLevel")) = 1 Then
                    If dataRow("ItemType").ToString() = "P" Then
                        If objCommon Is Nothing Then objCommon = New CCommon

                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.UnitId = CInt(dataRow("numUOMId").ToString())
                        Dim dtUnit As DataTable
                        dtUnit = objCommon.GetUOMConversionAllUnit()

                        With ddlItemUOM
                            .DataSource = dtUnit
                            .DataTextField = "vcUnitName"
                            .DataValueField = "UOMConversionFactor"
                            .DataBind()
                            .Items.Insert(0, "--Select One--")
                            .Items.FindByText("--Select One--").Value = "0~1"
                        End With

                        If Not ddlItemUOM.Items.FindByValue(dataRow("numIDUOMId").ToString() & "~" & dataRow("UOMConversionFactor").ToString()) Is Nothing Then
                            ddlItemUOM.Items.FindByValue(dataRow("numIDUOMId").ToString() & "~" & dataRow("UOMConversionFactor").ToString()).Selected = True
                        End If

                        If CCommon.ToBool(dataRow("bitKitParent")) Then
                            If Not chkOrderEditable Is Nothing Then
                                chkOrderEditable.Checked = CCommon.ToBool(dataRow("bitOrderEditable"))
                                chkOrderEditable.Visible = True
                            End If

                            If Not chkUseInNewItemConfiguration Is Nothing Then
                                chkUseInNewItemConfiguration.Checked = CCommon.ToBool(dataRow("bitUseInNewItemConfiguration"))
                                chkUseInNewItemConfiguration.Visible = True
                            End If

                            If Not chkSKU Is Nothing Then
                                chkSKU.Checked = CCommon.ToBool(dataRow("bitUseInDynamicSKU"))
                                chkSKU.Visible = True
                            End If

                            If Not ddlView Is Nothing Then
                                If Not ddlView.Items.FindByValue(CCommon.ToLong(dataRow("tintView"))) Is Nothing Then
                                    ddlView.Items.FindByValue(CCommon.ToLong(dataRow("tintView"))).Selected = True
                                End If

                                ddlView.Visible = True
                            End If

                            If Not rcbFields Is Nothing Then
                                rcbFields.Visible = True

                                rcbFields.Items.Add(New RadComboBoxItem("Primary Image", "0~1"))
                                rcbFields.Items.Add(New RadComboBoxItem("Item Name", "0~2"))
                                rcbFields.Items.Add(New RadComboBoxItem("SKU", "0~3"))
                                rcbFields.Items.Add(New RadComboBoxItem("Model ID", "0~4"))

                                Dim dtCustomFields As DataTable
                                Dim itemType As Int16
                                itemType = 0
                                If ddlProduct.SelectedValue = "P" Then
                                    itemType = 4
                                ElseIf ddlProduct.SelectedValue = "N" Then
                                    itemType = 5
                                ElseIf ddlProduct.SelectedValue = "S" Then
                                    itemType = 1
                                ElseIf chkSerializedItem.Checked = True Then
                                    itemType = 6
                                ElseIf chkKit.Checked = True Then
                                    itemType = 2
                                ElseIf chkAssembly.Checked = True Then
                                    itemType = 3
                                ElseIf chkMatrix.Checked = True Then
                                    itemType = 7
                                ElseIf chkAsset.Checked = True Then
                                    itemType = 8
                                End If

                                If ViewState("CusFields") Is Nothing Then
                                    Dim ObjCus As New CustomFields
                                    ObjCus.locId = 5
                                    ObjCus.RecordId = lngItemCode
                                    ObjCus.LocationType = itemType
                                    ObjCus.DomainID = Session("DomainID")
                                    dtCustomFields = ObjCus.GetCustFlds.Tables(0)
                                    ViewState("CusFields") = dtCustomFields
                                Else
                                    dtCustomFields = ViewState("CusFields")
                                End If

                                If Not dtCustomFields Is Nothing Then
                                    For Each drField In dtCustomFields.Rows
                                        rcbFields.Items.Add(New RadComboBoxItem(CCommon.ToString(drField("fld_label")), "1~" & CCommon.ToLong(drField("fld_id"))))
                                    Next
                                End If

                                Dim arrCheckedFields As String() = CCommon.ToString(dataRow("vcFields")).Split(",")

                                If Not arrCheckedFields Is Nothing AndAlso arrCheckedFields.Length > 0 Then
                                    For Each strCheckedField In arrCheckedFields
                                        If Not rcbFields.Items.FindItemByValue(strCheckedField) Is Nothing Then
                                            rcbFields.Items.FindItemByValue(strCheckedField).Checked = True
                                        End If
                                    Next
                                End If
                            End If
                        End If
                    Else
                        ddlItemUOM.Visible = False
                        lblItemUOM.Visible = True
                    End If
                Else
                    ddlItemUOM.Visible = False

                    Dim txtQuantity As TextBox
                    txtQuantity = e.Item.FindControl("txtQuantity")

                    Dim txtIDescription As TextBox
                    txtIDescription = e.Item.FindControl("txtIDescription")

                    Dim txtSequence As TextBox
                    txtSequence = e.Item.FindControl("txtSequence")

                    txtQuantity.Visible = False
                    txtIDescription.Visible = False
                    txtSequence.Visible = False

                    Dim lblQuantity As Label
                    lblQuantity = e.Item.FindControl("lblQuantity")

                    Dim lblIDescription As Label
                    lblIDescription = e.Item.FindControl("lblIDescription")

                    Dim lblSequence As Label
                    lblSequence = e.Item.FindControl("lblSequence")

                    lblItemUOM.Visible = True
                    lblQuantity.Visible = True
                    lblIDescription.Visible = True
                    lblSequence.Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlCategoryProfile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategoryProfile.SelectedIndexChanged
        Try
            GetCategoryProfileDetail()
            loadCategory()

            objItems.ItemCode = lngItemCode
            objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
            Dim dt As DataTable = objItems.ItemDetails()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Dim strListOfCategories As String() = CCommon.ToString(dt.Rows(0).Item("vcItemCategories")).Split(",")
                For k As Integer = 0 To strListOfCategories.Length - 1
                    For Each tn As RadTreeNode In rtvCategory.Nodes
                        If tn.Value = strListOfCategories(k) Then
                            tn.Checked = True
                        Else
                            FindNode(tn, strListOfCategories(k))
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String, Optional ByVal isSuccess As Boolean = False)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()

            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ChangeMessageCSS", "ChangeMessageCSS(" & If(isSuccess, 1, 0) & ")", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub chkMatrix_CheckedChanged(sender As Object, e As EventArgs) Handles chkMatrix.CheckedChanged
        Try
            If Not chkMatrix.Checked Then
                ddlItemGroup.SelectedValue = 0
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub AttributesControl(ByVal dtAttributes As DataTable, Optional ByVal isEdit As Boolean = 0, Optional ByVal dataRow As DataRow = Nothing)
        Try
            If Not dtAttributes Is Nothing AndAlso dtAttributes.Rows.Count > 0 Then
                plhItemAttributes.Controls.Clear()

                Dim table As HtmlTable
                Dim tr As HtmlTableRow
                Dim td As HtmlTableCell
                Dim dr As DataRow

                Dim i As Int32 = 0
                table = New HtmlTable
                table.Attributes.Add("id", "divAttributes")
                table.Attributes.Add("width", "100%")


                Dim tabIndex As Int32 = 20

                For Each dr In dtAttributes.Rows
                    tr = New HtmlTableRow()

                    td = New HtmlTableCell()
                    td.Attributes.Add("style", "width: 29%; text-align: right;padding-right: 5px;")
                    Dim label As New HtmlGenericControl("label")
                    label.InnerHtml = "<span style=""color:red"">* </span>" & dr("Fld_label")
                    td.Controls.Add(label)
                    tr.Controls.Add(td)

                    td = New HtmlTableCell()
                    td.Attributes.Add("style", "width: 70%;")
                    If dr("fld_type") = "SelectBox" Then
                        If CCommon.ToBool(dr("bitAutocomplete")) Then
                            Dim radcmb As RadComboBox = CreateRadComboBox(tr, td, dr("Fld_id"), CCommon.ToInteger(dr("Fld_Value")), dr("numlistid"))
                            radcmb.Enabled = chkMatrix.Enabled
                            radcmb.Width = 188

                            If Not String.IsNullOrEmpty(CCommon.ToString(dr("Fld_Value"))) Then
                                Dim objCommon As New CCommon
                                Dim vcData As String = objCommon.GetListItemValue(CCommon.ToString(dr("Fld_Value")), CCommon.ToLong(Session("DomainID")))

                                radcmb.Text = vcData
                                radcmb.SelectedValue = CCommon.ToLong(dr("Fld_Value"))
                            End If

                            AddHandler radcmb.ItemsRequested, AddressOf radcmb_ItemsRequested
                            tr.Controls.Add(td)
                        Else
                            Dim ddl As DropDownList = CreateDropdown(tr, td, dr("Fld_id"), CCommon.ToInteger(dr("Fld_Value")), dr("numlistid"))
                            ddl.Enabled = chkMatrix.Enabled
                            ddl.Width = 188
                            tr.Controls.Add(td)
                        End If
                    ElseIf dr("fld_type") = "CheckBox" Then
                        CreateChkBox(tr, td, dr("Fld_id"), CCommon.ToInteger(dr("Fld_Value")))
                        tr.Controls.Add(td)
                    End If

                    table.Controls.Add(tr)
                Next

                If table.Controls.Count > 0 Then
                    plhItemAttributes.Controls.Add(table)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub radcmb_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
        Try
            Dim radcmbSearch As RadComboBox = DirectCast(sender, RadComboBox)
            If Trim(e.Text).Length > 0 Then
                Dim itemsPerRequest As Integer = 10
                Dim itemOffset As Integer = e.NumberOfItems
                Dim endOffset As Integer = itemOffset + itemsPerRequest

                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = CCommon.ToLong(radcmbSearch.Attributes("ListID"))
                Dim dt As DataTable = objCommon.SearchListDetails(Trim(e.Text), e.NumberOfItems, itemsPerRequest)

                Try
                    If endOffset > objCommon.TotalRecords Then
                        endOffset = objCommon.TotalRecords
                    End If

                    For Each dr As DataRow In dt.Rows
                        radcmbSearch.Items.Add(New Telerik.Web.UI.RadComboBoxItem(dr("vcData").ToString(), dr("numListItemID").ToString()))
                    Next

                    If dt.Rows.Count > 0 Then
                        e.Message = [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset.ToString(), objCommon.TotalRecords.ToString())
                    Else
                        e.Message = "No matches"
                    End If
                Catch
                    e.Message = "No matches"
                End Try
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlItemGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlItemGroup.SelectedIndexChanged
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlWareHouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWareHouse.SelectedIndexChanged
        Try
            ddlWareHouse_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlWarehouse_SelectedIndexChanged()
        Try
            gvInventory.Rebind()
            radcmbWarehouseLocation.Focus()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadLocationsAndInventory()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            Dim dtWareHouse As DataTable
            dtWareHouse = objItem.GetWareHouses()
            radcmbLocation.DataSource = dtWareHouse
            radcmbLocation.DataTextField = "vcWareHouse"
            radcmbLocation.DataValueField = "numWareHouseID"
            radcmbLocation.DataBind()
            radcmbLocation.Items.Insert(0, New RadComboBoxItem("-- All --", "0"))


            objItems.ItemCode = lngItemCode
            objItems.byteMode = 1
            Dim dsInventory As DataSet = objItems.GetItemWareHouses()
            If dsInventory IsNot Nothing AndAlso dsInventory.Tables(0).Rows.Count > 0 Then
                divInventory.Visible = True
                Dim totalOnHand As Integer, totalAllocation As Integer, totalOrdered As Integer, totalOnOrder As Integer, totalBackOrder As Integer
                Dim dtInventory As DataTable = dsInventory.Tables(0)

                totalOnHand = Convert.ToInt32(dtInventory.Compute("SUM(OnHand)", String.Empty))
                lblOnHandText.Text = "(" + CCommon.GetDecimalFormat(totalOnHand) + ")"

                totalAllocation = Convert.ToInt32(dtInventory.Compute("SUM(Allocation)", String.Empty))
                lblAllocationText.Text = "(" + CCommon.GetDecimalFormat(totalAllocation) + ")"

                totalOrdered = totalOnHand + totalAllocation
                lblTotalOrdered.Text = ": " + CCommon.ToString(totalOrdered)

                totalOnOrder = Convert.ToInt32(dtInventory.Compute("SUM(OnOrder)", String.Empty))
                lblOrder.Text = "(" + CCommon.GetDecimalFormat(totalOnOrder) + ")"

                totalBackOrder = Convert.ToInt32(dtInventory.Compute("SUM(BackOrder)", String.Empty))
                lblBackOrderText.Text = "(" + CCommon.GetDecimalFormat(totalBackOrder) + ")"
            Else
                divInventory.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub btnAddInternalLoc_Click() Handles btnAddInternalLoc.Click
        Try
            If lngItemCode > 0 AndAlso CCommon.ToLong(ddlWareHouse.SelectedValue) > 0 Then
                Dim objItems As New CItems
                objItems.ItemCode = lngItemCode
                objItems.WarehouseID = ddlWareHouse.SelectedValue
                objItems.WareHouseItemID = CCommon.ToLong(hfWareHouseItemID.Value)
                objItems.WarehouseLocationID = CCommon.ToLong(radcmbWarehouseLocation.SelectedValue)
                objItems.WLocation = CCommon.ToString(radcmbWarehouseLocation.Text)
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")
                objItems.byteMode = 1

                If CCommon.ToLong(hfWareHouseItemID.Value) = 0 AndAlso objItems.IsDuplicateWarehouseLocation() Then
                    ShowMessage("Record with same external and internal location already added.")
                    Exit Sub
                End If

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    objItems.SaveWarehouseItems()
                    objTransactionScope.Complete()
                End Using

                lblAdjustmentSuccess.Text = "Record added successfully."
                lblAdjustmentSuccess.Visible = True

                Dim tempId As Long = CCommon.ToLong(hfWareHouseItemID.Value)
                radcmbWarehouseLocation.ClearSelection()
                gvInventory.Rebind()
            Else
                ShowMessage("Select warehouse.")
                ddlWareHouse.Focus()
            End If
        Catch ex As Exception
            If ex.Message.Contains("QTY_EXISTS_IN_EXTERNAL_LOCATION") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "QtyExists", "alert('You can not add an internal location until you remove all On-Hand quantity from your external location');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnCloseStockTransfer_Click(sender As Object, e As EventArgs) Handles btnCloseStockTransfer.Click
        Try
            lblTransferExc.Text = ""
            hdnTransferFromWarehouse.Value = ""
            txtQty.Visible = True
            hplSerialLot.Visible = False
            hplSerialLot.Text = "Added (0)"
            hdnSelectedSerialLot.Value = ""
            txtQty.Text = ""
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('hide');$('.modal-backdrop').remove();", True)
        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSaveStockTransfer_Click(sender As Object, e As EventArgs) Handles btnSaveStockTransfer.Click
        Try
            Dim qty As Int32 = 0
            Dim serialLot As String = ""

            If CCommon.ToBool(chkLotNo.Checked) Or CCommon.ToBool(chkSerializedItem.Checked) Then
                If Not String.IsNullOrEmpty(hdnSelectedSerialLot.Value) Then
                    Dim arrSeriaLot As String() = hdnSelectedSerialLot.Value.Split(",")

                    For Each item As String In arrSeriaLot
                        qty += CCommon.ToInteger(item.Split("-")(1))

                        If CCommon.ToBool(chkLotNo.Checked) Then
                            serialLot = IIf(String.IsNullOrEmpty(serialLot), item.Split("-")(0) & "(" & item.Split("-")(1) & ")", serialLot & "," & item.Split("-")(0) & "(" & item.Split("-")(1) & ")")
                        Else
                            serialLot = IIf(String.IsNullOrEmpty(serialLot), item.Split("-")(0), serialLot & "," & item.Split("-")(0))
                        End If
                    Next
                End If
            Else
                Int32.TryParse(txtQty.Text, qty)
            End If

            If qty > 0 AndAlso radTransferToWarehouse.SelectedValue > 0 Then
                Try
                    If (CCommon.ToBool(chkLotNo.Checked) Or CCommon.ToBool(chkSerializedItem.Checked)) AndAlso String.IsNullOrEmpty(serialLot) Then
                        ShowStockTransferError("Serial/Lot# are required.")
                        Exit Sub
                    End If

                    Dim objItem As New CItems
                    objItem.DomainID = Session("DomainID")
                    objItem.UserCntID = Session("UserContactID")
                    objItem.WareHouseItemID = CCommon.ToLong(hdnTransferFromWarehouse.Value)
                    objItem.StockTransferToWarehouseItemID = CCommon.ToLong(radTransferToWarehouse.SelectedValue)
                    objItem.NoofUnits = qty
                    objItem.SerialNo = serialLot
                    objItem.InventoryTransfer()

                    lblTransferExc.Text = ""
                    hdnSelectedSerialLot.Value = ""
                    hdnTransferFromWarehouse.Value = ""
                    txtQty.Visible = True
                    hplSerialLot.Visible = False
                    hplSerialLot.Text = "Added (0)"
                    txtQty.Text = ""
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('hide');$('.modal-backdrop').remove(); document.location.href=document.location.href;", True)
                Catch ex As Exception
                    If ex.Message.Contains("INSUFFICIENT_ONHAND_QUANTITY") Then
                        lblTransferExc.Text = "Qty to transfer is more than on hand quantity"
                    ElseIf ex.Message.Contains("INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY") Then
                        lblTransferExc.Text = "Invalid serial/lot number or lot quantity is more than available quantity."
                    Else
                        lblTransferExc.Text = ex.Message
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');$('.modal-backdrop').remove();", True)
                End Try

            Else
                lblTransferExc.Text = "Select transfer quantity and warehouse."
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');$('.modal-backdrop').remove();", True)
            End If


        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ShowStockTransferError(ByVal msg As String)
        lblTransferExc.Text = msg
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenStockTransfer", "$('[id$=divTransferStock]').modal('show');$('.modal-backdrop').remove();", True)
    End Sub



    Dim decTotalAdjustmentAmount As Decimal
    Dim Counted, OnHandQty
    Dim decAverageCost As Decimal
    Dim intDifference As Double

#Region "EDI Fields"
    Protected Sub BindEDICFValues()
        dsEDICF = New DataSet
        Dim dtEDIFields As New DataTable

        dtEDIFields.Columns.Add("FldDTLID", GetType(String))
        dtEDIFields.Columns.Add("Fld_ID", GetType(String))
        dtEDIFields.Columns.Add("Fld_Value", GetType(String))
        dtEDIFields.Columns.Add("numModifiedBy", GetType(Long))
        dtEDIFields.Columns.Add("bintModifiedDate", GetType(String))
        dsEDICF.Tables.Add(dtEDIFields)
    End Sub



    'Protected Sub lnkEDIFields_Click(sender As Object, e As EventArgs)
    '    If (tblEdiFields.Rows.Count > 0) Then
    '        divEDIFields.Style.Add("display", "")
    '    End If
    'End Sub

    'Protected Sub btnSaveEdiFields_Click(sender As Object, e As EventArgs)
    '    If (dsEDICF IsNot Nothing) Then
    '        Dim dtEDICF As DataTable = dsEDICF.Tables(0)
    '        If (dtEDICF.Rows.Count > 0) Then
    '            For Each row As DataRow In dtEDICF.Rows
    '                Dim GrpID As Integer = 20
    '                Dim FldDTLID As Long = CCommon.ToLong(row("FldDTLID"))
    '                Dim Fld_ID As Long = CCommon.ToLong(row("Fld_ID"))
    '                Dim RecId As Long = lngItemCode
    '                Dim numModifiedBy As Long = CCommon.ToLong(row("numModifiedBy"))
    '                Dim bintModifiedDate As String = row("bintModifiedDate")
    '                Dim Fld_Value As String = CType(tblEdiFields.FindControl("txtEDI" + row("Fld_id").ToString), TextBox).Text

    '                objCommon = New CCommon
    '                objCommon.UpdateEDICustomFields(GrpID, RecId, Fld_ID, Fld_Value, FldDTLID, numModifiedBy, bintModifiedDate)
    '            Next
    '        End If

    '    End If
    '    divEDIFields.Style.Add("display", "none")
    'End Sub
#End Region

    Private Sub btnSaveInventory_Click(sender As Object, e As EventArgs) Handles btnSaveInventory.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dtSerial As New DataTable

            CCommon.AddColumnsToDataTable(dt, "numWareHouseItemID,numItemCode,intAdjust,vcItemName,monAverageCost,numAssetChartAcntId,numReorder")
            CCommon.AddColumnsToDataTable(dtSerial, "numItemCode,numWareHouseID,numWareHouseItemID,vcSerialNo,byteMode,numReorder")
            Dim drSerial As DataRow

            Dim sb As New System.Text.StringBuilder
            Dim flag As Boolean = False

            Dim bitSerialized As Boolean = chkSerializedItem.Checked
            Dim bitLotNo As Boolean = chkLotNo.Checked
            Dim strItemName As String = CCommon.ToString(hdnItemName.Value)

            For Each dgGridItem As GridDataItem In gvInventory.Items
                OnHandQty = CCommon.ToDecimal(CType(dgGridItem.FindControl("lblAvailability"), Label).Text)
                Counted = CCommon.ToDecimal(CType(dgGridItem.FindControl("txtCounted"), TextBox).Text)
                decAverageCost = CCommon.ToDecimal(CType(dgGridItem.FindControl("lblInventoryAverageCost"), Label).Text)

                If Not (bitSerialized Or bitLotNo) Then
                    If CCommon.ToLong(CType(dgGridItem.FindControl("lblAssetChartAcntId"), Label).Text) = 0 Then
                        sb.Append(strItemName + "<br />")
                    End If
                    If CType(dgGridItem.FindControl("txtCounted"), TextBox).Visible AndAlso Counted <> OnHandQty Then
                        intDifference = Counted - OnHandQty
                        decTotalAdjustmentAmount += decAverageCost * intDifference

                        Dim dr As DataRow = dt.NewRow
                        dr("numWareHouseItemID") = CCommon.ToLong(CType(dgGridItem.FindControl("lblWarehouseItemID"), Label).Text)
                        dr("numItemCode") = lngItemCode
                        dr("intAdjust") = intDifference
                        dr("vcItemName") = strItemName
                        dr("monAverageCost") = decAverageCost
                        dr("numAssetChartAcntId") = CCommon.ToLong(CType(dgGridItem.FindControl("lblAssetChartAcntId"), Label).Text)
                        dt.Rows.Add(dr)
                    End If

                    If Not dgGridItem.FindControl("dlInternalLocation") Is Nothing Then
                        For Each dlItem As DataListItem In DirectCast(dgGridItem.FindControl("dlInternalLocation"), DataList).Items
                            If CCommon.ToDouble(DirectCast(dlItem.FindControl("txtInternalLocationQty"), TextBox).Text.Replace(",", "")) <> CCommon.ToDouble(DirectCast(dlItem.FindControl("hdnLocationOnHand"), HiddenField).Value) Then
                                intDifference = CCommon.ToDouble(DirectCast(dlItem.FindControl("txtInternalLocationQty"), TextBox).Text.Replace(",", "")) - CCommon.ToDouble(DirectCast(dlItem.FindControl("hdnLocationOnHand"), HiddenField).Value)
                                decTotalAdjustmentAmount += decAverageCost * intDifference

                                Dim dr As DataRow = dt.NewRow
                                dr("numWareHouseItemID") = CCommon.ToLong(DirectCast(dlItem.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                                dr("numItemCode") = lngItemCode
                                dr("intAdjust") = intDifference
                                dr("vcItemName") = strItemName
                                dr("monAverageCost") = decAverageCost
                                dr("numAssetChartAcntId") = CCommon.ToLong(ddlAssetAccount.SelectedValue)
                                dt.Rows.Add(dr)
                            End If
                        Next
                    End If
                End If
            Next

            If Not (bitSerialized Or bitLotNo) Then
                If sb.Length > 0 Then
                    ShowMessage("Asset account is not set for following items <br>" + sb.ToString + "Your option is to set Asset account and try again.")
                    Exit Sub
                End If

                Dim objItem As New CItems
                objItem.ItemCode = lngItemCode
                objItem.DomainID = Session("DomainID")
                objItem.UserCntID = Session("UserContactID")
                objItem.ReorderQty = If(ddlProduct.SelectedValue.ToLower() = "p" AndAlso divReorderQty.Visible, CCommon.ToDouble(txtReorderQty.Text), 0)
                objItem.ReorderPoint = If(ddlProduct.SelectedValue.ToLower() = "p" AndAlso divReorderPoint.Visible, CCommon.ToDouble(txtReOrderPoint.Text), 0)
                objItem.IsAutomateReorderPoint = chkAutomateReorderPoint.Checked
                objItem.dtAdjustmentDate = CDate(calAdjustmentDate.SelectedDate & " 12:00:00")

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    'Update Quantity
                    If dt.Rows.Count > 0 Then
                        ds.Tables.Add(dt.Copy)
                        ds.Tables(0).TableName = "Item"

                        ds.Tables.Add(dtSerial.Copy)
                        ds.Tables(1).TableName = "SerialLotNo"

                        objItem.str = ds.GetXml()
                    End If

                    objItem.UpdateInventoryAdjustments()

                    If dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            objItem.MakeItemQtyAdjustmentJournal(CCommon.ToLong(dr("numItemCode")), CCommon.ToString(dr("vcItemName")), CCommon.ToInteger(dr("intAdjust")), CCommon.ToDecimal(dr("monAverageCost")), CCommon.ToLong(dr("numAssetChartAcntId")), Session("UserContactID"), Session("DomainID"), boolUseOpeningBalanceEqityAccount:=False, dtAdjustmentDate:=calAdjustmentDate.SelectedDate)
                        Next
                    End If

                    objTransactionScope.Complete()
                End Using
            End If

            lblAdjustmentSuccess.Text = "Adjustment posted sucessfully"
            lblAdjustmentSuccess.Visible = True
            gvInventory.Rebind()

            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
            objCommon.Mode = 41
            objCommon.Str = lngItemCode
            Dim lngAvailableQty As Double = CCommon.ToDouble(objCommon.GetSingleFieldValue())

            If lngAvailableQty > 0 Then
                chkLotNo.Enabled = False
                chkSerializedItem.Enabled = False
            Else
                chkLotNo.Enabled = True
                chkSerializedItem.Enabled = True
            End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Protected Sub radcmbWarehouseLocation_ItemsRequested(sender As Object, e As RadComboBoxItemsRequestedEventArgs)
        Try
            If CCommon.ToLong(ddlWareHouse.SelectedValue) > 0 Then
                Dim objWarehouseLocation As New WarehouseLocation
                objWarehouseLocation.WarehouseID = CCommon.ToLong(ddlWareHouse.SelectedValue)
                objWarehouseLocation.DomainID = CCommon.ToLong(Session("DomainID"))
                objWarehouseLocation.SearchText = CCommon.ToString(e.Text)
                objWarehouseLocation.OffsetRecords = e.NumberOfItems
                objWarehouseLocation.PageSize = 20
                Dim dt As DataTable = objWarehouseLocation.SearchLocationByLocationName()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    radcmbWarehouseLocation.DataSource = dt
                    radcmbWarehouseLocation.DataBind()
                End If

                Dim endOffset As Integer = e.NumberOfItems + If(Not dt Is Nothing AndAlso dt.Rows.Count > 0, dt.Rows.Count, 0)
                Dim totalCount As Integer = If(Not dt Is Nothing AndAlso dt.Rows.Count > 0, dt.Rows(0)("TotalRecords"), 0)

                If endOffset = totalCount Then
                    e.EndOfItems = True
                End If

                e.Message = [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, totalCount)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectWarehouse", "alert('Select external location');", True)
                ddlWareHouse.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dlInternalLocation_ItemCommand(source As Object, e As DataListCommandEventArgs)
        Try
            If e.CommandName = "DeleteInternalLocation" Then
                Dim numItemCode As Int32 = lngItemCode

                Dim numWarehouseItemID As Int32 = CCommon.ToLong(e.CommandArgument)
                Dim numOnHand As Double = CCommon.ToDouble(CType(e.Item.FindControl("hdnLocationOnHand"), HiddenField).Value)
                Dim bitKitParent As Boolean = chkKit.Checked
                Dim itemName As String = CCommon.ToString(hdnItemName.Value)
                Dim numAverageCost As Long = CCommon.ToLong(lblAverageCost.Text)
                Dim numAssetChartAcntID As Long = CCommon.ToLong(ddlAssetAccount.SelectedValue)

                If numItemCode > 0 AndAlso numWarehouseItemID > 0 Then
                    Dim objItems = New CItems
                    objItems.ItemCode = numItemCode
                    objItems.WareHouseItemID = numWarehouseItemID

                    If numOnHand > 0 Then
                        ShowMessage("You are not allowed to delete warehouse item, Your option is to do adjustment of qty such that OnHand quantity goes to 0.")
                    Else
                        objItems.DomainID = Session("DomainID")
                        objItems.UserCntID = Session("UserContactID")
                        objItems.byteMode = 3

                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            objItems.AddUpdateWareHouseForItems()
                            objItems.MakeItemQtyAdjustmentJournal(numItemCode, itemName, -1 * numOnHand, numAverageCost, numAssetChartAcntID, Session("UserContactID"), Session("DomainID"))

                            objTransactionScope.Complete()
                        End Using
                    End If
                End If

                gvInventory.Rebind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub LoadAssetDepreciationGrid()
        Try
            Dim objItemDepreciation As New ItemDepreciation
            objItemDepreciation.DomainID = CCommon.ToLong(Session("DomainID"))
            objItemDepreciation.ItemCode = lngItemCode
            Dim dt As DataTable = objItemDepreciation.GetByItem()
            rptAssetDepreciation.DataSource = dt
            rptAssetDepreciation.DataBind()

            Dim accumulatedDepreciation As Double = 0

            If dt.Select("bitCurrent=1").Length > 0 Then
                For Each dr As DataRow In dt.Rows
                    accumulatedDepreciation += CCommon.ToDouble(dr("monDepreciation"))

                    If CCommon.ToBool(dr("bitCurrent")) Then
                        Exit For
                    End If
                Next
            End If

            lblAccumulatedDepreciation.Text = Session("Currency") & ReturnMoney(accumulatedDepreciation)
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class