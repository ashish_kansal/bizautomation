﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports System.Text
Imports ClosedXML.Excel
Imports System.IO

Namespace BACRM.UserInterface.Items
    Public Class frmInventoryValuation
        Inherits BACRMPage

#Region "Member Variables"

        Private objItem As CItems

#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    txtCurrentPage.Text = "1"
                    calReportDate.SelectedDate = DateAdd(DateInterval.Minute, -1 * CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), DateTime.UtcNow)
                    BindInventoryValuationReport(1)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#Region "Private Methods"

        Private Sub DisplayError(ByVal ex As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            Catch
                'DO NOT THROW ERROR
            End Try
        End Sub

        Private Sub BindInventoryValuationReport(ByVal currentPage As Integer)
            Try
                Dim dt As DataTable = GetReportData(currentPage)

                bizPager.PageSize = 100

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    bizPager.RecordCount = dt.Rows(0)("TotalRecords")
                Else
                    bizPager.RecordCount = 0
                End If

                bizPager.CurrentPageIndex = txtCurrentPage.Text

                gvReport.DataSource = dt
                gvReport.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function GetReportData(ByVal currentPage As Integer) As DataTable
            Try
                objItem = New CItems
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientTimeZoneOffset"))
                objItem.DateEntered = calReportDate.SelectedDate
                objItem.CurrentPage = currentPage

                Return objItem.GetInventoryValuation()
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

#Region "Event Handlers"

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrentPage.Text = bizPager.CurrentPageIndex
                BindInventoryValuationReport(bizPager.CurrentPageIndex)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub lkbDownload_Click(sender As Object, e As EventArgs) Handles lkbDownload.Click
            Try
                Dim dt As DataTable = GetReportData(-1)

                Dim workbook As New XLWorkbook
                Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Inventory Valuation")

                workSheet.Cell(1, 1).SetValue("Item Code").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 1).Style.Font.Bold = True
                workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 1).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 1).DataType = XLCellValues.Text

                workSheet.Cell(1, 2).SetValue("Item Name").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 2).Style.Font.Bold = True
                workSheet.Cell(1, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 2).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 2).DataType = XLCellValues.Text

                workSheet.Cell(1, 3).SetValue("SKU").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 3).Style.Font.Bold = True
                workSheet.Cell(1, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 3).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 3).DataType = XLCellValues.Text

                workSheet.Cell(1, 4).SetValue("UPC").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 4).Style.Font.Bold = True
                workSheet.Cell(1, 4).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 4).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 4).DataType = XLCellValues.Text

                workSheet.Cell(1, 5).SetValue("Total On Hand").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 5).Style.Font.Bold = True
                workSheet.Cell(1, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 5).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 5).DataType = XLCellValues.Text

                workSheet.Cell(1, 6).SetValue("Average Cost").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 6).Style.Font.Bold = True
                workSheet.Cell(1, 6).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 6).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 6).DataType = XLCellValues.Text

                workSheet.Cell(1, 7).SetValue("Total Valuation").SetDataType(XLCellValues.Text)
                workSheet.Cell(1, 7).Style.Font.Bold = True
                workSheet.Cell(1, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                workSheet.Cell(1, 7).Style.Fill.BackgroundColor = XLColor.LightGray
                workSheet.Cell(1, 7).DataType = XLCellValues.Text

                Dim i As Integer = 2
                For Each dr As DataRow In dt.Rows
                    workSheet.Cell(i, 1).Value = CCommon.ToString(dr("numItemCode"))
                    workSheet.Cell(i, 2).Value = CCommon.ToString(dr("vcItemName"))
                    workSheet.Cell(i, 3).Value = CCommon.ToString(dr("vcSKU"))
                    workSheet.Cell(i, 4).Value = CCommon.ToString(dr("numBarCodeId"))
                    workSheet.Cell(i, 5).Value = CCommon.ToDouble(dr("numTotalOnHand"))
                    workSheet.Cell(i, 6).Value = CCommon.ToDouble(dr("monAverageCost"))
                    workSheet.Cell(i, 7).Value = CCommon.GetDecimalFormat(CCommon.ToDouble(dr("monTotalValuation")))

                    workSheet.Cell(i, 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 1).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    workSheet.Cell(i, 2).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 2).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    workSheet.Cell(i, 3).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 3).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    workSheet.Cell(i, 4).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 4).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    workSheet.Cell(i, 5).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 5).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    workSheet.Cell(i, 6).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 6).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    workSheet.Cell(i, 7).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    workSheet.Cell(i, 7).Style.Border.SetOutsideBorderColor(XLColor.Black)

                    i = i + 1
                Next

                workSheet.Columns.AdjustToContents()

                Dim httpResponse As HttpResponse = Response
                httpResponse.Clear()
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                httpResponse.AddHeader("content-disposition", "attachment;filename=InventoryValuation" & Convert.ToDateTime(calReportDate.SelectedDate).ToString("yyyyMMdd") & ".xlsx")


                Using MemoryStream As New MemoryStream
                    workbook.SaveAs(MemoryStream)
                    MemoryStream.WriteTo(httpResponse.OutputStream)
                    MemoryStream.Close()
                End Using

                httpResponse.Flush()
                httpResponse.End()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnGo_Click(sender As Object, e As EventArgs)
            Try
                txtCurrentPage.Text = "1"
                BindInventoryValuationReport(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
#End Region

    End Class
End Namespace