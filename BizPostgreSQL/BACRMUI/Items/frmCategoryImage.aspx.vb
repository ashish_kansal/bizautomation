﻿Imports System.IO
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Public Class frmCategoryImage
    Inherits BACRMPage

    Dim strFileName As String
    Dim strFileTName As String
    Dim strFilePath As String
    Dim lngCategoryID As Long

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Try
        '    lngCategoryID = GetQueryStringVal( "CId")
        '    If Not IsPostBack Then

        '        LoadPic()
        '        btnClose.Attributes.Add("onclick", "return Close()")
        '    End If
        'Catch ex As Exception
        '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '    Response.Write(ex)
        'End Try
    End Sub

    'Sub LoadPic()
    '    Try
    '        Dim objItems As New CItems
    '        objItems.byteMode = 16
    '        objItems.DomainID = Session("DomainID")
    '        objItems.CategoryID = lngCategoryID
    '        Dim dtTemp As DataTable = objItems.SeleDelCategory

    '        If dtTemp.Rows.Count > 0 Then
    '            If CCommon.ToString(dtTemp.Rows(0).Item("vcPathForCategoryImage")) <> "" Then
    '                Image1.Visible = True
    '                Image1.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtTemp.Rows(0).Item("vcPathForCategoryImage")
    '                HiddenField2.Value = dtTemp.Rows(0).Item("vcPathForCategoryImage")
    '            Else : Image1.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub UploadImage()
    '    Try
    '        If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
    '            Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
    '        End If
    '        Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
    '        Dim ArrFilePath, ArrFileExt
    '        Dim strFileExt As String

    '        If txtThumbFile.PostedFile.FileName <> "" Then
    '            strFilePath = FilePath
    '            ArrFileExt = Split(txtThumbFile.PostedFile.FileName, ".")
    '            strFileExt = ArrFileExt(UBound(ArrFileExt))
    '            strFileName = "Category" & lngCategoryID & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".gif"
    '            strFilePath = strFilePath & "\" & strFileName
    '            If Not txtThumbFile.PostedFile Is Nothing Then txtThumbFile.PostedFile.SaveAs(strFilePath)
    '            HiddenField2.Value = strFileName
    '        Else : strFileName = HiddenField2.Value
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub UpdateImageDetails()
    '    Try
    '        Dim objItem As New CItems
    '        objItem.CategoryID = lngCategoryID
    '        objItem.PathForImage = strFileName
    '        objItem.UploadItemImage()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    Try
    '        HiddenField2.Value = ""
    '        UpdateImageDetails()
    '        LoadPic()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        UploadImage()
    '        UpdateImageDetails()
    '        LoadPic()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '    Try
    '        UploadImage()
    '        UpdateImageDetails()
    '        LoadPic()
    '        Response.Write("<script>window.close();</script>")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub


End Class