﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmPricingTable
    Inherits BACRMPage
    Dim dtItems As New DataTable
    Dim objPriceBookRule As PriceBookRule
    Dim lngPriceRuleID, lngView, lngItemCode As Long
    Dim i As Int32 = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngPriceRuleID = CCommon.ToLong(GetQueryStringVal("RuleID"))
            lngView = CCommon.ToLong(GetQueryStringVal("View"))
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))

            If Not IsPostBack Then
                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    chkValidationDisable.Checked = PersistTable(chkValidationDisable.ID)
                End If
                BindCurrency()
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadDetails()
        Try
            Dim objCommon As New CCommon
            objCommon.DomainID = Session("DomainID")
            Dim dtPriceLevel As DataTable = objCommon.GetNamedPriceLevel()

            If objPriceBookRule Is Nothing Then
                objPriceBookRule = New PriceBookRule
            End If
            objPriceBookRule.RuleID = lngPriceRuleID
            objPriceBookRule.ItemID = lngItemCode
            objPriceBookRule.DomainID = Session("DomainID")
            objPriceBookRule.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            dtItems = objPriceBookRule.GetPriceTable()

            If lngItemCode > 0 Then
                'chkValidationDisable.Visible = True
                ddlRuleType.Items.Add(New ListItem("Named Price", 3))
            End If

            If dtItems.Rows.Count > 0 Then
                If Not IsDBNull(dtItems.Rows(0)("tintRuleType")) Then
                    ddlRuleType.SelectedValue = dtItems.Rows(0)("tintRuleType")
                End If
                If Not IsDBNull(dtItems.Rows(0)("tintVendorCostType")) Then
                    ddlVendorCostType.SelectedValue = dtItems.Rows(0)("tintVendorCostType")
                End If
                If Not IsDBNull(dtItems.Rows(0)("tintDiscountType")) Then
                    ddlDiscountType.SelectedValue = dtItems.Rows(0)("tintDiscountType")
                End If
                If ddlRuleType.SelectedValue = "2" Then
                    ddlVendorCostType.Visible = True
                Else
                    ddlVendorCostType.Visible = False
                End If
            End If

            If lngItemCode > 0 Then
                Dim dr As DataRow
                For i As Integer = dtItems.Rows.Count To 19
                    dr = dtItems.NewRow

                    If Not dtPriceLevel Is Nothing AndAlso dtPriceLevel.Rows.Count > 0 AndAlso dtPriceLevel.Select("Id=" & (i + 1)).Count > 0 Then
                        Dim drName As DataRow = dtPriceLevel.Select("Id=" & (i + 1))(0)
                        dr("vcName") = CCommon.ToString(drName("Value"))
                    End If

                    dtItems.Rows.Add(dr)
                Next
            Else
                Dim tintRuleFor As Short

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 7
                objCommon.Str = lngPriceRuleID
                tintRuleFor = objCommon.GetSingleFieldValue()

                If lngView = 0 Then
                    Dim dr As DataRow
                    For i As Integer = dtItems.Rows.Count To 19
                        dr = dtItems.NewRow
                        dtItems.Rows.Add(dr)
                    Next
                Else
                    btnSave.Visible = False
                    divLabel.Visible = True
                    ddlRuleType.Visible = False
                    ddlVendorCostType.Visible = False

                    If tintRuleFor = 1 Then
                        lblRuleType.Text = ddlRuleType.SelectedItem.Text
                    Else
                        lblRuleType.Text = "Deduct from Vendor Cost"
                    End If
                    lblDiscountType.Text = ddlDiscountType.SelectedItem.Text
                End If

                If tintRuleFor = 2 Then
                    ddlRuleType.ClearSelection()
                    ddlRuleType.SelectedIndex = 0
                    ddlRuleType.Visible = False
                    ddlVendorCostType.Visible = False
                End If
            End If

            dgPriceTable.DataSource = dtItems
            dgPriceTable.DataBind()

            If ddlRuleType.SelectedValue = "3" Then
                dgPriceTable.Columns(3).Visible = False
                dgPriceTable.Columns(4).Visible = False
                chkValidationDisable.Visible = False
            End If

            'If ddlDiscountType.SelectedValue = "3" Or chkValidationDisable.Checked Then
            '    chkFromToAmount.Style.Add("display", "none")
            '    chkFromToAmount.Checked = False
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCurrency()
        Try
            Dim objCurrency As New CurrencyRates
            objCurrency.DomainID = Session("DomainID")
            objCurrency.GetAll = 0
            ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
            ddlCurrency.DataTextField = "vcCurrencyDesc"
            ddlCurrency.DataValueField = "numCurrencyID"
            ddlCurrency.DataBind()

            ddlCurrency.Items.Insert(0, New ListItem("Base Currency", 0))

            If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                ddlCurrency.Items.Remove(ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Sub LoadDefaultColumns()
        Try
            dtItems.Columns.Add("numPricingID") '' Primary key
            dtItems.Columns.Add("numPriceRuleID")
            dtItems.Columns.Add("numItemCode")
            dtItems.Columns.Add("vcName")
            dtItems.Columns.Add("intFromQty")
            dtItems.Columns.Add("intToQty")
            dtItems.Columns.Add("tintRuleType")
            dtItems.Columns.Add("tintVendorCostType")
            dtItems.Columns.Add("tintDiscountType")
            dtItems.Columns.Add("decDiscount")
            dtItems.Columns.Add("bitFromAndToIsAmount")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtgriditem As DataGridItem
        Dim dtrow As DataRow
        Dim ds As New DataSet
        Try
            objPriceBookRule = New PriceBookRule
            LoadDefaultColumns()
            Dim intFromQty, intToQty As Int32
            Dim decDiscount As Decimal
            Dim vcName As String

            For i As Integer = 0 To dgPriceTable.Items.Count - 1
                dtrow = dtItems.NewRow
                dtgriditem = dgPriceTable.Items(i)

                intFromQty = CCommon.ToInteger(CType(dtgriditem.FindControl("txtQtyFrom"), TextBox).Text)
                intToQty = CCommon.ToInteger(CType(dtgriditem.FindControl("txtQtyTo"), TextBox).Text)
                decDiscount = CCommon.ToDecimal(CType(dtgriditem.FindControl("txtDiscAmount"), TextBox).Text)
                'vcName = CCommon.ToString(CType(dtgriditem.FindControl("txtName"), TextBox).Text)
                vcName = CCommon.ToString(CType(dtgriditem.FindControl("lblName"), Label).Text)

                If Not String.IsNullOrEmpty(CType(dtgriditem.FindControl("txtDiscAmount"), TextBox).Text) AndAlso ((intFromQty > 0 AndAlso intToQty > 0) Or ddlRuleType.SelectedValue = "3") Then
                    dtrow.Item("numPricingID") = CCommon.ToLong(dtgriditem.Cells(0).Text)
                    dtrow.Item("numPriceRuleID") = lngPriceRuleID
                    dtrow.Item("numItemCode") = lngItemCode
                    dtrow.Item("vcName") = vcName
                    dtrow.Item("intFromQty") = IIf(ddlRuleType.SelectedValue = "3", 0, intFromQty)
                    dtrow.Item("intToQty") = IIf(ddlRuleType.SelectedValue = "3", 0, intToQty)
                    dtrow.Item("tintRuleType") = ddlRuleType.SelectedValue
                    dtrow.Item("tintVendorCostType") = ddlVendorCostType.SelectedValue
                    dtrow.Item("tintDiscountType") = ddlDiscountType.SelectedValue
                    'dtrow.Item("bitFromAndToIsAmount") = IIf((ddlDiscountType.SelectedValue = "3" Or chkValidationDisable.Checked), False, chkFromToAmount.Checked)
                    dtrow.Item("decDiscount") = decDiscount
                    dtItems.Rows.Add(dtrow)
                End If
            Next

            ds.Tables.Add(dtItems.Copy)
            ds.Tables(0).TableName = "Item"
            objPriceBookRule.RuleID = lngPriceRuleID
            objPriceBookRule.ItemID = lngItemCode
            objPriceBookRule.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
            objPriceBookRule.strItems = ds.GetXml()
            objPriceBookRule.ManagePriceTable()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "Register", "window.close();", True)

            PersistTable.Clear()
            PersistTable.Add(chkValidationDisable.ID, chkValidationDisable.Checked)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgPriceTable_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPriceTable.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            i = 0
        ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblPriceLevelName As Label = DirectCast(e.Item.FindControl("lblPriceLevelName"), Label)

            If Not lblPriceLevelName Is Nothing Then
                lblPriceLevelName.Text = "Price Level " & (i + 1)
                i = i + 1
            End If

            If lngView = 0 Then
                e.Item.FindControl("lblQtyFrom").Visible = False
                e.Item.FindControl("lblQtyTo").Visible = False
                e.Item.FindControl("lblDiscAmount").Visible = False
            Else
                e.Item.FindControl("txtQtyFrom").Visible = False
                e.Item.FindControl("txtQtyTo").Visible = False
                e.Item.FindControl("txtDiscAmount").Visible = False
            End If
        End If
    End Sub

    Private Sub ddlRuleType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRuleType.SelectedIndexChanged
        Try
            dgPriceTable.Columns(3).Visible = IIf(ddlRuleType.SelectedValue = "3", False, True)
            dgPriceTable.Columns(4).Visible = IIf(ddlRuleType.SelectedValue = "3", False, True)
            chkValidationDisable.Visible = IIf(ddlRuleType.SelectedValue = "3", False, True)

            If ddlRuleType.SelectedValue = "2" Then
                ddlVendorCostType.Visible = True
            Else
                ddlVendorCostType.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub ddlCurrency_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class