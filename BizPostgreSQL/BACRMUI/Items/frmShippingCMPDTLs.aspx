<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingCMPDTLs.aspx.vb"
    Inherits=".frmShippingCMPDTLs" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Company Details</title>
    <script language="javascript">
        function Close() {
            window.close();
            return false;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblShippingCompany" Text="" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="600px" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgShipcmp" AllowSorting="true" runat="server" Width="100%" CssClass="dg"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="intShipFieldID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="" HeaderStyle-Width="35%">
                            <ItemTemplate>
                                <div style="float: left">
                                    <asp:Label ID="lblFieldName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcFieldName") %>'> </asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:Label ID="lblToolTip" Text="[?]" CssClass="tip" runat="server" ToolTip='<%# DataBinder.Eval(Container,"DataItem.vcToolTip") %>'
                                        Visible='<%# Boolean.Parse(IIf(DataBinder.Eval(Container,"DataItem.vcToolTip")="","false","true")) %>' />
                                    &nbsp;
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="65%">
                            <ItemTemplate>
                                <asp:TextBox Width="300" ID="txtShipFieldValue" runat="server" Visible='<%# Boolean.Parse(IIf((DataBinder.Eval(Container, "DataItem.numListItemID") = 91 Or DataBinder.Eval(Container, "DataItem.numListItemID") = 88 Or DataBinder.Eval(Container, "DataItem.numListItemID") = 90) AndAlso (DataBinder.Eval(Container, "DataItem.vcFieldName") = "Rate Type" Or DataBinder.Eval(Container, "DataItem.vcFieldName") = "Shipping Label Type"), "false", "true"))%>' Text='<%# DataBinder.Eval(Container,"DataItem.vcShipFieldValue") %>'></asp:TextBox>
                                <asp:DropDownList ID="ddlFedExOption" runat="server" Visible='<%# Boolean.Parse(IIf((DataBinder.Eval(Container,"DataItem.numListItemID")=91 OR DataBinder.Eval(Container,"DataItem.numListItemID")=88) and DataBinder.Eval(Container,"DataItem.vcFieldName")="Rate Type" ,"true","false")) %>'>
                                    <asp:ListItem Text="List Rate" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Negotiated Rate" Selected="True" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlImageTypeFedex" runat="server" Visible='<%# Boolean.Parse(IIf(DataBinder.Eval(Container, "DataItem.numListItemID") = 91 And DataBinder.Eval(Container, "DataItem.vcFieldName") = "Shipping Label Type", "true", "false"))%>'>
                                    <asp:ListItem Text="PNG" Selected="True" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="ZPL" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlImageTypeUPS" runat="server" Visible='<%# Boolean.Parse(IIf(DataBinder.Eval(Container, "DataItem.numListItemID") = 88 And DataBinder.Eval(Container, "DataItem.vcFieldName") = "Shipping Label Type", "true", "false"))%>'>
                                    <asp:ListItem Text="GIF" Selected="True" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="ZPL" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlImageTypeUSPS" runat="server" Visible='<%# Boolean.Parse(IIf(DataBinder.Eval(Container, "DataItem.numListItemID") = 90 And DataBinder.Eval(Container, "DataItem.vcFieldName") = "Shipping Label Type", "true", "false"))%>'>
                                    <asp:ListItem Text="GIF" Selected="True" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="ZPL" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:CheckBox ID="chkIsEndicia" runat="server" Visible='<%# Boolean.Parse(IIf(DataBinder.Eval(Container, "DataItem.numListItemID") = 90 And DataBinder.Eval(Container, "DataItem.vcFieldName") = "Is Endicia", "true", "false"))%>'></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
        <%-- <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" CssClass="normal1">
                &nbsp;&nbsp;Padding Amount(in Base Currency):<asp:TextBox runat="server" ID="txtPaddingAmount" CssClass="signup" onkeypress="CheckNumber(1,event);" Width="40px"></asp:TextBox>&nbsp;
                <asp:DropDownList runat="server" ID="ddlPaddingAmountType" CssClass="signup" Width="80px">
                    <asp:ListItem Text="Flat" Value="" />
                    <asp:ListItem Text="Percentage" Value="%" />
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>--%>
    </asp:Table>
</asp:Content>
