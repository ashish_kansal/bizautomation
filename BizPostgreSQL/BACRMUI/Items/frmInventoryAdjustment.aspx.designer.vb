﻿''------------------------------------------------------------------------------
'' <auto-generated>
''     This code was generated by a tool.
''
''     Changes to this file may cause incorrect behavior and will be lost if
''     the code is regenerated. 
'' </auto-generated>
''------------------------------------------------------------------------------

'Option Strict On
'Option Explicit On


'Partial Public Class frmInventoryAdjustment

'    '''<summary>
'    '''btnSaveClose control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents btnSaveClose As Global.System.Web.UI.WebControls.Button

'    '''<summary>
'    '''btnCancel control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

'    '''<summary>
'    '''litMessage control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

'    '''<summary>
'    '''ddlAccount control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents ddlAccount As Global.System.Web.UI.WebControls.DropDownList

'    '''<summary>
'    '''ddlWarehouse control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents ddlWarehouse As Global.System.Web.UI.WebControls.DropDownList

'    '''<summary>
'    '''lblOnHandQty control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents lblOnHandQty As Global.System.Web.UI.WebControls.Label

'    '''<summary>
'    '''lblCurrentInvValue control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents lblCurrentInvValue As Global.System.Web.UI.WebControls.Label

'    '''<summary>
'    '''radQty control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents radQty As Global.System.Web.UI.WebControls.RadioButton

'    '''<summary>
'    '''radCost control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents radCost As Global.System.Web.UI.WebControls.RadioButton

'    '''<summary>
'    '''txtAdjustedQty control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents txtAdjustedQty As Global.System.Web.UI.WebControls.TextBox

'    '''<summary>
'    '''Label1 control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

'    '''<summary>
'    '''lblAdjustValue control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents lblAdjustValue As Global.System.Web.UI.WebControls.Label

'    '''<summary>
'    '''txtUnitCost control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents txtUnitCost As Global.System.Web.UI.WebControls.TextBox

'    '''<summary>
'    '''Label2 control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label

'    '''<summary>
'    '''txtMemo control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents txtMemo As Global.System.Web.UI.WebControls.TextBox

'    '''<summary>
'    '''calAdjustmentDate control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents calAdjustmentDate As Global.BACRM.Include.calandar

'    '''<summary>
'    '''ddlProject control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents ddlProject As Global.System.Web.UI.WebControls.DropDownList

'    '''<summary>
'    '''hdnAverageCost control.
'    '''</summary>
'    '''<remarks>
'    '''Auto-generated field.
'    '''To modify move field declaration from designer file to code-behind file.
'    '''</remarks>
'    Protected WithEvents hdnAverageCost As Global.System.Web.UI.WebControls.HiddenField
'End Class
