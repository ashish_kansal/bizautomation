﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports System.Collections.Generic

Public Class frmItemUOMConversion
    Inherits BACRMPage
#Region "Member Variables"
    Dim dtSourceUOM As DataTable
    Dim dtBuyUOM As DataTable
    Dim objItemUOMConversion As ItemUOMConversion
#End Region

#Region "Constructor"
    Sub New()
        objItemUOMConversion = New ItemUOMConversion
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""

            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            Dim charUnitType As Char = Session("UnitSystem")
            If charUnitType = "E" Then
                objCommon.UnitType = 1
            ElseIf charUnitType = "M" Then
                objCommon.UnitType = 2
            Else
                objCommon.UnitType = 0
            End If

            objItemUOMConversion.DomainID = CCommon.ToLong(Session("DomainID"))
            objItemUOMConversion.UserCntID = CCommon.ToLong(Session("UserContactID"))

            If Not Page.IsPostBack Then
                hdnItemCode.Value = GetQueryStringVal("ItemCode")
                BindData()
                ddlSourceUOM.Focus()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblException.Text = ex.Message
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindData()
        Try
            objItemUOMConversion.numItemCode = CCommon.ToLong(hdnItemCode.Value)
            Dim objData = objItemUOMConversion.GetAllByItem()

            hdnBaseUOM.Value = CCommon.ToLong(objData.BaseUOM)
            Dim listItemUOMConversion As List(Of ItemUOMConversion) = objData.ListItemUOMConversion
            
            If CCommon.ToLong(hdnBaseUOM.Value) = 0 Then
                pnlInsert.Visible = False
                lblException.Text = "Go to item details and select item Base UOM."
            Else
                If Not listItemUOMConversion Is Nothing AndAlso listItemUOMConversion.Count > 0 Then
                    lblBaseUOM.Visible = False
                    ddlTargetUOM.Visible = True
                Else
                    lblBaseUOM.Visible = True
                    ddlTargetUOM.Visible = False
                End If

                gvItemUOMConversion.DataSource = listItemUOMConversion
                gvItemUOMConversion.DataBind()

                ddlSourceUOM.Items.Clear()
                ddlTargetUOM.Items.Clear()

                ddlSourceUOM.Items.Add(New ListItem("-- Select One --", "0"))
                ddlTargetUOM.Items.Add(New ListItem("-- Select One --", "0"))

                

                'GET UOM
                Dim dtUOM As DataTable = objCommon.GetUOM()
                For Each dr As DataRow In dtUOM.Rows
                    If listItemUOMConversion.Find(Function(x) x.numSourceUOM = dr("numUOMId")) Is Nothing AndAlso dr("numUOMId") <> hdnBaseUOM.Value Then
                        ddlSourceUOM.Items.Add(New ListItem(dr("vcUnitName"), dr("numUOMId")))
                    End If

                    If listItemUOMConversion.Find(Function(x) x.numTargetUOM = dr("numUOMId")) Is Nothing Then
                        ddlTargetUOM.Items.Add(New ListItem(dr("vcUnitName"), dr("numUOMId")))
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            Dim intUnits As Double

            If ddlSourceUOM.SelectedValue > 0 AndAlso (lblBaseUOM.Visible Or ddlTargetUOM.SelectedValue > 0) And Double.TryParse(txtUnits.Text, intUnits) Then
                objItemUOMConversion.numItemCode = CCommon.ToLong(hdnItemCode.Value)
                objItemUOMConversion.numSourceUOM = CCommon.ToLong(ddlSourceUOM.SelectedValue)
                objItemUOMConversion.numTargetUOM = IIf(lblBaseUOM.Visible, CCommon.ToLong(hdnBaseUOM.Value), CCommon.ToLong(ddlTargetUOM.SelectedValue))
                objItemUOMConversion.numTargetUnit = intUnits
                objItemUOMConversion.Save()

                BindData()

                txtUnits.Text = ""
                ddlSourceUOM.Focus()
            Else
                lblException.Text = "Invalid data."

                If ddlSourceUOM.SelectedValue = "0" Then
                    ddlSourceUOM.Focus()
                ElseIf ddlTargetUOM.SelectedValue = "0" Then
                    ddlTargetUOM.Focus()
                Else
                    txtUnits.Focus()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblException.Text = ex.Message
        End Try
    End Sub

    Private Sub gvItemUOMConversion_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItemUOMConversion.RowCommand
        Try
            If e.CommandName = "Delete" Then
                objItemUOMConversion.numItemUOMConvID = CCommon.ToLong(e.CommandArgument)
                objItemUOMConversion.Delete()

                BindData()

                ddlSourceUOM.Focus()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblException.Text = ex.Message
        End Try
    End Sub

    Private Sub gvItemUOMConversion_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvItemUOMConversion.RowDeleting
        Try
            'NO CODE IS REQUIRED BUT WE HAVE TO HANDLE THIS EVENT FOR DELETE
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblException.Text = ex.Message
        End Try
    End Sub

#End Region
End Class