Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class frmPriceBookMgmtList
    Inherits BACRMPage
    Dim objPriceBookRule As PriceBookRule

    Dim lngItemCode, lngVendor As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
            lngVendor = CCommon.ToLong(GetQueryStringVal("Vendor"))

            btnBackItem.Attributes.Add("onclick", "return OpenItem(" & lngItemCode & ")")
            btnNew.Attributes.Add("onclick", "return New(" & lngItemCode & "," & lngVendor & ")")

            GetUserRightsForPage(13, 18)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnNew.Visible = False
            End If
            If Not IsPostBack Then

                objPriceBookRule = New PriceBookRule
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDetails()
        Try
            If objPriceBookRule Is Nothing Then
                objPriceBookRule = New PriceBookRule
            End If

            If lngItemCode > 0 And lngVendor > 0 Then
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")

                objCommon.Mode = 4
                objCommon.Str = lngItemCode
                lblItem.Text = objCommon.GetSingleFieldValue()

                objCommon.Mode = 5
                objCommon.Str = lngVendor
                lblVendor.Text = objCommon.GetSingleFieldValue()

                tblItemVendor.Visible = True
            End If

            objPriceBookRule.RuleID = 0

            objPriceBookRule.ItemID = lngItemCode
            objPriceBookRule.DivisionID = lngVendor

            objPriceBookRule.byteMode = 1
            objPriceBookRule.DomainID = Session("DomainID")
            If ViewState("Sort") = 0 Then
                objPriceBookRule.boolSort = False
            Else
                objPriceBookRule.boolSort = True
            End If
            dgPriceBookRule.DataSource = objPriceBookRule.GetPriceBookRule
            dgPriceBookRule.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPriceBookRule_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPriceBookRule.DeleteCommand
        Try
            objPriceBookRule = New PriceBookRule
            objPriceBookRule.RuleID = e.Item.Cells(0).Text
            objPriceBookRule.DelPriceBookRule()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgPriceBookRule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPriceBookRule.ItemCommand
        Try
            If e.CommandName = "RuleID" Then
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then Response.Redirect("../Items/frmPriceBookMgmt.aspx?RuleID=" & e.Item.Cells(0).Text, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgPriceBookRule_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPriceBookRule.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                End If
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPriceBookRule_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgPriceBookRule.SortCommand
        If ViewState("Sort") = 0 Then
            ViewState("Sort") = 1
        Else
            ViewState("Sort") = 0
        End If
        LoadDetails()
    End Sub
End Class