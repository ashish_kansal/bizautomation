﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRelatedItems.aspx.vb"
    EnableEventValidation="false" Inherits=".frmRelatedItems" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/biz.css" type="text/css" />

    <script type="text/javascript" language="javascript">
        var selectedItemCode = '';
        function Selectall1(a) {
            var str;
            if (typeof (a) == 'string') {
                a = document.getElementById(a)
            }
            if (a.checked == true) {
                for (var i = 2; i <= document.getElementById('GvsimilarItem').rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('GvsimilarItem_ctl' + str + '_chkSelect').checked = true;
                }
            }
            else if (a.checked == false) {
                for (var i = 2; i <= document.getElementById('GvsimilarItem').rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('GvsimilarItem_ctl' + str + '_chkSelect').checked = false;
                }
            }
        }

        function DeleteSimilarRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OnClientDropDownClosedHandler(sender, eventArgs) {
            selectedItemCode = sender.get_value();
        }

        function GetCheckedCheckBox(chk) {
            var dataGrid = document.all['GvsimilarItem'];
            var rows = dataGrid.rows;
            for (var index = 1; index < rows.length; index++) {

                var chkPreUpSell = rows[index].cells[1].childNodes[0];
                var chkPostUpSell = rows[index].cells[2].childNodes[0];
                var chkRequired = rows[index].cells[5].childNodes[0];
                debugger;
                if (chkPreUpSell.nextSibling.id == chk.id & chkPreUpSell.nextSibling.checked == true)
                {
                    chkPostUpSell.nextSibling.setAttribute("disabled", "true");
                    chkRequired.nextSibling.setAttribute("disabled", "true");
                }
                else if (chkPreUpSell.nextSibling.id == chk.id & chkPreUpSell.nextSibling.checked == false)
                {
                    chkPostUpSell.nextSibling.removeAttribute('disabled');
                    chkRequired.nextSibling.removeAttribute('disabled');
                }
                if (chkPostUpSell.nextSibling.id == chk.id & chkPostUpSell.nextSibling.checked == true)
                {
                    chkPreUpSell.nextSibling.setAttribute("disabled", "true");
                    chkRequired.nextSibling.setAttribute("disabled", "true");
                }
                else if (chkPostUpSell.nextSibling.id == chk.id & chkPostUpSell.nextSibling.checked == false) {
                    chkPreUpSell.nextSibling.removeAttribute('disabled');
                    chkRequired.nextSibling.removeAttribute('disabled');
                }
                if (chkRequired.nextSibling.id == chk.id & chkRequired.nextSibling.checked == true)
                {
                    chkPostUpSell.nextSibling.setAttribute("disabled", "true");
                    chkPreUpSell.nextSibling.setAttribute("disabled", "true");
                }
                else if (chkRequired.nextSibling.id == chk.id & chkRequired.nextSibling.checked == false) {
                    chkPostUpSell.nextSibling.removeAttribute('disabled');
                    chkPreUpSell.nextSibling.removeAttribute('disabled');
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            <asp:Label ID="lblItem" runat="server" Text="Item:"></asp:Label></label>
                        <telerik:RadComboBox ID="radSimilarItem" runat="server" Width="250" DropDownWidth="400px"
                            EnableScreenBoundaryDetection="true" EnableLoadOnDemand="true" AllowCustomText="True"
                            OnClientDropDownClosed="OnClientDropDownClosedHandler">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                        </telerik:RadComboBox>
                    </div>
                    &nbsp;&nbsp;
                    <%--<div class="form-group">
                        <label>Pre-Up Sell:</label>
                        <asp:CheckBox runat="server" ID="chkPreUpSellMain" />
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Post-Up Sell:</label>
                        <asp:CheckBox runat="server" ID="chkPostUpSellMain" />
                    </div>
                    &nbsp;&nbsp;--%>
                    <div class="form-group">
                        <label>Relation to parent item:</label>
                        <asp:TextBox ID="txtRelationship" Width="514px" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    
                    <asp:Button ID="btnAddSimilarItem" runat="server" CssClass="btn btn-primary" Text="Add Item" />
                    <asp:Button ID="btnDeleteSimilarItem" runat="server" CssClass="btn btn-danger" Text="Remove" />
                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:GridView ID="GvsimilarItem" Width="100%" runat="server" UseAccessibleHeader="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" DataKeyNames="numItemCode">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" onclick="javascript:Selectall1(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="25" />
                                <ItemStyle HorizontalAlign="Center" Width="25" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle HorizontalAlign="Center" Wrap="false"/>
                                <ItemStyle HorizontalAlign="Center" Width="85" />
                                <HeaderTemplate>
                                    Pre-Checkout
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="chkPreUpSell" runat="server" Checked='<%#CCommon.ToBool(Eval("bitPreUpSell"))%>' />--%>
                                    <asp:CheckBox ID="chkPreUpSell" runat="server" Checked='<%#CCommon.ToBool(Eval("bitPreUpSell"))%>' onclick="javascript:GetCheckedCheckBox(this);" AutoPostBack="true" OnCheckedChanged="chkPreUpSell_CheckedChanged"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle HorizontalAlign="Center" Wrap="false"/>
                                <ItemStyle HorizontalAlign="Center" Width="90" />
                                <HeaderTemplate>
                                    Post-Checkout
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPostUpSell" runat="server" Checked='<%#CCommon.ToBool(Eval("bitPostUpSell"))%>' onclick="javascript:GetCheckedCheckBox(this);" AutoPostBack="true" OnCheckedChanged="chkPostUpSell_CheckedChanged"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="vcItemName" HeaderText="Item" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText="Image">
                                <ItemTemplate>
                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1, 100, 56)%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--Added by Neelam on 10/13/2017 - New column: If checked, then the item (row) is automatically added to the cart (internal and e-commerce sale) when the parent item is added to the cart.--%>
                             <asp:TemplateField>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="85" />
                                <HeaderTemplate>
                                    Required
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRequired" runat="server" Checked='<%#CCommon.ToBool(Eval("bitRequired"))%>' onclick="javascript:GetCheckedCheckBox(this);" AutoPostBack="true" OnCheckedChanged="chkRequired_CheckedChanged"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="400" />
                            <asp:BoundField DataField="vcUpSellDesc" HeaderText="Relation to parent item" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Width="400" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

        <asp:Panel ID="pnlNote" runat="server" CssClass="signup">
            <%--Modified by Neelam on 10/13/2017 - Replaced the text which should also function as the requirements definition for how everything should work.--%>
            <%--<b>Note:</b> Selected related item must be part of at least one category to appear
            in e-commerce--%>

            <b>When “Pre-Check-out” check-box is selected:</b> BizAutomation will display a window with all the items that have this check box selected within this section, WHEN the parent item is selected, whether this is a customer shopping on any of your BizAutomation E-Commerce web sites, or an employee is using the new sales opportunity / order form. The trigger for this event is the “add item” function. 
            <p><b>“When “Post-Check-out” check-box is selected:</b> BizAutomation will display a window containing all the items that meet this condition, not only for this parent item, but any parent item added to the shopping cart for any BizAutomation E-Commerce site (Meaning it does not apply to internal Sales Opportunities / Orders) IF there are 10 items or less added to the shopping cart (over 10 items, and the window will no longer display). The trigger for this event is the “Submit” button click within the check-out page. So for example, say a customer checks out with Item-1 and Item-2. Item-1 has a related item called Widget-1 that has its “Post-UpSell” check box selected, and Item-2 has a related item called “Widget-2” which has its “Post-Up-Sell” check box selected. Both will display within the window after the submit button within the check-out page is clicked. Note – If the same item is related to 2 or more parent items, then the window will only display the item a single time.</p>
            <p><b>When “Required” check-box is selected:</b> This will automatically add this line item to the checkout list both on any BizAutomation e-commerce site being used, or for internal Sales Opportunities / Orders form where the parent item is added.</p> 
            <p><b>Note:</b> If there is a promotion rule in effect that impacts any related item, that discount will be reflected in the window displaying related items.</p> 
        </asp:Panel>
        <asp:HiddenField ID="hdnItemCode" runat="server" />
        <script language="javascript" type="text/javascript">
            function CheckSimilarItem() {
                var ItemCode = document.getElementById('hdnItemCode').value;
                if (ItemCode == selectedItemCode) {
                    alert('Same item not insert into related item');
                    return false;
                }
                return true;
            }
        </script>
    </form>
</body>
</html>
