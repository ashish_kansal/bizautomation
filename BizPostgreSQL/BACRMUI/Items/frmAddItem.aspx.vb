Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item

Partial Public Class frmAddItem
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not Page.IsPostBack Then
                If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                    divCreateOrMap.Visible = True
                End If

                If CCommon.ToBool(GetQueryStringVal("isFromAssembly")) AndAlso CCommon.ToLong(GetQueryStringVal("AssemblyItemID")) > 0 Then
                    radKit.Visible = False
                End If

                If CCommon.ToBool(GetQueryStringVal("isFromNewOrder")) Then
                    btnNext.Attributes.Add("onclick", "SendToNewOrderPage();")
                End If

                If CCommon.ToBool(GetQueryStringVal("isFromAssemblyKit")) Then
                    btnNext.Attributes.Add("onclick", "SendToAssemblyKitItemDetailPage();")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            If divCreateOrMap.Visible AndAlso rbExisting.Checked Then
                If CCommon.ToLong(hdnItemID.Value) > 0 Then

                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItem.ItemCode = CCommon.ToLong(hdnItemID.Value)

                    If Not objItem.IsKitWithChildKits() Then
                        Dim objOpportunity As New OppotunitiesIP
                        objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                        objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpportunity.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        objOpportunity.OpportunityDetails()

                        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItem.ItemCode = CCommon.ToLong(hdnItemID.Value)
                        objItem.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
                        Dim dtItemDetails As DataTable = objItem.ItemDetails()

                        Dim sku As String = ""
                        Dim asin As String = ""

                        If Not dtItemDetails Is Nothing AndAlso dtItemDetails.Rows.Count > 0 Then
                            asin = CCommon.ToString(dtItemDetails.Rows(0).Item("vcASIN"))
                            sku = CCommon.ToString(dtItemDetails.Rows(0).Item("vcItemSKU"))
                        End If

                        If (objOpportunity.Source = 1 Or objOpportunity.Source = 2 Or objOpportunity.Source = 3 Or objOpportunity.Source = 4) AndAlso String.IsNullOrEmpty(asin.Trim()) Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('Add ASIN to selected item in order to proceed.');", True)
                            Exit Sub
                        ElseIf String.IsNullOrEmpty(sku.Trim()) Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('Add SKU to selected item in order to proceed.');", True)
                            Exit Sub
                        End If

                        Dim objOpp As New MOpportunity
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("numOppID"))
                        objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        objOpp.OppItemCode = CCommon.ToLong(GetQueryStringVal("numOppItemID"))
                        objOpp.AddMappedItem(CCommon.ToLong(hdnItemID.Value))

                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CloseAndRefreshParent", "CloseAndRefreshParent();", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "KitChildItems", "alert('You need to add item manually to order as selected item is kit with child kits');", True)
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ItemRequired", "alert('Select an existing Item to map');", True)
                End If
            Else
                Dim strPage As String
                If radNonInventory.Checked = True Then
                    strPage = "Non-Inventory Items"
                ElseIf radInventoryItem.Checked = True Then
                    strPage = "Inventory Items"
                ElseIf radService.Checked = True Then
                    strPage = "Services"
                ElseIf radSerialized.Checked = True Then
                    strPage = "Serialized Items"
                ElseIf radKit.Checked = True Then
                    strPage = "Kits"
                ElseIf radAssembly.Checked = True Then
                    strPage = "Assembly"
                ElseIf radLot.Checked = True Then
                    strPage = "Lot No"
                End If

                Dim queryString As String = ""

                If CCommon.ToLong(GetQueryStringVal("numOppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numOppItemID")) > 0 Then
                    queryString = "&numOppID=" & CCommon.ToLong(GetQueryStringVal("numOppID")) & "&numOppItemID=" & CCommon.ToLong(GetQueryStringVal("numOppItemID"))
                ElseIf CCommon.ToBool(GetQueryStringVal("isFromAssembly")) AndAlso CCommon.ToLong(GetQueryStringVal("AssemblyItemID")) > 0 Then
                    queryString = "&isFromAssembly=" & CCommon.ToString(GetQueryStringVal("isFromAssembly")) & "&AssemblyItemID=" & CCommon.ToLong(GetQueryStringVal("AssemblyItemID"))
                End If

                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../Items/frmKitDetails.aspx?ItemCode=0&frm=" & strPage & queryString & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


End Class