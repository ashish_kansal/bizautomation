Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting
Imports System.IO
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Opportunities

Partial Public Class frmECommerceSettings
    Inherits BACRMPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            GetUserRightsForPage(MODULEID.Administration, 37)
            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                LoadDropDowns()
                LoadDetails()
                LoadDomainDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub
    Sub LoadDomainDetails()
        Dim dtTable As DataTable
        Dim objUserAccess As New UserAccess
        objUserAccess.DomainID = Session("DomainID")
        dtTable = objUserAccess.GetDomainDetails()
        If dtTable.Rows.Count > 0 Then
            txtSalesOrderTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcSalesOrderTabs"))
            txtSalesQuotesTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcSalesQuotesTabs"))
            txtItemPurchaseHistoryTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcItemPurchaseHistoryTabs"))
            txtItemsFrequentlyPurchasedTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcItemsFrequentlyPurchasedTabs"))
            txtOpenCasesTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcOpenCasesTabs"))
            txtOpenRMATabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcOpenRMATabs"))
            txtSupportTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcSupportTabs"))

            chkSalesOrderTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitSalesOrderTabs"))
            chkSalesQuotesTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitSalesQuotesTabs"))
            chkItemPurchaseHistoryTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitItemPurchaseHistoryTabs"))
            chkItemsFrequentlyPurchasedTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitItemsFrequentlyPurchasedTabs"))
            chkOpenCasesTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitOpenCasesTabs"))
            chkOpenRMATabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitOpenRMATabs"))
            chkSupportTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitSupportTabs"))
        End If
    End Sub
    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.SiteID = CCommon.ToInteger(ddlDefaultSite.SelectedValue)
            dtTable = objUserAccess.GetECommerceDetails()
            If dtTable.Rows.Count > 0 Then
                chkItemInStock.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitShowInStock"))
                'chkQuantOnHand.Checked = dtTable.Rows(0).Item("bitShowQOnHand")
                'If Not ddlColumns.Items.FindByValue(dtTable.Rows(0).Item("tintItemColumns")) Is Nothing Then
                '    ddlColumns.Items.FindByValue(dtTable.Rows(0).Item("tintItemColumns")).Selected = True
                'End If
                'If Not ddlCategory.Items.FindByValue(dtTable.Rows(0).Item("numDefaultCategory")) Is Nothing Then
                '    ddlCategory.Items.FindByValue(dtTable.Rows(0).Item("numDefaultCategory")).Selected = True
                'End If

                If Not ddlWareHouse.Items.FindByValue(dtTable.Rows(0).Item("numDefaultWareHouseID")) Is Nothing Then
                    ddlWareHouse.ClearSelection()
                    ddlWareHouse.Items.FindByValue(dtTable.Rows(0).Item("numDefaultWareHouseID")).Selected = True
                End If

                If Not ddlWarehouseAvailability.Items.FindByValue(dtTable.Rows(0).Item("tintWarehouseAvailability")) Is Nothing Then
                    ddlWarehouseAvailability.Items.FindByValue(dtTable.Rows(0).Item("tintWarehouseAvailability")).Selected = True
                End If
                chkDisplayQtyAvailable.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitDisplayQtyAvailable"))
                ' chkHideNewUsers.Checked = dtTable.Rows(0).Item("bitHideNewUsers")
                chkCreditStatus.Checked = dtTable.Rows(0).Item("bitCheckCreditStatus")
                chkAutoSelectWarehouse.Checked = dtTable.Rows(0).Item("bitAutoSelectWarehouse")

                txtThankYouRewriteUrl.Text = dtTable.Rows(0).Item("vcRedirectThankYouUrl")

                If Not ddlRelationship.Items.FindByValue(dtTable.Rows(0).Item("numRelationshipId")) Is Nothing Then
                    ddlRelationship.ClearSelection()
                    ddlRelationship.Items.FindByValue(dtTable.Rows(0).Item("numRelationshipId")).Selected = True
                End If

                If Not ddlDefaultClass.Items.FindByValue(dtTable.Rows(0).Item("numDefaultClass")) Is Nothing Then
                    ddlDefaultClass.ClearSelection()
                    ddlDefaultClass.Items.FindByValue(dtTable.Rows(0).Item("numDefaultClass")).Selected = True
                End If

                If Not ddlProfile.Items.FindByValue(dtTable.Rows(0).Item("numProfileId")) Is Nothing Then
                    ddlProfile.ClearSelection()
                    ddlProfile.Items.FindByValue(dtTable.Rows(0).Item("numProfileId")).Selected = True
                End If

                chkHidePrice.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitHidePriceBeforeLogin"))
                chkHideAddtoCart.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitHideAddtoCart"))
                chkShowPromoDetailsLink.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitShowPromoDetailsLink"))
                txtPreSellPage.Text = Convert.ToString(dtTable.Rows(0).Item("vcPreSellUp"))
                txtPostUpSell.Text = Convert.ToString(dtTable.Rows(0).Item("vcPostSellUp"))
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = 176
                'Modified By:Sachin Sadhu||Date:20thJune2014
                'Purpose    :Sales/Purchase Type Wise Order Status
                dtOrderStatus = objCommon.GetMasterListItemsWithRights(1)
                'end of code

                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = 176

                'Modified By:Sachin Sadhu||Date:20thJune2014
                'Purpose    :Sales/Purchase Type Wise Order Status
                dtFailedOrderStatus = objCommon.GetMasterListItemsWithRights(1)
                'end of code
                ddlStatus.DataSource = dtOrderStatus
                ddlStatus.DataTextField = "vcData"
                ddlStatus.DataValueField = "numListItemID"
                ddlStatus.DataBind()
                ddlStatus.Items.Insert(0, New ListItem("--Select One --", "0"))

                chkAuthCreditCard.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitAuthOnlyCreditCard"))
                If chkAuthCreditCard.Checked Then
                    If ddlStatus.Items.FindByValue(dtTable.Rows(0).Item("numAuthrizedOrderStatus")) IsNot Nothing Then
                        ddlStatus.ClearSelection()
                        ddlStatus.Items.FindByValue(dtTable.Rows(0).Item("numAuthrizedOrderStatus")).Selected = True
                    End If
                End If

                chkEmailStatus.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitSendEmail"))
                txtGoogleMerchantID.Text = CCommon.ToString(dtTable.Rows(0).Item("vcGoogleMerchantID"))
                txtGoogleMerchantKey.Text = CCommon.ToString(dtTable.Rows(0).Item("vcGoogleMerchantKey"))
                cbSandbox.Checked = CCommon.ToBool(dtTable.Rows(0).Item("IsSandbox"))

                txtPaypalUserName.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPaypalUserName"))
                txtPaypalPassword.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPaypalPassword"))
                txtPaypalSignature.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPaypalSignature"))
                chkPaypalSandbox.Checked = CCommon.ToBool(dtTable.Rows(0).Item("IsPaypalSandbox"))

                chkSkipStep2.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitSkipStep2"))
                chkCategorySettings.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitDisplayCategory"))
                chkShowPriceUsingPriceLevel.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitShowPriceUsingPriceLevel"))
                chkEnableSorting.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitEnableSecSorting"))
                'Author:Sachin Sadhu||Date:20-Jan-2014
                'Purpose:To add settings to change Price mode either ListPrice or Retail price 
                chkSortPriceMode.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitSortPriceMode"))
                'End Of code by Sachin
                'Author:Sachin Sadhu||Date:21-Jan-2014
                'Purpose:Dynamically apply Page size
                txtPageSize.Text = CCommon.ToInteger(dtTable.Rows(0).Item("numPageSize"))
                txtPageVariant.Text = CCommon.ToInteger(dtTable.Rows(0).Item("numPageVariant"))
                'End of Code by Sachin

                chkPreUpSell.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitPreSellUp"))
                chkPostSell.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitPostSellUp"))
                chkEnableElasticSearch.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitElasticSearch"))

                If Not ddlPriceLevel.Items.FindByValue(dtTable.Rows(0).Item("tintPreLoginProceLevel")) Is Nothing Then
                    ddlPriceLevel.ClearSelection()
                    ddlPriceLevel.Items.FindByValue(dtTable.Rows(0).Item("tintPreLoginProceLevel")).Selected = True
                End If
        
                BindGrid()

            Else
                If Not ddlDefaultSite.SelectedItem Is Nothing Then
                    ShowMessage("ECommerece settings are not found for Site """ & CCommon.ToString(ddlDefaultSite.SelectedItem.Text) & """")
                Else
                    ShowMessage("ECommerece settings are not found")
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim objUserAccess As New UserAccess
            Dim objGoogleCheckout As New GoogleCheckout
            objCommon = New CCommon
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.boolShowInStock = chkItemInStock.Checked
            objUserAccess.boolCheckCreditStatus = chkCreditStatus.Checked
            objUserAccess.boolAutoSelectWarehouse = chkAutoSelectWarehouse.Checked
            If ddlDefaultType.SelectedValue = 1 Then
                objUserAccess.WareHouseID = ddlDefaultValues.SelectedValue
            ElseIf ddlDefaultType.SelectedValue = 2 Then
                objUserAccess.DefaultClass = ddlDefaultValues.SelectedValue
            ElseIf ddlDefaultType.SelectedValue = 3 Then
                objUserAccess.Relationship = ddlDefaultValues.SelectedValue
            ElseIf ddlDefaultType.SelectedValue = 4 Then
                objUserAccess.ProfileID = ddlDefaultValues.SelectedValue
            ElseIf ddlDefaultType.SelectedValue = 5 Then
                objUserAccess.PreLoginPriceLevel = ddlDefaultValues.SelectedValue
            End If
            objUserAccess.WareHouseAvailability = ddlWarehouseAvailability.SelectedValue
            objUserAccess.DisplayQtyAvailable = chkDisplayQtyAvailable.Checked
            objUserAccess.AuthOnlyCreditCard = chkAuthCreditCard.Checked
            If chkAuthCreditCard.Checked Then
                objUserAccess.OrderStatusID = ddlStatus.SelectedValue
            End If

            objUserAccess.HidePrice = chkHidePrice.Checked
            objUserAccess.HideAddtoCart = chkHideAddtoCart.Checked
            objUserAccess.IsShowPromoDetailsLink = chkShowPromoDetailsLink.Checked
            objUserAccess.EmailStatus = chkEmailStatus.Checked

            If objGoogleCheckout.CheckGoogleCheckoutConfiguration(txtGoogleMerchantID.Text, txtGoogleMerchantKey.Text, cbSandbox.Checked) Then
                objUserAccess.GoogleMerchantID = txtGoogleMerchantID.Text
                objUserAccess.GoogleMerchantKey = txtGoogleMerchantKey.Text
                objUserAccess.IsSandbox = cbSandbox.Checked
            Else
                objUserAccess.GoogleMerchantID = ""
                objUserAccess.GoogleMerchantKey = ""
                objUserAccess.IsSandbox = 1
            End If
            objUserAccess.SiteID = ddlDefaultSite.SelectedValue
            objUserAccess.PaypalUserName = txtPaypalUserName.Text.Trim()
            objUserAccess.PaypalPassword = txtPaypalPassword.Text.Trim()
            objUserAccess.PaypalSignature = txtPaypalSignature.Text.Trim()
            objUserAccess.IsPaypalSandbox = CCommon.ToBool(chkPaypalSandbox.Checked)
            objUserAccess.IsSkipStep2 = chkSkipStep2.Checked
            objUserAccess.IsDisplayCategories = chkCategorySettings.Checked
            objUserAccess.IsShowPriceUsingPriceLevel = chkShowPriceUsingPriceLevel.Checked
            objUserAccess.IsEnableSorting = chkEnableSorting.Checked
            'Author:Sachin Sadhu||Date:20-Jan-2014
            'Purpose:To add settings to change Price mode either ListPrice or Retail price 
            objUserAccess.IsChangeSortPriceMode = chkSortPriceMode.Checked
            'End of code by sachin

            'Author:Sachin Sadhu||Date:21-Jan-2014
            'Purpose:Dynamically apply Page size
            If CCommon.ToInteger(txtPageSize.Text) > 0 Then
                objUserAccess.numPageSize = CCommon.ToInteger(txtPageSize.Text)
            Else
                objUserAccess.numPageSize = 15
            End If
            If CCommon.ToInteger(txtPageVariant.Text) > 0 Then
                objUserAccess.numPageVariant = CCommon.ToInteger(txtPageVariant.Text)
            Else
                objUserAccess.numPageVariant = 5
            End If

            'End of Code

            objUserAccess.bitPreUpSell = CCommon.ToBool(chkPreUpSell.Checked)
            objUserAccess.bitPostUpSell = CCommon.ToBool(chkPostSell.Checked)
            objUserAccess.vcPreSellUp = txtPreSellPage.Text
            objUserAccess.vcPostSellUp = txtPostUpSell.Text
            objUserAccess.vcRedirectThankYouUrl = txtThankYouRewriteUrl.Text

            objUserAccess.vcSalesOrderTabs = txtSalesOrderTabs.Text
            objUserAccess.vcSalesQuotesTabs = txtSalesQuotesTabs.Text
            objUserAccess.vcItemPurchaseHistoryTabs = txtItemPurchaseHistoryTabs.Text
            objUserAccess.vcItemsFrequentlyPurchasedTabs = txtItemsFrequentlyPurchasedTabs.Text
            objUserAccess.vcOpenCasesTabs = txtOpenCasesTabs.Text
            objUserAccess.vcOpenRMATabs = txtOpenRMATabs.Text
            objUserAccess.vcSupportTabs = txtSupportTabs.Text

            objUserAccess.bitSalesOrderTabs = chkSalesOrderTabs.Checked
            objUserAccess.bitSalesQuotesTabs = chkSalesQuotesTabs.Checked
            objUserAccess.bitItemPurchaseHistoryTabs = chkItemPurchaseHistoryTabs.Checked
            objUserAccess.bitItemsFrequentlyPurchasedTabs = chkItemsFrequentlyPurchasedTabs.Checked
            objUserAccess.bitOpenCasesTabs = chkOpenCasesTabs.Checked
            objUserAccess.bitOpenRMATabs = chkOpenRMATabs.Checked
            objUserAccess.bitSupportTabs = chkSupportTabs.Checked
            objUserAccess.bitElasticSearch = chkEnableElasticSearch.Checked

            objUserAccess.UpdateECommSettings()
            If ddlDefaultSite.SelectedValue > 0 Then
                objUserAccess.byteMode = 2
                objUserAccess.StrItems = GetItems()
                objUserAccess.ManageEComPaymentConfig(ddlDefaultSite.SelectedValue)
            End If

            LoadDetails()
            LoadDomainDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub ddlDefaultType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDefaultType.SelectedIndexChanged
        Try
            ddlDefaultValues.ClearSelection()
            ddlDefaultValues.Items.Clear()
            If ddlDefaultType.SelectedValue = 1 Then
                'Warehouse
                Dim objItems As New CItems
                objItems.byteMode = 0
                objItems.DomainID = Session("DomainID")

                ddlDefaultValues.DataValueField = "numWareHouseID"
                ddlDefaultValues.DataTextField = "vcWareHouse"
                ddlDefaultValues.DataSource = objItems.GetWareHouses
                ddlDefaultValues.DataBind()
                ddlDefaultValues.Items.Insert(0, "--Select One--")
                ddlDefaultValues.Items.FindByText("--Select One--").Value = 0
                If Not ddlDefaultValues.Items.FindByValue(ddlWareHouse.SelectedValue) Is Nothing Then
                    ddlDefaultValues.ClearSelection()
                    ddlDefaultValues.Items.FindByValue(ddlWareHouse.SelectedValue).Selected = True
                End If
                lblDynamicToolTip.ToolTip = "Any order created from Web store will use default warehouse set here, 'Auto Select' will be applicable in following situation For items belonging to multiple warehouses, if default warehouse has 0 on-hand, pull from 1st warehouse on list with sufficient qty-on-hand to satisfy order."
            ElseIf ddlDefaultType.SelectedValue = 2 Then
                'Class (New Customers from site)
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlDefaultValues.DataTextField = "ClassName"
                    ddlDefaultValues.DataValueField = "numChildClassID"
                    ddlDefaultValues.DataSource = dtClass
                    ddlDefaultValues.DataBind()
                End If

                ddlDefaultValues.Items.Insert(0, New ListItem("--Select One --", "0"))
                If Not ddlDefaultValues.Items.FindByValue(ddlDefaultClass.SelectedValue) Is Nothing Then
                    ddlDefaultValues.ClearSelection()
                    ddlDefaultValues.Items.FindByValue(ddlDefaultClass.SelectedValue).Selected = True
                End If
                lblDynamicToolTip.ToolTip = "Allow you to set default accounting class for new customers get registered on selected site."
            ElseIf ddlDefaultType.SelectedValue = 3 Then
                'Relationship (New Customers from site
                objCommon.sb_FillComboFromDBwithSel(ddlDefaultValues, 5, Session("DomainID"))
                If Not ddlDefaultValues.Items.FindByValue(ddlRelationship.SelectedValue) Is Nothing Then
                    ddlDefaultValues.ClearSelection()
                    ddlDefaultValues.Items.FindByValue(ddlRelationship.SelectedValue).Selected = True
                End If
                lblDynamicToolTip.ToolTip = "Any relationship created from Web store will create record using default relationship set here."
            ElseIf ddlDefaultType.SelectedValue = 4 Then
                'Profile (New Customers from site)
                objCommon.sb_FillComboFromDBwithSel(ddlDefaultValues, 21, Session("DomainID"))
                If Not ddlDefaultValues.Items.FindByValue(ddlProfile.SelectedValue) Is Nothing Then
                    ddlDefaultValues.ClearSelection()
                    ddlDefaultValues.Items.FindByValue(ddlProfile.SelectedValue).Selected = True
                End If
                lblDynamicToolTip.ToolTip = "Any relationship created from Web store will create record using default profile set here."
            ElseIf ddlDefaultType.SelectedValue = 5 Then
                'Pre-login Price
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")
                Dim dtPriceLevel As DataTable = objCommon.GetNamedPriceLevel()

                If Not dtPriceLevel Is Nothing AndAlso dtPriceLevel.Rows.Count > 0 Then
                    ddlDefaultValues.DataTextField = "Value"
                    ddlDefaultValues.DataValueField = "Id"
                    ddlDefaultValues.DataSource = dtPriceLevel
                    ddlDefaultValues.DataBind()
                End If

                ddlDefaultValues.Items.Insert(0, New ListItem("List Price", "0"))
                If Not ddlDefaultValues.Items.FindByValue(ddlPriceLevel.SelectedValue) Is Nothing Then
                    ddlDefaultValues.ClearSelection()
                    ddlDefaultValues.Items.FindByValue(ddlPriceLevel.SelectedValue).Selected = True
                End If
                lblDynamicToolTip.ToolTip = ""
            Else
                lblDynamicToolTip.ToolTip = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub

    Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            CCommon.AddColumnsToDataTable(dt, "numPaymentMethodId,bitEnable,numBizDocId,numBizdocStatus,numMirrorBizDocTemplateId,numOrderStatus,numFailedOrderStatus,numRecordOwner,numAssignTo")
            Dim IsAllFieldsSelected As Boolean = True

            For Each Item As DataGridItem In dgPaymentGateway.Items
                Dim dr As DataRow = dt.NewRow
                dr("numPaymentMethodId") = CCommon.ToLong(Item.Cells(0).Text)

                Dim ddlSalesOrder As New DropDownList
                Dim ddlMirrorBizdocStatus As New DropDownList
                Dim ddlOrderStatus As New DropDownList
                Dim ddlFailedOrderStatus As New DropDownList
                Dim ddlRecordOwner As New DropDownList
                Dim ddlAssignTo As New DropDownList
                Dim ddlBizdocStatus As New DropDownList
                Dim chkEnable As New CheckBox

                ddlSalesOrder = CType(Item.FindControl("ddlSalesOrder"), DropDownList)
                ddlMirrorBizdocStatus = CType(Item.FindControl("ddlMirrorBizDocTemplate"), DropDownList)
                ddlOrderStatus = CType(Item.FindControl("ddlOrderStatus"), DropDownList)
                ddlFailedOrderStatus = CType(Item.FindControl("ddlFailedOrderStatus"), DropDownList)
                ddlRecordOwner = CType(Item.FindControl("ddlRecordOwner"), DropDownList)
                ddlAssignTo = CType(Item.FindControl("ddlAssignTo"), DropDownList)
                ddlBizdocStatus = CType(Item.FindControl("ddlBizdocStatus"), DropDownList)
                chkEnable = CType(Item.FindControl("chkEnable"), CheckBox)

                dr("numBizDocStatus") = ddlBizdocStatus.SelectedValue
                dr("numMirrorBizDocTemplateId") = ddlMirrorBizdocStatus.SelectedValue
                dr("numOrderStatus") = ddlOrderStatus.SelectedValue
                dr("numFailedOrderStatus") = ddlFailedOrderStatus.SelectedValue
                dr("numRecordOwner") = ddlRecordOwner.SelectedValue
                dr("numAssignTo") = ddlAssignTo.SelectedValue
                dr("numBizDocId") = ddlSalesOrder.SelectedValue

                If (chkEnable.Checked) Then
                    If ddlSalesOrder.SelectedValue <> 0 And ddlBizdocStatus.SelectedValue <> 0 And ddlFailedOrderStatus.SelectedValue <> 0 And ddlRecordOwner.SelectedValue <> 0 And ddlAssignTo.SelectedValue <> 0 Then
                        dr("bitEnable") = 1

                    Else
                        dr("bitEnable") = 0
                        IsAllFieldsSelected = False
                    End If
                Else
                    dr("bitEnable") = 0
                End If

                dt.Rows.Add(dr)
            Next

            If Not IsAllFieldsSelected Then
                ShowMessage("All fields are mendatory for payment method you want to enable for e-commerce. Please set them and try again.")
            End If

            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "Item"
            Return ds.GetXml()
        Catch ex As Exception
            Throw
        End Try
    End Function


    Sub LoadDropDowns()
        Try
            Dim objItems As New CItems
            objItems.byteMode = 0
            objItems.DomainID = Session("DomainID")

            ddlWareHouse.DataValueField = "numWareHouseID"
            ddlWareHouse.DataTextField = "vcWareHouse"
            ddlWareHouse.DataSource = objItems.GetWareHouses
            ddlWareHouse.DataBind()
            ddlWareHouse.Items.Insert(0, "--Select One--")
            ddlWareHouse.Items.FindByText("--Select One--").Value = 0

            objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))

            Dim dtTable As DataTable
            Dim objSite As New Sites
            objSite.DomainID = CCommon.ToLong(Session("DomainID"))
            dtTable = objSite.GetSites()

            ddlDefaultSite.DataSource = dtTable
            ddlDefaultSite.DataTextField = "vcSiteName"
            ddlDefaultSite.DataValueField = "numSiteID"
            ddlDefaultSite.DataBind()

            'Accounting Class
            Dim dtClass As DataTable
            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.Mode = 1
            dtClass = objAdmin.GetClass()

            If dtClass.Rows.Count > 0 Then
                ddlDefaultClass.DataTextField = "ClassName"
                ddlDefaultClass.DataValueField = "numChildClassID"
                ddlDefaultClass.DataSource = dtClass
                ddlDefaultClass.DataBind()
            End If

            ddlDefaultClass.Items.Insert(0, New ListItem("--Select One --", "0"))

            FillPriceLevel()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub FillPriceLevel()
        Try
            Dim objCommon As New CCommon
            objCommon.DomainID = Session("DomainID")
            Dim dtPriceLevel As DataTable = objCommon.GetNamedPriceLevel()

            If Not dtPriceLevel Is Nothing AndAlso dtPriceLevel.Rows.Count > 0 Then
                ddlPriceLevel.DataTextField = "Value"
                ddlPriceLevel.DataValueField = "Id"
                ddlPriceLevel.DataSource = dtPriceLevel
                ddlPriceLevel.DataBind()
            End If

            ddlPriceLevel.Items.Insert(0, New ListItem("List Price", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Dim dtBizDoc As DataTable
    Dim dtMirrorBizDocTemplate As DataTable
    Dim dtOrderStatus As DataTable
    Dim dtBizdocStatus As DataTable
    Dim dtFailedOrderStatus As DataTable
    Dim dtUserList As DataTable
    Sub LoadDataTables()
        Try

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.BizDocType = 1
            If dtBizDoc Is Nothing Then
                dtBizDoc = objCommon.GetBizDocType
                For Each dr As DataRow In dtBizDoc.Rows
                    If dr("numListItemID") = CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) Then
                        dr("vcData") = dr("vcData") & " - Authoritative"
                    End If
                Next
            End If

            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = enmBizDocTemplate_BizDocID.SalesOrder
            objOppBizDoc.OppType = 1

            dtMirrorBizDocTemplate = objOppBizDoc.GetBizDocTemplateList()


            If dtUserList Is Nothing Then
                dtUserList = objCommon.ConEmpList(Session("DomainId"), False, 0)
            End If
            If dtOrderStatus Is Nothing Then
                objCommon.ListID = 176
                'Modified By :Sachin Sadhu||Date:20thJune2014
                'Purpose     :Sales/Purchase Type wise OrderStatus
                dtOrderStatus = objCommon.GetMasterListItemsWithRights(1)
                'end of code
            End If
            If dtFailedOrderStatus Is Nothing Then
                objCommon.ListID = 176
                'Modified By :Sachin Sadhu||Date:20thJune2014
                'Purpose     :Sales/Purchase Type wise OrderStatus
                dtFailedOrderStatus = objCommon.GetMasterListItemsWithRights(1)
                'end of code
            End If
            If dtBizdocStatus Is Nothing Then
                objCommon.ListID = 11

                dtBizdocStatus = objCommon.GetMasterListItemsWithRights()
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPaymentGateway_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaymentGateway.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim ddlSalesOrder As DropDownList
                Dim ddlMirrorBizDocTemplate As DropDownList
                Dim ddlOrderStatus As DropDownList
                Dim ddlBizdocStatus As DropDownList
                Dim ddlFailedOrderStatus As DropDownList
                Dim ddlRecordOwner As DropDownList
                Dim ddlAssignTo As DropDownList
                ddlSalesOrder = CType(e.Item.FindControl("ddlSalesOrder"), DropDownList)
                ddlMirrorBizDocTemplate = CType(e.Item.FindControl("ddlMirrorBizDocTemplate"), DropDownList)

                ddlOrderStatus = CType(e.Item.FindControl("ddlOrderStatus"), DropDownList)
                ddlBizdocStatus = CType(e.Item.FindControl("ddlBizdocStatus"), DropDownList)
                ddlFailedOrderStatus = CType(e.Item.FindControl("ddlFailedOrderStatus"), DropDownList)
                ddlRecordOwner = CType(e.Item.FindControl("ddlRecordOwner"), DropDownList)
                ddlAssignTo = CType(e.Item.FindControl("ddlAssignTo"), DropDownList)

                Dim listItem As ListItem
                For Each drRow As DataRow In dtBizDoc.Rows
                    If CCommon.ToString(dr("strType")) = "Sales Opportunity" Then
                        If drRow("numListItemId") <> enmBizDocTemplate_BizDocID.Invoice_1 Then
                            listItem = New ListItem()
                            listItem.Text = drRow("vcData")
                            listItem.Value = drRow("numListItemId")
                            ddlSalesOrder.Items.Add(listItem)
                        End If
                    Else
                        If drRow("numListItemId") = enmBizDocTemplate_BizDocID.Invoice_1 Then
                            listItem = New ListItem()
                            listItem.Text = drRow("vcData")
                            listItem.Value = drRow("numListItemId")
                            ddlSalesOrder.Items.Add(listItem)
                        End If
                    End If
                Next
                'ddlSalesOrder.Items.Insert(0, New ListItem("--Select One --", "0"))

                If Not ddlSalesOrder.Items.FindByValue(dr("numBizdocId")) Is Nothing Then
                    ddlSalesOrder.Items.FindByValue(dr("numBizdocId")).Selected = True
                End If

                If CCommon.ToString(dr("strType")) = "Sales Opportunity" Then
                    'ddlMirrorBizDocTemplate.Items.Insert(0, New ListItem("--Select Template--", "0"))
                    ddlMirrorBizDocTemplate.Items.Insert(1, New ListItem("Sales Opportunity", "1"))
                Else
                    ddlMirrorBizDocTemplate.DataTextField = "vcTemplateName"
                    ddlMirrorBizDocTemplate.DataValueField = "numBizdocTempId"
                    ddlMirrorBizDocTemplate.DataSource = dtMirrorBizDocTemplate
                    ddlMirrorBizDocTemplate.DataBind()
                    ddlMirrorBizDocTemplate.Items.Insert(0, New ListItem("--Select Template--", "0"))
                End If

                If Not ddlMirrorBizDocTemplate.Items.FindByValue(dr("numMirrorBizdocTemplateId")) Is Nothing Then
                    ddlMirrorBizDocTemplate.Items.FindByValue(dr("numMirrorBizdocTemplateId")).Selected = True
                End If

                ddlOrderStatus.DataTextField = "vcData"
                ddlOrderStatus.DataValueField = "numListItemID"
                ddlOrderStatus.DataSource = dtOrderStatus
                ddlOrderStatus.DataBind()
                ddlOrderStatus.Items.Insert(0, New ListItem("--Select One --", "0"))
                If Not ddlOrderStatus.Items.FindByValue(dr("numOrderStatus")) Is Nothing Then
                    ddlOrderStatus.Items.FindByValue(dr("numOrderStatus")).Selected = True
                End If

                ddlFailedOrderStatus.DataTextField = "vcData"
                ddlFailedOrderStatus.DataValueField = "numListItemID"
                ddlFailedOrderStatus.DataSource = dtOrderStatus
                ddlFailedOrderStatus.DataBind()
                ddlFailedOrderStatus.Items.Insert(0, New ListItem("--Select One --", "0"))
                If Not ddlFailedOrderStatus.Items.FindByValue(dr("numFailedOrderStatus")) Is Nothing Then
                    ddlFailedOrderStatus.Items.FindByValue(dr("numFailedOrderStatus")).Selected = True
                End If
                'Added by:sachin sadhu||Date:24thJune2013
                'Purpose:bind bizdoc status as per selection of bizdoc type
                Dim dvSource As DataView = dtBizdocStatus.DefaultView
                dvSource.RowFilter = "numListType IN (0," + CCommon.ToString(ddlSalesOrder.SelectedValue) + ")"

                ddlBizdocStatus.DataTextField = "vcData"
                ddlBizdocStatus.DataValueField = "numListItemID"
                ' ddlBizdocStatus.DataSource = dtBizdocStatus
                ddlBizdocStatus.DataSource = dvSource
                ddlBizdocStatus.DataBind()
                'end of Code
                ddlBizdocStatus.Items.Insert(0, New ListItem("--Select One --", "0"))
                If Not ddlBizdocStatus.Items.FindByValue(dr("numBizdocStatus")) Is Nothing Then
                    ddlBizdocStatus.Items.FindByValue(dr("numBizdocStatus")).Selected = True
                End If

                ddlRecordOwner.DataSource = dtUserList
                ddlRecordOwner.DataTextField = "vcUserName"
                ddlRecordOwner.DataValueField = "numContactID"
                ddlRecordOwner.DataBind()
                ddlRecordOwner.Items.Insert(0, New ListItem("--Select One --", "0"))
                If Not ddlRecordOwner.Items.FindByValue(dr("numRecordOwner")) Is Nothing Then
                    ddlRecordOwner.Items.FindByValue(dr("numRecordOwner")).Selected = True
                End If

                ddlAssignTo.DataSource = dtUserList
                ddlAssignTo.DataTextField = "vcUserName"
                ddlAssignTo.DataValueField = "numContactID"
                ddlAssignTo.DataBind()
                ddlAssignTo.Items.Insert(0, New ListItem("--Select One --", "0"))

                If Not ddlAssignTo.Items.FindByValue(dr("numAssignTo")) Is Nothing Then
                    ddlAssignTo.Items.FindByValue(dr("numAssignTo")).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            If ddlDefaultSite.SelectedValue > 0 Then

                LoadDataTables()
                Dim objUserAccess As New UserAccess
                Dim dtPaymentGateWay As New DataTable
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.byteMode = 1
                dtPaymentGateWay = objUserAccess.ManageEComPaymentConfig(ddlDefaultSite.SelectedValue)
                dgPaymentGateway.DataSource = dtPaymentGateWay
                dgPaymentGateway.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Added by :Sachin Sadhu||Date:24thJune2014
    'Purpose: BizDoc Type Wise BizDoc Status
    Protected Sub ddlSalesOrder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlBizdocStatus As New DropDownList
            Dim ddlSalesOrder As New DropDownList
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 11
            For Each item As DataGridItem In dgPaymentGateway.Items
                ddlSalesOrder = CType(item.FindControl("ddlSalesOrder"), DropDownList)
                dtBizdocStatus = objCommon.GetMasterListItemsWithRights(CCommon.ToLong(ddlSalesOrder.SelectedValue))
                ddlBizdocStatus = CType(item.FindControl("ddlBizdocStatus"), DropDownList)
                If ddlBizdocStatus.SelectedIndex <= 0 Then
                    ddlBizdocStatus.Focus()
                    ddlBizdocStatus.DataTextField = "vcData"
                    ddlBizdocStatus.DataValueField = "numListItemID"
                    ddlBizdocStatus.DataSource = dtBizdocStatus
                    ddlBizdocStatus.DataBind()
                    ddlBizdocStatus.Items.Insert(0, New ListItem("--Select One --", "0"))
                End If


            Next

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try



    End Sub
    'end of code
#Region "ExtraCode"
    'objCommon = New CCommon
    'objCommon.DomainID = Session("DomainID")ddlBizdocStatus
    'objCommon.BizDocType = 1
    'If dtBizDoc Is Nothing Then
    '    dtBizDoc = objCommon.GetBizDocType
    '    For Each dr As DataRow In dtBizDoc.Rows
    '        If dr("numListItemID") = CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) Then
    '            dr("vcData") = dr("vcData") & " - Authoritative"
    '        End If
    '    Next
    'End If

    'If dtUserList Is Nothing Then
    '    dtUserList = objCommon.ConEmpList(Session("DomainId"), False, 0)
    'End If
    'If dtOrderStatus Is Nothing Then
    '    objCommon.ListID = 176
    '    dtOrderStatus = objCommon.GetMasterListItemsWithRights()
    'End If
#End Region

    Private Sub ddlDefaultSite_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDefaultSite.SelectedIndexChanged
        Try
            LoadDetails()
            LoadDomainDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex)
        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            DisplayError(ex)
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As Exception)
        Try
            TryCast(Me.Master, ECommerceMenuMaster).ThrowError(exception)
        Catch ex As Exception

        End Try
    End Sub
End Class