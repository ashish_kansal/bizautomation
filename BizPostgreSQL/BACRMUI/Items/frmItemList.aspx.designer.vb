'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Items

    Partial Public Class frmItemList

        '''<summary>
        '''divMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''litMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

        '''<summary>
        '''divSuccessMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divSuccessMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''litSuccessmessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents litSuccessmessage As Global.System.Web.UI.WebControls.Literal

        '''<summary>
        '''litSummaryMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents litSummaryMessage As Global.System.Web.UI.WebControls.Literal

        '''<summary>
        '''divDropDowns control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divDropDowns As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnFilter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnFilter As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tdMain control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tdMain As Global.System.Web.UI.WebControls.PlaceHolder

        '''<summary>
        '''chkArchivedItems control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkArchivedItems As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''radCmbSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radCmbSearch As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''btnGo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnGotoRecord control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGotoRecord As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnBarcode control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnBarcode As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''btnNew control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnNew As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''ddlItemWarehouse control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlItemWarehouse As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''imgCartAdd control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents imgCartAdd As Global.System.Web.UI.WebControls.ImageButton

        '''<summary>
        '''btnOpenOrder control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnOpenOrder As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnExport control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnExport As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''hplAdjustInv control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hplAdjustInv As Global.System.Web.UI.WebControls.HyperLink

        '''<summary>
        '''btnDelete control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''lblList control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblList As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''bizPager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents bizPager As Global.Wuqi.Webdiyer.AspNetPager

        '''<summary>
        '''tblMultiSelect control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblMultiSelect As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''btnMultiSelectAdd control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnMultiSelectAdd As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnMultiSelectAddFinish control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnMultiSelectAddFinish As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''imgbtnShoppingCart control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents imgbtnShoppingCart As Global.System.Web.UI.WebControls.ImageButton

        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

        '''<summary>
        '''ddlFilterWarehouse control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlFilterWarehouse As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlAisle control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlAisle As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlRack control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlRack As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlShelf control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlShelf As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlBin control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlBin As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''hdnAppliedByFilter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnAppliedByFilter As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''btnFilterByWarehouse control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnFilterByWarehouse As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnSaveInventory control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSaveInventory As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''gvSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents gvSearch As Global.Telerik.Web.UI.RadGrid

        '''<summary>
        '''divItemAttributes control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divItemAttributes As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnSaveCloseAttributes control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSaveCloseAttributes As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnCloseAttr control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnCloseAttr As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''lblAttributeError control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblAttributeError As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''rptAttributes control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rptAttributes As Global.System.Web.UI.WebControls.Repeater

        '''<summary>
        '''txtDelItemIds control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtDelItemIds As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSortColumn control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortColumn As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSortOrder control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortOrder As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtTotalPageItems control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalPageItems As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtTotalRecordsItems control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalRecordsItems As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSortChar control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortChar As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''btnGo1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo1 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''txtCurrrentPage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtCurrrentPage As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''hdnData control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnData As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''hdnItemCnt control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnItemCnt As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''hdnItemCodes control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnItemCodes As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''hdnOppType control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnOppType As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''btnReload control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnReload As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''txtGridColumnFilter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtGridColumnFilter As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''hdnMultiSelectItemcodes control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnMultiSelectItemcodes As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''hdnItemGrpId control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnItemGrpId As Global.System.Web.UI.WebControls.HiddenField
    End Class
End Namespace
