Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Namespace BACRM.UserInterface.Items

    Public Class frmVendorList
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents dgVendor As System.Web.UI.WebControls.DataGrid
        Protected WithEvents txtSearch As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnGo As System.Web.UI.WebControls.Button
        Protected WithEvents ddlCompany As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents txtHidden As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtVendorId As System.Web.UI.WebControls.TextBox

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Session("Help") = "Item"
                BindGrid()
            End If
            btnAdd.Attributes.Add("onclick", "return Add()")
            btnSave.Attributes.Add("onclick", "return Save()")
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Dim objOpportunities As New COpportunities
            With objOpportunities
                .DomainID = Session("DomainID")
                .CompFilter = Trim(txtSearch.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = objOpportunities.ListCustomer().Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
                ddlCompany.Items.Insert(0, "--Select One--")
                ddlCompany.Items.FindByText("--Select One--").Value = 0
            End With
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Dim objItems As New CItems
            objItems.DivisionID = ddlCompany.SelectedItem.Value
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemCode")
            objItems.AddVendor()
            BindGrid()
        End Sub
        Sub BindGrid()
            Dim objItems As New CItems
            Dim dtVendors As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemCode")
            dtVendors = objItems.GetVendors
            dgVendor.DataSource = dtVendors
            dgVendor.DataBind()
        End Sub

        Private Sub dgVendor_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVendor.ItemCommand
            If e.CommandName = "Delete" Then
                Dim objItems As New CItems
                objItems.VendorID = e.Item.Cells(0).Text
                objItems.DomainID = Session("DomainID")
                If objItems.DeleteVendors = False Then
                    litMessage.Text = "Dependent Records Exists. Cannot Be deleted"
                Else
                    BindGrid()
                End If
            End If
        End Sub

        Private Sub dgVendor_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVendor.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                Dim rad As RadioButton
                rad = e.Item.FindControl("radPreffered")
                rad.Attributes.Add("onclick", "return CheckRadio('" & rad.ClientID & "')")
                Dim lbl As Label
                lbl = e.Item.FindControl("lblvendorID")
                If Session("VendorId") = lbl.Text Then
                    rad.Checked = True
                End If
            End If
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Dim dtVendorDetails As New DataTable
            Dim i As Integer
            Dim dr As DataRow
            dtVendorDetails.Columns.Add("VendorID")
            dtVendorDetails.Columns.Add("PartNo")
            dtVendorDetails.Columns.Add("monCost")
            Dim str As String() = txtHidden.Text.Split("~")
            Dim strValues As String()
            Dim strEditedfldlst As String
            For i = 0 To str.Length - 2
                strValues = str(i).Split(",")
                dr = dtVendorDetails.NewRow
                dr("VendorID") = strValues(0)
                dr("PartNo") = strValues(1)
                dr("monCost") = strValues(2)
                dtVendorDetails.Rows.Add(dr)
            Next
            dtVendorDetails.TableName = "Table"
            Dim ds As New DataSet
            ds.Tables.Add(dtVendorDetails)
            strEditedfldlst = ds.GetXml()
            ds.Tables.Remove(dtVendorDetails)

            Dim objItems As New CItems
            objItems.strFieldList = strEditedfldlst
            objItems.VendorID = IIf(txtVendorId.Text = "", 0, txtVendorId.Text)
            objItems.ItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemCode")
            objItems.UpdateVendor()

            Session("VendorId") = txtVendorId.Text
        End Sub
    End Class

End Namespace
