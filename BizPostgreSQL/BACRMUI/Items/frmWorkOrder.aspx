﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmWorkOrder.aspx.vb" Inherits="BACRM.UserInterface.Items.frmWorkOrder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/biz.workorders.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var dateRange = $("[id$=hdnDateRange]").val();
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");
            
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $("span.lblTotalProgress").html($("[id$=hdnTotalProgress]").val() + '%');
            $("div.divTotalProgress").css("width", $("[id$=hdnTotalProgress]").val() + '%');

            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetWorkOrderStatus',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "workOrderIDs": $("[id$=hdnWOID]").val()
                    }),
                    success: function (data) {
                        var obj = $.parseJSON(data.GetWorkOrderStatusResult);

                        if (obj == null || obj.length === 0) {
                            $("[id$=tblMain]").find("label.lblWorkOrderStatus").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                        } else {
                            $("[id$=tblMain]").find("label.lblWorkOrderStatus").closest("td").html(obj[0].vcWorkOrderStatus);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("[id$=tblMain]").find("label.lblWorkOrderStatus").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                    }
                });
            } catch (e) {

            }
        }

        function ShowLayout() {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?FormId=144&PType=3", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function AssgiendToChanged(ddl, taskID) {
            if (parseInt($(ddl).val()) > 0) {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/StagePercentageDetailsTaskService.svc/ChangeAssignee',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "taskID": taskID
                        , "assignedTo": parseInt($(ddl).val())
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgress]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgress]").hide();
                    },
                    success: function (data) {
                        $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } else {
                alert("Select task assignee.");
            }

            return true;
        }

        function TeamChanged(ddl) {
            $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
            return true;
        }

        function GradeChanged(ddl) {
            $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
            return true;
        }

        function OpenChangeTimeConfirmation() {
            $("#divTaskTimeConfirmation").modal("show");
            return false;
        }

        function ChangeTime(isMasterUpdateAlso) {
            $("#divTaskTimeConfirmation").modal("hide");
            var errorMessage = "";
            var promises = [];

            $("#tblManageWIP tr").each(function () {
                if ($(this).find(".hdnTaskID").length > 0 && parseInt($(this).find(".hdnTaskID").val()) > 0) {
                    if ($(this).find("[id*=txtTaskHours]").length > 0 && $(this).find("[id*=txtTaskMinutes]").length > 0) {
                        if (!$(this).find("[id*=txtTaskHours]").attr("disabled") && !$(this).find("[id*=txtTaskMinutes]").attr("disabled")) {
                            var request = $.ajax({
                                type: "POST",
                                url: '../WebServices/StagePercentageDetailsTaskService.svc/ChangeTime',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "taskID": parseInt($(this).find(".hdnTaskID").val())
                                    , "hours": parseInt($(this).find("[id*=txtTaskHours]").val())
                                    , "minutes": parseInt($(this).find("[id*=txtTaskMinutes]").val())
                                    , "isMasterUpdateAlso": isMasterUpdateAlso
                                }),
                                beforeSend: function () {
                                    $("[id$=UpdateProgress]").show();
                                },
                                complete: function () {
                                    $("[id$=UpdateProgress]").hide();
                                },
                                success: function (data) {

                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        if (objError.Message != null) {
                                            errorMessage += "<br/>" + $(this).find("#lblTaskName").text() + ":" + objError.Message;
                                        } else {
                                            errorMessage += "<br/>" + $(this).find("#lblTaskName").text() + ":" + objError;
                                        }
                                    } else {
                                        errorMessage += "<br/>" + $(this).find("#lblTaskName").text() + ":" + "Unknown error ocurred";
                                    }
                                }
                            });

                            promises.push(request);
                        }
                    }
                }
            });

            $.when.apply(null, promises).done(function () {
                if (errorMessage.length > 0) {
                    alert("Error occurred while upading change for following task(s):<br/>" + errorMessage);
                } else {
                    alert("Changes are updated successfully.");
                }
            })

            return false;
        }

        function openaddTaskWindow(StageDetailsId) {
            var DomainID = '<%= Session("DomainId")%>';

            $("#taskModal [id$=ddlTaskAssignTo]").val("0");
            $("#taskModal #hdnStageTaskDetailsId").val(StageDetailsId);
            $("#taskModal").modal('show');
        }

        function openEditTaskWindow(TaskId, taskName, hours, minutes, assignedTo) {
            var DomainID = '<%= Session("DomainId")%>';

            $("#taskModal #txtTaskName").val(taskName);
            $("#taskModal #txtTaskMinutes").val(minutes);
            $("#taskModal #txtTaskMinutes").attr("disabled", "disabled");
            $("#taskModal #txtTaskHours").val(hours);
            $("#taskModal #txtTaskHours").attr("disabled", "disabled");
            $("#taskModal [id$=ddlTaskAssignTo]").val(assignedTo);
            $("#taskModal [id$=ddlTaskAssignTo]").attr("disabled", "disabled");

            $("#taskModal #hdnStageTaskDetailsId").val(0);

            $("#taskModal .btn-add-task").text("Save");
            $("#taskModal .btn-add-task").attr("onclick", "UpdateTask(" + TaskId.toString() + ")");

            $("#taskModal").modal('show');
        }

        function UpdateTask(TaskId) {
            $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 50
                    , "updateRecordID": TaskId
                    , "updateValueID": 0
                    , "comments": $("#taskModal #txtTaskName").val()
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgress]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgress]").hide();
                },
                success: function (data) {
                    $("#taskModal").modal('hide');
                    document.location.href = document.location.href;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });

        }

        function AddTasktoStage() {
            var UserContactId = '<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            var StageDetailsId = $("#taskModal #hdnStageTaskDetailsId").val();

            if ($("#taskModal #txtTaskHours").val() == "") {
                $("#taskModal #txtTaskHours").val("0");
            }
            if ($("#taskModal #txtTaskMinutes").val() == "") {
                $("#taskModal #txtTaskMinutes").val("0");
            }
            if ($("#taskModal #hdnStageTaskDetailsId").val() == "0") {
                alert("Please select a stage");
                return false;
            }
            if ($("#taskModal #txtTaskName").val() == "") {
                alert("Please enter task name");
                $("#taskModal #txtTaskName").focus();
                return false;
            } else {
                var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + $("#taskModal #hdnStageTaskDetailsId").val() + "',TaskName:'" + $("#taskModal #txtTaskName").val() + "',Hours:'" + $("#taskModal #txtTaskHours").val() + "',Minutes:'" + $("#taskModal #txtTaskMinutes").val() + "',Assignto:'" + $("#taskModal [id$=ddlTaskAssignTo]").val() + "',ParentTaskId:0,OppID:0,IsTaskClosed:false,numTaskId:0,IsTaskSaved:true,IsAutoClosedTaskConfirmed:false,ProjectID:0,intTaskType:'0',WorkOrderID:" + $("[id$=hdnWOID]").val() + "}";
                $.ajax({
                    type: "POST",
                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTask",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var Jresponse = $.parseJSON(response.d);
                        if (Jresponse == true) {
                            $("#taskModal").modal('hide');
                            $("#taskModal #txtTaskMinutes").val('');
                            $("#taskModal #txtTaskHours").val('');
                            $("#taskModal #txtTaskName").val('');
                            $("#taskModal #hdnStageTaskDetailsId").val(0);
                            document.location.href = document.location.href;
                        }
                    },
                    beforeSend: function () {
                        $("[id$=UpdateProgress]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgress]").hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            }
        }

        function DeleteTask(btn, taskID) {
            var DomainID = '<%= Session("DomainId")%>';

            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteTask",
                data: "{DomainID:'" + DomainID + "',TaskId:'" + taskID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == "1") {
                        $(btn).closest("tr").remove();
                    }
                },
                beforeSend: function () {
                    $("[id$=UpdateProgress]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgress]").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        if (objError.Message != null) {
                            alert("Error occurred: " + objError.Message);
                        } else {
                            alert(objError);
                        }
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });

            return false;
        }

        function DateRangeChanged(dateRange) {
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");
            $("[id$=hdnDateRange]").val(dateRange);
            __doPostBack('<%= btnDateRange.UniqueID%>', '');

            return true;
        }

        function TeamGradeChanged(type, rb) {
            $("th.thTeamGrid").html(type + " (Capacity Load)");

            $("#tblManageWIP tr").each(function () {
                if ($(this).find("[id$=ddlTeam]").length > 0 && $(this).find("[id$=ddlGrade]").length > 0) {
                    if ($(rb).is(":checked")) {
                        if (type === "Team") {
                            $(this).find("[id$=ddlTeam]").show();
                            $(this).find("[id$=ddlGrade]").hide();
                        } else {
                            $(this).find("[id$=ddlTeam]").hide();
                            $(this).find("[id$=ddlGrade]").show();
                        }
                    } else {
                        if (type === "Team") {
                            $(this).find("[id$=ddlTeam]").hide();
                            $(this).find("[id$=ddlGrade]").show();
                        } else {
                            $(this).find("[id$=ddlTeam]").show();
                            $(this).find("[id$=ddlGrade]").hide();
                        }
                    }
                }
            });
        }

        function OpenTaskDocuments(a) {
            window.open("../Documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=T&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenDemandPlan() {
            var selectedItemIds = "";

            $("div[id$=rtlBOM] > table > tbody > tr").each(function () {
                if ($(this).find("[id$=chkSelect]").length > 0 && $(this).find("[id$=hdnItemCodeChild]").length > 0) {
                    if ($(this).find("[id$=chkSelect]").is(":checked") && parseInt($(this).find("[id$=hdnItemCodeChild]").val()) > 0) {
                        selectedItemIds += ((selectedItemIds.length > 0 ? "," : "") + $(this).find("[id$=hdnItemCodeChild]").val())
                    }
                }
            });

            if (selectedItemIds.length > 0) {
                window.open("../Opportunity/frmPlangProcurement.aspx?ItemIds=" + selectedItemIds, "_blank")
            } else {
                alert("Select at least one item.")
            }

            return false;
        }

        function BusinessProcessChanged(ddl) {
            if (parseInt($(ddl).val()) > 0 && parseInt($("[id$=hdnWOID]").val()) > 0) {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/StagePercentageDetailsService.svc/ChangeBusinessProcess',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "processType": 3
                        , "recordID": parseInt($("[id$=hdnWOID]").val())
                        , "processID": parseInt($(ddl).val())
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgress]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgress]").hide();
                    },
                    success: function (data) {
                        document.location.href = document.location.href;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            }
            return false;
        }

        function HoursChanged(txt) {
            if (parseInt($(txt).val()) > 24) {
                $(txt).val(24);
            }
            return false;
        }

        function MinutesChanged(txt) {
            if (parseInt($(txt).val()) > 59) {
                $(txt).val(59);
            }
            return false;
        }

        function PickList() {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmWorkOrderPickList.aspx?IDs=' + $("[id$=hdnWOID]").val(), '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');

            return false;
        }

        function OpenEmail(a, b) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
    <style type="text/css">
        #tblMain tr td {
            border-bottom: 1px solid #e1e1e1 !important;
        }

            #tblMain tr td pre {
                margin: 0 0 0px;
                font-size: inherit !important;
                font-family: inherit !important;
            }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
            padding: 0px;
        }

        .tableGroupHeader {
            background-color: #f5f5f5 !important;
            font-weight: bold;
        }

        #radPageView_WIP .box {
            border-radius: 0px;
            margin-bottom: 10px;
            box-shadow: none;
        }

        #radPageView_WIP .box-header {
            padding: 5px;
        }

            #radPageView_WIP .box-header > .box-tools {
                top: 2px;
            }

            #radPageView_WIP .box-header .box-title {
                font-size: 15px;
            }

        #divTaskLogWorkOrder .RadInput_Default .riTextBox, #divTaskLogWorkOrder .RadInputMgr_Default {
            border-color: #d2d6de;
        }

        #divTaskLogWorkOrder .RadInput table td.riCell {
            padding-right: 0px;
        }

        #divTaskLogWorkOrder .riSpin {
            background-color: #dce3ea !important;
        }

        .manage-wip-date-range li {
            background-color: #dae3f3;
            color: #000;
            font-weight: bold;
        }

            .manage-wip-date-range li a {
                padding: 7px 15px;
                border-top: 0px;
            }

        #radPageView_BOM tr.rtlHeader th {
            white-space: nowrap;
            background-color: #1473b4;
            color: #fff;
            padding: 5px;
            border-left-width: 0px;
            border-right-width: 0px;
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 14px;
        }

        #radPageView_BOM tr td {
            border-left-width: 0px;
            border-right-width: 0px;
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 14px;
        }

            #radPageView_BOM tr td.rtlCF {
                border-left-width: 1px;
            }

            #radPageView_BOM tr td.rtlCL {
                border-right-width: 1px;
            }

        #radPageView_BOM .RadTreeList table.rtlLines td.rtlL, .RadTreeList table.rtlVBorders td.rtlL {
            border-bottom-width: 0px;
        }

        #radPageView_BOM .RadTreeList .rtlTable .rtlRBtm td {
            border-bottom-width: 1px;
        }

        #radPageView_BOM ul {
            margin-bottom: 0px;
        }

        #radPageView_BOM .RadTreeList {
            border-style: none;
        }

        #radPageView_BOM .RadTreeList_Default .rtlA {
            background-color: #fff;
        }

        #divTaskControlsWorkOrder .taskTimer, #divTaskControlsWorkOrder .taskTimerInitial {
            font-size: 14px;
            color: #00a65a;
        }

        .btn-task-finish {
            background-color: white;
            color: black;
            border: 1px solid black;
            padding: 2px;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-12 col-md-3" id="OrderCustomerDetails" runat="server">
            <div class="">
                <div class="form-inline">
                    <div class="pull-left" style="width: 90%">
                        <div class="callout calloutGroup bg-theme">
                            <asp:Label ID="lblCustomerType" CssClass="customerType" Text="Customer" runat="server"></asp:Label>
                            <span>
                                <u>
                                    <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink" Style="font-size: 16px !important; font-weight: bold !important; font-style: italic !important">
                                    </asp:HyperLink></u>

                                <asp:Label ID="lblRelationCustomerType" CssClass="customerType" runat="server"></asp:Label>


                            </span>
                        </div>
                        <div class="record-small-box record-small-group-box">
                            <strong>
                                <asp:Label ID="lblContactName" runat="server" CssClass="text-color-theme" Text=""></asp:Label></strong>
                            <span class="contact">
                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></span>
                            <a id="btnSendEmail" runat="server" href="#">
                                <img src="../images/msg_unread_small.gif" />
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div id="trRecordOwner" runat="server" class="row">

                <div class="record-small-box record-small-group-box createdBySection">
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Finish</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblFinishedBy" runat="server"></asp:Label>
                    </span>
                </div>

                </div>
            </div>
        <div class="col-md-4">
            <div class="pull-right createdBySection">
<asp:LinkButton ID="btnPickBOM" runat="server" CssClass="btn btn-primary" Text="Pick BOM" OnClientClick="return PickList();"></asp:LinkButton>
                <asp:LinkButton ID="btnFinish" runat="server" CssClass="btn btn-success" OnClick="btnFinish_Click"><i class="fa fa-flag-checkered"></i>&nbsp;&nbsp;Finish Work Order</asp:LinkButton>
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClick="btnDelete_Click"><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Work Order&nbsp;&nbsp;(<asp:Label ID="lblWOName" runat="server" ForeColor="Gray" />)&nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frmworkorder.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:LinkButton ID="btnLayout" runat="server" CssClass="btn btn-default btn-sm" OnClientClick="return ShowLayout();"><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</asp:LinkButton>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <table class="table">
                <tr>
                    <td style="min-width:250px;border:1px solid #e4e4e4;">
                        <asp:Label ID="lblItemName" runat="server" Text="Test Item" style="font-size: 18px;font-weight: 600;"></asp:Label>
                        <br />
                        <br />
                        <asp:Image runat="server" ID="lmgItemImage" CssClass="img-responsive" style="margin:0 auto;" />
                    </td>
                    <td style="width:100%;padding:0px;border:none;">
                        <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder" style="margin-bottom: -1px;margin-top: -1px;margin-bottom:-1px;">
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <telerik:RadTabStrip ID="radWorkOrderTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                Skin="Default" ClickSelectedTab="True" MultiPageID="radMultiPage_WorkOrderTab"
                AutoPostBack="false" OnTabClick="radWorkOrderTab_TabClick">
                <Tabs>
                    <telerik:RadTab Text="Work In Progress (WIP)" Value="WIP" PageViewID="radPageView_WIP">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Manage WIP" Value="ManageWIP" PageViewID="radPageView_ManageWIP">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Bill Of Materials (BOM)" Value="BOM" PageViewID="radPageView_BOM">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage_WorkOrderTab" runat="server" CssClass="pageView">
                <telerik:RadPageView ID="radPageView_WIP" runat="server">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblTotalProgress"></span></label>
                                    <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
                                        <div class="progress-bar progress-bar-primary divTotalProgress" style="width: 0%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="list-inline" style="margin-bottom: 0px;">
                                    <li><b>Built (Remaining):</b></li>
                                    <li>
                                        <b><asp:Label ID="lblBuildStatus" runat="server"></asp:Label></b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-borderless tblPrimary" id="tableWIP">
                                    <asp:Repeater ID="rptMilestones" runat="server" OnItemDataBound="rptMilestones_ItemDataBound">
                                        <ItemTemplate>
                                            <tr style="margin-top: 10px; color: #fff;">
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px;">
                                                    <%# Eval("vcMileStone")%> - <%# Eval("numTotalProgress")%>% done
                                                </th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Team</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Assignee</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; width: 145px;"></th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Processed/Remaining</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Started On</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Actual Time Spent</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Performance vs Expected</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Finished On</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Documents</th>
                                            </tr>
                                            <asp:Repeater ID="rptStages" runat="server" OnItemDataBound="rptStages_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="10" style="color: #2a7bb1">
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <label><%# Eval("vcStageName")%></label>
                                                                <span style="font-size: 14px;" class="badge bg-green"><%# Eval("numTotalProgress")%>% done</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="rptTasks" runat="server" OnItemDataBound="rptTasks_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr style="border-radius: 2px; background: #f4f4f4; margin-bottom: 2px; border-left: 2px solid #e6e7e8;">
                                                                <td style="font-weight: 600;"><%# Eval("vcTaskName")%></td>
                                                                <td style="white-space: nowrap;"><%# Eval("vcWorkStation")%></td>
                                                                <td style="white-space: nowrap;"><%# Eval("vcAssignedTo")%></td>
                                                                <td style="width: 145px; text-align: center; white-space: nowrap;">
                                                                    <div id="divTaskControlsWorkOrder" runat="server">
                                                                        <%# Eval("vcTaskControls")%>
                                                                    </div>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <asp:Label ID="lblProcessedQuantity" runat="server" Text='<%# Eval("numProcessedQty")%>'></asp:Label>
                                                                    / 
                                                        <asp:Label ID="lblRemainingQuantity" runat="server" Text='<%# Eval("numRemainingQty")%>'></asp:Label>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <%# Eval("dtPlannedStartDate")%>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <%# Eval("vcActualTaskTimeHtml")%>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <asp:Label ID="lblPerformance" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <%# Eval("vcFinishDate")%>                                                            
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <input type="hidden" class="hdnTaskEstimationInMinutes" value='<%# Eval("numTaskEstimationInMinutes") %>' />
                                                                    <input type="hidden" class="hdnTaskID" value='<%# Eval("numTaskID") %>' />
                                                                    <input type="hidden" class="hdnTimeSpentInMinutes" value='<%# Eval("numTimeSpentInMinutes")%>' />
                                                                    <input type="hidden" class="hdnLastStartDate" value='<%# Eval("dtLastStartDate")%>' />
                                                                    
                                                                    <asp:PlaceHolder ID="plhDocuments" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="10" style="padding: 0px; margin: 0px; height: 5px;"></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_ManageWIP" runat="server">
                    <div class="row padbotom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblTotalProgress"></span></label>
                                    <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
                                        <div class="progress-bar progress-bar-primary divTotalProgress" style="width: 0%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="list-inline" style="margin-bottom: 0px;">
                                    <li style="vertical-align: top;">
                                        <telerik:RadDatePicker ID="rdManageWIPDate" runat="server" AutoPostBack="true" DateInput-CssClass="form-control" Width="100" OnSelectedDateChanged="rdManageWIPDate_SelectedDateChanged" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png"></telerik:RadDatePicker>
                                    </li>
                                    <li>
                                        <ul class="nav nav-pills manage-wip-date-range">
                                            <li id="liDateRange1" role="presentation" class="active"><a href="javascript:DateRangeChanged(1);">Day</a></li>
                                            <li id="liDateRange2" role="presentation"><a href="javascript:DateRangeChanged(2);">Week</a></li>
                                            <li id="liDateRange3" role="presentation"><a href="javascript:DateRangeChanged(3);">Month</a></li>
                                        </ul>
                                        <asp:Button runat="server" ID="btnDateRange" Style="display: none" OnClick="btnDateRange_Click" />
                                    </li>
                                    <li style="vertical-align: top;">
                                        <asp:DropDownList CssClass="form-control" ID="ddlBusinessProcess" runat="server" onChange="return BusinessProcessChanged(this);"></asp:DropDownList>
                                    </li>
                                    <li style="vertical-align: top;">
                                        <button class="btn btn-flat btn-primary" id="btnSaveManageWIP" runat="server" onclick="return OpenChangeTimeConfirmation();">Save</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-borderless tblPrimary" id="tblManageWIP">
                                    <tr>
                                        <td colspan="4"></td>
                                        <td style="text-align: center;">
                                            <ul class="list-inline" style="margin-bottom: 0px;">
                                                <li>
                                                    <input type="radio" name='TeamGradeHeader' checked="checked" onchange="TeamGradeChanged('Team',this);" />
                                                    Team</li>
                                                <li>
                                                    <input type="radio" name='TeamGradeHeader' onchange="TeamGradeChanged('Grade',this);" />
                                                    Grade</li>
                                            </ul>
                                        </td>
                                        <td colspan="4"></td>
                                    </tr>
                                    <asp:Repeater ID="rptMilestonesManageWIP" runat="server" OnItemDataBound="rptMilestonesManageWIP_ItemDataBound">
                                        <ItemTemplate>
                                            <tr style="margin-top: 10px; color: #fff;" class="trMilestone">
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px;">
                                                    <%# Eval("vcMileStone")%> - <%# Eval("numTotalProgress")%>% done
                                                </th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Hours</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Minutes</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Assignee (Capacity Load)</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center" class="thTeamGrid">Team (Capacity Load)
                                                </th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Actual Start</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Finished</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Task Hrs/Actual</th>
                                                <th style="white-space: nowrap; background-color: #1473b4; padding: 5px; text-align: center">Documents</th>
                                            </tr>
                                            <asp:Repeater ID="rptStagesManageWIP" runat="server" OnItemDataBound="rptStagesManageWIP_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="9" style="color: #2a7bb1">
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <label><%# Eval("vcStageName")%></label>
                                                                <span style="font-size: 14px;" class="badge bg-green"><%# Eval("numTotalProgress")%>% done</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="rptTasksManageWIP" runat="server" OnItemDataBound="rptTasksManageWIP_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr style="border-radius: 2px; background: #f4f4f4; margin-bottom: 2px; border-left: 2px solid #e6e7e8;">
                                                                <td>
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label id="lblTaskName"><%# Eval("vcTaskName")%></label>
                                                                            <%--<label>Task:</label>
                                                                <asp:DropDownList ID="ddlTasks" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                                                                        </div>
                                                                        <input type="hidden" class="hdnTaskID" value='<%# Eval("numTaskId") %>' />
                                                                        <button type="button" class="btn btn-default btn-xs btn-github btn-round" style="border-radius: 50%;" onclick='<%# "openaddTaskWindow(" & Eval("numStageDetailsId") & ")"%>'><i class="fa fa-plus"></i></button>
                                                                        <button type="button" class="btn btn-danger btn-xs btn-round" onclick='<%# "return DeleteTask(this," & Eval("numTaskId") & ");" %>' style="border-radius: 50%;"><i class="fa fa-times"></i></button>
                                                                        <button type="button" class="btn btn-info btn-xs btn-round" style="border-radius: 50%;" onclick='<%# "openEditTaskWindow(" & Eval("numTaskId") & ",""" & Eval("vcTaskName") & """," & Eval("numHours") & "," & Eval("numMinutes") & "," & Eval("numAssignTo") & ")"%>'><i class="fa fa-pencil"></i></button>
                                                                        <asp:Button runat="server" ID="btnStartAgain" CssClass="btn btn-xs btn-success pull-right" Text="Start Again" Visible="false" OnClientClick='<%# "return StartTaskAgainWorkOrder(" & Eval("numTaskId") & ");"%>' />
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <input type="number" class="form-control" id="txtTaskHours" max="24" min="0" runat="server" style="width: 60px" value='<%# Eval("numHours")%>' onchange="return HoursChanged(this);" />
                                                                </td>
                                                                <td>
                                                                    <input type="number" class="form-control" id="txtTaskMinutes" max="59" min="0" runat="server" style="width: 60px" value='<%# Eval("numMinutes")%>' onchange="return MinutesChanged(this);" />
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlAssignedTo" ClientIDMode="AutoID" runat="server" CssClass="form-control" ForeColor="Black" onchange='<%# "return AssgiendToChanged(this," & Eval("numTaskId") & ");"%>'>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlTeam" ClientIDMode="AutoID" runat="server" CssClass="form-control" ForeColor="Black" onchange="return TeamChanged(this);">
                                                                    </asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlGrade" ClientIDMode="AutoID" runat="server" CssClass="form-control" ForeColor="Black" onchange="return GradeChanged(this);" Style="display: none">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <%# IIf(Convert.ToDateTime(Eval("dtStartDate")).Year = 1900, "", Convert.ToDateTime(Eval("dtStartDate")).ToString(BACRM.BusinessLogic.Common.CCommon.GetValidationDateFormat() & " hh:mm tt"))%>
                                                                </td>
                                                                <td style="white-space: nowrap; text-align: center">
                                                                    <%# IIf(Convert.ToDateTime(Eval("dtFinishDate")).Year = 1900, "", Convert.ToDateTime(Eval("dtFinishDate")).ToString(BACRM.BusinessLogic.Common.CCommon.GetValidationDateFormat() & " hh:mm tt"))%>
                                                                </td>
                                                                <td style="width: 95px; white-space: nowrap; text-align: center">
                                                                    <asp:Label ID="lblEstimatedTaskTime" runat="server" Text='<%# Eval("vcEstimatedTaskTime")%>'></asp:Label>
                                                                    /
                                                        <asp:Label ID="lblActualTaskTime" runat="server" Text='<%# Eval("vcActualTaskTime")%>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <img src="../images/attachemnt.gif" onclick="OpenTaskDocuments('<%# Eval("numTaskID") %>')" />
                                                                    <asp:PlaceHolder ID="plhDocuments" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="9" style="padding: 0px; margin: 0px; height: 5px;"></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_BOM" runat="server">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblTotalProgress"></span></label>
                                    <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
                                        <div class="progress-bar progress-bar-primary divTotalProgress" style="width: 0%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <asp:Button ID="btnDemandPlan" runat="server" CssClass="btn btn-primary" Text="Open Selected Item(s) in Demand Plan" OnClientClick="return OpenDemandPlan();" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadTreeList ID="rtlBOM" runat="server" ParentDataKeyNames="numParentID" CssClass="tblPrimary" DataKeyNames="ID" AutoGenerateColumns="false" OnItemDataBound="rtlBOM_ItemDataBound">
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />
                                    <Columns>
                                        <telerik:TreeListBoundColumn DataField="numItemCode" UniqueName="numItemCode" Visible="false"></telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numItemDetailID" UniqueName="numItemDetailID" Visible="false"></telerik:TreeListBoundColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Item Name (SKU)" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <a href="../items/frmKitDetails.aspx?ItemCode=<%# DataBinder.Eval(Container, "DataItem.numItemCode")%>&frm=All%20Items" target="_blank" style="color: blue">
                                                        <%# DataBinder.Eval(Container, "DataItem.vcItemName")%></a>
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListBoundColumn HeaderText="Required" DataField="numRequiredQty" HeaderStyle-Width="150" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numOnHand" HeaderText="On Hand" HeaderStyle-Width="150" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numAvailable" HeaderText="Available" HeaderStyle-Width="150" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numBackOrder" HeaderText="Back-Order" HeaderStyle-Width="150" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numOnOrder" HeaderText="On-Order (Arrial Est)" HeaderStyle-Width="150" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Materials" HeaderStyle-Width="150" ItemStyle-Width="150" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMaterialCost" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.monCost") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListTemplateColumn HeaderStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                <asp:HiddenField ID="hdnItemCodeChild" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numItemCode")%>' />
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                    </Columns>
                                </telerik:RadTreeList>
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnWOID" />
     <asp:HiddenField runat="server" ID="hdnRecordOwner" />
     <asp:HiddenField runat="server" ID="hdnTasksAssignee" />
    <div id="divTaskLogWorkOrder" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width:1180px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Task Time Log</h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>Total Time:</label>
                                        <span id="spnTimeSpendWorkOrder" style="font-size: 14px;" class="badge bg-green"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="tblTaskTimeLogWorkOrder">
                                    <thead>
                                        <tr>
                                            <th style="max-width:200px;min-width:200px;white-space: nowrap">Employee</th>
                                            <th style="white-space: nowrap;max-width:112px;min-width:112px;">Action</th>
                                            <th style="width: 160px; white-space: nowrap">When</th>
                                            <th style="width: 110px; white-space: nowrap">Processed Qty</th>
                                            <th style="white-space: nowrap">Reason for Pause</th>
                                            <th style="width: 30%; white-space: nowrap">Notes</th>
                                            <th style="width: 65px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <ul class="list-unstyled">
                                                    <li><input name="TimeEntryTypeWorkOrder" type="radio" value="1" checked="checked" onchange="TimeEntryTypeChangedWorkOrder();" /> Single-Day Task</li>
                                                    <li><input name="TimeEntryTypeWorkOrder" type="radio" value="2" onchange="TimeEntryTypeChangedWorkOrder();" /> Multi-Day Task</li>
                                                </ul>
                                            </td>
                                            <td colspan="5" class="tdSingleDay">
                                                <ul class="list-inline">
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Date</label>
                                                            <telerik:RadDatePicker ID="rdpTaskStartDateSingleWorkOrder" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" ShowPopupOnFocus="true" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>Start Time</label>
                                                            <telerik:RadTimePicker ID="rtpStartTimeWorkOrder" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-inline">
                                                            <label>End Time</label>
                                                            <telerik:RadTimePicker ID="rtpEndTimeWorkOrder" runat="server" 
                                                                DateInput-CssClass="form-control" 
                                                                TimeView-Columns="8" 
                                                                EnableScreenBoundaryDetection="false" 
                                                                PopupDirection="BottomLeft" 
                                                                ShowPopupOnFocus="true"
                                                                TimeView-StartTime="00:00:00"
                                                                TimeView-Interval="00:15:00"
                                                                TimeView-EndTime="23:59:59"
                                                                ></telerik:RadTimePicker>
                                                        </div>
                                                        
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <select id="ddlActionWorkOrder" class="form-control" tabindex="50" style="width: 95px;" onchange="return TimeEntryActionChangeWorkOrder(this)">
                                                    <option value="1">Start</option>
                                                    <option value="2">Pause</option>
                                                    <option value="3">Resume</option>
                                                    <option value="4">Finish</option>
                                                </select>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top; white-space: nowrap;display:none">
                                                <ul class="list-inline">
                                                    <li>
                                                        <telerik:RadDatePicker ID="rdpTaskStartDateWorkOrder" TabIndex="51" runat="server" DateInput-DisplayDateFormat="MM/dd/yyyy" Width="105" DateInput-CssClass="form-control" DatePopupButton-HoverImageUrl="../Images/calendar25.png" DatePopupButton-ImageUrl="../Images/calendar25.png"></telerik:RadDatePicker>
                                                    </li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromHoursWorkOrder" TabIndex="52" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <telerik:RadNumericTextBox ID="txtStartFromMinutesWorkOrder" TabIndex="53" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></li>
                                                    <li style="vertical-align: top">
                                                        <asp:RadioButtonList ID="rblStartFromTimeWorkOrder" runat="server" TabIndex="54" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="AM" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <input type="text" class="form-control" id="txtAddProcessedUnitsWorkOrder" tabindex="55" style="display: none" />
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <asp:DropDownList ID="ddlAddReasonForPauseWorkOrder" runat="server" TabIndex="66" CssClass="form-control" Style="display: none">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="tdMultiDayWorkOrder" style="vertical-align: top;display:none">
                                                <textarea class="form-control" rows="3" id="txtAddNotesWorkOrder" tabindex="67" style="display: none"></textarea>
                                            </td>
                                            <td style="vertical-align: top; white-space: nowrap">
                                                <input type="button" class="btn btn-sm btn-success" id="btnAddTimeEntryWorkOrder" tabindex="68" onclick="return AddTimeEntryWorkOrder(0)" value="Finish Task" />&nbsp;
                                                <input type="button" class="btn btn-sm btn-default" id="btnCancelTimeEntryWorkOrder" tabindex="69" onclick="return CancelTimeEntryWorkOrder();" value="Cancel" style="display:none;">
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnTaskLogTaskIDWorkOrder" />
                    <button type="button" class="btn btn-success btn-start-again">Start Again & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divReasonForPauseWorkOrder" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reason for pause</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Reason for Pause</label>
                                <asp:DropDownList ID="ddlPauseReasonWorkOrder" CssClass="form-control" runat="server">
                                    <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>Notes</label>
                                <textarea id="txtPauseNotesWorkOrder" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnPausedTaskIDWorkOrder" />
                    <input type="hidden" id="hdnProcessedQtyWorkOrder" />
                    <button type="button" class="btn btn-primary" onclick="return TaskPausedWorkOrder();">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="taskModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Task</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="hdnStageTaskDetailsId" />
                    <input type="hidden" id="hdnIsAnyPendingTask" value="0" />
                    <div class="col-md-6">
                        <label><span class="text-danger">*</span>Task</label>
                        <input type="text" class="form-control" id="txtTaskName" autocomplete="off" maxlength="100" />
                        <div id="txtTaskNameautocomplete-list" class="autocomplete-items" style="display: none">
                            <div>No record found</div>
                        </div>
                    </div>
                    <div class="col-md-1" style="width: 12%;">
                        <label>Hours</label>
                        <input type="number" class="form-control" id="txtTaskHours" max="24" min="0" />
                    </div>
                    <div class="col-md-1" style="width: 12%;">
                        <label>Minutes</label>
                        <input type="number" class="form-control" id="txtTaskMinutes" max="59" min="0" />
                    </div>

                    <div class="col-md-3">
                        <label>Assign to</label>
                        <asp:DropDownList runat="server" ID="ddlTaskAssignTo" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-add-task" onclick="AddTasktoStage()">Add Task</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="divTaskTimeConfirmation" class="modal fade">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                </div>
                <div class="modal-body">
                    <p>Would you also like this change to modify the default process for use in  future tasks/records that use this process too ?</p>
                    <br />
                    <ul class="text-center list-inline">
                        <li>
                            <button class="btn btn-primary" onclick="return ChangeTime(false);">Just this record</button></li>
                        <li>
                            <button class="btn btn-primary" onclick="return ChangeTime(true);">Change the default for future records</button></li>
                        <li>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hdnDateRange" />
    <asp:HiddenField runat="server" ID="hdnTotalProgress" />
    <asp:HiddenField runat="server" ID="hdnWOStatus" />
    <asp:HiddenField runat="server" ID="hdnBuildProcess" />
</asp:Content>
