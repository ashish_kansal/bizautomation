﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting

Public Class frmNewItem
    Inherits BACRMPage

#Region "Member Variables"

    Dim formID As Long
    Dim itemType As String
    Dim dtTableInfo As DataTable
    Dim dtItemTax As DataTable
    Dim objPageControls As New PageControls
    Dim objItems As New CItems
    Dim lngItemCode As Long
    Dim strItemName As String

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'Gets Query String Value 
                formID = GetQueryStringVal("FormID")
                itemType = GetQueryStringVal("ItemType")

                HiddenFieldFormID.Value = formID
                HiddenFieldItemType.Value = itemType

                Dim strModuleName, strPermissionName As String
                Dim m_aryRightsForItem() As Integer = GetUserRightsForPage_Other(MODULEID.Items, 9, strModuleName, strPermissionName)

                If m_aryRightsForItem(RIGHTSTYPE.ADD) = 0 Then
                    Response.Redirect("../admin/authenticationpopup.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                    Exit Sub
                End If

                'sets page title
                Select Case formID
                    Case 86
                        If itemType = "Kits" Then
                            LabelItemType.Text = "Kit Item"
                        ElseIf itemType = "Assembly" Then
                            LabelItemType.Text = "Assembly"
                        ElseIf itemType = "Containers" Then
                            LabelItemType.Text = "Containers"
                        Else
                            LabelItemType.Text = "Inventory Item"
                        End If

                    Case 87
                        If String.IsNullOrEmpty(itemType) Then
                            LabelItemType.Text = "Non Inventory Item"
                        Else
                            LabelItemType.Text = "Service Item"
                        End If
                    Case 88
                        If itemType = "Serialized" Then
                            LabelItemType.Text = "Serialized Item"
                        ElseIf itemType = "Lot" Then
                            LabelItemType.Text = "LOT #s Item"
                        Else
                            LabelItemType.Text = "Serialized/LOT #s Item"
                        End If
                End Select

            LoadControls()

            btnSave.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
            End If

            If IsPostBack Then
                LoadControls()
                DisplayDynamicFlds()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub LoadControls()
        Try
            tblMain.Controls.Clear()
            Dim ds As DataSet
            Dim objPageLayout As New CPageLayout
            Dim fields() As String

            objPageLayout.UserCntID = 0
            objPageLayout.DomainID = Session("DomainID")
            objPageLayout.PageId = 5
            objPageLayout.numRelCntType = 0
            objPageLayout.FormId = HiddenFieldFormID.Value
            objPageLayout.PageType = 2

            ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
            dtTableInfo = ds.Tables(0)

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UOMAll = True
            Dim dtUnit As DataTable
            dtUnit = objCommon.GetItemUOM()

            Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
            Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
            Dim intCol1 As Int32 = 0
            Dim intCol2 As Int32 = 0
            Dim tblCell As TableCell

            Dim tblRow As TableRow
            Const NoOfColumns As Short = 2
            Dim ColCount As Int32 = 0

            ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
            objPageControls.CreateTemplateRow(tblMain)
            For Each dr As DataRow In dtTableInfo.Rows

                If ColCount = 0 Then
                    tblRow = New TableRow
                End If

                If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)
                    ColCount = 1
                End If

                If CCommon.ToString(dr("vcPropertyName")) = "BarCodeID" Then
                    dr("vcFieldName") = "UPC"
                ElseIf CCommon.ToString(dr("vcPropertyName")) = "SKU" Then
                    dr("vcFieldName") = "SKU"
                End If


                If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                    dr("vcValue") = objItems.GetType.GetProperty(dr("vcPropertyName")).GetValue(objItems, Nothing)

                    If HiddenFieldFormID.Value = "88" And CCommon.ToString(dr("vcPropertyName")) = "bitSerialized" Then
                        dr("vcValue") = True
                    End If
                End If

                If HiddenFieldFormID.Value = "87" And Not String.IsNullOrEmpty(HiddenFieldItemType.Value) And dr("vcPropertyName") = "Weight" Then
                ElseIf HiddenFieldFormID.Value = "86" And HiddenFieldItemType.Value = "Kits" And CCommon.ToString(dr("vcPropertyName")) = "AssetChartAcntId" Then
                Else
                    If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                        Dim dtData As DataTable
                        dtData = Nothing
                        If Not IsDBNull(dr("vcPropertyName")) Then
                            If dr("vcPropertyName") = "AssetChartAcntId" Then
                                dtData = New DataTable
                                Dim dtChartAcntDetails As DataTable
                                Dim objCOA As New ChartOfAccounting
                                objCOA.DomainID = Session("DomainId")
                                objCOA.AccountCode = "0101"
                                dtChartAcntDetails = objCOA.GetParentCategory()
                                dtData.Columns.Add("numAccountID")
                                dtData.Columns.Add("vcAccountName")
                                dtData.Columns.Add("vcAccountType")
                                For Each drChartAcnt As DataRow In dtChartAcntDetails.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numAccountID") = drChartAcnt("numAccountID")
                                    newRow("vcAccountName") = drChartAcnt("vcAccountName")
                                    newRow("vcAccountType") = drChartAcnt("vcAccountType")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "IncomeChartAcntId" Then
                                dtData = New DataTable
                                Dim dtChartAcntDetails As DataTable
                                Dim objCOA As New ChartOfAccounting
                                objCOA.DomainID = Session("DomainId")
                                objCOA.AccountCode = "0103"
                                dtChartAcntDetails = objCOA.GetParentCategory()
                                dtData.Columns.Add("numAccountID")
                                dtData.Columns.Add("vcAccountName")
                                dtData.Columns.Add("vcAccountType")
                                For Each drChartAcnt As DataRow In dtChartAcntDetails.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numAccountID") = drChartAcnt("numAccountID")
                                    newRow("vcAccountName") = drChartAcnt("vcAccountName")
                                    newRow("vcAccountType") = drChartAcnt("vcAccountType")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "COGSChartAcntId" Then
                                dtData = New DataTable
                                Dim dtChartAcntDetails As DataTable
                                Dim objCOA As New ChartOfAccounting
                                objCOA.DomainID = Session("DomainId")
                                objCOA.AccountCode = "0106"
                                dtChartAcntDetails = objCOA.GetParentCategory()
                                dtData.Columns.Add("numAccountID")
                                dtData.Columns.Add("vcAccountName")
                                dtData.Columns.Add("vcAccountType")
                                For Each drChartAcnt As DataRow In dtChartAcntDetails.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numAccountID") = drChartAcnt("numAccountID")
                                    newRow("vcAccountName") = drChartAcnt("vcAccountName")
                                    newRow("vcAccountType") = drChartAcnt("vcAccountType")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "BaseUnit" Or dr("vcPropertyName") = "SaleUnit" Or dr("vcPropertyName") = "PurchaseUnit" Then
                                dtData = New DataTable
                                dtData = dtUnit
                            ElseIf dr("vcPropertyName") = "ItemGroupID" Then
                                objItems.DomainID = Session("DomainID")
                                objItems.ItemCode = 0
                                dtData = objItems.GetItemGroups.Tables(0)
                            ElseIf dr("vcPropertyName") = "WarehouseID" Then
                                dtData = New DataTable
                                Dim dtWareHouses As DataTable
                                objItems.DomainID = Session("DomainID")
                                dtWareHouses = objItems.GetWareHouses()
                                dtData.Columns.Add("numWareHouseID")
                                dtData.Columns.Add("vcWareHouse")
                                For Each drChartAcnt As DataRow In dtWareHouses.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numWareHouseID") = drChartAcnt("numWareHouseID")
                                    newRow("vcWareHouse") = drChartAcnt("vcWareHouse")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "WarehouseLocationID" Then
                                dtData = New DataTable
                                Dim dtWareHouseLocation As DataTable
                                objItems.DomainID = Session("DomainID")
                                dtWareHouseLocation = objItems.GetWarehouseLocation()
                                dtData.Columns.Add("numWLocationID")
                                dtData.Columns.Add("vcLocation")
                                For Each drChartAcnt As DataRow In dtWareHouseLocation.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numWLocationID") = drChartAcnt("numWLocationID")
                                    newRow("vcLocation") = drChartAcnt("vcLocation")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objItems)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If
                        End If

                        Dim ddl As DropDownList
                        ddl = objPageControls.CreateCells(tblRow, dr, False, dtData, boolAdd:=True)

                        If dr("vcPropertyName") = "WarehouseID" Then
                            ddl.AutoPostBack = True
                            AddHandler ddl.SelectedIndexChanged, AddressOf PopulateWareHouseLocation
                        ElseIf dr("vcPropertyName") = "BaseUnit" Or dr("vcPropertyName") = "SaleUnit" Or dr("vcPropertyName") = "PurchaseUnit" Then
                            ddl.Items.FindByText("Units").Selected = True
                        ElseIf dr("vcPropertyName") = "AssetChartAcntId" Or dr("vcPropertyName") = "IncomeChartAcntId" Or dr("vcPropertyName") = "COGSChartAcntId" Then
                            Dim objUserAccess As New UserAccess
                            objUserAccess.DomainID = Session("DomainID")
                            Dim dtTable As DataTable = objUserAccess.GetDomainDetails()

                            Select Case CCommon.ToString(dr("vcPropertyName"))
                                Case "AssetChartAcntId"
                                    If Not ddl.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numAssetAccID"))) Is Nothing Then
                                        ddl.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numAssetAccID"))).Selected = True
                                    End If
                                Case "IncomeChartAcntId"
                                    If Not ddl.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numIncomeAccID"))) Is Nothing Then
                                        ddl.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numIncomeAccID"))).Selected = True
                                    End If
                                Case "COGSChartAcntId"
                                    If Not ddl.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numCOGSAccID"))) Is Nothing Then
                                        ddl.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0)("numCOGSAccID"))).Selected = True
                                    End If
                            End Select
                        Else
                            If CLng(dr("DependentFields")) > 0 And Not False Then
                                ddl.AutoPostBack = True
                                AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                            End If
                        End If

                        ddl.EnableViewState = True
                    Else
                        objPageControls.CreateCells(tblRow, dr, False, RecordID:=0, boolAdd:=True)
                    End If
                End If

                If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)

                    ColCount = 1
                End If


                If dr("intcoulmn") = 2 Then
                    If intCol1 <> intCol1Count Then
                        intCol1 = intCol1 + 1
                    End If

                    If intCol2 <> intCol2Count Then
                        intCol2 = intCol2 + 1
                    End If
                End If

                ColCount = ColCount + 1

                If NoOfColumns = ColCount Then
                    ColCount = 0
                    tblMain.Rows.Add(tblRow)
                End If
                ' End If
            Next

            If ColCount > 0 Then
                tblMain.Rows.Add(tblRow)
            End If

            'Add Client Side validation for custom fields
            Dim strValidation As String = objPageControls.GenerateJqueryValidationScript(dtTableInfo)
            ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ManageInventory()
        If objItems.WarehouseID > 0 Then
            Try
                objItems.WarehouseID = objItems.WarehouseID
                objItems.WarehouseLocationID = objItems.WarehouseLocationID
                objItems.WListPrice = objItems.WListPrice
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")

                objItems.byteMode = 1

                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("Fld_ID")
                dtCusTable.Columns.Add("Fld_Value")
                dtCusTable.TableName = "CusFlds"
                Dim drCusRow As DataRow

                Dim ds1 As New DataSet
                ds1.Tables.Add(dtCusTable)
                objItems.strFieldList = ds1.GetXml

                objItems.AddUpdateWareHouseForItems()
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

    Private Sub DisplayDynamicFlds()
        Try
            objPageControls.DisplayDynamicFlds(0, 0, Session("DomainID"), 5)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveCusField(itemId As Int32)
        Try
            objPageControls.SaveCusField(itemId, 0, Session("DomainID"), 5, tblMain)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function Save() As Boolean
        Try
            litMessage.Text = ""
            Dim itemType As String
            Select Case HiddenFieldFormID.Value
                Case 86
                    itemType = "P"
                Case 87
                    If String.IsNullOrEmpty(HiddenFieldItemType.Value) Then
                        itemType = "N"
                    Else
                        itemType = "S"
                    End If
                Case 88
                    itemType = "P"
            End Select


            With objItems
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .ItemType = itemType

                If HiddenFieldFormID.Value = "86" And HiddenFieldItemType.Value = "Kits" Then
                    .KitParent = True
                End If
                If HiddenFieldFormID.Value = "86" And HiddenFieldItemType.Value = "Containers" Then
                    .bitContainer = True
                End If
                If HiddenFieldFormID.Value = "86" And HiddenFieldItemType.Value = "Assembly" Then
                    .Assembly = True
                End If
                If HiddenFieldFormID.Value = "86" And HiddenFieldItemType.Value = "Asset" Then
                    .bitAsset = True
                End If
                If HiddenFieldFormID.Value = "88" And HiddenFieldItemType.Value = "Serialized" Then
                    .bitSerialized = True
                End If
                If HiddenFieldFormID.Value = "88" And HiddenFieldItemType.Value = "Lot" Then
                    .bitLotNo = True
                End If

                For Each dr As DataRow In dtTableInfo.Rows
                    If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then
                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                            If HiddenFieldFormID.Value = "87" And Not String.IsNullOrEmpty(HiddenFieldItemType.Value) And dr("vcPropertyName") = "Weight" Then
                            ElseIf HiddenFieldFormID.Value = "86" And HiddenFieldItemType.Value = "Kits" And CCommon.ToString(dr("vcPropertyName")) = "AssetChartAcntId" Then
                                Dim objUserAccess As New UserAccess
                                objUserAccess.DomainID = Session("DomainID")
                                Dim dtTable As DataTable = objUserAccess.GetDomainDetails()

                                If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                                    objItems.AssetChartAcntId = CCommon.ToLong(dtTable.Rows(0)("numAssetAccID"))
                                End If
                            Else
                                objPageControls.SetValueForStaticFields(dr, objItems, tblMain)
                            End If
                        End If
                    End If
                Next
            End With

            If HiddenFieldFormID.Value = 88 AndAlso Not objItems.bitSerialized AndAlso Not objItems.bitLotNo Then
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType(), "SerialLotFieldRequired", "alert('Either Is Serialized or Is Lot No has to be checked to create new Serialized/Lot item. Make sure fields are added in form.')", True)
                Return False
            End If

            Dim strError As String = objItems.ManageItemsAndKits()

            If strError.Length > 2 Then
                litMessage.Text = strError
                Return False
            End If

            If objItems.ItemCode > 0 Then
                lngItemCode = objItems.ItemCode 'To save custom field when creating item
                strItemName = objItems.ItemName
            End If


            If lngItemCode > 0 Then
                ''Saving Item Tax Types
                dtItemTax = New DataTable
                dtItemTax.Columns.Add("numTaxItemID")
                dtItemTax.Columns.Add("bitApplicable")
                Dim dr As DataRow
                If objItems.Taxable Then
                    dr = dtItemTax.NewRow
                    dr("numTaxItemID") = 0
                    dr("bitApplicable") = 1
                    dtItemTax.Rows.Add(dr)
                End If

                Dim ds As New DataSet
                Dim strdetails As String
                dtItemTax.TableName = "Table"
                ds.Tables.Add(dtItemTax)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                objItems.strItemTaxTypes = strdetails
                objItems.ManageItemTaxTypes()
                ds.Dispose()

                'Saves custom fields
                SaveCusField(objItems.ItemCode)

                If HiddenFieldFormID.Value = "86" Or HiddenFieldFormID.Value = "88" Then
                    ManageInventory()
                End If
            End If

            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region

#Region "Event Handlers"

    Private Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
        Try
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, tblMain, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub PopulateWareHouseLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddl As DropDownList
            ddl = CType(sender, DropDownList)
            Dim ddlWarehouseLocation As DropDownList

            If Not tblMain.FindControl("233Warehouse Location~0") Is Nothing Then
                ddlWarehouseLocation = CType(tblMain.FindControl("233Warehouse Location~0"), DropDownList)
                Dim dtData As New DataTable
                dtData = New DataTable
                Dim dtWareHouseLocation As DataTable
                objItems.WarehouseID = ddl.SelectedValue
                dtWareHouseLocation = objItems.GetWarehouseLocation()
                dtData.Columns.Add("numWLocationID")
                dtData.Columns.Add("vcLocation")
                For Each drChartAcnt As DataRow In dtWareHouseLocation.Rows
                    Dim newRow As DataRow
                    newRow = dtData.NewRow
                    newRow("numWLocationID") = drChartAcnt("numWLocationID")
                    newRow("vcLocation") = drChartAcnt("vcLocation")
                    dtData.Rows.Add(newRow)
                Next

                ddlWarehouseLocation.DataSource = dtData
                ddlWarehouseLocation.DataValueField = dtData.Columns(0).ColumnName
                ddlWarehouseLocation.DataTextField = dtData.Columns(1).ColumnName
                ddlWarehouseLocation.DataBind()
                ddlWarehouseLocation.Items.Insert(0, "-- Select One --")
                ddlWarehouseLocation.Items.FindByText("-- Select One --").Value = "0"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If Save() Then
                If CCommon.ToBool(GetQueryStringVal("isFromNewOrder")) Or CCommon.ToBool(GetQueryStringVal("isFromAssemblyKit")) Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "SendToParentPage(" & lngItemCode & ",'" & strItemName & "');"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                Else
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "window.opener.reDirectPage('../Items/frmKitDetails.aspx?ItemCode=" & objItems.ItemCode & "'); self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSaveNew_Click(sender As Object, e As EventArgs)
        Try
            If Save() Then
                If CCommon.ToBool(GetQueryStringVal("isFromNewOrder")) Or CCommon.ToBool(GetQueryStringVal("isFromAssemblyKit")) Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "SendToParentPage(" & lngItemCode & ",'" & strItemName & "');"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                Else
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "document.location.href=document.location.href;"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

    
End Class