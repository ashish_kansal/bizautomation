Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class frmItemGroupList
    Inherits BACRMPage
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                GetUserRightsForPage(37, 13)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnNew.Visible = False
                End If
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            dgItemGroup.DataSource = objItem.GetItemGroups.Tables(0)
            dgItemGroup.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgItemGroup_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItemGroup.ItemCommand
        Try
            If e.CommandName = "ItemGroup" Then
                Response.Redirect("../Items/frmItemGroup.aspx?ID=" & e.Item.Cells(0).Text)
            ElseIf e.CommandName = "Delete" Then
                Dim objItem As New CItems
                objItem.ItemGroupID = e.Item.Cells(0).Text
                objItem.DeleteItemGroups() 'Delete Options
                objItem.DomainID = Session("DomainID")
                objItem.ItemGroupID = 0
                dgItemGroup.DataSource = objItem.GetItemGroups.Tables(0)
                dgItemGroup.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub dgItemGroup_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItemGroup.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
End Class