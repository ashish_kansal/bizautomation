﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmUpdateInventory.aspx.vb"
    Inherits=".frmUpdateInventory" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Imports Wizard</title>
    <script language="javascript">
        function setDllValues() {
            if (document.getElementById('tbldtls').rows.length < 2) {
                alert("Please Upload the file first")
                return false;
            }
            var intCnt;
            inCnt = document.Form1.txtDllValue.value;
            document.Form1.txtDllValue.value = "";
            var dllValue;
            dllValue = "";
            for (var x = 0; x <= inCnt; x++) {
                if (dllValue != "") {
                    dllValue = dllValue + "," + document.getElementById('ddlDestination' + x).value;
                }
                else {
                    dllValue = document.getElementById('ddlDestination' + x).value;
                }
            }
            document.Form1.txtDllValue.value = dllValue;
        }

        function checkFileExt() {

            if (document.Form1.txtFile.value.length == 0) {
                alert("Plz Select File");
                return false;
            }
            else {

                var data = document.Form1.txtFile.value

                var extension = data.substr(data.lastIndexOf('.'));

                if (extension == '.csv' || extension == '.CSV') {
                    //|| extension =='xls' || extension =='XLS'               
                    return true;
                }
                else {
                    alert("Plz Select a .csv File");
                    return false;
                }

            }
        }

        function displayPBar() {

            if (document.getElementById('tbldtls').rows.length <= 2) {
                alert("please click on display records before importing")
                return false;
            }
            if (document.Form1.ddlSelection.value == -1) {
                alert("please select destination")
                return false;
            }
            if (document.Form1.pgBar.style.display == "none")
                document.Form1.pgBar.style.display = "";
            else
                document.Form1.pgBar.style.display = "none";
        }
        function ShowGroup() {
            if (document.Form1.ddlSelection.value == 0) {
                document.Form1.ddlGroup.style.display = '';
                document.getElementById('lblGroup').style.display = '';
            }
            else {
                document.Form1.ddlGroup.style.display = 'none';
                document.getElementById('lblGroup').style.display = 'none';
            }
        }

        function SingleSelect(current) {
            if (current.id == 'chkSerializedItem') {
                document.getElementById('chkLotNo').checked = false;
            }
            else if (current.id == 'chkLotNo') {
                document.getElementById('chkSerializedItem').checked = false;
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Update Inventory&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="middle">
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </td>
        </tr>
    </table>
    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" Width="100%" runat="server"
        BorderColor="black" GridLines="None" Height="400" BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%" cellspacing="4">
                    <tr>
                        <td class="normal1" align="center">
                            Step 1
                        </td>
                        <td class="normal1">
                            Select Item Group
                            <asp:DropDownList ID="ddlItemGroup" runat="server" CssClass="signup">
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkLotNo" runat="server" onclick="SingleSelect(this);" Text="Lot No" />
                            <asp:CheckBox ID="chkSerializedItem" runat="server" onclick="SingleSelect(this);"
                                Text="Serialize" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            Step 2
                        </td>
                        <td class="normal1">
                            <asp:LinkButton ID="lbDownloadCSV" Text="Download CSV Template" runat="server" CssClass="hyperlink" />
                            or
                            <asp:LinkButton ID="lbDownloadCSVWithData" Text="Download CSV Template with Data"
                                runat="server" CssClass="hyperlink" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            Step 3
                        </td>
                        <td class="normal1">
                            Source&nbsp;
                            <input runat="server" id="txtFile" type="file" class="signup">
                            &nbsp;
                            <asp:Button runat="server" ID="btnUpload" Text="Upload" CssClass="button"></asp:Button>
                            &nbsp;(Upload the CSV file which contains onhand quantity for selected warehouse)
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            Step 4
                        </td>
                        <td class="normal1">
                            Mapping
                            <asp:TextBox ID="txtDllValue" runat="server" Height="16px" Style="display: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="normal1">
                            <asp:Table ID="tbldtls" runat="server" Width="100%" GridLines="none" BorderWidth="0"
                                CellSpacing="1">
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            Step 5
                        </td>
                        <td class="normal1">
                            <asp:Button ID="btnDisplay" runat="server" Text="Display Records" CssClass="button"
                                Width="100"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            Step 6
                        </td>
                        <td class="normal1">
                            <asp:Button ID="btnImports" runat="server" Text="Update Inventory Item Warehouse"
                                CssClass="button"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
