<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAttributes.aspx.vb"
    Inherits=".frmAttributes" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" language="javascript">

        function Close() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            window.close()
            return false;
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
        <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="ImageButton Save" Text="Save" />
            <asp:Button ID="btnClose" runat="server" CssClass="ImageButton Cancel" Text="Close" />
        </div>
    </div>
    <div style="text-align:left; width: 456px;">
        <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
Inventory Details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
<div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>SKU (M)</label>
                                        <asp:TextBox ID="txtSKU" CssClass="form-control" runat="server" TabIndex="6">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>UPC (M)</label>
                                        <asp:TextBox ID="txtUPC" CssClass="form-control" runat="server" TabIndex="7">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                     <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label>List Price (I)</label>
                                        <asp:TextBox ID="txtListPrice" CssClass="form-control" runat="server" TabIndex="3">
                                        </asp:TextBox>
                                         <asp:HiddenField ID="hdnAttributes" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <asp:PlaceHolder ID="plhWareHouseAttributes" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
          
                       </asp:Content>
