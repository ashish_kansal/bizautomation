Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI
Partial Public Class frmAddWareHouse
    Inherits BACRMPage
    Dim lngWarehouseID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            lngWarehouseID = CCommon.ToLong(GetQueryStringVal("WID"))
            btnSave.Attributes.Add("onClick", "return Save()")
            btnSaveClose.Attributes.Add("onClick", "return Save()")
            If Not IsPostBack Then

                GetUserRightsForPage(37, 12)

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End If

                Dim objContact As New CContacts
                objContact.AddressType = CContacts.enmAddressType.ShipTo
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.RecordID = CCommon.ToLong(Session("UserDivisionID"))
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()
                Dim listItem As RadComboBoxItem
                For Each dr As DataRow In dtAddress.Rows
                    dr("vcFullAddress") = dr("vcFullAddress").ToString().Replace("<pre>", "").Replace("</pre>", ",").Replace("<br>", ",")
                    listItem = New RadComboBoxItem
                    listItem.Text = CCommon.ToString(dr("vcAddressName"))
                    listItem.Value = CCommon.ToString(dr("numAddressID"))
                    listItem.Attributes.Add("Address", dr("vcFullAddress"))
                    radcmbEmployerAddress.Items.Add(listItem)
                Next
                radcmbEmployerAddress.Items.Insert(0, New RadComboBoxItem("-- Select One --", "0"))

                If lngWarehouseID > 0 Then
                    LoadDetails()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objItem As New CItems
            Dim dtTable As DataTable
            objItem.WarehouseID = lngWarehouseID
            objItem.DomainID = Session("DomainID")
            dtTable = objItem.GetWareHouses
            If dtTable.Rows.Count = 1 Then
                txtCompName.Text = dtTable.Rows(0).Item("vcWareHouse")
                txtPrintNodeAPIKey.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPrintNodeAPIKey"))
                txtPrintNodePrinterID.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPrintNodePrinterID"))

                If CCommon.ToLong(dtTable.Rows(0).Item("numAddressID")) > 0 Then
                    If Not radcmbEmployerAddress.Items.FindItemByValue(dtTable.Rows(0).Item("numAddressID")) Is Nothing Then
                        radcmbEmployerAddress.Items.FindItemByValue(dtTable.Rows(0).Item("numAddressID")).Selected = True
                    End If

                    If radcmbEmployerAddress.SelectedValue <> "0" Then
                        lblAddress.Text = radcmbEmployerAddress.SelectedItem.Attributes("Address")
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If String.IsNullOrWhiteSpace(txtCompName.Text) Then
                DisplayError("Enter warehouse name.")
                Exit Sub
            ElseIf CCommon.ToLong(radcmbEmployerAddress.SelectedValue) = 0 Then
                DisplayError("Select address.")
                Exit Sub
            End If

            save()

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "WarehoseCreated", "alert('Wrehouse added.');", True)

            txtCompName.Text = ""
            radcmbEmployerAddress.Text = ""
            radcmbEmployerAddress.SelectedValue = ""
            txtPrintNodeAPIKey.Text = ""
            txtPrintNodePrinterID.Text = ""

            If Not radcmbEmployerAddress.SelectedItem Is Nothing AndAlso radcmbEmployerAddress.SelectedValue <> "0" Then
                lblAddress.Text = radcmbEmployerAddress.SelectedItem.Attributes("Address")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub save()
        Try
            Dim objItem As New CItems
            objItem.Warehouse = txtCompName.Text
            objItem.WarehouseID = lngWarehouseID
            objItem.ShipAddressId = CCommon.ToLong(radcmbEmployerAddress.SelectedValue)
            objItem.DomainID = Session("DomainID")
            objItem.PrintNodeAPIKey = txtPrintNodeAPIKey.Text.Trim()
            objItem.PrintNodePrinterID = txtPrintNodePrinterID.Text.Trim()
            objItem.ManageWarehouse()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If String.IsNullOrWhiteSpace(txtCompName.Text) Then
                DisplayError("Enter warehouse name.")
                Exit Sub
            ElseIf CCommon.ToLong(radcmbEmployerAddress.SelectedValue) = 0 Then
                DisplayError("Select address.")
                Exit Sub
            End If

            save()
            Response.Redirect("../Items/frmWarehouses.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub radcmbEmployerAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbEmployerAddress.SelectedIndexChanged
        Try
            If Not radcmbEmployerAddress.SelectedItem Is Nothing AndAlso radcmbEmployerAddress.SelectedValue <> "0" Then
                lblAddress.Text = radcmbEmployerAddress.SelectedItem.Attributes("Address")
            Else
                lblAddress.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class