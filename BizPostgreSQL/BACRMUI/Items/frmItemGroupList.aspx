<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmItemGroupList.aspx.vb"
    Inherits=".frmItemGroupList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Item Group</title>
    <script type="text/javascript">
        function New() {
            document.location.href = "frmItemGroup.aspx";
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <input type="button" runat="server" id="btnNew" class="btn btn-primary" value="New Matrix Group" onclick="New()" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Matrix Groups&nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frmitemgrouplist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
        <asp:DataGrid ID="dgItemGroup" AllowSorting="True" runat="server" Width="100%" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundColumn Visible="False" DataField="numItemGroupID"></asp:BoundColumn>
                <asp:ButtonColumn DataTextField="vcItemGroup" CommandName="ItemGroup" SortExpression="vcItemGroup"
                    HeaderText="Matrix Group"></asp:ButtonColumn>
                <asp:TemplateColumn ItemStyle-Width="25">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>
