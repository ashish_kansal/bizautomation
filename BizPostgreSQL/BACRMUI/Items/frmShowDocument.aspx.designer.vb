﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmShowDocument

    '''<summary>
    '''lblTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''main control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents main As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''pnlImageData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlImageData As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnUploadImage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUploadImage As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdate As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''table4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents table4 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''lblTest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTest As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnRefresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRefresh As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''HiddenField2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HiddenField2 As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnItemCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnItemCode As Global.System.Web.UI.WebControls.HiddenField
End Class
