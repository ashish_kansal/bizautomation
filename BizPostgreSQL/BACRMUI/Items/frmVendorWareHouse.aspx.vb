﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmVendorWareHouse
    Inherits BACRMPage

    
    Dim objItems As New CItems

    Dim lngItemCode, lngVendor, lngVWarehouse As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = GetQueryStringVal( "ItemCode")
            lngVendor = GetQueryStringVal( "Vendor")
            lngVWarehouse = GetQueryStringVal( "VWarehouse")

            If Not IsPostBack Then
                LoadVendor()

                If lngVendor > 0 Then
                    If ddlVendor.Items.FindByValue(lngVendor) IsNot Nothing Then
                        ddlVendor.ClearSelection()
                        ddlVendor.Items.FindByValue(lngVendor).Selected = True
                    End If
                End If

                LoadWareHouse()

                If lngVWarehouse > 0 Then
                    If ddlWarehouse.Items.FindByValue(lngVWarehouse) IsNot Nothing Then
                        ddlWarehouse.ClearSelection()
                        ddlWarehouse.Items.FindByValue(lngVWarehouse).Selected = True
                    End If
                End If

                LoadAddress()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadVendor()
        Try
            Dim dtVendors As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            dtVendors = objItems.GetVendors

            ddlVendor.DataSource = dtVendors
            ddlVendor.DataTextField = "Vendor"
            ddlVendor.DataValueField = "numVendorID"
            ddlVendor.DataBind()

            ddlVendor.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadWareHouse()
        Try
            Dim dtVendorsWareHouse As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.VendorID = IIf(ddlVendor.SelectedValue > 0, ddlVendor.SelectedValue, -1)
            objItems.byteMode = 1
            dtVendorsWareHouse = objItems.GetVendorsWareHouse

            ddlWarehouse.DataSource = dtVendorsWareHouse
            ddlWarehouse.DataTextField = "vcAddressName"
            ddlWarehouse.DataValueField = "numAddressID"
            ddlWarehouse.DataBind()

            ddlWarehouse.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LoadAddress()
        Try
            If ddlWarehouse.SelectedIndex > 0 Then
                Dim dtTable As DataTable
                Dim objContact As New CContacts

                objContact.DomainID = Session("DomainID")
                objContact.AddressID = ddlWarehouse.SelectedValue
                objContact.byteMode = 1
                dtTable = objContact.GetAddressDetail

                If dtTable.Rows.Count > 0 Then
                    lblWarehouseAddress.Text = CCommon.ToString(dtTable.Rows(0).Item("vcFullAddress"))
                Else
                    lblWarehouseAddress.Text = ""
                End If
            Else
                lblWarehouseAddress.Text = ""
            End If

            bindGrid()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        Try
            LoadWareHouse()
            LoadAddress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWarehouse.SelectedIndexChanged
        Try
            LoadAddress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindGrid()
        Dim objOpportunity As New MOpportunity
        objOpportunity.ItemCode = lngItemCode
        objOpportunity.DomainID = Session("DomainID")
        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        gvOrderTransit.DataSource = objOpportunity.GetItemTransit(3, ddlVendor.SelectedValue, ddlWarehouse.SelectedValue)
        gvOrderTransit.DataBind()
    End Sub
End Class