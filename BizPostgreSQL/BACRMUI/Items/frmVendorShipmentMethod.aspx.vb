﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmVendorShipmentMethod
    Inherits BACRMPage


    Dim objItems As New CItems

    Dim lngVendor, lngVWarehouse As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngVendor = GetQueryStringVal("Vendor")
            lngVWarehouse = GetQueryStringVal("VWarehouse")

            If Not IsPostBack Then
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                ddlWarehouse.DataSource = objMassSalesFulfillment.GetWarehouses()
                ddlWarehouse.DataTextField = "vcWareHouse"
                ddlWarehouse.DataValueField = "numWareHouseID"
                ddlWarehouse.DataBind()

                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            If lngVendor > 0 AndAlso lngVWarehouse > 0 Then
                objItems.DomainID = Session("DomainID")
                objItems.VendorID = lngVendor
                objItems.ShipAddressId = lngVWarehouse
                objItems.byteMode = 1
                objItems.WarehouseID = CCommon.ToLong(ddlWarehouse.SelectedValue)
                Dim dtVendorShipmentMethod As DataTable = objItems.GetVendorShipmentMethod
                If CCommon.ToBool(dtVendorShipmentMethod.Rows(0).Item("bitPrimary")) Then
                    chkPrimary.Checked = True
                Else
                    chkPrimary.Checked = False
                End If

                gvShipmentMethod.DataSource = dtVendorShipmentMethod
                gvShipmentMethod.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            save()
            Dim strScript As String = "<script language=JavaScript>self.close()</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub save()
        Try
            Dim dtShipmentMethod As New DataTable
            Dim drRow As DataRow
            Dim ds As New DataSet

            dtShipmentMethod.TableName = "ShipmentMethod"
            dtShipmentMethod.Columns.Add("numListItemID")
            dtShipmentMethod.Columns.Add("numListValue")
            dtShipmentMethod.Columns.Add("bitPreferredMethod")

            For Each gvr As GridViewRow In gvShipmentMethod.Rows
                Dim objSendEmail As New Email

                drRow = dtShipmentMethod.NewRow

                drRow("numListItemID") = gvShipmentMethod.DataKeys(gvr.DataItemIndex).Value
                drRow("numListValue") = CType(gvr.FindControl("txtDays"), TextBox).Text
                drRow("bitPreferredMethod") = CType(gvr.FindControl("rdbPreferred"), RadioButton).Checked

                dtShipmentMethod.Rows.Add(drRow)
            Next
            ds.Tables.Add(dtShipmentMethod)

            objItems = New CItems
            objItems.DomainID = Session("DomainID")
            objItems.VendorID = lngVendor
            objItems.ShipAddressId = lngVWarehouse
            objItems.WarehouseID = CCommon.ToLong(ddlWarehouse.SelectedValue)
            If chkPrimary.Checked = True Then
                objItems.bitPrimary = True
            Else
                objItems.bitPrimary = False
            End If
            objItems.str = ds.GetXml
            objItems.ManageVendorShipmentMethod()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlWarehouse_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class