﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmVendorShipmentMethod.aspx.vb"
    Inherits=".frmVendorShipmentMethod" MasterPageFile="~/common/PopupBootstrap.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Vendor WareHouse Method of Shipment</title>
    <script type="text/javascript">
        function checkRadioBtn(id) {
            var gv = document.getElementById('<%=gvShipmentMethod.ClientID%>');

            $("[id$=gvShipmentMethod] tr").not(":first").each(function () {
                var radioBtn = $(this).find("input[type=radio]");

                // Check if the id not same
                if (radioBtn != null && radioBtn.attr("name") != $(id).attr("name")) {
                    radioBtn[0].checked = false;
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:CheckBox ID="chkPrimary" Text="Primary" runat="server" />
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save & Close" />            
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="window.close()" Text="Close" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Vendor Lead Times
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlWarehouse_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvShipmentMethod" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" Width="100%" DataKeyNames="numListItemID">
                    <Columns>
                        <asp:BoundField DataField="ShipmentMethod" HeaderText="Shipment Method" HeaderStyle-Width="200px"></asp:BoundField>
                        <asp:TemplateField HeaderText="Days" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtDays" Text='<%# Eval("numListValue") %>' CssClass="form-control"
                                    onkeypress="CheckNumber(2,event)" MaxLength="3" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Preferred" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdbPreferred" runat="server" GroupName="PreferredMethod" Checked='<%# Eval("bitPreferredMethod") %>' onclick="checkRadioBtn(this);" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
