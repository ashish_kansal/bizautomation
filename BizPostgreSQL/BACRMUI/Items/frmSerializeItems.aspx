<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSerializeItems.aspx.vb"
    Inherits=".frmSerializeItems" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Serialize Items</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            var chkBox = $("input[id$='chkSelectAll']");
            chkBox.click(
          function () {
              $("#dgItem INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
        });

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function Close() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            window.close()
            return false;
        }
    </script>
    <style type="text/css">
        #resizeDiv {
            margin: 0px !important;
            padding: 5px;
        }

        #dgItem {
            border-collapse: collapse;
        }

        .ais td, .is td {
            border: 1px solid #dad8d8 !important;
            padding-left: 3px !important;
        }

        .rcTable > tbody > tr {
            background-color: inherit !important;
        }

        .rcTable tr td {
            background-color: none !important;
            border-width: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input" style="width:300px;">
            <asp:Button ID="btnSave" runat="server" CssClass="ImageButton Save" Text="Save" />
            <asp:Button ID="btnDelete" CssClass="ImageButton DeleteOrder" runat="server" Text="Delete"></asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="ImageButton Cancel" Text="Close" />
        </div>
    </div>
    <div style="text-align:left">
        <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Generate Serial #/Lot #
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="col-xs-12 col-sm-6">
        <div class="row">
            <div class="col-xs-12">
                <%--<div class="row" id="trLot" runat="server">
                    <div class="col-xs-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>
                                    <asp:Label ID="lblToolTip" runat="server" Text="[?]" CssClass="tip" ToolTip="Entering inventory for Lot #s is easy. To enter a new Lot#, enter the Lot # in the field, select quantity and select an expiration date.<br/><br/>To edit or delete a Lot # just click the manage serial/lot icon in “Inventory Management” grid."></asp:Label>
                                    <span style="color: red">* </span>Lot #
                                </label>
                                <asp:TextBox ID="txtLotNumber" runat="server" TabIndex="50" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label><span style="color: red">* </span>Qty</label>
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" Width="50" TabIndex="51"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label style="white-space: nowrap"><span style="color: red">* </span>Expiration Date</label>
                                <telerik:RadDatePicker ID="rdpExpirationDate" runat="server" Width="108" TabIndex="52"></telerik:RadDatePicker>
                            </div>
                            <asp:ImageButton ID="ibtnAddLot" runat="server" ImageUrl="~/images/AddRecord.png" TabIndex="53" ToolTip="Add" Height="16" />
                            <asp:ImageButton ID="ibtnClear" runat="server" ImageUrl="~/images/clear.png" TabIndex="54" ToolTip="Clear" Height="16" />
                        </div>
                    </div>
                </div>--%>
                <div class="row" id="trSerial" runat="server">
                    <div class="col-xs-12">
                        <asp:Label ID="Label1" runat="server" Text="[?]" CssClass="tip" ToolTip="Entering inventory for Serial #s is easy. Just enter a the Serial # or for multiple units enter many Serial #s separated by comma (e.g. 1234,1234,44444 would add 3 Serial #s which adds 3 units to inventory). A maximum qty of 100 total can be added at any one time.<br /><br />To delete a serial # just click the manage serial/lot icon in “Inventory Management” grid and deleting a number will adjust inventory and any related short term asset value downward."></asp:Label>
                        <span style="color: red">* </span>Serial #
                        <asp:Label ID="Label2" runat="server" Text="Example: A123,B123 adds 2 serial #s increasing stock by 2" Font-Italic="true" Style="float: right"></asp:Label>
                    </div>
          <br />
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label></label>
                            <asp:TextBox ID="txtSerialLot" runat="server" CssClass="form-control" TextMode="MultiLine" TabIndex="55" Width="780"></asp:TextBox>
                        </div>
                    </div>
                 <br />
                    <br />
                </div>
                       </div>
            </div>
        </div>
   

    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <asp:DataGrid ID="dgItem" runat="server" CssClass="dg" Width="780" AutoGenerateColumns="False" GridLines="Both">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="numWareHouseItmsDTLID" HeaderText="numWareHouseItmsDTLID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="numWareHouseItemID" HeaderText="numWareHouseItemID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcWarehouse" HeaderText="Warehouse, Location" ItemStyle-Wrap="false"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Serial#">
                <ItemTemplate>
                    <asp:TextBox ID="txtSerialNo" TextMode="SingleLine" Width="150" runat="server" CssClass="signup" Text='<%# Eval("vcSerialNo") %>'></asp:TextBox>
                    <asp:HiddenField ID="hfQty" runat="server" Value='<%# Eval("numQty") %>' />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Qty" HeaderStyle-Width="90">
                <ItemTemplate>
                    <asp:TextBox ID="txtQty" Width="80" runat="server" CssClass="signup" Text='<%# Eval("numQty") %>'></asp:TextBox>
                    <asp:Label ID="lblQty" runat="server" CssClass="signup" Text='<%# Eval("numQty") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Expiration Date" ItemStyle-Wrap="false" HeaderStyle-Width="100">
                <ItemTemplate>
                    <telerik:RadDatePicker ID="radExpirationDate" runat="server" SelectedDate='<%# If(String.IsNullOrEmpty(Eval("dExpirationDate").ToString()), DateTime.Now, Convert.ToDateTime(Eval("dExpirationDate")))%>' Width="92" CssClas=""></telerik:RadDatePicker>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-Width="25" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chk" runat="server" Visible='<%# IIf(Eval("numWareHouseItmsDTLID")=0,false,true) %>' />
                </ItemTemplate>
                <HeaderTemplate>
                    <asp:CheckBox ID="chkSelectAll" runat="server" />
                </HeaderTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <asp:HiddenField ID="hfAverageCost" runat="server" Value="0.0" />
    <asp:HiddenField ID="hfItemName" runat="server" Value="" />
    <asp:HiddenField ID="hfAssetChartAcntID" runat="server" Value="" />
    <asp:HiddenField ID="hdnWarehouseItemID" runat="server" />

             </div>
</asp:Content>
