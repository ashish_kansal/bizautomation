Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Partial Public Class frmItemGroup
    Inherits BACRMPage
    Dim lngID As Long
    Dim objItems As New CItems
    Dim ds As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            
            If Not IsPostBack Then

                Dim objCusFld As New CustomFields
                objCusFld.DomainID = Session("DomainID")
                objCusFld.locId = 9
                objCusFld.TabId = 0
                lstAttrAvailablefld.DataSource = objCusFld.GetFieldDetailsByLocAndTabId
                lstAttrAvailablefld.DataTextField = "fld_label"
                lstAttrAvailablefld.DataValueField = "fld_id"
                lstAttrAvailablefld.DataBind()

                btnAttrAdd.Attributes.Add("OnClick", "return move(lstAttrAvailablefld,lstAttrAddfld)")
                btnAttrRemove.Attributes.Add("OnClick", "return remove1(lstAttrAddfld,lstAttrAvailablefld)")
                lngID = CCommon.ToLong(GetQueryStringVal("ID"))
                hdnLngId.Value = lngID
                If hdnLngId.Value > 0 Then LoadDetails()
                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadMappedDropdownItem()
        Try
            Dim dtCustFld As DataTable
            Dim objCusField As New CustomFields()
            objCusField.DomainID = Session("DomainID")
            objCusField.locId = 5
            dtCustFld = objCusField.CustomFieldList
            Dim dv As DataView
            dv = New DataView(dtCustFld)
            dv.RowFilter = "numlistid>0"
            ddlMappedDropDown.DataSource = dv
            ddlMappedDropDown.DataValueField = "MergeFldLstId"
            ddlMappedDropDown.DataTextField = "Fld_label"
            ddlMappedDropDown.DataBind()
            ddlMappedDropDown.Items.Insert(0, New ListItem("-Select-", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadServiceItem()
        Try
            Dim objItems As New CItems
            Dim dtSItems As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.ItemType = "S"
            dtSItems = objItems.GetServiceItem.Tables(0)
            ddlProfileItem.DataSource = dtSItems
            ddlProfileItem.DataValueField = "numItemCode"
            ddlProfileItem.DataTextField = "vcItemName"
            ddlProfileItem.DataBind()
            ddlProfileItem.Items.Insert(0, New ListItem("-Select-", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadListItemDetails(ByRef ddl As DropDownList)
        Try
            Dim dtCustFld As DataTable
            Dim objCusField As New CustomFields()
            Dim strArr As String()
            strArr = ddlMappedDropDown.SelectedValue.Split("-")
            If strArr.Length > 1 Then
                objCusField.ListId = strArr(1)
                dtCustFld = objCusField.GetMasterListByListId
                ddl.DataSource = dtCustFld
                ddl.DataValueField = "numListItemID"
                ddl.DataTextField = "vcData"
                ddl.DataBind()
                ddl.Items.Insert(0, New ListItem("-Select-", "0"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDetails()
        Try
            objItems.ItemGroupID = hdnLngId.Value
            objItems.DomainID = Session("DomainID")
            ds = objItems.GetItemGroups
            txtIGroupName.Text = ds.Tables(0).Rows(0).Item("vcItemGroup")
            chkCombineAssemblies.Checked = CCommon.ToBool(ds.Tables(0).Rows(0).Item("bitCombineAssemblies"))
            If chkCombineAssemblies.Checked = True Then
                LoadMappedDropdownItem()
                LoadServiceItem()
                divMapDropDown.Visible = True
                divProfile.Visible = True
                If Not ddlMappedDropDown.Items.FindByValue(ds.Tables(0).Rows(0).Item("numMapToDropdownFld")) Is Nothing Then
                    ddlMappedDropDown.Items.FindByValue(ds.Tables(0).Rows(0).Item("numMapToDropdownFld")).Selected = True
                End If
                If Not ddlProfileItem.Items.FindByValue(ds.Tables(0).Rows(0).Item("numProfileItem")) Is Nothing Then
                    ddlProfileItem.Items.FindByValue(ds.Tables(0).Rows(0).Item("numProfileItem")).Selected = True
                End If
                If chkCombineAssemblies.Checked Then
                    dgKitItems.Columns(9).Visible = True
                    dgKitItems.Columns(9).HeaderText = ddlMappedDropDown.SelectedItem.Text
                End If
            Else
                divMapDropDown.Visible = False
                divProfile.Visible = False
            End If
            lstAttrAddfld.DataSource = ds.Tables(2)
            lstAttrAddfld.DataTextField = "Fld_label"
            lstAttrAddfld.DataValueField = "Fld_id"
            lstAttrAddfld.DataBind()

            dgKitItems.DataSource = ds.Tables(1)
            dgKitItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radItem_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radItem.SelectedIndexChanged
        Try
            If radItem.SelectedValue <> "" Then
                trKits.Visible = False
                rblKitItem.Items.Clear()
                LoadItemDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub LoadItemDetails()
        Try
            If radItem.SelectedValue <> "" Then
                Dim dtTable As DataTable
                If objItems Is Nothing Then objItems = New CItems
                objItems.ItemCode = radItem.SelectedValue
                dtTable = objItems.ItemDetails

                If dtTable.Rows.Count > 0 Then
                    If dtTable.Rows(0).Item("bitKitParent") = True Then
                        Dim dt As DataTable
                        objItems.ItemCode = radItem.SelectedValue
                        dt = objItems.GetChildItems
                        dt.TableName = "ChildItems"

                        If dt.Rows.Count > 0 Then
                            Dim dcItemText As DataColumn

                            dcItemText = New DataColumn("ItemText", System.Type.GetType("System.String"))
                            dcItemText.Expression = "vcItemName + '- Price is ' + UnitPrice"
                            dt.Columns.Add(dcItemText)

                            rblKitItem.DataSource = dt
                            rblKitItem.DataTextField = "ItemText"
                            rblKitItem.DataValueField = "numItemCode"
                            rblKitItem.DataBind()
                            trKits.Visible = True

                            rblKitItem.SelectedIndex = 0
                        Else : trKits.Visible = False
                        End If
                    Else : trKits.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub DropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        Dim lst As Telerik.Web.UI.RadComboBox
        Dim cell As TableCell
        Dim item As DataGridItem
        Dim index As Integer

        lst = sender
        cell = lst.Parent
        item = cell.Parent
        index = item.ItemIndex

        Dim objItems As New CItems
        If Not (item.Cells(0).Text = "") Then
            objItems.ItemCode = item.Cells(0).Text
            objItems.NoofUnits = 1
            objItems.OppId = 0
            objItems.DivisionID = 0
            objItems.DomainID = Session("DomainID")
            objItems.byteMode = 1
            objItems.Amount = 0
            If Not lst.SelectedValue = "" Then
                objItems.WareHouseItemID = lst.SelectedValue
            Else
                objItems.WareHouseItemID = Nothing
            End If
            Dim dtItemPricemgmt As DataTable

            If objItems.byteMode = 1 Then
                Dim ds As DataSet
                ds = objItems.GetPriceManagementList1
                dtItemPricemgmt = ds.Tables(0)
                If dtItemPricemgmt.Rows.Count = 0 Then
                    dtItemPricemgmt = ds.Tables(2)
                    dtItemPricemgmt.Merge(ds.Tables(1))
                Else : dtItemPricemgmt.Merge(ds.Tables(1))
                End If
            Else : dtItemPricemgmt = objItems.GetPriceManagementList1.Tables(0)
            End If
            If dtItemPricemgmt.Rows.Count > 0 Then
                item.Cells(7).Text = String.Format("{0:0.00}", dtItemPricemgmt.Rows(0)("ListPrice")) & "/Unit"
            End If
        End If
    End Sub
    Private Sub btnAddChilItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddChilItem.Click
        Try
            objItems.ItemGroupID = hdnLngId.Value
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = CCommon.ToLong(radItem.SelectedValue)
            objItems.NoofItemsReqForKit = 0
            objItems.byteMode = 0
            objItems.ItemGroupName = txtIGroupName.Text
            objItems.bitCombineAssemblies = chkCombineAssemblies.Checked
            If chkCombineAssemblies.Checked Then
                Dim strArr As String()
                strArr = ddlMappedDropDown.SelectedValue.Split("-")
                If strArr.Length > 1 Then
                    objItems.numMapToDropdownFld = strArr(1)
                End If
                objItems.numProfileItem = ddlProfileItem.SelectedValue
            End If

            If chkCombineAssemblies.Checked Then
                dgKitItems.Columns(9).Visible = True
                dgKitItems.Columns(9).HeaderText = ddlMappedDropDown.SelectedItem.Text
            End If

            If (rblKitItem.Items.Count > 0) Then
                objItems.DefaultSelect = CCommon.ToLong(rblKitItem.SelectedValue)
            Else
                objItems.DefaultSelect = CCommon.ToLong(radItem.SelectedValue)
            End If
            hdnLngId.Value = objItems.ManageItemsGroupsDTL_OPPNACC()

            If GetQueryStringVal("ID") = "" Then
                If hdnLngId.Value > 0 Then LoadDetails()
                'Response.Redirect("../Items/frmItemGroup.aspx?ID=" & lngID)
            Else
                objItems.ItemGroupID = hdnLngId.Value
                objItems.DomainID = Session("DomainID")
                ds = objItems.GetItemGroups
                dgKitItems.DataSource = ds.Tables(1)
                dgKitItems.DataBind()
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            For Each dgKitGridItem As DataGridItem In dgKitItems.Items
                If CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Checked = True Then
                    objItems.ItemGroupID = hdnLngId.Value
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = dgKitGridItem.Cells(0).Text
                    objItems.byteMode = 1
                    hdnLngId.Value = objItems.ManageItemsGroupsDTL_OPPNACC()
                End If
            Next
            objItems.ItemGroupID = hdnLngId.Value
            objItems.DomainID = Session("DomainID")
            ds = objItems.GetItemGroups
            dgKitItems.DataSource = ds.Tables(1)
            dgKitItems.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub dgKitItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgKitItems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim radKitWareHouse As Telerik.Web.UI.RadComboBox
                radKitWareHouse = e.Item.FindControl("radKitWareHouse")
                If e.Item.Cells(8).Text = "P" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    AddHandler radKitWareHouse.ItemDataBound, AddressOf radKitWareHouse_ItemDatabound
                    radKitWareHouse.DataSource = objCommon.GetWarehouseAttrBasedItem(e.Item.Cells(0).Text)
                    radKitWareHouse.DataBind()

                    Dim strarehousItemID As String = e.Item.Cells(1).Text.Trim
                    If CCommon.ToLong(strarehousItemID) > 0 Then
                        If Not radKitWareHouse.FindItemByValue(strarehousItemID) Is Nothing Then
                            radKitWareHouse.FindItemByValue(strarehousItemID).Selected = True
                        End If
                    End If
                Else
                    radKitWareHouse.Visible = False
                    CType(e.Item.FindControl("rfvKitWareHouse"), RequiredFieldValidator).Enabled = False
                End If
                If chkCombineAssemblies.Checked = True Then
                    Dim ddl As DropDownList = DirectCast(e.Item.FindControl("ddlItemAttribute"), DropDownList)
                    LoadListItemDetails(ddl)
                    Dim strListId As String = e.Item.Cells(10).Text.Trim
                    If CCommon.ToLong(strListId) > 0 Then
                        If Not ddl.Items.FindByValue(strListId) Is Nothing Then
                            ddl.Items.FindByValue(strListId).Selected = True
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Sub radKitWareHouse_ItemDatabound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs)
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString()
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Save()
                If GetQueryStringVal("ID") = "" Then
                    LoadDetails()
                    'Response.Redirect("../Items/frmItemGroup.aspx?ID=" & lngID)
                Else : LoadDetails()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                Save()
                Response.Redirect("../Items/frmItemGroupList.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End If
    End Sub

    Sub Save()
        Try
            Dim objItem As New CItems
            objItem.ItemGroupID = hdnLngId.Value
            objItem.ItemGroupName = txtIGroupName.Text
            objItem.bitCombineAssemblies = chkCombineAssemblies.Checked
            If chkCombineAssemblies.Checked Then
                Dim strArr As String()
                strArr = ddlMappedDropDown.SelectedValue.Split("-")
                If strArr.Length > 1 Then
                    objItem.numMapToDropdownFld = strArr(1)
                End If
                objItem.numProfileItem = ddlProfileItem.SelectedValue
            End If
            Dim dsNew As New DataSet
            Dim dr As DataRow

            Dim dtChildItem As New DataTable
            dtChildItem.Columns.Add("OppAccAttrID")
            dtChildItem.Columns.Add("QtyItemsReq")
            dtChildItem.Columns.Add("WareHouseItemId")
            dtChildItem.Columns.Add("ListItemId")
            For Each dgGridItem As DataGridItem In dgKitItems.Items
                dr = dtChildItem.NewRow
                dr("OppAccAttrID") = dgGridItem.Cells(0).Text
                'dr("QtyItemsReq") = CType(dgGridItem.FindControl("txtQuantity"), TextBox).Text
                dr("QtyItemsReq") = 0
                If dgGridItem.Cells(8).Text = "P" Then
                    If CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue <> "" Then
                        dr("WareHouseItemId") = CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue
                    End If
                End If
                If CType(dgGridItem.FindControl("ddlItemAttribute"), DropDownList).SelectedValue <> "" Then
                    dr("ListItemId") = CType(dgGridItem.FindControl("ddlItemAttribute"), DropDownList).SelectedValue
                End If
                dtChildItem.Rows.Add(dr)
            Next

            dsNew.Tables.Add(dtChildItem.Copy)
            objItem.strFieldList = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            dtChildItem.Rows.Clear()

            Dim dtTable As New DataTable
            dtTable.TableName = "Table"
            dtTable.Columns.Add("numOppAccAttrID")
            Dim str As String()
            Dim i As Integer
            str = txtAttrHidden.Text.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numOppAccAttrID") = str(i)
                dtTable.Rows.Add(dr)
            Next

            dsNew.Tables.Add(dtTable.Copy)
            objItem.str = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            objItem.DomainID = Session("DomainID")
            hdnLngId.Value = objItem.ManageItemGroups()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkCombineAssemblies_CheckedChanged(sender As Object, e As EventArgs)
        If chkCombineAssemblies.Checked = True Then
            LoadMappedDropdownItem()
            LoadServiceItem()
            divMapDropDown.Visible = True
            divProfile.Visible = True
        Else
            divMapDropDown.Visible = False
            divProfile.Visible = False
        End If
    End Sub
End Class