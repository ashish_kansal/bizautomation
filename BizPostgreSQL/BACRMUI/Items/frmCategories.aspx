<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCategories.aspx.vb"
    Inherits="BACRM.UserInterface.Items.Items_frmCategories" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Categories</title>
    <script type="text/javascript">
        function AddCategory() {
            if (document.getElementById('ddlCategoryProfile').value == '') {
                alert("Select category profile.");
                document.getElementById('ddlCategoryProfile').focus();
                return false;
            } else {
                return true;
            }
        }
        function ConfigureItems() {
            if (document.getElementById('ddlCategoryProfile').value == '') {
                alert("Select category profile.");
                document.getElementById('ddlCategoryProfile').focus();
                return false;
            } else {
                return true;
            }
        }
        function OpenCategory(a) {
            document.location.href = 'frmAssignCategories.aspx?CategoryID=' + a;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function Save() {
            if (document.getElementById('ddlSites').value > 0) {
                return false;
            }
            else {
                alert('Select a Site');
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }

        function OpenImage(a) {
            window.open('../Items/frmCategoryImage.aspx?CId=' + a, '', 'toolbar=no,titlebar=no,left=300, top=250,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function CategoryProfileAction(type) {
            var left = (window.screen.width / 2) - ((800 / 2) + 10);
            var top = (window.screen.height / 2) - ((500 / 2) + 50);

            if (type === 1) {
                //Add;
                window.open("../ECommerce/frmCategoryProfile.aspx", '', 'toolbar=no,titlebar=no,top=' + top + ',left=' + left + ',width=1000,height=500,scrollbars=yes,resizable=yes')
                return false;
            } else if (type === 2 || type === 3) {
                //Edit Or Delete
                if (document.getElementById('ddlCategoryProfile').value === '') {
                    alert("Select category profile.");
                    document.getElementById('ddlCategoryProfile').focus();
                    return false;
                }

                if (type === 2) {
                    //Edit
                    window.open("../ECommerce/frmCategoryProfile.aspx?numCategoryProfileID=" + document.getElementById('ddlCategoryProfile').value, '', 'toolbar=no,titlebar=no,top=0,left=100,width=800,height=400,scrollbars=yes,resizable=yes')
                    return false;
                } else if (type = 3) {
                    //Delete
                    if (confirm("Selected category profile will be deleted. Do you want to proceed?")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }

        }

       
    </script>
    <style type="text/css">
        .RadGrid .rgRow td, .RadGrid .rgAltRow td {
            padding-left: 0px !important; 
            padding-right: 0px !important; 
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Category Profile:</label>
                        <asp:Button ID="btnReload" runat="server" Text="" Style="display: none;" />
                        <asp:DropDownList ID="ddlCategoryProfile" runat="server" CssClass="form-control" Width="150" AutoPostBack="true"></asp:DropDownList>

                        <asp:Button ID="ibtnAddCategoryProfile" CssClass="btn btn-primary" Text="Add" runat="server" OnClientClick="return CategoryProfileAction(1);" />
                        <asp:Button ID="ibtnEditCategoryProfile" CssClass="btn btn-info" Text="Edit" runat="server" OnClientClick="return CategoryProfileAction(2);" />
                        <asp:Button ID="ibtnRemoveCategoryProfile" CssClass="btn btn-danger" Text="Delete" runat="server" OnClientClick="return CategoryProfileAction(3);" />
                    </div>

                </div>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnCategoryAdd" runat="server" CssClass="btn btn-primary" Text="Add Category" OnClientClick="return AddCategory();"></asp:Button>
                <asp:Button ID="btnConfigure" runat="server" CssClass="btn btn-primary" Text="Assign Items to Categories" OnClientClick="return ConfigureItems();"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:Label ID="lblCategoryProfileSites" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Categories&nbsp;<a href="#" onclick="return OpenHelpPopUp('Items/frmCategories.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <telerik:RadGrid ID="RadGrid1" runat="server" OnColumnCreated="RadGrid1_ColumnCreated" BorderWidth="0"
                    OnItemCreated="RadGrid1_ItemCreated" OnItemDataBound="RadGrid1_ItemDataBound" ShowFooter="false"
                    AutoGenerateColumns="false" EnableEmbeddedSkins="false" EnableLinqExpressions="false">
                    <MasterTableView HierarchyDefaultExpanded="false" HierarchyLoadMode="Client" DataKeyNames="numDepCategory1,numCategoryID,tintLevel"
                        Width="100%" ShowFooter="false" AllowPaging="true" PageSize="30" TableLayout="Fixed">
                        <PagerStyle Mode="NextPrevAndNumeric"  Position="TopAndBottom" BorderWidth="1" VerticalAlign="Middle" FirstPageImageUrl="../images/nav-left-x2.gif" PrevPageImageUrl="../images/nav-left.gif" LastPageImageUrl="../images/nav-right-x2.gif" NextPageImageUrl="../images/nav-right.gif" PageButtonCount="10" PagerTextFormat="{4} Records {2} to {3} from {5}" />
                        <SelfHierarchySettings ParentKeyName="numDepCategory1" KeyName="numCategoryID" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="tintLevel" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Category ID" ItemStyle-Width="100px" HeaderStyle-Width="100px">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numCategoryID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="vcCategoryName" HeaderText="Category" HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Left"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcCategoryNameURL" HeaderText="Friendly URL" HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Left"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcMatrixGroups" HeaderText="Matrix Group" HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Left"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ItemCount" HeaderText="Items" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ItemSubcategoryCount" ItemStyle-HorizontalAlign="Right" HeaderText="Items in subcategory" ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderStyle-HorizontalAlign="Left"></telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn  HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hplCategoryAdd" ToolTip="Edit Category" runat="server" CssClass="btn btn-xs btn-info"><i class="fa fa-pencil"></i></asp:HyperLink>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbHideCategory" runat="server" ToolTip="Hide category on sites" CommandName="HideCategory" CssClass="btn btn-xs bg-purple"><i class="fa fa-eye"></i></asp:LinkButton>
                                    <asp:LinkButton ID="lkbShowCategory" runat="server" ToolTip="Show category on sites" CommandName="ShowCategory" CssClass="btn btn-xs bg-purple" Visible="false"><i class="fa fa-eye-slash"></i></asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <NoRecordsTemplate>
                            <div style="font-weight: bold; text-align: center">
                                <b>Categories are not available for profile.</b>
                            </div>
                        </NoRecordsTemplate>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>
                <asp:TextBox ID="txtDelCaseId" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
                <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
                <asp:TextBox ID="txtCurrrentPage" runat="server" Text="1" Style="display: none"></asp:TextBox>
                <asp:Label ID="lblRecordCountCases" Visible="false" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>


