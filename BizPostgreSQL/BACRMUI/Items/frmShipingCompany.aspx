<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShipingCompany.aspx.vb" MasterPageFile="~/common/ECommerceMenuMaster.Master" Inherits=".frmShipingCompany" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Company Preffered</title>
    <script type="text/javascript">
        function OpenComDTL(a, b) {
            window.open('../Items/frmShippingCMPDTLs.aspx?ListItemID=' + a + '&ListItem=' + b, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
Shipping Carrier Setup&nbsp;<a href="#" onclick="return OpenHelpPopUp('Items/frmShipingCompany.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="pull-right" style="padding-bottom:5px; padding-right:10px;" >
            <asp:LinkButton runat="server" id="btnSave" OnClick="btnSave_Click" class="btn btn-primary">&nbsp;&nbsp;Save</asp:LinkButton>  &nbsp; 
        </div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table style="width:100%; ">
                    <tr>
                        <td style="Width:25%; vertical-align:top;">
                            <asp:DataGrid ID="dgShipcmp" AllowSorting="true" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="numListItemID"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcData" visible="false" HeaderText="Shipping Company"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="20%" ItemStyle-Width="10%" >
                                        <HeaderTemplate>
                                            <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <label>Ship-Via <asp:Label ID="Label88" Text="[?]" CssClass="tip" runat="server" ToolTip="To edit this list go to �Add & Edit Drop-Down Lists� from the admin section. Select the �Opportunities & Orders� module and �Ship Via� from the list drop down." /></label>
                                                    </td>
                                                </tr>
                                            </table>                                 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:80%">
                                                        <asp:Label ID="lblShipCmp" Text='<%#Eval("vcData") %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td style="width:20%">
                                                        <asp:HyperLink ID="hplEditData"  ImageUrl="~/images/edit.png" runat="server"></asp:HyperLink>
                                                    </td>
                                                </tr>
                                            </table>                                 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlData" Text='<%#Eval("vcData") %>' runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn Visible="False" DataField="numListItemID"></asp:BoundColumn>
                                    <asp:HyperLinkColumn NavigateUrl="#" DataTextField="vcData" HeaderText="Shipping Company"></asp:HyperLinkColumn>
                                    <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlData" Text='<%#Eval("vcData") %>' runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> --%>                       
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td style="width:3%"> &nbsp;</td>
                        <td style="Width:72%;  vertical-align:top;">
                            <asp:Panel ID="pnlRealTimeShippingQuotes" runat="server">
                                <asp:DataGrid ID="dgRealTimeShippingQuotes" AllowSorting="True" runat="server" CssClass="table table-striped table-bordered" UseAccessibleHeader="true"
                                    AutoGenerateColumns="False" HorizontalAlign="Left" CellSpacing="2"
                                    CellPadding="2">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numServiceTypeID"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllServicetype" runat="server" onclick="SelectAll('chkAllServicetype','chk')"
                                                    Text="Enabled " /><asp:Label ID="Label88" Text=" [?]" CssClass="tip" runat="server" ToolTip="Selecting to enable a shipping service adds that shipping service to the list of live rates you can obtain while building an order internally or on the web-store. Keep in mind that the more services you enable, the longer it takes to get back a rate from the live query. Too many rates and thus wait time, can have a deleterious affect on your customer�s check-out experience." />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chk" runat="server" Checked='<%# Eval("bitEnabled") %>' />
                                                <asp:Label Text='<%# Eval("intNsoftEnum") %>' runat="server" ID="lblNSoftEnm" Style='display: none;' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Shipping Carrier Service" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Eval("vcServiceName") %>' runat="server" ID="lblServiceName" />
                                                <asp:TextBox runat="server" ID="txtShippingCaption" Text='<%# Eval("vcServiceName") %>'
                                                    CssClass="form-control" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="From" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtFrom" Text='<%#Eval("intFrom")%>' CssClass="form-control"
                                                    onkeypress="CheckNumber(2,event)" Width="75px" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="To" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtTo" Text='<%#Eval("intTo")%>' CssClass="form-control"
                                                    onkeypress="CheckNumber(2,event)" Width="75px" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="MarkUp" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120px">
                                            <HeaderTemplate>
                                                <span style="float: left">MarkUp</span> <span style="float: right">
                                                    <asp:CheckBox ID="chkIsPercentage" Text="" runat="server" onclick="SelectAll('chkIsPercentage','chkIsPercentageClass')" /></span>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="form-inline">
                                                    <asp:TextBox runat="server" ID="txtMarkup" Text='<%# Eval("fltMarkup") %>' CssClass="form-control"
                                                        onkeypress="CheckNumber(1,event)" Width="50px" />
                                                    <label for="chkIsPercentage" runat="server" id="lblIsPercentage">
                                                        Is %</label><asp:CheckBox class="chkIsPercentageClass" Text="" runat="server" ID="chkIsPercentage"
                                                            Checked='<%# Eval("bitMarkupType") %>' />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Rate" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtRate" Text='<%# Eval("monRate") %>' CssClass="form-control"
                                                    onkeypress="CheckNumber(1,event)" Visible="false" Width="50px" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                                                    Visible="false" CommandArgument='<%# Eval("numServiceTypeID") %>' OnClientClick="return DeleteRecord()"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                

                
            </div>
        </div>
    </div>
</asp:Content>
