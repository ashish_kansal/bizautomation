﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Public Class frmRelatedItems
    Inherits BACRMPage
    Dim objItems As New CItems
    Dim lngItemCode As Long
    Dim strMode As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lngItemCode = GetQueryStringVal("ParentItemId")
        strMode = GetQueryStringVal("Mode")
        hdnItemCode.Value = lngItemCode
        If Not IsPostBack Then
            bindSimilarItemsGrid()
            btnDeleteSimilarItem.Attributes.Add("onclick", "return DeleteSimilarRecord()")
            btnAddSimilarItem.Attributes.Add("onclick", "return CheckSimilarItem()")
        End If

        If strMode = "0" Then
            btnAddSimilarItem.Visible = True
            btnDeleteSimilarItem.Visible = True
            lblItem.Visible = True
            btnAdd.Visible = False
        Else
            btnAddSimilarItem.Visible = False
            btnDeleteSimilarItem.Visible = False
            lblItem.Visible = False
            btnAdd.Visible = True
        End If

    End Sub

    Private Sub btnAddSimilarItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSimilarItem.Click
        'Istart Pinkal Patel ErrId:1387 Date:28-11-2011
        Try
            If radSimilarItem.SelectedValue <> "" Then
                Dim chk As New CheckBox
                Dim itemCode As Integer = radSimilarItem.SelectedValue
                Save(chk, itemCode, 0)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        Finally
            txtRelationship.Text = ""
            radSimilarItem.ClearSelection()
        End Try
        'IEnd Pinkal Patel ErrId:1387 Date:28-11-2011
    End Sub

    Sub bindSimilarItemsGrid()
        'Istart Pinkal Patel ErrId:1387 Date:28-11-2011
        Try
            objItems.DomainID = CCommon.ToLong(Session("DomainID"))
            objItems.ParentItemCode = lngItemCode ' CCommon.ToLong(GetQueryStringVal( "ItemCode"))
            objItems.byteMode = 2
            Dim dt As DataTable
            dt = objItems.GetSimilarItem
            If dt.Rows.Count > 0 Then
                GvsimilarItem.DataSource = dt
                GvsimilarItem.DataBind()
            End If
            For Each gvRow As GridViewRow In GvsimilarItem.Rows
                Dim chkPreUpSell As CheckBox = DirectCast(gvRow.FindControl("chkPreUpSell"), CheckBox)
                Dim chkPostUpSell As CheckBox = DirectCast(gvRow.FindControl("chkPostUpSell"), CheckBox)
                Dim chkRequired As CheckBox = DirectCast(gvRow.FindControl("chkRequired"), CheckBox)

                If (chkPreUpSell.Checked) Then
                    chkPostUpSell.Enabled = False
                    chkRequired.Enabled = False
                End If
                If (chkPostUpSell.Checked) Then
                    chkPreUpSell.Enabled = False
                    chkRequired.Enabled = False
                End If
                If (chkRequired.Checked) Then
                    chkPostUpSell.Enabled = False
                    chkPreUpSell.Enabled = False
                End If
            Next
            'gvModelItems.DataSource = objItems.GetSimilarItem
            'gvModelItems.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
        'IEnd Pinkal Patel ErrId:1387 Date:28-11-2011
    End Sub

    Private Sub btnDeleteSimilarItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteSimilarItem.Click
        'Istart Pinkal Patel ErrId:1387 Date:28-11-2011
        Try
            For Each dgKitGridItem As GridViewRow In GvsimilarItem.Rows
                If CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Checked = True And CType(dgKitGridItem.FindControl("chkSelect"), CheckBox).Visible = True Then
                    objItems.ItemCode = CCommon.ToLong(GvsimilarItem.DataKeys(dgKitGridItem.RowIndex).Value) 'CCommon.ToLong(dgKitGridItem.Cells(0).Text)
                    objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItems.ParentItemCode = lngItemCode
                    objItems.DeleteSimilarItems()
                End If
            Next
            bindSimilarItemsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
        'IEnd Pinkal Patel ErrId:1387 Date:28-11-2011
    End Sub

    Protected Sub chkPreUpSell_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = DirectCast(sender, CheckBox)
        Dim gr As GridViewRow = DirectCast(chk.Parent.Parent, GridViewRow)
        Dim itemCode As Integer = GvsimilarItem.DataKeys(gr.RowIndex).Value.ToString()

        Dim chkPreUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkPreUpSell"), CheckBox)
        Dim chkPostUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkPostUpSell"), CheckBox)
        Dim chkRequired As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkRequired"), CheckBox)

        If chkPreUpSell.Checked Then
            chkPostUpSell.Enabled = False
            chkRequired.Enabled = False
            Save(chkPreUpSell, itemCode, 1)
        Else
            chkPostUpSell.Enabled = True
            chkRequired.Enabled = True
        End If

        'For i As Integer = 0 To GvsimilarItem.Rows.Count - 1
        'Dim chkPreUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkPreUpSell"), CheckBox)
        'Dim chkPostUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkPostUpSell"), CheckBox)
        'Dim chkRequired As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkRequired"), CheckBox)

        'If chkPreUpSell.Checked Then
        '    Save(chkPreUpSell, itemCode, 1)
        'Else
        '    chkPostUpSell.Enabled = True
        '    chkRequired.Enabled = True
        'End If
        'Next
    End Sub

    Protected Sub chkPostUpSell_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = DirectCast(sender, CheckBox)
        Dim gr As GridViewRow = DirectCast(chk.Parent.Parent, GridViewRow)
        Dim itemCode As Integer = GvsimilarItem.DataKeys(gr.RowIndex).Value.ToString()

        Dim chkPreUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkPreUpSell"), CheckBox)
        Dim chkPostUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkPostUpSell"), CheckBox)
        Dim chkRequired As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkRequired"), CheckBox)

        If chkPostUpSell.Checked Then
            chkPreUpSell.Enabled = False
            chkRequired.Enabled = False
            Save(chkPostUpSell, itemCode, 1)
        Else
            chkPreUpSell.Enabled = True
            chkRequired.Enabled = True
        End If

        'For i As Integer = 0 To GvsimilarItem.Rows.Count - 1
        '    Dim chkPreUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkPreUpSell"), CheckBox)
        '    Dim chkPostUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkPostUpSell"), CheckBox)
        '    Dim chkRequired As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkRequired"), CheckBox)

        '    If chkPostUpSell.Checked Then
        '        Save(chkPostUpSell, itemCode, 1)
        '    Else
        '        chkPreUpSell.Enabled = True
        '        chkRequired.Enabled = True
        '    End If
        'Next
    End Sub

    Protected Sub chkRequired_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = DirectCast(sender, CheckBox)
        Dim gr As GridViewRow = DirectCast(chk.Parent.Parent, GridViewRow)
        Dim itemCode As Integer = GvsimilarItem.DataKeys(gr.RowIndex).Value.ToString()

        Dim chkPreUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkPreUpSell"), CheckBox)
        Dim chkPostUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkPostUpSell"), CheckBox)
        Dim chkRequired As CheckBox = DirectCast(GvsimilarItem.Rows(gr.RowIndex).Cells(0).FindControl("chkRequired"), CheckBox)

        If chkRequired.Checked Then
            chkPostUpSell.Enabled = False
            chkPreUpSell.Enabled = False
            Save(chkRequired, itemCode, 1)
        Else
            chkPostUpSell.Enabled = True
            chkPreUpSell.Enabled = True
        End If

        'For i As Integer = 0 To GvsimilarItem.Rows.Count - 1
        '    Dim chkPreUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkPreUpSell"), CheckBox)
        '    Dim chkPostUpSell As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkPostUpSell"), CheckBox)
        '    Dim chkRequired As CheckBox = DirectCast(GvsimilarItem.Rows(i).Cells(0).FindControl("chkRequired"), CheckBox)

        '    If chkRequired.Checked Then
        '        Save(chkRequired, itemCode, 1)
        '    Else
        '        chkPostUpSell.Enabled = True
        '        chkPreUpSell.Enabled = True
        '    End If
        'Next
    End Sub

    Protected Sub Save(chk As CheckBox, itemCode As Integer, byteMode As Integer)
        objItems = New CItems
        objItems.DomainID = CCommon.ToLong(Session("DomainID"))
        'objItems.ItemCode = radSimilarItem.SelectedValue
        objItems.ItemCode = itemCode
        objItems.ParentItemCode = lngItemCode
        objItems.Relationship = txtRelationship.Text.Trim()
        'Commented by Neelam on 10/12/2017 - This is not required any more
        If chk.Checked = True AndAlso chk.ID = "chkPreUpSell" Then
            objItems.bitPreUpSell = chk.Checked
        ElseIf chk.Checked = True AndAlso chk.ID = "chkPostUpSell" Then
            objItems.bitPostUpSell = chk.Checked
        ElseIf chk.Checked = True AndAlso chk.ID = "chkRequired" Then
            objItems.bitRequired = chk.Checked
        End If
        'objItems.bitPreUpSell = chkPreUpSellMain.Checked
        'objItems.bitPostUpSell = chkPostUpSellMain.Checked
        objItems.strPostUpSellDesc = txtRelationship.Text
        objItems.byteMode = byteMode
        objItems.ManageSimilarItem()
        bindSimilarItemsGrid()
    End Sub

    'Protected Sub GvsimilarItem_DataBound(sender As Object, e As DataGridItemEventArgs) Handles GvsimilarItem.DataBound
    '    If TypeOf e.Item Is DataGridItem Then
    '        Dim item As DataGridItem = DirectCast(e.Item, DataGridItem)
    '        If e.Item.DataItem("tranctrlcode") = "PAY" Or e.Item.DataItem("tranctrlcode") = "REC" Then

    '        End If
    '    End If
    'End Sub
End Class