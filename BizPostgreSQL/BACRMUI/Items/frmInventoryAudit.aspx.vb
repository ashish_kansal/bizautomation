﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Partial Public Class frmInventoryAudit
    Inherits BACRMPage

#Region "Private Properties"

    Dim filterFromDate As Date?
    Dim filterToDate As Date?
    Dim filterOrder As String
    Dim filterUser As String

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If CCommon.ToShort(GetQueryStringVal("IsFromItemGrid")) = 1 Then
                    gvAuditReport.Columns(0).Visible = True
                    gvAuditReport.Columns(1).Visible = True
                End If
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim objItems As New CItems
            objItems.WareHouseItemID = GetQueryStringVal("WareHouseItemID")
            objItems.FilterItemIDs = CCommon.ToString(GetQueryStringVal("ItemIDs"))
            objItems.FilterFromDate = filterFromDate
            objItems.FilterToDate = filterToDate
            objItems.FilterPOppName = filterOrder
            objItems.FilterModifiedBy = filterUser

            objItems.tintMode = IIf(chkOnlyAvgCost.Checked, 2, 1)

            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            objItems.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text.Trim())

            objItems.DomainID = Session("DomainID")
            objItems.PageSize = Convert.ToInt32(Session("PagingRows"))
            objItems.TotalRecords = 0
            objItems.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            gvAuditReport.DataSource = objItems.GetInventoryAudit()
            gvAuditReport.DataBind()

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objItems.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("radDateFrom") Is Nothing Then
                DirectCast(gvAuditReport.HeaderRow.FindControl("radDateFrom"), RadDatePicker).SelectedDate = filterFromDate
            End If
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("radDateTo") Is Nothing Then
                DirectCast(gvAuditReport.HeaderRow.FindControl("radDateTo"), RadDatePicker).SelectedDate = filterToDate
            End If
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("txtOrderName") Is Nothing Then
                DirectCast(gvAuditReport.HeaderRow.FindControl("txtOrderName"), TextBox).Text = filterOrder
            End If
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("rcbUsers") Is Nothing Then
                For Each objItem As RadComboBoxItem In DirectCast(gvAuditReport.HeaderRow.FindControl("rcbUsers"), RadComboBox).Items
                    If ("," & filterUser & ",").Contains("," & objItem.Value & ",") Then
                        objItem.Checked = True
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub chkOnlyAvgCost_CheckedChanged(sender As Object, e As EventArgs) Handles chkOnlyAvgCost.CheckedChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub bizPager_PageChanged(sender As Object, e As EventArgs) Handles bizPager.PageChanged
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub gvAuditReport_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                If Not e.Row.FindControl("rcbUsers") Is Nothing Then
                    Dim objCommon As New CCommon
                    objCommon.DomainID = Session("DomainID")
                    Dim dt As DataTable = objCommon.GetDropDownValue(0, "U", "")

                    DirectCast(e.Row.FindControl("rcbUsers"), RadComboBox).DataSource = dt.Select("numID <> 0").CopyToDataTable()
                    DirectCast(e.Row.FindControl("rcbUsers"), RadComboBox).DataTextField = "vcData"
                    DirectCast(e.Row.FindControl("rcbUsers"), RadComboBox).DataValueField = "numID"
                    DirectCast(e.Row.FindControl("rcbUsers"), RadComboBox).DataBind()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub lbClearGridCondition_Click(sender As Object, e As EventArgs)
        Try
            filterFromDate = Nothing
            filterToDate = Nothing
            filterOrder = ""
            filterUser = ""

            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnFilter_Click(sender As Object, e As EventArgs)
        Try
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("radDateFrom") Is Nothing Then
                filterFromDate = DirectCast(gvAuditReport.HeaderRow.FindControl("radDateFrom"), RadDatePicker).SelectedDate
            End If
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("radDateTo") Is Nothing Then
                filterToDate = DirectCast(gvAuditReport.HeaderRow.FindControl("radDateTo"), RadDatePicker).SelectedDate
            End If
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("txtOrderName") Is Nothing Then
                filterOrder = DirectCast(gvAuditReport.HeaderRow.FindControl("txtOrderName"), TextBox).Text
            End If
            If Not gvAuditReport.HeaderRow Is Nothing AndAlso Not gvAuditReport.HeaderRow.FindControl("rcbUsers") Is Nothing Then
                Dim selectedUsers As String = ""
                For Each objItem As RadComboBoxItem In DirectCast(gvAuditReport.HeaderRow.FindControl("rcbUsers"), RadComboBox).CheckedItems
                    selectedUsers = (selectedUsers & If(selectedUsers <> "", ",", "") & objItem.Value)
                Next
                filterUser = selectedUsers
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class