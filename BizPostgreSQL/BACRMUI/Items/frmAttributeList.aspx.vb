Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class frmAttributeList
    Inherits System.Web.UI.Page
    Dim m_aryRightsForPage() As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objCommon As New CCommon
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAttributeList.aspx", Session("userID"), 13, 15)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AC")
            Else
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnNew.Visible = False
                End If
            End If
            LoadDetails()
        End If
    End Sub

    Sub LoadDetails()
        Dim objItem As New CItems
        objItem.DomainID = Session("DomainID")
        objItem.extType = 2
        dgAttributes.DataSource = objItem.GetOptAccAttr.Tables(0)
        dgAttributes.DataBind()
    End Sub


    Private Sub dgAttributes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttributes.ItemCommand
        If e.CommandName = "Attribute" Then
            Response.Redirect("../Items/frmAttributes.aspx?ID=" & e.Item.Cells(0).Text)
        ElseIf e.CommandName = "Delete" Then
            Dim objItem As New CItems
            objItem.ItemOppAccAttr = e.Item.Cells(0).Text
            objItem.extType = 3
            objItem.ManageOptAccAttr() 'Delete Options
            objItem.DomainID = Session("DomainID")
            objItem.extType = 2
            objItem.ItemOppAccAttr = 0
            dgAttributes.DataSource = objItem.GetOptAccAttr.Tables(0)
            dgAttributes.DataBind()
        End If
    End Sub
End Class