Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class frmEcommerceDetails
    Inherits BACRMPage
    Dim PageID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            PageID = GetQueryStringVal( "Page")
            If Not IsPostBack Then
                
                Dim dtTable As DataTable
                'Commented by chintan- below part is obsolete which was used by portal shopping cart now we have seperate shopping cart functionality
                'If PageID = "H" Or PageID = "F" Then
                '    Dim objUserAccess As New UserAccess
                '    objUserAccess.DomainID = Session("DomainID")
                '    dtTable = objUserAccess.GetECommerceDetails()
                '    If dtTable.Rows.Count = 1 Then
                '        If PageID = "H" Then
                '            oEditHtml.Text = dtTable.Rows(0).Item("txtHeader")
                '        ElseIf PageID = "F" Then
                '            oEditHtml.Text = dtTable.Rows(0).Item("txtFooter")
                '        End If
                '    End If
                'Else
                Dim objConfigWizard As New FormGenericFormContents
                objConfigWizard.FormID = 3
                objConfigWizard.DomainID = Session("DomainID")
                dtTable = objConfigWizard.getFormHeaderDetails()
                If PageID = "LH" Then
                    oEditHtml.Text = dtTable.Rows(0).Item("ntxtHeader")
                ElseIf PageID = "LF" Then
                    oEditHtml.Text = dtTable.Rows(0).Item("ntxtFooter")
                ElseIf PageID = "LL" Then
                    oEditHtml.Text = dtTable.Rows(0).Item("ntxtLeft")
                ElseIf PageID = "LR" Then
                    oEditHtml.Text = dtTable.Rows(0).Item("ntxtRight")
                ElseIf PageID = "LS" Then
                    If dtTable.Rows(0).Item("ntxtStyle") = "" Then
                        oEditHtml.Text = ".normal1 { font-family : Tahoma, Verdana, Arial, Helvetica; font-size : 8pt; font-style : normal; font-variant : normal; font-weight : normal; color: #000000 } signup { FONT-FAMILY: arial; FONT-SIZE: 8pt; BACKGROUND: white; TEXT-DECORATION: none; } .text { font-family : Tahoma, Verdana, Arial, Helvetica; font-size : 8pt; font-style : normal; font-variant : normal; font-weight : normal; color: #000000 } .text_bold { font-family : Tahoma, Verdana, Arial, Helvetica; font-size : 8pt; font-style : normal; font-variant : normal; font-weight : bold; color: #000000 } "
                    Else : oEditHtml.Text = dtTable.Rows(0).Item("ntxtStyle")
                    End If
                End If
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objUserAccess As New UserAccess
            'If PageID = "H" Then
            '    objUserAccess.byteMode = 0
            'ElseIf PageID = "F" Then
            '    objUserAccess.byteMode = 1
            If PageID = "LH" Then
                objUserAccess.byteMode = 2
            ElseIf PageID = "LF" Then
                objUserAccess.byteMode = 3
            ElseIf PageID = "LL" Then
                objUserAccess.byteMode = 4
            ElseIf PageID = "LR" Then
                objUserAccess.byteMode = 5
            ElseIf PageID = "LS" Then
                objUserAccess.byteMode = 6
            End If
            objUserAccess.xmlStr = oEditHtml.Text
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.UpdateECommHeaderFooter()
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class