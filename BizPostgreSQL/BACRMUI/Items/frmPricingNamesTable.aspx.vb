﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmPricingNamesTable
    Inherits System.Web.UI.Page
    Dim objPriceBookRule As PriceBookRule
    Dim dtPriceNames As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dr As DataRow
        Try
            If Not IsPostBack Then
                If objPriceBookRule Is Nothing Then
                    objPriceBookRule = New PriceBookRule
                End If
                objPriceBookRule.DomainId = Session("DomainID")

                dtPriceNames = objPriceBookRule.GetPriceNamesTable()

                For i As Integer = dtPriceNames.Rows.Count To 19
                    dr = dtPriceNames.NewRow
                    dtPriceNames.Rows.Add(dr)
                Next
                dgPriceTable.DataSource = dtPriceNames
                dgPriceTable.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim dtgriditem As DataGridItem
        Dim dtrow As DataRow
        Dim ds As New DataSet
        Try
            objPriceBookRule = New PriceBookRule
            Dim vcName As String
            LoadDefaultColumns()

            For i As Integer = 0 To dgPriceTable.Items.Count - 1
                dtrow = dtPriceNames.NewRow
                dtgriditem = dgPriceTable.Items(i)

                vcName = CCommon.ToString(CType(dtgriditem.FindControl("txtPricingName"), TextBox).Text)

                'dtrow.Item("numDomainID") = Session("DomainID")
                'dtrow.Item("numCreatedBy") = Session("UserContactID")
                'dtrow.Item("dtCreatedDate") = Date.UtcNow
                'dtrow.Item("numModifiedBy") = Session("UserContactID")
                dtrow.Item("tintPriceLevel") = i + 1
                dtrow.Item("vcPriceLevelName") = vcName

                dtPriceNames.Rows.Add(dtrow)
            Next

            ds.Tables.Add(dtPriceNames.Copy)
            ds.Tables(0).TableName = "Item"
            objPriceBookRule.DomainId = Session("DomainID")
            objPriceBookRule.UserCntID = Session("UserContactID")

            objPriceBookRule.strItems = ds.GetXml()
            objPriceBookRule.ManagePriceNamesTable()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "Register", "window.close();", True)

            'PersistTable.Clear()
            'PersistTable.Add(chkValidationDisable.ID, chkValidationDisable.Checked)
            'PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDefaultColumns()
        Try
            dtPriceNames.Columns.Add("tintPriceLevel")
            dtPriceNames.Columns.Add("vcPriceLevelName")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub dgPriceTable_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPriceTable.ItemDataBound
    '    If e.Item.ItemType = ListItemType.Header Then

    '    End If
    'End Sub

End Class