Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Partial Public Class frmSerializeAssets
    Inherits BACRMPage
    Dim lngItemCode As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lngItemCode = GetQueryStringVal( "ItemCode")
        Try
            If Not IsPostBack Then
                LoadAsset()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadAsset()
        Try
            Dim objItems As New CItems
            Dim ds As DataSet
            objItems.ItemCode = lngItemCode
            ds = objItems.GetAssetSerial()
            dgItem.DataSource = ds.Tables(0)
            dgItem.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerate.Click
        Try
           

            Dim dtSerial As New DataTable
            Dim dr As DataRow
            Dim StartNo As Integer = CCommon.ToInteger(txtStart.Text)
            Dim EndNo As Integer = CCommon.ToInteger(txtEnd.Text)
            dtSerial.Columns.Add("numAssetItemDTLID", GetType(System.Decimal))
            dtSerial.Columns(0).AutoIncrement = True
            dtSerial.Columns.Add("numAssetItemID", GetType(System.Decimal))
            dtSerial.Columns.Add("vcSerialNo")
            dtSerial.Columns.Add("numBarCodeId", GetType(System.Decimal))
            dtSerial.Columns("numBarCodeId").DefaultValue = 0
            dtSerial.Columns.Add("vcModelId")
            dtSerial.Columns.Add("dtPurchase", GetType(System.DateTime))
            dtSerial.Columns.Add("dtWarrante", GetType(System.DateTime))
            dtSerial.Columns.Add("vcLocation")
            dtSerial.Columns.Add("Op_Flag", GetType(System.Int32))

            StartNo = 0


            If chkIsLot.Checked Then
                EndNo = txtStart.Text
                For i As Integer = StartNo To EndNo - 1
                    dr = dtSerial.NewRow
                    dr("numAssetItemDTLID") = 0
                    dr("numAssetItemID") = lngItemCode
                    'dr("numWareHouseItemID") = 0 'ddlWarehouse.SelectedValue
                    dr("vcSerialNo") = txtPrefix.Text.Trim()
                    'dr("vcWarehouse") = 0 'ddlWarehouse.SelectedValue
                    dr("Op_Flag") = 1
                    dtSerial.Rows.Add(dr)
                Next
            Else
                For i As Integer = StartNo To EndNo
                    dr = dtSerial.NewRow
                    dr("numAssetItemDTLID") = 0
                    dr("numAssetItemID") = lngItemCode
                    'dr("numWareHouseItemID") = 0 'ddlWarehouse.SelectedValue
                    dr("vcSerialNo") = txtPrefix.Text.Trim() + StartNo.ToString()
                    'dr("vcWarehouse") = 0 'ddlWarehouse.SelectedValue
                    dr("Op_Flag") = 1
                    dtSerial.Rows.Add(dr)
                    StartNo += 1
                Next
            End If


            Dim objItems As New CItems
            Dim ds As DataSet
            objItems.ItemCode = lngItemCode
            ds = objItems.GetAssetSerial()
            dtSerial.Merge(ds.Tables(0))
            dgItem.DataSource = dtSerial
            dgItem.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dr As DataRow

            dt.Columns.Add("numAssetItemDTLID", GetType(System.Decimal))
            dt.Columns(0).AutoIncrement = True
            dt.Columns.Add("numAssetItemID", GetType(System.Decimal))
            dt.Columns.Add("vcSerialNo")
            dt.Columns.Add("numBarCodeId", GetType(System.Decimal))
            dt.Columns.Add("vcModelId")
            dt.Columns.Add("dtPurchase", GetType(System.String))
            dt.Columns.Add("dtWarrante", GetType(System.String))
            dt.Columns.Add("vcLocation")
            dt.Columns.Add("Op_Flag", GetType(System.Int32))



            For Each item As DataGridItem In dgItem.Items
                dr = dt.NewRow
                dr("numAssetItemDTLID") = item.Cells(0).Text
                dr("numAssetItemID") = item.Cells(1).Text
                dr("vcSerialNo") = CType(item.FindControl("txtSerialNo"), TextBox).Text
                dr("vcModelId") = CType(item.FindControl("txtModelId"), TextBox).Text
                dr("numBarCodeId") = CType(item.FindControl("txtBarCodeId"), TextBox).Text

                Dim BizCalendar As UserControl
                BizCalendar = item.FindControl("calPurchaseDate")

                Dim strDueDate As String
                Dim _myControlType As Type = BizCalendar.GetType()
                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                If strDueDate <> "" Then
                    dr("dtPurchase") = strDueDate
                Else : dr("dtPurchase") = ""
                End If

                BizCalendar = item.FindControl("calWarranteDate")
                strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                If strDueDate <> "" Then
                    dr("dtWarrante") = strDueDate
                Else : dr("dtWarrante") = ""
                End If

                'dr("dtPurchase") = CType(item.FindControl("calPurchaseDate"), bizCalendar)
                'dr("dtWarrante") = item.Cells(5).Text
                If Not (CType(item.FindControl("ddlLocation"), DropDownList).SelectedItem) Is Nothing Then
                    dr("vcLocation") = CType(item.FindControl("ddlLocation"), DropDownList).SelectedItem.Value
                Else : dr("vcLocation") = ""

                End If

                dr("Op_Flag") = item.Cells(2).Text
                dt.Rows.Add(dr)
            Next

            dt.TableName = "SerializedAsset"
            ds.Tables.Add(dt)
            Dim objItem As New CItems
            objItem.strFieldList = ds.GetXml
            objItem.DomainID = Session("DomainID")
            objItem.ManageAssetSerial()

            'LoadWarehouse(1)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Function PopulateLocation() As DataTable
        Try
            Dim dt As New DataTable
            Dim objItem As New CItems

            objItem.DomainID = Session("DomainID")
            objItem.DivisionID = GetQueryStringVal( "DivisionID")

            dt = objItem.GetOrganizationLocation()
            Return dt
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Function
    Private Sub dgItem_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItem.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objItem As New CItems
                objItem.WareHouseDetailID = e.Item.Cells(0).Text
                objItem.DeleteCompanyAssetSerial()
                LoadAsset()
                'LoadWarehouse(1)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItem.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim ddlLocation As DropDownList
                ddlLocation = e.Item.FindControl("ddlLocation")
                If Not ddlLocation.Items.FindByText(e.Item.Cells(10).Text) Is Nothing Then
                    ddlLocation.Items.FindByText(e.Item.Cells(10).Text).Selected = True
                End If


                'ddlLocation
            End If
            'If e.Item.Controls("").ID = "" Then

            'End If
            'Dim dt As New DataTable
            'Dim ddl As DropDownList
            'ddl = e.Item.FindControl("ddlLocation")
            'dt = PopulateLocation()
            'ddl.DataSource = dt
            'ddl.DataTextField = "vcLocation"
            'ddl.DataBind()


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    
    Protected Sub chkIsLot_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkIsLot.CheckedChanged
        Try
            If chkIsLot.Checked = True Then
                lblPrefix.Text = "Lot Number"
                lblStart.Text = "Repeat Count"
                tdEnd.Visible = False
                tdEnd1.Visible = False
            Else
                lblPrefix.Text = "Prefix"
                lblStart.Text = "Start"
                tdEnd.Visible = True
                tdEnd1.Visible = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Function ReturnDate(ByVal CloseDate) As String
        Try
            If CloseDate Is Nothing Then Exit Function
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                If strTargetResolveDate = "01/01/1900" Then Return ""
                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                Dim strNow As Date
                strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class