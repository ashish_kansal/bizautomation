<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSerializeAssets.aspx.vb"
    Inherits=".frmSerializeAssets" %>

<%@ Register src="../include/calandar.ascx" tagname="Calendar" tagprefix="BizCalendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Serialize Items</title>
    <link href="../css/master.css" type="text/css" rel="STYLESHEET" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function Generate() {

            if (document.form1.ddlWarehouse.value == 0) {
                alert("Select Warehouse")
                document.form1.ddlWarehouse.focus()
                return false;
            }

            if (document.form1.chkIsLot.checked == false) {
                if (document.form1.txtStart.value == "") {
                    alert("Enter Start Number")
                    document.form1.txtStart.focus()
                    return false;
                }
                if (document.form1.txtEnd.value == "") {
                    alert("Enter End Number")
                    document.form1.txtEnd.focus()
                    return false;
                }
            }
            else {
                if (document.form1.txtPrefix.value == "") {
                    alert("Enter Lot Number")
                    document.form1.txtPrefix.focus()
                    return false;
                }
                if (document.form1.txtStart.value == "") {
                    alert("Enter Repeat Count")
                    document.form1.txtStart.focus()
                    return false;
                }
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <table border="0">
        <tr>
            <td class="text">
                <asp:Label ID="lblPrefix" runat="server" Text="Prefix"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPrefix" runat="server" CssClass="signup" Width="50px"></asp:TextBox>
            </td>
            <td class="text">
                <asp:Label ID="lblStart" runat="server" Text="Start"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtStart" runat="server" CssClass="signup" Width="75px"></asp:TextBox>
            </td>
            <td class="text" id="tdEnd" runat="server">
                End
                                <%--<BizCalendar:Calendar ID="calPurchaseDt" runat="server" />--%>
                                
            </td>
            <td id="tdEnd1" runat="server">
                <asp:TextBox ID="txtEnd" runat="server" CssClass="signup" Width="75px"></asp:TextBox>
            </td>
            <td>
                <asp:CheckBox ID="chkIsLot" runat="server" CssClass="signup" Text="Non-Sequential Serial" AutoPostBack="true" />
            </td>
            <td>
                <asp:Button ID="btnGenerate" runat="server" CssClass="button" Text="Generate Serial Number"
                    OnClientClick="return Generate();" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
            </td>
        </tr>
    </table>
    <asp:Table ID="table5" runat="server" BorderWidth="1" Width="100%" BackColor="white"
        CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None" Height="300"
        CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgItem" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numAssetItemDTLID" HeaderText="numAssetItemDTLID"
                            Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numAssetItemID" HeaderText="numAssetItemID" Visible="false">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Op_Flag" HeaderText="Op_Flag" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Serial#" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                                <asp:TextBox ID="txtSerialNo" TextMode="SingleLine" Width="150" runat="server" CssClass="signup"
                                    Text='<%# Eval("vcSerialNo") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BarCode Id" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                                <asp:TextBox ID="txtBarCodeId" TextMode="SingleLine" Width="150" runat="server" CssClass="signup"
                                    Text='<%# Eval("numBarCodeId") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Model Id" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                                <asp:TextBox ID="txtModelId" TextMode="SingleLine" Width="150" runat="server" CssClass="signup"
                                    Text='<%# Eval("vcModelId") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>                        
                        <asp:TemplateColumn HeaderText="Purchase Date" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                            <BizCalendar:Calendar ID="calPurchaseDate" runat="server" SelectedDate='<%# ReturnDate(DataBinder.Eval(Container.DataItem, "dtPurchase")) %>' /> 
                                <%--<BizCalendar:Calendar ID="calPurchaseDate" runat="server" />  <%# Eval("dtPurchase") %> --%>                             
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Warrante ExpireOn" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                            <BizCalendar:Calendar ID="calWarranteDate" runat="server" SelectedDate='<%# ReturnDate(DataBinder.Eval(Container.DataItem, "dtWarrante")) %>' /> 
                                <%--<Biz ID="calWarranteDate" runat="server" /> <%# Eval("dtWarrante") %>                                --%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Location" ItemStyle-VerticalAlign="Bottom">
                                    <ItemTemplate>
                                <asp:DropDownList ID="ddlLocation" width="150" CssClass="signup" AutoPostBack="true" runat="server" DataSource="<%# PopulateLocation() %>" DataTextField="vcLocation" DataValueField="vcLocation"  >
                                </asp:DropDownList> 
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete" Visible='<%# IIf(Eval("numAssetItemDTLID")=0,false,true) %>'
                                    CommandArgument='<%# Eval("numAssetItemDTLID") %>'></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcLocation" HeaderText="Location"
                            Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
