﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.Net
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item

Namespace BACRM.UserInterface.Items

    Public Class frmWorkOrder
        Inherits BACRMPage

#Region "Member Variables"

        Private objWorkOrder As WorkOrder
        Private dtEmployee As DataTable
        Private dtTeam As DataTable
        Private dtGrades As DataTable
        Private boolIntermediatoryPage As Boolean = True
        Dim m_aryRightsForTransOwn() As Integer
        Dim m_aryRightsWIPTab() As Integer
        Dim m_aryRightsManageWIPTab() As Integer

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                GetUserRightsForPage(16, 153)


                m_aryRightsWIPTab = GetUserRightsForPage_Other(16, 157)
                m_aryRightsManageWIPTab = GetUserRightsForPage_Other(16, 158)

                If m_aryRightsManageWIPTab(RIGHTSTYPE.VIEW) = 0 Then
                    radWorkOrderTab.FindTabByValue("ManageWIP").Visible = False
                End If

                If Not Page.IsPostBack Then
                    Dim m_aryRightsFinishWorkOrder() As Integer = GetUserRightsForPage_Other(16, 154)
                    Dim m_aryRightsDeleteWorkOrder() As Integer = GetUserRightsForPage_Other(16, 155)
                    Dim m_aryRightsEditBusinessProcess() As Integer = GetUserRightsForPage_Other(16, 159)
                    Dim m_aryRightsBOMTab() As Integer = GetUserRightsForPage_Other(16, 160)
                    Dim m_aryRightsPickBOM() As Integer = GetUserRightsForPage_Other(16, 162)
                    Dim m_aryRightsOpenDemandPlan() As Integer = GetUserRightsForPage_Other(16, 163)
                    Dim m_aryRightsForLayout() As Integer = GetUserRightsForPage_Other(16, 165)

                    If m_aryRightsFinishWorkOrder(RIGHTSTYPE.VIEW) = 0 Then
                        btnFinish.Visible = False
                    End If

                    If m_aryRightsDeleteWorkOrder(RIGHTSTYPE.VIEW) = 0 Then
                        btnDelete.Visible = False
                    End If

                    If m_aryRightsEditBusinessProcess(RIGHTSTYPE.UPDATE) = 0 Then
                        ddlBusinessProcess.Visible = False
                    End If

                    If m_aryRightsBOMTab(RIGHTSTYPE.VIEW) = 0 Then
                        radWorkOrderTab.FindTabByValue("BOM").Visible = False
                    End If

                    If m_aryRightsPickBOM(RIGHTSTYPE.VIEW) = 0 Then
                        btnPickBOM.Visible = False
                    End If

                    If m_aryRightsOpenDemandPlan(RIGHTSTYPE.VIEW) = 0 Then
                        btnDemandPlan.Visible = False
                    End If

                    If m_aryRightsForLayout(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If

                    hdnDateRange.Value = "1"
                    rdManageWIPDate.SelectedDate = DateTime.UtcNow.AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")) * -1)

                    BindProcessList()

                    objWorkOrder = New WorkOrder
                    objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                    objWorkOrder.WorkOrderID = CCommon.ToLong(GetQueryStringVal("WOID"))
                    objWorkOrder.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                    Dim ds As DataSet = objWorkOrder.GetWorkOrderDetailByID()

                    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        hdnRecordOwner.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numRecOwner"))
                        hdnTasksAssignee.Value = CCommon.ToString(ds.Tables(0).Rows(0)("vcTasksAssignee"))

                        Dim strModuleName, strPermissionName As String
                        Dim m_aryRightsForRecord() As Integer = GetUserRightsForPage_Other(16, 153, strModuleName, strPermissionName)

                        If m_aryRightsForRecord(RIGHTSTYPE.VIEW) = 1 Then
                            If CCommon.ToLong(Session("UserContactID")) <> CCommon.ToLong(ds.Tables(0).Rows(0)("numRecOwner")) AndAlso Not CCommon.ToString(ds.Tables(0).Rows(0)("vcTasksAssignee")).Contains("," & CCommon.ToLong(Session("UserContactID")) & ",") AndAlso CCommon.ToLong(Session("UserDivisionID")) <> CCommon.ToLong(ds.Tables(0).Rows(0)("numDivisionId")) Then
                                Response.Redirect("~/admin/authentication.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                            End If
                        End If

                        If m_aryRightsManageWIPTab(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSaveManageWIP.Visible = False
                        ElseIf m_aryRightsManageWIPTab(RIGHTSTYPE.UPDATE) = 1 Then
                            If CCommon.ToLong(Session("UserContactID")) <> CCommon.ToLong(ds.Tables(0).Rows(0)("numRecOwner")) AndAlso CCommon.ToString(ds.Tables(0).Rows(0)("vcTasksAssignee")).Contains("," & CCommon.ToLong(Session("UserContactID")) & ",") Then
                                btnSaveManageWIP.Visible = False
                            Else
                                btnSaveManageWIP.Visible = True
                            End If
                        End If

                        AddToRecentlyViewed(RecetlyViewdRecordType.WorkOrder, CCommon.ToLong(ds.Tables(0).Rows(0)("numWOId")))
                        hdnWOID.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numWOId"))
                        lblItemName.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcItemName"))
                        If CCommon.ToString(ds.Tables(0).Rows(0).Item("vcPathForTImage")) <> "" Then
                            lmgItemImage.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & ds.Tables(0).Rows(0).Item("vcPathForTImage")
                        Else
                            lmgItemImage.ImageUrl = "../Images/DefaultProduct.png"
                        End If
                        lblWOName.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcWorkOrderName"))
                        lblCreatedBy.Text = IIf(IsDBNull(ds.Tables(0).Rows(0).Item("CreatedBy")), "", ds.Tables(0).Rows(0).Item("CreatedBy"))
                        lblLastModifiedBy.Text = IIf(IsDBNull(ds.Tables(0).Rows(0).Item("ModifiedBy")), "", ds.Tables(0).Rows(0).Item("ModifiedBy"))
                        lblFinishedBy.Text = IIf(IsDBNull(ds.Tables(0).Rows(0).Item("CompletedBy")), "", ds.Tables(0).Rows(0).Item("CompletedBy"))
                        hdnTotalProgress.Value = CCommon.ToInteger(ds.Tables(0).Rows(0).Item("numTotalProgress"))
                        hdnWOStatus.Value = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numWOStatus"))
                        hdnBuildProcess.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numBuildProcessId"))
                        If CCommon.ToLong(ds.Tables(0).Rows(0)("numOppId")) > 0 Then
                            OrderCustomerDetails.Visible = True
                        Else
                            OrderCustomerDetails.Visible = False
                        End If
                        hplCustomer.Text = ds.Tables(0).Rows(0).Item("vcCompanyName")
                        If CCommon.ToString(ds.Tables(0).Rows(0).Item("vcContactName")) <> "" Then
                            lblContactName.Text = ds.Tables(0).Rows(0).Item("vcContactName")
                        Else
                            lblContactName.Text = ""
                        End If
                        If CCommon.ToString(ds.Tables(0).Rows(0).Item("Phone")) <> "" Then
                            lblPhone.Text = ds.Tables(0).Rows(0).Item("Phone")
                        Else
                            lblPhone.Text = ""
                        End If
                        If CCommon.ToString(ds.Tables(0).Rows(0).Item("PhoneExtension")) <> "" Then
                            lblPhone.Text = lblPhone.Text & "(" & CCommon.ToString(ds.Tables(0).Rows(0).Item("PhoneExtension")) & ")"
                        End If
                        If CCommon.ToString(ds.Tables(0).Rows(0).Item("vcEmail")) <> "" Then
                            btnSendEmail.Visible = True
                            btnSendEmail.Attributes.Add("onclick", "return OpenEmail('" & CCommon.ToString(ds.Tables(0).Rows(0).Item("vcEmail")) & "','')")
                        Else
                            btnSendEmail.Visible = False
                        End If
                        If CCommon.ToString(ds.Tables(0).Rows(0).Item("vcCompanyType")) <> "" Then
                            lblRelationCustomerType.Text = " (" & ds.Tables(0).Rows(0).Item("vcCompanyType") & ")"
                        End If

                        lblBuildStatus.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcBuiltQty"))

                        If CCommon.ToLong(hdnBuildProcess.Value) > 0 Then
                            ddlBusinessProcess.Items.RemoveAt(0)
                            ddlBusinessProcess.Items.Insert(0, New ListItem(CCommon.ToString(ds.Tables(0).Rows(0).Item("vcProcessName")), CCommon.ToLong(ds.Tables(0).Rows(0).Item("numBuildProcessId"))))
                        End If


                        If Not ddlBusinessProcess.Items.FindByValue(CCommon.ToLong(ds.Tables(0).Rows(0)("numBuildProcessId"))) Is Nothing Then
                            ddlBusinessProcess.Items.FindByValue(CCommon.ToLong(ds.Tables(0).Rows(0)("numBuildProcessId"))).Selected = True
                        End If

                        If CCommon.ToBool(ds.Tables(0).Rows(0)("bitTaskStarted")) Then
                            ddlBusinessProcess.Enabled = False
                        End If

                        Dim dt As DataTable = objCommon.GetMasterListItems(52, CCommon.ToLong(Session("DomainID")))
                        ddlPauseReasonWorkOrder.DataSource = dt
                        ddlPauseReasonWorkOrder.DataTextField = "vcData"
                        ddlPauseReasonWorkOrder.DataValueField = "numListItemID"
                        ddlPauseReasonWorkOrder.DataBind()
                        ddlPauseReasonWorkOrder.Items.Insert(0, New ListItem("-- Select One --", "0"))

                        ddlAddReasonForPauseWorkOrder.DataSource = dt
                        ddlAddReasonForPauseWorkOrder.DataTextField = "vcData"
                        ddlAddReasonForPauseWorkOrder.DataValueField = "numListItemID"
                        ddlAddReasonForPauseWorkOrder.DataBind()
                        ddlAddReasonForPauseWorkOrder.Items.Insert(0, New ListItem("-- Select One --", "0"))

                        Dim dtEmployee As DataTable = objCommon.ConEmpList(CCommon.ToLong(Session("DomainID")), False, 0)
                        ddlTaskAssignTo.DataSource = dtEmployee
                        ddlTaskAssignTo.DataValueField = "numContactID"
                        ddlTaskAssignTo.DataTextField = "vcUserName"
                        ddlTaskAssignTo.DataBind()
                        ddlTaskAssignTo.Items.Insert(0, New ListItem("-- Select One --", "0"))

                        PersistTable.Load(boolOnlyURL:=True)

                        If PersistTable.Count > 0 Then
                            If radWorkOrderTab.Tabs.Count > CCommon.ToInteger(PersistTable(PersistKey.SelectedTab)) Then
                                radWorkOrderTab.SelectedIndex = CCommon.ToInteger(PersistTable(PersistKey.SelectedTab))
                            Else
                                radWorkOrderTab.SelectedIndex = 0
                            End If
                        Else
                            radWorkOrderTab.SelectedIndex = 0
                        End If

                        radWorkOrderTab.MultiPage.FindPageViewByID(radWorkOrderTab.SelectedTab.PageViewID).Selected = True

                        If CCommon.ToLong(ds.Tables(0).Rows(0)("numBuildProcessId")) > 0 Then
                            If radWorkOrderTab.SelectedIndex = 1 Then
                                BindWIP(True)
                            Else
                                BindWIP(False)
                            End If
                        End If

                        BindBOM()

                        If CCommon.ToLong(hdnWOStatus.Value) = 23184 Then
                            btnFinish.Visible = False
                            btnDelete.Visible = False
                            btnPickBOM.Visible = False
                        End If

                        If CCommon.ToBool(ds.Tables(0).Rows(0)("bitBOMPicked")) Then
                            btnPickBOM.Visible = False
                        End If
                    Else
                        Response.Redirect("~/NotFound.aspx", False)
                    End If
                End If

                LoadControls()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub BindProcessList()
            Try
                Dim objAdmin = New CAdmin()
                Dim dtTable As DataTable
                objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
                objAdmin.Mode = 3
                dtTable = objAdmin.LoadProcessList()
                ddlBusinessProcess.DataSource = dtTable
                ddlBusinessProcess.DataTextField = "Slp_Name"
                ddlBusinessProcess.DataValueField = "Slp_Id"
                ddlBusinessProcess.DataBind()
                ddlBusinessProcess.Items.Insert(0, New ListItem("-- Select One --", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindWIP(ByVal isManageWIP As Boolean)
            Try
                Dim objWorkOrder As New WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                objWorkOrder.WorkOrderID = CCommon.ToLong(GetQueryStringVal("WOID"))
                Dim dt As DataTable = objWorkOrder.GetMilestones()

                If isManageWIP Then
                    rptMilestonesManageWIP.DataSource = dt
                    rptMilestonesManageWIP.DataBind()
                Else
                    rptMilestones.DataSource = dt
                    rptMilestones.DataBind()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindBOM()
            Try
                objWorkOrder = New WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                rtlBOM.DataSource = objWorkOrder.GetBOMDetail()
                rtlBOM.DataBind()
                rtlBOM.ExpandAllItems()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub LoadControls(Optional ByVal typeEvent As Long = 0)
            Try
                tblMain.Controls.Clear()

                Dim objPageLayout As New CPageLayout
                objPageLayout.DomainID = CCommon.ToLong(Session("DomainID"))
                objPageLayout.UserCntID = CCommon.ToLong(Session("UserContactId"))
                objPageLayout.RecordId = CCommon.ToLong(hdnWOID.Value)
                objPageLayout.CoType = ""
                objPageLayout.PageType = 3
                objPageLayout.FormId = 144
                objPageLayout.PageId = 153
                Dim ds As DataSet = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                Dim dtTableInfo As DataTable = ds.Tables(0)

                Dim objWOrkOrder As New WorkOrder
                objWOrkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                objWOrkOrder.WorkOrderID = CCommon.ToLong(GetQueryStringVal("WOID"))
                objWOrkOrder.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objWOrkOrder.GetDetailPageFieldsData()


                'lblTProgress.Text = objOpportunity.MilestoneProgress

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0

                Dim objPageControls As New PageControls
                objPageControls.WorkOrderId = CCommon.ToLong(hdnWOID.Value)
                objPageControls.strPageType = "WorkOrder"
                objPageControls.CreateTemplateRow(tblMain)

                Dim m_aryRightsForRecord() As Integer = GetUserRightsForPage_Other(16, 153)

                If m_aryRightsForRecord(RIGHTSTYPE.UPDATE) = 1 Then
                    If CCommon.ToLong(Session("UserContactID")) = CCommon.ToLong(hdnRecordOwner.Value) Then
                        objPageControls.EditPermission = 1
                    Else
                        objPageControls.EditPermission = 0
                    End If
                ElseIf m_aryRightsForRecord(RIGHTSTYPE.UPDATE) = 3 Then
                    objPageControls.EditPermission = 1
                Else
                    objPageControls.EditPermission = 0
                End If

     
                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView

                Dim distinctNames = (From row In dtTableInfo.AsEnumerable() Select row.Field(Of String)("vcGroupName")).Distinct()

                For Each drGroupName As String In distinctNames
                    If drGroupName Is Nothing Then
                        drGroupName = ""
                    End If
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = drGroupName
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)
                    dvExcutedView.RowFilter = "vcGroupName = '" & drGroupName & "'"
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND vcGroupName='" & drGroupName & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND vcGroupName='" & drGroupName & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox" Or dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                                If dr("vcPropertyName") <> "numPartner" And dr("vcPropertyName") <> "numPartenerContact" And dr("vcPropertyName") <> "numPartenerSource" Then
                                    dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                                End If
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            dr("vcValue") = objWOrkOrder.GetType.GetProperty(dr("vcPropertyName")).GetValue(objWOrkOrder, Nothing)
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable


                            If dr("vcPropertyName") = "AssignedTo" Then ' If dr("numFieldId") = 106 Or dr("numFieldId") = 262 Then
                                If boolIntermediatoryPage = False Then
                                    dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)

                                    'Commissionable Contacts
                                    Dim objUser As New UserAccess
                                    objUser.DomainID = Context.Session("DomainID")
                                    objUser.byteMode = 1

                                    Dim dt As DataTable = objUser.GetCommissionsContacts

                                    Dim item As ListItem
                                    Dim dr1 As DataRow

                                    For Each dr2 As DataRow In dt.Rows
                                        dr1 = dtData.NewRow
                                        dr1(0) = dr2("numContactID")
                                        dr1(1) = dr2("vcUserName")
                                        dtData.Rows.Add(dr1)
                                    Next
                                End If
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If boolIntermediatoryPage = False Then
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If


                            If boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, Nothing, objWOrkOrder.WorkOrderID)
                            Else
                                Dim ddl As DropDownList
                                If dr("vcPropertyName") = "AssignedTo" Then
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, boolEnabled:=CCommon.ToBool(dr("bitCanBeUpdated")))
                                Else
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objWOrkOrder)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=objWOrkOrder.WorkOrderID)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If

                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If


                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next

                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function GradeDropDownItem(ByVal grade As String, ByVal taskID As Long) As ListItem
            Try
                Dim listItem As New ListItem
                listItem.Value = grade

                Dim listCapacityLoad As New System.Collections.Generic.List(Of Double)

                If Not dtEmployee Is Nothing AndAlso dtEmployee.Rows.Count > 0 Then
                    For Each drEmployee As DataRow In dtEmployee.Rows
                        If Not dtGrades Is Nothing AndAlso dtGrades.Select("numTaskId=" & taskID & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID")) & " AND vcGradeId='" & grade & "'").Length > 0 Then
                            listCapacityLoad.Add(CCommon.ToDouble(drEmployee("numCapacityLoad")))
                        End If
                    Next
                End If


                Dim capacityLoad As Double = If(listCapacityLoad.Count > 0, listCapacityLoad.Average(), 0)

                If capacityLoad <= 50 Then
                    listItem.Attributes.Add("style", "background-color:#ccfcd9")
                ElseIf capacityLoad >= 51 AndAlso capacityLoad <= 75 Then
                    listItem.Attributes.Add("style", "background-color:#ffffcd")
                ElseIf capacityLoad >= 76 AndAlso capacityLoad <= 100 Then
                    listItem.Attributes.Add("style", "background-color:#ffdc7d")
                ElseIf capacityLoad >= 101 Then
                    listItem.Attributes.Add("style", "background-color:#ffd1d1")
                End If

                listItem.Text = grade & " (" & capacityLoad & "%)"

                Return listItem
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Protected Sub rptMilestones_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptStages") Is Nothing Then
                        objWorkOrder = New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                        objWorkOrder.MilestoneName = CCommon.ToString(drv("vcMileStoneName"))
                        DirectCast(e.Item.FindControl("rptStages"), Repeater).DataSource = objWorkOrder.GetStages()
                        DirectCast(e.Item.FindControl("rptStages"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptStages_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptTasks") Is Nothing Then
                        objWorkOrder = New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                        objWorkOrder.StageDetailsID = CCommon.ToLong(drv("numStageDetailsId"))
                        objWorkOrder.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        DirectCast(e.Item.FindControl("rptTasks"), Repeater).DataSource = objWorkOrder.GetTasks(1)
                        DirectCast(e.Item.FindControl("rptTasks"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptTasks_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    Dim numEsitmatedTaskMinutes As Integer = 0
                    Dim numActualTaskMinutes As Integer = 0

                    If CCommon.ToString(drv("vcEstimatedTaskTime")).Contains(":") Then
                        numEsitmatedTaskMinutes = (CCommon.ToInteger(CCommon.ToString(drv("vcEstimatedTaskTime")).Split(":")(0)) * 60) + CCommon.ToInteger(CCommon.ToString(drv("vcEstimatedTaskTime")).Split(":")(1))
                    End If

                    If CCommon.ToString(drv("vcActualTaskTime")).Contains(":") Then
                        numActualTaskMinutes = (CCommon.ToInteger(CCommon.ToString(drv("vcActualTaskTime")).Split(":")(0)) * 60) + CCommon.ToInteger(CCommon.ToString(drv("vcActualTaskTime")).Split(":")(1))
                    End If

                    If CCommon.ToBool(drv("bitTaskCompleted")) Then
                        Dim decDifferene As Double = Math.Ceiling(((numEsitmatedTaskMinutes - numActualTaskMinutes) / numActualTaskMinutes) * 100)

                        If decDifferene > 5 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = decDifferene & "% under"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-green"
                        ElseIf decDifferene <= 5 And decDifferene >= -5 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = "On time"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-green"
                        ElseIf decDifferene < -5 AndAlso decDifferene >= -25 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = decDifferene & "% Over"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-orange"
                        ElseIf decDifferene < -25 Then
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).Text = decDifferene & "% Over"
                            DirectCast(e.Item.FindControl("lblPerformance"), Label).CssClass = "text-red"
                        End If
                    End If

                    If m_aryRightsWIPTab(RIGHTSTYPE.UPDATE) = 3 Then
                        DirectCast(e.Item.FindControl("divTaskControlsWorkOrder"), HtmlGenericControl).Visible = True
                    ElseIf m_aryRightsWIPTab(RIGHTSTYPE.UPDATE) = 1 Then
                        If CCommon.ToLong(Session("UserContactID")) = CCommon.ToLong(drv("numAssignTo")) Then
                            DirectCast(e.Item.FindControl("divTaskControlsWorkOrder"), HtmlGenericControl).Visible = True
                        Else
                            DirectCast(e.Item.FindControl("divTaskControlsWorkOrder"), HtmlGenericControl).Visible = False
                        End If
                    Else
                        DirectCast(e.Item.FindControl("divTaskControlsWorkOrder"), HtmlGenericControl).Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptMilestonesManageWIP_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptStagesManageWIP") Is Nothing Then
                        objWorkOrder = New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                        objWorkOrder.MilestoneName = CCommon.ToString(drv("vcMileStoneName"))
                        DirectCast(e.Item.FindControl("rptStagesManageWIP"), Repeater).DataSource = objWorkOrder.GetStages()
                        DirectCast(e.Item.FindControl("rptStagesManageWIP"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptStagesManageWIP_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("rptTasksManageWIP") Is Nothing Then
                        objWorkOrder = New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                        objWorkOrder.StageDetailsID = CCommon.ToLong(drv("numStageDetailsId"))
                        objWorkOrder.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        DirectCast(e.Item.FindControl("rptTasksManageWIP"), Repeater).DataSource = objWorkOrder.GetTasks(2)
                        DirectCast(e.Item.FindControl("rptTasksManageWIP"), Repeater).DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rptTasksManageWIP_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If dtEmployee Is Nothing Then
                        objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                        objCommon.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        Dim tempDateTime As DateTime = DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset"))
                        dtEmployee = objCommon.GetEmployeesWithCapacityLoad(CCommon.ToLong(hdnWOID.Value), CCommon.ToShort(hdnDateRange.Value), New DateTime(rdManageWIPDate.SelectedDate.Value.Year, rdManageWIPDate.SelectedDate.Value.Month, rdManageWIPDate.SelectedDate.Value.Day, tempDateTime.Hour, tempDateTime.Minute, tempDateTime.Second))

                        dtEmployee.DefaultView.Sort = "numCapacityLoad ASC"
                        dtEmployee = dtEmployee.DefaultView.ToTable()
                    End If

                    If dtTeam Is Nothing Then
                        objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                        objCommon.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        Dim tempDateTime As DateTime = DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset"))
                        dtTeam = objCommon.GetTeamsWithCapacityLoad(CCommon.ToLong(hdnWOID.Value), CCommon.ToShort(hdnDateRange.Value), New DateTime(rdManageWIPDate.SelectedDate.Value.Year, rdManageWIPDate.SelectedDate.Value.Month, rdManageWIPDate.SelectedDate.Value.Day, tempDateTime.Hour, tempDateTime.Minute, tempDateTime.Second))

                        dtTeam.DefaultView.Sort = "numCapacityLoad ASC"
                        dtTeam = dtTeam.DefaultView.ToTable()
                    End If

                    If dtGrades Is Nothing Then
                        Dim objStageGradeDetails As New StageGradeDetails
                        objStageGradeDetails.DomainID = CCommon.ToLong(Session("DomainID"))
                        dtGrades = objStageGradeDetails.GetByProcess(CCommon.ToLong(hdnBuildProcess.Value))
                    End If

                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If Not e.Item.FindControl("ddlAssignedTo") Is Nothing Then
                        Dim listItem As ListItem

                        If Not dtEmployee Is Nothing AndAlso dtEmployee.Rows.Count > 0 Then
                            For Each drEmployee As DataRow In dtEmployee.Rows
                                listItem = New ListItem

                                If Not dtGrades Is Nothing AndAlso dtGrades.Select("numTaskId=" & CCommon.ToLong(drv("numReferenceTaskId")) & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID"))).Length > 0 Then
                                    listItem.Text = CCommon.ToString(drEmployee("vcEmployeeName")) & " (" & CCommon.ToString(drEmployee("numCapacityLoad")) & "%)" & " " & dtGrades.Select("numTaskId=" & CCommon.ToLong(drv("numReferenceTaskId")) & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID")))(0)("vcGradeId")
                                    listItem.Attributes.Add("Grade", dtGrades.Select("numTaskId=" & CCommon.ToLong(drv("numReferenceTaskId")) & " AND numAssigneId=" & CCommon.ToLong(drEmployee("numEmployeeID")))(0)("vcGradeId"))
                                Else
                                    listItem.Text = CCommon.ToString(drEmployee("vcEmployeeName")) & " (" & CCommon.ToString(drEmployee("numCapacityLoad")) & "%)"
                                    listItem.Attributes.Add("Grade", "")
                                End If


                                listItem.Value = CCommon.ToLong(drEmployee("numEmployeeID"))

                                If CCommon.ToLong(drEmployee("numCapacityLoad")) <= 50 Then
                                    listItem.Attributes.Add("style", "background-color:#ccfcd9")
                                ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 75 Then
                                    listItem.Attributes.Add("style", "background-color:#ffffcd")
                                ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 100 Then
                                    listItem.Attributes.Add("style", "background-color:#ffdc7d")
                                ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 101 Then
                                    listItem.Attributes.Add("style", "background-color:#ffd1d1")
                                End If

                                If CCommon.ToLong(drEmployee("numEmployeeID")) = CCommon.ToLong(drv("numAssignTo")) Then
                                    listItem.Selected = True

                                    If CCommon.ToLong(drEmployee("numCapacityLoad")) <= 50 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ccfcd9")
                                    ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 75 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ffffcd")
                                    ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drEmployee("numCapacityLoad")) <= 100 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ffdc7d")
                                    ElseIf CCommon.ToLong(drEmployee("numCapacityLoad")) >= 101 Then
                                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Style.Add("background-color", "#ffd1d1")
                                    End If
                                End If

                                DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Items.Add(listItem)
                            Next
                        End If

                        listItem = New ListItem
                        listItem.Text = "-- Select One --"
                        listItem.Value = "0"
                        listItem.Attributes.Add("style", "background-color:#ffffff")

                        DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Items.Insert(0, listItem)

                        If m_aryRightsManageWIPTab(RIGHTSTYPE.UPDATE) = 3 Then
                            DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = True
                        ElseIf m_aryRightsManageWIPTab(RIGHTSTYPE.UPDATE) = 1 Then
                            If CCommon.ToLong(Session("UserContactID")) = CCommon.ToLong(hdnRecordOwner.Value) Then
                                DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = True
                            Else
                                DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = False
                            End If
                        Else
                            DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).Enabled = False
                        End If
                    End If

                    If Not e.Item.FindControl("ddlTeam") Is Nothing Then
                        Dim listItem As ListItem

                        If Not dtTeam Is Nothing AndAlso dtTeam.Rows.Count > 0 Then
                            For Each drTeam As DataRow In dtTeam.Rows
                                listItem = New ListItem
                                listItem.Text = CCommon.ToString(drTeam("vcTeamName")) & " (" & CCommon.ToString(drTeam("numCapacityLoad")) & "%)"
                                listItem.Value = CCommon.ToLong(drTeam("numTeamID"))

                                If CCommon.ToLong(drTeam("numCapacityLoad")) <= 50 Then
                                    listItem.Attributes.Add("style", "background-color:#ccfcd9")
                                ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 75 Then
                                    listItem.Attributes.Add("style", "background-color:#ffffcd")
                                ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 100 Then
                                    listItem.Attributes.Add("style", "background-color:#ffdc7d")
                                ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 101 Then
                                    listItem.Attributes.Add("style", "background-color:#ffd1d1")
                                End If

                                If CCommon.ToLong(drTeam("numTeamID")) = CCommon.ToLong(drv("numTeam")) Then
                                    listItem.Selected = True

                                    If CCommon.ToLong(drTeam("numCapacityLoad")) <= 50 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ccfcd9")
                                    ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 51 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 75 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ffffcd")
                                    ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 76 AndAlso CCommon.ToLong(drTeam("numCapacityLoad")) <= 100 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ffdc7d")
                                    ElseIf CCommon.ToLong(drTeam("numCapacityLoad")) >= 101 Then
                                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Style.Add("background-color", "#ffd1d1")
                                    End If
                                End If

                                DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Items.Add(listItem)
                            Next
                        End If

                        listItem = New ListItem
                        listItem.Text = "-- Select One --"
                        listItem.Value = "0"
                        listItem.Attributes.Add("style", "background-color:#ffffff")

                        DirectCast(e.Item.FindControl("ddlTeam"), DropDownList).Items.Insert(0, listItem)
                    End If

                    If Not e.Item.FindControl("ddlGrade") Is Nothing Then
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("A+", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("A", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("A-", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("B+", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("B", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("B-", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("C+", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("C", CCommon.ToLong(drv("numReferenceTaskId"))))
                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Add(GradeDropDownItem("C-", CCommon.ToLong(drv("numReferenceTaskId"))))

                        Dim listItem As New ListItem
                        listItem = New ListItem
                        listItem.Text = "-- Select One --"
                        listItem.Value = "0"
                        listItem.Attributes.Add("style", "background-color:#ffffff")

                        DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.Insert(0, listItem)

                        If Not DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).SelectedItem.Attributes("Grade") Is Nothing Then
                            Dim grade As String = CCommon.ToString(DirectCast(e.Item.FindControl("ddlAssignedTo"), DropDownList).SelectedItem.Attributes("Grade"))
                            If Not DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.FindByValue(grade) Is Nothing Then
                                DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.FindByValue(grade).Selected = True
                                DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).BackColor = System.Drawing.ColorTranslator.FromHtml(DirectCast(e.Item.FindControl("ddlGrade"), DropDownList).Items.FindByValue(grade).Attributes("style").Replace("background-color:", ""))
                            End If
                        End If
                    End If

                    If CCommon.ToBool(drv("bitTaskStarted")) Then
                        DirectCast(e.Item.FindControl("btnStartAgain"), Button).Visible = True
                        DirectCast(e.Item.FindControl("txtTaskHours"), HtmlInputGenericControl).Attributes.Add("disabled", "")
                        DirectCast(e.Item.FindControl("txtTaskMinutes"), HtmlInputGenericControl).Attributes.Add("disabled", "")
                    End If

                    DirectCast(e.Item.FindControl("txtTaskHours"), HtmlInputGenericControl).ID = DirectCast(e.Item.FindControl("txtTaskHours"), HtmlInputGenericControl).ID & CCommon.ToString(drv("numTaskID"))
                    DirectCast(e.Item.FindControl("txtTaskMinutes"), HtmlInputGenericControl).ID = DirectCast(e.Item.FindControl("txtTaskMinutes"), HtmlInputGenericControl).ID & CCommon.ToString(drv("numTaskID"))

                    If Not e.Item.FindControl("plhDocuments") Is Nothing Then
                        Dim plhDocuments As PlaceHolder
                        plhDocuments = DirectCast(e.Item.FindControl("plhDocuments"), PlaceHolder)
                        Dim dtDocLinkName As New DataTable
                        Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                        With objOpp
                            .DomainID = Session("DomainID")
                            .DivisionID = CCommon.ToLong(CCommon.ToString(drv("numTaskID")))
                            dtDocLinkName = .GetDocumentFilesLink("T")
                        End With

                        If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then

                            Dim docRows = (From dRow In dtDocLinkName.Rows
                                           Where CCommon.ToString(dRow("cUrlType")) = "L"
                                           Select New With {Key .fileName = dRow("VcFileName"), .fileId = dRow("numGenericDocID")}).Distinct()
                            If docRows.Count() > 0 Then
                                Dim _docName = String.Empty
                                Dim countId = 0

                                For Each dr In docRows
                                    Dim strDocName = CCommon.ToString(dr.fileName)
                                    Dim fileID = CCommon.ToString(dr.fileId)
                                    Dim strFileLogicalPath = String.Empty
                                    If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                        Dim f As System.IO.FileInfo = New System.IO.FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                        Dim strFileSize As String = f.Length.ToString
                                        strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                                    End If

                                    Dim docLink As New HyperLink

                                    Dim imgDelete As New ImageButton
                                    imgDelete.ImageUrl = "../images/Delete24.png"
                                    imgDelete.Height = 13
                                    imgDelete.ToolTip = "Delete"
                                    imgDelete.ID = "imgDelete" + CCommon.ToString(countId)
                                    imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                                    Dim Space As New LiteralControl
                                    Space.Text = "&nbsp;"

                                    docLink.Text = strDocName
                                    docLink.NavigateUrl = strFileLogicalPath
                                    docLink.ID = "hpl" + CCommon.ToString(countId)
                                    docLink.Target = "_blank"
                                    Dim createDocDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                                    createDocDiv.Controls.Add(docLink)
                                    createDocDiv.Controls.Add(Space)
                                    createDocDiv.Controls.Add(imgDelete)
                                    plhDocuments.Controls.Add(createDocDiv)
                                    countId = countId + 1
                                Next
                                'imgDocument.Visible = True
                            Else
                                'imgDocument.Visible = False
                            End If

                            Dim linkRows = (From dRow In dtDocLinkName.Rows
                                            Where CCommon.ToString(dRow("cUrlType")) = "U"
                                            Select New With {Key .linkURL = dRow("VcFileName"), Key .linkName = dRow("VcDocName"), .fileId = dRow("numGenericDocID")}).Distinct()
                            If linkRows.Count() > 0 Then
                                Dim _linkName = String.Empty
                                Dim _linkURL = String.Empty
                                Dim fileLinkID = String.Empty
                                Dim countLinkId = 0

                                For Each dr In linkRows
                                    '_linkName = _linkName + ", " + CCommon.ToString(dr.linkName)
                                    '_linkURL = _linkURL + ",<br/> " + CCommon.ToString(dr.linkURL)

                                    _linkName = CCommon.ToString(dr.linkName)
                                    _linkURL = CCommon.ToString(dr.linkURL)
                                    fileLinkID = CCommon.ToString(dr.fileId)
                                    Dim uriBuilder As UriBuilder = New UriBuilder(_linkURL)
                                    'UriBuilder builder = New UriBuilder(myUrl)
                                    'charityNameText.NavigateUrl = builder.Uri.AbsoluteUri

                                    Dim link As New HyperLink
                                    Dim imgLinkDelete As New ImageButton
                                    imgLinkDelete.ImageUrl = "../images/Delete24.png"
                                    imgLinkDelete.Height = 13
                                    imgLinkDelete.ToolTip = "Delete"
                                    imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(countLinkId)
                                    imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileLinkID & ");return false;")

                                    link.Text = _linkName
                                    link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                                    'link.NavigateUrl = _linkURL
                                    link.ID = "hplLink" + CCommon.ToString(countLinkId)
                                    link.Target = "_blank"
                                    Dim createLinkDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                                    createLinkDiv.Controls.Add(link)
                                    createLinkDiv.Controls.Add(imgLinkDelete)
                                    plhDocuments.Controls.Add(createLinkDiv)
                                    countLinkId = countLinkId + 1
                                Next
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub radWorkOrderTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs)
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radWorkOrderTab.SelectedIndex)
                PersistTable.Save(boolOnlyURL:=True)

                Dim currentTab As Short = e.Tab.Index

                rptMilestones.DataSource = Nothing
                rptMilestones.DataBind()

                rptMilestonesManageWIP.DataSource = Nothing
                rptMilestonesManageWIP.DataBind()

                If currentTab = 0 Then
                    BindWIP(False)
                ElseIf currentTab = 1 Then
                    BindWIP(True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnDateRange_Click(sender As Object, e As EventArgs)
            Try
                BindWIP(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rdManageWIPDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs)
            Try
                BindWIP(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rtlBOM_ItemDataBound(sender As Object, e As Telerik.Web.UI.TreeListItemDataBoundEventArgs)
            Try
                If e.Item.ItemType = Telerik.Web.UI.TreeListItemType.Item Or e.Item.ItemType = Telerik.Web.UI.TreeListItemType.AlternatingItem Then
                    If Not CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitWorkOrder")) AndAlso CCommon.ToLong(hdnWOStatus.Value) <> 23184 Then
                        If Not CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitReadyToBuild")) Then
                            e.Item.Style.Add("background-color", "#FFE1E1 !important")
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

        Protected Sub btnFinish_Click(sender As Object, e As EventArgs)
            Try
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")
                Dim dtCommitAllocation As DataTable = objCommon.GetDomainSettingValue("tintCommitAllocation")

                If dtCommitAllocation Is Nothing Or dtCommitAllocation.Rows.Count = 0 Then
                    Throw New Exception("Not able to fetch commmit allocation global settings value.")
                    Exit Sub
                End If

                'Validate if there are any task which are pending to complete
                objWorkOrder = New WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                If objWorkOrder.IsTaskPending() Then
                    ShowMessage("Work order could not be completed until all tasks are finished.")
                Else
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim objWorkOrder As New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                        Dim itemCode As Long = CCommon.ToLong(objWorkOrder.GetFieldValue("numItemCode"))
                        Dim quantityBuilt As Double = CCommon.ToLong(objWorkOrder.GetFieldValue("numQuantityBuilt"))

                        'KEEP THIS BELOW ABOVE CALLES OTHERWISE QuantityBuild WILL BE WRONG VALUE
                        Dim objItems As New CItems
                        objItems.numWOId = CCommon.ToLong(hdnWOID.Value)
                        objItems.numWOStatus = 23184
                        objItems.UserCntID = Session("UserContactID")
                        objItems.DomainID = Session("DomainID")
                        objItems.ManageWorkOrderStatus()

                        btnFinish.Visible = False
                        btnDelete.Visible = False

                        objTransactionScope.Complete()
                    End Using
                End If
            Catch ex As Exception
                If ex.Message.Contains("BOM_LEFT_TO_BE_PICKED") Then
                    ShowMessage("You must have to pick all BOM before work order can be finished.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
            Try
                'Validate if there are any task which are pending to complete
                objWorkOrder = New WorkOrder
                objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                objWorkOrder.WorkOrderID = CCommon.ToLong(hdnWOID.Value)
                If objWorkOrder.IsCompleted() Then
                    ShowMessage("You are not allowed to delete work order once it is marked as finished")
                Else
                    If objWorkOrder.IsTaskStarted() Then
                        ShowMessage("You are not allowed to delete once work order tasks are started.")
                    Else
                        Dim objItems As New CItems
                        objItems.numWOId = CCommon.ToLong(hdnWOID.Value)
                        objItems.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItems.bitOrderDelete = False
                        objItems.DeleteWorkOrder()
                        Response.Redirect("~/Opportunity/frmWorkOrderList.aspx", False)
                    End If
                End If
            Catch ex As Exception
                If ex.Message.Contains("CHILD_WORK_ORDER_EXISTS") Then
                    DisplayError("You can't delete this work order untill all child work order(s) are deleted.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub
    End Class

End Namespace

