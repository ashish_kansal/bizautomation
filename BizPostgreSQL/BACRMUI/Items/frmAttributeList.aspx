<%@ Page Language="vb"  EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmAttributeList.aspx.vb" Inherits=".frmAttributeList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
</head>
<script type="text/javascript" language="javascript">
function New()
{
    window.location.href = "frmAttributes.aspx";
}

</script>
<body>
    <form id="form1" runat="server">
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Attributes&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
            </td>
             <td align="right">
                    <input type="button"  runat="server" id="btnNew" class="button" value="New Attributes" onclick="New()" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:table id="Table3" Width="100%" Runat="server" BorderWidth="1" Height="350" GridLines="None" CssClass="aspTable"
						BorderColor="black" CellSpacing="0" CellPadding="0">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
				
 
								<asp:datagrid id="dgAttributes" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
									BorderColor="white">
									<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
									<ItemStyle CssClass="is"></ItemStyle>
									<HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="numItemAttrOptAccID"></asp:BoundColumn>
										<asp:ButtonColumn DataTextField="vcName" SortExpression="vcName" HeaderText="<font color=white>Attribute</font>"
											CommandName="Attribute"></asp:ButtonColumn>
										<asp:TemplateColumn>
											<ItemTemplate>
												<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid>
							</asp:TableCell>
						</asp:TableRow>
					</asp:table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
