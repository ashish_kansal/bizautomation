<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmVendorList.aspx.vb" Inherits="BACRM.UserInterface.Items.frmVendorList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Vendor</title>
		<script language="javascript" type="text/javascript" >
		function CheckRadio(a)
		{
			var stri; 
			for (i = 1; i < dgVendor.rows.length; i++)
					{
					  if (i<10)
				        {
				           stri='0'+(i+1)
				        }
				         else 
				         {
				            stri=i+1
				         }
					  if (typeof(document.all['dgVendor_ctl'+(stri)+'_radPreffered'])!='undefined')
					  {
						document.all['dgVendor_ctl'+(stri)+'_radPreffered'].checked=false;
					  }
					}
					
					document.all[a].checked=true;
					
		}
		
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function Add()
			{
		
			if (document.Form1.ddlCompany.value==0)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			if (document.Form1.ddlCompany.selectedIndex==-1)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			}
		function Save()
		{
		    var stri;
			var str='';
			for (i = 1; i <= dgVendor.rows.length; i++)
				{
				    if (i<10)
				        {
				           stri='0'+(i+1)
				        }
				 else 
				 {
				    stri=i+1
				 }
				 if (typeof(document.all['dgVendor_ctl'+(stri)+'_txtPartNo'])!='undefined')
					  {
						str= str + document.all['dgVendor_ctl'+(stri)+'_lblvendorID'].innerText+',' + document.all['dgVendor_ctl'+(stri)+'_txtPartNo'].value+',' + document.all['dgVendor_ctl'+(stri)+'_txtCost'].value+'~'
					  }
				}
			for (i = 1; i <= dgVendor.rows.length; i++)
				{
				if (i<10)
				        {
				           stri='0'+(i+1)
				        }
				 else 
				 {
				    stri=i+1
				 }
				if (typeof(document.all['dgVendor_ctl'+(stri)+'_radPreffered'])!='undefined')
					  {
					 if (document.all['dgVendor_ctl'+(stri)+'_radPreffered'].checked==true)
					  {
						document.Form1.txtVendorId.value= document.all['dgVendor_ctl'+(stri)+'_lblvendorID'].innerText
					  }
						
					  }
				
				}
			
			document.Form1.txtHidden.value=str;
		}
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td class="normal1" align="right">Vendor
					</td>
					<td class="normal1"><asp:textbox id="txtSearch" CssClass="signup" Width="120" Runat="server"></asp:textbox>
					&nbsp;
					<asp:button id="btnGo" CssClass="button" Runat="server" Text="Go" Width="25"></asp:button>
					&nbsp;
					<asp:dropdownlist id="ddlCompany" CssClass="signup" Runat="server" Width="200"></asp:dropdownlist>
					&nbsp;
					<asp:button id="btnAdd" CssClass="button" Runat="server" Text="Add" Width="50"></asp:button>
					&nbsp;
					<asp:button id="btnSave" CssClass="button" Runat="server" Text="Save" Width="50"></asp:button>
					&nbsp;&nbsp;&nbsp;(Click here to save vendor details)
					</td>
				</tr>
			</table>
			<asp:datagrid id="dgVendor" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn Visible="False" DataField="numVendorID"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Preferred">
						<ItemTemplate>
							<asp:RadioButton ID="radPreffered" Runat="server" GroupName="rad"></asp:RadioButton>
							<asp:Label ID="lblvendorID" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numVendorID") %>' style="display:none" >
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Vendor" HeaderText="Vendor Name"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Part #">
						<ItemTemplate>
							<asp:TextBox ID="txtPartNo" Runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container.DataItem, "vcPartNo") %>'>
							</asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="ConName" HeaderText="Primary Contact"></asp:BoundColumn>
					<asp:BoundColumn DataField="Phone" HeaderText="Phone Number, Ext"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Cost">
						<ItemTemplate>
							<asp:TextBox ID="txtCost" Runat="server" CssClass="signup"  Text='<%# Format(DataBinder.Eval(Container.DataItem, "monCost"),"#0.00") %>'>
							</asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn>
						<HeaderTemplate>
							<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			<asp:TextBox ID="txtHidden" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtVendorId" Runat="server" style="DISPLAY:none"></asp:TextBox>
		</form>
	</body>
</html>
