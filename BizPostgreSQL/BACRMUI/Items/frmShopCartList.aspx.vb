Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class Items_frmShopCartList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then LoadDTLs()
            
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDTLs()
        Try
            Dim objItems As New CItems
            Dim dtTable As DataTable
            objItems.ItemCode = GetQueryStringVal( "ItemCode")
            objItems.byteMode = 0
            objItems.UserCntId = Session("UserContactID")
            RadEditor1.Content = objItems.ManageItemExtendedDesc
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objItems As New CItems
            objItems.ItemCode = GetQueryStringVal( "ItemCode")
            objItems.byteMode = 1
            objItems.str = RadEditor1.Content.Trim()
            objItems.UserCntId = Session("UserContactID")
            objItems.ManageItemExtendedDesc()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class