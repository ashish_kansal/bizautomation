﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfItemList.aspx.vb"
    Inherits=".frmConfItemList" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Column Customization</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("hdnCol").value = str;
            //  alert( document.getElementById("hdnCol").value)
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button" Width="50" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text="Item search result fields"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal><asp:HiddenField ID="hfRowIndex"
                    runat="server" />
            </td>
        </tr>
    </table>
    <asp:Table ID="tblAdvSearchFieldCustomization" runat="server" Width="600" GridLines="None"
        CssClass="aspTable" BorderColor="black" BorderWidth="1">
        <asp:TableRow ID="trView" Visible="false">
            <asp:TableCell>
                <table>
                    <tr>
                        <td class="normal1">View :
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlView" runat="server" CssClass="signup" AutoPostBack="True">
                                <asp:ListItem Value="3">1</asp:ListItem>
                                <asp:ListItem Value="4">2</asp:ListItem>
                                <asp:ListItem Value="5">3</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="trSort" Visible="false">
            <asp:TableCell>
                <table>
                    <tr>
                        <td class="normal1">Choose Sort Type :
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSortType" runat="server" CssClass="signup" AutoPostBack="True">
                                <asp:ListItem Value="85">Primary</asp:ListItem>
                                <asp:ListItem Value="84">Secondary</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;<asp:Label ID="lblBizdocStatus" Text="[?]" CssClass="tip" runat="server" ToolTip="<p>Primary Sort Columns will be available in Sort Dropdown within Shopping Cart.</p> While Secondary Sort columns will do sorting in background." />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="normal1" valign="top">Available Fields<br>
                            &nbsp;&nbsp;
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript: move(document.getElementById('lstAvailablefld'), document.getElementById('lstSelectedfld'))"/>
                            <br/>
                            <br/>
                            <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript: remove1(document.getElementById('lstSelectedfld'), document.getElementById('lstAvailablefld'));"/>
                        </td>
                        <td align="center" class="normal1">Selected Fields/ Choose Order<br>
                            <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <img id="btnMoveupOne" src="../images/upArrow.gif" alt="" onclick="javascript:MoveUp(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br/>
                            <br/>
                            <img id="btnMoveDownOne" src="../images/downArrow1.gif" alt="" onclick="javascript:MoveDown(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <asp:Label ID="lblMaxFields" runat="server" Text="(Max 15)"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trSalesFulfill">
            <asp:TableCell>
                <table width="100%">
                    <tr>
                        <td colspan="2" class="normal7">When you click Sales Fulfillment, the list will default based on your settings here:
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1">Filter :
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSortSalesFulFillment" runat="server" CssClass="signup">
                                <asp:ListItem Value="1">Fully Invoiced Items</asp:ListItem>
                                <asp:ListItem Value="2">Partially Invoiced Items</asp:ListItem>
                                <asp:ListItem Value="3">Non-invoiced items</asp:ListItem>
                                <asp:ListItem Value="0">All Open Items</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
    <input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
    <input id="hdSiteID" type="hidden" name="hdSave" runat="server" value="False" />
</asp:Content>
