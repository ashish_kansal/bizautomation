Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports System.Collections.Generic
Imports System.Xml.Linq


Partial Public Class frmSerializeItems
    Inherits BACRMPage

#Region "Member Variables"
    Dim lngItemCode As Long
    Dim iSerialize As Integer
    Dim vcExternalLocation As Long
    Dim vcInternalLocationID As Long
    Dim vcInternalLocationText As String

#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
        iSerialize = CCommon.ToInteger(GetQueryStringVal("Serialize"))
        vcExternalLocation = CCommon.ToLong(GetQueryStringVal("vcExternalLocation"))
        vcInternalLocationID = CCommon.ToLong(GetQueryStringVal("vcInternalLocationID"))
        vcInternalLocationText = CCommon.ToString(GetQueryStringVal("vcInternalLocationText"))
        Try
            lblErrorMessage.Text = ""
            If Not IsPostBack Then
                hdnWarehouseItemID.Value = CCommon.ToLong(GetQueryStringVal("numWarehouseItemID"))

                If CCommon.ToLong(hdnWarehouseItemID.Value) > 0 Then
                    If iSerialize = 0 Then
                        dgItem.Columns(3).HeaderText = "Lot#"
                        Page.Title = "LOT Items"
                    ElseIf iSerialize = 1 Then
                        dgItem.Columns(4).Visible = False
                        dgItem.Columns(5).Visible = False
                    End If

                    Dim dtItemDetails As DataTable
                    Dim objItems As New CItems
                    objItems.ItemCode = lngItemCode
                    objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
                    dtItemDetails = objItems.ItemDetails()
                    If dtItemDetails.Rows.Count > 0 Then
                        hfAverageCost.Value = String.Format("{0:###0.00}", dtItemDetails.Rows(0).Item("monAverageCost"))
                        hfItemName.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("vcItemName"))
                        hfAssetChartAcntID.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("numAssetChartAcntId"))
                    End If

                    PerformValidation()
                    LoadWarehouse()
                End If
            End If

            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub LoadWarehouse()
        Try
            Dim objItems As New CItems
            Dim ds As DataSet
            objItems.ItemCode = lngItemCode
            objItems.WareHouseItemID = CCommon.ToLong(hdnWarehouseItemID.Value)
            ds = objItems.GetItemWareHouses()

            Dim dt As DataTable = ds.Tables(1)
            dgItem.DataSource = dt
            dgItem.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub PerformValidation()
        Try
            'Validate if Asset Account is set
            Dim objItem As New CItems
            If Not objItem.ValidateItemAccount(lngItemCode, Session("DomainID"), 1) = True Then
                btnSave.Visible = False
                btnDelete.Visible = False
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("OE", Session("DomainID")) = 0 Then
                btnSave.Visible = False
                btnDelete.Visible = False
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SerializeItemValidate", "alert('Please Map Default Opening Balance Equity Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.')", True)

            End If

            txtSerialLot.Text = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim objItem As New CItems
            Dim intQtyAdj As Integer

            dt.Columns.Add("numWareHouseItmsDTLID", GetType(System.Decimal))
            dt.Columns(0).AutoIncrement = True
            dt.Columns.Add("numWareHouseItemID", GetType(System.Decimal))
            dt.Columns.Add("vcSerialNo")
            dt.Columns.Add("OldQty")
            dt.Columns.Add("NewQty")
            dt.Columns.Add("dExpirationDate")

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                For Each item As DataGridItem In dgItem.Items
                    dr = dt.NewRow
                    dr("numWareHouseItmsDTLID") = item.Cells(0).Text
                    dr("numWareHouseItemID") = item.Cells(1).Text
                    dr("vcSerialNo") = CType(item.FindControl("txtSerialNo"), TextBox).Text
                    dr("OldQty") = CCommon.ToLong(CType(item.FindControl("hfQty"), HiddenField).Value)

                    If iSerialize = 0 Then
                        dr("NewQty") = If(CType(item.FindControl("txtQty"), TextBox).Visible, CType(item.FindControl("txtQty"), TextBox).Text, CType(item.FindControl("lblQty"), Label).Text)
                    Else
                        dr("NewQty") = 1
                    End If

                    If dr("NewQty") > 0 Then
                        dt.Rows.Add(dr)

                        intQtyAdj = CCommon.ToInteger(dr("NewQty")) - CCommon.ToInteger(dr("OldQty"))
                        If intQtyAdj <> 0 Then
                            objItem.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), intQtyAdj, CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        End If
                    End If

                    If iSerialize = 0 Then
                        dr("dExpirationDate") = CCommon.ToSqlDate(CType(item.FindControl("radExpirationDate"), Telerik.Web.UI.RadDatePicker).SelectedDate)
                    Else
                        dr("dExpirationDate") = CCommon.ToSqlDate(New Date(1753, 1, 1))
                    End If
                Next

                dt.TableName = "SerializedItems"
                ds.Tables.Add(dt)

                objItem.strFieldList = ds.GetXml
                objItem.DomainID = Session("DomainID")
                objItem.UserCntID = Session("UserContactID")
                objItem.ManageItemSerialNo()

                objTransactionScope.Complete()
            End Using

            'To add new serial number-->End
            SerialAndLotItems()
            LoadWarehouse()

        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_SERIAL_NUMBER") Then
                lblErrorMessage.Text = "Duplicate serial numbers are not allowed."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub SerialAndLotItems()
        Try

            Dim vcSerialLotNo As String
                Dim finalListSerialLot As New List(Of SerialLot)

            If Not String.IsNullOrEmpty(txtSerialLot.Text.Trim()) Then
                'FIRST VALIDATE SERIAL/LOT# NUMBERS ARE NOT DUPLICATED IN TEXTBOX
                Dim rgx As New System.Text.RegularExpressions.Regex("^[^\(\)]*(\(\d+\)\([^\(\)]*\)){1,1}$")
                Dim listSerialLot As List(Of String) = txtSerialLot.Text.Split(",").ToList()


                If iSerialize = 0 Then 'Lot
                    Dim tempLotNo As String
                    Dim tempQty As Int32
                    Dim dtExpirationDate As DateTime

                    For Each strSerialLot As String In listSerialLot
                        If Not rgx.IsMatch(strSerialLot) Then
                            lblErrorMessage.Text = "Some Lot #s are not in valid format."
                            Exit Sub
                        Else
                            tempLotNo = strSerialLot.Substring(0, strSerialLot.Trim().IndexOf("(")).Trim()

                            If finalListSerialLot.FindIndex(Function(x) x.vcSerialLot.Equals(tempLotNo, StringComparison.OrdinalIgnoreCase)) <> -1 Then
                                lblErrorMessage.Text = "Duplicate Lot #s are not allowed."
                                Exit Sub
                            End If

                            If Not Int32.TryParse(strSerialLot.Substring(strSerialLot.Trim().IndexOf("(") + 1, strSerialLot.IndexOf(")") - strSerialLot.Trim().IndexOf("(") - 1), tempQty) Then
                                lblErrorMessage.Text = "Some Lot #s quantity value is invalid."
                                Exit Sub
                            End If

                            If Not DateTime.TryParse(DateFromFormattedDate(strSerialLot.Substring(strSerialLot.Trim().LastIndexOf("(") + 1, strSerialLot.LastIndexOf(")") - strSerialLot.LastIndexOf("(") - 1), Session("DateFormat")), dtExpirationDate) Then
                                lblErrorMessage.Text = "Some Lot #s expiration date is invlaid."
                                Exit Sub
                            End If

                            finalListSerialLot.Add(New SerialLot With {.vcSerialLot = tempLotNo, .numQty = tempQty, .dtExpirationDate = dtExpirationDate})
                        End If
                    Next
                ElseIf iSerialize = 1 Then
                    For Each strSerialLot As String In listSerialLot
                        If finalListSerialLot.FindIndex(Function(x) x.vcSerialLot.Equals(strSerialLot.Trim(), StringComparison.OrdinalIgnoreCase)) <> -1 Then
                            lblErrorMessage.Text = "Duplicate serial #s are not allowed."
                            Exit Sub
                        Else
                            finalListSerialLot.Add(New SerialLot With {.vcSerialLot = strSerialLot.Trim(), .numQty = 1, .dtExpirationDate = Nothing})
                        End If
                    Next
                End If

                'NOW CHECK IF SERIAL LOT# ARE NOT DUPLICATED ACROSS WAREHOUSES
                'NOTE: USER CAN NOT ADD DUPLICATE SERIAL NUMBER IRESPECTIVE OF WAREHOUSE OR LOCATION
                'NOTE: USER CAN ADD DUPLICATE LOT NUMBER IN UNIQUE WAREHOUSE AND LOCATION BUT NOT IN SAME
                Dim objItem As New CItems
                objItem.ItemCode = lngItemCode
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.WarehouseID = vcExternalLocation
                objItem.WareHouseItemID = CCommon.ToLong(hdnWarehouseItemID.Value)
                objItem.bitSerialized = CCommon.ToBool(iSerialize)

                Dim xEle = New XElement("SerialLots",
                                From strSerialLot In finalListSerialLot
                                Select New XElement("SerialLot",
                                                    New XElement("vcSerialLot", strSerialLot.vcSerialLot),
                                                    New XElement("numQty", strSerialLot.numQty),
                                                    New XElement("dtExpirationDate", strSerialLot.dtExpirationDate)
                                                    )
                                    )

                vcSerialLotNo = xEle.ToString()
                objItem.SerialNo = vcSerialLotNo

                'RETURNS TRUE IF SERIAL/LOT# ARE DUPLICATE
                If objItem.ValidateNewSerialLotNo() Then
                    If CCommon.ToBool(iSerialize) Then
                        lblErrorMessage.Text = "Duplicate serial #s are not allowed. Doen't matter if external & internal location is unique."
                    Else
                        lblErrorMessage.Text = "Duplicate lot #s are not allowed in unique external & internal location."
                    End If

                    Exit Sub
                End If





                Dim objItems As New CItems
                objItems.ItemCode = lngItemCode
                objItems.WarehouseID = vcExternalLocation
                objItems.WareHouseItemID = CCommon.ToLong(hdnWarehouseItemID.Value)
                objItems.WarehouseLocationID = vcInternalLocationID
                objItems.WLocation = vcInternalLocationText
                objItems.WListPrice = 0
                objItems.OnHand = 0
                objItems.ReOrder = 0
                objItems.WSKU = String.Empty
                objItems.WBarCode = String.Empty
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")
                objItems.SerialNo = vcSerialLotNo
                objItems.IsCreateGlobalLocation = False
                objItems.byteMode = 1

                'RETRIVE MATRIX ITEM ATTRIBUTES VALUE
                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("Fld_ID")
                dtCusTable.Columns.Add("Fld_Value")
                dtCusTable.TableName = "CusFlds"
                Dim drCusRow As DataRow

                Dim ds1 As New DataSet
                ds1.Tables.Add(dtCusTable)
                objItems.strFieldList = ds1.GetXml

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    objItems.SaveWarehouseItems()

                    If CCommon.ToLong(hdnWarehouseItemID.Value) = 0 Then
                    ElseIf CCommon.ToBool(iSerialize) AndAlso finalListSerialLot.Count > 0 Then
                        objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), finalListSerialLot.Sum(Function(x) x.numQty), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                    ElseIf CCommon.ToBool(iSerialize) AndAlso finalListSerialLot.Count > 0 Then
                        objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), finalListSerialLot.Sum(Function(x) x.numQty), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                    End If

                    objTransactionScope.Complete()
                    lblErrorMessage.Text = "Record added successfully."
                End Using

                txtSerialLot.Text = String.Empty

                Dim tempId As Long = CCommon.ToLong(hdnWarehouseItemID.Value)

            End If
        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_SERIALLOT") Then
                If CCommon.ToBool(iSerialize) Then
                    lblErrorMessage.Text = "Duplicate serial #s are not allowed. Doen't matter if external & internal location is unique."
                Else
                    lblErrorMessage.Text = "Duplicate lot #s are not allowed in unique external & internal location."
                End If
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                lblErrorMessage.Text = CCommon.ToString(ex)
            End If
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            For Each row As DataGridItem In dgItem.Items

                Dim checkbox As CheckBox = CType(row.FindControl("chk"), CheckBox)

                If checkbox.Checked Then

                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objItem.WareHouseDetailID = row.Cells(0).Text
                    'objItem.str = CType(row.FindControl("hfSerialNo"), HiddenField).Value
                    objItem.WareHouseItemID = row.Cells(1).Text

                    If Not objItem.DeleteWarehouseDetailItem() = True Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SerializeItemValidate", "alert('You can not delete this Serialize Item.')", True)
                        Return
                    End If

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        'make journal for Removed qty
                        If iSerialize = 1 Then
                            objItem.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), -1, CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        Else
                            If CCommon.ToInteger(CType(row.FindControl("txtQty"), TextBox).Text) > 0 Then
                                objItem.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), -1 * CCommon.ToInteger(CType(row.FindControl("txtQty"), TextBox).Text), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                            End If
                        End If

                        objTransactionScope.Complete()
                    End Using
                End If
            Next row
            LoadWarehouse()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
    Private Sub dgItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItem.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim chk As CheckBox = DirectCast(e.Item.FindControl("chk"), CheckBox)
                Dim txtQty As TextBox = DirectCast(e.Item.FindControl("txtQty"), TextBox)
                Dim lblQty As Label = DirectCast(e.Item.FindControl("lblQty"), Label)

                If CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitAddedFromPO")) Then
                    txtQty.Visible = False
                    lblQty.Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region


    Private Class SerialLot
        Public Property vcSerialLot As String
        Public Property numQty As Int32
        Public Property dtExpirationDate As DateTime?
    End Class

End Class


