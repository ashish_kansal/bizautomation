﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmOrderItemImage.aspx.vb" Inherits="frmOrderItemImage" %>

<asp:Content ID="childHead" runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Image</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="childContent" runat="server" ContentPlaceHolderID="Content">
    
    
        <asp:Table ID="Table2" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
            BorderWidth="1" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table width="100%">
                        <tr>
                            <td class="normal1" align="right">
                                Select Thumbnail Image
                            </td>
                            <td>
                                <input id="txtThumbFile" type="file" name="txtThumbFile" runat="server" style="width: 200px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <asp:Image ID="Image1" runat="server"></asp:Image>
                            </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnlLargeImage">
                            <tr>
                                <td class="normal1" align="right">
                                    Select Image
                                </td>
                                <td>
                                    <input id="txtMagFile" type="file" name="txtMagFile" runat="server" style="width: 200px">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <asp:Image ID="Image" runat="server"></asp:Image>
                                </td>
                            </tr>
                        </asp:Panel>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
       
</asp:Content>
<asp:Content ID = "childTittle" ContentPlaceHolderID ="PageTitle" runat ="server">
   <asp:Label Text="Item Image" runat="server" ID="lblTitle" />
</asp:Content>
<asp:Content ID = "childFilterAndView" ContentPlaceHolderID ="FiltersAndViews1" runat ="server">
     <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td align="right">
                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                    &nbsp;
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="50">
                    </asp:Button>
                    &nbsp;
                    <asp:Button ID="btnSaveClose" runat="server" CssClass="button" OnClientClick="window.opener.location.reload(true);window.close()" Text="Save &amp; Close">
                    </asp:Button>
                    &nbsp;
                    <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.opener.location.reload(true);window.close()" Text="Close" Width="50">
                    </asp:Button>
                    &nbsp;
                </td>
            </tr>
        </table>
</asp:Content>