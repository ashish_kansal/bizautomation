﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmItemParent.aspx.vb" Inherits=".frmItemParent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function OpenItem(a) {
            if (window.opener != null) {
                window.opener.OpenItem(a);
            } else {
                alert("Parent windows is closed.");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float: right;">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" OnClientClick="return Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Child Membership
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:DataList ID="dtParentDetail" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
        <HeaderTemplate>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width: 100%">Parent</th>
                        <th style="white-space: nowrap">Child member of</th>
                        <th style="white-space: nowrap">Qty req. for parent</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <ul class="list-inline">
                        <li><a href="javascript:OpenItem(<%# DataBinder.Eval(Container, "DataItem.numItemCode")%>)">
                            <img src="../images/tag.png" align="BASELINE" style="float: none;"></a></li>
                        <li><%# Eval("vcItemName")%></li>
                    </ul>
                </td>
                <td style="text-align: center"><%# Eval("ParentItemType")%></td>
                <td style="text-align: center"><%# Eval("numQtyItemsReq")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:DataList>
</asp:Content>
