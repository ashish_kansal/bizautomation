﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShowDocument.aspx.vb" MasterPageFile="~/common/Popup.Master" Inherits=".frmShowDocument" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="headImage" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="contentTitle" runat="server" ContentPlaceHolderID="PageTitle">
    <asp:Label Text="Large Image" runat="server" ID="lblTitle" />
</asp:Content>
<asp:Content ID="contentFilterAndViews" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="float: right;">
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                    Text="Close" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contentImage" ContentPlaceHolderID="Content" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <asp:Table ID="main" class="aspTable" BorderColor="black" BorderWidth="1" CellSpacing="0"
        CellPadding="0" GridLines="None" runat="server" Style="min-height: 400px; min-width: 550px;">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell Width="100%">
                <asp:Panel ID="pnlImageData" runat="server">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnUploadImage" runat="server" CssClass="button" Text="Upload Documents and Images"
                                    UseSubmitBehavior="false"></asp:Button>
                                <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" UseSubmitBehavior="false">
                                </asp:Button>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" OnClientClick="return DeleteRecord();">
                                </asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Table ID="table4" runat="server" BorderWidth="1" Width="550px" Height="300px"
                                    BackColor="white" CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None"
                                    CssClass="aspTable">
                                    <asp:TableRow>
                                      <asp:TableCell >
                                          <asp:Label ID ="lblTest" Text ="test" runat ="server" ></asp:Label>
                                      </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button ID="btnRefresh" runat="server" CssClass="button" Style="display: none;"
                    UseSubmitBehavior="false" Text="Refresh" Width="50"></asp:Button>
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <asp:HiddenField ID="hdnItemCode" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
