<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmItemImage.aspx.vb" Inherits="frmItemImage" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="headImage" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/plUpload.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../JavaScript/PlUpload/plupload.js"></script>
    <script type="text/javascript" src="../JavaScript/PlUpload/plupload.silverlight.js"></script>
    <script type="text/javascript" src="../JavaScript/PlUpload/plupload.flash.js"></script>
    <script type="text/javascript" src="../JavaScript/PlUpload/plupload.html4.js"></script>
    <script type="text/javascript" src="../JavaScript/PlUpload/plupload.html5.js"></script>
    <script type="text/javascript" src="../JavaScript/PlUpload/jquery.plupload.queue.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.ae.image.resize.min.js"></script>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close()
            return false;
        }

        $(document).ready(function () {
            InitializeValidation();
        });

        function DeleteRecord() {
            if ($('.cbRowItem :checkbox:checked').length > 0) {
                if (confirm('Are you sure, you want to delete the selected record?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert("Select atleast one record to delete.");
                return false;
            }
        }

        function SelectSingleRadiobutton(rdBtnID) {
            $("#gvImageData").find("INPUT[type='radio']").prop('checked', false);
            $("#" + rdBtnID).prop("checked", true);
        }



        $(document).ready(function () {

            var chkBox = $(".cbHeader input"); //$("input[id$='chkSelectAll']");
            chkBox.click(
          function () {
              $(".cbRowItem input").prop('checked', chkBox.is(':checked'));
          });
            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $(".RadGrid_Default INPUT[type='checkbox']").click(
        function (e) {
            if (!$(this)[0].checked) {
                chkBox.prop("checked", false);
            }
        });
        });


        $(function () {
            var Height = "";
            var Width = "";
            var ItemCode = "";

            Height = Number($("#txtHeight").val());
            Width = Number($("#txtWidth").val());
            ItemCode = Number($("#hdnItemCode").val());

            $("#uploader").pluploadQueue({

                runtimes: 'html5,flash',
                url: '../common/ImageUploadResize.ashx',
                max_file_size: '300mb',
                chunk_size: '300mb',
                unique_names: true,
                headers: { "Height": Height, "Width": Width },
                // Resize images on clientside if we can
                resize: { width: 1000, height: 1000, quality: 90 },


                // Specify what files to browse for
                filters: [
	            { title: "Image files", extensions: "jpg,gif,png,txt,pdf,docx,xlsx,ppt" },
	            { title: "Zip files", extensions: "zip" }
                ],

                // Flash settings
                flash_swf_url: '../JavaScript/PlUpload/plupload.flash.swf',

                // Silverlight settings
                silverlight_xap_url: '../JavaScript/PlUpload/plupload.silverlight.xap',

                preinit:
                   {


                       UploadFile: function (up, file) {
                           Height = Number($("#txtHeight").val());
                           Width = Number($("#txtWidth").val());
                           ItemCode = Number($("#hdnItemCode").val());
                           if ($("#rbMeasureWidth").is(":checked")) {
                               if (Width > 0)
                                   up.settings.url = '../common/ImageUploadResize.ashx?measure=width=' + Width + '&ItemCode=' + ItemCode;
                           } else if (Height > 0) {
                               up.settings.url = '../common/ImageUploadResize.ashx?measure=height=' + Height + '&ItemCode=' + ItemCode;
                           }
                           //console.log(up.settings.url);

                       },
                       UploadComplete: function (up, file) {
                           $("#btnRefresh").click();
                       }
                   }

            });
            $('.plupload_header_title').text("Select item documents and images");
            $('plupload_header_text').text('Add item documents and images to the upload queue and click the start button.');
            $('#uploader_browse').text('Add Files');

            windowResize();
            $('#clear').click(function (e) {
                e.preventDefault();
                $("#uploader").pluploadQueue().splice();
                $(".plupload_buttons").css("display", "block");
                $(".plupload_upload_status").css("display", "none");
                $(".plupload_button_clear").addClass("plupload_clear_disabled");
            });
        });


        $(function () {
            $(".resizeme").aeImageResize({ height: 50, width: 50 });
        });

    </script>
</asp:Content>
<asp:Content ID="contentTitle" runat="server" ContentPlaceHolderID="PageTitle">
    <asp:Label Text="Item Documents and Images" runat="server" ID="lblTitle" />
</asp:Content>
<asp:Content ID="contentFilterAndViews" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="float: right;">
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.opener.location.reload(true);window.close()"
                    Text="Close" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="contentImage" ContentPlaceHolderID="Content" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <asp:Table ID="main" class="aspTable" BorderColor="black" BorderWidth="1" CellSpacing="0"
        CellPadding="0" GridLines="None" runat="server" Style="min-height: 400px; min-width: 550px;">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell Width="100%">
                <asp:Panel ID="pnlImageUpload" runat="server">
                    <div id="uploader" style="width: 540px;">
                        <p>
                            You browser doesn't have Flash or HTML5 support.
                        </p>
                    </div>

                    <fieldset>
                        <legend>&nbsp; <b>Resize Image</b>
                            <asp:Label ID="lblHelp" runat="server" ToolTip="Bizautomation recommends resizing high resolution image to smaller size thumbnails in order to create fast loading of product images on your web store for your customer." class="tip">[?]</asp:Label>
                            &nbsp;</legend>
                        <table cellspacing="0" cellpadding="0" border="0" width="0">
                            <tr>
                                <td align="left">
                                    <asp:RadioButton ID="rbMeasureWidth" CssClass="signup" Checked="true" Text="Set Width To" GroupName="measure"
                                        runat="server" />
                                    &nbsp;
                                </td>
                                <td align="center">:
                                </td>
                                <td align="left">&nbsp;
                                    <asp:TextBox ID="txtWidth" Width="30px" CssClass="required_integer {required:'#rbMeasureWidth:checked' ,number:true, messages:{required:'Please provide value!',number:'provide valid value!'}} signup"
                                        runat="server" Text="200"></asp:TextBox>
                                    &nbsp; Pixels , preserve Width/Height ratio .
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:RadioButton ID="rbMeasureHeight" CssClass="signup" Text=" Set Height To "
                                        GroupName="measure" runat="server" />
                                    &nbsp;
                                </td>
                                <td align="center">:
                                </td>
                                <td align="left">&nbsp;
                                    <asp:TextBox ID="txtHeight" CssClass="required_integer {required:'#rbMeasureHeight:checked' ,number:true, messages:{required:'Please provide value!',number:'provide valid value!'}} signup"
                                        Width="30px" runat="server" Text="200"></asp:TextBox>
                                    &nbsp; Pixels , preserve Width/Height ratio .
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <br />
                                </td>
                                <td>
                                    <br />
                                </td>
                                <td>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlImageData" runat="server">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnUploadImage" runat="server" CssClass="button" Text="Upload Documents and Images"
                                    UseSubmitBehavior="false"></asp:Button>
                                <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" UseSubmitBehavior="false"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" OnClientClick="return DeleteRecord();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Table ID="table4" runat="server" BorderWidth="1" Width="550px" Height="300px"
                                    BackColor="white" CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None"
                                    CssClass="aspTable">
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Top">
                                            <asp:GridView ID="gvImageData" AutoGenerateColumns="false" runat="server" Width="100%"
                                                CssClass="dg" DataKeyNames="numItemImageId">
                                                <AlternatingRowStyle CssClass="ais" />
                                                <RowStyle CssClass="is" />
                                                <HeaderStyle CssClass="hs" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemStyle Width="5%" />
                                                        <ItemTemplate>
                                                            <asp:Literal ID="ltrlImage" runat="server" Text='<%#IIf(Eval("bitIsImage") = "1", Eval("vcPathForTImage"), Eval("vcPathForImage"))%>'></asp:Literal>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText=" Document ">

                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlShowDocument" Target="_blank" Text='<%#Eval("vcPathForImage")%>' runat="server">
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Default Image">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                                        <ItemTemplate>
                                                            <asp:RadioButton ID="rbDefault" Checked='<%#Eval("bitDefault")%>' OnClick="javascript:SelectSingleRadiobutton(this.id)"
                                                                runat="server" ClientIDMode="Predictable" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sort Order">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtDisplayOrder" Width="30px" runat="server" CssClass="required_integer {required:true ,number:true, messages:{required:'Please provide value!',number:'provide valid value!'}} signup"
                                                                Text='<%#Eval("intDisplayOrder") %>'>
                                                            </asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="cbSelectAll" runat="server" CssClass="cbHeader" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNumItemImageId" Text='<%#Eval("numItemImageId")%>' runat="server"
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblVcPathForTImage" Text='<%#Eval("vcPathForTImage")%>' runat="server"
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblVcPathForImage" Text='<%#Eval("vcPathForImage")%>' runat="server"
                                                                Visible="false">
                                                            </asp:Label>
                                                            <asp:CheckBox ID="cbRow" runat="server" CssClass="cbRowItem" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <div class="signup">
                                                        No Records Found...
                                                    </div>
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button ID="btnRefresh" runat="server" CssClass="button" Style="display: none;"
                    UseSubmitBehavior="false" Text="Refresh" Width="50"></asp:Button>
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <asp:HiddenField ID="hdnItemCode" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
