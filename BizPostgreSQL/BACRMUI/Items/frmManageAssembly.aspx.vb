Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities

Partial Public Class frmManageAssembly
    Inherits BACRMPage
    Dim lngItemCode As Long
    Dim lngWItemID As Long
    Dim lngQty As Long
    Dim objItems As CItems
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim strModuleName, strPermissionName As String
            Dim m_aryRightsAddWorkOrder() As Integer = GetUserRightsForPage_Other(16, 152, strModuleName, strPermissionName)

            If m_aryRightsAddWorkOrder(RIGHTSTYPE.ADD) = 0 Then
                Response.Redirect("../admin/authenticationpopup.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                Exit Sub
            End If

            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
            lngWItemID = CCommon.ToLong(GetQueryStringVal("WItemID"))
            lngQty = CCommon.ToLong(GetQueryStringVal("Qty"))

            If Not IsPostBack Then
                BindBusinessProcess()

                objCommon.sb_FillConEmpFromDBSel(ddlWOAssignTo, Session("DomainID"), 0, 0)

                calenderStartDate.SelectedDate = DateTime.Now.Date

                If Not Session("DFItemsToPurchase") Is Nothing Then
                    For Each objItem In DirectCast(Session("DFItemsToPurchase"), System.Collections.Generic.List(Of BACRM.BusinessLogic.Opportunities.OpportunityItemsReleaseDates))
                        AddItem(objItem.ItemCode, 0, objItem.ItemCode, objItem.ItemName, 0, "", objItem.WarehouseItemID, objItem.Warehouse, objItem.Quantity, DateTime.UtcNow.AddMinutes(-1 * Convert.ToInt32(Session("ClientMachineUTCTimeOffset"))), DateTime.UtcNow.AddMinutes(-1 * Convert.ToInt32(Session("ClientMachineUTCTimeOffset"))).AddDays(1), objItem.BuildManagerID, objItem.BusinessProcess, objItem.BuildManager, objItem.BusinessProcessID, 0, "", objItem.AssetAccountID, objItem.AverageCost)
                    Next

                    BindGrid()

                    Session("DFItemsToPurchase") = Nothing
                End If
            End If

            If CCommon.ToBool(CCommon.ToLong(GetQueryStringVal("ReloadParent"))) Then
                btnClose.Attributes.Add("onclick", "javascript:opener.location.reload(true);window.close();return false;")
            Else
                btnClose.Attributes.Add("onclick", "return Close()")
            End If

            btnAddMore.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub PerformValidation()
        Try

            'Validate if Asset Account is set
            If objItems Is Nothing Then objItems = New CItems
            If Not objItems.ValidateItemAccount(lngItemCode, Session("DomainID"), 1) = True Then
                btnSave.Visible = False
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                Exit Sub
            End If


            If ChartOfAccounting.GetDefaultAccount("OE", Session("DomainID")) = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SerializeItemValidate", "alert('Please Map Default Opening Balance Equity Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.')", True)
                btnSave.Visible = False

                Exit Sub
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub bindItemsGrid(Optional ByVal lngEditItemCode As Long = 0)
        Try
            Dim objItems As New CItems
            If lngEditItemCode = 0 Then
                objItems.ItemCode = radCmbSearch.SelectedValue
            Else
                objItems.ItemCode = lngEditItemCode
            End If
            objItems.WareHouseItemID = CCommon.ToLong(ddlAssWareHouse.SelectedValue)
            Dim dtChildItems As DataTable = objItems.GetChildItemsForKitsAss
            If dtChildItems.Rows.Count = 0 Then
                btnSave.Visible = False
                litMessage.Text = "No child items exists for creating Assembly / Work order"
            Else

                rtlChildItems.DataSource = dtChildItems
                rtlChildItems.DataBind()
                rtlChildItems.ExpandAllItems()
            End If
            dtChildItems.Columns.Add("MaximumPossibleUnits", Type.GetType("System.Decimal"))

            Dim drArray As DataRow() = dtChildItems.Select("numItemKitID=0 AND numWarehouseItmsID > 0")

            If drArray.Length > 0 Then
                For Each row As DataRow In drArray
                    row.Item("MaximumPossibleUnits") = Math.Floor(row.Item("numOnHand") / IIf(row.Item("numUOMQuantity") = 0, 1, row.Item("numUOMQuantity")))
                Next

                Dim dt = drArray.CopyToDataTable()

                lblMaxQty.Text = CCommon.ToString(dt.Compute("min(MaximumPossibleUnits)", ""))
            Else
                lblMaxQty.Text = 0
            End If

        Catch ex As Exception
            If ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                btnSave.Visible = False
                divChildItems.Visible = False
                litMessage.Text = "Selected warehouse is not available in all inventory child items."
            Else
                Throw
            End If
        End Try
        'Try
        '    Dim objItems As New CItems
        '    If lngEditItemCode = 0 Then
        '        objItems.ItemCode = radCmbSearch.SelectedValue
        '    Else
        '        objItems.ItemCode = hdnEditItemCode.Value
        '    End If
        '    objItems.WareHouseItemID = CCommon.ToLong(ddlAssWareHouse.SelectedValue)

        '    RadGrid1.MasterTableView.FilterExpression = "numItemKitID = 0"

        '    Dim dtTable As DataTable = objItems.GetChildItemsForKitsAss

        '    If dtTable.Rows.Count = 0 Then
        '        btnSave.Visible = False
        '        litMessage.Text = "No child items exists for creating Assembly / Work order"
        '    Else
        '        RadGrid1.DataSource = dtTable
        '        RadGrid1.DataBind()

        '        dtTable.Columns.Add("MaximumPossibleUnits", Type.GetType("System.Decimal"))

        '        Dim drArray As DataRow() = dtTable.Select("numItemKitID=0 AND numWarehouseItmsID > 0")

        '        If drArray.Length > 0 Then
        '            For Each row As DataRow In drArray
        '                row.Item("MaximumPossibleUnits") = Math.Floor(row.Item("numOnHand") / IIf(row.Item("numUOMQuantity") = 0, 1, row.Item("numUOMQuantity")))
        '            Next

        '            Dim dt = drArray.CopyToDataTable()

        '            lblMaxQty.Text = CCommon.ToString(dt.Compute("min(MaximumPossibleUnits)", ""))
        '        Else
        '            lblMaxQty.Text = 0
        '        End If

        '        HideExpandColumnRecursive(RadGrid1.MasterTableView)
        '    End If
        'Catch ex As Exception
        '    If ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
        '        btnSave.Visible = False
        '        trChildItems.Visible = False
        '        litMessage.Text = "Selected warehouse is not available in all inventory child items."
        '    Else
        '        Throw
        '    End If
        'End Try
    End Sub

    Public Sub HideExpandColumnRecursive(ByVal tableView As GridTableView)
        Dim nestedViewItems As GridItem() = tableView.GetItems(GridItemType.NestedView)
        For Each nestedViewItem As GridNestedViewItem In nestedViewItems
            For Each nestedView As GridTableView In nestedViewItem.NestedTableViews
                nestedView.Style("border") = "0"

                Dim MyExpandCollapseButton As Button = DirectCast(nestedView.ParentItem.FindControl("MyExpandCollapseButton"), Button)
                If nestedView.Items.Count = 0 Then
                    If Not MyExpandCollapseButton Is Nothing Then
                        MyExpandCollapseButton.Style("visibility") = "hidden"
                    End If
                    nestedViewItem.Visible = False
                Else
                    If Not MyExpandCollapseButton Is Nothing Then
                        MyExpandCollapseButton.Style.Remove("visibility")
                    End If
                End If

                If nestedView.HasDetailTables Then
                    HideExpandColumnRecursive(nestedView)
                End If
            Next
        Next
    End Sub

    Public Sub CreateExpandCollapseButton(ByVal item As GridItem, ByVal columnUniqueName As String)
        If TypeOf item Is GridDataItem Then
            If item.FindControl("MyExpandCollapseButton") Is Nothing Then
                Dim button As New Button()
                AddHandler button.Click, AddressOf button_Click_Collapse_Expand
                button.CommandName = "ExpandCollapse"
                button.CssClass = IIf((item.Expanded), "rgCollapse", "rgExpand")
                button.ID = "MyExpandCollapseButton"

                If item.OwnerTableView.HierarchyLoadMode = GridChildLoadMode.Client Then
                    Dim script As String = [String].Format("$find(""{0}"")._toggleExpand(this, event); return false;", item.Parent.Parent.ClientID)

                    button.OnClientClick = script
                End If


                Dim level As Integer = item.ItemIndexHierarchical.Split(":"c).Length - 1
                item.Cells(5).Style("padding-left") = (level * 10) & "px"

                'Dim cell As TableCell = item.Cells(4)
                Dim cell As TableCell = DirectCast(item, GridDataItem)(columnUniqueName)
                cell.Controls.Add(button)
                cell.Controls.Add(New LiteralControl("&nbsp;"))

                'cell.Controls.Add(New LiteralControl(DataBinder.Eval(item.DataItem, columnUniqueName).ToString()))
                cell.Controls.Add(New LiteralControl((DirectCast(item, GridDataItem)).GetDataKeyValue(columnUniqueName).ToString()))

                cell.Style("padding-left") = (level * 10) & "px"
            End If
        End If
    End Sub

    Sub button_Click_Collapse_Expand(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, Button).CssClass = IIf((CType(sender, Button).CssClass = "rgExpand"), "rgCollapse", "rgExpand")

    End Sub


    Private Sub btnBuildStock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildStock.Click
        For Each row As GridViewRow In grdAddedItems.Rows
            bindItemsGrid(CType(row.FindControl("hdnNumItemCode"), HiddenField).Value)
            For Each dataGridItems As TreeListDataItem In rtlChildItems.Items
                If CType(dataGridItems.FindControl("lblItemType"), Label).Text = "P" AndAlso
                   CCommon.ToLong(dataGridItems("numItemKitID").Text) = 0 Then
                    Dim numUnits As Double = CCommon.ToDouble(CType(dataGridItems.FindControl("lblUOMQuantity"), Label).Text * txtUnits.Text)
                    Dim numOnHand As Double = CCommon.ToDouble(CType(dataGridItems.FindControl("lblOnHand"), Label).Text)

                    If numUnits > numOnHand Then
                        litMessage.Text = "Some items do not have OnHand qty."
                        Exit Sub
                    End If
                End If
            Next
        Next

        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
            'Make as entry in assembly item table to keep the track of average cost of child items which are used in building assembly.
            ' It is required to keep this track because when some one try to diassemble item we have to update average cost of child item
            For Each row As GridViewRow In grdAddedItems.Rows
                Dim objAssembledItem As New AssembledItem
                objAssembledItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objAssembledItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objAssembledItem.ItemCode = CType(row.FindControl("hdnNumItemCode"), HiddenField).Value
                objAssembledItem.WarehouseItemID = CType(row.FindControl("hdnWareHouseId"), HiddenField).Value
                objAssembledItem.AssembledQty = CCommon.ToLong(CType(row.FindControl("lblUnits"), Label).Text)
                Dim lngAssembledItemID As Long = objAssembledItem.Save()
                bindItemsGrid(CType(row.FindControl("hdnNumItemCode"), HiddenField).Value)
                For Each dataGridItems As TreeListDataItem In rtlChildItems.Items
                    If CType(dataGridItems.FindControl("lblItemType"), Label).Text = "P" AndAlso
                       CCommon.ToLong(dataGridItems("numItemKitID").Text) = 0 Then
                        objItems = New CItems()
                        objItems.byteMode = 1
                        objItems.WareHouseItemID = CType(dataGridItems.FindControl("lblWareHouseItemId"), Label).Text
                        objItems.NoofUnits = CType(dataGridItems.FindControl("lblUOMQuantity"), Label).Text * txtUnits.Text
                        objItems.QtyRequiredForSingleBuild = CType(dataGridItems.FindControl("lblUOMQuantity"), Label).Text
                        objItems.UserCntID = Session("UserContactID")
                        objItems.AssembledItemID = lngAssembledItemID
                        objItems.UpdateInventory()
                    End If
                Next
                objItems = New CItems()
                'First make journal entry and update new average cost of assembly
                objItems.MakeAssemblyItemQtyAdjustmentJournal(CType(row.FindControl("hdnNumItemCode"), HiddenField).Value, CCommon.ToString(CType(row.FindControl("lblItemCode"), Label).Text), CCommon.ToLong(CType(row.FindControl("lblUnits"), Label).Text), String.Format("{0:###0.00}", CCommon.ToDecimal(CType(row.FindControl("hfAverageCost"), HiddenField).Value)), CCommon.ToLong(CType(row.FindControl("hfAssetChartAcntID"), HiddenField).Value), Session("UserContactID"), Session("DomainID"), CCommon.ToLong(CType(row.FindControl("hdnWareHouseId"), HiddenField).Value))

                objItems.byteMode = 0
                objItems.WareHouseItemID = CType(row.FindControl("hdnWareHouseId"), HiddenField).Value
                objItems.NoofUnits = CCommon.ToLong(CType(row.FindControl("lblUnits"), Label).Text)
                objItems.UserCntID = Session("UserContactID")
                objItems.AssembledItemID = lngAssembledItemID
                objItems.UpdateInventory()

            Next

            objTransactionScope.Complete()
        End Using
        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Work Order created Successfully.');javascript:opener.location.reload(true);window.close();return false;", True)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Assembly item created.');window.opener.location.reload(true);window.close();", True)

        Return
        litMessage.Text = "Assembly item created."
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objItems As New CItems
            'For Each dataGridItems As DataGridItem In dgChildItems.Items
            '    If CType(dataGridItems.FindControl("lblItemType"), Label).Text = "P" Then
            '        objItems.byteMode = 1
            '        objItems.WareHouseItemID = CType(dataGridItems.FindControl("ddlWarehouse"), DropDownList).SelectedValue
            '        objItems.NoofUnits = dataGridItems.Cells(2).Text * txtUnits.Text
            '        objItems.UpdateInventory()
            '    End If
            'Next
            'Validate if Default Work in Progress Acount is set or not
            If ChartOfAccounting.GetDefaultAccount("WP", Session("DomainID")) = 0 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "Validation", "alert('Can not save record, your option is to set default Work in Progress Account from Administration->Global Settings->Accounting tab->Default Accounts');", True)
                Exit Sub
            End If
            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                For Each row As GridViewRow In grdAddedItems.Rows
                    objItems = New CItems()
                    objItems.WareHouseItemID = CType(row.FindControl("hdnWareHouseId"), HiddenField).Value
                    objItems.NoofUnits = CType(row.FindControl("lblUnits"), Label).Text
                    objItems.ItemCode = CType(row.FindControl("hdnNumItemCode"), HiddenField).Value
                    objItems.Instruction = CType(row.FindControl("hdnComments"), HiddenField).Value
                    objItems.CompliationDate = CCommon.ToSqlDate(System.DateTime.Now)
                    objItems.AssignTo = CType(row.FindControl("hdnJobManager"), HiddenField).Value
                    objItems.BusinessProcessId = CType(row.FindControl("hdnBusinessProcessId"), HiddenField).Value

                    Dim endDate As DateTime = Convert.ToDateTime(CType(row.FindControl("rdpFinishDate"), BACRM.Include.calandar).SelectedDate)
                    Dim startDate As DateTime = Convert.ToDateTime(CType(row.FindControl("rdpStartDate"), BACRM.Include.calandar).SelectedDate)


                    objItems.EndDate = New DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    objItems.StartDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    objItems.ProjectFinishDate = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    objItems.SalesOrderId = CType(row.FindControl("hdnSOId"), HiddenField).Value
                    'objItems.EndDate = CCommon.ToSqlDate(System.DateTime.Now)
                    'objItems.StartDate = CCommon.ToSqlDate(System.DateTime.Now)
                    'objItems.ProjectFinishDate = CCommon.ToSqlDate(System.DateTime.Now)
                    objItems.UserCntID = Session("UserContactID")
                    objItems.DomainID = Session("DomainID")
                    objItems.ManageWorkOrder()
                    'Following is the entry that needs to be created when the work order is created.
                    '(All of these are assets accounts.)

                    'Inventory for Item 1 Credit                    20
                    'Inventory for Item 2 Credit                    100
                    'Work In Progress    Debit                      120
                    objItems.MakeAssemblyWorkOrderJournal(CType(row.FindControl("hdnNumItemCode"), HiddenField).Value, CType(row.FindControl("lblItemCode"), Label).Text, CCommon.ToInteger(CType(row.FindControl("lblUnits"), Label).Text), CCommon.ToDecimal(CType(row.FindControl("hfAverageCost"), HiddenField).Value), CCommon.ToDecimal(CType(row.FindControl("hfAssetChartAcntID"), HiddenField).Value), Session("UserContactID"), Session("DomainID"), CCommon.ToLong(CType(row.FindControl("hdnWareHouseId"), HiddenField).Value))

                Next

                objTransactionScope.Complete()
            End Using

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "CloseAndRedirect();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As RadComboBoxItemsRequestedEventArgs)
        Try
            If e.Text.Trim().Length > 0 Then
                Dim searchCriteria As String
                Dim itemOffset As Integer = e.NumberOfItems

                Dim ds As DataSet = GetItems(True, e.Text.Trim().Replace("%", "[%]"))

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Dim endOffset As Integer

                    endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, ds.Tables(0).Rows.Count)
                    e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                    e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))

                    For Each dataRow As DataRow In ds.Tables(0).Rows
                        Dim item As New RadComboBoxItem()
                        item.Text = DirectCast(dataRow("vcItemName"), String)
                        item.Value = dataRow("numItemCode").ToString()
                        item.Attributes.Add("vcModelID", CCommon.ToString(dataRow("vcModelID")))

                        radCmbSearch.Items.Add(item)

                        item.DataBind()
                    Next
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
        End Try
    End Sub

    Protected Sub radCmbOrder_ItemsRequested(sender As Object, e As RadComboBoxItemsRequestedEventArgs)
        Try
            If e.Text.Trim().Length > 0 Then
                Dim searchCriteria As String
                Dim itemOffset As Integer = e.NumberOfItems

                Dim ds As DataSet = GetOpenSalesOrders(True, e.Text.Trim().Replace("%", "[%]"))

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Dim endOffset As Integer

                    endOffset = Math.Min(itemOffset + radCmbOrder_Search.ItemsPerRequest, ds.Tables(0).Rows.Count)
                    e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                    e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Orders <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))

                    For Each dataRow As DataRow In ds.Tables(0).Rows
                        Dim item As New RadComboBoxItem()
                        item.Text = DirectCast(dataRow("vcPOppName"), String)
                        item.Value = dataRow("numOppId").ToString()
                        item.Attributes.Add("vcCompanyName", CCommon.ToString(dataRow("vcCompanyName")))

                        radCmbOrder_Search.Items.Add(item)

                        item.DataBind()
                    Next
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
        End Try
    End Sub
    Private Function GetItems(ByVal isFromSearch As Boolean, ByVal searchText As String) As DataSet
        Try

            Dim objItems As New CItems
            Dim strCondition As String = ""

            With objItems
                .ItemClassification = 0 'CCommon.ToLong(ddlFilter.SelectedValue) 'ddlFilter.SelectedItem.Value
                .CurrentPage = -1
                .DomainID = Session("DomainID")
                .PageSize = IIf(isFromSearch, -1, Convert.ToInt32(Session("PagingRows")))
                .TotalRecords = 0
                .columnName = "vcItemName"
                .SortCharacter = "0"
                .columnSortOrder = "Asc"
                .ItemType = "P"
                .Assembly = True

                .ItemGroupID = CCommon.ToLong(GetQueryStringVal("ItemGroup"))
                .UserCntID = Session("UserContactID")
                .SearchText = searchText

                'Create filter conditions as per selection within rad dropdowns
                Dim strMain As String() = {""}
                Dim strName As String = ""
                Dim strValue As String = ""


                objItems.strCondition = strCondition
                objItems.byteMode = 0
                objItems.bitMultiSelect = False
            End With
            Dim ds As DataSet = objItems.GetItemOrKitList


            Return ds
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GetOpenSalesOrders(ByVal isFromSearch As Boolean, ByVal searchText As String) As DataSet
        Try

            Dim objOpportunity As New COpportunities
            Dim strCondition As String = ""

            With objOpportunity
                .CurrentPage = -1
                .DomainID = Session("DomainID")
                .PageSize = IIf(isFromSearch, -1, Session("PagingRows"))
                .TotalRecords = 0
                .columnName = "vcPOppName"
                .SortCharacter = "0"
                .columnSortOrder = "Asc"
                .Shipped = 0
                .intType = 0
                .UserCntID = Session("UserContactID")
                .RegularSearchCriteria = "Opp.vcPoppName ilike '%" & searchText & "%' "

                'Create filter conditions as per selection within rad dropdowns
                Dim strMain As String() = {""}
                Dim strName As String = ""
                Dim strValue As String = ""

            End With
            Dim ds As DataSet = objOpportunity.GetDealsList1()


            Return ds
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub BindBusinessProcess()
        Try
            Dim objAdmin = New CAdmin()
            objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
            objAdmin.SalesProsID = 0
            objAdmin.Mode = 3
            ddlBusinessProcess.Items.Clear()
            Dim dtConfiguration = objAdmin.LoadProcessList()
            If Not dtConfiguration Is Nothing AndAlso dtConfiguration.Rows.Count > 0 Then
                ddlBusinessProcess.DataSource = dtConfiguration
                ddlBusinessProcess.DataTextField = "Slp_Name"
                ddlBusinessProcess.DataValueField = "Slp_Id"
                ddlBusinessProcess.DataBind()
            End If
            ddlBusinessProcess.Items.Insert(0, New ListItem("-Select-", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ddlAssWareHouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssWareHouse.SelectedIndexChanged
        Try
            bindItemsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub radCmbSearch_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        If radCmbSearch.SelectedValue.Length > 0 Then
            lngItemCode = radCmbSearch.SelectedValue
            ddlAssWareHouse.Items.Clear()
            ddlAssWareHouse.DataSource = objCommon.LoadWarehouseBasedOnItemsWithAttr(lngItemCode)
            ddlAssWareHouse.DataTextField = "vcWareHouse"
            ddlAssWareHouse.DataValueField = "numWareHouseItemId"
            ddlAssWareHouse.DataBind()

            lngWItemID = 0
            ddlAssWareHouse.ClearSelection()

            If ddlAssWareHouse.Items.Count > 0 Then
                If ddlAssWareHouse.Items.FindByValue(lngWItemID) IsNot Nothing Then
                    ddlAssWareHouse.ClearSelection()
                    ddlAssWareHouse.Items.FindByValue(lngWItemID).Selected = True
                End If

                bindItemsGrid()
            Else
                divChildItems.Visible = False
                trNoWarehouses.Visible = True
            End If
            txtUnits.Text = lngQty

            Dim dtItemDetails As DataTable
            If objItems Is Nothing Then objItems = New CItems
            objItems.ItemCode = radCmbSearch.SelectedValue
            objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
            dtItemDetails = objItems.ItemDetails()
            If dtItemDetails.Rows.Count > 0 Then
                hfAverageCost.Value = String.Format("{0:###0.00}", dtItemDetails.Rows(0).Item("monAverageCost"))
                hfItemName.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("vcItemName"))
                hfAssetChartAcntID.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("numAssetChartAcntId"))

                If ddlBusinessProcess.Items.FindByValue(CCommon.ToLong(dtItemDetails.Rows(0).Item("numBusinessProcessId"))) IsNot Nothing Then
                    ddlBusinessProcess.ClearSelection()
                    ddlBusinessProcess.Items.FindByValue(CCommon.ToLong(dtItemDetails.Rows(0).Item("numBusinessProcessId"))).Selected = True
                End If


            End If

            Dim objItem As New CItems
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            objItem.ItemCode = CCommon.ToLong(radCmbSearch.SelectedValue)
            objItem.BuildProcessId = CCommon.ToLong(ddlBusinessProcess.SelectedValue)
            Dim dtStartDate As DateTime = Convert.ToDateTime(calenderStartDate.SelectedDate)
            objItem.StartDate = New DateTime(dtStartDate.Year, dtStartDate.Month, dtStartDate.Day, 0, 0, 0).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
            objItem.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objItem.Quantity = 1
            Dim dt As DataTable = objItem.GetAssemblyMfgTimeCost()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                hdnMaterialCost.Value = CCommon.ToDouble(dt.Rows(0)("monMaterialCost"))
                hdnOverheadCost.Value = CCommon.ToDouble(dt.Rows(0)("monMFGOverhead"))
                hdnLabourCost.Value = CCommon.ToDouble(dt.Rows(0)("monLaborCost"))
                hdnProjectedFinishDate.Value = Convert.ToDateTime(dt.Rows(0)("dtProjectedFinishDate"))


                lblProjectFinishDate.Text = Convert.ToDateTime(dt.Rows(0)("dtProjectedFinishDate")).ToString(CCommon.GetValidationDateFormat())
                lblMaterialCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monMaterialCost")))
                lblOverheadCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monMFGOverhead")))
                lblLabourCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monLaborCost")))
                lblUnitTotalCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monMaterialCost")) + CCommon.ToDouble(dt.Rows(0)("monMFGOverhead")) + CCommon.ToDouble(dt.Rows(0)("monLaborCost"))) & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), (CCommon.ToDouble(dt.Rows(0)("monMaterialCost")) + CCommon.ToDouble(dt.Rows(0)("monMFGOverhead")) + CCommon.ToDouble(dt.Rows(0)("monLaborCost"))) * CCommon.ToDouble(txtUnits.Text))
            End If

            PerformValidation()
        Else
            ClearControls(False, True)
            divItemDetail.Style.Remove("display")
        End If
    End Sub

    Public Sub BindGrid()

        Dim dtSavedTable As DataTable
        dtSavedTable = DirectCast(ViewState("ItemsToSave"), DataTable)
        grdAddedItems.DataSource = dtSavedTable
        grdAddedItems.DataBind()
    End Sub

    Protected Sub btnAddMore_Click(sender As Object, e As EventArgs) Handles btnAddMore.Click
        Try
            Dim lngSelectedItemCode As Long
            Dim lngSalesOrderId As Long
            Dim vcItemName As String
            Dim vcOrderName As String

            If CCommon.ToInteger(hdnEditItemCode.Value) > 0 Then
                lngSelectedItemCode = CCommon.ToInteger(hdnEditItemCode.Value)
            Else
                lngSelectedItemCode = CCommon.ToInteger(radCmbSearch.SelectedValue)
            End If

            If CCommon.ToInteger(radCmbOrder_Search.SelectedIndex) > 0 Then
                lngSalesOrderId = CCommon.ToLong(radCmbOrder_Search.SelectedValue)
            Else
                lngSalesOrderId = CCommon.ToLong(hdnEditSoid.Value)
            End If

            AddItem(lngSelectedItemCode, _
                    lngSalesOrderId, _
                    lngSelectedItemCode, _
                    CCommon.ToString(radCmbSearch.Text), _
                    lngSalesOrderId, _
                    CCommon.ToString(radCmbOrder_Search.Text), _
                    CCommon.ToLong(ddlAssWareHouse.SelectedValue), _
                    CCommon.ToString(ddlAssWareHouse.SelectedItem.Text), _
                    CCommon.ToDouble(txtUnits.Text), _
                    calenderStartDate.SelectedDate, _
                    calenderFinishDate.SelectedDate, _
                    CCommon.ToLong(ddlWOAssignTo.SelectedValue), _
                    CCommon.ToString(ddlBusinessProcess.SelectedItem.Text), _
                    CCommon.ToString(ddlWOAssignTo.SelectedItem.Text), _
                    CCommon.ToLong(ddlBusinessProcess.SelectedValue), _
                    CCommon.ToDouble(lblMaxQty.Text), _
                    CCommon.ToString(txtComments.Text), _
                    CCommon.ToLong(hfAssetChartAcntID.Value), _
                    CCommon.ToDouble(hfAverageCost.Value))
            BindGrid()
            ClearControls(True, True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub AddItem(ByVal lngSelectedItemCode As Long, _
                        ByVal lngSalesOrderId As Long, _
                        ByVal numItemCode As Long, _
                        ByVal vcItemName As String, _
                        ByVal numSalesOrderId As Long, _
                        ByVal vcSalesOrderName As String, _
                        ByVal numWareHouseId As Long, _
                        ByVal vcWarehouseName As String, _
                        ByVal numUnits As Double, _
                        ByVal dtmStartDate As DateTime, _
                        ByVal dtmFinishDate As DateTime, _
                        ByVal numAssigneTo As Long, _
                        ByVal vcBusinessProcessName As String, _
                        ByVal vcAssignToName As String, _
                        ByVal numBusinessProcessId As Long, _
                        ByVal numMaxBuildQty As Double, _
                        ByVal vcComments As String, _
                        ByVal numAssetChartAcntID As Long, _
                        ByVal numAverageCost As Double)
        Try
            If ViewState("ItemsToSave") Is Nothing Then
                Dim dt As New DataTable
                dt.Columns.Add("numItemCode", System.Type.GetType("System.Int64"))
                dt.Columns.Add("vcItemName", System.Type.GetType("System.String"))
                dt.Columns.Add("numWareHouseId", System.Type.GetType("System.Int64"))
                dt.Columns.Add("vcWarehouseName", System.Type.GetType("System.String"))
                dt.Columns.Add("numUnits", System.Type.GetType("System.Int64"))
                dt.Columns.Add("dtmStartDate", System.Type.GetType("System.String"))
                dt.Columns.Add("dtmFinishDate", System.Type.GetType("System.String"))
                dt.Columns.Add("vcAssignToName", System.Type.GetType("System.String"))
                dt.Columns.Add("numAssigneTo", System.Type.GetType("System.Int64"))
                dt.Columns.Add("vcBusinessProcessName", System.Type.GetType("System.String"))
                dt.Columns.Add("vcComments", System.Type.GetType("System.String"))
                dt.Columns.Add("numBusinessProcessId", System.Type.GetType("System.Int64"))
                dt.Columns.Add("numMaxBuildQty", System.Type.GetType("System.Int64"))
                dt.Columns.Add("numAssetChartAcntID", System.Type.GetType("System.Int64"))
                dt.Columns.Add("numAverageCost", System.Type.GetType("System.Decimal"))
                dt.Columns.Add("numSalesOrderId", System.Type.GetType("System.Int64"))
                dt.Columns.Add("vcSalesOrderName", System.Type.GetType("System.String"))

                ViewState("ItemsToSave") = dt
            End If
            Dim dtSavedTable As DataTable
            dtSavedTable = DirectCast(ViewState("ItemsToSave"), DataTable)


            Dim dr As DataRow() = dtSavedTable.Select("numItemCode='" & lngSelectedItemCode & "' AND numSalesOrderId='" & lngSalesOrderId & "'")
            If dr.Count = 1 Then
                Dim drSelectedRows As DataRow = dr(0)
                drSelectedRows("numWareHouseId") = numWareHouseId
                drSelectedRows("vcWarehouseName") = vcWarehouseName
                drSelectedRows("numUnits") = numUnits
                drSelectedRows("dtmStartDate") = dtmStartDate
                drSelectedRows("dtmFinishDate") = dtmFinishDate
                drSelectedRows("vcAssignToName") = vcAssignToName
                drSelectedRows("numAssigneTo") = numAssigneTo
                drSelectedRows("vcBusinessProcessName") = vcBusinessProcessName
                drSelectedRows("numBusinessProcessId") = numBusinessProcessId
                drSelectedRows("numMaxBuildQty") = numMaxBuildQty
                drSelectedRows("vcComments") = vcComments
                drSelectedRows("numAssetChartAcntID") = numAssetChartAcntID
                drSelectedRows("numAverageCost") = numAverageCost
                drSelectedRows("numSalesOrderId") = lngSalesOrderId
                drSelectedRows("vcSalesOrderName") = vcSalesOrderName
                ViewState("ItemsToSave") = dtSavedTable
            Else
                Dim drNewRow As DataRow
                drNewRow = dtSavedTable.NewRow()
                drNewRow("numItemCode") = numItemCode
                drNewRow("vcItemName") = vcItemName
                drNewRow("numSalesOrderId") = numSalesOrderId
                drNewRow("vcSalesOrderName") = vcSalesOrderName
                drNewRow("numWareHouseId") = numWareHouseId
                drNewRow("vcWarehouseName") = vcWarehouseName
                drNewRow("numUnits") = numUnits
                drNewRow("dtmStartDate") = dtmStartDate
                drNewRow("dtmFinishDate") = dtmFinishDate
                drNewRow("numAssigneTo") = numAssigneTo
                drNewRow("vcBusinessProcessName") = vcBusinessProcessName
                drNewRow("vcAssignToName") = vcAssignToName
                drNewRow("numBusinessProcessId") = numBusinessProcessId
                drNewRow("numMaxBuildQty") = numMaxBuildQty
                drNewRow("vcComments") = vcComments
                drNewRow("numAssetChartAcntID") = numAssetChartAcntID
                drNewRow("numAverageCost") = numAverageCost
                dtSavedTable.Rows.Add(drNewRow)
                ViewState("ItemsToSave") = dtSavedTable
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ClearControls(ByVal clearItemDropDown As Boolean, ByVal clearOrderDropdown As Boolean)
        Try
            radCmbSearch.ClearSelection()
            radCmbSearch.Text = String.Empty

            radCmbOrder_Search.ClearSelection()
            radCmbOrder_Search.Text = String.Empty

            ddlAssWareHouse.ClearSelection()
            ddlWOAssignTo.ClearSelection()
            ddlBusinessProcess.ClearSelection()
            txtUnits.Text = 1
            hfAssetChartAcntID.Value = 0
            hfAverageCost.Value = 0
            lblMaxQty.Text = "0"
            hdnEditSoid.Value = "0"
            txtComments.Text = ""
            hdnEditItemCode.Value = "0"
            calenderStartDate.SelectedDate = ""
            calenderFinishDate.SelectedDate = ""
            lblProjectFinishDate.Text = ""
            lblMaterialCost.Text = ""
            lblOverheadCost.Text = ""
            lblLabourCost.Text = ""
            hdnProjectedFinishDate.Value = ""
            hdnMaterialCost.Value = ""
            hdnOverheadCost.Value = ""
            hdnLabourCost.Value = ""
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub grdAddedItems_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If (e.CommandName = "Delete") Then
            Dim dtSavedTable As DataTable
            dtSavedTable = DirectCast(ViewState("ItemsToSave"), DataTable)
            Dim dr As DataRow() = dtSavedTable.Select("numItemCode='" & CCommon.ToInteger(e.CommandArgument) & "'")
            If dr.Length > 0 Then
                dr(0).Delete()
            End If
            ViewState("ItemsToSave") = dtSavedTable
            BindGrid()
        End If
        If (e.CommandName = "Edit") Then
            Dim dtSavedTable As DataTable
            dtSavedTable = DirectCast(ViewState("ItemsToSave"), DataTable)
            Dim dr As DataRow() = dtSavedTable.Select("numItemCode='" & CCommon.ToInteger(e.CommandArgument) & "'")
            If dr.Length > 0 Then
                Dim drNewRow As DataRow
                drNewRow = dr(0)
                radCmbSearch.Text = drNewRow("vcItemName")
                radCmbOrder_Search.Text = drNewRow("vcSalesOrderName")
                hdnEditSoid.Value = drNewRow("numSalesOrderId")
                hdnEditItemCode.Value = CCommon.ToInteger(e.CommandArgument)
                If ddlAssWareHouse.Items.FindByValue(drNewRow("numWareHouseId")) IsNot Nothing Then
                    ddlAssWareHouse.ClearSelection()
                    ddlAssWareHouse.Items.FindByValue(drNewRow("numWareHouseId")).Selected = True
                End If
                hfAssetChartAcntID.Value = drNewRow("numAssetChartAcntID")
                hfAverageCost.Value = drNewRow("numAverageCost")
                txtUnits.Text = drNewRow("numUnits")
                If CCommon.ToString(drNewRow("dtmStartDate")) <> "" Then
                    calenderStartDate.SelectedDate = drNewRow("dtmStartDate")
                End If
                If CCommon.ToString(drNewRow("dtmFinishDate")) <> "" Then
                    calenderFinishDate.SelectedDate = drNewRow("dtmFinishDate")
                End If
                If ddlWOAssignTo.Items.FindByValue(drNewRow("numAssigneTo")) IsNot Nothing Then
                    ddlWOAssignTo.ClearSelection()
                    ddlWOAssignTo.Items.FindByValue(drNewRow("numAssigneTo")).Selected = True
                End If
                If ddlBusinessProcess.Items.FindByValue(drNewRow("numBusinessProcessId")) IsNot Nothing Then
                    ddlBusinessProcess.ClearSelection()
                    ddlBusinessProcess.Items.FindByValue(drNewRow("numBusinessProcessId")).Selected = True
                End If
                lblMaxQty.Text = drNewRow("numMaxBuildQty")
                txtComments.Text = drNewRow("vcComments")
                bindItemsGrid(hdnEditItemCode.Value)
            End If
        End If
    End Sub
    Private Sub ResetGridState(ByVal grid As RadGrid)
        grid.MasterTableView.SortExpressions.Clear()
        grid.MasterTableView.GroupByExpressions.Clear()
        grid.CurrentPageIndex = 0
        grid.EditIndexes.Clear()
        grid.MasterTableView.IsItemInserted = False

        For Each column As GridColumn In grid.MasterTableView.RenderColumns
            column.CurrentFilterFunction = GridKnownFunction.NoFilter
            column.CurrentFilterValue = String.Empty
            column.AndCurrentFilterFunction = GridKnownFunction.NoFilter
            column.AndCurrentFilterValue = String.Empty
        Next

        grid.MasterTableView.FilterExpression = String.Empty
        grid.Rebind()
    End Sub
    Protected Sub grdAddedItems_RowEditing(sender As Object, e As GridViewEditEventArgs)

    End Sub

    Protected Sub grdAddedItems_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)

    End Sub

    Protected Sub grdAddedItems_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                Dim rdpStartDate As BACRM.Include.calandar = DirectCast(e.Row.FindControl("rdpStartDate"), BACRM.Include.calandar)
                Dim rdpFinishDate As BACRM.Include.calandar = DirectCast(e.Row.FindControl("rdpFinishDate"), BACRM.Include.calandar)

                If Not rdpStartDate Is Nothing Then
                    Dim startDate As DateTime

                    If DateTime.TryParse(CCommon.ToString(drv("dtmStartDate")), startDate) Then
                        rdpStartDate.SelectedDate = startDate
                    Else
                        rdpStartDate.SelectedDate = DateTime.UtcNow.AddMinutes(-1 * Convert.ToInt32(Session("ClientMachineUTCTimeOffset")))
                    End If
                End If

                If Not rdpFinishDate Is Nothing Then
                    Dim finishDate As DateTime

                    If DateTime.TryParse(CCommon.ToString(drv("dtmFinishDate")), finishDate) Then
                        rdpFinishDate.SelectedDate = finishDate
                    Else
                        rdpFinishDate.SelectedDate = DateTime.UtcNow.AddMinutes(-1 * Convert.ToInt32(Session("ClientMachineUTCTimeOffset"))).AddDays(1)
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class