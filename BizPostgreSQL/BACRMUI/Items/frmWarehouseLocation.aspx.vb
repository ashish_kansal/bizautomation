﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System.Web.Services
Imports Newtonsoft.Json

Public Class frmWarehouseLocation
    Inherits BACRMPage
    Dim lngWarehouseID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            lngWarehouseID = CCommon.ToLong(GetQueryStringVal("WarehouseID"))

            If Not IsPostBack Then
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCommon.GetDomainSettingValue("bitCloneLocationWithItem")

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    chkIncludeInternalLocation.Checked = CCommon.ToBool(dt.Rows(0)("bitCloneLocationWithItem"))
                End If


                bindWarehouse()
                If lngWarehouseID > 0 Then
                    PersistTable.Clear()
                    PersistTable.Add(ddlWarehouse.ID, lngWarehouseID)
                    PersistTable.Save(boolOnlyURL:=True)
                End If

                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    If Not ddlWarehouse.Items.FindByValue(CCommon.ToString(PersistTable(ddlWarehouse.ID))) Is Nothing Then
                        ddlWarehouse.Items.FindByValue(CCommon.ToString(PersistTable(ddlWarehouse.ID))).Selected = True
                        BindInternalLocation()
                    End If
                End If
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub bindWarehouse()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            ddlWarehouse.DataSource = objItem.GetWareHouses
            ddlWarehouse.DataTextField = "vcWareHouse"
            ddlWarehouse.DataValueField = "numWareHouseID"
            ddlWarehouse.DataBind()
            ddlWarehouse.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub BindInternalLocation()
        Dim objItem As New CItems
        Dim dt As New DataTable
        objItem.DomainID = Session("DomainID")
        objItem.WarehouseID = ddlWarehouse.SelectedValue
        dt = objItem.GetWarehouseLocation
        ddlAisle.Items.Clear()
        Dim Aisles = (From p In dt.AsEnumerable() Select p("vcAisle")).Distinct()
        For Each d In Aisles
            ddlAisle.Items.Add(New ListItem(d, d))
        Next
        ddlAisle.Items.Insert(0, New ListItem("-Select-", ""))
        ddlRack.Items.Clear()
        Dim racks = (From p In dt.AsEnumerable() Select p("vcRack")).Distinct()
        For Each d In racks
            ddlRack.Items.Add(New ListItem(d, d))
        Next
        ddlRack.Items.Insert(0, New ListItem("-Select-", ""))
        ddlShelf.Items.Clear()
        Dim Shelfs = (From p In dt.AsEnumerable() Select p("vcShelf")).Distinct()
        For Each d In Shelfs
            ddlShelf.Items.Add(New ListItem(d, d))
        Next
        ddlShelf.Items.Insert(0, New ListItem("-Select-", ""))
        ddlBin.Items.Clear()
        Dim Bins = (From p In dt.AsEnumerable() Select p("vcBin")).Distinct()
        For Each d In Bins
            ddlBin.Items.Add(New ListItem(d, d))
        Next
        ddlBin.Items.Insert(0, New ListItem("-Select-", ""))
    End Sub
    Sub BindGrid()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            objItem.WarehouseID = ddlWarehouse.SelectedValue
            objItem.vcAsile = ddlAisle.SelectedValue
            objItem.vcRack = ddlRack.SelectedValue
            objItem.vcShelf = ddlShelf.SelectedValue
            objItem.vcBin = ddlBin.SelectedValue
            objItem.WLocation = txtLocation.Text.Trim()
            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            objItem.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text)
            objItem.PageSize = Convert.ToInt32(Session("PagingRows"))
            gvWarehouseLocation.DataSource = objItem.GetWarehouseLocationPaging()
            gvWarehouseLocation.DataBind()

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objItem.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlWarehouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWarehouse.SelectedIndexChanged
        Try
            BindInternalLocation()
            PersistTable.Clear()
            PersistTable.Add(ddlWarehouse.ID, ddlWarehouse.SelectedValue)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnFilterByWarehouse_Click(sender As Object, e As EventArgs) Handles btnFilterByWarehouse.Click
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")

            For Each gvRow As GridViewRow In gvWarehouseLocation.Rows
                If CType(gvRow.FindControl("chk"), CheckBox).Checked = True Then
                    objItem.WarehouseLocationID = CCommon.ToLong(gvRow.Cells(0).Text)
                    objItem.DeleteWarehouseLocation()
                End If
            Next
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    <WebMethod()>
    Public Shared Function WebMethodUpdateInternalLocation(ByVal DomainID As Integer, ByVal numWLocationID As Long, ByVal vcAisle As String, ByVal vcRack As String, ByVal vcShelf As String, ByVal vcBin As String, ByVal vcLocation As String) As String
        Try
            Dim objItems = New CItems()
            Dim slpid As Long = 0
            objItems.DomainID = DomainID
            objItems.WarehouseLocationID = numWLocationID
            objItems.vcAsile = vcAisle
            objItems.vcRack = vcRack
            objItems.vcShelf = vcShelf
            objItems.vcBin = vcBin
            objItems.WLocation = vcLocation
            objItems.UpdateWareHouseLocation()
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(True, Formatting.None)
            Return json
        Catch ex As Exception
            Dim strError As String = ""

            strError = ex.Message
            Throw New Exception(strError)
            'Throw ex
        End Try
    End Function
    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBulkRemove_Click(sender As Object, e As EventArgs)
        Try
            Dim objItemWarehouse As New ItemWarehouse
            objItemWarehouse.DomainID = CCommon.ToLong(Session("DomainID"))
            objItemWarehouse.BulkRemove()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class