﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmDisassembleItem.aspx.vb" Inherits=".frmDisassembleItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Save() {
            var message = ""
            if ($("#txtQuantity").val() == "") {
                message += "Enter quantity to disassemble.\n";
            }
            else if (parseInt($("#txtQuantity").val()) <= 0) {
                message += "Quantity must be greater than 0.\n";
            }

            if ($("#ddlWarehouses").val() == 0) {
                message += "Select Warehouse";
            }

            if (message.length > 0) {
                alert(message);
                return false;
            } else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table style="width:100%">
        <tr>
            <td style="text-align: center;">
                <asp:label ID="litMessage" runat="server" ForeColor="Red"></asp:label>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Assembling History
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:GridView ID="dgAssembledHistory" runat="server" CellSpacing="3" CellPadding="3" DataKeyNames="ID,tintType,monAverageCost" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" CssClass="dg" Width="980">
        <HeaderStyle Height="30" />
        <Columns>
            <asp:BoundField HeaderText="Type" DataField="AssembledType" />
            <asp:BoundField HeaderText="Warehouse" DataField="vcWarehouse" />
            <asp:TemplateField HeaderText="Quantity">
                <ItemTemplate>
                    <asp:Label ID="lblAssembledQty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.numAssembledQty")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Assembled Date" DataField="dtCreatedDate" />
            <asp:BoundField HeaderText="Assembled By" DataField="vcUserName" />
            <asp:TemplateField HeaderText="Quantity to Disassembled" ItemStyle-Width="170" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:TextBox ID="txtQuantity" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container,"DataItem.numAssembledQty") %>'></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120">
                <ItemTemplate>
                    <asp:Button ID="lkbDisassembled"  CssClass="btn btn-primary" Text="Disassemble" CommandName="Disassemble" CommandArgument='<%# Container.DataItemIndex %>'  runat="server"></asp:Button>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hdnItemCode" runat="server" />
</asp:Content>