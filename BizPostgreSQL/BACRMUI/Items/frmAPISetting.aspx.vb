﻿Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts


Partial Public Class frmAPISetting
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindDropDowns()
                LoadDetails()
                'btnImportItems.Attributes.Add("onclick", "javascript:return ValidateImportItem();")
                Dim webAPiId As Integer = GetQueryStringVal("WebApiID")
                EnableCofigurationNotes(webAPiId)
                btnSave.Attributes.Add("onclick", "return ValidateSave();")
            End If
            lblErrorMessage.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub EnableCofigurationNotes(ByVal WebApiId As Integer)
        Try
            Select Case WebApiId

                Case 2
                    AmazonNotes.Visible = True
                    trFbaBizDoc.Visible = True
                    trFbaBizDocStatus.Visible = True
                    trFbaOrderStatus.Visible = True

                Case 5
                    EbayNotes.Visible = True

                Case 7
                    GoogleNotes.Visible = True
                Case 8
                    MagentoNotes.Visible = True

            End Select

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Sub BindDropDowns()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim dtChartAcntDetails As DataTable
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "010401"
            dtChartAcntDetails = objCOA.GetParentCategory()
            'BindCOA(ddlExpenseAccount, dtChartAcntDetails)


            objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlBizDoc, 27, Session("DomainID"))

            'Changed by Sachin Sadhu||Date:16thJune2014
            'Purpose :Filter Status as per order type:sales
            objCommon.sb_FillComboFromDBwithSel(ddlOrderStatus, 176, Session("DomainID"), "1")
            objCommon.sb_FillComboFromDBwithSel(ddlUnShipmentOrderStatus, 176, Session("DomainID"), "1")
            'end of code

            'Changed by Sachin Sadhu||Date:16thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            objCommon.sb_FillComboFromDBwithSel(ddlFBABizDoc, 27, Session("DomainID"))
            'End of code
            objCommon.sb_FillConEmpFromDBSel(ddlRecordOwner, Session("DomainID"), 0, 0)
            objCommon.sb_FillConEmpFromDBSel(ddlAssignTo, Session("DomainID"), 0, 0)
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            ddlWareHouse.DataValueField = "numWareHouseID"
            ddlWareHouse.DataTextField = "vcWareHouse"
            ddlWareHouse.DataSource = objItems.GetWareHouses
            ddlWareHouse.DataBind()
            ddlWareHouse.Items.Insert(0, "--Select One--")
            ddlWareHouse.Items.FindByText("--Select One--").Value = 0

            'objItems.str = "Discount"
            'ddlDiscountItemMapping.DataValueField = "numItemCode"
            'ddlDiscountItemMapping.DataTextField = "vcItemName"
            'ddlDiscountItemMapping.DataSource = objItems.GetUnArchivedServiceItems
            'ddlDiscountItemMapping.DataBind()
            'ddlDiscountItemMapping.Items.Insert(0, "--Select One--")
            'ddlDiscountItemMapping.Items.FindByText("--Select One--").Value = 0

            ddlSalesTaxItemMapping.DataValueField = "numItemCode"
            ddlSalesTaxItemMapping.DataTextField = "vcItemName"
            ddlSalesTaxItemMapping.DataSource = objItems.GetUnArchivedServiceItems
            ddlSalesTaxItemMapping.DataBind()
            ddlSalesTaxItemMapping.Items.Insert(0, "--Select One--")
            ddlSalesTaxItemMapping.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex

        End Try

    End Sub
    Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
        Try
            'Changed by Sachin Sadhu||Date:16thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc.SelectedValue))
            'End of code

            'objCommon.sb_FillComboFromDBwithSel(ddlFBABizDocStatus, 11, Session("DomainID"), CCommon.ToString(ddlFBABizDoc.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub ddlFBABizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFBABizDoc.SelectedIndexChanged
        Try
            'Changed by Sachin Sadhu||Date:16thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            objCommon.sb_FillComboFromDBwithSel(ddlFBABizDocStatus, 11, Session("DomainID"), CCommon.ToString(ddlFBABizDoc.SelectedValue))
            'End of code

            'objCommon.sb_FillComboFromDBwithSel(ddlFBABizDocStatus, 11, Session("DomainID"), CCommon.ToString(ddlFBABizDoc.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindCOA(ByVal ddl As DropDownList, ByVal dt As DataTable)
        Try
            Dim item As ListItem
            For Each dr As DataRow In dt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddl.Items.Add(item)
            Next
            ddl.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub LoadDetails()
        Try
            Dim objAPI As New WebAPI

            objAPI.Mode = 3
            'objAPI.DomainID = 0 'If not the WebApi is Mapped for the Domain (Takes from Procedure by default)
            objAPI.DomainID = CCommon.ToLong(Session("DomainId")) 'Only if WebApi is already Mapped for the Domain
            objAPI.WebApiId = GetQueryStringVal("WebApiID")

            Dim dtAPI As DataTable = objAPI.GetWebApi()

            If dtAPI.Rows.Count > 0 Then
                Dim dr As DataRow = dtAPI.Rows(0)
                lblPageTitle.Text = dr("vcProviderName").ToString()
                lblEnableItemUpdate.Text = "Enable Updating Item details to " & dr("vcProviderName").ToString() & "."
                lblEnableInventoryUpdate.Text = "Enable Updating Inventory to " & dr("vcProviderName").ToString() & "."
                lblEnableOrdersImport.Text = "Enable Order import from " & dr("vcProviderName").ToString() & "."
                lblEnableTrackingUpdate.Text = "Enable Updating OrderTracking No to " & dr("vcProviderName").ToString() & "."
                lblFirstFieldName.Text = dr("vcFirstFldName").ToString()
                lblSecondFieldName.Text = dr("vcSecondFldName").ToString()
                lblThirdFieldName.Text = dr("vcThirdFldName").ToString()
                lblFourthFieldName.Text = dr("vcFourthFldName").ToString()
                lblFifthFieldName.Text = dr("vcFifthFldName").ToString()
                lblSixthFieldName.Text = dr("vcSixthFldName").ToString()
                lblSeventhFieldName.Text = dr("vcSeventhFldName").ToString()
                lblEighthFieldName.Text = dr("vcEighthFldName").ToString()
                lblNinthFieldName.Text = dr("vcNinthFldName").ToString()
                lblTenthFieldName.Text = dr("vcTenthFldName").ToString()
                txtFirstFieldValue.Text = dr("vcFirstFldValue").ToString()
                txtSecondFieldValue.Text = dr("vcSecondFldValue").ToString()
                txtThirdFieldValue.Text = dr("vcThirdFldValue").ToString()
                txtFourthFieldValue.Text = dr("vcFourthFldValue").ToString()
                txtFifthFieldValue.Text = dr("vcFifthFldValue").ToString()
                txtSixthFieldValue.Text = dr("vcSixthFldValue").ToString()
                txtSeventhFieldValue.Text = dr("vcSeventhFldValue").ToString()
                txtEighthFieldValue.Text = dr("vcEighthFldValue").ToString()
                calNinthFieldValue.SelectedDate = dr("vcNinthFldValue").ToString()
                calTenthFieldValue.SelectedDate = dr("vcTenthFldValue").ToString()
                If objAPI.WebApiId = 2 Then ' Amazon US
                    lblSixteenthFldName.Text = "FBA Order Last Updated date"
                    calSixteenthFldValue.SelectedDate = dr("vcSixteenthFldValue").ToString()
                End If
                chkEnable.Checked = CCommon.ToBool(dr("bitEnableAPI"))

                chkEnableItemUpdate.Checked = CCommon.ToBool(dr("bitEnableItemUpdate"))
                chkEnableInventoryUpdate.Checked = CCommon.ToBool(dr("bitEnableInventoryUpdate"))
                chkEnableOrdersImport.Checked = CCommon.ToBool(dr("bitEnableOrderImport"))
                chkEnableTrackingUpdate.Checked = CCommon.ToBool(dr("bitEnableTrackingUpdate"))

                If Not ddlBizDoc.Items.FindByValue(dr("numBizDocId")) Is Nothing Then
                    ddlBizDoc.Items.FindByValue(dr("numBizDocId")).Selected = True
                    'Added By :Sachin Sadhu||Date:16thJune2014
                    'Purpose: to select entered bizdoc status
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"), CCommon.ToString(dr("numBizDocId")))
                    'End of code
                End If
                If Not ddlBizDocStatus.Items.FindByValue(dr("numBizDocStatusId")) Is Nothing Then
                    ddlBizDocStatus.Items.FindByValue(dr("numBizDocStatusId")).Selected = True
                End If
                If Not ddlOrderStatus.Items.FindByValue(dr("numOrderStatus")) Is Nothing Then
                    ddlOrderStatus.Items.FindByValue(dr("numOrderStatus")).Selected = True
                End If
                If Not ddlFBABizDoc.Items.FindByValue(dr("numFBABizDocId")) Is Nothing Then
                    ddlFBABizDoc.Items.FindByValue(dr("numFBABizDocId")).Selected = True

                    'Added By :Sachin Sadhu||Date:16thJune2014
                    'Purpose: to select entered bizdoc status
                    objCommon.sb_FillComboFromDBwithSel(ddlFBABizDocStatus, 11, Session("DomainID"), CCommon.ToString(dr("numFBABizDocId")))
                    'End of code
                End If
                If Not ddlFBABizDocStatus.Items.FindByValue(dr("numFBABizDocStatusId")) Is Nothing Then
                    ddlFBABizDocStatus.Items.FindByValue(dr("numFBABizDocStatusId")).Selected = True
                End If
                If Not ddlUnShipmentOrderStatus.Items.FindByValue(dr("numUnShipmentOrderStatus")) Is Nothing Then
                    ddlUnShipmentOrderStatus.Items.FindByValue(dr("numUnShipmentOrderStatus")).Selected = True
                End If
                If Not ddlRecordOwner.Items.FindByValue(dr("numRecordOwner")) Is Nothing Then
                    ddlRecordOwner.Items.FindByValue(dr("numRecordOwner")).Selected = True
                End If
                If Not ddlAssignTo.Items.FindByValue(dr("numAssignTo")) Is Nothing Then
                    ddlAssignTo.Items.FindByValue(dr("numAssignTo")).Selected = True
                End If
                If Not ddlWareHouse.Items.FindByValue(dr("numWareHouseID")) Is Nothing Then
                    ddlWareHouse.Items.FindByValue(dr("numWareHouseID")).Selected = True
                End If
                If Not ddlRelationship.Items.FindByValue(dr("numRelationshipId")) Is Nothing Then
                    ddlRelationship.Items.FindByValue(dr("numRelationshipId")).Selected = True
                End If
                If Not ddlProfile.Items.FindByValue(dr("numProfileId")) Is Nothing Then
                    ddlProfile.Items.FindByValue(dr("numProfileId")).Selected = True
                End If
                'If Not ddlExpenseAccount.Items.FindByValue(dr("numExpenseAccountId")) Is Nothing Then
                '    ddlExpenseAccount.Items.FindByValue(dr("numExpenseAccountId")).Selected = True
                'End If

                'If Not ddlDiscountItemMapping.Items.FindByValue(dr("numShippingServiceItemID")) Is Nothing Then
                '    ddlDiscountItemMapping.Items.Remove(ddlDiscountItemMapping.Items.FindByValue(dr("numShippingServiceItemID")))
                'End If

                If Not ddlSalesTaxItemMapping.Items.FindByValue(dr("numShippingServiceItemID")) Is Nothing Then
                    ddlSalesTaxItemMapping.Items.Remove(ddlSalesTaxItemMapping.Items.FindByValue(dr("numShippingServiceItemID")))
                End If

                If Not ddlSalesTaxItemMapping.Items.FindByValue(dr("numDiscountItemMapping")) Is Nothing Then
                    ddlSalesTaxItemMapping.Items.Remove(ddlSalesTaxItemMapping.Items.FindByValue(dr("numDiscountItemMapping")))
                End If

                'If Not ddlDiscountItemMapping.Items.FindByValue(dr("numDiscountItemMapping")) Is Nothing Then
                '    ddlDiscountItemMapping.Items.FindByValue(dr("numDiscountItemMapping")).Selected = True
                'End If

                If Not ddlSalesTaxItemMapping.Items.FindByValue(dr("numSalesTaxItemMapping")) Is Nothing Then
                    ddlSalesTaxItemMapping.Items.FindByValue(dr("numSalesTaxItemMapping")).Selected = True
                End If
                hdnShippingServiceItemMapping.Value = CCommon.ToInteger(dr("numShippingServiceItemID"))
                hdnDiscountServiceItemMapping.Value = CCommon.ToInteger(dr("numDiscountItemMapping"))

            End If

            If objAPI.WebApiId = 1 Then
                btnGenToken.Visible = True
            Else
                btnGenToken.Visible = False
            End If
            If lblFirstFieldName.Text.Length = 0 Then
                lblFirstFieldName.Visible = False
                txtFirstFieldValue.Visible = False
            End If
            If lblSecondFieldName.Text.Length = 0 Then
                lblSecondFieldName.Visible = False
                txtSecondFieldValue.Visible = False
            End If
            If lblThirdFieldName.Text.Length = 0 Then
                lblThirdFieldName.Visible = False
                txtThirdFieldValue.Visible = False
            End If
            If lblFourthFieldName.Text.Length = 0 Then
                lblFourthFieldName.Visible = False
                txtFourthFieldValue.Visible = False
            End If
            If lblFifthFieldName.Text.Length = 0 Then
                lblFifthFieldName.Visible = False
                txtFifthFieldValue.Visible = False
            End If
            If lblSixthFieldName.Text.Length = 0 Then
                lblSixthFieldName.Visible = False
                txtSixthFieldValue.Visible = False
            End If
            If lblSeventhFieldName.Text.Length = 0 Then
                lblSeventhFieldName.Visible = False
                txtSeventhFieldValue.Visible = False
            End If
            If lblEighthFieldName.Text.Length = 0 Then
                lblEighthFieldName.Visible = False
                txtEighthFieldValue.Visible = False
            End If
            If lblNinthFieldName.Text.Length = 0 Then
                lblNinthFieldName.Visible = False
                calNinthFieldValue.Visible = False
            End If
            If lblTenthFieldName.Text.Length = 0 Then
                lblTenthFieldName.Visible = False
                calTenthFieldValue.Visible = False
            End If
            If objAPI.WebApiId = 2 Then
                lblSixteenthFldName.Visible = True
                calSixteenthFldValue.Visible = True
            Else
                lblSixteenthFldName.Visible = False
                calSixteenthFldValue.Visible = False
            End If



        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnImportItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnImportItems.Click
        Try
            Dim strMessage As String
            Dim objUserAccess As New UserAccess()
            objUserAccess.DomainID = Session("DomainID")
            Dim dtDomainDetails As DataTable = objUserAccess.GetDomainDetails()
            Dim DefaultIncomeAccID As Integer = 0, DefaultCOGSAccID As Integer = 0, DefaultAssetAccID As Integer = 0

            If dtDomainDetails.Rows.Count > 0 Then
                DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows(0)("numIncomeAccID"))
                DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows(0)("numCOGSAccID"))
                DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows(0)("numAssetAccID"))

                If DefaultIncomeAccID = 0 OrElse DefaultCOGSAccID = 0 OrElse DefaultAssetAccID = 0 Then
                    strMessage = "Unable to import Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,"
                    ClientScript.RegisterClientScriptBlock(Page.GetType, "alert", "alert(" & strMessage & ");", True)

                Else
                    SaveAPISettings()
                    Dim objWebApi As New WebAPI
                    objWebApi.DomainID = CCommon.ToLong(Session("DomainID"))
                    objWebApi.UserContactID = CCommon.ToLong(Session("UserContactID"))
                    objWebApi.WebApiId = GetQueryStringVal("WebApiID")
                    objWebApi.FlagItemImport = 1
                    objWebApi.ManageWebApiItemImport()

                    Dim objCommon As New CCommon()
                    objCommon.Mode = 34
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UpdateRecordID = GetQueryStringVal("WebApiID")
                    objCommon.UpdateValueID = 1
                    objCommon.UpdateSingleFieldValue()

                End If

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub SaveAPISettings()
        Try
            Dim objAPI As New WebAPI
            objAPI.WebApiId = GetQueryStringVal("WebApiID")
            objAPI.DomainID = Session("DomainID")
            objAPI.Value1 = txtFirstFieldValue.Text.Trim()
            objAPI.Value2 = txtSecondFieldValue.Text.Trim()
            objAPI.Value3 = txtThirdFieldValue.Text.Trim()
            objAPI.Value4 = txtFourthFieldValue.Text.Trim()
            objAPI.Value5 = txtFifthFieldValue.Text.Trim()
            objAPI.Value6 = txtSixthFieldValue.Text.Trim()
            objAPI.Value7 = txtSeventhFieldValue.Text.Trim()
            objAPI.Value8 = txtEighthFieldValue.Text.Trim()
            objAPI.Value9 = calNinthFieldValue.SelectedDate
            objAPI.Value10 = calTenthFieldValue.SelectedDate
            objAPI.EnableAPI = chkEnable.Checked
            objAPI.RelationshipID = ddlRelationship.SelectedValue
            objAPI.ProfileID = ddlProfile.SelectedValue
            objAPI.RecordOwner = ddlRecordOwner.SelectedValue
            objAPI.AssignTo = ddlAssignTo.SelectedValue
            objAPI.WareHouseID = ddlWareHouse.SelectedValue
            objAPI.OrderStatus = ddlOrderStatus.SelectedValue
            objAPI.UnShipmentOrderStatus = ddlUnShipmentOrderStatus.SelectedValue
            objAPI.BizDocID = ddlBizDoc.SelectedValue
            objAPI.BizDocStatusID = ddlBizDocStatus.SelectedValue
            objAPI.FbaBizDocID = ddlFBABizDoc.SelectedValue
            objAPI.FbaBizDocStatusID = ddlFBABizDocStatus.SelectedValue
            'objAPI.ExpenseAccountId = ddlExpenseAccount.SelectedValue
            ' objAPI.DiscountItemMapping = ddlDiscountItemMapping.SelectedValue
            objAPI.SalesTaxItemMapping = ddlSalesTaxItemMapping.SelectedValue

            objAPI.EnableItemUpdate = chkEnableItemUpdate.Checked
            objAPI.EnableInventoryUpdate = chkEnableInventoryUpdate.Checked
            objAPI.EnableOrderImport = chkEnableOrdersImport.Checked
            objAPI.EnableTrackingUpdate = chkEnableTrackingUpdate.Checked

            objAPI.SaveAPISettings()

            If chkEnable.Checked = False Then
                Dim objWebApi As New WebAPI
                objWebApi.DomainID = Session("DomainID")
                objWebApi.UserContactID = Session("UserContactID")
                objWebApi.WebApiId = GetQueryStringVal("WebApiID")
                objWebApi.FlagItemImport = 0
                objWebApi.ManageWebApiItemImport()

                Dim objCommon As New CCommon()
                objCommon.Mode = 34
                objCommon.DomainID = Session("DomainID")
                objCommon.UpdateRecordID = GetQueryStringVal("WebApiID")
                objCommon.UpdateValueID = 0
                objCommon.UpdateSingleFieldValue()
            End If

        Catch exSql As SqlClient.SqlException
            If exSql.Message = "AR_and_AP_Relationship_NotSet" Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""' );", True)

            End If

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strMessage As String = ""
            strMessage = ValidateShippingFromAddress()
            If strMessage = "" Then
                strMessage = ValidateOrderImportDate()
                If strMessage = "" Then
                    SaveAPISettings()
                Else
                    lblErrorMessage.Text = strMessage
                    lblErrorMessage.Visible = True
                End If

            Else
                'ClientScript.RegisterClientScriptBlock(Page.GetType, "alert", "AlertShippingFromMissing();", True)
                lblErrorMessage.Text = strMessage
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Function ValidateOrderImportDate() As String
        Dim vaild As Boolean = True
        Dim strMessage As String = ""
        Try
            Dim WebApiID As Integer = CCommon.ToInteger(GetQueryStringVal("WebApiID"))
            If WebApiID = 2 Then 'AmazonUS
                Dim CurrentTime As New DateTime
                CurrentTime = DateTime.Now

                Dim OrderDate As New DateTime
                OrderDate = Convert.ToDateTime(calNinthFieldValue.SelectedDate)
                'OrderDate = DateFromFormattedDate(calNinthFieldValue.SelectedDate, Session("DateFormat"))
                'OrderDate = calTenthFieldValue.SelectedDate
                Dim FBAOrderDate As New DateTime
                'FBAOrderDate = calSixteenthFldValue.SelectedDate
                'FBAOrderDate = DateFromFormattedDate(calSixteenthFldValue.SelectedDate, Session("DateFormat"))
                FBAOrderDate = Convert.ToDateTime(calSixteenthFldValue.SelectedDate)
                Dim span As New TimeSpan
                span = CurrentTime.Subtract(OrderDate)
                If span.Days > 90 Then
                    strMessage = "You cannot set Last Order updated date time to earlier than 90 days from current date time"
                    vaild = False
                ElseIf span.TotalMinutes <= 0 Then
                    strMessage = "You cannot set Last Order updated date time to later than current date time"
                    vaild = False

                End If
                span = CurrentTime.Subtract(FBAOrderDate)
                If span.Days > 90 Then
                    strMessage = "You cannot set FBA Last Order updated date time to earlier than 90 days from current date time"
                    vaild = False
                ElseIf span.TotalMinutes <= 0 Then
                    strMessage = "You cannot set FBA Last Order updated date time to later than current date time"
                    vaild = False

                End If
            End If

        Catch ex As Exception
            Throw ex

        End Try

        Return strMessage
    End Function

    Private Sub btnGenToken_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenToken.Click
        Try
            If (txtSecondFieldValue.Text.Length > 2 And txtThirdFieldValue.Text.Length > 2) Then
                Dim strToken As String = Login()
                If (strToken.Length > 2) Then
                    txtFourthFieldValue.Text = strToken
                Else
                    txtFourthFieldValue.Text = ""
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function Login() As String
        Try
            Dim strAPIURL As String = "http://www." + txtFirstFieldValue.Text.Trim() + ".com/api/rest/?method=auth.login&user=" + txtSecondFieldValue.Text.Trim() + "&section=" + txtThirdFieldValue.Text.Trim()
            Dim strResponse As String = WebAPI.GetResponse(strAPIURL)
            If (strResponse.IndexOf("<token>") > 0) Then
                Dim start As Integer = strResponse.IndexOf("<token>", StringComparison.OrdinalIgnoreCase)
                Return strResponse.Substring(start + 7, 12)
            End If

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ValidateShippingFromAddress() As String
        'Dim strMessage As String = ""

        Try
            Dim IsValid As Boolean = True
            Dim strMessage As String = ""
            Dim dtShipFrom As DataTable
            Dim objContacts As New CContacts
            objContacts.ContactID = Session("UserContactID")
            objContacts.DomainID = Session("DomainID")
            dtShipFrom = objContacts.GetBillOrgorContAdd()

            If dtShipFrom.Rows.Count > 0 Then

                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcShipCountry")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False

                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcShipState")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcFirstname")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcLastname")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcCompanyName")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcPhone")).Trim().Length < 4 Then
                    strMessage = "Contact phone number not set in your contact details"
                    IsValid = False
                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcShipPostCode")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If
                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcShipCity")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If

                If CCommon.ToString(dtShipFrom.Rows(0).Item("vcShipStreet")).Trim().Length < 2 Then
                    strMessage = "Shipping Address not set in Organization Details, "
                    IsValid = False
                End If

            Else

                IsValid = False

            End If

            Return strMessage

        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class