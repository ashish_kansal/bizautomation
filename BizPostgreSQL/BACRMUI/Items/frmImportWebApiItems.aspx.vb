﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting

Imports System.Data

Public Class frmImportWebApiItems
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                BindDropDowns()
                btnImportItems.Attributes.Add("onclick", "javascript:return ValidateImportItem();")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Sub BindDropDowns()
        Try
            Dim objWebApi As New WebAPI
            objWebApi.Mode = 4
            objWebApi.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dtWebAPI As DataTable = objWebApi.GetWebApi()
            ddlWebApi.DataValueField = "WebApiId"
            ddlWebApi.DataTextField = "vcProviderName"
            ddlWebApi.DataSource = dtWebAPI
            ddlWebApi.DataBind()
            ddlWebApi.Items.Insert(0, "--Select One--")
            ddlWebApi.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnImportItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportItems.Click
        Try
            Dim objUserAccess As New UserAccess()
            objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dtDomainDetails As DataTable = objUserAccess.GetDomainDetails()
            Dim DefaultIncomeAccID As Integer = 0, DefaultCOGSAccID As Integer = 0, DefaultAssetAccID As Integer = 0

            If dtDomainDetails.Rows.Count > 0 Then
                DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows(0)("numIncomeAccID"))
                DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows(0)("numCOGSAccID"))
                DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows(0)("numAssetAccID"))
                If DefaultIncomeAccID = 0 OrElse DefaultCOGSAccID = 0 OrElse DefaultAssetAccID = 0 Then
                    ' Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set default IncomeAccID,COGSAccID and AssetAccID  from ""Administration->Domain Details->Accounting""')", True)
                    Dim ExcepitonMessage As String = "Please Set default IncomeAccID,COGSAccID and AssetAccID  from ""Administration->Global Settings->Accounting""."
                    ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & ExcepitonMessage & "'); window.close();", True)
                    Exit Sub
                End If
            End If

            Dim objWebApi As New WebAPI
            objWebApi.DomainID = CCommon.ToLong(Session("DomainID"))
            objWebApi.UserContactID = CCommon.ToLong(Session("UserContactID"))
            objWebApi.WebApiId = CCommon.ToInteger(ddlWebApi.SelectedValue)
            objWebApi.FlagItemImport = 1 & "|" & CCommon.ToString(ddlListingType.SelectedValue)
            objWebApi.ManageWebApiItemImport()

            'Dim objCommon As New CCommon()
            'objCommon.Mode = 34
            'objCommon.DomainID = Session("DomainID")
            'objCommon.UpdateRecordID = CCommon.ToLong(ddlWebApi.SelectedValue)
            'objCommon.UpdateValueID = 1
            'objCommon.UpdateSingleFieldValue()
            Dim strMessage As String = "Request to import Item is sent to Marketplace. You will be notified by an Email with Imported item details once items are imported to BizAutomation."
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "'); window.close();", True)
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Request to import Item is passed to Marketplace. You will be notified by an Email with item details once items are imported to BizAutomation.');window.close();", True)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class