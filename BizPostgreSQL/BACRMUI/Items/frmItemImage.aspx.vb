Imports System.IO
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Partial Class frmItemImage
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strFileName As String
    Dim strFileTName As String
    Dim strFilePath As String
   
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Dim DomainId As Integer = CCommon.ToInteger((Session("DomainId")))
            If DomainId < 1 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Close", "window.close();", True)
            Else

                hdnItemCode.Value = CCommon.ToString(Me.lngItemCode)

                If Not IsPostBack Then
                    rbMeasureWidth.Checked = True
                    BindImageData()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Try
            Dim strNumItemImageIds As String = ""
            Dim strImages As String = ""
            Dim strTImages As String = ""
            For i As Integer = 0 To gvImageData.Rows.Count - 1
                Dim cbRow As CheckBox = CType(gvImageData.Rows(i).FindControl("cbRow"), CheckBox)
                If cbRow.Checked Then
                    Dim lblNumItemImageId As Label = CType(gvImageData.Rows(i).FindControl("lblNumItemImageId"), Label)
                    Dim lblVcPathForImage As Label = CType(gvImageData.Rows(i).FindControl("lblVcPathForImage"), Label)
                    Dim lblVcPathForTImage As Label = CType(gvImageData.Rows(i).FindControl("lblVcPathForTImage"), Label)

                    strNumItemImageIds = strNumItemImageIds & lblNumItemImageId.Text & ","
                    strImages = strImages & lblVcPathForImage.Text & ","
                    strTImages = strTImages & lblVcPathForTImage.Text & ","

                End If
            Next

            Dim strImage As String() = strImages.Split(",")
            Dim strTImage As String() = strTImages.Split(",")

            If strNumItemImageIds <> "" Then
                Dim objItem As New CItems
                Dim result As Boolean
                objItem.DomainID = Session("DomainId")
                objItem.ItemCode = Me.lngItemCode
                result = objItem.DeleteItemImages(strNumItemImageIds)
                If result = True Then
                    For j As Integer = 0 To strImage.Length - 2
                        If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & strImage(j)) Then
                            File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & strImage(j))
                        End If
                    Next
                    For j As Integer = 0 To strTImage.Length - 2
                        If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & strTImage(j)) Then
                            File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & strTImage(j))
                        End If
                    Next
                End If
                BindImageData()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Public Sub BindImageData()
        Try
            Dim objItem As New CItems
            objItem.ItemCode = Me.lngItemCode
            objItem.DomainID = Session("DomainId")

            Dim dtImage As DataTable
            dtImage = objItem.ImageItemDetails()

            If dtImage.Rows.Count > 0 Then
                gvImageData.DataSource = dtImage
                gvImageData.DataBind()
                pnlImageData.Visible = True
                pnlImageUpload.Visible = False
            Else
                pnlImageData.Visible = False
                pnlImageUpload.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
        Try
            Dim strNumItemImageIds As String = ""
            For i As Integer = 0 To gvImageData.Rows.Count - 1
                Dim rbDefault As RadioButton = CType(gvImageData.Rows(i).FindControl("rbDefault"), RadioButton)
                Dim txtDisplayOrder As TextBox = CType(gvImageData.Rows(i).FindControl("txtDisplayOrder"), TextBox)
                Dim lblNumItemImageId As Label = CType(gvImageData.Rows(i).FindControl("lblNumItemImageId"), Label)

                Dim objItem As New CItems

                objItem.bitDefault = rbDefault.Checked
                objItem.DisplayOrder = CCommon.ToInteger(txtDisplayOrder.Text)
                objItem.numItemImageId = CCommon.ToInteger(lblNumItemImageId.Text)

                Dim result As Boolean
                result = objItem.AddUpdateImageItem()
            Next

            BindImageData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub gvImageData_DataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvImageData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ltrlImage As Literal = e.Row.FindControl("ltrlImage")
                Dim hlShowDocument As HyperLink = e.Row.FindControl("hlShowDocument")

                If CCommon.CheckForImage(ltrlImage.Text) Then
                    hlShowDocument.NavigateUrl = CCommon.GetDocumentPath(CCommon.ToLong(Session("DomainID"))) & ltrlImage.Text
                    ltrlImage.Text = CCommon.GetImageHTML(ltrlImage.Text, 1)
                Else
                    Dim rbDefault As RadioButton = e.Row.FindControl("rbDefault")
                    rbDefault.Visible = False
                    Dim strDocumentImage As String
                    hlShowDocument.NavigateUrl = CCommon.GetDocumentPath(CCommon.ToLong(Session("DomainID"))) & ltrlImage.Text
                    strDocumentImage = CCommon.GetDocumentImage(ltrlImage.Text)
                    ltrlImage.Text = "<img class=""resizeme"" style=""border-width:0px;"" alt=""change image"" src=""../Images/" & strDocumentImage & """>"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefresh.Click
        Try
            BindImageData()
            pnlImageData.Visible = True
            pnlImageUpload.Visible = False
            rbMeasureWidth.Checked = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnUploadImage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUploadImage.Click
        Try
            pnlImageData.Visible = False
            pnlImageUpload.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


#Region "Custom Properties"
    Public ReadOnly Property lngItemCode() As Long
        Get
            Dim o As Object = ViewState("ItemCode")
            If o IsNot Nothing Then
                Return CCommon.ToLong(o)
            Else
                If GetQueryStringVal("ItemCode") <> "" Then
                    ViewState("ItemCode") = GetQueryStringVal("ItemCode")
                    Return CCommon.ToLong(ViewState("ItemCode"))
                Else
                    Return 0
                End If
            End If
        End Get
    End Property
#End Region

#Region "Extra Code"
    'Private Sub gvImageData_DataBound(sender As Object, e As EventArgs) Handles gvImageData.DataBound
    '    Try
    '        '<%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
    '        Dim strLink As String
    '        For Each gvRow As GridViewRow In gvImageData.Rows
    '            If gvRow.RowType = DataControlRowType.DataRow Then
    '                strLink = CCommon.GetImageHTML(gvRow.DataItem(0).Text, 1)
    '                gvRow.Cells(0).Text = strLink
    '            End If
    '        Next
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
#End Region

End Class

