Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Partial Public Class frmPriceBookMgmt
    Inherits BACRMPage
    Dim lngRuleId As Long
   
    Dim objPriceBookRule As PriceBookRule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngRuleId = CCommon.ToLong(GetQueryStringVal("RuleId"))
            If Not IsPostBack Then

                If lngRuleId > 0 Then
                    pnlSel.Visible = True
                    objCommon = New CCommon
                    GetUserRightsForPage(13, 19)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSave.Visible = False
                            btnSaveClose.Visible = False
                        End If
                    End If
                    'objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    'objCommon.sb_FillComboFromDBwithSel(ddlRelationship1, 5, Session("DomainID"))
                    'objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
                    LoadDetails()
                    hplPricingTable.Attributes.Add("onclick", "return PriceTable(" & lngRuleId & ")")
                    hplIndividualItems.Attributes.Add("onclick", "return OpenConfig(2,1," & lngRuleId & ")")
                    hplClassification.Attributes.Add("onclick", "return OpenConfig(2,2," & lngRuleId & ")")
                    hplIndividualOrg.Attributes.Add("onclick", "return OpenConfig(3,1," & lngRuleId & ")")
                    hplRelProfile.Attributes.Add("onclick", "return OpenConfig(3,2," & lngRuleId & ")")
                    'btnAddItem.Attributes.Add("onclick", "return CheckRad(" & radSelType1.ClientID & ")")
                    'btnAddItemGroup.Attributes.Add("onclick", "return CheckRad(" & radSelType2.ClientID & ")")
                    'btnAddCompany.Attributes.Add("onclick", "return CheckRad(" & radSelType4.ClientID & ")")
                    'btnAddRel.Attributes.Add("onclick", "return CheckRad(" & radSelType5.ClientID & ")")
                    'btnAddRelPro.Attributes.Add("onclick", "return CheckRad(" & radSelType6.ClientID & ")")
                Else
                    Dim lngItemCode, lngVendor As Long
                    lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
                    lngVendor = CCommon.ToLong(GetQueryStringVal("Vendor"))

                    If lngItemCode > 0 And lngVendor > 0 Then
                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")

                        Dim vcItemName, vcVendorName As String
                        objCommon.Mode = 4
                        objCommon.Str = lngItemCode
                        vcItemName = objCommon.GetSingleFieldValue()

                        objCommon.Mode = 5
                        objCommon.Str = lngVendor
                        vcVendorName = objCommon.GetSingleFieldValue()

                        txtRuleName.Text = vcItemName & " - " & vcVendorName
                        txtRuleDesc.Text = vcItemName & " - " & vcVendorName
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            objPriceBookRule = New PriceBookRule
            objPriceBookRule.RuleID = lngRuleId
            objPriceBookRule.byteMode = 0
            dtTable = objPriceBookRule.GetPriceBookRule
            If dtTable.Rows.Count > 0 Then
                txtRuleName.Text = dtTable.Rows(0).Item("vcRuleName")
                txtRuleDesc.Text = dtTable.Rows(0).Item("vcRuleDescription")
                hdnRuleFor.Value = CCommon.ToShort(dtTable.Rows(0).Item("tintRuleFor"))
                chkRoundTo.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitRoundTo"))
                ddlRoundTo.SelectedValue = CCommon.ToShort(dtTable.Rows(0).Item("tintRoundTo"))

                If hdnRuleFor.Value = "2" Then
                    hplPricingTable.Text = hplPricingTable.Text.Replace("Pricing Table", "Costing Table")
                    hplIndividualOrg.Text = hplIndividualOrg.Text.Replace("customers individually", "vendors")
                    lblStep3Caption.Text = "Select Vendors"
                End If

                hindButton()

                If Not ddlRuleType.Items.FindByValue(dtTable.Rows(0).Item("tintRuleType")) Is Nothing Then
                    ddlRuleType.Items.FindByValue(dtTable.Rows(0).Item("tintRuleType")).Selected = True
                End If

                If dtTable.Rows(0).Item("tintPricingMethod") = 2 Then
                    rbPricingFormula.Checked = True
                    If CCommon.ToInteger(dtTable.Rows(0).Item("tintRuleType")) > 0 Then
                        ddlRuleType.SelectedValue = dtTable.Rows(0).Item("tintRuleType")
                    End If
                    ddlDiscountType.SelectedValue = dtTable.Rows(0).Item("tintDiscountType")
                    txtDiscount.Text = dtTable.Rows(0).Item("decDiscount")
                    txtQntyItems.Text = dtTable.Rows(0).Item("intQntyItems")
                    txtMaxPerOrAmt.Text = dtTable.Rows(0).Item("decMaxDedPerAmt")
                Else
                    rbPricingTable.Checked = True
                End If

                If Not ddlVendorCostType.Items.FindByValue(dtTable.Rows(0).Item("tintVendorCostType")) Is Nothing Then
                    ddlVendorCostType.Items.FindByValue(dtTable.Rows(0).Item("tintVendorCostType")).Selected = True
                End If

                If ddlRuleType.SelectedValue = "2" Then
                    ddlVendorCostType.Visible = True
                Else
                    ddlVendorCostType.Visible = False
                End If


                Select Case dtTable.Rows(0).Item("tintStep2")
                    Case 1
                        rbIndividualItem.Checked = True
                        hplIndividualItems.Text = hplIndividualItems.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("Step2Count")).ToString + ")"
                        hplClassification.Text = hplClassification.Text + " (0)"
                    Case 2
                        rbItemClassification.Checked = True
                        hplClassification.Text = hplClassification.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("Step2Count")).ToString + ")"
                        hplIndividualItems.Text = hplIndividualItems.Text + " (0)"
                    Case 3
                        rbAllItems.Checked = True
                End Select
                Select Case dtTable.Rows(0).Item("tintStep3")
                    Case 1
                        rbIndividualOrg.Checked = True
                        hplIndividualOrg.Text = hplIndividualOrg.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("Step3Count")).ToString + ")"
                        hplRelProfile.Text = hplRelProfile.Text + " (0)"
                    Case 2
                        rbRelProfile.Checked = True
                        hplRelProfile.Text = hplRelProfile.Text + " (" + CCommon.ToInteger(dtTable.Rows(0).Item("Step3Count")).ToString + ")"
                        hplIndividualOrg.Text = hplIndividualOrg.Text + " (0)"
                    Case 3
                        rbAllOrg.Checked = True
                End Select


                'LoadRuleAppDetails(IIf(IsDBNull(dtTable.Rows(0).Item("tintRuleAppType")), 0, dtTable.Rows(0).Item("tintRuleAppType")))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Sub LoadRuleAppDetails(ByVal shortRuleAppType As Short)
    '    Try
    '        objPriceBookRule.RuleID = lngRuleId
    '        objPriceBookRule.DomainID = Session("DomainID")
    '        objPriceBookRule.RuleAppType = shortRuleAppType
    '        Dim ds As DataSet

    '        ds = objPriceBookRule.GetPriceBookRuleDTL

    'lstItemGroupAvail.DataSource = ds.Tables(1)
    'lstItemGroupAvail.DataTextField = "vcItemGroup"
    'lstItemGroupAvail.DataValueField = "numItemGroupID"
    'lstItemGroupAvail.DataBind()

    'lstItemsGroupAdd.Items.Clear()
    'lstItemsAdd.Items.Clear()
    'lstCompany.Items.Clear()
    'lstRel.Items.Clear()
    'lstRelPro.Items.Clear()

    'If shortRuleAppType = 1 Then
    '    radSelType1.Checked = True
    '    lstItemsAdd.DataSource = ds.Tables(2)
    '    lstItemsAdd.DataTextField = "vcItemName"
    '    lstItemsAdd.DataValueField = "numPriceBookRuleDTLID"
    '    lstItemsAdd.DataBind()
    'ElseIf shortRuleAppType = 2 Then
    '    radSelType2.Checked = True
    '    lstItemsGroupAdd.DataSource = ds.Tables(2)
    '    lstItemsGroupAdd.DataTextField = "vcItemGroup"
    '    lstItemsGroupAdd.DataValueField = "numPriceBookRuleDTLID"
    '    lstItemsGroupAdd.DataBind()
    'ElseIf shortRuleAppType = 3 Then
    '    radSelType3.Checked = True
    'ElseIf shortRuleAppType = 4 Then
    '    radSelType4.Checked = True
    '    lstCompany.DataSource = ds.Tables(2)
    '    lstCompany.DataTextField = "vcCompanyName"
    '    lstCompany.DataValueField = "numPriceBookRuleDTLID"
    '    lstCompany.DataBind()
    'ElseIf shortRuleAppType = 5 Then
    '    radSelType5.Checked = True
    '    lstRel.DataSource = ds.Tables(2)
    '    lstRel.DataTextField = "vcData"
    '    lstRel.DataValueField = "numPriceBookRuleDTLID"
    '    lstRel.DataBind()
    'ElseIf shortRuleAppType = 6 Then
    '    radSelType6.Checked = True
    '    lstRelPro.DataSource = ds.Tables(2)
    '    lstRelPro.DataTextField = "Data"
    '    lstRelPro.DataValueField = "numPriceBookRuleDTLID"
    '    lstRelPro.DataBind()
    'ElseIf shortRuleAppType = 7 Then
    '    radSelType7.Checked = True
    'End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lngRuleId = Save()
            If lngRuleId > 0 Then
                If CCommon.ToLong(GetQueryStringVal("RuleId")) = 0 Then Response.Redirect("../Items/frmPriceBookMgmt.aspx?RuleId=" & lngRuleId, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            lngRuleId = Save()
            If lngRuleId > 0 Then
                Response.Redirect("../Items/frmPriceBookMgmtList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function Save() As Long
        Try
            objPriceBookRule = New PriceBookRule
            objPriceBookRule.RuleName = txtRuleName.Text
            objPriceBookRule.RuleDescription = txtRuleDesc.Text
            objPriceBookRule.RuleID = lngRuleId
            objPriceBookRule.IsRoundTo = chkRoundTo.Checked
            If chkRoundTo.Checked Then
                objPriceBookRule.RoundTo = CCommon.ToShort(ddlRoundTo.SelectedValue)
            End If
            objPriceBookRule.PricingMethod = IIf(rbPricingFormula.Checked, 2, IIf(rbPricingTable.Checked, 1, 0)) 'Pricetable or PricingFormula
            objPriceBookRule.RuleFor = CCommon.ToShort(hdnRuleFor.Value)

            If rbPricingFormula.Checked = True Then
                If txtQntyItems.Text = "0" Then
                    litMessage.Text = "Quantity of items purchased can not be 0"
                    Exit Function
                End If
                objPriceBookRule.RuleType = ddlRuleType.SelectedValue ' add or deduct 
                objPriceBookRule.VendorCostType = ddlVendorCostType.SelectedValue ' add or deduct 
                objPriceBookRule.DiscountType = ddlDiscountType.SelectedValue ' % or flat
                objPriceBookRule.DiscountAmount = IIf(txtDiscount.Text = "", 0, txtDiscount.Text)
                objPriceBookRule.QntyofItems = IIf(txtQntyItems.Text = "", 1, txtQntyItems.Text)
                objPriceBookRule.MaxDedPerAmt = IIf(txtMaxPerOrAmt.Text = "", 0, txtMaxPerOrAmt.Text)
            End If

            objPriceBookRule.Step2 = IIf(rbIndividualItem.Checked, 1, IIf(rbItemClassification.Checked, 2, IIf(rbAllItems.Checked, 3, 0)))
            objPriceBookRule.Step3 = IIf(rbIndividualOrg.Checked, 1, IIf(rbRelProfile.Checked, 2, IIf(rbAllOrg.Checked, 3, 0)))

            objPriceBookRule.DomainID = Session("DomainID")
          
            Return objPriceBookRule.ManagePriceBookRule()
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                Throw ex
            End If
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Items/frmPriceBookMgmtList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Private Sub btnAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItem.Click
    '    Try
    '        If radItem.Value <> "" Then
    '            objPriceBookRule = New PriceBookRule
    '            objPriceBookRule.RuleID = lngRuleId
    '            objPriceBookRule.RuleAppType = 1
    '            objPriceBookRule.RuleValue = radItem.Value
    '            objPriceBookRule.byteMode = 0
    '            objPriceBookRule.ManagePriceBookRuleDTL()
    '            LoadRuleAppDetails(1)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnRemItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemItem.Click
    '    Try
    '        If lstItemsAdd.Items.Count > 0 Then
    '            If lstItemsAdd.SelectedItem.Selected = True Then
    '                objPriceBookRule = New PriceBookRule
    '                objPriceBookRule.RuleDTLID = lstItemsAdd.SelectedValue
    '                objPriceBookRule.byteMode = 1
    '                objPriceBookRule.ManagePriceBookRuleDTL()
    '                LoadRuleAppDetails(1)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnAddItemGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemGroup.Click
    '    Try
    '        If lstItemGroupAvail.Items.Count > 0 Then
    '            objPriceBookRule = New PriceBookRule
    '            objPriceBookRule.RuleID = lngRuleId
    '            objPriceBookRule.RuleAppType = 2
    '            objPriceBookRule.RuleValue = lstItemGroupAvail.SelectedValue
    '            objPriceBookRule.byteMode = 0
    '            objPriceBookRule.ManagePriceBookRuleDTL()
    '            LoadRuleAppDetails(2)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnRemItemGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemItemGroup.Click
    '    Try
    '        If lstItemsGroupAdd.Items.Count > 0 Then
    '            If lstItemsGroupAdd.SelectedItem.Selected = True Then
    '                objPriceBookRule = New PriceBookRule
    '                objPriceBookRule.RuleDTLID = lstItemsGroupAdd.SelectedValue
    '                objPriceBookRule.byteMode = 1
    '                objPriceBookRule.ManagePriceBookRuleDTL()
    '                LoadRuleAppDetails(2)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnAddCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCompany.Click
    '    Try
    '        If radCmbCompany.Value <> "" Then
    '            objPriceBookRule = New PriceBookRule
    '            objPriceBookRule.RuleID = lngRuleId
    '            objPriceBookRule.RuleAppType = 4
    '            objPriceBookRule.RuleValue = radCmbCompany.Value
    '            objPriceBookRule.byteMode = 0
    '            objPriceBookRule.ManagePriceBookRuleDTL()
    '            LoadRuleAppDetails(4)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnRemCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemCompany.Click
    '    Try
    '        If lstCompany.Items.Count > 0 Then
    '            If lstCompany.SelectedItem.Selected = True Then
    '                objPriceBookRule = New PriceBookRule
    '                objPriceBookRule.RuleDTLID = lstCompany.SelectedValue
    '                objPriceBookRule.byteMode = 1
    '                objPriceBookRule.ManagePriceBookRuleDTL()
    '                LoadRuleAppDetails(4)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnAddRel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRel.Click
    '    Try
    '        If ddlRelationship.Items.Count > 0 Then
    '            objPriceBookRule = New PriceBookRule
    '            objPriceBookRule.RuleID = lngRuleId
    '            objPriceBookRule.RuleAppType = 5
    '            objPriceBookRule.RuleValue = ddlRelationship.SelectedValue
    '            objPriceBookRule.byteMode = 0
    '            objPriceBookRule.ManagePriceBookRuleDTL()
    '            LoadRuleAppDetails(5)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnDelRel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelRel.Click
    '    Try
    '        If lstRel.Items.Count > 0 Then
    '            If lstRel.SelectedItem.Selected = True Then
    '                objPriceBookRule = New PriceBookRule
    '                objPriceBookRule.RuleDTLID = lstRel.SelectedValue
    '                objPriceBookRule.byteMode = 1
    '                objPriceBookRule.ManagePriceBookRuleDTL()
    '                LoadRuleAppDetails(5)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnAddRelPro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRelPro.Click
    '    Try
    '        If ddlRelationship1.Items.Count > 0 And ddlProfile.Items.Count > 0 Then
    '            objPriceBookRule = New PriceBookRule
    '            objPriceBookRule.RuleID = lngRuleId
    '            objPriceBookRule.RuleAppType = 6
    '            objPriceBookRule.RuleValue = ddlRelationship1.SelectedValue
    '            objPriceBookRule.Profile = ddlProfile.SelectedValue
    '            objPriceBookRule.byteMode = 0
    '            objPriceBookRule.ManagePriceBookRuleDTL()
    '            LoadRuleAppDetails(6)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnDelRelPro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelRelPro.Click
    '    Try
    '        If lstRelPro.Items.Count > 0 Then
    '            If lstRel.SelectedItem.Selected = True Then
    '                objPriceBookRule = New PriceBookRule
    '                objPriceBookRule.RuleDTLID = lstRelPro.SelectedValue
    '                objPriceBookRule.byteMode = 1
    '                objPriceBookRule.ManagePriceBookRuleDTL()
    '                LoadRuleAppDetails(6)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Sub hindButton()
        trPricingFormula.Visible = IIf(hdnRuleFor.Value = "2", False, True)
        trPricingFormulaTable.Visible = IIf(hdnRuleFor.Value = "2", False, True)

        trItemClassification.Visible = IIf(hdnRuleFor.Value = "2", False, True)
        trAllItems.Visible = IIf(hdnRuleFor.Value = "2", False, True)

        trRelProfile.Visible = IIf(hdnRuleFor.Value = "2", False, True)
        trAllOrg.Visible = IIf(hdnRuleFor.Value = "2", False, True)
    End Sub

End Class