﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmNewItem.aspx.vb" Inherits=".frmNewItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Item</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.js"></script>
    <script type="text/javascript" src="../JavaScript/additional-methods.min.js"></script>
    <script type="text/javascript" src="../JavaScript/CustomFieldValidation.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
           $('form').find('input[type=text],textarea,select').filter(':visible:first').focus()
        });
        function Close() {
            window.close()
            return false;
        }
        function Save() {
            validateCustomFields();
            return $("#form1").valid();
        }
        function SingleSelect(current) {
            if (current.id == '212Is Serialized') {
                document.getElementById('216Is Lot No').checked = false;
            }
            else if (current.id == '216Is Lot No') {
                document.getElementById('212Is Serialized').checked = false;
            }
        }
        function SendToParentPage(itemCode, itemName) {
            if (window.opener != null) {
                window.opener.SelectNewAddedItem(itemCode, itemName);
            }

            window.close();

            return false;
        }
    </script>
    <style type="text/css">
        label.valid {
            width: 24px;
            height: 24px;
            background: url(assets/img/valid.png) center center no-repeat;
            display: inline-block;
            text-indent: -9999px;
        }

        label.error {
            font-weight: bold;
            color: red;
            padding: 2px 8px;
            margin-top: 2px;
        }

         #tblMain td:nth-child(1), td:nth-child(3) {
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div>
            <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:LinkButton ID="btnSaveNew" runat="server" style="text-decoration:none;" CssClass="btn btn-primary" OnClick="btnSaveNew_Click">Save &amp; New</asp:LinkButton>
                        <asp:LinkButton ID="btnSave" runat="server" style="text-decoration:none;" CssClass="btn btn-primary">Save &amp; Close</asp:LinkButton>
                        <asp:LinkButton ID="btnClose" runat="server" style="text-decoration:none;" CssClass="btn btn-primary">Close</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal4" align="center">
                                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    New
    <asp:Label ID="LabelItemType" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:HiddenField ID="HiddenFieldFormID" runat="server" />
    <asp:HiddenField ID="HiddenFieldItemType" runat="server" />
    <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="800px"
        CssClass="aspTable" BorderColor="black" GridLines="None" BorderWidth="1" runat="server">
    </asp:Table>
</asp:Content>
