﻿<%--<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmInventoryAdjustment.aspx.vb" Inherits=".frmInventoryAdjustment" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Inventory Adjustment</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript">

        function UpdateAdjustmentValue() {
            if (Number($('#txtAdjustedQty').val()) + Number($('#lblOnHandQty').text()) < 0) {
                alert('Can not accept value, it creates negative inventory.');
                $('#txtAdjustedQty').val('');
                $('#lblAdjustValue').text('');
                return false;
            }

            if (Number($('#txtAdjustedQty').val()) < 0) {
                if ((Number($('#txtAdjustedQty').val()) * -1) > Number($('#lblOnHandQty').text())) {
                    alert('Adjusted Qty can not be lesser than on hand qty');
                    $('#txtAdjustedQty').val('');
                    $('#lblAdjustValue').text('');
                    return false;
                }
            }

            var AdjustValue = roundNumber(Number($('#txtAdjustedQty').val()) * Number($('#txtUnitCost').val()), 4);
            $('#lblAdjustValue').text(' Adjustment value: ' + AdjustValue.toString());

        }
        function Save() {
            if ($('#ddlAccount').val() == 0) {
                alert('Please select adjustment account');
                return false;
            }
            if ($('#ddlWarehouse').val() == 0) {
                alert('Please select warehouse');
                return false;
            }
            if ($('#txtAdjustedQty').val() == "" && $("#radQty").is(":checked")) {
                alert('Please enter adjustment quantity');
                return false;
            }
            if ($('#txtDate').val() == "") {
                alert('Please enter adjustment date');
                return false;
            }
            return true;
        }
        function roundNumber(num, dec) {
            var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
            return result;
        }

        $(document).ready(function () {

            if ($("#radQty").is(":checked")) {
                $(".trQty").show();
            }
            else {
                $(".trQty").hide();
            }

            $("#radQty,#radCost").on("click",function () {

                if ($("#radQty").is(":checked")) {
                    $(".trQty").show();
                }
                else {
                    $(".trQty").hide();
                }
            })

        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Make Adjustment"
                OnClientClick="return Save();"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Inventory Adjustment
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="1" cellspacing="1" width="800px">
        <tr>
            <td align="right" class="normal1" width="250px">Select account impacted by adjustment:<font color="red">*</font>
            </td>
            <td align="left">
                <asp:DropDownList runat="server" ID="ddlAccount" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">Warehouse:<font color="red">*</font>
            </td>
            <td align="left">
                <asp:DropDownList runat="server" ID="ddlWarehouse" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">On Hand Qty:
            </td>
            <td align="left" class="normal1">
                <asp:Label Text="" runat="server" ID="lblOnHandQty" Font-Bold="true" />
                &nbsp;&nbsp;&nbsp;&nbsp; Current Inventory Value
                <asp:Label Text="" runat="server" ID="lblCurrentInvValue" Font-Bold="true" CssClass="normal1" />
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <fieldset>
                    <legend>Adjust
                        <asp:RadioButton ID="radQty" Text="Qty and Average Cost" runat="server" GroupName="Adjust" Checked="true" CssClass="tip" ToolTip="Allows you to change quantity on hand as well as avg cost per unit."  />
                        <asp:RadioButton ID="radCost" Text="Only Average Cost" runat="server" GroupName="Adjust"  CssClass="tip" ToolTip="Allows you to change total asset value & Avg. cost, without any change in quantity." />
                    </legend>
                    <table border="0" cellpadding="1" cellspacing="1" width="800px">
                        <tr class="trQty">
                            <td align="right" class="normal1" width="240px">Adjust Qty By:<font color="red">*</font>
                            </td>
                            <td align="left" class="normal1">
                                <asp:TextBox runat="server" ID="txtAdjustedQty" CssClass="signup" Width="40px" onkeypress="CheckNumber(2,event);"
                                    onkeyup="UpdateAdjustmentValue();" /><asp:Label ID="Label1" Text="[?]" CssClass="tip"
                                        runat="server" ToolTip="Use negative number for deduction. i.e  -5 will deduct five units from On Hand." />
                                &nbsp;&nbsp;<asp:Label Text="" runat="server" ID="lblAdjustValue" CssClass="normal1" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="normal1" width="240px">Estimated Unit Cost:
                            </td>
                            <td align="left" class="normal1">
                                <asp:TextBox runat="server" ID="txtUnitCost" CssClass="signup" Width="80px" onkeypress="CheckNumber(1,event)"
                                    onkeyup="UpdateAdjustmentValue();" />
                                <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="enter new unit cost of item,If you do not want to enter new unit cost then set it to 0.0. in either case biz will automatically update average cost of item based what you enter as new unit cost." />
                            </td>
                        </tr>
                    </table>

                </fieldset>
            </td>

        </tr>


        <tr>
            <td align="right" class="normal1">Memo:
            </td>
            <td align="left" class="normal1">
                <asp:TextBox runat="server" ID="txtMemo" CssClass="signup" Width="200px" Text="Stock Adjustment:" />
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">Adjustment Date:
            </td>
            <td align="left" class="normal1">
                <BizCalendar:Calendar ID="calAdjustmentDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">Project:
            </td>
            <td align="left" class="normal1">
                <asp:DropDownList runat="server" ID="ddlProject" CssClass="signup">
                </asp:DropDownList>
                &nbsp;&nbsp;<asp:Label Text="[?]" CssClass="tip" runat="server" ToolTip="Selecting project will be tacked in report of P&L by Project" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnAverageCost" runat="server" />
</asp:Content>--%>
