<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmItemNavigation.aspx.vb"
    Inherits="BACRM.UserInterface.Outlook.frmItemNavigation" ClientIDMode="Static" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table>
        <tr>
            <td valign="top" align="left">
                <asp:ScriptManager runat="server" ID="scManager" />
                <table>
                    <tr>
                        <td valign="top" align="left">
                            <telerik:RadTreeView ID="RadItemNavigation" runat="server" AllowNodeEditing="false"
                                ClientIDMode="Static">
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
