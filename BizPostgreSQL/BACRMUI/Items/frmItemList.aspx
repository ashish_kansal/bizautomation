<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmItemList.aspx.vb"
    Inherits="BACRM.UserInterface.Items.frmItemList" MasterPageFile="~/common/GridMaster.Master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Item</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link href="../images/BizSkin/RadComboBox.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/x.js"></script>
    <script src="../Scripts/xtableheaderfixed.js"></script>
    <script src="../JavaScript/colResizable-1.6.min.js"></script>
    <style>
        .alert-succss {
            background: #177219;
            color: #fff;
            padding: 5px
        }

        #resizeBox {
            /*display: inline-block;
            height: 500px;*/
            overflow-x: scroll;
            overflow-y: hidden;
        }

        .labelWIdth {
            width: 100px;
            display: inline-block;
        }

        .divLeft {
            float: left;
        }

        .rgHeader {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
        }*/

        .InVisibleControl {
            display: none
        }
        
        .fixed-header
        {
             position: fixed;
            top:85px; 
            left: 75px;
            overflow-x:hidden;
        }
    </style>
    <script type="text/javascript">
        

        var popUpObj;
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function CheckNumberAllowPlus(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!((k > 47 && k < 58) || k == 43 || k == 44 || k == 45 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!((k > 47 && k < 58) || k == 43 || k == 45 || k == 8 || k == 37 || k == 39 || k == 16 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function OpenNewSalesOrder(type) {
            var target;

            if (parent.parent.Popup == null || parent.parent.Popup == 'undefined') {
                target = '../opportunity/frmNewOrder.aspx';
                target = target + '?rtyWR=&uihTR=&tyrCV=&pluYR=&fghTY=';
            }
            else {
                popUpObj = parent.parent.Popup;
            }


            if (type == 'add') {
                if (popUpObj == null) {
                    return false;
                }
                else {
                    if (popUpObj.closed == true) {
                        return false;
                    }

                    GetItemCodes();

                    if ($('[id$=hdnItemCodes]').val() == '') {
                        return false;
                    }

                    $('[id$=btnReload]').click();
                }
            }
            else {
                if (popUpObj == null) {
                    return false;
                }
                else {
                    if (popUpObj.closed == true) {
                        return false;
                    }
                    else {
                        popUpObj = window.open('', "popNewOrder", 'toolbar=no,titlebar=no,left=150, top=150,width=' + a + ',height=' + b + ',scrollbars=yes,resizable=yes');
                        popUpObj.focus();
                    }
                }

                return false;
            }
        }

        function BindOrderGrid() {
            var target;

            if (parent.parent.Popup == null) {
                target = '../opportunity/frmNewOrder.aspx';
                target = target + '?rtyWR=&uihTR=&tyrCV=&pluYR=&fghTY=';
                popUpObj = window.open(target, "popNewOrder");
            }
            else {
                popUpObj = parent.parent.Popup;
            }

            try {
                popUpObj.reloadPage('add');
            }
            catch (err) {
                alert("Script error : " + err.message);
                return false;
            }
        }

        function OpenNewAsset(a, b, c) {

            if (b == 1) {
                document.location.href = "../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Asset&AssetCode=" + a;
            }
            else {
                window.open("../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=frmAccounts&AssetCode=" + a + "&numDivisionID=" + c);
            }
            return false;
        }

        function GoAdminLinks() {
            document.location.href = '../admin/frmAdminSection.aspx'
            return false;
        }

        function OpenAddItem() {
            window.open("../Items/frmAddItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767", '', 'toolbar=no,titlebar=no,left=300,top=200,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function reDirectPage(a) {
            document.location = a;
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=21', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OpenFilterSetting() {
            window.open('../Items/frmItemFilterList.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OpenItem(a, b) {
            document.location = '../Items/frmKitDetails.aspx?ItemCode=' + a + '&frm=' + b;
        }

        function DeleteRecord() {
            var ContactId = '';
            $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                if (this.checked) {

                    if (this.id.indexOf("chkSelectAll") == -1) {
                        var lblItem = this.id.replace('chkSelect', 'lbl1');
                        ContactId = ContactId + ',' + document.getElementById(lblItem).innerHTML;
                    }
                }
            });

            if (ContactId == '') {
                alert("Please select atleast one item to delete.");
                return false;
            }
            else {
                document.getElementById('txtDelItemIds').value = ContactId;

                if (confirm('Are you sure, you want to delete the selected record?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function UpdateAverageCostForLineItem(CurrentRow) {

            var AdjQty = (Number($(CurrentRow).find("[id$=txtCounted]").val()) - Number($(CurrentRow).find("[id$=hdnAvailability]").val())).toPrecision(8);

            if (AdjQty != 0) {
                if (AdjQty > 0) { 
                    $(CurrentRow).find("[id$=txtAdjQty]").val("+" + AdjQty.toString()).css("color", "green"); 
                } else { 
                    $(CurrentRow).find("[id$=txtAdjQty]").val(AdjQty.toString()).css("color", "red"); 
                }
            }
            var NewInventoryValue = (Number($(CurrentRow).find("[id$=txtCounted]").val()) * Number($(CurrentRow).find("[id$=hdnInventoryAverageCost]").val())).toPrecision(8);
            //$(CurrentRow).find("[id$=lblNewInventoryValue]").text(parseFloat(NewInventoryValue).toFixed(2));

        }
        function pageLoaded() {
            var flag = 0;
            var IsDisableSaveInventoryOption = false;
            var IsColumnNotAdded = false;
            try {
                if ($("[id$=txtCounted]").length == 0) {
                        $("#btnSaveInventory").css("display", "none");
                    } else {
                    $("#btnSaveInventory").css("display", "inline");
                }
                $("[id$=txtCounted]").each(function () {
                    var CurrentRow = $(this).parents("tr").filter(':first')
                    
                    var itemType = $(CurrentRow).find("[id$=hdnItemType]").val();
                    var IsWareHouseItemId = $(CurrentRow).find("[id$=hdnWareHouseItemId]").val();
                    if (itemType != "P" || IsWareHouseItemId == "0") {
                        $(CurrentRow).find("[id$=txtCounted]").attr("disabled", "disabled");
                        $(CurrentRow).find("[id$=txtAdjQty]").attr("disabled", "disabled");
                        $(CurrentRow).find("[id$=txtUnitCost]").attr("disabled", "disabled");
                    }
                })

                var PageName ='<%= GetQueryStringVal("Page")%>';
                if (PageName == "Services" || PageName == "Non-Inventory Items") {
                    $("#sectionFilteredInternalLocation").css("display", "none");
                } else {
                    $("#sectionFilteredInternalLocation").css("display", "inline");
                    
                }
            } catch{

            }

            $("[id$=txtAdjQty]").keypress(function () {
                //var CurrentRow = $(this).parents("tr").filter(':first');
                //var chk = CurrentRow.find("[id$=chk]");
                //if (chk != null) {
                //    chk.prop("checked", true);
                //}


                if (CheckNumberAllowPlus(2, event)) {
                    flag = 1;
                }

            });

            $("[id$=txtAdjQty]").focusout(function () {
                if (flag = 1) {
                    var CurrentRow = $(this).parents("tr").filter(':first');
                    var CurrentOnHand = Number($(CurrentRow).find("[id$=hdnAvailability]").val());
                    CurrentOnHand = (CurrentOnHand + Number($(CurrentRow).find("[id$=txtAdjQty]").val())).toPrecision(8);
                    if (CurrentOnHand < 0) {
                        alert('Adjustment can not be less than current on hand qty!');
                        $(CurrentRow).find("[id$=txtAdjQty]").val('');
                        return;
                    }
                    if (!isNaN(CurrentOnHand)) {
                        $(CurrentRow).find("[id$=txtCounted]").val(CurrentOnHand.toString());
                        UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
                    }
                }
                var index = $('.txtClassQtyAdjust').index(this) + 1;
                $(".txtClassQtyCount:eq(" + index + ")").focus();
            });
            $("[id$=txtCounted]").focusout(function () {
                 if (flag = 1) {
                        var CurrentRow = $(this).parents("tr").filter(':first');
                        var CurrentOnHand = Number($(CurrentRow).find("[id$=hdnAvailability]").val());
                        CurrentOnHand = (CurrentOnHand + Number($(CurrentRow).find("[id$=txtAdjQty]").val())).toPrecision(8);
                        if (CurrentOnHand < 0) {
                            alert('Adjustment can not be less than current on hand qty!');
                            $(CurrentRow).find("[id$=txtAdjQty]").val('');
                            return;
                        }
                        if (!isNaN(CurrentOnHand)) {
                            $(CurrentRow).find("[id$=txtCounted]").val(CurrentOnHand.toString());
                            UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
                        }
                }
            });
            $("[id$=txtCounted]").keydown(function (e) {
                var keyCode = e.keyCode || e.which;

                
                if (keyCode == 9 || keyCode == 13) {
                   
                    e.preventDefault();
                    var index = $('.txtClassQtyCount').index(this) + 1;
                    $(".txtClassQtyCount:eq(" + index + ")").focus();
                    $(".txtClassQtyCount:eq(" + index + ")").select();
                }
            });

            $("[id$=txtCounted]").change(function () {
                UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
            });
            $('body').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    $("#divDropDowns #btnGo").trigger('click');
                    return false;
                }

            });
            if (parent.parent.Popup == null || parent.parent.Popup == 'undefined') {
                $('[id$=imgCartAdd]').hide();
                $('[id$=btnOpenOrder]').hide();
                $('[id$=divWarehouse]').hide();
            }
            else {
                if (parent.parent.Popup.closed == true) {
                    $('[id$=imgCartAdd]').hide();
                    $('[id$=btnOpenOrder]').hide();
                    $('[id$=divWarehouse]').hide();
                }
                else {
                    $('[id$=imgCartAdd]').show();
                    $('[id$=btnOpenOrder]').show();
                    $('[id$=divWarehouse]').show();
                }
            }

            $('[id$=hdnOppType]').val(parseInt(parent.parent.sOppType));
            $('[id$=hplClearGridCondition]').unbind('click');
            $("[id$=hplClearGridCondition]").click(function () {

                $('[id$=txtGridColumnFilter]').val("");

                $('[id$=divDropDowns]').find('.RadComboBox').each(function () {
                    var Combo = $find($(this).attr('id'));
                    Combo.clearSelection();

                    var items = Combo.get_items();
                    for (var x = 0; x < Combo.get_items().get_count(); x++) {
                        items._array[x].uncheck();
                    }
                });

                $('[id$=btnGo1]').trigger('click');
            });

            $('.RadComboBox').click(function () {
                ShowDropDownFunction($(this).attr("id"));
            });

            //Export Checkbox
            var chkBox = $("[id$=chkAllAPI]");
            $(chkBox).click(function () {
                $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                    if (this.id.indexOf("chkSelectAPI") >= 0) {
                        $(this).prop('checked', chkBox.is(':checked'))
                    }
                })
            });

            //Export Checkbox
            var chkBox1 = $("[id$=chkSelectAll]");
            $(chkBox1).click(function () {
                $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                    if (this.id.indexOf("chkSelect") >= 0 && this.id.indexOf("chkSelectAPI") == -1) {
                        //                        console.log($(this))
                        $(this).prop('checked', chkBox1.is(':checked'))
                    }
                })
            });


            //$("#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00 colgroup col:not([style])").css("width", "25px");
            //SetColumnWithToAuto();
            $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00").colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip2'></div>",
                draggingClass: "dragging",
                minWidth: 30,
                resizeMode:"overflow",
                onResize: onItemGridColumnResized
            });

            $(".rgMasterTable > thead").outerWidth($("#resizeBox").outerWidth());

            var stickyOffset = $('.rgMasterTable > thead').offset().top;

            $(window).scroll(function(){
                var sticky = $('.rgMasterTable > thead'),
                    scroll = $(window).scrollTop();
    
                if (scroll >= stickyOffset) {
                    sticky.addClass('fixed-header');

                    $('.rgMasterTable > thead > tr > th').each(function(index,element){
                        var thWidth = $(element).outerWidth();
                        var tdWidth = $('.rgMasterTable > tbody > tr:eq(0) > td:eq(' + index + ')').outerWidth();

                        if(thWidth > tdWidth)
                        {
                            $('.rgMasterTable > tbody > tr:eq(0) > td:eq(' + index + ')').each(function(j,td){
                                $(td).css("min-width",thWidth.toString() + "px");
                            });
                        } 
                        else
                        {
                            $(element).css("min-width",tdWidth.toString() + "px");
                        }
                    });
                } 
                else { 
                    sticky.removeClass('fixed-header');

                    $('.rgMasterTable > thead > tr > th').each(function(index,element){
                        $(element).css("min-width","");
                    });

                    $('.rgMasterTable > tbody > tr > td').each(function(index,element){
                        $(element).css("min-width","");
                    });
                }
            });

            $("#resizeBox").scroll(function(){
                $(".rgMasterTable > thead").scrollLeft($("#resizeBox").scrollLeft());
            });

            if($("#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00 > colgroup > col").length > $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00 > thead > tr > th").length){
                $( "#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00 > colgroup > col" ).each(function(){
                    if($(this).attr("style") == null){
                        $(this).remove();
                    }
                });
            }
        }

        var onItemGridColumnResized = function (e) {
            $("th.rgExpandCol").attr("id", "rgExpandColItemList");
            var columns = $(e.currentTarget).find("th");
            var msg = "";

            columns.each(function () {
                var thId = $(this).attr("id");
                var thWidth = $(this).outerWidth();
                msg += (thId || "anchor") + ':' + thWidth + ';';
            }
            )

            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/SaveGridColumnWidth",
                data: "{str:'" + msg + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
        };

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function SetColumnWithToAuto() {
            $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00 colgroup col").each(function () {
                if ($(this).width() == 0) {
                    $(this).attr("style", "width:auto;");
                }
            });
        }

        function BarcodeRecord() {
            var ItemListId = '';
            $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                if (this.checked) {
                    if (this.id.indexOf("chkSelectAll") == -1 && this.id.indexOf("chkAllAPI") == -1 && this.id.indexOf("chkSelectAPI") == -1) {
                        var lblItem = this.id.replace('chkSelect', 'lbl1');
                        ItemListId = ItemListId + ',' + document.getElementById(lblItem).innerHTML;
                    }
                }
            });

            if (ItemListId == '') {
                alert("Please select atleast one item to generate Barcode.");
                return false;
            }
            else {
                window.open('../Items/frmItemBarCode.aspx?ItemList=' + ItemListId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
                return false;
            }
        }

        function ShowDropDownFunction(dropDownName) {
            setTimeout(function () {
                var combo = $find(dropDownName);
                combo.showDropDown();
            }, 1000);
        }

        function HideDropDownFunction(dropDownName) {
            var combo = $find(dropDownName);
            combo.hideDropDown();
        }

        function getFilterExpressions() {
            var strValues;
            strValues = '';
            $('[id$=divDropDowns]').find('.RadComboBox').each(function () {
                var combo = $find($(this).attr('id'));
                var iCnt = 0;

                strValues = strValues + '~' + $(this).attr('id') + ':';
                for (iCnt = 0; iCnt <= combo.get_checkedItems().length - 1; iCnt++) {
                    strValues = strValues + combo.get_checkedItems()[iCnt].get_value() + ',';
                }

                $('[id$=hdnData]').val(strValues);
            });
        }

        function OnClientItemChecked(sender, eventArgs) {
            var comboName = $(sender).attr("id");
            var item = eventArgs.get_item();
            sender.set_text("You checked " + item.get_text());
        }

        function CheckAll(id, chkAll) {
            var Combo = $find($("[id$=" + id + "]").attr("id"));
            var items = Combo.get_items();

            for (var x = 0; x < Combo.get_items().get_count(); x++) {
                if ($(chkAll).is(':checked'))
                    items._array[x].check();
                else
                    items._array[x].uncheck();
            }
        }

        function GetItemCodes() {
            var ItemListId = '';
            $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                if (this.checked) {
                    if (this.id.indexOf("chkSelectAll") == -1 && this.id.indexOf("chkAllAPI") == -1 && this.id.indexOf("chkSelectAPI") == -1) {
                        var lblItem = this.id.replace('chkSelect', 'lbl1');
                        ItemListId = ItemListId + ',' + document.getElementById(lblItem).innerHTML + ':' + $(this).parent('td').parent('tr').find('.pricelevel').val();
                    }
                }
            });

            if (ItemListId == '') {
                alert("Please select atleast one item to add it to order.");
                $('[id$=hdnItemCodes]').val('');
            }
            else {
                $('[id$=hdnItemCodes]').val(ItemListId);
            }
        }

        function OnClientBlur(sender, args) {
            sender.hideDropDown();
        }

        function OrderSearchClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnGotoRecord]").click();
        }

        function HandleItemListEnterKeyPress(sender, eventArgs) {
            if (eventArgs.get_domEvent().keyCode == 13) {
                $("#divDropDowns #btnGo").click();
            }
        }
        function OpenHelp() {
            window.open('../Help/Items.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function ValidateMultiSelectAdd() {
            var ItemId = '';
            $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                if (this.checked) {

                    if (this.id.indexOf("chkSelectAll") == -1) {
                        var lblItem = this.id.replace('chkSelect', 'lbl1');
                        ItemId = ItemId + ',' + document.getElementById(lblItem).innerHTML;
                    }
                }
            });

            if (ItemId == '') {
                alert("Please select atleast one item to Add.");
                return false;
            }
            else {
                $('[id$=hdnMultiSelectItemcodes]').val(ItemId);
                $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                    this.checked = false;
                });
            }
        }

        function OpenMultiSelectPopup(CustId) {
            window.opener.external.comeback();
            window.opener.focus();
            window.close();
            window.opener.document.getElementById('btn2').click().focus();

        }

        function CLoseMe() {
            var win = window.open(window.location.href, "_self");
            win.close();
        }

        function showAttrDiv() {
            $("#divItemAttributes").show();
            return false;
        }

        function HideAttrDiv() {
            $("#divItemAttributes").hide();
            return false;
        }

        function OpenInventoryReport() {
            var ItemId = '';
            $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                if (this.checked) {
                    var lblItem = this.id.replace('chkSelect', 'lbl1');
                    ItemId = ItemId + ',' + document.getElementById(lblItem).innerHTML;
                }
            });

            if (ItemId == '') {
                alert("Please select atleast one item to Add.");
                return false;
            }
            else {
                var w = screen.width;
                var h = screen.height;
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2);
                window.open("../Items/frmInventoryAudit.aspx?IsFromItemGrid=1&WareHouseItemID=" + ($("[id$=ddlFilterWarehouse]").val() || 0) +"&ItemIDs=" + ItemId, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
            }
            
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="divSuccessMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-succss">
                <h4 style="float: left; padding-right: 10px;">
                 <i class="fa fa-check-circle"></i></h4>
                <asp:Literal ID="litSuccessmessage" runat="server" Text="Adjustment posted successfully"></asp:Literal>
                <div class="pull-right">
                    <asp:Literal ID="litSummaryMessage" runat="server" Text="Summary:   +44 units, with +22.22 Asset Value"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div runat="server" id="divDropDowns" class="componentpanelNew row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:Button runat="server" ID="btnFilter" Text="Filter" CssClass="btn btn-primary" OnClientClick="getFilterExpressions();" />
                <asp:PlaceHolder ID="tdMain" runat="server"></asp:PlaceHolder>
            </div>
            <div class="pull-left">
                <div class="form-inline" style="display: inline">
                    <label>Include Archived Items:</label>
                    <asp:CheckBox Style="vertical-align: middle" runat="server" AutoPostBack="true" ID="chkArchivedItems" />
                    <telerik:RadComboBox ID="radCmbSearch" runat="server" DropDownWidth="700px" Width="200"
                        Skin="Default" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                        ItemsPerRequest="10" EnableVirtualScrolling="true" ItemRequestTimeout="1000" DropDownCssClass="ItemListSlide"
                        OnItemsRequested="radCmbSearch_ItemsRequested" HighlightTemplatedItems="true" ClientIDMode="Static" ZIndex="999"
                        TabIndex="503" MaxHeight="230" OnClientSelectedIndexChanged="OrderSearchClientSelectedIndexChanged" OnClientKeyPressing="HandleItemListEnterKeyPress">
                        <HeaderTemplate>
                            <table style="width: 500px">
                                <tr>
                                    <th style="width: 100px; text-align: center; font-weight: bold">Item ID
                                    </th>
                                    <th style="width: 250px; text-align: center; font-weight: bold">Name
                                    </th>
                                    <th style="width: 150px; text-align: center; font-weight: bold">Model ID
                                    </th>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table style="width: 500px">
                                <tr>
                                    <td style="width: 100px">
                                        <%# DataBinder.Eval(Container, "Value")%>
                                    </td>
                                    <td style="width: 250px;">
                                        <%# DataBinder.Eval(Container, "Text")%>
                                    </td>
                                    <td style="width: 150px">
                                        <%# DataBinder.Eval(Container, "Attributes['vcModelID']")%>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </telerik:RadComboBox>
                    <asp:Button ID="btnGo" CssClass="btn btn-primary" Text="Go" runat="server"></asp:Button>
                    <asp:Button ID="btnGotoRecord" runat="server" Text="Go to Record" Style="display: none" />
                </div>
                <div class="btn-group">
                    <asp:LinkButton ID="btnBarcode" runat="server" CssClass="btn btn-default" ToolTip="Barcode"><i class="fa fa-barcode"></i></asp:LinkButton>
                    <a href="#" class="btn btn-default" onclick="OpenFilterSetting()" title="Filter Settings"><i class="fa fa-gear"></i></a>
                    <%--<asp:LinkButton runat="server" ID="btnExport" Text="Export" CssClass="btn btn-default" Visible="true"><i class="fa fa-file-excel-o"></i></asp:LinkButton>--%>
                </div>
                <asp:Button runat="server" ID="btnNew" Text="New" Style="display: none" CssClass="btn btn-primary" />
                <div class="form-inline" id="divWarehouse" style="display: none">
                    <label>Warehouse</label>
                    <asp:DropDownList ID="ddlItemWarehouse" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                    </asp:DropDownList>
                </div>
                <asp:ImageButton ID="imgCartAdd" runat="server" CssClass="imageAlign" Style="display: none; vertical-align: top" Height="25" ImageUrl="~/images/cart_add.png" AlternateText="Add To Cart" ToolTip="Add To Cart"></asp:ImageButton>
                <asp:Button ID="btnOpenOrder" CssClass="button" Text="Open Order" runat="server" UseSubmitBehavior="false" Style="display: none"></asp:Button>

            </div>

            <div class="pull-right">
                <asp:LinkButton runat="server" ID="btnExport" Text="Export"  Visible="False"><img alt="" src="../images/Excel.png" width="30px" height="30px"/></asp:LinkButton>
                <button id="btnAddNewItem" onclick="return OpenPopUp('../Items/frmAddItem.aspx');" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                <asp:HyperLink ID="hplAdjustInv" NavigateUrl="frmInventoryAdjustmentInBatch.aspx" runat="server" Visible="true" CssClass="btn btn-primary"><i class="fa fa-wrench"></i>&nbsp;&nbsp;Manage Inventory</asp:HyperLink>
                <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger" runat="server" OnClientClick="javascript:return DeleteRecord();" OnClick="btnDelete_Click"><i class="fa fa-trash" ></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblList" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frmitemlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="MultiSelectContent" ContentPlaceHolderID="MultiSelectFunctionCluster" runat="server">
    <table id="tblMultiSelect" runat="server" visible="false">
        <tr>
            <td>
                <asp:Button ID="btnMultiSelectAdd" OnClientClick="javascript:return ValidateMultiSelectAdd();" OnClick="btnMultiSelectAdd_Click" CssClass="btn btn-primary float-right" Style="color: white" Text="Add" runat="server"></asp:Button>&nbsp;
            </td>
            <td>
                <asp:Button ID="btnMultiSelectAddFinish" OnClientClick="javascript:return ValidateMultiSelectAdd();" OnClick="btnMultiSelectAddFinish_Click" CssClass="btn btn-primary float-right" Style="color: white" Text="Add / Finish" runat="server"></asp:Button>
            </td>
            <td>
                <asp:ImageButton ID="imgbtnShoppingCart" OnClick="imgbtnShoppingCart_Click" runat="server" class="float-right" ImageUrl="~/images/icons/cart_large.png" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
    <div class="col-xs-12">
    <div class="pull-right" id="sectionFilteredInternalLocation" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="form-inline">
            <a href="javascript:OpenInventoryReport();" class="btn btn-primary">Inventory Audit Report</a>
            <label>Warehouse</label>
            <asp:DropDownList ID="ddlFilterWarehouse" runat="server" Width="100" AutoPostBack="true" CssClass="form-control">
                <%--<asp:ListItem Value="0">-Select-</asp:ListItem>--%>
                <asp:ListItem Value="0">All</asp:ListItem>
            </asp:DropDownList>

            <label>Aisle</label>
            <asp:DropDownList ID="ddlAisle" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                <asp:ListItem Value="">-Select-</asp:ListItem>
            </asp:DropDownList>

            <label>Rack</label>
            <asp:DropDownList ID="ddlRack" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                <asp:ListItem Value="">-Select-</asp:ListItem>
            </asp:DropDownList>

            <label>Shelf</label>
            <asp:DropDownList ID="ddlShelf" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                <asp:ListItem Value="">-Select-</asp:ListItem>
            </asp:DropDownList>

            <label>Bin</label>
            <asp:DropDownList ID="ddlBin" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                <asp:ListItem Value="">-Select-</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdnAppliedByFilter" Value="0" runat="server" />
            <asp:Button ID="btnFilterByWarehouse" runat="server" Text="Go" CssClass="btn btn-primary" />&nbsp;&nbsp;
            <asp:Button ID="btnSaveInventory" runat="server" Text="Save Inventory" CssClass="btn btn-primary" />
        </div>
    </div>
     </div>
    </div>
    <div id="resizeBox">
        <telerik:RadGrid ID="gvSearch" OnDataBound="gvSearch_DataBound" runat="server" Width="100%" AutoGenerateColumns="False" ShowFooter="false" EnableEmbeddedSkins="false" AllowMultiRowSelection="False">
            <MasterTableView DataKeyNames="numItemCode,charItemType" ExpandCollapseColumn-Visible="false" HierarchyLoadMode="ServerOnDemand" CssClass="table table-bordered table-striped" Width="100%">
                <DetailTables>
                    <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                        ShowFooter="false" DataKeyNames="numItemCode" CssClass="table table-bordered table-striped">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Location" DataField="vcWareHouse1">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="OnHand">
                                <ItemTemplate>
                                    <asp:Label ID="lblOnHand1" runat="server" Text=' <%# Eval("OnHand1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblOnHandTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Buy OnHand">
                                <ItemTemplate>
                                    <asp:Label ID="lblPurchaseOnHand1" runat="server" Text=' <%# Eval("PurchaseOnHand1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblPurchaseOnHandTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Sell OnHand">
                                <ItemTemplate>
                                    <asp:Label ID="lblSalesOnHand1" runat="server" Text=' <%# Eval("SalesOnHand1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblSalesOnHandTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="OnOrder">
                                <ItemTemplate>
                                    <asp:Label ID="lblOnOrder1" runat="server" Text=' <%# Eval("OnOrder1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblOnOrderTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Reorder">
                                <ItemTemplate>
                                    <asp:Label ID="lblReorder1" runat="server" Text=' <%# Eval("Reorder1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblReorderTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Allocation">
                                <ItemTemplate>
                                    <asp:Label ID="lblAllocation1" runat="server" Text=' <%# Eval("Allocation1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblAllocationTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="BackOrder">
                                <ItemTemplate>
                                    <asp:Label ID="lblBackOrder1" runat="server" Text=' <%# Eval("BackOrder1", "{0:#,##0}")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblBackOrderTotal" runat="server"></asp:Label>
                                </FooterTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="List Price (I)" DataField="Price1">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="SKU (M)" DataField="SKU1">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="UPC (M)" DataField="BarCode1">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>
                <Columns>
                </Columns>
            </MasterTableView>

        </telerik:RadGrid>
    </div>

    <div class="overlay" id="divItemAttributes" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 400px; border: 1px solid #ddd;">
            <div class="dialog-header">
                <b style="font-size: 14px;">Select Item Attributes?</b>
                <div style="float: right; padding: 7px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSaveCloseAttributes" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" />
                            </td>
                            <td>
                                <asp:Button ID="btnCloseAttr" OnClientClick="javascript: return HideAttrDiv();" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close" />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="dialog-body">
                <div style="text-align: center">
                    <asp:Label ID="lblAttributeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
                <asp:Repeater ID="rptAttributes" runat="server">
                    <HeaderTemplate>
                        <table style="width: 100%; padding: 0px; border-spacing: 10px;">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="min-height: 30px;">
                            <td style="white-space: nowrap; text-align: right">
                                <asp:Label ID="lblAttributeName" runat="server" Text='<%# Eval("Fld_label")%>' Font-Bold="true"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnFldID" runat="server" Value='<%# Eval("Fld_id")%>' />
                                <asp:HiddenField ID="hdnListID" runat="server" Value='<%# Eval("numlistid")%>' />
                                <asp:DropDownList ID="ddlAttributes" runat="server" Width="150"></asp:DropDownList>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtDelItemIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageItems" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsItems" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnData" runat="server" />
    <asp:HiddenField ID="hdnItemCnt" runat="server" />
    <asp:HiddenField ID="hdnItemCodes" runat="server" />
    <asp:HiddenField ID="hdnOppType" runat="server" />
    <asp:Button ID="btnReload" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:HiddenField ID="hdnMultiSelectItemcodes" runat="server" />
    <asp:HiddenField ID="hdnItemGrpId" runat="server" Value="0" />
</asp:Content>
