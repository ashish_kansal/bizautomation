﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Accounting
Public Class frmAddEditInventory
    Inherits BACRMPage
    Dim lngItemCode As Long
    Dim iSerialize, iLotNo, iKit, iAssembly As Integer
    Dim lngItemGroup As Long

    Dim ds As DataSet
    Dim objItems As New CItems


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                If lngItemGroup > 0 Then
                    trSKU.Visible = True
                    trUPC.Visible = True
                Else
                    trSKU.Visible = False
                    trUPC.Visible = False
                End If

                BindWareHouse()

                populateDataSet()
                AttributesControl()
                BindInventory()

                If iSerialize = 1 Or iLotNo = 1 Then
                    BindWareHouseItem()

                    trListPrice.Visible = True
                    tblSerialLot.Visible = True

                    If iLotNo = 1 Then
                        lblSerialNoCaption.Text = "Lot No"
                        trLotQty.Visible = True
                    Else
                        lblSerialNoCaption.Text = "Serial No"
                    End If
                End If

                If iKit = 1 Or iAssembly = 1 Then
                    trOnHand.Visible = False
                    trReOrder.Visible = False
                End If

                Dim dtItemDetails As DataTable
                objItems.ItemCode = lngItemCode
                objItems.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
                dtItemDetails = objItems.ItemDetails()
                If dtItemDetails.Rows.Count > 0 Then
                    hfAverageCost.Value = String.Format("{0:###0.00}", dtItemDetails.Rows(0).Item("monAverageCost"))
                    hfItemName.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("vcItemName"))
                    hfAssetChartAcntID.Value = CCommon.ToString(dtItemDetails.Rows(0).Item("numAssetChartAcntId"))
                End If

                PerformValidation()


            End If

            If IsPostBack Then
                litMessage.Text = ""

                populateDataSet()
                AttributesControl()
            End If

            txtLotQty.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            txtOnHand.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            txtReOrder.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            txtListPrice.Attributes.Add("onkeypress", "CheckNumber(1,event)")

            btnSave.Attributes.Add("onclick", "return SaveWareHouse()")
            btnSaveSerialNo.Attributes.Add("onclick", "return SaveSerialNo()")

            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindWareHouse()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            ddlWareHouse.DataSource = objItem.GetWareHouses
            ddlWareHouse.DataTextField = "vcWareHouse"
            ddlWareHouse.DataValueField = "numWareHouseID"
            ddlWareHouse.DataBind()
            ddlWareHouse.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
    Sub BindWareHouseLocation()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            objItem.WarehouseID = ddlWareHouse.SelectedValue
            ddlWarehouseLocation.DataSource = objItem.GetWarehouseLocation()
            ddlWarehouseLocation.DataTextField = "vcLocation"
            ddlWarehouseLocation.DataValueField = "numWLocationID"
            ddlWarehouseLocation.DataBind()
            ddlWarehouseLocation.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
    Sub BindWareHouseItem()
        Try
            Dim dtWareHouseItem As DataTable
            dtWareHouseItem = ds.Tables(0)

            If dtWareHouseItem.Rows.Count > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                ddlWareHouseItem.DataSource = dtWareHouseItem
                ddlWareHouseItem.DataTextField = "vcWareHouse"
                ddlWareHouseItem.DataValueField = "numWareHouseItemID"
                ddlWareHouseItem.DataBind()
                ddlWareHouseItem.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
    Sub populateDataSet()
        Try
            objItems = New CItems
            objItems.ItemCode = lngItemCode
            objItems.byteMode = 1
            ds = objItems.GetItemWareHouses()
            Dim dtTable, dtTable1 As DataTable
            dtTable = ds.Tables(2)
            If iSerialize = 1 Or iLotNo = 1 Then
                dtTable1 = ds.Tables(1)
            Else : dtTable1 = ds.Tables(0)
            End If
            Dim dr, dr1 As DataRow
            For Each dr In dtTable.Rows
                If dr("fld_type") = "TextBox" Or dr("fld_type") = "TextArea" Then
                    For Each dr1 In dtTable1.Rows
                        If dr1(dr("Fld_label")) = "0" Then dr1(dr("Fld_label")) = ""
                    Next
                End If
            Next
            ds.AcceptChanges()
            If ds.Relations.Count < 1 Then
                ds.Tables(0).TableName = "WareHouse"
                ds.Tables(1).TableName = "SerializedItems"
                ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
                ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
                ds.Relations.Add("Customers", ds.Tables(0).Columns("numWareHouseItemID"), ds.Tables(1).Columns("numWareHouseItemID"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub AttributesControl()
        Try
            Dim dtAttributes As DataTable
            dtAttributes = ds.Tables(2)

            If dtAttributes.Rows.Count > 0 Then
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim dr As DataRow

                For Each dr In dtAttributes.Rows
                    objRow = New HtmlTableRow

                    objCell = New HtmlTableCell
                    objCell.Align = "Right"
                    objCell.Attributes.Add("class", "normal1")
                    objCell.InnerText = dr("Fld_label") & " : "
                    objCell.Width = "8%"
                    objRow.Cells.Add(objCell)

                    If dr("fld_type") = "SelectBox" Then
                        objCell = New HtmlTableCell
                        objCell.Width = "20%"
                        objCell.Align = "left"
                        CreateDropdown(objRow, objCell, dr("Fld_id"), 0, dr("numlistid"))
                    ElseIf dr("fld_type") = "CheckBox" Then
                        objCell = New HtmlTableCell
                        objCell.Width = "20%"
                        objCell.Align = "left"
                        CreateChkBox(objRow, objCell, dr("Fld_id"), 0)
                    End If

                    If iSerialize = 0 AndAlso iLotNo = 0 Then
                        tblWareHouseAttributes.Rows.Add(objRow)
                    ElseIf iSerialize = 1 Or iLotNo = 1 Then
                        tblSerialNoAttributes.Rows.Add(objRow)
                    End If
                Next
                'ElseIf iSerialize = 1 Or iLotNo = 1 Then
                '    For Each dr In dtAttributes.Rows

                '    Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindInventory()
        Try
            gvInventory.DataSource = ds
            gvInventory.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            objItems = New CItems



            objItems.ItemCode = lngItemCode

            objItems.WareHouseItemID = CCommon.ToLong(hfWareHouseItemID.Value)
            objItems.WarehouseID = ddlWareHouse.SelectedValue
            'objItems.WLocation = txtLocation.Text.Trim()
            objItems.WarehouseLocationID = CCommon.ToLong(ddlWarehouseLocation.SelectedValue)
            objItems.WListPrice = CCommon.ToDecimal(txtListPrice.Text.Trim())
            objItems.OnHand = CCommon.ToDouble(txtOnHand.Text.Trim())
            objItems.ReOrder = CCommon.ToDouble(txtReOrder.Text.Trim())
            objItems.WSKU = txtSKU.Text.Trim
            objItems.WBarCode = txtUPC.Text.Trim
            objItems.DomainID = Session("DomainID")
            objItems.UserCntID = Session("UserContactID")

            objItems.byteMode = 1

            Dim dtCusTable As New DataTable
            dtCusTable.Columns.Add("Fld_ID")
            dtCusTable.Columns.Add("Fld_Value")
            dtCusTable.TableName = "CusFlds"
            Dim drCusRow As DataRow

            Dim dtTable As DataTable
            Dim dr As DataRow
            dtTable = ds.Tables(2)

            If dtTable.Rows.Count > 0 Then
                For Each dr In dtTable.Rows
                    drCusRow = dtCusTable.NewRow
                    drCusRow("Fld_ID") = dr("fld_id")

                    If dr("fld_type") = "SelectBox" Then
                        Dim ddl As DropDownList
                        ddl = tblWareHouseAttributes.FindControl(dr("Fld_id"))
                        drCusRow("Fld_Value") = CStr(ddl.SelectedItem.Value)
                    ElseIf dr("fld_type") = "CheckBox" Then
                        Dim chk As CheckBox
                        chk = tblWareHouseAttributes.FindControl(dr("Fld_id"))
                        If chk.Checked = True Then
                            drCusRow("Fld_Value") = "1"
                        Else : drCusRow("Fld_Value") = "0"
                        End If
                    End If

                    dtCusTable.Rows.Add(drCusRow)
                    dtCusTable.AcceptChanges()
                Next
            End If

            Dim ds1 As New DataSet
            ds1.Tables.Add(dtCusTable)
            objItems.strFieldList = ds1.GetXml

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                objItems.AddUpdateWareHouseForItems()

                If CCommon.ToLong(hfWareHouseItemID.Value) = 0 Then
                    If iSerialize = 0 And iLotNo = 0 And iAssembly = 0 Then 'Exclude  journal entry for serialized items, aseembly item
                        objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), CCommon.ToDouble(txtOnHand.Text), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                    End If
                End If

                objTransactionScope.Complete()
            End Using

            EmptyTextBoxes(Page)

            populateDataSet()
            BindInventory()
            BindWareHouseItem()

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub PerformValidation()
        Try

            'Validate if Asset Account is set
            If objItems Is Nothing Then objItems = New CItems
            If Not objItems.ValidateItemAccount(lngItemCode, Session("DomainID"), 1) = True Then
                btnSave.Visible = False
                btnSaveSerialNo.Visible = False
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                Exit Sub
            End If


            If ChartOfAccounting.GetDefaultAccount("OE", Session("DomainID")) = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SerializeItemValidate", "alert('Please Map Default Opening Balance Equity Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.')", True)
                btnSave.Visible = False
                btnSaveSerialNo.Visible = False
                Exit Sub
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSaveSerialNo_Click(sender As Object, e As System.EventArgs) Handles btnSaveSerialNo.Click
        Try




            objItems = New CItems
            objItems.ItemCode = lngItemCode

            objItems.DomainID = Session("DomainID")
            objItems.UserCntID = Session("UserContactID")
            objItems.byteMode = 2

            objItems.WareHouseItmsDTLID = CCommon.ToLong(hfnumWareHouseItmsDTLID.Value)
            objItems.WareHouseItemID = ddlWareHouseItem.SelectedValue
            objItems.SerialNo = txtSerialNo.Text.Trim
            objItems.Comments = txtComments.Text.Trim
            objItems.Quantity = CCommon.ToLong(txtLotQty.Text.Trim)

            Dim dtCusTable As New DataTable
            dtCusTable.Columns.Add("Fld_ID")
            dtCusTable.Columns.Add("Fld_Value")
            dtCusTable.TableName = "CusFlds"
            Dim drCusRow As DataRow

            Dim dtTable As DataTable
            Dim dr As DataRow
            dtTable = ds.Tables(2)

            If dtTable.Rows.Count > 0 Then
                For Each dr In dtTable.Rows
                    drCusRow = dtCusTable.NewRow
                    drCusRow("Fld_ID") = dr("fld_id")

                    If dr("fld_type") = "SelectBox" Then
                        Dim ddl As DropDownList
                        ddl = tblSerialLot.FindControl(dr("Fld_id"))
                        drCusRow("Fld_Value") = CStr(ddl.SelectedItem.Value)
                    ElseIf dr("fld_type") = "CheckBox" Then
                        Dim chk As CheckBox
                        chk = tblSerialLot.FindControl(dr("Fld_id"))
                        If chk.Checked = True Then
                            drCusRow("Fld_Value") = "1"
                        Else : drCusRow("Fld_Value") = "0"
                        End If
                    End If

                    dtCusTable.Rows.Add(drCusRow)
                    dtCusTable.AcceptChanges()
                Next
            End If

            Dim ds1 As New DataSet
            ds1.Tables.Add(dtCusTable)
            objItems.strFieldList = ds1.GetXml

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                objItems.AddUpdateWareHouseForItems()

                If CCommon.ToLong(hfnumWareHouseItmsDTLID.Value) = 0 Then
                    If iSerialize = 1 Or iLotNo = 1 Then 'include  journal entry for serialized items
                        If iSerialize = 1 Then
                            'decTotalAdjustmentAmount = 1 * CCommon.ToDecimal(hfAverageCost.Value)
                            'intAdjQty = 1
                            objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), 1, CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        ElseIf iLotNo = 1 Then
                            'decTotalAdjustmentAmount = CCommon.ToLong(txtLotQty.Text.Trim) * CCommon.ToDecimal(hfAverageCost.Value)
                            'intAdjQty = CCommon.ToInteger(txtLotQty.Text.Trim)
                            objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), CCommon.ToInteger(txtLotQty.Text.Trim), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        End If
                        'Dim journalID As Long = SaveJournalHeader()
                        'SaveDataToGeneralJournalDetails(journalID)
                    End If
                End If

                objTransactionScope.Complete()
            End Using

            EmptyTextBoxes(Page)

            populateDataSet()
            gvInventory.Rebind()
            BindWareHouseItem()
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)
            ddlWareHouseItem.Enabled = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvInventory_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvInventory.ItemCommand
        Try
            If e.CommandName = "Edit_WareHouse" Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
                EmptyTextBoxes(Page)

                hfWareHouseItemID.Value = item.GetDataKeyValue("numWareHouseItemID").ToString()

                Dim dtTable As DataTable
                dtTable = ds.Tables(0)
                Dim dr As DataRow

                Dim dRows As DataRow() = dtTable.Select("numWareHouseItemID = " & CCommon.ToLong(hfWareHouseItemID.Value).ToString())
                If dRows.Length > 0 Then

                    dr = dRows(0)

                    If ddlWareHouse.Items.FindByValue(dr("numWareHouseID")) IsNot Nothing Then
                        ddlWareHouse.ClearSelection()
                        ddlWareHouse.Items.FindByValue(dr("numWareHouseID")).Selected = True
                        BindWareHouseLocation()
                    End If

                    If ddlWarehouseLocation.Items.FindByValue(dr("numWLocationID")) IsNot Nothing Then
                        ddlWarehouseLocation.ClearSelection()
                        ddlWarehouseLocation.Items.FindByValue(dr("numWLocationID")).Selected = True
                    End If


                    txtOnHand.Text = dr("OnHand")
                    lblOnHand.Text = dr("OnHand")
                    txtReOrder.Text = dr("ReOrder")
                    'txtLocation.Text = dr("Location")
                    txtListPrice.Text = dr("Price")
                    txtSKU.Text = dr("SKU")
                    txtUPC.Text = dr("BarCOde")

                    txtOnHand.Visible = False
                    lblOnHand.Visible = True
                    Dim dtAttributes As DataTable
                    dtAttributes = ds.Tables(2)
                    Dim drCusRow As DataRow

                    If iSerialize = 0 AndAlso iLotNo = 0 Then
                        For Each drCusRow In dtAttributes.Rows
                            If drCusRow("fld_type") = "SelectBox" Then
                                Dim ddl As DropDownList
                                ddl = tblWareHouseAttributes.FindControl(drCusRow("Fld_id"))

                                If ddl.Items.FindByValue(dr(drCusRow("Fld_label"))) IsNot Nothing Then
                                    ddl.ClearSelection()
                                    ddl.Items.FindByValue(dr(drCusRow("Fld_label"))).Selected = True
                                End If
                            ElseIf drCusRow("fld_type") = "CheckBox" Then
                                Dim chk As CheckBox
                                chk = tblWareHouseAttributes.FindControl(drCusRow("Fld_id"))
                                If dr(drCusRow("Fld_label")) = 1 Then
                                    chk.Checked = True
                                Else
                                    chk.Checked = False
                                End If
                            End If
                        Next
                    End If
                End If

            ElseIf e.CommandName = "Delete_WareHouse" Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

                objItems = New CItems
                objItems.ItemCode = lngItemCode

                'make Delete journal entry 
                'objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), -1 * CCommon.ToInteger(item.GetDataKeyValue("OnHand")), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                If item.GetDataKeyValue("IsDeleteKitWarehouse") IsNot Nothing AndAlso item.GetDataKeyValue("bitKitParent") IsNot Nothing AndAlso _
                   CCommon.ToBool(item.GetDataKeyValue("IsDeleteKitWarehouse")) = True AndAlso CCommon.ToBool(item.GetDataKeyValue("bitKitParent")) = True AndAlso Not CCommon.ToBool(item.GetDataKeyValue("bitChildItemWarehouse")) Then

                    objItems.DomainID = Session("DomainID")
                    objItems.UserCntID = Session("UserContactID")
                    objItems.byteMode = 3

                    objItems.WareHouseItemID = item.GetDataKeyValue("numWareHouseItemID").ToString()
                    objItems.AddUpdateWareHouseForItems()
                    EmptyTextBoxes(Page)
                    populateDataSet()
                ElseIf CCommon.ToBool(item.GetDataKeyValue("bitChildItemWarehouse")) Then
                    litMessage.Text = "You are not allowed to delete warehouse item, because item warehouse is used as child of other assembly or kit item(s)."
                ElseIf CCommon.ToDouble(item.GetDataKeyValue("OnHand")) > 0 Then
                    litMessage.Text = "You are not allowed to delete warehouse item, Your option is to do adjustment of qty such that OnHand quantity goes to 0."

                Else
                    objItems.DomainID = Session("DomainID")
                    objItems.UserCntID = Session("UserContactID")
                    objItems.byteMode = 3

                    objItems.WareHouseItemID = item.GetDataKeyValue("numWareHouseItemID").ToString()

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        objItems.AddUpdateWareHouseForItems()
                        objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), -1 * CCommon.ToDouble(item.GetDataKeyValue("OnHand")), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))

                        objTransactionScope.Complete()
                    End Using

                    EmptyTextBoxes(Page)
                    populateDataSet()
                End If

                gvInventory.Rebind()
                BindWareHouseItem()

            ElseIf e.CommandName = "Edit_SerialLot" Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
                EmptyTextBoxes(Page)

                hfnumWareHouseItmsDTLID.Value = item.GetDataKeyValue("numWareHouseItmsDTLID").ToString()

                Dim dtTable As DataTable
                dtTable = ds.Tables(1)
                Dim dr As DataRow

                Dim dRows As DataRow() = dtTable.Select("numWareHouseItmsDTLID = " & CCommon.ToLong(hfnumWareHouseItmsDTLID.Value).ToString())
                If dRows.Length > 0 Then

                    dr = dRows(0)

                    If ddlWareHouseItem.Items.FindByValue(dr("numWareHouseItemID")) IsNot Nothing Then
                        ddlWareHouseItem.ClearSelection()
                        ddlWareHouseItem.Items.FindByValue(dr("numWareHouseItemID")).Selected = True
                    End If

                    ddlWareHouseItem.Enabled = False

                    txtSerialNo.Text = dr("vcSerialNo")
                    txtLotQty.Text = dr("numQty")
                    lblLotQty.Text = dr("numQty")
                    txtComments.Text = dr("Comments")


                    txtLotQty.Visible = False
                    lblLotQty.Visible = True

                    Dim dtAttributes As DataTable
                    dtAttributes = ds.Tables(2)
                    Dim drCusRow As DataRow

                    If iSerialize = 1 Or iLotNo = 1 Then
                        For Each drCusRow In dtAttributes.Rows
                            If drCusRow("fld_type") = "SelectBox" Then
                                Dim ddl As DropDownList
                                ddl = tblSerialNoAttributes.FindControl(drCusRow("Fld_id"))

                                If ddl.Items.FindByValue(dr(drCusRow("Fld_label"))) IsNot Nothing Then
                                    ddl.ClearSelection()
                                    ddl.Items.FindByValue(dr(drCusRow("Fld_label"))).Selected = True
                                End If
                            ElseIf drCusRow("fld_type") = "CheckBox" Then
                                Dim chk As CheckBox
                                chk = tblSerialNoAttributes.FindControl(drCusRow("Fld_id"))
                                If dr(drCusRow("Fld_label")) = 1 Then
                                    chk.Checked = True
                                Else
                                    chk.Checked = False
                                End If
                            End If
                        Next
                    End If
                End If
            ElseIf e.CommandName = "Delete_SerialLot" Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

                objItems = New CItems
                objItems.ItemCode = lngItemCode

                If CCommon.ToDouble(item.GetDataKeyValue("OnHand")) > 0 Then
                    litMessage.Text = "You are not allowed to delete warehouse item, Your option is to do adjustment of qty such that OnHand quantity goes to 0."
                Else
                    objItems.DomainID = Session("DomainID")
                    objItems.UserCntID = Session("UserContactID")
                    objItems.byteMode = 4

                    objItems.WareHouseItmsDTLID = item.GetDataKeyValue("numWareHouseItmsDTLID").ToString()

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        objItems.AddUpdateWareHouseForItems()

                        If iSerialize = 1 Then
                            objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), -1, CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        ElseIf iLotNo Then
                            objItems.MakeItemQtyAdjustmentJournal(lngItemCode, CCommon.ToString(hfItemName.Value), -1 * CCommon.ToInteger(item.GetDataKeyValue("numQty")), CCommon.ToDecimal(hfAverageCost.Value), CCommon.ToLong(hfAssetChartAcntID.Value), Session("UserContactID"), Session("DomainID"))
                        End If

                        objTransactionScope.Complete()
                    End Using
                    EmptyTextBoxes(Page)
                    populateDataSet()

                End If
                gvInventory.Rebind()
                BindWareHouseItem()

            End If
        Catch ex As Exception
            Select Case ex.Message
                Case "OpportunityItems_Depend"
                    litMessage.Text = "Deleted Warehouse is being used by Opportunity, It can not be deleted."
                Case "OpportunityKitItems_Depend"
                    litMessage.Text = "Deleted Warehouse is being used by Opportunity Kit Items, It can not be deleted."
                Case "KitItems_Depend"
                    litMessage.Text = "Deleted Warehouse is being used by Kit Items, It can not be deleted."
                Case Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
            End Select
        End Try
    End Sub

    Private Sub gvInventory_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvInventory.NeedDataSource
        Try
            gvInventory.DataSource = ds
            'gvInventory.Rebind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub frmAddEditInventory_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            lngItemCode = GetQueryStringVal("ItemCode")
            iSerialize = GetQueryStringVal("Serialize")
            iLotNo = GetQueryStringVal("LotNo")
            lngItemGroup = GetQueryStringVal("ItemGroup")

            iKit = GetQueryStringVal("Kit")
            iAssembly = GetQueryStringVal("Assembly")

            populateDataSet()

            Dim i As Integer
            Dim bField As GridBoundColumn
            Dim radColumn As Telerik.Web.UI.GridColumn

            For i = 0 To ds.Tables(0).Columns.Count - 1
                radColumn = gvInventory.Columns.FindByUniqueNameSafe(ds.Tables(0).Columns(i).ColumnName)

                If radColumn IsNot Nothing Then
                    gvInventory.Columns.Remove(radColumn)
                End If

                bField = New GridBoundColumn

                bField.HeaderText = ds.Tables(0).Columns(i).ColumnName
                bField.DataField = ds.Tables(0).Columns(i).ColumnName
                bField.UniqueName = ds.Tables(0).Columns(i).ColumnName

                If bField.UniqueName = "bitKitParent" OrElse bField.UniqueName = "IsDeleteKitWarehouse" Then
                Else
                    gvInventory.Columns.Add(bField)
                End If

            Next

            Dim gv As GridTableView
            gv = gvInventory.MasterTableView.DetailTables(0)

            For i = 0 To ds.Tables(1).Columns.Count - 1
                radColumn = gv.Columns.FindByUniqueNameSafe(ds.Tables(1).Columns(i).ColumnName)

                If radColumn IsNot Nothing Then
                    gv.Columns.Remove(radColumn)
                End If

                bField = New GridBoundColumn

                bField.HeaderText = ds.Tables(1).Columns(i).ColumnName
                bField.DataField = ds.Tables(1).Columns(i).ColumnName
                bField.UniqueName = ds.Tables(1).Columns(i).ColumnName

                gv.Columns.Add(bField)
            Next

            gvInventory.Columns.FindByUniqueNameSafe("numItemCode").Visible = False
            gvInventory.Columns.FindByUniqueNameSafe("numWareHouseItemID").Visible = False
            gvInventory.Columns.FindByUniqueNameSafe("vcWarehouse").HeaderText = "Location"
            gvInventory.Columns.FindByUniqueNameSafe("numWareHouseID").Visible = False
            gvInventory.Columns.FindByUniqueNameSafe("numWLocationID").Visible = False
            gvInventory.Columns.FindByUniqueNameSafe("Location").Visible = False
            gvInventory.Columns.FindByUniqueNameSafe("Op_Flag").Visible = False
            gvInventory.Columns.FindByUniqueNameSafe("Price").HeaderText = "List Price (I)"

            gvInventory.Columns.FindByUniqueNameSafe("OnHand").HeaderText = "OnHand"
            gvInventory.Columns.FindByUniqueNameSafe("PurchaseOnHand").HeaderText = "Buy OnHand"
            gvInventory.Columns.FindByUniqueNameSafe("SalesOnHand").HeaderText = "Sell OnHand"

            gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("numWareHouseItmsDTLID").Visible = False
            gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("numWareHouseItemID").Visible = False
            gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("Op_Flag").Visible = False
            gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("OldQty").Visible = False
            gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("vcWarehouse").Visible = False

            If iLotNo = 1 Then
                gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("vcSerialNo").HeaderText = "Lot No"
                gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("numQty").HeaderText = "Lot Qty"
            Else
                gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("vcSerialNo").HeaderText = "Serial No"
                gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe("numQty").Visible = False
            End If

            If lngItemGroup > 0 Then
                gvInventory.Columns.FindByUniqueNameSafe("SKU").Visible = True
                gvInventory.Columns.FindByUniqueNameSafe("BarCode").Visible = True

                gvInventory.Columns.FindByUniqueNameSafe("SKU").HeaderText = "SKU (M)"
                gvInventory.Columns.FindByUniqueNameSafe("BarCode").HeaderText = "UPC (M)"
            Else
                gvInventory.Columns.FindByUniqueNameSafe("SKU").Visible = False
                gvInventory.Columns.FindByUniqueNameSafe("BarCode").Visible = False
            End If

            Dim dtAttributes As DataTable
            dtAttributes = ds.Tables(2)
            Dim dr As DataRow

            If iSerialize = 0 AndAlso iLotNo = 0 Then
                For Each dr In dtAttributes.Rows
                    gvInventory.Columns.FindByUniqueNameSafe(dr("Fld_label")).Visible = False
                    gvInventory.Columns.FindByUniqueNameSafe(dr("Fld_label") & "Value").Visible = True
                    gvInventory.Columns.FindByUniqueNameSafe(dr("Fld_label") & "Value").HeaderText = dr("Fld_label")
                Next
            ElseIf iSerialize = 1 Or iLotNo = 1 Then
                For Each dr In dtAttributes.Rows
                    gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe(dr("Fld_label")).Visible = False
                    gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe(dr("Fld_label") & "Value").Visible = True
                    gvInventory.MasterTableView.DetailTables(0).Columns.FindByUniqueNameSafe(dr("Fld_label") & "Value").HeaderText = dr("Fld_label")
                Next
            End If

            gvInventory.MasterTableView.EnableColumnsViewState = False
            gvInventory.MasterTableView.DetailTables(0).EnableColumnsViewState = False



        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Sub EmptyTextBoxes(parent As Control)
        ' Loop through all the controls on the page
        For Each c As Control In parent.Controls
            ' Check and see if it's a textbox
            If (c.[GetType]() = GetType(TextBox)) Then
                DirectCast(c, TextBox).Text = ""
            ElseIf (c.[GetType]() = GetType(DropDownList)) Then
                DirectCast(c, DropDownList).ClearSelection()
                DirectCast(c, DropDownList).SelectedIndex = -1
            ElseIf (c.[GetType]() = GetType(CheckBox)) Then
                DirectCast(c, CheckBox).Checked = False
            ElseIf (c.[GetType]() = GetType(HiddenField)) Then
                If Not (c.ClientID = "hfAverageCost" Or c.ClientID = "hfItemName" Or c.ClientID = "hfAssetChartAcntID") Then
                    DirectCast(c, HiddenField).Value = "0"
                End If
            End If

            If c.HasControls Then
                EmptyTextBoxes(c)
            End If
        Next
        txtOnHand.Visible = True
        lblOnHand.Visible = False

        txtLotQty.Visible = True
        lblLotQty.Visible = False
    End Sub

    Private Sub ddlWareHouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWareHouse.SelectedIndexChanged
        Try
            BindWareHouseLocation()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class