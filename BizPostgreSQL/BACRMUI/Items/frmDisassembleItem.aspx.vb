﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities

Public Class frmDisassembleItem
    Inherits BACRMPage
#Region "Memeber Variable"
    Private objAssembledItem As AssembledItem
    Private lngItemCode As Long
#End Region

#Region "Constructor"
    Sub New()
        objAssembledItem = New AssembledItem
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litMessage.Text = ""

            If Not Page.IsPostBack Then
                lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
                hdnItemCode.Value = lngItemCode

                If lngItemCode > 0 Then
                    BindAssemblingHistory()
                End If

                btnClose.Attributes.Add("onclick", "return Close();")
            Else
                lngItemCode = CCommon.ToLong(hdnItemCode.Value)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindAssemblingHistory()
        Try
            objAssembledItem.DomainID = CCommon.ToLong(Session("DomainID"))
            objAssembledItem.ItemCode = lngItemCode
            objAssembledItem.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))

            dgAssembledHistory.DataSource = objAssembledItem.GetByItemCode()
            dgAssembledHistory.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    'Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
    '    Try
    '        If lngItemCode > 0 Then
    '            Dim errorMessage As String = ""

    '            If ddlWarehouses.SelectedValue = "0" Then
    '                errorMessage = "Select warehouse. <br />"
    '            End If

    '            Dim quantity As Int32 = 0

    '            If String.IsNullOrEmpty(txtQuantity.Text) Then
    '                errorMessage += "Select quantity to disassemble."
    '            ElseIf Not Int32.TryParse(txtQuantity.Text, quantity) Then
    '                errorMessage += "Quantity must be nummeric."
    '            ElseIf quantity = 0 Then
    '                errorMessage += "Quantity must be greater than 0."
    '            End If

    '            If errorMessage.Length > 0 Then
    '                litMessage.Text = errorMessage
    '            Else
    '                Dim objItem As New CItems
    '                objItem.DomainID = Session("DomainID")
    '                objItem.UserCntID = Session("UserContactID")
    '                objItem.ItemCode = lngItemCode
    '                objItem.WareHouseItemID = CCommon.ToLong(ddlWarehouses.SelectedValue)
    '                objItem.NoofUnits = CCommon.ToInteger(txtQuantity.Text)
    '                objItem.DisassembleItem()

    '                ClientScript.RegisterStartupScript(Me.GetType(), "SaveClose", "window.opener.location.href=window.opener.location.href; window.close();", True)
    '            End If
    '        Else
    '            litMessage.Text = "Select valid item first."
    '        End If
    '    Catch ex As Exception
    '        If ex.Message.Contains("INSUFFICIENT_ONHAND_QTY") Then
    '            litMessage.Text = "Quantity entered to disassemble is more than On Hand quantity."
    '        Else
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End If
    '    End Try
    'End Sub
#End Region

    Private Sub dgAssembledHistory_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles dgAssembledHistory.RowCommand
        Try
            If e.CommandName = "Disassemble" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = dgAssembledHistory.Rows(index)

                Dim id As Long = CCommon.ToLong(dgAssembledHistory.DataKeys(index)("ID"))
                Dim monAverageCost As Double = CCommon.ToDouble(dgAssembledHistory.DataKeys(index)("monAverageCost"))
                Dim tintType As Short = CCommon.ToShort(dgAssembledHistory.DataKeys(index)("tintType"))
                Dim quantity As Int32 = 0
                Dim maxQuantity As Int32

                If id > 0 Then
                    maxQuantity = CCommon.ToInteger(DirectCast(row.FindControl("lblAssembledQty"), Label).Text)

                    If Not Int32.TryParse(DirectCast(row.FindControl("txtQuantity"), TextBox).Text, quantity) Then
                        litMessage.Text = "Enter numeric value in quantity to disassemble."
                        Exit Sub
                    Else
                        If quantity > 0 Then
                            If quantity > maxQuantity Then
                                litMessage.Text = "Quantity to disassemble can not be greater than assembled quantity."
                                Exit Sub
                            Else
                                Using objTrasactionScope As New Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    objAssembledItem.DomainID = CCommon.ToLong(Session("DomainID"))
                                    objAssembledItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                                    objAssembledItem.ID = id
                                    objAssembledItem.AssembledQty = quantity
                                    objAssembledItem.Type = IIf(tintType = 2, AssembledItem.AssemblyType.WorkOrder, AssembledItem.AssemblyType.Build)
                                    objAssembledItem.DisassembleItem()

                                    If tintType = 2 Then
                                        'make journal entry
                                        Dim objItems As New CItems
                                        objItems.ReverseAssemblyWorkOrderJournal(id, quantity, Session("UserContactID"), Session("DomainID"))

                                        'Delete Work Order
                                        Dim objWorkOrder As New WorkOrder
                                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                                        objWorkOrder.WorkOrderID = id
                                        objWorkOrder.Disassembled()
                                    ElseIf tintType = 1 Then
                                        'make journal entry
                                        Dim objItems As New CItems
                                        objItems.ReverseAssemblyItemQtyAdjustmentJournal(id, lngItemCode, quantity, monAverageCost, Session("UserContactID"), Session("DomainID"))

                                        'Delete record from assembled item history
                                        objAssembledItem.Delete()
                                    End If

                                    objTrasactionScope.Complete()
                                End Using

                                BindAssemblingHistory()
                            End If
                        Else
                            litMessage.Text = "Quantity to disassemble must be greater than 0."
                            Exit Sub
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.Contains("INSUFFICIENT_ONHAND_QTY") Then
                litMessage.Text = "Quantity entered to disassemble is more than On Hand quantity."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub dgAssembledHistory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles dgAssembledHistory.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim index As Integer = Convert.ToInt32(e.Row.RowIndex)

                Dim tintType As Short = CCommon.ToShort(dgAssembledHistory.DataKeys(index)("tintType"))

                If tintType = 2 Then
                    DirectCast(e.Row.FindControl("txtQuantity"), TextBox).Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class