Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection
Imports BACRM.BusinessLogic.Contacts
Imports System.IO
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports ClosedXML.Excel

Namespace BACRM.UserInterface.Items

    Partial Class frmItemList
        Inherits BACRMPage

        Dim dsFilter As New DataSet
        Dim dsItems As DataSet

        Dim objContact As New CContacts
        Dim RegularSearch As String
        Dim CustomSearch As String
        Shared lngCustId As Long = 0
        Shared warehouseid As Long = 0

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Dim strColumn As String

        Dim strPage As String
        Dim Asset As Integer
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'Make MultiSelect Cluster Visible
                If (GetQueryStringVal("CustId") <> "" And Session("MultiSelectItemCodes") Is Nothing) Then
                    tblMultiSelect.Visible = True
                    lngCustId = CCommon.ToLong(GetQueryStringVal("CustId"))

                ElseIf (GetQueryStringVal("CustId") = "" And Session("MultiSelectItemCodes") Is Nothing) Then
                    lngCustId = 0

                End If

                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                strPage = GetQueryStringVal("Page")
                Asset = 0
                If GetQueryStringVal("Asset") <> "" Then
                    Asset = GetQueryStringVal("Asset")
                End If

                HideControls()
                LoadDynamicDropdowns()

                If Not strPage Is Nothing Then
                    strPage = strPage.Replace("%20", " ")
                Else
                    strPage = "All Items"
                End If

                If Session("SOItems") IsNot Nothing AndAlso DirectCast(Session("SOItems"), DataSet).Tables(0).Rows.Count > 0 Then
                    btnOpenOrder.Text = "Open Order (" & DirectCast(Session("SOItems"), DataSet).Tables(0).Rows.Count & ")"
                End If

                If Not IsPostBack Then
                    If (GetQueryStringVal("WID") <> "") Then
                        ddlFilterWarehouse.SelectedValue = CCommon.ToLong(GetQueryStringVal("WID"))
                    End If
                    If GetQueryStringVal("Export") <> "" Then
                        btnExport.Visible = CCommon.ToString(GetQueryStringVal("Export"))
                    End If

                    If GetQueryStringVal("CompanyName") <> "" And GetQueryStringVal("SearchText") <> "" Then
                        PersistTable.Clear()
                        PersistTable.Add(PersistKey.CurrentPage, "1")
                        PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim)
                        PersistTable.Add(PersistKey.SortColumnName, IIf(txtSortColumn.Text = "", "vcItemName", txtSortColumn.Text))
                        PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                        PersistTable.Add(PersistKey.SearchValue, GetQueryStringVal("SearchText"))
                        PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                        PersistTable.Save()
                    End If

                    'Check if user has rights to edit grid configuration
                    Dim m_aryRightsForViewGridConfiguration() As Integer = GetUserRightsForPage_Other(37, 130)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If
                    Dim m_aryRightsForSaveInventoryConfiguration() As Integer = GetUserRightsForPage_Other(37, 149)
                    If m_aryRightsForSaveInventoryConfiguration(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSaveInventory.Visible = False
                    End If
                    If Asset = 0 Then
                        btnNew.Attributes.Add("onclick", "return OpenAddItem()")
                        btnNew.Text = "New Item"
                        btnDelete.Visible = True
                    Else
                        If GetQueryStringVal("numDivisionID") = "" Then
                            btnNew.Attributes.Add("onclick", "return OpenNewAsset(-1,1,0)")
                        Else
                            btnNew.Attributes.Add("onclick", "return OpenNewAsset(-1,2," & CCommon.ToLong(GetQueryStringVal("numDivisionID")) & ")")
                        End If
                        btnNew.Text = "New Asset"
                        btnDelete.Visible = False
                    End If
                    btnBarcode.Attributes.Add("onclick", "return BarcodeRecord()")

                    Session("Help") = "Item"
                    GetUserRightsForPage(37, 9)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnNew.Visible = False
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False

                    Dim m_aryRightsForManageInventory As Integer() = GetUserRightsForPage_Other(37, 132)
                    If m_aryRightsForManageInventory(RIGHTSTYPE.VIEW) = 0 Then
                        hplAdjustInv.Visible = False
                    End If

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortChar.Text = PersistTable(PersistKey.SortCharacter)
                        txtSortColumn.Text = PersistTable(PersistKey.SortColumnName)
                        txtSortOrder.Text = PersistTable(PersistKey.SortOrder)
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        chkArchivedItems.Checked = CCommon.ToBool(PersistTable(chkArchivedItems.ID))
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    imgCartAdd.Attributes.Add("onclick", "return OpenNewSalesOrder('add');")
                    btnOpenOrder.Attributes.Add("onclick", "return OpenNewSalesOrder('open');")


                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 Then
                        txtGridColumnFilter.Text = ""
                    End If

                    If (lngCustId <> 0) Then
                        tblMultiSelect.Visible = True
                    End If
                    BindWareHouse()
                    BindDatagrid()
                    lblList.Text = strPage

                End If

                If Asset = 0 Then
                    btnNew.Text = "New Item"
                Else
                    btnNew.Text = "New Asset"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Public Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                dsItems = GetItems(False, radCmbSearch.Text)

                Dim dtItems As DataTable
                dtItems = dsItems.Tables(0)

                Dim dtColumns As DataTable
                dtColumns = dsItems.Tables(1)

                Dim m_aryRightsForInlineEdit() As Integer
                m_aryRightsForInlineEdit = GetUserRightsForPage_Other(37, 9)

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtItems.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim)
                PersistTable.Add(PersistKey.SortColumnName, IIf(txtSortColumn.Text = "", "vcItemName", txtSortColumn.Text))
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                PersistTable.Add(chkArchivedItems.ID, chkArchivedItems.Checked)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()

                If CreateCol Then
                    Dim htGridColumnSearch As New Hashtable

                    If txtGridColumnFilter.Text.Trim.Length > 0 Then
                        Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                        Dim strIDValue() As String

                        For j = 0 To strValues.Length - 1
                            strIDValue = strValues(j).Split(":")

                            htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                        Next
                    End If

                    Dim i As Integer
                    Dim bField As GridBoundColumn
                    Dim Tfield As GridTemplateColumn
                    gvSearch.Columns.Clear()
                    bField = New GridBoundColumn
                    bField.DataField = "numItemCode"
                    bField.Visible = False
                    gvSearch.Columns.Add(bField)

                    For i = 0 To dtColumns.Rows.Count - 1
                        Tfield = New GridTemplateColumn

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dtColumns.Rows(i), m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 21, IIf(txtSortColumn.Text = "", "vcItemName", txtSortColumn.Text), txtSortOrder.Text, isTelerik:=True)
                        Tfield.HeaderStyle.Width = New Unit(CCommon.ToLong(dtColumns.Rows(i)("intColumnWidth")))
                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dtColumns.Rows(i), m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 21, IIf(txtSortColumn.Text = "", "vcItemName", txtSortColumn.Text), txtSortOrder.Text, isTelerik:=True)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtColumns.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New GridTemplateColumn
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 21, isTelerik:=True)
                    Tfield.HeaderStyle.Width = New Unit(30)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 21, isTelerik:=True)
                    gvSearch.Columns.Add(Tfield)
                End If

                gvSearch.DataSource = dtItems
                gvSearch.DataBind()

                If m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtColumns)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function ConvertDataTableToHTML(ByVal dt As DataTable) As String
            Dim html As String = "<table>"
            html += "<tr>"

            For i As Integer = 0 To dt.Columns.Count - 1
                html += "<td>" & dt.Columns(i).ColumnName & "</td>"
            Next

            html += "</tr>"

            For i As Integer = 0 To dt.Rows.Count - 1
                html += "<tr>"

                For j As Integer = 0 To dt.Columns.Count - 1
                    html += "<td>" & dt.Rows(i)(j).ToString() & "</td>"
                Next

                html += "</tr>"
            Next

            html += "</table>"
            Return html
        End Function

        Private Function GetItems(ByVal isFromSearch As Boolean, ByVal searchText As String) As DataSet
            Try

                Dim objItems As New CItems
                Dim strCondition As String = ""

                With objItems
                    .ItemClassification = 0
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortCharacter = CCommon.ToString(txtSortChar.Text)
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = IIf(isFromSearch, -1, Convert.ToInt32(txtCurrrentPage.Text.Trim()))
                    .DomainID = Session("DomainID")
                    .PageSize = IIf(isFromSearch, -1, Convert.ToInt32(Session("PagingRows")))
                    .TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        If txtSortColumn.Text.Split("~").Length = 2 Then
                            .columnName = txtSortColumn.Text.Split("~")(1)
                        Else
                            .columnName = txtSortColumn.Text
                        End If
                    Else : .columnName = "vcItemName"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    If strPage = "Services" Then
                        .ItemType = "S"
                    ElseIf strPage = "Kits" Then
                        .ItemType = "P"
                        .KitParent = True
                    ElseIf strPage = "Inventory Items" Then
                        .ItemType = "P"
                        btnBarcode.Visible = True
                    ElseIf strPage = "Non-Inventory Items" Then
                        .ItemType = "N"
                    ElseIf strPage = "Serialized Items" Then
                        .ItemType = "P"
                        .bitSerialized = True
                    ElseIf strPage = "Assembly" Then
                        .ItemType = "P"
                        .Assembly = True
                    ElseIf strPage = "Asset Items" Then
                        .ItemType = "P"
                        .bitAsset = True
                    ElseIf strPage = "Rental Items" Then
                        .ItemType = "P"
                        .bitAsset = True
                        .bitRental = True
                    ElseIf strPage = "Containers" Then
                        .ItemType = "P"
                        .bitContainer = True
                    End If

                    .DashboardReminderType = CCommon.ToShort(GetQueryStringVal("ReminderType"))
                    .ItemGroupID = CCommon.ToLong(GetQueryStringVal("ItemGroup"))
                    .UserCntID = Session("UserContactID")
                    .SearchText = searchText

                    'Create filter conditions as per selection within rad dropdowns
                    Dim strMain As String() = {""}
                    Dim strName As String = ""
                    Dim strValue As String = ""

                    If hdnData.Value IsNot Nothing AndAlso CCommon.ToString(hdnData.Value).Length > 0 Then
                        strMain = CCommon.ToString(hdnData.Value).Trim(",").Split("~")
                        If strMain.Length > 0 Then
                            For Each strItem As String In strMain
                                strName = If(strItem.Split(":").Length > 0, strItem.Split(":")(0).Trim(","), "")
                                strValue = If(strItem.Split(":").Length > 1, strItem.Split(":")(1).Trim(","), "")
                                If strValue.Length > 0 AndAlso strValue.Length > 0 Then
                                    For Each drRow As DataRow In dsFilter.Tables(1).Rows

                                        If CCommon.ToString(Mid(strName, strName.LastIndexOf("_") + 2)) = CCommon.ToString(drRow("vcDBColumnName")) AndAlso IsNumeric(Mid(strName, strName.LastIndexOf("_") + 2)) = False Then
                                            strCondition = strCondition & " AND COALESCE(" & CCommon.ToString(drRow("vcDBColumnName")) & ",0) IN (" & strValue & ")"

                                        ElseIf IsNumeric(Mid(strName, strName.LastIndexOf("_") + 2)) = True AndAlso String.IsNullOrEmpty(CCommon.ToString(drRow("vcDBColumnName"))) Then
                                            strCondition = strCondition & " AND i.numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & CCommon.ToLong(Mid(strName, strName.LastIndexOf("_") + 2)) & " and Fld_Value IN ('" & strValue & "'))"

                                        End If

                                    Next
                                End If

                            Next

                        End If
                    End If
                End With
                GridColumnSearchCriteria()

                objItems.RegularSearchCriteria = RegularSearch
                objItems.CustomSearchCriteria = CustomSearch
                objItems.strCondition = strCondition
                objItems.bitArchieve = chkArchivedItems.Checked
                objItems.byteMode = 0
                objItems.bitApplyFilter = hdnAppliedByFilter.Value
                objItems.WarehouseID = ddlFilterWarehouse.SelectedValue
                objItems.vcAsile = ddlAisle.SelectedValue
                objItems.vcRack = ddlRack.SelectedValue
                objItems.vcShelf = ddlShelf.SelectedValue
                objItems.vcBin = ddlBin.SelectedValue
                If (lngCustId <> 0) Then
                    objItems.bitMultiSelect = True
                Else
                    objItems.bitMultiSelect = False
                End If

                Dim ds As DataSet = objItems.GetItemOrKitList

                If Not isFromSearch Then
                    bizPager.PageSize = Session("PagingRows")
                    bizPager.RecordCount = objItems.TotalRecords
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                End If
                HideControls()
                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Function ReturnMoney(ByVal Money)
            Try
                Return Math.Round(Money, 2)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnSaveInventory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInventory.Click
            Try
                Dim ds As New DataSet
                Dim dt As New DataTable
                Dim dtSerial As New DataTable

                CCommon.AddColumnsToDataTable(dt, "numWareHouseItemID,numItemCode,intAdjust,vcItemName,monAverageCost,numAssetChartAcntId,numReorder")
                CCommon.AddColumnsToDataTable(dtSerial, "numItemCode,numWareHouseID,numWareHouseItemID,vcSerialNo,byteMode,numReorder")
                Dim drSerial As DataRow

                Dim sb As New System.Text.StringBuilder
                Dim flag As Boolean = False

                Dim bitSerialized As Boolean = 0
                Dim bitLotNo As Boolean = 0
                Dim OnHandQty As Decimal
                Dim Counted As Decimal
                Dim decAverageCost As Decimal
                Dim intDifference As Int32
                Dim decTotalAdjustmentAmount As Decimal = 0
                Dim lngItemCode As Long
                Dim AdjustmentQtySummary As Double = 0
                For Each dgGridItem As GridDataItem In gvSearch.Items
                    Dim strItemName As String = CCommon.ToString(CType(dgGridItem.FindControl("hdnItemName"), HiddenField).Value)
                    OnHandQty = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnAvailability"), HiddenField).Value)
                    Counted = CCommon.ToDecimal(CType(dgGridItem.FindControl("txtCounted"), TextBox).Text)
                    If CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnitCost"), TextBox).Text) > 0 Then
                        decAverageCost = CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnitCost"), TextBox).Text)
                    Else
                        decAverageCost = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnInventoryAverageCost"), HiddenField).Value)
                    End If
                    lngItemCode = CCommon.ToLong(CType(dgGridItem.FindControl("hdnItemCode"), HiddenField).Value)
                    bitSerialized = CCommon.ToBool(CType(dgGridItem.FindControl("hdnbitSerialized"), HiddenField).Value)
                    bitLotNo = CCommon.ToBool(CType(dgGridItem.FindControl("hdnbitLotNo"), HiddenField).Value)


                    If (Counted > 0 AndAlso Not bitSerialized AndAlso Not bitLotNo) Then
                        If CCommon.ToLong(CType(dgGridItem.FindControl("hdnAssetChartAcntId"), HiddenField).Value) = 0 Then
                            sb.Append(strItemName + "<br />")
                        End If
                        If Counted <> OnHandQty Then
                            intDifference = Counted - OnHandQty
                            decTotalAdjustmentAmount += decAverageCost * intDifference
                            AdjustmentQtySummary = AdjustmentQtySummary + intDifference
                            Dim dr As DataRow = dt.NewRow
                            dr("numWareHouseItemID") = CCommon.ToLong(CType(dgGridItem.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                            dr("numItemCode") = lngItemCode
                            dr("intAdjust") = intDifference
                            dr("vcItemName") = strItemName
                            dr("monAverageCost") = decAverageCost
                            dr("numAssetChartAcntId") = CCommon.ToLong(CType(dgGridItem.FindControl("hdnAssetChartAcntId"), HiddenField).Value)
                            dt.Rows.Add(dr)
                        End If
                    End If
                Next

                Dim objItem As New CItems
                objItem.ItemCode = lngItemCode
                objItem.DomainID = Session("DomainID")
                objItem.UserCntID = Session("UserContactID")
                objItem.ReorderQty = 0
                objItem.ReorderPoint = 0
                objItem.dtAdjustmentDate = System.DateTime.Now

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    'Update Quantity
                    If dt.Rows.Count > 0 Then
                        ds.Tables.Add(dt.Copy)
                        ds.Tables(0).TableName = "Item"

                        ds.Tables.Add(dtSerial.Copy)
                        ds.Tables(1).TableName = "SerialLotNo"

                        objItem.str = ds.GetXml()
                    End If

                    objItem.UpdateInventoryAdjustments()

                    If dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            objItem.MakeItemQtyAdjustmentJournal(CCommon.ToLong(dr("numItemCode")), CCommon.ToString(dr("vcItemName")), CCommon.ToDouble(dr("intAdjust")), CCommon.ToDecimal(dr("monAverageCost")), CCommon.ToLong(dr("numAssetChartAcntId")), Session("UserContactID"), Session("DomainID"), boolUseOpeningBalanceEqityAccount:=False, dtAdjustmentDate:=System.DateTime.Now)
                        Next
                    End If

                    objTransactionScope.Complete()
                End Using
                'End If
                'Dim strAdjustmentQtySummary As String
                'If AdjustmentQtySummary > 0 Then
                '    strAdjustmentQtySummary = "+" & AdjustmentQtySummary
                'Else
                '    strAdjustmentQtySummary = "-" & AdjustmentQtySummary
                'End If
                ShowSuccessMessage("Adjustment posted sucessfully", "Summary: " & AdjustmentQtySummary & " units, with " & decTotalAdjustmentAmount & " Asset Value")
                txtCurrrentPage.Text = 1
                hdnAppliedByFilter.Value = 1
                BindDatagrid()
                hdnAppliedByFilter.Value = 0
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub
        Private Sub btnFilterByWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilterByWarehouse.Click
            Try
                txtCurrrentPage.Text = 1
                hdnAppliedByFilter.Value = 1
                BindDatagrid()
                hdnAppliedByFilter.Value = 0
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                ddlFilterWarehouse.SelectedValue = "0"

                ddlAisle.Items.Clear()
                ddlAisle.Items.Insert(0, New ListItem("-Select-", ""))

                ddlRack.Items.Clear()
                ddlRack.Items.Insert(0, New ListItem("-Select-", ""))

                ddlShelf.Items.Clear()
                ddlShelf.Items.Insert(0, New ListItem("-Select-", ""))

                ddlBin.Items.Clear()
                ddlBin.Items.Insert(0, New ListItem("-Select-", ""))

                radCmbSearch.ClearSelection()
                radCmbSearch.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
            Try
                'Response.Redirect("../Items/frmKitDetails.aspx?frm=" & Page)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Function ReturnDate(ByVal CloseDate) As String
            Try
                If CloseDate Is Nothing Then Exit Function
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b></font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                        temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = strTargetResolveDate
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub HideControls()
            If GetQueryStringVal("Export") <> "" And (txtCurrrentPage.Text = "1" Or txtCurrrentPage.Text.Trim = "") Then
                btnExport.Visible = True
            Else
                btnExport.Visible = False
            End If
        End Sub

        Public Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try
                Dim obj As New CItems
                With obj
                    .DomainID = Session("DomainID")
                    .WarehouseID = ddlFilterWarehouse.SelectedValue
                End With
                Dim ds As DataSet = obj.GetItemExport
                Dim dtItem As DataTable
                dtItem = ds.Tables(0)

                Dim tablestr As String = ConvertDataTableToHTML(dtItem)

                Dim httpResponse As HttpResponse = Response
                Using wb As New XLWorkbook()
                    wb.Worksheets.Add(dtItem, "ItemList")

                    httpResponse.Clear()
                    httpResponse.Buffer = True
                    httpResponse.Charset = ""
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    httpResponse.AddHeader("content-disposition", "attachment;filename=DetailReport.xlsx")
                    Using MyMemoryStream As New MemoryStream()
                        wb.SaveAs(MyMemoryStream)
                        MyMemoryStream.WriteTo(httpResponse.OutputStream)
                        httpResponse.Flush()
                        httpResponse.End()
                    End Using
                End Using
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try

        End Sub

        Private Sub gvSearch_DetailTableDataBind(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles gvSearch.DetailTableDataBind
            Try
                Dim parentItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)

                If parentItem.Edit Then
                    Return
                End If

                If parentItem.GetDataKeyValue("charItemType").ToString() = "P" Then
                    Dim objItems As New CItems
                    Dim dsInventory As DataSet

                    objItems.ItemCode = Convert.ToInt32(parentItem.GetDataKeyValue("numItemCode").ToString())
                    objItems.byteMode = 1
                    dsInventory = objItems.GetItemWareHouses()
                    Dim dtTableInventory As DataTable
                    dtTableInventory = dsInventory.Tables(0)

                    For Each col As DataColumn In dtTableInventory.Columns
                        If col.ColumnName <> "numItemCode" AndAlso dtTableInventory.Columns.Contains(col.ColumnName & "1") = False Then
                            col.ColumnName = col.ColumnName & "1"
                        End If
                    Next
                    dtTableInventory.AcceptChanges()

                    For Each dr As DataRow In dsInventory.Tables(2).Rows
                        Dim Column As New GridBoundColumn
                        Column.HeaderText = dr("Fld_label").ToString()
                        Column.UniqueName = dr("Fld_label").ToString()
                        Column.DataField = dr("Fld_label").ToString() & "Value1"

                        'If dtTableInventory.Columns.Contains(col.ColumnName & "1") = False Then Continue For
                        e.DetailTableView.Columns.Add(Column)
                    Next

                    e.DetailTableView.DataSource = dtTableInventory

                Else
                    Dim dtTableInventory As New DataTable
                    dtTableInventory.Columns.Add("numItemCode", System.Type.GetType("System.Int32"))
                    dtTableInventory.Columns.Add("vcWareHouse1")
                    dtTableInventory.Columns.Add("OnHand1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("PurchaseOnHand1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("SalesOnHand1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("OnOrder1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("Reorder1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("Allocation1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("BackOrder1", System.Type.GetType("System.Double"))
                    dtTableInventory.Columns.Add("Price1")
                    dtTableInventory.Columns.Add("SKU1")
                    dtTableInventory.Columns.Add("BarCode1")

                    e.DetailTableView.DataSource = dtTableInventory
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub BindWareHouse()
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            ddlFilterWarehouse.Items.Clear()
            ddlFilterWarehouse.DataSource = objItem.GetWareHouses
            ddlFilterWarehouse.DataTextField = "vcWareHouse"
            ddlFilterWarehouse.DataValueField = "numWareHouseID"
            ddlFilterWarehouse.DataBind()
            ddlFilterWarehouse.Items.Insert(0, New ListItem("All", "0"))
        End Sub
        Private Sub ddlFilterWarehouse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilterWarehouse.SelectedIndexChanged
            Try
                BindInternalLocation()
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Public Sub BindInternalLocation()
            Dim objItem As New CItems
            Dim dt As New DataTable
            objItem.DomainID = Session("DomainID")
            objItem.WarehouseID = ddlFilterWarehouse.SelectedValue
            dt = objItem.GetWarehouseLocation
            ddlAisle.Items.Clear()
            Dim Aisles = (From p In dt.AsEnumerable() Select p("vcAisle")).Distinct()
            For Each d In Aisles
                ddlAisle.Items.Add(New ListItem(d, d))
            Next
            ddlAisle.Items.Insert(0, New ListItem("-Select-", ""))
            ddlRack.Items.Clear()
            Dim racks = (From p In dt.AsEnumerable() Select p("vcRack")).Distinct()
            For Each d In racks
                ddlRack.Items.Add(New ListItem(d, d))
            Next
            ddlRack.Items.Insert(0, New ListItem("-Select-", ""))
            ddlShelf.Items.Clear()
            Dim Shelfs = (From p In dt.AsEnumerable() Select p("vcShelf")).Distinct()
            For Each d In Shelfs
                ddlShelf.Items.Add(New ListItem(d, d))
            Next
            ddlShelf.Items.Insert(0, New ListItem("-Select-", ""))
            ddlBin.Items.Clear()
            Dim Bins = (From p In dt.AsEnumerable() Select p("vcBin")).Distinct()
            For Each d In Bins
                ddlBin.Items.Add(New ListItem(d, d))
            Next
            ddlBin.Items.Insert(0, New ListItem("-Select-", ""))
        End Sub
        Private Sub gvSearch_Init(sender As Object, e As EventArgs) Handles gvSearch.Init
            Try
                If ddlItemWarehouse.Items.Count = 0 Then
                    Dim objItem As New CItems
                    objItem.DomainID = Session("DomainID")
                    ddlItemWarehouse.DataSource = objItem.GetWareHouses
                    ddlItemWarehouse.DataTextField = "vcWareHouse"
                    ddlItemWarehouse.DataValueField = "numWareHouseID"
                    ddlItemWarehouse.DataBind()

                End If

                If Page.IsPostBack Then
                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSearch.NeedDataSource
            Try
                If Not e.IsFromDetailTable Then
                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSearch.PreRender
            'HideExpandColumnRecursive(gvSearch.MasterTableView)
        End Sub

        Public Sub HideExpandColumnRecursive(ByVal tableView As GridTableView)
            Dim nestedViewItems As GridItem() = tableView.GetItems(GridItemType.NestedView)
            For Each nestedViewItem As GridNestedViewItem In nestedViewItems
                For Each nestedView As GridTableView In nestedViewItem.NestedTableViews
                    If nestedView.Items.Count = 0 Then
                        Dim cell As TableCell = nestedView.ParentItem("ExpandColumn")
                        cell.Controls(0).Visible = False
                        cell.Text = " "
                        nestedViewItem.Visible = False
                    Else
                        Dim OnHand1, PurchaseOnHand1, SalesOnHand1, OnOrder1, Reorder1, Allocation1, BackOrder1 As Double
                        OnHand1 = 0
                        PurchaseOnHand1 = 0
                        SalesOnHand1 = 0
                        OnOrder1 = 0
                        Reorder1 = 0
                        Allocation1 = 0
                        BackOrder1 = 0
                        For Each gvr As GridDataItem In nestedView.Items
                            OnHand1 += CCommon.ToDouble(CType(gvr.FindControl("lblOnHand1"), Label).Text)
                            PurchaseOnHand1 += CCommon.ToDouble(CType(gvr.FindControl("lblPurchaseOnHand1"), Label).Text)
                            SalesOnHand1 += CCommon.ToDouble(CType(gvr.FindControl("lblSalesOnHand1"), Label).Text)
                            OnOrder1 += CCommon.ToDouble(CType(gvr.FindControl("lblOnOrder1"), Label).Text)
                            Reorder1 += CCommon.ToDouble(CType(gvr.FindControl("lblReorder1"), Label).Text)
                            Allocation1 += CCommon.ToDouble(CType(gvr.FindControl("lblAllocation1"), Label).Text)
                            BackOrder1 += CCommon.ToDouble(CType(gvr.FindControl("lblBackOrder1"), Label).Text)
                        Next

                        Dim footer As GridFooterItem = DirectCast(nestedView.GetItems(GridItemType.Footer)(0), GridFooterItem)

                        CType(footer.FindControl("lblOnHandTotal"), Label).Text = String.Format("{0:#,##0}", OnHand1)
                        CType(footer.FindControl("lblPurchaseOnHandTotal"), Label).Text = String.Format("{0:#,##0}", PurchaseOnHand1)
                        CType(footer.FindControl("lblSalesOnHandTotal"), Label).Text = String.Format("{0:#,##0}", SalesOnHand1)
                        CType(footer.FindControl("lblOnOrderTotal"), Label).Text = String.Format("{0:#,##0}", OnOrder1)
                        CType(footer.FindControl("lblReorderTotal"), Label).Text = String.Format("{0:#,##0}", Reorder1)
                        CType(footer.FindControl("lblAllocationTotal"), Label).Text = String.Format("{0:#,##0}", Allocation1)
                        CType(footer.FindControl("lblBackOrderTotal"), Label).Text = String.Format("{0:#,##0}", BackOrder1)
                    End If
                    If nestedView.HasDetailTables Then
                        HideExpandColumnRecursive(nestedView)
                    End If
                Next
            Next
        End Sub

        Private Sub LoadDynamicDropdowns()
            Try
                objContact.DomainID = Session("DomainId")
                objContact.FormId = 76
                objContact.UserCntID = Session("UserContactId")
                objContact.ContactType = 0

                dsFilter = objContact.GetColumnConfiguration
                If dsFilter.Tables.Count > 1 AndAlso dsFilter.Tables(1) IsNot Nothing Then
                    If dsFilter.Tables(1).Rows.Count > 0 Then
                        For Each drItem As DataRow In dsFilter.Tables(1).Rows

                            Dim radSearch As New RadComboBox
                            radSearch.ID = If(String.IsNullOrEmpty(CCommon.ToString(drItem("vcDbColumnName"))), CCommon.ToString(drItem("numFieldID1")), CCommon.ToString(drItem("vcDbColumnName")))
                            radSearch.CheckBoxes = True
                            radSearch.Width = 150
                            radSearch.DropDownWidth = 200
                            radSearch.ZIndex = "999"
                            radSearch.EnableEmbeddedSkins = False
                            radSearch.Skin = "Black"
                            radSearch.EnableTextSelection = False
                            radSearch.ShowDropDownOnTextboxClick = True
                            radSearch.AllowCustomText = False
                            radSearch.CloseDropDownOnBlur = True
                            radSearch.EnableViewState = False
                            'radSearch.Label = CCommon.ToString(drItem("vcFieldName"))
                            radSearch.Attributes.Add("style", "margin-left:3px")
                            radSearch.EmptyMessage = CCommon.ToString(drItem("vcFieldName"))
                            radSearch.OnClientItemChecked = "OnClientItemChecked"
                            radSearch.OnClientBlur = "OnClientBlur"

                            Dim dtData As DataTable
                            dtData = GetDropDownData(CCommon.ToString(drItem("vcDbColumnName")), CCommon.ToString(drItem("numListID")))
                            radSearch.Items.Clear()
                            If dtData IsNot Nothing AndAlso dtData.Rows.Count > 0 Then
                                For i As Integer = 0 To dtData.Rows.Count - 1
                                    radSearch.Items.Add(New RadComboBoxItem(CCommon.ToString(dtData.Rows(i)(1)), CCommon.ToString(dtData.Rows(i)(0))))
                                Next
                            End If

                            radSearch.FooterTemplate = New FooterTemplate()
                            radSearch.Items.Insert(0, New RadComboBoxItem("None", "0"))
                            tdMain.Controls.Add(radSearch)
                        Next
                        btnFilter.Visible = True

                    Else
                        btnFilter.Visible = False

                    End If
                End If
            Catch ex As Exception
            End Try
        End Sub

        Function GetDropDownData(ByVal DBColumnName As String, ByVal ListID As Long) As DataTable
            Try
                Dim dtData As New DataTable

                If DBColumnName = "numShipClass" OrElse DBColumnName = "numItemClassification" OrElse DBColumnName = "numItemClass" Then
                    dtData = objCommon.GetMasterListItems(ListID, CCommon.ToLong(HttpContext.Current.Session("DomainID")))

                ElseIf DBColumnName = "numItemGroup" Then
                    Dim objItems As New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = 0
                    dtData = objItems.GetItemGroups.Tables(0)

                ElseIf DBColumnName = "numCategoryID" Then
                    Dim objItems As New CItems
                    objItems.byteMode = 18
                    objItems.DomainID = Session("DomainID")
                    objItems.SiteID = IIf(Not Session("SiteID") Is Nothing, Session("SiteID"), 0)
                    dtData = objItems.SeleDelCategory

                ElseIf ListID > 0 Then
                    dtData = objCommon.GetMasterListItems(ListID, CCommon.ToLong(HttpContext.Current.Session("DomainID")))

                End If

                Return dtData
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnFilter.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList

                    For i As Integer = 0 To strValues.Length - 1
                        If (strValues(i).Contains("vcAttributeIDs")) Then
                            strIDValue = strValues(i).Split(":")
                            strID = strIDValue(0).Split("~")
                        Else
                            strIDValue = strValues(i).Split(":")
                            strID = strIDValue(0).Split("~")
                        End If

                        If strIDValue(1).ToString <> "" Then

                            If strID(0).Contains("CFW.Cust") Then
                                Select Case strID(3).Trim()
                                    Case "TextBox", "TextArea"
                                        strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " And CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " And CFW.Fld_Value " & " ILike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    Case "CheckBox"
                                        If strIDValue(1).ToLower() = "yes" Then
                                            strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                        ElseIf strIDValue(1).ToLower() = "no" Then
                                            strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values_Item CFWInner WHERE CFWInner.RecId=Item.numItemCode AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                        End If
                                    Case "SelectBox"
                                        If (strIDValue(1).Contains(",")) Then
                                            If strID(4) = 9 Then
                                                strCustomCondition.Add(" I.numItemCode in (select distinct numItemCode from ItemAttributes IA where IA.numDomainId=" & Session("DomainID") & " AND IA.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and IA.Fld_Value IN (SELECT outparam FROM SplitString('" & strIDValue(1) & "', ',') ) )")
                                            Else
                                                strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value IN (SELECT outparam FROM SplitString('" & strIDValue(1) & "', ',') ) )")
                                            End If
                                        Else
                                            If strID(4) = 9 Then
                                                strCustomCondition.Add(" I.numItemCode in (select distinct numItemCode from ItemAttributes IA where IA.numDomainId=" & Session("DomainID") & " AND IA.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and IA.Fld_Value " & "=" & strIDValue(1) & ")")
                                            Else
                                                strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                            End If
                                        End If
                                    Case "DateField"
                                        If strID(4) = "From" Then
                                            Dim fromDate As Date
                                            If Date.TryParse(strIDValue(1), fromDate) Then
                                                strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                            End If
                                        ElseIf strID(4) = "To" Then
                                            Dim toDate As Date
                                            If Date.TryParse(strIDValue(1), toDate) Then
                                                strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                            End If
                                        End If
                                    Case "CheckBoxList"
                                        Dim items As String() = strIDValue(1).Split(",")
                                        Dim searchString As String = ""

                                        For Each item As String In items
                                            searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueItem(" & strID(0).Replace("CFW.Cust", "") & ",Item.numItemCode) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                        Next

                                        strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                    Case Else
                                        strCustomCondition.Add(" I.numItemCode in (select distinct Item.numItemCode from Item left join CFW_Fld_Values_Item CFW ON Item.numItemCode=CFW.RecId where Item.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                                End Select
                            Else
                                Select Case strID(3).Trim()
                                    Case "Website", "Email", "TextBox", "Label"
                                        If strID(0) = "I.monAverageCost" Or
                                            strID(0) = "V.monCost" Or
                                            strID(0) = "V.intMinQty" Or
                                            strID(0) = "I.fltWidth" Or
                                            strID(0) = "I.fltHeight" Or
                                            strID(0) = "I.fltWeight" Or
                                            strID(0) = "I.fltLength" Then
                                            strRegularCondition.Add(strID(0) & " = " & strIDValue(1).Replace("'", "''"))
                                        Else
                                            strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                        End If
                                    Case "SelectBox"
                                        If (strIDValue(1).Contains(",")) Then
                                            If strID(0) = "OL.numWebApiId" Then
                                                If strIDValue(1).StartsWith("Sites~") Then
                                                    strRegularCondition.Add("OL.numSiteID=" & strIDValue(1).Replace("Sites~", ""))
                                                Else
                                                    strRegularCondition.Add(strID(0) & "IN (SELECT Id FROM SplitIDs('" & strIDValue(1) & "', ',') )")
                                                End If
                                            ElseIf strID(0) = "Opp.tintSource" Then
                                                strRegularCondition.Add("Opp.tintSource=" & strIDValue(1).ToString.Split("~")(0) & " and Opp.tintSourceType=" & strIDValue(1).ToString.Split("~")(1))
                                            ElseIf strID(0) = "vcAttributeIDs" Then
                                                ' strRegularCondition.Add("Stuff((SELECT N',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=72 AND numItemCode=I.numItemCode FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,1,N'') =" & "'" + strIDValue(1).ToString + "'")
                                                'strRegularCondition.Add("I.numItemCode in (select distinct ItemAttributes.numItemCode from ItemAttributes where " + strIDValue(1).ToString)
                                                strRegularCondition.Add("I.numItemCode in (select distinct ItemAttributes.numItemCode from ItemAttributes where FLD_ID In (SELECT Id From SplitIDs(('" + strIDValue(1).ToString + "'), ',') ) and FLD_Value IN (SELECT outparam FROM SplitString(('" + strIDValue(2).ToString + "'), ',') ))")

                                            Else
                                                strRegularCondition.Add(strID(0) & " IN (SELECT Id FROM SplitIDs('" & strIDValue(1) & "', ',') )")
                                            End If
                                        Else
                                            If strID(0) = "OL.numWebApiId" Then
                                                If strIDValue(1).StartsWith("Sites~") Then
                                                    strRegularCondition.Add("OL.numSiteID=" & strIDValue(1).Replace("Sites~", ""))
                                                Else
                                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                                End If
                                            ElseIf strID(0) = "Opp.tintSource" Then
                                                strRegularCondition.Add("Opp.tintSource=" & strIDValue(1).ToString.Split("~")(0) & " and Opp.tintSourceType=" & strIDValue(1).ToString.Split("~")(1))
                                            ElseIf strID(0) = "vcAttributeIDs" Then
                                                'strRegularCondition.Add("Stuff((SELECT N',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=72 AND numItemCode=I.numItemCode FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,1,N'') =" & "'" + strIDValue(1).ToString + "'")
                                                strRegularCondition.Add("I.numItemCode in (select distinct ItemAttributes.numItemCode from ItemAttributes where FLD_ID In (SELECT Id From SplitIDs(('" + strIDValue(1).ToString + "'), ',') ) and FLD_Value IN (SELECT outparam FROM SplitString(('" + strIDValue(2).ToString + "'), ',') ))")
                                            Else
                                                strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                            End If
                                        End If
                                    Case "TextArea"
                                        strRegularCondition.Add(" Cast(" & strID(0) & " as varchar) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    Case "CheckBox"
                                        If strID(0) = ".WorkOrder" Then
                                            If strIDValue(1).ToLower() = "yes" Or strIDValue(1).ToLower() = "true" Then
                                                strRegularCondition.Add("(SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0")
                                            Else
                                                strRegularCondition.Add("COALESCE((SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0),0) = 0")
                                            End If
                                        End If
                                    Case "DateField"
                                        If strID(4) = "From" Then
                                            Dim fromDate As Date
                                            If Date.TryParse(strIDValue(1), fromDate) Then
                                                strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                            End If
                                        ElseIf strID(4) = "To" Then
                                            Dim toDate As Date
                                            If Date.TryParse(strIDValue(1), toDate) Then
                                                strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                            End If
                                        End If
                                End Select
                            End If

                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
                If GetQueryStringVal("Export") = "True" Then
                    RegularSearch = " (COALESCE(WarehouseItems.numOnHand,0) + COALESCE(WarehouseItems.numAllocation,0) >0) and (COALESCE(I.monAverageCost,0)>0) "
                End If
                If GetQueryStringVal("CompanyName") = "numVendorID" AndAlso CCommon.ToLong(GetQueryStringVal("SearchText")) > 0 Then
                    RegularSearch = IIf(RegularSearch.Length > 0, RegularSearch & " AND ", "") & " V.numVendorID = " & CCommon.ToLong(GetQueryStringVal("SearchText"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Class FooterTemplate
            Implements ITemplate

            Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
                Dim table As New HtmlTable()
                Dim row As New HtmlTableRow()
                Dim cell As New HtmlTableCell()
                Dim chkAll As New CheckBox
                chkAll.Text = "Select All"
                chkAll.Attributes.Add("onclick", "CheckAll('" & container.NamingContainer.ID & "',this)")
                cell.Controls.Add(chkAll)
                row.Controls.Add(cell)
                table.Controls.Add(row)
                container.Controls.Add(table)


            End Sub

        End Class

        Private Sub chkArchivedItems_CheckedChanged(sender As Object, e As EventArgs) Handles chkArchivedItems.CheckedChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub imgCartAdd_Click(sender As Object, e As ImageClickEventArgs) Handles imgCartAdd.Click
            Try

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnReload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReload.Click

            Try
                If CCommon.ToString(hdnItemCodes.Value).Length > 0 Then
                    Dim strItems As String() = CCommon.ToString(hdnItemCodes.Value).Trim(",").Split(",")
                    Dim strItemCode As String()
                    Dim dsItems As DataSet = Session("SOItems")

                    Dim lngWarehouseID As Long = 0
                    Dim lngWarehouseItemID As Long = 0
                    Dim dblPrice As Double = 0
                    Dim strWarehouse As String = ""

                    For Each strItem As String In strItems
                        strItemCode = strItem.Split(":")

                        lngWarehouseID = 0
                        lngWarehouseItemID = 0
                        dblPrice = 0
                        strWarehouse = ""

                        If strItem.Length > 0 Then
                            lngWarehouseID = If(strItemCode(1).Split("~").Length > 0, CCommon.ToLong(strItemCode(1).Split("~")(0)), 0)
                            lngWarehouseItemID = If(strItemCode(1).Split("~").Length > 1, CCommon.ToLong(strItemCode(1).Split("~")(1)), 0)
                            dblPrice = If(strItemCode(1).Split("~").Length > 2, CCommon.ToDouble(strItemCode(1).Split("~")(2)), 0)
                            strWarehouse = If(strItemCode(1).Split("~").Length > 3, CCommon.ToString(strItemCode(1).Split("~")(3)), "")

                            If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                                Dim dr() As DataRow
                                'dr = dsItems.Tables(0).Select("numItemCode='" & CCommon.ToLong(strItemCode(0)) & "'")
                                dr = dsItems.Tables(0).Select(" numItemCode='" & CCommon.ToLong(strItemCode(0)) & "'" & " and numWarehouseItmsID = '" & IIf(lngWarehouseItemID > 0, lngWarehouseItemID, 0) & "'")
                                If dr.Length > 0 Then
                                    dr(0)("numUnitHour") = CCommon.ToLong(dr(0)("numUnitHour")) + 1
                                    dr(0)("numWarehouseID") = If(lngWarehouseID > 0, lngWarehouseID, CCommon.ToLong(ddlItemWarehouse.SelectedValue))
                                    dr(0)("monPrice") = If(dblPrice > 0, dblPrice, CCommon.ToDouble(dr(0)("monPrice")))

                                    dr(0)("numWarehouseItmsID") = lngWarehouseItemID
                                    dr(0)("Warehouse") = If(strWarehouse = "", ddlItemWarehouse.SelectedItem.Text, "")
                                    dsItems.Tables(0).AcceptChanges()
                                    Session("SOItems") = dsItems
                                Else
                                    AddItemToSession(CCommon.ToLong(strItemCode(0)), 1, dblPrice, lngWarehouseID, lngWarehouseItemID, strWarehouse)
                                End If
                            Else
                                AddItemToSession(CCommon.ToLong(strItemCode(0)), 1, dblPrice, lngWarehouseID, lngWarehouseItemID, strWarehouse)
                            End If
                        End If
                    Next

                End If

                If Session("SOItems") IsNot Nothing AndAlso DirectCast(Session("SOItems"), DataSet).Tables(0).Rows.Count > 0 Then
                    btnOpenOrder.Text = "Open Order (" & DirectCast(Session("SOItems"), DataSet).Tables(0).Rows.Count & ")"
                Else
                    btnOpenOrder.Text = "Open Order"
                End If

                BindDatagrid()
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "BindGrid", "<script>BindOrderGrid();</script>", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try

        End Sub

        Dim dsTemp As DataSet
        Dim objItems As CItems
        Private Sub AddItemToSession(ByVal lngItemCode As Long,
                                     ByVal dcQty As Decimal,
                                     ByVal dcPrice As Decimal,
                                     ByVal lngWarehouseId As Long,
                                     ByVal lngItemWarehouseID As Long,
                                     ByVal strWarehouse As String)
            Try
                If dsTemp Is Nothing AndAlso Session("SOItems") Is Nothing Then
                    createSet()
                ElseIf dsTemp Is Nothing AndAlso Session("SOItems") IsNot Nothing Then
                    dsTemp = Session("SOItems")
                End If

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = lngItemCode
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                Dim dtTable, dtItemTax As DataTable
                Dim dsItemWarehouses As New DataSet
                If objItems Is Nothing Then objItems = New CItems
                Dim strUnit As String = ""
                Dim objOpportunity As New MOpportunity
                'Dim lngItemWarehouseID As Long = 0
                objOpportunity.OppItemCode = 0 'CCommon.ToLong(txtHidEditOppItem.Text)
                objOpportunity.OpportunityId = 0 'CCommon.ToLong(GetQueryStringVal("opid"))
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.Mode = 2
                objItems.ItemCode = lngItemCode
                dtTable = objItems.ItemDetails

                objItems.byteMode = 1
                If lngItemWarehouseID = 0 Then
                    dsItemWarehouses = objItems.GetItemWareHouses()
                    If dsItemWarehouses.Tables.Count > 0 AndAlso dsItemWarehouses.Tables(0).Rows.Count > 0 Then
                        Dim drWarehouse() As DataRow = dsItemWarehouses.Tables(0).Select("numWareHouseID = " & CCommon.ToLong(ddlItemWarehouse.SelectedValue))
                        If drWarehouse IsNot Nothing AndAlso drWarehouse.Length > 0 Then
                            lngItemWarehouseID = CCommon.ToLong(drWarehouse(0)("numWareHouseItemID"))
                        Else
                            If litMessage.Text.Contains("Warehouse is not found for '" & objItems.ItemName & "'") = False Then
                                litMessage.Text = "<p>" & litMessage.Text & "Warehouse is not found for '" & objItems.ItemName & "'" & "</p>"
                            End If
                            Return
                        End If
                    End If
                End If

                If IsDuplicate(lngItemCode, lngItemWarehouseID) Then
                    '    If litMessage.Text.Contains("Item with itemcode " & lngItemCode & " is already exists in order.'") = False Then
                    '        litMessage.Text = "<p>" & litMessage.Text & "Item with itemcode " & lngItemCode & " is already exists in order.'" & "</p>"
                    '    End If
                    '    Return
                    Dim dr() As DataRow
                    'dr = dsItems.Tables(0).Select("numItemCode='" & CCommon.ToLong(strItemCode(0)) & "'")
                    dr = dsTemp.Tables(0).Select(" numItemCode='" & lngItemCode & "'" & " and numWarehouseItmsID = " & lngItemWarehouseID)
                    If dr.Length > 0 Then
                        dr(0)("numUnitHour") = CCommon.ToLong(dr(0)("numUnitHour")) + 1
                        dr(0)("numWarehouseID") = If(lngWarehouseId > 0, lngWarehouseId, CCommon.ToLong(ddlItemWarehouse.SelectedValue))
                        dr(0)("monPrice") = dcPrice
                        dr(0)("numWarehouseItmsID") = lngItemWarehouseID
                        dr(0)("Warehouse") = If(strWarehouse = "", ddlItemWarehouse.SelectedItem.Text, "")
                        dsItems.Tables(0).AcceptChanges()
                        Session("SOItems") = dsTemp
                        Return
                    End If
                End If

                If (hdnOppType.Value = 2) Then
                    strUnit = "numPurchaseUnit"
                ElseIf (hdnOppType.Value = 1) Then
                    strUnit = "numSaleUnit"
                End If

                Dim strApplicable As String = ""
                If (hdnOppType.Value = 1 And hdnOppType.Value = 1) Then
                    objItems.DomainID = Session("DomainID")
                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next
                End If

                'strUnit = "numSaleUnit"
                'objItems.DomainID = Session("DomainID")
                'dtItemTax = objItems.ItemTax()
                'For Each dr As DataRow In dtItemTax.Rows
                '    strApplicable = strApplicable & CCommon.ToBool(dr("bitApplicable")) & ","
                'Next

                If dtTable.Rows.Count > 0 Then

                    Dim PageType As Integer
                    If hdnOppType.Value = 1 Then
                        PageType = 1
                        strApplicable = strApplicable & CCommon.ToBool(dtTable.Rows(0).Item("bitTaxable"))
                    End If

                    objItems.ItemName = CCommon.ToString(dtTable.Rows(0).Item("vcItemName"))
                    objItems.type = CCommon.ToInteger(dtTable.Rows(0).Item("tintStandardProductIDType"))
                    objItems.ItemDesc = CCommon.ToString(dtTable.Rows(0).Item("txtItemDesc"))
                    'objItems.Description = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    objItems.ModelID = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    objItems.PathForImage = CCommon.ToString(dtTable.Rows(0).Item("vcPathForImage"))
                    objItems.ItemType = CCommon.ToString(dtTable.Rows(0).Item("ItemType"))

                    objItems.AllowDropShip = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDropShip"))
                    objItems.VendorID = CCommon.ToLong(dtTable.Rows(0).Item("numVendorID"))
                    objItems.IsPrimaryVendor = True

                    objItems.Weight = CCommon.ToDouble(dtTable.Rows(0).Item("fltWeight"))
                    objItems.FreeShipping = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShipping"))
                    objItems.Height = CCommon.ToDouble(dtTable.Rows(0).Item("fltHeight"))
                    objItems.Width = CCommon.ToDouble(dtTable.Rows(0).Item("fltWidth"))
                    objItems.Length = CCommon.ToDouble(dtTable.Rows(0).Item("fltLength"))
                    objItems.ListPrice = If(dcPrice = 0, CCommon.ToDouble(dtTable.Rows(0).Item("monListPrice")), dcPrice)
                    objItems.SKU = CCommon.ToString(dtTable.Rows(0).Item("vcSKU"))

                    Dim dblUOMConversionFactor, dblPurchaseUOMConversionFactor, dblSalesUOMConversionFactor As Double
                    If strUnit.Length > 0 Then
                        objItems.UnitofMeasure = dtTable.Rows(0).Item("vcUnitofMeasure")

                        objItems.BaseUnit = CCommon.ToLong(dtTable.Rows(0).Item("numBaseUnit"))
                        objItems.PurchaseUnit = CCommon.ToLong(dtTable.Rows(0).Item("numPurchaseUnit"))
                        objItems.SaleUnit = CCommon.ToLong(dtTable.Rows(0).Item("numSaleUnit"))

                        objCommon.DomainID = Session("DomainID")
                        objCommon.ItemCode = objItems.ItemCode
                        objCommon.UnitId = CCommon.ToLong(dtTable.Rows(0).Item("numSaleUnit"))
                        dtUnit = objCommon.GetItemUOMConversion()
                        dblUOMConversionFactor = CCommon.ToDouble(dtUnit(0)("BaseUOMConversion"))
                        dblPurchaseUOMConversionFactor = dtUnit(0)("PurchaseUOMConversion")
                        dblSalesUOMConversionFactor = dtUnit(0)("SaleUOMConversion")

                    End If

                    objCommon = New CCommon
                    Dim objItem As New CItems
                    If Not objItem.ValidateItemAccount(lngItemCode, Session("DomainID"), 1) = True Then
                        litMessage.Text = "<p>" & litMessage.Text & "Please Set Income,Asset,COGs(Expense) Account for '" & objItems.ItemName & "' Item from Administration->Inventory->Item Details" & "</p>"
                        Return
                    End If

                    Dim dtTaxTypes As New DataTable
                    Dim ObjTaxItems As New TaxDetails
                    Dim chkTaxItems As New CheckBoxList
                    Dim strTax As String = ""
                    If CCommon.ToShort(hdnOppType.Value) = 1 Then
                        ObjTaxItems.DomainID = Session("DomainID")
                        dtTaxTypes = ObjTaxItems.GetTaxItems

                        For Each drTax As DataRow In dtTaxTypes.Rows
                            dtTable.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                            dtTable.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                        Next

                        Dim dr As DataRow
                        dr = dtTaxTypes.NewRow
                        dr("numTaxItemID") = 0
                        dr("vcTaxName") = "Sales Tax(Default)"
                        dtTaxTypes.Rows.Add(dr)

                        chkTaxItems.DataTextField = "vcTaxName"
                        chkTaxItems.DataValueField = "numTaxItemID"
                        chkTaxItems.DataSource = dtTaxTypes
                        chkTaxItems.DataBind()
                        objCommon = New CCommon
                        For Each Item As ListItem In chkTaxItems.Items
                            If Item.Selected = True Then
                                If Session("BaseTaxCalcOn") = 1 Then 'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                    strTax = strTax & objCommon.TaxPercentage(0, CType(Me.Parent.FindControl("ddlBillCountry"), DropDownList).SelectedValue, CType(Me.Parent.FindControl("ddlBillState"), DropDownList).SelectedValue, Session("DomainID"), Item.Value, CType(Me.Parent.FindControl("txtCity"), TextBox).Text, CType(Me.Parent.FindControl("txtPostal"), TextBox).Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                                Else
                                    strTax = strTax & objCommon.TaxPercentage(0, CType(Me.Parent.FindControl("ddlShipCountry"), DropDownList).SelectedValue, CType(Me.Parent.FindControl("ddlShipState"), DropDownList).SelectedValue, Session("DomainID"), Item.Value, CType(Me.Parent.FindControl("txtShipCity"), TextBox).Text, CType(Me.Parent.FindControl("txtShipPostal"), TextBox).Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                                End If
                            Else
                                strTax = strTax & "0#1,"
                            End If
                        Next
                        strTax = strTax.TrimEnd(",")
                    End If

                    If Not dsTemp.Tables(0).Columns.Contains("Op_Flag") Then dsTemp.Tables(0).Columns.Add("Op_Flag")
                    If Not dsTemp.Tables(0).Columns.Contains("bitIsAuthBizDoc") Then dsTemp.Tables(0).Columns.Add("bitIsAuthBizDoc")
                    If Not dsTemp.Tables(0).Columns.Contains("numUnitHourReceived") Then dsTemp.Tables(0).Columns.Add("numUnitHourReceived")
                    If Not dsTemp.Tables(0).Columns.Contains("numQtyShipped") Then dsTemp.Tables(0).Columns.Add("numQtyShipped")
                    If Not dsTemp.Tables(0).Columns.Contains("numUOM") Then dsTemp.Tables(0).Columns.Add("numUOM")
                    If Not dsTemp.Tables(0).Columns.Contains("vcUOMName") Then dsTemp.Tables(0).Columns.Add("vcUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("UOMConversionFactor") Then dsTemp.Tables(0).Columns.Add("UOMConversionFactor")
                    If Not dsTemp.Tables(0).Columns.Contains("monTotAmtBefDiscount") Then dsTemp.Tables(0).Columns.Add("monTotAmtBefDiscount")
                    If Not dsTemp.Tables(0).Columns.Contains("vcBaseUOMName") Then dsTemp.Tables(0).Columns.Add("vcBaseUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("bitDiscountType") Then dsTemp.Tables(0).Columns.Add("bitDiscountType")
                    If Not dsTemp.Tables(0).Columns.Contains("numVendorWareHouse") Then dsTemp.Tables(0).Columns.Add("numVendorWareHouse")
                    If Not dsTemp.Tables(0).Columns.Contains("numShipmentMethod") Then dsTemp.Tables(0).Columns.Add("numShipmentMethod")
                    If Not dsTemp.Tables(0).Columns.Contains("numSOVendorId") Then dsTemp.Tables(0).Columns.Add("numSOVendorId")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseID") Then dsTemp.Tables(0).Columns.Add("numWarehouseID")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseItmsID") Then dsTemp.Tables(0).Columns.Add("numWarehouseItmsID")

                    Session("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, "P", objItems.ItemType,
                                                                                         objItems.AllowDropShip, objItems.KitParent, lngItemCode, dcQty,
                                                                                         CCommon.ToDecimal(objItems.ListPrice), objItems.ItemDesc,
                                                                                         If(lngItemWarehouseID = 0, CCommon.ToLong(ddlItemWarehouse.SelectedValue), lngItemWarehouseID),
                                                                                         objItems.ItemName, If(strWarehouse = "", ddlItemWarehouse.SelectedItem.Text, ""),
                                                                                         0, objItems.ModelID.Trim, If(CCommon.ToShort(hdnOppType.Value) = 1, objItems.SaleUnit, objItems.PurchaseUnit),
                                                                                         objItems.UnitofMeasure, If(dblUOMConversionFactor = 0, 1, dblUOMConversionFactor),
                                                                                         If(CCommon.ToShort(hdnOppType.Value) = 1, "Sales", "Purchase"), 0, 0, strApplicable, strTax, "", chkTaxItems,
                                                                                         0, "", "", 0, 0, 0, CCommon.ToLong(objItems.VendorID), 0, 0,
                                                                                         ToWarehouseItemID:=CCommon.ToLong(ddlItemWarehouse.SelectedValue),
                                                                                         vcBaseUOMName:=objItems.UnitofMeasure,
                                                                                         numPrimaryVendorID:=CCommon.ToLong(objItems.VendorID),
                                                                                         fltItemWeight:=CCommon.ToLong(objItems.Weight),
                                                                                         IsFreeShipping:=CCommon.ToBool(objItems.FreeShipping),
                                                                                         fltHeight:=CCommon.ToDouble(objItems.Height),
                                                                                         fltWidth:=CCommon.ToDouble(objItems.Width),
                                                                                         fltLength:=CCommon.ToDouble(objItems.Length),
                                                                                         strSKU:=CCommon.ToString(objItems.SKU))


                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub createSet()
            Try
                If Not IsPostBack And Not Session("SOItems") Is Nothing Then
                    Dim ds1 As DataSet = Session("SOItems")
                    If ds1.Tables(0).Rows.Count > 0 Then
                        Exit Sub
                    End If
                End If
                dsTemp = New DataSet
                Dim dtItem As New DataTable
                Dim dtSerItem As New DataTable
                Dim dtChildItems As New DataTable
                dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("numItemCode")
                dtItem.Columns.Add("numUnitHour")

                dtItem.Columns.Add("monPrice")
                dtItem.Columns.Add("numUOM")
                dtItem.Columns.Add("vcUOMName")
                dtItem.Columns.Add("UOMConversionFactor")

                dtItem.Columns.Add("monTotAmount", GetType(Decimal))
                dtItem.Columns.Add("numSourceID")
                dtItem.Columns.Add("vcItemDesc")
                dtItem.Columns.Add("vcModelID")
                dtItem.Columns.Add("numWarehouseID")
                dtItem.Columns.Add("vcItemName")
                dtItem.Columns.Add("Warehouse")
                dtItem.Columns.Add("numWarehouseItmsID")
                dtItem.Columns.Add("ItemType")
                dtItem.Columns.Add("Attributes")
                dtItem.Columns.Add("Op_Flag")
                dtItem.Columns.Add("bitWorkOrder")
                dtItem.Columns.Add("vcInstruction")
                dtItem.Columns.Add("bintCompliationDate")
                dtItem.Columns.Add("numWOAssignedTo")

                dtItem.Columns.Add("DropShip", GetType(Boolean))
                dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
                dtItem.Columns.Add("fltDiscount", GetType(Decimal))
                dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

                dtItem.Columns.Add("bitTaxable0")
                dtItem.Columns.Add("Tax0", GetType(Decimal))

                dtItem.Columns.Add("numVendorWareHouse")
                dtItem.Columns.Add("numShipmentMethod")
                dtItem.Columns.Add("numSOVendorId")

                dtItem.Columns.Add("numProjectID")
                dtItem.Columns.Add("numProjectStageID")
                dtItem.Columns.Add("charItemType")
                dtItem.Columns.Add("numToWarehouseItemID")
                dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
                dtItem.Columns.Add("numUnitHourReceived")
                dtItem.Columns.Add("numQtyShipped")
                dtItem.Columns.Add("vcBaseUOMName")
                dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("fltItemWeight")
                dtItem.Columns.Add("IsFreeShipping")
                dtItem.Columns.Add("fltHeight")
                dtItem.Columns.Add("fltWidth")
                dtItem.Columns.Add("fltLength")
                dtItem.Columns.Add("vcSKU")

                If (1 = 1) Then
                    '''Loading Other Taxes
                    Dim dtTaxTypes As DataTable
                    Dim ObjTaxItems As New TaxDetails
                    ObjTaxItems.DomainID = Session("DomainID")
                    dtTaxTypes = ObjTaxItems.GetTaxItems

                    For Each drTax As DataRow In dtTaxTypes.Rows
                        dtItem.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                        dtItem.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                    Next

                    Dim dr As DataRow
                    dr = dtTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dtTaxTypes.Rows.Add(dr)
                End If

                dtSerItem.Columns.Add("numWarehouseItmsDTLID")
                dtSerItem.Columns.Add("numWItmsID")
                dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtSerItem.Columns.Add("vcSerialNo")
                dtSerItem.Columns.Add("Comments")
                dtSerItem.Columns.Add("Attributes")

                dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numItemCode")
                dtChildItems.Columns.Add("numQtyItemsReq")
                dtChildItems.Columns.Add("vcItemName")
                dtChildItems.Columns.Add("monListPrice")
                dtChildItems.Columns.Add("UnitPrice")
                dtChildItems.Columns.Add("charItemType")
                dtChildItems.Columns.Add("txtItemDesc")
                dtChildItems.Columns.Add("numWarehouseItmsID")
                dtChildItems.Columns.Add("Op_Flag")
                dtChildItems.Columns.Add("ItemType")
                dtChildItems.Columns.Add("Attr")
                dtChildItems.Columns.Add("vcWareHouse")

                dtItem.TableName = "Item"
                dtSerItem.TableName = "SerialNo"
                dtChildItems.TableName = "ChildItems"

                dsTemp.Tables.Add(dtItem)
                dsTemp.Tables.Add(dtSerItem)
                dsTemp.Tables.Add(dtChildItems)
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
                dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
                Session("SOItems") = dsTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function IsDuplicate(ByVal ItemCode As String, ByVal WareHouseItemID As String) As Boolean
            'validate duplicate entry on refresh / when js is disabled
            If (dsTemp.Tables(0).Select(" numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID <> "", " and numWarehouseItmsID = '" & WareHouseItemID & "'", "")).Length > 0) Then
                Return True
            End If
            Return False
        End Function

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub ShowSuccessMessage(ByVal message As String, ByVal summarymessage As String)
            Try
                divSuccessMessage.Style.Add("display", "")
                litSuccessmessage.Text = message
                litSummaryMessage.Text = summarymessage
                divSuccessMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
            Try
                If txtDelItemIds.Text.Trim().Length > 1 Then
                    Dim strItemId As String() = txtDelItemIds.Text.Trim(",").Split(",")
                    Dim objItems As New CItems
                    'Dim ds As DataSet
                    'Dim strItemName As String
                    'Dim decAverageCost As Decimal
                    'Dim lngAssetCOA As Long
                    Dim dtItemImages As DataTable
                    'Dim dtItemDetails As DataTable
                    For i As Integer = 0 To strItemId.Length - 1
                        objItems.ItemCode = strItemId(i)
                        objItems.DomainID = Session("DomainID")
                        dtItemImages = objItems.ImageItemDetails()

                        objItems.DeleteItems()

                        Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & CCommon.ToString(objItems.ItemCode) & ".xml"
                        If System.IO.File.Exists(FilePath) Then
                            System.IO.File.Delete(FilePath)
                        End If

                        If dtItemImages.Rows.Count > 0 Then
                            For Each row As DataRow In dtItemImages.Rows
                                If row("vcPathForImage") <> "" Then
                                    If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage")) Then
                                        File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage"))
                                    End If
                                End If
                                If row("vcPathForTImage") <> "" Then
                                    If File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage")) Then
                                        File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage"))
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If

            Catch ex As Exception
                Select Case ex.Message
                    Case "OpportunityItems_Depend"
                        ShowMessage("Item(s) you are trying to delete are being used by Opportunity, It can not be deleted.")
                    Case "OpportunityKitItems_Depend"
                        ShowMessage("Item(s) you are trying to delete are being used by Opportunity Kit Items, It can not be deleted.")
                    Case "KitItems_Depend"
                        litMessage.Text = "Item(s) you are trying to delete are being used by Kit Items, It can not be deleted."
                        'Case "Inventory_Adjustment"
                        '    litMessage.Text = "selected item can not be deleted since its stock is affected by adjustment,your option is to use inventory adjustment and mark item as archived form item details."
                    Case "FY_CLOSED"
                        litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                    Case Else
                        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        DisplayError(CCommon.ToString(ex))
                End Select
            End Try
            BindDatagrid()
        End Sub

        Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As RadComboBoxItemsRequestedEventArgs)
            Try
                If e.Text.Trim().Length > 0 Then
                    Dim searchCriteria As String
                    Dim itemOffset As Integer = e.NumberOfItems

                    Dim ds As DataSet = GetItems(True, e.Text.Trim().Replace("%", "[%]"))

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        Dim endOffset As Integer

                        endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, ds.Tables(0).Rows.Count)
                        e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                        e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))

                        For Each dataRow As DataRow In ds.Tables(0).Rows
                            Dim item As New RadComboBoxItem()
                            item.Text = DirectCast(dataRow("vcItemName"), String)
                            item.Value = dataRow("numItemCode").ToString()
                            item.Attributes.Add("vcModelID", CCommon.ToString(dataRow("vcModelID")))

                            radCmbSearch.Items.Add(item)

                            item.DataBind()
                        Next
                    End If

                    'If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    '   

                    '    endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, ds.Tables(0).Rows.Count)
                    '    e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                    '    e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Orders <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))

                    '    For Each dr As DataRow In ds.Tables(0).Rows
                    '        Dim item As New RadComboBoxItem()
                    '        item.Text = CCommon.ToString(dr("numItemCode"))
                    '        item.Value = CCommon.ToString(dr("numItemCode"))

                    '        item.DataItem = dr
                    '        radCmbSearch.Items.Add(item)
                    '        item.DataBind()
                    '    Next
                    'End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
            End Try
        End Sub

        Private Sub InitializeSearchClientSideTemplate(ByVal dt As DataTable)
            Try

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    'genrate header template dynamically
                    radCmbSearch.HeaderTemplate = New SearchTemplate(ListItemType.Header, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)

                    'generate Clientside Template dynamically
                    radCmbSearch.ItemTemplate = New SearchTemplate(ListItemType.Item, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGotoRecord_Click(sender As Object, e As EventArgs) Handles btnGotoRecord.Click
            Try
                Response.Redirect("~/Items/frmKitDetails.aspx?ItemCode=" & radCmbSearch.SelectedValue & "&frm=All Items", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        '''' Multi Select Cluster Controls Click Events'''''''''''''''''

        Protected Sub btnMultiSelectAdd_Click(sender As Object, e As EventArgs)
            If (Session("MultiSelectItemCodes") IsNot Nothing) Then
                Session("MultiSelectItemCodes") = Session("MultiSelectItemCodes").ToString + hdnMultiSelectItemcodes.Value
            Else
                Session("MultiSelectItemCodes") = hdnMultiSelectItemcodes.Value
            End If
            hdnMultiSelectItemcodes.Value = ""
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ItemsAdded", "alert('Items added successfully');", True)
        End Sub

        Protected Sub btnMultiSelectAddFinish_Click(sender As Object, e As EventArgs)
            If (Session("MultiSelectItemCodes") IsNot Nothing) Then
                Session("MultiSelectItemCodes") = Session("MultiSelectItemCodes").ToString + hdnMultiSelectItemcodes.Value
            Else
                Session("MultiSelectItemCodes") = hdnMultiSelectItemcodes.Value
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenMultiSelectGrid", "OpenMultiSelectPopup('" & lngCustId & "');", True)
        End Sub

        Protected Sub imgbtnShoppingCart_Click(sender As Object, e As ImageClickEventArgs)
            If (Session("MultiSelectItemCodes") IsNot Nothing) Then
                Session("MultiSelectItemCodes") = Session("MultiSelectItemCodes").ToString + hdnMultiSelectItemcodes.Value
            Else
                Session("MultiSelectItemCodes") = hdnMultiSelectItemcodes.Value
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenMultiSelectGrid", "OpenMultiSelectPopup('" & lngCustId & "');", True)
        End Sub

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Protected Sub gvSearch_DataBound(sender As Object, e As EventArgs)

            Dim headerItem As GridHeaderItem = CType(gvSearch.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)

            Dim htFilterValues As New Hashtable
            Dim strValues() As String
            Dim strIDValue() As String
            Dim strMultipleValues() As String

            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                strValues = txtGridColumnFilter.Text.Trim(";").Split(";")

                For j = 0 To strValues.Length - 1
                    strIDValue = strValues(j).Split(":")
                    'If (strIDValue(1).Contains(",")) Then
                    '    strMultipleValues = strIDValue(1).Split(",")
                    'End If
                    htFilterValues.Add(strIDValue(0), strIDValue(1))
                Next
            End If

            ' Dim strFilterArray As String()
            Dim strItemGrpId As String
            'If (txtGridColumnFilter.Text IsNot "") Then
            '    strFilterArray = txtGridColumnFilter.Text.Split(";")
            'End If

            If (lngCustId <> 0) Then
                'For Each HeaderCell As GridTableHeaderCell In headerItem.Cells
                '    If (HeaderCell.Controls.Count > 0 AndAlso HeaderCell.Controls.Count = 3) Then
                '        If (TypeOf (HeaderCell.Controls(2)) Is Telerik.Web.UI.RadComboBox) Then
                '            CType(headerItem.FindControl(HeaderCell.Controls(2).ClientID), RadComboBox).CheckBoxes = True

                '            If htFilterValues.ContainsKey(HeaderCell.Controls(2).ClientID) Then
                '                For Each item As RadComboBoxItem In CType(headerItem.FindControl(HeaderCell.Controls(2).ClientID), RadComboBox).Items

                '                    If (htFilterValues(HeaderCell.Controls(2).ClientID).ToString.Contains(",")) Then
                '                        strMultipleValues = htFilterValues(HeaderCell.Controls(2).ClientID).ToString.Split(",")
                '                    End If

                '                    If (strMultipleValues Is Nothing And htFilterValues(HeaderCell.Controls(2).ClientID).ToString = item.Value) Then
                '                        item.Checked = True
                '                    ElseIf (strMultipleValues IsNot Nothing) Then
                '                        For Each value As String In strMultipleValues
                '                            If (item.Value = value) Then
                '                                item.Checked = True
                '                            End If
                '                        Next
                '                    End If
                '                Next
                '            End If
                '        End If
                '    End If
                'Next
            End If

            Dim imgOpenAttr As ImageButton = CType(headerItem.FindControl("imgOpenItemAttr"), ImageButton)
            If (txtGridColumnFilter.Text.Contains("numItemGroup") = True And lngCustId <> 0) Then
                strValues = txtGridColumnFilter.Text.Trim(";").Split(";")
                For Each str As String In strValues
                    If (str.Contains("numItemGroup") = True) Then
                        strItemGrpId = str
                    End If
                Next

                Dim ddlItemGroup As DropDownList = CType(headerItem.FindControl(strItemGrpId.Split(":")(0).ToString), DropDownList)
                Dim dsData As New DataSet
                Dim objItems As New CItems
                objItems.DomainID = Session("DomainID")
                objItems.ItemCode = 0
                objItems.ItemGroupID = CCommon.ToLong(ddlItemGroup.SelectedValue)
                dsData = objItems.GetItemGroups
                hdnItemGrpId.Value = ddlItemGroup.SelectedValue
                If (dsData.Tables.Count >= 3 AndAlso dsData.Tables(2).Rows.Count > 0) Then
                    imgOpenAttr.Visible = True
                    ' imgOpenAttr.Attributes.Add("onclick", "javascript: showAttrDiv();")
                    imgOpenAttr.OnClientClick = "return showAttrDiv();"
                    rptAttributes.DataSource = dsData.Tables(2)
                    rptAttributes.DataBind()

                End If

            Else
                ' imgOpenAttr.Visible = False
            End If

        End Sub

        Private Sub rptAttributes_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAttributes.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim dr As DataRowView = DirectCast(e.Item.DataItem, System.Data.DataRowView)
                    Dim ddlAttributes As DropDownList = DirectCast(e.Item.FindControl("ddlAttributes"), DropDownList)
                    Dim hdnFldID As HiddenField = DirectCast(e.Item.FindControl("hdnFldID"), HiddenField)
                    Dim hdnListID As HiddenField = DirectCast(e.Item.FindControl("hdnListID"), HiddenField)

                    If e.Item.ItemIndex = 0 Then
                        ddlAttributes.Focus()
                    End If

                    Dim dsData As New DataSet
                    Dim objItems As New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemGroupID = CCommon.ToLong(hdnItemGrpId.Value)
                    objItems.numListid = CCommon.ToLong(hdnListID.Value)
                    objItems.WarehouseID = 0
                    dsData = objItems.GetAttributesForSelectedItemGroup
                    If (dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0) Then
                        ddlAttributes.DataSource = dsData.Tables(0)
                        ddlAttributes.DataTextField = "vcData"
                        ddlAttributes.DataValueField = "numListItemID"
                        ddlAttributes.DataBind()
                        ddlAttributes.Items.Insert(0, "--Select One--")
                        ddlAttributes.Items.FindByText("--Select One--").Value = "0"
                    End If
                    ' objCommon.sb_FillAttibuesForEcommFromDB(ddlAttributes, hdnListID.Value, hdnCurrentSelectedItem.Value, "", 0)

                    'If Not dr("Fld_Value") Is Nothing AndAlso Not ddlAttributes.Items.FindByValue(dr("Fld_Value")) Is Nothing Then
                    '    ddlAttributes.Items.FindByValue(dr("Fld_Value")).Selected = True
                    'End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSaveCloseAttributes_Click(sender As Object, e As EventArgs) Handles btnSaveCloseAttributes.Click
            Try
                Dim vcAttributes As String
                Dim vcAttributeIDs As String
                Dim vcAttributeValues As String

                For Each item As RepeaterItem In rptAttributes.Items
                    Dim lblAttributeName As Label = DirectCast(item.FindControl("lblAttributeName"), Label)
                    Dim ddlAttributes As DropDownList = DirectCast(item.FindControl("ddlAttributes"), DropDownList)
                    Dim hdnFldID As HiddenField = DirectCast(item.FindControl("hdnFldID"), HiddenField)

                    If CCommon.ToLong(ddlAttributes.SelectedValue) = 0 Then
                        'Exit Sub
                    Else
                        'vcAttributes = IIf(String.IsNullOrEmpty(vcAttributes), "", vcAttributes & ",") & lblAttributeName.Text & ":" & ddlAttributes.SelectedItem.Text
                        'vcAttributeIDs = IIf(String.IsNullOrEmpty(vcAttributeIDs), "", vcAttributeIDs & ",") & hdnFldID.Value & ":" & ddlAttributes.SelectedItem.Value
                        vcAttributeIDs = IIf(String.IsNullOrEmpty(vcAttributeIDs), "", vcAttributeIDs & ",") & hdnFldID.Value
                        vcAttributeValues = IIf(String.IsNullOrEmpty(vcAttributeValues), "", vcAttributeValues & ",") & ddlAttributes.SelectedItem.Value
                    End If
                Next
                If (txtGridColumnFilter.Text.Contains("vcAttributeIDs")) Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue(), strID() As String

                    For i As Integer = 0 To strValues.Length - 1
                        If (strValues(i).Contains("vcAttributeIDs")) Then
                            strIDValue = strValues(i).Split(":")
                            strIDValue(1) = vcAttributeIDs
                            strIDValue(2) = vcAttributeValues + ";"
                            strValues(i) = strIDValue(0) + ":" + strIDValue(1) + ":" + strIDValue(2)
                        End If
                    Next
                    txtGridColumnFilter.Text = String.Join(";", strValues)
                Else
                    txtGridColumnFilter.Text = txtGridColumnFilter.Text + "vcAttributeIDs~0~0~SelectBox~0:" + vcAttributeIDs + ":" + vcAttributeValues + ";"
                End If
                divItemAttributes.Style.Add("display", "none")
                ' GridColumnSearchCriteria()
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class


    Public Class MyTemp
        Inherits frmItemList
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim Field1, Field2, Field3, Field4, FormID, FormFieldId As String
        Dim Custom As Boolean
        Dim ColumnWidth As Integer
        Dim lngWarehouseID As Long

        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal fld2 As String, ByVal fld3 As String, ByVal fld4 As String, ByVal numFormID As Long, Optional ByVal dtRow As DataRow = Nothing, _
                Optional ByVal WarehouseID As Long = 0)
            Try
                TemplateType = type
                Field1 = fld1
                Field2 = fld2
                Field3 = fld3
                Field4 = fld4
                FormID = numFormID
                lngWarehouseID = WarehouseID

                If dtRow IsNot Nothing Then
                    FormFieldId = dtRow.Item("numFieldId")
                    Custom = dtRow.Item("bitCustomField")
                    ColumnWidth = CCommon.ToInteger(dtRow("intColumnWidth"))
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lbl2 As Label = New Label()
                Dim lbl3 As Label = New Label()

                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType

                    Case ListItemType.Header
                        Dim cell As GridTableHeaderCell = CType(Container, GridTableHeaderCell)

                        If Field1 <> "CheckBox" And Field1 <> "CheckBox1" Then
                            If Field2 = "vcPriceLevelDetail" Then
                                lbl1.ID = Field2
                                lbl1.Text = Field1
                            End If

                            If Field4 = False Then
                                lbl1.ID = Field2
                                lbl1.Text = Field1
                                Container.Controls.Add(lbl1)
                            ElseIf Field4 = True Then
                                lnkButton.ID = Field2
                                lnkButton.Text = Field1
                                lnkButton.Attributes.Add("onclick", "Return SortColumn('" & Field2 & "')")
                                Container.Controls.Add(lnkButton)
                            End If
                        ElseIf Field1 = "CheckBox" Then
                            Dim chk As New CheckBox
                            chk.ID = "chkSelectAll"
                            Container.Controls.Add(chk)
                        ElseIf Field1 = "CheckBox1" Then
                            Dim chk As New CheckBox
                            chk.ID = "chkAllAPI"
                            chk.Text = "Export To API"
                            Container.Controls.Add(chk)
                        End If

                        If ColumnWidth > 0 Then
                            cell.Width = ColumnWidth
                        End If

                        cell.Attributes.Add("id", FormID & "~" & FormFieldId & "~" & Custom)
                    Case ListItemType.Item
                        If Field1 <> "CheckBox" And Field1 <> "CheckBox1" Then

                            If Field2 = "vcPriceLevelDetail" Then
                                Dim ddlPriceLevel As New DropDownList
                                ddlPriceLevel.ID = "ddlPriceLevel"
                                ddlPriceLevel.Attributes.Add("class", "pricelevel")
                                AddHandler ddlPriceLevel.DataBinding, AddressOf BindDropDownValue
                                Container.Controls.Add(ddlPriceLevel)
                            Else
                                AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                                Container.Controls.Add(lbl1)
                            End If

                        ElseIf Field1 = "CheckBox" Then
                            Dim chk As New CheckBox
                            chk.ID = "chkSelect"
                            AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            Container.Controls.Add(chk)
                            Container.Controls.Add(lbl1)
                            Container.Controls.Add(lbl2)
                            Container.Controls.Add(lbl3)
                        ElseIf Field1 = "CheckBox1" Then
                            Dim chk As New CheckBox
                            chk.ID = "chkSelectAPI"
                            AddHandler chk.DataBinding, AddressOf BindCheckBox
                            Container.Controls.Add(chk)
                            Container.Controls.Add(lbl1)
                            Container.Controls.Add(lbl2)
                            Container.Controls.Add(lbl3)
                        End If
                End Select
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindCheckBox(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim chkBox As CheckBox = CType(Sender, CheckBox)
                Dim Container As Telerik.Web.UI.GridDataItem = CType(chkBox.NamingContainer, Telerik.Web.UI.GridDataItem)
                chkBox.ID = "chkSelectAPI"
                chkBox.Checked = DataBinder.Eval(Container.DataItem, Field2)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As Telerik.Web.UI.GridDataItem = CType(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem)
                lbl1.ID = "lbl1"
                lbl1.Text = DataBinder.Eval(Container.DataItem, Field2)
                lbl1.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As Telerik.Web.UI.GridDataItem = CType(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem)
                If Field2 = "vcItemName" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcItemName")), "", DataBinder.Eval(Container.DataItem, "vcItemName"))
                    lbl1.Attributes.Add("onclick", "return OpenItem('" & DataBinder.Eval(Container.DataItem, "numItemCode") & "','" & Field3 & "')")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"

                ElseIf Field2 = "vcPathForTImage" Then
                    Dim strImagePath As String = ""
                    strImagePath = CCommon.GetDocumentPath(Session("DomainID")) & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcPathForTImage")), "", DataBinder.Eval(Container.DataItem, "vcPathForTImage"))

                    If IsDBNull(DataBinder.Eval(Container.DataItem, "vcPathForTImage")) = True Then
                        lbl1.Text = ""
                    Else
                        If DataBinder.Eval(Container.DataItem, "vcPathForTImage") = "" Then
                            lbl1.Text = ""
                        Else
                            lbl1.Text = "<img style=""vertical-align: middle;text-align:center"" height=""60px"" width=""60px"" src=""" & strImagePath & """ />"
                        End If
                    End If

                Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field2)), "", DataBinder.Eval(Container.DataItem, Field2))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As Telerik.Web.UI.GridDataItem = CType(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDropDownValue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim ddlPriceLevel As DropDownList = CType(Sender, DropDownList)
                Dim Container As Telerik.Web.UI.GridDataItem = CType(ddlPriceLevel.NamingContainer, Telerik.Web.UI.GridDataItem)

                Dim dtItemsWarehousePrice As New DataTable
                Dim objItems As New CItems
                objItems.ItemCode = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numItemCode"))
                objItems.DomainID = Session("DomainID")
                objItems.WarehouseID = lngWarehouseID
                objItems.DivisionID = 0

                Dim dsPriceLevel As DataSet = objItems.GetPriceLevelWithWarehouseItemPrice()
                dtItemsWarehousePrice = dsPriceLevel.Tables(0)
                ddlPriceLevel.Items.Clear()

                Dim Litem As ListItem
                If dtItemsWarehousePrice.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItemsWarehousePrice.Rows
                        Litem = New ListItem()
                        Litem.Text = If(CCommon.ToString(dr("vcWareHouse")).Length > 0,
                                        String.Format("{1}-{0}", CCommon.GetDecimalFormat(dr("decDiscount")), CCommon.ToString(dr("vcWareHouse"))), _
                                        String.Format("{0}", CCommon.GetDecimalFormat(dr("decDiscount"))))

                        Litem.Value = CCommon.ToString(dr("KeyValue"))
                        Litem.Attributes("OptionGroup") = "Warehouse Price"
                        ddlPriceLevel.Items.Add(Litem)
                    Next
                End If

                If objItems.WarehouseID > 0 Then
                    Dim dtItemsPriceLevel As New DataTable
                    dtItemsPriceLevel = dsPriceLevel.Tables(1)
                    If dtItemsPriceLevel.Rows.Count > 0 Then
                        For Each dr As DataRow In dtItemsPriceLevel.Rows
                            Litem = New ListItem()
                            Litem.Text = (String.Format("{2} ({0}-{1})", CCommon.ToInteger(dr("intFromQty")), CCommon.ToInteger(dr("intToQty")), CCommon.GetDecimalFormat(dr("decDiscount"))))

                            Litem.Value = String.Format("{0}~{1}~{2}~{3}", CCommon.ToLong(dr("numWareHouseID")), CCommon.ToLong(dr("numWareHouseItemID")), CCommon.ToDecimal(dr("decDiscount")), CCommon.ToString(dr("vcWareHouse")))
                            Litem.Attributes("OptionGroup") = "Price Table"
                            ddlPriceLevel.Items.Add(Litem)
                        Next
                    End If

                    Dim dtItemsPriceRule As New DataTable
                    dtItemsPriceRule = dsPriceLevel.Tables(2)
                    If dtItemsPriceRule.Rows.Count > 0 Then
                        For Each dr As DataRow In dtItemsPriceRule.Rows
                            Litem = New ListItem()
                            Litem.Text = (String.Format("{2} ({0}-{1})", CCommon.ToInteger(dr("intFromQty")), CCommon.ToInteger(dr("intToQty")), CCommon.GetDecimalFormat(dr("decDiscount"))))

                            Litem.Value = String.Format("{0}~{1}~{2}~{3}", CCommon.ToLong(dr("numWareHouseID")), CCommon.ToLong(dr("numWareHouseItemID")), CCommon.ToDecimal(dr("decDiscount")), CCommon.ToString(dr("vcWareHouse")))
                            Litem.Attributes("OptionGroup") = "Price Rule"
                            ddlPriceLevel.Items.Add(Litem)
                        Next
                    End If
                End If

                ddlPriceLevel.ClearSelection()
                If ddlPriceLevel.Items.Count = 0 Then
                    ddlPriceLevel.Items.Insert(0, New ListItem("--Select One--", "0"))
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class

#Region "RadComboBox Template"
    Public Class SearchTemplate
        Implements ITemplate

        Dim ItemTemplateType As ListItemType
        Dim dtTable1 As DataTable
        Dim i As Integer = 0
        Dim _DropDownWidth As Double

        Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 600)
            Try
                ItemTemplateType = type
                dtTable1 = dtTable
                _DropDownWidth = DropDownWidth
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
            Try
                i = 0
                Dim label1 As Label
                Dim label2 As Label
                Dim img As System.Web.UI.WebControls.Image
                Dim table1 As New HtmlTable
                Dim tblCell As HtmlTableCell
                Dim tblRow As HtmlTableRow
                table1.Width = "100%"
                Dim ul As New HtmlGenericControl("ul")

                Select Case ItemTemplateType
                    Case ListItemType.Header
                        For Each dr As DataRow In dtTable1.Rows
                            Dim li As New HtmlGenericControl("li")
                            li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                            li.Style.Add("float", "left")
                            li.Style.Add("text-align", "center")
                            li.Style.Add("font-weight", "bold")
                            li.InnerText = dr("vcFieldName").ToString
                            ul.Controls.Add(li)
                        Next

                        container.Controls.Add(ul)
                    Case ListItemType.Item
                        For Each dr As DataRow In dtTable1.Rows
                            Dim li As New HtmlGenericControl("li")
                            Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                            li.Style.Add("width", Unit.Pixel(width).Value & "px")
                            li.Style.Add("float", "left")

                            If dr("bitCustomField") = "1" Then
                                label2 = New Label
                                label2.CssClass = "normal1"
                                label2.Attributes.Add("style", "display:inline-block;width:" & width)
                                AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                li.Controls.Add(label2)
                            Else
                                label1 = New Label
                                label1.CssClass = "normal1"
                                label1.Attributes.Add("style", "display:inline-block;width:" & width)
                                AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                li.Controls.Add(label1)
                            End If

                            ul.Controls.Add(li)
                        Next

                        container.Controls.Add(ul)
                End Select
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim target As Label = CType(sender, Label)
            Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
            Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName").ToString()))
            'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
            target.Text = itemText
            i = i + 1
        End Sub

        Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim target As Label = CType(sender, Label)
            Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
            Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName").ToString()))

            target.Text = itemText
            i = i + 1
        End Sub

    End Class
#End Region
End Namespace
