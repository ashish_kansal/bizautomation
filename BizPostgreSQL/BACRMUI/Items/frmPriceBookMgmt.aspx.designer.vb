'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmPriceBookMgmt

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnSaveClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveClose As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''UpdateProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtRuleName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRuleName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRuleDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRuleDesc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''chkRoundTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkRoundTo As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ddlRoundTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRoundTo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''hdnRuleFor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnRuleFor As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''pnlSel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rbPricingTable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbPricingTable As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''hplPricingTable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplPricingTable As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''trPricingFormula control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trPricingFormula As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbPricingFormula control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbPricingFormula As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''trPricingFormulaTable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trPricingFormulaTable As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlRuleType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRuleType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlVendorCostType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlVendorCostType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlDiscountType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDiscountType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDiscount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDiscount As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtQntyItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQntyItems As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMaxPerOrAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMaxPerOrAmt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rbIndividualItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbIndividualItem As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''hplIndividualItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplIndividualItems As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''trItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trItemClassification As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbItemClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbItemClassification As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''hplClassification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplClassification As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''trAllItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trAllItems As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbAllItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbAllItems As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''lblStep3Caption control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStep3Caption As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rbIndividualOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbIndividualOrg As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''hplIndividualOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplIndividualOrg As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''trRelProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trRelProfile As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbRelProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbRelProfile As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''hplRelProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplRelProfile As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''trAllOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trAllOrg As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbAllOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbAllOrg As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''pnlSel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSel1 As Global.System.Web.UI.WebControls.Panel
End Class
