﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCartItemFilters.aspx.vb"
    Inherits=".frmCartItemFilters" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Column Customization</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            if ($('#ddlAvailableFields').val() == 0) {
                alert('Please select Available field to add to cart filters!');
                return false;
            }
            if ($('#ddlSites').val() == 0) {
                alert('Please select Site to add filters within it!');
                return false;
            }

            return true;
            //  alert( document.getElementById("hdnCol").value)
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                var i;
                i = 0;
                $("#gvCartFilters INPUT[type='checkbox']").each(function () {
                    if (this.checked) {
                        i = i + 1;
                    }
                });

                if (i == 0) {
                    alert("Please select atleast one item to delete.");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete" />
            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button" Width="50" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text="Select Cart Item Filter Fields"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Table ID="tblCartFilters" runat="server" Width="600" Height="600" GridLines="None"
        CssClass="aspTable" BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td class="normal1" align="right">Available Fields
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlAvailableFields" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Select Site
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlSites" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvCartFilters" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="tbl" Width="100%" CellPadding="2" CellSpacing="2">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex +1 %>
                                            <asp:HiddenField Id="hdnFieldID" runat="server" Value='<%#Eval("numFieldID")%>' />
                                            <asp:HiddenField Id="hdnSiteID" runat="server" Value='<%#Eval("numSiteID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Site Name" DataField="vcSiteName"
                                        ShowHeader="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Filter Name" DataField="vcFieldName"
                                        ShowHeader="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll('chkSelectAll','chkSelect')" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkSelect" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                </PagerTemplate>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
    <input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
</asp:Content>
