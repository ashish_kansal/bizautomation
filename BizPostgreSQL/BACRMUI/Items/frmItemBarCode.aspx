<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmItemBarCode.aspx.vb"
    Inherits=".frmItemBarCode" MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Item Barcode Generate</title>
    <script src="../JavaScript/jquery-barcode-2.0.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function PrintBarcode() {
            var str = '';
            i = 0;
            $("#gvItemBarcode .tblBarcode").each(function () {
                //var currentId = $(this).attr('id');
                
                //$(this).find('#lblItemName').val().replace('"', "\\\"");
                if ($('#chkPrintInSinglePage').is(":checked") == true) {

                    if (i == $("#gvItemBarcode .tblBarcode").length - 1) {
                        str = str + '<div><table><tr style="vertical-align: top;"><td width="50%"><div style="' + $(this).attr('style') + '">' + $(this).html() + '</div></td></table></div>';
                    }
                    else {
                        str = str + '<div style="page-break-after:always"><table><tr style="vertical-align: top;"><td width="50%"><div style="' + $(this).attr('style') + '">' + $(this).html() + '</div></td></table></div>';
                    }

                    //if (i % 2 == 0)
                    //    str = str + '<tr><td width="50%" style="page-break-after:always"><div style="' + $(this).attr('style') + ';page-break-after:always">' + $(this).html() + '</div></td>';
                    //else
                    //    str = str + '<td width="50%" style="page-break-after:always"><div style="' + $(this).attr('style') + ';page-break-after:always">' + $(this).html() + '</div></td></tr>';
                }
                else {

                    if (i % 2 == 0)
                        str = str + '<tr style="vertical-align: top;"><td width="50%"><div style="' + $(this).attr('style') + '">' + $(this).html() + '</div></td>';
                    else
                        str = str + '<td width="50%"><div style="' + $(this).attr('style') + '">' + $(this).html() + '</div></td></tr>';

                }
                i += 1;
            });

            if (i == 0) {
                alert('No barcode to print.');
                return false;
            }

            if ($('#chkPrintInSinglePage').is(":checked") == true) {
                printIt(str);
            }
            else {

                if (i % 2 != 0)
                    str = str + '</tr>';

                str = '<table style="width: 595px;">' + str + '</table>';
                //console.log(str);
                printIt(str);

            }

        }

        function printIt(printThis) {
            win = window.open();
            self.focus();
            win.document.open();
            win.document.write('<' + 'html' + '><' + 'head' + '><' + 'style' + '>');
            win.document.write('body, td { font-family: Verdana; font-size: 10px;}');
            win.document.write('ul {margin: 0;padding: 0;margin-left:10px;}');
            win.document.write('<' + '/' + 'style' + '><' + '/' + 'head' + '><' + 'body' + '>');
            win.document.write(printThis);
            win.document.write('<' + '/' + 'body' + '><' + '/' + 'html' + '>');

            if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                win.print();
            }
            else {
                win.document.close();
                win.print();
                win.close();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table id="Table3" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="normal1">
                        <asp:CheckBox ID="chkAllItems" runat="server" Text="Include All Items" AutoPostBack="true" />
                    </td>
                    <td class="normal1">
                        <asp:CheckBox ID="chkPrintInSinglePage" runat="server" Text="Print Each Barcode In a Single Page" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print"></asp:Button>&nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Item Barcode Generate
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table id="table1" width="800px">
        <tr>
            <td class="normal1" align="right">Barcode based on
            </td>
            <td colspan="8" class="normal1">
                <asp:RadioButtonList ID="rblBarcodeBasedOn" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0" Selected="True">UPC/Barcode</asp:ListItem>
                    <asp:ListItem Value="1">SKU</asp:ListItem>
                    <asp:ListItem Value="2">Vendor Part #</asp:ListItem>
                    <asp:ListItem Value="3">Serial #/LOT #</asp:ListItem>
                    <asp:ListItem Value="4">Item ID</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Bar Type
            </td>
            <td>
                <asp:DropDownList ID="ddlBarcodeType" runat="server">
                    <asp:ListItem Value="code11">code 11</asp:ListItem>
                    <asp:ListItem Value="code39" Selected="True">code 39</asp:ListItem>
                    <asp:ListItem Value="code93">code 93</asp:ListItem>
                    <asp:ListItem Value="code128">code 128</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right">Bar Width
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtWidth" runat="server" CssClass="signup" Width="50" Text="1"></asp:TextBox>
            </td>
            <td class="normal1" align="right">Bar Height
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtHeight" runat="server" CssClass="signup" Width="50" Text="50"></asp:TextBox>
            </td>
            <td class="normal1" align="right">Item Name Length
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtItemNameLength" runat="server" CssClass="signup" Width="50" Text="10"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Button ID="btnGenerateBarcode" runat="server" CssClass="button" Text="Barcode"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <asp:GridView ID="gvItemBarcode" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
                    AutoGenerateColumns="False" DataKeyNames="numItemCode">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblItemName" Text='<%#Eval("vcItemName")%>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="vcItemName" HeaderText="Item Name" ItemStyle-HorizontalAlign="Center" />--%>
                        <asp:BoundField DataField="vcWareHouse" HeaderText="WareHouse/Vendor" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="vcAttribute" HeaderText="Attribute" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="numBarCodeId" HeaderText="Item ID/UPC/Barcode/SKU/Part #/Serial #/LOT #"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Barcode Image">
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
