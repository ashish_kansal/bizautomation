﻿Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports System.Reflection
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Partial Public Class frmUpdateInventory
    Inherits BACRMPage

#Region "Decleration"
    Dim strFileName As String  'Variable to hold the FileName string
    Dim lngDivisionID As Long
    Dim lngContactID As Long
    Dim txtState As String
    Dim txtCountry As String
    Dim cmbAnnualRevenue As String
    Dim cmbEmployees As Long

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            
            GetUserRightsForPage(37, 33)
            If Not IsPostBack Then
                LoadItemGroup()
                pgBar.Style.Add("display", "none")
                ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Import"
            End If
            If Session("FileLocation") <> "" Then displayDropdowns()
            btnUpload.Attributes.Add("onClick", "return checkFileExt()")
            btnDisplay.Attributes.Add("onClick", "return setDllValues()")
            btnImports.Attributes.Add("onClick", "return displayPBar()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadItemGroup()
        Try
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = 0
            ddlItemGroup.DataSource = objItems.GetItemGroups.Tables(0)
            ddlItemGroup.DataTextField = "vcItemGroup"
            ddlItemGroup.DataValueField = "numItemGroupID"
            ddlItemGroup.DataBind()
            ddlItemGroup.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lbDownloadCSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDownloadCSV.Click
        DownloadTemplate(0)
    End Sub

    Private Sub lbDownloadCSVWithData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDownloadCSVWithData.Click
        DownloadTemplate(1)
    End Sub

    Sub DownloadTemplate(ByVal mode As Short)
        Dim objItems As New CItems
        Dim dt As New DataTable
        Dim DataTable As DataTable

        Dim objItem As New CItems
        objItem.DomainID = Session("DomainID")
        objItem.ItemGroupID = ddlItemGroup.SelectedValue
        If chkLotNo.Checked Or chkSerializedItem.Checked Then
            objItem.bitSerialized = chkSerializedItem.Checked
            objItem.bitLotNo = chkLotNo.Checked

            objItem.byteMode = 2
        Else
            objItem.byteMode = mode
        End If

        DataTable = objItem.GetImportWareHouseItems()

        If objItem.byteMode = 0 Then
            For Each dr As DataRow In DataTable.Rows
                dt.Columns.Add(dr("vcFormFieldName").ToString.Replace(",", " "))
            Next
        End If


        'Dim objImport As New ImportWizard
        'objImport.DomainId = Session("DomainID")
        ' objImport.ImportType = 7
        'objImport.Relationship = ddlItemGroup.SelectedValue
        'Dim DataTable As DataTable = objImport.GetConfiguration.Tables(0)

        'For Each dr As DataRow In DataTable.Rows
        '    dt.Columns.Add(dr("vcFormFieldName").ToString.Replace(",", " "))
        'Next

        'If ddlItemGroup.SelectedValue > 0 Then
        '    objItems.ItemGroupID = ddlItemGroup.SelectedValue
        '    objItems.DomainID = Session("DomainID")
        '    Dim ds As New DataSet
        '    ds = objItems.GetItemGroups
        '    For Each dr As DataRow In ds.Tables(2).Rows
        '        dt.Columns.Add(dr("vcFormFieldName").ToString.Replace(",", " "))
        '    Next
        'End If

        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "inline;filename=ImportItemWareHouseTemplate." & Now.Month.ToString & "." & Now.Day.ToString & "." & Now.Year.ToString & ".csv")
        Response.Charset = ""
        Me.EnableViewState = False

        If objItem.byteMode = 0 Then
            CSVExport.ProduceCSV(dt, Response.Output, True)
        Else
            CSVExport.ProduceCSV(DataTable, Response.Output, True)
        End If
        Response.End()
    End Sub
#End Region

#Region "Display Dropdowns"

    Private Sub displayDropdowns()
        Try
            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As New StreamReader(fileLocation)
            Dim intCount As Integer = 0
            Dim intCountFor As Integer = 0
            Dim strSplitValue() As String
            Dim strSplitHeading() As String
            Dim strLine As String = streamReader.ReadLine()
            Dim dataTableMap As New DataTable
            dataTableMap.Columns.Add("Destination")
            dataTableMap.Columns.Add("ID")
            Try
                Do While Not strLine Is Nothing
                    Dim dataRowMap As DataRow
                    dataRowMap = dataTableMap.NewRow
                    strSplitHeading = Split(strLine, ",")
                    For intCountFor = 0 To strSplitHeading.Length - 1
                        dataRowMap = dataTableMap.NewRow
                        dataRowMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                        dataRowMap("ID") = intCountFor
                        dataTableMap.Rows.Add(dataRowMap)
                    Next
                    Exit Do
                Loop
                Dim trHeader As New TableRow
                Dim trDetail As New TableRow
                Dim tableHC As TableHeaderCell
                Dim tableDtl As TableCell
                Dim drHeading As DataRow
                intCount = 0
                dataTableHeading = Session("dataTableHeadingObj")
                Dim ddlDestination As DropDownList
                Dim arryList() As String
                Dim strdllValue As String
                Dim intArryValue As Integer
                If Session("dllValue") <> "" Then
                    strdllValue = Session("dllValue")
                    arryList = Split(strdllValue, ",")
                End If
                tbldtls.Rows.Clear()
                For Each drHeading In dataTableHeading.Rows
                    trHeader.CssClass = "hs"
                    tableHC = New TableHeaderCell
                    tableHC.ID = intCount
                    tableHC.Text = drHeading.Item(1)
                    tableHC.CssClass = "normal5"
                    trHeader.Cells.Add(tableHC)
                    tableDtl = New TableCell
                    ddlDestination = New DropDownList
                    ddlDestination.CssClass = "signup"
                    ddlDestination.ID = "ddlDestination" & intCount.ToString
                    ddlDestination.DataSource = dataTableMap
                    ddlDestination.DataTextField = "Destination"
                    ddlDestination.DataValueField = "ID"
                    ddlDestination.DataBind()
                    If Session("dllValue") <> "" Then
                        If arryList.Length > 9 Then
                            intArryValue = CType(arryList(intCount), Integer)
                            ddlDestination.SelectedIndex = intArryValue
                        ElseIf ddlDestination.Items.Count > intCount Then
                            ddlDestination.SelectedIndex = intCount
                        End If
                    ElseIf ddlDestination.Items.Count > intCount Then
                        ddlDestination.SelectedIndex = intCount
                    End If
                    tableDtl.Controls.Add(ddlDestination)
                    trDetail.Cells.Add(tableDtl)
                    intCount += 1
                    trDetail.CssClass = "is"
                    tbldtls.Rows.Add(trDetail)
                    tbldtls.Rows.AddAt(0, trHeader)
                Next
                Session("IntCount") = intCount - 1
                If txtDllValue.Text <> "" Then Session("dllValue") = txtDllValue.Text
                txtDllValue.Text = intCount - 1
            Catch ex As Exception
                Throw ex
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Display Click"
    Private Sub btnDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As StreamReader
            streamReader = File.OpenText(fileLocation)
            Dim intCount As Integer = Session("IntCount")
            Dim intCountFor As Integer = 0
            Dim strSplitValue() As String
            Dim strSplitHeading() As String
            ' Dim strLine As String = streamReader.ReadLine()
            Dim arryList() As String
            Dim strdllValue As String
            Dim intArryValue As Integer
            Dim intCountSession As Integer = Session("IntCount")
            dataTableDestination = Session("dataTableDestinationObj")
            dataTableDestination.Clear()
            Dim csv As CsvReader = New CsvReader(streamReader, True)
            Try
                csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                csv.SupportsMultiline = True

                strdllValue = Session("dllValue")
                arryList = Split(strdllValue, ",")
                'strLine = streamReader.ReadLine()
                'Do While Not strLine Is Nothing
                '    Dim dataRowDestination As DataRow
                '    dataRowDestination = dataTableDestination.NewRow
                '    strSplitValue = Split(strLine, ",")
                '    dataTableHeading = Session("dataTableHeadingObj")
                '    Dim drHead As DataRow
                '    intCount = 0
                '    For Each drHead In dataTableHeading.Rows
                '        intArryValue = CType(arryList(intCount), Integer)
                '        dataRowDestination(drHead.Item(1)) = strSplitValue(intArryValue).ToString
                '        intCount += 1
                '    Next
                '    dataTableDestination.Rows.Add(dataRowDestination)
                '    strLine = streamReader.ReadLine()
                'Loop

                'Dim headers As String() = csv.GetFieldHeaders()
                'Dim i As Integer
                Dim dataRowDestination As DataRow
                ''For i = 0 To csv.FieldCount - 1
                ''    dataTableDestination.Columns.Add(headers(i))
                ''    'Response.Write(String.Format("{0} = {1};", headers(i), csv(i)))
                ''Next
                Dim drHead As DataRow
                dataTableHeading = Session("dataTableHeadingObj")
                Dim i1000 As Integer = 0
                While (csv.ReadNextRecord())
                    dataRowDestination = dataTableDestination.NewRow
                    intCount = 0
                    For Each drHead In dataTableHeading.Rows
                        intArryValue = CType(arryList(intCount), Integer)
                        dataRowDestination(drHead.Item(1)) = csv(intArryValue).ToString
                        intCount += 1
                    Next
                    dataTableDestination.Rows.Add(dataRowDestination)
                    i1000 = i1000 + 1
                    If i1000 = 5000 Then Exit While 'Modified by Anoop 29/04/2009
                End While
                Dim drDetination As DataRow
                Dim iCount As Integer = 0
                Dim iCountRow As Integer = 1
                Dim dtCol As DataColumn
                Dim trDetail As TableRow
                Dim tableCell As TableCell
                'tbldtls.Rows.Clear()
                Dim k As Integer = 0
                For Each drDetination In dataTableDestination.Rows
                    trDetail = New TableRow
                    If k = 0 Then
                        trDetail.CssClass = "ais"
                        k = 1
                    Else
                        trDetail.CssClass = "is"
                        k = 0
                    End If
                    For iCount = 1 To dataTableDestination.Columns.Count - 1
                        tableCell = New TableCell
                        tableCell.Text = drDetination.Item(iCount)
                        tableCell.CssClass = "normal1"
                        trDetail.Cells.Add(tableCell)
                    Next
                    iCountRow += 1
                    tbldtls.Rows.AddAt(iCountRow, trDetail)
                Next
            Catch ex As Exception
                Response.Write(ex)
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
#End Region

#Region "Create Table Schema"
    Private Sub createTableSchema()
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim dataTable As New DataTable
            Dim numRows As Integer
            Dim i As Integer
            dataTableHeading.Columns.Add("Id")
            dataTableHeading.Columns.Add("Destination")
            dataTableHeading.Columns.Add("FType")
            dataTableHeading.Columns.Add("cCtype")
            dataTableHeading.Columns.Add("vcAssociatedControlType")
            dataTableHeading.Columns.Add("vcDbColumnName")
            dataTableHeading.Columns.Add("numListID")
            Dim dataRowHeading As DataRow

            Dim objImport As New ImportWizard
            objImport.DomainId = Session("DomainID")
            objImport.ImportType = 7
            objImport.Relationship = ddlItemGroup.SelectedValue
            dataTable = objImport.GetConfiguration.Tables(0)
            numRows = dataTable.Rows.Count()
            For i = 0 To numRows - 1
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = dataTable.Rows(i).Item("vcFormFieldName")
                dataRowHeading("Id") = dataTable.Rows(i).Item("numFormFieldId")
                dataRowHeading("FType") = IIf(dataTable.Rows(i).Item("bitCustomFld") = "False", "0", "1")
                dataRowHeading("cCtype") = dataTable.Rows(i).Item("cCtype")
                dataRowHeading("vcAssociatedControlType") = dataTable.Rows(i).Item("vcAssociatedControlType")
                dataRowHeading("vcDbColumnName") = dataTable.Rows(i).Item("vcDbColumnName")
                dataRowHeading("numListID") = dataTable.Rows(i).Item("numListID")
                dataTableHeading.Rows.Add(dataRowHeading)
            Next

            If chkLotNo.Checked Or chkSerializedItem.Checked Then
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = IIf(chkLotNo.Checked, "LOT No", "Serial No")
                dataRowHeading("Id") = 0
                dataRowHeading("FType") = 0
                dataRowHeading("cCtype") = "R"
                dataRowHeading("vcAssociatedControlType") = "TextBox"
                dataRowHeading("vcDbColumnName") = "vcSerialNo"
                dataRowHeading("numListID") = 0
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = "Comments"
                dataRowHeading("Id") = 0
                dataRowHeading("FType") = 0
                dataRowHeading("cCtype") = "R"
                dataRowHeading("vcAssociatedControlType") = "TextBox"
                dataRowHeading("vcDbColumnName") = "vcComments"
                dataRowHeading("numListID") = 0
                dataTableHeading.Rows.Add(dataRowHeading)

                If chkLotNo.Checked Then
                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = "Qty"
                    dataRowHeading("Id") = 0
                    dataRowHeading("FType") = 0
                    dataRowHeading("cCtype") = "R"
                    dataRowHeading("vcAssociatedControlType") = "TextBox"
                    dataRowHeading("vcDbColumnName") = "numQty"
                    dataRowHeading("numListID") = 0
                    dataTableHeading.Rows.Add(dataRowHeading)
                End If
            End If

            Session("dataTableHeadingObj") = dataTableHeading
            Dim drHead As DataRow
            dataTableDestination.Columns.Add("SlNo")
            dataTableDestination.Columns("SlNo").AutoIncrement = True
            dataTableDestination.Columns("SlNo").AutoIncrementSeed = 1
            dataTableDestination.Columns("SlNo").AutoIncrementStep = 1
            For Each drHead In dataTableHeading.Rows
                dataTableDestination.Columns.Add(drHead.Item(1))
            Next
            Session("dataTableDestinationObj") = dataTableDestination
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Upload Click"
    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                pgBar.Style.Add("display", "")
                Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)
                Dim SaveLocation As String = Server.MapPath("../Documents/Docs") & "\" & fileName
                Try
                    txtFile.PostedFile.SaveAs(SaveLocation)
                    Session("FileLocation") = SaveLocation
                    litMessage.Text = "The file has been uploaded"
                    createTableSchema()
                    displayDropdowns()
                    pgBar.Style.Add("display", "none")
                Catch ex As Exception
                    pgBar.Style.Add("display", "none")
                    Throw ex
                End Try
            Else
                Session("IntCount") = ""
                Session("FileLocation") = ""
                Session("dataTableDestinationObj") = ""
                Session("dataTableHeadingObj") = ""
                Session("dllValue") = ""
                tbldtls.Rows.Clear()
                litMessage.Text = "Please select a file to upload."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Imports Click"
    Private Sub btnImports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImports.Click
        Dim objItems As New CItems
        Dim objImpWzd As New ImportWizard
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim lngItemID As Long
            dataTableDestination = Session("dataTableDestinationObj")
            dataTableHeading = Session("dataTableHeadingObj")
            Dim drDestination As DataRow
            Dim drHead As DataRow
            Dim intCount As Integer = 0

            For Each drDestination In dataTableDestination.Rows
                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("Fld_ID")
                dtCusTable.Columns.Add("Fld_Value")
                dtCusTable.TableName = "CusFlds"
                Dim drCusRow As DataRow

                objItems = New CItems

                Dim vcFieldtype As Char
                Dim vcAssociatedControlType As String

                For Each dr As DataRow In dataTableHeading.Rows
                    vcFieldtype = dr("cCtype")
                    If vcFieldtype = "R" Then
                        vcAssociatedControlType = dr("vcAssociatedControlType")
                        Select Case vcAssociatedControlType
                            Case "TextBox"
                                AssignValuesEditBox(objItems, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                            Case "SelectBox"
                                If dr("vcDbColumnName").IndexOf("numWareHouseID") > -1 Then
                                    drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(6, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                End If
                                AssignValuesEditBox(objItems, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                        End Select
                    ElseIf vcFieldtype = "C" Then
                        vcAssociatedControlType = dr("vcAssociatedControlType")

                        drCusRow = dtCusTable.NewRow
                        drCusRow("Fld_ID") = dr("Id")

                        Select Case vcAssociatedControlType
                            Case "SelectBox"
                                drCusRow("Fld_Value") = objImpWzd.GetStateAndCountry(18, CCommon.ToString(drDestination(dr("Destination"))), Session("DomainID"), CCommon.ToLong(dr("numListID")))
                            Case "CheckBox"
                                If UCase(drDestination(dr("Destination").ToString)) = "TRUE" Or UCase(drDestination(dr("Destination").ToString)) = "YES" Then
                                    drCusRow("Fld_Value") = 1
                                Else
                                    drCusRow("Fld_Value") = 0
                                End If
                            Case Else
                                drCusRow("Fld_Value") = drDestination(dr("Destination").ToString).ToString
                        End Select

                        dtCusTable.Rows.Add(drCusRow)
                    End If
                Next
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")

                If objItems.ItemCode > 0 Then
                    Dim ds As New DataSet
                    ds.Tables.Add(dtCusTable)
                    objItems.strFieldList = ds.GetXml

                    objItems.AddUpdateWareHouseForItems()
                End If
            Next
            litMessage.Text = "Records are sucessfully saved into database"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        Finally
            pgBar.Style.Add("display", "none")
            Session("IntCount") = ""
            Session("FileLocation") = ""
            Session("dataTableDestinationObj") = ""
            Session("dataTableHeadingObj") = ""
            Session("dllValue") = ""
            tbldtls.Rows.Clear()
        End Try
    End Sub
#End Region

    Public Sub SetProperties(ByRef objLeadBoxData As FormGenericLeadBox, ByVal sPropertyName As String, ByVal sPropertyValue As String)
        Try
            Dim theType As Type = objLeadBoxData.GetType
            Dim myProperties() As PropertyInfo = theType.GetProperties((BindingFlags.Public Or BindingFlags.Instance))
            Dim fillObject As Object = objLeadBoxData
            Dim PropertyItem As PropertyInfo
            For Each PropertyItem In myProperties                                                       'Loop thru Writeonly public properties
                With PropertyItem
                    If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                        Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                            Case "System.String" : PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)
                            Case "System.Int64", "System.Int32", "System.Int16"                         'Integer type properties
                                If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CInt(sPropertyValue), Nothing) 'Typecasting to integer before setting the value
                            Case "System.Boolean"                                                       'Boolean properties
                                If IsBoolean(sPropertyValue) Then PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing) 'Typecasting to boolean before setting the value
                        End Select
                        Exit Sub
                    End If
                End With
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function IsBoolean(ByVal sTrg As String) As Boolean
        Try
            Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
        Catch Ex As InvalidCastException
            Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
        End Try
        Return True                                                                             'Test passed, its booelan
    End Function

    Sub AssignValuesEditBox(ByVal objItems As CItems, ByVal vcValue As String, ByVal vcColumnName As String)
        Try
            Select Case vcColumnName
                Case "numItemCode" : objItems.ItemCode = vcValue
                Case "numWareHouseItemID" : objItems.WareHouseItemID = vcValue
                Case "numWareHouseID" : objItems.WarehouseID = vcValue
                Case "numOnHand" : objItems.OnHand = IIf(IsNumeric(vcValue), vcValue, 0)
                Case "numReorder" : objItems.ReOrder = IIf(IsNumeric(vcValue), vcValue, 0)
                Case "vcLocation" : objItems.WLocation = vcValue
                Case "monWListPrice" : objItems.WListPrice = IIf(IsNumeric(Replace(vcValue, ",", "")), vcValue, 0)
                Case "vcWHSKU" : objItems.WSKU = vcValue
                Case "vcBarCode" : objItems.WBarCode = vcValue
                Case "vcSerialNo" : objItems.SerialNo = vcValue
                Case "vcComments" : objItems.Comments = vcValue
                Case "numQty" : objItems.Quantity = IIf(IsNumeric(Replace(vcValue, ",", "")), vcValue, 0)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
