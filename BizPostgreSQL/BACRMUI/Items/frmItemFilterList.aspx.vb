﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.IO
Partial Public Class frmItemFilterList
    Inherits BACRMPage
    Dim objContact As CContacts

    Dim intFormID As Int16
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "Save()")
            intFormID = 76 ' GetQueryStringVal("FormID")

            If Not IsPostBack Then

                GetUserRightsForPage(37, 9)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False
                BindLists()
            End If

            lblTitle.Text = "Filter Settings"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindLists()
        Try
            Dim ds As DataSet
            objContact = New CContacts
            objContact.DomainID = Session("DomainId")
            objContact.FormId = intFormID
            objContact.UserCntID = Session("UserContactId")
            objContact.ContactType = 0

            ds = objContact.GetColumnConfiguration
            lstAvailablefld.DataSource = ds.Tables(0)
            lstAvailablefld.DataTextField = "vcFieldName"
            lstAvailablefld.DataValueField = "numFieldID"
            lstAvailablefld.DataBind()
            lstSelectedfld.DataSource = ds.Tables(1)
            lstSelectedfld.DataValueField = "numFieldID"
            lstSelectedfld.DataTextField = "vcFieldName"
            lstSelectedfld.DataBind()

            'If ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
            '    chkArchivedItems.Checked = CCommon.ToBool(ds.Tables(2).Rows(0)("Custom"))
            'End If
            'btnSave.Attributes.Add("onclick", "javascript:opener.location.reload(true);window.close();return false;")
            btnSave.Attributes.Add("onclick", "Save();")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objContact Is Nothing Then objContact = New CContacts

            Dim dsNew As New DataSet
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numFieldID")
            dtTable.Columns.Add("bitCustom")
            dtTable.Columns.Add("tintOrder")
            Dim i As Integer

            Dim dr As DataRow
            Dim str As String()
            str = hdnCol.Value.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numFieldID") = str(i).Split("~")(0)
                dr("bitCustom") = str(i).Split("~")(1)
                dr("tintOrder") = i
                dtTable.Rows.Add(dr)
            Next

            'dr = dtTable.NewRow
            'dr("numFieldID") = -1
            'dr("bitCustom") = If(chkArchivedItems.Checked, 1, 0)
            'dr("tintOrder") = 6
            'dtTable.Rows.Add(dr)
            'dtTable.AcceptChanges()

            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)

            objContact.FormId = intFormID
            objContact.ContactType = 0
            objContact.DomainID = Session("DomainId")
            objContact.UserCntID = Session("UserContactId")
            objContact.strXml = dsNew.GetXml
            objContact.SaveContactColumnConfiguration()
            dsNew.Tables.Remove(dsNew.Tables(0))

            BindLists()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "Reload", "javascript:opener.location.reload(true);window.close();return false;", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class