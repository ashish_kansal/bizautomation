﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCategoryAdd.aspx.vb" MasterPageFile="~/common/ECommerceMenuMaster.Master" Inherits=".frmCategoryAdd" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Category Detail</title>
    <script type="text/javascript" src="../JavaScript/jquery.ae.image.resize.min.js"></script>

    <script type="text/javascript">

        //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html
        $(function () {
            $(".resizeme").aeImageResize({ height: 50, width: 50 });
        });

        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    console.log(img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    var siteID = document.getElementById('hdnSiteID').value;
                    img.setAttribute("src", img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    console.log(img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }
        function afterClientCheck(tree, eventArgs) {
            var node = eventArgs.get_node();
            if (node.get_checked()) {
                for (var i = 0; i < tree.get_allNodes().length; i++) {
                    if (tree.get_allNodes()[i] != node)
                        tree.get_allNodes()[i].set_checked(false);
                }
            }
        }
        function treeExpandAllNodes() {
            var treeView = $find("rtvParentCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }
        function treeCollapseAllNodes() {
            var treeView = $find("rtvParentCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function Save() {
            if (document.getElementById("txtCategoryName").value == "") {
                alert("Enter Category Name")
                document.getElementById("txtCategoryName").focus();
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnSaveAndClose" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSaveAndClose" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Category Detail
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Category Name<span style="color: red"> *</span></label>
                        <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Friendly URL<span style="color: red"> *</span></label>
                        <asp:TextBox ID="txtFriendURL" runat="server" CssClass="form-control"></asp:TextBox>
                        <p>For example, if your category name is abc and you want url pattern be different than default bizautomation pattern then use this like following.<br /> 
                            1) www.xyz.com/abc instead of default bizautomation url pattern than type "abc" (Don't include domain name) in above text box. <br />
                            2) www.xyz.com/category/abc instead of default bizautomation url pattern than type "category/abc" (Don't include domain name) in above text box. <br />
                            Go to User Friendly URLs section in Design tab to complete the further process.</p>
                    </div>
                    <div class="form-group">
                        <label>Parent Category</label>
                        <div style="border: 1px solid lightgray; padding: 5px; overflow: auto; height: 300px;">
                            <a href="javascript: treeExpandAllNodes();">Expand All </a>
                            &nbsp; <a href="javascript: treeCollapseAllNodes();">Collapse All
                            </a>
                            <telerik:RadTreeView ID="rtvParentCategory" CheckBoxes="true" ClientIDMode="Static" OnClientNodeChecked="afterClientCheck" runat="server">
                                <DataBindings>
                                    <telerik:RadTreeNodeBinding Expanded="true"></telerik:RadTreeNodeBinding>
                                </DataBindings>
                            </telerik:RadTreeView>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Sort Order</label>
                        <asp:TextBox ID="txtSortOrder" onkeypress="javascript:CheckNumber(2,event);" runat="server" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Matrix Group</label>
                        <div>
                            <telerik:RadComboBox runat="server" ID="rcbMatrixGroup" Width="100%" CheckBoxes="true" EmptyMessage="Select Matrix Group(s)"></telerik:RadComboBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
             <div class="form-group">
                <label>Page Title (Optional)</label>
                <asp:TextBox ID="txtMetaTitle" class="form-control" runat="server">
                </asp:TextBox>
            </div>
            <div class="form-group">
                <label>Meta Keywords (Optional)</label>
                <asp:TextBox ID="txtMetaKeywords" class="form-control" runat="server" TextMode="MultiLine" Rows="4">
                </asp:TextBox>
            </div>
             <div class="form-group">
                <label>Meta Description (Optional)</label>
                <asp:TextBox ID="txtMetaDescription" class="form-control" runat="server" TextMode="MultiLine" Rows="4">
                </asp:TextBox>
            </div>
            <div class="form-group">
                <label>Description</label>
                <telerik:RadEditor ID="txtDescription" runat="server" Height="400px" OnClientPasteHtml="OnClientPasteHtml"
                    ToolsFile="~/Marketing/EditorTools.xml" AllowScripts="true">
                    <Content>
                    </Content>
                </telerik:RadEditor>
            </div>
            <div class="form-group">
                <label>Select Image</label>
                <input id="txtThumbFile" type="file" name="txtThumbFile" runat="server" style="width: 200px">
                <asp:Image ID="imgCategory" Class="resizeme" runat="server"></asp:Image>
                &nbsp;
                <asp:LinkButton ID="lbDeleteImage" Text="Delete Image" Visible="false" runat="server">
                </asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HiddenField2" runat="server" />
    <asp:HiddenField ID="hdnCategoryProfile" runat="server" />
</asp:Content>
