﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmItemUOMConversion.aspx.vb" Inherits=".frmItemUOMConversion" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #gvItemUOMConversion {
            border-collapse: collapse !important;
            border-color: #dad8d8 !important;
        }

        tr.is td, tr.ais td {
            border: 1px solid #dad8d8;
        }
    </style>
    <script type="text/javascript">
        function Delete(button) {
            function DeleteConfirm(arg) {
                if (arg) {
                    __doPostBack(button.name, "");
                }
            }

            radconfirm("UOM conversion will be deleted. Do you want to proceed?", DeleteConfirm, 450, 100, null, "Delete UOM Conversion");
            return false;
        }

        function CloseAndRefresh() {
            window.opener.location.href = window.opener.location.href;
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="return CloseAndRefresh();" CssClass="button" Style="float: right;" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Item UOM Conversion
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
                <ConfirmTemplate>
                    <div class="rwDialogPopup radconfirm">
                        <div class="rwDialogText">
                            {1}
                        </div>
                        <div class="rwDialogButtons">
                            <a class="rwPopupButton" onclick="$find('{0}').close(true); return false;"><span class="rwOuterSpan"><span class="rwInnerSpan">Yes</span></span></a>
                            <a class="rwPopupButton" onclick="$find('{0}').close(false); return false;"><span class="rwOuterSpan"><span class="rwInnerSpan">No</span></span></a>
                        </div>
                    </div>
                </ConfirmTemplate>
            </telerik:RadWindowManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <table width="800" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="lblException" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlInsert" runat="server">
                            <table style="margin: 0 auto;">
                                <tr>
                                    <td>1
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSourceUOM" runat="server" Width="120"></asp:DropDownList>
                                    </td>
                                    <td>=
                                    </td>
                                    <td style="white-space: nowrap;">
                                        <asp:TextBox ID="txtUnits" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblBaseUOM" runat="server" Text="Base UOM"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlTargetUOM" runat="server" Width="120" Visible="false"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView CssClass="dg" ID="gvItemUOMConversion" runat="server" AutoGenerateColumns="false" Width="100%">
                            <HeaderStyle Height="25" CssClass="hs"  Font-Bold="true"/>
                            <RowStyle CssClass="is" HorizontalAlign="Center" Wrap="false" />
                            <AlternatingRowStyle CssClass="ais" HorizontalAlign="Center" Wrap="false" />
                            <EmptyDataRowStyle CssClass="is" Font-Bold="true" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        UOM Conversion
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# "<b>1</b> <I>" & Eval("SourceUOM") & "</I> = <b>" & Eval("numTargetUnit") & "</b> <I>" & Eval("TargetUOM") & "</I>"%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="25">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" CommandName="Delete" OnClientClick="return Delete(this);" ImageUrl="../images/delete2.gif" Height="20" ToolTip="Delete" CommandArgument='<%# Eval("numItemUOMConvID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnItemCode" runat="server" />
    <asp:HiddenField ID="hdnBaseUOM" runat="server" />
</asp:Content>
