<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/DetailPage.Master" ValidateRequest="false"
    CodeBehind="frmKitDetails.aspx.vb" Inherits=".Items_frmKitDetails" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Item Details</title>
    <script src="../javascript/biz.kitswithchildkits.js" type="text/javascript"></script>
    <script src="../javascript/slick.min.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../CSS/slick.css" />
    <link rel="Stylesheet" href="../CSS/slick-theme.css" />
    <style type="text/css">
        a.imagelink {
            background: url(~/images/StockTransfer.png) no-repeat left top;
            padding-left: 25px;
        }

        .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px; /*width: 125px;*/
        }

        .rcbHeader ul, .rcbFooter ul, .rcbItem ul, .rcbHovered ul, .rcbDisabled ul, .ulItems {
            width: 100%;
            display: inline-block;
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        .rcbList {
            display: inline-block;
            width: 100%;
            list-style-type: none;
        }

        .col1, .col2, .col3, .col4, .col5, .col6 {
            float: left;
            width: 80px;
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            display: block; /*border:0px solid red;*/
        }

        .col1 {
            width: 140px;
        }

        .col2 {
            width: 200px;
        }

        .col3 {
            width: 60px;
            text-align: center;
        }

        .col4 {
            width: 60px;
            text-align: center;
        }

        .col5 {
            width: 75px;
            text-align: center;
        }

        .col6 {
            width: 60px;
            text-align: center;
        }

        input[type="checkbox"] {
            margin: 0px !important;
        }

        .rcbInput {
            height: 19px !important;
        }

        .tooltip {
            width: 500px;
        }

        .gridViewPager td {
            padding-left: 4px;
            padding-right: 4px;
            padding-top: 1px;
            padding-bottom: 2px;
        }

        #divKitAssembltChildItems .select2-container {
            margin-top: 0px !important;
            margin-left: 0px !important;
        }

        #divKitChild img.ul-child-kit-item-image {
            margin: 0 auto;
        }

        #divKitChild ul.ul-child-kit-item {
            font-size: 16px;
        }

        #divKitChild ul.ul-child-kit-item {
            font-size: 16px;
        }

        #divKitChild li.ul-child-kit-item-name {
            font-weight: bold;
        }

        #divKitChild .slick-track {
            display: flex !important;
        }

        #divKitChild .slick-item {
            background-color: #fff;
            border: 1px solid #dbdbdb;
            margin: 0 17px;
            height: inherit !important;
            outline: none;
        }

            #divKitChild .slick-item:hover, #divKitChild .slick-item:focus {
                background-color: #efefef;
                color: #282828;
                border: 1px solid #000;
            }

            #divKitChild .slick-item.active {
                background-color: #efefef;
                color: #282828;
                border: 1px solid #dbdbdb !important;
            }

        .slick-prev:before, .slick-next:before {
            font-family: FontAwesome;
            font-size: 20px;
            line-height: 1;
            color: #9e9e9e;
            opacity: 0.75;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .slick-prev:before {
            content: "\f053";
        }

        [dir="rtl"] .slick-prev:before {
            content: "\f054";
        }

        .slick-next:before {
            content: "\f054";
        }

        [dir="rtl"] .slick-next:before {
            content: "\f053";
        }
    </style>
    <script type="text/javascript" src="/BACRMUI/JavaScript/jquery.slimscroll.min.js"></script>
    <script type="text/javascript">
        var columns;
        var userDefaultPageSize = '<%= Session("PagingRows")%>';
        var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
        var varPageSize = parseInt(userDefaultPageSize, 10);
        var varCharSearch = 1;
        if (parseInt(userDefaultCharSearch, 10) > 0) {
            varCharSearch = parseInt(userDefaultCharSearch, 10);
        }

        function SetSelectedSerialLot(serialLot) {
            $("[id$=hdnSelectedSerialLot]").val(serialLot);
            var qty = 0;
            var array = serialLot.split(",");
            $.each(array, function (i) {
                qty += parseInt(array[i].toString().split("-")[1]);
            });
            $("[id$=hplSerialLot]").text("Added (" + qty + ")");
        }

        function OpenSertailLot(oppID, oppItemID, itemID, warehouseItemID, itemType) {
            window.open('../opportunity/frmAddSerialLotNumbers.aspx?oppID=' + oppID + '&oppItemID=' + oppItemID + '&itemID=' + itemID + '&warehouseItemID=' + warehouseItemID + "&itemType=" + itemType + '&type=1', '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
            return false;
        }

        function getCalculatedPrice() {
            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetKitAssemblyCalculatedPrice',
                data: JSON.stringify({
                    itemCode: parseInt($("[id$=hdnItemId]").val() || 0),
                    divisionID: 0,
                    quantity: 1,
                    priceBasedOn: parseInt($("[id$=ddlKitAssemblyPriceBasedOn]").val())
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $("[id$=lblCalculatedPrice]").text("$" + data.d);

                    $("[id$=divKitAssemblyPrice]").show();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error occured while getting calculated price: " + errorThrown);
                }
            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });


        function treeExpandAllNodes() {
            var treeView = $find("rtvCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }

        function treeCollapseAllNodes() {
            var treeView = $find("rtvCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }
        function CheckRadio(a, b) {
            $("[id$=dgVendor] tr").not(':first').each(function () {
                $(this).find("[id$=radPreffered]").attr("checked", false);
            });

            document.getElementById(a).checked = true;
            $("#txtVendorId").val(b.innerText);
        }
        function OpenInventoryReport(a) {
            var w = screen.width;
            var h = screen.height;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("../Items/frmInventoryAudit.aspx?WareHouseItemID=" + a, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletePrimaryVendor() {
            alert("You can not delete primary window because item is used in assembly.");
            return true;
        }
        function Add() {
            if ($find('radCmbCompany').get_value() == '') {
                alert("Select Vendor")
                return false;
            }
        }
        function openAdjust(a) {
            window.location.href = "../Items/frmInventoryAdjustmentInBatch.aspx";
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function openVendor(a) {
            document.getElementById('cntDoc').src = "../Items/frmVendorList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=" + a;
            document.getElementById('divVendor').style.visibility = "visible";
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;
            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;
            }
        }
        function Save(a, b) {
            var radTab = $find("radItemTab");

            if (b == 1) {
                if ($find('radCmbOrganization').get_value() == "") {
                    alert("Select Asset Owner");
                    radTab.set_selectedIndex(0);
                    $find('radCmbOrganization').get_inputDomElement().focus();
                    return false;
                }

                if (document.getElementById("txtAssetName").value == "") {
                    alert("Enter Asset name");
                    radTab.set_selectedIndex(0);
                    document.getElementById("txtAssetName").focus();
                    return false;
                }

                if (document.getElementById("ddlAssetItemType").value == 0) {
                    alert("Select Item Type");
                    radTab.set_selectedIndex(0);
                    document.getElementById("ddlAssetItemType").focus();
                    return false;
                }

                if (document.getElementById("optAppreciation").checked == true) {
                    if (parseFloat(document.getElementById("txtAppreciation").value) == 0) {
                        alert("Enter Appreciation");
                        document.getElementById("txtAppreciation").focus();
                        return false;
                    }
                }

                if (document.getElementById("optDepreciation").checked == true) {
                    if (parseFloat(document.getElementById("txtDepreciation").value) == 0) {
                        alert("Enter Depreciation");
                        document.getElementById("txtDepreciation").focus();
                        return false;
                    }
                }
            }
            else {
                if (document.getElementById("txtItem").value == "") {
                    alert("Enter item/kit name");
                    radTab.set_selectedIndex(0);
                    document.getElementById("txtItem").focus();
                    return false;
                }

                if (document.getElementById("ddlProduct").value == 0) {
                    alert("Select item type");
                    radTab.set_selectedIndex(0);
                    document.getElementById("ddlProduct").focus();
                    return false;
                }

                if (parseInt(a) != 104) {
                    if (document.getElementById("ddlIncomeAccount") != null)
                        if (document.getElementById("ddlIncomeAccount").value == 0) {
                            alert("Select Income Account");
                            radTab.set_selectedIndex(0);
                            document.getElementById("ddlIncomeAccount").focus();
                            return false;
                        }
                    if (document.getElementById("ddlAssetAccount") != null)
                        if (document.getElementById("ddlAssetAccount").value == 0) {
                            alert("Select Asset Account");
                            radTab.set_selectedIndex(0);
                            document.getElementById("ddlAssetAccount").focus();
                            return false;
                        }
                    if (document.getElementById("ddlCOGS") != null)
                        if (document.getElementById("ddlCOGS").value == 0) {
                            alert("Select COGS(Expense) Account");
                            radTab.set_selectedIndex(0);
                            document.getElementById("ddlCOGS").focus();
                            return false;
                        }
                }

                if ($('[id$=chkAllowDropShip]').is(':checked')) {
                    var bPrefferedVendor = false;
                    $("[id$=dgVendor] tr").each(function () {
                        if ($(this).find("[id*='radPreffered']").is(':checked')) {
                            bPrefferedVendor = true;
                        }
                    });

                    if (bPrefferedVendor == false) {
                        alert("You must add a primary vendor to this item first for Drop-Ship.");
                        return false;
                    }
                }
            }
            if ($("#<%=ddlContainer.ClientID%> option:selected").val() > 0) {
                if ($("#<%=ddlBaseUOM.ClientID%> option:selected").val() <= 0) {
                    alert("Please Select Base UOM");
                    $("#<%=ddlBaseUOM.ClientID%>").focus();
                    return false;
                }
                if ($("#<%=txtNoOfItem.ClientID%>").val() > 0) {
                } else {
                    alert("Please enter no of Item added to container");
                    $("#<%=txtNoOfItem.ClientID%>").focus();
                    return false;
                }
            }
        }
        function DeleteRecord() {

            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenImage(a) {
            window.open('../Items/frmItemImage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=250,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenWebAPIItem(a) {
            window.open('../Items/frmWebAPIItemDetails.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=250,width=720,height=600,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenShopCartDTL(a) {
            window.open('../Items/frmShopCartList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=250,width=720,height=600,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTax(a) {
            window.open('../Items/frmCommericialTax.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=250,width=650,height=400,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenOptAnAcc(a) {
            window.open('../items/frmOptAndAccPopup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300,top=250,width=650,height=400,scrollbars=yes,resizable=yes');
            return false;
        }

        function ShowSelectParents() {
            if (document.getElementById("radRegularItem").checked == true) {
                if (hplSelectKits != null) {
                    hplSelectKits.style.display = 'none';
                }

            }
            else {
                if (hplSelectKits != null) {
                    hplSelectKits.style.display = '';
                }
            }
        }
        function KitAssembly() {

            //if (document.getElementById('chkAssembly').checked == true) {
            //    document.getElementById('chkKit').checked = true;
            //}
        }
        function OpenManAssem(a) {
            var h = screen.height;
            var w = screen.width;

            window.open('../Items/frmManageAssembly.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenDisassembleWindow(a) {
            window.open('../Items/frmDisassembleItem.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }
        function SerialNo(a, b, c, d, e, f) {
            window.open('../Items/frmSerializeitems.aspx?ItemCode=' + a + '&Serialize=' + b + '&numWarehouseItemID=' + c + '&vcExternalLocation=' + d + '&vcInternalLocationID=' + e + '&vcInternalLocationText=' + f, '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }
        function MatrixItem(a, b, c, d, e, f, g) {
            window.open('../Items/frmAttributes.aspx?ItemGroup=' + a + '&ItemCode=' + b + '&ListPrice=' + c + '&SKU=' + d + '&Barcode=' + e + '&numWarehouseItemID=' + f + '&IsMatrix=' + g, '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }
        function AddEditInventory(a, b, c) {
            window.location.href = '../Items/frmInventoryAdjustmentInBatch.aspx?ItemCode=' + a + '&WarehouseItemID=' + b + '&WarehouseID=' + c;
            return false;
        }
        function SerialAssets(a, b) {
            window.open('../Items/frmSerializeAssets.aspx?ItemCode=' + a + '&DivisionID=' + b, '', 'toolbar=no,titlebar=no,left=200,top=250,width=950,height=550,scrollbars=yes,resizable=yes');
            return false;
        }
        function AddChildItem() {
            var obj = $('#txtChildItem').select2('data');

            if (obj == null) {
                alert("Please Select Child Item")
                return false;
            } else if (obj.charItemType == "P" && !obj.bitKitParent && !obj.bitAssembly && parseInt(obj.numDivisionID || 0) == 0) {
                if ($find('radcmbChildPrimaryVendor').get_value() == '') {
                    alert("Select primary vendor for selected item");
                    return false;
                } else {
                    $("[id$=hdnSelectedChildItemPrimaryVendor]").val($find('radcmbChildPrimaryVendor').get_value());
                }
            } else {
                $("[id$=hdnSelectedChildItemPrimaryVendor]").val("0");
            }

            $("[id$=hdnSelectedChildItem]").val(obj.id.toString().split("-")[0]);

            return true;
        }

        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    console.log(img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    var siteID = document.getElementById('hdnSiteID').value;
                    img.setAttribute("src", img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    console.log(img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }

        function SingleSelect(current) {
            if (current.id == 'chkSerializedItem') {
                document.getElementById('chkLotNo').checked = false;
            }
            else if (current.id == 'chkLotNo') {
                document.getElementById('chkSerializedItem').checked = false;
            }
        }

        function DeleteSelected(a) {
            var ida = a.parentNode.parentNode.parentNode.parentNode.id;

            var mytool_array = ida.split("_");
            document.getElementById('hfRowIndex').value = mytool_array[2];

        }

        function SelectAll(a) {
            var ida = a.parentNode.parentNode.parentNode.parentNode.id;

            var mytool_array = ida.split("_");

            var _grid = igtbl_getGridById("uwItemTabxxctl2xUltraWebGrid1");

            var row = _grid.Rows.getRow(mytool_array[2]);

            var ChildRows = row.getChildRows();
            if (ChildRows != null) {
                for (j = 0; j < ChildRows.length; j++) {

                    var ChildRow = ChildRows[j];
                    var child = igtbl_getRowById(ChildRow.id);

                    //getting the cell object
                    var _CellObj = child.getCellFromKey("Select");

                    //getting the checkbox with the object or tag name.
                    If(a.checked)
                    child.getCellFromKey("Select").setValue(1);
                    Else
                    child.getCellFromKey("Select").setValue(0);
                }
            }
        }

        function CheckConversion(a) {
            var ddUOM = document.getElementById(a.id.substring(0, a.id.lastIndexOf("_")) + "_ddlItemUOM");
            var txtQty = document.getElementById(a.id.substring(0, a.id.lastIndexOf("_")) + "_txtQuantity");

            var ConversionFactor = 1;

            if (ddUOM != null) {
                ConversionFactor = ddUOM.options[ddUOM.selectedIndex].value.split("~")[1];
            }

            var Conversion = (ConversionFactor * txtQty.value)
            //if (!isInt(Conversion)) {
            //    if (isInt(Math.round(Conversion))) {
            //        alert("Converted Base UOM is " + Conversion + " but will be rounded off to " + Math.round(Conversion));
            //    }
            //    else {
            //        alert("Enter valid Quantity");
            //        txtQty.value = "0";
            //    }
            //}
        }

        function isInt(n) {
            return n % 1 == 0;
        }

        function OpenVendorPriceRange(a, b) {
            window.location.href = '../items/frmPriceBookMgmtList.aspx?ItemCode=' + a + '&Vendor=' + b;
            return false;
        }

        function OpenVendorsWareHouse(a, b, c) {
            window.open('../items/frmVendorWareHouse.aspx?Vendor=' + a + '&VWarehouse=' + b + '&ItemCode=' + c, '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenActionItem(a) {
            window.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CommId=" + a;

            return false;
        }

        function OpenOpp(a, b, c) {

            var str;
            if (c <= 0) {
                str = "../opportunity/frmOpportunities.aspx?frm=ItemDetails&OpID=" + a;
            }
            else {
                str = "../opportunity/frmReturnDetail.aspx?ReturnID=" + a + "&title = ";
            }

            window.open(str, '_blank');
            return false;
        }

        function OpenOppOnRightClick(a, b, c) {
            var str;
            if (c <= 0) {
                window.open("../opportunity/frmOpportunities.aspx?frm=ItemDetails&OpID=" + a, '_blank');
            }
            else {
                window.open("../opportunity/frmReturnDetail.aspx?ReturnID=" + a + "&title = ", '_blank');
            }
        }

        function OpenItemDependenciesParentItems(a) {
            window.open('../Items/frmItemDependenciesParentItems.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenPriceLevel(a) {
            var h = screen.height;
            var w = screen.width;

            window.open('../Items/frmPricingTable.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=200,top=50,width=900,height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenGLTransaction(a) {
            window.location.href = "../Accounting/frmNewJournalEntry.aspx?frm=ItemDetail&JournalId=" + a + "&ItemCode=" + document.getElementById("hdnItemId").value;

            return false;
        }

        function OpenBizIncome(url) {
            window.open(url, "BizInvoiceGL", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }

        function reDirect(a) {
            document.location.href = a;
            return false;
        }

        function OpenBill(a) {
            window.open('../Accounting/frmAddBill.aspx?BillID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenTimeExpense(url) {

            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
            return false;
        }

        function OpenGL(lngItemCode) {
            window.open("../Accounting/frmGeneralLedger.aspx?ItemCode=" + lngItemCode + "&Mode=6", '_blank')
            window.focus();
            return false;
        }

        function OpenChildMembership() {
            var h = screen.height;
            var w = screen.width;
            var itemCode = $("[id$=hdnItemId]").val() || 0;
            window.open('../Items/frmItemParent.aspx?ItemCode=' + itemCode, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OnClientItemsRequesting(sender, eventArgs) {
            var context = eventArgs.get_context();

            if ($("[id$=hdnIsAssembly]").val() == "1") {
                context["bitAssembly"] = "1";
            } else {
                context["bitAssembly"] = "0";
            }

            context["ItemCode"] = $("[id$=hdnItemId]").val() || 0;
        }

        function ExpandCollapse(id) {
            try {
                var uniqueCode = id.getAttribute("UniqueID");
                var className = id.className;
                var newClassName;

                if (className.indexOf("rtlCollapse") >= 0) {
                    newClassName = className.replace("rtlCollapse", "rtlExpand");
                    id.className = newClassName;
                } else if (className.indexOf("rtlExpand") >= 0) {
                    newClassName = className.replace("rtlExpand", "rtlCollapse");
                    id.className = newClassName;
                }


                if (id != null) {
                    $('[id$=rtlChildItems] table > tbody > tr[uniqueid*="' + uniqueCode + "-" + '"]').each(function () {
                        if ($(this).attr("UniqueID").indexOf(uniqueCode + "-") >= 0) {
                            var btn = $(this).find("[id$=ExpandCollapseButton]")

                            if (className.indexOf("rtlCollapse") >= 0) {
                                $(this).hide();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlCollapse", "rtlExpand");
                                }

                            } else if (className.indexOf("rtlExpand") >= 0) {
                                $(this).show();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlExpand", "rtlCollapse");
                                }
                            }
                        }
                    });
                }
            }
            catch (ex) {
            }
            finally {
                return false;
            }
        }

        function OpenItemUOMConversion(itemCode) {
            window.open('../Items/frmItemUOMConversion.aspx?ItemCode=' + itemCode, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=300,height=600,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Items.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OpenMassStockTransfer(itemCode, warehouseItemID) {
            itemName = $("[id$=lblItemID]").text().replace(/'/g, "");;
            window.open('../opportunity/frmMassStockTransfer.aspx?ItemName=' + itemName + "&ItemCode=" + itemCode + '&WarehouseItemID=' + warehouseItemID, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=1200,height=600,scrollbars=yes,resizable=yes');
            return false;
        }

        function ClearInternalLocation(lkbClearSelection, warehouseItemID, internLocationID) {
            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/ClearWarehouseInternalLocation',
                data: JSON.stringify({
                    warehouseItemID: warehouseItemID
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $(lkbClearSelection).hide();
                    var combo = $find(internLocationID);
                    combo.clearSelection();
                    combo.set_emptyMessage("");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error occured while clearing internal location: " + errorThrown);
                }
            });

            return false;
        }

        function onRadComboBoxLoad(sender) {
            var div = sender.get_element();

            $telerik.$(div).bind('mouseenter', function () {
                if (!sender.get_dropDownVisible()) {
                    sender.showDropDown();
                }
            });


            $telerik.$(".RadComboBoxDropDown").mouseleave(function (e) {
                hideDropDown("#" + sender.get_id(), sender, e);
            });


            $telerik.$(div).mouseleave(function (e) {
                hideDropDown(".RadComboBoxDropDown", sender, e);
            });


        }

        function hideDropDown(selector, combo, e) {
            var tgt = e.relatedTarget;
            var parent = $telerik.$(selector)[0];
            var parents = $telerik.$(tgt).parents(selector);

            if (tgt != parent && parents.length == 0) {
                if (combo.get_dropDownVisible())
                    combo.hideDropDown();
            }
        }

        function Save() {
            var datePicker = $telerik.findControl(document, "calAdjustmentDate");
            var date = datePicker.get_selectedDate();

            if (datePicker.isEmpty()) {
                alert('Please enter adjustment date');
                return false;
            }

            return true;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });


        function CheckNumberAllowPlus(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!((k > 47 && k < 58) || k == 43 || k == 44 || k == 45 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!((k > 47 && k < 58) || k == 43 || k == 45 || k == 8 || k == 37 || k == 39 || k == 16 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function UpdateAverageCostForLineItem(CurrentRow) {

            var AdjQty = (Number($(CurrentRow).find("[id$=txtCounted]").val()) - Number($(CurrentRow).find("[id$=lblOnHandQty]").text())).toPrecision(8);

            var TotalQty = (Number($(CurrentRow).find("[id$=lblOnHandQty]").text()) + Number(AdjQty)).toPrecision(8);
            if (AdjQty != 0) {
                if (AdjQty > 0) { $(CurrentRow).find("[id$=txtAdjQty]").val("+" + AdjQty.toString()).css("color", "green"); }
                else { $(CurrentRow).find("[id$=txtAdjQty]").val(AdjQty.toString()).css("color", "red"); }
            }
            var NewInventoryValue = (Number($(CurrentRow).find("[id$=txtCounted]").val()) * Number($(CurrentRow).find("[id$=lblAverageCost]").text())).toPrecision(8);
            $(CurrentRow).find("[id$=lblNewInventoryValue]").text(parseFloat(NewInventoryValue).toFixed(2));

        }

        function UpdateTotalFooter() {
            var sumOfValues1 = 0.0;
            var sumOfValues2 = 0.0;

            $("[id$=gvInventory] tr").each(function () {

                //if ($(this).find("[id$=chk]").is(':checked')) {
                //    sumOfValues1 += Number($(this).find("[id$=lblCurrentInventoryValue]").text());
                //    sumOfValues2 += Number($(this).find("[id$=lblNewInventoryValue]").text());
                //}


                sumOfValues1 += Number($(this).find("[id$=lblCurrentInventoryValue]").text());
                sumOfValues2 += Number($(this).find("[id$=lblNewInventoryValue]").text());


            });

            $("[id$=lblCurrentInventoryValueTotal]").text(parseFloat(sumOfValues1).toFixed(2));
            $("[id$=lblNewInventoryValueTotal]").text(parseFloat(sumOfValues2).toFixed(2));

        }


        function roundNumber(num, dec) {
            var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
            return result;
        }

        function DeleteWarehouse() {
            if (confirm("Warehouse will be deleted. Do you want to proceed?")) {
                return true;
            } else {
                return false;
            }
        }

        function DeleteWarehouseLocation() {
            if (confirm("Warehouse location will be deleted. Do you want to proceed?")) {
                return true;
            } else {
                return false;
            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        //Used by Update Panel
        function pageLoaded() {
            BindInlineEdit();

            $("#ddlKitAsseblyChildItemSearchType").val($("#hdnKitAsseblyChildItemSearchType").val());

            if ($("[id$=chkAmtBasedonIndItems]").is(":checked")) {
                getCalculatedPrice();
            }

            $("[id$=chkExpense]").change(function () {
                if ($(this).is(":checked")) {
                    $("[id$=ddlExpense]").show();
                    $("[id$=lblExpense]").show();
                    $("[id$=tdAsset1]").show();
                    $("[id$=tdddlAsset1]").show();
                } else {
                    $("[id$=ddlExpense]").hide();
                    $("[id$=lblExpense]").hide();
                    $("[id$=tdAsset1]").hide();
                    $("[id$=tdddlAsset1]").hide();
                }
            });

            $('[id$=chkRental]').change(function () {
                if ($(this).is(":checked")) {
                    if (!$('[id$=chkAsset]').is(":checked")) {
                        $('[id$=chkAsset]').prop("checked", true);
                    }

                    $('[id$=chkAsset]').attr("disabled", true);
                } else {
                    $('[id$=chkAsset]').removeAttr("disabled");
                }
            });

            $('[id$=chkLotNo]').change(function () {
                if ($(this).is(":checked")) {
                    if ($('[id$=pnlAssetRental]').is(":visible")) {
                        $('[id$=pnlAssetRental]').hide();
                    }
                }
                else {
                    if (!$('[id$=pnlAssetRental]').is(":visible")) {
                        $('[id$=pnlAssetRental]').show();
                    }
                }
            });

            $(':input:enabled:visible:first[type=text]').focus()
            var mainChkBox = $(".cbHeader input");

            mainChkBox.click(function () {
                if (this.checked) {
                    $('[id$=radCategoryList_DropDown] .rcbCheckBox').each(function () {
                        var checkState = 0;
                        if (!this.checked) {
                            checkState = 1;
                        }
                        this.checked = true;
                        if (checkState == 1) {
                            this.click();
                        }
                    });

                }
                else {

                    $('[id$=radCategoryList_DropDown] .rcbCheckBox').each(
                        function () {
                            var checkState = 0;
                            if (this.checked) {
                                checkState = 1;
                            }
                            this.checked = false;
                            if (checkState == 1) {
                                this.click();
                            }

                        });


                }
            });

            $("[id$=chkVirtual]").change(function () {
                if ($(this).is(":checked")) {
                    $("[id$=txtAverageCost]").prop("disabled", "disabled");
                    $("[id$=txtAverageCost]").val("0.0000");
                } else {
                    $("[id$=txtAverageCost]").prop("disabled", "");
                }

            });

            $("[id$=chkAmtBasedonIndItems]").change(function () {
                if ($(this).is(":checked")) {
                    getCalculatedPrice();
                } else {
                    $("[id$=divKitAssemblyPrice]").hide();
                }
            });

            $("[id$=ddlKitAssemblyPriceBasedOn]").change(function () {
                if ($(this).is(":checked")) {
                    getCalculatedPrice();
                } else {
                    $("[id$=divKitAssemblyPrice]").hide();
                }
            });

            //Highlight selected row
            //$(".table-bordered tr").click(function () {
            //    $('.table-bordered tr>td').each(function () {
            //        $(this).css('background-color', '');
            //    });
            //    $(this).find('td').css('background-color', '#D6D7D8');
            //});


            $("[id$=txtCounted]").each(function () {
                var CurrentRow = $(this).parents("tr").filter(':first')
                var AdjQty = (Number($(CurrentRow).find("[id$=txtCounted]").val()) - Number($(CurrentRow).find("[id$=lblOnHandQty]").text())).toPrecision(8);

                var TotalQty = (Number($(CurrentRow).find("[id$=lblOnHandQty]").text()) + Number(AdjQty)).toPrecision(8);
                if (AdjQty != 0) {
                    if (AdjQty > 0) { $(CurrentRow).find("[id$=txtAdjQty]").val("+" + AdjQty.toString()).css("color", "green"); }
                    else { $(CurrentRow).find("[id$=txtAdjQty]").val(AdjQty.toString()).css("color", "red"); }
                }
                var NewInventoryValue = (Number($(CurrentRow).find("[id$=txtCounted]").val()) * Number($(CurrentRow).find("[id$=lblAverageCost]").text()))
                $(CurrentRow).find("[id$=lblNewInventoryValue]").text(parseFloat(NewInventoryValue).toFixed(2));

            })

            UpdateTotalFooter();

            $("[id$=txtCounted]").change(function () {
                UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
                UpdateTotalFooter();
            });

            //$("[id$=txtCounted]").keypress(function () {
            //    var CurrentRow = $(this).parents("tr").filter(':first');
            //    var chk = CurrentRow.find("[id$=chk]");
            //    if (chk != null) {
            //        chk.prop("checked", true);
            //    }
            //});

            var flag = 0;
            $("[id$=txtAdjQty]").keypress(function () {
                //var CurrentRow = $(this).parents("tr").filter(':first');
                //var chk = CurrentRow.find("[id$=chk]");
                //if (chk != null) {
                //    chk.prop("checked", true);
                //}


                if (CheckNumberAllowPlus(2, event)) {
                    flag = 1;
                }

            });

            $("[id$=txtAdjQty]").focusout(function () {
                if (flag = 1) {
                    var CurrentRow = $(this).parents("tr").filter(':first');
                    var CurrentOnHand = Number($(CurrentRow).find("[id$=lblOnHandQty]").text());
                    CurrentOnHand = (CurrentOnHand + Number($(CurrentRow).find("[id$=txtAdjQty]").val())).toPrecision(8);
                    if (CurrentOnHand < 0) {
                        alert('Adjustment can not be less than current on hand qty!');
                        $(CurrentRow).find("[id$=txtAdjQty]").val('');
                        return;
                    }
                    if (!isNaN(CurrentOnHand)) {
                        $(CurrentRow).find("[id$=txtCounted]").val(CurrentOnHand.toString());
                        UpdateAverageCostForLineItem($(this).parents("tr").filter(':first'))
                    }
                }

            });

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetItemsForKits',
                data: JSON.stringify({
                    searchText: "abcxyz",
                    pageIndex: 1,
                    pageSize: 10,
                    isGetDefaultColumn: true,
                    isAssembly: $("[id$=hdnIsAssembly]").val() == "1" ? true : false,
                    itemCode: ($("[id$=hdnItemId]").val() || 0),
                    searchType: "1"
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });

            function formatItem(row) {
                var numOfColumns = 0;
                var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
                if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                    ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
                } else {
                    ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
                }

                ui = ui + "</td>";
                ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
                ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                $.each(columns, function (index, column) {
                    if (numOfColumns == 4) {
                        ui = ui + "</tr><tr>";
                        numOfColumns = 0;
                    }

                    if (numOfColumns == 0) {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    else {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    numOfColumns += 2;
                });
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";
                ui = ui + "</td>";
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";

                return ui;
            }

            $('#txtChildItem').select2(
                {
                    placeholder: 'Select Item',
                    minimumInputLength: varCharSearch,
                    multiple: false,
                    formatResult: formatItem,
                    width: "200px",
                    dropdownCssClass: "bigdrop",
                    dataType: "json",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '../common/Common.asmx/GetItemsForKits',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            return JSON.stringify({
                                searchText: term,
                                pageIndex: page,
                                pageSize: varPageSize,
                                isGetDefaultColumn: false,
                                isAssembly: $("[id$=hdnIsAssembly]").val() == "1" ? true : false,
                                itemCode: ($("[id$=hdnItemId]").val() || 0),
                                searchType: $("#ddlKitAsseblyChildItemSearchType").val()
                            });
                        },
                        results: function (data, page) {

                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            var more = (page.page * varPageSize) < data.Total;
                            return { results: $.parseJSON(data.results), more: more };
                        }
                    }
                });

            $('#txtChildItem').on("change", function (e) {
                if (e.added != null) {
                    if (e.added.charItemType == "P" && !e.added.bitKitParent && !e.added.bitAssembly && parseInt(e.added.numDivisionID || 0) == 0) {
                        $("[id$=divChildItemPrimaryVendor]").show();
                    } else {
                        $("[id$=divChildItemPrimaryVendor]").hide();
                    }
                }
            });

            $("#rbAssetGridUnit").change(function () {
                if ($(this).is(":checked")) {
                    $("#tblAssetDepreciation td.tdUnit").show();
                    $("#tblAssetDepreciation td.tdTotal").hide();
                } else {
                    $("#tblAssetDepreciation td.tdUnit").hide();
                    $("#tblAssetDepreciation td.tdTotal").show();
                }
            });
            $("#rbAssetGridTotal").change(function () {
                if ($(this).is(":checked")) {
                    $("#tblAssetDepreciation td.tdUnit").hide();
                    $("#tblAssetDepreciation td.tdTotal").show();
                } else {
                    $("#tblAssetDepreciation td.tdUnit").show();
                    $("#tblAssetDepreciation td.tdTotal").hide();
                }
            });

            $("#ddlKitAsseblyChildItemSearchType").change(function () {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/SavePersistTable',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "isMasterPage": true
                        , "boolOnlyURL": false
                        , "strParam": "KitAsseblyChildItemSearchType"
                        , "strPageName": ""
                        , "values": "[{\"key\": \"KitAsseblyChildItemSearchType\",\"value\" : \"" + $("#ddlKitAsseblyChildItemSearchType").val() + "\"}]"
                    }),
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error occurred.");
                    }
                });
            });
        }

        function OpenItem(a) {
            var win = window.open("../Items/frmKitDetails.aspx?ItemCode=" + a, '_blank');
            if (win) {
                win.focus();
            }
        }

        function ChangeMessageCSS(isSuccess) {
            if (isSuccess == 1) {
                $("#divMessage").find("div.alert").removeClass("alert-warning").addClass("alert-success");
                $("#divMessage").find("i.icon").removeClass("fa-warning").addClass("fa-check");
            } else {
                $("#divMessage").find("div.alert").removeClass("alert-success").addClass("alert-warning");
                $("#divMessage").find("i.icon").removeClass("fa-check").addClass("fa-warning");
            }
        }

        function SaveCurrencyPrice() {
            $("[id$=UpdateProgress]").show();

            try {
                var arrRecords = [];

                $("[id$=gvCurrencies] > tbody > tr").not(":first").each(function (index, tr) {
                    if ($(this).find("[id$=txtCurrencyPrice]").length > 0) {
                        var objItem = {};
                        objItem.numCurrencyID = parseInt($(tr).find("[id$=hdnCurrencyID]").val());
                        objItem.monListPrice = parseFloat($(this).find("[id$=txtCurrencyPrice]").val().replace(",", ""));
                        arrRecords.push(objItem);
                    }
                });

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/SaveItemCurrencyPrice',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "itemCode": parseInt($("[id$=hdnItemId]").val())
                        , "records": JSON.stringify(arrRecords)
                    }),
                    success: function (data) {
                        $("[id$=UpdateProgress]").hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("[id$=UpdateProgress]").hide();

                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred while updating alt currencies price.");
                        }
                    }
                });
            } catch (e) {
                $("[id$=UpdateProgress]").hide();
                alert("Unknown error occurred while updating alt currencies price.");
            }
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function OpenItemCustomFieldLink(id) {
            if ($("#" + id) != null && $("#" + id).val() != "") {
                window.open($("#" + id).val(), "_blank");
            }
        }

        function OpenNewItemPopup(url) {
            if (url != "") {
                OpenPopUp(url + "&isFromAssemblyKit=true");
            }
        }

        function SelectNewAddedItem(itemCode, itemName) {
            $('#txtChildItem').select2('data', { id: itemCode, text: itemName });
        }

        function OpenPopUp(url) {
            window.open(url, "", "toolbar = no, titlebar = no, left = 100, top = 100, width = 1200, height = 645, scrollbars = yes, resizable = yes");
        }

        function validateAssetDepreciationPeriod(e) {
            var newValue = e.target.value;
            if (!e.target.validity.valid) {
                newValue = parseInt(newValue) + 0.5;
            }

            if (newValue > 30) {
                newValue = 30;
            } else if (newValue < 1) {
                newValue = "";
            }

            e.target.value = newValue;
        }

        function EditRate(e) {
            $(e.target).closest("td").find("span.rate").hide();
            $(e.target).closest("td").find("input.txtRate").show();
            e.preventDefault();
        }

        function UpdateRate(event) {
            if (event.keyCode === 13) {
                event.preventDefault();

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/UpdateItemDepreciationRate',
                    contentType: "application/json",
                    dataType: "json",
                    async: false,
                    data: JSON.stringify({
                        "id": parseInt($(event.target).attr("id"))
                        , "rate": parseFloat($(event.target).val())
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgress]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgress]").hide();
                    },
                    success: function (data) {
                        document.location.href = document.location.href;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert($.parseJSON(jqXHR.responseText));
                    }
                });
            }
        }

        function OpenConfigurator() {
            $("#divKitChild #divChildKitContainer").html("");
            LoadChildKits(parseInt($("[id$=hdnItemId]").val()));
        }

        function OpenVendorCostTable(numVendorTCode,numItemCode,numVendorID) {
            var w = 1200;
            var h = 700;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("../common/frmManageVendorCostTable.aspx?IsUpdateDefaultVendorCostRange=0&VendorID=" + numVendorID + "&ItemID=" + numItemCode + "&VendorTCode=" + numVendorTCode, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
    <style type="text/css">
        #Table1 {
            width: 69%;
        }

        .tblspace {
            border-collapse: separate;
            border-spacing: 5px;
        }

            .tblspace td {
                font-weight: bold;
            }

                .tblspace td input, select {
                    font-weight: normal;
                }

        #ctl00_TabsPlaceHolder_gvInventory .rgMasterTable {
            border-collapse: collapse !important;
            border-color: #dad8d8 !important;
        }

        .bigdrop {
            width: 600px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <%--    <div id="trRecordOwner" runat="server" class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="record-small-box">
                <a href="#" class="small-box-footer">Created By <i class="fa fa-user"></i></a>
                <div class="inner">
                    <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="record-small-box">
                <a href="#" class="small-box-footer">Last Modified By <i class="fa fa-user"></i></a>
                <div class="inner">
                    <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="record-small-box">
            </div>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-3">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="pull-left callout bg-theme">
                        <%--<strong>Item ID: </strong>--%>
                        <span>
                            <asp:Label Text="" runat="server" ID="lblItemID" Font-Size="Larger" /></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">

            <div class="record-small-box record-small-group-box createdBySingleSection" id="trRecordOwner" runat="server">

                <a href="#">Created</a>
                <span class="innerCreated">
                    <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                </span>
                <a href="#">Modified</a>
                <span class="innerCreated">
                    <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <asp:LinkButton ID="lkbDisassemble" runat="server" CssClass="btn btn-primary" Visible="false" UseSubmitBehavior="false">Disassemble Item</asp:LinkButton>
                <asp:LinkButton ID="btnManAssem" runat="server" CssClass="btn btn-primary" Visible="false" UseSubmitBehavior="false">Create Assembly / Work Order</asp:LinkButton>
                <asp:LinkButton ID="btnCloneItem" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false"><i class="fa fa-clone"></i>&nbsp;&nbsp;Clone Item</asp:LinkButton>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary" Style="margin-top: 5px;"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <telerik:RadTabStrip ID="radItemTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="true" SelectedIndex="0" MultiPageID="radMultiPage_ItemTab"
        ClientIDMode="Static" AutoPostBack="true">
        <Tabs>
            <telerik:RadTab Text="Asset Details" Value="AssetDetails" PageViewID="radPageView_AssetDetails">
            </telerik:RadTab>
            <telerik:RadTab Text="Item Details" Value="ItemDetails" PageViewID="radPageView_ItemDetails">
            </telerik:RadTab>
            <telerik:RadTab Text="Inventory" Visible="false" Value="Inventory" PageViewID="radPageView_Inventory">
            </telerik:RadTab>
            <telerik:RadTab Text="Vendor" Value="Vendor" PageViewID="radPageView_Vendor">
            </telerik:RadTab>
            <telerik:RadTab Text="Vendor Warehouses" Value="VendorWarehouses" PageViewID="radPageView_VendorWarehouses" Style="display: none">
            </telerik:RadTab>
            <telerik:RadTab Text="Related Items" Value="relatedItems" PageViewID="radPageView_relatedItems">
            </telerik:RadTab>
            <telerik:RadTab Text="Maintenance & Repair" Visible="false" Value="MaintenanceRepair" PageViewID="radPageView_MaintenanceRepair">
            </telerik:RadTab>
            <telerik:RadTab Text="Transactions" Value="ItemDependencies" PageViewID="radPageView_ItemDependencies">
            </telerik:RadTab>
            <telerik:RadTab Text="Ecommerce" Value="ItemEcommerce" PageViewID="radPageView_ItemEcommerce">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_ItemTab" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_AssetDetails" runat="server" visi>
            <div class="table-responsive">
                <br />
                <table width="70%" id="Table1" runat="server">
                    <tr>
                        <td class="normal1" align="right" nowrap>Asset Owner<font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <telerik:RadComboBox ID="radCmbOrganization" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True"
                                EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                        <td class="normal1" align="right" nowrap>
                            <table width="100%" id="Table3" runat="server">
                                <tr>
                                    <td align="right" class="normal1">Department
                                    </td>
                                    <td align="right" class="normal1">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="signup" AutoPostBack="true"
                                            Width="125">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="normal1" align="right">Item Type <font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlAssetItemType" runat="server" CssClass="signup" AutoPostBack="true"
                                Width="180">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                <asp:ListItem Value="P">Inventory Item</asp:ListItem>
                                <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                <asp:ListItem Value="S">Service</asp:ListItem>
                                <asp:ListItem Value="A">Asset</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" nowrap>Asset Name<font color="red">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAssetName" CssClass="signup" runat="server" Width="180">
                            </asp:TextBox>
                        </td>
                        <td class="normal1" align="right" nowrap>
                            <asp:HyperLink ID="hplAssetImage" runat="server" CssClass="hyperlink">Upload Image</asp:HyperLink>
                        </td>
                        <td align="right" class="normal1">Serial #
                        </td>
                        <td class="normal1">
                            <asp:TextBox ID="txtAssetSerial" CssClass="signup" runat="server" Width="180">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Model ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtAssetModelId" CssClass="signup" runat="server" Width="180">
                            </asp:TextBox>
                        </td>
                        <td class="normal1" align="left">Item Class :
                        <asp:DropDownList ID="ddlAssetIClass" runat="server" CssClass="signup">
                        </asp:DropDownList>
                        </td>
                        <td class="normal1" align="right" nowrap>Purchase Date
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calPurchaseDt" runat="server" ClientIDMode="AutoID" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" nowrap>Item Group
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAssetItemClass" Width="180" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td align="right" rowspan="2">
                            <table width="100%" id="Table2" runat="server" boarder="2">
                                <tr>
                                    <td align="right">
                                        <asp:RadioButton ID="optAppreciation" Text="Appreciation" Width="100" CssClass="signup"
                                            runat="server" GroupName="rad" />
                                    </td>
                                    <td align="right">
                                        <asp:TextBox ID="txtAppreciation" CssClass="signup" runat="server" Width="130">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:RadioButton ID="optDepreciation" Text="Depreciation" Width="100" CssClass="signup"
                                            runat="server" GroupName="rad" />
                                    </td>
                                    <td align="right">
                                        <asp:TextBox ID="txtDepreciation" CssClass="signup" runat="server" Width="130">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="normal1" align="right" nowrap>Warrantee Expiration
                        </td>
                        <td class="normal1">
                            <BizCalendar:Calendar ID="calExpiration" runat="server" ClientIDMode="AutoID" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Asset Account
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlAssetAcCOA" runat="server" Width="180" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="right" id="tdUPCLabel" runat="server">UPC
                        </td>
                        <td id="tdUPC" runat="server">
                            <asp:TextBox ID="txtBarCode" runat="server" CssClass="signup" Text="0">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Description
                        </td>
                        <td colspan="4">
                            <asp:TextBox ID="txtAssetDescription" Width="650" Height="65" runat="server" CssClass="signup"
                                TextMode="MultiLine">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="normal1">&nbsp;
                        </td>
                        <td colspan="4">
                            <asp:Button ID="btnAddAssetSerial" runat="server" CssClass="button" Text="Add Serial"
                                Visible="true" Width="175" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <asp:HiddenField ID="hfBaseUnit" runat="server" Value="" />
            <asp:HiddenField ID="hfPurchaseUnit" runat="server" Value="" />
            <asp:HiddenField ID="hfSaleUnit" runat="server" Value="" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ItemDetails" runat="server">
            <div class="table-responsive">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Primary</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="tblspace" width="100%">
                            <tr>
                                <td class="normal1" align="right" nowrap width="8%">Item Name : <font color="red">*</font>
                                </td>
                                <td class="normal1" width="20%" align="left">
                                    <asp:TextBox ID="txtItem" CssClass="signup" runat="server" Width="250px">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right" nowrap width="8%" id="tdUPCV2Label" runat="server">
                                    <asp:Label ID="lblUPC" runat="server" Text="UPC :" Visible="false"></asp:Label>
                                </td>
                                <td class="normal1" width="20%" align="left" id="tdUPCV2" runat="server">
                                    <asp:TextBox ID="txtUPC" runat="server" CssClass="signup" Width="180px" Text="0"
                                        Visible="false" MaxLength="20">
                                    </asp:TextBox>&nbsp;
                                        <asp:DropDownList runat="server" ID="ddlStandardProductIDType" CssClass="signup">
                                            <asp:ListItem Text="text" Selected="True" Value="0" />
                                        </asp:DropDownList>
                                    <asp:Label ID="lblTipStandardProductIDType" Text="[?]" CssClass="tip" runat="server"
                                        ToolTip="This field idendicates that value stored in UPC input will act as selected value in amazon marketplace" />
                                </td>
                                <td class="normal1" align="right" width="8%">Item Group:
                                </td>
                                <td class="normal1" width="20%" align="left">
                                    <asp:DropDownList ID="ddlIClassification" Width="188" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" width="8%" id="tdModelIdLabel" runat="server">ModelID:
                                </td>
                                <td class="normal1" align="left" width="20%" id="tdModelId" runat="server">
                                    <asp:TextBox ID="txtModelID" CssClass="signup" runat="server" Width="250px">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right" width="8%">
                                    <span id="tdSKU1" runat="server">SKU : </span>
                                </td>
                                <td class="normal1" align="left" width="20%">
                                    <asp:TextBox ID="txtSKU" runat="server" CssClass="signup" Width="180">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right" width="8%" id="tdASINLabel" runat="server">
                                    <span id="Span1" runat="server">ASIN : </span>
                                </td>
                                <td class="normal1" align="left" width="20%" id="tdASIN" runat="server">
                                    <asp:TextBox ID="txtASIN" runat="server" CssClass="signup" Width="180">
                                    </asp:TextBox>
                                </td>

                            </tr>
                            <tr>
                                <td class="normal1" align="right" id="td1" runat="server" width="8%">
                                    <asp:Label ID="lblManufacturerName" runat="server" Text="Manufacturer"></asp:Label>:
                                </td>
                                <td id="td2" runat="server" align="left" width="20%">
                                    <telerik:RadComboBox AccessKey="V" ID="radcmbManufacturer" Width="250px" DropDownWidth="600px"
                                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                        ClientIDMode="Static"
                                        ShowMoreResultsBox="true" EmptyMessage="Select a Company Or Type your own text"
                                        Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                    </telerik:RadComboBox>
                                </td>
                                <td class="normal1" align="right" width="8%"></td>
                                <td class="normal1" align="left" width="20%">
                                    <asp:CheckBox ID="chkLotNo" runat="server" onclick="SingleSelect(this);" Text="Lot No" />
                                    <asp:CheckBox ID="chkSerializedItem" runat="server" onclick="SingleSelect(this);" Text="Serialize" />
                                    &nbsp;
                                <asp:CheckBox ID="chkMatrix" runat="server" Text="Matrix" AutoPostBack="true" />
                                    &nbsp;<asp:CheckBox ID="chkArchieve" runat="server" Text="Archive" />
                                </td>
                                <td class="normal1" align="right" width="8%" style="white-space: nowrap">Item Type : <font color="red">*</font>
                                </td>
                                <td class="normal1" align="left" width="20%">
                                    <asp:DropDownList ID="ddlProduct" runat="server" CssClass="signup" AutoPostBack="true">
                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        <asp:ListItem Value="P">Inventory Item</asp:ListItem>
                                        <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                        <asp:ListItem Value="S">Service</asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;&nbsp;
                                        <asp:Label ID="lblInventoryItemType" runat="server" Text=""></asp:Label>
                                </td>

                            </tr>
                            <tr style="height: 28px;">
                                <td class="normal1" align="right" style="white-space: nowrap" id="tdChildMemberShipLabel" runat="server">Child Membership:</td>
                                <td class="normal1" align="left" id="tdChildMemberShip" runat="server">
                                    <table border="0" style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:HyperLink ID="hplChildMembership" runat="server" NavigateUrl="javascript:OpenChildMembership();">0</asp:HyperLink>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                                <td class="normal1" align="right">
                                    <asp:Label ID="lblMaxWOTitle" runat="server" Text="Max WO Qty:" Visible="false" /></td>
                                <td class="normal1" align="left">
                                    <asp:Label ID="lblMaxWO" runat="server" Visible="false"></asp:Label></td>
                                <td class="normal1" align="right" width="8%" id="tdItemGroupLabel" runat="server">Matrix Group
                                </td>
                                <td class="normal1" align="left" width="20%" id="tdItemGroup" runat="server">
                                    <asp:DropDownList ID="ddlItemGroup" Width="188" runat="server" CssClass="signup" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" width="8%" id="tdItemClassLabel" runat="server">Item Class :
                                </td>
                                <td class="normal1" align="left" width="20%" id="tdItemClass" runat="server">
                                    <asp:DropDownList ID="ddlItemClass" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                </td>

                                <td align="right">
                                    <asp:HyperLink ID="hplPriceLevel" runat="server" CssClass="hyperlink" Visible="false">Price Levels</asp:HyperLink>
                                    <br />
                                    <asp:Label ID="lblTimeContract" Text="[?]" CssClass="tip" runat="server" ToolTip="Selecting this means that each time this item is added to a sales order, it will add time to the customer�s time contract (if a contract has been created for that customer), with the number of units in the order equaling the number of hours contributed to the contract. Conversely, removing the item from an order will add time back to the time contract. Adding the item to a sales opportunity will not deduct or add hours, however if the opportunity is converted into an order, then at that point the time will be added to the contract.
" />
                                </td>
                                <td align="left">
                                    <asp:HyperLink ID="hplShoppingCart" Visible="false" runat="server" CssClass="hyperlink">Shopping Cart Extended Details
                                    </asp:HyperLink>
                                    <asp:CheckBox ID="chkArchieveItemSetting" runat="server" Text="Archive when in sales order" />
                                    <br />
                                    <asp:CheckBox ID="chkTimeContractFromSalesOrder" runat="server" Text="Create time contracts from sales orders" />

                                </td>

                                <td></td>
                                <td class="normal1">
                                    <asp:Panel ID="pnlAssetRental" runat="server" Visible="false">
                                        <asp:CheckBox ID="chkAsset" runat="server" Text="Asset" />
                                        <asp:CheckBox ID="chkRental" runat="server" Text="Rental" />
                                    </asp:Panel>
                                    <asp:CheckBox ID="chkExpense" runat="server" Text="Expense Item" Visible="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="center" colspan="1">
                                    <asp:Image ID="imgItem" runat="server" Width="100" AlternateText="Image Thumbali" />
                                    <br />
                                    <span style="text-align: center">
                                        <asp:HyperLink ID="hplImage" runat="server" CssClass="hyperlink">Image</asp:HyperLink>
                                    </span>
                                </td>
                                <td colspan="3" valign="top" align="left">
                                    <span class="normal1">Short Description :</span>
                                    <br />
                                    <asp:TextBox ID="txtDescription" Width="100%" Height="65" runat="server" CssClass="signup"
                                        TextMode="MultiLine">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right" id="tdListPrice" runat="server" width="8%">List Price (NI/I)
                                </td>
                                <td id="tdListPrice1" runat="server" align="left" width="20%">
                                    <asp:TextBox ID="txtLstPrice" CssClass="signup" runat="server" Width="180">
                                    </asp:TextBox>
                                    <a href="#" data-toggle="modal" data-target="#divCurrencies">Alt Currencies</a>
                                    <div id="divCurrencies" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header" style="padding: 0px 5px 0px 5px">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">List Prices for alternate currencies</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <asp:GridView ID="gvCurrencies" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true" CssClass="table table-bordered table-striped">
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Currency" DataField="vcCurrencyDesc" />
                                                            <asp:TemplateField HeaderText="List Price">
                                                                <ItemTemplate>
                                                                    <asp:TextBox runat="server" ID="txtCurrencyPrice" CssClass="form-control" Width="120" Text='<%# CCommon.GetDecimalFormat(Eval("monListPrice"))%>'></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnCurrencyID" runat="server" Value='<%# Eval("numCurrencyID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="modal-footer" style="padding: 0px 5px 0px 5px">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="SaveCurrencyPrice();">Save & Close</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="2" style="vertical-align: top">
                                    <asp:PlaceHolder ID="plhItemAttributes" runat="server"></asp:PlaceHolder>
                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
                <div class="box box-primary box-solid" id="divAssetDepreciation" runat="server" visible="false" style="display: none">
                    <div class="box-header">
                        <h3 class="box-title">Asset Depreciation</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <div class="form-inline">
                                        <label style="width: 105px; text-align: right;">Depreciate for</label>
                                        <input type="number" value="1" min="1" max="30" step="0.5" oninput="validateAssetDepreciationPeriod(event)" id="txtAssetDepreciationPeriod" runat="server" class="form-control" style="width: 70px;" />
                                        <b>Years</b>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-inline">
                                        <label style="width: 105px; text-align: right;">Residual Value $</label>
                                        <input type="number" id="txtAssetResidual" runat="server" class="form-control" style="width: 100px" value="0" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-inline">
                                        <label style="width: 105px; text-align: right;">Method</label>
                                        <asp:DropDownList ID="ddlAssetDepreciationMethod" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="Strait Line" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Declining Balance" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <table class="table table-bordered table-striped tblPrimary" id="tblAssetDepreciation">
                                    <tr>
                                        <th>Year</th>
                                        <th>Starting Value</th>
                                        <th>% Rate</th>
                                        <th>Depreciation</th>
                                        <th>Ending Value</th>
                                    </tr>
                                    <asp:Repeater ID="rptAssetDepreciation" runat="server">
                                        <ItemTemplate>
                                            <tr style='<%# If(CCommon.ToBool(Eval("bitCurrent")), "color:#000000", "color:#afabab") %>'>
                                                <td><%# Eval("intYear") %></td>
                                                <td class="tdUnit"><%# Session("Currency") & ReturnMoney(Eval("monStartingValue")) %></td>
                                                <td class="tdTotal" style="display: none"><%# Session("Currency") & ReturnMoney(Eval("monStartingValueTotal")) %></td>
                                                <td>
                                                    <span class="rate"><%# If(CCommon.ToBool(Eval("bitClosed")) Or CCommon.ToBool(Eval("bitLastYear")), Eval("numRate"), Eval("numRate") & "&nbsp;&nbsp;<a class='btn btn-xs btn-primary' href='#' title='Edit' onclick='EditRate(event);'><i class='fa fa-pencil-square-o'></i></a>")  %></span>
                                                    <input type="number" min="1" max="100" id='<%# Eval("ID") %>' class="txtRate" value='<%# Eval("numRate") %>' onkeypress="UpdateRate(event)" style="display: none" />
                                                </td>
                                                <td class="tdUnit"><%# Session("Currency") & ReturnMoney(Eval("monDepreciation")) %></td>
                                                <td class="tdTotal" style="display: none"><%# Session("Currency") & ReturnMoney(Eval("monDepreciationTotal")) %></td>
                                                <td class="tdUnit"><%# Session("Currency") & ReturnMoney(Eval("monEndingValue")) %></td>
                                                <td class="tdTotal" style="display: none"><%# Session("Currency") & ReturnMoney(Eval("monEndingValueTotal")) %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" class="text-right"><b>Accumulated Depreciation</b></td>
                                            <td>
                                                <asp:Label ID="lblAccumulatedDepreciation" runat="server"></asp:Label></td>
                                            <td>
                                                <input type="radio" id="rbAssetGridUnit" name="AssetGrid" checked="checked" />
                                                <b>Per-Unit</b>
                                                <input type="radio" id="rbAssetGridTotal" name="AssetGrid" />
                                                <b>Total</b>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Custom Fields</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="rblCustom" class="tblspace" width="100%" runat="server">
                        </table>
                        <table id="tblCustomEDIFields" class="tblspace" width="100%" runat="server">
                        </table>
                        <asp:PlaceHolder ID="plhCustomControls" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Accounting & Measures</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="tblspace" width="100%">
                            <tr>
                                <td class="normal1" align="right" width="8%">
                                    <asp:Label ID="lblAccountName" runat="server">Income :  </asp:Label>
                                    <font color="red">*</font>
                                </td>
                                <td class="normal1" width="20%" align="left">
                                    <asp:DropDownList ID="ddlIncomeAccount" runat="server" Width="180" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                                <td class="normal1" align="right" width="8%">
                                    <span id="trProducts2" runat="server">
                                        <asp:Label ID="lblAverageCost" runat="server" Text="Average Cost :" Visible="false">
                                        </asp:Label>
                                    </span>
                                </td>
                                <td class="normal1" width="20%" align="left">
                                    <asp:TextBox ID="txtAverageCost" runat="server" CssClass="signup" Width="100px" Visible="false" onkeypress="CheckNumber(1,event)">
                                            
                                    </asp:TextBox>
                                    <asp:CheckBox ID="chkVirtual" runat="server" Text="Virtual Inventory" />
                                    <asp:Label Visible="false" ID="lblAvgCostToolTip" Text="[?]" CssClass="tip" runat="server"
                                        ToolTip="You can not manually edit average cost since this item either has onHand qty in warehouse or it is being used in Sales or Purchase Order, Please use inventory adjustment to adjust Qty so average cost will automatially updated. When average cost is zero, please create purchase order of this item on closing PO it will update average cost." />
                                </td>
                                <td class="normal1" align="right" width="8%">
                                    <asp:Label ID="lblBaseUOM" runat="server" Text="Base UOM"></asp:Label>
                                    <asp:HyperLink ID="hplBaseUOM" runat="server" Visible="false"></asp:HyperLink>
                                </td>
                                <td width="20%" align="left">
                                    <asp:DropDownList ID="ddlBaseUOM" runat="server" Width="180" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td3" align="right" class="normal1" runat="server" width="8%">
                                    <span id="tdAsset1" runat="server">
                                        <asp:Label ID="lblAsset" runat="server" Text="Asset :"></asp:Label><font color="red"><asp:Label
                                                ID="lblAssetStar" runat="server" Text="*"></asp:Label></font> </span>
                                </td>
                                <td id="Td4" class="normal1" runat="server" align="left">
                                    <span id="tdddlAsset1" runat="server">
                                        <asp:DropDownList ID="ddlAssetAccount" runat="server" Width="180" CssClass="signup">
                                        </asp:DropDownList>
                                    </span>
                                </td>
                                <td width="8%"></td>
                                <td class="normal1" align="left" width="20%">
                                    <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                                        RepeatColumns="3">
                                    </asp:CheckBoxList>
                                    <asp:DropDownList ID="ddlCRVTextTypes" runat="server" Style="margin-top: 5px;">
                                        <asp:ListItem Text="-- Select CRV Tax --" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="normal1" align="right">Sale UOM
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddSalesUOM" runat="server" Width="180" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td5" class="normal1" align="right" runat="server">
                                    <span id="tdlblCogs1" runat="server">
                                        <asp:Label ID="lblCOGS" runat="server" Text="COGS :"></asp:Label><font color="red"><asp:Label
                                                ID="lblCOGSStar" runat="server" Text="*"></asp:Label></font> </span>
                                </td>
                                <td id="Td6" runat="server" align="left">
                                    <span id="tdddlCogs1" runat="server">
                                        <asp:DropDownList ID="ddlCOGS" runat="server" Width="180" CssClass="signup">
                                        </asp:DropDownList>
                                    </span>
                                </td>
                                <td class="normal1" align="right">
                                    <span style="display: none"><span id="trProducts1" runat="server">Labour Cost : </span></span>
                                </td>
                                <td align="left">
                                    <span style="display: none">
                                        <asp:TextBox ID="txtLabourCost" runat="server" CssClass="signup" Width="100px" onkeypress="CheckNumber(1,event)">
                                        </asp:TextBox></span>
                                </td>
                                <td class="normal1" align="right">Buy UOM
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddPurchaseUOM" runat="server" Width="180" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">
                                    <asp:Label ID="lblExpense" runat="server" Text="Expense :" Style="display: none"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlExpense" runat="server" Width="180" CssClass="signup" Style="display: none">
                                    </asp:DropDownList>
                                </td>
                                <td class="normal1" align="right"></td>
                                <td align="left"></td>
                                <td class="normal1" align="right"></td>
                                <td align="left"></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="box box-primary box-solid" id="divShipping" runat="server">
                    <div class="box-header">
                        <h3 class="box-title">Shipping</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="tblspace" width="100%">
                            <tr>
                                <td align="right" class="normal1" width="8%">
                                    <span id="tdWeight3" runat="server">Weight (lbs):</span>
                                </td>
                                <td class="normal1" width="20%" align="left">
                                    <span id="tdWeight2" runat="server">
                                        <asp:TextBox ID="txtWeight" runat="server" CssClass="signup">
                                        </asp:TextBox>
                                    </span>
                                </td>
                                <td class="normal1" width="8%" align="right">
                                    <span id="trDimen1" runat="server">Length (inches) :</span>
                                </td>
                                <td class="normal1" width="20%" align="left">
                                    <asp:TextBox ID="txtLength" runat="server" CssClass="signup">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right">&nbsp;<br />
                                    Shipping Class :</td>
                                <td width="20%" align="left">
                                    <asp:CheckBox ID="chkAllowDropShip" runat="server" Text="Drop-Ship" /><br />
                                    <asp:DropDownList ID="ddlShipClass" runat="server" CssClass="signup" Style="width: 160px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">
                                    <span id="trDimen2" runat="server">Height (inches) : </span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtHeight" runat="server" CssClass="signup">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right">
                                    <span id="trDimen3" runat="server">Width (inches) : </span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtWidth" runat="server" CssClass="signup">
                                    </asp:TextBox>
                                </td>
                                <td class="normal1" align="right" width="8%">
                                    <label runat="server" id="lblContainer">Container :</label></td>
                                <td width="20%">
                                    <asp:DropDownList ID="ddlContainer" runat="server"></asp:DropDownList>
                                    <asp:TextBox ID="txtNoOfItem" Text="0" Width="50px" Height="22px" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="box box-primary box-solid" id="tdAssembly" runat="server" visible="false">
                    <div class="box-header">
                        <h3 class="box-title">Kit sub-items / Assembly B.O.M. list</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row padbottom10">
                            <div class="col-xs-12">
                                <ul class="list-inline">
                                    <li>
                                        <asp:CheckBox ID="chkKit" Visible="false" runat="server" Text="Is Kit" /></li>
                                    <li>
                                        <asp:CheckBox ID="chkAssembly" Visible="false" runat="server" Text="Is Assembly" /></li>
                                    <li>
                                        <div id="divPriceBasedOn" runat="server" visible="false">
                                            <asp:Label ID="Label1" runat="server" CssClass="tip" Text="[?]" ToolTip="When creating a sale, the base value used (before discounts or markups are applied) will be the �sum total of child item�s list price� option means that instead of using the kit or assembly�s �List price� the sum-total of all the child item�s �List price� will be used. If �sum total of child item�s Average Cost� or �sum total of child item�s Primary Vendor Cost� is selected then cost value sum-total will be based on Average Cost or primary vendor cost."></asp:Label>
                                            <asp:CheckBox ID="chkAmtBasedonIndItems" Visible="false" runat="server" Text="Calculate price based on " />
                                        </div>
                                    </li>
                                    <li>
                                        <div id="divPriceBasedOnValues" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlKitAssemblyPriceBasedOn" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Sum total of child item's List Price" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Sum total of child item's Average Cost" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Sum total of child item's Primary Vendor Cost" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Kit/Assembly list price plus sum total of child item's list price" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </li>
                                    <li>
                                        <div id="divKitAssemblyPrice" style="display: none">
                                            <b>Calculated parent kit / assembly unit price: </b>
                                            <label id="lblCalculatedPrice" style="font-weight: normal"></label>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:DropDownList ID="ddlKitSelectionType" runat="server" Visible="false">
                                            <asp:ListItem Text="Multi-select" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Single-select" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:CheckBox runat="server" ID="chkSoWorkOrder" Text="Auto-check WO from new SO" Font-Bold="true" Visible="false" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row" id="divKitAssembltChildItems" runat="server">
                            <div class="col-xs-12">
                                <ul class="list-inline">
                                    <li>
                                        <a id="lkbNewItem" title="New Item" href="javascript:OpenPopUp('../Items/frmAddItem.aspx?isFromAssemblyKit=true')">
                                            <asp:Image ID="Image5" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                                        </a>
                                        <li><b>Add Item:</b></li>
                                    <li>
                                        <select id="ddlKitAsseblyChildItemSearchType" class="select2-choice">
                                            <option value="1" selected="selected">Contains</option>
                                            <option value="2">Starts-with</option>
                                            <option value="3">Ends-with</option>
                                        </select>
                                        <asp:HiddenField runat="server" ID="hdnKitAsseblyChildItemSearchType" />
                                    </li>
                                    <li>
                                        <input type="text" id="txtChildItem" /></li>
                                    <li>
                                        <asp:Button ID="btnAddChilItem" runat="server" CssClass="btn btn-success" Text="Add Item" Style="margin-left: 5px; margin-right: 5px;" />
                                        <asp:Button ID="btnRemove" runat="server" CssClass="btn btn-danger" Text="Remove Item" />
                                        <asp:HiddenField ID="hdnSelectedChildItem" runat="server" />
                                        <asp:HiddenField ID="hdnSelectedChildItemPrimaryVendor" runat="server" />
                                    </li>
                                    <li>
                                        <div class="form-inline" id="divChildItemPrimaryVendor" style="display: none">
                                            <div class="form-group">
                                                <label><span style="color: red">* </span>Primary vendor:</label>
                                                <telerik:RadComboBox AccessKey="C" Width="230" ID="radcmbChildPrimaryVendor" DropDownWidth="600px"
                                                    Skin="Default" runat="server" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                    ClientIDMode="Static">
                                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                </telerik:RadComboBox>
                                            </div>
                                            <div class="form-group">
                                                <label>Part #:</label>
                                                <asp:TextBox ID="txtVendorPart" runat="server" Width="80" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label>Cost:</label>
                                                <asp:TextBox ID="txtVendorCost" runat="server" Width="80" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label>Min Order Qty:</label>
                                                <asp:TextBox ID="txtVendorMinOrderQuantity" runat="server" Width="80" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-inline">
                                            <label>Warehouse</label>
                                            <asp:DropDownList ID="ddlGridWareHouse" Width="200px" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                                            <label id="divMaxBuildQty" runat="server">
                                                Buildable Qty:
                                                <asp:Label ID="lblMaxBuildQty" runat="server" Text="0"></asp:Label></label>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="Label4" runat="server" CssClass="tip" Text="[?]" ToolTip="If you use �Kits with Child Kits� to setup dynamic configurations, where upstream item selections populate downstream selections, then it�s possible to occasionally end up with an an orphaned parent where no items can be selected based on the variables. Selecting this check box will enable you to handle these scenarios by displaying a message requesting that the user try a different configuration path. Dynamic settings are configured from: Admin | Add/Edit Drop-down lists | Drop-Down Field Relationships | Add New Relationship Module (Kits with Child Kits)."></asp:Label>
                                        <asp:CheckBox runat="server" ID="chkPreventOrphanedParents" Text="Prevent Orphaned Parents" Font-Bold="true" Visible="false" />
                                    </li>
                                    <li>
                                        <div class="form-inline" id="divBuildProcess" runat="server" visible="false">
                                            <label>
                                                Start this Build Process<a title="It�s assumed that the completion of this build process will result in the building of 1 Assembly unit from its BOM" class="tip" href="javascript:void(0)">[?]</a>
                                                <asp:DropDownList ID="ddlBusinessProcess" Width="200px" CssClass="form-control" runat="server"></asp:DropDownList>
                                                When Work Order Created</label><br />
                                        </div>
                                    </li>
                                    <li>
                                        <a id="btnNewKitItem" runat="server" class="btn btn-primary" visible="false" href="javascript:void(0)" onclick="OpenConfigurator();">Configure & Add New Item</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12">
                                <telerik:RadTreeList ID="rtlChildItems" runat="server" ParentDataKeyNames="numParentID" DataKeyNames="ID" AutoGenerateColumns="false" AllowSorting="true">
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />

                                    <Columns>
                                        <telerik:TreeListBoundColumn DataField="numItemKitID" UniqueName="numItemKitID" Visible="false"></telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numItemCode" UniqueName="numItemCode" Visible="false"></telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numItemDetailID" UniqueName="numItemDetailID" Visible="false"></telerik:TreeListBoundColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Child Item (SKU)" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="300">
                                            <ItemTemplate>
                                                <ul class="list-inline">
                                                    <li><a href="javascript:OpenItem(<%# DataBinder.Eval(Container, "DataItem.numItemCode")%>)">
                                                        <img src="../images/tag.png" align="BASELINE" style="float: none;"></a></li>
                                                    <li><%# DataBinder.Eval(Container, "DataItem.vcItemName")%>
                                                </ul>
                                                <br />
                                                <asp:CheckBox runat="server" ID="chkUseInNewItemConfiguration" Font-Italic="true" Text="Use selection for adding new item(s) from configurator" Visible="false" />
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListBoundColumn DataField="charItemType" HeaderText="Type" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Units Required" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuantity" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.numConQty") %>' Visible="false"></asp:Label>
                                                <asp:TextBox ID="txtQuantity" runat="server" Width="40px" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.numConQty") %>'
                                                    onchange='CheckConversion(this)'>
                                                </asp:TextBox>
                                                <br />
                                                <asp:CheckBox runat="server" ID="chkOrderEditable" Text="Order Editable" Font-Bold="true" Visible="false" />
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="UOM" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="110">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlItemUOM" runat="server" CssClass="signup" Width="90" onchange='CheckConversion(this)'>
                                                </asp:DropDownList>
                                                <asp:Label ID="lblItemUOM" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.vcIDUnitName") %>'
                                                    Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Sequence" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80" AllowSorting="true" SortExpression="sintOrder">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSequence" runat="server" Width="30px" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.sintOrder") %>'>
                                                </asp:TextBox>
                                                <asp:Label ID="lblSequence" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.sintOrder") %>'
                                                    Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListBoundColumn DataField="numOnHand" HeaderText="On-Hand" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListBoundColumn DataField="numOnOrder" HeaderText="On-Order" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Item Description" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="200">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtIDescription" Width="200" Height="30" Visible="false" runat="server" CssClass="signup"
                                                    Text='<%# DataBinder.Eval(Container,"DataItem.txtItemDesc") %>' TextMode="MultiLine">
                                                </asp:TextBox>
                                                <asp:Label ID="lblIDescription" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.txtItemDesc") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListTemplateColumn HeaderText="Settings" ItemStyle-Wrap="false" HeaderStyle-Width="300">
                                            <ItemTemplate>
                                                <ul class="list-inline">
                                                    <li>
                                                        <asp:DropDownList runat="server" ID="ddlView" Visible="false" Width="120">
                                                            <asp:ListItem Text="Vertical value list" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Horizontal value list" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </li>
                                                    <li>
                                                        <telerik:RadComboBox runat="server" ID="rcbFields" Visible="false" CheckBoxes="true" Width="150" EmptyMessage="Fields to display"></telerik:RadComboBox>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListTemplateColumn UniqueName="SKU" ItemStyle-Wrap="false" HeaderStyle-Width="100">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label18" Text="[?]" CssClass="tip" runat="server" title="If the child kit check box is selected, then when selecting items from
the configurator while building a new order, the value from the item
selected during configuration (This field source is selected from the
�Settings� column here within child kit grid e.g. Item Name, SKU,
etc..) will replace the static SKU name used in the parent Kit when
it�s added to an order. For example, if the configuration is made up
of 2 items and from the �Settings� column both were set to SKU and
the 1st items SKU value is �A� and the 2nd item�s SKU value is �123�,
the parent kit�s SKU value will be replaced with a new one called
�A-123� when the order is created." />
                                                <b>SKU</b>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSKU" runat="server" Visible="false" />
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                        <telerik:TreeListBoundColumn DataField="ItemType" Visible="false" UniqueName="ItemType">
                                        </telerik:TreeListBoundColumn>
                                        <telerik:TreeListTemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                            </ItemTemplate>
                                        </telerik:TreeListTemplateColumn>
                                    </Columns>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="true" ScrollHeight="500px" />
                                    </ClientSettings>
                                </telerik:RadTreeList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Inventory" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>External Location:</label>
                                <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control" AutoPostBack="true" Width="150" TabIndex="1">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hfWareHouseItemID" runat="server" />
                                <label>Internal Location&nbsp;&nbsp;<asp:Label ID="lblToolTipInternalLocation" runat="server" CssClass="tip" ToolTip="You can select an internal location when adding a new warehouse to an item, but afterwards you�ll need to use �Transfer Inventory� to change or add a new internal location">[?]</asp:Label></label>
                                <telerik:RadComboBox runat="server" ID="radcmbWarehouseLocation" Width="150" Skin="Default" AllowCustomText="True" ShowMoreResultsBox="true"
                                    EnableLoadOnDemand="True" OnItemsRequested="radcmbWarehouseLocation_ItemsRequested" ClientIDMode="Static" DataTextField="vcLocation" DataValueField="numWLocationID">
                                </telerik:RadComboBox>
                            </div>
                            <asp:Button ID="btnAddInternalLoc" CssClass="btn btn-sm btn-primary" Text="Add" runat="server"></asp:Button>
                            <div class="form-group" id="divReorderQty" runat="server">
                                &nbsp;&nbsp;
                                    <asp:Label runat="server" ID="lblReorderTipAssembly" CssClass="tip" ToolTip="Assembly Re-Ordering -  When on-hand reaches the re-order point, a trigger is created if the �Auto-create POs when Inventory is low:� rule is turned on from Administration | Global Settings | Order Mgt. Unlike regular items or even kits, the trigger doesn�t generate purchase prompts or orders used to replenish inventory. Instead, it creates work orders (WOs), in the quanity set within the �Re-Order Qty� field. If the qty set for the WOs created can�t be �completed� due to BOMs lacking sufficient qty, then the before mentioned auto-create rule will either create PO records for primary vendors, or if the rule is set to the �PO Prompt� option, you�ll be able to manually create POs from the WO list by filtering for WOs that contain a back-order quantity." Visible="false" Text="[?]"></asp:Label>
                                <asp:Label runat="server" ID="lblReorderTipOtherItems" CssClass="tip" ToolTip="Item Re-Ordering � Depending on how your administrator has configured the �Auto-create POs when Inventory is low� rule within Administration | Global Settings | Order Mgt.  BizAutomation will do the following: After a sales order is created: If on-hand reaches the re-order point for the item, either a Purchase Order for the primary vendor will be created, or a window will prompt the user to create a Purchase Oder. In either case, the quantity used for the item will be taken from the �Re-Order Qty�." Visible="false" Text="[?]"></asp:Label>
                                <label>Re-Order Qty:</label>
                                <asp:TextBox ID="txtReorderQty" runat="server" Width="80" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group" id="divReorderPoint" runat="server">
                                <asp:Label runat="server" Text="[?]" ID="lblReorderTipAssemblyHeader" CssClass="tip" ToolTip="Assembly Re-Ordering -  When on-hand reaches the re-order point, a trigger is created if the �Auto-create POs when Inventory is low:� rule is turned on from Administration | Global Settings | Order Mgt. Unlike regular items or even kits, the trigger doesn�t generate purchase prompts or orders used to replenish inventory. Instead, it creates work orders (WOs), in the quanity set within the �Re-Order Qty� field. If the qty set for the WOs created can�t be �completed� due to BOMs lacking sufficient qty, then the before mentioned auto-create rule will either create PO records for primary vendors, or if the rule is set to the �PO Prompt� option, you�ll be able to manually create POs from the WO list by filtering for WOs that contain a back-order quantity." Visible="false"></asp:Label>
                                <asp:Label runat="server" Text="[?]" ID="lblReorderTipOtherItemsHeader" CssClass="tip" ToolTip="Item Re-Ordering � Depending on how your administrator has configured the �Auto-create POs when Inventory is low� rule within Administration | Global Settings | Order Mgt.  BizAutomation will do the following: After a sales order is created: If on-hand reaches the re-order point for the item, either a Purchase Order for the primary vendor will be created, or a window will prompt the user to create a Purchase Oder. In either case, the quantity used for the item will be taken from the �Re-Order Qty�." Visible="false"></asp:Label>
                                <label>Re-Order Point:</label>
                                <asp:TextBox ID="txtReOrderPoint" runat="server" Width="80" CssClass="form-control" onkeypress="CheckNumber(2,event);"></asp:TextBox>
                                &nbsp;&nbsp;
                                <asp:Label runat="server" Text="[?]" ID="Label2" CssClass="tip" ToolTip="Formula (Global setting | General) rules to automate re-order qty and re-order point value based on unit sales based on last 30-90 days, or historical sales from same period 1 year prior and lead time (days) x Primary Vendor lead time set from the primary vendor�s ship-to address link."></asp:Label>
                                <asp:CheckBox runat="server" Text="Automate Re-Order Point & Qty" ID="chkAutomateReorderPoint" />
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="form-inline">
                            <div class="form-group">
                                <asp:Label ID="lblAdjustmentSuccess" Style="color: #00a65a" runat="server" Visible="false"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label><span style="color: red">*</span> Adjustment Date:</label>
                                <telerik:RadDatePicker runat="server" ID="calAdjustmentDate" Width="100" TabIndex="102" />
                            </div>
                            <asp:Button ID="btnSaveInventory" Text="Save Inventory" runat="server" CssClass="btn btn-primary" OnClientClick="return Save();" TabIndex="104" Style="padding-left: 5px;" />
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnItemGroup" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <telerik:RadGrid ID="gvInventory" runat="server" Width="100%" AutoGenerateColumns="false" ShowFooter="true" EnableEmbeddedSkins="false">
                            <MasterTableView DataKeyNames="numWareHouseItemID,OnHand" AllowAutomaticUpdates="true" AllowAutomaticInserts="true" EditMode="InPlace" HierarchyLoadMode="Client" DataMember="WareHouse" CssClass="table table-bordered table-striped">
                                <Columns>
                                    <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete_WareHouse" CssClass="btn btn-xs btn-danger" OnClientClick="return DeleteWarehouse();" ToolTip="Delete"><i class="fa fa-trash"></i>
                                            </asp:LinkButton>
                                            <asp:ImageButton ID="ibtnManageSerial" runat="server" OnClientClick="return " ToolTip="Serial/Lot" ImageUrl="~/images/Barcode.png" Height="20" Style="vertical-align: middle" />
                                            <asp:ImageButton ID="ibtnManageMatrix" runat="server" OnClientClick="return " ToolTip="Matrix" ImageUrl="~/images/ig_treePlus.gif" Height="5" Style="vertical-align: middle" />
                                            <asp:LinkButton ID="ibtnTransferStok" runat="server" ToolTip="Inventory Transfer" Visible="false" OnClientClick='<%# "return OpenMassStockTransfer(" & Eval("numItemID") & "," & Eval("numWareHouseItemID") & ");"%>'><img id="img1" src='../images/StockTransfer.png' border='0'/>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="External Location" DataField="vcExternalLocation" />
                                    <telerik:GridTemplateColumn HeaderText="Internal Location" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:DataList ID="dlInternalLocation" runat="server" RepeatColumns="2" RepeatLayout="Table" OnItemCommand="dlInternalLocation_ItemCommand">
                                                <ItemTemplate>
                                                    <ul class="list-inline">
                                                        <li style="padding-left: 0px; padding-right: 0px;">
                                                            <asp:Label ID="lblInternalLocation" runat="server" Text='<%# Eval("Location")%>'></asp:Label>
                                                            <asp:HiddenField ID="hdnWarehouseItemID" runat="server" Value='<%# Eval("WarehouseItemID")%>' />
                                                            <asp:HiddenField ID="hdnLocationID" runat="server" Value='<%# Eval("WarehouseLocationID")%>' />
                                                            <asp:HiddenField ID="hdnWarehouse" runat="server" Value='<%# Eval("Warehouse")%>' />
                                                            <asp:HiddenField ID="hdnLocation" runat="server" Value='<%# Eval("Location")%>' />
                                                            <asp:HiddenField ID="hdnLocationOnHand" runat="server" Value='<%# Eval("OnHand")%>' />
                                                        </li>
                                                        <li style="padding-left: 0px; padding-right: 0px;">
                                                            <asp:TextBox ID="txtInternalLocationQty" CssClass="form-control" Width="80" runat="server" Text='<%# String.Format("{0:#,##0.#####}", Eval("OnHand"))%>'></asp:TextBox></li>
                                                        <li style="padding-left: 0px; padding-right: 0px;">
                                                            <asp:ImageButton ID="ibtnManageSerial" runat="server" ToolTip="Serial/Lot" ImageUrl="~/images/Barcode.png" Height="20" Style="vertical-align: middle" /></li>
                                                        <li style="padding-left: 0px; padding-right: 0px;">
                                                            <asp:LinkButton ID="imgDelete" runat="server" CommandArgument='<%# Eval("WarehouseItemID")%>' CommandName="DeleteInternalLocation" CssClass="btn btn-xs btn-danger" OnClientClick="return DeleteWarehouseLocation();" ToolTip="Delete"><i class="fa fa-trash"></i></asp:LinkButton></li>
                                                        <li style="padding-left: 0px; padding-right: 0px;"><a href='<%# "javascript:OpenInventoryReport(" & Eval("WarehouseItemID") & ");"%>' style="vertical-align: middle">
                                                            <img src="../images/Audit.png" style="height: 20px" /></a>
                                                    </ul>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Buildable Qty" DataField="numBuildableQty" UniqueName="BuildableQty" Visible="false" />
                                    <telerik:GridTemplateColumn UniqueName="Availability" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAvailabilityHeader" runat="server" Text="On-Hand"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAvailability" runat="server" Text='<%# String.Format("{0:#,##0.#####}", Convert.ToDouble(Eval("OnHand")) + Convert.ToDouble(Eval("Allocation")))%>'></asp:Label>
                                            <asp:Label Text='<%# Eval("numAssetChartAcntId") %>' runat="server" ID="lblAssetChartAcntID" Style="display: none;" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="OnHand" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOnHandHeader" runat="server" Text="Availability"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOnHand" runat="server" Text='<%# Eval("OnHand", "{0:#,##0.#####}")%>'>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="OnOrder" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOnOrderHeader" runat="server" Text="On-Order"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOnOrder" runat="server" Text='<%# Eval("OnOrder", "{0:#,##0.#####}")%>'>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Requisitions" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblRequisitionsHeader" runat="server" Text="Requisitions"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequisitions" runat="server" Text='<%# Eval("Requisitions", "{0:#,##0.#####}")%>'>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="OnAllocation" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAllocationHeader" runat="server" Text="Allocation"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAllocation" runat="server" Text='<%# Eval("Allocation", "{0:#,##0.#####}")%>'>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="BackOrder" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblBackOrderHeader" runat="server" Text="Back-Order"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBackOrder" runat="server" Text='<%# Eval("BackOrder", "{0:#,##0.#####}")%>'>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="List Price(I)" DataField="Price" UniqueName="Price" Visible="false" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:#,##0.00}" />
                                    <telerik:GridBoundColumn HeaderText="SKU (M)" Visible="false" DataField="SKU" UniqueName="SKU" />
                                    <telerik:GridBoundColumn HeaderText="UPC (M)" Visible="false" DataField="BarCode" UniqueName="BarCode" />
                                    <telerik:GridBoundColumn HeaderText="Attributes" Visible="false" DataField="vcAttribute" UniqueName="Attribute" />
                                    <%-- Richa Start --%>
                                    <telerik:GridTemplateColumn HeaderText="Counted<font color=#ff0000>*</font>" UniqueName="Counted" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="125px">
                                        <ItemTemplate>
                                            <asp:TextBox Text='<%# Eval("TotalOnHand") %>' ID="txtCounted" CssClass="form-control" runat="server" Width="120px" AutoComplete="OFF" Style="text-align: right" onkeypress="CheckNumber(2,event);"></asp:TextBox>
                                            <asp:Label Text='<%# Eval("TotalOnHand")%>' runat="server" ID="lblOnHandQty" Style="display: none;" />
                                            <asp:Label Text='<%# Eval("monAverageCost") %>' runat="server" ID="lblInventoryAverageCost" Style="display: none;" />
                                            <asp:Label Text='<%# Eval("numWarehouseItemID") %>' runat="server" ID="lblWarehouseItemID" Style="display: none;" />
                                            <asp:Label Text='<%# Eval("numWarehouseID") %>' runat="server" ID="LblnumWarehouseID" Style="display: none;" />
                                            <asp:Label Text='<%# Eval("numWLocationID") %>' runat="server" ID="LblnumWLocationID" Style="display: none;" />

                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Qty to Adjust" UniqueName="Qty to Adjust" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="125px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAdjQty" CssClass="form-control" runat="server" Width="120px" AutoComplete="OFF" Text="" Style="text-align: right"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <strong>Total:</strong>
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Current value ($)" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:Label Text='<%# String.Format("{0:###00.00}",Eval("monCurrentValue")) %>' runat="server" ID="lblCurrentInventoryValue" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label Text='' runat="server" ID="lblCurrentInventoryValueTotal" Font-Bold="true" />
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Adjusted value ($)" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" HeaderStyle-Width="170px">
                                        <ItemTemplate>
                                            <asp:Label Text="" runat="server" ID="lblNewInventoryValue" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label Text='' runat="server" ID="lblNewInventoryValueTotal" Font-Bold="true" />
                                        </FooterTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%-- Richa End --%>
                                    <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <%--<asp:ImageButton ID="imgbtnManageInventory" runat="server" OnClientClick="return " ToolTip="Manage Inventory" ImageUrl="~/images/ManageInventory.png" Height="20" Style="vertical-align: middle" />--%>
                                            <asp:ImageButton ID="ibtnAudit" runat="server" OnClientClick="return " ToolTip="Audit" ImageUrl="~/images/Audit.png" Height="20" Style="vertical-align: middle" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings AllowExpandCollapse="true" />
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Vendor" runat="server">
            <div class="table-responsive">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>Vendor:</label>
                                <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    ClientIDMode="Static"
                                    ShowMoreResultsBox="true"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                            </div>
                            <asp:Button ID="btnAdd" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:DataGrid ID="dgVendor" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" ClientIDMode="AutoID" UseAccessibleHeader="true">
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="numVendorID"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Primary">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="radPreffered" runat="server" GroupName="rad"></asp:RadioButton>
                                            <asp:Label ID="lblvendorID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numVendorID") %>'
                                                Style="display: none">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Vendor" HeaderText="Vendor Name"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Part #">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPartNo" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container.DataItem, "vcPartNo") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            Cost (Qty of 1 Base UOM)
                                        <asp:Label ID="lblCostUOM" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="form-inline">
                                                <asp:TextBox ID="txtCost" runat="server" CssClass="form-control">
                                                </asp:TextBox>&nbsp; <a href="javascript:void(0);" onclick="OpenVendorCostTable('<%# Eval("numVendorTCode") %>','<%# Eval("numItemCode") %>','<%# Eval("numVendorID") %>');">Cost Table</a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Min Order Qty (Qty of 1 Base UOM)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMinQty" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Notes">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNotes" CssClass="form-control" NumberFormat-DecimalDigits="0" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcNotes")%>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnIsPrimaryVendor" runat="server" />
                                            <asp:HiddenField ID="hdnUsedInAssembly" runat="server" />
                                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_VendorWarehouses" runat="server">
            <div class="table-responsive">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvVendorWareHouse" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" Width="100%" UseAccessibleHeader="true">
                                <Columns>
                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor Name"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Warehouse Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a href="#" onclick="OpenVendorsWareHouse('<%# Eval("numVendorID") %>','<%# Eval("numAddressID") %>','<%# Eval("numItemCode") %>');">
                                                <%# Eval("vcAddressName")%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Address" DataField="vcFullAddress" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Items currently in transit" DataField="VendorTransitCount"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_relatedItems" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="table-responsive">
                        <iframe id="IframeRelatedItems" runat="server" frameborder="0" width="98%" height="600px" clientidmode="Static"></iframe>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_MaintenanceRepair" runat="server">
            <div class="table-responsive">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvMaintenanceRepair" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a href="#" onclick="OpenActionItem('<%# Eval("numCommId") %>');">
                                                <%# Eval("Task")%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="dtStartTime" HeaderText="Due Date" HtmlEncode="false"></asp:BoundField>
                                    <asp:BoundField DataField="dtEventClosedDate" HeaderText="Date Completed" HtmlEncode="false"></asp:BoundField>
                                    <asp:BoundField DataField="textdetails" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="vcActivity" HeaderText="Activity"></asp:BoundField>
                                    <asp:BoundField DataField="monMRItemAmount" HeaderText="Amount" DataFormatString="{0:#,###.00}" HtmlEncode="false"></asp:BoundField>
                                    <asp:BoundField DataField="vcVendor" HeaderText="Vendor"></asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No Maintenance & Repair found for this item.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ItemDependencies" runat="server">
            <div>
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <asp:HyperLink ID="hrfGL" runat="server" Text="" Style="cursor: pointer"><img style="vertical-align:sub" src="../images/open_new_window_notify.gif" > GL Transactions</asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
            <telerik:RadTabStrip ID="radTabItemDependencies" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                Skin="Default" ClickSelectedTab="True" MultiPageID="radMultiPage_ItemDependencies" SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="Orders/Returns" Value="ItemOrders" PageViewID="radPageView_ItemOrders">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage_ItemDependencies" runat="server" SelectedIndex="0" CssClass="pageView">
                <telerik:RadPageView ID="radPageView_ItemOrders" runat="server">
                    <div>
                        <div class="row">
                            <%--<div class="col-xs-12">--%>
                            <div class="col-md-3">
                                <label>Order Type &nbsp;</label>
                                <%--<asp:DropDownList runat="server" ID="ddlIDOppType" AutoPostBack="true">
                                    <asp:ListItem Text="--All--" Value="0" />
                                    <asp:ListItem Text="Sales Orders" Value="1" />
                                    <asp:ListItem Text="Open Sales Orders" Value="7" />
                                    <asp:ListItem Text="Closed Sales Orders" Value="8" />
                                    <asp:ListItem Text="Purchase Orders" Value="2" />
                                    <asp:ListItem Text="Open Purchase Orders" Value="9" />
                                    <asp:ListItem Text="Closed Purchase Orders" Value="10" />
                                    <asp:ListItem Text="Sales Opportunities" Value="3" />
                                    <asp:ListItem Text="Purchase Opportunities" Value="4" />
                                    <asp:ListItem Text="Sales Returns" Value="5" />
                                    <asp:ListItem Text="Purchase Returns" Value="6" />
                                </asp:DropDownList>--%>
                                <telerik:RadComboBox ID="radIDOppType" runat="server" AutoPostBack="true" OnClientLoad="onRadComboBoxLoad">
                                    <Items>
                                        <telerik:RadComboBoxItem Selected="true" Text="--All--" Value="0" />
                                        <telerik:RadComboBoxItem Text="Sales Orders" Value="1" />
                                        <telerik:RadComboBoxItem Text="Open Sales Orders" Value="7" />
                                        <telerik:RadComboBoxItem Text="Closed Sales Orders" Value="8" />
                                        <telerik:RadComboBoxItem Text="Purchase Orders" Value="2" />
                                        <telerik:RadComboBoxItem Text="Open Purchase Orders" Value="9" />
                                        <telerik:RadComboBoxItem Text="Closed Purchase Orders" Value="10" />
                                        <telerik:RadComboBoxItem Text="Sales Opportunities" Value="3" />
                                        <telerik:RadComboBoxItem Text="Purchase Opportunities" Value="4" />
                                        <telerik:RadComboBoxItem Text="Sales Returns" Value="5" />
                                        <telerik:RadComboBoxItem Text="Purchase Returns" Value="6" />
                                        <telerik:RadComboBoxItem Text="Stock Transfers" Value="11" />
                                    </Items>
                                </telerik:RadComboBox>
                            </div>
                            <div class="col-md-3">
                                <div id="divParentKitItems" runat="server">
                                    <b>This item is part of kit item<b><br />
                                        <asp:HyperLink ID="hplItemDependencies" runat="server" CssClass="hyperlink">View list of Parent Kit Items</asp:HyperLink>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Warehouse/Location &nbsp;</label>
                                <telerik:RadComboBox ID="radcmbLocation" runat="server" AutoPostBack="true" OnClientLoad="onRadComboBoxLoad" />
                            </div>
                            <div class="pull-right" style="padding-right: 30px;" runat="server" id="divInventory">
                                <b>On-Hand</b>
                                <asp:Label runat="server" ID="lblOnHandText"></asp:Label>&nbsp;&nbsp;<b>Allocation</b>
                                <asp:Label runat="server" ID="lblAllocationText"></asp:Label><asp:Label runat="server" ID="lblTotalOrdered"></asp:Label><br />
                                <b>On-Order</b>
                                <asp:Label runat="server" ID="lblOrder"></asp:Label>&nbsp;&nbsp;<b>On-BackOrder</b>
                                <asp:Label runat="server" ID="lblBackOrderText"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvItemDependencies" runat="server" ShowFooter="true" AllowPaging="true" PagerStyle-Font-Italic="true"
                                        OnPageIndexChanging="OnPageIndexChanging" PageSize="15" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Right"
                                        CssClass="table table-bordered table-striped tblPrimary" AllowSorting="true" OnSorting="gvItemDependencies_Sorting"
                                        PagerSettings-FirstPageText="First" PagerSettings-LastPageText="Last" PagerSettings-Mode="NextPreviousFirstLast"
                                        PagerStyle-CssClass="gridViewPager" PagerStyle-Font-Underline="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Order ID" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="false">
                                                <ItemTemplate>
                                                    <a href="#" style='display: <%# IIf(Convert.ToString(Eval("OppType")).ToLower() = "stock transfer", "none", "")%>' oncontextmenu="OpenOpp('<%# Eval("numoppid") %>','<%# Eval("tintOppType") %>', '<%# Eval("numReturnHeaderID") %>');" onclick="OpenOpp('<%# Eval("numoppid") %>','<%# Eval("tintOppType") %>', '<%# Eval("numReturnHeaderID") %>');">
                                                        <%# Eval("vcPoppName")%>
                                                    </a>
                                                    <span style='display: <%# IIf(Convert.ToString(Eval("OppType")).ToLower() = "stock transfer", "", "none")%>'><%# Eval("vcPoppName")%></span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="OppType" HeaderText="Type" ItemStyle-Font-Bold="false"></asp:BoundField>
                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" HtmlEncode="false" ItemStyle-Font-Bold="false"></asp:BoundField>
                                            <asp:BoundField DataField="AccountClosingDate" HeaderText="Due Date" HtmlEncode="false" Visible="false" ItemStyle-Font-Bold="false"></asp:BoundField>
                                            <asp:BoundField DataField="OppStatus" HeaderText="Status" ItemStyle-Font-Bold="false"></asp:BoundField>
                                            <asp:BoundField DataField="vcCompanyname" HeaderText="Organization" ItemStyle-Font-Bold="false"></asp:BoundField>
                                            <asp:BoundField DataField="vcItemName" HeaderText="Kit Item" ItemStyle-Font-Bold="false"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Ordered" SortExpression="numOnOrder" ItemStyle-Font-Bold="false">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:#,##0.#####}", Convert.ToDouble(Eval("numOnOrder")))%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Allocation/Order BO/On-Order" SortExpression="numAllocation" ItemStyle-Font-Bold="false">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:#,##0.#####}", Convert.ToDouble(Eval("numAllocation")))%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Released/Received" SortExpression="numQtyReleasedReceived" ItemStyle-Font-Bold="false">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:#,##0.#####}", Convert.ToDouble(Eval("numQtyReleasedReceived")))%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="vcWareHouse" HeaderText="Warehouse/Location" ItemStyle-Font-Bold="false"></asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No records found.
                                        </EmptyDataTemplate>
                                        <FooterStyle CssClass="gridFooter" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" Position="Top" LastPageText="Last" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ItemEcommerce" runat="server">
            <div class="table-responsive">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Primary</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <asp:CheckBox ID="chkFreeShipping" runat="server" Text="Free Shipping" />
                        <asp:CheckBox ID="chkAllowBackOrder" runat="server" Text="Allow Back Order in e-Commerce" />
                    </div>
                </div>
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Web API</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>Marketplace Sync:</label>
                                <telerik:RadComboBox ID="radWebAPIList" runat="server" CheckBoxes="true">
                                </telerik:RadComboBox>
                            </div>
                            <asp:HyperLink ID="hplWebAPI" runat="server" CssClass="hyperlink">Web API Item Extended Details</asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Search Engine Optimization</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>Page Title (Optional)&nbsp;&nbsp;<asp:Label ID="Label3" Text="[?]" CssClass="tip" runat="server" ToolTip="<strong>Page Title</strong><p>Specify title you want to use in the page</p>" /></label>
                                    <asp:TextBox ID="txtPageTitle" class="form-control" runat="server">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>Meta Keywords (Optional)&nbsp;&nbsp;<asp:Label ID="lblMetaKeywords" Text="[?]" CssClass="tip" runat="server" ToolTip="<strong>Meta Keywords</strong><p>Specify unique meta keywords, or leave blank to use default site wide keywords as defined in the <em>Settings</em> page.</p>" /></label>
                                    <asp:TextBox ID="txtMetaKeywords" class="form-control" runat="server">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>Meta Description (Optional)&nbsp;&nbsp;<asp:Label ID="lblMetaDescription" Text="[?]" CssClass="tip" runat="server" ToolTip="<strong>Meta Description</strong><p>Specify unique meta description, or leave blank to use default site wide description as defined in the <em>Settings</em> page.</p>" /></label>
                                    <asp:TextBox ID="txtMetaDescription" class="form-control" runat="server">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Categories / Long Description
                                    &nbsp;
                                    <asp:Label ID="lblLongdescription" Text="[?]" CssClass="tip" runat="server" ToolTip=" This field is used by Biz. e-Commerce website.It will not be shown in internal Sales order." /></h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <label>Category Profile:</label>
                                            <asp:DropDownList ID="ddlCategoryProfile" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <asp:Label ID="lblProfileSites" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div style="border: 1px solid lightgray; padding: 5px; overflow: auto; height: 507px; width: 400px; padding-top: 6px;">
                                            <a href="javascript: treeExpandAllNodes();">Expand All</a>
                                            &nbsp;<a href="javascript: treeCollapseAllNodes();">Collapse All</a>
                                            &nbsp;<asp:HyperLink ID="hlAddNewCategory" Text="Add New Category" runat="server"></asp:HyperLink>
                                            <telerik:RadTreeView ID="rtvCategory" CheckBoxes="true" ClientIDMode="Static" runat="server">
                                                <DataBindings>
                                                    <telerik:RadTreeNodeBinding Expanded="true"></telerik:RadTreeNodeBinding>
                                                </DataBindings>
                                            </telerik:RadTreeView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-1"></div>
                            <div class="col-xs-12 col-sm-7">
                                <div class="form-group">
                                    <label>Short Description</label>
                                    <telerik:RadEditor ID="RadEditor2" runat="server" Height="195px" OnClientPasteHtml="OnClientPasteHtml"
                                        ToolsFile="~/Marketing/EditorTools.xml" AllowScripts="true">
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                                </div>
                                <div class="form-group">
                                    <label>Long Description</label>
                                    <telerik:RadEditor ID="RadEditor1" runat="server" Height="315px" OnClientPasteHtml="OnClientPasteHtml"
                                        ToolsFile="~/Marketing/EditorTools.xml" AllowScripts="true">
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage><asp:HiddenField ID="hfRowIndex" runat="server" />
    <asp:HiddenField ID="hfMetaID" runat="server" />
    <asp:HiddenField ID="hdnIsAssembly" runat="server" />
    <asp:HiddenField ID="hdnItemName" runat="server" />
    <asp:TextBox ID="txtVendorId" runat="server" Style="display: none">
    </asp:TextBox><asp:HiddenField ID="hdnItemId" runat="server" />


    <div class="modal" id="divTransferStock" runat="server">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Inventory Transfer</h4>
                </div>
                <div class="modal-body">
                    <asp:HiddenField ID="hdnTransferFromWarehouse" runat="server" />
                    <asp:HiddenField ID="hdnSelectedSerialLot" runat="server" />
                    <asp:Label ID="lblTransferExc" runat="server" ForeColor="Red"></asp:Label>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <label><span class="label" style="color: red">* </span>Enter the on-hand qty you�d like transferred from this location:</label>
                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Width="80"></asp:TextBox>
                                <asp:HyperLink ID="hplSerialLot" runat="server" Visible="false">Added (0)</asp:HyperLink>
                            </div>
                        </div>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <label><span class="label" style="color: red">* </span>Select where you�d like this qty transferred:</label>
                                <telerik:RadComboBox ID="radTransferToWarehouse" Width="199" runat="server" DropDownWidth="613px" Height="100"
                                    AccessKey="W" ClientIDMode="Static" DropDownCssClass="multipleRowsColumns" EnableScreenBoundaryDetection="true" TabIndex="500">
                                    <HeaderTemplate>
                                        <ul>
                                            <li class="col1">Warehouse</li>
                                            <li class="col2">On Hand</li>
                                            <li class="col3">On Order</li>
                                            <li class="col4">On Allocation</li>
                                            <li class="col5">BackOrder</li>
                                        </ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <ul>
                                            <li style="display: none">
                                                <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                            <li class="col1">
                                                <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                            <li class="col2">
                                                <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                            <li class="col3">
                                                <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                            &nbsp;</li>
                                            <li class="col4">
                                                <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                            <li class="col5">
                                            <li class="col5">
                                                <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                            <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                Style="display: none"></asp:Label></li>
                                        </ul>
                                    </ItemTemplate>
                                </telerik:RadComboBox>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCloseStockTransfer" CssClass="btn btn-default pull-left" runat="server" Text="Close" />
                    <asp:Button ID="btnSaveStockTransfer" CssClass="btn btn-primary" runat="server" Text="Save & Close" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal" id="divKitChild">
        <div class="modal-dialog" style="color: #000; background-color: #FFF; width:97%;height:96%;margin:20px;border: 1px solid #ddd;overflow: hidden">
            <div class="modal-content" style="height:100%;">
                <div class="modal-header" style="height: 70px;position: relative;background-color: #f7f7f7 !important;color: #000;">
                    <div style="float: left">
                        <img alt="" src="../images/ItemConfig.png" style="vertical-align: middle; padding-right: 5px; max-height: 50px" id="imgKit" />
                        <b style="font-size: 14px">
                            <label id="lblKitItem"></label>
                        </b>
                        &nbsp;&nbsp;
                    <label id="lblKitDescription" style="max-width: 400px; vertical-align:middle"></label>
                        <b>CONFIGURED PRICE:</b>
                        <label id="lblKitCalculaedPrice" style="color: #00b050; font-size: 18px">$ 0.00</label>
                    </div>
                    <div style="float: right; padding-right: 7px;">
                        <input type="button" class="btn btn-primary" value="Create New Kit" onclick="CreateNewItem(false,true,false);" />
                        <input type="button" class="btn btn-primary" value="Create New Assembly" onclick="CreateNewItem(true,false,false);" />
                        <input type="button" class="btn btn-primary" value="Reset" onclick="ResetKitConfiguration();" />
                        <input type="button" class="btn btn-primary" value="Cancel" onclick="$('#divKitChild').modal('hide');" />
                    </div>
                </div>
                <div class="modal-body" style="overflow-y: auto; max-height: 94%; margin-top: 1px;">
                    <div style="text-align: center">
                        <asp:Label ID="lblKitChildError" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </div>
                    <div class="panel-group" id="divChildKitContainer">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay" id="divLoader" style="display:none">
        <div class="overlayContent" style="background-color:#fff; color: #000; text-align: center; width: 280px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Processing Request</h3>
        </div>
    </div>
   <%-- <div class="overlay1" id="" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 97%; height: 96%; margin: 20px; border: 1px solid #ddd; overflow: hidden">
            <div class="dialog-header" style="height: 70px">
            </div>
            <div class="dialog-body">
            </div>
        </div>
    </div>--%>

    <%--<div class="overlay" id="divEDIFields" runat="server" style="display: none">
           <div class="overlayContent" style="color: #000; background-color: #FFF; width: 350px; border: 1px solid #ddd;   ">
               <table border="0" class="dialog-body">
                   <tr>
                       <td style="text-align:center;">
                            <asp:Button ID="btnCancelEdiFields" runat="server" OnClientClick="javascript: HideEDIPopup(); return false;" CssClass="btn btn-primary" ForeColor="White" Text="Cancel" />
                           <asp:Button ID="btnSaveEdiFields" OnClientClick="if(!ValidateEDITextboxes()){return false;}" runat="server" OnClick="btnSaveEdiFields_Click" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" />
                       </td>
                   </tr>
                   <tr>
                       <td>
                           &nbsp;
                           <asp:Table ID="tblEdiFields" CellPadding="3" CellSpacing="3"  runat="server" CssClass="table table-responsive tblNoBorder" style="width:100%" >

                           </asp:Table>
                       </td>
                   </tr>
                   <tr><td>&nbsp;</td></tr>
               </table>
            </div>
      </div>--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Item Details&nbsp;&nbsp;<asp:Label ID="lblitemcode" runat="server" ForeColor="Gray" />
    &nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frmkitdetails.aspx')"><label class="badge bg-yellow">?</label></a>





</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
