﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmItemBarCode
    Inherits BACRMPage

    Dim dsDisplayFields As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                PersistTable.Load("", False, "../Items/frmItemBarCode.aspx")
                If PersistTable.Count > 0 Then
                    rblBarcodeBasedOn.SelectedValue = PersistTable(rblBarcodeBasedOn.ID)
                    If ddlBarcodeType.Items.FindByValue(PersistTable(ddlBarcodeType.ID)) IsNot Nothing Then
                        ddlBarcodeType.ClearSelection()
                        ddlBarcodeType.Items.FindByValue(PersistTable(ddlBarcodeType.ID)).Selected = True
                    End If

                    txtWidth.Text = CCommon.ToDouble(PersistTable(txtWidth.ID))
                    txtHeight.Text = CCommon.ToDouble(PersistTable(txtHeight.ID))
                    txtItemNameLength.Text = CCommon.ToDouble(PersistTable(txtItemNameLength.ID))
                    chkAllItems.Checked = CCommon.ToBool(PersistTable(chkAllItems.ID))
                    chkPrintInSinglePage.Checked = CCommon.ToBool(PersistTable(chkPrintInSinglePage.ID))
                End If

                BindDatagrid()
            End If
            txtWidth.Attributes.Add("onkeypress", "CheckNumber(2)")
            txtHeight.Attributes.Add("onkeypress", "CheckNumber(2)")

            txtItemNameLength.Attributes.Add("onkeypress", "CheckNumber(2)")
            btnCancel.Attributes.Add("onclick", "return Close()")
            btnPrint.Attributes.Add("onclick", "return PrintBarcode()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim objItems As New CItems

            objItems.UserCntID = Session("UserContactID")
            objItems.DomainID = Session("DomainID")
            objItems.str = GetQueryStringVal("ItemList").ToString.Trim(",")
            Dim dtList As DataTable
            dtList = objItems.GetItemBarcodeList(chkAllItems.Checked, rblBarcodeBasedOn.SelectedValue)
            dsDisplayFields = objItems.GetItemBarcodeDisplayFields(chkAllItems.Checked)

            gvItemBarcode.DataSource = dtList
            gvItemBarcode.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGenerateBarcode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateBarcode.Click
        Try
            BindDatagrid()

            PersistTable.Clear()
            PersistTable.Add(rblBarcodeBasedOn.ID, CCommon.ToLong(rblBarcodeBasedOn.SelectedValue))
            PersistTable.Add(ddlBarcodeType.ID, ddlBarcodeType.SelectedValue)
            PersistTable.Add(txtWidth.ID, txtWidth.Text)
            PersistTable.Add(txtHeight.ID, txtHeight.Text)
            PersistTable.Add(txtItemNameLength.ID, txtItemNameLength.Text)
            PersistTable.Add(chkAllItems.ID, chkAllItems.Checked)
            PersistTable.Add(chkPrintInSinglePage.ID, chkPrintInSinglePage.Checked)
            PersistTable.Save("", False, "../Items/frmItemBarCode.aspx")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvItemBarcode_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemBarcode.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Dim imgBarcode As WebControls.Image = DirectCast(e.Row.FindControl("imgBarcode"), WebControls.Image)
                If CCommon.ToInteger(txtWidth.Text) <= 0 Then
                    txtWidth.Text = "1"
                End If

                If CCommon.ToInteger(txtHeight.Text) <= 0 Then
                    txtHeight.Text = "50"
                End If

                If CCommon.ToInteger(txtItemNameLength.Text) < 0 Then
                    txtItemNameLength.Text = "10"
                End If
                Dim strItemName As String = DataBinder.Eval(e.Row.DataItem, "vcItemName")

                If strItemName.Length > txtItemNameLength.Text Then
                    strItemName = strItemName.Substring(0, txtItemNameLength.Text)
                End If

                strItemName = HttpUtility.HtmlEncode(strItemName)
                'imgBarcode.ImageUrl = String.Format("../Common/Barcode.ashx?code={0}&barSize={2}&ShowCodeString={3}&title={1}", DataBinder.Eval(e.Row.DataItem, "numBarCodeId"), strItemName, txtSize.Text, chkBarcode.Checked)

                Dim strDisplayFields As String = ""
                Dim strDisplayContent As String = ""
                Dim drDisplayField() As DataRow = dsDisplayFields.Tables(0).Select("numItemCode =" & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")))
                If drDisplayField.Length > 0 Then
                    strDisplayFields = "<ul style=""width: 200px;"">"
                    For Each drField As DataRow In dsDisplayFields.Tables(1).Rows
                        If CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName"))).Length > 0 AndAlso _
                           CCommon.ToString(drField("vcDbColumnName")) <> "numItemCode" AndAlso _
                           CCommon.ToString(drField("vcDbColumnName")) <> "vcItemName" Then
                            Select Case CCommon.ToString(drField("vcAssociatedControlType"))

                                Case "SelectBox"
                                    If CCommon.ToString(drField("vcDbColumnName")) = "numItemGroup" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("vcItemGroup"))

                                    ElseIf CCommon.ToString(drField("vcDbColumnName")) = "numShipClass" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("numShipClass"))

                                    ElseIf CCommon.ToString(drField("vcDbColumnName")) = "charItemType" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("ItemType"))

                                    ElseIf CCommon.ToString(drField("vcDbColumnName")) = "numBaseUnit" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("vcBaseUnit"))

                                    ElseIf CCommon.ToString(drField("vcDbColumnName")) = "numItemClassification" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("ItemClassification"))

                                    ElseIf CCommon.ToString(drField("vcDbColumnName")) = "numSaleUnit" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("vcSaleUnit"))

                                    ElseIf CCommon.ToString(drField("vcDbColumnName")) = "numPurchaseUnit" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("vcPurchaseUnit"))

                                    ElseIf CCommon.ToBool(drField("bitCustomField")) = True Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    Else
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    End If

                                Case "ListBox"
                                    If CCommon.ToString(drField("vcDbColumnName")) = "numVendorID" Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)("vcVendor"))

                                    ElseIf CCommon.ToBool(drField("bitCustomField")) = True Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    Else
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    End If

                                Case "CheckBox"
                                    If CCommon.ToBool(drField("bitCustomField")) = True Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    Else
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    End If

                                Case "TextBox"
                                    If CCommon.ToBool(drField("bitCustomField")) = True Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    Else
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    End If

                                Case Else
                                    If CCommon.ToBool(drField("bitCustomField")) = True Then
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    Else
                                        strDisplayContent = CCommon.ToString(drField("vcFieldName")) & " : " & CCommon.ToString(drDisplayField(0)(drField("vcDbColumnName")))

                                    End If

                            End Select

                            strDisplayFields = strDisplayFields + "<li>" & strDisplayContent & "</li>"
                        End If
                    Next
                    strDisplayFields = strDisplayFields + "</ul>"
                End If

                e.Row.Cells(4).Text = String.Format("<table class='tblBarcode'><tr style='vertical-align: top;'><td style='border-width:0px;'><div id='bcTarget{0}' class='barcode'></div></td></tr><tr><td style='border-width:0px;padding-left: 20px;'>" & strDisplayFields & "</td></tr></table><script language='javascript' type='text/javascript'> $('#bcTarget{0}').barcode('{1}', '{2}',{{barWidth:{3}, barHeight:{4}}},'{5}')</script>",
                                                    e.Row.RowIndex, DataBinder.Eval(e.Row.DataItem, "numBarCodeId"), ddlBarcodeType.SelectedValue, txtWidth.Text, txtHeight.Text, strItemName)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub chkAllItems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllItems.CheckedChanged
        Try
            BindDatagrid()

            PersistTable.Clear()
            PersistTable.Add(rblBarcodeBasedOn.ID, CCommon.ToLong(rblBarcodeBasedOn.SelectedValue))
            PersistTable.Add(ddlBarcodeType.ID, ddlBarcodeType.SelectedValue)
            PersistTable.Add(txtWidth.ID, txtWidth.Text)
            PersistTable.Add(txtHeight.ID, txtHeight.Text)
            PersistTable.Add(txtItemNameLength.ID, txtItemNameLength.Text)
            PersistTable.Add(chkAllItems.ID, chkAllItems.Checked)
            PersistTable.Add(chkPrintInSinglePage.ID, chkPrintInSinglePage.Checked)
            PersistTable.Save("", False, "../Items/frmItemBarCode.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try
            PersistTable.Clear()
            PersistTable.Add(rblBarcodeBasedOn.ID, CCommon.ToLong(rblBarcodeBasedOn.SelectedValue))
            PersistTable.Add(ddlBarcodeType.ID, ddlBarcodeType.SelectedValue)
            PersistTable.Add(txtWidth.ID, txtWidth.Text)
            PersistTable.Add(txtHeight.ID, txtHeight.Text)
            PersistTable.Add(txtItemNameLength.ID, txtItemNameLength.Text)
            PersistTable.Add(chkAllItems.ID, chkAllItems.Checked)
            PersistTable.Add(chkPrintInSinglePage.ID, chkPrintInSinglePage.Checked)
            PersistTable.Save("", False, "../Items/frmItemBarCode.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class