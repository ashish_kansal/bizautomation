﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmItemDependenciesParentItems.aspx.vb" Inherits=".frmItemDependenciesParentItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Item Dependencies Parent Items</title>
    <script type="text/javascript">
        function OpenItem(a) {
            str = "../Items/frmKitDetails.aspx?ItemCode=" + a;
            window.opener.location.href = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Parent Kit Items
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="600px">
        <tr>
            <td>
                <asp:GridView ID="gvItemDependencies" runat="server" AutoGenerateColumns="False"
                    CssClass="tbl" Width="100%">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField HeaderText="Item">
                            <ItemTemplate>
                                <a href="#" onclick="OpenItem('<%# Eval("numItemCode")%>');">
                                    <%# Eval("vcItemName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ItemType" HeaderText="Item Type"></asp:BoundField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
