﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Partial Public Class frmPriceBookSteps
    Inherits BACRMPage
    Dim objPriceBookRule As PriceBookRule
    Dim lngRuleId As Long
    Dim StepNo, StepValue As Short
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngRuleId = GetQueryStringVal( "RuleId")
            StepNo = GetQueryStringVal( "Step")
            StepValue = GetQueryStringVal( "StepValue")
            If Not IsPostBack Then
                
                If lngRuleId > 0 Then
                    LoadDetails()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            objPriceBookRule = New PriceBookRule
            If StepNo = 2 Then
                Select Case StepValue
                    Case 1
                        pnlItems.Visible = True
                        objPriceBookRule.RuleAppType = 1
                    Case 2
                        pnlItemClassification.Visible = True
                        objPriceBookRule.RuleAppType = 2
                End Select
            ElseIf StepNo = 3 Then
                Select Case StepValue
                    Case 1
                        pnlOrganizations.Visible = True
                        objPriceBookRule.RuleAppType = 3
                    Case 2
                        pnlRelProfiles.Visible = True
                        objPriceBookRule.RuleAppType = 4
                End Select
            End If
            objPriceBookRule.RuleID = lngRuleId
            objPriceBookRule.DomainID = Session("DomainID")


            Dim ds As DataSet

            ds = objPriceBookRule.GetPriceBookRuleDTL

            'lstItemGroupAvail.DataSource = ds.Tables(1)
            'lstItemGroupAvail.DataTextField = "vcItemGroup"
            'lstItemGroupAvail.DataValueField = "numItemGroupID"
            'lstItemGroupAvail.DataBind()

            'lstItemsGroupAdd.Items.Clear()
            'lstItemsAdd.Items.Clear()
            'lstCompany.Items.Clear()
            'lstRel.Items.Clear()
            'lstRelPro.Items.Clear()

            If objPriceBookRule.RuleAppType = 1 Then

                dgItems.DataSource = ds.Tables(0)
                dgItems.DataBind()

            ElseIf objPriceBookRule.RuleAppType = 2 Then

                ddlItemClassification.DataTextField = "ItemClassification"
                ddlItemClassification.DataValueField = "numItemClassification"
                ddlItemClassification.DataSource = ds.Tables(0)
                ddlItemClassification.DataBind()
                ddlItemClassification.Items.Insert(0, New ListItem("--Select One--", 0))

                dgItemClass.DataSource = ds.Tables(1)
                dgItemClass.DataBind()
               
            ElseIf objPriceBookRule.RuleAppType = 3 Then

                dgOrg.DataSource = ds.Tables(0)
                dgOrg.DataBind()


            ElseIf objPriceBookRule.RuleAppType = 4 Then
                
                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))

                dgRelationship.DataSource = ds.Tables(0)
                dgRelationship.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAddItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddItem.Click
        Try
            If radItem.SelectedValue <> "" Then
                objPriceBookRule = New PriceBookRule
                objPriceBookRule.RuleID = lngRuleId
                objPriceBookRule.RuleAppType = 1
                objPriceBookRule.RuleValue = radItem.SelectedValue
                objPriceBookRule.byteMode = 0
                objPriceBookRule.ManagePriceBookRuleDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemItem.Click
        Try
            objPriceBookRule = New PriceBookRule
            For Each Item As DataGridItem In dgItems.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPriceBookRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPriceBookRule.byteMode = 1
                    objPriceBookRule.RuleAppType = 1
                    objPriceBookRule.ManagePriceBookRuleDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportItem.Click
        ExportToExcel.DataGridToExcel(dgItems, Response)
    End Sub

    Private Sub btnExportItemClassification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportItemClassification.Click
        ExportToExcel.DataGridToExcel(dgItemClass, Response)
    End Sub

    Private Sub btnAddItemClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemClass.Click
        Try
            If ddlItemClassification.SelectedValue > 0 Then
                objPriceBookRule = New PriceBookRule
                objPriceBookRule.RuleID = lngRuleId
                objPriceBookRule.RuleAppType = 2
                objPriceBookRule.RuleValue = ddlItemClassification.SelectedValue
                objPriceBookRule.byteMode = 0
                objPriceBookRule.ManagePriceBookRuleDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveItemClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveItemClass.Click
        Try
            objPriceBookRule = New PriceBookRule
            For Each Item As DataGridItem In dgItemClass.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPriceBookRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPriceBookRule.byteMode = 1
                    objPriceBookRule.RuleAppType = 2
                    objPriceBookRule.ManagePriceBookRuleDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOrg.Click
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                objPriceBookRule = New PriceBookRule
                objPriceBookRule.RuleID = lngRuleId
                objPriceBookRule.RuleAppType = 3
                objPriceBookRule.RuleValue = radCmbCompany.SelectedValue
                objPriceBookRule.byteMode = 0
                objPriceBookRule.ManagePriceBookRuleDTL()

                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveOrg.Click
        Try
            objPriceBookRule = New PriceBookRule
            For Each Item As DataGridItem In dgOrg.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPriceBookRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPriceBookRule.byteMode = 1
                    objPriceBookRule.ManagePriceBookRuleDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportOrg.Click
        ExportToExcel.DataGridToExcel(dgOrg, Response)
    End Sub

    Private Sub btnAddRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRelProfile.Click
        Try
            If CCommon.ToLong(ddlRelationship.SelectedValue) > 0 And CCommon.ToLong(ddlProfile.SelectedValue) > 0 Then
                objPriceBookRule = New PriceBookRule
                objPriceBookRule.RuleID = lngRuleId
                objPriceBookRule.RuleAppType = 4
                objPriceBookRule.RuleValue = ddlRelationship.SelectedValue
                objPriceBookRule.Profile = ddlProfile.SelectedValue
                objPriceBookRule.byteMode = 0
                objPriceBookRule.ManagePriceBookRuleDTL()
                LoadDetails()
            End If
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveRelProfile.Click
        Try
            objPriceBookRule = New PriceBookRule
            For Each Item As DataGridItem In dgRelationship.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objPriceBookRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objPriceBookRule.byteMode = 1
                    objPriceBookRule.ManagePriceBookRuleDTL()
                End If
            Next
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportRelProfile.Click
        ExportToExcel.DataGridToExcel(dgRelationship, Response)
    End Sub
End Class