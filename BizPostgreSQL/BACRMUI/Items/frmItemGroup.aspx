<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmItemGroup.aspx.vb"
    Inherits=".frmItemGroup" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Item Group</title>
    <script type="text/javascript">
     function validateCombineAssemb() {
            if ($("#<%=chkCombineAssemblies.ClientID%>").prop("checked") == true) {
                if ($("#<%=ddlMappedDropDown.ClientID%> option:selected").val() == 0 || $("#<%=ddlMappedDropDown.ClientID%> option:selected").val() == '') {
                    alert("Please Select Mapped Dropdown");
                    return false;
                }
                if ($("#<%=ddlProfileItem.ClientID%> option:selected").val() == 0 || $("#<%=ddlProfileItem.ClientID%> option:selected").val() == '') {
                    alert("Please Select Profile Item");
                    return false;
                }
            }
        }
        function MoveUp(tbox) {
            var j = 0;
            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i - 1] != null) {
                    if (tbox.options[i].selected && tbox.options[i].value != "") {

                        var SelectedText, SelectedValue;
                        SelectedValue = tbox.options[i].value;
                        SelectedText = tbox.options[i].text;
                        tbox.options[i].value = tbox.options[i - 1].value;
                        tbox.options[i].text = tbox.options[i - 1].text;
                        tbox.options[i - 1].value = SelectedValue;
                        tbox.options[i - 1].text = SelectedText;
                        j = i - 1;
                    }
                }
            }
            tbox.options[j].selected = true;
            return false;
        }

        function MoveDown(tbox) {
            var j = 0;
            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i - 1] != null) {
                    if (tbox.options[i].selected && tbox.options[i].value != "") {

                        var SelectedText, SelectedValue;
                        SelectedValue = tbox.options[i].value;
                        SelectedText = tbox.options[i].text;
                        tbox.options[i].value = tbox.options[i + 1].value;
                        tbox.options[i].text = tbox.options[i + 1].text;
                        tbox.options[i + 1].value = SelectedValue;
                        tbox.options[i + 1].text = SelectedText;
                        j = i + 1;
                    }
                }
            }
            tbox.options[j].selected = true;
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function AddOpt(tbox) {
            for (var j = 0; j < tbox.options.length; j++) {
                if (tbox.options[j].value == $find('radItem').get_value()) {
                    alert("Item is already selected");
                    return false;
                }
            }
            var no = new Option();
            no.value = $find('radItem').get_value();
            no.text = $find('radItem').get_text();
            tbox.options[tbox.options.length] = no;
            return false;
        }

        function RemoveOpt(fbox) {
            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";
                    BumpUp(fbox);
                    return false;
                }
            }
            BumpUp(fbox);
            return false;
        }

        function remove1(fbox, tbox) {
            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }

        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }

        function Save() {
            str = '';
            for (var i = 0; i < document.getElementById("lstAttrAddfld").options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById("lstAttrAddfld").options[i].value;
                SelectedText = document.getElementById("lstAttrAddfld").options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById("txtAttrHidden").value = str;

            var isGrpOneValid = Page_ClientValidate("vgSave");

            if (typeof (Page_Validators) == "undefined") {
                return true;
            }

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            //display all summaries.
            for (i = 0; i < Page_ValidationSummaries.length; i++) {
                summary = Page_ValidationSummaries[i];
                //does this summary need to be displayed?
                if (fnJSDisplaySummary(summary.validationGroup)) {
                    summary.style.display = ""; //"none"; "inline";
                }
            }

            if (isGrpOneValid) {
                return true; //postback only when BOTH validations pass.
            }
            else
                return false;
        }

        /* determines if a Validation Summary for a given group needs to display */
        function fnJSDisplaySummary(valGrp) {
            var rtnVal = false;
            for (i = 0; i < Page_Validators.length; i++) {
                if (Page_Validators[i].validationGroup == valGrp) {
                    if (!Page_Validators[i].isvalid) { //at least one is not valid.
                        rtnVal = true;
                        break; //exit for-loop, we are done.
                    }
                }
            }
            return rtnVal;
        }
        function OnClientItemsRequesting(sender, eventArgs) {
            var context = eventArgs.get_context();
            context["Text"] = eventArgs.get_text();
            context["checkAssembly"] = $("#<%=chkCombineAssemblies.ClientID%>").prop("checked")
        }
    </script>

    <style type="text/css">
        .data-center {
            justify-content: initial !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="vgSave" ShowSummary="false" ShowMessageBox="true" />
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" ValidationGroup="vgSave"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text="Save & Close" ValidationGroup="vgSave"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Item Group
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <div class="table-responsive">
        <div class="row padbottom10">
            <div class="col-xs-12 col-sm-4">
                <div class="form-inline">
                    <label>Item Group Name:</label>
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtIGroupName"></asp:TextBox>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="pull-right">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Combine Assemblies:</label>
                            <asp:CheckBox ID="chkCombineAssemblies" AutoPostBack="true" OnCheckedChanged="chkCombineAssemblies_CheckedChanged" runat="server" />
                        </div>
                        <div class="form-group" runat="server" id="divMapDropDown" visible="false">
                            <label>Map to this drop-down:</label>
                            <asp:DropDownList ID="ddlMappedDropDown" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group" runat="server" id="divProfile" visible="false">
                            <label>Profile Item:</label>
                            <asp:DropDownList ID="ddlProfileItem" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Choose Options And Accessories</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row padbottom10">
                            <div class="col-xs-12">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>Add Item:</label>
                                        <telerik:RadComboBox ID="radItem" runat="server" Width="200" DropDownWidth="200px"
                                            EnableScreenBoundaryDetection="true" AutoPostBack="true" EnableLoadOnDemand="true" OnClientItemsRequesting="OnClientItemsRequesting"
                                            AllowCustomText="True">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                        </telerik:RadComboBox>
                                    </div>
                                    <div class="form-group" id="trKits" runat="server" visible="false">
                                        <label></label>
                                        <asp:RadioButtonList ID="rblKitItem" runat="server"></asp:RadioButtonList>
                                    </div>
                                    <asp:HiddenField ID="hdnLngId" runat="server" Value="0" />
                                      <asp:Button ID="btnAddChilItem" OnClientClick="validateCombineAssemb()" runat="server" CssClass="btn btn-primary" Text="Add Item" />
                                    <asp:Button ID="btnRemove" runat="server" CssClass="btn btn-danger" Text="Remove Item" />
                                </div>
                            </div>
                        </div>
                        <asp:DataGrid ID="dgKitItems"  runat="server" Width="100%" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" ClientIDMode="AutoID" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numWarehouseItmsID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="vcItemName" HeaderText="Child Item"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Warehouse">
                                    <ItemTemplate>
                                        <telerik:RadComboBox ID="radKitWareHouse" runat="server" Width="250" Height="80" DropDownWidth="550px"
                                            AccessKey="W" AutoPostBack="true" OnSelectedIndexChanged="DropDown_SelectedIndexChanged">
                                            <HeaderTemplate>
                                                <table style="width: 550px; text-align: left">
                                                    <tr>
                                                        <td></td>
                                                        <td class="normal1" style="width: 125px;">Warehouse
                                                        </td>
                                                        <td class="normal1" style="width: 200px;">Attributes
                                                        </td>
                                                        <td class="normal1" style="width: 50px;">On Hand
                                                        </td>
                                                        <td class="normal1" style="width: 50px;">On Order
                                                        </td>
                                                        <td class="normal1" style="width: 75px;">On Allocation
                                                        </td>
                                                        <td class="normal1" style="width: 50px;">BackOrder
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table style="width: 550px; text-align: left">
                                                    <tr>
                                                        <td style="display: none">
                                                            <%#DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%>
                                                        </td>
                                                        <td style="width: 125px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "vcWareHouse")%>
                                                        </td>
                                                        <td style="width: 200px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "Attr")%>
                                                        </td>
                                                        <td style="width: 50px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "numOnHand")%>
                                                        </td>
                                                        <td style="width: 50px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                                        </td>
                                                        <td style="width: 75px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "numAllocation")%>
                                                        </td>
                                                        <td style="width: 50px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "numBackOrder")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator ID="rfvKitWareHouse" runat="server" ErrorMessage="Warehouse Required"
                                            ControlToValidate="radKitWareHouse" Display="Dynamic" SetFocusOnError="true"
                                            ValidationGroup="vgSave" Text="*"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="txtItemDesc" HeaderText="Desc"></asp:BoundColumn>
                                <asp:BoundColumn DataField="charItemType" HeaderText="Type"></asp:BoundColumn>
                                <asp:BoundColumn DataField="UnitPrice" HeaderText="Unit Price"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ItemType" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn Visible="False">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlItemAttribute" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="numListId" Visible="false"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Choose Attributes</h3>

                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row data-center">
                            <div class="col-xs-12 col-sm-5">
                                <div class="form-group text-center">
                                    <label>Available Attributes</label>
                                    <asp:ListBox ID="lstAttrAvailablefld" runat="server" Width="200" Height="200" CssClass="form-control" Style="margin: 0 auto;"></asp:ListBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-2 text-center">
                                <asp:Button ID="btnAttrAdd" CssClass="btn btn-default" runat="server" Text="Add >"></asp:Button>
                                <br />
                                <br />
                                <asp:Button ID="btnAttrRemove" CssClass="btn btn-default" runat="server" Text="< Remove"></asp:Button>
                            </div>
                            <div class="col-xs-12 col-sm-5 text-center">
                                <div class="form-group">
                                    <label>Selected Attributes</label>
                                    <asp:ListBox ID="lstAttrAddfld" runat="server" Width="200" Height="200" CssClass="form-control" Style="margin: 0 auto;"></asp:ListBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:TextBox Style="display: none" ID="txtOpptHidden" runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="txtAttrHidden" runat="server"></asp:TextBox>

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
