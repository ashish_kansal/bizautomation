<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmAddWareHouse.aspx.vb" Inherits=".frmAddWareHouse" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Warehouse Detail</title>
    <script type="text/javascript">
        function Save() {

            if (document.getElementById('txtCompName').value == '') {
                alert("Enter Warehouse Name");
                document.getElementById('txtCompName').focus()
                return false;
            }

            var radcmbEmployerAddress = $find('radcmbEmployerAddress');
            if (radcmbEmployerAddress != null && radCmbCompany.get_value() == '0') {
                alert("Select address");
                radcmbEmployerAddress.focus();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Warehouse Detail
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label>Warehouse <span style="color: red">*</span></label>
                        <asp:TextBox ID="txtCompName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label>PrintNode API Key</label>
                        <asp:TextBox ID="txtPrintNodeAPIKey" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label>PrintNode Printer ID</label>
                        <asp:TextBox ID="txtPrintNodePrinterID" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label>Address <span style="color: red">*</span></label>
                        <div>
                            <telerik:RadComboBox ID="radcmbEmployerAddress" runat="server" AutoPostBack="true" Width="150" />
                            <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
