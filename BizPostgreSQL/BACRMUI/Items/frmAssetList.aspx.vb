﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Items
    Public Class frmAssetList
        Inherits BACRMPage
        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Session("OwnerId") = Nothing
                Session("OwnerName") = Nothing

                If Not IsPostBack Then
                    btnNew.Text = "New Asset"
                    FillFilter()

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortChar.Text = PersistTable(PersistKey.SortCharacter)
                        txtSortColumn.Text = PersistTable(PersistKey.SortColumnName)
                        txtSortOrder.Text = PersistTable(PersistKey.SortOrder)
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))

                        If ddlSearch.Items.FindByValue(CCommon.ToString(PersistTable(ddlSearch.ID))) IsNot Nothing Then
                            ddlSearch.ClearSelection()
                            ddlSearch.Items.FindByValue(CCommon.ToString(PersistTable(ddlSearch.ID))).Selected = True
                        End If
                        txtSearch.Text = PersistTable(PersistKey.SearchValue)

                        If ddlFilter.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) IsNot Nothing Then
                            ddlFilter.ClearSelection()
                            ddlFilter.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        End If
                    End If

                    LoadAssets()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub FillFilter()

            objCommon.sb_FillComboFromDBwithSel(ddlFilter, 36, Session("DomainID"))
        End Sub
        Public Sub LoadAssets()
            Try
                Dim ds As DataSet
                Dim objItems As New CItems

                btnNew.Attributes.Add("onclick", "return OpenNewAsset(-1," & 0 & ",'frmAssetList')")
                With objItems

                    If ddlFilter.SelectedItem Is Nothing Then
                        FillFilter()
                    End If
                    If radCmbCompany.SelectedValue <> "" Then
                        .DivisionID = radCmbCompany.SelectedValue
                    Else
                        .DivisionID = 0
                    End If

                    .ItemClassification = ddlFilter.SelectedItem.Value
                    .SortCharacter = txtSortChar.Text.Trim()

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = Convert.ToInt32(txtCurrrentPage.Text.Trim())

                    If ddlSearch.SelectedItem.Value <> "" And txtSearch.Text <> "" And ddlSearch.SelectedItem.Value <> "vcWarehouse" Then
                        .KeyWord = ddlSearch.SelectedItem.Value & " ilike '%" & txtSearch.Text.Trim & "%'"
                    Else
                        .KeyWord = ""
                    End If

                    .DomainID = Session("DomainID")
                    .PageSize = Convert.ToInt32(Session("PagingRows"))
                    .TotalRecords = 0

                    If txtSortColumn.Text <> "" Then
                        If txtSortColumn.Text.Split("~").Length = 2 Then
                            .columnName = txtSortColumn.Text.Split("~")(1)
                        Else
                            .columnName = txtSortColumn.Text
                        End If
                    Else : .columnName = "vcItemName"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    ds = .getCompanyAssetsSerial

                End With

                bizPager.PageSize = Session("PagingRows")
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                bizPager.RecordCount = objItems.TotalRecords

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(ds.Tables(0).Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim)
                PersistTable.Add(PersistKey.SortColumnName, IIf(txtSortColumn.Text = "", "vcItemName", txtSortColumn.Text))
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                PersistTable.Add(PersistKey.SearchValue, txtSearch.Text.Trim)
                PersistTable.Add(ddlSearch.ID, ddlSearch.SelectedValue)
                PersistTable.Add(PersistKey.FilterBy, ddlFilter.SelectedValue)
                PersistTable.Save()

                'If objItems.TotalRecords <> 0 Then
                '    hideItems.Visible = True
                '    lblRecordsItems.Text = objItems.TotalRecords
                '    Dim strTotalPage As String()
                '    Dim decTotalPage As Decimal
                '    decTotalPage = lblRecordsItems.Text / Session("PagingRows")
                '    decTotalPage = Math.Round(decTotalPage, 2)
                '    strTotalPage = CStr(decTotalPage).Split(".")
                '    If (lblRecordsItems.Text Mod Session("PagingRows")) = 0 Then
                '        lblTotalItems.Text = strTotalPage(0)
                '        txtTotalPageItems.Text = strTotalPage(0)
                '    Else
                '        lblTotalItems.Text = strTotalPage(0) + 1
                '        txtTotalPageItems.Text = strTotalPage(0) + 1
                '    End If
                '    txtTotalRecordsItems.Text = lblRecordsItems.Text
                'End If

                If ds.Relations.Count < 1 Then
                    ds.Tables(0).TableName = "Asset"
                    ds.Tables(1).TableName = "AssetSerial"
                    'ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
                    'ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
                    ds.Relations.Add("Asset", ds.Tables(0).Columns("numItemcode"), ds.Tables(1).Columns("numAssetItemId"))
                End If

                gvAssetItem.DataSource = ds
                gvAssetItem.DataBind()
                'uwItem.Bands(0).DataKeyField = "numItemcode"
                'uwItem.Bands(1).DataKeyField = "numAssetItemId"
                'uwItem.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Function ReturnDate(ByVal CloseDate) As String
            Try
                If CloseDate Is Nothing Then Exit Function
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                    If strTargetResolveDate = "01/01/1900" Then Return ""
                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b></font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                        temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = strTargetResolveDate
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub gvAssetItem_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAssetItem.NeedDataSource
            Try
                'LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                LoadAssets()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
