﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCularisSetting.aspx.vb"
    Inherits=".frmCularisSetting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cularis API Settings</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <script type="text/javascript">
        function Save() {
            if (document.getElementById('ddlWareHouse').value == 0) {
                alert("Select Warehouse")
                return false;
            }
            if (document.getElementById('ddlUsers').value == 0) {
                alert("Select Record Owner")
                return false;
            }
            if (document.getElementById('ddlIncomeAccount').value == 0) {
                alert("Select Income Account")
                return false;
            }
            if (document.getElementById('ddlAssetAccount').value == 0) {
                alert("Select Asset Account")
                return false;
            }
            if (document.getElementById('ddlCOGS').value == 0) {
                alert("Select COGS")
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Cularis API Settings&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="60" />
            </td>
        </tr>
    </table>
    <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="asptable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            Default WareHouse
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlWareHouse" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Default Record Owner
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlUsers" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Income Account
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlIncomeAccount" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Asset Account
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAssetAccount" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            COGS
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCOGS" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
