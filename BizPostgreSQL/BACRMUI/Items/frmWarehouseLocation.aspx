﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmWarehouseLocation.aspx.vb" Inherits=".frmWarehouseLocation" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected records?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function editLocation(Locationid, Aisle, Rack, Shelf, Bin, vcLocation) {
            $("#EditLocationModal").modal("show");
            $("#txtAisleName").val(Aisle);
            $("#txtRackName").val(Rack);
            $("#txtShelfName").val(Shelf);
            $("#txtBinName").val(Bin);
            $("#txtLocationName").val(vcLocation);
            $("#hdnLocationID").val(Locationid);
            return false;
        }
        function saveLocation() {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',numWLocationID:'" + $("#hdnLocationID").val() + "',vcAisle:'" + $("#txtAisleName").val() + "',vcRack:'" + $("#txtRackName").val().trim() + "',vcShelf:'" + $("#txtShelfName").val().trim() + "',vcBin:'" + $("#txtBinName").val().trim() + "', vcLocation:'" + $("#txtLocationName").val().trim() + "'}";
            $.ajax({
                type: "POST",
                url: "../Items/frmWarehouseLocation.aspx/WebMethodUpdateInternalLocation",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == true) {
                        $("#EditLocationModal").modal("hide");
                        $("#btnFilterByWarehouse").click();
                    } else {
                        $("#UpdateProgress").css("display", "none");
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }

        function ConfirmBulkRemove() {
            if (confirm("Only internal locations with 0 on hand will be removed from items that currently have them mapped.\n\nWould you like to bulk remove these internal locations?")) {
                return true;
            }

            return false;
        }


        function ClearFilters() {
            $("[id$=ddlAisle]").val("0");
            $("[id$=ddlRack]").val("0");
            $("[id$=ddlShelf]").val("0");
            $("[id$=ddlBin]").val("0");
            $("[id$=txtLocation]").val("");
            $("[id$=btnFilterByWarehouse]").trigger('click');
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $("[id$=chkIncludeInternalLocation]").change(function(){
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "mode":47
                        ,"updateRecordID":0
                        ,"updateValueID":($(this).is(":checked")? 1 : 0)
                        ,"comments":""
                    }),
                    success: function (data) {
    
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Warehouse:</label>
                    <asp:DropDownList runat="server" ID="ddlWarehouse" AutoPostBack="true" Width="180" CssClass="form-control">
                    </asp:DropDownList>

                    <label>Aisle</label>
                    <asp:DropDownList ID="ddlAisle" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                        <asp:ListItem Value="">-Select-</asp:ListItem>
                    </asp:DropDownList>

                    <label>Rack</label>
                    <asp:DropDownList ID="ddlRack" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                        <asp:ListItem Value="">-Select-</asp:ListItem>
                    </asp:DropDownList>

                    <label>Shelf</label>
                    <asp:DropDownList ID="ddlShelf" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                        <asp:ListItem Value="">-Select-</asp:ListItem>
                    </asp:DropDownList>

                    <label>Bin</label>
                    <asp:DropDownList ID="ddlBin" runat="server" Width="100" AutoPostBack="false" CssClass="form-control">
                        <asp:ListItem Value="">-Select-</asp:ListItem>
                    </asp:DropDownList>

                    <label>Location</label>
                    <asp:TextBox ID="txtLocation" runat="server" Width="100" AutoPostBack="false" CssClass="form-control"></asp:TextBox>

                    <asp:Button ID="btnFilterByWarehouse" runat="server" Text="Go" CssClass="btn btn-primary" />
                    
                </div>
            </div>
            <asp:TextBox ID="txtCurrrentPage" Visible="false" runat="server"></asp:TextBox>
            <div class="pull-right">
                <asp:Button ID="btnBulkRemove" runat="server" Text="Bulk Remove" CssClass="btn btn-primary" OnClientClick="return ConfirmBulkRemove();" OnClick="btnBulkRemove_Click" />
                <a href="frmWarehouseLocationAdd.aspx?WLocationID=<%= ddlWarehouse.SelectedValue %>" class="btn btn-primary">Add Location</a>
                <asp:LinkButton runat="server" CssClass="btn btn-danger" ID="btnDelete" OnClientClick="return DeleteRecord();"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
    
    <div id="EditLocationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md" style="width: 40%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Location</h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <input type="hidden" id="hdnLocationID" />
                        <div class="col-md-2">
                            <label>Aisle</label>
                            <input type="text" class="form-control" id="txtAisleName" />
                        </div>
                         <div class="col-md-2">
                            <label>Rack</label>
                            <input type="text" class="form-control" id="txtRackName" />
                        </div>
                         <div class="col-md-2">
                            <label>Shelf</label>
                            <input type="text" class="form-control" id="txtShelfName" />
                        </div>
                         <div class="col-md-2">
                            <label>Bin</label>
                            <input type="text" class="form-control" id="txtBinName" />
                        </div>
                        <div class="col-md-4">
                            <label>Custom Value</label>
                            <input type="text" class="form-control" id="txtLocationName" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="saveLocation()">Save & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Warehouse locations&nbsp;<a href="#" onclick="return OpenHelpPopUp('items/frmwarehouselocation.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <h5 style="font-weight:bold">Include internal locations when cloning items</h5>&nbsp;<asp:CheckBox runat="server" id="chkIncludeInternalLocation" style="margin-top:7px" />&nbsp;&nbsp;
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
    <a href="javascript:ClearFilters();" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvWarehouseLocation" AllowSorting="True" runat="server" Width="100%" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true">
                    <EmptyDataTemplate>No records found</EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="numWLocationID" HeaderText="ID"></asp:BoundField>
                        <asp:BoundField DataField="vcLocation" HeaderText="Location"></asp:BoundField>
                        <asp:BoundField DataField="vcAisle" HeaderText="Aisle"></asp:BoundField>
                        <asp:BoundField DataField="vcRack" HeaderText="Rack"></asp:BoundField>
                        <asp:BoundField DataField="vcShelf" HeaderText="Shelf"></asp:BoundField>
                        <asp:BoundField DataField="vcBin" HeaderText="Bin"></asp:BoundField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                            <HeaderTemplate>
                                <asp:CheckBox ID="SelectAllCheckBox" runat="server" onclick="SelectAll('SelectAllCheckBox', 'chkSelected');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%" HeaderText="Edit">
                            <ItemTemplate>
                                <a class="btn btn-xs btn-primary" onclick="editLocation('<%# Eval("numWLocationID") %>','<%# Eval("vcAisle") %>','<%# Eval("vcRack") %>','<%# Eval("vcShelf") %>','<%# Eval("vcBin") %>','<%# Eval("vcLocation") %>')">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
