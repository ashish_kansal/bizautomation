<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmShopCartList.aspx.vb"
    Inherits=".Items_frmShopCartList" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shopping Cart Extended Details</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close()
            return false;
        }
        //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html
        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    console.log(img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    var siteID = document.getElementById('hdnSiteID').value;
                    img.setAttribute("src", img.src.substring(img.src.indexOf(siteID)).replace(siteID, ''));
                    console.log(img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
            </asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Shopping Cart Extended Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
        runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <telerik:RadEditor ID="RadEditor1" runat="server" Height="515px" OnClientPasteHtml="OnClientPasteHtml"
                    ToolsFile="~/Marketing/EditorTools.xml" AllowScripts="true">
                    <Content>
                    </Content>
                </telerik:RadEditor>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
