﻿Imports System.Web.Optimization

Public Class BundleConfig
    Public Shared Sub RegisterBundles(ByVal bundles As BundleCollection)

        BundleTable.Bundles.Add(New ScriptBundle("~/bundle/js").Include("~/JavaScript/jquery-1.9.1.min.js",
       "~/JavaScript/bootstrap.min.js",
       "~/JavaScript/biz.min.js",
       "~/JavaScript/jquery.smartmenus.js",
       "~/JavaScript/jquery.slimscroll.min.js",
       "~/JavaScript/jquery.jeditable.js",
       "~/JavaScript/comboClientSide.js",
       "~/JavaScript/jquery.validate.min.js",
       "~/JavaScript/jquery.validate.custom-script.js",
       "~/JavaScript/jquery.metadata.min.js",
       "~/JavaScript/jscal2.js",
       "~/JavaScript/en.js",
        "~/JavaScript/Common.js",
        "~/javascript/CustomFieldValidation.js",
        "~/JavaScript/dateFormat.js"))

        BundleTable.Bundles.Add(New StyleBundle("~/bundle/css").Include("~/CSS/bootstrap.min.css",
        "~/CSS/font-awesome.min.css").Include("~/CSS/biz.css", New CssRewriteUrlTransform()).Include(
        "~/CSS/biz-skin.css",
        "~/CSS/jquery.smartmenus.bootstrap.css",
        "~/CSS/jscal2.css"))
    End Sub
End Class
