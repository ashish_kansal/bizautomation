<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSpecDocuments.aspx.vb"
    Inherits="BACRM.UserInterface.Documents.frmSpecDocuments" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Specific Documents</title>
    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function openFile(url) {
            window.open(url, '', "width=800,height=600,status=no,top=20,left=20,scrollbars=no,resizable=yes");
            return false;
        }
        function OpenDocument(url) {
            window.opener.location.href = url;
            self.close();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td class="normal1">
                <asp:HyperLink ID="hplNew" CssClass="hyperlink" runat="server">
                            <img src="../images/icons/fileopen_large.png" />
							<img src="../images/AddRecord.png" />
                </asp:HyperLink>
            </td>
            <td>
                <div class="input-part">
                    <div class="right-input">
                        <asp:Button ID="btnClose" CssClass="button" runat="server" Width="50" Text="Close"></asp:Button>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Documents
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="0" cellpadding="2" width="1000px">
        <tr>
            <td class="normal1" align="right">Filter by Document Category
            </td>
            <td>
                <asp:DropDownList ID="ddlCategory" AutoPostBack="True" CssClass="signup" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgDocs" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numGenericDocID" HeaderText="numGenericDocID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="vcfiletype" HeaderText="vcfiletype"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="VcFileName" HeaderText="VcFileName"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="Open Document">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" Target="_blank" runat="server">
							<img src="../images/icons/fileopen_small.png" />
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Name / Description">
                            <ItemTemplate>
                                <%#Eval("vcDocName") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="File Type">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplEdit" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcfiletype") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--  <asp:BoundColumn DataField="Category" HeaderText="Document Category"></asp:BoundColumn>
                        <asp:BoundColumn DataField="BusClass" HeaderText="Document Status"></asp:BoundColumn>--%>
                        <asp:TemplateColumn HeaderText="Approved / Declined / Pending">
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rbApproved" runat="server" Checked='<%#IIf(Eval("Approved") = 1, True, False)%>' Enabled="false" Style="padding-left: 15px; padding-right: 25px;" />
                                <asp:RadioButton ID="rbDeclined" runat="server" Checked='<%#IIf(Eval("Declined") = 1, True, False)%>' Enabled="false" Style="padding-left: 10px; padding-right: 25px;" />
                                <asp:RadioButton ID="rbPending" runat="server" Checked='<%#IIf(Eval("Pending") = 1, True, False)%>' Enabled="false" Style="padding-left: 10px;" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Mod" HeaderText="Last Modified By/On"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckedOut" SortExpression="CheckedOut" HeaderText="Last Checked-Out By/On"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
