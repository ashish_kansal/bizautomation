Imports System.Xml
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Documents
    Public Class frmSpecDocsEform
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Dim strURL As String
        Protected WithEvents btnURecord As System.Web.UI.WebControls.Button
        Protected WithEvents btnPRecord As System.Web.UI.WebControls.Button
        Protected WithEvents btnUeForm As System.Web.UI.WebControls.Button
        Protected WithEvents btnPeform As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveToNetk As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveEform As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblDetails As System.Web.UI.WebControls.Table
        Protected WithEvents tbleform As System.Web.UI.WebControls.Table
        Private designerPlaceholderDeclaration As System.Object
        Dim intCount, k As Integer
        Dim tblcell As TableCell
        Dim tblRow As TableRow
        Dim CurrentElement As String
        Protected WithEvents btnSendEmail As System.Web.UI.WebControls.Button
        Protected WithEvents pgBar As System.Web.UI.HtmlControls.HtmlImage
        Dim txt As TextBox
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                 ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Documents"
                pgBar.Style.Add("display", "")
                strURL = GetQueryStringVal( "FileName")
                DisplayDetails()
                btnClose.Attributes.Add("onclick", "return Close()")
                btnUeForm.Attributes.Add("onclick", "return alertUpEform()")
                btnURecord.Attributes.Add("onclick", "return alertUpBizForm()")
                btnPeform.Attributes.Add("onclick", "return alertPeForm()")
                btnPRecord.Attributes.Add("onclick", "return alertPBizForm()")
                pgBar.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDetails()
            Dim reader As XmlTextReader = Nothing
            Try
                reader = New XmlTextReader(Replace(strURL, "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName")))
                reader.WhitespaceHandling = WhitespaceHandling.None

                ' Create an XmlDocument from the XmlTextReader
                Dim myXmlDocument As XmlDocument = New XmlDocument
                myXmlDocument.Load(reader)

                ' Start from the document Element
                DisplayTree(myXmlDocument.DocumentElement)
                If Not tblRow Is Nothing Then tbleform.Rows.Add(tblRow)
            Catch ex As Exception
                'Response.Write("Exception:" & e.ToString())
                Throw ex
            Finally
                If Not reader Is Nothing Then reader.Close()
            End Try
        End Sub

        Public Sub DisplayTree(ByVal node As XmlNode)
            Try
                If Not IsNothing(node) Then Format(node)
                If node.HasChildNodes Then
                    node = node.FirstChild
                    While Not IsNothing(node)
                        DisplayTree(node)
                        node = node.NextSibling
                    End While
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub Format(ByVal node As XmlNode)
            Try
                If Not node.HasChildNodes Then
                    If node.LocalName <> "#text" Then
                        If k = 0 Then tblRow = New TableRow
                        CurrentElement = node.Name
                        tblcell = New TableCell
                        tblcell.CssClass = "normal1"
                        tblcell.HorizontalAlign = HorizontalAlign.Right
                        tblcell.Text = node.LocalName
                        tblRow.Cells.Add(tblcell)

                        tblcell = New TableCell
                        txt = New TextBox
                        txt.CssClass = "signup"
                        txt.ID = intCount
                        tblcell.Controls.Add(txt)
                        tblRow.Cells.Add(tblcell)

                        k = k + 1
                        If k = 3 Then
                            k = 0
                            tbleform.Rows.Add(tblRow)
                        End If
                        intCount = intCount + 1
                    End If
                    'Response.Write(Strings.Chr(9) & node.LocalName & "=" & node.Value)
                Else
                    If node.FirstChild.LocalName <> "#text" Then
                        k = 0
                        If Not tblRow Is Nothing Then tbleform.Rows.Add(tblRow)
                        tblRow = New TableRow

                        tblcell = New TableCell
                        tblcell.CssClass = "normal1"
                        tblcell.Text = "<b>" & node.LocalName & "</b>"
                        tblcell.ColumnSpan = 6
                        tblRow.Cells.Add(tblcell)
                        tbleform.Rows.Add(tblRow)
                        ' Response.Write("<br><b>" & node.LocalName & "</b><br>")
                        CurrentElement = node.Name
                    ElseIf node.FirstChild.LocalName = "#text" Then
                        If k = 0 Then tblRow = New TableRow
                        CurrentElement = node.Name
                        tblcell = New TableCell
                        tblcell.CssClass = "normal1"
                        tblcell.HorizontalAlign = HorizontalAlign.Right
                        tblcell.Text = node.LocalName
                        tblRow.Cells.Add(tblcell)

                        tblcell = New TableCell
                        txt = New TextBox
                        txt.ID = intCount
                        txt.CssClass = "signup"
                        tblcell.Controls.Add(txt)
                        tblRow.Cells.Add(tblcell)
                        txt.Text = node.FirstChild.Value
                        k = k + 1
                        If k = 3 Then
                            k = 0
                            tbleform.Rows.Add(tblRow)
                        End If
                        intCount = intCount + 1
                        'CType(Page.FindControl(CurrentElement), TextBox).Text = node.Value
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveEform_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEform.Click
            Try
                intCount = 0
                readXml()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub readXml()
            Dim reader As XmlTextReader = Nothing
            Try
                reader = New XmlTextReader(Replace(strURL, "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName")))
                reader.WhitespaceHandling = WhitespaceHandling.None

                ' Create an XmlDocument from the XmlTextReader
                Dim myXmlDocument As XmlDocument = New XmlDocument
                myXmlDocument.Load(reader)
                Dim strF As String
                strF = Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now)

                ' Start from the document Element
                FindNodes(myXmlDocument.DocumentElement, myXmlDocument)
                Dim strNewFileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Form" & strF & ".xml"
                Dim objDocuments As New DocumentList
                objDocuments.SpecDocID = GetQueryStringVal( "SpecID")
                objDocuments.FileName = "../documents/docs/Form" & strF & ".xml"
                objDocuments.UpdateSpecDocuments()
                myXmlDocument.Save(strNewFileName)

            Catch ex As Exception
                'Response.Write("Exception:" & e.ToString())
                Throw ex
            Finally
                If Not reader Is Nothing Then reader.Close()
            End Try
        End Sub

        Public Sub FindNodes(ByVal node As XmlNode, ByVal myXmlDocument As XmlDocument)
            Try
                If Not IsNothing(node) Then SaveXML(node, myXmlDocument)
                If node.HasChildNodes Then
                    node = node.FirstChild
                    While Not IsNothing(node)
                        FindNodes(node, myXmlDocument)
                        node = node.NextSibling
                    End While
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveXML(ByVal node As XmlNode, ByVal myXmlDocument As XmlDocument)
            Try
                If Not node.HasChildNodes Then
                    If node.LocalName <> "#text" Then
                        Dim n As XmlNode = myXmlDocument.CreateNode(XmlNodeType.Text, "", "#text")
                        n.Value = CType(Page.FindControl(intCount), TextBox).Text
                        node.AppendChild(n)
                        intCount = intCount + 1
                    End If
                Else
                    If node.FirstChild.LocalName = "#text" Then
                        node.FirstChild.Value = CType(Page.FindControl(intCount), TextBox).Text
                        intCount = intCount + 1
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnPeform_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPeform.Click
            Try
                Dim dtEformConiguration As DataTable
                Dim dtEformDetails As DataTable
                Dim dtCustomFields As DataTable
                dtCustomFields = Session("CusFields")
                Dim i As Integer
                Dim str As String
                Dim objDocuments As New DocumentList
                objDocuments.SpecDocID = GetQueryStringVal( "SpecID")
                dtEformConiguration = objDocuments.GetEformConfiguration
                If dtEformConiguration.Rows.Count > 0 Then
                    For i = 0 To dtEformConiguration.Rows.Count - 1
                        If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                            str = str + dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) + ","
                        Else : str = str + "'0',"
                        End If
                    Next
                    objDocuments.strSQL = str.TrimEnd(",")
                    objDocuments.byteMode = 0
                    dtEformDetails = objDocuments.GetEformDetails
                    For i = 0 To dtEformConiguration.Rows.Count - 1
                        If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                            If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 1 Then
                                CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = dtEformDetails.Rows(0).Item(i)
                            ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 2 Then
                                If dtEformConiguration.Rows(i).Item("vcColumnName") = "numManagerID~2" Then
                                    CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = GetManager(dtEformDetails.Rows(0).Item(i))
                                Else : CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = GetListName(dtEformDetails.Rows(0).Item(i))
                                End If
                            End If
                        Else
                            If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 1 Then
                                For k = 0 To dtCustomFields.Rows.Count - 1
                                    If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                        CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = dtCustomFields.Rows(k).Item("Value")
                                    End If
                                Next
                            ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 2 Then
                                For k = 0 To dtCustomFields.Rows.Count - 1
                                    If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                        CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = GetListName(dtCustomFields.Rows(k).Item("Value"))
                                    End If
                                Next

                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function GetManager(ByVal MangerID As Integer) As String
            Try
                Dim objDocuments As New DocumentList
                objDocuments.ManagerID = MangerID
                Return objDocuments.GetManager
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetListName(ByVal Listitem As Integer) As String
            Try
                Dim objDocuments As New DocumentList
                objDocuments.ListItemID = Listitem
                Return objDocuments.GetListItem
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetListNameCustomFields(ByVal Listitem As Integer) As String
            Try
                Dim objDocuments As New DocumentList
                objDocuments.ListItemID = Listitem
                Return objDocuments.GetListItem
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnUeForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUeForm.Click
            Try
                Dim dtEformConiguration As DataTable
                Dim dtEformDetails As DataTable
                Dim dtCustomFields As DataTable
                dtCustomFields = Session("CusFields")
                Dim i As Integer
                Dim str As String
                Dim objDocuments As New DocumentList
                objDocuments.SpecDocID = GetQueryStringVal( "SpecID")
                dtEformConiguration = objDocuments.GetEformConfiguration
                If dtEformConiguration.Rows.Count > 0 Then
                    For i = 0 To dtEformConiguration.Rows.Count - 1
                        If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                            str = str + dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) + ","
                        Else : str = str + "'0',"
                        End If
                    Next
                    objDocuments.strSQL = str.TrimEnd(",")
                    objDocuments.byteMode = 0
                    dtEformDetails = objDocuments.GetEformDetails
                    For i = 0 To dtEformDetails.Columns.Count - 1
                        If CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "" Then
                            If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                                If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 1 Then
                                    CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = dtEformDetails.Rows(0).Item(i)
                                ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 2 Then
                                    If dtEformConiguration.Rows(i).Item("vcColumnName") = "numManagerID~2" Then
                                        CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = GetManager(dtEformDetails.Rows(0).Item(i))
                                    Else : CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = GetListName(dtEformDetails.Rows(0).Item(i))
                                    End If
                                End If
                            Else
                                If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 1 Then
                                    For k = 0 To dtCustomFields.Rows.Count - 1
                                        If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                            CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = dtCustomFields.Rows(k).Item("Value")
                                        End If
                                    Next
                                ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 2 Then
                                    For k = 0 To dtCustomFields.Rows.Count - 1
                                        If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                            CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = GetListName(dtCustomFields.Rows(k).Item("Value"))
                                        End If
                                    Next
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnPRecord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPRecord.Click
            Try
                Dim dtEformConiguration As DataTable
                Dim dtEformDetails As DataTable
                Dim dtCustomFields As DataTable
                dtCustomFields = Session("CusFields")
                Dim i As Integer
                Dim str As String
                Dim objDocuments As New DocumentList
                objDocuments.SpecDocID = GetQueryStringVal( "SpecID")
                dtEformConiguration = objDocuments.GetEformConfiguration
                If dtEformConiguration.Rows.Count > 0 Then
                    For i = 0 To dtEformConiguration.Rows.Count - 1
                        If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                            If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = "1" Then
                                If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                                    str = str & dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) & "=" & IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", "'" & CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text.ToString) & "',"
                                End If
                            ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = "2" Then
                                'str = str & dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) & "=" & IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", "'" & CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text) & "'" & ","
                                objDocuments.strSQL = IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text)
                                objDocuments.columnName = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0)
                                objDocuments.byteMode = 2
                                objDocuments.GetEformDetails()
                            End If
                        Else
                            If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 1 Then
                                For k = 0 To dtCustomFields.Rows.Count - 1
                                    If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                        objDocuments.strSQL = CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text
                                        objDocuments.columnName = dtCustomFields.Rows(k).Item("fld_id")
                                        objDocuments.byteMode = 4
                                        objDocuments.GetEformDetails()
                                    End If
                                Next
                            ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 2 Then
                                For k = 0 To dtCustomFields.Rows.Count - 1
                                    If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                        objDocuments.strSQL = CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text
                                        objDocuments.columnName = dtCustomFields.Rows(k).Item("fld_id")
                                        objDocuments.byteMode = 5
                                        objDocuments.GetEformDetails()
                                    End If
                                Next
                            End If
                        End If
                    Next
                    If str <> "" Then
                        objDocuments.strSQL = str.TrimEnd(",")
                        objDocuments.byteMode = 1
                        objDocuments.GetEformDetails()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnURecord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnURecord.Click
            Try
                Dim dtEformConiguration As DataTable
                Dim dtEformDetails As DataTable
                Dim dtCustomFields As DataTable
                dtCustomFields = Session("CusFields")
                Dim i As Integer
                Dim str As String
                Dim objDocuments As New DocumentList
                objDocuments.SpecDocID = GetQueryStringVal( "SpecID")
                dtEformConiguration = objDocuments.GetEformConfiguration
                If dtEformConiguration.Rows.Count > 0 Then
                    For i = 0 To dtEformConiguration.Rows.Count - 1
                        If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then
                            str = str + dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) + ","
                        Else : str = str + "'0',"
                        End If
                    Next
                    objDocuments.strSQL = str.TrimEnd(",")
                    objDocuments.byteMode = 0
                    dtEformDetails = objDocuments.GetEformDetails
                    str = ""
                    For i = 0 To dtEformConiguration.Rows.Count - 1
                        If Not dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) = "CustomFields" Then

                            If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = "1" Then
                                If Not IsDBNull(dtEformDetails.Rows(0).Item(i)) Then
                                    If dtEformDetails.Rows(0).Item(i) = "" Then
                                        str = str & dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) & "=" & IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", "'" & CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text.ToString) & "',"
                                    End If
                                Else : str = str & dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0) & "=" & IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", "'" & CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text.ToString) & "',"
                                End If
                            ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = "2" Then
                                If Not IsDBNull(dtEformDetails.Rows(0).Item(i)) Then
                                    If dtEformDetails.Rows(0).Item(i) = 0 Then
                                        objDocuments.strSQL = IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text)
                                        objDocuments.columnName = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0)
                                        objDocuments.byteMode = 3
                                        objDocuments.GetEformDetails()
                                    End If
                                Else
                                    objDocuments.strSQL = IIf(CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text = "", "''", CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text)
                                    objDocuments.columnName = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(0)
                                    objDocuments.byteMode = 3
                                    objDocuments.GetEformDetails()
                                End If
                            End If
                        Else
                            If dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 1 Then
                                For k = 0 To dtCustomFields.Rows.Count - 1
                                    If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                        objDocuments.strSQL = CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text
                                        objDocuments.columnName = dtCustomFields.Rows(k).Item("fld_id")
                                        objDocuments.byteMode = 6
                                        objDocuments.GetEformDetails()
                                    End If
                                Next
                            ElseIf dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(1) = 2 Then
                                For k = 0 To dtCustomFields.Rows.Count - 1
                                    If dtCustomFields.Rows(k).Item("fld_label") = dtEformConiguration.Rows(i).Item("vcColumnName").Split("~")(2) Then
                                        objDocuments.strSQL = CType(Page.FindControl(dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")(1)), TextBox).Text
                                        objDocuments.columnName = dtCustomFields.Rows(k).Item("fld_id")
                                        objDocuments.byteMode = 7
                                        objDocuments.GetEformDetails()
                                    End If
                                Next

                            End If
                        End If
                    Next
                    If str <> "" Then
                        objDocuments.strSQL = str.TrimEnd(",")
                        objDocuments.byteMode = 1
                        objDocuments.GetEformDetails()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
