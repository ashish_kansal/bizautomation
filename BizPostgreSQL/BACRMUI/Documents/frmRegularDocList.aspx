﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRegularDocList.aspx.vb"
    Inherits="BACRM.UserInterface.Documents.frmRegularDocList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Documents List</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $("#txtSearch").select2({
                placeholder: 'Search Template',
                multiple: false,
                width: "180px",
                dropdownCssClass: 'bigdropdoclist',
                minimumInputLength: 1,
                dataType: "json",
                allowClear: true,
                formatResult: formatItem,
                ajax: {
                    quietMillis: 500,
                    url: '../common/Common.asmx/SearchGenericDocuments',
                    type: 'POST',
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    dataType: 'json',
                    data: function (term, page) {
                        return JSON.stringify({
                            searchText: term,
                            category: parseInt($("[id$=hdnCategoty]").val()),
                            documentType: $('input[name$="TemplateType"]:checked').attr("Value"),
                            pageIndex: page,
                            pageSize: 10
                        });
                    },
                    results: function (data, page) {
                        if (data.hasOwnProperty("d")) {
                            if (data.d == "Session Expired") {
                                alert("Session expired.");
                            } else {
                                data = $.parseJSON(data.d)
                            }
                        }
                        else
                            data = $.parseJSON(data);

                        return { results: $.parseJSON(data.results), more: ((page.page * 10) < data.Total) };
                    }
                }
            });

            if($("[id$=hdnSelectedValue]").val() != ""){
                $('#txtSearch').select2("data", JSON.parse($("[id$=hdnSelectedValue]").val()));
            }

            $("#txtSearch").on("change", function (e) {
                if (e.added != null) {
                    $("[id$=hdnSelectedValue]").val(JSON.stringify(e.added));
                    $("[id$=hdnGenDocID]").val(e.added.id);
                    $("[id$=btnGo]").click();
                }
            });

            $("#txtSearch").on("select2-removed", function (e) {
                $("[id$=hdnSelectedValue]").val("");
                $("[id$=hdnGenDocID]").val("0");
                $("[id$=btnGo]").click();
            });

            $("[id$=hplClearGridCondition]").click(function () {
                $('#txtSearch').select2("data", null);
                $("#rbAll").attr("checked", "checked");
                $("[id$=hdnSelectedValue]").val("");
                $("[id$=hdnGenDocID]").val("0");
                $("[id$=btnGo]").click();
            });
        }
        function formatItem(result) {
            return '<div class="row">' +
              '<div class="col-md-6">' + result.text + '</div>' +
              '<div class="col-md-6">' + result.vcModuleName + '</div>' +
              '</div>';
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteTemplate() {
            alert("Default Email template can not be deleted !");
            return false;
        }
        function OpenET() {
            window.open('../Marketing/frmEmailTemplate.aspx', '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
        }
        function reLoad() {
            document.getElementById("btnGo1").click();
        }
    </script>
    <style type="text/css">
        .liDocSearch .select2-container {
            margin-top:0px;
        }

        .liDocSearch .select2-container .select2-choice {
            border-radius:0px;
            height: 28px;
        }

        .bigdropdoclist {
            width:90% !important;
            max-width:780px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">

    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-inline">
                    <li class="liDocSearch">
                        <asp:UpdatePanel runat="server" ID="uplSearchPanel">
                            <ContentTemplate>
                                <input type="text" id="txtSearch" />
                                <asp:HiddenField ID="hdnSelectedValue" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:HiddenField ID="hdnGenDocID" runat="server" />
                        <asp:HiddenField ID="hdnCategoty" runat="server" />
                    </li>
                    <li style="vertical-align:middle">
                        <asp:RadioButton ID="rbAll" runat="server" Text="All Templates" GroupName="TemplateType" Value="0" Checked="true" OnCheckedChanged="rbAll_CheckedChanged" AutoPostBack="true"></asp:RadioButton></li>
                    <li style="vertical-align:middle">
                        <asp:RadioButton ID="rbCustom" runat="server" Text="Custom Templates" GroupName="TemplateType" Value="1" OnCheckedChanged="rbCustom_CheckedChanged" AutoPostBack="true"></asp:RadioButton></li>
                    <li style="vertical-align:middle">
                        <asp:RadioButton ID="rbDefault" runat="server" Text="Default Templates" GroupName="TemplateType"  Value="2" OnCheckedChanged="rbDefault_CheckedChanged" AutoPostBack="true"></asp:RadioButton></li>
                </ul>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnGo" runat="server" style="display:none" OnClick="btnGo_Click" />
                <asp:HyperLink ID="hplNew" Visible="false" runat="server" CssClass="btn btn-primary">Create Email Template</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Document Templates&nbsp;<a href="#" onclick="return OpenHelpPopUp('documents/frmregulardoclist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        ShowPageIndexBox="Never"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgDocs" runat="server" Width="100%" CssClass="table table-bordered table-striped" AllowSorting="True"
                    AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numGenericDocID" HeaderText="numGenericDocID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numDocCategory" HeaderText="numDocCategory"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="VcFileName" HeaderText="VcFileName"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcDocName" SortExpression="vcDocName" HeaderText="<font>Document Name</font>" CommandName="Name"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="Category" SortExpression="Category" HeaderText="<font>Document Category</font>"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="<font>File Type</font>">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcfiletype") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="BusClass" SortExpression="BusClass" HeaderText="<font>Document Status</font>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Mod" SortExpression="Mod" HeaderText="<font>Last Modified By, Last Modified On</font>"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
