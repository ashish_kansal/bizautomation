<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddSpecificDocuments.aspx.vb"
    Inherits="BACRM.UserInterface.Documents.frmAddSpecificDocuments" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add Specific Documents</title>
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <link rel="stylesheet" href="../CSS/select2.css" />
    <script language="javascript">
        function openApp(a, b, c) {
            window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&RecID=' + c + "&DocName=" + document.form1.txtDocName.value, '', 'toolbar=no,titlebar=no,left=300,top=450,width=800,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function Save() {
            if (document.getElementById("radUpload").checked || document.getElementById("radUploadExternal").checked) {
                if (document.form1.txtDocName.value == "") {
                    alert("Enter Name")
                    document.form1.txtDocName.focus()
                    return false;
                }
                if (document.form1.ddlCategory.value == 0) {
                    alert("Select Document Category")
                    document.form1.ddlCategory.focus()
                    return false;
                }
            }
        }


        $(function () {
            onLoad();

            $("input[name='ctl00$Content$rad']").change(function () { onLoad(); });

            function onLoad() {
                if (document.getElementById("radUpload").checked || document.getElementById("radUploadExternal").checked) {
                    $('#tblDocument').show();
                }
                else {
                    $('#tblDocument').hide();
                }
            }

            var columns;
            var userDefaultPageSize = '<%= Session("PagingRows")%>';
            var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
            var varPageSize = parseInt(userDefaultPageSize, 10);
            var varCharSearch = 1;
            if (parseInt(userDefaultCharSearch, 10) > 0) {
                varCharSearch = parseInt(userDefaultCharSearch, 10);
            }

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

            function formatItem(row) {
                var numOfColumns = 0;
                var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
                if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                    ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
                } else {
                    ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
                }

                ui = ui + "</td>";
                ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
                ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";

                $.each(columns, function (index, column) {
                    if (numOfColumns == 4) {
                        ui = ui + "</tr><tr>";
                        numOfColumns = 0;
                    }

                    if (numOfColumns == 0) {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    else {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    numOfColumns += 2;
                });
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";
                ui = ui + "</td>";
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";


                return ui;
            }

            $('#txtItem').on("change", function (e) {
                if (e.added != null) {
                    $('#hdnTempSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));
                    $("#hdnTempSelectedItemWarehouse").val(e.added.numWareHouseItemID);
                    $("#hdnCurrentSelectedItem").val(e.added.id.toString().split("-")[0]);
                    document.getElementById("lkbItemSelected").click();
                }

            })


            $('#txtItem').on("select2-removed", function (e) {
                $("#hdnCurrentSelectedItem").val("");
                document.getElementById("lkbItemRemoved").click();
            })

            $('#txtItem').select2(
            {
                placeholder: 'Select Items',
                minimumInputLength: varCharSearch,
                multiple: false,
                formatResult: formatItem,
                width: "550px",
                dataType: "json",
                allowClear: true,
                ajax: {
                    quietMillis: 500,
                    url: '../common/Common.asmx/GetSearchedItems',
                    type: 'POST',
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    dataType: 'json',
                    data: function (term, page) {
                        return JSON.stringify({
                            searchText: term,
                            pageIndex: page,
                            pageSize: varPageSize,
                            divisionId: 0,
                            isGetDefaultColumn: false,
                            warehouseID: 0,
                            searchOrdCusHistory: 0,
                            oppType: 1,
                            isTransferTo: false,
                            IsCustomerPartSearch: false,
                            searchType: "1"
                        });
                    },
                    results: function (data, page) {

                        if (data.hasOwnProperty("d"))
                            data = $.parseJSON(data.d)
                        else
                            data = $.parseJSON(data);

                        var more = (page.page * varPageSize) < data.Total;
                        return { results: $.parseJSON(data.results), more: more };
                    }
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"></asp:Button>
                        <asp:Button ID="btnSaveCLose" Text="Save &amp; Close" runat="server" CssClass="button"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Documents
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="1000px" align="center">
        <tr id="trOrganization" runat="server" visible="false">
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radUploadOrganization" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/Building-16.gif&quot;/&gt; Organization"></asp:RadioButton>
                <%--<asp:DropDownList ID="ddlOrganizationDocuments" CssClass="signup" runat="server" Width="130">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>--%>
                &nbsp;&nbsp;<telerik:RadComboBox ID="radOrganizationDocuments" runat="server" CheckBoxes="true">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr id="trItem" runat="server">
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radUploadItem" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/tag.png&quot;/&gt; Upload from the Item" style="float:left;"></asp:RadioButton>
                <div style="float:left;">
                <asp:UpdatePanel ID="UpdatePanelItem" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                       &nbsp; <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server"></asp:TextBox>
                        <%--<asp:DropDownList ID="ddlItemDocuments" CssClass="signup" runat="server" Width="130">
                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                        </asp:DropDownList>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
                    </div>
                <div style="float:left;">
                <asp:UpdatePanel ID="UpdatePanelItemDetials" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="lkbItemSelected" />
                        <asp:AsyncPostBackTrigger ControlID="lkbItemRemoved" />
                    </Triggers>
                    <ContentTemplate>
                        &nbsp;<telerik:RadComboBox ID="radItemDocuments" runat="server" CheckBoxes="true">
                        </telerik:RadComboBox>
                        <asp:LinkButton ID="lkbItemSelected" runat="server" Style="display: none"></asp:LinkButton>
                        <asp:LinkButton ID="lkbItemRemoved" runat="server" Style="display: none"></asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
                    </div>
                <asp:HiddenField ID="hdnTempSelectedItems" runat="server" />
                <asp:HiddenField ID="hdnTempSelectedItemWarehouse" runat="server" />
                <asp:HiddenField ID="hdnCurrentSelectedItem" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radDocumentLib" AutoPostBack="false" Checked="true" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/tf_note.gif&quot;/&gt; Upload from the Document Library"></asp:RadioButton>

                Select Category
                <asp:DropDownList ID="ddldocumentsCtgr" AutoPostBack="true" CssClass="signup" runat="server"
                    Width="130">
                </asp:DropDownList>
                Select a document from library
                <asp:DropDownList ID="ddldocuments" CssClass="signup" runat="server" Width="130">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding: 10px;" colspan="2">
                <asp:RadioButton ID="radUpload" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/icons/drive.png&quot;/&gt; Upload a Document"></asp:RadioButton>
                <input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding: 10px; vertical-align: top;" colspan="2">
                <asp:RadioButton ID="radUploadExternal" AutoPostBack="false" Checked="false" runat="server" Font-Bold="true"
                    CssClass="normal1" GroupName="rad" Text="&lt;img src=&quot;../images/icons/googleDrive.png&quot;/&gt; Path/URL to Document"></asp:RadioButton>
                <asp:TextBox ID="txtPath" runat="server" Width="250" TextMode="MultiLine" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table id="tblDocument">
        <tr>
            <td class="normal1" align="right">Document Name<font color="red">*</font>
            </td>
            <td>
                <asp:TextBox ID="txtDocName" runat="server" CssClass="signup" Width="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Document Category<font color="red">*</font>
            </td>
            <td colspan="3">
                <asp:DropDownList ID="ddlCategory" TabIndex="3" CssClass="signup" runat="server"
                    Width="200">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" valign="top" align="right">Document Status
            </td>
            <td valign="top" colspan="3">
                <asp:DropDownList ID="ddlClass" TabIndex="3" CssClass="signup" runat="server" Width="200">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Description
            </td>
            <td>
                <asp:TextBox ID="txtDesc" runat="server" Width="400" TextMode="MultiLine" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
