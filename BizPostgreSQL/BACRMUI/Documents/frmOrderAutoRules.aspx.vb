﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Partial Public Class frmOrderAutoRules
    Inherits BACRMPage
    Public objCommon As New CCommon
    Public objOrderRule As New OrderAutoRules
    Public objExhibit2 As New DataTable
    Public objTempDt As New DataTable
    Dim WithEvents objDeleteButton As New Button
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim blnQuery As Boolean
        blnQuery = False
        If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access

            objCommon = New CCommon
            GetUserRightsForPage(13, 28)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If
            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
        End If
        If (Not Session("DomainID") Is Nothing) Then

            If Not IsPostBack Then
                ddlBizDocStatus.Enabled = False
                ddlBizDocStatus1.Enabled = False
                hdRuleID.Value = 0
                ListBillShipp()
                objCommon.sb_FillComboFromDBwithSel(ddlItemClass, 36, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus1, 11, Session("DomainID"))

                With objOrderRule
                    .DomainId = Session("DomainID")
                    .RuleID = 0
                    .GetOrderRule()
                    'If > 0 Then
                    hdRuleID.Value = .RuleID
                    ddlBillTo1.Items.FindByValue(.BillToID).Selected = True
                    ddlShipTo1.Items.FindByValue(.ShipToID).Selected = True
                    ddlBizDocStatus1.Items.FindByValue(.BizDocStatus).Selected = True
                    'ddlBillTo1.SelectedItem.Text = .BillToText
                    'ddlBillTo1.SelectedItem.Value = .BillToID
                    'ddlShipTo1.SelectedItem.Text = .ShipToText
                    'ddlShipTo1.SelectedItem.Value = .ShipToID
                    'ddlBizDocStatus1.SelectedItem.Text = .BizDocStatusText
                    'ddlBizDocStatus1.SelectedItem.Value = .BizDocStatus
                    If .FullPaid = "Yes" Then
                        chkAmount1.Checked = True
                    Else
                        chkAmount1.Checked = False
                    End If

                    If .BizDocStatus = 0 Then
                        ddlBizDocStatus1.Enabled = False
                        chkBizDoc1.Checked = False
                    Else
                        ddlBizDocStatus1.Enabled = True
                        chkBizDoc1.Checked = True
                    End If
                    chkRule1.Checked = .Active
                    'End If
                    CreateExhibit2Grid(.RuleID)
                End With
                blnQuery = True

            End If

        End If

        If Not IsPostBack Then
            If blnQuery = False Then
                ddlBizDocStatus.Enabled = False
                ddlBizDocStatus1.Enabled = False
                hdRuleID.Value = 0
                ListBillShipp()
                objCommon.sb_FillComboFromDBwithSel(ddlItemClass, 36, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus1, 11, Session("DomainID"))
                CreateExhibit2Grid(-1)
            End If
            Session("TempOrderRuleData") = objExhibit2
            dgReport.DataSource = Session("TempOrderRuleData")
            dgReport.DataBind()
        End If

    End Sub
    Private Sub CreateExhibit2Grid(ByVal intRuleID As Integer)
        Try
            objOrderRule.DomainId = Session("DomainID")
            objOrderRule.RuleID = intRuleID
            objExhibit2 = objOrderRule.GetOrderRuleDetails
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("Domai nID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub ListBillShipp()
        Try
            Dim dtBillShipp As New DataTable
            Dim dbCol As New DataColumn
            Dim objRow As DataRow


            dbCol.ColumnName = "BillTypID"
            dbCol.DataType = GetType(Integer)
            dtBillShipp.Columns.Add(dbCol)


            dbCol = New DataColumn

            dbCol.ColumnName = "BillType"
            dbCol.DataType = GetType(String)

            dtBillShipp.Columns.Add(dbCol)


            objRow = dtBillShipp.NewRow

            objRow.Item("BillTypID") = -1
            objRow.Item("BillType") = "-Select One--"

            dtBillShipp.Rows.Add(objRow)


            objRow = dtBillShipp.NewRow

            objRow.Item("BillTypID") = 0
            objRow.Item("BillType") = "Employer"

            dtBillShipp.Rows.Add(objRow)

            objRow = dtBillShipp.NewRow

            objRow.Item("BillTypID") = 1
            objRow.Item("BillType") = "Customer"

            dtBillShipp.Rows.Add(objRow)

            objRow = dtBillShipp.NewRow

            objRow.Item("BillTypID") = 2
            objRow.Item("BillType") = "Other"

            dtBillShipp.Rows.Add(objRow)




            ddlBillTo1.DataSource = dtBillShipp
            ddlBillTo1.DataValueField = "BillTypID"
            ddlBillTo1.DataTextField = "BillType"
            ddlBillTo1.DataBind()

            ddlBillTo2.DataSource = dtBillShipp
            ddlBillTo2.DataValueField = "BillTypID"
            ddlBillTo2.DataTextField = "BillType"
            ddlBillTo2.DataBind()

            ddlShipTo1.DataSource = dtBillShipp
            ddlShipTo1.DataValueField = "BillTypID"
            ddlShipTo1.DataTextField = "BillType"
            ddlShipTo1.DataBind()

            ddlShipTo2.DataSource = dtBillShipp
            ddlShipTo2.DataValueField = "BillTypID"
            ddlShipTo2.DataTextField = "BillType"
            ddlShipTo2.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try

            'CreateExhibit2Grid()


            objTempDt = New DataTable
            objTempDt = Session("TempOrderRuleData")

            If ddlItemType.SelectedIndex = 0 Then
                litMessage.Text = "Select An Item Type"
                Exit Sub
            End If

            If ddlItemClass.SelectedIndex = 0 Then
                litMessage.Text = "Select An Item Classification"
                Exit Sub
            End If

            If ddlBillTo2.SelectedIndex = 0 Then
                litMessage.Text = "Select A Bill To Type"
                Exit Sub
            End If

            If ddlShipTo2.SelectedIndex = 0 Then
                litMessage.Text = "Select A Ship To Type"
                Exit Sub
            End If


            If objOrderRule.ValidationRules(objTempDt, ddlItemType.SelectedItem.Value, ddlItemClass.SelectedItem.Value) = True Then

                Session("TempOrderRuleData") = objOrderRule.AddEditGrid(objTempDt, 1, ddlItemType.SelectedItem.Text, ddlItemClass.SelectedItem.Text, ddlBillTo2.SelectedItem.Text, _
                                         ddlShipTo2.SelectedItem.Text, IIf(chkAmount.Checked = True, "Yes", "No"), IIf(ddlBizDocStatus.SelectedIndex = 0, "", ddlBizDocStatus.SelectedItem.Text), _
                                         ddlItemType.SelectedItem.Value, ddlItemClass.SelectedItem.Value, ddlBillTo2.SelectedItem.Value, ddlShipTo2.SelectedItem.Value, _
                                         IIf(chkAmount.Checked = True, 1, 0), ddlBizDocStatus.SelectedItem.Value, IIf(chkRule2.Checked = True, 1, 0))

                ddlBillTo2.SelectedIndex = 0
                ddlShipTo2.SelectedIndex = 0
                ddlBizDocStatus.SelectedIndex = 0
                chkAmount.Checked = False
                chkBizDoc.Checked = False
                ddlBizDocStatus.Enabled = False
                ddlItemType.SelectedIndex = 0
                ddlItemClass.SelectedIndex = 0
                ddlItemType.Focus()

            Else
                'MsgBox("Item Type And Item Classification Already Exist", MsgBoxStyle.Information)
                litMessage.Text = "Item Type And Item Classification Already Exist."
            End If

            dgReport.DataSource = Session("TempOrderRuleData")
            dgReport.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dgReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReport.ItemCommand
        Try
            Dim objRow() As DataRow
            Dim objOrderRule As New OrderAutoRules

            If e.CommandName = "Delete" Then
                objTempDt = Session("TempOrderRuleData")

                'objRow = objTempDt.Select("numItemTypeID=" & e.Item.Cells(6).Text & " AND numItemClassID=" & e.Item.Cells(7).Text)
                objRow = objTempDt.Select("numItemTypeID=" & e.Item.Cells(7).Text & " AND numItemClassID=" & e.Item.Cells(8).Text)
                Dim intLoop As Integer
                For intLoop = 0 To UBound(objRow)

                    If objOrderRule.DeleteRuleDetailsExhit2(CType(objRow(intLoop).Item("numRuleDetailsId"), Integer)) = 1 Then
                        objTempDt.Rows.Remove(objRow(intLoop))
                        objTempDt.AcceptChanges()
                    End If

                Next

                Session("TempOrderRuleData") = objTempDt
                dgReport.DataSource = Session("TempOrderRuleData")
                dgReport.DataBind()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            objOrderRule.DomainId = Session("DomainID")
            objOrderRule.RuleID = hdRuleID.Value
            objOrderRule.AmountPaid = IIf(chkAmount1.Checked = True, 1, 0)
            objOrderRule.BizDocStatus = ddlBizDocStatus1.SelectedItem.Value
            objOrderRule.BillToID = ddlBillTo1.SelectedItem.Value
            objOrderRule.ShipToID = ddlShipTo1.SelectedItem.Value
            objTempDt = Session("TempOrderRuleData")

            Dim objGItem As DataGridItem
            For Each objGItem In dgReport.Items
                If CType(objGItem.FindControl("chkActive"), CheckBox).Checked = True Then
                    objTempDt.Rows(objGItem.ItemIndex).Item("btActive") = 1
                ElseIf CType(objGItem.FindControl("chkActive"), CheckBox).Checked = False Then
                    objTempDt.Rows(objGItem.ItemIndex).Item("btActive") = 0
                End If
            Next
            objTempDt.AcceptChanges()
            objOrderRule.DtRule = Session("TempOrderRuleData")
            objOrderRule.ModeID = IIf(hdRuleID.Value = 0, 0, 1)
            objOrderRule.Active = chkRule1.Checked

            hdRuleID.Value = objOrderRule.SaveRules
            If hdRuleID.Value <> -1 Then
                litMessage.Text = "Order Automation Rules Saved Successfully"
            Else
                litMessage.Text = "Order Automation Rules Not Saved Successfully"
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            objOrderRule.DomainId = Session("DomainID")
            objOrderRule.RuleID = hdRuleID.Value
            objOrderRule.BillToID = ddlBillTo1.SelectedItem.Value
            objOrderRule.ShipToID = ddlShipTo1.SelectedItem.Value
            objOrderRule.DtRule = Session("TempOrderRuleData")
            objOrderRule.ModeID = 2

            If objOrderRule.SaveRules <> -1 Then
                litMessage.Text = "Order Automation Rules Deleted Successfully"
                CreateExhibit2Grid(-1)
                Session("TempOrderRuleData") = objExhibit2
                dgReport.DataSource = Session("TempOrderRuleData")
                dgReport.DataBind()
                ddlBillTo1.SelectedIndex = 0
                ddlShipTo2.SelectedIndex = 0
                ddlBizDocStatus1.SelectedIndex = 0
                chkAmount1.Checked = False
                chkBizDoc1.Checked = False
            Else
                litMessage.Text = "Order Automation Rules Not Deleted Successfully"
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub chkBizDoc_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkBizDoc.CheckedChanged
        If chkBizDoc.Checked = True Then
            ddlBizDocStatus.Enabled = True
        Else
            ddlBizDocStatus.Enabled = False
        End If
    End Sub
    Protected Sub chkBizDoc1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkBizDoc1.CheckedChanged
        If chkBizDoc1.Checked = True Then
            ddlBizDocStatus1.Enabled = True
        Else
            ddlBizDocStatus1.Enabled = False
        End If
    End Sub

    Private Sub dgReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReport.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim chkActive As CheckBox
            chkActive = e.Item.FindControl("chkActive")
            'objRow = objTempDt.Select("numBizDocTypeID=" & e.Item.Cells(9).Text & " AND EmployeeID='" & e.Item.Cells(13).Text & "' AND Range='" & e.Item.Cells(1).Text & "'")
            If e.Item.Cells(15).Text = "True" Then
                chkActive.Checked = True
            ElseIf e.Item.Cells(15).Text = "False" Then
                chkActive.Checked = False
            End If
        End If
    End Sub
End Class