﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrderAutoRules.aspx.vb"
    Inherits=".frmOrderAutoRules" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Order Automation Rule</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table id="Table1" height="2" cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="59px">
                        </asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <br />
    <table id="Table4" height="10" cellspacing="0" cellpadding="0" border="0" width="100%"
        bordercolor="black">
        <tr align="center" class="normal1">
            <td class="normal1">
                <asp:HiddenField ID="hdRuleID" runat="server" Value="0" />
            </td>
        </tr>
        <tr align="center" class="normal1">
            <td class="normal4" valign="middle">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Order Automation Rules
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div>
        <br />
        <asp:Table ID="tblDocRule" runat="server" GridLines="None" BorderColor="black" Width="100%"
            Height="100" CssClass="aspTable" BorderWidth="1">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table id="Table2" height="10" cellspacing="0" cellpadding="0" border="0" width="100%"
                        bordercolor="black" class="aspTable">
                        <tr>
                            <td class="aspTable">
                                <table cellspacing="2" cellpadding="0" border="0" style="width: 100%" bordercolor="black">
                                    <tr class="normal1">
                                        <td>
                                            <asp:CheckBox ID="chkRule1" runat="server" Text="" />If “drop-ship” is checked for
                                            an inventory item when creating new Authoritative Sales BizDoc then auto-create
                                            matching Purchase Orders for primary vendors where:
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td>
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        Bill-to is :
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="signup" ID="ddlBillTo1" runat="server" Width="200">
                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        and Ship-to is
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="signup" ID="ddlShipTo1" runat="server" Width="200" AutoPostBack="True">
                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td align="left">
                                            <asp:CheckBox ID="chkAmount1" runat="server" Text="ONLY" />
                                            IF Amount is paid in full.
                                        </td>
                                    </tr>
                                    <tr class="normal1">
                                        <td align="left">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:CheckBox ID="chkBizDoc1" runat="server" Text="ONLY" AutoPostBack="True" />
                                                        Authoritative Sales BizDoc Status is set to
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="signup" ID="ddlBizDocStatus1" runat="server" Width="200">
                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table ID="tblDocStatus" runat="server" GridLines="None" Width="100%" Height="300"
            CssClass="aspTableDTL" BorderWidth="1">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table id="Table3" height="10" cellspacing="0" cellpadding="0" border="0" width="100%"
                        class="aspTableDTL">
                        <tr>
                            <td class="aspTable">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkRule2" runat="server" Text="" />
                                                        If item is a
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlItemType" runat="server" CssClass="signup" Width="200px">
                                                            <asp:ListItem Text="-Select One--" Value="21"></asp:ListItem>
                                                            <asp:ListItem Value="1">Services</asp:ListItem>
                                                            <asp:ListItem Value="2">Inventory Items</asp:ListItem>
                                                            <asp:ListItem Value="3">Non-Inventory Items</asp:ListItem>
                                                            <%--<asp:ListItem Value="4">Serialized Items</asp:ListItem>
                                                <asp:ListItem Value="5">Kits</asp:ListItem>
                                                <asp:ListItem Value="6">Assemblies</asp:ListItem>
                                                <asp:ListItem Value="7">Matrix Items</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        where item classification is
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="signup" ID="ddlItemClass" runat="server" Width="200">
                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        then WHEN it&#39;s added to an Authoritative Sales BizDoc,
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="normal1" align="left" colspan="2">
                                                        auto-create matching Purchase Orders for its primary vendor, where Bill-to is
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="signup" ID="ddlBillTo2" runat="server" Width="200">
                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        and Ship-to is
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="signup" ID="ddlShipTo2" runat="server" Width="200">
                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="normal1">
                            <td align="left">
                                <asp:CheckBox ID="chkAmount" runat="server" Text="ONLY" />
                                IF Amount is paid in full.
                            </td>
                        </tr>
                        <tr class="normal1">
                            <td>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:CheckBox ID="chkBizDoc" runat="server" Text="ONLY" AutoPostBack="True" />
                                            Authoritative Sales BizDoc Status is set to
                                        </td>
                                        <td>
                                            <asp:DropDownList CssClass="signup" ID="ddlBizDocStatus" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add Rule" Width="59px">
                                            </asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="normal1">
                            <td align="left">
                                <asp:DataGrid ID="dgReport" AutoGenerateColumns="False" runat="server" Width="100%"
                                    CssClass="dg">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Active">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkActive" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Item Type" HeaderText="Item Type"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Item Classification" HeaderText=" Item Classification">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Bill To" HeaderText="Bill To"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Ship To" HeaderText="Ship To"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Amount Paid Full" HeaderText="Amount Paid Full"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Biz Doc Status" HeaderText="Biz Doc Status"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numItemTypeID" HeaderText="numItemTypeID" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="numItemClassID" HeaderText="numItemClassID" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="numBillToID" HeaderText="numBillToID" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="numShipToID" HeaderText="numShipToID" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="btFullPaid" HeaderText="btFullPaid" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="numBizDocStatus" HeaderText="numBizDocStatus" Visible="False">
                                        </asp:BoundColumn>
                                        <%--<asp:ButtonColumn
                        CommandName="Delete" HeaderText="Delete" Text="Delete">--%>
                                        <%--</asp:ButtonColumn>--%>
                                        <asp:TemplateColumn HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:Button ID="btnDeleteAction" runat="server" CssClass="button Delete" Text="X"
                                                    CommandName="Delete"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="numRuleDetailsID" HeaderText="numRuleDetailsID" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="btActive" HeaderText="btActive" Visible="False"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        </td> </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>
