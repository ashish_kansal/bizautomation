﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizDocAppRule.aspx.vb"
    Inherits=".frmBizDocAppRule" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizDoc Approval Rule</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function DeleteRule1() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function Add() {
            var str = '';
            var str1 = '';
            for (var i = 0; i < document.form1.lstSelectedfld.options.length; i++) {
                var SelectedValue;
                SelectedValue = document.form1.lstSelectedfld.options[i].value;

                str = str + SelectedValue + ',';

                var SelectedText = document.form1.lstSelectedfld.options[i].text;
                str1 = str1 + SelectedText + ',';
            }
            document.form1.hdnEmpID.value = str;
            document.form1.hdnEmpName.value = str1;
            //            alert(document.form1.hdnEmpID.value)
            //            alert(str1);
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    BizDoc Approval Rule
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tblOppr" runat="server" GridLines="None" Width="100%" Height="300"
        CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="normal1">
                                        Available Employees<br />
                                        <asp:ListBox ID="lstAvailablefld" runat="server" Width="180" Height="200" CssClass="signup"
                                            EnableViewState="true"></asp:ListBox>
                                    </td>
                                    <td style="width: 50px" align="center">
                                        <input type="button" id="btnAdd" class="button" value=">>" onclick="javascript:move(document.form1.lstAvailablefld,document.form1.lstSelectedfld)" />
                                        <br />
                                        <br />
                                        <input type="button" id="btnRemove" class="button" value="<<" onclick="javascript:remove1(document.form1.lstSelectedfld,document.form1.lstAvailablefld)" />
                                    </td>
                                    <td class="normal1" style="width: 200px">
                                        Employee Document Approver List<br />
                                        <asp:ListBox ID="lstSelectedfld" runat="server" Width="180" Height="200" CssClass="signup"
                                            EnableViewState="true"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" width="100%" cellpadding="0" border="0" class="style11">
                    <tr>
                        <td>
                            <table cellspacing="2" cellpadding="0" border="0" width="100%x">
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="normal1">
                                                    WHEN the following BizDoc is created
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="ddlBizDoc1" runat="server" Width="198px"
                                                        Height="20px" AutoPostBack="true">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    IF the total amount is between
                                                </td>
                                                <td>
                                                    <span class="style9">
                                                        <asp:TextBox ID="txtAmoutFrom" runat="server" CssClass="signup" Width="125px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</span>and &nbsp;</span> <span class="style9">
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAmountTo" runat="server" CssClass="signup" Width="115px"></asp:TextBox>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td class="normal1">
                                        Add the above selected employees to the list of approvers within the BizDoc<br />
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td class="normal1">
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:CheckBox ID="chkAmount1" runat="server"
                                            Text="The moment BizDoc is created." />
                                        <br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:CheckBox ID="chkBizDoc1" runat="server"
                                            Text="When the BizDoc has been paid in full." />
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="normal1">
                                                    AND create an action item with the following &quot;type&quot; value
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="ddlActionType1" runat="server" Width="265px"
                                                        Height="20px">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;for each approver <i>(requesting approval or declaination) </i>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="normal1" valign="middle">
                                                    WHEN ALL the approvers have approved the BizDoc (and none have declined) change
                                                    the BiizDoc Status value to
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="dllBizDocStatus1" runat="server" CssClass="signup" Height="20px"
                                                        Width="265px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="normal1" valign="middle">
                                                    IF one or more approvers decline the BizDoc, change the status value to
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="dllBizDocStatus2" runat="server" Width="265px"
                                                        Height="20px">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddRule" runat="server" CssClass="button" Text="Add Rule" Width="90px"
                                                        OnClientClick="return Add();" OnClick="btnAdd_Click"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td class="normal1" align="right">
                                        <asp:DataGrid ID="dgRule" AutoGenerateColumns="False" runat="server" Width="100%"
                                            CssClass="dg">
                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                            <ItemStyle CssClass="is"></ItemStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="BizDocType" HeaderText="BizDoc Type"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Range" HeaderText="Total Amount Range"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Condition" HeaderText="Condition"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Employee" HeaderText="Approvers"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="btBizDocCreated" HeaderText="btBizDocCreated" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="btBizDocPaidFull" HeaderText="btBizDocPaidFull" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Action Type" HeaderText="Action Type" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Approve Status" HeaderText="Approve Status" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Decline Status" HeaderText="Decline Status" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="numBizDocTypeID" HeaderText="numBizDocTypeID" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="numActionTypeID" HeaderText="numActionTypeID" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="numApproveDocStatusID" HeaderText="numApproveDocStatusID"
                                                    Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="numDeclainDocStatusID" HeaderText="numDeclainDocStatusID"
                                                    Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="EmployeeID" HeaderText="EmployeeID" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDeleteDocRule" runat="server" CssClass="button Delete" Text="X"
                                                            CommandName="Delete"></asp:Button>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="numBizDocAppID" HeaderText="numBizDocAppID" Visible="False">
                                                </asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td class="hs1" align="left" colspan="2">
                                        <b><font style="font-size: 8pt" face="Arial">BizDoc Status Rules </font></b>
                                    </td>
                                    <%--<td align="center" class="style11" valign="top">
                                        BizDoc Status Rules</td>--%>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="normal1">
                                                    <asp:CheckBox ID="chkBizDoc" runat="server" Text="WHEN " AutoPostBack="true" />
                                                    the following BizDoc
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="ddlBizDoc2" runat="server" Width="220px"
                                                        Height="20px" AutoPostBack="true">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    status is
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="dllBizDocStatus3" runat="server" Width="180px"
                                                        Height="20px">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    Create an action item for the following employee
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="ddlEmployee" runat="server" Width="220px"
                                                        Height="20px">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    with the &quot;type&quot; value set to
                                                </td>
                                                <td>
                                                    <asp:DropDownList CssClass="signup" ID="ddlActionType2" runat="server" Width="180px"
                                                        Height="20px">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <i>(requesting approval or declaination)</i>
                                                </td>
                                                <td>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddDocStatus" runat="server" CssClass="button" Text="Add Status"
                                                        Width="90px"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <asp:DataGrid ID="dgBizDocStatus" AutoGenerateColumns="False" runat="server" Width="100%"
                                        CssClass="dg">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="BizDocTypeValue" HeaderText="BizDoc Type"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="BizDocStatusValue" HeaderText="Status"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="EmployeeValue" HeaderText="Employee"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ActionTypeValue" HeaderText="Type Value"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="BizDocTypeID" HeaderText="BizDocTypeID" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="BizDocStatusID" HeaderText="BizDocStatusID" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="EmployeeID" HeaderText="EmployeeID" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ActionTypeID" HeaderText="ActionTypeID" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="numBizDocStatusID" HeaderText="numBizDocStatusID" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDeleteDoc" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteDoc">
                                                    </asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                    </asp:DataGrid>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="normal1">
                            <asp:HiddenField ID="hdRuleID" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnEmpID" runat="server" />
    <asp:HiddenField ID="hdnEmpName" runat="server" />
</asp:Content>
