Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports Aspose.Words
Imports Aspose.Pdf
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Opportunities
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Documents
    Public Class frmAddSpecificDocuments
        Inherits BACRMPage

        Dim lngRecID As Long
        Dim lngDocID As Long
        Dim strType As String


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngRecID = CCommon.ToLong(GetQueryStringVal("yunWE")) ' RecordID
                strType = GetQueryStringVal("Type")
                lngDocID = CCommon.ToLong(GetQueryStringVal("tyuTN"))

                If Not IsPostBack Then
                    objCommon.sb_FillComboFromDBwithSel(ddldocumentsCtgr, 29, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlClass, 28, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))

                    If strType = "A" Or strType = "CS" Or strType = "C" Or strType = "O" Or strType = "P" Then
                        objCommon = New CCommon
                        objCommon.UserCntID = Session("UserContactID")

                        If strType = "A" Then 'Accounts
                            objCommon.DivisionID = lngRecID
                            objCommon.charModule = "D"
                        ElseIf strType = "CS" Then 'Cases
                            objCommon.CaseID = lngRecID
                            objCommon.charModule = "S"
                        ElseIf strType = "C" Then 'Contacts
                            objCommon.ContactID = lngRecID
                            objCommon.charModule = "C"
                        ElseIf strType = "O" Then 'Orders
                            objCommon.OppID = lngRecID
                            objCommon.charModule = "O"
                        ElseIf strType = "P" Then 'Projects
                            objCommon.ProID = lngRecID
                            objCommon.charModule = "P"
                        End If

                        objCommon.GetCompanySpecificValues1()

                        If objCommon.DivisionID > 0 And (strType = "CS" Or strType = "O" Or strType = "P") Then
                            radUploadOrganization.Text = "<img src='../images/Building-16.gif'/>  " & objCommon.GetCompanyName
                            trOrganization.Visible = True

                            Dim objDocuments As New DocumentList
                            Dim dtDocuments As DataTable
                            objDocuments.RecID = objCommon.DivisionID
                            objDocuments.strType = "A"
                            objDocuments.DocCategory = 0
                            objDocuments.DomainID = Session("DomainID")
                            objDocuments.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                            radOrganizationDocuments.DataSource = objDocuments.GetSpecificDocuments1
                            radOrganizationDocuments.DataTextField = "vcdocname"
                            radOrganizationDocuments.DataValueField = "numGenericDocid"
                            radOrganizationDocuments.DataBind()

                            radOrganizationDocuments.ShowDropDownOnTextboxClick = True
                            radOrganizationDocuments.CheckBoxes = True
                            radOrganizationDocuments.EnableTextSelection = True
                        End If
                    End If

                    If lngRecID > 0 Then
                        btnSaveCLose.Attributes.Add("onclick", "return Save()")
                    End If
                End If

                'If radUpload.Checked = True Then 'Or radUploadMrg.Checked = True
                '    Response.Redirect("../Documents/frmGenDocPopUp.aspx?frm=frmSpecDocuments&Type=" & strType & "&RecID=" & lngRecID & "&")
                'End If
                'If radDocumentLib.Checked = True Then
                '    'pnlDocuments.Visible = True
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub


        Sub LoadDocuments()
            Try
                Dim objDocuments As New DocumentList
                Dim dtDocuments As DataTable
                objDocuments.DocCategory = ddldocumentsCtgr.SelectedValue
                objDocuments.DomainID = Session("DomainID")
                dtDocuments = objDocuments.GetGenericDocList
                ddldocuments.DataSource = dtDocuments
                ddldocuments.DataTextField = "vcdocname"
                ddldocuments.DataValueField = "numGenericDocid"
                ddldocuments.DataBind()
                ddldocuments.Items.Insert(0, "--Select One--")
                ddldocuments.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Sub FillMergeFields(ByVal strFilePath As String)
        '    Try
        '        Dim strFName As String()
        '        Dim Name As String()
        '        strFName = Split(strFilePath, ".")
        '        Name = Split(strFName(2), "/")
        '        Dim dtTable As DataTable
        '        If (strType = "C") Then
        '            Dim objPageLayout As New CPageLayout
        '            objPageLayout.ContactID = lngRecID
        '            objPageLayout.DomainID = Session("DomainID")
        '            'dtTable = objPageLayout.GetContactInfoEditIP
        '        ElseIf (strType = "A") Then
        '            Dim objCompany As New CAccounts
        '            objCompany.DivisionID = lngRecID
        '            dtTable = objCompany.GetCompanyPrimaryContact
        '        End If
        '        If Not dtTable Is Nothing Then
        '            If dtTable.Rows.Count > 0 Then
        '                Dim doc As Document = New Document(ConfigurationManager.AppSettings("PortalLocation") & strFName(2) & "." & strFName(3))

        '                doc.Range.Replace("<<FirstName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcFirstName")), "", _
        '                dtTable.Rows(0).Item("vcFirstName")), False, False)

        '                doc.Range.Replace("<<LastName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcLastName")), "", _
        '                dtTable.Rows(0).Item("vcLastName")), False, False)

        '                doc.Range.Replace("<<CompanyName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcCompanyName")), "", _
        '                dtTable.Rows(0).Item("vcCompanyName")), False, False)

        '                doc.Range.Replace("<<DOB>>", IIf(IsDBNull(dtTable.Rows(0).Item("bintDOB")), "", _
        '                dtTable.Rows(0).Item("bintDOB")), False, False)

        '                doc.Range.Replace("<<AsstName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcAsstName")), "", _
        '                dtTable.Rows(0).Item("vcAsstName")), False, False)

        '                doc.Range.Replace("<<AsstPhone>>", IIf(IsDBNull(dtTable.Rows(0).Item("numAsstPhone")), "", _
        '                dtTable.Rows(0).Item("numAsstPhone")), False, False)

        '                doc.Range.Replace("<<Phone>>", IIf(IsDBNull(dtTable.Rows(0).Item("numPhone")), "", _
        '                dtTable.Rows(0).Item("numPhone")), False, False)

        '                doc.Range.Replace("<<Comments>>", IIf(IsDBNull(dtTable.Rows(0).Item("txtNotes")), "", _
        '                dtTable.Rows(0).Item("txtNotes")), False, False)

        '                doc.Range.Replace("<<Address>>", IIf(IsDBNull(dtTable.Rows(0).Item("Address")), "", _
        '                dtTable.Rows(0).Item("Address")), False, False)

        '                Dim strSavePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & Name(3) & ".xml"

        '                doc.Save(strSavePath, SaveFormat.WordML)
        '                'Dim docRem As Document = New Document(strFilePath)
        '                'docRem.Remove()
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub btnSaveCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCLose.Click
            Try
                If CCommon.ToLong(Session("DomainId")) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                End If

                If radUploadOrganization.Checked = True AndAlso radOrganizationDocuments.CheckedItems().Count > 0 Then
                    Dim objDocuments As New DocumentList
                    Dim dtDocuments As DataTable

                        For Each item As RadComboBoxItem In radOrganizationDocuments.CheckedItems
                            If item.Checked = True Then
                                objDocuments = New DocumentList()
                                objDocuments.GenDocID = item.Value
                                objDocuments.UserCntID = Session("UserContactID")
                                objDocuments.DomainID = Session("DomainID")
                                dtDocuments = objDocuments.GetDocByGenDocID
                                If dtDocuments.Rows.Count > 0 Then
                                    UploadFile(dtDocuments.Rows(0).Item("cUrlType"), dtDocuments.Rows(0).Item("vcfiletype"), dtDocuments.Rows(0).Item("VcFileName"),
                                    dtDocuments.Rows(0).Item("vcDocName"), dtDocuments.Rows(0).Item("vcDocdesc"),
                                    CCommon.ToLong(dtDocuments.Rows(0).Item("numDocStatus")), CCommon.ToLong(dtDocuments.Rows(0).Item("numDocCategory")))
                                End If
                            End If
                        Next

                ElseIf radUploadItem.Checked = True AndAlso radItemDocuments.CheckedItems().Count > 0 Then
                    Dim objItem As New BACRM.BusinessLogic.Item.CItems
                    objItem.ItemCode = hdnCurrentSelectedItem.Value
                    objItem.DomainID = Session("DomainId")

                    Dim dtImage As DataTable
                    dtImage = objItem.ImageItemDetails()

                    If dtImage.Rows.Count > 0 Then
                        For Each item As RadComboBoxItem In radItemDocuments.CheckedItems
                            If item.Checked = True Then

                                Dim drFLCheck() As DataRow = dtImage.Select("numItemImageId=" & item.Value)

                                If drFLCheck.Length > 0 Then
                                    Dim strFName As String()
                                    Dim strFilePath, strFileType As String

                                    strFName = Split(drFLCheck(0)("vcPathForImage"), ".")
                                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File

                                    UploadFile("L", strFileType, drFLCheck(0)("vcPathForImage"),
                                               strFName(0), "", 0, 0)
                                End If
                            End If
                        Next
                    End If
                ElseIf radDocumentLib.Checked = True Then
                    Dim objDocuments As New DocumentList
                    Dim dtDocuments As DataTable
                    objDocuments.GenDocID = ddldocuments.SelectedItem.Value
                    objDocuments.UserCntID = Session("UserContactID")
                    objDocuments.DomainID = Session("DomainID")
                    dtDocuments = objDocuments.GetDocByGenDocID
                    If dtDocuments.Rows.Count > 0 Then

                        UploadFile(dtDocuments.Rows(0).Item("cUrlType"), dtDocuments.Rows(0).Item("vcfiletype"), dtDocuments.Rows(0).Item("VcFileName"),
                        dtDocuments.Rows(0).Item("vcDocName"), dtDocuments.Rows(0).Item("vcDocdesc"),
                        CCommon.ToLong(dtDocuments.Rows(0).Item("numDocStatus")), CCommon.ToLong(dtDocuments.Rows(0).Item("numDocCategory")))
                    End If
                ElseIf radUpload.Checked = True Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    If (fileupload.PostedFile.ContentLength > 0) Then
                        strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                        If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                            Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                        End If
                        strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                        strFName = Split(strFileName, ".")
                        strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                        fileupload.PostedFile.SaveAs(strFilePath)

                        UploadFile("L", strFileType, strFileName, txtDocName.Text, txtDesc.Text, ddlClass.SelectedItem.Value, ddlCategory.SelectedItem.Value)
                    End If
                ElseIf radUploadExternal.Checked = True Then
                    UploadFile("U", "", txtPath.Text, txtDocName.Text, txtDesc.Text, ddlClass.SelectedItem.Value, ddlCategory.SelectedItem.Value)
                End If
                If strType = "T" Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "if (window.opener != null) {window.opener.location.reload(true);Close();}self.close();", True)
                Else
                    Response.Redirect("../documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                If strType = "T" Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "if (window.opener != null) {window.opener.location.reload(true);Close();}self.close();", True)
                Else
                    Response.Redirect("../documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As String)
            Try
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngRecID
                    .DocumentType = IIf(lngRecID > 0, 2, 1) '1=generic,2=specific
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Private Sub btnSavePdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePdf.Click
        '    Try
        '        If lngDocID > 0 Then
        '            Dim strPath As String() = txtHidden.Text.Split("~")(0).Split(".")
        '            Dim strFilePath As String = ConfigurationManager.AppSettings("PortalLocation") & strPath(2) & "." & strPath(3)
        '            Dim strFName As String = strPath(2).Split("/")(3)
        '            Dim dtTable As DataTable
        '            Dim doc As Document = New Document(strFilePath)

        '            Dim strSavePathTemp As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFName & "~.xml"
        '            Dim strSavePathPDF As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFName & ".Pdf"

        '            doc.Save(strSavePathTemp, SaveFormat.AsposePdf)
        '            Dim pdf As Aspose.Pdf.Pdf = New Aspose.Pdf.Pdf()
        '            pdf.BindXML(strSavePathTemp, Nothing)
        '            pdf.IsImagesInXmlDeleteNeeded = True
        '            pdf.Save(strSavePathPDF)

        '            If File.Exists(strSavePathTemp) Then File.Delete(strSavePathTemp)
        '            Dim arrOutPut As String()
        '            Dim objDocuments As New DocumentList
        '            With objDocuments
        '                .SpecDocID = 0
        '                .DomainID = Session("DomainId")
        '                .ContactID = Session("UserContactID")
        '                .UrlType = URLType
        '                .DocumentStatus = ddlClass.SelectedItem.Value
        '                .DocCategory = ddlCategory.SelectedItem.Value
        '                .FileType = ".pdf"
        '                .DocName = txtDocName.Text
        '                .DocDesc = txtDesc.Text
        '                .FileName = "../documents/docs/" & strFName & ".Pdf"
        '                .RecID = lngRecID
        '                .strType = strType
        '                .byteMode = 0
        '                arrOutPut = .SaveSpecDocuments()
        '            End With
        '        End If
        '        Response.Redirect("../documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Private Sub ddldocumentsCtgr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddldocumentsCtgr.SelectedIndexChanged
            Try
                LoadDocuments()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lkbItemSelected_Click(sender As Object, e As EventArgs) Handles lkbItemSelected.Click
            Try
                'hdnCurrentSelectedItem.Value
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                objItem.ItemCode = hdnCurrentSelectedItem.Value
                objItem.DomainID = Session("DomainId")

                Dim dtImage As DataTable
                dtImage = objItem.ImageItemDetails()

                radItemDocuments.DataSource = dtImage
                radItemDocuments.DataTextField = "vcPathForImage"
                radItemDocuments.DataValueField = "numItemImageId"
                radItemDocuments.DataBind()

                radItemDocuments.ShowDropDownOnTextboxClick = True
                radItemDocuments.CheckBoxes = True
                radItemDocuments.EnableTextSelection = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lkbItemRemoved_Click(sender As Object, e As EventArgs) Handles lkbItemRemoved.Click
            Try
                radItemDocuments.Items.Clear()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
