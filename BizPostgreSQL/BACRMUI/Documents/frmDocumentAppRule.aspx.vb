﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json
Imports System.Collections.Generic

Namespace BACRM.UserInterface.AutomatonRules
    Public Class frmDocumentAppRule
        Inherits BACRMPage
        Dim lngRuleId As Long
        Dim objAutomatonRule As AutomatonRule
        Dim objCommon As CCommon

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngRuleId = CCommon.ToLong(GetQueryStringVal("RuleId"))
                If Not IsPostBack Then
                    BindRuleModule()
                    If lngRuleId > 0 Then
                        LoadDetails()
                    End If

                    'Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
                    'Dim Items As ARObject = Newtonsoft.Json.JsonConvert.DeserializeObject(Of ARObject)(lblExistingQuery.Text)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindRuleModule()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.boolWorkFlow = 1

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.getFormList()

                ddlRuleModule.DataSource = dtFormsList
                ddlRuleModule.DataTextField = "vcFormName"
                ddlRuleModule.DataValueField = "numFormId"
                ddlRuleModule.DataBind()

                ddlRuleModule.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlRuleModule.SelectedIndex = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDetails()
            Try
                Dim dtTable As DataTable

                objAutomatonRule = New AutomatonRule
                objAutomatonRule.RuleID = lngRuleId
                objAutomatonRule.DomainID = Session("DomainID")
                objAutomatonRule.byteMode = 0

                Dim dsList As DataSet

                dsList = objAutomatonRule.GetRuleMaster
                dtTable = dsList.Tables(0)

                If dtTable.Rows.Count > 0 Then
                    tblConditionAction.Visible = True
                    txtRuleName.Text = dtTable.Rows(0).Item("vcRuleName")
                    txtRuleDesc.Text = dtTable.Rows(0).Item("vcRuleDescription")

                    If Not ddlRuleModule.Items.FindByValue(dtTable.Rows(0).Item("tintModuleType")) Is Nothing Then
                        ddlRuleModule.ClearSelection()
                        ddlRuleModule.Items.FindByValue(dtTable.Rows(0).Item("tintModuleType")).Selected = True
                        ddlRuleModule.Enabled = False
                    End If

                    If Not ddlRuleStatus.Items.FindByValue(dtTable.Rows(0).Item("tintStatus")) Is Nothing Then
                        ddlRuleStatus.ClearSelection()
                        ddlRuleStatus.Items.FindByValue(dtTable.Rows(0).Item("tintStatus")).Selected = True
                    End If

                    Dim mask As Short = dtTable.Rows(0).Item("tintBasedOn")

                    BindRuleField()

                    While mask <> 0
                        For i As Integer = cblBasedOn.Items.Count - 1 To 0 Step -1
                            If (mask - cblBasedOn.Items(i).Value >= 0) Then
                                mask = mask - cblBasedOn.Items(i).Value
                                cblBasedOn.Items(i).Selected = True

                                'If cblBasedOn.Items(i).Value = "8" Then
                                '    If Not ddlAttribute.Items.FindByValue(dtTable.Rows(0).Item("numFormFieldIdChange")) Is Nothing Then
                                '        ddlAttribute.ClearSelection()
                                '        ddlAttribute.Items.FindByValue(dtTable.Rows(0).Item("numFormFieldIdChange")).Selected = True
                                '    End If
                                'End If
                            End If
                        Next
                    End While

                    Dim dtRule As DataTable
                    dtRule = dsList.Tables(1)

                    Dim arrvcJSON As String() = New [String](dtRule.Rows.Count - 1) {}
                    Dim arrvcCondition As String() = New [String](dtRule.Rows.Count - 1) {}

                    For i As Integer = 0 To dtRule.Rows.Count - 1
                        arrvcJSON(i) = dtRule.Rows(i)("vcJSON")
                        arrvcCondition(i) = dtRule.Rows(i)("vcCondition")
                    Next

                    txtExistingJSON.Text = String.Join("$^$", arrvcJSON)
                    txtExistingQuery.Text = String.Join("$^$", arrvcCondition)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindRuleField()
            Try
                Dim dtTableInfo As DataTable
                Dim dsList As DataSet

                If objAutomatonRule Is Nothing Then
                    objAutomatonRule = New AutomatonRule
                End If
                objAutomatonRule.DomainID = Session("DomainID")
                objAutomatonRule.byteMode = 2
                objAutomatonRule.RuleModuleType = ddlRuleModule.SelectedValue

                dsList = objAutomatonRule.GetRuleMaster
                dtTableInfo = dsList.Tables(0)

                'ddlAttribute.DataSource = dtTableInfo
                'ddlAttribute.DataTextField = "vcFormFieldName"
                'ddlAttribute.DataValueField = "numFormFieldId"
                'ddlAttribute.DataBind()

                'ddlAttribute.Items.Insert(0, New ListItem("--Select--", "0"))
                'ddlAttribute.SelectedIndex = 0

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateRuleModuleFieldListScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "RuleModuleFieldListScript", strValidation, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                lngRuleId = Save()
                If lngRuleId > 0 Then
                    Response.Redirect("../Documents/frmDocumentAppRule.aspx?RuleId=" & lngRuleId)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                lngRuleId = Save()
                If lngRuleId > 0 Then
                    Response.Redirect("../Documents/frmDocumentAppRuleList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                Response.Redirect("../Documents/frmDocumentAppRuleList.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function Save() As Long
            Try
                objAutomatonRule = New AutomatonRule
                objAutomatonRule.RuleID = lngRuleId
                objAutomatonRule.DomainID = Session("DomainID")

                objAutomatonRule.RuleName = txtRuleName.Text
                objAutomatonRule.RuleDescription = txtRuleDesc.Text
                objAutomatonRule.RuleStatus = ddlRuleStatus.SelectedValue
                objAutomatonRule.RuleModuleType = ddlRuleModule.SelectedValue

                Dim RuleBasedOn As Short = 0
                For Each li As ListItem In cblBasedOn.Items
                    If li.Selected Then
                        RuleBasedOn += li.Value
                    End If
                Next

                objAutomatonRule.RuleBasedOn = RuleBasedOn
                objAutomatonRule.FormFieldIdChange = 0 'ddlAttribute.SelectedValue

                objAutomatonRule.RuleID = objAutomatonRule.ManageRuleMaster()

                If lngRuleId > 0 Then

                    Dim RuleJSON As String()
                    RuleJSON = txtExistingJSON.Text.Split(New String() {"$^$"}, StringSplitOptions.RemoveEmptyEntries)

                    Dim RuleQuery As String()
                    RuleQuery = txtExistingQuery.Text.Split(New String() {"$^$"}, StringSplitOptions.RemoveEmptyEntries)

                    For i As Integer = 0 To RuleJSON.Length - 1
                        Dim RuleActionList As ARObject = Newtonsoft.Json.JsonConvert.DeserializeObject(Of ARObject)(RuleJSON(i))

                        objAutomatonRule.vcCondition = RuleQuery(i)
                        objAutomatonRule.vcJSON = RuleJSON(i)
                        objAutomatonRule.ConditionOrder = i
                        objAutomatonRule.RuleConditionID = objAutomatonRule.ManageRuleCondition

                        For j As Integer = 0 To RuleActionList.actions.Count - 1
                            Dim ruleAct As actionsObject = RuleActionList.actions(j)
                            objAutomatonRule.ActionType = ruleAct.Actval

                            If objAutomatonRule.ActionType = 4 Then 'Field value set
                                objAutomatonRule.ActionTemplateID = ruleAct.ActTID.Split("~")(2)
                                objAutomatonRule.vcFieldValue = ruleAct.ActFValue
                            ElseIf objAutomatonRule.ActionType = 5 Then 'BizDoc Approval Request
                                objAutomatonRule.ActionTemplateID = 0
                                objAutomatonRule.vcFieldValue = ruleAct.ActFValue
                            ElseIf objAutomatonRule.ActionType = 2 Then 'Tickler Action
                                objAutomatonRule.ActionTemplateID = ruleAct.ActTID 'Template ID
                                objAutomatonRule.vcFieldValue = ruleAct.ActFValue 'Contact ID
                            End If
                            objAutomatonRule.ActionOrder = j
                            objAutomatonRule.ManageRuleAction()
                        Next
                    Next
                End If

                Return objAutomatonRule.RuleID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlRuleModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRuleModule.SelectedIndexChanged
            Try
                BindRuleField()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class

    Public Class ARObject
        Private m_operator As String
        Public Property [operator]() As String
            Get
                Return m_operator
            End Get
            Set(ByVal value As String)
                m_operator = value
            End Set
        End Property

        Private m_expressions As List(Of Expression)
        Public Property expressions() As List(Of Expression)
            Get
                Return m_expressions
            End Get
            Set(ByVal value As List(Of Expression))
                m_expressions = value
            End Set
        End Property

        Private m_actions As List(Of actionsObject)
        Public Property actions() As List(Of actionsObject)
            Get
                Return m_actions
            End Get
            Set(ByVal value As List(Of actionsObject))
                m_actions = value
            End Set
        End Property

        Private m_nestedexpressions As List(Of ARObject)
        Public Property nestedexpressions() As List(Of ARObject)
            Get
                Return m_nestedexpressions
            End Get
            Set(ByVal value As List(Of ARObject))
                m_nestedexpressions = value
            End Set
        End Property
    End Class

    Public Class Expression
        Private m_colval As String
        Public Property colval() As String
            Get
                Return m_colval
            End Get
            Set(ByVal value As String)
                m_colval = value
            End Set
        End Property

        Private m_coldisp As String
        Public Property coldisp() As String
            Get
                Return m_coldisp
            End Get
            Set(ByVal value As String)
                m_coldisp = value
            End Set
        End Property

        Private m_opval As String
        Public Property opval() As String
            Get
                Return m_opval
            End Get
            Set(ByVal value As String)
                m_opval = value
            End Set
        End Property

        Private m_opdisp As String
        Public Property opdisp() As String
            Get
                Return m_opdisp
            End Get
            Set(ByVal value As String)
                m_opdisp = value
            End Set
        End Property

        Private m_val As String
        Public Property val() As String
            Get
                Return m_val
            End Get
            Set(ByVal value As String)
                m_val = value
            End Set
        End Property
    End Class

    Public Class actionsObject
        Private m_Actval As String
        Public Property Actval() As String
            Get
                Return m_Actval
            End Get
            Set(ByVal value As String)
                m_Actval = value
            End Set
        End Property

        Private m_Actvaldisp As String
        Public Property Actvaldisp() As String
            Get
                Return m_Actvaldisp
            End Get
            Set(ByVal value As String)
                m_Actvaldisp = value
            End Set
        End Property

        Private m_ActTID As String
        Public Property ActTID() As String
            Get
                Return m_ActTID
            End Get
            Set(ByVal value As String)
                m_ActTID = value
            End Set
        End Property

        Private m_ActFValue As String
        Public Property ActFValue() As String
            Get
                Return m_ActFValue
            End Get
            Set(ByVal value As String)
                m_ActFValue = value
            End Set
        End Property
    End Class

End Namespace
