Imports System.Xml
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Documents

Public Class frmMapSpecDocConFields
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
    Protected WithEvents tblDetails As System.Web.UI.WebControls.Table
    Protected WithEvents tblBizFields As System.Web.UI.WebControls.Table
    Protected WithEvents btnSaveEform As System.Web.UI.WebControls.Button
    Protected WithEvents btnClose As System.Web.UI.WebControls.Button
    Dim strURL As String
        Protected WithEvents pgBar As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents btnAutoMap As System.Web.UI.WebControls.Button
        Protected WithEvents txtHidden As System.Web.UI.WebControls.TextBox
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                pgBar.Style.Add("display", "")
                If Not IsPostBack Then
                    If GetQueryStringVal( "Filetype") <> ".xml" Then
                        Response.Redirect(GetQueryStringVal( "FileName"))
                        Exit Sub
                    End If
                End If
                CreateDataTable()
                CreateSchema()
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Documents"
                    GetEformConfiguration()
                End If
                strURL = "../documents/frmSpecDocsConEform.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SpecID=" & GetQueryStringVal( "SpecID") & "&Filetype=" & GetQueryStringVal( "Filetype") & "&FileName=" & GetQueryStringVal( "FileName")
                btnClose.Attributes.Add("onclick", "return Close()")
                pgBar.Style.Add("display", "none")
                btnAutoMap.Attributes.Add("onclick", "return Automap()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub GetEformConfiguration()
            Try
                Dim i As Integer
                Dim dtEformConiguration As DataTable
                Dim objDocuments As New DocumentList
                objDocuments.SpecDocID = CCommon.ToLong(GetQueryStringVal("SpecID"))
                dtEformConiguration = objDocuments.GetEformConfiguration
                Dim strValues As String()
                Dim ddl As DropDownList
                For i = 0 To dtEformConiguration.Rows.Count - 1
                    strValues = dtEformConiguration.Rows(i).Item("vcEFormFld").Split("~")
                    ddl = Page.FindControl(strValues(1))
                    If Not ddl.Items.FindByValue(dtEformConiguration.Rows(i).Item("vcColumnName")) Is Nothing Then
                        ddl.Items.FindByValue(dtEformConiguration.Rows(i).Item("vcColumnName")).Selected = True
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreateSchema()
            Try
                Dim i, k, j As Integer
                Dim str As String
                Dim currentElement As String
                Dim currentText As String
                Dim dtData As New DataTable
                Dim dtBizFields As DataTable
                dtData.Columns.Add("EformFields")
                dtData.Columns.Add("EformFieldName")
                Dim dr As DataRow
                Dim reader As New XmlTextReader(Replace(GetQueryStringVal( "FileName"), "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName")))
                While reader.Read()
                    Select Case reader.NodeType
                        Case XmlNodeType.Element
                            currentElement = reader.LocalName
                        Case XmlNodeType.Text
                            currentText = reader.Value
                        Case XmlNodeType.EndElement
                            If reader.LocalName = currentElement Then
                                dr = dtData.NewRow
                                dr("EformFields") = currentElement
                                dtData.Rows.Add(dr)
                            End If
                    End Select
                End While
                Session("dtData") = dtData

                dtBizFields = Session("EformFields")
                Dim tblCell As TableCell
                Dim tblRow As TableRow
                Dim ddl As DropDownList
                For i = 0 To dtData.Rows.Count - 1
                    If k = 0 Then tblRow = New TableRow

                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    tblCell.HorizontalAlign = HorizontalAlign.Right
                    tblCell.Text = dtData.Rows(i).Item("EformFields")
                    tblRow.Cells.Add(tblCell)

                    tblCell = New TableCell
                    ddl = New DropDownList
                    ddl.ID = i
                    ddl.CssClass = "signup"
                    ddl.DataSource = dtBizFields
                    ddl.DataTextField = "Name"
                    ddl.DataValueField = "ColumnName"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = 0
                    tblCell.Controls.Add(ddl)
                    tblRow.Cells.Add(tblCell)
                    k = k + 1
                    If k = 3 Then
                        k = 0
                        tblBizFields.Rows.Add(tblRow)
                    End If
                Next
                If Not tblRow Is Nothing Then tblBizFields.Rows.Add(tblRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreateDataTable()
            Try
                Dim dtTable As New DataTable
                Dim dtConCusTable As DataTable
                Dim dr As DataRow
                dtConCusTable = Session("CusFields")
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("ColumnName")
                dtTable.Columns.Add("EformField")

                dr = dtTable.NewRow
                dr("Name") = "First Name"
                dr("ColumnName") = "vcFirstName" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Last name"
                dr("ColumnName") = "vcLastName" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Position"
                dr("ColumnName") = "vcPosition" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Phone"
                dr("ColumnName") = "numPhone" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Ext"
                dr("ColumnName") = "numPhoneExtension" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Cell"
                dr("ColumnName") = "numCell" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Home"
                dr("ColumnName") = "NumHomePhone" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Email"
                dr("ColumnName") = "vcEmail" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Fax"
                dr("ColumnName") = "vcFax" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Street"
                dr("ColumnName") = "vcStreet" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "City"
                dr("ColumnName") = "vcCity" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "PostalCode"
                dr("ColumnName") = "intPostalCode" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "State"
                dr("ColumnName") = "vcState" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Country"
                dr("ColumnName") = "vcCountry" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Contact Type"
                dr("ColumnName") = "numContactType" & "~2"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Gender"
                dr("ColumnName") = "charSex" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Department"
                dr("ColumnName") = "vcDepartment" & "~2"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Category"
                dr("ColumnName") = "vcCategory" & "~2"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Manager"
                dr("ColumnName") = "numManagerID" & "~2"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Team"
                dr("ColumnName") = "numTeam" & "~2"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Comments"
                dr("ColumnName") = "txtNotes" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Assistant's First name"
                dr("ColumnName") = "VcAsstFirstName" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Assistant's Last name"
                dr("ColumnName") = "vcAsstLastName" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Assistant's Email"
                dr("ColumnName") = "vcAsstEmail" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Assistant's Phone"
                dr("ColumnName") = "numAsstPhone" & "~1"
                dtTable.Rows.Add(dr)

                dr = dtTable.NewRow
                dr("Name") = "Assistant's Ext"
                dr("ColumnName") = "numAsstExtn" & "~1"
                dtTable.Rows.Add(dr)

                If dtConCusTable.Rows.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To dtConCusTable.Rows.Count - 1
                        If Not dtConCusTable.Rows(i).Item("fld_type") = "CheckBox" Then
                            dr = dtTable.NewRow
                            dr("Name") = dtConCusTable.Rows(i).Item("fld_label")
                            dr("ColumnName") = "CustomFields" & "~" & IIf(dtConCusTable.Rows(i).Item("fld_type") = "SelectBox", 2, 1) & "~" & dtConCusTable.Rows(i).Item("fld_label") & "~" & dtConCusTable.Rows(i).Item("fld_id")
                            dtTable.Rows.Add(dr)
                        End If
                    Next
                End If
                Session("EformFields") = dtTable
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveEform_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEform.Click
            Try
                pgBar.Style.Add("display", "")
                Dim dtTable As DataTable
                Dim dtFormattedTable As New DataTable
                Dim dr As DataRow
                Dim i As Integer
                Dim ddl As DropDownList
                dtTable = Session("dtData")
                dtFormattedTable.Columns.Add("ColumnName")
                dtFormattedTable.Columns.Add("FieldType") ' 1 for Textbox , 2 For Dropdown
                dtFormattedTable.Columns.Add("EformField")
                For i = 0 To dtTable.Rows.Count - 1
                    If CType(Page.FindControl(i), DropDownList).SelectedIndex > 0 Then
                        dr = dtFormattedTable.NewRow
                        Dim str As String()
                        str = CType(Page.FindControl(i), DropDownList).SelectedValue.Split("~")
                        dr("ColumnName") = CType(Page.FindControl(i), DropDownList).SelectedValue
                        dr("FieldType") = str(1)
                        dr("EformField") = dtTable.Rows(i).Item("EformFields") & "~" & i
                        dtFormattedTable.Rows.Add(dr)
                    End If
                Next
                Dim objDocuments As New DocumentList
                Dim dsNew As New DataSet
                dtFormattedTable.TableName = "Table"
                dsNew.Tables.Add(dtFormattedTable.Copy)
                objDocuments.strSpecDocDetails = dsNew.GetXml
                dsNew.Tables.Remove(dsNew.Tables(0))
                objDocuments.SpecDocID = GetQueryStringVal( "SpecID")
                objDocuments.EformConfiguration()
                Response.Redirect(strURL)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAutoMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAutoMap.Click
            Try
                Dim dtData As DataTable
                Dim i, j As Integer
                Dim dtBizFields As DataTable
                dtData = Session("dtData")
                dtBizFields = Session("EformFields")
                Dim ddl As DropDownList
                For i = 0 To dtData.Rows.Count - 1
                    For j = 0 To dtBizFields.Rows.Count - 1
                        If UCase(dtData.Rows(i).Item("EformFields")) = UCase(dtBizFields.Rows(j).Item("Name")) Then
                            ddl = Page.FindControl(i)
                            ddl.ClearSelection()
                            ddl.Items.FindByText(dtBizFields.Rows(j).Item("Name")).Selected = True
                        End If
                    Next
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
