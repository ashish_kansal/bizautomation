Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class frmDocApprovers
    Inherits BACRMPage
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(10, 19)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AC")
            Else
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then tblAdd.Visible = False
            End If
            If Not IsPostBack Then
                radCmbCompany.Focus()
                BindGrid()

                If GetQueryStringVal("DocType") = "B" Then
                    objCommon.OppID = CCommon.ToLong(GetQueryStringVal("OpID"))
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                ElseIf GetQueryStringVal("DocType") = "C" Then
                    objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("RecID"))
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                ElseIf GetQueryStringVal("DocType") = "A" Then
                    objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("RecID"))
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                End If
                If GetQueryStringVal("DocType") <> "D" Then
                    Dim strCompany As String
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                    radCmbCompany.Text = strCompany
                    FillContact(ddlContacts)
                End If
            End If
            litMessage.Text = ""
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = CCommon.ToLong(GetQueryStringVal("DocID"))
            objDocuments.CDocType = GetQueryStringVal("DocType")

            If GetQueryStringVal("AppStatus") <> "" Then
                objDocuments.CDocType = IIf(GetQueryStringVal("DocType") = "", 1, 0)
                objDocuments.DomainID = Session("DomainID")

                dgApprovers.DataSource = objDocuments.GetApproveStatus(CType(GetQueryStringVal("AppStatus"), Short))
            Else
                dgApprovers.DataSource = objDocuments.GetApprovers
            End If

            dgApprovers.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgApprovers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgApprovers.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = CCommon.ToLong(GetQueryStringVal("DocID"))
                objDocuments.ContactID = CCommon.ToLong(e.Item.Cells(1).Text)
                objDocuments.CDocType = GetQueryStringVal("DocType")
                objDocuments.byteMode = 2
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillContact(ByVal ddlCombo As DropDownList)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = radCmbCompany.SelectedValue
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SendEmail()
        Try

            Dim objSendEmail As New Email

            objSendEmail.DomainID = Session("DomainID")
            objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL_REQUEST"
            objSendEmail.ModuleID = 1
            objSendEmail.RecordIds = ddlContacts.SelectedValue
            objSendEmail.AdditionalMergeFields.Add("DocumentName", GetQueryStringVal("DocName"))
            objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
            objSendEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", GetPortalDirectLoginLink())
            objSendEmail.AdditionalMergeFields.Add("Signature", Session("Signature"))
            objSendEmail.FromEmail = Session("UserEmail")
            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
            objSendEmail.SendEmailTemplate()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetPortalDirectLoginLink() As String

        Dim objUserAccess As New UserAccess
        objUserAccess.ContactID = ddlContacts.SelectedValue
        objUserAccess.DomainID = Session("DomainID")
        Dim dtUser As DataTable = objUserAccess.GetExtranetUserDetails()

        If dtUser.Rows.Count = 1 Then
            Dim strPortalApprovalLink As String
            strPortalApprovalLink = CCommon.ToString(Session("PortalURL")) & QueryEncryption.EncryptQueryString("&from=email&u=" + CCommon.ToString(dtUser.Rows(0)("vcEmail")) + "&p=" + objCommon.Encrypt(CCommon.ToString(dtUser.Rows(0)("vcPassword"))))
            strPortalApprovalLink = "<a href=" & strPortalApprovalLink & " target='_blank' >click here</a>"
            Return strPortalApprovalLink
        End If
        Return ""
    End Function
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = GetQueryStringVal("DocID")
            objDocuments.ContactID = ddlContacts.SelectedValue
            objDocuments.CDocType = GetQueryStringVal("DocType")
            objDocuments.byteMode = 1
            objDocuments.UserCntID = Session("UserContactID")
            Try
                objDocuments.ManageApprovers()
            Catch ex As Exception
                If ex.Message = "No_EXTRANET" Then
                    litMessage.Text = "Can not add selected contact to approver list. Your option is to add contact as external users from ""Administrator->User Administration->External Users"" and try again."
                    Exit Sub
                Else
                    Throw ex
                End If
            End Try








            BindGrid()

            SendEmail()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If radCmbCompany.SelectedValue <> "" Then
                ddlContacts.Enabled = True
                FillContact(ddlContacts)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgApprovers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgApprovers.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    If Not e.Item.FindControl("btnDelete") Is Nothing Then
                        CType(e.Item.FindControl("btnDelete"), Button).Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
End Class