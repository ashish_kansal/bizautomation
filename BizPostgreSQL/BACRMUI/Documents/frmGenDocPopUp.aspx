<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmGenDocPopUp.aspx.vb"
    MasterPageFile="~/common/Popup.Master" Inherits="BACRM.UserInterface.Documents.frmGenDocPopUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Documents</title>
    <script language="javascript">
        function Save() {
            if (document.form1.txtDocName.value == "") {
                alert("Enter Name")
                document.form1.txtDocName.focus()
                return false;
            }
            //if (document.form1.ddlCategory.value == 0) {
            //    alert("Select Document Category")
            //    document.form1.ddlCategory.focus()
            //    return false;
            //}

        }
        function Focus() {
            document.form1.txtDocName.focus();
        }
        function Close() {
            window.close()
            return false;
        }
        $(document).ready(function () {
            $(':input:enabled:visible:first[type=text]').focus()
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save &amp; Close">
                        </asp:Button>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                        </asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Document
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellpadding="0" cellspacing="0" align="center" width="100%" height="100%">
        <tr>
            <td valign="top" colspan="2">
                <asp:Table ID="tblOppr" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
                    CssClass="aspTable" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <br>
                            <table width="100%" border="0" width="100%">
                                <tr>
                                    <td rowspan="7" valign="top">
                                        <img src="../images/Document-48.gif" />
                                    </td>
                                    <td class="normal1" align="right">
                                        Document Name<font color="red">*</font>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDocName" runat="server" CssClass="signup" Width="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Document Category
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlCategory" TabIndex="3" CssClass="signup" runat="server"
                                            Width="200">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" valign="top" align="right">
                                        Document Status
                                    </td>
                                    <td valign="top" colspan="3">
                                        <asp:DropDownList ID="ddlClass" TabIndex="3" CssClass="signup" runat="server" Width="200">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Description
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDesc" runat="server" Width="400" TextMode="MultiLine" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Upload Document from
                                        <br>
                                        Your Local Network
                                    </td>
                                    <td>
                                        <input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td class="normal1" align="left">
                                        &nbsp;&nbsp;&nbsp;Or
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Path/URL to Document
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPath" runat="server" Width="250" TextMode="MultiLine" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
</asp:Content>
