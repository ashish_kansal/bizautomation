Imports System.Xml
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Documents

    Public Class frmSpecDocuments
        Inherits BACRMPage
        Dim objDocuments As DocumentList

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Table3 As System.Web.UI.WebControls.Table
        Protected WithEvents ddlCategory As System.Web.UI.WebControls.DropDownList
        Protected WithEvents dgDocs As System.Web.UI.WebControls.DataGrid
        Protected WithEvents hplNew As System.Web.UI.WebControls.HyperLink
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Dim lngRecID As Long
        Dim strType As String
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngRecID = CCommon.ToLong(GetQueryStringVal("yunWE")) ' RecordID
                strType = GetQueryStringVal("Type")
                If Not IsPostBack Then

                    objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                    bindGrid()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
                hplNew.NavigateUrl = "../documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" & strType & "&yunWE=" & lngRecID
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
            Try
                bindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub bindGrid()
            Try
                If objDocuments Is Nothing Then objDocuments = New DocumentList
                objDocuments.RecID = lngRecID
                objDocuments.strType = strType
                objDocuments.DocCategory = ddlCategory.SelectedItem.Value
                objDocuments.DomainID = Session("DomainID")
                objDocuments.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dgDocs.DataSource = objDocuments.GetSpecificDocuments1
                dgDocs.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    'Dim hplFileName As HyperLink
                    Dim hpl As HyperLink
                    Dim hplEdit As HyperLink
                    hplEdit = e.Item.FindControl("hplEdit")
                    hpl = e.Item.FindControl("hplLink")
                    'hplFileName = e.Item.FindControl("hplFileName")
                    btnDelete = e.Item.FindControl("btnDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                   
                    If hpl.Text.ToUpper = "WEB URL" Then
                        hpl.NavigateUrl = e.Item.Cells(2).Text
                    Else
                        hpl.NavigateUrl = "frmViewAttachment.aspx?docid=" & CCommon.ToLong(e.Item.Cells(0).Text.Trim)
                    End If

                    hplEdit.Attributes.Add("onclick", "return OpenDocument('../documents/frmDocuments.aspx?frm=frmSpecDocuments&Type=" & strType & "&RecID=" & lngRecID & "&DocID=" & e.Item.Cells(0).Text() & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDocs.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    objDocuments = New DocumentList
                    With objDocuments
                        .UserCntID = Session("UserContactId")
                        .DomainID = Session("DomainId")
                        .GenDocID = e.Item.Cells(0).Text()
                        .DelDocumentsByGenDocID()
                    End With
                    bindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
