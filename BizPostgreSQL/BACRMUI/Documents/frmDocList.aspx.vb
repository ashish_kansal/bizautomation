Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Documents
    Public Class frmDocList
        Inherits BACRMPage
        Dim strColumn As String
        Dim ds As New DataSet
        Public lngCategory As Long
        Public lngStatus As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'To Set Permission
                GetUserRightsForPage(14, 2)
                If Not IsPostBack Then

                    Dim objDocuments As New DocumentList
                    Dim ds As New DataSet
                    objDocuments.DomainID = Session("DomainID")
                    ds = objDocuments.GetDocumentStatus

                    ddlCategory.DataSource = ds.Tables(0)
                    ddlCategory.DataTextField = "vcData"
                    ddlCategory.DataValueField = "numListItemID"
                    ddlCategory.DataBind()
                    ddlCategory.Items.Insert(0, New ListItem("--Select One--", "0"))

                    ddlStatus.DataSource = ds.Tables(1)
                    ddlStatus.DataTextField = "vcData"
                    ddlStatus.DataValueField = "numListItemID"
                    ddlStatus.DataBind()
                    ddlStatus.Items.Insert(0, New ListItem("--Select One--", "0"))

                    If GetQueryStringVal("Status") <> "" Then
                        If GetQueryStringVal("Status") = "0" Then
                            ddlStatus.ClearSelection()
                            ddlStatus.Items(0).Selected = True
                        Else
                            If Not ddlStatus.Items.FindByValue(GetQueryStringVal("Status")) Is Nothing Then
                                ddlStatus.ClearSelection()
                                ddlStatus.Items.FindByValue(GetQueryStringVal("Status")).Selected = True
                                lngStatus = ddlStatus.SelectedItem.Value
                            End If
                        End If
                    End If

                    If GetQueryStringVal("Category") <> "" Then
                        If GetQueryStringVal("Category") = "0" Then
                            ddlCategory.ClearSelection()
                            ddlCategory.Items(0).Selected = True
                            hplNew.Visible = False
                        ElseIf GetQueryStringVal("Category") = "369" Or GetQueryStringVal("Category") = "370" Then
                            objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                            If Not ddlCategory.Items.FindByValue(GetQueryStringVal("Category")) Is Nothing Then
                                ddlCategory.Items.FindByValue(GetQueryStringVal("Category")).Selected = True
                                lngCategory = ddlCategory.SelectedItem.Value
                            End If
                            If ds.Tables(1).Rows.Count > 0 Then ddlStatus.ClearSelection()
                            lngStatus = 0

                            If lngCategory = 369 Then
                                hplNew.Visible = True
                                hplNew.Attributes.Add("onclick", "return OpenET()")
                            End If
                        Else
                            If Not ddlCategory.Items.FindByValue(GetQueryStringVal("Category")) Is Nothing Then
                                ddlCategory.ClearSelection()
                                If Not ddlCategory.Items.FindByValue(GetQueryStringVal("Category")) Is Nothing Then
                                    ddlCategory.Items.FindByValue(GetQueryStringVal("Category")).Selected = True
                                    lngCategory = ddlCategory.SelectedItem.Value
                                End If
                            End If
                            hplNew.Visible = False
                        End If




                        'If Not ddlCategory.Items.FindByValue(GetQueryStringVal( "Category")) Is Nothing Then
                        '    ddlCategory.Items.FindByValue(GetQueryStringVal( "Category")).Selected = True
                        '    If ddlCategory.SelectedValue = "369" Then
                        '        hplNew.Visible = True
                        '        hplNew.Attributes.Add("onclick", "return OpenET()")
                        '    Else
                        '        hplNew.Visible = False
                        '    End If
                        'End If
                    End If

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        If Not ddlSort.Items.FindByValue(PersistTable(PersistKey.FilterBy)) Is Nothing Then
                            ddlSort.Items.FindByValue(PersistTable(PersistKey.FilterBy)).Selected = True
                        End If
                        txtKeyWord.Text = CCommon.ToString(PersistTable(PersistKey.SearchValue))
                    End If


                    BindDocumentFlow()
                Else
                    'lngStatus = ddlStatus.SelectedItem.Value
                    lngCategory = ddlCategory.SelectedItem.Value
                End If


            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindDocumentFlow()
            Try

                Dim dtDocuments As DataTable
                Dim objDocuments As New DocumentList
               
                With objDocuments
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortCharacter = txtSortChar.Text.Trim()
                    .KeyWord = txtKeyWord.Text
                    If GetQueryStringVal("Status") <> "" Then
                        If GetQueryStringVal("Status") = "0" Then
                            .DocCategory = 0
                        Else
                            .DocCategory = ddlStatus.SelectedValue
                        End If
                    End If
                    .DocumentStatus = ddlCategory.SelectedValue
                    .RecID = Session("UserContactID")
                    .GroupID = ddlSort.SelectedItem.Value
                    .SortOrder = ddlSort.SelectedItem.Value
                    .strType = ""
                    '.DocCategory = ddlCategory.SelectedValue
                    'If txtCurrrentPage.Text.Trim <> "" Then
                    '    .CurrentPage = txtCurrrentPage.Text
                    'Else : .CurrentPage = 1
                    'End If
                    '.PageSize = Session("PagingRows")
                    '.TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "GP.bintModifiedDate"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                End With
                ds = objDocuments.GetDocumentFlow
                dtDocuments = ds.Tables(0)
                ' lblRecordCount.Text = objDocuments.TotalRecords
                'tblpaging.Visible = False 'paging is not working hiding it.
                'If objDocuments.TotalRecords = 0 Then
                '    lblRecordCount.Text = 0
                'Else
                '    lblRecordCount.Text = String.Format("{0:#,###}", objDocuments.TotalRecords)
                'Dim strTotalPage As String()
                'Dim decTotalPage As Decimal
                'decTotalPage = lblRecordCount.Text / Session("PagingRows")
                'decTotalPage = Math.Round(decTotalPage, 2)
                'strTotalPage = CStr(decTotalPage).Split(".")

                'If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                '    lblTotal.Text = strTotalPage(0)
                '    txtTotalPage.Text = strTotalPage(0)
                'Else
                '    lblTotal.Text = strTotalPage(0) + 1
                '    txtTotalPage.Text = strTotalPage(0) + 1
                'End If
                'txtTotalRecords.Text = lblRecordCount.Text
                'End If

                If ds.Relations.Count < 1 Then
                    ds.Tables(0).TableName = "Documents"
                    ds.Tables(1).TableName = "DocumentFlow"
                    ds.Relations.Add("Document", ds.Tables(0).Columns("numGenericDocId"), ds.Tables(1).Columns("numGenericDocId"))
                End If
                gvDocList.DataSource = ds
                gvDocList.DataBind()

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtDocuments.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                PersistTable.Add(PersistKey.SearchValue, txtKeyWord.Text)
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Save()
             Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDocumentFlow()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDocumentFlow()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

      
        Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
            Try
                lngCategory = ddlCategory.SelectedItem.Value
                BindDocumentFlow()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDocumentFlow()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Function ReturnDate(ByVal CloseDate) As String
            Try
                If CloseDate Is Nothing Then Exit Function
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b></font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                        temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = strTargetResolveDate
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Private Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
            lngStatus = ddlStatus.SelectedItem.Value
        End Sub
      
    End Class
End Namespace