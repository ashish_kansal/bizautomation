﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts


Partial Public Class frmBizDocAppRule
    Inherits BACRMPage
    Dim objContacts As CContacts
    Dim objCommon As CCommon
    Dim objTempDt As New DataTable
    Dim objBizDocRule As New BizDocRules
    Private Sub FillEmployee()
        Dim objRow As DataRow
        Dim dtEmployeeList As New DataTable
        Dim dtEmp As New DataTable
        Dim objEmp As DataRow

        objContacts = New CContacts
        objCommon = New CCommon
        objContacts.DomainID = Session("DomainID")
        dtEmployeeList = objContacts.EmployeeList
        lstAvailablefld.Items.Clear()
        lstSelectedfld.Items.Clear()

        dtEmp.Columns.Add("numContactID")
        dtEmp.Columns.Add("vcUserName")

        objEmp = dtEmp.NewRow
        objEmp.Item("numContactID") = 0
        objEmp.Item("vcUserName") = "-Select One--"
        dtEmp.Rows.Add(objEmp)



        For Each objRow In dtEmployeeList.Rows
            lstAvailablefld.Items.Add(objRow.Item("vcUserName"))
            lstAvailablefld.Items(lstAvailablefld.Items.Count - 1).Value = objRow.Item("numContactID")
            objEmp = dtEmp.NewRow
            objEmp.Item("numContactID") = objRow.Item("numContactID")
            objEmp.Item("vcUserName") = objRow.Item("vcUserName")
            dtEmp.Rows.Add(objEmp)
            dtEmp.AcceptChanges()
        Next

        ddlEmployee.DataTextField = "vcUserName"
        ddlEmployee.DataValueField = "numContactID"
        ddlEmployee.DataSource = dtEmp
        ddlEmployee.DataBind()
    End Sub
    Private Sub FillList()
        Try

            Dim dtBizDoc As New DataTable

            FillEmployee()

           
            objCommon.sb_FillComboFromDBwithSel(ddlBizDoc1, 27, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlBizDoc2, 27, Session("DomainID"))

            'Changed by Sachin Sadhu||Date:16thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            objCommon.sb_FillComboFromDBwithSel(dllBizDocStatus1, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc1.SelectedValue))
            objCommon.sb_FillComboFromDBwithSel(dllBizDocStatus2, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc1.SelectedValue))
            objCommon.sb_FillComboFromDBwithSel(dllBizDocStatus3, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc2.SelectedValue))
            'end of code

            objCommon.sb_FillComboFromDBwithSel(ddlActionType1, 32, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlActionType2, 32, Session("DomainID"))

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dddlBizDoc1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc1.SelectedIndexChanged
        Try
            'Changed by Sachin Sadhu||Date:17thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            Dim objCommon1 As New CCommon
            objCommon1.sb_FillComboFromDBwithSel(dllBizDocStatus1, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc1.SelectedValue))
            objCommon1.sb_FillComboFromDBwithSel(dllBizDocStatus2, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc1.SelectedValue))
            'End of code

            'objCommon.sb_FillComboFromDBwithSel(ddlFBABizDocStatus, 11, Session("DomainID"), CCommon.ToString(ddlFBABizDoc.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dddlBizDoc2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc2.SelectedIndexChanged
        Try
            'Changed by Sachin Sadhu||Date:17thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            Dim objCommon1 As New CCommon
            objCommon1.sb_FillComboFromDBwithSel(dllBizDocStatus3, 11, Session("DomainID"), CCommon.ToString(ddlBizDoc2.SelectedValue))
            'End of code

            'objCommon.sb_FillComboFromDBwithSel(ddlFBABizDocStatus, 11, Session("DomainID"), CCommon.ToString(ddlFBABizDoc.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        chkBizDoc.Attributes.Add("OnCheckedChanged", "chkBizDoc_CheckedChanged")
        If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access
           
            objCommon = New CCommon
            GetUserRightsForPage(13, 29)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If
            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
        End If
        If Not IsPostBack Then
            FillList()
            GetBizDocRule()
            FillBizDocStautsRule()
            ddlBizDoc2.Enabled = False
            dllBizDocStatus3.Enabled = False
            ddlActionType2.Enabled = False
            ddlEmployee.Enabled = False

            dgRule.DataSource = Session("dtTempBizDocRule")
            dgRule.DataBind()
            dgBizDocStatus.DataSource = Session("dtTempBizDocStatus")
            dgBizDocStatus.DataBind()
        End If

        txtAmoutFrom.Attributes.Add("onkeypress", "CheckNumber(1,event);")
        txtAmountTo.Attributes.Add("onkeypress", "CheckNumber(1,event);")
    End Sub

    Private Sub FillBizDocStautsRule()
        Try

            With objBizDocRule
                .DomainID = Session("DomainID")
                .GetBizStatusRule()
                Session("dtTempBizDocStatus") = .BizDocStatusDt
            End With
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetBizDocRule()
        Try
            objBizDocRule.DomainID = Session("DomainID")
            objTempDt = objBizDocRule.GetBizDocRuleDetails
            Session("dtTempBizDocRule") = objTempDt
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    '(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    Protected Sub btnAdd_Click()
        Try
            Dim objRow As DataRow

            Dim dtEmployeeList As New DataTable
            objContacts = New CContacts
            objCommon = New CCommon

            objContacts.DomainID = Session("DomainID")
            dtEmployeeList = objContacts.EmployeeList
            'lstAvailablefld.Items.Clear()

            'Response.Write(lstAvailablefld.Items.Count)
            'Response.Write(lstSelectedfld.Items.Count)
            'For Each objRow In dtEmployeeList.Rows
            '    lstSelectedfld.Items.Add(objRow.Item("vcUserName"))
            '    lstSelectedfld.Items(lstSelectedfld.Items.Count - 1).Value = objRow.Item("numContactID")
            'Next

            If Not hdnEmpID.Value.Contains(",") Then
                litMessage.Text = "Add Atleast One Employee"
                lstSelectedfld.Focus()
                Exit Sub
            End If

            If ddlBizDoc1.SelectedIndex = 0 Then
                litMessage.Text = "Select A BizDoc Type"
                ddlBizDoc1.Focus()
                Exit Sub
            End If

            If IsNumeric(txtAmoutFrom.Text) = False Or IsNumeric(txtAmountTo.Text) = False Then
                litMessage.Text = "Enter Valid Range Amount"
                txtAmoutFrom.Focus()
                Exit Sub
            End If

            If txtAmoutFrom.Text = vbNullString Then
                litMessage.Text = "Enter Amount Range From"
                txtAmoutFrom.Focus()
                Exit Sub
            End If

            If txtAmountTo.Text = vbNullString Then
                litMessage.Text = "Enter Amount Range To"
                txtAmountTo.Focus()
                Exit Sub
            End If

            If ddlActionType1.SelectedIndex = 0 Then
                litMessage.Text = "Select An Action Type"
                ddlActionType1.Focus()
                Exit Sub
            End If

            If dllBizDocStatus2.SelectedIndex = 0 Then
                litMessage.Text = "Select A Biz Doc Status"
                dllBizDocStatus2.Focus()
                Exit Sub
            End If



            If chkBizDoc.Checked = True Then
                If ddlBizDoc2.SelectedIndex = 0 Then
                    litMessage.Text = "Select A BizDoc Type"
                    ddlBizDoc2.Focus()
                    Exit Sub
                End If

                If dllBizDocStatus3.SelectedIndex = 0 Then
                    litMessage.Text = "Select A Biz Doc Status"
                    dllBizDocStatus3.Focus()
                    Exit Sub
                End If

                If ddlEmployee.SelectedIndex = 0 Then
                    litMessage.Text = "Select An Employee"
                    ddlEmployee.Focus()
                    Exit Sub
                End If

                If ddlActionType2.SelectedIndex = 0 Then
                    litMessage.Text = "Select An Action Type"
                    ddlActionType2.Focus()
                    Exit Sub
                End If
            End If

            Dim strEmployeeId As String
            Dim strEmployee As String
            Dim intLoop As Integer

            strEmployeeId = ""
            strEmployee = ""

            'For intLoop = 0 To strEmpID.Length - 1
            '    strEmployeeId = strEmployeeId & "," & strEmpID(intLoop)
            '    strEmployee = strEmployee & "," & strEmpID(intLoop)
            'Next

            strEmployee = hdnEmpName.Value.TrimEnd(",")
            strEmployeeId = hdnEmpID.Value.TrimEnd(",")



            With objBizDocRule
                .BizDocRuleDt = Session("dtTempBizDocRule")
                .BizDocTypeID = ddlBizDoc1.SelectedItem.Value
                .BizDocType = ddlBizDoc1.SelectedItem.Text
                .RangeFrom = txtAmoutFrom.Text
                .RangeTo = txtAmountTo.Text
                .BizDocCreated = IIf(chkAmount1.Checked = True, 1, 0)
                .BizDocPaidFull = IIf(chkBizDoc1.Checked = True, 1, 0)
                .ActionTypeID = ddlActionType1.SelectedItem.Value
                .ActionType = ddlActionType1.SelectedItem.Text
                .ApproveDocStatusID = dllBizDocStatus1.SelectedItem.Value
                .BizDocApprovalStatus = dllBizDocStatus1.SelectedItem.Text
                .DeclainDocStatusID = dllBizDocStatus2.SelectedItem.Value
                .BizDocDeclainStatus = dllBizDocStatus2.SelectedItem.Text
                .SelectedEmployeeID = strEmployeeId
                .SelectedEmployee = strEmployee
                If objBizDocRule.ValidationRulesBizDoc = False Then
                    litMessage.Text = "Same Conditioned BizDoc Rule Is Already Exist"
                    Exit Sub
                End If
                .AddEditBizDocGrid()
                Session("dtTempBizDocRule") = .BizDocRuleDt
                dgRule.DataSource = Session("dtTempBizDocRule")
                dgRule.DataBind()
            End With

            ddlActionType1.SelectedIndex = 0
            ddlBizDoc1.SelectedIndex = 0
            dllBizDocStatus1.SelectedIndex = 0
            dllBizDocStatus2.SelectedIndex = 0
            txtAmountTo.Text = ""
            txtAmoutFrom.Text = ""
            chkAmount1.Checked = False
            chkBizDoc1.Checked = False

            FillEmployee()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            objBizDocRule.BizDocRuleDt = Session("dtTempBizDocRule")
            objBizDocRule.BizDocStatusDt = Session("dtTempBizDocStatus")
            objBizDocRule.DomainID = Session("DomainID")
            If objBizDocRule.SaveBizDocRule <> 0 Then
                litMessage.Text = "BizDoc Approval Rules Saved Successfuly"
                FillEmployee()
            Else
                litMessage.Text = "BizDoc Approval Rules Not Saved Successfuly"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            objBizDocRule.DomainID = Session("DomainID")
            objBizDocRule.ModeID = -1

            If objBizDocRule.DeleteBizDocRule = 1 Then
                litMessage.Text = "BizDoc Approval Rules Deleted Successfuly"
                FillList()
                GetBizDocRule()
                FillBizDocStautsRule()
                dgRule.DataSource = Session("dtTempBizDocRule")
                dgRule.DataBind()
                FillEmployee()
            Else
                litMessage.Text = "BizDoc Approval Rules Not Deleted Successfuly"
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub dgRule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRule.ItemCommand
        Dim objRow() As DataRow
        Dim lngFrom As Long
        Dim lngTo As Long
        Dim strRange() As String

        Dim objBizDocRule As New BizDocRules


        If e.CommandName = "Delete" Then
            objTempDt = Session("dtTempBizDocRule")
            'objTempDt.Rows.Find(CType(e.Item.Cells(0).FindControl("lblOppItemCode"), Label).Text).Delete()
            objRow = objTempDt.Select("numBizDocTypeID=" & e.Item.Cells(9).Text & " AND EmployeeID='" & e.Item.Cells(13).Text & "' AND Range='" & e.Item.Cells(1).Text & "'")

            Dim intLoop As Integer
            For intLoop = 0 To UBound(objRow)
                objTempDt.Rows.Remove(objRow(intLoop))
                objTempDt.AcceptChanges()

                If Len(e.Item.Cells(1).Text) > 2 Then
                    strRange = Split(e.Item.Cells(1).Text, "to")
                    lngFrom = CType(strRange(0), Long)
                    lngTo = CType(strRange(1), Long)
                    With objBizDocRule
                        .DomainID = Session("DomainID")
                        .BizDocTypeID = e.Item.Cells(9).Text
                        .RangeFrom = lngFrom
                        .RangeTo = lngTo
                        Call .DeleteBizDocRuleTran()
                    End With
                End If

            Next

            Session("dtTempBizDocRule") = objTempDt
            dgRule.DataSource = Session("dtTempBizDocRule")
            dgRule.DataBind()
            FillEmployee()
        End If
    End Sub
    Private Sub DeleteRule(ByVal numBizDocTypeID As String, ByVal EmployeeID As String, ByVal Range As String)
        Try
            Dim objRow() As DataRow
            Dim lngFrom As Long
            Dim lngTo As Long
            Dim strRange() As String

            Dim objBizDocRule As New BizDocRules



            objTempDt = Session("dtTempBizDocRule")
            'objTempDt.Rows.Find(CType(e.Item.Cells(0).FindControl("lblOppItemCode"), Label).Text).Delete()
            'objRow = objTempDt.Select("numBizDocTypeID=" & e.Item.Cells(9).Text & " AND EmployeeID='" & e.Item.Cells(13).Text & "' AND Range='" & e.Item.Cells(1).Text & "'")
            objRow = objTempDt.Select("numBizDocTypeID=" & numBizDocTypeID & " AND EmployeeID='" & EmployeeID & "' AND Range='" & Range & "'")

            Dim intLoop As Integer
            For intLoop = 0 To UBound(objRow)
                objTempDt.Rows.Remove(objRow(intLoop))
                objTempDt.AcceptChanges()

                If Len(Range) > 2 Then
                    strRange = Split(Range, "to")
                    lngFrom = CType(strRange(0), Long)
                    lngTo = CType(strRange(1), Long)
                    With objBizDocRule
                        .DomainID = Session("DomainID")
                        .BizDocTypeID = numBizDocTypeID
                        .RangeFrom = lngFrom
                        .RangeTo = lngTo
                        Call .DeleteBizDocRuleTran()
                    End With
                End If

            Next

            Session("dtTempBizDocRule") = objTempDt
            dgRule.DataSource = Session("dtTempBizDocRule")
            dgRule.DataBind()
            FillEmployee()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnAddDocStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDocStatus.Click
        Try
            If ddlBizDoc2.SelectedIndex = 0 Then
                litMessage.Text = "Select BizDoc Type"
                Exit Sub
            End If

            If dllBizDocStatus3.SelectedIndex = 0 Then
                litMessage.Text = "Select BizDoc Status"
                Exit Sub
            End If

            If ddlEmployee.SelectedIndex = 0 Then
                litMessage.Text = "Select Employee"
                Exit Sub
            End If

            If ddlActionType2.SelectedIndex = 0 Then
                litMessage.Text = "Select Action Type"
                Exit Sub
            End If


            With objBizDocRule
                .BizDocStatusDt = Session("dtTempBizDocStatus")
                .BizDocTypeID = ddlBizDoc2.SelectedItem.Value
                .BizDocStatusID = dllBizDocStatus3.SelectedItem.Value
                .ActionTypeID = ddlActionType2.SelectedItem.Value
                .EmployeeID = ddlEmployee.SelectedItem.Value
                .BizDocTypeValue = ddlBizDoc2.SelectedItem.Text
                .BizDocStatusValue = dllBizDocStatus3.SelectedItem.Text
                .ActionTypeValue = ddlActionType2.SelectedItem.Text
                .EmployeeValue = ddlEmployee.SelectedItem.Text
                If .ValidationBizDocStatus = False Then
                    litMessage.Text = "Same Rule For The BizDoc Status Is Already Exist"
                    Exit Sub
                End If
                .AddEditBizDocStatusGrid()
                Session("dtTempBizDocStatus") = .BizDocStatusDt
                dgBizDocStatus.DataSource = Session("dtTempBizDocStatus")
                dgBizDocStatus.DataBind()

                ddlBizDoc2.SelectedIndex = 0
                dllBizDocStatus3.SelectedIndex = 0
                ddlActionType2.SelectedIndex = 0
                ddlEmployee.SelectedIndex = 0
                ddlBizDoc2.Focus()
            End With

            FillEmployee()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub chkBizDoc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkBizDoc.CheckedChanged
        If chkBizDoc.Checked = False Then
            ddlBizDoc2.Enabled = False
            dllBizDocStatus3.Enabled = False
            ddlActionType2.Enabled = False
            ddlEmployee.Enabled = False
            FillEmployee()
        Else
            FillEmployee()
            ddlBizDoc2.Enabled = True
            dllBizDocStatus3.Enabled = True
            ddlActionType2.Enabled = True
            ddlEmployee.Enabled = True
            ddlBizDoc2.Focus()
        End If
    End Sub

    Private Sub dgBizDocStatus_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocStatus.ItemCommand
        Dim objRow() As DataRow
        Dim objBizDocRule As New BizDocRules


        If e.CommandName = "DeleteDoc" Then
            objBizDocRule.BizDocStatusDt = Session("dtTempBizDocStatus")
            'objTempDt.Rows.Find(CType(e.Item.Cells(0).FindControl("lblOppItemCode"), Label).Text).Delete()

            objRow = objBizDocRule.BizDocStatusDt.Select("BizDocTypeID=" & e.Item.Cells(4).Text & " AND BizDocStatusID=" & e.Item.Cells(5).Text & " AND EmployeeID=" & e.Item.Cells(6).Text & " AND ActionTypeID=" & e.Item.Cells(7).Text)

            Dim intLoop As Integer
            For intLoop = 0 To UBound(objRow)
                objBizDocRule.BizDocStatusDt.Rows.Remove(objRow(intLoop))
                objBizDocRule.BizDocStatusDt.AcceptChanges()

                With objBizDocRule
                    .DomainID = Session("DomainID")
                    .BizDocTypeID = e.Item.Cells(4).Text
                    .BizDocStatusID = e.Item.Cells(5).Text
                    .EmployeeID = e.Item.Cells(6).Text
                    .ActionTypeID = e.Item.Cells(7).Text
                    Call .DeleteBizDocStatus()
                End With

            Next

            Session("dtTempBizDocStatus") = objBizDocRule.BizDocStatusDt
            dgBizDocStatus.DataSource = Session("dtTempBizDocStatus")
            dgBizDocStatus.DataBind()

            Call FillEmployee()
        End If
    End Sub

    Private Sub dgBizDocStatus_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocStatus.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim btnDeleteDoc As Button
            btnDeleteDoc = e.Item.FindControl("btnDeleteDoc")
            'objRow = objTempDt.Select("numBizDocTypeID=" & e.Item.Cells(9).Text & " AND EmployeeID='" & e.Item.Cells(13).Text & "' AND Range='" & e.Item.Cells(1).Text & "'")
            btnDeleteDoc.Attributes.Add("onclick", "return DeleteRule1()")
            btnDeleteDoc.CommandName = "DeleteDoc"
        End If
    End Sub

    Private Sub dgRule_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRule.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim btnDeleteAction As Button
            btnDeleteAction = e.Item.FindControl("btnDeleteDocRule")
            'objRow = objTempDt.Select("numBizDocTypeID=" & e.Item.Cells(9).Text & " AND EmployeeID='" & e.Item.Cells(13).Text & "' AND Range='" & e.Item.Cells(1).Text & "'")
            btnDeleteAction.Attributes.Add("onclick", "return DeleteRule1()")
            btnDeleteAction.CommandName = "Delete"

        End If
    End Sub
End Class