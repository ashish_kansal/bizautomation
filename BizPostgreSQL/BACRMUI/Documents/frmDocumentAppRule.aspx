﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDocumentAppRule.aspx.vb"
    Inherits="BACRM.UserInterface.AutomatonRules.frmDocumentAppRule" MasterPageFile="~/common/DetailPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Document/BizDoc Approval Rule</title>
    <script src="../JavaScript/json2.js" type="text/javascript"></script>
    <script src="../JavaScript/AutomationRule.js" type="text/javascript"></script>
    <style type="text/css">
        .selected
        {
            background-color: Gray;
            color: White;
        }
        .seperatorf
        {
            border-right: 2px solid brown;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function ChangeBasedOn() {
            var CHK = document.getElementById("cblBasedOn");
            var checkbox = CHK.getElementsByTagName("input");
            //var label = CHK.getElementsByTagName("label");
            var counter = 0;

            $('#ddlAttribute').attr('disabled', true);

            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked) {
                    counter = counter + 1;

                    if (i == 3)
                        $('#ddlAttribute').attr('disabled', false);
                    //console.log("Selected = " + label[i].innerHTML);
                }
            }
            if (counter == 0) {
                return false;
            }
            return true;
        }

        function Save() {
            if (document.getElementById("ddlRuleModule").value == 0) {
                alert("Select Rule Module");
                document.getElementById("ddlRuleModule").focus();
                return false;
            }

            if (document.getElementById("txtRuleName").value == "") {
                alert("Enter rule name");
                document.getElementById("txtRuleName").focus();
                return false;
            }

            if (ChangeBasedOn() == false) {
                alert("Select Rule Based On");
                return false;
            }

            GetConditionJSON();

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return Save()" />
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close"
                OnClientClick="return Save()" />
            <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="button" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Automation Rule
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
     <asp:Table ID="Table3" Width="100%" runat="server" BorderWidth="1" GridLines="None"
        CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table>
                    <tr>
                        <td>
                            Rule Module
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRuleModule" runat="server" CssClass="signup" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rule Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtRuleName" runat="server" Width="200" CssClass="signup" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Rule Description
                        </td>
                        <td>
                            <asp:TextBox ID="txtRuleDesc" runat="server" Width="300" CssClass="signup" MaxLength="500"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Rule Based On
                        </td>
                        <td>
                            <asp:CheckBoxList ID="cblBasedOn" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow"
                                onclick="ChangeBasedOn();" CssClass="signup">
                                <asp:ListItem Value="1">Record is created</asp:ListItem>
                                <asp:ListItem Value="2">Record is modified</asp:ListItem>
                                <asp:ListItem Value="4">Record is deleted</asp:ListItem>
                                <%--<asp:ListItem Value="8">Record attributes change</asp:ListItem>--%>
                            </asp:CheckBoxList>
                            <%-- &nbsp;&nbsp;<asp:DropDownList ID="ddlAttribute" runat="server" CssClass="signup">
        </asp:DropDownList>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRuleStatus" runat="server" CssClass="signup">
                                <asp:ListItem Value="0">Active</asp:ListItem>
                                <asp:ListItem Value="1">InActive</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table class="normal1" id="tblConditionAction" runat="server" visible="false">
                    <tr>
                        <td colspan="2">
                            <br />
                            <br />
                            <b>Conditions and Actions</b>
                            <asp:Button ID="btnAddCondition" runat="server" Text="Add Condition" CssClass="button" />
                            <asp:Button ID="btnAddAction" runat="server" Text="Add Action" CssClass="button" />
                            <asp:Button ID="btnCondition" runat="server" Text="Get Condition" CssClass="button"
                                Style="display: none" />
                            <asp:Button ID="btnQuery" runat="server" Text="Get Query" CssClass="button" Style="display: none" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="query">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="txtExistingJSON" runat="server" Style="display: none">
                </asp:TextBox>
                <asp:TextBox ID="txtExistingQuery" runat="server" Style="display: none"> </asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <script language="javascript" type="text/javascript">
        ChangeBasedOn();
        $(document).ready(function () {
            $('#btnCondition').click(function () {
                //                var query = {};
                //                query = getCondition('.query > table');
                //                var l = JSON.stringify(query);
                //                alert(l);
                //                return false;

                GetConditionJSON();
                return false;
            });

            $('#btnQuery').click(function () {
                //                var con = getCondition('.query >table');
                //                var k = getQuery(con);
                //                alert(k);
                //                return false;

                if ($('.query >table').length != 0) {
                    var len = $('.query >table').length;
                    for (var i = 0; i < len; i++) {
                        var con = getCondition($('.query >table')[i]);
                        var k = getQuery(con);
                        //console.log(k);
                    }
                }
                return false;

            });

            $('#btnAddCondition').click(function () {
                addqueryroot('.query', false);
                return false;
            });

            $('#btnAddAction').click(function () {
                if ($(".query > table").hasClass("selected")) {
                    var Rows = $(".query > table.selected").children().children();
                    var tdAction = $(Rows[1]).find('.tdAction');
                    addAction(tdAction);
                }
                else {
                    //addAction('.query');
                }
                return false;
            });


            //addqueryroot('.query', false);

            if ($('#txtExistingJSON').val().length > 0) {
                var q = [];
                q = $('#txtExistingJSON').val().split("$^$");
                //console.log(q.length);

                for (var i = 0; i < q.length; i++) {
                    //console.log(q[i]);
                    GetQueryUI('.query', JSON.parse(q[i]));
                }
            }
        });

        function GetConditionJSON() {
            if ($('.query >table').length != 0) {
                var len = $('.query >table').length;

                var qJSON = [];
                var qText = [];

                for (var i = 0; i < len; i++) {
                    var query = {};
                    var query = getCondition($('.query >table')[i]);
                    var l = JSON.stringify(query);
                    //console.log(l);
                    qJSON[i] = {};
                    qJSON[i] = l;

                    var k = getQuery(query);
                    //console.log(k);
                    qText[i] = {};
                    qText[i] = k;
                }
                $('#txtExistingJSON').val(qJSON.join("$^$"));
                $('#txtExistingQuery').val(qText.join("$^$"));
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
   
</asp:Content>
