<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false"
    CodeBehind="frmDocuments.aspx.vb" MasterPageFile="~/common/DetailPage.Master"
    Inherits="BACRM.UserInterface.Documents.frmDocuments" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Documents</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link href="../JavaScript/MultiSelect/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../JavaScript/MultiSelect/bootstrap-multiselect.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });
        })
        function pageLoaded() {
            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert('Default Email template can not be deleted !');
            return false;
        }
        //        function OpenLink(Link) {<a href="frmAddSpecificDocuments.aspx">frmAddSpecificDocuments.aspx</a>
        //            window.open(Link);
        //            return false;
        //        }
        function OpenLink(Link) {
            window.open(Link);
            return false;
        }
        function OpenURL(Link) {
            window.open(Link);
            return false;
        }
        function Save(cint) {
            $("#hdnGroups").val($("#ddlGroups").val().toString());
            if (document.form1.txtDocName.value == "") {
                alert("Enter Document Name")
                document.form1.txtDocName.focus()
                return false;
            }
            //if (document.form1.ddlCategory.value == 0) {
            //    alert("Select Document Category")
            //    document.form1.ddlCategory.focus()
            //    return false;
            //}
            //if (document.form1.ddlClass.value == 0) {
            //    alert("Select Document Status")
            //    document.form1.ddlClass.focus()
            //    return false;
            //}
        }
        //        function openApp(a, b) {
        //            var str1 = "../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=" + a + "&DocType=" + b + "&DocName='" + document.form1.txtDocName.value + "','',toolbar=no,titlebar=no,left=300,top=450,width=800,height=400,scrollbars=yes,resizable=yes";
        //            alert(str1);
        //            window.open("../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=" + a + "&DocType=" + b + "&DocName='" + document.form1.txtDocName.value + "','',toolbar=no,titlebar=no,left=300,top=450,width=800,height=400,scrollbars=yes,resizable=yes");
        //            return false;
        //        }
        function openApp(a, b) {

            window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + "&DocName=" + document.form1.txtDocName.value, '', 'toolbar=no,titlebar=no,left=300,top=450,width=800,height=700,scrollbars=yes,resizable=yes')

            return false;

        }


        function MarketingDept() {
            if (document.getElementById('ddlCategory').value == 369) {

                document.getElementById('forMarketingDept').disabled = false
            }
            else {
                document.getElementById('forMarketingDept').disabled = true
            }
            return false
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField" || name == "CustomField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }


        function OnClientLoad(combobox, args) {
            makeUnselectable(combobox.get_element());
        }

        function makeUnselectable(element) {
            $telerik.$("*", element).attr("unselectable", "on");
        }

        function getSelectedItemValue(combobox, args) {

            var value = combobox.get_value();
            var editor = $find("<%=RadEditor1.ClientID%>");
            editor.pasteHtml(value);
        }

        function getSelectedBizDocItemValue(combobox, args) {

            var value = combobox.get_value();
            var editor = $find("<%=RadEditor1.ClientID%>");
            editor.pasteHtml(value);
            if (value == "##BizDoc Template Merge fields without HTML & CSS##") {

                $find("<%= radcmbCustomField.ClientID %>").set_visible(true);
                editor.set_html("");


            }
            else {
                $find("<%= radcmbCustomField.ClientID %>").set_visible(false);
                return false;
            }
            __doPostBack("<%= radcmbBizDocMergeFields.ClientID %>", '');
        }

        function Confirm(sender) {
            var selectedText = $(sender).find("option:selected").text();
            var confirmMsg = confirm("Selecting this module will remove any merge fields already added to your template (because merge fields from two modules, can't be mixed). Are you sure you want to do this ?");
            if (confirmMsg == true) {
                var editor = $find("<%=RadEditor1.ClientID%>");
                editor.set_html("");
                __doPostBack('ddlEmailMergeModule', '');
                return true;
            }
            else {
                var hv = $('#hfModule').val();
                sender.selectedIndex = hv;
                return false;
            }
        }

        function Save() {

            $("#hdnGroups").val($("#ddlGroups").val().toString());
            if ($('#txtDocName').val() == "") {
                alert("Enter Template Name")
                document.getElementById("txtDocName").focus();
                return false;
            }
            if ($('#txtSubject').val() == 0) {
                alert("Enter Subject")
                document.getElementById("txtSubject").focus();
                return false;
            }
            if ($('#ddlEmailMergeModule').val() == 0) {
                alert("Select Module")
                document.getElementById("ddlEmailMergeModule").focus();
                return false;
            }
        }

        function ValidateRestore() {
            if (confirm("Your changes will be replaced by default template. Do you want to proceed?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
    <style>
        .multiselect-group {
            background-color: #3e3e3e !important;
        }

            .multiselect-group a:hover {
                background-color: #3e3e3e !important;
            }

            .multiselect-group a {
                color: #fff !important;
            }

        .multiselect-container > li.multiselect-group label {
            padding: 3px 20px 3px 5px !important;
        }

        .multiselect-container {
            min-width: 250px;
            border: 1px solid #e1e1e1;
        }

        .colRep {
            padding-left: 0px;
            padding-right: 0px;
        }

            .colRep table {
                background: #fff;
            }

        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
                float: left !important;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .colMdoff .form-group {
            margin-top: 12px;
            margin-bottom: 12px;
        }

        .td1class {
            width: 185px !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
            text-align: right;
        }

        .tdtopbottom {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .tdpaddingHorizontal {
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12 colRep">
            <table id="tblMenu" bordercolor="black" cellspacing="0" class="table table-responsive table-bordered" cellpadding="0"
                width="100%" border="0" runat="server">
                <tr>
                    <td class="tr1" align="center">
                        <asp:hyperlink id="hplTransfer" runat="server" visible="true" cssclass="BizLink"
                            text="Record Owner:" tooltip="Transfer Ownership">
                        </asp:hyperlink>
                        &nbsp;
                <asp:label id="lblRecordOwner" runat="server" forecolor="Black"></asp:label>
                    </td>
                    <td align="center" class="leftBorder">
                        <b>Created By: </b>
                        <asp:label id="lblCreatedBy" runat="server" forecolor="Black"></asp:label>
                    </td>
                    <td align="center" class="leftBorder">
                        <b>Last Modified By: </b>
                        <asp:label id="lblLastModifiedBy" runat="server" forecolor="Black"></asp:label>
                    </td>
                    <td class="leftBorder" align="center">
                        <b>Last Checked-Out By,On: </b>
                        <asp:label id="lblLastCheckedOutByOn" runat="server" forecolor="Black"></asp:label>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:button id="btnCheckOut" runat="server" cssclass="btn btn-warning" text="Checkout" visible="false" />
                <asp:button id="btnApprovers" runat="server" cssclass="btn btn-primary" text="List of Approvers" />
                <asp:button id="btnSaveCLose" text="Save &amp; Close" runat="server" onclick="btnSaveCLose_Click" cssclass="btn btn-primary"></asp:button>
                <asp:button id="btnCancel" runat="server" cssclass="btn btn-primary" text="Cancel"></asp:button>
                <asp:button id="btnActDelete" runat="server" cssclass="btn btn-danger" text="X"></asp:button>
                <asp:linkbutton id="lnkDeleteAction" runat="server" visible="false"><font color="#730000">*</font></asp:linkbutton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server" ClientIDMode="Static">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Email Template Name<span style="color: red"> *</span></label>
                <asp:textbox id="txtDocName" runat="server" cssclass="form-control"></asp:textbox>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Category</label>
                <asp:dropdownlist id="ddlCategory" cssclass="form-control" runat="server"></asp:dropdownlist>
            </div>
        </div>
    </div>
    <div class="row" id="divSubject" runat="server">
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Subject<span style="color: red"> *</span></label>
                <asp:textbox id="txtSubject" runat="server" cssclass="form-control"></asp:textbox>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>When sent, update �Last Follow-up� date</label>
                <div>
                    <asp:checkbox id="chkLastFollowUpDate" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10" id="divEmailMrgeModule" runat="server">
        <div class="col-xs-12">
            <div class="form-inline">
                <label>Module<span style="color: red"> *</span></label>
                <asp:dropdownlist id="ddlEmailMergeModule" width="180px" autopostback="true" onselectedindexchanged="ddlEmailMergeModule_SelectedIndexChanged1" cssclass="form-control" runat="server"></asp:dropdownlist>
                <asp:dropdownlist id="ddlBizDocOppType" runat="server" cssclass="form-control" onselectedindexchanged="ddlBizDocOppType_SelectedIndexChanged" autopostback="true" width="100" visible="false">
                    <asp:ListItem Text="-- Select --" Value="0">
                    </asp:ListItem>
                    <asp:ListItem Text="Sales" Value="1">
                    </asp:ListItem>
                    <asp:ListItem Text="Purchase" Value="2">
                    </asp:ListItem>
                </asp:dropdownlist>
                <asp:dropdownlist id="ddlBizDocType" runat="server" cssclass="form-control" autopostback="true" onselectedindexchanged="ddlBizDocType_SelectedIndexChanged" width="160" visible="false"></asp:dropdownlist>
                <asp:dropdownlist id="ddlBizDocTemplate" onselectedindexchanged="ddlBizDocTemplate_SelectedIndexChanged" width="160" runat="server" cssclass="form-control" autopostback="true" visible="false"></asp:dropdownlist>
                <telerik:RadComboBox ID="radcmbMergerFields" Visible="true" DropDownWidth="280" Width="200" ExpandDirection="up" runat="server" OnClientSelectedIndexChanged="getSelectedItemValue" OnClientLoad="OnClientLoad">
                </telerik:RadComboBox>
                <telerik:RadComboBox ID="radcmbBizDocMergeFields" AutoPostBack="true" Visible="false" DropDownWidth="300" Width="100" ExpandDirection="up" runat="server" OnSelectedIndexChanged="radcmbBizDocMergeFields_SelectedIndexChanged">
                    <Items>
                        <telerik:RadComboBoxItem Value="0" Text="--Select Option--" />
                        <telerik:RadComboBoxItem Value="##BizDocTemplateFromGlobalSettings##" Text="BizDoc HTML & CSS Template" />
                        <telerik:RadComboBoxItem Value="##BizDoc Template Merge fields without HTML & CSS##" Text="BizDoc Template Merge fields without HTML & CSS" />
                    </Items>
                </telerik:RadComboBox>
                <telerik:RadComboBox ID="radcmbCustomField" DropDownWidth="250" Visible="true" Width="200" runat="server" ExpandDirection="up" OnClientSelectedIndexChanged="getSelectedItemValue" OnClientLoad="OnClientLoad"></telerik:RadComboBox>
                <asp:hiddenfield id="hfModule" runat="server" />
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-sm-12 col-md-6">
            <div class="form-inline">
                <div class="form-group">
                    <label>Add Category & Email Templates to users in the following permission groups</label>
                    <asp:dropdownlist id="ddlGroups" cssclass="option-droup-multiSelection-Group" multiple="multiple" runat="server"></asp:dropdownlist>
                    <asp:hiddenfield id="hdnGroups" runat="server" value="0" />
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-inline">
                <div class="form-group">
                    <label><asp:checkbox id="chkFollowUpStatus" style="float: left" runat="server" />&nbsp;&nbsp;Change Follow-up Status to</label>
                    <asp:dropdownlist id="ddlFollowupStatus" cssclass="form-control" runat="server"></asp:dropdownlist>
                    <asp:button id="btnRestoreToDefault" runat="server" text="Restore Default Template" cssclass="btn btn-flat btn-warning" visible="false" onclientclick="return ValidateRestore();" onclick="btnRestoreToDefault_Click" />
                </div>

            </div>
        </div>
    </div>
    <div class="row padbottom10" id="divHTMLEditor" runat="server">
        <div class="col-xs-12">
            <telerik:RadEditor ID="RadEditor1" runat="server" Height="350px" Width="100%" OnClientPasteHtml="OnClientPasteHtml"
                OnClientCommandExecuting="OnClientCommandExecuting" ToolsFile="~/Marketing/EditorTools.xml">
            </telerik:RadEditor>
        </div>
    </div>
    <div class="row padbottom10" id="divSimpleEditor" runat="server">
        <div class="col-xs-12">
            <asp:textbox id="txtDesc" runat="server" Width="100%" textmode="MultiLine" Rows="10" cssclass="form-control"></asp:textbox>
        </div>
    </div>
    <asp:panel runat="server" id="pnlDocumentUpload">
        <div class="row">
            <div class="col-md-12" id="trUploadFile" runat="server">
                <div class="form-group">
                    <label>
                        Upload Document from Your Local Network</label>
                        <input class="form-control" id="fileupload" type="file" name="fileupload" runat="server">
                </div>
            </div>
            <div class="col-md-12 text-center" id="trOr" runat="server">
                Or
            </div>
            <div class="col-md-12" id="trURLOfDocument" runat="server">
                <div class="form-group">
                    <label>Path/URL to Document</label>
                    <asp:TextBox ID="txtPath" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </asp:panel>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3" style="text-decoration: underline">Approval Status</label>
                <div class="col-md-3">
                    Pending:
                    <asp:label id="lblPending" runat="server"></asp:label>
                </div>
                <div class="col-md-3">
                    Approved:
                    <asp:label id="lblApproved" runat="server"></asp:label>
                </div>
                <div class="col-md-3">
                    Declined:
                    <asp:label id="lblDeclined" runat="server"></asp:label>
                </div>
            </div>
        </div>
    </div>
    <asp:panel id="pnlApprove" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-4">Comments</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="txtComment" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-4">Document is Ready for Approval</label>
                    <div class="col-md-8">
                        <asp:Button ID="btnApprove" runat="server" CssClass="btn btn-primary" Text="Approve" />
                        <asp:Button ID="btnDecline" runat="server" CssClass="btn btn-danger" Text="Decline" />
                    </div>
                </div>
            </div>
        </div>
    </asp:panel>
    <div class="row">
        <div class="col-xs-12 text-center">
            <asp:literal id="litMessage" runat="server"></asp:literal>
        </div>
    </div>
    <asp:textbox id="txtRecOwner" runat="server" style="display: none"></asp:textbox>
    <asp:hiddenfield id="hdnCheckoutStatus" runat="server" />
    <asp:hiddenfield id="hdnLastCheckedOutBy" runat="server" />
    <asp:hiddenfield id="hdnSystemTemplateName" runat="server" />

    <asp:hiddenfield id="hdnDocCategory" runat="server" />
    <asp:hiddenfield id="hdnDocStatus" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Email Template Profile
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:label runat="server" id="lblDownloadFile" text="Download file : " visible="false"></asp:label>
    <asp:hyperlink id="hplDoc" runat="server" cssclass="hyperlink" target="_blank">
													Open Document</asp:hyperlink>
    <asp:updateprogress id="UpdateProgress" runat="server" clientidmode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:updateprogress>
</asp:Content>
