Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports System.IO
Namespace BACRM.UserInterface.Documents

    Public Class frmGenDocPopUp
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblOppr As System.Web.UI.WebControls.Table
        Protected WithEvents txtDocName As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlCategory As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlClass As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtDesc As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtPath As System.Web.UI.WebControls.TextBox
        Protected WithEvents fileupload As System.Web.UI.HtmlControls.HtmlInputFile
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Dim strDocName As String                                        'To Store the information where the File is stored.
        Dim strFileType As String
        Dim strFileName As String
        Dim URLType As String
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Dim lngRecID As Long
        Dim lngDocID As Long
        Dim strType As String
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngRecID = CCommon.ToLong(GetQueryStringVal("RecID")) ' RecordID
                strType = GetQueryStringVal("Type")
                If Not IsPostBack Then
                    Session("Help") = "Documents"


                    GetUserRightsForPage(14, 1)
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSave.Visible = False
                        Exit Sub
                    Else : btnSave.Visible = True
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlClass, 28, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                End If
                btnSave.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub UploadFile()
            Try
                Dim strFName As String()
                Dim strFilePath As String
                If (fileupload.PostedFile.ContentLength > 0) Then
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    strDocName = strFName(0)                  'Getting the Name of the File without Extension
                    'strFilePath = Server.MapPath("Docs") & "\" & strFileName
                    fileupload.PostedFile.SaveAs(strFilePath)
                    strFileName = strFileName
                    URLType = "L"
                Else
                    strFileName = txtPath.Text    'Getting the File Name
                    URLType = "U"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If CCommon.ToLong(Session("DomainId")) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                End If
                UploadFile()
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = ddlClass.SelectedItem.Value
                    .DocCategory = ddlCategory.SelectedItem.Value
                    .FileType = strFileType
                    .DocName = txtDocName.Text
                    .DocDesc = txtDesc.Text
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngRecID
                    .DocumentType = IIf(lngRecID > 0, 2, 1) '1=generic,2=specific
                    arrOutPut = .SaveDocuments()
                End With
                Session("DocID") = arrOutPut(0)

                If lngRecID > 0 Then
                    If GetQueryStringVal("frm") = "frmSpecDocuments" Then
                        Response.Redirect("../Documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID & "&", False)
                    End If
                Else
                    Dim strScript As String = "window.opener.reDirectPage('../Documents/frmDocuments.aspx?DocId=" & Session("DocID") & "&frm=DocList&SI1=" & ddlClass.SelectedValue & "&SI2=" & ddlCategory.SelectedValue & "&TabID=1');self.close();"
                    If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                        ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript, True)
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace


