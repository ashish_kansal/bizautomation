<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmDocApprovers.aspx.vb"
    Inherits=".frmDocApprovers" MasterPageFile="~/common/Popup.Master" Title="Approval List"
    ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function Save() {
            if ($find('radCmbCompany').get_value() == "" || $find('radCmbCompany').get_value() == "0") {
                alert("Select Customer or Vendor")
                return false;
            }
            if (document.getElementById("ddlContacts").value == "0") {
                alert("Select Contact")
                document.getElementById("ddlContacts").focus()
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table id="Table3" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="button"></asp:Button>
                    </td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="2" width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Approval List
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="750px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%" border="0" class="aspTable" id="tblAdd" runat="server">
                    <tr>
                        <td width="120" align="right" nowrap>Customer or Vendor<font color="#ff3333"> *</font>
                        </td>
                        <td>
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Contact<font color="#ff3333"> *</font>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContacts" runat="server" Width="200" CssClass="signup">
                            </asp:DropDownList>
                            <asp:Button ID="btnAdd" runat="server" Text="Add Approver" CssClass="button" OnClientClick="javascript:return Save()" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgApprovers" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numDocID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numContactID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="cDocType"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Name" HeaderText="Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcCompanyName" HeaderText="Company"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Status" HeaderText="Status"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcComment" HeaderText="Comments"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="On">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "dtApprovedOn"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    OnClientClick="return DeleteRecord();"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
