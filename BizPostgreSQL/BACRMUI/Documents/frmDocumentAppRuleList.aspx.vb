﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmDocumentAppRuleList
    Inherits BACRMPage
    Dim objAutomatonRule As AutomatonRule

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindGrid()
        Try
            Dim dtTable As DataTable
            objAutomatonRule = New AutomatonRule
            objAutomatonRule.DomainID = Session("DomainID")
            objAutomatonRule.byteMode = 1

            Dim dsList As DataSet

            dsList = objAutomatonRule.GetRuleMaster
            dtTable = dsList.Tables(0)

            gvRule.DataSource = dtTable
            gvRule.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvRule_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRule.RowCommand
        Try
            If e.CommandName = "DeleteRecord" Then
                If objAutomatonRule Is Nothing Then objAutomatonRule = New AutomatonRule
                objAutomatonRule.DomainID = Session("DomainID")
                objAutomatonRule.RuleID = e.CommandArgument
                objAutomatonRule.byteMode = 1

                objAutomatonRule.ManageRuleMaster()
                BindGrid()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvRule_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRule.RowDataBound
        Try
            If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Row.FindControl("btnDelete")

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class