<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmDocList.aspx.vb"
    Inherits="BACRM.UserInterface.Documents.frmDocList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Documents In Workflow</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script language="javascript">

        function DeleteAccount() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteTemplate() {
            alert("Default Email template can not be deleted !");
            return false;
        }

        function OpenET() {
            window.open('../Marketing/frmEmailTemplate.aspx', '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
        }

        function reLoad() {
            document.getElementById("btnGo1").click();
        }

        function beforesort() {
            var grid = igtbl_getGridById('UltraWebGrid1');
            grid.Allowsort = 3;
        }
        function OpenCompany(a, b, c, d, e, f) {
            if (b == 0) {
                window.location.href = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=doclist&DivID=" + a + "&SI1=" + e + "&SI2=" + f
            }

            else if (b == 1) {
                window.location.href = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=doclist&DivID=" + a + "&SI1=" + e + "&SI2=" + f
            }
            else if (b == 2) {
                window.location.href = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=doclist&klds+7kldf=fjk-las&DivId=" + a + "&SI1=" + e + "&SI2=" + f
            }

            return false;
        }

        function OpenBizInvoice(a, b, c, d, e) {
            if (c == 'B') {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            else if (c == 'S') {
                window.open('../documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&tyuTN=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            else if (c == 'D') {
                window.location.href = "../Documents/frmDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocId=" + b + "&frm=DocList&SI1=" + d + "&SI2=" + e
            }
            return false;
        }

        function OpenOpp(a, b, c) {
            if (a > 0) {
                var str;
                window.open('../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=' + a + '&OppType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                //                   window.opener.location.href = str;
            }
            else {
                window.open('../documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&tyuTN=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            return false;
        }

        function OpenContact(a, b, c) {
            window.location.href = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=tickler&fda45s=oijf6d67s&CntId=" + a 

            return false;
        }

        function OpemEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="view-cases">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="normal1" align="right">
                    <label>
                        View
                    </label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="True" CssClass="signup">
                        <asp:ListItem Value="1">My Documents</asp:ListItem>
                        <asp:ListItem Value="3">All Documents</asp:ListItem>
                        <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                        <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                        <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                        <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" CssClass="signup" ID="ddlCategory" Style="display: none">
                    </asp:DropDownList>
                </td>
                <asp:DropDownList runat="server" CssClass="signup" ID="ddlStatus" Style="display: none">
                </asp:DropDownList>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div class="right-input">
        <table align="right">
            <tr>
                <td width="200">
                    <asp:HyperLink ID="hplNew" Visible="false" runat="server" CssClass="hyperlink">Create Email Template</asp:HyperLink>
                </td>
                <td class="normal1" align="right">
                    <label>
                        Keyword Search
                    </label>
                </td>
                <td>
                    <asp:TextBox ID="txtKeyWord" CssClass="signup" runat="server"></asp:TextBox>&nbsp;
                </td>
                <td>
                    <asp:Button ID="btnGo" CssClass="button" runat="server" Text="Go"></asp:Button>
                </td>
            </tr>
        </table>
    </div>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Documents In Workflow
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <telerik:RadGrid ID="gvDocList" runat="server" Width="100%" AutoGenerateColumns="False"
                    GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="tbl aspTable">
                    <MasterTableView DataKeyNames="numGenericDocId" HierarchyLoadMode="Client" DataMember="Documents">
                        <DetailTables>
                            <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                ShowFooter="true" DataKeyNames="numGenericDocId" DataMember="DocumentFlow" CssClass="tbl aspTable">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="numGenericDocId" MasterKeyField="numGenericDocId" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approval Request Date">
                                        <ItemTemplate>
                                            <%# ReturnDate(Eval("Approval Request Date")) %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Company" ItemStyle-Width="20%" DataField="Company">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Name" ItemStyle-Width="20%" DataField="Name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Phone, Ext" DataField="Phone">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Email Address">
                                        <ItemTemplate>
                                            <a href="javascript:void(0)" onclick="OpemEmail('<%# Eval("Email") %>',<%# Eval("numContactID") %>);">
                                                <%#Eval("Email")%>
                                            </a>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Status" DataField="Status">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Comments" DataField="Comments">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Approved or Declained On">
                                        <ItemTemplate>
                                            <%# ReturnDate(DataBinder.Eval(Container.DataItem, "ApprovedOn")) %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Organization" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <a href="javascript:void(0)" onclick="OpenCompany('<%# Eval("numDivisionId") %>','<%# Eval("tintCRMType") %>',1,1,<%#lngStatus%>,<%#lngCategory%>);">
                                        <%#Eval("Organization")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Document Name" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numGenericDocId") %>','<%# Eval("BizDocTypeId") %>',<%# lngStatus %>,<%# lngCategory %>);">
                                        <%#Eval("Document Name")%>
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Document Category" ItemStyle-Width="10%" DataField="Document Category">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="File Type">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hplFileType" NavigateUrl="#" runat="server" CssClass="hyperlink"
                                        Text='<%# Eval("File Type") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Document Status" ItemStyle-Width="15%" DataField="Document Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Pending" DataField="Pending">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Approved" DataField="Approved">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Declined" DataField="Declined">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Created or Last Modified By, On">
                                <ItemTemplate>
                                    <%# Eval("CreatedBy") & "," & ReturnDate(Eval("ModifiedOn"))%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
</asp:Content>
