'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.WebHtmlEditor
Imports System.IO
Imports BACRM.BusinessLogic.Marketing
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Documents
    Public Class frmDocuments : Inherits BACRMPage
        Dim strDocName As String                                        'To Store the information where the File is stored.
        Dim strFileType As String
        Dim strFileName As String
        Dim URLType As String
        Dim lngRecID As Long
        Dim strDocumentSection As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try

                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                'objCommon.CheckdirtyForm(Page)


                If GetQueryStringVal("New") = "1" Then
                    Session("DocID") = Nothing
                    tblMenu.Visible = False
                Else : tblMenu.Visible = True
                End If

                If GetQueryStringVal("DocId") <> "" Then
                    Session("DocID") = GetQueryStringVal("DocId")
                End If
                lngRecID = CCommon.ToLong(GetQueryStringVal("RecID")) ' RecordID
                strDocumentSection = GetQueryStringVal("Type")

                If Session("DocumentRepositaryEnabled") = "1" Then
                    btnCheckOut.Visible = True
                End If

                If Not IsPostBack Then
                    BindBizDocsCategory()
                    BindFollowUpStatus()
                    PopulateGroups()
                    AddToRecentlyViewed(RecetlyViewdRecordType.Document, Session("DocID"))

                    objCommon = New CCommon
                    GetUserRightsForPage(14, 3)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC", False)
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnSaveCLose.Visible = False
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    End If
                    ' objCommon.sb_FillComboFromDBwithSel(ddlClass, 28, Session("DomainID"))
                    ' objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                    If Session("DocID") <> "" Then
                        LoadInformation()
                        btnSaveCLose.Attributes.Add("onclick", "return Save(2)")
                    Else
                        hplDoc.Visible = False
                        btnSaveCLose.Attributes.Add("onclick", "return Save(1)")
                    End If

                    ddlEmailMergeModule.Attributes.Add("onChange", "return Confirm(this);")

                End If
                btnSaveCLose.Attributes.Add("onclick", "return Save()")
                'Set Image Manage and Template Manager
                Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)

                ' ddlCategory.Attributes.Add("onchange", "return MarketingDept()")
                btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                btnApprovers.Attributes.Add("onclick", "return openApp(" & Session("DocID") & ",'D');")  '" & IIf(lngRecID > 0, strDocumentSection, "D") & "')")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Sub PopulateGroups()
            Try
                Dim objUserAccess As UserAccess
                objUserAccess = New UserAccess
                objUserAccess.SelectedGroupTypes = "1,4"
                objUserAccess.DomainID = Session("DomainID")
                ddlGroups.DataTextField = "vcGroupName"
                ddlGroups.DataValueField = "numGroupID"
                ddlGroups.DataSource = objUserAccess.GetGroups
                ddlGroups.DataBind()
                ddlGroups.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindBizDocsCategory()
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = 29
                ddlCategory.Items.Clear()
                ddlCategory.DataSource = objCommon.GetMasterListItemsWithOutDefaultItemsRights
                ddlCategory.DataTextField = "vcData"
                ddlCategory.DataValueField = "numListItemID"
                ddlCategory.DataBind()
                ddlCategory.Items.Insert(0, New ListItem("--Select--", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindFollowUpStatus()
            Try
                Dim dt As New DataTable()
                ddlFollowupStatus.Items.Clear()
                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                objCommon.ListID = CCommon.ToLong(30)
                dt = objCommon.GetMasterListItemsWithRights()
                Dim item As ListItem = New ListItem("-- All --", "0")
                ddlFollowupStatus.Items.Add(item)
                ddlFollowupStatus.AutoPostBack = False
                For Each dr As DataRow In dt.Rows
                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                    ddlFollowupStatus.Items.Add(item)
                Next
            Catch ex As Exception

            End Try
        End Sub
        Sub LoadInformation()
            Try
                Dim dtDocDetails As DataTable
                Dim objDocuments As New DocumentList
                With objDocuments
                    .GenDocID = Session("DocID")
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtDocDetails = .GetDocByGenDocID
                End With
                txtRecOwner.Text = dtDocDetails.Rows(0).Item("numCreatedBy")
                txtDocName.Text = dtDocDetails.Rows(0).Item("vcDocName")
                If Not ddlCategory.Items.FindByValue(dtDocDetails.Rows(0).Item("numCategoryId")) Is Nothing Then
                    ddlCategory.Items.FindByValue(dtDocDetails.Rows(0).Item("numCategoryId")).Selected = True
                End If
                If Not ddlFollowupStatus.Items.FindByValue(dtDocDetails.Rows(0).Item("numFollowUpStatusId")) Is Nothing Then
                    ddlFollowupStatus.Items.FindByValue(dtDocDetails.Rows(0).Item("numFollowUpStatusId")).Selected = True
                End If
                Dim strGroupName As String
                strGroupName = dtDocDetails.Rows(0).Item("vcGroupsPermission")
                Dim strArray As String()
                strArray = strGroupName.Split(",")
                For Each strIndivItem As String In strArray
                    If Not ddlGroups.Items.FindByValue(strIndivItem) Is Nothing Then
                        ddlGroups.Items.FindByValue(strIndivItem).Selected = True
                    End If
                Next
                hdnGroups.Value = strGroupName
                chkLastFollowUpDate.Checked = dtDocDetails.Rows(0).Item("bitLastFollowupUpdate")
                chkFollowUpStatus.Checked = dtDocDetails.Rows(0).Item("bitUpdateFollowupStatus")
                'If Not ddlCategory.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocCategory")) Is Nothing Then
                '    ddlCategory.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocCategory")).Selected = True
                'End If

                hdnDocCategory.Value = dtDocDetails.Rows(0).Item("numDocCategory").ToString() 'Added by Priya (Needs DocCategory while updating Email Template) (14 Feb 2018)

                If dtDocDetails.Rows(0).Item("cUrlType") = "L" Then
                    If Not IsDBNull(dtDocDetails.Rows(0).Item("VcFileName")) Then
                        hplDoc.NavigateUrl = "frmViewAttachment.aspx?docid=" & Session("DocID")  'Attributes.Add("onclick", "return OpenLink('" & CCommon.GetDocumentPath(Session("DomainID")) & dtDocDetails.Rows(0).Item("VcFileName") & "')")
                        hplDoc.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName"))
                    End If
                    lblDownloadFile.Visible = True
                    trUploadFile.Visible = True
                    trURLOfDocument.Visible = False
                Else
                    hplDoc.Attributes.Add("onclick", "return OpenURL('" & dtDocDetails.Rows(0).Item("VcFileName") & "')")
                    txtPath.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName"))
                    trURLOfDocument.Visible = True
                    trUploadFile.Visible = False
                End If
                trOr.Visible = False
                txtSubject.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                'If Not ddlClass.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocStatus")) Is Nothing Then
                '    ddlClass.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocStatus")).Selected = True
                'End If
                hdnDocStatus.Value = dtDocDetails.Rows(0).Item("numDocStatus")  'Added by Priya (Needs DocumentStatus while updating Email Template) (14 Feb 2018)

                'If ddlCategory.SelectedValue = "369" Then
                If hdnDocCategory.Value = "369" Then
                    RadEditor1.Content = CCommon.ToString(dtDocDetails.Rows(0).Item("vcDocdesc"))
                    BindEmailMergeModule()
                    If Not ddlEmailMergeModule.Items.FindByValue(dtDocDetails.Rows(0).Item("numModuleID")) Is Nothing Then
                        ddlEmailMergeModule.Items.FindByValue(dtDocDetails.Rows(0).Item("numModuleID")).Selected = True
                        hfModule.Value = ddlEmailMergeModule.SelectedValue

                        If (ddlEmailMergeModule.SelectedValue = 8) Then
                            ddlBizDocOppType.Visible = True
                            ddlBizDocType.Visible = True
                            ddlBizDocTemplate.Visible = True
                            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select BizDoc Template--", "0"))
                            radcmbBizDocMergeFields.Visible = True
                            radcmbMergerFields.Visible = False

                            ddlBizDocOppType.SelectedValue = dtDocDetails.Rows(0).Item("BizDocOppType").ToString()
                            BindBizDocsType()
                            ddlBizDocType.SelectedValue = dtDocDetails.Rows(0).Item("BizDocType").ToString()
                            BindBizDocsTemplate()
                            ddlBizDocTemplate.SelectedValue = dtDocDetails.Rows(0).Item("BizDocTemplate").ToString()
                            If (RadEditor1.Content.Contains("##BizDocTemplateFromGlobalSettings##")) Then
                                radcmbBizDocMergeFields.SelectedValue = "##BizDocTemplateFromGlobalSettings##"
                                radcmbCustomField.Visible = False
                            Else
                                radcmbBizDocMergeFields.SelectedValue = "##BizDoc Template Merge fields without HTML & CSS##"
                                radcmbCustomField.Visible = True
                                FillMergeFieldsForBizDocs()
                            End If
                        Else
                            ddlBizDocOppType.Visible = False
                            ddlBizDocType.Visible = False
                            ddlBizDocTemplate.Visible = False
                            radcmbBizDocMergeFields.Visible = False
                            radcmbMergerFields.Visible = True
                            radcmbCustomField.Visible = True
                            radcmbCustomField.Items.Clear()
                        End If

                    End If
                    'objDocuments.BindEmailTemplateMergeFields(RadEditor1, ddlEmailMergeModule.SelectedValue)
                    BindEmailTemplateMergeFields(objDocuments, ddlEmailMergeModule.SelectedValue)
                    BindCustomField()

                Else
                    txtDesc.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("vcDocdesc"))
                End If
                hdnCheckoutStatus.Value = dtDocDetails.Rows(0).Item("tintCheckOutStatus") ' 1-chekedout,0-checked in
                hdnLastCheckedOutBy.Value = dtDocDetails.Rows(0).Item("numLastCheckedOutBy")
                ChangeRepositaryStatus()
                ViewState("FileName") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("VcFileName")), "", dtDocDetails.Rows(0).Item("VcFileName"))
                ViewState("URLType") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("cUrlType")), "", dtDocDetails.Rows(0).Item("cUrlType"))
                ViewState("FileType") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcfiletype")), "", dtDocDetails.Rows(0).Item("vcfiletype"))
                lblRecordOwner.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("RecordOwner")), "", dtDocDetails.Rows(0).Item("RecordOwner"))
                lblCreatedBy.Text = dtDocDetails.Rows(0).Item("Created")
                lblLastModifiedBy.Text = dtDocDetails.Rows(0).Item("Modified")
                lblLastCheckedOutByOn.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("CheckedOut"))
                If dtDocDetails.Rows(0).Item("AppReq") = 1 Then
                    pnlApprove.Visible = True
                Else : pnlApprove.Visible = False
                End If
                lblPending.Text = dtDocDetails.Rows(0).Item("Pending")
                lblApproved.Text = dtDocDetails.Rows(0).Item("Approved")
                lblDeclined.Text = dtDocDetails.Rows(0).Item("Declined")
                If CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName")).Contains("#SYS#") Then
                    btnActDelete.Visible = False
                    lnkDeleteAction.Visible = True
                    lnkDeleteAction.Attributes.Add("onclick", "return DeleteMessage();")
                    hdnSystemTemplateName.Value = CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName"))
                End If
                If dtDocDetails.Rows(0).Item("numDocCategory") = 369 Then
                    divEmailMrgeModule.Visible = True
                    divSubject.Visible = True
                    hplDoc.Visible = False
                    pnlDocumentUpload.Visible = False
                    divSimpleEditor.Visible = False
                    divHTMLEditor.Visible = True
                Else
                    divEmailMrgeModule.Visible = False
                    divSubject.Visible = False
                    hplDoc.Visible = True
                    pnlDocumentUpload.Visible = True
                    divSimpleEditor.Visible = True
                    divHTMLEditor.Visible = False
                End If

                BindContactPosition()
                Dim strContactPosition As String() = CCommon.ToString(dtDocDetails.Rows(0).Item("vcContactPosition")).Split(",")
                For k As Integer = 0 To strContactPosition.Length - 1
                    'If radContactPosition.Items.FindItemByValue(strContactPosition(k)) IsNot Nothing Then
                    '    radContactPosition.Items.FindItemByValue(strContactPosition(k)).Checked = True
                    'End If
                Next

                If CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName")).StartsWith("#SYS#") Then
                    btnRestoreToDefault.Visible = True
                Else
                    btnRestoreToDefault.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindContactPosition()
            Try
                Dim objCommon As New CCommon
                'radContactPosition.DataSource = objCommon.GetMasterListItems(41, Session("DomainID"))
                'radContactPosition.DataTextField = "vcData"
                'radContactPosition.DataValueField = "numListItemID"
                'radContactPosition.DataBind()

                'radContactPosition.ShowDropDownOnTextboxClick = True
                'radContactPosition.CheckBoxes = True
                'radContactPosition.Width = 175
                'radContactPosition.EnableTextSelection = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub UploadFile()
            Try
                Dim strFName As String()
                Dim strFilePath As String
                If (fileupload.PostedFile.ContentLength > 0) Then
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    strDocName = strFName(0)                  'Getting the Name of the File without Extension
                    fileupload.PostedFile.SaveAs(strFilePath)
                    strFileName = strFileName
                    URLType = "L"
                ElseIf txtPath.Text <> "" Then
                    strFileName = txtPath.Text    'Getting the File Name
                    URLType = "U"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub btnSaveCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCLose.Click
            Try
                If Not fileupload.PostedFile Is Nothing Then
                    If Not (fileupload.PostedFile.ContentLength = 0 And txtPath.Text = "") Then
                        UploadFile()
                    Else
                        URLType = ViewState("URLType")
                        strFileName = ViewState("FileName")
                        strFileType = ViewState("FileType")
                    End If
                Else
                    URLType = ViewState("URLType")
                    strFileName = ViewState("FileName")
                    strFileType = ViewState("FileType")
                End If
                If CCommon.ToLong(Session("DomainId")) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS", False)
                End If
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList
                With objDocuments
                    .DomainID = Session("DomainId")
                    .GenDocID = Session("DocID")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType

                    'If ddlCategory.SelectedValue = "369" Then
                    If hdnDocCategory.Value = "369" Then
                        .DocDesc = RadEditor1.Content
                    Else
                        .DocDesc = txtDesc.Text
                    End If

                    .DocumentStatus = hdnDocStatus.Value    'ddlClass.SelectedItem.Value
                    .DocCategory = hdnDocCategory.Value     'ddlCategory.SelectedItem.Value
                    .DocumentSection = strDocumentSection
                    .RecID = lngRecID
                    .DocumentType = IIf(lngRecID > 0, 2, 1)     '1=generic,2=specific

                    'If ddlCategory.SelectedValue = 369 Then
                    If hdnDocCategory.Value = "369" Then
                        .FileType = ""
                        .FileName = hdnSystemTemplateName.Value ' will be blank except when its system template
                    Else
                        .FileType = strFileType
                        .FileName = IIf(URLType = "U", txtPath.Text, strFileName)
                    End If
                    .DocName = txtDocName.Text
                    .Subject = txtSubject.Text
                    .vcGroupsPermission = hdnGroups.Value
                    .numCategoryId = ddlCategory.SelectedValue
                    .bitLastFollowupUpdate = chkLastFollowUpDate.Checked
                    .numFollowUpStatusId = ddlFollowupStatus.SelectedValue
                    .bitUpdateFollowupStatus = chkFollowUpStatus.Checked

                    'If ddlCategory.SelectedValue = "369" Then
                    If hdnDocCategory.Value = "369" Then
                        .ModuleID = CCommon.ToLong(ddlEmailMergeModule.SelectedValue)
                    End If
                    .ContactPosition = GetSelectedContactPosition()

                    If (CCommon.ToLong(ddlEmailMergeModule.SelectedValue) = 8) Then
                        .BizDocOppType = ddlBizDocOppType.SelectedValue
                        .BizDocType = ddlBizDocType.SelectedValue
                        .BizDocTemplate = ddlBizDocTemplate.SelectedValue
                    Else
                        .BizDocOppType = ""
                        .BizDocType = ""
                        .BizDocTemplate = ""
                    End If

                    arrOutPut = .SaveDocumentsForEmailTemplate() '.SaveDocuments()
                End With
                Session("DocID") = arrOutPut(0)
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function GetSelectedContactPosition()
            Try
                Dim strContactPosition As String = ""
                'If radContactPosition.CheckedItems().Count > 0 Then
                '    For Each item As RadComboBoxItem In radContactPosition.CheckedItems
                '        If item.Checked = True Then
                '            strContactPosition = strContactPosition + item.Value + ","
                '        End If
                '    Next
                'End If
                Return strContactPosition.TrimEnd(",")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub PageRedirect()
            Try
                Dim SI1 As Long
                Dim SI2 As Long
                Dim BizDocDate As String
                Dim TabID As Long

                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                End If

                SI2 = 1 'ddlCategory.SelectedValue ' Changed by Priya
                TabID = 1

                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                End If
                If GetQueryStringVal("BizDocDate") <> "" Then
                    BizDocDate = GetQueryStringVal("BizDocDate")
                End If
                If GetQueryStringVal("TabID") <> "" Then
                    TabID = GetQueryStringVal("TabID")
                End If

                If GetQueryStringVal("frm") = "frmSpecDocuments" Then
                    Response.Redirect("../Documents/frmSpecDocuments.aspx?Type=" & strDocumentSection & "&yunWE=" & lngRecID, False)
                ElseIf GetQueryStringVal("frm") = "DocList" Then
                    'Response.Redirect("../Documents/frmDocList.aspx?Status=" & SI1 & "&Category=" & SI2 & "&TabID=" & TabID) --- OLD 
                    Response.Redirect("../Documents/frmRegularDocList.aspx?Status=" & SI1 & "&Category=" & SI2 & "&TabID=" & TabID, False)
                ElseIf GetQueryStringVal("frm") = "Tickler" Then
                    Response.Redirect("../common/frmTicklerdisplay.aspx", False)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            PageRedirect()
        End Sub

        'Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        '    Try
        '        If ddlCategory.SelectedItem.Value = 369 Then
        '            trSubject.Visible = True
        '            hplDoc.Visible = False
        '            trEmailMergeModule.Visible = True
        '        Else
        '            trSubject.Visible = False
        '            hplDoc.Visible = True
        '            trEmailMergeModule.Visible = True
        '        End If

        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .GenDocID = Session("DocID")
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactId")
                End With
                If objDocuments.DelDocumentsByGenDocID() = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    PageRedirect()
                    'Response.Redirect("../Documents/frmDocList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
            Try
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = Session("DocID")
                objDocuments.ContactID = Session("UserContactID")
                objDocuments.CDocType = "D"
                objDocuments.byteMode = 3
                objDocuments.Comments = txtComment.Text
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                pnlApprove.Visible = False

                SendEmail(True)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
            Try
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = Session("DocID")
                objDocuments.ContactID = Session("UserContactID")
                objDocuments.CDocType = "D"
                objDocuments.byteMode = 4
                objDocuments.Comments = txtComment.Text
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                pnlApprove.Visible = False
                SendEmail(False)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckOut.Click
            Try
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = Session("DocID")
                objDocuments.byteMode = IIf(hdnCheckoutStatus.Value = 0, 1, 2)
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.DomainID = Session("DomainID")
                objDocuments.ManageDocumentRepositary()
                LoadInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub FillCustomTags()
            Try
                Dim dtDocDetails As DataTable
                Dim objDocuments As New DocumentList()
                dtDocDetails = objDocuments.getCustomTags()

                Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("MergeField"), EditorDropDown)
                ddl.Items.Clear()
                For Each row As DataRow In dtDocDetails.Rows
                    ddl.Items.Add(row("vcMergeField").ToString, row("vcMergeFieldValue").ToString)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindEmailMergeModule()
            Try
                Dim objDocument As New DocumentList
                Dim dtTable As DataTable
                objDocument.ModuleID = 0
                objDocument.byteMode = 1
                dtTable = objDocument.GetMergeFieldsByModuleID()

                ddlEmailMergeModule.DataSource = dtTable
                ddlEmailMergeModule.DataTextField = "vcModuleName"
                ddlEmailMergeModule.DataValueField = "numModuleID"
                ddlEmailMergeModule.DataBind()
                ddlEmailMergeModule.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindCustomField()
            Try
                'Item Detail
                'Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("CustomField"), EditorDropDown)
                'ddl.Items.Clear()

                If ddlEmailMergeModule.SelectedItem.Text.Contains("Leads/Prospects/Accounts") Then
                    Dim dtCustFld As DataTable
                    Dim objCusField As New CustomFields()
                    objCusField.DomainID = Session("DomainID")
                    objCusField.locId = 45
                    dtCustFld = objCusField.CustomFieldList
                    For Each row As DataRow In dtCustFld.Rows
                        Dim Item As New RadComboBoxItem()
                        Item.Text = row("Fld_label").ToString()
                        Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                        radcmbCustomField.Items.Add(Item)
                        Item.DataBind()
                    Next
                ElseIf ddlEmailMergeModule.SelectedItem.Text.Contains("Opportunity/Order") Then
                    Dim dtCustFld As DataTable
                    Dim objCusField As New CustomFields()
                    objCusField.DomainID = Session("DomainID")
                    objCusField.locId = 26 ' Sales and Purchase Combined
                    dtCustFld = objCusField.CustomFieldList
                    For Each row As DataRow In dtCustFld.Rows
                        Dim Item As New RadComboBoxItem()
                        Item.Text = row("Fld_label").ToString()
                        Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                        radcmbCustomField.Items.Add(Item)
                        Item.DataBind()
                    Next
                    'ElseIf ddlEmailMergeModule.SelectedItem.Text.Contains("Contacts") Then
                    '    Dim dtCustFld As DataTable
                    '    Dim objCusField As New CustomFields()
                    '    objCusField.DomainID = Session("DomainID")
                    '    objCusField.locId = 45
                    '    dtCustFld = objCusField.CustomFieldList
                    '    For Each row As DataRow In dtCustFld.Rows
                    '        Dim Item As New RadComboBoxItem()
                    '        Item.Text = row("Fld_label").ToString()
                    '        Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                    '        radcmbCustomField.Items.Add(Item)
                    '        Item.DataBind()
                    '    Next
                ElseIf ddlEmailMergeModule.SelectedItem.Text.Contains("BizDocs") Then
                    Dim dtCustFld As DataTable
                    Dim objCusField As New CustomFields()
                    objCusField.DomainID = Session("DomainID")
                    objCusField.locId = 8
                    dtCustFld = objCusField.CustomFieldList
                    For Each row As DataRow In dtCustFld.Rows
                        Dim Item As New RadComboBoxItem()
                        Item.Text = row("Fld_label").ToString()
                        Item.Value = "#" & row("Fld_label").ToString.Trim.Replace(" ", "") & "#"
                        radcmbCustomField.Items.Add(Item)
                        Item.DataBind()
                    Next
                End If
                If Not (ddlEmailMergeModule.SelectedItem.Text.Contains("BizDocs")) Then
                    radcmbCustomField.Items.Insert(0, New RadComboBoxItem("--Add Custom Merge Field--", "0"))
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ChangeRepositaryStatus()
            If hdnCheckoutStatus.Value = "0" Then
                btnCheckOut.Text = "CheckOut"
            ElseIf hdnCheckoutStatus.Value = "1" Then
                btnCheckOut.Text = "CheckIn"
                If Session("UserContactID") = hdnLastCheckedOutBy.Value Then
                    btnSaveCLose.Visible = True
                Else
                    btnSaveCLose.Visible = False
                    btnCheckOut.Visible = False
                End If
            End If
        End Sub

        Private Sub SendEmail(ByVal boolApproved As Boolean)
            Try
                Dim objSendEmail As New Email

                objSendEmail.DomainID = Session("DomainID")
                objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL/DECLINED_NOTIFICATION"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = txtRecOwner.Text
                objSendEmail.AdditionalMergeFields.Add("DocumentApprovalStatus", IIf(boolApproved, "Approved", "Declined"))
                objSendEmail.AdditionalMergeFields.Add("DocumentApprovalComment", txtComment.Text.Trim)
                objSendEmail.AdditionalMergeFields.Add("DocumentName", txtDocName.Text.Trim)
                objSendEmail.AdditionalMergeFields.Add("Signature", Session("Signature"))
                objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
                objSendEmail.FromEmail = Session("UserEmail")
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                objSendEmail.SendEmailTemplate()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Protected Sub BindEmailTemplateMergeFields(ByVal objDocuments As DocumentList, ByVal ModuleID As Long)
            Try
                Dim AlertDTLID As Long = 0
                Dim dtTable As DataTable
                objDocuments.ModuleID = ModuleID
                objDocuments.byteMode = 0
                dtTable = objDocuments.GetMergeFieldsByModuleID()
                
                radcmbMergerFields.DataSource = dtTable
                radcmbMergerFields.DataTextField = "vcMergeField"
                radcmbMergerFields.DataValueField = "vcMergeFieldValue"
                radcmbMergerFields.DataBind()
                radcmbMergerFields.Items.Insert(0, New RadComboBoxItem("--Add Default Merge Field--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub BindBizDocsType()
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.BizDocType = ddlBizDocOppType.SelectedValue
                ddlBizDocType.Items.Clear()
                ddlBizDocType.DataSource = objCommon.GetBizDocType
                ddlBizDocType.DataTextField = "vcData"
                ddlBizDocType.DataValueField = "numListItemID"
                ddlBizDocType.DataBind()
                ddlBizDocType.Items.Insert(0, New ListItem("--Select BizDoc Type--", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindBizDocsTemplate()
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = ddlBizDocType.SelectedValue
                objOppBizDoc.OppType = ddlBizDocOppType.SelectedValue
                objOppBizDoc.byteMode = 0

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                ddlBizDocTemplate.DataSource = dtBizDocTemplate
                ddlBizDocTemplate.DataTextField = "vcTemplateName"
                ddlBizDocTemplate.DataValueField = "numBizDocTempID"
                ddlBizDocTemplate.DataBind()

                ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select BizDoc Template--", 0))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub ddlBizDocType_SelectedIndexChanged(sender As Object, e As EventArgs)
            BindBizDocsTemplate()
            BindFollowUpStatus()
        End Sub

        Protected Sub ddlEmailMergeModule_SelectedIndexChanged1(sender As Object, e As EventArgs)
            BindFollowUpStatus()
            Dim objDocument As New DocumentList
            If (ddlEmailMergeModule.SelectedValue = 8) Then
                ddlBizDocOppType.Visible = True
                ddlBizDocType.Visible = True
                ddlBizDocTemplate.Visible = True
                ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select BizDoc Template--", "0"))
                radcmbBizDocMergeFields.Visible = True
                radcmbMergerFields.Visible = False
            Else
                ddlBizDocOppType.Visible = False
                ddlBizDocType.Visible = False
                ddlBizDocTemplate.Visible = False
                radcmbBizDocMergeFields.Visible = False
                radcmbMergerFields.Visible = True
                BindEmailTemplateMergeFields(objDocument, ddlEmailMergeModule.SelectedValue)
                radcmbCustomField.Items.Clear()
                BindCustomField()
            End If

        End Sub

        Protected Sub radcmbBizDocMergeFields_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
            BindFollowUpStatus()
            If (radcmbBizDocMergeFields.SelectedValue = "##BizDoc Template Merge fields without HTML & CSS##") Then
                FillMergeFieldsForBizDocs()
            ElseIf (radcmbBizDocMergeFields.SelectedValue = "##BizDocTemplateFromGlobalSettings##") Then
                RadEditor1.Content = ""
                radcmbCustomField.Visible = False
                RadEditor1.Content = "##BizDocTemplateFromGlobalSettings##"
            End If

        End Sub

        Protected Sub ddlBizDocTemplate_SelectedIndexChanged(sender As Object, e As EventArgs)
            BindFollowUpStatus()
            If (radcmbBizDocMergeFields.SelectedValue = "##BizDocTemplateFromGlobalSettings##") Then
                RadEditor1.Content = "##BizDocTemplateFromGlobalSettings##"
            ElseIf (radcmbBizDocMergeFields.SelectedValue = "##BizDoc Template Merge fields without HTML & CSS##") Then
                RadEditor1.Content = ""
                FillMergeFieldsForBizDocs()
            End If
        End Sub

        Protected Sub ddlBizDocOppType_SelectedIndexChanged(sender As Object, e As EventArgs)
            BindFollowUpStatus()
            BindBizDocsType()
        End Sub

        Protected Function CompareDatatables(strBizDocUI As String) As DataTable

            Dim dtAllElement = New DataTable()
            Dim dcName = New DataColumn("Name", GetType(String))
            Dim dcValue = New DataColumn("Value", GetType(String))
            dtAllElement.Columns.Add(dcName)
            dtAllElement.Columns.Add(dcValue)

            dtAllElement.Rows.Add("Amount Paid Popup", "#AmountPaidPopUp#")
            dtAllElement.Rows.Add("Amount Paid", "#AmountPaid#")
            dtAllElement.Rows.Add("Assignee Name", "#AssigneeName#")
            dtAllElement.Rows.Add("Assignee Email", "#AssigneeEmail#")
            dtAllElement.Rows.Add("Assignee Phone No", "#AssigneePhoneNo#")
            dtAllElement.Rows.Add("Balance Due", "#BalanceDue#")
            dtAllElement.Rows.Add("Billing Terms Name", "#BillingTermsName#")
            dtAllElement.Rows.Add("Billing Terms", "#BillingTerms#")
            dtAllElement.Rows.Add("Billing Terms-From Date", "#BillingTermFromDate#")
            dtAllElement.Rows.Add("BizDoc-Comments", "#Comments#")
            dtAllElement.Rows.Add("BizDoc Created Date", "#BizDocCreatedDate#")
            dtAllElement.Rows.Add("BizDoc ID", "#BizDocID#")
            dtAllElement.Rows.Add("BizDoc Status", "#BizDocStatus#")
            dtAllElement.Rows.Add("BizDoc Summary", "#BizDocSummary#")
            dtAllElement.Rows.Add("BizDoc Template Name", "#BizDocTemplateName#")
            dtAllElement.Rows.Add("BizDoc Type", "#BizDocType#")

            dtAllElement.Rows.Add("Change Customer/Vendor Bill To Header", "#OppOrderChangeBillToHeader#")
            dtAllElement.Rows.Add("Change Customer/Vendor Ship To Header", "#OppOrderChangeShipToHeader#")
            dtAllElement.Rows.Add("Change Employer Bill To Header", "#EmployerChangeBillToHeader(Bill To)#")
            dtAllElement.Rows.Add("Change Employer Ship To Header", "#EmployerChangeShipToHeader(Ship To)#")
            dtAllElement.Rows.Add("Currency", "#Currency#")

            dtAllElement.Rows.Add("Customer/Vendor Bill To Address Name", "#Customer/VendorBillToAddressName#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To Address", "#Customer/VendorBillToAddress#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To City", "#Customer/VendorBillToCity#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To Company Name", "#Customer/VendorBillToCompanyName#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To Country", "#Customer/VendorBillToCountry#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To Postal", "#Customer/VendorBillToPostal#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To State", "#Customer/VendorBillToState#")
            dtAllElement.Rows.Add("Customer/Vendor Bill To Street", "#Customer/VendorBillToStreet#")
            dtAllElement.Rows.Add("Customer/Vendor Organization Comments", "#Customer/VendorOrganizationComments#")
            dtAllElement.Rows.Add("Customer/Vendor Organization Contact Email", "#Customer/VendorOrganizationContactEmail#")
            dtAllElement.Rows.Add("Customer/Vendor Organization Contact Name", "#Customer/VendorOrganizationContactName#")
            dtAllElement.Rows.Add("Customer/Vendor Organization Contact Phone", "#Customer/VendorOrganizationContactPhone#")
            dtAllElement.Rows.Add("Customer/Vendor Organization Name", "#Customer/VendorOrganizationName#")
            dtAllElement.Rows.Add("Customer/Vendor Organization Phone", "#Customer/VendorOrganizationPhone#")

            dtAllElement.Rows.Add("Customer/Vendor Ship To Address Name", "#Customer/VendorShipToAddressName#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To Address", "#Customer/VendorShipToAddress#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To City", "#Customer/VendorShipToCity#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To Company Name", "#Customer/VendorShipToCompanyName#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To Country", "#Customer/VendorShipToCountry#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To Postal", "#Customer/VendorShipToPostal#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To State", "#Customer/VendorShipToState#")
            dtAllElement.Rows.Add("Customer/Vendor Ship To Street", "#Customer/VendorShipToStreet#")

            dtAllElement.Rows.Add("Deal or Order ID", "#OrderID#")
            dtAllElement.Rows.Add("Discount", "#Discount#")
            dtAllElement.Rows.Add("Due Date Change Popup", "#ChangeDueDate#")
            dtAllElement.Rows.Add("Due Date", "#DueDate#")

            dtAllElement.Rows.Add("Employer Bill To Address Name", "#EmployerBillToAddressName#")
            dtAllElement.Rows.Add("Employer Bill To Address", "#EmployerBillToAddress#")
            dtAllElement.Rows.Add("Employer Bill To City", "#EmployerBillToCity#")
            dtAllElement.Rows.Add("Employer Bill To Company Name", "#EmployerBillToCompanyName#")
            dtAllElement.Rows.Add("Employer Bill To Country", "#EmployerBillToCountry#")
            dtAllElement.Rows.Add("Employer Bill To Postal", "#EmployerBillToPostal#")
            dtAllElement.Rows.Add("Employer Bill To State", "#EmployerBillToState#")
            dtAllElement.Rows.Add("Employer Bill To Street", "#EmployerBillToStreet#")
            dtAllElement.Rows.Add("Employer Organization Name", "#EmployerOrganizationName#")
            dtAllElement.Rows.Add("Employer Organization Phone", "#EmployerOrganizationPhone#")

            dtAllElement.Rows.Add("Employer Ship To Address Name", "#EmployerShipToAddressName#")
            dtAllElement.Rows.Add("Employer Ship To Address", "#EmployerShipToAddress#")
            dtAllElement.Rows.Add("Employer Ship To City", "#EmployerShipToCity#")
            dtAllElement.Rows.Add("Employer Ship To Company Name", "#EmployerShipToCompanyName#")
            dtAllElement.Rows.Add("Employer Ship To Country", "#EmployerShipToCountry#")
            dtAllElement.Rows.Add("Employer Ship To Postal", "#EmployerShipToPostal#")
            dtAllElement.Rows.Add("Employer Ship To State", "#EmployerShipToState#")
            dtAllElement.Rows.Add("Employer Ship To Street", "#EmployerShipToStreet#")
            dtAllElement.Rows.Add("Inventory Status", "#InventoryStatus#")
            dtAllElement.Rows.Add("Logo", "#Logo#")
            dtAllElement.Rows.Add("Order Record Owner", "#OrderRecOwner#")
            dtAllElement.Rows.Add("PO/Invoice #", "#P.O.NO#")
            dtAllElement.Rows.Add("Products", "#Products#")
            dtAllElement.Rows.Add("Ship Via", "#ShippingCompany#")
            dtAllElement.Rows.Add("Total Quantity", "#TotalQuantity#")
            dtAllElement.Rows.Add("Tracking Numbers", "#TrackingNo#")
            dtAllElement.Rows.Add("Total Qyt by UOM", "#TotalQytbyUOM#")

            dtAllElement.Rows.Add("BillingContact", "#BillingContact#")
            dtAllElement.Rows.Add("ShippingContact", "#ShippingContact#")
            dtAllElement.Rows.Add("SO-Comments", "#SOComments#")
            dtAllElement.Rows.Add("Parcel Shipping Account #", "#ParcelShippingAccount#")
            dtAllElement.Rows.Add("Opp/Order Bill To Address Name", "#OppOrderBillToAddressName#")
            dtAllElement.Rows.Add("Opp/Order Bill To Address", "#OppOrderBillToAddress#")
            dtAllElement.Rows.Add("Opp/Order Bill To City", "#OppOrderBillToCity#")
            dtAllElement.Rows.Add("Opp/Order Bill To Company Name", "#OppOrderBillToCompanyName#")
            dtAllElement.Rows.Add("Opp/Order Bill To Country", "#OppOrderBillToCountry#")
            dtAllElement.Rows.Add("Opp/Order Bill To Postal", "#OppOrderBillToPostal#")
            dtAllElement.Rows.Add("Opp/Order Bill To State", "#OppOrderBillToState#")
            dtAllElement.Rows.Add("Opp/Order Bill To Street", "#OppOrderBillToStreet#")

            dtAllElement.Rows.Add("Opp/Order Ship To Address Name", "#OppOrderShipToAddressName#")
            dtAllElement.Rows.Add("Opp/Order Ship To Address", "#OppOrderShipToAddress#")
            dtAllElement.Rows.Add("Opp/Order Ship To City", "#OppOrderShipToCity#")
            dtAllElement.Rows.Add("Opp/Order Ship To Company Name", "#OppOrderShipToCompanyName#")
            dtAllElement.Rows.Add("Opp/Order Ship To Country", "#OppOrderShipToCountry#")
            dtAllElement.Rows.Add("Opp/Order Ship To Postal", "#OppOrderShipToPostal#")
            dtAllElement.Rows.Add("Opp/Order Ship To State", "#OppOrderShipToState#")
            dtAllElement.Rows.Add("Opp/Order Ship To Street", "#OppOrderShipToStreet#")
            dtAllElement.Rows.Add("Opp/Order Created Date", "#OrderCreatedDate#")

            dtAllElement.Rows.Add("Partner Source", "#PartnerSource#")
            dtAllElement.Rows.Add("Release Date", "#ReleaseDate#")
            dtAllElement.Rows.Add("Required Date", "#RequiredDate#")
            dtAllElement.Rows.Add("Customer PO#", "#CustomerPO#")


            Dim dtBizDocElement = New DataTable()
            Dim colName = New DataColumn("Name", GetType(String))
            Dim colValue = New DataColumn("Value", GetType(String))
            dtBizDocElement.Columns.Add(colName)
            dtBizDocElement.Columns.Add(colValue)

            For Each drow As DataRow In dtAllElement.Rows
                If (strBizDocUI.Contains(drow("Value").ToString())) Then
                    dtBizDocElement.Rows.Add(drow.ItemArray)
                End If
            Next
            Return dtBizDocElement
        End Function

        Protected Function FillMergeFieldsForBizDocs()
            radcmbCustomField.Visible = True

            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)
            Dim dt As DataTable = objOppBizDoc.GetOpportunityModifiedBizDocs()

            If dt.Rows.Count > 0 Then
                Dim OppID, OppBizDocId As Long
                OppID = CCommon.ToLong(dt.Rows(0).Item("numOppID"))
                OppBizDocId = CCommon.ToLong(dt.Rows(0).Item("numOppBizDocsId"))
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                Dim strBizDocUI As String

                objOppBizDocs.OppBizDocId = OppBizDocId
                objOppBizDocs.OppId = OppID
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

                If dtOppBiDocDtl.Rows.Count > 0 Then

                    If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then
                        strBizDocUI = HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())

                        Dim dtBizDocElement As DataTable = CompareDatatables(strBizDocUI)

                        Dim item1 As New RadComboBoxItem
                        item1.Text = "Organization Name"
                        item1.Value = "##OrganizationName##"
                        Dim item2 As New RadComboBoxItem
                        item2.Text = "Contact First Name"
                        item2.Value = "##ContactFirstName##"
                        Dim item3 As New RadComboBoxItem
                        item3.Text = "Contact Last Name"
                        item3.Value = "##ContactLastName##"
                        Dim item4 As New RadComboBoxItem
                        item4.Text = "My Signature"
                        item4.Value = "##Signature##"

                        radcmbCustomField.Items.Clear()
                        radcmbCustomField.DataSource = dtBizDocElement
                        radcmbCustomField.DataTextField = "Name"
                        radcmbCustomField.DataValueField = "Value"
                        radcmbCustomField.DataBind()
                        radcmbCustomField.Items.Insert(0, New RadComboBoxItem("--Select --"))
                        radcmbCustomField.Items.Add(item1)
                        radcmbCustomField.Items.Add(item2)
                        radcmbCustomField.Items.Add(item3)
                        radcmbCustomField.Items.Add(item4)

                    End If
                End If
            End If
        End Function

        Protected Sub btnRestoreToDefault_Click(sender As Object, e As EventArgs)
            Try
                Dim objDocuments As New DocumentList
                objDocuments.DomainID = 0
                objDocuments.FileName = ViewState("FileName")
                Dim dtDocDetails As DataTable = objDocuments.GetByFileName()

                If Not dtDocDetails Is Nothing AndAlso dtDocDetails.Rows.Count > 0 Then
                    If hdnDocCategory.Value = "369" Then
                        RadEditor1.Content = CCommon.ToString(dtDocDetails.Rows(0).Item("vcDocdesc"))
                    Else
                        txtDesc.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("vcDocdesc"))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace

