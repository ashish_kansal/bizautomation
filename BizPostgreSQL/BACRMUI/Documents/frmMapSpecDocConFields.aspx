<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" SmartNavigation="true" Codebehind="frmMapSpecDocConFields.aspx.vb" Inherits="BACRM.UserInterface.Documents.frmMapSpecDocConFields"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>BizForm to E-Form Mapping</title>
		<script language="javascript">
		function Close()
		{
			opener.location.reload(true); 
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td>
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;BizForm to E-Form Mapping&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td>
						<IMG src="../images/pgBar.gif" style="display:none" runat="server" id="pgBar">
					</td>
					<td align="right">
						<asp:button id="btnAutoMap" Text="Auto-Map" Runat="server" CssClass="button"></asp:button>
						<asp:button id="btnSaveEform" Text="Save &amp; Go to E-Form" Runat="server" CssClass="button"></asp:button>
						<asp:button id="btnClose" Text="Close" Runat="server" CssClass="button" Width="50"></asp:button></td>
				</tr>
			</table>
			<asp:table id="tblDetails" Runat="server" Width="100%" Height="400" BorderColor="black" GridLines="None" CssClass="aspTable"
				BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<asp:Table ID="tblBizFields" Runat="server"></asp:Table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
		</form>
	</body>
</HTML>
