﻿Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Documents
    Public Class frmRegularDocList
        Inherits BACRMPage
        Dim strColumn As String
        Dim ds As New DataSet
        Public lngCategory As Long
        Public lngStatus As Long

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                lngStatus = CCommon.ToLong(GetQueryStringVal("Status"))
                lngCategory = CCommon.ToLong(GetQueryStringVal("Category"))
                hdnCategoty.Value = lngCategory

                If lngCategory = 369 Or lngCategory = 370 Then
                    lngStatus = 0

                    If lngCategory = 369 Then
                        hplNew.Visible = True
                        hplNew.Attributes.Add("onclick", "return OpenET()")
                    End If
                End If

                'To Set Permission
                GetUserRightsForPage(14, 2)
                If Not IsPostBack Then
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))

                        If CCommon.ToShort(PersistTable("TemplateType")) = 2 Then
                            rbDefault.Checked = True
                        ElseIf CCommon.ToShort(PersistTable("TemplateType")) = 1 Then
                            rbCustom.Checked = True
                        Else
                            rbAll.Checked = True
                        End If
                    End If

                    BindDatagrid()
                End If


            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim dtDocuments As DataTable
                Dim objDocuments As New DocumentList
                With objDocuments
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .DocCategory = lngCategory
                    .SortCharacter = txtSortChar.Text.Trim()
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "bintcreateddate"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    If rbCustom.Checked Then
                        .DocumentType = 1
                    ElseIf rbDefault.Checked Then
                        .DocumentType = 2
                    Else
                        .DocumentType = 0
                    End If

                    .GenDocID = CCommon.ToLong(hdnGenDocID.Value)
                End With

                dtDocuments = objDocuments.GetDocuments

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objDocuments.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                dgDocs.DataSource = dtDocuments
                dgDocs.DataBind()

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                If rbDefault.Checked Then
                    PersistTable.Add("TemplateType", 2)
                ElseIf rbCustom.Checked Then
                    PersistTable.Add("TemplateType", 1)
                Else
                    PersistTable.Add("TemplateType", 0)
                End If

                PersistTable.Save()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDocs.ItemCommand
            Try
                If e.CommandName = "Name" Then
                    Session("DocID") = e.Item.Cells(0).Text()
                    Response.Redirect("../Documents/frmDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocId=" & Session("DocID") & "&frm=DocList&SI1=" & lngStatus & "&SI2=" & lngCategory & "&TabID=1", False)

                ElseIf e.CommandName = "Delete" Then
                    Dim objDocuments As New DocumentList
                    With objDocuments
                        .UserCntID = Session("UserContactId")
                        .DomainID = Session("DomainId")
                        .GenDocID = e.Item.Cells(0).Text()
                        .DelDocumentsByGenDocID()
                    End With
                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgDocs_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDocs.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If txtSortColumn.Text <> strColumn Then
                    txtSortColumn.Text = strColumn
                    txtSortOrder.Text = "A"
                Else
                    If txtSortOrder.Text = "D" Then
                        txtSortOrder.Text = "A"
                    Else : txtSortOrder.Text = "D"
                    End If
                End If

                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    Dim hpl As HyperLink
                    hpl = e.Item.FindControl("hplLink")
                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    hpl.NavigateUrl = "frmViewAttachment.aspx?docid=" & CCommon.ToLong(e.Item.Cells(0).Text.Trim) 'CCommon.GetDocumentPath(Session("DomainID")) & e.Item.Cells(2).Text
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    If e.Item.Cells(2).Text.Contains("#SYS#") Then 'FileName contains #SYS#
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteTemplate()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Function ReturnDate(ByVal CloseDate) As String
            Try
                If CloseDate Is Nothing Then Exit Function
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b></font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                        temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = strTargetResolveDate
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Protected Sub btnGo_Click(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub rbAll_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub rbCustom_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub rbDefault_CheckedChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace
