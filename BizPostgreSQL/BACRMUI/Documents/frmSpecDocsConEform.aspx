<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" SmartNavigation="true" Codebehind="frmSpecDocsConEform.aspx.vb" Inherits="BACRM.UserInterface.Documents.frmSpecDocsEform"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>E-Form</title>
		<script language="javascript">
		function Close()
		{
			opener.location.reload(true); 
			window.close()
			return false;
		}
		function alertUpEform()
		{
			var bln;
			bln=confirm('You are about to populate the fields that are empty in the target E-Form with the mapped fields that are populated from the selected BizAutomation record (the �BizForm�). Fields in the E-Form that are already populated will be left as is.')
				if (bln==true)
				{
				return true;
				}
				else
				{
					return false;
				}
		}
		function alertUpBizForm()
		{
			var bln;
			bln=confirm('You are about to populate the fields that are empty in the target BizAutomation record (the �BizForm�) with the mapped fields that are populated from the selected E-form. Fields in the BizForm that are already populated will be left as is.')
				if (bln==true)
				{
				return true;
				}
				else
				{
					return false;
				}
		}
		function alertPBizForm()
		{
			var bln;
			bln=confirm('You are about to populate ALL the fields in the target BizAutomation record (the �BizForm�) with the values of the mapped fields from the selected E-form. ALL mapped fields in the BizForm will be replaced !.')
				if (bln==true)
				{
				return true;
				}
				else
				{
					return false;
				}
		}
		function alertPeForm()
		{
			var bln;
			bln=confirm('You are about to populate ALL the fields in the target E-Form with the values of the mapped fields from the selected BizAutomation record (the �BizForm�). ALL mapped fields in the E-Form will be replaced !.')
				if (bln==true)
				{
				return true;
				}
				else
				{
					return false;
				}
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<IMG src="../images/pgBar.gif" style="DISPLAY:none" runat="server" id="pgBar">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;e-Form&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align=center >
						<asp:button id="btnURecord" CssClass="BUTTONMagenta" Width="100" Runat="server" Text="Update BizForm"></asp:button>
						<asp:button id="btnUeForm" CssClass="BUTTONMagenta" Runat="server" Width="100" Text="Update e-Form"></asp:button>
						<br>
						<asp:button id="btnPRecord" CssClass="BUTTONPurple" Runat="server" Width="100" Text="Populate BizForm"></asp:button>
						<asp:button id="btnPeform" CssClass="BUTTONPurple" Runat="server" Width="100" Text="Populate e-Form"></asp:button>
					</td>
					<td align="right" valign="bottom">
						<asp:button id="btnSendEmail" CssClass="button" Width="100" Runat="server" Text="Send as Email"></asp:button>
						<asp:button id="btnSaveToNetk" CssClass="button" Width="140" Runat="server" Text="Save to Local  Network"></asp:button>
						<asp:button id="btnSaveEform" CssClass="button" Width="140" Runat="server" Text="Save To Document List"></asp:button>
						<asp:button id="btnClose" CssClass="button" Runat="server" Width="50" Text="Close"></asp:button>
					</td>
				</tr>
			</table>
			<asp:Table ID="tblDetails" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTable"
				Height="400">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<asp:Table ID="tbleform" Runat="server"></asp:Table>
						<br>
					</asp:TableCell>
				</asp:TableRow>
			</asp:Table>
		</form>
	</body>
</HTML>
