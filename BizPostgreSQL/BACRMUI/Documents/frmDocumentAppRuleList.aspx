﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDocumentAppRuleList.aspx.vb"
    Inherits=".frmDocumentAppRuleList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Document/BizDoc Approval Rule List</title>
    <script language="javascript" type="text/javascript">
        function OpenRule(a) {
            str = "../Documents/frmDocumentAppRule.aspx?RuleID=" + a;
            document.location.href = str;
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnNew" runat="server" Text="New Rule" CssClass="button" OnClientClick="return OpenRule('0')" />
            &nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Automation Rule List
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:GridView ID="gvRule" AutoGenerateColumns="False" runat="server" Width="100%"
        CssClass="tbl" DataKeyNames="numRuleID">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <Columns>
            <asp:TemplateField HeaderText="Rule Name">
                <ItemTemplate>
                    <a href="javascript:OpenRule('<%# Eval("numRuleID") %>')">
                        <%# Eval("vcRuleName")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="vcModuleType" HeaderText="Module Type"></asp:BoundField>
            <asp:BoundField DataField="vcStatus" HeaderText="Status"></asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteRecord"
                        CommandArgument='<%# Eval("numRuleID") %>'></asp:Button>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
    </asp:GridView>
</asp:Content>
