Imports Microsoft.Web.UI.WebControls
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class reportslinks : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tvReports As Microsoft.Web.UI.WebControls.TreeView
        Protected WithEvents tblMile As System.Web.UI.WebControls.Table
        Protected WithEvents dlRepotList As System.Web.UI.WebControls.DataList

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region


        Dim objCommon As CCommon

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    objCommon = New CCommon
                    GetUserRightsForPage(8, 43)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    ' = "Reports"
                    createMainLink()
                End If
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('6');}else{ parent.SelectTabByValue('6'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub createMainLink()
            Try
                Dim objReports As New CReports
                Dim i As Integer
                Dim dtReports As New DataTable
                Dim dtReportlinks As New DataTable
                objReports.ModuleID = 8
                If Session("dlSelIndex") <> 0 Then dlRepotList.SelectedIndex = Session("dlSelIndex") - 1
                dtReports = objReports.GetHeaderForPredefinedRept
                dlRepotList.DataSource = dtReports
                dlRepotList.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dlRepotList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlRepotList.ItemCommand
            Try
                If e.CommandName = "Link" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlRepotList.SelectedIndex = e.Item.ItemIndex
                    Session("dlSelIndex") = CInt(e.Item.ItemIndex) + 1
                    createMainLink()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dlRepotList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlRepotList.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    Dim tbl As Table
                    Dim lbl As Label
                    lbl = e.Item.FindControl("lblReptID")
                    tbl = e.Item.FindControl("tblSubLink")
                    Dim objReports As New CReports
                    Dim dtReportlinks As New DataTable
                    Dim tblCell As TableCell
                    Dim tblRow As TableRow
                    Dim hpl As HyperLink
                    Dim ibtnEdit As ImageButton
                    Dim lbl1 As Label
                    Dim i As Integer

                    If lbl.Text = "0" Then
                        Dim objCustomQueryReport As New CustomQueryReport
                        objCustomQueryReport.ReportDomainID = CCommon.ToLong(Session("DomainID"))
                        dtReportlinks = objCustomQueryReport.GetByDomainID()

                        For i = 0 To dtReportlinks.Rows.Count - 1
                            tblCell = New TableCell
                            tblCell.Width = Unit.Pixel(40)
                            tblRow = New TableRow

                            ibtnEdit = New ImageButton
                            ibtnEdit.ImageUrl = "../images/edit.png"
                            ibtnEdit.Height = New Unit(16)
                            ibtnEdit.Attributes.Add("onclick", "return OpenAddReportWindow(" & dtReportlinks.Rows(i).Item("numReportID") & ")")
                            tblCell.Controls.Add(ibtnEdit)

                            lbl1 = New Label
                            lbl1.Text = "&nbsp;&nbsp;&nbsp;"
                            tblCell.Controls.Add(lbl1)
                            tblRow.Cells.Add(tblCell)
                            tblCell = New TableCell
                            hpl = New HyperLink
                            hpl.Text = "<b>" & dtReportlinks.Rows(i).Item("vcReportName") & "</b>" & " - " & dtReportlinks.Rows(i).Item("vcReportDescription")
                            hpl.NavigateUrl = "../reports/frmRunCustomReport.aspx?numReportID=" & dtReportlinks.Rows(i).Item("numReportID")
                            hpl.CssClass = "normal5"
                            tblCell.Controls.Add(hpl)
                            tblRow.Cells.Add(tblCell)
                            tbl.Rows.Add(tblRow)
                        Next
                    Else
                        objReports.ModuleID = 8
                        objReports.GroupID = lbl.Text
                        dtReportlinks = objReports.GetPredefinedReptlinks
                        For i = 0 To dtReportlinks.Rows.Count - 1
                            tblCell = New TableCell
                            tblCell.Width = Unit.Pixel(40)
                            tblRow = New TableRow
                            lbl1 = New Label
                            lbl1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                            tblCell.Controls.Add(lbl1)
                            tblRow.Cells.Add(tblCell)
                            tblCell = New TableCell
                            hpl = New HyperLink
                            hpl.Text = "<b>" & dtReportlinks.Rows(i).Item("RptHeading") & "</b>" & " - " & dtReportlinks.Rows(i).Item("RptDesc")
                            hpl.NavigateUrl = "../reports/" & dtReportlinks.Rows(i).Item("vcFileName")
                            hpl.CssClass = "normal5"
                            tblCell.Controls.Add(hpl)
                            tblRow.Cells.Add(tblCell)
                            tbl.Rows.Add(tblRow)
                        Next
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace