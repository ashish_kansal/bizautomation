﻿Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Public Class rptSalesConversionReportDetail
    Inherits BACRMPage

    Dim objPredefinedReports As New PredefinedReports
    Dim dtReports As DataTable
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                GetUserRightsForPage(8, 105)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                End If

                bindReports()
                 ' = "Reports"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindReports()
        Try
            objPredefinedReports.DomainID = Session("DomainID")
            objPredefinedReports.UserCntID = Session("UserContactID")

            objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(GetQueryStringVal( "SD")))
            objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(GetQueryStringVal( "ED"))))
            objPredefinedReports.SortOrder = CCommon.ToInteger(GetQueryStringVal( "RT"))
            objPredefinedReports.ContactID = CCommon.ToLong(GetQueryStringVal( "CId"))
            objPredefinedReports.byteMode = CCommon.ToInteger(GetQueryStringVal( "Type"))

            Dim ds As New DataSet
            ds = objPredefinedReports.GetSalesConversionReport

            dtReports = ds.Tables(0)

            If dtReports.Rows.Count > 0 Then
                lblEmployee.Text = dtReports.Rows(0)("EmployeeName")
            Else
                lblEmployee.Text = ""
            End If

            If objPredefinedReports.byteMode = 2 Then
                lblTitle.Text = "Sales Conversion Performance Detail"

                gvSalesConversionDetail.DataSource = dtReports
                gvSalesConversionDetail.DataBind()
            ElseIf objPredefinedReports.byteMode = 3 Then
                lblTitle.Text = "Sales Conversion Performance - Sales Order"

                gvSalesConversionSalesOrder.DataSource = dtReports
                gvSalesConversionSalesOrder.DataBind()
            End If
            
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        If CCommon.ToInteger(GetQueryStringVal( "Type")) = 2 Then
            ExportToExcel.DataGridToExcel(gvSalesConversionDetail, Response)
        ElseIf CCommon.ToInteger(GetQueryStringVal( "Type")) = 3 Then
            ExportToExcel.DataGridToExcel(gvSalesConversionSalesOrder, Response)
        End If
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Function ReturnName(ByVal SDate) As String
        Try
            Dim strDate As String = ""
            If Not IsDBNull(SDate) Then
                strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strDate = "<font color=red>" & strDate & "</font>"
                ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                    strDate = "<font color=orange>" & strDate & "</font>"
                End If
            End If
            Return strDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class