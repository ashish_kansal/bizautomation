Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmMonthToMonthComparisonPrint : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblreportby As System.Web.UI.WebControls.Label
        Protected WithEvents lblcurrent As System.Web.UI.WebControls.Label
        Protected WithEvents lblReportHeader As System.Web.UI.WebControls.Label
        Protected WithEvents Table9 As System.Web.UI.WebControls.Table
        Protected WithEvents tblData As System.Web.UI.WebControls.Table
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim dtMonthToMonth As DataTable
        Dim dtMonthToMonth1 As DataTable
        Dim dsSplit As DataSet
        Dim objPredefinedReports As New PredefinedReports

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here

                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                DisplayRecords()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                Dim intPReportsCount As Integer
                Dim objDashboard As New DashBoard
                ''Declare the DataTable Row Object.
                'Dim trRow As TableRow
                'Dim trRow1 As TableRow
                ''Declare The DataTable Cell Objects.
                'Dim tcColName As TableCell
                'Dim tcColName1 As TableCell
                'Declare the DataTable Row Object.
                Dim tblRow As TableRow
                'Declare The DataTable Cell Objects.
                Dim tblCell As TableCell
                Dim lbl As Label

                Dim tcColValue As TableCell
                Dim tcColValue1 As TableCell
                Dim strImage As String
                If GetQueryStringVal("team1") > 0 Then
                    objPredefinedReports.DomainID = Session("DomainID")
                    objPredefinedReports.UserCntID = Session("UserContactID")

                    If GetQueryStringVal("month1") > 0 Then
                        ''objPredefinedReports.FirstStartDate = Format(Integer.Parse(GetQueryStringVal("month1") + 1), "00") & "/01/" & GetQueryStringVal("year1")

                        'objPredefinedReports.FirstStartDate = Format(New Date(GetQueryStringVal("year1"), Integer.Parse(GetQueryStringVal("month1") + 1), "00"), 1)
                        objPredefinedReports.FirstStartDate = New Date(GetQueryStringVal("year1"), Format(Integer.Parse(GetQueryStringVal("month1") + 1), "00"), 1)
                        ''objPredefinedReports.FirstEndDate = Format(Integer.Parse(GetQueryStringVal("month1") + 1), "00") & "/" & objDashboard.GetLastDay(Format(Integer.Parse(GetQueryStringVal("month1") + 1), "00") & "/01/" & GetQueryStringVal("year1")) & "/" & GetQueryStringVal("year1")
                        objPredefinedReports.FirstEndDate = New Date(GetQueryStringVal("year1"), Format(Integer.Parse(GetQueryStringVal("month1") + 1), "00"), objDashboard.GetLastDay(New Date(GetQueryStringVal("year1"), Format(Integer.Parse(GetQueryStringVal("month1") + 1), "00"), 1)))
                    End If

                    If GetQueryStringVal("month2") > 0 Then
                        ''objPredefinedReports.SecondStartDate = Format(Integer.Parse(GetQueryStringVal("month2") + 1), "00") & "/01/" & GetQueryStringVal("year2")
                        objPredefinedReports.SecondStartDate = New Date(GetQueryStringVal("year2"), Format(Integer.Parse(GetQueryStringVal("month2") + 1), "00"), 1)
                        ''objPredefinedReports.SecondEndDate = Format(Integer.Parse(GetQueryStringVal("month2") + 1), "00") & "/" & objDashboard.GetLastDay(Format(Integer.Parse(GetQueryStringVal("month2") + 1), "00") & "/01/" & GetQueryStringVal("year2")) & "/" & GetQueryStringVal("year2")
                        objPredefinedReports.SecondEndDate = New Date(GetQueryStringVal("year2"), Format(Integer.Parse(GetQueryStringVal("month2") + 1), "00"), objDashboard.GetLastDay(New Date(GetQueryStringVal("year2"), Format(Integer.Parse(GetQueryStringVal("month2") + 1), "00"), 1)))
                    End If

                    If GetQueryStringVal("team1") > 0 Then
                        objPredefinedReports.CompareFirst = GetQueryStringVal("team1")
                    Else : objPredefinedReports.CompareFirst = 0
                    End If
                    If GetQueryStringVal("team2") > 0 Then
                        objPredefinedReports.CompareSecond = GetQueryStringVal("team2")
                    Else : objPredefinedReports.CompareSecond = 0
                    End If

                    objPredefinedReports.SortOrder = Integer.Parse(GetQueryStringVal("listtype"))
                    'objPredefinedReports.TerritoryID = Session("UserTerID")

                    Select Case GetQueryStringVal("ReportType")
                        Case 0 : objPredefinedReports.UserRights = 0
                        Case 1 : objPredefinedReports.UserRights = 1
                        Case 2 : objPredefinedReports.UserRights = 2
                    End Select

                    objPredefinedReports.ReportType = 22
                    Dim intRowCount As Integer
                    dsSplit = objPredefinedReports.GetMonthToMonthComparison
                    dtMonthToMonth = dsSplit.Tables(0)
                    dtMonthToMonth1 = dsSplit.Tables(1)

                    For intRowCount = 1 To tblData.Rows.Count - 1
                        tblData.Rows(intRowCount).Cells.Clear()
                    Next

                    Dim FirstDisplay As String
                    If GetQueryStringVal("listtype") = 0 Then
                        FirstDisplay = GetQueryStringVal("month1") & "," & GetQueryStringVal("year1") & "," & "Team1"
                    Else : FirstDisplay = GetQueryStringVal("month1") & "," & GetQueryStringVal("year1") & "," & "Territory1"
                    End If

                    Dim SecondDisplay As String
                    If GetQueryStringVal("listtype") = 0 Then
                        SecondDisplay = GetQueryStringVal("month2") & "," & GetQueryStringVal("year2") & "," & "Team2"
                    Else : SecondDisplay = GetQueryStringVal("month2") & "," & GetQueryStringVal("year2") & "," & "Territory2"
                    End If

                    'For intPReportsCount = 0 To dtMonthToMonth.Rows.Count - 1
                    '    'Get a New Row Object.
                    '    trRow = New TableRow
                    '    'Get the New Column Objects and assign the values And Add to row.
                    '    'Set the image strng for graph.
                    '    strImage = "<img src='../images/bottom_line.gif' width='" & dtMonthToMonth.Rows(intPReportsCount).Item("Amount") * 2 & "' height='30' > "

                    '    ' NAME
                    '    tcColName = New TableCell
                    '    tcColName.CssClass = "text"


                    '    tcColName.Controls.Add(New LiteralControl(FirstDisplay))
                    '    tcColName.HorizontalAlign = HorizontalAlign.Right
                    '    'tcColName.Width="40%"
                    '    tcColName.Height.Point(20)
                    '    tcColName.VerticalAlign = VerticalAlign.Bottom
                    '    trRow.Cells.Add(tcColName)

                    '    ' VALUE
                    '    tcColValue = New TableCell
                    '    tcColValue.CssClass = "text"
                    '    tcColValue.Controls.Add(New LiteralControl("&nbsp;&nbsp" & strImage & "&nbsp;&nbsp;&nbsp; " & Format(dtMonthToMonth.Rows(intPReportsCount).Item("Amount"), "#,###.00")))
                    '    tcColValue.HorizontalAlign = HorizontalAlign.Left
                    '    trRow.Cells.Add(tcColValue)

                    '    'Add the row to the table
                    '    tblData.Rows.Add(trRow)
                    'Next

                    'For intPReportsCount = 0 To dtMonthToMonth1.Rows.Count - 1
                    '    'Get a New Row Object.
                    '    trRow1 = New TableRow
                    '    'Get the New Column Objects and assign the values And Add to row.
                    '    'Set the image strng for graph.
                    '    strImage = "<img src='../images/bottom_line.gif' width='" & dtMonthToMonth1.Rows(intPReportsCount).Item("Amount1") * 2 & "' height='30' > "

                    '    ' NAME
                    '    tcColName1 = New TableCell
                    '    tcColName1.CssClass = "text"


                    '    tcColName1.Controls.Add(New LiteralControl(SecondDisplay))
                    '    tcColName1.HorizontalAlign = HorizontalAlign.Right
                    '    'tcColName.Width="40%"
                    '    tcColName1.Height.Point(20)
                    '    tcColName1.VerticalAlign = VerticalAlign.Bottom
                    '    trRow1.Cells.Add(tcColName1)

                    '    ' VALUE
                    '    tcColValue1 = New TableCell
                    '    tcColValue1.CssClass = "text"
                    '    tcColValue1.Controls.Add(New LiteralControl("&nbsp;&nbsp" & strImage & "&nbsp;&nbsp;&nbsp; " & Format(dtMonthToMonth1.Rows(intPReportsCount).Item("Amount1"), "#,###.00")))
                    '    tcColValue1.HorizontalAlign = HorizontalAlign.Left
                    '    trRow1.Cells.Add(tcColValue1)

                    '    'Add the row to the table
                    '    tblData.Rows.Add(trRow1)
                    'Next
                    For intPReportsCount = 0 To dtMonthToMonth.Rows.Count - 1
                        tblRow = New TableRow
                        tblCell = New TableCell
                        tblRow = New TableRow
                        tblCell.Text = FirstDisplay
                        tblCell.CssClass = "normal1"
                        tblCell.Width = Unit.Pixel(100)
                        tblCell.HorizontalAlign = HorizontalAlign.Right
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        lbl = New Label
                        lbl.Height = Unit.Pixel(1)
                        tblCell.Height = Unit.Pixel(1)
                        Dim strcolour As String = "LawnGreen"
                        lbl.BackColor = System.Drawing.Color.FromName(strcolour)
                        If (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)) <> 0 Then
                            lbl.Width = Unit.Pixel(dtMonthToMonth.Rows(intPReportsCount).Item(0) * 400 / (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)))
                        End If
                        tblCell.Controls.Add(lbl)
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblCell.Text = String.Format("{0:#,##0.00}", dtMonthToMonth.Rows(intPReportsCount).Item(0))
                        tblRow.Cells.Add(tblCell)
                        tblData.Rows.Add(tblRow)
                    Next

                    For intPReportsCount = 0 To dtMonthToMonth1.Rows.Count - 1
                        tblRow = New TableRow
                        tblCell = New TableCell
                        tblRow = New TableRow
                        tblCell.Text = SecondDisplay
                        tblCell.CssClass = "normal1"
                        tblCell.Width = Unit.Pixel(100)
                        tblCell.HorizontalAlign = HorizontalAlign.Right
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        lbl = New Label
                        lbl.Height = Unit.Pixel(1)
                        tblCell.Height = Unit.Pixel(1)
                        Dim strcolour As String = "LightBlue"
                        lbl.BackColor = System.Drawing.Color.FromName(strcolour)
                        If (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)) <> 0 Then
                            lbl.Width = Unit.Pixel(dtMonthToMonth1.Rows(intPReportsCount).Item(0) * 400 / (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)))
                        End If
                        tblCell.Controls.Add(lbl)
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblCell.Text = String.Format("{0:#,##0.00}", dtMonthToMonth1.Rows(intPReportsCount).Item(0))
                        tblRow.Cells.Add(tblCell)
                        tblData.Rows.Add(tblRow)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace