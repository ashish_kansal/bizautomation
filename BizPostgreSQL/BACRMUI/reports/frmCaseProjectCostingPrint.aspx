<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCaseProjectCostingPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCaseProjectCostingPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
    <title>Projects & Cases Costing Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="print_head" align="center">
						<asp:Label ID="lblReportHeader" CssClass="print_head" Runat="server">Projects & Cases Costing Report</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" > Organization :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lblOrgName" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="lblType" runat="server" Font-Names="Verdana" Font-Size="9px"></asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lblCPName" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
			    <tr>  	
			        <td align="right" class="normal7">
					    Total Profit or Loss : <asp:Label runat="server" ID="lblProfit"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
    			    </td>
			    </tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			 <asp:datagrid id="dgCosting" CssClass="dg" Width="100%" CellPadding="0" CellSpacing="0" Runat="server"
				AutoGenerateColumns="false" AllowSorting="True">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font color=white>Name, Created On, By</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Type" SortExpression="Type"  HeaderText="<font color=white>Type</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="TimeEntered" SortExpression="TimeEntered"  HeaderText="<font color=white>Time Entered On, By</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="TimeType" SortExpression="TimeType"  HeaderText="<font color=white>Time Type</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="RateHr" SortExpression="RateHr"  HeaderText="<font color=white>Rate/Hr</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Hrs" SortExpression="Hrs" HeaderText="<font color=white>Hrs Entered</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="AmountBilled" SortExpression="AmountBilled"  HeaderText="<font color=white>AmountBilled </font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="labourrate" SortExpression="labourrate"  HeaderText="<font color=white>Labour Cost</font>"></asp:BoundColumn>
								</Columns>
			</asp:datagrid>
			
						<script language="JavaScript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmCaseProjectCosting.aspx";
			}
			</script>
    </form>
</body>
</html>
