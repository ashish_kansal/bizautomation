Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class ConclusionAnalysis : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents rdlReportType As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents tblData As System.Web.UI.WebControls.Table
        Protected WithEvents btnBack As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnGo As System.Web.UI.WebControls.Button
        Protected WithEvents btnPrint As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
        Protected WithEvents btnChooseTeams As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnChooseTerritories As System.Web.UI.WebControls.LinkButton
        Protected WithEvents txtToDateDisplay As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtFromDateDisplay As System.Web.UI.WebControls.TextBox
        Protected WithEvents lblFilterBy As System.Web.UI.WebControls.Label
        Protected WithEvents ddlFilterBy As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.LinkButton

        Dim intaryRights() As Integer
        Protected WithEvents Table9 As System.Web.UI.WebControls.Table
        Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents btnAddtoMyRtpList As System.Web.UI.WebControls.LinkButton
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim objPredefinedReports As New PredefinedReports
        Dim dtPredefinedReports As DataTable
       

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                Dim strReportType As String
                strReportType = GetQueryStringVal("Type")
                If Not IsPostBack Then
                    ' = "Reports"
                    Dim objCommon As CCommon
                    If strReportType = "Deal" Then
                        GetUserRightsForPage(8, 24)
                    ElseIf strReportType = "Source" Then
                        GetUserRightsForPage(8, 25)
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                End If

                DisplayRecords(strReportType)
                If strReportType = "Deal" Then
                    lblHeader.Text = "Deal Conclusion Analysis"
                    lblFilterBy.Visible = True
                    ddlFilterBy.Visible = True
                    btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(10)")
                    btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(10)")

                Else
                    lblHeader.Text = "Source Conclusion Analysis"
                    lblFilterBy.Visible = False
                    ddlFilterBy.Visible = False
                    btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(11)")
                    btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(11)")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords(ByVal strDeal As String)
            Try
                Dim intPReportsCount As Integer
                'Declare the DataTable Row Object.
                Dim trRow As TableRow
                'Declare The DataTable Cell Objects.
                Dim tcColName As TableCell
                Dim tcColValue As TableCell
                Dim strImage As String

                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))

                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.TerritoryID = Session("UserTerID")

                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 0
                    Case 1 : objPredefinedReports.UserRights = 1
                    Case 2 : objPredefinedReports.UserRights = 2
                End Select

                If strDeal = "Deal" Then
                    objPredefinedReports.ReportType = 10
                Else : objPredefinedReports.ReportType = 11
                End If

                If ddlFilterBy.SelectedIndex = 0 Then objPredefinedReports.DealStatus = 1
                If ddlFilterBy.SelectedIndex = 1 Then objPredefinedReports.DealStatus = 2

                Dim intRowCount As Integer
                dtPredefinedReports = objPredefinedReports.GetDealConclusion(strDeal)
                For intRowCount = 1 To tblData.Rows.Count - 1
                    tblData.Rows(intRowCount).Cells.Clear()
                Next
                For intPReportsCount = 0 To dtPredefinedReports.Rows.Count - 1
                    'Get a New Row Object.
                    trRow = New TableRow
                    'Get the New Column Objects and assign the values And Add to row.
                    'Set the image strng for graph.
                    strImage = "<img src='../images/bottom_line.gif' width='" & dtPredefinedReports.Rows(intPReportsCount).Item("Percentage") * 5 & "' height='20' > "

                    ' NAME
                    tcColName = New TableCell
                    tcColName.CssClass = "text"
                    tcColName.Controls.Add(New LiteralControl(dtPredefinedReports.Rows(intPReportsCount).Item("ConclusionBasis")))
                    tcColName.HorizontalAlign = HorizontalAlign.Right
                    'tcColName.Width="40%"
                    tcColName.Height.Point(20)
                    tcColName.VerticalAlign = VerticalAlign.Bottom
                    trRow.Cells.Add(tcColName)

                    ' VALUE
                    tcColValue = New TableCell
                    tcColValue.CssClass = "text"
                    tcColValue.Controls.Add(New LiteralControl(strImage & "&nbsp; " & Format(dtPredefinedReports.Rows(intPReportsCount).Item("Percentage"), "#,###.00") & "%"))
                    tcColValue.HorizontalAlign = HorizontalAlign.Left
                    trRow.Cells.Add(tcColValue)

                    'Add the row to the table
                    tblData.Rows.Add(trRow)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                If GetQueryStringVal( "Type").ToString().Length > 0 Then
                    DisplayRecords(GetQueryStringVal( "Type").ToString())
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFilterBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterBy.SelectedIndexChanged
            Try
                If ddlFilterBy.SelectedIndex = 0 Then objPredefinedReports.DealStatus = 1
                If ddlFilterBy.SelectedIndex = 1 Then objPredefinedReports.DealStatus = 2
                DisplayRecords(GetQueryStringVal( "Type").ToString())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/ConclusionAnalysisPrint.aspx?fdt=" & calFrom.SelectedDate & "&tdt=" & DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) & "&Type=" & GetQueryStringVal("Type") & "&Criteria=" & ddlFilterBy.SelectedIndex & "&ReportType=" & rdlReportType.SelectedIndex, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords(GetQueryStringVal( "Type").ToString())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                Dim strPath As String = Server.MapPath("../documents/docs")
                Dim strref As String
                DisplayRecords(GetQueryStringVal( "Type"))
                If GetQueryStringVal( "Type") = "Deal" Then
                    objPredefinedReports.ExportToExcel(dtPredefinedReports.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "DealConclusionAnalysis", Session("DateFormat"))
                    strref = "../Documents/docs/" & Session("UserName") & "_" & Session("UserID") & "_DealConclusionAnalysis.csv"
                Else
                    objPredefinedReports.ExportToExcel(dtPredefinedReports.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "SourceConclusionAnalysis", Session("DateFormat"))
                    strref = "../Documents/docs/" & Session("UserName") & "_" & Session("UserID") & "_SourceConclusionAnalysis.csv"
                End If

                Response.Write("<script language=javascript>")
                Response.Write("alert('Exported Successfully !')")
                Response.Write("</script>")

                Response.Write("<script language=javascript>")
                Response.Write("window.open('" & strref & "')")
                Response.Write("</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                If GetQueryStringVal( "Type") = "Deal" Then
                    objPredefinedReports.ReportID = 2
                Else : objPredefinedReports.ReportID = 3
                End If
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace