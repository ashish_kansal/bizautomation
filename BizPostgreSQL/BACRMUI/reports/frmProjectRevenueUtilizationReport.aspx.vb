﻿Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports System.Configuration

Namespace BACRM.UserInterface.Reports
    Public Class frmProjectRevenueUtilizationReport : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then
                    GetUserRightsForPage(8, 107)

                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False

                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub BindDatagrid()
            Try
                Dim dtTable As DataTable
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.DomainID = Session("DomainID")

                dtTable = objPredefinedReports.GetProjectRevenueUtilization
                gvProjectReport.DataSource = dtTable
                gvProjectReport.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles btnExportToExcel.Click
                ExportToExcel.DataGridToExcel(gvProjectReport, Response)
        End Sub

        Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        End Sub
    End Class
End Namespace
