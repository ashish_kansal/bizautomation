Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmCases : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents txtFromDateDisplay As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtToDateDisplay As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnGo As System.Web.UI.WebControls.Button
        Protected WithEvents rdlReportType As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents lblHeader As System.Web.UI.WebControls.Label
        Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnChooseTeams As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnChooseTerritories As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnPrint As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnBack As System.Web.UI.WebControls.LinkButton
        Protected WithEvents Table9 As System.Web.UI.WebControls.Table
        Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents ddlFilter As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlAgent As System.Web.UI.WebControls.DropDownList
        Protected WithEvents OrderList As System.Web.UI.WebControls.DataList
        Protected WithEvents dlCases As System.Web.UI.WebControls.DataList
        Protected WithEvents btnAddtoMyRtpList As System.Web.UI.WebControls.LinkButton
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim dtCases As DataTable
        Dim objPredefinedReports As New PredefinedReports
        Dim SortField As String

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then

                    ' = "Reports"
                    GetUserRightsForPage(8, 62)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    objCommon.sb_FillComboFromDBwithSel(ddlFilter, 14, Session("DomainID"))
                    DisplayRecords()
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(21)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(21)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                objPredefinedReports.UserCntID = Session("UserContactID")

                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select

                objPredefinedReports.ReportType = 21
                objPredefinedReports.CaseStatus = ddlFilter.SelectedValue
                dtCases = objPredefinedReports.GetCases
                Session("dtcases") = dtCases
                dlCases.DataSource = dtCases
                dlCases.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                Dim strPath As String = Server.MapPath("")
                Dim dtDtlreport As DataTable
                dtDtlreport = Session("dtDtlreport")
                objPredefinedReports.ExportToExcel(dtDtlreport.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "Cases", Session("DateFormat"))

                Response.Write("<script language=javascript>")
                Response.Write("alert('Exported Successfully !')")
                Response.Write("</script>")
                Dim strref As String = Session("SiteType") & "//" & Request.ServerVariables("HTTP_HOST") & Request.ApplicationPath() & "/reports/" & Session("UserName") & "_" & Session("UserID") & "_Cases.csv"
                Response.Write("<script language=javascript>")
                Response.Write("window.open('" & strref & "')")
                Response.Write("</script>")
                ' ExportToExcel.DataGridToExcel(dgReptCases, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/frmCasesPrint.aspx?fdt=" & calFrom.SelectedDate & "&tdt=" & calTo.SelectedDate & "&ReportType=" & rdlReportType.SelectedIndex & "&CaseStatus=" & ddlFilter.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dlCases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlCases.ItemCommand
            Try
                If e.CommandName = "Name" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlCases.SelectedIndex = e.Item.ItemIndex
                    DisplayRecords()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dlCases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlCases.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    Dim dtDtlreport As DataTable
                    Dim dgReptCases As DataGrid

                    dgReptCases = e.Item.FindControl("dgReptCases")
                    AddHandler dgReptCases.ItemDataBound, AddressOf Me.dgReport_ItemCommand
                    objPredefinedReports.FromDate = calFrom.SelectedDate
                    objPredefinedReports.ToDate = DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate))
                    objPredefinedReports.UserCntID = CType(e.Item.FindControl("lblAssignTo"), Label).Text
                    objPredefinedReports.CaseStatus = ddlFilter.SelectedValue
                    Select Case rdlReportType.SelectedIndex
                        Case 0 : objPredefinedReports.UserRights = 1
                        Case 1 : objPredefinedReports.UserRights = 2
                        Case 2 : objPredefinedReports.UserRights = 3
                    End Select

                    objPredefinedReports.ReportType = 21
                    dtDtlreport = objPredefinedReports.GetReptCasesForAgents
                    dgReptCases.DataSource = dtDtlreport
                    dgReptCases.DataBind()
                    Session("dtDtlreport") = dtDtlreport
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplCompany, hplCases As HyperLink
                    hplCompany = e.Item.FindControl("hplCompany")
                    hplCases = e.Item.FindControl("hplCases")
                    If e.Item.Cells(3).Text = 2 Then
                        hplCompany.NavigateUrl = "../Account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseReport&CmpID=" & e.Item.Cells(0).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(2).Text

                    ElseIf e.Item.Cells(3).Text = 1 Then
                        hplCompany.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseReport&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text

                    ElseIf e.Item.Cells(3).Text = 0 Then
                        hplCompany.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseReport&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text

                    End If
                    hplCases.NavigateUrl = "../Cases/frmCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseReport&CaseID=" & e.Item.Cells(4).Text

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 23
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace