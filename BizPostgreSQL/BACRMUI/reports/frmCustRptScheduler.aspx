<%@ Page Language="vb" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="frmCustRptScheduler.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCustRptScheduler" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

  <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  

    <title>Report Scheduler</title>
    <script language="Javascript" type ="text/javascript">
    
 function Save()
 {
        var a = null;
        var f = document.forms[0];
        var e = f.elements["rdlRecurringTemplate"];

        for (var i=0; i < e.length; i++)
        {
           if (e[i].checked)
            {
                a = e[i].value;
                break;
            }
        }
           if (document.form1.ddlCustRpt.value == 0)
             {
                   alert("Select The Report");
    	           document.form1.ddlCustRpt.focus();
		           return false;
             }   
            if(document.form1.txtTemplateName.value=="")            
             {
                alert("Enter a Schedule Name");
    	        document.form1.txtTemplateName.focus();
		        return false;
             }
            
              
        if(a==0)
        {
            if(document.form1.txtDays.value=="" || document.form1.txtDays.value==0 || document.form1.txtDays.value>28)            
             {
                alert("Enter a valid dialy schedule");
    	        document.form1.txtDays.focus();
		        return false;
             }
        }
        if(a==1)
        {
           if(document.form1.rdbMonthlyDays.checked==true)
           {
              if(document.form1.txtMonthlyDays.value=="" ||document.form1.txtMonthlyDays.value==0 || document.form1.txtMonthlyDays.value>28)
              {
                  alert("Enter a valid monthly schedule");
    	          document.form1.txtMonthlyDays.focus();
		          return false;
              }
           }
             if(document.form1.rdbMonthlyWeekDays.checked==true)
               {
                    if(document.form1.txtMonthlyWeekDays.value=="" || document.form1.txtMonthlyWeekDays.value==0 ||document.form1.txtMonthlyWeekDays.value>28)
                    {
                        alert("Enter a valid monthly schedule");
                        document.form1.txtMonthlyWeekDays.focus();
                        return false; 
                    }
               }
        }
    
         if(document.form1.CalStartDate_txtDate.value=="")
         {
               alert("Enter Start Date");
               document.form1.CalStartDate_txtDate.focus();
               return false; 
         }
        
        if(document.form1.rdlStopAfter.checked==true && document.form1.rdlEndDate.checked==true)
        {
           if(document.form1.CalEndDate_txtDate.value=="")
             {
                   alert("Enter End Date");
                   document.form1.CalEndDate_txtDate.focus();
                   return false; 
             }
            
          
//            if(document.form1.CalStartDate_txtDate.value<document.form1.CalEndDate_txtDate.value)
//            {
//                   alert("Start Date is less than End Date");
//                   document.form1.CalStartDate_txtDate.focus();
//                   return false; 
//            }
            if(document.form1.CalStartDate_txtDate.value>document.form1.CalEndDate_txtDate.value)
            {
                   alert("End Date is less than Start Date");
                   document.form1.CalEndDate_txtDate.focus();
                   return false; 
            }
            
         }
       if(document.form1.rdlStopAfter.checked==true && document.form1.rdlNoTransaction.checked==true)
           {
               if(document.form1.txtNoTransactions.value=="" || document.form1.txtNoTransactions.value==0)
                {
                   alert("Enter Number of Transactions");
                   document.form1.txtNoTransactions.focus();
                   return false; 
               }
            }
 }

 function HideTempPanel()
 {
    //alert("Siva");
    var a = null;
    var f = document.forms[0];
    var e = f.elements["rdlRecurringTemplate"];

    for (var i=0; i < e.length; i++)
    {
       if (e[i].checked)
        {
            a = e[i].value;
            break;
        }
    }
  if(a==0)
    {
        document.getElementById("pnlDaily").style.visibility='';
        document.getElementById("pnlMonthly").style.visibility='hidden';
        document.getElementById("pnlYearly").style.visibility='hidden';
//        document.getElementById("txtDays").value=1;
    }
  if(a==1)
    {
        document.getElementById("pnlDaily").style.visibility='hidden';
        document.getElementById("pnlMonthly").style.visibility=''
        document.getElementById("pnlYearly").style.visibility='hidden';
//        document.getElementById("txtMonthlyDays").value=1;
//        document.getElementById("txtMonthlyWeekDays").value=1;
    }
  if(a==2)
    {
        document.getElementById("pnlDaily").style.visibility='hidden';
        document.getElementById("pnlMonthly").style.visibility='hidden';
        document.getElementById("pnlYearly").style.visibility='';
    }
					
 }
 
 function HidePanel()
 {
       alert('SP');
       document.getElementById("pnlMonthly").style.visibility='hidden';
       document.getElementById("pnlYearly").style.visibility='hidden';
 }
	
 function CheckNumber(cint)
    {
	    if (cint==1)
	        {
		        if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
		            {
					    window.event.keyCode=0;
		            }
	        }
       	if (cint==2)
	       {
		        if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
		        {
				    window.event.keyCode=0;
		    }
	}
						
}
	function OpemEmail(URL)
		{
			window.open(URL,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
			return false;
		}
		</script>
</head>
<body onload="HideTempPanel();">
    <form id="form1" runat="server" >
    
    <br />
  <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td >&nbsp;&nbsp;&nbsp;Report Scheduler &nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					
						<td align="right">
						<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save & Close"></asp:button>						
						<%--<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>--%>
						
				</td>
				</tr>
				 
			</TABLE>
			
			
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None" Height="300px">
				<asp:tableRow ID="TableRow1" VerticalAlign="Top" runat="server">
					<asp:tableCell ID="TableCell1" runat="server">
						<asp:table id="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="0px" Runat="server" Width="100%" 
									BorderColor="Black">
									<asp:tableRow ID="TableRow2" runat="server">
										<asp:tableCell ID="TableCell2" runat="server">
											<br/>
											<table id="tblDetails" runat="server" width="100%" border="0">
											    <tr id="Tr5" runat="server" align ="left">
												    <td></td>
												    <td id="Td7" class="normal1" align="left" runat="server" width="120px"  >
												        Select Saved Custom Report <font color="#ff0000">*</font>
													</td>
													<td id="Td8" class="normal1" runat="server" align ="left">
														<asp:DropDownList ID="ddlCustRpt" runat="server" CssClass="signup" Width="300px"></asp:DropDownList>
													</td>
												</tr>
												<tr id="Tr1" runat="server" align ="left">
												<td></td>
													<td id="Td1" class="normal1" align="left" runat="server" width="120px"  >Scheduler Name<font color="#ff0000">*</font>
													</td>
													<td id="Td2" class="normal1" runat="server" align ="left">
														<asp:TextBox ID="txtTemplateName" Width="150px" CssClass="signup" runat ="server" MaxLength="50"></asp:TextBox>
														</td>
													</tr>
												<tr id="Tr2" runat="server">
												<td></td>
													<td id="Td3" class="normal1" colspan="2" align="left" runat="server" width="120px"><b>Select Interval</b>
													</td>
																								
												</tr>
												<tr id="Tr3" runat="server" >
												<td></td>
												<td id="Td4" class="normal1" align="left"    runat="server" width ="80px">
												  <asp:Panel ID="pnlRaido" runat ="server" >
												   		 <asp:RadioButtonList ID="rdlRecurringTemplate" runat="server"  onclick="HideTempPanel();" RepeatDirection="vertical">
												  	  	  		 <asp:ListItem Value="0" Selected="true" Text="Daily" ></asp:ListItem>
												  	  	  		 <asp:ListItem Value="1" Text="Monthly"></asp:ListItem>
												  	  	  		 <asp:ListItem Value="2" Text="Yearly"></asp:ListItem>
												          </asp:RadioButtonList> 	
												        						  
												  </asp:Panel>
												</td>
												
												<td class="normal1" id="td5" runat="server"  align ="left"  >
												    <asp:Panel ID="pnlDaily" runat ="server">
												   		 Every <asp:TextBox ID="txtDays" runat ="server" Width="30px" Height="15px" MaxLength ="2"  Text ="1"></asp:TextBox> 	Day(s)
												        						  
												  </asp:Panel>
												  <asp:Panel ID="pnlMonthly" runat ="server"  >
												      <asp:RadioButton ID="rdbMonthlyDays" runat="server" GroupName ="GrpMonthly" Checked ="true"/>
												      <asp:DropDownList ID="ddlMonthlyDays"   runat ="server" width="50px"  CssClass="signup">
												      <asp:ListItem Value ="1" text="1st"></asp:ListItem>
												      <asp:ListItem Value ="2" text="2nd"></asp:ListItem>
												      <asp:ListItem Value ="3" text="3rd"></asp:ListItem>
												      <asp:ListItem Value ="4" text="4th"></asp:ListItem>
												      <asp:ListItem Value ="5" text="5th"></asp:ListItem>
												      <asp:ListItem Value ="6" text="6th"></asp:ListItem>
												      <asp:ListItem Value ="7" text="7th"></asp:ListItem>
												      <asp:ListItem Value ="8" text="8th"></asp:ListItem>
												      <asp:ListItem Value ="9" text="9th"></asp:ListItem>
												      <asp:ListItem Value ="10" text="10th"></asp:ListItem>
												      <asp:ListItem Value ="11" text="11th"></asp:ListItem>
												      <asp:ListItem Value ="12" text="12th"></asp:ListItem>
												      <asp:ListItem Value ="13" text="13th"></asp:ListItem>
												      <asp:ListItem Value ="14" text="14th"></asp:ListItem>
												      <asp:ListItem Value ="15" text="15th"></asp:ListItem>
												      <asp:ListItem Value ="16" text="16th"></asp:ListItem>
												      <asp:ListItem Value ="17" text="17th"></asp:ListItem>
												      <asp:ListItem Value ="18" text="18th"></asp:ListItem>
												      <asp:ListItem Value ="19" text="19th"></asp:ListItem>
												      <asp:ListItem Value ="20" text="20th"></asp:ListItem>
												      <asp:ListItem Value ="21" text="21th"></asp:ListItem>
												      <asp:ListItem Value ="22" text="22th"></asp:ListItem>
												      <asp:ListItem Value ="23" text="23th"></asp:ListItem>
												      <asp:ListItem Value ="24" text="24th"></asp:ListItem>
												      <asp:ListItem Value ="25" text="25th"></asp:ListItem>
												      <asp:ListItem Value ="26" text="26th"></asp:ListItem>
												      <asp:ListItem Value ="27" text="27th"></asp:ListItem>
												      <asp:ListItem Value ="28" text="28th"></asp:ListItem>
												     <%-- <asp:ListItem Value ="29" text="Last"></asp:ListItem>--%>
												      
												      </asp:DropDownList> of every 
												      <asp:TextBox ID="txtMonthlyDays" runat ="server" Width="30px" Height="15px" MaxLength ="2" Text ="1"></asp:TextBox> month(s) <br /><br />
												      <asp:RadioButton ID="rdbMonthlyWeekDays" runat="server"  GroupName ="GrpMonthly" />The
												       <asp:DropDownList ID="ddlMonthlyWeekDays" runat ="server" width="70px"  CssClass="signup">
												       <asp:ListItem Value ="1" text="First"></asp:ListItem>
												      <asp:ListItem Value ="2" text="Second"></asp:ListItem>
												      <asp:ListItem Value ="3" text="Third"></asp:ListItem>
												      <asp:ListItem Value ="4" text="Fourth"></asp:ListItem>
												      <asp:ListItem Value ="5" text="Last"></asp:ListItem>
												      </asp:DropDownList> 												      
												      <asp:DropDownList ID="ddlMonthlyWeek" runat ="server" width="80px"  CssClass="signup">
												      <asp:ListItem Value ="1" text="Sunday"></asp:ListItem>
												      <asp:ListItem Value ="2" text="Monday"></asp:ListItem>
												      <asp:ListItem Value ="3" text="Tuesday"></asp:ListItem>
												      <asp:ListItem Value ="4" text="Wednesday"></asp:ListItem>
												      <asp:ListItem Value ="5" text="Thursday"></asp:ListItem>
												      <asp:ListItem Value ="6" text="Friday"></asp:ListItem>
												      <asp:ListItem Value ="7" text="Saturday"></asp:ListItem>
												      </asp:DropDownList> 	
												       of every <asp:TextBox ID="txtMonthlyWeekDays" MaxLength ="2" runat ="server" Width="30px" Height="15px"  Text ="1"></asp:TextBox> month(s)
												  </asp:Panel>
												  <asp:Panel ID="pnlYearly" runat="server" >
												    Every   <asp:DropDownList ID="ddlYearlyMonth" runat ="server" width="100px"  CssClass="signup">
												    <asp:ListItem Value ="1" text="January"></asp:ListItem>
												      <asp:ListItem Value ="2" text="February"></asp:ListItem>
												      <asp:ListItem Value ="3" text="March"></asp:ListItem>
												      <asp:ListItem Value ="4" text="April"></asp:ListItem>
												      <asp:ListItem Value ="5" text="May"></asp:ListItem>
												      <asp:ListItem Value ="6" text="June"></asp:ListItem>
												      <asp:ListItem Value ="7" text="July"></asp:ListItem>
												      <asp:ListItem Value ="8" text="August"></asp:ListItem>
												      <asp:ListItem Value ="9" text="September"></asp:ListItem>
												      <asp:ListItem Value ="10" text="October"></asp:ListItem>
												      <asp:ListItem Value ="11" text="November"></asp:ListItem>
												      <asp:ListItem Value ="12" text="December"></asp:ListItem>												     
												    </asp:DropDownList> 
												     <asp:DropDownList ID="ddlYearlyDays" runat ="server" width="50px"  CssClass="signup">
												       <asp:ListItem Value ="1" text="1st"></asp:ListItem>
												      <asp:ListItem Value ="2" text="2nd"></asp:ListItem>
												      <asp:ListItem Value ="3" text="3rd"></asp:ListItem>
												      <asp:ListItem Value ="4" text="4th"></asp:ListItem>
												      <asp:ListItem Value ="5" text="5th"></asp:ListItem>
												      <asp:ListItem Value ="6" text="6th"></asp:ListItem>
												      <asp:ListItem Value ="7" text="7th"></asp:ListItem>
												      <asp:ListItem Value ="8" text="8th"></asp:ListItem>
												      <asp:ListItem Value ="9" text="9th"></asp:ListItem>
												      <asp:ListItem Value ="10" text="10th"></asp:ListItem>
												      <asp:ListItem Value ="11" text="11th"></asp:ListItem>
												      <asp:ListItem Value ="12" text="12th"></asp:ListItem>
												      <asp:ListItem Value ="13" text="13th"></asp:ListItem>
												      <asp:ListItem Value ="14" text="14th"></asp:ListItem>
												      <asp:ListItem Value ="15" text="15th"></asp:ListItem>
												      <asp:ListItem Value ="16" text="16th"></asp:ListItem>
												      <asp:ListItem Value ="17" text="17th"></asp:ListItem>
												      <asp:ListItem Value ="18" text="18th"></asp:ListItem>
												      <asp:ListItem Value ="19" text="19th"></asp:ListItem>
												      <asp:ListItem Value ="20" text="20th"></asp:ListItem>
												      <asp:ListItem Value ="21" text="21th"></asp:ListItem>
												      <asp:ListItem Value ="22" text="22th"></asp:ListItem>
												      <asp:ListItem Value ="23" text="23th"></asp:ListItem>
												      <asp:ListItem Value ="24" text="24th"></asp:ListItem>
												      <asp:ListItem Value ="25" text="25th"></asp:ListItem>
												      <asp:ListItem Value ="26" text="26th"></asp:ListItem>
												      <asp:ListItem Value ="27" text="27th"></asp:ListItem>
												      <asp:ListItem Value ="28" text="28th"></asp:ListItem>
												      <%--<asp:ListItem Value ="29" text="Last"></asp:ListItem>--%>
												     </asp:DropDownList> 
												  </asp:Panel>
									  </td>
									
												</tr>
												<tr id="Tr4" runat="server">
												<td></td>
													<td id="Td6" colspan ="2" class="normal1"   align="left" runat="server" width="110px"><b>Select Date Range</b>
													</td>
												 
												</tr>
												<tr class="normal1">
												<td></td>
												<td align ="left"  >Start on after<font color="#ff0000">*</font></td><td align ="left" > <BizCalendar:Calendar ID="CalStartDate" runat="server" />
												
												</td>
												
												
												</tr>
												<tr class="normal1">
													<td></td>
												<td align ="left">
											
												 <asp:RadioButton GroupName ="EndDate" ID="rdlNoEndDate" Runat="server" Checked ="true"  CssClass="normal1" Text ="No End Date"></asp:RadioButton>
				                                
												</td>
												</tr>
												<tr class="normal1">
												 <td></td>
												<td align ="left"  > <asp:RadioButton GroupName ="EndDate" ID="rdlStopAfter" Runat="server" CssClass="normal1"></asp:RadioButton> Stop after</td>
												<td>
												<table id="tblNoEndDate" >
												<tr>
												<td align ="left">
												 <asp:RadioButton GroupName ="NoEndDate" ID="rdlEndDate" Runat="server" Checked="true" CssClass="normal1" Text =""></asp:RadioButton>
												</td>
												<td align ="left">
												 <BizCalendar:Calendar ID="CalEndDate" runat="server" />
												</td>
												</tr>
												</table>
												</td>
												 
				                               	</tr>
				                               	<tr>
				                               	<td></td>
				                               	<td></td>
				                               		<td align ="left"> <asp:RadioButton GroupName ="NoEndDate" ID="rdlNoTransaction" Runat="server" CssClass="normal1" Text ="No of Transactions"></asp:RadioButton>
												 <asp:TextBox ID="txtNoTransactions" Width="30px" CssClass="signup" runat ="server" MaxLength="2"></asp:TextBox>
				                               	</td>
				                               	</tr>
											</table>											
										</asp:tableCell>
									</asp:tableRow>
								</asp:table>						
					</asp:tableCell>			    
				</asp:tableRow>
				<asp:TableRow>
				    <asp:TableCell CssClass="normal1">
				         After running report, email as attached PDF to the following employees:
				         <asp:DropDownList ID="ddlEmployeeList" runat="server" CssClass="signup" Width="150px"></asp:DropDownList>
				         &nbsp;<asp:Button Text="Add" CssClass="button" runat="server" ID="btnAddEmployee" />
				    </asp:TableCell>
				</asp:TableRow>
				<asp:TableRow>
				    <asp:TableCell CssClass="normal1">
				         <asp:DataGrid ID="dgEmployee" Width="100%" runat="server" AutoGenerateColumns="false" >
				          <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							                <ItemStyle CssClass="is"></ItemStyle>
							                <HeaderStyle CssClass="hs"></HeaderStyle>
							                <Columns>
							                 <asp:BoundColumn DataField="numContactId" Visible="False"></asp:BoundColumn>
							                    	<asp:TemplateColumn HeaderText="Email">
														<ItemTemplate>
															<asp:HyperLink ID="hplEmail" Runat="server" CssClass="hyperlink" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>' Target="_blank" >
															</asp:HyperLink>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText = ""> 														
														<ItemTemplate>
															<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>															
														</ItemTemplate>
													</asp:TemplateColumn>
							                </Columns>
					    </asp:DataGrid>
							             
				    </asp:TableCell>
				</asp:TableRow>
				</asp:table>
			 <asp:TextBox ID="txtScheduleId" runat="server"  Text="0" style="display:none"></asp:TextBox>
    </form>
</body>
</html>
