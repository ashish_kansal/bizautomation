Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class ActivityReportCompany : Inherits BACRMPage

        Protected WithEvents lblOppAmt As System.Web.UI.WebControls.Label
        Protected WithEvents lblLeadProm As System.Web.UI.WebControls.Label
        Protected WithEvents lblOppOpened As System.Web.UI.WebControls.Label
        Protected WithEvents lblTotPros As System.Web.UI.WebControls.Label
        Protected WithEvents lblNoPros As System.Web.UI.WebControls.Label
        Protected WithEvents lblTotAcc As System.Web.UI.WebControls.Label
        Protected WithEvents lblComSch As System.Web.UI.WebControls.Label
        Protected WithEvents lblComComp As System.Web.UI.WebControls.Label
        Protected WithEvents lblComPast As System.Web.UI.WebControls.Label
        Protected WithEvents lblTaskSch As System.Web.UI.WebControls.Label
        Protected WithEvents lblTaskPast As System.Web.UI.WebControls.Label
        Protected WithEvents lblCaseOpen As System.Web.UI.WebControls.Label
        Protected WithEvents lblConversion As System.Web.UI.WebControls.Label
        Protected WithEvents lblTaskComp As System.Web.UI.WebControls.Label
        Protected WithEvents lblCasePast As System.Web.UI.WebControls.Label
        Protected WithEvents lblCaseComp As System.Web.UI.WebControls.Label

        Protected WithEvents lnkLeadProm As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkTotPros As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkNoPros As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkTotAcc As System.Web.UI.WebControls.HyperLink

        Protected WithEvents lnkOppOpened As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkComSch As System.Web.UI.WebControls.HyperLink

        Protected WithEvents lnkComPast As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkTaskSch As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkComComp As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkTaskComp As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkTaskPast As System.Web.UI.WebControls.HyperLink

        Protected WithEvents lnkCaseOpen As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkCaseComp As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lnkCasePast As System.Web.UI.WebControls.HyperLink

        Dim dtFrom1 As Date
        Dim dtTo1 As Date
        Dim intId As Integer
        Dim strName As String
        Dim strType As String

        Protected WithEvents lblUserAcc As System.Web.UI.WebControls.Label
        Protected WithEvents lblDealCloseWon As System.Web.UI.WebControls.Label
        Protected WithEvents lnkDealCloseWon As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lblDealWonAmt As System.Web.UI.WebControls.Label
        Protected WithEvents lblDealLost As System.Web.UI.WebControls.Label
        Protected WithEvents hplDealLostLink As System.Web.UI.WebControls.HyperLink
        Protected WithEvents lblDealLostAmt As System.Web.UI.WebControls.Label
        Dim strActDate As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                dtFrom1 = GetQueryStringVal( "dtFrom1")
                dtTo1 = GetQueryStringVal( "dtTo1")
                strActDate = GetQueryStringVal( "strActDate")
                intId = GetQueryStringVal( "UserCntId")
                strName = GetQueryStringVal( "strName")
                strType = GetQueryStringVal( "frType")
                'sb_DisplayData(lngCntId, lngDivId, dtFrom1, dtTo1, strActDate)
                 ' = "Reports"
                sb_DisplayData(dtFrom1, dtTo1, strActDate, intId)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub sb_DisplayData(ByVal lngCntId As Long, ByVal lngDivId As Long, ByVal dtFrom As Long, ByVal dtTo As Long, ByVal strActDate As String)
        Private Sub sb_DisplayData(ByVal dtFrom As Date, ByVal dtTo As Date, ByVal strActDate As String, ByVal intId As Integer)
            Try
                lblUserAcc.Text = strName.Trim
                Dim stroptType As String
                Dim objPerformance As New Performance
                Dim dtPerformance As DataTable
                '' objPerformance.UserCntId = intId
                objPerformance.DomianId = Session("domainid")
                objPerformance.StartDate = dtFrom
                objPerformance.EndDate = dtTo
                objPerformance.PerType = 2
                objPerformance.TeamType = 5

                objPerformance.UserCntId = IIf(GetQueryStringVal( "UserCntID") <> "", GetQueryStringVal( "UserCntID"), Session("UserContactID"))
                dtPerformance = objPerformance.GetActivityReptByTeam

                '' Session("PerUserID") = intId
                If dtPerformance.Rows.Count = 1 Then
                    lblOppOpened.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("OppOpened"))
                    lblOppAmt.Text = String.Format("{0:#,##0.00}", dtPerformance.Rows(0).Item("OppOpenAmt"))
                    lblDealCloseWon.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("OppClosed"))
                    lblDealWonAmt.Text = String.Format("{0:#,##0.00}", dtPerformance.Rows(0).Item("OppCloseAmt"))
                    lblDealLost.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("OppLost"))
                    lblDealLostAmt.Text = String.Format("{0:#,##0.00}", dtPerformance.Rows(0).Item("OppLostAmt"))
                    lblLeadProm.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("LeadProm"))
                    lblTotPros.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TotProsp"))
                    lblNoPros.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("NoProspProm"))
                    lblTotAcc.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TotNoAccount"))
                    lblComSch.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CommSch"))
                    lblComComp.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CommComp"))
                    lblComPast.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CommPast"))
                    lblTaskSch.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TaskSch"))
                    lblTaskComp.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TaskComp"))
                    lblTaskPast.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TaskPast"))
                    lblCaseOpen.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CaseOpen"))
                    lblCaseComp.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CaseComp"))
                    lblCasePast.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CasePast"))
                    If Not (lblOppOpened.Text = 0 Or lblDealCloseWon.Text = 0) Then
                        lblConversion.Text = Format(CLng(lblDealCloseWon.Text) / (CLng(lblDealCloseWon.Text) + CLng(lblDealLost.Text)) * 100, "###0.00") & "%"
                    Else : lblConversion.Text = "0%"
                    End If
                    lnkLeadProm.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=L"
                    lnkTotPros.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=P"
                    lnkNoPros.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=R"
                    lnkTotAcc.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=A"

                    lnkOppOpened.NavigateUrl = "../reports/ActivityReportOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=O"
                    lnkDealCloseWon.NavigateUrl = "../reports/ActivityReportOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=W"
                    hplDealLostLink.NavigateUrl = "../reports/ActivityReportOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=L"

                    lnkComSch.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=S&Task=1"
                    lnkComComp.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=C&Task=1"
                    lnkComPast.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=P&Task=1"

                    lnkTaskSch.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=S&Task=3"
                    lnkTaskComp.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=C&Task=3"
                    lnkTaskPast.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=P&Task=3"

                    lnkCaseOpen.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=O"
                    lnkCaseComp.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=C"
                    lnkCasePast.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 2 & "&RepType=P"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace