<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCaseComparison.aspx.vb"
    Inherits="frmCaseComparison" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Case Comparison</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
Case Comparison
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table1" BorderWidth="1" CellPadding="0" CellSpacing="0" Width="500px"
        Height="350" runat="server" GridLines="None" BorderColor="black">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgList" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                   AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numUserID" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcFirstName" HeaderText="First Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcLastName" HeaderText="Last Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="NoofOpp"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:HyperLink ID="hplOpen" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
