﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmKPIReportList
    Inherits BACRMPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                BindDataGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindDataGrid()
        Try
            Dim objReportManage As New CustomReportsManage
            objReportManage.DomainID = Session("DomainId")

            Dim dt As DataTable = objReportManage.GetReportKPIGroupListMaster()

            gvReport.DataSource = dt
            gvReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvReport_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReport.RowCommand
        Try
            If e.CommandName = "KPIDetail" Then
                Response.Redirect("../reports/frmManageKPIReport.aspx?KPIGroupID=" & gvReport.DataKeys(e.CommandArgument).Value, False)
            ElseIf e.CommandName = "DeleteReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                Dim objReportManage As New CustomReportsManage
                objReportManage.DomainID = Session("DomainId")
                objReportManage.ReportKPIGroupID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                objReportManage.tintMode = 2
                objReportManage.ManageReportKPIGroupListMaster()

                BindDataGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class