<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProjectStatus.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmProjectStatus"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Project Status</title>		
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  

		<script language="javascript">
		function OpenSelTeam(repNo)
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function PopupCheck()
		{
		    document.frmProjectStatus.btnGo.click()
		}
		</script>
	</HEAD>
	<body>
		
		<form id="frmProjectStatus" method="post" runat="server">
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<table width="100%">
				<tr>
					<td class="normal1">
						&nbsp;
						<table>
						    <tr>
						        <td>
						        <table>
						            <tr>
						                <td> <asp:CheckBox ID="chkOnlyCompleted" Runat="server" CssClass="normal1"></asp:CheckBox>
						                    Only show Projects where the last stage was completed on or before
						                </td>
						                <td>
						                <BizCalendar:Calendar ID="cal" runat="server" />
						                </td>
						            </tr>
						        </table>						        
						        </td>						      
						        <td>
						        <asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>
						        </td>
							</tr>
					</table>
					</td>
					<td>
						<asp:RadioButtonList ID="rdlReportType" Runat="server" AutoPostBack="True" CssClass="normal1" RepeatLayout="Table"
							RepeatDirection="Horizontal">
							<asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
							<asp:ListItem Value="2">Team Selected</asp:ListItem>
							<asp:ListItem Value="3">Territory Selected</asp:ListItem>
						</asp:RadioButtonList>
					</td>
					<td align="right" colspan="2">
						<asp:Button ID="btnAddtoMyRtpList" Runat="server" CssClass="button" Text="Add To My Reports"></asp:Button>
					</td>
				</tr>
		
			</table>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; Project Status 
									&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:Button ID="btnChooseTeams" Runat="server" Width="120" CssClass="button" Text="Choose Teams"></asp:Button>&nbsp;
						<asp:Button ID="btnChooseTerritories" Runat="server" Width="120" CssClass="button" Text="Choose Territories"></asp:Button>&nbsp;
						<asp:button id="btnExportToExcel" Runat="server" Width="120" Text="Export to Excel" CssClass="button"></asp:button>&nbsp;
						<asp:Button id="btnPrint" Runat="server" CssClass="button" Text="Print" Width="50"></asp:Button>
						<asp:Button ID="btnBack" Runat="server" CssClass="button" Text="Back" Width="50"></asp:Button>
					</td>
				</tr>
			</table>
			<asp:table id="Table9" BorderWidth="1" Height="400" CellPadding="0" CellSpacing="0" Runat="server" CssClass="aspTable"
				Width="100%" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:DataGrid ID="dgProjectStatus" CssClass="dg" Width="100%" Runat="server" BorderColor="white"
							AutoGenerateColumns="False" AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numProID" Visible="false"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="DueDate" HeaderText="<font color=white>Due Date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "DueDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:buttoncolumn CommandName="Project" DataTextField="ProjectName" SortExpression="ProjectName" HeaderText="<font color=white>Project Name</font>"></asp:buttoncolumn>
								<asp:BoundColumn DataField="LastMileStoneCompleted" DataFormatString="{0:f}" SortExpression="LastMileStoneCompleted"
									HeaderText="<font color=white>Last Milestone Completed (%)</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="LastStageCompleted" SortExpression="LastStageCompleted" HeaderText="<font color=white>Last Stage Completed</font>"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			<asp:PostBackTrigger ControlID="btnPrint" />
			
			</Triggers>
			</asp:updatepanel>

		</form>
	</body>
</HTML>
