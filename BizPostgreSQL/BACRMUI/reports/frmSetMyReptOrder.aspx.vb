Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Partial Class frmSetMyReptOrder
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                 ' = "Reports"
                GetReportslist()
            End If
            btnDelete.Attributes.Add("OnClick", "return DeleteLinks(document.Form1.lstLinks)")
            btnClose.Attributes.Add("onclick", "return Close()")
            btnSaveOrder.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub GetReportslist()
        Try
            Dim objPredefinedReports As New PredefinedReports
            Dim dtMyReportList As DataTable
            Dim i As Integer
            Dim objCell As TableCell
            Dim objRow As TableRow
            Dim hplLink As HyperLink
            objPredefinedReports.UserCntID = Session("UserContactID")
            dtMyReportList = objPredefinedReports.GetMyReportList
            lstLinks.DataSource = dtMyReportList
            lstLinks.Items.Clear()
            For Each row As DataRow In dtMyReportList.Rows
                Dim itm As New ListItem
                itm.Text = row.Item("Report")
                itm.Value = row.Item("rptID") & "~" & row.Item("tintType")
                lstLinks.Items.Add(itm)
            Next
            'lstLinks.DataTextField = "Report"
            'lstLinks.DataValueField = "rptID"
            'lstLinks.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOrder.Click
        Try
            Dim strLinks As String()
            Dim i As Integer
            strLinks = txthidden.Text.Split(",")
            Dim dtLinks As New DataTable
            dtLinks.Columns.Add("RPTID")
            dtLinks.Columns.Add("Sequence")
            dtLinks.Columns.Add("Type")
            Dim dr As DataRow
            For i = 0 To strLinks.Length - 2
                dr = dtLinks.NewRow
                dr("RPTID") = strLinks(i).Split("~")(0)
                dr("Type") = strLinks(i).Split("~")(1)
                dr("Sequence") = i + 1
                dtLinks.Rows.Add(dr)
            Next
            Dim objPedefinedreports As New PredefinedReports
            Dim dsNew As New DataSet
            dtLinks.TableName = "Table"
            dsNew.Tables.Add(dtLinks.Copy)
            objPedefinedreports.strLinks = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            objPedefinedreports.UserCntID = Session("UserContactID")
            objPedefinedreports.SaveReportsOrder()
            GetReportslist()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
