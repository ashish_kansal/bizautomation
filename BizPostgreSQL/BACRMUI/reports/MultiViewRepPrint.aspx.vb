''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : MultiViewRepPrint
''' 
''' -----------------------------------------------------------------------------
''' 
''' <summary>
'''     This module will get all data from the Database and display 
''' it in the MultiView Report and then Print the Page.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Maha]	18/06/2005	Created
''' </history>
''' -----------------------------------------------------------------------------

Imports BACRM.BusinessLogic.Reports
Imports System.Data

Namespace BACRM.UserInterface.Reports
    Public Class MultiViewRepPrint
        Inherits BACRMPage

#Region "Declarations"
        Protected WithEvents tblSalesPeople As System.Web.UI.WebControls.Table
        Protected WithEvents tblItemSold As System.Web.UI.WebControls.Table
        Protected WithEvents tblAccounts As System.Web.UI.WebControls.Table
        Protected WithEvents tblDealSources As System.Web.UI.WebControls.Table
        Protected WithEvents tblColor1 As System.Web.UI.WebControls.Table
        Protected WithEvents tblColor2 As System.Web.UI.WebControls.Table
        Protected WithEvents tblColor3 As System.Web.UI.WebControls.Table
        Protected WithEvents tblDealsInPipeline As System.Web.UI.WebControls.Table
        Protected WithEvents tblDealHistory As System.Web.UI.WebControls.Table
        Protected WithEvents radMonth As System.Web.UI.WebControls.RadioButton
        Protected WithEvents radQuarter As System.Web.UI.WebControls.RadioButton
        Protected WithEvents radYTD As System.Web.UI.WebControls.RadioButton
        Protected WithEvents imgbtnGo As System.Web.UI.WebControls.ImageButton
        Protected WithEvents lblRepTitle As System.Web.UI.WebControls.Label
        Protected WithEvents lblQuotaAmt As System.Web.UI.WebControls.Label
        Protected WithEvents imgQuotaAmt As System.Web.UI.WebControls.Image
        Protected WithEvents imgDealWonAmt As System.Web.UI.WebControls.Image
        Protected WithEvents lblDealWonAmt As System.Web.UI.WebControls.Label
        Protected WithEvents lblPipelineAmt As System.Web.UI.WebControls.Label
        Protected WithEvents imgPipelineAmt As System.Web.UI.WebControls.Image

        'Define an Array for the rights of the user.
        Dim intm_aryRightsForPage() As Integer


        'Define Boolean variable to check if only MyAccounts need to be shown for Top 5 Accounts.
        'If it is false, Corporate Wide would be shown.
        Dim bolMyAcctTopAccts As Boolean
        Protected WithEvents SalesVsQuota As System.Web.UI.WebControls.Table

        'Define Boolean variable to check if only MyAccounts need to be shown for Top 10 Deals in Pipeline.
        'If it is false, Corporate Wide would be shown.
        Dim bolMyAcctTopPipeDeals As Boolean
#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime thepage is called. In this event we will 
        ''' get the data from the DB and populate the Tables for the MultiView Report.
        ''' </summary>
        ''' <param name="sender">Returns the sender as object.</param>
        ''' <param name="e">Returns the object event args.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	18/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Get the User rights for territory.        
            intm_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights("MultiViewRep.aspx", Session("userID"), 24, 1)

            If Not IsPostBack Then
                PopulateMiniReports()
            End If
        End Sub


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to fetch all the necessary data from DB and
        ''' populates in to the mini reports.
        ''' </summary>
        ''' <param name="intFromDate">Represents the From date.</param>
        ''' <param name="intToDate">Represents the To date.</param>
        ''' <param name="intFromMonth">Represents the From month.</param>
        ''' <param name="intToMonth">Represents the To month.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	17/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub sb_FillData(ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal intFromMonth As Integer, ByVal intToMonth As Integer)

            'Declare A variable for the row counter.
            Dim intRowCounter As Integer
            'Declare a variable for the column counter.
            Dim intColCounter As Integer

            Dim objDashBoard As New DashBoard
            Dim dtGetMySales As DataTable
            Dim intMySelf As Integer
            intMySelf = Request.QueryString("MySelf")

            Try

                'Display records based on selection of Radio Button
                'MySelf or TeamSelected
                If intMySelf = 1 Then
                    objDashBoard.TypeId = 0
                Else
                    objDashBoard.TypeId = 8
                End If

                'Get the Data for the My Sales Amt.
                '***** New code by Maha **************
                objDashBoard.DomainID = Session("DomainID")
                objDashBoard.FromDate = dtFromDate
                objDashBoard.ToDate = dtToDate
                objDashBoard.UserCntID = Session("UserContactID")
                'dtGetMySales = objDashBoard.GetMySales

                Dim dtGetTop5Sales As DataTable
                objDashBoard.UserRights = intm_aryRightsForPage(RIGHTSTYPE.VIEW)
                dtGetTop5Sales = objDashBoard.GetTop5Sales
                dgTop5sales.DataSource = dtGetTop5Sales
                dgTop5sales.DataBind()



                Dim dtTop5ItemsSold As DataTable
                dtTop5ItemsSold = objDashBoard.GetTop5ItemsSold
                objDashBoard.UserRights = intm_aryRightsForPage(RIGHTSTYPE.VIEW)
                dgTop5itemsSold.DataSource = dtTop5ItemsSold
                dgTop5itemsSold.DataBind()

                '*****************
                'Get the Data for the Top 5 Accounts.
                Dim dtGetTop5Accounts As DataTable
                objDashBoard.UserRights = intm_aryRightsForPage(RIGHTSTYPE.VIEW)
                dtGetTop5Accounts = objDashBoard.GetTop5Accounts
                dgTop5Accounts.DataSource = dtGetTop5Accounts
                dgTop5Accounts.DataBind()

                '*****************
                'Get the Data for the Top 5 Deals Sources.
                Dim dtGetTop5DealSource As DataTable
                objDashBoard.UserRights = intm_aryRightsForPage(RIGHTSTYPE.VIEW)
                dtGetTop5DealSource = objDashBoard.GetTop5DealSource
                dgTop5DealSources.DataSource = dtGetTop5DealSource
                dgTop5DealSources.DataBind()

                '*****************
                'Get the Data for the Sales Vs Quota.
                Dim strHTMLBreaks As String
                Dim dtGetSalesVsQuota As DataTable
                Dim intGetSalesVsQuotaCount As Integer
                objDashBoard.GetYear = Year(dtFromDate)
                objDashBoard.FromMonth = intFromMonth
                objDashBoard.ToMonth = intToMonth
                dtGetSalesVsQuota = objDashBoard.GetSalesVsQuota
                'drdrMultiViewRep.Read()
                Dim tblRow As TableRow
                Dim tblCell As TableCell
                Dim img As System.Web.UI.WebControls.Image

                tblRow = New TableRow
                tblRow.Height = Unit.Pixel(100)
                tblCell = New TableCell
                tblCell.Text = "Quota=<br>" & String.Format("{0:#,##0.00}", dtGetSalesVsQuota.Rows(0).Item("Quota"))
                tblCell.CssClass = "normal1"
                tblCell.VerticalAlign = VerticalAlign.Bottom
                tblCell.Height = Unit.Pixel(100)
                tblCell.Width = Unit.Pixel(30)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                img = New System.Web.UI.WebControls.Image
                img.Width = Unit.Pixel(10)
                img.Height = Unit.Pixel(dtGetSalesVsQuota.Rows(0).Item("QuotaPer"))
                img.BackColor = System.Drawing.Color.FromName("#52658C")
                tblCell.VerticalAlign = VerticalAlign.Bottom
                tblCell.Controls.Add(img)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Width = Unit.Pixel(30)
                tblCell.Text = "Deals=<br>" & String.Format("{0:#,##0.00}", dtGetSalesVsQuota.Rows(0).Item("DealsWon"))
                tblCell.VerticalAlign = VerticalAlign.Bottom
                tblCell.CssClass = "normal1"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                img = New System.Web.UI.WebControls.Image
                img.Width = Unit.Pixel(10)
                img.Height = Unit.Pixel(dtGetSalesVsQuota.Rows(0).Item("DealsWonPer"))
                img.BackColor = System.Drawing.Color.FromName("#52658C")
                tblCell.VerticalAlign = VerticalAlign.Bottom
                tblCell.Controls.Add(img)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Width = Unit.Pixel(30)
                tblCell.Text = "Pipeline=<br>" & String.Format("{0:#,##0.00}", dtGetSalesVsQuota.Rows(0).Item("Pipeline"))
                tblCell.VerticalAlign = VerticalAlign.Bottom
                tblCell.CssClass = "normal1"
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                img = New System.Web.UI.WebControls.Image
                img.Width = Unit.Pixel(10)
                img.Height = Unit.Pixel(dtGetSalesVsQuota.Rows(0).Item("PipelinePer"))
                img.BackColor = System.Drawing.Color.FromName("#52658C")
                tblCell.VerticalAlign = VerticalAlign.Bottom
                tblCell.Controls.Add(img)
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Width = Unit.Pixel(30)
                tblRow.Cells.Add(tblCell)

                tblSales.Rows.Add(tblRow)





                '*****************
                'Get the Data for the Top 10 Deals In Pipeline.
                Dim dtGetTop10DealsInPipeline As DataTable
                Dim intGetTop10DealsInPiplelineCount As Integer
                objDashBoard.UserRights = intm_aryRightsForPage(RIGHTSTYPE.VIEW)
                dtGetTop10DealsInPipeline = objDashBoard.GetTop10DealsInPipeline

                dgTop10dealsPipeline.DataSource = dtGetTop10DealsInPipeline
                dgTop10dealsPipeline.DataBind()
            Catch ex As Exception
                Throw New Exception("errors", ex)
            End Try

        End Sub

        Sub LoadQuarterData()
            Dim intCrtMth As Int16 = Month(Now())
            Dim dtDate As Date
            Dim quarter As Integer
            quarter = Request.QueryString("quarter")
            Dim Year As Integer
            Year = Request.QueryString("Year")

            Select Case quarter
                Case 0
                    '' sb_FillData("01/01/" & ddListYear.SelectedItem.Text, "03/31/" & ddListYear.SelectedItem.Text, 1, 3)
                    sb_FillData(New Date(Year, 1, 1), New Date(Year, 3, 1).AddMonths(1).AddDays(-1), 1, 3)
                Case 1
                    ''dt = New Date(ddListYear.SelectedItem.Text, 4, 1)
                    ''sb_FillData("04/01/" & ddListYear.SelectedItem.Text, "06/30/" & ddListYear.SelectedItem.Text, 4, 6)
                    sb_FillData(New Date(Year, 4, 1), New Date(Year, 6, 1).AddMonths(1).AddDays(-1), 4, 6)
                Case 2
                    ''sb_FillData("07/01/" & ddListYear.SelectedItem.Text, "09/30/" & ddListYear.SelectedItem.Text, 7, 9)
                    sb_FillData(New Date(Year, 7, 1), New Date(Year, 9, 1).AddMonths(1).AddDays(-1), 7, 9)
                Case 3
                    ''sb_FillData("10/01/" & ddListYear.SelectedItem.Text, "12/31/" & ddListYear.SelectedItem.Text, 10, 12)
                    sb_FillData(New Date(Year, 10, 1), New Date(Year, 12, 1).AddMonths(1).AddDays(-1), 10, 12)
            End Select
        End Sub
        Private Sub PopulateMiniReports()
            Dim objDashboard As New DashBoard
            Dim Criteria As Integer
            Criteria = Request.QueryString("Criteria")
            Dim Year1 As Integer
            Year1 = Request.QueryString("Year")
            Dim month1 As Integer
            month1 = Request.QueryString("month")
            Dim quarter As Integer
            quarter = Request.QueryString("quarter")

            Select Case Criteria

                Case "1"
                    Dim intCrtMth As Int16 = Month(Now())
                    If Year1 > 0 Then
                        If Year1 = Year(Now()) Then
                            sb_FillData(New Date(Year1, 1, 1), Now(), 1, intCrtMth)
                        Else
                            sb_FillData(New Date(Year1, 1, 1), New Date(Year1, 12, 31), 1, 12)
                        End If
                    End If
                Case "2"

                    Dim intCrtMth As Int16 = Month(Now())
                    If quarter > 0 Then
                        Select Case quarter
                            Case 1

                                sb_FillData(New Date(Year1, 1, 1), New Date(Year1, 3, 31), 1, 3)
                            Case 2
                                sb_FillData(New Date(Year1, 4, 1), New Date(Year1, 6, 30), 4, 6)
                            Case 3
                                sb_FillData(New Date(Year1, 7, 1), New Date(Year1, 9, 30), 7, 9)
                            Case 4
                                sb_FillData(New Date(Year1, 10, 1), New Date(Year1, 12, 31), 10, 12)
                        End Select
                    End If
                Case "3"

                    Dim CurrentDate As Date = New Date(Year1, month1, 1)
                    If Year1 > 0 And Year1 > 0 Then
                        sb_FillData(New Date(Year1, month1, 1), New Date(Year1, month1, objDashboard.GetLastDay(CurrentDate)), Format(month1, "00"), Format(month1, "00"))
                    End If
            End Select

        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This This procedure will format the dates for the fucntion to get data 
        ''' for report for printing.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	18/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------

    End Class
End Namespace