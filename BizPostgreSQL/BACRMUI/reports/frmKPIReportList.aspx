﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmKPIReportList.aspx.vb" Inherits=".frmKPIReportList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>KPI Report List</title>
    <script type="text/javascript" language="javascript">
        function NewReport() {
            window.location.href = "../reports/frmManageKPIReport.aspx";
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected Report?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <asp:Literal ID="litMessage" runat="server" EnableViewState="false"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnNewReport" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="true" OnClientClick="return NewReport()"><i class="fa fa-plus-circle"></i>&nbsp;New KPI or Scorecard Portlet</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    KPI Reports

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:Table ID="table2" CellPadding="0" CellSpacing="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="red" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-responsive table-bordered" Width="100%" DataKeyNames="numReportKPIGroupID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:ButtonField DataTextField="vcKPIGroupName" CommandName="KPIDetail" SortExpression="vcRuleName"
                            HeaderText="KPI Group" ButtonType="Link"></asp:ButtonField>
                        <asp:BoundField DataField="vcKPIGroupDescription" SortExpression="vcKPIGroupDescription"
                            HeaderText="Description"></asp:BoundField>
                        <asp:BoundField DataField="vcKPIGroupReportType" SortExpression="vcKPIGroupReportType"
                            HeaderText="Type"></asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick="javascript:return DeleteRecord();" Text="X" CommandName="DeleteReport"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No records found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
