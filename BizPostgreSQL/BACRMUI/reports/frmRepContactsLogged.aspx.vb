Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmRepContactsLogged : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents dgContacts As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim dtContacts As DataTable
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.Division = GetQueryStringVal( "DivID")
                dtContacts = objPredefinedReports.GetReptPortalConatcts
                dgContacts.DataSource = dtContacts
                dgContacts.DataBind()
                 ' = "Reports"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgContacts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContacts.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplNoOfTimes As HyperLink
                    hplNoOfTimes = e.Item.FindControl("hplNoOfTimes")
                    hplNoOfTimes.Attributes.Add("onclick", "return OpenBizList(" & e.Item.Cells(0).Text & ")")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
