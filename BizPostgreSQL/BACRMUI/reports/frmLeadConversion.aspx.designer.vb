'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Reports

    Partial Public Class frmLeadConversion

        '''<summary>
        '''calFrom control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents calFrom As Global.BACRM.Include.calandar

        '''<summary>
        '''calTo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents calTo As Global.BACRM.Include.calandar

        '''<summary>
        '''btnSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''rdlReportType control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rdlReportType As Global.System.Web.UI.WebControls.RadioButtonList

        '''<summary>
        '''lblEmployee control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblEmployee As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''ddlEmployee control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlEmployee As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''lblLeads control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblLeads As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''ddlLeads control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlLeads As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''btnAddtoMyRtpList control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddtoMyRtpList As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''btnChooseTeams control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnChooseTeams As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''btnPrint control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnPrint As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''btnBack control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnBack As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''rdlLeadConversion control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rdlLeadConversion As Global.System.Web.UI.WebControls.RadioButtonList

        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

        '''<summary>
        '''Table9 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Table9 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''AmountWon1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents AmountWon1 As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''Label1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''AmountWon2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents AmountWon2 As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''TotalAmountWon control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents TotalAmountWon As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''btnGo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
