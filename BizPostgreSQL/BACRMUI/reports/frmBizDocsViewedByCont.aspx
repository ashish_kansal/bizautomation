<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBizDocsViewedByCont.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmBizDocsViewedByCont"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>BizDocs Viewed By Contacts</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<table>
                <td width="100%"></td>
                <td  align="center">
		           <asp:UpdateProgress ID ="up1"  runat="server" DynamicLayout="false">
				        <ProgressTemplate>
				         <asp:Image ID="Image1"  ImageUrl="~/images/updating.gif" runat="server"/>
				         </ProgressTemplate>
    			    </asp:UpdateProgress>
    		    </td>
	    </table>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<asp:DataGrid ID="dgBizDocs" CssClass="dg" Width="100%" Runat="server" AutoGenerateColumns="false"
				AllowSorting="True">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					
					<asp:BoundColumn DataField="vcBizDocID" SortExpression="vcBizDocID" HeaderText="<font color=white>BizDoc Name</font>"></asp:BoundColumn>
						<asp:BoundColumn DataField="Date" SortExpression="Date" HeaderText="<font color=white>Access Date, Time </font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="BizDoc" SortExpression="BizDoc" HeaderText="<font color=white>BizDoc Type</font>"></asp:BoundColumn>
				
					<asp:BoundColumn DataField="ActivityType" SortExpression="ActivityType" HeaderText="<font color=white>Activity Type</font>"></asp:BoundColumn>
					
				</Columns>
			</asp:DataGrid>
			</ContentTemplate>
			</asp:updatepanel>
		</form>
	</body>
</HTML>
