<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOpenOpportunities.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmOpenOpportunities" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Predefined Report</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            //  alert("Check");
            document.form1.btnGo.click()

        }
    </script>
     <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
     <div class="row form-group">
        <div class="col-md-12">
              <div class="pull-right">
                 <asp:RadioButtonList ID="rdlReportType" CssClass="normal1" runat="server" AutoPostBack="True"
                    RepeatDirection="Horizontal" RepeatLayout="Table">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            </div>
         </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
                <label>
                    Opportunity Type
                </label>
               <asp:DropDownList ID="ddlOpportunityType" CssClass="form-control" runat="server" AutoPostBack="True"
                                Width="130">
                                <asp:ListItem Value="0">Sales</asp:ListItem>
                                <asp:ListItem Value="1">Purchase</asp:ListItem>
                            </asp:DropDownList>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnAddtoMyRtpList" CssClass="btn btn-primary" runat="server"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                <asp:LinkButton ID="btnChooseTeams" CssClass="btn btn-primary" runat="server">Choose Teams</asp:LinkButton>
                 <asp:LinkButton ID="btnChooseTerritories" CssClass="btn btn-primary" runat="server"
                                        >Choose Territories</asp:LinkButton>
                <asp:LinkButton ID="btnExportToExcel" CssClass="btn btn-primary" runat="server" ><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                <asp:LinkButton ID="btnPrint" CssClass="btn btn-primary" runat="server"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server"  ><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                </asp:UpdatePanel>
                
            </div>
                </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Open Opportunity
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:DataGrid ID="dgOpportunityOpen" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" runat="server" AutoGenerateColumns="false"
        AllowSorting="true">
        <AlternatingItemStyle CssClass="ais" />
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="numOppID" Visible="false"></asp:BoundColumn>
            <asp:ButtonColumn DataTextField="OpportunityName" CommandName="Opportuntity" SortExpression="OpportunityName"
                HeaderText="Opportunity Name"></asp:ButtonColumn>
            <asp:TemplateColumn HeaderText="Date Created" SortExpression="bintCreatedDate">
                <ItemTemplate>
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate"))%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="LastMileStoneCompleted" DataFormatString="{0:f}" SortExpression="LastMileStoneCompleted"
                HeaderText="Last Milestone Completed (%)"></asp:BoundColumn>
            <asp:BoundColumn DataField="LastStageCompleted" SortExpression="LastStageCompleted"
                HeaderText="Last Stage Completed"></asp:BoundColumn>
            <asp:BoundColumn DataField="numRecOwner" Visible="true" SortExpression="numRecOwner"
                HeaderText="Record Owner"></asp:BoundColumn>
            <asp:BoundColumn DataField="numAssignedTo" Visible="true" SortExpression="numAssignedTo"
                HeaderText=" Assigned To"></asp:BoundColumn>
            <asp:BoundColumn DataField="monPAmount" DataFormatString="{0:#,#0.00}" Visible="true"
                SortExpression="monPAmount" HeaderText="Deal Amount"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:Button ID="btnGo" runat="server" Style="display: none"></asp:Button>
        </div>
</asp:Content>
