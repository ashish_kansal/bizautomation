''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmCustomReportConfigSaver
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is the Custom Report Config Repository file(it saves the report configuration) and also Initiates the Report in a New Window
'''     (This is done so that the form does not submit and the custom report config parameters are not lost)
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	11/04/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Partial Class frmCustomReportConfigSaver : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim objCustomReports As New CustomReports
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the page is called.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/04/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Put user code to initialize the page here
            With objCustomReports
                .DomainID = Session("DomainID")                                 'Set the Domain ID
                .UserCntID = Session("userContactId")                                     'Set the Domain ID
                .XMLFilePath = CCommon.GetDocumentPhysicalPath() 'set the file path
            End With

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the Open Report button is clicked.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    '''     This opens a popup window and calls the form to create the report in the new poopup window
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/04/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnOpenReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenReport.Click
        Try
            Dim sCustRptOrderedColumns As String = hdCustomReportConfig.Value           'To contain the list of columns in the Generated Report
            objCustomReports.saveCustomReportConfig(sCustRptOrderedColumns)             'Save the new xml file
            litClientMessage.Text = "<script language='javascript'>OpenCustomReport();</script>" 'Attach client side script to the literal to open a new window
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
