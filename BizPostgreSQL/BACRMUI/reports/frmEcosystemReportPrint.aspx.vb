Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmEcosystemReportPrint : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblreportby As System.Web.UI.WebControls.Label
        Protected WithEvents lblcurrent As System.Web.UI.WebControls.Label
        Protected WithEvents lblReportHeader As System.Web.UI.WebControls.Label
        Protected WithEvents dgEcosystemReport As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim SortField As String
        Dim dtEcosystem As DataTable
        Dim objPredefinedReports As New PredefinedReports

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                DisplayRecords()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.Customer = GetQueryStringVal( "Customer")
                objPredefinedReports.Division = GetQueryStringVal( "DivID")
                objPredefinedReports.TerritoryID = Session("TerritoryID")

                Select Case GetQueryStringVal( "ReportType")
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select

                'If SortField = "" Then
                'SortField = "Employee"
                'End If
                SortField = GetQueryStringVal( "sortby")

                objPredefinedReports.ReportType = 26
                dtEcosystem = objPredefinedReports.GetEcoSystemReport

                Dim dv As DataView = New DataView(dtEcosystem)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgEcosystemReport.DataSource = dv
                ' dgEcosystemReport.DataSource = dtEcosystem.DataSet
                'dgAverageSalesCycle.CssClass = "normalbol"
                'dgAverageSalesCycle.ItemStyle.CssClass = "tr1"
                dgEcosystemReport.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace