<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCampaign.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCampaign"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title> Campaign</title>
		
		<SCRIPT language="JavaScript" src="../javascript/date-picker.js" type="text/javascript">
		</SCRIPT>
		<script language="javascript">
		function OpenPopup(fileName,id)
		{
			if(id.length!=0)
			{
			window.open(fileName + "?Id=" + id,'','toolbar=no,titlebar=no,left=200, top=300,width=800,height=450,scrollbars=no,resizable=yes')
			return false;
			}
			else
			{
			window.open(fileName + "?Id=" + id,'','toolbar=no,titlebar=no,left=200, top=300,width=800,height=450,scrollbars=no,resizable=yes')
			return false;
			}
		}
		function OpenTerritoryPopup(fileName,id)
		{
			if(id.length!=0)
			{
			window.open(fileName + "?Id=" + id,'','toolbar=no,titlebar=no,left=200, top=300,width=200,height=200,scrollbars=no,resizable=yes')
			return false;
			}
			else
			{
			window.open(fileName + "?Id=" + id,'','toolbar=no,titlebar=no,left=200, top=300,width=200,height=200,scrollbars=no,resizable=yes')
			return false;
			}
		}
		
		function OpenSelCampaign(repNo)
		{
		
			window.open("../Reports/frmSelectCampaign.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=700,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenSelTeam(repNo)
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenCampDetails(CampID)
		{
		
			window.open("../Reports/frmCampaignDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CampID="+CampID,'','toolbar=no,titlebar=no,left=200, top=300,width=700,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function PopupCheck()
		{
		    document.frmCampaign.btnGo.click()
		}
		</script>
	</HEAD>
	<body>
		
		<form id="frmCampaign" method="post" runat="server">
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
	
		
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<table width="100%">
				<tr>
					<td><asp:radiobuttonlist id="rdlReportType" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="normal1"
							AutoPostBack="True" Runat="server">
							<asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
							<asp:ListItem Value="2">Team Selected</asp:ListItem>
							<asp:ListItem Value="3">Territory Selected</asp:ListItem>
						</asp:radiobuttonlist></td>
					<td align="right"><asp:button id="btnAddtoMyRtpList" CssClass="button" Text="Add To My Reports" Runat="server"></asp:button></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; Campaign Report 
									&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnOpenCampaign" CssClass="button" Runat="server" Text="Select Open Campaigns"
							Width="150"></asp:button>
						<asp:button id="btnChooseTeams" CssClass="button" Runat="server" Text="Choose Teams" Width="120"></asp:button>
						<asp:button id="btnChooseTerritories" CssClass="button" Runat="server" Text="Choose Territories"
							Width="120"></asp:button>
						<asp:button id="btnExportToExcel" CssClass="button" Runat="server" Text="Export to Excel" Width="120"></asp:button>
						<asp:button id="btnPrint" CssClass="button" Runat="server" Text="Print" Width="50"></asp:button>
						<asp:button id="btnBack" CssClass="button" Runat="server" Text="Back" Width="50"></asp:button>
					</td>
				</tr>
			</table>
			<asp:table id="Table9" Runat="server" Width="100%" GridLines="None" BorderColor="black" Height="400" CssClass="aspTable"
				CellSpacing="0" CellPadding="0" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:DataGrid ID="dgCampaign" CssClass="dg" Width="100%" Runat="server" AutoGenerateColumns="false"
							AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numCampaignId" Visible="False"></asp:BoundColumn>
								<asp:ButtonColumn  DataTextField="CampaignName" CommandName="Campaign" HeaderText="<font color=white>Campaign Name</font>" SortExpression="CampaignName"></asp:ButtonColumn>
								<asp:TemplateColumn SortExpression="intLaunchDate" HeaderText="<font color=white>Launch Date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "intLaunchDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="intEndDate" HeaderText="<font color=white>End Date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "intEndDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CampaignType" SortExpression="CampaignType" HeaderText="<font color=white>Type</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="CampaignStatus" SortExpression="CampaignStatus" HeaderText="<font color=white>Status</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="NoOfDeals" DataFormatString="{0:#,##0}" SortExpression="NoOfDeals" HeaderText="<font color=white>Deals and Accounts Generated</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="monCampaignCost" DataFormatString="{0:#,##0.00}" SortExpression="monCampaignCost"
									HeaderText="<font color=white>Cost</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="income" DataFormatString="{0:#,##0.00}" SortExpression="income" HeaderText="<font color=white>Income</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="ROI" DataFormatString="{0:#,##0.00}" SortExpression="ROI" HeaderText="<font color=white>ROI(Net)</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="DealCost" DataFormatString="{0:#,##0.00}" SortExpression="DealCost" HeaderText="<font color=white>Deal Cost(avg)</font>"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<asp:Button ID="btnGo" Runat="server" style="display:none"></asp:Button>
			</ContentTemplate>
			<Triggers>
			
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			<asp:PostBackTrigger ControlID="btnPrint" />
			</Triggers>
			</asp:updatepanel>
			</form>
	</body>
</HTML>
