<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBestPartnersPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmBestPartnersPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmBestPartnersPrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	    
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="print_head" align="center">
						<asp:Label ID="lblReportHeader" CssClass="print_head" Runat="server">Best Partners Report</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
<asp:DataGrid ID="dgBestPartners" CssClass="dg" Width="100%" CellPadding="0" CellSpacing="0" Runat="server"
				AutoGenerateColumns="false" AllowSorting="True">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Partner" SortExpression="Partner" ItemStyle-HorizontalAlign="Left"
						HeaderText="<font color=white>Partner Name, Division</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Referrals" SortExpression="Referrals" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white># of referrals</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealsWon" DataFormatString="{0:f}" SortExpression="DealsWon" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white># of Deals won from referrals</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealsWon" SortExpression="DealsWon" ItemStyle-HorizontalAlign="Center" HeaderText="<font color=white>Amount generated from referrals (Deals Won)</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			
						<script language="JavaScript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmBestPartners.aspx";
			}
			</script>

		</form>
	</body>
</HTML>
