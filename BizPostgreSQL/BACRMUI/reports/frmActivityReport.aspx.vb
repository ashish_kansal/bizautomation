Imports BACRM.BusinessLogic.Forecasting
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmActivityReport : Inherits BACRMPage

       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = "Reports"
                    
                    GetUserRightsForPage(8, 63)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Dim intCount As Integer
                    For intCount = 2004 To 2050
                        ddlYear.Items.Add(intCount)
                    Next
                    ddlYear.Items.FindByValue(Year(Now)).Selected = True
                    ddlMonth.SelectedIndex = Month(Now)
                    calDay.SelectedDate = ""
                    calMonth.SelectedDate = ""
                    ''Call DisplayEmployees(ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value & "/31/" & ddlYear.SelectedItem.Value, "M", ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value)
                    Call DisplayEmployees(New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1), New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1).AddMonths(1).AddDays(-1), "M", New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1))
                    ''Call sb_DisplayData(ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value & "/31/" & ddlYear.SelectedItem.Value, "M", ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value)
                    Call sb_DisplayData(New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1), New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1).AddMonths(1).AddDays(-1), "M", New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1))
                End If
                LoadDropdowns()
                btnTeams.Attributes.Add("onclick", "return OpenSelTeam()")
                btnBack.Attributes.Add("onclick", "return Back()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                Dim objForeCast As New Forecast
                Dim dtTeamForForecast As DataTable
                objForeCast.UserCntID = Session("UserContactID")
                objForeCast.DomainID = Session("DomainID")
                objForeCast.ForecastType = 5
                dtTeamForForecast = objForeCast.GetTeamsForForRep
                lstTeam.DataSource = dtTeamForForecast
                lstTeam.DataTextField = "vcData"
                lstTeam.DataValueField = "numlistitemid"
                lstTeam.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnMonthGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMonthGo.Click
            Try
                calDay.SelectedDate = ""
                calMonth.SelectedDate = ""
                If Request.Form("cmbYear") <> "0" And Request.Form("cmbMonth") <> "0" Then
                    If ddlMonth.SelectedItem.Value <> "0" And ddlYear.SelectedItem.Value <> "0" Then
                        ''  Call DisplayEmployees(ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value & "/31/" & ddlYear.SelectedItem.Value, "M", ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value)
                        Call DisplayEmployees(New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1), New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1).AddMonths(1).AddDays(-1), "M", New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1))
                        ''Call sb_DisplayData(ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value & "/31/" & ddlYear.SelectedItem.Value, "M", ddlMonth.SelectedItem.Value & "/01/" & ddlYear.SelectedItem.Value)
                        Call sb_DisplayData(New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1), New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1).AddMonths(1).AddDays(-1), "M", New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnDayGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDayGo.Click
            Try
                If calDay.SelectedDate <> Nothing Then
                    Dim strStartDate, strEnddate As Date
                    strStartDate = calDay.SelectedDate
                    strEnddate = DateAdd(DateInterval.Day, 1, strStartDate)
                    Call DisplayEmployees(strStartDate, strEnddate, "D", strStartDate)
                    Call sb_DisplayData(strStartDate, strEnddate, "D", strStartDate)
                End If
                calMonth.SelectedDate = ""
                ddlYear.SelectedIndex = -1
                ddlMonth.SelectedIndex = -1
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnWeekGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWeekGo.Click
            Try
                If calMonth.SelectedDate <> Nothing Then
                    Dim dtDate As Date
                    dtDate = calMonth.SelectedDate
                    Dim dtStDate As Date = dtDate.AddDays(-dtDate.DayOfWeek)
                    Dim dtEndDate As Date = dtDate.AddDays(6 - dtDate.DayOfWeek)
                    Call DisplayEmployees(dtStDate, dtEndDate, "W", dtStDate)
                    Call sb_DisplayData(dtStDate, dtEndDate, "W", dtStDate)
                End If
                ddlYear.SelectedIndex = -1
                ddlMonth.SelectedIndex = -1
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub sb_DisplayData(ByVal dtFrom As Date, ByVal dtTo As Date, ByVal strType As String, ByVal strActDate As String)
            Try
                'lblUserAcc.Text = Session("username")
                Dim stroptType As String
                Dim objPerformance As New Performance
                Dim dtPerformance As DataTable
                objPerformance.UserCntId = Session("UserContactID")
                objPerformance.DomianId = Session("domainid")
                objPerformance.StartDate = dtFrom
                objPerformance.EndDate = dtTo
                objPerformance.PerType = 1
                objPerformance.TeamType = 5
                dtPerformance = objPerformance.GetActivityReptByTeam
                If dtPerformance.Rows.Count = 1 Then
                    'lblOppOpened.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("OppOpened")), 0, dtPerformance.Rows(0).Item("OppOpened"))
                    'lblOppAmt.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("OppOpenAmt")), 0, dtPerformance.Rows(0).Item("OppOpenAmt"))
                    'lblDealCloseWon.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("OppClosed")), 0, dtPerformance.Rows(0).Item("OppClosed"))
                    'lblDealWonAmt.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("OppCloseAmt")), 0, dtPerformance.Rows(0).Item("OppCloseAmt"))
                    'lblDealLost.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("OppLost")), 0, dtPerformance.Rows(0).Item("OppLost"))
                    'lblDealLostAmt.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("OppLostAmt")), 0, dtPerformance.Rows(0).Item("OppLostAmt"))
                    'lblLeadProm.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("LeadProm")), 0, dtPerformance.Rows(0).Item("LeadProm"))
                    'lblTotPros.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("TotProsp")), 0, dtPerformance.Rows(0).Item("TotProsp"))
                    'lblNoPros.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("NoProspProm")), 0, dtPerformance.Rows(0).Item("NoProspProm"))
                    'lblTotAcc.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("TotNoAccount")), 0, dtPerformance.Rows(0).Item("TotNoAccount"))
                    'lblComSch.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("CommSch")), 0, dtPerformance.Rows(0).Item("CommSch"))
                    'lblComComp.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("CommComp")), 0, dtPerformance.Rows(0).Item("CommComp"))
                    'lblComPast.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("CommPast")), 0, dtPerformance.Rows(0).Item("CommPast"))
                    'lblTaskSch.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("TaskSch")), 0, dtPerformance.Rows(0).Item("TaskSch"))
                    'lblTaskComp.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("TaskComp")), 0, dtPerformance.Rows(0).Item("TaskComp"))
                    'lblTaskPast.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("TaskPast")), 0, dtPerformance.Rows(0).Item("TaskPast"))
                    'lblCaseOpen.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("CaseOpen")), 0, dtPerformance.Rows(0).Item("CaseOpen"))
                    'lblCaseComp.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("CaseComp")), 0, dtPerformance.Rows(0).Item("CaseComp"))
                    'lblCasePast.Text = IIf(IsDBNull(dtPerformance.Rows(0).Item("CasePast")), 0, dtPerformance.Rows(0).Item("CasePast"))

                    lblOppOpened.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("OppOpened"))
                    lblOppAmt.Text = String.Format("{0:#,##0.00}", dtPerformance.Rows(0).Item("OppOpenAmt"))
                    lblDealCloseWon.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("OppClosed"))
                    lblDealWonAmt.Text = String.Format("{0:#,##0.00}", dtPerformance.Rows(0).Item("OppCloseAmt"))
                    lblDealLost.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("OppLost"))
                    lblDealLostAmt.Text = String.Format("{0:#,##0.00}", dtPerformance.Rows(0).Item("OppLostAmt"))
                    lblLeadProm.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("LeadProm"))
                    lblTotPros.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TotProsp"))
                    lblNoPros.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("NoProspProm"))
                    lblTotAcc.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TotNoAccount"))
                    lblComSch.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CommSch"))
                    lblComComp.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CommComp"))
                    lblComPast.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CommPast"))
                    lblTaskSch.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TaskSch"))
                    lblTaskComp.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TaskComp"))
                    lblTaskPast.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("TaskPast"))
                    lblCaseOpen.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CaseOpen"))
                    lblCaseComp.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CaseComp"))
                    lblCasePast.Text = String.Format("{0:#,##0}", dtPerformance.Rows(0).Item("CasePast"))

                    If Not (lblOppOpened.Text = 0 Or lblDealCloseWon.Text = 0) Then
                        lblConversion.Text = Format(CLng(lblDealCloseWon.Text) / (CLng(lblDealCloseWon.Text) + CLng(lblDealLost.Text)) * 100, "###0.00") & "%"
                    Else : lblConversion.Text = "0%"
                    End If
                    lnkLeadProm.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=L"
                    hplLeadPromCmp.NavigateUrl = "#"
                    hplLeadPromCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOrgComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=L&UserCntId=" & txtUserId.Text & "')")

                    lnkTotPros.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=P"
                    hplTotProsCmp.NavigateUrl = "#"
                    hplTotProsCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOrgComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=P&UserCntId=" & txtUserId.Text & "')")

                    lnkNoPros.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=R"
                    hplNoProsCmp.NavigateUrl = "#"
                    hplNoProsCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOrgComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=R&UserCntId=" & txtUserId.Text & "')")

                    lnkTotAcc.NavigateUrl = "../reports/ActivityReportProspect.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=A"
                    hplTotAccCmp.NavigateUrl = "#"
                    hplTotAccCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOrgComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=A&UserCntId=" & txtUserId.Text & "')")

                    lnkOppOpened.NavigateUrl = "../reports/ActivityReportOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=O"
                    hplOppOpnCmp.NavigateUrl = "#"
                    hplOppOpnCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOppComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=O&UserCntId=" & txtUserId.Text & "')")

                    lnkDealCloseWon.NavigateUrl = "../reports/ActivityReportOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=W"
                    hplDealCloseWonCmp.NavigateUrl = "#"
                    hplDealCloseWonCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOppComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=W&UserCntId=" & txtUserId.Text & "')")

                    hplDealLostLink.NavigateUrl = "../reports/ActivityReportOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=L"
                    hplDealLostLinkCmp.NavigateUrl = "#"
                    hplDealLostLinkCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmOppComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=L&UserCntId=" & txtUserId.Text & "')")

                    lnkComSch.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=S&Task=1"
                    hplComSchCmp.NavigateUrl = "#"
                    hplComSchCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmActComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=S&Task=1&UserCntId=" & txtUserId.Text & "')")

                    lnkComComp.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=C&Task=1"
                    hplComCompCmp.NavigateUrl = "#"
                    hplComCompCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmActComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=C&Task=1&UserCntId=" & txtUserId.Text & "')")

                    lnkComPast.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=P&Task=1"
                    hplComPastCmp.NavigateUrl = "#"
                    hplComPastCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmActComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=P&Task=1&UserCntId=" & txtUserId.Text & "')")

                    lnkTaskSch.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=S&Task=3"
                    hplTaskSchCmp.NavigateUrl = "#"
                    hplTaskSchCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmActComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=S&Task=3&UserCntId=" & txtUserId.Text & "')")

                    lnkTaskComp.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=C&Task=3"
                    hplTaskCompCmp.NavigateUrl = "#"
                    hplTaskCompCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmActComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=C&Task=3&UserCntId=" & txtUserId.Text & "')")

                    lnkTaskPast.NavigateUrl = "../reports/ActivityReportAction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=P&Task=3"
                    hplTaskPastCmp.NavigateUrl = "#"
                    hplTaskPastCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmActComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=P&Task=3&UserCntId=" & txtUserId.Text & "')")

                    lnkCaseOpen.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=O"
                    hplCaseOpenCmp.NavigateUrl = "#"
                    hplCaseOpenCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmCaseComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=0&UserCntId=" & txtUserId.Text & "')")

                    lnkCaseComp.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=C"
                    hplCaseCompCmp.NavigateUrl = "#"
                    hplCaseCompCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmCaseComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=C&UserCntId=" & txtUserId.Text & "')")

                    lnkCasePast.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=" & 1 & "&RepType=P"
                    hplCasePastCmp.NavigateUrl = "#"
                    hplCasePastCmp.Attributes.Add("onclick", "return OpenindRept('../reports/frmCaseComparison.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & dtFrom & "&enDate=" & dtTo & "&type=3&RepType=P&UserCntId=" & txtUserId.Text & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayEmployees(ByVal dtFrom As Date, ByVal dtTo As Date, ByVal strType As String, ByVal strActDate As String)
            Try
                Dim objcell As TableCell
                Dim objRow As TableRow
                Dim hpl As HyperLink
                Dim i, k As Integer
                Dim objPerformance As New Performance
                Dim dtTeamMembers As DataTable
                objPerformance.UserCntId = Session("UserContactID")
                objPerformance.DomianId = Session("domainid")
                dtTeamMembers = objPerformance.GetMembersOfTeam
                k = 0
                txtUserId.Text = ""
                For i = 0 To dtTeamMembers.Rows.Count - 1
                    If k = 0 Then objRow = New TableRow
                    objcell = New TableCell

                    hpl = New HyperLink
                    hpl.CssClass = "hyperlink"
                    objcell.CssClass = "normal1"
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objcell.ForeColor = System.Drawing.Color.FromName("blue")
                    hpl.Text = dtTeamMembers.Rows(i).Item("vcUserName")
                    hpl.Attributes.Add("onclick", "return OpenindRept('" & "../reports/ActivityReportCompany.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&dtFrom1=" & dtFrom & "&dtTo1=" & dtTo & "&strActDate=" & strActDate & "&UserCntId=" & dtTeamMembers.Rows(i).Item("numContactID") & "&strName=" & dtTeamMembers.Rows(i).Item("vcUserName").trim & "&frType=" & strType & "')")
                    txtUserId.Text = txtUserId.Text & dtTeamMembers.Rows(i).Item("numContactID") & ","
                    objcell.Controls.Add(hpl)
                    objRow.Cells.Add(objcell)
                    k = k + 1
                    If k = 3 Then
                        tblUsers.Rows.Add(objRow)
                        k = 0
                    End If
                    If k <> 0 And i = dtTeamMembers.Rows.Count - 1 Then tblUsers.Rows.Add(objRow)
                Next
                txtUserId.Text = txtUserId.Text.TrimEnd(",")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 42
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
