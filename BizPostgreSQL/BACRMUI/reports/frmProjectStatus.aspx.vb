Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmProjectStatus : Inherits BACRMPage

        Dim SortField As String
       
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim dtProjectStatus As DataTable
        Dim objPredefinedReports As New PredefinedReports

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then
                    
                    GetUserRightsForPage(8, 32)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    DisplayRecords()
                     ' = "Reports"
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(22)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(22)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")

                If chkOnlyCompleted.Checked = True And cal.SelectedDate <> Nothing Then objPredefinedReports.StartDate = cal.SelectedDate
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.TerritoryID = Session("TerritoryID")

                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select
                objPredefinedReports.ClientOffsetTime = Session("ClientMachineUTCTimeOffset")
                objPredefinedReports.ReportType = 22
                dtProjectStatus = objPredefinedReports.GetProjectStatus
                Dim dv As DataView = New DataView(dtProjectStatus)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgProjectStatus.DataSource = dv
                dgProjectStatus.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgProjectStatus_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgProjectStatus.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        '    Dim strPath As String = Server.MapPath("../documents/docs")
        '    DisplayRecords()
        '    objPredefinedReports.ExportToExcel(dtProjectStatus.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "ProjectStatus", Session("DateFormat"))

        '    Response.Write("<script language=javascript>")
        '    Response.Write("alert('Exported Successfully !')")
        '    Response.Write("</script>")
        '    Dim strref As String = "../Documents/docs/" & Session("UserName") & "_" & Session("UserID") & "_ProjectStatus.csv"

        '    Response.Write("<script language=javascript>")
        '    Response.Write("window.open('" & strref & "')")
        '    Response.Write("</script>")
        'End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                If chkOnlyCompleted.Checked = True And cal.SelectedDate <> Nothing Then
                    Server.Transfer("../reports/frmProjectStatusPrint.aspx?fdt=" & cal.SelectedDate & "&ReportType=" & rdlReportType.SelectedIndex)
                Else : Server.Transfer("../reports/frmProjectStatusPrint.aspx?fdt=0" & "&ReportType=" & rdlReportType.SelectedIndex)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 7
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgProjectStatus_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgProjectStatus.ItemCommand
            Try
                If e.CommandName = "Project" Then
                   Response.Redirect("../projects/frmProjects.aspx?frm=ProjectStatus&ProId=" & e.Item.Cells(0).Text & "&SI1=" & SI1 & "&SI2=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgProjectStatus, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace