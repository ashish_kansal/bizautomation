Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports System
Imports System.Web.UI.WebControls
Imports System.Text
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmLeadConversion : Inherits BACRMPage

        Protected ChartImageAssigned As System.Web.UI.WebControls.Image
        Protected ChartImageOwned As System.Web.UI.WebControls.Image
       

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then

                    GetUserRightsForPage(8, 70)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    LoadDropDownList()
                    ' = "Reports"
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(34)")
                ' btnPrint.Attributes.Add("onclick", "print()")
                ''btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(34)")
                DisplayChart()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Public Function DisplayChart()
            Try
                Dim lobjLeadConversion As New BusinessLogic.Reports.LeadConversion
                Dim dtLeadConversion As DataTable
                Dim xValuesAssigned As New StringBuilder
                Dim yValuesAssigned As New StringBuilder
                Dim xValuesOwned As New StringBuilder
                Dim yValuesOwned As New StringBuilder
                Dim TotalLeadAssigned As Decimal
                Dim TotalLeadOwned As Decimal
                Dim TotalLeadAccountAssigned As Decimal
                Dim TotalLeadAccountOwned As Decimal
                Dim TotalLeadAmountAssigned As Decimal
                Dim TotalLeadAmountOwned As Decimal
                Dim TotalLeadAmount As Decimal

                Select Case rdlLeadConversion.SelectedIndex
                    Case 0 : lobjLeadConversion.PerType = 1
                    Case 1 : lobjLeadConversion.PerType = 2
                End Select

                Select Case rdlReportType.SelectedIndex
                    Case 0 : lobjLeadConversion.UserCntId = Session("UserContactID")
                    Case 1 : lobjLeadConversion.UserCntId = IIf(ddlEmployee.SelectedItem.Value.ToString = "0", Session("UserContactID"), CInt(ddlEmployee.SelectedItem.Value))
                End Select

                lobjLeadConversion.LeadGroupId = CInt(ddlLeads.SelectedItem.Value)
                lobjLeadConversion.DomianId = Session("DomainID")
                lobjLeadConversion.StartDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                lobjLeadConversion.EndDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))


                dtLeadConversion = lobjLeadConversion.GetLeadConversionDetails()
                If dtLeadConversion.Rows.Count > 0 Then
                    If lobjLeadConversion.PerType = 1 Then
                        xValuesAssigned.Append("Total Leads Assigned - " & dtLeadConversion.Rows(0)("TotalLeadAssigned"))
                    ElseIf lobjLeadConversion.PerType = 2 Then
                        xValuesAssigned.Append("Total Leads Assigned - " & dtLeadConversion.Rows(0)("TotalLeadAssigned"))
                    End If

                    yValuesAssigned.Append(CDec(100 - TotalLeadAccountAssigned))
                    xValuesAssigned.Append("|")
                    yValuesAssigned.Append("|")

                    If lobjLeadConversion.PerType = 1 Then
                        xValuesAssigned.Append("Total Leads Converted Into Prospects(Of those Leads Assigned) - " & dtLeadConversion.Rows(0)("TotalLeadAccountAssigned"))
                    ElseIf lobjLeadConversion.PerType = 2 Then
                        xValuesAssigned.Append("Total Leads Converted Into Accounts(Of those Leads Assigned) - " & dtLeadConversion.Rows(0)("TotalLeadAccountAssigned"))
                    End If
                    TotalLeadAccountAssigned = CDec((dtLeadConversion.Rows(0)("TotalLeadAccountAssigned") / IIf(dtLeadConversion.Rows(0)("TotalLeadAssigned") = 0, 1, dtLeadConversion.Rows(0)("TotalLeadAssigned"))) * 100)
                    yValuesAssigned.Append(TotalLeadAccountAssigned)

                    If lobjLeadConversion.PerType = 1 Then
                        xValuesOwned.Append("Total Leads Owned - " & dtLeadConversion.Rows(0)("TotalLeadOwned"))
                    ElseIf lobjLeadConversion.PerType = 2 Then
                        xValuesOwned.Append("Total Leads Owned - " & dtLeadConversion.Rows(0)("TotalLeadOwned"))
                    End If

                    yValuesOwned.Append(CDec(100 - TotalLeadAccountOwned))
                    xValuesOwned.Append("|")
                    yValuesOwned.Append("|")

                    If lobjLeadConversion.PerType = 1 Then
                        xValuesOwned.Append("Total Leads Converted Into Prospects(Of those Leads owned) - " & dtLeadConversion.Rows(0)("TotalLeadAccountOwned"))
                    ElseIf lobjLeadConversion.PerType = 2 Then
                        xValuesOwned.Append("Total Leads Converted Into Accounts(Of those Leads owned) - " & dtLeadConversion.Rows(0)("TotalLeadAccountOwned"))
                    End If

                    TotalLeadAccountOwned = CDec((dtLeadConversion.Rows(0)("TotalLeadAccountOwned") / IIf(dtLeadConversion.Rows(0)("TotalLeadOwned") = 0, 1, dtLeadConversion.Rows(0)("TotalLeadOwned"))) * 100)
                    yValuesOwned.Append(TotalLeadAccountOwned)

                    TotalLeadAmountAssigned = Math.Round(dtLeadConversion.Rows(0)("TotalLeadAmountAssigned"), 2)
                    TotalLeadAmountOwned = Math.Round(dtLeadConversion.Rows(0)("TotalLeadAmountOwned"), 2)
                    TotalLeadAmount = Math.Round(dtLeadConversion.Rows(0)("TotalLeadAmount"), 2)
                End If

                ChartImageAssigned.ImageUrl = "../Reports/ChartGenerator.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&" + "xValues=" + xValuesAssigned.ToString() + "&yValues=" + yValuesAssigned.ToString()
                ChartImageOwned.ImageUrl = "../Reports/ChartGenerator.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&" + "xValues=" + xValuesOwned.ToString() + "&yValues=" + yValuesOwned.ToString()
                If lobjLeadConversion.PerType = 1 Then
                    AmountWon1.Text = "Amount in Pipeline  - " & TotalLeadAmountAssigned
                    AmountWon2.Text = "Amount in Pipeline  - " & TotalLeadAmountOwned
                    TotalAmountWon.Text = "Total Amount in Pipeline  - " & TotalLeadAmount
                Else
                    AmountWon1.Text = "Amount Won - " & TotalLeadAmountAssigned
                    AmountWon2.Text = "Amount Won - " & TotalLeadAmountOwned
                    TotalAmountWon.Text = "Total Amount Won - " & TotalLeadAmount
                End If
                ChartImageAssigned.Visible = True
                ChartImageOwned.Visible = True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub LoadDropDownList()
            Try
                Dim objReports As New LeadsActivity
                objReports.UserCntID = Session("UserContactID")
                Select Case rdlReportType.SelectedIndex
                    Case 0 : objReports.UserRights = 1
                    Case 1 : objReports.UserRights = 2
                    Case 2 : objReports.UserRights = 3
                End Select
                objReports.ListType = IIf(rdlReportType.SelectedValue = True, 0, 1)
                objReports.TeamType = 34
                objReports.DomainID = Session("DomainID")
                ddlEmployee.DataSource = objReports.GetTeamMembers
                ddlEmployee.DataTextField = "Name"
                ddlEmployee.DataValueField = "numUserId"
                ddlEmployee.DataBind()
                ddlEmployee.Items.Insert(0, "All")
                ddlEmployee.Items.FindByText("All").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub rdlLeadConversion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlLeadConversion.SelectedIndexChanged
            Try
                DisplayChart()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmployee.SelectedIndexChanged
            Try
                DisplayChart()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                LoadDropDownList()
                DisplayChart()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlLeads_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeads.SelectedIndexChanged
            Try
                DisplayChart()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 48
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                DisplayChart()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace