' Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports
Imports System.Configuration

Namespace BACRM.UserInterface.Reports
    Public Class frmAvgSalesCycle1 : Inherits BACRMPage

        Dim SortField As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Dim strColumn As String
       
        Protected WithEvents tbl As System.Web.UI.WebControls.Table
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then


                    GetUserRightsForPage(8, 26)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExport.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    LoadDropdowns()
                    BindDatagrid()
                End If
                litMessage.Text = ""
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam()")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(31)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub BindDatagrid()
            Try
                'Dim dtLeads As New DataTable
                Dim dtSales As DataTable
                Dim objSales As New AvgSalesCycle

                objSales.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objSales.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))

                Dim SortChar As Char
                If ViewState("SortChar") <> "" Then
                    SortChar = ViewState("SortChar")
                Else : SortChar = "0"
                End If
                With objSales
                    Select Case rdlReportType.SelectedIndex
                        Case 0 : objSales.UserRights = 1
                        Case 1 : objSales.UserRights = 2
                        Case 2 : objSales.UserRights = 3
                    End Select
                    .UserCntID = Session("UserContactID")
                    .TeamMemID = ddlTeamMembers.SelectedValue
                    .ListType = IIf(rdlReportType.SelectedValue = True, 0, 1)
                    .TeamType = 31
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If (txtDeal.Text = "") Then txtDeal.Text = "0"
                    .Sort = CInt(txtDeal.Text)
                    .DomainID = Session("DomainID")
                    If ViewState("Column") <> "" Then
                        .columnName = ViewState("Column")
                    Else : .columnName = "bintcreateddate"
                    End If
                    If Session("Asc") = 1 Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                End With
                dtSales = objSales.GetAvgSalesList
                If dtSales.Rows.Count > 0 Then
                    lbl1.Text = Format(dtSales.Compute("AVG(monPAmount)", String.Empty), "#,##0.00")
                    lbl2.Text = Format(dtSales.Compute("SUM(days)", String.Empty) / dtSales.Rows.Count, "#,##0.00")
                Else
                    lbl1.Text = "0.00"
                    lbl2.Text = "0.00"
                End If
                Dim dv As DataView = New DataView(dtSales)
                dv.Sort = objSales.columnName & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgAvgSales.DataSource = dv
                dgAvgSales.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgAvgSales_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAvgSales.ItemCommand
            Try
                Dim lngDivID As Long
                If Not e.CommandName = "Sort" Then lngDivID = CLng(e.Item.Cells(0).Text())
                If e.CommandName = "Company" Then
                    Response.Redirect("../Leads/frmLeads.aspx?frm=AvgSalesCycle1&DivID=" & lngDivID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                End If

                If e.CommandName = "Customer" Then
                    Dim OpporID, CompanyId, DivisionID, CRMTYPE, TerritryID, ContactID As Long

                    CompanyId = e.Item.Cells(0).Text()
                    TerritryID = e.Item.Cells(1).Text()
                    DivisionID = e.Item.Cells(2).Text()
                    CRMTYPE = e.Item.Cells(3).Text()
                    ContactID = e.Item.Cells(4).Text()

                    If CRMTYPE = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=AvgSalesCycle1&CmpID=" & CompanyId & "&DivID=" & DivisionID & "& CRMTYPE=" & CRMTYPE & "&CntID=" & ContactID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                    ElseIf CRMTYPE = 2 Then
                        Response.Redirect("../account/frmAccounts.aspx?frm=AvgSalesCycle1&CmpID=" & CompanyId & "&klds+7kldf=fjk-las&DivId=" & DivisionID & "& CRMTYPE=" & CRMTYPE & "&CntID=" & ContactID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgAvgSales_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAvgSales.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If ViewState("Column") <> strColumn Then
                    ViewState("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub LoadMembers()
            Try
                Dim objSales As New AvgSalesCycle
                objSales.UserCntID = Session("UserContactID")
                Select Case rdlReportType.SelectedIndex
                    Case 0 : objSales.UserRights = 1
                    Case 1 : objSales.UserRights = 2
                    Case 2 : objSales.UserRights = 3
                End Select

                objSales.ListType = IIf(rdlReportType.SelectedValue = True, 0, 1)
                objSales.TeamType = 31
                objSales.DomainID = Session("DomainID")
                ddlTeamMembers.DataSource = objSales.GetTeamMembers
                ddlTeamMembers.DataTextField = "Name"
                ddlTeamMembers.DataValueField = "numUserId"
                ddlTeamMembers.DataBind()
                ddlTeamMembers.Items.Insert(0, "All Team Members")
                ddlTeamMembers.Items.FindByText("All Team Members").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub LoadDropdowns()
            Try
                LoadMembers()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=FileName" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".xls")
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As New System.IO.StringWriter
                Dim htmlWrite As New HtmlTextWriter(stringWrite)
                ClearControls(dgAvgSales)
                dgAvgSales.GridLines = GridLines.Both
                dgAvgSales.HeaderStyle.Font.Bold = True
                dgAvgSales.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.End()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub ClearControls(ByVal control As Control)
            Try
                Dim i As Integer
                For i = control.Controls.Count - 1 To 0 Step -1
                    ClearControls(control.Controls(i))
                Next i

                If TypeOf control Is System.Web.UI.WebControls.Image Then control.Parent.Controls.Remove(control)
                If (Not TypeOf control Is TableCell) Then
                    If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                        Dim literal As New LiteralControl
                        control.Parent.Controls.Add(literal)
                        Try
                            literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                        Catch
                        End Try
                        control.Parent.Controls.Remove(control)
                    Else
                        If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                            Dim literal As New LiteralControl
                            control.Parent.Controls.Add(literal)
                            literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                            literal.Text = Replace(literal.Text, "white", "black")
                            control.Parent.Controls.Remove(control)
                        End If
                    End If
                End If
                Return
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDeal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeal.Click
            Try
                BindDatagrid()
                LoadDropdowns()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/frmAvgSalesCycle1Print.aspx?&fdt=" & calFrom.SelectedDate & "&todt=" & calTo.SelectedDate & "&SortChar=" & ViewState("SortChar") & "&rdlReportType=" & rdlReportType.SelectedIndex & "&TeamMemID=" & ddlTeamMembers.SelectedValue & "&ListType=" & rdlReportType.SelectedValue & "&Sort=" & txtDeal.Text & "&Column=" & ViewState("Column"), False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 4
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace