<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBestAccounts.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmBestAccounts" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Best Accounts</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenindRept(url) {
            //alert(url);
            window.open(url, '', 'toolbar=no,titlebar=no,left=200, top=300,width=900,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
            <asp:Label ID="lblCriterial" runat="server" CssClass="normal1">Criteria </asp:Label>
            <asp:DropDownList ID="ddlCriteria" runat="server" CssClass="form-control" AutoPostBack="True">
                                <asp:ListItem Value="0" Selected="True">Select Criteria</asp:ListItem>
                                <asp:ListItem Value="1">For Year</asp:ListItem>
                                <asp:ListItem Value="2">For Year & Quarter</asp:ListItem>
                                <asp:ListItem Value="3">For Year & Month</asp:ListItem>
                            </asp:DropDownList>
            <asp:Label ID="lblYear" runat="server" Visible="False" CssClass="normal1">Year </asp:Label>
            <asp:DropDownList ID="ddlYear" Visible="False" runat="server" AutoPostBack="True"
                                CssClass="form-control">
                            </asp:DropDownList>
            <asp:Label ID="lblQuarter" Visible="False" runat="server" CssClass="normal1">Quarter </asp:Label>
            <asp:DropDownList ID="ddlQuarter" Visible="False" runat="server" AutoPostBack="True"
                                CssClass="form-control">
                                <asp:ListItem Value="0" Selected="True">-- Select One --</asp:ListItem>
                                <asp:ListItem Value="1">First Quarter</asp:ListItem>
                                <asp:ListItem Value="2">Second Quarter</asp:ListItem>
                                <asp:ListItem Value="3">Third Quarter</asp:ListItem>
                                <asp:ListItem Value="4">Fourth Quarter</asp:ListItem>
                            </asp:DropDownList>
            <asp:Label ID="lblMonth" runat="server" Visible="False" CssClass="normal1">Month </asp:Label>
                            <asp:DropDownList ID="ddlMonth" Visible="False" runat="server" AutoPostBack="True"
                                CssClass="form-control">
                            </asp:DropDownList>
                </div>
            
                </div>
                <div class="pull-right">
                <asp:RadioButtonList ID="rdlReportType" runat="server" AutoPostBack="True" CssClass="normal1"
                    RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
    <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                    <asp:LinkButton ID="btnChooseTeams" runat="server" CssClass="btn btn-primary">Choose Teams</asp:LinkButton>
                             <asp:LinkButton ID="btnChooseTerritories" runat="server" CssClass="btn btn-primary">Choose Territories</asp:LinkButton>
                    <asp:LinkButton ID="btnExportToExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
            </div>
        </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Best Accounts
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:DataGrid ID="dgBestAccounts" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" runat="server" BorderColor="white"
        AutoGenerateColumns="False" AllowSorting="True">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="ContactID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="ClosedDealsAmount" DataFormatString="{0:#,##0.00}" SortExpression="ClosedDealsAmount"
                HeaderText="Closed Deals(Amount)"></asp:BoundColumn>
            <asp:TemplateColumn SortExpression="DealsClosed" HeaderText="Deal Closed">
                <ItemTemplate>
                    <asp:HyperLink ID="hplDealsClosed" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DealsClosed") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--<asp:buttoncolumn CommandName="DealClosed" DataTextField="DealsClosed" DataTextFormatString="{0:#,##0}" SortExpression="DealsClosed" HeaderText="Deals Closed"></asp:buttoncolumn>--%>
            <asp:BoundColumn DataField="OpportunitiesInPipelineAmount" DataFormatString="{0:#,##0.00}"
                SortExpression="OpportunitiesInPipelineAmount" ItemStyle-HorizontalAlign="Center"
                HeaderText="Opportunities in Pipeline (Amount)"></asp:BoundColumn>
            <asp:TemplateColumn SortExpression="OpportunitiesInPipeline" HeaderText="Opportunities in Pipeline">
                <ItemTemplate>
                    <asp:HyperLink ID="hplOpportunitiesInPipeline" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OpportunitiesInPipeline") %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--<asp:BoundColumn DataField="OpportunitiesInPipeline" DataFormatString="{0:#,##0}" SortExpression="OpportunitiesInPipeline" HeaderText="Opportunities in Pipeline"></asp:BoundColumn>--%>
            <asp:ButtonColumn CommandName="Company" DataTextField="CustomerDivision" SortExpression="CustomerDivision"
                HeaderText="Customer-Division"></asp:ButtonColumn>
            <asp:BoundColumn DataField="Industry" SortExpression="Industry" HeaderText="Industry">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="AccountOwner" SortExpression="AccountOwner" HeaderText="Account Owner">
            </asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:Button ID="btnGo" runat="server" Style="display: none"></asp:Button>
        </div>
</asp:Content>
