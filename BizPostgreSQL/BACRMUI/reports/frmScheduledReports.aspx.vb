﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports

Public Class frmScheduledReports
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                BindScheduleReportGroups()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindScheduleReportGroups()
        Try
            Dim objSRG As New ScheduledReportsGroup
            objSRG.DomainID = CCommon.ToLong(Session("DomainID"))
            objSRG.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            rptScheduledReports.DataSource = objSRG.GetAll(0)
            rptScheduledReports.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub rptScheduledReports_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If Not e.Item.FindControl("rptReports") Is Nothing Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                    Dim rptReports As Repeater = DirectCast(e.Item.FindControl("rptReports"), Repeater)

                    Dim objSRGR As New ScheduledReportsGroupReports
                    objSRGR.DomainID = CCommon.ToLong(Session("DomainID"))
                    rptReports.DataSource = objSRGR.GetSRGReports(2, CCommon.ToLong(drv("ID")), 0, 0, "", 0, 0)
                    rptReports.DataBind()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region


End Class