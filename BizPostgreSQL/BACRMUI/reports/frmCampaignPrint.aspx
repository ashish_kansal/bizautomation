<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCampaignPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCampaignPrint"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmCampaignPrint</title>
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body>
		<form id="frmCampaignReportPrint" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Campaign Report</asp:Label>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:DataGrid ID="dgCampaign" CssClass="dg" Width="100%" Runat="server" AutoGenerateColumns="false"
				AllowSorting="True">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
				<asp:BoundColumn DataField="CampaignName" SortExpression="CampaignName" HeaderText="<font color=white>Campaign Name</font>"></asp:BoundColumn>
					<asp:TemplateColumn SortExpression="intLaunchDate" HeaderText="<font color=white>Launch Date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "intLaunchDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="intEndDate" HeaderText="<font color=white>End Date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "intEndDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
					
					<asp:BoundColumn DataField="CampaignType" SortExpression="CampaignType" HeaderText="<font color=white>Type</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="CampaignStatus" SortExpression="CampaignStatus" HeaderText="<font color=white>Status</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="NoOfDeals" SortExpression="NoOfDeals" HeaderText="<font color=white>Deals and Accounts Generated</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="monCampaignCost" DataFormatString="{0:#,##0.00}" SortExpression="monCampaignCost"
									HeaderText="<font color=white>Cost</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Income" SortExpression="Income" HeaderText="<font color=white>Income(Tot)</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="ROI" SortExpression="ROI" HeaderText="<font color=white>ROI(Net)</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealCost" SortExpression="DealCost" HeaderText="<font color=white>Deal Cost(avg)</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			
			<script language="JavaScript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmCampaign.aspx";
			}
			</script>
		</form>
	</body>
</HTML>
