﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmProjectRevenueUtilizationReport.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmProjectRevenueUtilizationReport" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Project Revenue & Utilization Report </title>
    <script language="javascript">
        function OpenPro(a, b) {
            var str
            str = "../projects/frmProjects.aspx?frm=ProjectList&SelectedIndex=3&ProId=" + a;

            if (b == 1) {
                window.open(str);
            }
            else {
                document.location.href = str;
            }
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
     <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>
                        From
                    </label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                    <label>
                        To
                    </label>
                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    <asp:Button ID="btnSearch" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
                </div>
            </div>
           <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                           
                    <asp:LinkButton ID="btnExportToExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                   
                        </ContentTemplate>
                      <Triggers>
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                      
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>

        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Project Revenue & Utilization
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="table-responsive">
    <asp:GridView ID="gvProjectReport" AutoGenerateColumns="False" runat="server" Width="100%"
        CssClass="table table-responsive table-bordered tblDataGrid" DataKeyNames="">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <Columns>
            <asp:TemplateField HeaderText="Project ID" SortExpression="vcProjectID">
                <ItemTemplate>
                    <a href="javascript:OpenPro('<%#Eval("numProid")%>','0')">
                        <%# Eval("vcProjectID")%>
                    </a><a href="javascript:OpenPro('<%#Eval("numProid")%>','1')">
                        <img src='../images/open_new_window_notify.gif' width='15px' /></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Project Name" SortExpression="vcProjectName">
                <ItemTemplate>
                    <a href="javascript:OpenPro('<%#Eval("numProid")%>','0')">
                        <%# Eval("vcProjectName")%>
                    </a><a href="javascript:OpenPro('<%#Eval("numProid")%>','1')">
                        <img src='../images/open_new_window_notify.gif' width='15px' /></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="vcRecordOwner" HeaderText="Record Owner"></asp:BoundField>
            <asp:BoundField DataField="vcCompanyName" HeaderText="Client"></asp:BoundField>
            <asp:BoundField DataField="vcInternalProjectManager" HeaderText="Internal Project Manager">
            </asp:BoundField>
            <asp:BoundField DataField="vcCustomerProjectManager" HeaderText="Customer Project Manager">
            </asp:BoundField>
            <asp:BoundField DataField="vcReviewer" HeaderText="Reviewer"></asp:BoundField>
            <asp:BoundField DataField="monIncome" HeaderText="Income" DataFormatString="{0:##,#00.00}"
                HtmlEncode="false"></asp:BoundField>
            <asp:BoundField DataField="monExpense" HeaderText="Expense" DataFormatString="{0:##,#00.00}"
                HtmlEncode="false"></asp:BoundField>
            <asp:BoundField DataField="monBalance" HeaderText="Balance" DataFormatString="{0:##,#00.00}"
                HtmlEncode="false"></asp:BoundField>
            <asp:BoundField DataField="numSO" HeaderText="SO"></asp:BoundField>
            <asp:BoundField DataField="numPO" HeaderText="PO"></asp:BoundField>
            <asp:BoundField DataField="numBills" HeaderText="Bills"></asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
    </asp:GridView>
        </div>
</asp:Content>
