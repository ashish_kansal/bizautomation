<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="ActivityReportProspect.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits="BACRM.UserInterface.Reports.ActivityReportProspect" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Activity Report</title>
    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        //        function fnSortByChar(varSortChar) {
        //            document.form1.txtSortChar.value = varSortChar;
        //            document.form1.submit();
        //        }
        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblRecordCount" runat="server" Visible="false"></asp:Label>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Company List
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dtgAccount" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
       AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="False" DataField="numContactID" HeaderText="numContactID">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCompanyID" HeaderText="numCompanyID">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numDivisionID" HeaderText="numDivisionID">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCreatedBy" HeaderText="numCreatedBy">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numRecOwner" HeaderText="numRecOwner">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numTerID" HeaderText="numTerID"></asp:BoundColumn>
            <asp:BoundColumn DataField="CompanyName" SortExpression="CompanyName" HeaderText="Account Name">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="PrimaryContact" SortExpression="PrimaryContact" HeaderText="Primary Contact">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Phone - Ext">
            </asp:BoundColumn>
            <asp:HyperLinkColumn Target="_blank" SortExpression="vcEmail" DataNavigateUrlField="vcEmail"
                DataTextField="vcEmail" HeaderText="Email Address">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:HyperLinkColumn>
            <asp:BoundColumn DataField="vcRating" SortExpression="vcRating" HeaderText="Rating">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vcStatus" SortExpression="vcStatus" HeaderText="Status">
            </asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
