<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false"  CodeBehind="frmGrossProfitRept.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmGrossProfitRept" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Untitled Page</title>
    <script>
            function OpenAddAmt()
		{
		  window.open('../TimeAndExpense/frmAddAmount.aspx','','toolbar=no,titlebar=no,left=300,top=400,width=500,height=400,scrollbars=yes,resizable=yes');
		  return false;  
		}
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <menu1:menu id="webmenu1" runat="server"></menu1:menu>
	<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>
			<table width="100%">
				<tr>
					<td class="normal1" >
					     Year  
					    <asp:DropDownList ID="ddlYear" CssClass="signup" runat="server" Width="100" ></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
					Choose Period From
					    <asp:DropDownList ID="ddlFrom" CssClass="signup" runat="server" Width="100" >
					        <asp:ListItem>Jan</asp:ListItem>
					        <asp:ListItem>Feb</asp:ListItem>
					        <asp:ListItem>Mar</asp:ListItem>
					        <asp:ListItem>Apr</asp:ListItem>
					        <asp:ListItem>May</asp:ListItem>
					        <asp:ListItem>Jun</asp:ListItem>
					        <asp:ListItem>Jul</asp:ListItem>
					        <asp:ListItem>Aug</asp:ListItem>
					        <asp:ListItem>Sep</asp:ListItem>
					        <asp:ListItem>Oct</asp:ListItem>
					        <asp:ListItem>Nov</asp:ListItem>
					        <asp:ListItem>Dec</asp:ListItem>
					    </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
					To
					    <asp:DropDownList ID="ddlTo" CssClass="signup" runat="server" Width="100" >
					        <asp:ListItem>Jan</asp:ListItem>
					        <asp:ListItem>Feb</asp:ListItem>
					        <asp:ListItem>Mar</asp:ListItem>
					        <asp:ListItem>Apr</asp:ListItem>
					        <asp:ListItem>May</asp:ListItem>
					        <asp:ListItem>Jun</asp:ListItem>
					        <asp:ListItem>Jul</asp:ListItem>
					        <asp:ListItem>Aug</asp:ListItem>
					        <asp:ListItem>Sep</asp:ListItem>
					        <asp:ListItem>Oct</asp:ListItem>
					        <asp:ListItem>Nov</asp:ListItem>
					        <asp:ListItem>Dec</asp:ListItem>
					    </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
					  
					    <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button"/>
					</td>
					<td>
					</td>
				</tr>
				<tr>
				<td class="normal1">
				<asp:RadioButton ID="chkAmountPaid" AutoPostBack="true"  Checked="true"  runat="server" GroupName="rad" Text="Calculate Income Based on Amount Paid"  />
				<asp:RadioButton ID="chkTotalAmt" runat="server" AutoPostBack="true" GroupName="rad" Text="Calculate Income Based on Total Amount" />
				</td>
					<td align="right"><asp:button id="btnAddtoMyRtpList" CssClass="button" Text="Add To My Reports" Runat="server"></asp:button></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td vAlign="bottom">
							<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; Gross Profit/Loss Report 
									&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnBack" Width="50" CssClass="button" Text="Back" Runat="server"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="Table9" Width="100%" Height="400" Runat="server" GridLines="None" BorderColor="black" CssClass="aspTable"
				CellSpacing="0" CellPadding="0" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
					<br />
					<table>
					<tr>
					    <td class="text_bold" align="right" >
					    Income :
					    </td>
					    <td class="text_bold">
					    <asp:Label ID="lblIncome" runat="server" ></asp:Label>
					    </td>
					</tr>
					<tr>
					    <td class="normal1" align="right">
					    Sales Made :
					    </td>
					    <td class="normal1" >
					    <asp:Label ID="lblSalesMade" runat="server" ></asp:Label>
					    </td>
					</tr>
					<tr>
					    <td class="text_bold" align="right">
					    Expense :
					    </td>
					    <td class="text_bold">
					    <asp:Label ID="lblExpense" runat="server" ></asp:Label>
					    </td>
					</tr>
					<tr>
					    <td class="normal1" align="right">
					    Purchase Made :
					    </td>
					    <td class="normal1">
					    <asp:Label ID="lblPurchase" runat="server" ></asp:Label>
					    </td>
					</tr>
					<tr>
					    <td class="normal1" align="right">
					    Employee Labor Costs :
					    </td>
					    <td class="normal1">
					    <asp:Label ID="lblLabor" runat="server" ></asp:Label>
					    </td>
					</tr>
					<tr>
					    <td class="normal1" align="right">
					     Gross Profit / Loss:
					    </td>
					    <td class="normal1">
					    <asp:Label ID="lblGross" EnableViewState="false"  runat="server" ></asp:Label>
					    </td>
					</tr>
					</table>
					<br />
					<table width="100%">
					    <tr>
					        <td>
					        
					        
					           <ie:tabstrip id="tsVert" runat="server" BorderWidth="0px" targetID="mpages" TabDefaultStyle="FILTER: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E9ECF7, endColorstr=#628BAE, MakeShadow=true);font-family:arial;font-weight:bold;font-size:8pt;color:black;height:21;text-align:center;BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid;"
							TabHoverStyle="FILTER: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#93A0BF, endColorstr=#546083, MakeShadow=true);color:#ffffff;"
							TabSelectedStyle="FILTER: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#93A0BF, endColorstr=#546083, MakeShadow=true);font-family:arial;font-weight:bold;font-size:8pt;color:#ffffff;height:21;text-align:center; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid;">
							<ie:Tab Text="&nbsp;&nbsp;Sales Made&nbsp;&nbsp;"></ie:Tab>
							<ie:Tab Text="&nbsp;&nbsp;Purchase Made&nbsp;&nbsp;"></ie:Tab>
							<ie:Tab Text="&nbsp;&nbsp;Employee Labor Costs&nbsp;&nbsp;"></ie:Tab>
						</ie:tabstrip>
				
					
						<IE:MULTIPAGE id="mpages" runat="server" BorderWidth="0px" BorderStyle="None" Width="100%">
							<IE:PageView>
							<asp:table id="Table7" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%"
																	BorderColor="black" GridLines="None" Height="250">
																	<asp:TableRow>
																		<asp:TableCell VerticalAlign="Top">
																			<asp:datagrid id="dgSales" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																				BorderColor="white">
																				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																				<ItemStyle CssClass="is"></ItemStyle>
																				<HeaderStyle CssClass="hs"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Sales ID" DataField="vcPOppname"></asp:BoundColumn>
																					<asp:TemplateColumn  HeaderText="Deal Close Date">
															                            <ItemTemplate>
																                            <%#Formatdate(DataBinder.Eval(Container.DataItem, "bintAccountClosingDate"))%>
															                            </ItemTemplate>
														                            </asp:TemplateColumn>
																					<asp:BoundColumn HeaderText="Total Amount" DataFormatString="{0:#,###.00}" DataField="totAmount"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Amount Paid" DataFormatString="{0:#,###.00}" DataField="AmtPaid"></asp:BoundColumn>
																				</Columns>
																			</asp:datagrid>
																		</asp:TableCell>
																	</asp:TableRow>
																</asp:table>
							
							</IE:PageView>
							<IE:PageView>
							<asp:table id="Table1" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%"
																	BorderColor="black" GridLines="None" Height="250">
																	<asp:TableRow>
																		<asp:TableCell VerticalAlign="Top">
																			<asp:datagrid id="dgPurchase" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																				BorderColor="white">
																				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																				<ItemStyle CssClass="is"></ItemStyle>
																				<HeaderStyle CssClass="hs"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Purchase ID" DataField="vcPOppname"></asp:BoundColumn>
																					<asp:TemplateColumn  HeaderText="Deal Close Date">
															                            <ItemTemplate>
																                            <%#Formatdate(DataBinder.Eval(Container.DataItem, "bintAccountClosingDate"))%>
															                            </ItemTemplate>
														                            </asp:TemplateColumn>
																					<asp:BoundColumn HeaderText="Total Amount" DataFormatString="{0:#,###.00}" DataField="totAmount"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Amount Paid" DataFormatString="{0:#,###.00}" DataField="AmtPaid"></asp:BoundColumn>
																				</Columns>
																			</asp:datagrid>
																		</asp:TableCell>
																	</asp:TableRow>
																</asp:table>
							
							</IE:PageView>
							<IE:PageView>
							<asp:table id="Table2" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%"
																	BorderColor="black" GridLines="None" Height="250">
																	<asp:TableRow>
																	
																		<asp:TableCell VerticalAlign="Top">
																		<table width="100%">
																	    <tr>
    																	    <td class="normal1" align="right" >
    																	     Filter By Department
    																	    </td>
    																	    <td >
    																	        <asp:DropDownList CssClass="signup" ID="ddldepartment" runat="server" AutoPostBack="true"  ></asp:DropDownList>
    																	    </td>
    																	    <td>
    																	        <asp:Button ID="btnAddAmount" CssClass="button"  runat="server" Text="Add Additional Amount"/>
    																	    </td>
    																	
																	    </tr>
																	    <tr>
    																	    <td colspan="3">
    																	    <asp:datagrid id="dgEmployee" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																				BorderColor="white">
																				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																				<ItemStyle CssClass="is"></ItemStyle>
																				<HeaderStyle CssClass="hs"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="numUserID" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn DataField="monHourlyRate" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Employee Name,Title" DataField="Name"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="NB Hours" DataField="NonBillTime"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="NB Amount" DataFormatString="{0:#,##0.00}" DataField="NonBillTimeAmt"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="B Hours" DataField="BillTime"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="B Amount" DataFormatString="{0:#,##0.00}" DataField="BillTimeAmt"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Overtime Hrs" DataField="OvertimeHrs"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Overtime Amt" DataField="OvertimeAmt" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="NB Expenses" DataField="NonBillExp" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="B Expenses" DataField="BillExp" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Commission Amount" DataField="CommAmt" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Salary Based" DataField="Salary"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Hourly Based" DataField="Hourly" ></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Additional Amount">
																					    <ItemTemplate>
																					        <asp:Label ID="lblAdddAmount" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AddAmount","{0:#,##0.00}") %>'></asp:Label>
																					        <asp:Label ID="lblnumUserID" style="display:none" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "numUserID") %>'></asp:Label>
																					    </ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn HeaderText="Total Amount"  DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
																				 
																				</Columns>
																			</asp:datagrid>
    																	    </td>
																	    </tr>
																	    <tr>
    																	    <td>
    																	    
    																	    </td>
																	    </tr>
																	</table>
																		    
																		<br />
																			
																		</asp:TableCell>
																	</asp:TableRow>
																</asp:table>
							
							</IE:PageView>
							</IE:MULTIPAGE>
					        </td>
					    </tr>
					
					</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			</asp:updatepanel>

    </form>
</body>
</html>
