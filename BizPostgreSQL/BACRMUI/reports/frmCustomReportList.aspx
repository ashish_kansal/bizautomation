﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmCustomReportList.aspx.vb" Inherits=".frmCustomReportList" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Reports & Dashboards</title>
    <script type="text/javascript">
        $(document).ajaxStart(function () {
            $("[id$=UpdateProgress]").show();
        }).ajaxStop(function () {
            $("[id$=UpdateProgress]").hide();
        });

        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href");
                if (target === "#tabReportList") {
                    $("[id$=hdnSelectedTab]").val(1);
                } else {
                    $("[id$=hdnSelectedTab]").val(2);
                }
            });
        });

        function NewReport() {
            window.location.href = "../reports/frmReportStep1.aspx";
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected Report?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function DeleteTemplate() {
            if (confirm('Are you sure, you want to delete the selected dashboard template?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function DuplicateRecord() {
            if (confirm('Are you sure, you want to duplicate the selected Report?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenDBConf() {
            var DashBoardRptConf = window.open('../ReportDashboard/frmDashboardAllowedReports.aspx', 'DashBoardRptConf', 'toolbar=no,titlebar=no,top=100,left=300,scrollbars=yes,resizable=yes');
            DashBoardRptConf.focus()
            return false;
        }

        function OpenKPIGroups() {
            window.location.href = "../reports/frmKPIReportList.aspx";
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Reports.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenScheduledReports(reportID) {
            try {
                $("#btnAddToSRG").removeAttr("onclick");
                $("#ddlSRG option").not(":first").remove();

                $.when(LoadAvailableSRG()).then(function () {
                    $("#modalSRG").modal("show");
                    $("#btnAddToSRG").attr("onclick", "AddToScheduledReports(" + reportID + ")");
                });
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function LoadAvailableSRG() {
            try {
                return $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetValidSRG',
                    contentType: "application/json",
                    dataType: "json",
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.GetValidSRGResult);
                            if (obj != null) {
                                obj.forEach(function (e) {
                                    $("#ddlSRG").append("<option value='" + (e.ID || 0) + "'>" + (e.vcName || "") + "</option>");
                                });
                            }
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }


        function AddToScheduledReports(reportID) {
            try {
                if (parseInt($("#ddlSRG").val()) > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/AddToSRGReports',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "srgID": parseInt($("#ddlSRG").val())
                            , "reportID": reportID
                        }),
                        success: function (data) {
                            $("#btnAddToSRG").removeAttr("onclick");
                            $("#modalSRG").modal("hide");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert($.parseJSON(jqXHR.responseText));
                        }
                    });
                } else {
                    alert("Select scheduled reports group");
                }
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 10000;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <asp:HiddenField ID="hdnSelectedTab" runat="server" Value="1" />
        <div class="col-md-12 form-inline form-group">
            <div class="pull-right">
                <asp:LinkButton ID="btnNewReport" runat="server"
                    CssClass="btn btn-primary" UseSubmitBehavior="true" OnClientClick="return NewReport()"><i class="fa fa-plus-circle"></i>&nbsp;New Custom Report</asp:LinkButton>
                &nbsp;
                   <asp:LinkButton runat="server" ID="btnAllowList" OnClientClick="return OpenDBConf()"
                       CssClass="btn btn-primary">Allowed Reports</asp:LinkButton>
                &nbsp;
                   <asp:LinkButton runat="server" ID="btnKPIGroups" OnClientClick="return OpenKPIGroups()"
                       CssClass="btn btn-primary">KPI & Scorecard groups</asp:LinkButton>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="false"></asp:Literal>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px;background-color: #fff;">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Reports&nbsp;<a href="#" onclick="return OpenHelpPopUp('reports/frmcustomreportlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tabReportList" data-toggle="tab">Report List</a></li>
                        <li><a href="#tabDashboardTemplates" id="dashboadTemplate" data-toggle="tab">Dashboard Templates</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tabReportList">
                            <div class="table-responsive">
                                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered" DataKeyNames="numReportID" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Eval("vcReportName")%>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <b>Name</b>
                                                <br />
                                                <asp:TextBox ID="txtReportName" runat="server" OnTextChanged="txtReportName_TextChanged" />
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Eval("vcReportType")%>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <b>Type</b>
                                                <br />
                                                <asp:DropDownList ID="ddlReportType" runat="server" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="-1" Text="-- Select One --"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="Tabular"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Summary"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Matrix"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="KPI"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="ScoreCard"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="" HeaderText="Source"></asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Eval("vcModuleName")%>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <b>Module(s) report is based on</b>
                                                <br />
                                                <asp:DropDownList ID="ddlModule" runat="server" OnSelectedIndexChanged="ddlModule_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Eval("vcPerspective")%>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <b>Perspective</b>
                                                <br />
                                                <asp:DropDownList ID="ddlPerspective" runat="server" OnSelectedIndexChanged="ddlPerspective_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="-1" Text="-- Select One --"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="All Records"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="User logged in (Assigned-To/Record Owner)"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="User logged in (Assigned-To)"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="User logged in (Record Owner)"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="User logged in (Team Scope)"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="User logged in (Territory Scope)"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Eval("vcTimeline")%>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <b>Timeline</b>
                                                <br />
                                                <asp:DropDownList ID="ddlTimeline" runat="server" OnSelectedIndexChanged="ddlTimeline_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="-1" Text="-- Select One --"></asp:ListItem>
                                                    <asp:ListItem Value='AllTime' Text='All Time'></asp:ListItem>
                                                    <asp:ListItem Value='Custom' Text='Custom'></asp:ListItem>
                                                    <asp:ListItem Value='CurYear' Text='Current CY'></asp:ListItem>
                                                    <asp:ListItem Value='PreYear' Text='Previous CY'></asp:ListItem>
                                                    <asp:ListItem Value='Pre2Year' Text='Previous 2 CY'></asp:ListItem>
                                                    <asp:ListItem Value='Ago2Year' Text='2 CY Ago'></asp:ListItem>
                                                    <asp:ListItem Value='NextYear' Text='Next CY'></asp:ListItem>
                                                    <asp:ListItem Value='CurPreYear' Text='Current and Previous CY'></asp:ListItem>
                                                    <asp:ListItem Value='CurPre2Year' Text='Current and Previous 2 CY'></asp:ListItem>
                                                    <asp:ListItem Value='CurNextYear' Text='Current and Next CY'></asp:ListItem>
                                                    <asp:ListItem Value='CuQur' Text='Current CQ'></asp:ListItem>
                                                    <asp:ListItem Value='CurNextQur' Text='Current and Next CQ'></asp:ListItem>
                                                    <asp:ListItem Value='CurPreQur' Text='Current and Previous CQ'></asp:ListItem>
                                                    <asp:ListItem Value='NextQur' Text='Next CQ'></asp:ListItem>
                                                    <asp:ListItem Value='PreQur' Text='Previous CQ'></asp:ListItem>
                                                    <asp:ListItem Value='LastMonth' Text='Last Month'></asp:ListItem>
                                                    <asp:ListItem Value='ThisMonth' Text='This Month'></asp:ListItem>
                                                    <asp:ListItem Value='NextMonth' Text='Next Month'></asp:ListItem>
                                                    <asp:ListItem Value='CurPreMonth' Text='Current and Previous Month'></asp:ListItem>
                                                    <asp:ListItem Value='CurNextMonth' Text='Current and Next Month'></asp:ListItem>
                                                    <asp:ListItem Value='LastWeek' Text='Last Week'></asp:ListItem>
                                                    <asp:ListItem Value='ThisWeek' Text='This Week'></asp:ListItem>
                                                    <asp:ListItem Value='NextWeek' Text='Next Week'></asp:ListItem>
                                                    <asp:ListItem Value='Yesterday' Text='Yesterday'></asp:ListItem>
                                                    <asp:ListItem Value='Today' Text='Today'></asp:ListItem>
                                                    <asp:ListItem Value='Tomorrow' Text='Tomorrow'></asp:ListItem>
                                                    <asp:ListItem Value='Last7Day' Text='Last 7 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Last30Day' Text='Last 30 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Last60Day' Text='Last 60 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Last90Day' Text='Last 90 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Last120Day' Text='Last 120 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Next7Day' Text='Next 7 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Next30Day' Text='Next 30 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Next60Day' Text='Next 60 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Next90Day' Text='Next 90 Days'></asp:ListItem>
                                                    <asp:ListItem Value='Next120Day' Text='Next 120 Days'></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="vcLocationSource" HeaderText="Location Source"></asp:BoundField>
                                        <asp:TemplateField ItemStyle-Width="175" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hplRun" runat="server" Text="Run" CssClass="btn btn-success btn-xs" ToolTip="Run Report"><i class="fa fa-play" aria-hidden="true"></i></asp:HyperLink>
                                                &nbsp;&nbsp;
                                                <asp:HyperLink ID="hplEdit" runat="server" Text="Edit" CssClass="btn btn-info btn-xs" ToolTip="Edit Report"><i class="fa fa-pencil" aria-hidden="true"></i></asp:HyperLink>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkMyReport" runat="server" Text='My Report' ToolTip="Add to My Report List" CssClass="btn btn-warning btn-xs" CommandName="AddToMyReport"><i class="fa fa-file-text" aria-hidden="true"></i></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkDuplicate" runat="server" Text='Duplicate' CssClass="btn bg-purple btn-xs" ToolTip="Create Duplicate of Report" CommandName="DuplicateReport" OnClientClick="javascript:return DuplicateRecord();"><i class="fa fa-copy" aria-hidden="true"></i></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="btn btn-danger btn-xs" ToolTip="Delete Report" OnClientClick="javascript:return DeleteRecord();" CommandName="DeleteReport"><i class="fa fa-trash-o" aria-hidden="true"></i></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <a class="btn bg-teal btn-xs" title="Add to scheduled reports" href='<%# "javascript:OpenScheduledReports(" & Eval("numReportID") & ");"%>'><i class="fa fa-plus"></i></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabDashboardTemplates">
                            <div class="table-responsive">
                                <asp:GridView ID="gvDashboardTemplate" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped" DataKeyNames="numTemplateID">
                                    <Columns>
                                        <asp:BoundField HeaderText="Name" DataField="vcTemplateName" />
                                        <asp:TemplateField ItemStyle-Width="25" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="btn btn-danger btn-xs" ToolTip="Delete Template" OnClientClick="javascript:return DeleteTemplate();" CommandName="DeleteTemplate"><i class="fa fa-trash-o" aria-hidden="true"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Data Available
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="modalSRG" role="dialog">
        <div class="modal-dialog modal-md" style="width:400px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Select Scheduled Reports Group</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Scheduled Reports Group</label>
                        <select id="ddlSRG" class="form-control">
                            <option value="0">-- Select One --</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 5px;">
                    <button type="button" id="btnAddToSRG" class="btn btn-primary">Add to Report Group</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" CssClass="button" runat="server" Style="display: none" Text="Go"></asp:Button>
</asp:Content>
