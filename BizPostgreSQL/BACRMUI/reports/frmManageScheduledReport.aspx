﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmManageScheduledReport.aspx.vb" Inherits=".frmManageScheduledReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../JavaScript/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.tokeninput.js"></script>
    <link href="../Styles/token-input.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/token-input-facebook.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        var recordsPerPage = 10;
        var totalRecords = 0;
        var timer;
        var saved_tokens = [];
        var hdnTo = [];
        var token_count = 0;

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });


        $(document).ready(function () {
            $("#txtEmailAddress").tokenInput("../contact/EmailList.ashx", {
                txtInput: "to",
                theme: "facebook",
                tokenDelete: "token-input-delete-token-facebook",
                prePopulate: "",
                tokenLimit: 5,
                propertyToSearch: "first_name",
                resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.first_name + " " + item.last_name_actual + "</div><div class='email'>" + item.email + "</div></div></li>" },
                tokenFormatter: function (item) { return "<li><p title='" + item.email + "'>" + item.first_name + " " + item.last_name_actual + " (" + item.company_type + ")</p></li>" }
            });

            if (parseInt($("[id$=hdnSRGID]").val() || 0) > 0) {
                $("#btnSaveClose").removeAttr("disabled");
                $("#btnSaveClose").removeAttr("title");
                $("#btnSendTestReport").removeAttr("disabled");
                $("#btnSendTestReport").removeAttr("title");
                $("#btnDelete").removeAttr("disabled");
                $("#btnDelete").removeAttr("title");
                $("#btnSelectReports").removeAttr("disabled");
                $("#btnSelectReports").removeAttr("title");
                $("#btnRemoveReports").removeAttr("disabled");
                $("#btnRemoveReports").removeAttr("title");
                LoadScheduledReportsGroup();
                LoadScheduledReportsGroupReports();
            }

            $("input[name='reporttype']").change(function () {
                if (parseInt($("input[name='reporttype']:checked").val()) == 6 && parseInt($("[id$=ddlDashboardTemplates]").val()) == 0) {
                    alert("Select dashboard");
                    $("[id$=ddlDashboardTemplates]").focus();
                    $("#tblSelectReports > tbody > tr").not(":first").remove();
                    $("#tblSelectReports > tbody").append("<tr><td colspan='6'>No Data</td></tr>");
                } else {
                    LoadRecordsWithPagination(1);
                }
            });

            $("[id$=ddlDashboardTemplates]").change(function () {
                LoadRecordsWithPagination();
            });

            $('#txtSearchReport').change(function () {
                LoadRecordsWithPagination();
            });

            var selectedReports = [];
            if ($("[id$=hdnSelectedReports]").val() != "") {
                selectedReports = $.parseJSON($("[id$=hdnSelectedReports]").val());
            }

            $("#spnReportCount").text(selectedReports.length);
        });

        function LoadScheduledReportsGroup() {
            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetSRG',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                    }),
                    success: function (data) {
                        var obj = $.parseJSON(data.GetSRGResult);

                        if (obj != null && obj.length > 0) {
                            $("#txtReportGroupName").val(obj[0].vcName || "");
                            $("[id$=ddlEmailTemplates]").val(parseInt(obj[0].numEmailTemplate || 0));
                            if ((obj[0].vcSelectedTokens || "") != "") {
                                InsertEmail(obj[0].vcSelectedTokens);
                            }
                            $("#ddlFrequency").val(parseInt(obj[0].tintFrequency || 0));

                            var startDate = new Date(obj[0].dtStartDate);

                            var rdpStartDate = $find("<%= rdpStartDate.ClientID%>");
                            rdpStartDate.set_selectedDate(startDate);

                            if (startDate.getHours() == 0) {
                                $("#ddlTime").val(12);
                                $("#ddlAMPM").val(1);
                            }
                            else if (startDate.getHours() > 12) {
                                $("#ddlTime").val(startDate.getHours() % 12);
                                $("#ddlAMPM").val(2);
                            } else {
                                $("#ddlTime").val(startDate.getHours());
                                $("#ddlAMPM").val(1);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function SaveScheduledReportsGroup(isClose) {
            try {
                var isValidEmails = true;
                var startDate = null;
                var rdpStartDate = $find("<%= rdpStartDate.ClientID%>");

                if ($("#txtReportGroupName").val() == "") {
                    alert("Report group name is required.");
                    $("#txtReportGroupName").focus();
                    return false;
                } else if (parseInt($("[id$=ddlEmailTemplates]").val()) == 0) {
                    alert("Email template is required.");
                    $("[id$=ddlEmailTemplates]").focus();
                    return false;
                } else if (saved_tokens.length == 0) {
                    alert("Recipient email address(s) are required.");
                    $("#txtEmailAddress").focus();
                    return false;
                } else if (rdpStartDate.get_selectedDate() == null) {
                    alert("Start Date is required.");
                    return false;
                } else {
                    var selectedTokens = ""
                    var selectedEmails = ""
                    var selectedContactIds = ""
                    
                    
                    saved_tokens.forEach(function (n) {
                        selectedTokens += (n.id + '|')
                        selectedEmails += ((selectedEmails != "" ? "," : "") + n.id.split('~')[1]);
                        selectedContactIds += ((selectedContactIds != "" ? "," : "") + n.id.split('~')[2]);

                        if (!isEmail(n.id.split('~')[1])) {
                            isValidEmails = false;
                        }
                    });

                    if (isValidEmails) {
                        if (saved_tokens.length > 5) {
                            alert("You can add max 5 email addresses.");
                            return false;
                        } else {
                            startDate = rdpStartDate.get_selectedDate();

                            if (parseInt($("#ddlAMPM").val()) == 2) {
                                if (parseInt($("#ddlTime").val()) == 12) {
                                    startDate.setHours(12, 0, 0, 0);
                                } else {
                                    startDate.setHours(parseInt($("#ddlTime").val()) + 12, 0, 0, 0);
                                }
                            } else {
                                if (parseInt($("#ddlTime").val()) == 12) {
                                    startDate.setHours(0, 0, 0, 0);
                                } else {
                                    startDate.setHours(parseInt($("#ddlTime").val()), 0, 0, 0);
                                }
                            }

                            $.ajax({
                                type: "POST",
                                url: '../WebServices/CommonService.svc/SaveSRG',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                                    , "name": $("#txtReportGroupName").val()
                                    , "frequency": parseInt($("#ddlFrequency").val())
                                    , "datetime": "\/Date(" + startDate.getTime() + ")\/"
                                    , "emailTemplate": parseInt($("[id$=ddlEmailTemplates]").val())
                                    , "selectedTokens": selectedTokens
                                    , "recipientsEmail": selectedEmails
                                    , "recipientsContactId": selectedContactIds
                                }),
                                success: function (data) {
                                    if (isClose) {
                                        if (window.opener != null) {
                                            window.opener.location.href = window.opener.location.href;
                                        }

                                        window.close();
                                    } else {
                                        var obj = $.parseJSON(data.SaveSRGResult);

                                        if (obj != null && parseInt(obj) > 0 && parseInt($("[id$=hdnSRGID]").val() || 0) == 0) {
                                            $("[id$=hdnSRGID]").val(obj);
                                            $("#btnSaveClose").removeAttr("disabled");
                                            $("#btnSaveClose").removeAttr("title");
                                            $("#btnSendTestReport").removeAttr("disabled");
                                            $("#btnSendTestReport").removeAttr("title");
                                            $("#btnDelete").removeAttr("disabled");
                                            $("#btnDelete").removeAttr("title");
                                            $("#btnSelectReports").removeAttr("disabled");
                                            $("#btnSelectReports").removeAttr("title");
                                            $("#btnRemoveReports").removeAttr("disabled");
                                            $("#btnRemoveReports").removeAttr("title");
                                        }
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert("Unknown error ocurred");
                                }
                            });
                        }                        
                    } else {
                        alert("Enter valid recipient email address(s).");
                        $("#txtEmailAddress").focus();
                        return false;
                    }
                }
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function DeleteScheduledReportsGroup() {
            try {
                if (confirm("Schedule report group will be deleted. Do you want to proceed?")) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/DeleteSRG',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "srgID": ($("[id$=hdnSRGID]").val() || 0).toString()
                        }),
                        success: function (data) {
                            CloseAndRefresh();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function getReports(page) {
            try {
                totalRecords = 0;
                $("#tblSelectReports > tbody > tr").not(":first").remove();

                return $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetSRGReports',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "srgID": 0
                        , "mode": 1
                        , "reportType": parseInt($("input[name='reporttype']:checked").val())
                        , "templateID": parseInt($("[id$=ddlDashboardTemplates]").val())
                        , "searchText": $("#txtSearchReport").val()
                        , "currentPage": (page || 1)
                        , "pageSize": recordsPerPage
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.GetSRGReportsResult);
                            if (obj != null) {
                                var html = "";
                                var records = $.parseJSON(obj.Records);
                                totalRecords = obj.TotalRecords;

                                var selectedReports = [];
                                if ($("[id$=hdnSelectedReports]").val() != "") {
                                    selectedReports = $.parseJSON($("[id$=hdnSelectedReports]").val());
                                }

                                if (totalRecords > 0) {
                                    records.forEach(function (e) {
                                        html += "<tr id='" + (e.tintReportType || 0) + "~" + (e.numReportID || 0) + "'>";
                                        html += "<td style='width:70%'>" + (e.vcReportName || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcReportType || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcModuleName || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcPerspective || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcTimeline || "") + "</td>";

                                        var result = $.grep(selectedReports, function (f) { return f.ReportType == (e.tintReportType || 0) && f.ReportID == (e.numReportID || 0) });

                                        html += "<td><input type='checkbox' class='chkSelect' onchange='return RecordSelectionChanged(this," + (e.tintReportType || 0) + "," + (e.numReportID || 0) + ");' " + ((result.length > 0) ? "checked" : "") + " /></td>";
                                        html += "</tr>";
                                    });
                                } else {
                                    html += "<tr><td colspan='6'>No Data</td></tr>";
                                }

                                $("#tblSelectReports > tbody").append(html);

                                if (!$('#modalReports').hasClass('in')); {
                                    $('#modalReports').modal('show');
                                }
                            }
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function LoadRecordsWithPagination() {
            $.when(getReports()).then(function () {
                if (totalRecords > 0) {
                    $("#liRecordDisplay").html("<b>" + 1 + " to " + (totalRecords > recordsPerPage ? recordsPerPage : totalRecords) + " of " + totalRecords + "</b>");
                } else {
                    $("#liRecordDisplay").html("<b>0 to 0 of 0</b>");
                }

                $("#ms-pagination-left").pagination('destroy');

                $("#ms-pagination-left").pagination({
                    items: totalRecords,
                    itemsOnPage: recordsPerPage,
                    displayedPages: 3,
                    hrefTextPrefix: "#",
                    onPageClick: function (page) {
                        $("#liRecordDisplay").html("<b>" + ((page - 1) * recordsPerPage + 1) + " to " + (totalRecords > (page * recordsPerPage) ? (page * recordsPerPage) : totalRecords) + " of " + totalRecords + "</b>");
                        getReports(page);
                    }
                });
            });
        }

        function RecordSelectionChanged(chk, reportType, reportID) {
            try {
                var rowID = $(chk).closest("tr").attr("id");

                var selectedReports = [];
                if ($("[id$=hdnSelectedReports]").val() != "") {
                    selectedReports = $.parseJSON($("[id$=hdnSelectedReports]").val());
                }

                if ($(chk).is(":checked")) {
                    if (selectedReports.length >= 5) {
                        alert("You’ve reached the maximum number of reports that can be added to a report group");
                        $(chk).prop("checked", false);
                        return false;
                    } else {
                        var result = $.grep(selectedReports, function (f) { return f.ReportType == reportType && f.ReportID == reportID });
                        if (result.length == 0) {
                            selectedReports.push({ "ReportType": reportType, "ReportID": reportID, "SortOrder": 0 });
                        }
                    }
                } else {
                    selectedReports = jQuery.grep(selectedReports, function (f) {
                        return !(f.ReportType == reportType && f.ReportID == reportID);
                    });
                }

                $("#spnReportCount").text(selectedReports.length);
                $("[id$=hdnSelectedReports]").val(JSON.stringify(selectedReports));
            } catch (e) {
                alert("Unknown error occurred.")
            }
        }

        function DeleteSRGReport() {
            try {
                var selectedReports = [];

                $("#tblReports tr").each(function (tr) {
                    if ($(this).find("input.chkSelect").is(":checked")) {
                        var obj = new Object();
                        obj.tintReportType = parseInt($(tr).attr("id").split("~")[0]);
                        obj.numReportID = parseInt($(tr).attr("id").split("~")[1]);
                        selectedReports.push(obj);
                    }
                });

                if (selectedReports.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/DeleteSRGReport',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                            , "selectedReports": JSON.stringify(selectedReports)
                        }),
                        success: function (data) {
                            LoadScheduledReportsGroupReports();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                } else {
                    alert("Select at least one record.");
                }
            } catch (e) {
                alert("Unknown error ocurred");
            }
        }

        function TestSRG() {
            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/TestSRG',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)                        
                    }),
                    success: function (data) {
                        alert("Email sent")
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }
        }

        function LoadScheduledReportsGroupReports() {
            try {
                $("#tblReports tr").not(":first").remove();

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetSRGReports',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                        , "mode": 2
                        , "reportType": 0
                        , "templateID": 0
                        , "searchText": ""
                        , "currentPage": 1
                        , "pageSize": recordsPerPage
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.GetSRGReportsResult);
                            if (obj != null) {
                                var html = "";
                                var records = $.parseJSON(obj.Records);
                                var selectedReports = [];

                                if (records != null && records.length > 0) {
                                    records.forEach(function (e) {
                                        html += "<tr id='" + (e.tintReportType || 0) + "~" + (e.numReportID || 0) + "'>";
                                        html += "<td style='width:70%'>" + (e.vcReportName || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcReportType || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcModuleName || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcPerspective || "") + "</td>";
                                        html += "<td style='white-space:nowrap;'>" + (e.vcTimeline || "") + "</td>";
                                        html += "<td><input type='text' value=" + (e.intSortOrder || 0) + " class='form-control sortorder' onchange='return UpdateReportSortOrder(" + (e.ID || 0) + ",this)'></td>";
                                        html += "<td><input type='checkbox' class='chkSelect' report='" + (e.ID || 0) + "'/></td>";
                                        html += "</tr>";

                                        selectedReports.push({ "ReportType": (e.tintReportType || 0), "ReportID": (e.numReportID || 0), "SortOrder": (e.intSortOrder || 0) });
                                    });
                                } else {
                                    html += "<tr><td colspan='7'>Report(s) are not selected.</td></tr>";
                                }

                                $("#spnReportCount").text(selectedReports.length);
                                $("[id$=hdnSelectedReports]").val(JSON.stringify(selectedReports))

                                $("#tblReports > tbody").append(html);
                            }
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }
        }

        function SaveScheduledReportsGroupReports() {
            try {
                var selectedReports = [];
                if ($("[id$=hdnSelectedReports]").val() != "") {
                    selectedReports = $.parseJSON($("[id$=hdnSelectedReports]").val());
                }

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/SaveSRGReports',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                        , "reports": JSON.stringify(selectedReports)
                    }),
                    success: function (data) {
                        LoadScheduledReportsGroupReports();
                        $("#modalReports").modal("hide");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }

            return false;
        }

        function DeleteScheduledReportsGroupeports() {
            try {
                var selectedReports = "";

                $("#tblReports tr").not(":first").each(function () {
                    if ($(this).find("input.chkSelect").is(":checked")) {
                        selectedReports = selectedReports + (selectedReports.length > 0 ? "," : "") + $(this).find("input.chkSelect").attr("report");
                    }
                });

                if (selectedReports != null) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/DeleteSRGReports',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                            , "reports": selectedReports
                        }),
                        success: function (data) {
                            LoadScheduledReportsGroupReports();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                } else {
                    alert("Select at least one record.");
                }
            } catch (e) {
                alert("Unknown error ocurred");
            }

            return false;
        }

        function UpdateReportSortOrder(recordID, txt) {
            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/UpdateRGReportSortOrder',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "srgID": parseInt($("[id$=hdnSRGID]").val() || 0)
                        ,"srgrID":recordID
                        , "sortOrder": parseInt($(txt).val() || 0)
                    }),
                    success: function (data) {
                        var selectedReports = [];
                        if ($("[id$=hdnSelectedReports]").val() != "") {
                            selectedReports = $.parseJSON($("[id$=hdnSelectedReports]").val());
                        }

                        selectedReports.forEach(function (f) {
                            if (f.ReportType == parseInt($(txt).closest("tr").attr("id").split("~")[0]) && f.ReportID == parseInt($(txt).closest("tr").attr("id").split("~")[1])) {
                                f.SortOrder = parseInt($(txt).val() || 0);
                            }
                        });
   
                        $("[id$=hdnSelectedReports]").val(JSON.stringify(selectedReports));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }

            return false;
        }

        function ShowEmailTemplate() {
            try {
                if (parseInt($("[id$=ddlEmailTemplates]").val()) > 0) {
                    $("#divETSubject").html("");
                    $("#divETBody").html("");

                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetGenericDocument',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "docID": parseInt($("[id$=ddlEmailTemplates]").val())
                        }),
                        success: function (data) {
                            var obj = $.parseJSON(data.GetGenericDocumentResult);

                            if (obj != null) {
                                $("#divETSubject").html(obj[0].vcSubject || "");
                                $("#divETBody").html(obj[0].vcDocdesc || "");
                            }

                            $("#modalETPreview").modal("show");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                } else {
                    alert("Select email template");
                }
            } catch (e) {
                alert("Unknown error occurred.")
            }
        }

        function InsertEmail(strID) {
            var ArrEmail = strID.toString().split("|");
            var Arr1;
            for (i = 0; i < ArrEmail.length; ++i) {
                if (ArrEmail[i].length > 0) {
                    Arr1 = ArrEmail[i].split("~");
                    $("#txtEmailAddress").tokenInput("add", { id: (Arr1[2] || 0), first_name: (Arr1[3] || "-"), last_name_actual: (Arr1[4] || "-"), email: Arr1[1], company_type: (Arr1[5] || "-") });
                }
            }
        }

        function CloseAndRefresh() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            window.close();
        }
    </script>
    <style type="text/css">
        .token-input-list-facebook {
            width:100% !important;
        }

        .RadInput_Default .riTextBox {
            border-color: #d2d6de !important;
            padding: 6px 12px !important;
            font: inherit !important;
            color: #555 !important;
        }

        #liRecordDisplay {
            line-height: 34px;
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <input type="button" id="btnSave" value="Save" class="btn btn-primary" onclick="return SaveScheduledReportsGroup(false);" />
                <input id="btnSaveClose" type="button" value="Save & Close" title="First save report deails" class="btn btn-primary" disabled="disabled" onclick="return SaveScheduledReportsGroup(true);" />
                <input id="btnSendTestReport" type="button" value="Send test report to my inbox" title="First save report deails" class="btn btn-primary" disabled="disabled" onclick="return TestSRG();" />
                <input type="button" id="btnCancel" value="Cancel" class="btn btn-primary" onclick="CloseAndRefresh();" />
                &nbsp;&nbsp;&nbsp;
                <input id="btnDelete" type="button" value="Delete" class="btn btn-danger" title="First save report deails" disabled="disabled" onclick="return DeleteScheduledReportsGroup();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Scheduled Reports <b>(Max 20)</b>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridFilterTools" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000; display: none">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="row padbottom10">
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label>Report Group Name<span style="color: red"> *</span></label>
                <input id="txtReportGroupName" class="form-control" />
            </div>
            <div class="form-group">
                <label>Email template to use when sending report<span style="color: red"> *</span></label>
                <a href="javascript:ShowEmailTemplate();" class="pull-right"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                <asp:DropDownList ID="ddlEmailTemplates" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label>Frequency<span style="color: red"> *</span></label>
                <select id="ddlFrequency" class="form-control">
                    <option value="1">Daily</option>
                    <option value="2">Weekly</option>
                    <option value="3">Monthly</option>
                </select>
            </div>
            <div class="col-xs-6" style="padding-left: 0px; padding-right: 0px;">
                <div class="form-group">
                    <label>Start Date<span style="color: red"> *</span></label>
                    <div>
                        <telerik:RadDatePicker ID="rdpStartDate" runat="server" DateInput-CssClass="form-control"></telerik:RadDatePicker>
                    </div>
                </div>
            </div>
            <div class="col-xs-6" style="padding-left: 0px; padding-right: 0px;">
                <div class="form-group">
                    <label>Time<span style="color: red"> *</span></label>
                    <div>
                        <ul class="list-inline" style="display: inline">
                            <li>
                                <select id="ddlTime" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </li>
                            <li>
                                <select id="ddlAMPM" class="form-control">
                                    <option value="1">AM</option>
                                    <option value="2">PM</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label>Recipient email address(s) to receive report(s)<span style="color: red"> *</span></label>
                <i class="pull-right">(Max 5)</i>
                <textarea id="txtEmailAddress" style="height: 110px" class="form-control"></textarea>                
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <input type="button" id="btnSelectReports" class="btn btn-success" title="First save report deails" disabled="disabled" value="Select & Add" onclick="LoadRecordsWithPagination(1);" />
                <input type="button" id="btnRemoveReports" class="btn btn-danger" title="First save report deails" disabled="disabled" value="Remove Report" onclick="return DeleteScheduledReportsGroupeports();" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="tblReports" class="table table-bordered table-striped">
                    <tr>
                        <th style="width: 70%">Report</th>
                        <th style="white-space: nowrap">Type</th>
                        <th style="white-space: nowrap">Module</th>
                        <th style="white-space: nowrap">Perspective</th>
                        <th style="white-space: nowrap;">Timeline</th>
                        <th style="white-space: nowrap; width: 80px;">Sort Order</th>
                        <th style="width: 30px"></th>
                    </tr>
                    <tr id="trNoData">
                        <td colspan="7">Report(s) are not selected.</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="modalReports" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Select Reports<i> (Max 5)</i></h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <ul class="list-inline">
                                    <li>
                                        <label>Type:</label></li>
                                    <li class="radio" style="padding-left: 20px;">
                                        <input type="radio" value="1" style="margin-left: -15px" name="reporttype" checked="checked" />Custom Reports</li>
                                    <li class="radio" style="padding-left: 20px;">
                                        <input type="radio" value="2" style="margin-left: -15px" name="reporttype" />Predefined Reports</li>
                                    <li class="radio" style="padding-left: 20px;">
                                        <input type="radio" value="3" style="margin-left: -15px" name="reporttype" />My Reports</li>
                                    <li class="radio" style="padding-left: 20px;">
                                        <input type="radio" value="4" style="margin-left: -15px" name="reporttype" />Private Saved Search</li>
                                    <li class="radio" style="padding-left: 20px;">
                                        <input type="radio" value="5" style="margin-left: -15px" name="reporttype" />Shared Saved Search</li>
                                    <li class="radio" style="padding-left: 20px;">
                                        <input type="radio" value="6" style="margin-left: -15px" name="reporttype" />Dashboard</li>
                                    <li>
                                        <asp:DropDownList ID="ddlDashboardTemplates" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right" style="vertical-align: middle; line-height: 34px;">
                                <div class="form-inline">
                                    <label>Selected Reports:</label>
                                    <span id="spnReportCount" class="badge bg-green"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <div class="form-inline">
                                    <label>Search</label>
                                    <input type="text" class="form-control" id="txtSearchReport" placeholder="Enter text" />
                                </div>
                            </div>
                            <div class="pull-right">
                                <ul class="list-inline" id="min-grid-controls">
                                    <li id="liRecordDisplay"></li>
                                    <li>
                                        <ul id="ms-pagination-left" style="margin: 0px"></ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="tblSelectReports" class="table table-bordered table-striped" style="margin-bottom: 0px;">
                                    <tr>
                                        <th>Report</th>
                                        <th>Type</th>
                                        <th>Module</th>
                                        <th>Perspective</th>
                                        <th>Timeline</th>
                                        <th style="width: 30px"></th>
                                    </tr>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="padding: 5px;">
                    <button type="button" class="btn btn-primary" onclick="return SaveScheduledReportsGroupReports();">Add to Report Group</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="modalETPreview" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Email Template</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Subject</label>
                        <div id="divETSubject">

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <div id="divETBody">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="Div1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Email Template</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Subject</label>
                        <div id="div2">

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <div id="div3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnSelectedReports" />
    <asp:HiddenField runat="server" ID="hdnSRGID" />
</asp:Content>
