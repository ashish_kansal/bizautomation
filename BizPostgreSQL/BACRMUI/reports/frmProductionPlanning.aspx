﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmProductionPlanning.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmProductionPlanning" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../JavaScript/dhtmlxGantt/dhtmlxgantt.css?v=6.3.7" rel="stylesheet" type="text/css" />
    <%--<link href="../JavaScript/dhtmlxGantt/skins/dhtmlxgantt_material.css?v=6.3.7" rel="stylesheet" type="text/css" />--%>
    <script src="../JavaScript/dhtmlxGantt/dhtmlxgantt.js?v=6.3.7" type="text/javascript"></script>
    <style type="text/css">
        /*.gantt_task_line.gantt_task_inline_color {
            border: none;
        }*/

        .gantt_wrapper {
            height: 75vh;
            width: 100%;
        }

        .tooltip {
            top: 0px !important;
        }

        .tooltip-arrow {
            top: 15% !important;
        }

        .tooltip-inner {
            max-width: 100% !important;
        }

        .box-header > .box-tools {
            position: absolute;
            right: 0px;
            top: 3px;
        }

        .manage-wip-date-range li {
            background-color: #dae3f3;
            color: #000;
            font-weight: bold;
        }

        .manage-wip-date-range li a {
            padding: 7px 15px;
            border-top: 0px;
        }
    </style>
    
    <script type="text/javascript">
        var zoomConfig = {
            levels: [
                {
                    name: "minute",
                    scale_height: 100,
                    min_column_width: 30,
                    scales: [
                        { unit: "hour", step: 1, format: "%g %a" },
		                { unit: "day", step: 1, format: "%j %F, %l" },
		                { unit: "minute", step: 15, format: "%i" }
                    ]
                },
                {
                    name: "day",
                    scale_height: 27,
                    min_column_width: 80,
                    scales: [
                        { unit: "day", step: 1, format: "%d %M" }
                    ]
                },
                {
                    name: "week",
                    scale_height: 50,
                    min_column_width: 50,
                    scales: [
                        {
                            unit: "week", step: 1, format: function (date) {
                                var dateToStr = gantt.date.date_to_str("%d %M");
                                var endDate = gantt.date.add(date, -6, "day");
                                var weekNum = gantt.date.date_to_str("%W")(date);
                                return "#" + weekNum + ", " + dateToStr(date) + " - " + dateToStr(endDate);
                            }
                        },
                        { unit: "day", step: 1, format: "%j %D" }
                    ]
                },
                {
                    name: "month",
                    scale_height: 50,
                    min_column_width: 120,
                    scales: [
                        { unit: "month", format: "%F, %Y" },
                        { unit: "week", format: "Week #%W" }
                    ]
                }
            ]
        };

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            BindReportData();
        }

        function ProcessTypeChanged() {
            BindReportData();
        }

        function BindReportData() {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                if (datepicker.get_selectedDate() != null) {
                    var todayDate = new Date(datepicker.get_selectedDate());
                    var fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 0, 0, 0, 0);
                    var toDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 23, 59, 59, 999);

                    var view = parseInt($("#hdnDateRange").val());
                    if (view === 2) {
                        var weekStart = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay()));
                        var weekEnd = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay() + 6));

                        fromDate = new Date(weekStart.getFullYear(), weekStart.getMonth(), weekStart.getDate(), 0, 0, 0, 0);
                        toDate = new Date(weekEnd.getFullYear(), weekEnd.getMonth(), weekEnd.getDate(), 23, 59, 59, 999);
                    } else if (view === 3) {
                        fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1, 0, 0, 0, 0);
                        toDate = new Date(todayDate.getFullYear(), todayDate.getMonth() + 1, 0, 23, 59, 59, 999);
                    }

                    $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsService.svc/GetProductionPlanningReportData',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "processType": parseInt($("input[name='ProcessType']:checked").val())
                            , "fromDate": "\/Date(" + fromDate.getTime() + ")\/"
                            , "toDate": "\/Date(" + toDate.getTime() + ")\/"
                            , "pageIndex": 1
                            , "pageSize": 10
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetProductionPlanningReportDataResult);
                                obj.data = $.parseJSON(obj.data);
                                obj.collections.links = $.parseJSON(obj.collections.links);

                                for (var i = 0; i < obj.data.length; i++) {
                                    if (obj.data[i].start_date != null) {
                                        obj.data[i].start_date = new Date(obj.data[i].start_date);
                                    }
                                    if (obj.data[i].end_date != null) {
                                        obj.data[i].end_date = new Date(obj.data[i].end_date);
                                    }
                                }

                                // specifying the date format
                                gantt.config.date_format = "%m/%d/%Y %H:%i";
                                gantt.config.grid_width = 390;
                                gantt.config.duration_unit = "month";
                                gantt.config.duration_step = 1;
                                gantt.config.date_grid = "%F %d";

                                gantt.config.columns = [
                                    { name: "text", label: "Order (Progress)", tree: true, width: 400, "resize": true, min_width: 10 },
                                    //{ name: "vcItem", label: "Item (Qty)", width: 170, "resize": true, min_width: 10 },
                                    //{ name: "dtRequestedFinish", label: "RF Date", width: 90, "resize": true, min_width: 10 },
                                    //{ name: "dtProjectedFinish", label: "PF Date", width: 90, "resize": true, min_width: 10 },
                                    //{ name: "dtItemReleaseDate", label: "IR Date", width: 90, "resize": true, min_width: 10 },
                                    //{ name: "dtActualStartDate", label: "Actual Start", width: 90, "resize": true, min_width: 10 },
                                    //{ name: "start_date", align: "center", width: 90, "resize": true },
                                    { name: "vcDurationHours", label: "Time", align: "center", width: 80, "resize": true },
                                ];



                                gantt.ext.zoom.init(zoomConfig);
                                gantt.ext.zoom.setLevel("minute");

                                var radios = document.getElementsByName("scale");
                                for (var i = 0; i < radios.length; i++) {
                                    radios[i].onclick = function (event) {
                                        gantt.ext.zoom.setLevel(event.target.value);
                                    };
                                }

                                gantt.templates.progress_text = function (start, end, task) {
                                    return "<div style='text-align: left;color: white;padding-left: 5px;'>" + Math.round(task.progress * 100) + "% </div>";
                                };

                                // initializing gantt
                                gantt.init("divgantt");
                                gantt.parse(obj);

                                

                                gantt.attachEvent("onBeforeTaskDrag", function (id, mode, e) {
                                    if (id.startsWith("WO") || id.startsWith("MS") || id.startsWith("S") || id.startsWith("TC") || id.startsWith("TS")) {
                                        return false;
                                    } else {
                                        return true;
                                    }
                                });

                                gantt.attachEvent("onAfterTaskUpdate", function (id, item) {
                                    try {
                                        if (id.startsWith("T")) {
                                            $.ajax({
                                                type: "POST",
                                                url: '../WebServices/StagePercentageDetailsTaskService.svc/ChangeTimeProductionPlanning',
                                                contentType: "application/json",
                                                dataType: "json",
                                                data: JSON.stringify({
                                                    "taskID": parseInt(id.replace("T",""))
                                                    , "startDate": "\/Date(" + item.start_date.getTime() + ")\/"
                                                    , "endDate": "\/Date(" + item.end_date.getTime() + ")\/"
                                                }),
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    if (IsJsonString(jqXHR.responseText)) {
                                                        var objError = $.parseJSON(jqXHR.responseText)
                                                        alert("Error occurred while updating task time: " + replaceNull(objError.ErrorMessage));
                                                    } else {
                                                        alert("Unknown error ocurred while updating task time.");
                                                    }
                                                }
                                            });
                                        }
                                    } catch (e) {
                                        alert("Error ocurred while updating task time.");
                                    }
                                });

                                //gantt.attachEvent("onAfterTaskDrag", function (id, mode, e) {
                                //    var test = 1;
                                //});

                                gantt.attachEvent("onBeforeLightbox", function (id) {
                                    return false;
                                });
                            } catch (err) {
                                alert("Unknown error occurred while showing report data.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading report data: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while fetching report data.");
                            }
                        }
                    });
                } else {
                    alert("Select date.");
                }                
            } catch (e) {
                alert("Unknown error ocurred while loading report.");
            }
        }

        function dateSelected(sender, eventArgs) {
            BindReportData();
        }

        function DateRangeChanged(dateRange) {
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");
            $("[id$=hdnDateRange]").val(dateRange);
            BindReportData();

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-inline">
                    <li>
                        <input type="radio" name="ProcessType" value="3" checked="checked" onchange="ProcessTypeChanged();" />
                        <b>Build</b>
                        <input type="radio" name="ProcessType" value="1" onchange="ProcessTypeChanged();" />
                        <b>Project</b>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <div class="form-inline gantt_control">
                    <input type="radio" id="scale1" class="gantt_radio" name="scale" value="minute" checked>
                    <label for="scale1">Minute scale</label>

                    <input type="radio" id="scale2" class="gantt_radio" name="scale" value="day">
                    <label for="scale1">Day scale</label>

                    <input type="radio" id="scale3" class="gantt_radio" name="scale" value="week">
                    <label for="scale2">Week scale</label>

                    <input type="radio" id="scale4" class="gantt_radio" name="scale" value="month">
                    <label for="scale3">Month scale</label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Production Planning&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <ul class="list-inline" style="margin-bottom: 0px;">
        <li style="vertical-align: top;">
            <telerik:RadDatePicker ID="rdpDate" runat="server" DateInput-CssClass="form-control" Width="100" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png">
                <ClientEvents OnDateSelected="dateSelected" />
            </telerik:RadDatePicker>
        </li>
        <li>
            <ul class="nav nav-pills manage-wip-date-range">
                <li id="liDateRange1" role="presentation" class="active"><a href="javascript:DateRangeChanged(1);">Day</a></li>
                <li id="liDateRange2" role="presentation"><a href="javascript:DateRangeChanged(2);">Week</a></li>
                <li id="liDateRange3" role="presentation"><a href="javascript:DateRangeChanged(3);">Month</a></li>
            </ul>
        </li>
        <li style="vertical-align: top;">
            <button class="btn btn-flat btn-primary" onclick="return LoadPrevious();">Previous</button>
            <button class="btn btn-flat btn-primary" onclick="return LoadNext();">Next</button>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="divgantt" class="gantt_wrapper panel">
            </div>
        </div>
    </div>

    <input type="hidden" id="hdnDateRange" value="1" />
</asp:Content>
