<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOpportunitySources.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmOpportunitySources" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Opportunity Sources</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }

    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
      <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
            <label>
                From
            </label>
            <BizCalendar:Calendar ID="calFrom" runat="server" />
            <label>To</label>
            <BizCalendar:Calendar ID="calTo" runat="server" />
            <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
        </div>
            <div class="pull-right">
                 <asp:RadioButtonList ID="rdlReportType" runat="server" AutoPostBack="True" CssClass="normal1"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
            </div>
                </div>
            </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
     <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
                <label>
                    Opportunity Type
                </label>
               <asp:DropDownList ID="ddlOpportunityType" CssClass="form-control" runat="server" AutoPostBack="True"
                                Width="130">
                                <asp:ListItem Value="0">Sales</asp:ListItem>
                                <asp:ListItem Value="1">Purchase</asp:ListItem>
                            </asp:DropDownList>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnAddtoMyRtpList" CssClass="btn btn-primary" runat="server"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                <asp:LinkButton ID="btnChooseTeams" CssClass="btn btn-primary" runat="server">Choose Teams</asp:LinkButton>
                 <asp:LinkButton ID="btnChooseTerritories" CssClass="btn btn-primary" runat="server"
                                        >Choose Territories</asp:LinkButton>
                <asp:LinkButton ID="btnExportToExcel" CssClass="btn btn-primary" runat="server" ><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                <asp:LinkButton ID="btnPrint" CssClass="btn btn-primary" runat="server"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server"  ><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                </asp:UpdatePanel>
                
            </div>
                </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblHeader" runat="server">Opportunity Sources</asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table9" Height="400" runat="server" Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server" ID="tblData" Width="100%" Height="20px" CellPadding="0"
                    CellSpacing="0" BorderWidth="0px" CssClass="dg">
                    <asp:TableHeaderRow Height="20px" CssClass="hs">
                        <asp:TableCell Width="25%" HorizontalAlign="Left" Text="Conclusion Basis"></asp:TableCell>
                        <asp:TableCell Width="75%" HorizontalAlign="Left" Text="Percentage (%)"></asp:TableCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
