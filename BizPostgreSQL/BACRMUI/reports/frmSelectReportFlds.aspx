<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSelectReportFlds.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmSelectReportFlds"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Select Report Fields</title>
		<script language="javascript">
			function MoveUp(tbox)
			{
			 
			for(var i=1; i<tbox.options.length; i++)
			{
				if(tbox.options[i].selected && tbox.options[i].value != "") 
				{
				
				var SelectedText,SelectedValue;
				SelectedValue = tbox.options[i].value;
				SelectedText = tbox.options[i].text;
				tbox.options[i].value=tbox.options[i-1].value;
				tbox.options[i].text=tbox.options[i-1].text;
				tbox.options[i-1].value=SelectedValue;
				tbox.options[i-1].text=SelectedText;
				}
			}
			return false;
			}
			function MoveDown(tbox)
			{
			 
			for(var i=0; i<tbox.options.length-1; i++)
			{
				if(tbox.options[i].selected && tbox.options[i].value != "") 
				{
				
				var SelectedText,SelectedValue;
				SelectedValue = tbox.options[i].value;
				SelectedText = tbox.options[i].text;
				tbox.options[i].value=tbox.options[i+1].value;
				tbox.options[i].text=tbox.options[i+1].text;
				tbox.options[i+1].value=SelectedValue;
				tbox.options[i+1].text=SelectedText;
				}
			}
				return false;
			}
		
		sortitems = 0;  // 0-False , 1-True
		function move(fbox,tbox)
			{
			 
			for(var i=0; i<fbox.options.length; i++)
			{
				if(fbox.options[i].selected && fbox.options[i].value != "") 
				{
				/// to check for duplicates 
				for (var j=0;j<tbox.options.length;j++)
					{
						if (tbox.options[j].value == fbox.options[i].value)	
						{
							alert("Item is already selected");
							return false;
						}
					}
				
				var no = new Option();
				no.value = fbox.options[i].value;
				no.text = fbox.options[i].text;
				tbox.options[tbox.options.length] = no;
				fbox.options[i].value = "";
				fbox.options[i].text = "";
				
				}
			}
				BumpUp(fbox);
				if (sortitems) SortD(tbox);
				return false;
			}
			
			function remove(fbox,tbox)
			{
			 
			for(var i=0; i<fbox.options.length; i++)
			{
				if(fbox.options[i].selected && fbox.options[i].value != "") 
				{
				/// to check for duplicates 
				for (var j=0;j<tbox.options.length;j++)
					{
						if (tbox.options[j].value == fbox.options[i].value)	
						{
								fbox.options[i].value = "";
								fbox.options[i].text = "";
							BumpUp(fbox);
							if (sortitems) SortD(tbox);
							return false;
							
							
							//alert("Item is already selected");
							//return false;
						}
					}
				
				var no = new Option();
				no.value = fbox.options[i].value;
				no.text = fbox.options[i].text;
				tbox.options[tbox.options.length] = no;
				fbox.options[i].value = "";
				fbox.options[i].text = "";
				
				}
			}
				BumpUp(fbox);
				if (sortitems) SortD(tbox);
				return false;
			}
			
			function BumpUp(box) 
			{
			for(var i=0; i<box.options.length; i++) 
			{
			if(box.options[i].value == "")  
			{
				for(var j=i; j<box.options.length-1; j++)  
				{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
				}
				var ln = i;
				break;
			}
			}
			if(ln < box.options.length) 
			{
			box.options.length -= 1;
			BumpUp(box);
			}
			}


		function SortD(box) 
		{
			var temp_opts = new Array();
			var temp = new Object();
			for(var i=0; i<box.options.length; i++)  
			{
				temp_opts[i] = box.options[i];
			}
			for(var x=0; x<temp_opts.length-1; x++)  
			{
				for(var y=(x+1); y<temp_opts.length; y++)  
				{
					if(temp_opts[x].text > temp_opts[y].text) 
					{
						temp = temp_opts[x].text;
						temp_opts[x].text = temp_opts[y].text;
						temp_opts[y].text = temp;
						temp = temp_opts[x].value;
						temp_opts[x].value = temp_opts[y].value;
						temp_opts[y].value = temp;
					}
				}
			}
			for(var i=0; i<box.options.length; i++)  
			{
				box.options[i].value = temp_opts[i].value;
				box.options[i].text = temp_opts[i].text;
			}
		}
		function Save()
		{
			var str='';
			for(var i=0; i<document.Form1.lstFldAdded.options.length; i++)
			{
				var SelectedText,SelectedValue;
				SelectedValue = document.Form1.lstFldAdded.options[i].value;
				SelectedText = document.Form1.lstFldAdded.options[i].text;
				str=str+ SelectedValue +','
			}
			document.Form1.hdnValue.value=str;
			
		}
		function Close()
		{
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<table>
                <td width="100%"></td>
                <td  align="center">
		           <asp:UpdateProgress ID ="up1"  runat="server" DynamicLayout="false">
				        <ProgressTemplate>
				         <asp:Image ID="Image1"  ImageUrl="~/images/updating.gif" runat="server"/>
				         </ProgressTemplate>
    			    </asp:UpdateProgress>
    		    </td>
	    </table>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colSpan="3"><br>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom">
					<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Select 
									Fields&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:button>
						<asp:button id="btnClose" Runat="server" Text="Cancel" CssClass="button"></asp:button></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:table id="tbl" Runat="server" BorderColor="black" GridLines="None" Width="100%" CellSpacing="0" CssClass="aspTable"
							CellPadding="0" BorderWidth="1">
							<asp:TableRow>
								<asp:TableCell>
									<table width="100%">
										<TR>
											<TD class="normal1" nowrap>
												Available Fields<br>
												<br>
												<asp:listbox id="lstFielAvail" runat="server" Width="200" Height="80" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</TD>
											<td align="center">
												<asp:button id="btnAdd" CssClass="button" Runat="server" Text="Add >"></asp:button>
												<br>
												<asp:button id="btnRemove" CssClass="button" Runat="server" Text="< Remove"></asp:button>
											</td>
											<td class="normal1">
												Selected Fields<br>
												<asp:listbox id="lstFldAdded" Width="200" Height="80" runat="server" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</td>
										</TR>
									</table>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table></td>
				</tr>
			</table>
			<asp:TextBox ID="hdnValue" Runat="server" style="DISPLAY:none"></asp:TextBox>
			</ContentTemplate>
			</asp:updatepanel>

		</form>
	</body>
</HTML>
