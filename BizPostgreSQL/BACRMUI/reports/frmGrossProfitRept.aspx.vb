Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmGrossProfitRept : Inherits BACRMPage

       

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    
                    GetUserRightsForPage(8, 67)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    objCommon.sb_FillComboFromDBwithSel(ddldepartment, 19, Session("DomainID"))
                    ddldepartment.Items.FindByValue(0).Text = "All"
                    Dim i As Integer
                    For i = 1950 To 2050
                        ddlYear.Items.Add(i)
                    Next
                    ddlYear.Items.FindByValue(Year(Now)).Selected = True
                    ddlTo.SelectedIndex = Month(Now) - 1
                    DisplayDetails()
                End If
                btnAddAmount.Attributes.Add("onclick", "return OpenAddAmt()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDetails()
            Try
                Dim objPreReports As New PredefinedReports
                ''objPreReports.FromDate = ddlFrom.SelectedIndex + 1 & "/1/" & ddlYear.SelectedValue
                objPreReports.FromDate = New Date(ddlYear.SelectedValue, ddlFrom.SelectedIndex + 1, 1)

                ''objPreReports.ToDate = IIf(ddlTo.SelectedIndex = 11, 1, ddlTo.SelectedIndex + 1) & "/1/" & IIf(ddlTo.SelectedIndex = 11, 1, ddlYear.SelectedValue + 1)
                If ddlTo.SelectedIndex = 11 Then
                    objPreReports.ToDate = New Date(ddlYear.SelectedValue + 1, 1, 1)
                Else : objPreReports.ToDate = New Date(ddlYear.SelectedValue, ddlTo.SelectedIndex + 1, 1)
                End If
                ''objPreReports.ToDate = New Date(ddlYear.SelectedValue, IIf(ddlTo.SelectedIndex = 11, 12, ddlTo.SelectedIndex + 1), 1)
                objPreReports.DomainID = Session("DomainID")
                objPreReports.DepID = ddldepartment.SelectedValue
                Dim ds As New DataSet
                ds = objPreReports.GetProfitLoss
                dgSales.DataSource = ds.Tables(0)
                dgPurchase.DataSource = ds.Tables(1)
                dgEmployee.DataSource = ds.Tables(2)
                dgSales.DataBind()
                dgPurchase.DataBind()
                dgEmployee.DataBind()
                Dim i As Integer
                Dim SalesMade, purchaseMade, empExp As Double
                If chkAmountPaid.Checked = True Then
                    For Each dr As DataGridItem In dgSales.Items
                        SalesMade = SalesMade + IIf(dr.Cells(4).Text.Trim = "&nbsp;", 0, dr.Cells(4).Text)
                    Next
                Else
                    For Each dr As DataGridItem In dgSales.Items
                        SalesMade = SalesMade + IIf(dr.Cells(3).Text = "", 0, dr.Cells(3).Text)
                    Next
                End If

                lblSalesMade.Text = String.Format("{0:#,##0.00}", SalesMade)
                lblIncome.Text = lblSalesMade.Text
                For Each dr As DataGridItem In dgPurchase.Items
                    purchaseMade = purchaseMade + IIf(dr.Cells(3).Text = "", 0, dr.Cells(3).Text)
                Next
                lblPurchase.Text = String.Format("{0:#,##0.00}", purchaseMade)
                For Each dr As DataGridItem In dgEmployee.Items
                    empExp = empExp + IIf(dr.Cells(15).Text = "", 0, dr.Cells(15).Text)
                Next
                lblLabor.Text = String.Format("{0:#,##0.00}", empExp)
                lblExpense.Text = String.Format("{0:#,##0.00}", empExp + purchaseMade)

                lblGross.Text = lblSalesMade.Text - lblExpense.Text
                If lblGross.Text < 0 Then lblGross.ForeColor = Color.Red
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddldepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddldepartment.SelectedIndexChanged
            Try
                DisplayDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmployee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEmployee.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(15).Text = String.Format("{0:#,##0.00}", CDbl(CType(e.Item.FindControl("lblAdddAmount"), Label).Text) + CDbl(e.Item.Cells(11).Text) + CDbl(e.Item.Cells(4).Text) + CDbl(e.Item.Cells(6).Text) + CDbl(e.Item.Cells(8).Text) + CDbl(e.Item.Cells(9).Text) + CDbl(e.Item.Cells(10).Text))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 49
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub chkAmountPaid_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAmountPaid.CheckedChanged
            Try
                DisplayDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub chkTotalAmt_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTotalAmt.CheckedChanged
            Try
                DisplayDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function Formatdate(ByVal CloseDate As Date) As String
            Try
                Dim strTargetResolveDate As String = ""
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace