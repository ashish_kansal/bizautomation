<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmLeadActivity.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmLeadActivity" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Leads Activity</title>
    <script language="javascript" type="text/javascript">
        function fnSortByChar(varSortChar) {
            document.form1.txtSortChar.value = varSortChar;
            if (document.form1.txtCurrrentPage != null) {
                document.form1.txtCurrrentPage.value = 1;
            }
            document.form1.btnGo.click();
        }
        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects";
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }

        function OpenSelTeam() {

            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=31", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click();
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                <label>From</label>
                <BizCalendar:Calendar ID="calFrom" runat="server" />
                <label>To</label>
                <BizCalendar:Calendar ID="calTo" runat="server" />
                <asp:Button ID="btnSearch" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
            </div>
                </div>
            <div class="pull-right">
                <div class="form-inline">
                 <asp:RadioButtonList ID="rdlReportType" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                    RepeatLayout="Table" AutoPostBack="True">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
            
                <label id="tdTeams" runat="server">Leads</label>
           
                 <asp:DropDownList ID="ddlLeads" runat="server" CssClass="form-control" AutoPostBack="true">
                                <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Webleads" Value="1"></asp:ListItem>
                                <asp:ListItem Text="My Leads" Value="5"></asp:ListItem>
                                <asp:ListItem Text="public Leads" Value="2"></asp:ListItem>
                            </asp:DropDownList>
            
                <label>Team Members</label>
           
                <asp:DropDownList ID="ddlTeamMembers" runat="server" CssClass="form-control" AutoPostBack="true">
                            </asp:DropDownList>
                <asp:DropDownList ID="ddlGroup" Style="display: none" runat="server" CssClass="form-control"
                               AutoPostBack="true">
                            </asp:DropDownList>
           
                <label>Follow-Up Status</label>
            
                <asp:DropDownList ID="ddlFollow" runat="server" CssClass="form-control" AutoPostBack="true">
                            </asp:DropDownList>
           
            <div class="col-md-1" id="Td1" runat="server">
                 <asp:Label ID="lblRecordCount" runat="server" Visible="false"></asp:Label>
            </div>
        </div>
                </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnAddtoMyRtpList" CssClass="btn btn-primary" runat="server"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                <asp:LinkButton ID="btnChooseTeams" CssClass="btn btn-primary" runat="server">Choose Teams</asp:LinkButton>
                 <asp:LinkButton ID="btnChooseTerritories" CssClass="btn btn-primary" runat="server"
                                        >Choose Territories</asp:LinkButton>
                <asp:LinkButton ID="btnExport" CssClass="btn btn-primary" runat="server" ><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                <asp:LinkButton ID="btnPrint" CssClass="btn btn-primary" runat="server"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server"  ><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExport" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                </asp:UpdatePanel>
            </div>
                </div>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lbLeads" runat="server" Text="Lead Activity"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:DataGrid ID="dgLeads" runat="server" AllowSorting="true" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%"
        AutoGenerateColumns="False" EnableViewState="true">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="False" DataField="numDivisionID"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numRecOwner" HeaderText="numRecOwner">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numTerId"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Date" SortExpression="bintCreatedDate">
                <ItemTemplate>
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate")) %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="vcGrpName" HeaderText="Lead" SortExpression="vcGrpName">
            </asp:BoundColumn>
            <asp:ButtonColumn DataTextField="vcCompanyName" HeaderText="Lead Name" SortExpression="vcCompanyName"
                CommandName="Company"></asp:ButtonColumn>
            <asp:BoundColumn DataField="Employees" SortExpression="Employees" HeaderText="Employees">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vcUserName" SortExpression="vcUserName" HeaderText="Record Owner">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="AssignedTo" SortExpression="AssignedTo" HeaderText="Assigned To">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="FolStatus" SortExpression="FolStatus" HeaderText="FollowUpStatus">
            </asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox><asp:TextBox
        ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox><asp:TextBox
            ID="txtSortChar" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button" Style="display: none">
    </asp:Button>
        </div>
</asp:Content>
