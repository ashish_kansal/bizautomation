﻿Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Public Class rptSalesConversionReport
    Inherits BACRMPage

    Dim objPredefinedReports As New PredefinedReports
    Dim dtReports As DataTable
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then

                GetUserRightsForPage(8, 105)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                End If

                calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Year, -1, Date.UtcNow))
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                bindReports()
                ' = "Reports"
            End If
            btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(53)")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub bindReports()
        Try
            objPredefinedReports.DomainID = Session("DomainID")
            objPredefinedReports.UserCntID = Session("UserContactID")

            objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
            objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
            objPredefinedReports.SortOrder = ddlFilter.SelectedItem.Value
            objPredefinedReports.byteMode = 1

            Dim ds As New DataSet
            ds = objPredefinedReports.GetSalesConversionReport

            dtReports = ds.Tables(0)
            gvSalesConversion.DataSource = dtReports
            gvSalesConversion.DataBind()

            If ds.Tables(1).Rows.Count > 0 Then
                lblTotalConversion.Text = IIf(IsDBNull(ds.Tables(1).Rows(0)("TotalLPConvertedPer")), "", ds.Tables(1).Rows(0)("TotalLPConvertedPer"))
                lblLeadPerDay.Text = IIf(IsDBNull(ds.Tables(1).Rows(0)("TotalLPConverted")), "", ds.Tables(1).Rows(0)("TotalLPConverted"))
                lblAvg1stIncome.Text = String.Format("{0:##,#00.00}", ds.Tables(1).Rows(0)("AvgFirstSales"))
                lblIncome.Text = String.Format("{0:##,#00.00}", ds.Tables(1).Rows(0)("TotalSales"))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            bindReports()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            bindReports()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        ExportToExcel.DataGridToExcel(gvSalesConversion, Response)
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
  
    Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
        Try
            Dim objPredefinedReports As New PredefinedReports
            objPredefinedReports.UserCntID = Session("UserContactID")
            objPredefinedReports.ReportID = 53
            objPredefinedReports.AddToMyReportList()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class