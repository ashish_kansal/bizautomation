Imports BACRM.BusinessLogic.Forecasting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmForeCastReptItem : Inherits BACRMPage

        Dim stFMonth, strSMonth, strTMonth, strfromdate, strfromdateend, strfromdate1, strfromdate1end, strtodate, strtodateend, strSQL1 As String
        Dim intSold(3), intPipeline(3) As Integer
        Dim dblProbality(3) As Double
        Dim intP1, intP2, intP3, intYear As Integer
        Dim dblForecastAmt, dblTotalForAmt As Double
       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    GetUserRightsForPage(8, 49)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExcel.Visible = False
                    End If
                     ' = "Reports"
                    LoadDropdowns()
                End If
                intYear = ddlYear.SelectedItem.Value
                If ddlQuarter.SelectedItem.Value = 1 Then  ' if it is first quarter
                    stFMonth = "January"
                    strSMonth = "February"
                    strTMonth = "March"
                    ''strfromdate = "01/01/" & intYear
                    ''strfromdateend = "01/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 1 + 1, 1))) & "/" & intYear
                    ''strfromdate1 = "02/01/" & intYear
                    ''strfromdate1end = "02/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 2 + 1, 1))) & "/" & intYear
                    ''strtodate = "03/01/" & intYear
                    ''strtodateend = "03/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 3 + 1, 1))) & "/" & intYear

                    strfromdate = New Date(intYear, 1, 1)
                    strfromdateend = New Date(intYear, 1, Day(DateAdd("d", -1, DateSerial(Year(Now), 1 + 1, 1))))
                    strfromdate1 = New Date(intYear, 2, 1)
                    strfromdate1end = New Date(intYear, 2, Day(DateAdd("d", -1, DateSerial(Year(Now), 2 + 1, 1))))
                    strtodate = New Date(intYear, 3, 1)
                    strtodateend = New Date(intYear, 3, Day(DateAdd("d", -1, DateSerial(Year(Now), 3 + 1, 1))))
                ElseIf ddlQuarter.SelectedItem.Value = 2 Then ' if it is second quarter
                    stFMonth = "April"
                    strSMonth = "May"
                    strTMonth = "June"
                    ''strfromdate = "04/01/" & intYear
                    ''strfromdateend = "04/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 4 + 1, 1))) & "/" & intYear
                    ''strfromdate1 = "05/01/" & intYear
                    ''strfromdate1end = "05/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 5 + 1, 1))) & "/" & intYear
                    ''strtodate = "06/01/" & intYear
                    ''strtodateend = "06/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 6 + 1, 1))) & "/" & intYear

                    strfromdate = New Date(intYear, 4, 1)
                    strfromdateend = New Date(intYear, 4, Day(DateAdd("d", -1, DateSerial(Year(Now), 4 + 1, 1))))
                    strfromdate1 = New Date(intYear, 5, 1)
                    strfromdate1end = New Date(intYear, 5, Day(DateAdd("d", -1, DateSerial(Year(Now), 5 + 1, 1))))
                    strtodate = New Date(intYear, 6, 1)
                    strtodateend = New Date(intYear, 6, Day(DateAdd("d", -1, DateSerial(Year(Now), 6 + 1, 1))))
                ElseIf ddlQuarter.SelectedItem.Value = 3 Then ' if it is thrid quarter
                    stFMonth = "July"
                    strSMonth = "August"
                    strTMonth = "September"
                    ''strfromdate = "07/01/" & intYear
                    ''strfromdateend = "07/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 7 + 1, 1))) & "/" & intYear
                    ''strfromdate1 = "08/01/" & intYear
                    ''strfromdate1end = "08/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 8 + 1, 1))) & "/" & intYear
                    ''strtodate = "09/01/" & intYear
                    ''strtodateend = "09/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 9 + 1, 1))) & "/" & intYear

                    strfromdate = New Date(intYear, 7, 1)
                    strfromdateend = New Date(intYear, 7, Day(DateAdd("d", -1, DateSerial(Year(Now), 7 + 1, 1))))
                    strfromdate1 = New Date(intYear, 8, 1)
                    strfromdate1end = New Date(intYear, 8, Day(DateAdd("d", -1, DateSerial(Year(Now), 8 + 1, 1))))
                    strtodate = New Date(intYear, 9, 1)
                    strtodateend = New Date(intYear, 9, Day(DateAdd("d", -1, DateSerial(Year(Now), 9 + 1, 1))))
                ElseIf ddlQuarter.SelectedItem.Value = 4 Then ' if it is fourth quarter
                    stFMonth = "October"
                    strSMonth = "November"
                    strTMonth = "December"
                    ''strfromdate = "10/01/" & intYear
                    ''strfromdateend = "10/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 10 + 1, 1))) & "/" & intYear
                    ''strfromdate1 = "11/01/" & intYear
                    ''strfromdate1end = "11/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 11 + 1, 1))) & "/" & intYear
                    ''strtodate = "12/01/" & intYear
                    ''strtodateend = "12/" & Day(DateAdd("d", -1, DateSerial(Year(Now), 11 + 1, 1))) & "/" & intYear

                    strfromdate = New Date(intYear, 10, 1)
                    strfromdateend = New Date(intYear, 10, Day(DateAdd("d", -1, DateSerial(Year(Now), 10 + 1, 1))))
                    strfromdate1 = New Date(intYear, 11, 1)
                    strfromdate1end = New Date(intYear, 11, Day(DateAdd("d", -1, DateSerial(Year(Now), 11 + 1, 1))))
                    strtodate = New Date(intYear, 12, 1)
                    strtodateend = New Date(intYear, 12, Day(DateAdd("d", -1, DateSerial(Year(Now), 11 + 1, 1))))
                End If
                GetReport()
                btnTeams.Attributes.Add("onclick", "return OpenSelTeam()")
                btnTer.Attributes.Add("onclick", "return OpenTerritory()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                ddlYear.Items.Add(Year(Now()) - 2)
                ddlYear.Items.Add(Year(Now()) - 1)
                ddlYear.Items.Add(Year(Now()))
                ddlYear.Items.Add(Year(Now()) + 1)
                ddlYear.Items.Add(Year(Now()) + 2)
                ddlYear.Items.Add(Year(Now()) + 3)
                ddlYear.Items.Add(Year(Now()) + 4)
                ddlYear.Items.FindByValue(Year(Now())).Selected = True
                Dim dtItem As DataTable
                Dim objOpportunity As New MOpportunity()
                objOpportunity.DomainID = Session("DomainId")
                dtItem = objOpportunity.ItemByDomainID
                ddlItem.DataSource = dtItem
                ddlItem.DataTextField = "vcItemname"
                ddlItem.DataValueField = "numItemcode"
                ddlItem.DataBind()
                ddlItem.Items.Insert(0, "--Select One--")
                ddlItem.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GetReport()
            Try
                If ddlQuarter.SelectedItem.Value > 0 Then
                    If chkShowEmpFor.Checked = True Then
                        Dim objForecast As New Forecast
                        Dim dtForecastUsers As New DataTable
                        Dim intTeam, i As Integer
                        Dim objCell As HtmlTableCell
                        Dim objRow As HtmlTableRow
                        Dim tbl As Table
                        Dim lintrow As Integer
                        Dim lintrowcount As Integer

                        objForecast.UserCntID = Session("UserContactID")
                        objForecast.DomainID = Session("DomainId")
                        objForecast.ForecastType = 20
                        dtForecastUsers = objForecast.GetForRepByTeam
                        If dtForecastUsers.Rows.Count > 0 Then
                            intTeam = dtForecastUsers.Rows(0).Item("numTeam")
                            objCell = New HtmlTableCell
                            objRow = New HtmlTableRow

                            lintrowcount = tblDetails.Rows.Count
                            For lintrow = 2 To lintrowcount
                                tblDetails.Rows.RemoveAt(1)
                            Next
                            objCell.InnerText = IIf(IsDBNull(dtForecastUsers.Rows(0).Item("vcdata")), "", dtForecastUsers.Rows(0).Item("vcdata"))
                            objCell.Attributes.Add("class", "text_bold")
                            objCell.ColSpan = 7
                            objCell.Width = "80%"
                            objRow.Cells.Add(objCell)
                            objCell = New HtmlTableCell
                            Dim lbl As New Label
                            'lbl.ID = "lbl" & dtForecastUsers.Rows(i).Item("numTeam")
                            lbl.CssClass = "text"
                            objCell.Align = "middle"
                            objCell.Controls.Add(lbl)
                            objCell.Width = "20%"
                            objRow.Cells.Add(objCell)
                            tblDetails.Rows.Add(objRow)
                            dblForecastAmt = 0
                            dblTotalForAmt = 0
                            For i = 0 To dtForecastUsers.Rows.Count - 1
                                If intTeam = dtForecastUsers.Rows(i).Item("numTeam") Then
                                    ForecastItemReport(0, dtForecastUsers.Rows(i).Item("numContactID"), dtForecastUsers.Rows(i).Item("vcGivenName"), tblDetails)
                                Else
                                    'Dim lbl1 As Label
                                    'lbl1 = Page.FindControl("lbl" & dtForecastUsers.Rows(i - 1).Item("numTeam"))
                                    'lbl1.Text = dblForecastAmt
                                    dblForecastAmt = 0
                                    intTeam = dtForecastUsers.Rows(i).Item("numTeam")
                                    objCell = New HtmlTableCell
                                    objRow = New HtmlTableRow
                                    objCell.InnerText = IIf(IsDBNull(dtForecastUsers.Rows(i).Item("vcdata")), "", dtForecastUsers.Rows(i).Item("vcdata"))
                                    objCell.Attributes.Add("class", "text_bold")
                                    objCell.ColSpan = 6
                                    objCell.Width = "80%"
                                    objRow.Cells.Add(objCell)
                                    objCell = New HtmlTableCell
                                    Dim lbl2 As New Label
                                    lbl2.CssClass = "text"
                                    'lbl2.ID = "lbl" & dtForecastUsers.Rows(i).Item("numTeam")
                                    objCell.Align = "middle"
                                    objCell.Controls.Add(lbl2)
                                    objCell.Width = "20%"
                                    objRow.Cells.Add(objCell)
                                    tblDetails.Rows.Add(objRow)
                                    ForecastItemReport(0, dtForecastUsers.Rows(i).Item("numContactID"), dtForecastUsers.Rows(i).Item("vcGivenName"), tblDetails)
                                End If
                            Next
                            'Dim lbl3 As Label
                            'lbl3 = Page.FindControl("lbl" & dtForecastUsers.Rows(i - 1).Item("numTeam"))
                            'lbl3.Text = dblForecastAmt
                        End If
                    End If
                    If chkShowPartFor.Checked = True Then
                        Dim objForecast As New Forecast
                        Dim dtForecastUsers As New DataTable
                        Dim intTerr, i As Integer
                        Dim objCell As HtmlTableCell
                        Dim objRow As HtmlTableRow
                        Dim tbl As Table
                        objForecast.UserCntID = Session("UserContactID")
                        objForecast.DomainID = Session("DomainId")
                        objForecast.ForecastType = 4
                        dtForecastUsers = objForecast.GetForRepByTerritory
                        If dtForecastUsers.Rows.Count > 0 Then
                            intTerr = dtForecastUsers.Rows(0).Item("numTerID")
                            objCell = New HtmlTableCell
                            objRow = New HtmlTableRow
                            objCell.InnerText = IIf(IsDBNull(dtForecastUsers.Rows(0).Item("vcTerName")), "", dtForecastUsers.Rows(0).Item("vcTerName"))
                            objCell.Attributes.Add("class", "text_bold")
                            objCell.ColSpan = 7
                            objCell.Width = "80%"
                            objRow.Cells.Add(objCell)
                            objCell = New HtmlTableCell
                            Dim lbl As New Label
                            'lbl.ID = "lbl" & dtForecastUsers.Rows(i).Item("numTeam")
                            lbl.CssClass = "text"
                            objCell.Align = "middle"
                            objCell.Controls.Add(lbl)
                            objCell.Width = "20%"
                            objRow.Cells.Add(objCell)
                            tblDetails.Rows.Add(objRow)
                            dblForecastAmt = 0
                            For i = 0 To dtForecastUsers.Rows.Count - 1
                                If intTerr = dtForecastUsers.Rows(i).Item("numTerID") Then
                                    ForecastItemReport(dtForecastUsers.Rows(i).Item("numDivisionID"), 0, dtForecastUsers.Rows(i).Item("vcCompanyName"), tblDetails)
                                Else
                                    'Dim lbl1 As Label
                                    'lbl1 = Page.FindControl("lbl" & dtForecastUsers.Rows(i - 1).Item("numTeam"))
                                    'lbl1.Text = dblForecastAmt
                                    dblForecastAmt = 0
                                    intTerr = dtForecastUsers.Rows(i).Item("numTeam")
                                    objCell = New HtmlTableCell
                                    objRow = New HtmlTableRow
                                    objCell.InnerText = IIf(IsDBNull(dtForecastUsers.Rows(i).Item("vcTerName")), "", dtForecastUsers.Rows(i).Item("vcTerName"))
                                    objCell.Attributes.Add("class", "text_bold")
                                    objCell.ColSpan = 6
                                    objCell.Width = "80%"
                                    objRow.Cells.Add(objCell)
                                    objCell = New HtmlTableCell
                                    Dim lbl2 As New Label
                                    lbl2.CssClass = "text"
                                    'lbl2.ID = "lbl" & dtForecastUsers.Rows(i).Item("numTerID")
                                    objCell.Align = "middle"
                                    objCell.Controls.Add(lbl2)
                                    objCell.Width = "20%"
                                    objRow.Cells.Add(objCell)
                                    tblDetails.Rows.Add(objRow)
                                    ForecastItemReport(dtForecastUsers.Rows(i).Item("numDivisionID"), 0, dtForecastUsers.Rows(i).Item("vcCompanyName"), tblDetails)
                                End If
                            Next
                            'Dim lbl3 As Label
                            'lbl3 = Page.FindControl("lbl" & dtForecastUsers.Rows(i - 1).Item("numTeam"))
                            'lbl3.Text = dblForecastAmt
                        End If
                    End If
                    lblForecastamt.Text = Format(dblTotalForAmt, "#,##0.00")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ForecastItemReport(ByVal lngDivID As Integer, ByVal lngCntID As Integer, ByVal UserName As String, ByVal objTable As HtmlTable)
            Try
                Dim objCell As HtmlTableCell
                Dim objRow As HtmlTableRow
                Dim objForecast As New Forecast
                Dim dtForecast As DataTable
                objForecast.UserCntID = lngCntID
                objForecast.DomainID = Session("DomainID")
                objForecast.byteMode = 0
                objForecast.IntYear = intYear
                objForecast.Type = 1
                objForecast.DivisionID = lngDivID
                objForecast.ItemCode = ddlItem.SelectedItem.Value
                If ddlQuarter.SelectedItem.Value = 1 Then  ' if it is first quarter
                    objForecast.firstMonth = 1
                    objForecast.secondMonth = 2
                    objForecast.thirdMonth = 3
                ElseIf ddlQuarter.SelectedItem.Value = 2 Then ' if it is second quarter
                    objForecast.firstMonth = 4
                    objForecast.secondMonth = 5
                    objForecast.thirdMonth = 6
                ElseIf ddlQuarter.SelectedItem.Value = 3 Then ' if it is thrid quarter
                    objForecast.firstMonth = 7
                    objForecast.secondMonth = 8
                    objForecast.thirdMonth = 9
                ElseIf ddlQuarter.SelectedItem.Value = 4 Then ' if it is fourth quarter
                    objForecast.firstMonth = 10
                    objForecast.secondMonth = 11
                    objForecast.thirdMonth = 12
                End If

                dtForecast = objForecast.GetForecastItems
                dblForecastAmt = dblForecastAmt + CType(dtForecast.Compute("sum(ForeCastAmount)", ""), Decimal)

                objCell = New HtmlTableCell
                objRow = New HtmlTableRow
                objTable.Width = "100%"
                objCell.InnerText = "User Account:" & UserName
                objCell.ColSpan = 6
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)
                objTable.Rows.Add(objRow)

                'First Month
                objRow = New HtmlTableRow
                objRow.Attributes.Add("class", "tr1")
                objCell = New HtmlTableCell
                objCell.InnerText = stFMonth
                objCell.Width = "10%"
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                If dtForecast.Rows.Count > 0 Then
                    objCell.InnerText = Format(CInt(IIf(IsDBNull(dtForecast.Rows(0).Item("QuotaAmt")), 0, dtForecast.Rows(0).Item("QuotaAmt"))), "#,##0.00")
                Else : objCell.InnerText = "-"
                End If
                objCell.Attributes.Add("class", "normal1")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(0).Item("sold"), "#,##0.00")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(0).Item("Pipeline"), "#,##0.00")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(0).Item("Probability"), "#,##0.00")
                objCell.Width = "20%"
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(0).Item("ProbableAmt"), "#,##0.00")
                objCell.Width = "20%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                If dtForecast.Rows.Count > 0 Then
                    Dim hpl As New HyperLink
                    'hpl.Attributes.Add("onclick", "return OpenForAmt(" & dtForecast.Rows(0).Item("numforecastid") & ")")
                    hpl.Text = Format(dtForecast.Rows(0).Item("ForeCastAmount"), "#,##0.00")
                    'hpl.CssClass = "hyperlink"
                    objCell.Controls.Add(hpl)
                Else : objCell.InnerText = "-"
                End If

                ' ''If dtForecastOppHstr.Rows.Count > 0 Then
                ' ''    Dim hpl As New HyperLink
                ' ''    hpl.Attributes.Add("onclick", "return OpenForAmt(" & dtForecastOppHstr.Rows(0).Item("numforecastid") & ")")
                ' ''    hpl.Text = Format(dtForecastOppHstr.Rows(0).Item("monForecastAmount"), "#,##0.00")
                ' ''    hpl.CssClass = "hyperlink"
                ' ''    objCell.Controls.Add(hpl)
                ' ''Else
                ' ''    objCell.InnerText = "-"
                ' ''End If

                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objCell.Width = "20%"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)
                objTable.Rows.Add(objRow)

                'Second Month
                objRow = New HtmlTableRow
                objCell = New HtmlTableCell
                objCell.InnerText = strSMonth
                objCell.Width = "10%"
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                If dtForecast.Rows.Count > 0 Then
                    objCell.InnerText = Format(CInt(IIf(IsDBNull(dtForecast.Rows(1).Item("QuotaAmt")), 0, dtForecast.Rows(1).Item("QuotaAmt"))), "#,##0.00")
                Else : objCell.InnerText = "-"
                End If
                objCell.Attributes.Add("class", "normal1")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(1).Item("sold"), "#,##0.00")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(1).Item("Pipeline"), "#,##0.00")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(1).Item("Probability"), "#,##0.00")
                objCell.Width = "20%"
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(1).Item("ProbableAmt"), "#,##0.00")
                objCell.Width = "20%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                If dtForecast.Rows.Count > 0 Then
                    Dim hpl As New HyperLink
                    'hpl.Attributes.Add("onclick", "return OpenForAmt(" & dtForecast.Rows(1).Item("numforecastid") & ")")
                    hpl.Text = Format(dtForecast.Rows(1).Item("ForeCastAmount"), "#,##0.00")
                    'hpl.CssClass = "hyperlink"
                    objCell.Controls.Add(hpl)
                Else : objCell.InnerText = "-"
                End If
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objCell.Width = "20%"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)
                objTable.Rows.Add(objRow)

                'Third Month
                objRow = New HtmlTableRow
                objRow.Attributes.Add("class", "tr1")
                objCell = New HtmlTableCell
                objCell.InnerText = strTMonth
                objCell.Width = "10%"
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                If dtForecast.Rows.Count > 0 Then
                    objCell.InnerText = Format(CInt(IIf(IsDBNull(dtForecast.Rows(2).Item("QuotaAmt")), 0, dtForecast.Rows(2).Item("QuotaAmt"))), "#,##0.00")
                Else : objCell.InnerText = "-"
                End If
                objCell.Attributes.Add("class", "normal1")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(2).Item("sold"), "#,##0.00")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(2).Item("Pipeline"), "#,##0.00")
                objCell.Width = "10%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(2).Item("Probability"), "#,##0.00")
                objCell.Width = "20%"
                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                objCell.InnerText = Format(dtForecast.Rows(2).Item("ProbableAmt"), "#,##0.00")
                objCell.Width = "20%"
                objCell.Align = "middle"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)

                objCell = New HtmlTableCell
                If dtForecast.Rows.Count > 0 Then
                    Dim hpl As New HyperLink
                    'hpl.Attributes.Add("onclick", "return OpenForAmt(" & dtForecast.Rows(2).Item("numforecastid") & ")")
                    hpl.Text = Format(dtForecast.Rows(2).Item("ForeCastAmount"), "#,##0.00")
                    'hpl.CssClass = "hyperlink"
                    objCell.Controls.Add(hpl)
                Else : objCell.InnerText = "-"
                End If

                objCell.Attributes.Add("class", "normal1")
                objCell.Align = "middle"
                objCell.Width = "20%"
                objCell.Attributes.Add("class", "normal1")
                objRow.Cells.Add(objCell)
                objTable.Rows.Add(objRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 20
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
            Try
                ExportToExcel.aspTableToExcel(tblDetails, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                GetReport()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace

