﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Public Class frmAuditReport1
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(8, 106)
            If Not IsPostBack Then
                Dim objItems As New CItems
                objItems.DomainID = CCommon.ToDouble(Session("DomainID"))
                gvAuditReport.DataSource = objItems.GetAuditReport()
                gvAuditReport.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class