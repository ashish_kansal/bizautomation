Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Partial Class frmMyReports : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim iSequenceCounter As Integer
    Dim objReportManage As New CustomReportsManage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                ' = "Reports"
                GetMyreportList()
                'GetCustomMyReportList()                                 'Display the list of Custom Reports
            End If
            'btnEditOrder.Attributes.Add("onclick", "return OpenReportsOrder()")
            'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.frames['topframe'].SelectTabByValue('6');}else{ parent.frames['topframe'].SelectTabByValue('6'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub GetMyreportList()
        Try
            Dim objReportManage As New CustomReportsManage
            objReportManage.DomainID = Session("DomainId")
            objReportManage.UserCntID = Session("UserContactID")
            Dim dtMyReportList As DataTable = objReportManage.GetUserReportList

            gvReport.DataSource = dtMyReportList
            gvReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvReport_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReport.RowCommand
        Try
            If e.CommandName = "DeleteReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                objReportManage.DomainID = Session("DomainId")
                objReportManage.ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                objReportManage.DeleteReportListMaster()

                GetMyreportList()
            ElseIf e.CommandName = "AddToMyReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                objReportManage.ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                objReportManage.UserCntID = Session("UserContactId")
                objReportManage.DomainID = Session("DomainId")
                objReportManage.tintReportType = 1
                objReportManage.ManageUserReportList()
            ElseIf e.CommandName = "DuplicateReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                With objReportManage
                    .ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .ManageReportListMaster(bitDuplicate:=True)
                End With

                Response.Redirect("../reports/frmReportStep2.aspx?ReptID=" & objReportManage.ReportID)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvReport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvReport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                iSequenceCounter = 0
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                iSequenceCounter = iSequenceCounter + 1
                Dim lblSequence As Label = e.Row.FindControl("lblSequence")
                lblSequence.Text = iSequenceCounter

                Dim hplRun As HyperLink = e.Row.FindControl("hplRun")
                Dim hplEdit As HyperLink = e.Row.FindControl("hplEdit")
                Dim lnkDelete As LinkButton = e.Row.FindControl("lnkDelete")

                If CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "tintReportType")) = 1 Then
                    hplRun.NavigateUrl = "../reports/frmCustomReportRun.aspx?ReptID=" & DataBinder.Eval(e.Row.DataItem, "numReportID")
                    hplEdit.NavigateUrl = "../reports/frmReportStep2.aspx?ReptID=" & DataBinder.Eval(e.Row.DataItem, "numReportID")
                Else
                    hplRun.Visible = False
                    hplEdit.Visible = False
                    lnkDelete.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
