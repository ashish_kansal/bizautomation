<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmMyReports.aspx.vb"
    Inherits="frmMyReports" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>My Reports</title>
    <script language="javascript">
        //function OpenReportsOrder() {
        //    window.open("../reports/frmSetMyReptOrder.aspx", '', 'width=650,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
        //    return false;
        //}


        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected Report?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function PopupCheck() {
            document.form1.btnGo.click()
        }
        function OpenHelp() {
            window.open('../Help/Reports.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    My Reports&nbsp;<a href="#" onclick="return OpenHelpPopUp('reports/frmmyreports.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" DataKeyNames="numReportID" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateField HeaderStyle-Width="55" ItemStyle-Width="55">
                            <HeaderTemplate>
                                Sr. No
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSequence"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Name
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="txtReportName" runat="server" NavigateUrl='<%# Eval("vcFileName")%>' Text='<%# Eval("RPtHeading")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplRun" runat="server" Text="Run" CssClass="btn btn-success btn-xs" ToolTip="Run Report"><i class="fa fa-play" aria-hidden="true"></i></asp:HyperLink>
                                &nbsp;&nbsp;
                                                <asp:HyperLink ID="hplEdit" runat="server" Text="Edit" CssClass="btn btn-info btn-xs" ToolTip="Edit Report"><i class="fa fa-pencil" aria-hidden="true"></i></asp:HyperLink>
                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="btn btn-danger btn-xs" ToolTip="Delete Report" OnClientClick="javascript:return DeleteRecord();" CommandName="DeleteReport"><i class="fa fa-trash-o" aria-hidden="true"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
