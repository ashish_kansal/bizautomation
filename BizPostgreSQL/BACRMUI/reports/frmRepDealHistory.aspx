<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmRepDealHistory.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits="BACRM.UserInterface.Reports.frmRepDealHistory" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Deal History</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
            <label>
                From
            </label>
            <BizCalendar:Calendar ID="calFrom" runat="server" />
            <label>To</label>
            <BizCalendar:Calendar ID="calTo" runat="server" />
            <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
        </div>
            <div class="pull-right">
                  <asp:RadioButtonList ID="rdlReportType" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                    RepeatLayout="Table" AutoPostBack="True">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
            </div>
                </div>
            </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
                <label>
                    Filter :
                </label>
               <asp:DropDownList ID="ddlFilter" CssClass="form-control" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="1">Open & Completed Sales Deals</asp:ListItem>
                    <asp:ListItem Value="2">Open & Completed Purchase Deals</asp:ListItem>
                    <asp:ListItem Value="3">Lost Sales Deals</asp:ListItem>
                    <asp:ListItem Value="4">Lost Purchase Deals</asp:ListItem>
                    <asp:ListItem Value="5">Organization</asp:ListItem>
                </asp:DropDownList>
                <div id="tdOrg" runat="server">
                <label id="tdDivision" runat="server">Organization</label>
                <asp:TextBox ID="txtCompName" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:Button ID="btnCompanyGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True"
                                CssClass="form-control">
                            </asp:DropDownList>
                    </div>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnAddtoMyRtpList" CssClass="btn btn-primary" runat="server"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                <asp:LinkButton ID="btnChooseTeams" CssClass="btn btn-primary" runat="server">Choose Teams</asp:LinkButton>
                 <asp:LinkButton ID="btnChooseTerritories" CssClass="btn btn-primary" runat="server"
                                        >Choose Territories</asp:LinkButton>
                <asp:LinkButton ID="btnExport" CssClass="btn btn-primary" runat="server" ><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                <asp:LinkButton ID="btnPrint" CssClass="btn btn-primary" runat="server"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server"  ><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExport" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                </asp:UpdatePanel>
                
            </div>
                </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Deal History
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="Table9" Width="100%" Height="400" runat="server" CellSpacing="0" CellPadding="0"
        CssClass="aspTable" GridLines="None" BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgDeals" AllowSorting="True" runat="server" Width="100%" CssClass="table table-responsive table-bordered tblDataGrid"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numOppId" HeaderText="numOppId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numCompanyID" HeaderText="numCompanyID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numTerID" HeaderText="numTerID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numDivisionID" HeaderText="numDivisionID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="tintCRMType" HeaderText="tintCRMType"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numContactID" HeaderText="numContactID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="Name" SortExpression="Name" HeaderText="Name"
                            CommandName="Name"></asp:ButtonColumn>
                        <asp:ButtonColumn DataTextField="Company" SortExpression="Company" HeaderText="Organization"
                            CommandName="Customer"></asp:ButtonColumn>
                        <asp:ButtonColumn DataTextField="Contact" SortExpression="Contact" HeaderText="Contact"
                            CommandName="Contact"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="vcusername" SortExpression="vcusername" HeaderText="Purchasing Mgr"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Type" SortExpression="Type" HeaderText="Type"></asp:BoundColumn>
                        <asp:BoundColumn DataField="STAGE" SortExpression="STAGE" HeaderText="Status"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Due Date" SortExpression="CloseDate">
                            <ItemTemplate>
                                <%# ReturnName(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="monPAmount" DataFormatString="{0:#,##0.00}" SortExpression="monPAmount"
                            HeaderText="Amount"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
