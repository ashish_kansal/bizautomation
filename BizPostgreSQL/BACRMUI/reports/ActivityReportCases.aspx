<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="ActivityReportCases.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.ActivityReportCases" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>ActivityReport</title>
    <script language="javascript">
        //		function fnSortByChar(varSortChar)
        //			{
        //				document.Form1.txtSortChar.value = varSortChar;
        //				document.Form1.submit();
        //			}
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Cases
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dgCases" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
       AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="False" DataField="ActCntID"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCompanyID"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numTerID"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numDivisionID"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCreatedBy"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="tintCRMType"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCaseID"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcCaseNumber" SortExpression="vcCaseNumber" HeaderText="Case #">
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Resolve Date" SortExpression="inttargetresolvedate">
                <ItemTemplate>
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "inttargetresolvedate")) %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="CaseOwner" SortExpression="CaseOwner" HeaderText="Case Owner">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vccompanyname" SortExpression="vccompanyname" HeaderText="Account Name">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Account Contact">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="textsubject" SortExpression="textsubject" HeaderText="Subject">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="priority" SortExpression="priority" HeaderText="Priority">
            </asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
