<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOpportunityStatusPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmOpportunityStatusPrint"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       <title>frmOpportunityStatusPrint</title>
		
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server"></asp:Label>
					</td>
				</tr>
				
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:DataGrid ID="dgOpportunityStatus" CssClass="dg" Width="100%" CellPadding="0" CellSpacing="0"
				Runat="server" AutoGenerateColumns="false">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="DueDate" SortExpression="DueDate" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Due Date</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="OpportunityName" SortExpression="OpportunityName" HeaderText="<font color=white>Opportunity Name</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Amount" DataFormatString="{0:f}" SortExpression="Amount" HeaderText="<font color=white>Amount %</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastMileStoneCompleted" DataFormatString="{0:f}" SortExpression="LastMileStoneCompleted"
						HeaderText="<font color=white>Last Milestone Completed (%)</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastStageCompleted" SortExpression="LastStageCompleted" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>Last Stage Completed</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<script language="JavaScript">
				function Lfprintcheck()
				{
					this.print()
					//history.back()
					document.location.href = "frmOpportunityStatus.aspx";
				}
			</script>
		</form>
	</body>
</HTML>
