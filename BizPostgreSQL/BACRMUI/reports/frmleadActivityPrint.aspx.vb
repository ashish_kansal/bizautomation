Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports System.Configuration

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmleadActivityPrint : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                'Set teh dates to a range of one week ending today.
                lbltodt.Text = FormattedDateFromDate(GetQueryStringVal( "tdt"), Session("DateFormat"))
                lblfromdt.Text = FormattedDateFromDate(GetQueryStringVal( "fdt"), Session("DateFormat"))
                BindDatagrid()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim dtLeads As DataTable
                Dim objReports As New LeadsActivity
                objReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lblfromdt.Text))
                objReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lbltodt.Text))
                With objReports
                    .GroupID = CInt(GetQueryStringVal( "GroupID"))
                    .FollowUpStatus = GetQueryStringVal( "FollowUpStatus")

                    Select Case GetQueryStringVal( "rdlReportType")
                        Case 0 : objReports.UserRights = 1
                        Case 1 : objReports.UserRights = 2
                        Case 2 : objReports.UserRights = 3
                    End Select
                    .UserCntID = Session("UserContactID")
                    .TeamMemID = GetQueryStringVal( "TeamMemID")
                    .ListType = IIf(GetQueryStringVal( "ListType") = True, 0, 1)
                    .TeamType = 31
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .DomainID = Session("DomainID")
                    If GetQueryStringVal( "Column") <> "" Then
                        .columnName = GetQueryStringVal( "Column")
                    Else : .columnName = "bintcreateddate"
                    End If
                    If Session("Asc") = 1 Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                End With
                dtLeads = objReports.GetAllLeadList
                Dim dv As DataView = New DataView(dtLeads)
                dv.Sort = objReports.columnName & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgLeads.DataSource = dv
                dgLeads.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace