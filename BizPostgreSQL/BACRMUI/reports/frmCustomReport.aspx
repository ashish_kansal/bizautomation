<%@ Page Language="vb" AspCompat="true" AutoEventWireup="false" CodeBehind="frmCustomReport.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmCustomReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Report</title>
    <%--<script src="../JavaScript/jquery.min.js" type="text/javascript"></script>--%>

    <script language="javascript" type="text/javascript">
        function Redirect(a) {
            window.location.href = "../reports/frmCustomReportWiz.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=" + a
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        }

        $(document).ready(function () {
            $('a[href*="frmMailDtl.aspx"]').click(function () {
                window.open($(this).attr('href'), '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            });
        });
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
<label>Report Name :</label>&nbsp;<asp:Label runat="server" ID="lblReportName" CssClass="normal7"></asp:Label>&nbsp;
<label>Report Description :</label>&nbsp;<asp:Label runat="server" ID="lblReportDesc" CssClass="normal7"></asp:Label>&nbsp;
<label>Report Generated Date :</label>&nbsp;<asp:Label runat="server" ID="lblGeneratedOn" CssClass="normal7"></asp:Label>&nbsp;

                </div>
            </div>
           
                <div class="pull-right">
                    <asp:UpdatePanel ID="Updatepanel1" runat="server">
                        <ContentTemplate>
                             <asp:LinkButton runat="server" ID="btnEdit" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnExpToExcel" CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp;Export To Excel</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnExpToPdf" CssClass="btn btn-primary"><i class="fa fa-file-pdf-o"></i>&nbsp;Export To Pdf</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnAddMyreport" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;Add To My Reports</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnclose" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;Close</asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"></asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnEdit" />
                            <asp:PostBackTrigger ControlID="btnExpToExcel" />
                            <asp:PostBackTrigger ControlID="btnExpToPdf" />
                            <asp:PostBackTrigger ControlID="btnAddMyreport" />
                            <asp:PostBackTrigger ControlID="btnclose" />
                            <asp:PostBackTrigger ControlID="btnDelete" />
                        </Triggers>
                    </asp:UpdatePanel>
                                   
                
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table runat="server" ID="tblReport" Width="100%" BorderWidth="1" CellSpacing="0"
        CellPadding="0" BorderColor="Black" Height="300" CssClass="aspTable">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <asp:GridView ID="dgReport" AllowSorting="true" runat="server" Width="100%" CssClass="table table-responsive table-bordered tblDataGrid"
                    AutoGenerateColumns="false">
                    <%--<HeaderStyle CssClass="hs" />
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <FooterStyle CssClass="hs" />--%>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell>
                <asp:Table runat="server" ID="tblSummaryGrid" Width="100%" BorderWidth="1" CellSpacing="1"
                    CellPadding="0" BorderColor="Black">
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Label runat="server" ID="Label1" CssClass="normal7"></asp:Label>
    <asp:TextBox runat="server" ID="txtGridType" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtSql" Style="display: none"></asp:TextBox>
</asp:Content>
