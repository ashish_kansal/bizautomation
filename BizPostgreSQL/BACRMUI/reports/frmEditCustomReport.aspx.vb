﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmEditCustomReport
    Inherits BACRMPage
#Region "Memeber Variables"
    Private objCustomQueryReport As CustomQueryReport
#End Region

#Region "Constructor"
    Sub New()
        Try
            objCustomQueryReport = New CustomQueryReport
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""

            If Not Page.IsPostBack Then
                If CCommon.ToLong(GetQueryStringVal("numReportID")) > 0 Then
                    hdnReportID.Value = CCommon.ToLong(GetQueryStringVal("numReportID"))
                    BindReportData()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If Not String.IsNullOrEmpty(txtReportName.Text.Trim()) Then
                objCustomQueryReport.ReportID = CCommon.ToLong(hdnReportID.Value)
                objCustomQueryReport.ReportName = txtReportName.Text.Trim()
                objCustomQueryReport.ReportDescription = txtReportDescription.Text.Trim()
                objCustomQueryReport.EmailTo = txtEmailTo.Text.Trim()
                objCustomQueryReport.SaveEmailChanges()

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CloseWindow", "CloseAndRefresh()", True)
            Else
                lblException.Text = "Required fields detail is not provided."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindReportData()
        Try
            objCustomQueryReport.ReportID = CCommon.ToLong(hdnReportID.Value)
            Dim objNewCustomQueryReport As CustomQueryReport = objCustomQueryReport.GetByID()

            If objCustomQueryReport.ReportID > 0 Then
                txtReportName.Text = objNewCustomQueryReport.ReportName
                txtReportDescription.Text = objNewCustomQueryReport.ReportDescription
                txtEmailTo.Text = objNewCustomQueryReport.EmailTo
            Else
                lblException.Text = "Report doesn't exists."
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

End Class