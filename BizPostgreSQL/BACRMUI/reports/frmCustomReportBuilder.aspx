<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCustomReportBuilder.aspx.vb" Inherits="frmCustomReportBuilder" ValidateRequest="false"%>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Custom Report Builder</title>
		
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  
		<script language="JavaScript" src="../javascript/CustomReports.js" type="text/javascript"></script>
		
		<script language="javascript" type="text/javascript" >
		function OpenSelTeam(repNo)
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function PopupCheck()
		{
		  
		}
		</script>
		
	</HEAD>
	<body >
		
		<form id="frmCustomReportBuilder" method="post" runat="server">
		<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>		
	   <%-- <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>--%>
			<asp:table id="tblCustomReports" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server"
				 GridLines="None" CssClass="aspTable">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top" Width="49%" RowSpan="2" style="PADDING-LEFT:10px;">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="bottom" width="130">
									<table class="TabStyle">
							        <tr>
								        <td>&nbsp;&nbsp;&nbsp; Organizations 
												&nbsp;&nbsp;&nbsp;
											</td>
										</tr>
									</table>
								</td>
								<td valign="top" align="right">
									<asp:button id="btnOrgReport" Runat="server" Text="Go" Width="35px"  CssClass="button" CommandName="OrgReport"
										OnCommand="btnGetReportParameters_Click" style="margin-bottom:3px;"></asp:button>&nbsp;</td>
							</tr>
						</table>
						<asp:table id="tblOrgReport" runat="server" Width="100%" Height="118px" borderColor="black"
							BorderWidth="1px" CellPadding="0" CellSpacing="1">
							<asp:TableRow>
								<asp:TableCell CssClass="normal1" HorizontalAlign="Right" VerticalAlign="Top" Width="30%" style="PADDING-top:7px;">
									Relationships
								</asp:TableCell>
								<asp:TableCell CssClass="normal1" VerticalAlign="Top">
									<asp:RadiobuttonList id="rbCRMType" runat="server" CssClass="normal1" RepeatDirection="Horizontal">
										<asp:ListItem Value="1" Selected="True">Prospects</asp:ListItem>
										<asp:ListItem Value="2">Accounts</asp:ListItem>
									</asp:RadiobuttonList>
									&nbsp;&nbsp;
									<asp:DropDownList id="ddlRelationShips" runat="server" CssClass="signup"></asp:DropDownList>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="normal1" HorizontalAlign="Right">Include Custom Fields?</asp:TableCell>
								<asp:TableCell CssClass="normal1">
									&nbsp;
									<asp:CheckBox Runat="server" ID="cbIncludeCustomFieldsOrg"></asp:CheckBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="normal1" HorizontalAlign="Right">
									Include contacts? 
								</asp:TableCell>
								<asp:TableCell CssClass="normal1">
									<asp:RadiobuttonList id="rbIncludeContacts" runat="server" CssClass="normal1" RepeatDirection="Horizontal">
										<asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
										<asp:ListItem Value="0">No</asp:ListItem>
									</asp:RadiobuttonList>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow ID="trContactTypes" Runat="Server" style="display:inline;">
								<asp:TableCell CssClass="normal1" HorizontalAlign="Right">
									Contact Types
								</asp:TableCell>
								<asp:TableCell CssClass="normal1">
									&nbsp;&nbsp;
									<asp:DropDownList id="ddlContactTypes" runat="server" CssClass="signup"></asp:DropDownList>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</asp:TableCell>
					<asp:TableCell Width="2%" RowSpan="2">&nbsp;</asp:TableCell>
					<asp:TableCell VerticalAlign="Top" Width="49%" style="PADDING-Right:10px;">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="bottom" width="250">
									<table class="TabStyle" borderColor="black" cellSpacing="0" border="0">
										<tr>
											<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp; Opportunities & Items 
												&nbsp;&nbsp;&nbsp;
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top" align="right">
									<asp:button id="btnOppItemsReport" Runat="server" Text="Go" Width="35px" CssClass="button" CommandName="OppItemsReport"
										OnCommand="btnGetReportParameters_Click" style="margin-bottom:3px;"></asp:button>&nbsp;</td>
							</tr>
						</table>
						<asp:table id="tblOppAndItems" borderColor="black" runat="server" Width="100%" height="25px" 
							BorderWidth="1px" CellPadding="0" CellSpacing="1">
							<asp:TableRow>
								<asp:TableCell CssClass="normal1">&nbsp;
									<asp:CheckBox Runat="server" ID="cbIncludeCustomFieldsOppItems" Text="Include any Custom Fields associated with selection"></asp:CheckBox>
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="normal1">
									<asp:Radiobuttonlist Runat="server" ID="rbOppItemType" CssClass="normal1" RepeatDirection="Horizontal">
										<asp:ListItem Value="S" Selected="True">Sales Opportunities</asp:ListItem>
										<asp:ListItem Value="P">Purchase Opportunities</asp:ListItem>
									</asp:Radiobuttonlist>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top" Width="45%" style="PADDING-Right:10px;PADDING-top:15px;">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="bottom" width="130">
									<table class="TabStyle" borderColor="black" cellSpacing="0" border="0">
										<tr>
											<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp; Leads &nbsp;&nbsp;&nbsp;
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top" align="right">
									<asp:button id="btnLeadsReport" Runat="server" Text="Go" Width="35px" CssClass="button" CommandName="LeadsReport"
										OnCommand="btnGetReportParameters_Click" style="margin-bottom:3px;"></asp:button>&nbsp;</td>
							</tr>
						</table>
						<asp:table id="tblLeads" borderColor="black" runat="server" Width="100%" height="30px" BorderWidth="1px"
							CellPadding="0" CellSpacing="1" GridLines="None">
							<asp:TableRow>
								<asp:TableCell CssClass="normal1">&nbsp;Leads with Leads promoted information</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</asp:TableCell>
				</asp:TableRow>
				
				<asp:TableRow ID="tblReportParameters" Runat="server" Visible="False" VerticalAlign =Top>
				
					<asp:TableCell ColumnSpan="3" CssClass="normal1" VerticalAlign="Top" Width="100%" style="PADDING-Left:10px;PADDING-Right:8px;PADDING-top:10px;">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="bottom" width="250"  CssClass="normal1" >
									
											<iframe name="frmCustomReportInitiator" src="frmCustomReportConfigSaver.aspx" style="visibility:hidden;"
								width="2" height="2" frameborder="0"></iframe> 
						<ie:tabstrip id="tsCustomRptParams" runat="server" BorderWidth="0px" targetID="mpages" TabDefaultStyle="FILTER: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E9ECF7, endColorstr=#628BAE, MakeShadow=true);font-family:arial;font-weight:bold;font-size:8pt;color:black;height:21;text-align:center;BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid;"
							TabHoverStyle="FILTER: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#93A0BF, endColorstr=#546083, MakeShadow=true);color:#ffffff;"
							TabSelectedStyle="FILTER: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#93A0BF, endColorstr=#546083, MakeShadow=true);font-family:arial;font-weight:bold;font-size:8pt;color:#ffffff;height:21;text-align:center; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid;">
							<IE:TAB Text="&nbsp;&nbsp;Report Layout&nbsp;&nbsp;"></IE:TAB>
							<IE:TAB Text="&nbsp;&nbsp;Report Columns&nbsp;&nbsp;"></IE:TAB>
							<IE:TAB Text="&nbsp;&nbsp;Aggeregated Columns&nbsp;&nbsp;"></IE:TAB>
							<IE:TAB Text="&nbsp;&nbsp;Column Sequence&nbsp;&nbsp;"></IE:TAB>
							<IE:TAB Text="&nbsp;&nbsp;Report Criteria&nbsp;&nbsp;"></IE:TAB>
						</ie:tabstrip>
										
								</td>
								<td style="width=100px"></td>
								<td vAlign="top" >  
								<asp:Label  CssClass="normal1" ID="Label1" Runat="Server" Text = "Report Type: " />
								<asp:Label  CssClass="normal1" ID="lblReportType" Runat="Server" />
								</td>
								<td vAlign="top" align="right">
									<asp:Button Runat="server" ID="btnRunReport" CssClass="button" Text="Run Report" > </asp:Button></td>
							</tr>
						</table>
						<IE:MULTIPAGE id="mpages" runat="server" BorderWidth="22px" BorderStyle="None" Width="100%"  VerticalAlign="Top">
							<IE:PAGEVIEW >
								<asp:Table  Runat="server" ID="tblReportLayout" width="100%" BorderWidth="1px" cellspacing="2"
									cellpadding="2" BorderColor="Black">
									<asp:TableRow  VerticalAlign="Top">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="20%" style="PADDING-Left:50px;">
											<asp:RadioButton Runat="server" ID="rbReportType" Checked="True" Text="Tabular Report"></asp:RadioButton>
											<br>
											<img src="../images/tabularReportLayout.gif" hspace="20" width="140" height="100">
										</asp:TableCell>
										<asp:TableCell CssClass="normal1" width="80%" VerticalAlign="Top" style="PADDING-Top:50px;">Tabular reports are the simplest and fastest way to get a listing of your data.</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
							</IE:PAGEVIEW>
							<IE:PAGEVIEW>
								<asp:Table Runat="server" ID="tblReportColumns" width="100%" BorderWidth="1px" cellspacing="2"
									cellpadding="2" BorderColor="Black">
									<asp:TableRow ID="tblFieldsGroupHeaders" Runat="server">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="30%" style="PADDING-Left:50px;">
											<span style="FONT-WEIGHT: bold">Available Fields</span>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">&nbsp;</asp:TableCell>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="65%">
											<span style="FONT-WEIGHT: bold">Selected Fields</span>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow ID="tblFieldsGroup1" Runat="server" Visible="False">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="30%" style="PADDING-Left:50px;">
											<span style="Width: 140px;height: 18px;">Organization Details</span><input type="button" id="btnSelectAllGroup1" Class="button" Value="Select All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup1)"><br>
											<asp:ListBox id="ddlReportColumnGroup1" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											<br>
											<input type="button" id="btnAddOne" Class="button" Value="Add>" style="width:65;" onclick="javascript:moveForOrderingColumns(document.frmCustomReportBuilder.ddlReportColumnGroup1,document.frmCustomReportBuilder.lstReportColumnsOrder);move(document.frmCustomReportBuilder.ddlReportColumnGroup1,document.frmCustomReportBuilder.ddlReportColumnGroup1Sel)">
											<br>
											<br>
											<input type="button" id="btnRemoveOne" Class="button" value="<Remove" style="width:65;"
												onclick="javascript:removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup1Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup1Sel,document.frmCustomReportBuilder.ddlReportColumnGroup1)">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="65%">
											<span style="Width: 140px;height: 18px;">&nbsp;</span><input type="button" id="btnRemoveAllGroup1" Class="button" Value="Remove All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup1Sel);removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup1Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup1Sel,document.frmCustomReportBuilder.ddlReportColumnGroup1)"><br>
											<asp:ListBox id="ddlReportColumnGroup1Sel" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow ID="tblFieldsGroup2" Runat="server" Visible="False">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" style="PADDING-Left:50px;">
											<span style="Width: 140px;height: 18px;">Contact Details</span><input type="button" id="btnSelectAllGroup2" Class="button" Value="Select All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup2)"><br>
											<asp:ListBox id="ddlReportColumnGroup2" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											<br>
											<input type="button" id="btnAddTwo" Class="button" Value="Add>" style="width:65;" onclick="javascript:moveForOrderingColumns(document.frmCustomReportBuilder.ddlReportColumnGroup2,document.frmCustomReportBuilder.lstReportColumnsOrder);move(document.frmCustomReportBuilder.ddlReportColumnGroup2,document.frmCustomReportBuilder.ddlReportColumnGroup2Sel)">
											<br>
											<br>
											<input type="button" id="btnRemoveTwo" Class="button" value="<Remove" style="width:65;"
												onclick="javascript:removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup2Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup2Sel,document.frmCustomReportBuilder.ddlReportColumnGroup2)">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="65%">
											<span style="Width: 140px;height: 18px;">&nbsp;</span><input type="button" id="btnRemoveAllGroup2" Class="button" Value="Remove All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup2Sel);removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup2Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup2Sel,document.frmCustomReportBuilder.ddlReportColumnGroup2)"><br>
											<asp:ListBox id="ddlReportColumnGroup2Sel" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow ID="tblFieldsGroup3" Runat="server" Visible="False">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" style="PADDING-Left:50px;">
											<span style="Width: 140px;height: 18px;">Opportunity Details</span><input type="button" id="btnSelectAllGroup3" Class="button" Value="Select All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup3)"><br>
											<asp:ListBox id="ddlReportColumnGroup3" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											<br>
											<input type="button" id="btnAddThree" Class="button" Value="Add>" style="width:65;" onclick="javascript:moveForOrderingColumns(document.frmCustomReportBuilder.ddlReportColumnGroup3,document.frmCustomReportBuilder.lstReportColumnsOrder);move(document.frmCustomReportBuilder.ddlReportColumnGroup3,document.frmCustomReportBuilder.ddlReportColumnGroup3Sel)">
											<br>
											<br>
											<input type="button" id="btnRemoveThree" Class="button" value="<Remove" style="width:65;"
												onclick="javascript:removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup3Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup3Sel,document.frmCustomReportBuilder.ddlReportColumnGroup3)">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="65%">
											<span style="Width: 140px;height: 18px;">&nbsp;</span><input type="button" id="btnRemoveAllGroup3" Class="button" Value="Remove All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup3Sel);removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup3Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup3Sel,document.frmCustomReportBuilder.ddlReportColumnGroup3)"><br>
											<asp:ListBox id="ddlReportColumnGroup3Sel" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow ID="tblFieldsGroup4" Runat="server" Visible="False">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" style="PADDING-Left:50px;">
											<span style="Width: 140px;height: 18px;">Item Details</span><input type="button" id="btnSelectAllGroup4" Class="button" Value="Select All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup4)"><br>
											<asp:ListBox id="ddlReportColumnGroup4" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											<br>
											<input type="button" id="btnAddFour" Class="button" Value="Add>" style="width:65;" onclick="javascript:moveForOrderingColumns(document.frmCustomReportBuilder.ddlReportColumnGroup4,document.frmCustomReportBuilder.lstReportColumnsOrder);move(document.frmCustomReportBuilder.ddlReportColumnGroup4,document.frmCustomReportBuilder.ddlReportColumnGroup4Sel)">
											<br>
											<br>
											<input type="button" id="btnRemoveFour" Class="button" value="<Remove" style="width:65;"
												onclick="javascript:removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup4Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup4Sel,document.frmCustomReportBuilder.ddlReportColumnGroup4)">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="65%">
											<span style="Width: 140px;height: 18px;">&nbsp;</span><input type="button" id="btnRemoveAllGroup4" Class="button" Value="Remove All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup4Sel);removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup4Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup4Sel,document.frmCustomReportBuilder.ddlReportColumnGroup4)"><br>
											<asp:ListBox id="ddlReportColumnGroup4Sel" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow ID="tblFieldsGroup5" Runat="server" Visible="False">
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" style="PADDING-Left:50px;">
											<span style="Width: 140px;height: 18px;">Lead Details</span><input type="button" id="btnSelectAllGroup5" Class="button" Value="Select All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup5)"><br>
											<asp:ListBox id="ddlReportColumnGroup5" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											<br>
											<input type="button" id="btnAddFive" Class="button" Value="Add>" style="width:65;" onclick="javascript:moveForOrderingColumns(document.frmCustomReportBuilder.ddlReportColumnGroup5,document.frmCustomReportBuilder.lstReportColumnsOrder);move(document.frmCustomReportBuilder.ddlReportColumnGroup5,document.frmCustomReportBuilder.ddlReportColumnGroup5Sel)">
											<br>
											<br>
											<input type="button" id="btnRemoveFive" Class="button" value="<Remove" style="width:65;"
												onclick="javascript:removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup5Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup5Sel,document.frmCustomReportBuilder.ddlReportColumnGroup5)">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="65%">
											<span style="Width: 140px;height: 18px;">&nbsp;</span><input type="button" id="btnRemoveAllGroup5" Class="button" Value="Remove All" style="width:75;MARGIN-BOTTOM: 3px;"
												onclick="javascript:selectAll(document.frmCustomReportBuilder.ddlReportColumnGroup5Sel);removeFromOrderingColumns(document.frmCustomReportBuilder.lstReportColumnsOrder,document.frmCustomReportBuilder.ddlReportColumnGroup5Sel);remove(document.frmCustomReportBuilder.ddlReportColumnGroup5Sel,document.frmCustomReportBuilder.ddlReportColumnGroup5)"><br>
											<asp:ListBox id="ddlReportColumnGroup5Sel" runat="server" CssClass="signup" Width="215" Height="100"
												SelectionMode="Multiple" EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
							</IE:PAGEVIEW>
							<IE:PAGEVIEW>
							<asp:Table  Runat="server" ID="Table1" width="100%" BorderWidth="1px" BorderColor="Black">
									<asp:TableRow  VerticalAlign="Top">
									<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="20%" >
											<asp:datagrid id="dgAggeregateColumns" runat="server" Width="100%" AutoGenerateColumns="False"
												CssClass="dg" AlternatingItemStyle-CssClass="is" ItemStyle-CssClass="ais" HeaderStyle-CssClass="hs"
												HeaderStyle-ForeColor="White" DataKeyField="vcDbFieldName"  BorderWidth="1px" CellPadding="0" CellSpacing="0"
												ShowHeader="True" HeaderStyle-Height="24px" EnableViewState="False">
												<Columns>
													<asp:BoundColumn Visible="False" DataField="vcDbFieldName"></asp:BoundColumn>
													<asp:BoundColumn Visible="True" HeaderText="Columns" DataField="vcScrFieldName" ItemStyle-Width="40%"
														ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
													<asp:BoundColumn Visible="True" HeaderText="Sum" DataField="vcColumnSum" ItemStyle-Width="15%"></asp:BoundColumn>
													<asp:BoundColumn Visible="True" HeaderText="Average" DataField="vcColumnAvg" ItemStyle-Width="15%"></asp:BoundColumn>
													<asp:BoundColumn Visible="True" HeaderText="Largest Value" DataField="vcColumnLValue" ItemStyle-Width="15%"></asp:BoundColumn>
													<asp:BoundColumn Visible="True" HeaderText="Smallest Value" DataField="vcColumnSValue" ItemStyle-Width="15%"></asp:BoundColumn>
												</Columns>
											</asp:datagrid>
									</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
							</IE:PAGEVIEW>
							<IE:PAGEVIEW>
								<asp:Table Runat="server" ID="tblColumnsOrder" width="100%" BorderWidth="1px" cellspacing="2"
									cellpadding="2" BorderColor="Black">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="30%" style="PADDING-Left:50px;">
											<span style="FONT-WEIGHT: bold">Set Order for your Selected Columns</span><br>
											<span style="FONT-STYLE: italic">(Fields at the top will appear on the left and first 100 columns will be displayed only)</span><br>
											<asp:ListBox id="lstReportColumnsOrder" runat="server" CssClass="signup" Width="250" Height="200"
												EnableViewState="False"></asp:ListBox>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											&nbsp;<input type="button" id="btnMoveupTopColumns" Class="button" value="    Top   " onclick="javascript:moveTop(document.frmCustomReportBuilder.lstReportColumnsOrder)">
											<br>
											<br />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img id="btnMoveupColumns" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.frmCustomReportBuilder.lstReportColumnsOrder)"/>
											<br>
											<br>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img id="btnMoveDownColumns" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.frmCustomReportBuilder.lstReportColumnsOrder)"/>
											<br><br>
											&nbsp;<input type="button" id="btnMoveupLastColumns" Class="button" value="Bottom" onclick="javascript:moveBottom(document.frmCustomReportBuilder.lstReportColumnsOrder)">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1">&nbsp;</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
							</IE:PAGEVIEW>
							<IE:PAGEVIEW>
								<asp:Table Runat="server" ID="tblSelectCriteria" width="100%" BorderWidth="1px" cellspacing="2"
									cellpadding="0" BorderColor="Black">
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;PADDING-Right:50px;">
											<span style="FONT-WEIGHT: bold;">Standard Filters</span><br>
											<hr color="black" size="1">
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" style="PADDING-Left:50px;">
											<br>
											<asp:listbox id="ddlStandardFilters" Runat="server" Rows="1" SelectionMode="Single" CssClass="signup"
												EnableViewState="False">
												<asp:ListItem Value="All">All</asp:ListItem>
												<asp:ListItem Value="dtRecordCreatedOn">Created between</asp:ListItem>
												<asp:ListItem Value="dtRecordLastModifiedOn">Last Modified between</asp:ListItem>
											</asp:listbox>
										</asp:TableCell>
										<asp:TableCell HorizontalAlign="Left" VerticalAlign="Bottom" CssClass="normal1">
											<span style="Width: 90px;">From</span>
											<span style="Width: 60px;">To</span><br>
											<BizCalendar:Calendar ID="calFrom" runat="server" />
											&nbsp;
											<BizCalendar:Calendar ID="calTo" runat="server" />
										</asp:TableCell>
										<asp:TableCell CssClass="normal1">
											<span style="Width: 140px;">Views</span><br>
											<input type="button" id="btnChooseTeams" Runat="server" Style="Width:'100px'" runat="server"
												Class="button" Value="Choose Teams" onclick="javascript: OpenSelTeam(32);" NAME="btnChooseTeams">&nbsp; 
											&nbsp;<input type="button" ID="btnChooseTerritories" Runat="server" Style="Width:'100px'" runat="server"
												Class="button" Value="Choose Territories" onclick="javascript: OpenTerritory(32);" NAME="btnChooseTerritories">&nbsp;
												<input id="hdTeams" type="hidden" name="hdTeams" runat="server"> <input id="hdTerritories" type="hidden" name="hdTerritories" runat="server">
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" style="PADDING-Left:50px;">
											<asp:RadioButtonList ID="rdlReportType" AutoPostBack="False" Runat="server" CssClass="normal1" RepeatLayout="Table"
												RepeatDirection="Horizontal" EnableViewState="False">
												<asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
												<asp:ListItem Value="2">Team Selected</asp:ListItem>
												<asp:ListItem Value="3">Territory Selected</asp:ListItem>
											</asp:RadioButtonList>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;PADDING-Right:50px;PADDING-Bottom:0px;">
											<span style="FONT-WEIGHT: bold;">Advanced Filters</span>
											<hr color="black" size="1">
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;PADDING-Top:0px;"><asp:RadioButtonList ID="rbAdvFilterJoiningCondition" AutoPostBack="False" Runat="server" CssClass="normal1" RepeatLayout="Table"
												RepeatDirection="Horizontal" EnableViewState="False">
												<asp:ListItem Value="A" Selected="True">Match all the following</asp:ListItem>
												<asp:ListItem Value="R">Match any of the following</asp:ListItem>
											</asp:RadioButtonList>
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;">
											<span style="Width: 245px;height: 18px;">Field</span><span style="Width: 145px;height: 18px;">Operator</span><span style="Width: 245px;height: 18px;">Value</span><br>
											<asp:listbox id="ddlField1" Runat="server" Rows="1" SelectionMode="Single" style="width:240px;"
												CssClass="signup" EnableViewState="False"></asp:listbox>&nbsp;
											<asp:dropdownlist id="ddlCondition1" Runat="server" CssClass="signup" style="width:140px;" EnableViewState="False">
												<asp:ListItem Value="EQ">Equals</asp:ListItem>
												<asp:ListItem Value="LT">Less than</asp:ListItem>
												<asp:ListItem Value="GT">Greater than</asp:ListItem>
												<asp:ListItem Value="LEQ">Less or Equal</asp:ListItem>
												<asp:ListItem Value="GEQ">Greater or Equal</asp:ListItem>
												<asp:ListItem Value="NEQ">Not Equal to</asp:ListItem>
												<asp:ListItem Value="STW">Starts with</asp:ListItem>
												<asp:ListItem Value="LIKE">Includes</asp:ListItem>
												<asp:ListItem Value="NOT LIKE">Excludes</asp:ListItem>
											</asp:dropdownlist>&nbsp;
											<input id="txtValue1" type="text" runat="server" NAME="txtValue1" class="signup" maxlength="50">
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;">
											<asp:listbox id="ddlField2" Runat="server" Rows="1" SelectionMode="Single" style="width:240px;"
												CssClass="signup" EnableViewState="False"></asp:listbox>&nbsp;
											<asp:dropdownlist id="ddlCondition2" Runat="server" CssClass="signup" style="width:140px;" EnableViewState="False">
												<asp:ListItem Value="EQ">Equals</asp:ListItem>
												<asp:ListItem Value="LT">Less than</asp:ListItem>
												<asp:ListItem Value="GT">Greater than</asp:ListItem>
												<asp:ListItem Value="LEQ">Less or Equal</asp:ListItem>
												<asp:ListItem Value="GEQ">Greater or Equal</asp:ListItem>
												<asp:ListItem Value="NEQ">Not Equal to</asp:ListItem>
												<asp:ListItem Value="STW">Starts with</asp:ListItem>
												<asp:ListItem Value="LIKE">Includes</asp:ListItem>
												<asp:ListItem Value="NOT LIKE">Excludes</asp:ListItem>
											</asp:dropdownlist>&nbsp;
											<input id="txtValue2" type="text" runat="server" NAME="txtValue2" class="signup" maxlength="50">
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;">
											<asp:listbox id="ddlField3" Runat="server" Rows="1" SelectionMode="Single" style="width:240px;"
												CssClass="signup" EnableViewState="False"></asp:listbox>&nbsp;
											<asp:dropdownlist id="ddlCondition3" Runat="server" CssClass="signup" style="width:140px;" EnableViewState="False">
												<asp:ListItem Value="EQ">Equals</asp:ListItem>
												<asp:ListItem Value="LT">Less than</asp:ListItem>
												<asp:ListItem Value="GT">Greater than</asp:ListItem>
												<asp:ListItem Value="LEQ">Less or Equal</asp:ListItem>
												<asp:ListItem Value="GEQ">Greater or Equal</asp:ListItem>
												<asp:ListItem Value="NEQ">Not Equal to</asp:ListItem>
												<asp:ListItem Value="STW">Starts with</asp:ListItem>
												<asp:ListItem Value="LIKE">Includes</asp:ListItem>
												<asp:ListItem Value="NOT LIKE">Excludes</asp:ListItem>
											</asp:dropdownlist>&nbsp;
											<input id="txtValue3" type="text" runat="server" NAME="txtValue3" class="signup" maxlength="50">
										</asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
										<asp:TableCell ColumnSpan="3" CssClass="normal1" style="PADDING-Left:50px;">
											<asp:listbox id="ddlField4" Runat="server" Rows="1" SelectionMode="Single" style="width:240px;"
												CssClass="signup" EnableViewState="False"></asp:listbox>&nbsp;
											<asp:dropdownlist id="ddlCondition4" Runat="server" CssClass="signup" style="width:140px;" EnableViewState="False">
												<asp:ListItem Value="EQ">Equals</asp:ListItem>
												<asp:ListItem Value="LT">Less than</asp:ListItem>
												<asp:ListItem Value="GT">Greater than</asp:ListItem>
												<asp:ListItem Value="LEQ">Less or Equal</asp:ListItem>
												<asp:ListItem Value="GEQ">Greater or Equal</asp:ListItem>
												<asp:ListItem Value="NEQ">Not Equal to</asp:ListItem>
												<asp:ListItem Value="STW">Starts with</asp:ListItem>
												<asp:ListItem Value="LIKE">Includes</asp:ListItem>
												<asp:ListItem Value="NOT LIKE">Excludes</asp:ListItem>
											</asp:dropdownlist>&nbsp;
											<input id="txtValue4" type="text" runat="server" NAME="txtValue4" class="signup" maxlength="50">
										</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
							</IE:PAGEVIEW>
						</IE:MULTIPAGE>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<input type="hidden" id="hdReportType" runat="server"> <input type="hidden" id="hdReportName" runat="server">
			<input type="hidden" id="hdReportDesc" runat="server"> <input type="hidden" id="hdCustomReportConfig" runat="server">
			<asp:Literal ID="litClientSideScript" Runat="Server" Visible="False" EnableViewState="False"></asp:Literal>
			<asp:Button Runat="server" ID="btnAddToMyReports" CssClass="button" Text="Add to My Reports"
				style="VISIBILITY:hidden"></asp:Button>
		<asp:Button ID="btnGo" Runat="server" style="display:none"></asp:Button>
		<%--</ContentTemplate>
			</asp:updatepanel>--%>
		</form>
	</body>
</HTML>
