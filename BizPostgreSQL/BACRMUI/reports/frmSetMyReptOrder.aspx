<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSetMyReptOrder.aspx.vb"
    Inherits="frmSetMyReptOrder" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>My Report Order</title>
    <script language="javascript" src="../javascript/AutoLeadsRoutingRules.js"></script>
    <%--<script language="javascript">
        function DeleteLinks(tbox) {

            for (var i = 0; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    tbox.options[i].value = "";
                    tbox.options[i].text = "";
                    BumpUp(tbox);
                }
            }
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }
        function MoveUp(tbox) {

            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i - 1].value;
                    tbox.options[i].text = tbox.options[i - 1].text;
                    tbox.options[i - 1].value = SelectedValue;
                    tbox.options[i - 1].text = SelectedText;
                }
            }
            return false;
        }
        function MoveDown(tbox) {

            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i + 1].value;
                    tbox.options[i].text = tbox.options[i + 1].text;
                    tbox.options[i + 1].value = SelectedValue;
                    tbox.options[i + 1].text = SelectedText;
                }
            }
            return false;
        }
        function Close() {
            window.close()
            return false;
        }
        function Save() {
            var str = '';
            for (var i = 0; i < document.form1.lstLinks.options.length; i++) {
                var SelectedValue;
                SelectedValue = document.form1.lstLinks.options[i].value;
                str = str + SelectedValue + ','
            }
            document.form1.txthidden.value = str;

        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnSaveOrder" runat="server" CssClass="button" Text="Save Reports Order">
                </asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Set My Reports Order
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table9" BorderWidth="1" Height="400" runat="server" Width="100%" BorderColor="black"
        CssClass="aspTable" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table>
                    <tr>
                        <td class="normal1" align="center" colspan="2">
                            Set Order (Highlight then click up or down to set order)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ListBox ID="lstLinks" runat="server" Width="500" Height="400" CssClass="signup">
                            </asp:ListBox>
                        </td>
                        <td>
                            <img id="btnMoveupOne" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.form1.lstLinks,0)" />
                            <br />
                            <br>
                            <img id="btnMoveDownOne" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.form1.lstLinks,0)" />
                            <br />
                            <br />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Remove ->"></asp:Button>
                            <br />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txthidden" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
