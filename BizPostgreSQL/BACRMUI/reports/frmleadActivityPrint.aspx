<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmleadActivityPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmleadActivityPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Lead Activity </title>
</head>
<body>
    <form id="frmLeadActivityPrint" runat="server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
	        </table>
	<br />
	        <table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						Lead Activity Report
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<tr>
								<td class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</td>
								<td class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></td>
							</tr>
							<tr>
								<td class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</td>
								<td class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></td>
							</tr>
						</table>				
						</td>
			</table>
			<script language="Javascript" type="text/javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			    <tr>
			        <td>
        			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" 
				    BorderColor="black" GridLines="None" Height="350">
				    <asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
				
						<asp:datagrid id="dgLeads" runat="server" AllowSorting="true" CssClass="dg" Width="100%"
							AutoGenerateColumns="False" EnableViewState="true">
						        	<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							        <ItemStyle CssClass="is"></ItemStyle>
							        <HeaderStyle CssClass="hs"></HeaderStyle>
							        <Columns>
								        <asp:BoundColumn Visible="False" DataField="numDivisionID"></asp:BoundColumn>
								        <asp:BoundColumn Visible="False" DataField="numRecOwner" HeaderText="numRecOwner"></asp:BoundColumn>
								        <asp:BoundColumn Visible="False" DataField="numTerId"></asp:BoundColumn>
								        <asp:TemplateColumn HeaderText="<font color=white>Date</font>" SortExpression="bintCreatedDate">
									        <ItemTemplate>
										        <%# ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate")) %>
									        </ItemTemplate>
								        </asp:TemplateColumn>
								        <asp:BoundColumn DataField="vcGrpName" HeaderText="<font color=white>Lead</font>"
									        SortExpression="vcGrpName" ></asp:BoundColumn>
        									
								        <asp:ButtonColumn DataTextField="vcCompanyName" HeaderText="<font color=white>Lead Name</font>"
									        SortExpression="vcCompanyName" CommandName="Company"></asp:ButtonColumn>
								        <asp:BoundColumn DataField="Employees" SortExpression="Employees" HeaderText="<font color=white>Employees</font>"></asp:BoundColumn>
								        <asp:BoundColumn DataField="vcUserName" SortExpression="vcUserName" HeaderText="<font color=white>Record Owner</font>"></asp:BoundColumn>																
								        <asp:BoundColumn DataField="AssignedTo" SortExpression="AssignedTo" HeaderText="<font color=white>Assigned To</font>"></asp:BoundColumn>
								        <asp:BoundColumn DataField="FolStatus" SortExpression="FolStatus" HeaderText="<font color=white>FollowUpStatus</font>"></asp:BoundColumn>
        								
							        </Columns>
						    </asp:datagrid>
						
					   </asp:tableCell>
				       </asp:tableRow>
			          </asp:table>
			        </td>
			    </tr>
			</table>
			 <script language="JavaScript"  type="text/javascript">
                 function Lfprintcheck()
                 {
	                this.print()
	                //history.back()
	                document.location.href = "frmLeadActivity.aspx";
                 }
			</script>
    </form>
</body>
</html>
