Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmSelfServicePortal : Inherits BACRMPage

        Dim objPredefinedReports As New PredefinedReports
        Dim dtPredefinedReports As DataTable
        Dim SortField As String
       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then

                    GetUserRightsForPage(8, 60)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    DisplayRecords()
                    ' = "Reports"
                End If
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(29)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                Dim intPReportsCount As Integer

                Dim trRow As TableRow
                'Declare The DataTable Cell Objects.
                Dim tcColName As TableCell
                Dim tcColValue As TableCell
                Dim strImage As String

                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportType = 29
                dtPredefinedReports = objPredefinedReports.GetRepSelfServicePortal
                Dim dv As DataView = New DataView(dtPredefinedReports)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgPortal.DataSource = dv
                dgPortal.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 37
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgPortal_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPortal.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplNoOfTimes As HyperLink
                    hplNoOfTimes = e.Item.FindControl("hplNoOfTimes")
                    hplNoOfTimes.Attributes.Add("onclick", "return OpenContactList(" & e.Item.Cells(1).Text & ")")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgPortal_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPortal.ItemCommand
            Try
                If e.CommandName = "Company" Then
                    If e.Item.Cells(3).Text = 1 Then
                        Response.Redirect("../Account/frmProspects.aspx?frm=Portal&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(2).Text, False)
                    ElseIf e.Item.Cells(3).Text = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=Portal&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text, False)
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgPortal_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgPortal.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgPortal, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/frmSelfServicePortalPrint.aspx?fdt=" & calFrom.SelectedDate & "&tdt=" & DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)), False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace
