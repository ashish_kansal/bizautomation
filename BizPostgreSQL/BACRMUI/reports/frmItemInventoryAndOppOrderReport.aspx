﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmItemInventoryAndOppOrderReport.aspx.vb" Inherits=".frmItemInventoryAndOppOrderReport" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Item:</label>
                        <telerik:RadComboBox ID="radItem" runat="server" Width="200" DropDownWidth="200px"
                            EnableScreenBoundaryDetection="true" EnableLoadOnDemand="true" AllowCustomText="True"
                            ClientIDMode="Static">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                        </telerik:RadComboBox>
                    </div>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" />
                    <asp:UpdatePanel ID="excelupdate" runat="server" class="form-group">
                        <ContentTemplate>
                            <asp:LinkButton runat="server" ID="ibExportExcel" CssClass="btn btn-success" AlternateText="Export to Excel"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ibExportExcel" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Items Inventory and Total Sales/Purchase Orders Quantity
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-striped">
                    <Columns>
                        <asp:BoundField HeaderText="Item Code" DataField="numItemCode" />
                        <asp:BoundField HeaderText="Item Name" DataField="vcItemName" />
                        <asp:BoundField HeaderText="Item Classification" DataField="vcItemClassification" />
                        <asp:BoundField HeaderText="On Hand" DataField="numOnHand" />
                        <asp:BoundField HeaderText="On Order" DataField="numOnOrder" />
                        <asp:BoundField HeaderText="On Allocation" DataField="numAllocation" />
                        <asp:BoundField HeaderText="On BackOrder" DataField="numBackOrder" />
                        <asp:BoundField HeaderText="Open Purchase Orders" DataField="numQtyOpenPO" />
                        <asp:BoundField HeaderText="Closed Purchase Orders" DataField="numQtyClosedPO" />
                        <asp:BoundField HeaderText="Purchase Return" DataField="numQtyPOReturn" />
                        <asp:BoundField HeaderText="Open Sales Orders" DataField="numQtyOpenSO" />
                        <asp:BoundField HeaderText="Closed Sales Orders" DataField="numQtyClosedSO" />
                        <asp:BoundField HeaderText="Sales Return" DataField="numQtySOReturn" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
           <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
