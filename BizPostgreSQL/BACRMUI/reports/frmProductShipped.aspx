<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProductShipped.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmProductShipped"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Products Shipped</title>
		
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  

		<script language="javascript">
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
			function PopupCheck()
		{
		    document.form1.btnGo.click()
		}
		</script>
	</HEAD>
	<body >
		
		<form id="form1" method="post" runat="server">
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<table width="100%">
				<tr>
					<td class="normal1" noWrap>
					    <table >
				        <td >&nbsp;&nbsp;From</td>
						<td><BizCalendar:Calendar ID="calFrom" runat="server" /></td>
						<td>&nbsp;&nbsp;&nbsp;To</td>
						<td><BizCalendar:Calendar ID="calTo" runat="server" /></td>
						<td  valign="middle">
						&nbsp;&nbsp;&nbsp;<asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>
						</td>
						</table>
						</td>
					<td align="right"><asp:button id="btnAddtoMyRtpList" CssClass="button" Text="Add To My Reports" Runat="server"></asp:button></td>
				</tr>
			</table>
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; Products 
									Shipped/Received&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1">&nbsp; Filter
						<asp:dropdownlist id="ddlFilter" CssClass="signup" Runat="server" AutoPostBack="True">
							<asp:ListItem Value="0">-- Select One --</asp:ListItem>
							<asp:ListItem Value="1">Shipped</asp:ListItem>
							<asp:ListItem Value="2">Received</asp:ListItem>
						</asp:dropdownlist></td>
					<td align="right">&nbsp;
						<asp:button id="btnChooseTerritories" Width="120" CssClass="button" Text="Choose Territories"
							Runat="server"></asp:button>&nbsp;
						<asp:button id="btnExportToExcel" Width="120" CssClass="button" Text="Export to Excel" Runat="server"></asp:button>&nbsp;
						<asp:button id="btnPrint" Width="50" CssClass="button" Text="Print" Runat="server"></asp:button>&nbsp;
						<asp:button id="btnBack" Width="50" CssClass="button" Text="Back" Runat="server"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="Table9" Width="100%" Height="400" Runat="server" GridLines="None" BorderColor="black" CssClass="aspTable"
				CellSpacing="0" CellPadding="0" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:datagrid id="dgProductRec" Width="100%" CssClass="dg" Runat="server" BorderColor="white"
							AllowSorting="True" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numCompanyID" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="numDivisionID" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="ContactID" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="numOppID" Visible="False"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="LastLoggedIn" HeaderText="<font color=white>Date of Event</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "bintShippedDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Event" SortExpression="Event" HeaderText="<font color=white>Event</font>"></asp:BoundColumn>
								<asp:ButtonColumn CommandName="Opp" DataTextField="vcPOppName" SortExpression="vcPOppName" HeaderText="<font color=white>Order/Deal ID</font>"></asp:ButtonColumn>
								<asp:ButtonColumn CommandName="Company" DataTextField="Company" SortExpression="Company" HeaderText="<font color=white>Shipping Company</font>"></asp:ButtonColumn>
								<asp:BoundColumn DataField="vcTrackingURL" SortExpression="vcTrackingURL" HeaderText="<font color=white>Tracking URL</font>"></asp:BoundColumn>
							</Columns>
						</asp:datagrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			<asp:PostBackTrigger ControlID="btnPrint" />
			</Triggers>
			</asp:updatepanel>
			</form>
	</body>
</HTML>
