<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="ActivityReportAction.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.ActivityReportAction" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>ActivityReport</title>
    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        //		function fnSortByChar(varSortChar)
        //			{
        //				document.Form1.txtSortChar.value = varSortChar;
        //				document.Form1.submit();
        //			}
        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Action Items
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dgAction" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
        AllowSorting="True">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:TemplateColumn HeaderText="Due Date" SortExpression="dtStartTime">
                <ItemTemplate>
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Task" SortExpression="Task" HeaderText="Type"></asp:BoundColumn>
            <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="Phone - Ext.">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vcCompanyName" SortExpression="vcCompanyName" HeaderText="Customer">
            </asp:BoundColumn>
            <asp:HyperLinkColumn Target="_blank" SortExpression="vcEmail" DataNavigateUrlField="vcEmail"
                DataTextField="vcEmail" HeaderText="Email"></asp:HyperLinkColumn>
        </Columns>
    </asp:DataGrid>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
