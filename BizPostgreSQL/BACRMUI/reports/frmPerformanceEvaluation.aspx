﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmPerformanceEvaluation.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmPerformanceEvaluation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-header > .box-tools {
            position: absolute;
            right: 0px;
            top: 3px;
        }

        .manage-wip-date-range li {
            background-color: #dae3f3;
            color: #000;
            font-weight: bold;
        }

            .manage-wip-date-range li a {
                padding: 7px 15px;
                border-top: 0px;
            }
    </style>
    <script type="text/javascript">
        var domainID = "<%# Session("DomainID")%>";
        var currency = "<%= Session("Currency") %>";

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        $(document).ready(function () {
            $.when(BindTeams(), BindEmpoyees(), BindProcess()).then(function () {
                BindReportData();
            });

            $("#ddlProcess").change(function () {
                $("#ddlMilestone").find('option').not(':first').remove();
                $("#ddlStage").find('option').not(':first').remove();
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindMilestone()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlMilestone").change(function () {
                $("#ddlStage").find('option').not(':first').remove();
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindStages()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlStage").change(function () {
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindTasks()).then(function () {
                        BindReportData();
                    });
                }
            });
        });

        function ProcessTypeChanged() {
            ClearControls();

            $.when(BindProcess()).then(function () {
                BindReportData();
            });
        }

        function ClearControls() {
            $("#ddlProcess").find('option').not(':first').remove();
            $("#ddlMilestone").find('option').not(':first').remove();
            $("#ddlStage").find('option').not(':first').remove();
            $("#btnDropDownTask").html("All" + " <span class='caret'></span>");
            $("#ulTaskOptions").html("");
            $("#ulTeamOptions input[type='checkbox']").each(function () {
                $(this).closest("li").show()
                $(this).prop("checked", "");
            });
            $("#btnDropDownTeam").html("All" + " <span class='caret'></span>");
            $("#ulResourceOptions input[type='checkbox']").each(function () {
                $(this).closest("li").show()
                $(this).prop("checked", "");
            });
            $("#btnDropDownResource").html("All" + " <span class='caret'></span>");
            $("#divMain").html("");
        }

        function BindProcess() {
            $("#ddlProcess").find('option').not(':first').remove();

            return $.ajax({
                type: "POST",
                url: '../admin/frmAdminBusinessProcess.aspx/WebMethodGetProcessList',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": domainID
                    , "Mode": $("input[name='ProcessType']:checked").val()
                }),
                success: function (data) {
                    try {
                        var Jresponse = $.parseJSON(data.d);

                        $.each(Jresponse, function (index, value) {
                            $("#ddlProcess").append("<option value=" + value.Slp_Id + ">" + value.Slp_Name + "</option>");
                        });

                        if ($("#ddlProcess option").length > 1) {
                            $('#ddlProcess option:eq(1)').attr('selected', 'selected');
                            BindMilestone();
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading process.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading process: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading process");
                    }
                }
            });
        }

        function BindMilestone() {
            try {
                $("#ddlMilestone").find('option').not(':first').remove();

                if (parseInt($("#ddlProcess").val()) > 0) {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsService.svc/GetMilestonesByProcess',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "processID": parseInt($("#ddlProcess").val())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetMilestonesByProcessResult);
                                if (obj != null && obj.length > 0) {
                                    $.each(obj, function (index, value) {
                                        $("#ddlMilestone").append("<option value=" + (index + 1) + ">" + value.vcMileStoneName + "</option>");
                                    });
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading milestones.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading milestones.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading milestones.");
            }
        }

        function BindStages() {
            try {
                $("#ddlStage").find('option').not(':first').remove();

                if (parseInt($("#ddlMilestone").val()) > 0) {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsService.svc/GetStagesByProcess',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "processID": parseInt($("#ddlProcess").val())
                            , "milestoneName": replaceNull($("#ddlMilestone option:selected").text())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetStagesByProcessResult);
                                if (obj != null && obj.length > 0) {
                                    $.each(obj, function (index, value) {
                                        $("#ddlStage").append("<option value=" + value.numStageDetailsId + ">" + value.vcStageName + "</option>");
                                    });
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading milestones.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading milestones.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading milestones.");
            }
        }

        function BindTasks() {
            try {
                $("#ulTaskOptions").html("");

                if (parseInt($("#ddlStage").val()) > 0) {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsTaskService.svc/GetTasksByStage',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "stageDetailsID": parseInt($("#ddlStage").val())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetTasksByStageResult);
                                if (obj != null && obj.length > 0) {
                                    $.each(obj, function (index, value) {
                                        $("#ulTaskOptions").append("<li><input type='checkbox' onchange='TaskSelectionChanged();' id='chkTask" + value.numTaskId.toString() + "' /><label>" + replaceNull(value.vcTaskName) + "</label></li>");
                                    });
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading tasks.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading tasks.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading tasks.");
            }
        }

        function TaskSelectionChanged() {
            try {
                var selected = 0;

                $("#ulTaskOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selected++;
                    }
                });

                $("#btnDropDownTask").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                $("#divMain").html("");
                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetSelectedTaskValues() {
            try {
                var selectedValues = "";

                $("#ulTaskOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkTask", "");
                    }
                });

                return selectedValues;
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function BindTeams() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetListDetails',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "listID": 35
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetListDetailsResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ulTeamOptions").append("<li><input type='checkbox' onchange='TeamSelectionChanged();' id='chkTeam" + value.numListItemID.toString() + "' /><label>" + replaceNull(value.vcData) + "</label></li>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading teams.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading teams: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading teams");
                    }
                }
            });
        }

        function TeamSelectionChanged() {
            try {
                var selected = 0;
                var arrTeams = [];

                $("#ulTeamOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        arrTeams.push($(this).attr("id").replace("chkTeam", ""));
                        selected++;
                    }
                });

                $("#btnDropDownTeam").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                $("#btnDropDownEmployee").html("All" + " <span class='caret'></span>");
                $("#ulEmployeeOptions input[type='checkbox']").each(function () {
                    $(this).is(":checked").prop("checked", "false");

                    if (jQuery.inArray($(this).closest("li").attr("team"), arrTeams) === -1) {
                        $(this).closest("li").hide();
                    } else {
                        $(this).closest("li").show();
                    }
                });

                $("#divMain").html("");
                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetSelectedTeamValues() {
            try {
                var selectedValues = "";

                $("#ulTeamOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkTeam", "");
                    }
                });

                return selectedValues;
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function BindEmpoyees() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetEmployees',
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetEmployeesResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ulResourceOptions").append("<li team='" + value.numTeam.toString() + "'><input type='checkbox' onchange='EmployeeSelectionChanged();' id='chkEmployee" + value.numContactID.toString() + "' /><label>" + replaceNull(value.vcUserName) + "</label></li>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading resource.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading resource: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading resource");
                    }
                }
            });
        }

        function EmployeeSelectionChanged() {
            try {
                var selected = 0;

                $("#ulEmployeeOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selected++;
                    }
                });

                $("#btnDropDownEmployee").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                $("#divMain").html("");
                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetSelectedEmployeeValues() {
            try {
                var selectedValues = "";

                $("#ulEmployeeOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkEmployee", "");
                    }
                });

                return selectedValues;
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function BindReportData() {
            try {
                var rdpFromDate = $find("<%= rdpFromDate.ClientID%>");
                var rdpToDate = $find("<%= rdpToDate.ClientID%>");

                if (rdpFromDate.get_selectedDate() != null && rdpToDate.get_selectedDate() != null) {
                    if (parseInt($("#ddlProcess").val()) > 0) {
                        var view = parseInt($("#hdnDateRange").val());

                        var fromDateSelected = new Date(rdpFromDate.get_selectedDate());
                        var toDateSelected = new Date(rdpToDate.get_selectedDate());

                        var fromDate = new Date(fromDateSelected.getFullYear(), fromDateSelected.getMonth(), fromDateSelected.getDate(), 0, 0, 0, 0);
                        var toDate = new Date(toDateSelected.getFullYear(), toDateSelected.getMonth(), toDateSelected.getDate(), 23, 59, 59, 999);

                        $.ajax({
                            type: "POST",
                            url: '../WebServices/StagePercentageDetailsService.svc/GetPerformanceEvaluationReportData',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "processType": parseInt($("input[name='ProcessType']:checked").val())
                                , "processID": parseInt($("#ddlProcess").val())
                                , "milestoneName": (parseInt($("#ddlMilestone").val()) > 0 ? replaceNull($("#ddlMilestone option:selected").text()) : "")
                                , "stageDetailsID": parseInt($("#ddlStage").val())
                                , "taskIds": GetSelectedTaskValues()
                                , "teamIds": GetSelectedTeamValues()
                                , "employeeIds": GetSelectedEmployeeValues()
                                , "fromDate": "\/Date(" + fromDate.getTime() + ")\/"
                                , "toDate": "\/Date(" + toDate.getTime() + ")\/"                                
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.GetPerformanceEvaluationReportDataResult);
                                    DisplayReportData(obj)
                                } catch (err) {
                                    alert("Unknown error occurred while loading report data.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred while loading report data: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred while loading report data.");
                                }
                            }
                        });
                    } else {
                        alert("Select process.");
                    }
                } else {
                    alert("Select from and to date.");
                }
            } catch (e) {
                alert("Unknown error ocurred while loading report data.");
            }
        }

        function DisplayReportData(obj) {
            try {
                var milestones = $.parseJSON(obj.Milestones);
                var stages = $.parseJSON(obj.Stages);
                var tasks = $.parseJSON(obj.Tasks);
                var reportData = $.parseJSON(obj.ReportData);
                var employees = $.parseJSON(obj.Employees);
                var totalWorkOrders = (obj.TotalWorkOrders || 0)

                var html = "";
                html += "<table class='table table-bordered'>";
                html += "<thead>";
                html += "<tr>";
                html += "<th>Resource</th>";
                html += "<th>Team</th>";
                html += "<th style='width:122px'>On-Time or Early</th>";
                html += "<th style='width:120px'>Budget Impact</th>";
                html += "<th style='width:90px'>Availability</th>";
                html += "<th style='width:90px'>Productivity</th>";
                html += "<th style='width:120px'>Units</th>";
                html += "<th style='width:120px'>Labor per unit</th>";
                html += "<th style='width:140px'>Build Cycles / WOs</th>";
                html += "</tr>";
                html += "</thead>";
                html += "<tbody>";
                html += "<tr class='processParent'>";
                html += "<td colspan='2' style='font-weight:bold;padding:3px;'>" + $("#ddlProcess option:selected").text(); + "</td>";
                html += "<td class='text-center'><span class='lblOnTime'></span></td>";
                html += "<td class='text-right'><span class='lblBudgetImpact'></span></td>";
                html += "<td><span class='lblAvailability'></span></td>";
                html += "<td><span class='lblProductivity'></span></td>";
                html += "<td><span class='lblUnits'></span></td>";
                html += "<td class='text-right'><span class='lblLabour'></span></td>";
                html += "<td>" + totalWorkOrders + "</td>";
                html += "</tr>";
                if (milestones != null && milestones.length > 0) {
                    html += LoadMilestones(milestones, stages, tasks, reportData, employees);
                }
                html += "</tbody>";
                html += "</table>";

                $("#divMain").html(html);

                var arrProcess = ["Process"];
                UpdateTotals(arrProcess, "processChild");

                if (milestones != null && milestones.length > 0) {
                    UpdateTotals(milestones, "milestoneChild");
                }

                if (stages != null && stages.length > 0) {
                    UpdateTotals(stages, "stageChild");
                }

                if (tasks != null && tasks.length > 0) {
                    UpdateTotals(tasks, "taskChild");
                }
            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function LoadMilestones(milestones, stages, tasks, reportData, employees) {
            var html = "";
            var colspan = 9;

            $.each(milestones, function (index, e) {
                html += "<tr class='milestoneParent" + index + "'><td style='font-weight:bold;padding:3px;' colspan='2'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"milestoneChild" + index + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a>  " + replaceNull(e.vcMileStoneName) + "</td>";
                html += "<td class='text-center'><span class='lblOnTime'></span></td>";
                html += "<td class='text-right'><span class='lblBudgetImpact'></span></td>";
                html += "<td><span class='lblAvailability'></span></td>";
                html += "<td><span class='lblProductivity'></span></td>";
                html += "<td><span class='lblUnits'></span></td>";
                html += "<td class='text-right'><span class='lblLabour'></span></td>";
                html += "<td></td>";
                html += "</tr>";
                html += LoadStages(index, replaceNull(e.vcMileStoneName), stages, tasks, reportData, employees);
            });

            return html;
        }

        function LoadStages(milestoneIndex, milestone, stages, tasks, reportData, employees) {
            var html = "";
            var colspan = 9;

            if (stages != null && stages.length > 0) {
                $.each(stages, function (index, e) {
                    if (e.vcMileStoneName === milestone) {
                        html += "<tr class='milestoneChild" + milestoneIndex + " stageParent" + e.numStageDetailsId + "'><td style='padding-left:20px;font-weight:bold;padding-top:3px;padding-bottom:3px;' colspan='2'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"stageChild" + index + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a>  " + replaceNull(e.vcStageName) + "</td>";
                        html += "<td class='text-center'><span class='lblOnTime'></span></td>";
                        html += "<td class='text-right'><span class='lblBudgetImpact'></span></td>";
                        html += "<td><span class='lblAvailability'></span></td>";
                        html += "<td><span class='lblProductivity'></span></td>";
                        html += "<td><span class='lblUnits'></span></td>";
                        html += "<td class='text-right'><span class='lblLabour'></span></td>";
                        html += "<td></td>";
                        html += "</tr>";
                        html += LoadTasks(milestoneIndex, index, (e.numStageDetailsId || 0), tasks, reportData, employees);
                    }
                });
            }

            return html;
        }

        function LoadTasks(milestoneIndex, stageIndex, stageDetailsId, tasks, reportData, employees) {
            var html = "";

            if (tasks != null && tasks.length > 0) {
                $.each(tasks, function (index, e) {
                    if (e.numStageDetailsId === stageDetailsId) {
                        html += "<tr class='milestoneChild" + milestoneIndex + " stageChild" + stageIndex + " taskParent" + e.numTaskID + "'><td colspan='2' style='padding-left:40px;font-weight:bold;padding-top:3px;padding-bottom:3px;'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"taskChild" + e.numTaskID + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a> " + replaceNull(e.vcTaskName) + "</td>";
                        html += "<td class='text-center'><span class='lblOnTime'></span></td>";
                        html += "<td class='text-right'><span class='lblBudgetImpact'></span></td>";
                        html += "<td><span class='lblAvailability'></span></td>";
                        html += "<td><span class='lblProductivity'></span></td>";
                        html += "<td><span class='lblUnits'></span></td>";
                        html += "<td class='text-right'><span class='lblLabour'></span></td>";
                        html += "<td></td>";
                        html += "</tr>";

                        if (reportData != null && reportData.length > 0) {
                            var obj = reportData.filter(function (result) {
                                return result.numTaskID === e.numTaskID;
                            });

                            if (obj.length > 0) {
                                obj.forEach(function (o) {
                                    html += "<tr class='processChild milestoneChild" + milestoneIndex + " stageChild" + e.numStageDetailsId + " taskChild" + o.numTaskID + "' style='border-radius: 2px; background: #f4f4f4; margin-bottom: 2px; border-left: 2px solid #e6e7e8;font-weight: 600;'>";
                                    html += "<td class='text-right' style='padding-left:60px;'>" + replaceNull(o.vcEmployee) + "</td>";
                                    html += "<td>" + replaceNull(o.vcTeam) + "</td>";

                                    var objEmployee = employees.filter(function (result) {
                                        return result.numContactId === o.numAssignedTo;
                                    });

                                    var classOnTime = "";
                                    var classBudget = "bg-green";

                                    if((o.numOnTimeOrEarly || 0) <= 50) {
                                        classOnTime = "bg-red";
                                    } else if ((o.numOnTimeOrEarly || 0) > 50 && (o.numOnTimeOrEarly || 0) <= 75) {
                                        classOnTime = "bg-yellow";
                                    } else {
                                        classOnTime = "bg-green";
                                    }

                                    if ((o.monBudgetImpact || 0) > 0) {
                                        classBudget = "bg-red";
                                    } 

                                    html += "<td class='text-center'><span style='font-size: 14px;' class='badge " + classOnTime + " lblEmpOnTime'>" + (o.numOnTimeOrEarly || 0) + "%</span></td>";
                                    html += "<td class='text-right'><span style='font-size: 14px;' class='badge " + classBudget + " lblEmpBudgetImpact'>" + currency + " " + (o.monBudgetImpact || 0) + "</span></td>";
                                    html += "<td><span class='lblEmpAvailability'>" + ((objEmployee != null && objEmployee.length > 0) ? (objEmployee[0].numAvailabilityPercent || 0) : 0) + "%</span></td>";
                                    html += "<td><span class='lblEmpProductivity'>" + ((objEmployee != null && objEmployee.length > 0) ? (objEmployee[0].numProductivityPercent || 0) : 0) + "%</span></td>";
                                    html += "<td><span class='lblEmpUnits'>" + (o.fltUnits || 0) + "</span></td>";
                                    html += "<td class='text-right'><span class='lblEmpLabour'>" + currency + " " + (o.monLabourPerUnit || 0) + "</span></td>";
                                    html += "<td></td>";
                                    html += "</tr>";
                                });
                            }
                        }
                    }
                });
            }

            return html;
        }

        function dateSelected(sender, eventArgs) {
            BindReportData();
        }

        function ExpandCollapse(cssClass, a) {
            if ($(a).html() === "<i class=\"fa fa-chevron-up\"></i>") {
                $(a).html("<i class=\"fa fa-chevron-right\"></i>");
                $("tr." + cssClass).hide();
            } else {
                $(a).html("<i class=\"fa fa-chevron-up\"></i>");
                $("tr." + cssClass).each(function () {
                    if ($(this).is('[class*=" stageParent"]')) {
                        $(this).find("a.aExpandCollapse").html("<i class=\"fa fa-chevron-up\"></i>");
                    }
                });
                $("tr." + cssClass).show();
            }
        }

        function UpdateTotals(arrItems,cssClass) {
            try {
                $.each(arrItems, function (index, e) {
                    var totalOnTime = 0.0;
                    var onTimeCount = 0;

                    var totalBudgetImpact = 0.0;
                    var budgetImpactCount = 0;

                    var totalAvailability = 0.0;
                    var availabilityCount = 0;

                    var totalProductivity = 0.0;
                    var productivityCount = 0;

                    var totalUnits = 0.0;
                    var unitsCount = 0;

                    var totalLabourCost = 0.0;
                    var labourCostCount = 0;
                    
                    var trCss = "";
                    if (cssClass === "processChild") {
                        trCss = cssClass;
                    } else if (cssClass === "milestoneChild") {
                        trCss = cssClass + index;
                    } else if (cssClass === "stageChild") {
                        trCss = cssClass + e.numStageDetailsId;
                    } else if (cssClass === "taskChild") {
                        trCss = cssClass + e.numTaskID;
                    }

                    $("#divMain table tr." + trCss).each(function () {
                        if ($(this).find("span.lblEmpOnTime").length > 0) {
                            totalOnTime += parseInt($(this).find("span.lblEmpOnTime").text().replace("%", ""));
                            onTimeCount += 1;
                        }

                        if ($(this).find("span.lblEmpBudgetImpact").length > 0) {
                            totalBudgetImpact += parseFloat($(this).find("span.lblEmpBudgetImpact").text().replace(currency, ""));
                            budgetImpactCount += 1;
                        }

                        if ($(this).find("span.lblEmpAvailability").length > 0) {
                            totalAvailability += parseInt($(this).find("span.lblEmpAvailability").text().replace("%", ""));
                            availabilityCount += 1;
                        }

                        if ($(this).find("span.lblEmpProductivity").length > 0) {
                            totalProductivity += parseInt($(this).find("span.lblEmpProductivity").text().replace("%", ""));
                            productivityCount += 1;
                        }

                        if ($(this).find("span.lblEmpUnits").length > 0) {
                            totalUnits += parseFloat($(this).find("span.lblEmpUnits").text());
                            unitsCount += 1;
                        }

                        if ($(this).find("span.lblEmpLabour").length > 0) {
                            totalLabourCost += parseFloat($(this).find("span.lblEmpLabour").text().replace(currency,""));
                            labourCostCount += 1;
                        }
                    });

                    if (onTimeCount > 0) {
                        var avgOnTime = parseInt(totalOnTime / onTimeCount);

                        if (avgOnTime <= 50) {
                            $("#divMain table tr." + trCss.replace("Child","Parent")).find("span.lblOnTime").addClass("badge bg-red");
                        } else if (avgOnTime > 50 && avgOnTime <= 75) {
                            $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblOnTime").addClass("badge bg-yellow");
                        } else {
                            $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblOnTime").addClass("badge bg-green");
                        }

                        $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblOnTime").text(avgOnTime + "%");
                    }

                    if (budgetImpactCount > 0) {
                        var avgBudgetImpact = parseInt(totalBudgetImpact / budgetImpactCount);

                        if (avgBudgetImpact <= 0) {
                            $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblBudgetImpact").addClass("badge bg-green");
                        } else {
                            $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblBudgetImpact").addClass("badge bg-red");
                        }

                        $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblBudgetImpact").text(currency + " " + avgBudgetImpact);
                    }

                    if (availabilityCount > 0) {
                        var avgAvailability = parseInt(totalAvailability / availabilityCount);
                        $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblAvailability").text(avgAvailability + "%");
                    }

                    if (productivityCount > 0) {
                        var avgProductivity = parseInt(totalProductivity / productivityCount);
                        $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblProductivity").text(avgProductivity + "%");
                    }

                    if (unitsCount > 0) {
                        var avgUnits = parseInt(totalUnits / unitsCount);
                        $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblUnits").text(avgUnits);
                    }

                    if (labourCostCount > 0) {
                        var avgLabour = parseInt(totalLabourCost / labourCostCount);
                        $("#divMain table tr." + trCss.replace("Child", "Parent")).find("span.lblLabour").text(currency + " " + avgLabour);
                    }
                });
            } catch (e) {
                alert("Unknown error occurred while updating totals.");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-inline">
                    <li>
                        <input type="radio" name="ProcessType" value="3" checked="checked" onchange="ProcessTypeChanged();" />
                        <b>Build</b>
                        <input type="radio" name="ProcessType" value="1" onchange="ProcessTypeChanged();" />
                        <b>Project</b>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Process</label>
                            <select class="form-control" id="ddlProcess">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Milestone</label>
                            <select class="form-control" id="ddlMilestone">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Stage</label>
                            <select class="form-control" id="ddlStage">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Task</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownTask" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All
                              <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulTaskOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Team</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownTeam" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulTeamOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Resource</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownResource" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulResourceOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Performance Evaluation (Process & Resource Efficiency)&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <div class="form-inline">
        <div class="form-group">
            <label>From</label>
            <telerik:RadDatePicker ID="rdpFromDate" runat="server" DateInput-CssClass="form-control" Width="100" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png">
                <ClientEvents OnDateSelected="dateSelected" />
            </telerik:RadDatePicker>
        </div>
        <div class="form-group">
            <label>To</label>
            <telerik:RadDatePicker ID="rdpToDate" runat="server" DateInput-CssClass="form-control" Width="100" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png">
                <ClientEvents OnDateSelected="dateSelected" />
            </telerik:RadDatePicker>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" id="divMain">
        </div>
    </div>
</asp:Content>
