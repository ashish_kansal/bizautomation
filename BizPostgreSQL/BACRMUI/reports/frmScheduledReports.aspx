﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmScheduledReports.aspx.vb" Inherits=".frmScheduledReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function AddScheduleReport(id) {
            var h = screen.height;
            var w = screen.width;

            window.open('../reports/frmManageScheduledReport.aspx?srgID=' + id, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function ExpandCollapse(id, a) {
            if ($("#tblSRG tr.tr" + id).is(":visible")) {
                $("#tblSRG tr.tr" + id).hide();
                $(a).html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
            } else {
                $("#tblSRG tr.tr" + id).show();
                $(a).html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');
            }
        }

        function ConfirmDelete() {
            var selectedReports = "";

            $("#tblSRG tr").each(function () {
                if ($(this).find("input.chkSelect").length > 0 && $(this).find("input.chkSelect").is(":checked")) {
                    selectedReports = selectedReports + (selectedReports.length > 0 ? "," : "") + parseInt($(this).find("[id$=hdnID]").val());
                }
            });

            if (selectedReports > 0) {
                if (confirm("Are you sure you want to delete selected report groups?")) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/DeleteSRG',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "srgID": selectedReports
                        }),
                        success: function (data) {
                            document.location.href = document.location.href;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                }
            } else {
                alert("Select at least one record.");
            }

            return false;
        }

        function ShowEmailTemplate(docID) {
            try {
                $("#divETSubject").html("");
                $("#divETBody").html("");

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetGenericDocument',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "docID": docID
                    }),
                    success: function (data) {
                        var obj = $.parseJSON(data.GetGenericDocumentResult);

                        if (obj != null) {
                            $("#divETSubject").html(obj[0].vcSubject || "");
                            $("#divETBody").html(obj[0].vcDocdesc || "");
                        } 

                        $("#modalETPreview").modal("show");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            } catch (e) {
                alert("Unknown error occurred.")
            }
        }

        function OpenSearchResult(a, b) {
            if (b == 15) {
                window.open('../admin/frmItemSearchRes.aspx?SearchID=' + a, '_blank');
            }
            if (b == 1) {
                window.open('../admin/frmAdvancedSearchRes.aspx?SearchID=' + a, '_blank');
            }
            if (b == 29) {
                window.open('../admin/frmAdvSerInvItemsRes.aspx?SearchID=' + a, '_blank');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add New Report Group" OnClientClick="return AddScheduleReport(0);" />
                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return ConfirmDelete();" Text="Delete" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Scheduled Reports & Saved Searches&nbsp;<a href="#" onclick="return OpenHelpPopUp('reports/frmscheduledreports.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Repeater ID="rptScheduledReports" runat="server" OnItemDataBound="rptScheduledReports_ItemDataBound">
                    <HeaderTemplate>
                        <table class="table table-bordered table-striped" id="tblSRG">
                            <tr>
                                <th style="width: 30px"></th>
                                <th>Report Group Name</th>
                                <th>Schedule</th>
                                <th>Email Template</th>
                                <th>Recipients</th>
                                <th style="width: 30px"></th>
                                <th style="width: 30px"></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <a href="#" onclick='<%# "ExpandCollapse(" & Eval("ID") & ",this)"%>'><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                            </td>
                            <td><a href='<%# "javascript:AddScheduleReport(" & Eval("ID") & ");" %>'><%# Eval("vcName")%></a></td>
                            <td><%# Eval("vcSchedule")%></td>
                            <td><a href='<%# "javascript:ShowEmailTemplate(" & Eval("numEmailTemplate") & ")"%>'><%# Eval("vcEmailTemplate")%></a></td>
                            <td><%# Eval("vcRecipients")%></td>
                            <td></td>
                            <td>
                                <input type="checkbox" class="chkSelect" />
                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ID")%>' />
                            </td>
                        </tr>
                        <tr class='<%# "tr" & Eval("ID")%>'>
                            <td colspan="7">
                                <asp:Repeater ID="rptReports" runat="server">
                                    <HeaderTemplate>
                                        <table class="table" style="margin-bottom: 0px;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("vcReportName")%></td>
                                            <td><b>Type:</b> <%# Eval("vcReportType")%></td>
                                            <td><b>Module:</b> <%# Eval("vcModuleName")%></td>
                                            <td><b>Perspective:</b> <%# Eval("vcPerspective")%></td>
                                            <td><b>Timeline:</b> <%# Eval("vcTimeline")%></td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="modalETPreview" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Email Template</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Subject</label>
                        <div id="divETSubject">

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <div id="divETBody">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
