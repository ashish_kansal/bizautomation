
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProjectStatusPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmProjectStatusPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmProjectStatusPrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" CssClass="print_head" Runat="server">Project Status Report</asp:Label>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:DataGrid ID="dgProjectStatus" CssClass="dg" Width="100%" CellPadding="0" CellSpacing="0"
				Runat="server" AutoGenerateColumns="false" AllowSorting="True">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="DueDate" SortExpression="DueDate" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Due Date</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="ProjectName" ItemStyle-HorizontalAlign="Left" SortExpression="ProjectName"
						HeaderText="<font color=white>Project Name</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastMileStoneCompleted" DataFormatString="{0:f}" SortExpression="LastMileStoneCompleted"
						HeaderText="<font color=white>Last Milestone Completed (%)</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastStageCompleted" SortExpression="LastStageCompleted" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>Last Stage Completed</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<script language="JavaScript">
				function Lfprintcheck()
				{
					this.print()
					//history.back()
					document.location.href = "frmProjectStatus.aspx";
				}
			</script>
		</form>
	</body>
</HTML>
