﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Public Class CommissionReports
    Inherits BACRMPage
    Dim bitCommContact As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            bitCommContact = GetQueryStringVal( "CommContact")

            If Not IsPostBack Then

                hfCommAppliesTo.Value = Session("tintComAppliesTo")

                If bitCommContact Then
                    Dim objUser As New UserAccess
                    objUser.DomainID = Session("DomainID")

                    Dim dt As DataTable
                    dt = objUser.GetCommissionsContacts

                    ddlOrganization.DataSource = dt
                    ddlOrganization.DataTextField = "vcCompanyName"
                    ddlOrganization.DataValueField = "numDivisionID"
                    ddlOrganization.DataBind()
                    tdOrganization.Visible = True
                Else
                    gvCommissionsReports.Columns(0).Visible = False
                End If

                Select Case Session("tintComAppliesTo")
                    Case 1
                        lblName.Text = "Item"
                        radItem.Visible = True
                    Case 2
                        lblName.Text = "Item Classification"
                        ddlItemClassification.Visible = True
                        Dim objCommon As CCommon
                        If objCommon Is Nothing Then objCommon = New CCommon
                        objCommon.sb_FillComboFromDBwithSel(ddlItemClassification, 36, Session("DomainID")) ''tem Classification
                    Case 3
                        lblName.Text = "All Items"
                    Case Else
                        ddlReportOwner.Visible = False
                        btnAdd.Visible = False
                End Select

                bingGrid()
            End If
            btnAdd.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub bingGrid()
        Dim objReport As New CommissionRule
        objReport.UserCntId = Session("UserContactID")
        objReport.DomainID = Session("DomainID")
        objReport.bitCommContact = bitCommContact

        Dim ds As DataSet = objReport.GetCommissionReports

        If ds.Tables.Count > 0 Then
            gvCommissionsReports.DataSource = ds.Tables(0)
            gvCommissionsReports.DataBind()
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objReport As New CommissionRule

            Select Case Session("tintComAppliesTo")
                Case 1
                    objReport.ItemID = radItem.SelectedValue
                Case 2
                    objReport.ItemID = ddlItemClassification.SelectedValue
                Case 3
                    objReport.ItemID = 0
            End Select

            If bitCommContact Then
                objReport.UserCntId = ddlOrganization.SelectedValue
            Else
                objReport.UserCntId = Session("UserContactID")
            End If

            objReport.AssignTo = ddlReportOwner.SelectedValue
            objReport.bitCommContact = bitCommContact
            objReport.DomainID = Session("DomainID")
            objReport.ManageCommissionReports()

            bingGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvCommissionsReports_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCommissionsReports.RowCommand
        Try
            If e.CommandName = "DeleteReport" Then
                Dim objReport As New CommissionRule
                objReport.numComReports = e.CommandArgument
                objReport.DeleteCommissionReports()

                bingGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class