Imports BACRM.BusinessLogic.Reports
Namespace BACRM.UserInterface.Reports
    Partial Public Class frmCustomReportWizard
        Inherits BACRMPage
        Dim CustReportId As Long
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
             
                ' btnNext2.Attributes.Add("onclick", "return BindSortOrderList")
                btnNext2.Attributes.Add("onclick", "return BindSortOrderList();")
                btnNext3.Attributes.Add("onclick", "return BindFilterList();")
                btnAddFilterRow.Attributes.Add("onclick", "return AddFilterRow();")
                btnNext4.Attributes.Add("onclick", "return Summation();")
                btnAddSummationRow.Attributes.Add("onclick", "return AddSummationRow();")
                btnReport.Attributes.Add("onclick", "return Report();")
                btnSave.Attributes.Add("onclick", "return Save();")

                If Not IsPostBack Then
                    LoadDropDowns()
                    CustReportId = Request.QueryString("ReportId")
                    txtReportId.Text = CustReportId
                    Dim objReports As New CustomReports
                    Dim dtTable As DataTable
                    objReports.ReportID = CustReportId
                    objReports.DomainID = Session("DomainId")
                    dtTable = objReports.getReportDetails()
                    If dtTable.Rows.Count > 0 Then
                        If Not ddlModule.Items.FindByValue(dtTable.Rows(0).Item("numModuleId")) Is Nothing Then
                            ddlModule.SelectedItem.Selected = False
                            ddlModule.Items.FindByValue(dtTable.Rows(0).Item("numModuleId")).Selected = True
                        End If

                        If Not lstGroup.Items.FindByValue(dtTable.Rows(0).Item("numGroupId")) Is Nothing Then
                            lstGroup.SelectedItem.Selected = False
                            lstGroup.Items.FindByValue(dtTable.Rows(0).Item("numGroupId")).Selected = True
                        End If
                        txtSql.Text = dtTable.Rows(0).Item("textQuery")
                        txtReportDesc.Text = dtTable.Rows(0).Item("vcReportName")
                        txtReportName.Text = dtTable.Rows(0).Item("vcReportDescription")
                        BindReportFields()
                        BindSelectedFields()
                        BindOrderList()
                        BindfilterLists()
                        BindSummationLists()
                        BindDataGrid()
                    End If
                End If

                If Not Session("ReportFields") Is Nothing Then
                    FillColumnsArray()
                End If
                If txtColumnsSelected.Text <> "" Then
                    spnSelColumns.InnerHtml = txtColumnsSelected.Text
                End If

                If txtColumnlist.Text <> "" Then
                    spnListOrder.InnerHtml = txtColumnlist.Text
                End If
                If txtspanList.Text <> "" Then
                    list.InnerHtml = txtspanList.Text
                End If
                If txtspanSummation.Text <> "" Then
                    spnSummation.InnerHtml = txtspanSummation.Text
                End If



            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub FillColumnsArray()
            Try
                If Not Session("ReportFields") Is Nothing Then
                    Dim dtReportFields As New DataTable
                    dtReportFields = Session("ReportFields")

                    Dim sClientSideString As New System.Text.StringBuilder                              'Declare a string builder
                    Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index

                    sClientSideString.Append(vbCrLf & "<script language='javascript'>")                  'Start with the script block
                    sClientSideString.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)      'Create a array to hold the field information in javascript
                    sClientSideString.Append(createClientSideScript(dtReportFields, iRowIndex))      'Call function to create a client side script
                    sClientSideString.Append("</script>")                                                'end the client side javascript block
                    litClientScript.Text &= vbCrLf & sClientSideString.ToString()
                End If
            Catch ex As Exception

            End Try
        End Sub
        Sub LoadDropDowns()
            Try
                'step 1 loading the module and group information
                Dim ds As DataSet
                Dim ojReports As New CustomReports
                ojReports.numCustModuleId = 1   'setting the group as 
                ds = ojReports.GetModules()
                ddlModule.DataSource = ds.Tables(0)
                ddlModule.DataTextField = "vcCustModuleName"
                ddlModule.DataValueField = "numCustModuleId"
                ddlModule.DataBind()
                If Not ddlModule.Items.FindByValue("1") Is Nothing Then
                    ddlModule.Items.FindByValue("1").Selected = True
                End If

                lstGroup.DataSource = ds.Tables(1)
                lstGroup.DataTextField = "vcFieldGroupName"
                lstGroup.DataValueField = "numReportOptionGroupId"
                lstGroup.DataBind()

                If lstGroup.Items.Count > 0 Then
                    lstGroup.Items(0).Selected = True
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
            Try
                LoadGroup()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub LoadGroup()
            Try
                Dim ds As DataSet
                Dim ojReports As New CustomReports
                ojReports.numCustModuleId = ddlModule.SelectedValue
                ds = ojReports.GetModules()

                lstGroup.DataSource = ds.Tables(1)
                lstGroup.DataTextField = "vcFieldGroupName"
                lstGroup.DataValueField = "numFieldGroupId"
                lstGroup.DataBind()

                If lstGroup.Items.Count > 0 Then
                    lstGroup.Items(0).Selected = True
                End If
            Catch ex As Exception

            End Try
        End Sub

        Private Sub btnNext1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext1.Click
            uwOppTab.SelectedTabIndex = 1
            BindReportFields()
        End Sub
        Sub BindReportFields()
            Dim objCustomReports As New CustomReports
            Dim dtReportFields, dtCustomReportFields As DataTable           'declare a datatable to store the default fields and the custom fields resp.
            Dim drCustomReportFieldsCopy As DataRow                         'Declare a DataRow
            dtReportFields = objCustomReports.getColumnsForCustomReport(lstGroup.SelectedValue) 'call to get the default fields
            Session("ReportFields") = dtReportFields
            Dim dtCell As TableCell
            Dim dtRow As TableRow

            'dtReportFields = Session("ReportFields")

            Dim sClientSideString As New System.Text.StringBuilder                              'Declare a string builder
            Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index

            sClientSideString.Append(vbCrLf & "<script language='javascript'>")                  'Start with the script block
            sClientSideString.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)      'Create a array to hold the field information in javascript
            sClientSideString.Append(createClientSideScript(dtReportFields, iRowIndex))      'Call function to create a client side script
            sClientSideString.Append("</script>")                                                'end the client side javascript block
            litClientScript.Text &= vbCrLf & sClientSideString.ToString()



            Dim dvReportFieldsOrder, dvReportFieldsGroup1, dvReportFieldsGroup2, dvReportFieldsGroup3, dvReportFieldsGroup4, dvReportFieldsGroup5 As DataView 'declare a databviews for each of the list box
            dvReportFieldsGroup1 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup2 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup3 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup4 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup5 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsOrder = dtReportFields.DefaultView                            'get the default view

            'For Each row As DataRow In dtReportFields.Rows



            'Next
            Dim aspTable As New Table
            aspTable.Width = Unit.Percentage(100)
            aspTable.BorderWidth = Unit.Pixel(1)
            aspTable.CellPadding = 0
            aspTable.CellSpacing = 0
            aspTable.GridLines = GridLines.Both
            dvReportFieldsGroup1.RowFilter = "numFieldGroupID = 6"
            If dvReportFieldsGroup1.Count > 0 Then
                dtRow = New TableRow
                dtCell = New TableCell
                dtCell.ColumnSpan = 4
                Dim l As New Label
                l.Text = "Contacts"
                l.CssClass = "normal7"
                dtCell.Controls.Add(l)
                dtRow.Cells.Add(dtCell)
                aspTable.Rows.Add(dtRow)
                dtRow = New TableRow
                dtCell = New TableCell
                dtRow.Width = Unit.Percentage(100)
                Dim cellcount As Integer = 0
                Dim count As Integer = 0
                For Each drv As DataRowView In dvReportFieldsGroup1

                    If cellcount = 4 Then
                        cellcount = 0
                        dtRow.Cells.Add(dtCell)
                        aspTable.Rows.Add(dtRow)
                        dtRow = New TableRow
                        dtCell = New TableCell
                        dtRow.Width = Unit.Percentage(100)
                    End If
                    Dim chk As New CheckBox
                    chk.Text = drv.Item("vcScrFieldName")
                    If CType(IIf(IsDBNull(drv.Item("boolDefaultColumnSelection")), False, drv.Item("boolDefaultColumnSelection")), Boolean) = True Then
                        chk.Checked = True
                    End If
                    chk.ID = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                    chk.CssClass = "normal1"
                    chk.Width = Unit.Percentage(20)
                    dtCell.Controls.Add(chk)
                    cellcount = cellcount + 1
                    count = count + 1
                    If dvReportFieldsGroup1.Count = count Then
                        dtRow.Cells.Add(dtCell)
                        aspTable.Rows.Add(dtRow)
                    End If
                Next
            End If
            dvReportFieldsGroup2.RowFilter = "numFieldGroupID = 7"

            If dvReportFieldsGroup2.Count > 0 Then
                dtRow = New TableRow
                dtCell = New TableCell
                dtCell.ColumnSpan = 4
                Dim l As New Label
                l.Text = "Organization"
                l.CssClass = "normal7"
                dtCell.Controls.Add(l)
                dtRow.Cells.Add(dtCell)
                aspTable.Rows.Add(dtRow)
                dtCell.Width = Unit.Percentage(100)
                dtRow = New TableRow
                dtCell = New TableCell
                dtRow.Width = Unit.Percentage(100)
                Dim cellcount As Integer = 0
                Dim count As Integer = 0
                For Each drv As DataRowView In dvReportFieldsGroup2

                    If cellcount = 4 Then
                        cellcount = 0
                        dtRow.Cells.Add(dtCell)
                        aspTable.Rows.Add(dtRow)
                        dtRow = New TableRow
                        dtCell = New TableCell
                        dtRow.Width = Unit.Percentage(100)
                        dtCell.Width = Unit.Percentage(100)

                    End If
                    Dim chk As New CheckBox
                    chk.Text = drv.Item("vcScrFieldName")
                    If CType(IIf(IsDBNull(drv.Item("boolDefaultColumnSelection")), False, drv.Item("boolDefaultColumnSelection")), Boolean) = True Then
                        chk.Checked = True
                    End If
                    chk.ID = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID")
                    chk.CssClass = "normal1"
                    chk.Width = Unit.Percentage(20)
                    dtCell.Controls.Add(chk)
                    cellcount = cellcount + 1
                    count = count + 1
                    If dvReportFieldsGroup1.Count = count Then
                        dtRow.Cells.Add(dtCell)
                        aspTable.Rows.Add(dtRow)
                    End If
                Next
            End If
            spnSelColumns.Controls.Clear()
            spnSelColumns.Controls.Add(aspTable)
        End Sub

        'Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
        '    If uwOppTab.SelectedTabIndex = 1 Then
        '        BindReportFields()
        '    ElseIf uwOppTab.SelectedTabIndex = 2 Then
        '        BindSortListBox()
        '    End If
        'End Sub

        'Private Sub btnNext2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext2.Click
        '    Try
        '        'BindReportFields()
        '        uwOppTab.SelectedTabIndex = 2
        '        BindSortListBox()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Sub BindSortListBox()
            Try
                'Dim objCustomReports As New CustomReports
                Dim dtReportFields, dtCustomReportFields As DataTable           'declare a datatable to store the default fields and the custom fields resp.
                'Dim drCustomReportFieldsCopy As DataRow                         'Declare a DataRow
                'dtReportFields = objCustomReports.getColumnsForCustomReport(lstGroup.SelectedValue)
                dtReportFields = Session("ReportFields")

                Dim sClientSideString As New System.Text.StringBuilder                              'Declare a string builder
                Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index

                sClientSideString.Append(vbCrLf & "<script language='javascript'>")                  'Start with the script block
                sClientSideString.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)      'Create a array to hold the field information in javascript
                sClientSideString.Append(createClientSideScript(dtReportFields, iRowIndex))      'Call function to create a client side script
                sClientSideString.Append("</script>")                                                'end the client side javascript block
                litClientScript.Text &= vbCrLf & sClientSideString.ToString()


                'Dim i As Integer = 0
                'For i = 0 To dtReportFields.Rows.Count - 1

                '    If Not uwOppTab.FindControl(dtReportFields.Rows(i).Item("vcDbFieldName") & "~" & dtReportFields.Rows(i).Item("numFieldGroupID")) Is Nothing Then
                '        Dim chk As New CheckBox
                '        chk = CType(uwOppTab.FindControl(dtReportFields.Rows(i).Item("vcDbFieldName") & "" & dtReportFields.Rows(i).Item("numFieldGroupID")), CheckBox)
                '        If chk.Checked = True Then
                '            Dim item As ListItem
                '            item.Text = dtReportFields.Rows(i).Item("vcScrFieldName")
                '            item.Value = dtReportFields.Rows(i).Item("vcDbFieldName")
                '            lstReportColumnsOrder.Items.Add(item)
                '        End If
                '    End If
                'Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function createClientSideScript(ByVal dtTable As DataTable, ByRef iRowIndex As Int16) As String
            Try
                Dim sDynmicFieldInfo As New System.Text.StringBuilder                   'Create a Stringbuilder object
                Dim drDataRow As DataRow                                                'Declare a datarow to scrollthrough the rows in the datatable

                For Each drDataRow In dtTable.Rows                                      'Start scrolling through the table rows
                    'sDynmicFieldInfo.Append("arrFieldConfig[0] = new AssignedToList('--Select One--','0');") 'start creating the array elements, each holding field information
                  
                    sDynmicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new AssignedToList('" & drDataRow.Item("vcDbFieldName") & "~" & drDataRow.Item("numFieldGroupID") & "','" & drDataRow.Item("vcScrFieldName") & "','" & drDataRow.Item("numFieldGroupID") & "','" & drDataRow.Item("boolSummationPossible") & "','" & IIf(IsDBNull(drDataRow.Item("vcFieldType")), "", drDataRow.Item("vcFieldType")) & "');") 'start creating the array elements, each holding field information
                    iRowIndex += 1                                                      'increment the row counter
                Next
                Return sDynmicFieldInfo.ToString()                                      'return the client script
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
            Try
                Dim dtReport As DataTable
                Dim Sql As String
                Sql = "select "
                Sql = Sql & txtFields.Text & " from  "
                If lstGroup.SelectedValue = 4 Then
                    Sql = Sql & " AdditionalContactsInformation ACI where ACI.numDomainId = " & Session("DomainID")
                ElseIf lstGroup.SelectedValue = 5 Then
                    Sql = Sql & "  CompanyInfo , DivisionMaster  join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID  where ACI.numDomainId = " & Session("DomainID")
                End If
                If txtFilterFields.Text <> "" Then
                    Sql = Sql & " and (" & txtFilterFields.Text & ")"
                End If
                If txtSummationFields.Text <> "" Then
                    Sql = Sql & "                                 "
                    Sql = Sql & "select " & txtSummationFields.Text & " from  "
                    If lstGroup.SelectedValue = 4 Then
                        Sql = Sql & " AdditionalContactsInformation ACI where ACI.numDomainId = " & Session("DomainID")
                    ElseIf lstGroup.SelectedValue = 5 Then
                        Sql = Sql & "  CompanyInfo , DivisionMaster  join AdditionalContactsInformation ACI on DivisionMaster.numDivisionID=ACI.numDivisionID  where ACI.numDomainId = " & Session("DomainID")
                    End If
                    If txtFilterFields.Text <> "" Then
                        Sql = Sql & " and (" & txtFilterFields.Text & ")"
                    End If
                End If
                txtSql.Text = Sql
                BindDataGrid()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindDataGrid()
            Try
                Dim dtReport As DataTable
                Dim Sql As String
                Sql = txtSql.Text
                If Sql <> "" Then
                    Dim objCustomReports As New CustomReports
                    objCustomReports.DynamicQuery = Sql
                    Dim ds As DataSet
                    ds = objCustomReports.ExecuteDynamicSql()
                    dgReport.DataSource = ds.Tables(0)
                    dgReport.DataBind()
                End If
                uwOppTab.SelectedTabIndex = 5
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReport.ItemDataBound
            Select Case e.Item.ItemType

                Case ListItemType.Item, ListItemType.AlternatingItem

                Case ListItemType.Footer


            End Select
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If txtCheckedFields.Text <> "" And txtIsChecked.Text <> "" Then

                    Dim ReportId As Long = 0
                    If Not txtReportId.Text Is Nothing Then
                        ReportId = CInt(txtReportId.Text)
                    End If

                    Dim objCustomReport As New CustomReports
                    objCustomReport.ReportName = txtReportName.Text
                    objCustomReport.DynamicQuery = txtSql.Text
                    objCustomReport.ReportDesc = txtReportDesc.Text
                    objCustomReport.UserCntID = Session("userContactId")
                    objCustomReport.DomainID = Session("DomainId")
                    objCustomReport.ReportID = ReportId
                    objCustomReport.GroupId = lstGroup.SelectedValue
                    objCustomReport.numCustModuleId = ddlModule.SelectedValue

                    ReportId = objCustomReport.savecustomReport()
                    txtReportId.Text = ReportId
                    SaveCheckedFields(ReportId)
                    SaveOrderList(ReportId)
                    SaveFilterList(ReportId)
                    SaveSummationList(ReportId)
                End If
            Catch ex As Exception

            End Try
        End Sub
        Sub SaveCheckedFields(ByVal ReportId As Long)
            Try
                If txtCheckedFields.Text <> "" And txtIsChecked.Text <> "" Then
                    Dim strCheckedFields As String()
                    Dim strCheckedValues As String()
                    Dim strFields As String
                    Dim ds As New DataSet
                    strCheckedFields = txtCheckedFields.Text.Split("|")
                    strCheckedValues = txtIsChecked.Text.Split("|")
                    If strCheckedFields.Length = strCheckedValues.Length Then
                        Dim dtFields As New DataTable
                        dtFields.Columns.Add("vcField")
                        dtFields.Columns.Add("vcValue")
                        Dim dtRow As DataRow
                        Dim i As Integer = 0
                        For i = 0 To strCheckedFields.Length - 1
                            dtRow = dtFields.NewRow
                            dtRow("vcField") = strCheckedFields(i)
                            dtRow("vcValue") = strCheckedValues(i)
                            dtFields.Rows.Add(dtRow)
                        Next
                        dtFields.TableName = "Table"
                        ds.Tables.Add(dtFields.Copy)
                        strFields = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))


                        Dim objCustomReport As New CustomReports
                        objCustomReport.ReportID = ReportId
                        objCustomReport.strCheckedFields = strFields
                        objCustomReport.SaveCustomReportCheckedFields()


                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub SaveOrderList(ByVal ReportId As Long)
            Try
                If txtOrderFieldsText.Text <> "" Or txtOrderFieldsValue.Text <> "" Then
                    Dim strOrderText As String()
                    Dim strOrderValues As String()
                    Dim strFields As String
                    Dim ds As New DataSet
                    strOrderText = txtOrderFieldsText.Text.Split("|")
                    strOrderValues = txtOrderFieldsValue.Text.Split("|")
                    If strOrderText.Length = strOrderValues.Length Then
                        Dim dtFields As New DataTable
                        dtFields.Columns.Add("vcText")
                        dtFields.Columns.Add("vcValue")
                        dtFields.Columns.Add("numOrder")
                        Dim dtRow As DataRow
                        Dim i As Integer = 0
                        For i = 0 To strOrderText.Length - 1
                            dtRow = dtFields.NewRow
                            dtRow("vcText") = strOrderText(i)
                            dtRow("vcValue") = strOrderValues(i)
                            dtRow("numOrder") = i + 1
                            dtFields.Rows.Add(dtRow)
                        Next
                        dtFields.TableName = "Table"
                        ds.Tables.Add(dtFields.Copy)
                        strFields = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))


                        Dim objCustomReport As New CustomReports
                        objCustomReport.ReportID = ReportId
                        objCustomReport.strOrderFieldsText = strFields
                        objCustomReport.SaveCustomReportOrderList()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub SaveFilterList(ByVal ReportId As Long)
            Try
                If txtFilterFieldsText.Text <> "" And txtFilterFieldsValue.Text <> "" And txtFilterFieldsOperator.Text <> "" And txtFilterValue.Text <> "" Then
                    Dim strFilterFieldsText As String()
                    Dim strFilterFieldsOperator As String()
                    Dim strFilterFieldsValue As String()
                    Dim strFilterValue As String()
                    Dim strFields As String
                    Dim ds As New DataSet
                    strFilterFieldsText = txtFilterFieldsText.Text.Split("|")
                    strFilterFieldsOperator = txtFilterFieldsOperator.Text.Split("|")
                    strFilterFieldsValue = txtFilterFieldsValue.Text.Split("|")
                    strFilterValue = txtFilterValue.Text.Split("|")

                    If strFilterFieldsText.Length = strFilterFieldsOperator.Length Then
                        Dim dtFields As New DataTable
                        dtFields.Columns.Add("vcFilterFieldsText")
                        dtFields.Columns.Add("vcFilterFieldsValue")
                        dtFields.Columns.Add("vcFilterFieldsOperator")
                        dtFields.Columns.Add("vcFilterValue")
                        Dim dtRow As DataRow
                        Dim i As Integer = 0
                        For i = 0 To strFilterFieldsText.Length - 1
                            dtRow = dtFields.NewRow
                            dtRow("vcFilterFieldsText") = strFilterFieldsText(i)
                            dtRow("vcFilterFieldsValue") = strFilterFieldsValue(i)
                            dtRow("vcFilterFieldsOperator") = strFilterFieldsOperator(i)
                            dtRow("vcFilterValue") = strFilterValue(i)
                            dtFields.Rows.Add(dtRow)
                        Next
                        dtFields.TableName = "Table"
                        ds.Tables.Add(dtFields.Copy)
                        strFields = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))


                        Dim objCustomReport As New CustomReports
                        objCustomReport.ReportID = ReportId
                        objCustomReport.strOrderFieldsText = strFields
                        objCustomReport.SaveCustomReportFilterList()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub SaveSummationList(ByVal Reportid As Long)
            Try
                If txtsumFieldsText.Text <> "" And txtsumrFieldsValue.Text <> "" And txtsumFieldsOperator.Text <> "" Then
                    Dim strsumFieldsText As String()
                    Dim strsumFieldsValue As String()
                    Dim strsumFieldsOperator As String()
                    Dim strFields As String
                    Dim ds As New DataSet
                    strsumFieldsText = txtsumFieldsText.Text.Split("|")
                    strsumFieldsValue = txtsumrFieldsValue.Text.Split("|")
                    strsumFieldsOperator = txtsumFieldsOperator.Text.Split("|")
                    If strsumFieldsText.Length = strsumFieldsValue.Length Then

                        Dim dtFields As New DataTable
                        dtFields.Columns.Add("vcText")
                        dtFields.Columns.Add("vcValue")
                        dtFields.Columns.Add("vcOperator")
                        Dim dtRow As DataRow
                        Dim i As Integer = 0
                        For i = 0 To strsumFieldsText.Length - 1
                            dtRow = dtFields.NewRow
                            dtRow("vcText") = strsumFieldsText(i)
                            dtRow("vcValue") = strsumFieldsValue(i)
                            dtRow("vcOperator") = strsumFieldsOperator(i)
                            dtFields.Rows.Add(dtRow)
                        Next
                        dtFields.TableName = "Table"
                        ds.Tables.Add(dtFields.Copy)
                        strFields = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))


                        Dim objCustomReport As New CustomReports
                        objCustomReport.ReportID = Reportid
                        objCustomReport.strOrderFieldsText = strFields
                        objCustomReport.SaveCustomReportSumList()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindSelectedFields()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getCheckedFields()
                If dtTable.Rows.Count > 0 Then
                    For Each row As DataRow In dtTable.Rows
                        CType(uwOppTab.FindControl(row.Item("vcFieldName")), CheckBox).Checked = row.Item("vcValue")
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindOrderList()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getReportOrderList()
                If dtTable.Rows.Count > 0 Then
                    For Each row As DataRow In dtTable.Rows
                        Dim listItem As New ListItem
                        listItem.Text = row.Item("vcFieldText")
                        listItem.Value = row.Item("vcValue")
                        lstReportColumnsOrder.Items.Add(listItem)
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindfilterLists()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getReportFilterList()
                If dtTable.Rows.Count > 0 Then
                    noFilterRows.Text = dtTable.Rows.Count
                End If
                Dim strFieldSelected(dtTable.Rows.Count - 1) As String
                Dim strFieldOperator(dtTable.Rows.Count - 1) As String
                Dim strFieldValue(dtTable.Rows.Count - 1) As String
                Dim ifilter As Integer = 0
                For Each row As DataRow In dtTable.Rows
                    strFieldSelected(ifilter) = row("vcfieldsValue")
                    strFieldOperator(ifilter) = row("vcfieldsOperator")
                    strFieldValue(ifilter) = row("vcFilterValue")
                    ifilter = ifilter + 1
                Next
                Dim i As Integer = 0
                Dim builder As New System.Text.StringBuilder
                Dim dtReportFields As DataTable
                dtReportFields = Session("ReportFields")
                builder = builder.Append("<TABLE class=normal1><TBODY>")
                If dtTable.Rows.Count = 0 Then
                    For i = 0 To 4
                        Dim dvReportFieldsOrder, dvReportFieldsGroup1, dvReportFieldsGroup2, dvReportFieldsGroup3, dvReportFieldsGroup4, dvReportFieldsGroup5 As DataView 'declare a databviews for each of the list box
                        dvReportFieldsGroup1 = dtReportFields.DefaultView
                        dvReportFieldsGroup2 = dtReportFields.DefaultView
                        builder = builder.Append("<TR id=tr" & i + 1 & "><TD id=td" & i + 1 & ">Select Field")
                        builder = builder.Append("<SELECT class=signup id=ddlField" & i & " style='WIDTH: 150px' name=ddlField" & i & "><OPTION value=0>--Select One--</OPTION>")

                        dvReportFieldsGroup1.RowFilter = "numFieldGroupID = 6"
                        If dvReportFieldsGroup1.Count > 0 Then
                            builder = builder.Append("<OPTGROUP label=Contacts>")
                            For Each drv As DataRowView In dvReportFieldsGroup1
                                builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & ">" & drv.Item("vcScrFieldName") & "</OPTION>")
                            Next
                            builder = builder.Append("</OPTGROUP>")
                        End If
                        dvReportFieldsGroup2.RowFilter = "numFieldGroupID = 7"
                        If dvReportFieldsGroup2.Count > 0 Then
                            builder = builder.Append("<OPTGROUP label=Organization>")
                            For Each drv As DataRowView In dvReportFieldsGroup2
                                builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & ">" & drv.Item("vcScrFieldName") & "</OPTION>")
                            Next
                            builder = builder.Append("</OPTGROUP>")
                        End If
                        builder = builder.Append("</SELECT><SELECT class=signup id=Operator" & i & " name=Operator" & i & ">")

                        builder = builder.Append("<OPTION value==>equals</OPTION>")
                        builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                        builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                        builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                        builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                        builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                        builder = builder.Append("<OPTION value=Like >contains</OPTION>")
                        builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                        builder = builder.Append("<OPTION value=sw >starts with</OPTION>")
                        builder = builder.Append("<OPTION value=Ew >end with</OPTION>")

                        builder = builder.Append("</SELECT><INPUT class=signup id=Value" & i & " title=Value" & i & " maxLength=1000 value='' name=Value" & i & ">")

                        builder = builder.Append("</td></tr>")
                    Next
                Else
                    For i = 0 To CInt(noFilterRows.Text) - 1
                        Dim dvReportFieldsOrder, dvReportFieldsGroup1, dvReportFieldsGroup2, dvReportFieldsGroup3, dvReportFieldsGroup4, dvReportFieldsGroup5 As DataView 'declare a databviews for each of the list box
                        dvReportFieldsGroup1 = dtReportFields.DefaultView
                        dvReportFieldsGroup2 = dtReportFields.DefaultView
                        builder = builder.Append("<TR id=tr" & i + 1 & "><TD id=td" & i + 1 & ">Select Field")
                        builder = builder.Append("<SELECT class=signup id=ddlField" & i & " style='WIDTH: 150px' name=ddlField" & i & "><OPTION value=0>--Select One--</OPTION>")

                        dvReportFieldsGroup1.RowFilter = "numFieldGroupID = 6"
                        If dvReportFieldsGroup1.Count > 0 Then
                            builder = builder.Append("<OPTGROUP label=Contacts>")
                            For Each drv As DataRowView In dvReportFieldsGroup1
                                If strFieldSelected(i).Trim = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) Then
                                    builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & " selected >" & drv.Item("vcScrFieldName") & "</OPTION>")
                                Else
                                    builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & ">" & drv.Item("vcScrFieldName") & "</OPTION>")
                                End If

                            Next
                            builder = builder.Append("</OPTGROUP>")
                        End If
                        dvReportFieldsGroup2.RowFilter = "numFieldGroupID = 7"
                        If dvReportFieldsGroup2.Count > 0 Then
                            builder = builder.Append("<OPTGROUP label=Organization>")
                            For Each drv As DataRowView In dvReportFieldsGroup2
                                ' builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & ">" & drv.Item("vcScrFieldName") & "</OPTION>")
                                If strFieldSelected(i) = drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) Then
                                    builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & " selected >" & drv.Item("vcScrFieldName") & "</OPTION>")
                                Else
                                    builder = builder.Append("<OPTION value=" & drv.Item("vcDbFieldName") & "~" & drv.Item("numFieldGroupID") & "~" & IIf(IsDBNull(drv.Item("vcFieldType")), "", drv.Item("vcFieldType")) & ">" & drv.Item("vcScrFieldName") & "</OPTION>")
                                End If
                            Next
                            builder = builder.Append("</OPTGROUP>")
                        End If
                        builder = builder.Append("</SELECT><SELECT class=signup id=Operator" & i & " name=Operator" & i & ">")
                        If strFieldOperator(i).Trim = "=" Then
                            builder = builder.Append("<OPTION value== selected>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= selected>not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= < >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "!=" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= selected>not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "<" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' selected>less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = ">" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' selected>greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "<=" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' selected>less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = ">=" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= 'selected>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "Like" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like selected>contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "Not Like" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like >contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'selected>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "sw" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like >contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw selected>starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew>end with</OPTION>")
                        ElseIf strFieldOperator(i).Trim = "Ew" Then
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like >contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw >starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew selected>end with</OPTION>")
                        Else
                            builder = builder.Append("<OPTION value==>equals</OPTION>")
                            builder = builder.Append("<OPTION value=!= >not equal to</OPTION>")
                            builder = builder.Append("<OPTION value= '<' >less than</OPTION>")
                            builder = builder.Append("<OPTION value= '>' >greater than</OPTION>")
                            builder = builder.Append("<OPTION value= '<=' >less or equal</OPTION>")
                            builder = builder.Append("<OPTION value=' >= '>greater or equal</OPTION>")
                            builder = builder.Append("<OPTION value=Like >contains</OPTION>")
                            builder = builder.Append("<OPTION value='Not Like'>does not contain</OPTION>")
                            builder = builder.Append("<OPTION value=sw >starts with</OPTION>")
                            builder = builder.Append("<OPTION value=Ew >end with</OPTION>")
                        End If
                        builder = builder.Append("</SELECT><INPUT class=signup id=Value" & i & " title=Value" & i & " maxLength=1000 value=" & strFieldValue(i) & " name=Value" & i & ">")

                        builder = builder.Append("</td></tr>")
                    Next
                End If

                builder = builder.Append("</TBODY></TABLE>")
                txtspanList.Text = builder.ToString
                'Textbox1.Text = builder.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindSummationLists()
            Try
                Dim objReport As New CustomReports
                Dim dtTable As DataTable
                objReport.ReportID = CInt(txtReportId.Text)
                dtTable = objReport.getReportSummationList()
                If dtTable.Rows.Count > 0 Then
                    noSummationRows.Text = dtTable.Rows.Count
                End If
                Dim strFieldSelected(dtTable.Rows.Count - 1) As String
                Dim strFieldOperator(dtTable.Rows.Count - 1) As String

                Dim ifilter As Integer = 0
                For Each row As DataRow In dtTable.Rows
                    strFieldSelected(ifilter) = row("vcValue")
                    strFieldOperator(ifilter) = row("vcOperator")
                    ifilter = ifilter + 1
                Next
                Dim i As Integer = 0
                Dim builder As New System.Text.StringBuilder
                builder = builder.Append("<TABLE class=normal1><TBODY>")
                If dtTable.Rows.Count = 0 Then
                    For i = 0 To 4

                        builder = builder.Append("<TR id=tr" & i + 1 & "><TD id=td" & i + 1 & "> Select Field ")
                        builder = builder.Append("<SELECT class=signup id=ddlFieldSummation" & i & " style='WIDTH: 150px' name=ddlFieldSummation" & i & ">")
                        builder = builder.Append("<OPTION value=0>--Select One--</OPTION>")
                        For Each lstItem As ListItem In lstReportColumnsOrder.Items
                            If lstItem.Value.Split("~")(3) = True Then

                                builder = builder.Append("<OPTION value=" & lstItem.Value & ">" & lstItem.Text & "</OPTION>")

                            End If
                        Next
                        builder = builder.Append("</SELECT></TD><TD>")

                        builder = builder.Append("<SELECT class=signup id=OperatorSummation" & i & " name=OperatorSummation" & i & ">")
                        builder = builder.Append("<OPTION value=Sum selected>Sum</OPTION>")
                        builder = builder.Append("<OPTION value=Avg>Average</OPTION>")
                        builder = builder.Append("<OPTION value=Max>Largest Value</OPTION>")
                        builder = builder.Append("<OPTION value=Min>SmallestValue</OPTION>")
                        builder = builder.Append("</SELECT></TD></TR>")
                    Next
                Else
                    For i = 0 To dtTable.Rows.Count - 1
                        builder = builder.Append("<TR id=tr" & i + 1 & "><TD id=td" & i + 1 & "> Select Field ")
                        builder = builder.Append("<SELECT class=signup id=ddlFieldSummation" & i & " style='WIDTH: 150px' name=ddlFieldSummation" & i & ">")
                        builder = builder.Append("<OPTION value=0>--Select One--</OPTION>")
                        For Each lstItem As ListItem In lstReportColumnsOrder.Items
                            If lstItem.Value.Split("~")(3) = True Then
                                Dim strvalue As String = strFieldSelected(i).Split("~")(0) & "~" & strFieldSelected(i).Split("~")(1)
                                Dim listvalue As String = lstItem.Value.Split("~")(0) & "~" & lstItem.Value.Split("~")(1)
                                If strvalue = listvalue Then
                                    builder = builder.Append("<OPTION value=" & lstItem.Value & " selected>" & lstItem.Text & "</OPTION>")
                                Else
                                    builder = builder.Append("<OPTION value=" & lstItem.Value & ">" & lstItem.Text & "</OPTION>")
                                End If
                            End If
                        Next
                        builder = builder.Append("</SELECT></TD><TD>")

                        builder = builder.Append("<SELECT class=signup id=OperatorSummation" & i & " name=OperatorSummation" & i & ">")

                        If strFieldOperator(i) = "Sum" Then
                            builder = builder.Append("<OPTION value=Sum selected>Sum</OPTION>")
                            builder = builder.Append("<OPTION value=Avg>Average</OPTION>")
                            builder = builder.Append("<OPTION value=Max>Largest Value</OPTION>")
                            builder = builder.Append("<OPTION value=Min>SmallestValue</OPTION>")
                        ElseIf strFieldOperator(i) = "Avg" Then
                            builder = builder.Append("<OPTION value=Sum >Sum</OPTION>")
                            builder = builder.Append("<OPTION value=Avg selected>Average</OPTION>")
                            builder = builder.Append("<OPTION value=Max>Largest Value</OPTION>")
                            builder = builder.Append("<OPTION value=Min>SmallestValue</OPTION>")
                        ElseIf strFieldOperator(i) = "Max" Then
                            builder = builder.Append("<OPTION value=Sum >Sum</OPTION>")
                            builder = builder.Append("<OPTION value=Avg>Average</OPTION>")
                            builder = builder.Append("<OPTION value=Max selected>Largest Value</OPTION>")
                            builder = builder.Append("<OPTION value=Min>SmallestValue</OPTION>")
                        ElseIf strFieldOperator(i) = "Min" Then
                            builder = builder.Append("<OPTION value=Sum >Sum</OPTION>")
                            builder = builder.Append("<OPTION value=Avg>Average</OPTION>")
                            builder = builder.Append("<OPTION value=Max>Largest Value</OPTION>")
                            builder = builder.Append("<OPTION value=Min selected>SmallestValue</OPTION>")
                        Else
                            builder = builder.Append("<OPTION value=Sum selected>Sum</OPTION>")
                            builder = builder.Append("<OPTION value=Avg>Average</OPTION>")
                            builder = builder.Append("<OPTION value=Max>Largest Value</OPTION>")
                            builder = builder.Append("<OPTION value=Min>SmallestValue</OPTION>")
                        End If
                        builder = builder.Append("</SELECT></TD></TR>")
                    Next
                End If
                builder = builder.Append("</TBODY></TABLE>")
                txtspanSummation.Text = builder.ToString
                Textbox1.Text = builder.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace