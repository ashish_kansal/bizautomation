<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmRepContactsLogged.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmRepContactsLogged"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>Access Details of Contacts</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">		
		<script language=javascript >
		function OpenBizList(ConID)
		{
		window.open("../Reports/frmBizDocsViewedByCont.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ConID="+ConID,'','toolbar=no,titlebar=no,left=200, top=250,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
			function PopupCheck()
		{
		    document.form1.btnGo.click()
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<table width="100%" class="aspTable" style="height:200px"><tr><td valign="top">
		<table>
                <td width="100%"></td>
                <td  align="center">
		           <asp:UpdateProgress ID ="up1"  runat="server" DynamicLayout="false">
				        <ProgressTemplate>
				         <asp:Image ID="Image1"  ImageUrl="~/images/updating.gif" runat="server"/>
				         </ProgressTemplate>
    			    </asp:UpdateProgress>
    		    </td>
	    </table>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<asp:DataGrid ID="dgContacts" CssClass="dg" Width="100%" Runat="server" AutoGenerateColumns="false"
				AllowSorting="True">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="numContactID"  Visible=False ></asp:BoundColumn>
					<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font color=white>Contacts</font>"></asp:BoundColumn>
					<asp:TemplateColumn SortExpression="bintLastLoggedIn" HeaderText="<font color=white>Last Access date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "bintLastLoggedIn")) %>
									</ItemTemplate>
								</asp:TemplateColumn>

					<asp:TemplateColumn SortExpression="numNoofTimes" HeaderText="<font color=white>No of Times Logged in</font>">
						<ItemTemplate>
							<asp:HyperLink ID="hplNoOfTimes" CssClass=hyperlink Runat="server" text='<%# DataBinder.Eval(Container,"DataItem.numNoofTimes") %>'>
							</asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
			</ContentTemplate>
			</asp:updatepanel>
</td></tr></table>
		</form>
	</body>
</HTML>
