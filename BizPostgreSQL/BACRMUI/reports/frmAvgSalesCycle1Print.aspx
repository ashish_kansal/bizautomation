<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAvgSalesCycle1Print.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmAvgSalesCycle1Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Average Sales Cycle</title>
</head>
<body>
    <form id="frmAvgSalesCyclePrint"  runat="server">
            <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
	        </table>
	<br />
	        <table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						Average Sales Cycle Report
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<tr>
								<td class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</td>
								<td class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></td>
							</tr>
							<tr>
								<td class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</td>
								<td class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></td>
							</tr>
						</table>				
						</td>
			</table>
				
			<script language="Javascript" type="text/javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="center">
						<asp:table runat="server" id="tblData" width="100%" CellPadding="2" CellSpacing="1" BorderWidth="0px">
							<asp:TableRow>
							<asp:TableCell>
							<asp:datagrid id="dgAvgSales" runat="server" AllowSorting="true" CssClass="dg" Width="100%"
							AutoGenerateColumns="False" EnableViewState="true">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="CName" SortExpression="CName" HeaderText="<font color=white>Customer</font>"></asp:BoundColumn>								
								<asp:TemplateColumn HeaderText="<font color=white>Date created</font>" SortExpression="bintCreatedDate">
									<ItemTemplate>
										<%#ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate"))%>										   
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="<font color=white>Date Closed</font>" SortExpression="bintAccountClosingDate">
									<ItemTemplate>									
										<%#ReturnName(DataBinder.Eval(Container.DataItem, "bintAccountClosingDate"))%>										   
									</ItemTemplate>
								</asp:TemplateColumn>
																
								<asp:BoundColumn DataField="days"  SortExpression="days" HeaderText="<font color=white>Number of days before first deal won</font>"
									 ></asp:BoundColumn>
                                   									 
								<asp:BoundColumn DataField="monPAmount"  DataFormatString="{0:#,##0.00}" SortExpression="monPAmount" HeaderText="<font color=white>Total Amount Of first deal won</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="numRecOwner" SortExpression="numRecOwner" HeaderText="<font color=white>Record Owner</font>"></asp:BoundColumn>																
								<asp:BoundColumn DataField="AssignedTo" SortExpression="AssignedTo" HeaderText="<font color=white>Assigned To</font>"></asp:BoundColumn>
							</Columns>
						    </asp:datagrid>
						    </asp:TableCell>
						</asp:TableRow>
						</asp:table>
					</td>
				</tr>
				
			</table>
			<table  cellpadding="0" cellspacing="0"  Width="100%">
                    <tr>
                        <td  align="right" class="text_bold">
                        Average Number Of Days In System Before First Deal Won :<asp:Label ID="lbl2"   runat="server"></asp:Label>
                        <br />
                        Average Total Amount For First Deal : <asp:Label ID="lbl1" runat="server"></asp:Label><asp:label id="lblRecordCount" runat="server" Visible="false"></asp:label></td>
                        </tr>
            </table>
            <script language="JavaScript"  type="text/javascript">
 function Lfprintcheck()
 {
	this.print()
	//history.back()
	document.location.href = "frmAvgSalesCycle1.aspx";
}
			</script>
    </form>
</body>
</html>
