Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmCustomReportVal : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim ddl As String = GetQueryStringVal("ddl")
                'If ddl.Split("~")(2) = "Date Field" Or ddl.Split("~")(2) = "Date Field U" Then
                'If Not IsPostBack Then
                '    Call FillCombo()
                '    Dim strNow As Date
                '    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                '    Calendar1.VisibleDate = strNow
                '    ddlYear.SelectedItem.Selected = False
                '    ddlYear.Items.FindByValue(Year(Calendar1.VisibleDate)).Selected = True
                '    ddlMonth.SelectedItem.Selected = False
                '    ddlMonth.Items.FindByValue(Month(Calendar1.VisibleDate)).Selected = True
                'End If
                'btnClose.Visible = False
                'btnInsert.Visible = False
                'Else
                pnlCal.Visible = False
                BindListItems()

                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#Region "Fill Combo"
        Private Sub FillCombo()
            Try
                Dim i As Integer
                For i = 1900 To 2100
                    ddlYear.Items.Add(i)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "Functional Method"

        Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
            Try
                Dim script As String = "<script language=""javascript"">"
                Dim bodyscript As String = "window.opener.document.getElementById('" & GetQueryStringVal("obj") & "').value = '';"
                Dim focus As String = "window.opener.document.getElementById('" & GetQueryStringVal("obj") & "').focus();"
                Dim closer As String = "window.close();"
                Dim endscript As String = "</script" & ">"
                Response.Write(script + focus + bodyscript + closer + endscript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
            Try
                If e.Day.Date = DateTime.Now.ToString("d") Then e.Cell.BackColor = System.Drawing.Color.LightGray
                If e.Day.IsOtherMonth = True Then
                    e.Cell.ForeColor = System.Drawing.Color.FromName("White")
                    e.Cell.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function getIndex(ByVal MonthName As String) As Integer
            Try
                Return Microsoft.VisualBasic.Switch(MonthName = "January", 0, MonthName = "February", 1, MonthName = "March", 2, MonthName = "April", 3, MonthName = "May", 4, MonthName = "June", 5, MonthName = "July", 6, MonthName = "August", 7, MonthName = "September", 8, MonthName = "October", 9, MonthName = "November", 10, MonthName = "December", 11)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function getMonth(ByVal intMonth As Int16) As String
            Try
                Select Case intMonth
                    Case 1 : Return "Jan"
                    Case 2 : Return "Feb"
                    Case 3 : Return "Mar"
                    Case 4 : Return "Apr"
                    Case 5 : Return "May"
                    Case 6 : Return "Jun"
                    Case 7 : Return "Jul"
                    Case 8 : Return "Aug"
                    Case 9 : Return "Sep"
                    Case 10 : Return "Oct"
                    Case 11 : Return "Nov"
                    Case 12 : Return "Dec"
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

#Region "Navigation Method"

        Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
            Try
                Calendar1.VisibleDate = DateAdd(DateInterval.Year, -1, Calendar1.VisibleDate)
                ddlYear.SelectedItem.Selected = False
                ddlYear.Items.FindByValue(Year(Calendar1.VisibleDate)).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
            Try
                Calendar1.VisibleDate = DateAdd(DateInterval.Month, -1, Calendar1.VisibleDate)
                ddlMonth.SelectedItem.Selected = False
                ddlMonth.Items.FindByValue(Month(Calendar1.VisibleDate)).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
            Try
                Calendar1.VisibleDate = DateAdd(DateInterval.Month, 1, Calendar1.VisibleDate)
                ddlMonth.SelectedItem.Selected = False
                ddlMonth.Items.FindByValue(Month(Calendar1.VisibleDate)).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
            Try
                Calendar1.VisibleDate = DateAdd(DateInterval.Year, 1, Calendar1.VisibleDate)
                ddlYear.SelectedItem.Selected = False
                ddlYear.Items.FindByValue(Year(Calendar1.VisibleDate)).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        Private Sub ddlYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
            Try
                Calendar1.VisibleDate = New Date(ddlYear.SelectedItem.Value, Calendar1.VisibleDate.Month, 15)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
            Try
                Calendar1.VisibleDate = New Date(Calendar1.VisibleDate.Year, ddlMonth.SelectedItem.Value, 15)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindListItems()
            Try
                Dim ddl As String = GetQueryStringVal("ddl")
                Dim i As Integer = CInt(GetQueryStringVal("i"))
                If ddl <> "0" Then
                    Dim listId As Integer = ddl.Split("~")(3)
                    Dim bitCustom As Integer = ddl.Split("~")(4)
                    Dim type As String = ddl.Split("~")(2)
                    Dim objCommon As New CCommon
                    Dim dtTable As DataTable

                    If type = "Drop Down List" Then
                        If listId <> 0 Then
                            Dim ichk As Integer = 1
                            dtTable = objCommon.GetMasterListItems(listId, Session("DomainID"))
                            txtnochkBox.Text = dtTable.Rows.Count
                            If dtTable.Rows.Count > 0 Then
                                For Each row As DataRow In dtTable.Rows
                                    Dim rad As New RadioButton
                                    Dim tr As New HtmlTableRow
                                    Dim td As New HtmlTableCell

                                    rad.ID = "rad" & ichk
                                    rad.GroupName = "Values"
                                    rad.Text = row.Item("vcData")
                                    rad.CssClass = "normal1"
                                    td.Controls.Add(rad)
                                    Dim txt As New TextBox
                                    txt.Text = row.Item("numListItemId")
                                    txt.Style("Display") = "none"
                                    txt.ID = "txt" & ichk
                                    td.Controls.Add(txt)
                                    td.VAlign = "top"
                                    tr.Cells.Add(td)
                                    tr.VAlign = "top"
                                    tblHValues.Rows.Add(tr)
                                    ichk = ichk + 1
                                Next
                            End If
                        End If
                    ElseIf type = "Customdata" Or type = "Check box" Or type = "Date Field" Or type = "Date Field U" Then
                        If type = "Date Field" Then
                            listId = 20
                        ElseIf type = "Date Field U" Then
                            listId = 21
                        End If

                        Dim ichk As Integer = 1
                        If type = "Check box" Then
                            dtTable = objCommon.GetCustomData(9)
                        Else : dtTable = objCommon.GetCustomData(listId)
                        End If

                        txtnochkBox.Text = dtTable.Rows.Count
                        If dtTable.Rows.Count > 0 Then
                            For Each row As DataRow In dtTable.Rows
                                Dim rad As New RadioButton
                                Dim tr As New HtmlTableRow
                                Dim td As New HtmlTableCell
                                rad.ID = "rad" & ichk
                                rad.GroupName = "Values"
                                rad.Text = row.Item("vcOptionName")
                                rad.CssClass = "normal1"
                                td.Controls.Add(rad)
                                Dim txt As New TextBox
                                txt.Text = row.Item("vcSqlValue")
                                txt.Style("Display") = "none"
                                txt.ID = "txt" & ichk
                                td.Controls.Add(txt)
                                td.VAlign = "top"
                                tr.Cells.Add(td)
                                tr.VAlign = "top"
                                tblHValues.Rows.Add(tr)
                                ichk = ichk + 1
                            Next
                        End If
                    ElseIf type = "State" Then
                        Dim objUserAcces As New UserAccess
                        Dim ichk As Integer = 1
                        objUserAcces.DomainID = Session("DomainId")
                        dtTable = objUserAcces.SelAllStates
                        txtnochkBox.Text = dtTable.Rows.Count
                        For Each row As DataRow In dtTable.Rows
                            Dim rad As New RadioButton
                            Dim tr As New HtmlTableRow
                            Dim td As New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcState")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)
                            Dim txt As New TextBox
                            txt.Text = row.Item("numStateID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next
                    ElseIf type = "Contact" Then
                        Dim ichk As Integer = 1
                        dtTable = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                        txtnochkBox.Text = dtTable.Rows.Count + 1
                        Dim rad As New RadioButton
                        Dim tr As New HtmlTableRow
                        Dim td As New HtmlTableCell
                        Dim txt As New TextBox
                        For Each row As DataRow In dtTable.Rows
                            txt = New TextBox
                            rad = New RadioButton
                            tr = New HtmlTableRow
                            td = New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcUserName")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)
                            txt.Text = row.Item("numContactID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next

                        txt = New TextBox
                        rad = New RadioButton
                        tr = New HtmlTableRow
                        td = New HtmlTableCell

                        rad.ID = "rad" & ichk
                        rad.GroupName = "Values"
                        rad.Text = "Current User"
                        rad.CssClass = "normal1"
                        td.Controls.Add(rad)
                        txt.Text = "@numUserCntId"
                        txt.Style("Display") = "none"
                        txt.ID = "txt" & ichk
                        td.Controls.Add(txt)
                        td.VAlign = "top"
                        tr.Cells.Add(td)
                        tr.VAlign = "top"
                        tblHValues.Rows.Add(tr)

                    ElseIf type = "Terriotary" Then
                        Dim ichk As Integer = 1
                        Dim objUserAccess As New UserAccess
                        objUserAccess.UserCntID = Session("UserContactId")
                        objUserAccess.DomainID = Session("DomainID")
                        dtTable = objUserAccess.GetTerritoryForUsers
                        txtnochkBox.Text = dtTable.Rows.Count
                        For Each row As DataRow In dtTable.Rows
                            Dim rad As New RadioButton
                            Dim tr As New HtmlTableRow
                            Dim td As New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcTerName")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)
                            Dim txt As New TextBox
                            txt.Text = row.Item("numTerritoryID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next
                    ElseIf type = "UOM" Then
                        Dim ichk As Integer = 1

                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.UOMAll = True
                        dtTable = objCommon.GetItemUOM()
                        txtnochkBox.Text = dtTable.Rows.Count
                        For Each row As DataRow In dtTable.Rows
                            Dim rad As New RadioButton
                            Dim tr As New HtmlTableRow
                            Dim td As New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcUnitName")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)

                            Dim txt As New TextBox
                            txt.Text = row.Item("numUOMID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next
                    ElseIf type = "ItemGroup" Then
                        Dim ichk As Integer = 1

                        Dim objItems As New CItems
                        objItems.DomainID = Session("DomainID")
                        objItems.ItemCode = 0
                        dtTable = objItems.GetItemGroups.Tables(0)
                        txtnochkBox.Text = dtTable.Rows.Count
                        For Each row As DataRow In dtTable.Rows
                            Dim rad As New RadioButton
                            Dim tr As New HtmlTableRow
                            Dim td As New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcItemGroup")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)

                            Dim txt As New TextBox
                            txt.Text = row.Item("numItemGroupID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next
                    ElseIf type = "IncomeAccount" Or type = "AssetAcCOA" Or type = "COG" Then
                        Dim ichk As Integer = 1

                        Dim objCOA As New ChartOfAccounting
                        objCOA.DomainId = Session("DomainId")

                        Select Case type
                            Case "IncomeAccount"
                                objCOA.AccountCode = "0103"
                            Case "AssetAcCOA"
                                objCOA.AccountCode = "0101"
                            Case "COG"
                                objCOA.AccountCode = "0104"
                        End Select

                        dtTable = objCOA.GetParentCategory()

                        txtnochkBox.Text = dtTable.Rows.Count
                        For Each row As DataRow In dtTable.Rows
                            Dim rad As New RadioButton
                            Dim tr As New HtmlTableRow
                            Dim td As New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcAccountName")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)

                            Dim txt As New TextBox
                            txt.Text = row.Item("numAccountID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next
                    ElseIf type = "Currency" Then
                        Dim ichk As Integer = 1

                        Dim objCurrency As New CurrencyRates
                        objCurrency.DomainID = Session("DomainID")
                        objCurrency.GetAll = 0
                        dtTable = objCurrency.GetCurrencyWithRates()

                        txtnochkBox.Text = dtTable.Rows.Count
                        For Each row As DataRow In dtTable.Rows
                            Dim rad As New RadioButton
                            Dim tr As New HtmlTableRow
                            Dim td As New HtmlTableCell
                            rad.ID = "rad" & ichk
                            rad.GroupName = "Values"
                            rad.Text = row.Item("vcCurrencyDesc")
                            rad.CssClass = "normal1"
                            td.Controls.Add(rad)

                            Dim txt As New TextBox
                            txt.Text = row.Item("numCurrencyID")
                            txt.Style("Display") = "none"
                            txt.ID = "txt" & ichk
                            td.Controls.Add(txt)
                            td.VAlign = "top"
                            tr.Cells.Add(td)
                            tr.VAlign = "top"
                            tblHValues.Rows.Add(tr)
                            ichk = ichk + 1
                        Next
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click
            Try
                Dim ddl As String = GetQueryStringVal("ddl")
                'If ddl.Split("~")(2) = "Date Field" Or ddl.Split("~")(2) = "Date Field U" Then
                '    Response.Write("<script>opener.InserFldValue('" & FormattedDateFromDate(Calendar1.SelectedDate, Session("DateFormat")) & "','" & FormattedDateFromDate(Calendar1.SelectedDate, "YYYY/MM/DD") & "','" & GetQueryStringVal( "i") & "');self.close();</script>")
                '    Exit Sub
                'End If
                Dim ichk As Integer
                For ichk = 1 To CInt(txtnochkBox.Text)
                    Dim chk As New CheckBox
                    Dim txt As New TextBox
                    chk = CType(radOppTab.MultiPage.FindControl("rad" & ichk), CheckBox)
                    txt = CType(radOppTab.MultiPage.FindControl("txt" & ichk), TextBox)
                    If chk.Checked = True Then
                        Response.Write("<script>opener.InserFldValue('" & chk.Text & "','" & txt.Text & "','" & GetQueryStringVal("i") & "');self.close();</script>")
                        Exit For
                    End If
                Next
                Exit Sub
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
            Try
                Dim ddl As String = GetQueryStringVal("ddl")
                If ddl.Split("~")(2) = "Date Field" Or ddl.Split("~")(2) = "Date Field U" Then
                    Response.Write("<script>opener.InserFldValue('" & FormattedDateFromDate(Calendar1.SelectedDate, Session("DateFormat")) & "','" & FormattedDateFromDate(Calendar1.SelectedDate, "YYYY/MM/DD") & "','" & GetQueryStringVal("i") & "');self.close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace