<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBizDocReport.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmBizDocReport"%>
<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>BizDoc Report</title>
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
		<script language="javascript">
		function OpenSelTeam(repNo)
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenBizInvoice(a,b)
			{
				window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID='+a + '&OppBizId='+b,'','toolbar=no,titlebar=no,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
				return false;
			}
			function PopupCheck()
		{
		    document.Form1.btnGo.click()
		}
		</script>
	</HEAD>
	<body>
		
		<form id="Form1" method="post" runat="server">
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
	
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<table width="100%">
				<tr>
					<td class="normal1" nowrap>
						Sort Order
						<asp:DropDownList ID="ddlSortOrder" AutoPostBack="True" Runat="server" CssClass="normal1">
							<asp:ListItem>-- Select One --</asp:ListItem>
							<asp:ListItem>Balance Due / Days Overdue</asp:ListItem>
							<asp:ListItem>Days Overdue / Balance Due</asp:ListItem>
						</asp:DropDownList>
						&nbsp;&nbsp; Choose BizDoc
						<asp:DropDownList id="ddlChooseBizDoc" AutoPostBack="True" Runat="server" CssClass="normal1"></asp:DropDownList>
					</td>
					<td class="normal1">
						<asp:RadioButtonList ID="rdlReportType" Runat="server" AutoPostBack="True" CssClass="normal1" RepeatLayout="Table"
							RepeatDirection="Horizontal">
							<asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
							<asp:ListItem Value="2">Team Selected</asp:ListItem>
							<asp:ListItem Value="3">Territory Selected</asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
				<tr>
					<td align="right" colspan="2">
						<asp:Button ID="btnAddtoMyRtpList" Runat="server" CssClass="button" Text="Add To My Reports"></asp:Button>
					</td>
				</tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; BizDoc Report 
									&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:Button ID="btnChooseTeams" Runat="server" Width="120" CssClass="button" Text="Choose Teams"></asp:Button>&nbsp;
						<asp:Button ID="btnChooseTerritories" Runat="server" Width="120" CssClass="button" Text="Choose Territories"></asp:Button>&nbsp;
						<asp:button id="btnExportToExcel" Width="120" Runat="server" Text="Export to Excel" CssClass="button"></asp:button>&nbsp;
						<asp:Button id="btnPrint" Runat="server" CssClass="button" Text="Print" Width="50"></asp:Button>&nbsp;
						<asp:Button ID="btnBack" Runat="server" CssClass="button" Text="Back" Width="50"></asp:Button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="Table9" BorderWidth="1" CellPadding="0" CellSpacing="0" Height="400" Runat="server" CssClass="aspTable"
				Width="100%" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:DataGrid ID="dgBizDocReport" CssClass="dg" Width="100%" Runat="server" BorderColor="white"
							AutoGenerateColumns="False" AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="ContactID" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="numOppid" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="numOppBizDocsId" Visible="false"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="DueDate" HeaderText="<font color=white>Due Date</font>">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "DueDate")) %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="GrandTotal" DataFormatString="{0:#,##0.00}" SortExpression="GrandTotal" HeaderText="<font color=white>Grand Total</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="AmountPaid" DataFormatString="{0:#,##0.00}" SortExpression="AmountPaid" HeaderText="<font color=white>Amount Paid</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="BalanceDue" DataFormatString="{0:#,##0.00}" SortExpression="BalanceDue" HeaderText="<font color=white>Balance Due</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="DaysPastdue" SortExpression="DaysPastdue" HeaderText="<font color=white>Days-Pastdue</font>"></asp:BoundColumn>
								<asp:buttoncolumn CommandName="Company" DataTextField="CustomerDivision" SortExpression="CustomerDivision"
									HeaderText="<font color=white>Customer-Division</font>"></asp:buttoncolumn>
								<asp:buttoncolumn CommandName="BizDocs" DataTextField="BizDocID" SortExpression="BizDocID" HeaderText="<font color=white>BizDocID</font>"></asp:buttoncolumn>
								<asp:BoundColumn DataField="BillingTerms" SortExpression="BillingTerms" HeaderText="<font color=white>Billing Terms</font>"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
				<asp:Button ID="btnGo" Runat="server" style="display:none"></asp:Button>
			</ContentTemplate>
			<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			<asp:PostBackTrigger ControlID="btnPrint" />
			</Triggers>
			</asp:updatepanel>
		
		</form>
	</body>
</HTML>
