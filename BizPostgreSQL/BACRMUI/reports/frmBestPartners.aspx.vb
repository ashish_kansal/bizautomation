Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmBestPartners : Inherits BACRMPage

        Dim SortField As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
        Dim dtBestPartners As DataTable
        Dim objPredefinedReports As New PredefinedReports
#End Region

       

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then
                    
                    GetUserRightsForPage(8, 36)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                     ' = "Reports"
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                End If
                DisplayRecords()
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(27)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(27)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.TerritoryID = Session("TerritoryID")
                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select

                'If SortField = "" Then
                'SortField = "Employee"
                'End If

                objPredefinedReports.ReportType = 27
                dtBestPartners = objPredefinedReports.GetBestPartners
                Dim dv As DataView = New DataView(dtBestPartners)
                dv.Sort = SortField
                dgBestPartners.DataSource = dv 'dtAverageSalesCycle.DataSet
                'dgAverageSalesCycle.CssClass = "normalbol"
                'dgAverageSalesCycle.ItemStyle.CssClass = "tr1"
                dgBestPartners.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgBestPartners_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgBestPartners.SortCommand
            Try
                SortField = e.SortExpression
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        '    Dim strPath As String = Server.MapPath("") & "/Report"

        '    objPredefinedReports.ExportToExcel(dtBestPartners.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "BestPartners", Session("DateFormat"))

        '    Response.Write("<script language=javascript>")
        '    Response.Write("alert('Exported Successfully !')")
        '    Response.Write("</script>")
        '    Dim strref As String = Session("SiteType") & "//" & Request.ServerVariables("HTTP_HOST") & Request.ApplicationPath() & "/reports/Report/" & Session("UserName") & "_" & Session("UserID") & "_BestPartners.csv"


        '    Response.Write("<script language=javascript>")
        '    Response.Write("window.open('" & strref & "')")
        '    Response.Write("</script>")
        'End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Server.Transfer("../reports/frmBestPartnersPrint.aspx?fdt=" & calFrom.SelectedDate & "&tdt=" & DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) & "&Type=" & GetQueryStringVal( "Type") & "&ReportType=" & rdlReportType.SelectedIndex)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 30
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgBestPartners_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBestPartners.ItemCommand
            Try
                If e.CommandName = "Company" Then
                    If e.Item.Cells(3).Text = 1 Then
                       Response.Redirect("../Account/frmProspects.aspx?frm=BestPartners&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(2).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    ElseIf e.Item.Cells(3).Text = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=BestPartners&CmpID=" & e.Item.Cells(0).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgBestPartners, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace