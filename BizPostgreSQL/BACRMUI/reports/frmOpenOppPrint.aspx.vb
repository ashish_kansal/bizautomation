Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Partial Public Class frmOpenOppPrint : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                DisplayRecords()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function DisplayRecords()
            Try
                Dim dtOpportunityOpen As DataTable
                Dim objPredefinedReports As New PredefinedReports
                Dim SortField As String
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.TerritoryID = Session("TerritoryID")
                Select Case CCommon.ToInteger(GetQueryStringVal( "Type"))
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select
                objPredefinedReports.SalesOrPurchase = CInt(GetQueryStringVal( "SalesOrPurchase")) + 1
                objPredefinedReports.ReportType = 30
                dtOpportunityOpen = objPredefinedReports.GetOpportunityOpen
                Dim dv As DataView = New DataView(dtOpportunityOpen)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgOpportunityOpen.DataSource = dv
                dgOpportunityOpen.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace