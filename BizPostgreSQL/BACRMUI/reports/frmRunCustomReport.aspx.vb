﻿Imports BACRM.BusinessLogic.Common

Public Class frmRunCustomReport
    Inherits BACRMPage
#Region "Memeber Variables"
    Private objCustomQueryReport As CustomQueryReport
#End Region

#Region "Constructor"
    Sub New()
        Try
            objCustomQueryReport = New CustomQueryReport
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnReportID.Value = CCommon.ToLong(GetQueryStringVal("numReportID"))
                BindReportData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindReportData()
        Try
            objCustomQueryReport.ReportID = CCommon.ToLong(hdnReportID.Value)
            Dim objNewCustomQueryReport As CustomQueryReport = objCustomQueryReport.GetByID()

            If objNewCustomQueryReport.ReportID > 0 Then
                lblReportName.Text = objNewCustomQueryReport.ReportName

                Dim dtReportData As DataTable = objNewCustomQueryReport.Execute()
                rgReportData.DataSource = dtReportData
                rgReportData.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

    Private Sub btnSendEmail_Click(sender As Object, e As EventArgs) Handles btnSendEmail.Click
        Try
            Dim strFileName As String = ""
            Dim strFilePhysicalLocation As String = ""

            Dim objCustomQuery As New CustomQueryReport
            objCustomQuery.ReportID = hdnReportID.Value
            Dim dt As DataTable = objCustomQuery.Execute()

            Dim strReportHtml As String

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Dim reportDataHtml As String

                Dim strReportDataBuilder As New System.Text.StringBuilder
                strReportDataBuilder.Append("<table id='tblReportData' style='width:100%'>")

                'HEADER
                strReportDataBuilder.Append("<tr>")
                For Each myColumn As DataColumn In dt.Columns
                    strReportDataBuilder.Append("<th>")
                    strReportDataBuilder.Append(myColumn.ColumnName)
                    strReportDataBuilder.Append("</th>")
                Next
                strReportDataBuilder.Append("</tr>")

                'DATA
                For Each myRow As DataRow In dt.Rows
                    strReportDataBuilder.Append("<tr>")
                    For Each myColumn As DataColumn In dt.Columns
                        strReportDataBuilder.Append("<td>")
                        strReportDataBuilder.Append(CCommon.ToString(myRow(myColumn.ColumnName)))
                        strReportDataBuilder.Append("</td>")
                    Next
                    strReportDataBuilder.Append("</tr>")
                Next

                strReportDataBuilder.Append("</table>")

                strReportHtml = "<html><head><title></title><style type='text/css'>#tblReportData {margin: 0;padding: 0px;border-spacing: 0px;border-collapse: collapse !important;border-color: #dad8d8 !important;font-family: 'Bookman Old Style';} #tblReportData tr td {border-top: 1px solid #fff;line-height: 20px;border: 1px solid #dad8d8;padding-left: 7px;font-size: 13px;} #tblReportData th {height: 25px;background: #e5e5e5;border: 1px solid #dad8d8;font-size: 14px;font-weight: bold;text-align: center;}  #tblReportData tr:nth-child(odd) {background-color: #f2f2f2;} #tblReportData tr:nth-child(even) {background-color: #fff;}</style></head><body><table style=""width:100%""><tr><td style=""text-align:center"">Test Report</td><tr><td>" + strReportDataBuilder.ToString() + "</td></tr></table></body></html>"
            End If




            Try
                Dim objHTMLToPDF As New HTMLToPDF
                strFileName = objHTMLToPDF.ConvertHTML2PDF(strReportHtml, CCommon.ToLong(Session("DomainID")))
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                Dim objEmail As New Email 
                Dim reportName As String = "Test Report"
                Dim listAttachments As New System.Collections.Generic.List(Of Tuple(Of String, String))
                listAttachments.Add(Tuple.Create(Of String, String)(strFileName, strFilePhysicalLocation))
                objEmail.ListAttachment = listAttachments
                objEmail.SendSimpleEmail(reportName, "", "", "noreply@bizautomation.com", "sandeep@bizautomation.com", DomainID:=Session("DomainID"))
            Catch ex As Exception

            Finally
                Try
                    Dim file As New System.IO.FileInfo(strFilePhysicalLocation)
                    file.Delete()
                Catch ex As Exception

                End Try
            End Try
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class