<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAverageSalesCyclePrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmAverageSalesCyclePrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmAverageSalesCyclePrint</title>
		
		<script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
		<table id="table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</td>
					<td class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></td>
				</tr>
			</table>
			<br/>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Average Sales Cycle Report</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<tr>
								<td class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="trUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</td>
								<td class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></td>
							</tr>
							<tr>
								<td class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</td>
								<td class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<br/>
			<asp:DataGrid ID="dgAverageSalesCycle" CssClass="dg" Width="100%" CellPadding="0" CellSpacing="0"
				Runat="server" AutoGenerateColumns="false">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Employee" SortExpression="Employee" ItemStyle-HorizontalAlign="Left"
						HeaderText="<font color=white>Employee</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealsWon" SortExpression="DealsWon" HeaderText="<font color=white>DealsWon</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Closing" DataFormatString="{0:f}" SortExpression="Closing" HeaderText="<font color=white>Successful Closing %</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Amount" SortExpression="Amount" ItemStyle-HorizontalAlign="Right" HeaderText="<font color=white>Amount (Deals Won)</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<script language="JavaScript" type="text/javascript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmAverageSalesCycle.aspx";
			}
			</script>
			
		</form>
	</body>
</HTML>
