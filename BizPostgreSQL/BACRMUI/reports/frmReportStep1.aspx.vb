﻿Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Common

Public Class frmReportStep1
    Inherits BACRMPage

    Dim objReportManage As New CustomReportsManage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                BindReportModuleANDGroup()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindReportModuleANDGroup()
        Try
            Dim ds As DataSet
            objReportManage = New CustomReportsManage
            ds = objReportManage.GetReportModuleANDGroupMaster

            Dim dtModuleList As DataTable
            dtModuleList = ds.Tables(0)

            Dim dtModuleGroupList As DataTable
            dtModuleGroupList = ds.Tables(1)

            Dim str As String = ""
            Dim strGroup As String = ""

            For Each drModule As DataRow In dtModuleList.Rows

                Dim foundGroup As DataRow()
                foundGroup = dtModuleGroupList.Select("numReportModuleID=" & CCommon.ToLong(drModule("numReportModuleID")))

                strGroup = ""
                For Each drGroup As DataRow In foundGroup
                    strGroup += String.Format("<li><a href='#' id='Group_{0}'><span>{1}</span></a></li>", drGroup("numReportModuleGroupID"), drGroup("vcGroupName"))
                Next

                str += String.Format("<li><span class='folder'>{0}</span><ul id='Module_{1}'>{2}</ul></li>", drModule("vcModuleName"), drModule("numReportModuleID"), strGroup)
            Next

            litModuleGroup.Text = "<ul id='ulModuleGroup' class='filetree'>" & str & "</ul>"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            objReportManage = New CustomReportsManage

            With objReportManage
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .ReportName = txtReportName.Text.Trim
                .ReportDescription = txtReportDescription.Text.Trim
                .ReportModuleID = CCommon.ToLong(hfModuleID.Value)
                .ReportModuleGroupID = CCommon.ToLong(hfGroupID.Value)
                .tintReportType = CCommon.ToLong(ddlReportType.SelectedValue)
                .ManageReportListMaster()
            End With

            Response.Redirect("../reports/frmReportStep2.aspx?ReptID=" & objReportManage.ReportID, False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class