<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomRptList.aspx.vb"
    Inherits=".frmCustomRptList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<%@ Register  Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Report List</title>
    <script type="text/javascript" language="javascript">
        function NewReport() {
            window.location.href = "../reports/frmCustomReportWiz.aspx";
            return false;
        }
        //        function fnSortByChar(varSortChar) {
        //            document.form1.txtSortChar.value = varSortChar;
        //            if (document.form1.txtCurrrentPage != null) {
        //                document.form1.txtCurrrentPage.value = 1;
        //            }
        //            document.form1.btnGo.click();
        //        }
        function AddToMyReport(a) {

            document.form1.txtReportId.value = a;
            document.form1.btnMyReport.click();
            alert('Added To MyReports');
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12 form-group form-inline">
            <div class="col-md-8">
                <div class="pull-left">
                    Module
                <asp:DropDownList AutoPostBack="true" ID="ddlModule" runat="server" CssClass="form-control">
                </asp:DropDownList>
                    Report Description or Report Name
                <asp:TextBox ID="txtReportND" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:Button runat="server" CssClass="btn btn-primary" Text="Go" ID="btnSearch" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <asp:LinkButton ID="btnNewReport" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="true" OnClientClick="return NewReport()"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Custom Report</asp:LinkButton>
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="false"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Reports
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="table2" CellPadding="0" CellSpacing="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="red" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgReport" AllowSorting="true" runat="server" Width="100%" CssClass="table table-responsive table-bordered tblDataGrid"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="ReportID"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="vcReportName"  HeaderText="">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplRun" runat="server" Text='Run'>
                                </asp:HyperLink>&nbsp;&nbsp;
                                <asp:Label ID="Label1" runat="server" Text="  "></asp:Label>
                                <asp:HyperLink ID="hplEdit" runat="server" Text='Edit'>
                                </asp:HyperLink>&nbsp;&nbsp;
                                <asp:HyperLink ID="hplMyReport" runat="server" Text='My Report'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcReportName" SortExpression="vcReportName" HeaderText="Report Name">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcReportDescription" SortExpression="vcReportDescription"
                            HeaderText="Report Description"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick ="javascript:return DeleteRecord();" Text="X" CommandName="Delete">
                                </asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" CssClass="button" runat="server" Style="display: none" Text="Go">
    </asp:Button>
    <asp:Button ID="btnMyReport"  CssClass="button" runat="server" Style="display: none"
        Text="Go"></asp:Button>
    <asp:TextBox ID="txtReportId" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
