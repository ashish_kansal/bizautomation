Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Reports
Imports System.Data
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Partial Public Class frmCaseProjectCostingPrint : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                BindDataGrid()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDataGrid()
            Try
                Dim DivId As Long = GetQueryStringVal( "DivId")
                Dim CaseId As Long = GetQueryStringVal( "CaseId")
                Dim ProID As Long = GetQueryStringVal( "ProID")
                lblOrgName.Text = GetQueryStringVal( "DivName")
                If CaseId > 0 Then
                    lblType.Text = "Case : "
                    lblCPName.Text = GetQueryStringVal( "CaseName").Trim()
                End If
                If ProID > 0 Then
                    lblType.Text = "Project : "
                    lblCPName.Text = GetQueryStringVal( "ProName").Trim()
                End If
                Dim objPredefinedReports As New PredefinedReports
                Dim dtTable As DataTable
                objPredefinedReports.Division = DivId
                objPredefinedReports.CaseID = CaseId
                objPredefinedReports.ProID = ProID

                ' objPredefinedReports.ProID = ddlPro.SelectedValue
                objPredefinedReports.DomainID = Session("DomainId")
                dtTable = objPredefinedReports.GetCaseProjectCosting()
                If dtTable.Rows.Count > 0 Then
                    Dim amountbiled As Integer = CType(dtTable.Compute("SUM(AmountBilled)", ""), Double)
                    Dim labourrate As Integer = CType(dtTable.Compute("SUM(labourrate)", ""), Double)
                    Dim cost As Double = amountbiled - labourrate
                    If cost > 0 Then
                        lblProfit.CssClass = "normal7"
                        lblProfit.Text = cost.ToString
                    Else
                        lblProfit.CssClass = "normal12"
                        lblProfit.Text = "( " & cost.ToString & " )"
                    End If
                Else : lblProfit.Text = ""
                End If
                dgCosting.DataSource = dtTable
                dgCosting.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace