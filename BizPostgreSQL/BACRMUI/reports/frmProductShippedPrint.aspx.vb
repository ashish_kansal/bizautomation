Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmProductShippedPrint : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblreportby As System.Web.UI.WebControls.Label
        Protected WithEvents lblcurrent As System.Web.UI.WebControls.Label
        Protected WithEvents lblReportHeader As System.Web.UI.WebControls.Label
        Protected WithEvents Label3 As System.Web.UI.WebControls.Label
        Protected WithEvents lblfromdt As System.Web.UI.WebControls.Label
        Protected WithEvents Label4 As System.Web.UI.WebControls.Label
        Protected WithEvents lbltodt As System.Web.UI.WebControls.Label
        Protected WithEvents dgItems As System.Web.UI.WebControls.DataGrid
        Protected WithEvents dgProductRec As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                'Set teh dates to a range of one week ending today.
                lbltodt.Text = FormattedDateFromDate(GetQueryStringVal("tdt"), Session("DateFormat"))
                lblfromdt.Text = FormattedDateFromDate(GetQueryStringVal("fdt"), Session("DateFormat"))
                Dim SortField As String
                SortField = GetQueryStringVal("Sort")
                Dim objPredefinedReports As New PredefinedReports
                Dim dtProducts As DataTable
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lblfromdt.Text))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lbltodt.Text))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportType = 25
                objPredefinedReports.SortOrder = GetQueryStringVal("Filter")
                dtProducts = objPredefinedReports.GetRepProductRecievedorShipped
                dgProductRec.DataSource = dtProducts
                dgProductRec.DataBind()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
