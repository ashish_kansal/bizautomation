﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmReportStep2.aspx.vb" Inherits=".frmReportStep2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Create New Report</title>
    
    <script src="https://code.jquery.com/ui/1.12.0-rc.1/jquery-ui.min.js" type="text/javascript"></script>
    <link href="jqTree/Report.css" rel="stylesheet" />
    <link href="jqTree/jquery.treeview.css" rel="stylesheet" />
    <script src="jqTree/jquery.treeview.js" type="text/javascript"></script>
    <script src="jqTree/underscore.js" type="text/javascript"></script>
    <script src="jqTree/Reports.js" type="text/javascript"></script>
    <script src="jqTree/FilterLogicParser.js" type="text/javascript"></script>
    <script src="jqTree/jp.js" type="text/javascript"></script>
    <script src="jqTree/ui.dropdownchecklist.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/dateFormat.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        var strSearchConst = 'Quick Find';
        var thSelIndex = -1;
        var thSelGroupIndex = -1;

        var isValidDrop = true;
        var isGroupChange = false;
        var InputCondition = "";
        var NewFilterCondition = false;
        var FilterRemoveIndex = -1;
        var summaryGrpClass = "";

        var responseJson = null;
        var data = [];
        var arrAggregateSum = new Array();

        var boolInitial = true;

        $(document).ready(function () {
            InitializeValidation();

            BindTreeControl();

            BindDateFieldFilter();

            $("#browser").treeview({
                control: "#treecontrol"
            });

            LoadReportControls(true);
            boolInitial = false;

            JQueryOnLoad();

            $("[id$=txtNoRows]").change(function () {
                var max = parseInt($(this).attr('max'));
                if ($(this).val() > max) {
                    $(this).val(max);
                }
            });     
        });

        function Save() {
            GetReportResponse(true, false);

            return false;
        }

        function SaveAndRun() {
            GetReportResponse(true, false);
            window.open("../reports/frmCustomReportRun.aspx?ReptID=" + $("[id$=hfReportID]").val(), "_blank");
            return false;
        }

        function Close() {
            window.location.href = "../reports/frmCustomReportList.aspx";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="col-md-12" style="z-index:19">
        <div class="row">
          <center>  <asp:Label ID="lblmsg" runat="server" Width="750px"></asp:Label></center>
        </div>
    <div class="pull-left">
        <div class="form-inline">
            <label>Report Name</label>
            <asp:TextBox ID="txtReportName" runat="server"  CssClass="signup form-control" MaxLength="100"></asp:TextBox>
            <label>Report Description</label>
            <asp:TextBox ID="txtReportDescription" runat="server" CssClass="signup form-control" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
        <div class="pull-right">
            <div class="form-inline">
                <asp:Button ID="btnRunReport" runat="server" Text="Run Report" CssClass="btn btn-primary" OnClientClick="javascript:return SaveAndRun();"></asp:Button>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="javascript:return Save();"></asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-danger" Text="Close" OnClientClick="javascript:return Close();"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    <asp:Label ID="lblReportName" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div style="background-color: #fff;">
        <asp:Table ID="tblReport" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
            GridLines="None">
            <asp:TableRow>
                <asp:TableCell Width="18%" VerticalAlign="Top">
                    <table width="100%">
                        <tr style="background-color: #F2F2F2">
                            <td>
                                <strong>Fields</strong>
                            </td>
                            <td align="right">
                                <input type="submit" id="btnFieldSearchAll" class="btn btn-default" value="All" />
                                <a href="#" id="btnFieldSearchLetter" class="btn btn-default "><i class="fa fa-font"></i></a>
                                <a href="#" id="btnFieldSearchNumber" class="btn btn-default"><i class="fa fa-sort-numeric-desc"></i></a>
                                <a href="#" id="btnFieldSearchDate" class="btn btn-default"><i class="fa fa-calendar"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="container">
                                    <div class="SearchBox">
                                        <div id="SearchForm">
                                            <input type="submit" id="btnGo" class="buttonGo" value="" />
                                            <asp:TextBox ID="txtSearchGroup" runat="server" CssClass="Searchinput" MaxLength="20"
                                                Text="Quick Find"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="treeDiv" style="overflow: auto; min-width: 250px; height: 500px;">
                                        <ul id="browser" class="filetree">
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                      <table width="100%" class="table table-responsive">
            <tr style="background-color: #F2F2F2">
                <td>
                    <table>
                        <tr >
                            <td colspan="2"><strong>Filters</strong>
                                <input type="submit" id="AddNewFilter" value="Add" class="buttonNew btn btn-default" />
                                <div class="thFilterMenu"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%"><strong>Show</strong>   <select id="ddlRecordFilter" class="form-control">
                                <option value="0">All Records</option>
                                <option value="1">User logged in (Assigned-To/Record Owner)</option>
                                <option value="4">User logged in (Assigned-To)</option>
                                <option value="5">User logged in (Record Owner)</option>
                                <option value="2">User logged in (Team Scope)</option>
                                <option value="3">User logged in (Territory Scope)</option>
                                </select>
                            </td>
                            <td id="tdNoRows">
                                <strong>Number of Records to Display <i>(Max 100)</i></strong> <input type="number" id="txtNoRows" autocomplete="off" max="100" class="form-control"/>
                            </td>
                            <td id="tdHideSummaryDetail">
                                <input type="checkbox" id="bitHideSummaryDetail"/> <label for="bitHideSummaryDetail">Hide Detail Summary</label>
                            </td>
                        </tr>
                        <tr style="background-color: #F2F2F2" >
                            <td colspan="3"><strong><span id="lblDateCaption">Date Field</span></strong>
                                <select id="ddlDateFieldFilter" class="form-control">
                                    </select>
                                <span id="lblDateRangeCaption">Range</span> <select id="ddlDateFieldRange" class="form-control">
                                    </select>
                                <span id="spnDateCustom">
                                    <ul class="list-inline" style="margin-top:10px">
                                        <li><b>From</b></li>
                                        <li><input type="text" class="form-control" id="txtFilter_FromDate" autocomplete="off"/></li>
                                        <li><img id="imgFilter_FromDate" class="hyperlink" style="border-width:0px;" src="../Images/Calender.gif"></li>
                                        <li>&nbsp;&nbsp;</li>
                                        <li><b>To</b></li>
                                        <li><input type="text"  class="form-control" id="txtFilter_ToDate" autocomplete="off"/></li>
                                        <li><img id="imgFilter_ToDate" class="hyperlink" style="border-width:0px;" src="../Images/Calender.gif"></li>
                                        <li>&nbsp;&nbsp;</li>
                                        <li><input type="submit" id="btnDateFieldFilter" value="Ok" class="buttonNew btn btn-default" /></li>
                                    </ul>
                                </span>
                            </td>
                        </tr>
                        <tr style="background-color: #F2F2F2;display: none" id="trKPIScoreCard">
                            <td valign="top" ><strong>Measure Type</strong>
                                <select id="ddlKPIMeasureType" class="form-control">
</select>
                            </td>
                             <td id="tdKPI">
                                <strong>Threshold</strong> <select id="ddlKPIThreshold" class="form-control">
                                     <option value="lt">less than</option>
                                     <option value="gt">greater than</option>
                                     <option value="le">less or equal</option>
                                     <option value="ge">greater or equal</option>
                                     </select> <input type="text" id="txtKPIThreshold" autocomplete="off" maxlength="12" class="form-control required_decimal {required:false ,number:true, maxlength:12, messages:{number:'provide valid value!'}}"/>
                            </td>
                            <td id="trScoreCard" valign="top">
                                <ul>
                                    <li>Display <img src='../images/Circle_Green_16.png'/> when value has met or exceeded goal <input type="text" id="txtScoreCardThreshold1" autocomplete="off" maxlength="12" class="form-control required_decimal {required:false ,number:true, maxlength:12, messages:{number:'provide valid value!'}}"/></li>
                                    <li>
                                Display <img src='../images/Circle_Yellow_16.png'/> when value has met or exceeded warning <input type="text" id="txtScoreCardThreshold2" autocomplete="off" maxlength="12" class=" form-control required_decimal {required:false ,number:true, maxlength:12, messages:{number:'provide valid value!'}}"/>
                                    </li>
                                    <li>
                                Display <img src='../images/Circle_Red_16.png'/> otherwise 
                                    </li>
                                </ul>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="FilterLogicSection" style="padding-left: 50px;"></div>
                    <div id="fieldFiltersSection" style="height: 100px; padding-left: 50px;"></div>
                </td>
            </tr>
        </table>

        <table width="100%" class="table table-responsive">
            <tr>
                <td style="background-color: #F2F2F2">
                   <span id="spnPreviewCaption" style="font-weight:bold;">Preview</span>&nbsp;&nbsp;
                                  <a href="#" id="ReportLayoutFormatType" style="font-weight: bold;">Report Format Type</a>&nbsp;&nbsp;
                    <a href="#" id="RemoveAllColumns">Remove All Columns</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="ReportResult" style="min-height:300px;">
                        <div id="JCLRgrips"></div>
                        <div id="JCLRgripsGrp"></div>
                        <table id="GridTable" class="tbl JColResizer  table table-responsive table-bordered">
                            <thead>
                                <tr class="HeaderList">
                                    <%--<th id="DragHelp">Add columns by dragging fields into the preview pane.</th>--%>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <table id="MatrixTable" class="dgNHover JColResizer">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <div id="GridMenu" style="display: none">
                            <ul class="x-menu-list">
                                <li class="x-menu-list-item">
                                    <a href="#" id="menuSortAscending">
                                        <img class="menu-icon menu-sortAsc" />
                                        <span class="x-menu-item-text">Sort Ascending</span>
                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="menuSortDescending">
                                        <img class="menu-icon menu-sortDesc" />
                                        <span class="x-menu-item-text">Sort Descending</span>
                                    </a>
                                </li>
                                <li class="x-menu-sep">
                                    <span class="menu-sep"></span>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="menuGroupColumn">
                                        <img class="menu-icon menu-Group" />
                                        <span class="x-menu-item-text">Group by this Field</span>
                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="menuSummarizeColumn">
                                        <img class="menu-icon menu-Group" />
                                        <span class="x-menu-item-text">Summarize by this Field</span>
                                    </a>
                                </li>
                                <li class="x-menu-sep">
                                    <span class="menu-sep"></span>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="menuRemoveColumn">
                                        <img class="menu-icon menu-remove" />
                                        <span class="x-menu-item-text">Remove Column</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="GroupMenu" style="display: none">
                            <ul class="x-menu-list">
                                <%--<li class="x-menu-list-item">
                                <a href="#" id="GroupSortAscending">
                                    <img class="menu-icon menu-sortAsc" />
                                    <span class="x-menu-item-text" >Sort Group Ascending</span>
                                </a>
                            </li>
                            <li class="x-menu-list-item">
                                <a href="#" id="GroupSortDescending">
                                    <img class="menu-icon menu-sortDesc" />
                                    <span class="x-menu-item-text" >Sort Group Descending</span>
                                </a>
                            </li>
                            <li class="x-menu-sep">
                                <span  class="menu-sep"> </span>
                            </li>--%>
                                <li class="x-menu-list-item">
                                    <a href="#" id="GroupMoveUp">
                                        <img class="menu-icon menu-GroupMoveUp" />
                                        <span class="x-menu-item-text">Move Group Up</span>
                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="GroupMoveDown">
                                        <img class="menu-icon menu-GroupMoveDown" />
                                        <span class="x-menu-item-text">Move Group Down</span>
                                    </a>
                                </li>
                                <li class="x-menu-sep">
                                    <span class="menu-sep"></span>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="GroupRemove">
                                        <img class="menu-icon menu-remove" />
                                        <span class="x-menu-item-text">Remove Group</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="LayoutFormatMenu" style="display: none">
                            <ul class="x-menu-list">
                                <li class="x-menu-list-item">
                                    <a href="#" id="LayoutFormatTabular">
                                        <img class="menu-icon menu-Tabular" />
                                        <span class="x-menu-item-text">Tabular</span>
                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="LayoutFormatSummary">
                                        <img class="menu-icon menu-Summary" />
                                        <span class="x-menu-item-text">Summary</span>
                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="LayoutFormatMatrix">
                                        <img class="menu-icon menu-Matrix" />
                                        <span class="x-menu-item-text">Matrix</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div id="FilterMenu" style="display: none">
                            <ul class="x-menu-list">
                                <li class="x-menu-list-item">
                                    <a href="#" id="FilterAddNewCondition">
                                        <img class="menu-icon menu-FieldFilter" />
                                        <span class="x-menu-item-text">Field Filter
                                            <br />
                                            <span class="example-text">e.g., Account Name
                                            <b>equals</b>
                                                Acme
                                            </span>
                                        </span>

                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="FilterLogic">
                                        <img class="menu-icon menu-FilterLogic" />
                                        <span class="x-menu-item-text">Filter Logic
                                    <br />
                                            <span class="example-text">e.g., Filter 1
                                            <b>AND</b>
                                                (Filter 2
                                            <b>OR</b>
                                                Filter 3)
                                            </span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div id="MatrixBreakMenu" style="display: none">
                            <ul class="x-menu-list">
                                <li class="x-menu-list-item">
                                    <a href="#" id="MatrixBreakMenuRemoveGroup">
                                        <img class="menu-icon menu-remove" />
                                        <span class="x-menu-item-text">Remove Group</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div id="aggItemMenu" style="display: none">
                            <ul class="x-menu-list">
                                <li class="x-menu-list-item">
                                    <a href="#" id="MatrixSummarizeField">
                                        <img class="menu-icon menu-SummarizeField" />
                                        <span class="x-menu-item-text">Summarize this Field...</span>
                                    </a>
                                </li>
                                <li class="x-menu-list-item">
                                    <a href="#" id="MatrixRemoveSummary">
                                        <img class="menu-icon menu-remove" />
                                        <span class="x-menu-item-text">Remove Summary</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div id="dialog-form" title="Summarize">
                            <form>
                                <fieldset>
                                    <table width="100%">
                                        <tr>
                                            <td>Field</td>
                                            <td>Sum</td>
                                            <td>Average</td>
                                            <td>Max</td>
                                            <td>Min</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span id="spnMatrixSummaryFieldLabel"></span>
                                                <input id="hdnMatrixSummaryFieldID" type="hidden" name="hdnMatrixSummaryFieldID">
                                            </td>
                                            <td>
                                                <input id="chkSum" type="checkbox" name="chkSum">
                                            </td>
                                            <td>
                                                <input id="chkAverage" type="checkbox" name="chkAverage"></td>
                                            <td>
                                                <input id="chkMax" type="checkbox" name="chkMax"></td>
                                            <td>
                                                <input id="chkMin" type="checkbox" name="chkMin"></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <asp:HiddenField ID="hfReportID" runat="server" />
    <asp:HiddenField ID="hfDateFormat" runat="server" />
    <asp:HiddenField ID="hfValidDateFormat" runat="server" />
</asp:Content>
