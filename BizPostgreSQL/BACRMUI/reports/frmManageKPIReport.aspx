﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmManageKPIReport.aspx.vb" Inherits=".frmManageKPIReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>KPI Report List</title>
    <style>
        .StepClass
        {
            color: Black;
            font-size: 12px;
            font-family: Arial; /*font-weight:bold;*/
        }
    </style>
    <script language="javascript" type="text/javascript">
        function Save() {

            if (document.getElementById("txtReportName").value == "") {
                alert("Enter KPI Group Name");
                document.getElementById('txtReportName').focus();
                return false;
            }

            return true;
        }

        function Close() {
            window.location.href = "../reports/frmKPIReportList.aspx",false;
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected Report?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function AddReport() {
            if (document.getElementById("ddlCustomReport").value == "0") {
                alert("Select Report");
                document.getElementById('ddlCustomReport').focus();
                return false;
            }

            return true;
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                  <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="javascript:return Save();" ><i class="fa fa-floppy-o"></i>&nbsp;Save</asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"
                            OnClientClick="javascript:return Save();" ><i class="fa fa-floppy-o"></i>&nbsp;Save & Close</asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="javascript:return Close();"><i class="fa fa-times-circle-o"></i>&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Add KPI or Scorecard reports to form a group portlet
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div style="background-color: #fff">
        <div class="row">
            <div class="col-md-12 form-group">
                <div class="col-md-3">
                    <label>Group Name</label>
                </div>
                <div class="col-md-4 form-group">
                    <asp:TextBox ID="txtReportName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
                <div class="clearfix"></div>
                 <div class="col-md-3">
                    <label>Description</label>
                </div>
                <div class="col-md-4 form-group">
                    <asp:TextBox ID="txtReportDescription" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3">
                    <label>Type</label>
                </div>
                <div class="col-md-4 form-group">
                   <asp:RadioButtonList ID="rdbType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="3" Selected="True">KPI</asp:ListItem>
                        <asp:ListItem Value="4">ScoreCard</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <asp:Panel ID="pnlStep2" runat="server" Visible="false">
            <div class="row">
                <div class="col-md-12 form-group">
                    <label style="font-size:14px;"> KPI Score Card Reports</label>
                    <hr />
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 form-group form-inline">
                    <div class="col-md-3">
                        <label>Available Reports</label>
                    </div>
                    <div class="col-md-4 form-group">
                        <asp:DropDownList ID="ddlCustomReport" runat="server" CssClass="form-control">
                        </asp:DropDownList> &nbsp;
                        <asp:Button ID="btnAddReport" runat="server" Text="Add Report" CssClass="btn btn-primary" OnClientClick="javascript:return AddReport();" />
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div class="col-md-12 form-group">

                     <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false"
                            CssClass="table table-responsive table-bordered" Width="100%" DataKeyNames="numReportID">
                            <AlternatingRowStyle CssClass="ais" />
                            <RowStyle CssClass="is" />
                            <HeaderStyle CssClass="hs" />
                            <Columns>
                                <asp:BoundField DataField="vcModuleName" HeaderText="Module"></asp:BoundField>
                                <asp:BoundField DataField="vcGroupName" HeaderText="Type"></asp:BoundField>
                                <asp:BoundField DataField="vcReportName" SortExpression="vcReportName" HeaderText="Report Name"></asp:BoundField>
                                <asp:BoundField DataField="vcReportDescription" SortExpression="vcReportDescription"
                                    HeaderText="Report Description"></asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick="javascript:return DeleteRecord();" Text="X" CommandName="DeleteReport"></asp:Button>
                                        <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
