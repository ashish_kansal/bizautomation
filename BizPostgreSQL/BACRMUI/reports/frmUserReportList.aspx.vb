﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmUserReportList
    Inherits BACRMPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindUserReportList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindUserReportList()
        Try
            Dim objReportManage As New CustomReportsManage

            Dim dtUserReportList As DataTable
            Dim i As Integer
            Dim objCell As TableCell
            Dim objRow As TableRow
            Dim hplLink As HyperLink
            objReportManage.DomainID = Session("DomainId")
            objReportManage.UserCntID = Session("UserContactID")
            dtUserReportList = objReportManage.GetUserReportList
            For i = 0 To dtUserReportList.Rows.Count - 1
                objRow = New TableRow
                objCell = New TableCell
                objCell.Text = i + 1 & " )"
                objCell.CssClass = "normal1"
                objCell.HorizontalAlign = HorizontalAlign.Right
                objRow.Cells.Add(objCell)

                objCell = New TableCell
                hplLink = New HyperLink
                hplLink.CssClass = "hyperlink"
                objCell.CssClass = "normal1"
                hplLink.Text = dtUserReportList.Rows(i).Item("RPtHeading") & " - " & dtUserReportList.Rows(i).Item("RptDesc")
                hplLink.NavigateUrl = "../reports/" & dtUserReportList.Rows(i).Item("vcFileName")
                objCell.Controls.Add(hplLink)
                objRow.Cells.Add(objCell)
                tblReortLinks.Rows.Add(objRow)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class