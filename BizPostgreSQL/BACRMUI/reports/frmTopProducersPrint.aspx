<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmTopProducersPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmTopProducersPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmTopProducersPrint</title>
	
		
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Top Producers Report</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
				<asp:datagrid id="dgTopProducers" CellPadding="0" CellSpacing="0" Runat="server" Width="100%"
				AutoGenerateColumns="False" CssClass="dg">
				<AlternatingItemStyle CssClass="is"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Employee" SortExpression="Employee" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Employee Name</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="ProspectsOwned" ItemStyle-HorizontalAlign="Left" SortExpression="ProspectsOwned"
						HeaderText="<font color=white>Prospects Owned</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="AccountsOwned"  SortExpression="AccountsOwned"
						HeaderText="<font color=white>Accounts Owned</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="OpportunitiesOpened" SortExpression="OpportunitiesOpened" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>Opportunities Opened</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealsWon" SortExpression="DealsWon" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>Deals Won</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealsLost" SortExpression="DealsLost" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>Deals Lost</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="PercentageWon" SortExpression="PercentageWon" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>% Won</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="AmountWon" SortExpression="AmountWon" ItemStyle-HorizontalAlign="center"
						HeaderText="<font color=white>Amount Won</font>"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			<script language="JavaScript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmTopProducers.aspx";
			}
			</script>
		</form>
	</body>
</HTML>
