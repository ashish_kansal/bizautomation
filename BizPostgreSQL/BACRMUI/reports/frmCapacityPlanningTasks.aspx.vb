﻿Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports

    Public Class frmCapacityPlanningTasks
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    rdpDate.SelectedDate = DateTime.UtcNow.AddMinutes(-1 * CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('reports/frmcapacityplanningresources.aspx?type=" & GetQueryStringVal("type") & "')")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub


        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class

End Namespace