﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CommissionReports.aspx.vb"
    Inherits=".CommissionReports" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Commission Item Report</title>
    <script language="javascript" type="text/javascript">
        function Save() {

            if (document.form1.hfCommAppliesTo.value == "1") {
                if ($find('radItem').get_value() == "") {
                    alert("Select Item")
                    return false;
                }
            }
            else if (document.form1.hfCommAppliesTo.value == "2") {
                if (document.form1.ddlItemClassification.value == "0") {
                    alert("Select Item Classification")
                    document.form1.ddlItemClassification.focus()
                    return false;
                }
            }

            if (document.form1.ddlReportOwner.value == "0") {
                alert("Select Report Owner")
                document.form1.ddlReportOwner.focus()
                return false;
            }
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Commission Item Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="100%"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-inline">
                         <asp:Label ID="lblName" runat="server" Text="Item" CssClass="signup"></asp:Label>
                                        <asp:HiddenField ID="hfCommAppliesTo" runat="server" />
                        <telerik:RadComboBox ID="radItem" runat="server" Width="150" DropDownWidth="200px"
                                            EnableLoadOnDemand="true" Visible="false" ClientIDMode="Static">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                        </telerik:RadComboBox>
                                        <asp:DropDownList ID="ddlItemClassification" runat="server" CssClass="form-control" Visible="false">
                                        </asp:DropDownList>
                        Report Owner
                         <asp:DropDownList ID="ddlReportOwner" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1">Assign To</asp:ListItem>
                                            <asp:ListItem Value="2">Record Owner</asp:ListItem>
                                        </asp:DropDownList>
                            
                        <div id="tdOrganization" runat="server" visible="false">Organization &nbsp;
                             <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary"></asp:Button>
                        </div>
                    </div>
                        </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <asp:GridView ID="gvCommissionsReports" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="table table-responsive table-bordered" Width="100%" DataKeyNames="numComReports">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <asp:BoundField DataField="vcCompanyName" Visible="true" HeaderText="Organization" />
                                    <asp:BoundField DataField="vcItemName" Visible="true" HeaderText="Item" />
                                    <asp:BoundField DataField="ReportOwner" Visible="true" HeaderText="ReportOwner" />
                                    <asp:TemplateField HeaderStyle-Width="10" ItemStyle-Width="10">
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="DeleteReport"
                                                CommandArgument='<%# Eval("numComReports")  %>'></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                    </div>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
