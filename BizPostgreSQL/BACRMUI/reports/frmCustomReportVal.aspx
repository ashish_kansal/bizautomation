<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomReportVal.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmCustomReportVal" MasterPageFile ="~/common/Popup.Master"%>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
     
    <link href="~/images/BizSkin/TabStrip.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Report Field Values</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
           <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:textbox id="txtnochkBox" text="0" runat="server" style="display: none">
    </asp:textbox>
    <table width="100%">
        <tr>
            <td align="right">
                <asp:button runat="server" id="btnInsert" text="Insert" cssclass="button" />
                &nbsp;
                <asp:button text="Close" cssclass="button" onclientclick="javascript:self.close()"
                    id="btnClose" runat="server" />
               &nbsp;
            </td>
        </tr>
    </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
   Field Value
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
  <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true" Skin="Default" 
        ClickSelectedTab="True" AutoPostBack="false" SelectedIndex="0" MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="Field Values&nbsp;&nbsp;" Value="FieldValues" PageViewID="radPageView_FieldValues">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView"
        >
        <telerik:RadPageView ID="radPageView_FieldValues" runat="server">
            <asp:table id="tblValues" runat="server" borderwidth="1" width="500px" gridlines="None"
                bordercolor="black" cssclass="aspTable" height="300">
                <asp:tablerow verticalalign="Top">
                    <asp:tablecell verticalalign="Top">
                        <table id="tblHValues" runat="server" valign="top">
                        </table>
                    </asp:tablecell>
                </asp:tablerow>
                <asp:tablerow verticalalign="Top">
                    <asp:tablecell horizontalalign="Center">
                        <asp:panel runat="server" id="pnlCal">
                            <table width="220px" align="center" bgcolor="#c6d3e7">
                                <tr>
                                    <td align="center">
                                        <asp:linkbutton id="btnFirst" style="text-decoration: none" accesskey="P" runat="server"
                                            font-names="Webdings" forecolor="Black" causesvalidation="False" tooltip="Previous Year"
                                            enableviewstate="False">9</asp:linkbutton>
                                        <asp:linkbutton id="btnPrev" style="text-decoration: none" runat="server" font-names="Webdings"
                                            forecolor="Black" tooltip="Previous Month" enableviewstate="False">3</asp:linkbutton>
                                        <asp:dropdownlist id="ddlMonth" runat="server" font-names="Arial" font-size="11px"
                                            forecolor="White" width="80px" backcolor="#8ca2bd" font-bold="True" autopostback="True"
                                            cssclass="slt9Normal">
                                            <asp:listitem value="1">January</asp:listitem>
                                            <asp:listitem value="2">February</asp:listitem>
                                            <asp:listitem value="3">March</asp:listitem>
                                            <asp:listitem value="4">April</asp:listitem>
                                            <asp:listitem value="5">May</asp:listitem>
                                            <asp:listitem value="6">June</asp:listitem>
                                            <asp:listitem value="7">July</asp:listitem>
                                            <asp:listitem value="8">August</asp:listitem>
                                            <asp:listitem value="9">September</asp:listitem>
                                            <asp:listitem value="10">October</asp:listitem>
                                            <asp:listitem value="11">November</asp:listitem>
                                            <asp:listitem value="12">December</asp:listitem>
                                        </asp:dropdownlist>
                                        <asp:dropdownlist id="ddlYear" font-size="11px" runat="server" font-names="Arial"
                                            forecolor="White" width="55px" backcolor="#8ca2bd" font-bold="True" autopostback="True"
                                            cssclass="slt9Normal">
                                        </asp:dropdownlist>
                                        <asp:linkbutton id="btnNext" style="text-decoration: none" runat="server" font-names="Webdings"
                                            forecolor="Black" tooltip="Next Month" enableviewstate="False">4</asp:linkbutton>
                                        <asp:linkbutton id="btnLast" style="text-decoration: none" runat="server" font-names="Webdings"
                                            forecolor="Black" tooltip="Next Year" enableviewstate="False">:</asp:linkbutton>
                                        <asp:linkbutton id="LinkButton1" font-bold="true" onclientclick="javascript:self.close()"
                                            forecolor="black" font-size="10px" font-names="Arial" runat="server">X</asp:linkbutton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:calendar id="Calendar1" borderwidth="0" runat="server" forecolor="#00000" width="220px"
                                            font-size="9" height="167px" showtitle="False" daynameformat="FirstTwoLetters"
                                            selectionmode="Day" font-name="arial" selecteddaystyle-backcolor="red" othermonthdaystyle-forecolor="white">
                                            <daystyle font-size="10pt" forecolor="DimGray" backcolor="white"></daystyle>
                                            <nextprevstyle font-size="10pt" font-names="arial" font-bold="True" forecolor="White"
                                                backcolor="#00A2FF"></nextprevstyle>
                                            <dayheaderstyle forecolor="Black" backcolor="#8ca2bd"></dayheaderstyle>
                                            <titlestyle font-size="10pt" font-names="arial" font-bold="True" forecolor="White"
                                                backcolor="#00A2FF"></titlestyle>
                                            <othermonthdaystyle forecolor="Silver"></othermonthdaystyle>
                                            <selecteddaystyle backcolor="DimGray" />
                                        </asp:calendar>
                                    </td>
                                </tr>
                            </table>
                        </asp:panel>
                    </asp:tablecell>
                </asp:tablerow>
            </asp:table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>

