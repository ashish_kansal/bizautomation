<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmMonthToMonthComparisonPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmMonthToMonthComparisonPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmMonthToMonthComparisonPrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Month To Month Comparison Report</asp:Label>
					</td>
				</tr>
			</table>
			<BR>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<asp:table id="Table9" BorderWidth="1" Height="400" Runat="server" Width="100%" BorderColor="black"
				GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:table runat="server" id="tblData" Height="30px" CellPadding="0" CellSpacing="1" BorderWidth="0px">
							<asp:TableRow Height="20px" HorizontalAlign="Left"></asp:TableRow>
						</asp:table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<script language="JavaScript">
				function Lfprintcheck()
				{
					this.print()
					//history.back()
					document.location.href = "frmMonthToMonthComparison.aspx";
				}
			</script>
		</form>
	</body>
</html>
