<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="ActivityReportOpportunity.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.ActivityReportOpportunity" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Activity Report</title>
    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
 
    .hs {
        border: 1px solid #e4e4e4;
        border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    text-align: center;
    font-weight:bold;
        }
           </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Reports
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dgSales" AllowSorting="True" runat="server" Width="100%" CssClass="dg table table-responsive table-bordered"
        AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="False" DataField="numOppId" HeaderText="numOppId"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCompanyID" HeaderText="numCompanyID">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numTerID" HeaderText="numTerID"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numDivisionID" HeaderText="numDivisionID">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numCreatedBy" HeaderText="numCreatedBy">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="tintCRMType" HeaderText="tintCRMType">
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="numContactID" HeaderText="numContactID">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="OppType" SortExpression="OppType" HeaderText="Type">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Company" SortExpression="Company" HeaderText="Customer">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Contact" SortExpression="Contact" HeaderText="Contact">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vcusername" SortExpression="vcusername" HeaderText="Account Mgr">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="STAGE" SortExpression="STAGE" HeaderText="Status"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Due Date" SortExpression="CloseDate">
                <ItemTemplate>
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="monPAmount" DataFormatString="{0:#,###.00}" SortExpression="monPAmount"
                HeaderText="Amount"></asp:BoundColumn>
        </Columns>
        <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
        </PagerStyle>
    </asp:DataGrid>
    <asp:TextBox ID="txtTotalPageSales" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsSales" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortCharSales" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPageSales" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
