Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class ActivityReportOpportunity : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCurrrentPageSales As System.Web.UI.WebControls.TextBox
        Protected WithEvents Table1 As System.Web.UI.WebControls.Table
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents dgSales As System.Web.UI.WebControls.DataGrid
        Protected WithEvents txtTotalPageSales As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtTotalRecordsSales As System.Web.UI.WebControls.TextBox
        Dim strColumn As String
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then
                    ' = "Reports"
                    txtCurrrentPageSales.Text = 1
                    BindDatagrid()
                End If
                'If txtSortCharSales.Text <> "" Then
                '    BindDatagrid()
                '    txtSortCharSales.Text = ""
                'End If
                litMessage.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities
                With objOpportunity
                    .UserCntID = IIf(GetQueryStringVal("UserCntID") <> "", GetQueryStringVal("UserCntID"), Session("UserContactID"))
                    .TeamType = 5
                    .DomainID = Session("DomainID")

                    'If txtCurrrentPageSales.Text.Trim <> "" Then
                    '    .CurrentPage = txtCurrrentPageSales.Text
                    'Else : .CurrentPage = 1
                    'End If
                    If txtCurrrentPageSales.Text.Trim = "" Then txtCurrrentPageSales.Text = 1
                    .CurrentPage = txtCurrrentPageSales.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    'If strColumn <> "" Then
                    '    .columnName = strColumn
                    'Else : .columnName = "opp.bintCreatedDate"
                    'End If
                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "opp.bintcreateddate"
                    End If


                    'If Session("Asc") = 1 Then
                    '    .columnSortOrder = "Asc"
                    'Else : .columnSortOrder = "Desc"
                    'End If
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    If GetQueryStringVal("RepType") = "O" Then
                        .OppStatus = 0
                    ElseIf GetQueryStringVal("RepType") = "W" Then
                        .OppStatus = 1
                    ElseIf GetQueryStringVal("RepType") = "L" Then
                        .OppStatus = 2
                    End If

                    .RepType = GetQueryStringVal("Type")
                    .startDate = GetQueryStringVal("stDate")
                    .endDate = GetQueryStringVal("enDate")
                End With
                dtOpportunity = objOpportunity.GetPerAnalysysisOppList
                If objOpportunity.TotalRecords = 0 Then
                    'lblRecordsSales.Text = 0
                Else
                    'lblRecordsSales.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
                    'Dim strTotalPage As String()
                    'Dim decTotalPage As Decimal
                    'decTotalPage = lblRecordsSales.Text / Session("PagingRows")
                    'decTotalPage = Math.Round(decTotalPage, 2)
                    'strTotalPage = CStr(decTotalPage).Split(".")
                    'If (lblRecordsSales.Text Mod Session("PagingRows")) = 0 Then
                    '    lblTotalSales.Text = strTotalPage(0)
                    '    txtTotalPageSales.Text = strTotalPage(0)
                    'Else
                    '    lblTotalSales.Text = strTotalPage(0) + 1
                    '    txtTotalPageSales.Text = strTotalPage(0) + 1
                    'End If
                    'txtTotalRecordsSales.Text = lblRecordsSales.Text
                End If

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpportunity.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPageSales.Text

                dgSales.DataSource = dtOpportunity
                dgSales.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPageSales.Text = bizPager.CurrentPageIndex
                BindDatagrid()
                'txtCurrrentPage_TextChanged(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgSales_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgSales.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    'Session("Asc") = 0
                    txtSortOrder.Text = "A"
                Else
                    'If Session("Asc") = 0 Then
                    '    Session("Asc") = 1
                    'Else : Session("Asc") = 0
                    'End If
                    If txtSortOrder.Text = "A" Then
                        txtSortOrder.Text = "D"
                    Else : txtSortOrder.Text = "A"
                    End If
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace