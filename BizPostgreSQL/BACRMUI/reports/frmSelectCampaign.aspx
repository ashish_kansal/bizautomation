<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSelectCampaign.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmSelectCampaign"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Select Campaign</title>
		<script language="javascript">
			function MoveUp(tbox)
			{
			 
			for(var i=1; i<tbox.options.length; i++)
			{
				if(tbox.options[i].selected && tbox.options[i].value != "") 
				{
				
				var SelectedText,SelectedValue;
				SelectedValue = tbox.options[i].value;
				SelectedText = tbox.options[i].text;
				tbox.options[i].value=tbox.options[i-1].value;
				tbox.options[i].text=tbox.options[i-1].text;
				tbox.options[i-1].value=SelectedValue;
				tbox.options[i-1].text=SelectedText;
				}
			}
			return false;
			}
			function MoveDown(tbox)
			{
			 
			for(var i=0; i<tbox.options.length-1; i++)
			{
				if(tbox.options[i].selected && tbox.options[i].value != "") 
				{
				
				var SelectedText,SelectedValue;
				SelectedValue = tbox.options[i].value;
				SelectedText = tbox.options[i].text;
				tbox.options[i].value=tbox.options[i+1].value;
				tbox.options[i].text=tbox.options[i+1].text;
				tbox.options[i+1].value=SelectedValue;
				tbox.options[i+1].text=SelectedText;
				}
			}
				return false;
			}
		
		sortitems = 0;  // 0-False , 1-True
		function move(fbox,tbox)
			{
			 
			for(var i=0; i<fbox.options.length; i++)
			{
				if(fbox.options[i].selected && fbox.options[i].value != "") 
				{
				/// to check for duplicates 
				for (var j=0;j<tbox.options.length;j++)
					{
						if (tbox.options[j].value == fbox.options[i].value)	
						{
							alert("Item is already selected");
							return false;
						}
					}
				
				var no = new Option();
				no.value = fbox.options[i].value;
				no.text = fbox.options[i].text;
				tbox.options[tbox.options.length] = no;
				fbox.options[i].value = "";
				fbox.options[i].text = "";
				
				}
			}
				BumpUp(fbox);
				if (sortitems) SortD(tbox);
				return false;
			}
			
			function remove(fbox,tbox)
			{
			 
			for(var i=0; i<fbox.options.length; i++)
			{
				if(fbox.options[i].selected && fbox.options[i].value != "") 
				{
				/// to check for duplicates 
				for (var j=0;j<tbox.options.length;j++)
					{
						if (tbox.options[j].value == fbox.options[i].value)	
						{
								fbox.options[i].value = "";
								fbox.options[i].text = "";
							BumpUp(fbox);
							if (sortitems) SortD(tbox);
							return false;
							
							
							//alert("Item is already selected");
							//return false;
						}
					}
				
				var no = new Option();
				no.value = fbox.options[i].value;
				no.text = fbox.options[i].text;
				tbox.options[tbox.options.length] = no;
				fbox.options[i].value = "";
				fbox.options[i].text = "";
				
				}
			}
				BumpUp(fbox);
				if (sortitems) SortD(tbox);
				return false;
			}
			
			function BumpUp(box) 
			{
			for(var i=0; i<box.options.length; i++) 
			{
			if(box.options[i].value == "")  
			{
				for(var j=i; j<box.options.length-1; j++)  
				{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
				}
				var ln = i;
				break;
			}
			}
			if(ln < box.options.length) 
			{
			box.options.length -= 1;
			BumpUp(box);
			}
			}


		function SortD(box) 
		{
			var temp_opts = new Array();
			var temp = new Object();
			for(var i=0; i<box.options.length; i++)  
			{
				temp_opts[i] = box.options[i];
			}
			for(var x=0; x<temp_opts.length-1; x++)  
			{
				for(var y=(x+1); y<temp_opts.length; y++)  
				{
					if(temp_opts[x].text > temp_opts[y].text) 
					{
						temp = temp_opts[x].text;
						temp_opts[x].text = temp_opts[y].text;
						temp_opts[y].text = temp;
						temp = temp_opts[x].value;
						temp_opts[x].value = temp_opts[y].value;
						temp_opts[y].value = temp;
					}
				}
			}
			for(var i=0; i<box.options.length; i++)  
			{
				box.options[i].value = temp_opts[i].value;
				box.options[i].text = temp_opts[i].text;
			}
		}
		function Save()
		{
			var str='';
			for(var i=0; i<document.Form1.lstTeamAdd.options.length; i++)
			{
				var SelectedText,SelectedValue;
				SelectedValue = document.Form1.lstTeamAdd.options[i].value;
				SelectedText = document.Form1.lstTeamAdd.options[i].text;
				str=str+ SelectedValue +','
			}
			document.Form1.hdnValue.value=str;
			
		}
		function Close()
		{
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
	

			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colSpan="3"><br>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
							<tr>
								<td class="Text_bold_White" noWrap height="23">&nbsp;&nbsp;&nbsp;Select Open Campaign&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:button>
						<asp:button id="btnClose" Runat="server" Text="Cancel" CssClass="button"></asp:button></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:table id="tblTerritories" Runat="server" BorderColor="black" GridLines="None" Width="100%" CssClass="aspTable"
							CellSpacing="0" CellPadding="0" BorderWidth="1">
							<asp:TableRow>
								<asp:TableCell>
									<table>
										<TR>
											<TD class="normal1" nowrap>
												Available Campaign<br>
												<br>
												<asp:listbox id="lstTeamAvail" runat="server" Width="300" Height="80" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</TD>
											<td align="center">
												<asp:button id="btnAdd" CssClass="button" Runat="server" Text="Add >"></asp:button>
												<br>
												<asp:button id="btnRemove" CssClass="button" Runat="server" Text="< Remove"></asp:button>
											</td>
											<td class="normal1">
												The user will ONLY be able to access
												<br>
												records from the following Campaign.<br>
												<asp:listbox id="lstTeamAdd" Width="300" Height="80" runat="server" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</td>
										</TR>
									</table>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table></td>
				</tr>
			</table>
			<asp:TextBox ID="hdnValue" Runat="server" style="DISPLAY:none"></asp:TextBox>
			
		</form>
	</body>
</HTML>
