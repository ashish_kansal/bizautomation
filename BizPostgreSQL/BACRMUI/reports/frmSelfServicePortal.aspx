<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSelfServicePortal.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits="BACRM.UserInterface.Reports.frmSelfServicePortal" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Self-Service Portal</title>
    <script language="javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenContactList(DivID) {
            window.open("../Reports/frmRepContactsLogged.aspx?DivID=" + DivID, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>

    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
     <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>
                        From
                    </label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" />
                    <label>
                        To
                    </label>
                    <BizCalendar:Calendar ID="calTo" runat="server" />
                    <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
                </div>
            </div>
             <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                             <asp:LinkButton ID="btnChooseTerritories" runat="server" CssClass="btn btn-primary">Choose Territories</asp:LinkButton>
                    <asp:LinkButton ID="btnExportToExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                            <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Self-Service Portal (Activity)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="Table9" CellPadding="0" CellSpacing="0" Height="400" runat="server"
        CssClass="aspTable" Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgPortal" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" runat="server" BorderColor="white"
                    AutoGenerateColumns="False" AllowSorting="True">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ContactID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="LastLoggedIn" HeaderText="Last Logged in on">
                            <ItemTemplate>
                                <%# ReturnName(DataBinder.Eval(Container.DataItem, "LastLoggedIn")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Company" SortExpression="Company" HeaderText="Parent Name, Division"></asp:BoundColumn>
                        <asp:BoundColumn DataField="RelationShip" SortExpression="RelationShip" HeaderText="Relationship"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="Times" HeaderText="No of Times Logged in">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplNoOfTimes" CssClass="hyperlink" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Times") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="NoofDeals" SortExpression="NoofDeals" HeaderText="Number of deals won"></asp:BoundColumn>
                        <asp:BoundColumn DataField="AmtWon" SortExpression="AmtWon" DataFormatString="{0:#,##0.00}"
                            HeaderText="Amount of deals won"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
