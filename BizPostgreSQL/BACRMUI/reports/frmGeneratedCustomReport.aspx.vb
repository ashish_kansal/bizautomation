''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmGeneratedCustomReport
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is the Generated Custom Report
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	11/04/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Partial Class frmGeneratedCustomReport
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim objCustomReports As CustomReports
    Dim sColumnSorted As String
    Dim sSortOrder As String
    Dim dsCustomReport As DataSet                                           'Create a object of a DataSet
    Dim boolPreCreatedCustomReport As Boolean
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the page is called.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/04/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Put user code to initialize the page here
            boolPreCreatedCustomReport = IIf(GetQueryStringVal( "numCustReportID") > 0, True, False) 'If this is a pre-created Report then set the flag
            If boolPreCreatedCustomReport Then
                btnBackToMyReports.Visible = True                       'Make the back to My Reprots button visible
                btnClose.Visible = False                                'Hide the Close button
                btnAddToMyReport.Visible = False                        'Hide the Add to My Reprots button
                btnDeleteCustomReport.Visible = True                    'It is possible to delete a Report
            Else
                Dim objForm As Object                                   'The generic object
                objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                objForm.visible = False                                 'Hide the web control
                btnBackToMyReports.Visible = False                      'Hide the Back Botton
                btnDeleteCustomReport.Visible = False                   'It is not possible to delete a Report
            End If
            objCustomReports = New CustomReports                        'Create a new instance of the Custom Report Object
            With objCustomReports
                .UserCntID = Session("UserContactID")                          'Set the User ID into class variable
                .DomainID = Session("DomainID")                         'Set the Domain ID into class variable
                .tintType = 50
                .XMLFilePath = CCommon.GetDocumentPhysicalPath() 'Set the path to the xml file where the config is temp stored
                .RefreshInterval = ConfigurationManager.AppSettings("CustomReportRefreshInterval") 'Set the refresh interval for the global temp tables
            End With
            If Not IsPostBack Then
                 ' = "Reports"
                CallToGenerateCustomReport()                            'Call to generate custom report
                callToBindAdditionalTriggers()                          'Attach client events to form elements
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to execute the steps for generating custom reports.
    ''' </summary>
    ''' <remarks>
    '''     Steps involved are:
    '''     1) Get the file containing the config
    '''     2) Construct the query by parsing the same using a stored procedure
    '''     3) Query the appropriate views to get the resultset
    '''     4) Display the Report in the DataGrid
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/04/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function CallToGenerateCustomReport()
        Try
            If Not boolPreCreatedCustomReport Then                                  'If this is not a precreated report
                Dim dsCustomReportConfig As DataSet = objCustomReports.GetCustomReportConfigFromXMLfile() 'Call to get the custom report configuration into a DataSet
                dsCustomReport = objCustomReports.GetCustomReportResultSet(dsCustomReportConfig) 'Call to get the RecordSet for displaying the custom report
                tblCustomReportHeaderSeaction.Width = "975"                        'Set the width of the table to 800
            Else
                dsCustomReport = objCustomReports.GetCustomReportResultSet(GetQueryStringVal( "numCustReportID")) 'Call to get the RecordSet for displaying the custom report
                tblCustomReportHeaderSeaction.Width = "840"                       'Set the width of the table to 100%
            End If
            Dim dtCustomReport As DataTable = GetRowsInPageRange(dsCustomReport.Tables(0)) 'Create a object of a DataSet
            DataBindEditBoxes(dsCustomReport.Tables(0).Rows.Count)
            Dim dvCustomReport As DataView = dtCustomReport.DefaultView             'Get the default view
            If txtColumnSorted.Text <> "" Then dvCustomReport.Sort = txtColumnSorted.Text & " " & txtSortOrder.Text 'Set the sorting
            dgCustomReport.DataSource = dvCustomReport                              'Set the Default View of the result as the source for the datagrid
            dgCustomReport.DataBind()                                               'Bind the DataGrid
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to populate the edit boxes/ other controls with values
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="numRowCount">Nos of records</param>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/16/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub DataBindEditBoxes(ByVal numRowCount As Integer)
        Try
            txtTotalPage.Text = Math.Ceiling(numRowCount / Session("PagingRows")) 'Bind the nos of pages
            lblTotal.Text = Math.Ceiling(numRowCount / Session("PagingRows")) 'Display the mos of pages
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the page is closed.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/05/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ''Commented by Siva
            ''objCustomReports.DeleteCustomReportConfigXMLfile()          'Delete the Config file 
            litClientSideScript.Text = "<script language='javascript'>this.close();</script>" 'Attach client side script to the literal to close this window
            litClientSideScript.Visible = True                          'Make the Client Side Literal visible
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the report is exported to Excel.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/05/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        Try
            If Not boolPreCreatedCustomReport Then                                                  'If this is not a precreated report
                Dim dsCustomReportConfig As DataSet = objCustomReports.GetCustomReportConfigFromXMLfile() 'Call to get the custom report configuration into a DataSet
                dsCustomReport = objCustomReports.GetCustomReportResultSet(dsCustomReportConfig)    'Call to get the RecordSet for displaying the custom report
            Else : dsCustomReport = objCustomReports.GetCustomReportResultSet(GetQueryStringVal( "numCustReportID")) 'Call to get the RecordSet for displaying the custom report
            End If
            CallToAddSummationsToTheDataTable(dsCustomReport)                                       'Add the Summation Columns to the DataTable before exporting
            Dim dtCustomReport As DataTable = dsCustomReport.Tables(0)                              'Create a object of a DataSet
            Dim objPredefinedReports As New PredefinedReports                                       'Declare and instantiate an object of predefined reports (created by Maha) and used for exporting the report to ms-excel
            Dim strPath As String = objCustomReports.XMLFilePath & "\Report"                        'Path where the files are to be exported
            Dim strRef As String                                                                    'Variable to capture the file name 
            objPredefinedReports.ExportToExcel(dtCustomReport.DataSet, objCustomReports.XMLFilePath, strPath, Session("UserName"), Session("UserID"), "Custom_Report", Session("DateFormat"))
            strRef = Session("SiteType") & "//" & Request.ServerVariables("HTTP_HOST") & Request.ApplicationPath() & "/../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/Report/" & Session("UserName") & "_" & Session("UserID") & "_Custom_Report.csv"
            litClientSideScript.Text = "<script language=javascript>alert('Exported Successfully !');window.open('" & strRef & "');</script>" 'Alert to user and open the Excel
            litClientSideScript.Visible = True                                                      'Make the Client Side Literal visible
            CallToGenerateCustomReport()                                                            'Call to generate custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to add the summations requested to the same table by adding a new row
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/05/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function CallToAddSummationsToTheDataTable(ByRef dsCustomReport As DataSet)
        Try
            Dim dtCustomReport As DataTable = dsCustomReport.Tables(0)                  'Declare a DataTable
            Dim dtCustomReportAggregation As DataTable = dsCustomReport.Tables(1)       'Create a object of a DataTable and Get the order in which aggregataion is displayed
            Dim dtCustomReportRecordCount As DataTable = dsCustomReport.Tables(2)       'Create a object of a DataTable and Get teh flag if Record Count is to be displayed or not

            Dim drCustomReportEmptyRow, drCustomReportSummationRow, drCustomReportAvgRow, drCustomReportLargestRow, drCustomReportSmallestRow As DataRow  'Declare a new DataRow to hold the aggregation values
            drCustomReportEmptyRow = dtCustomReport.NewRow()                            'Instantiate a new datarow
            drCustomReportSummationRow = dtCustomReport.NewRow()                        'Instantiate a new datarow
            drCustomReportAvgRow = dtCustomReport.NewRow()                              'Instantiate a new datarow
            drCustomReportLargestRow = dtCustomReport.NewRow()                          'Instantiate a new datarow
            drCustomReportSmallestRow = dtCustomReport.NewRow()                         'Instantiate a new datarow

            Dim arrColumnAggregationSeq(), arrIndColumnAggregationSeq() As String       'Declare a Array
            arrColumnAggregationSeq = CStr(dtCustomReportAggregation.Rows(0).Item("AggregateReportColumns")).Split("|")
            Dim iAggregationRowSeq, iAggregationColSeq As Integer                       'Declare a row and column index variable
            For iAggregationColSeq = 0 To dtCustomReport.Columns.Count - 1              'Loop through the columns
                If dtCustomReport.Columns(iAggregationColSeq).DataType Is System.Type.GetType("System.String") Then 'check the DataType of the Parent table column
                    drCustomReportSummationRow.Item(iAggregationColSeq) = "Total:"
                    drCustomReportAvgRow.Item(iAggregationColSeq) = "Average:"
                    drCustomReportLargestRow.Item(iAggregationColSeq) = "Largest Value:"
                    drCustomReportSmallestRow.Item(iAggregationColSeq) = "Smallest Value:"
                    Exit For
                End If
            Next
            For iAggregationRowSeq = 0 To arrColumnAggregationSeq.Length - 1            'Loop through the array
                arrIndColumnAggregationSeq = arrColumnAggregationSeq(iAggregationRowSeq).Split(",")
                If arrIndColumnAggregationSeq(0) = 1 Then
                    Try
                        drCustomReportSummationRow.Item(iAggregationRowSeq) = Math.Round(Convert.ToDouble(dtCustomReport.Compute("Sum([" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "]") = False)), 2) 'Set the summation data
                    Catch ex As Exception
                    End Try
                End If
                If arrIndColumnAggregationSeq(1) = 1 Then
                    Try
                        drCustomReportAvgRow.Item(iAggregationRowSeq) = Math.Round(Convert.ToDouble(dtCustomReport.Compute("Avg([" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "]") = False)), 2) 'Set the Average data
                    Catch ex As Exception
                    End Try
                End If
                If arrIndColumnAggregationSeq(2) = 1 Then
                    Try
                        drCustomReportLargestRow.Item(iAggregationRowSeq) = Math.Round(Convert.ToDouble(dtCustomReport.Compute("Max([" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "]") = False)), 2) 'Set the Largest data
                    Catch ex As Exception
                    End Try
                End If
                If arrIndColumnAggregationSeq(3) = 1 Then
                    Try
                        drCustomReportSmallestRow.Item(iAggregationRowSeq) = Math.Round(Convert.ToDouble(dtCustomReport.Compute("Min([" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationRowSeq).ColumnName & "]") = False)), 2) 'Set the Smallest data
                    Catch ex As Exception
                    End Try
                End If
            Next
            dtCustomReport.Rows.Add(drCustomReportEmptyRow)                                         'Add the empty row
            dtCustomReport.Rows.Add(drCustomReportSummationRow)                                     'Add the summation row
            dtCustomReport.Rows.Add(drCustomReportAvgRow)                                           'Add the avg row
            dtCustomReport.Rows.Add(drCustomReportLargestRow)                                       'Add the largest data row
            dtCustomReport.Rows.Add(drCustomReportSmallestRow)                                      'Add the smallest data row
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to attach additional client side events to form controls
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/05/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindAdditionalTriggers()
        Try
            btnAddToMyReport.Attributes.Add("onclick", "javascript: return RequestReportNameAndDesc('divColHeader');") 'Call to add client events when the radios are clicked
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the DataGrid is sorted.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/07/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub dgCustomReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCustomReport.SortCommand
        Try
            sColumnSorted = e.SortExpression.ToString()                 'Retrieve the column sorting
            If txtColumnSorted.Text <> sColumnSorted Then
                txtColumnSorted.Text = sColumnSorted
                txtSortOrder.Text = "Asc"                               'Default sort is ascending
            Else
                If txtSortOrder.Text = "Asc" Then                       'Toggle sort order
                    txtSortOrder.Text = "Desc"
                Else : txtSortOrder.Text = "Asc"
                End If
            End If
            CallToGenerateCustomReport()                                'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the data is bound to the DataGrid.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/07/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub dgCustomReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCustomReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Footer Then
                Dim dtCustomReport As DataTable = dsCustomReport.Tables(0)                  'Declare a DataTable
                Dim dtCustomReportAggregation As DataTable = dsCustomReport.Tables(1)       'Create a object of a DataTable and Get the order in which aggregataion is displayed
                Dim dtCustomReportRecordCount As DataTable = dsCustomReport.Tables(2)       'Create a object of a DataTable and Get teh flag if Record Count is to be displayed or not
                If dtCustomReportRecordCount.Rows(0).Item("RecordCountSummation") Then      'If Record Count is to be displayed
                    lblTotalRecordCount.Text = dtCustomReport.Rows.Count & " Records found."
                End If

                Dim arrColumnAggregationSeq(), arrIndColumnAggregationSeq() As String       'Declare a Array
                arrColumnAggregationSeq = CStr(dtCustomReportAggregation.Rows(0).Item("AggregateReportColumns")).Split("|")

                Dim iAggregationSeq As Integer
                For iAggregationSeq = 0 To arrColumnAggregationSeq.Length - 1               'Loop through the array
                    arrIndColumnAggregationSeq = arrColumnAggregationSeq(iAggregationSeq).Split(",")
                    If arrIndColumnAggregationSeq(0) = 1 Then
                        Try
                            e.Item.Cells(iAggregationSeq).Text = "Total:" & Math.Round(Convert.ToDouble(dtCustomReport.Compute("Sum([" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "]") = False)), 2)
                        Catch ex As Exception
                        End Try
                    End If
                    If arrIndColumnAggregationSeq(1) = 1 Then
                        Try
                            e.Item.Cells(iAggregationSeq).Text &= "<br>Avg:" & Math.Round(Convert.ToDouble(dtCustomReport.Compute("Avg([" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "]") = False)), 2)
                        Catch ex As Exception
                        End Try
                    End If
                    If arrIndColumnAggregationSeq(2) = 1 Then
                        Try
                            e.Item.Cells(iAggregationSeq).Text &= "<br>Largest:" & Math.Round(Convert.ToDouble(dtCustomReport.Compute("Max([" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "]") = False)), 2)
                        Catch ex As Exception
                        End Try
                    End If
                    If arrIndColumnAggregationSeq(3) = 1 Then
                        Try
                            e.Item.Cells(iAggregationSeq).Text &= "<br>Smallest:" & Math.Round(Convert.ToDouble(dtCustomReport.Compute("Min([" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "])", IsDBNull("[" & dtCustomReport.Columns(iAggregationSeq).ColumnName & "]") = False)), 2)
                        Catch ex As Exception
                        End Try
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the back to My Reports button is clicked.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnBackToMyReports_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackToMyReports.Click
        Try
            Response.Redirect("frmMyReports.aspx", True) 'Redirect to the My Reports Screen
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time the Delete button is clicked.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/16/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnDeleteCustomReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteCustomReport.Click
        Try
            objCustomReports.DeleteCustomReport(GetQueryStringVal( "numCustReportID")) 'Call to delete a custom Report
            Response.Redirect("frmMyReports.aspx", True)                                'Redirect to the My Reports Screen
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to fill the search results for Adv.Search Screen from the XML file (intermediate search) when the page index changes
    ''' </summary>
    ''' <remarks> When the page number is specifically entered in the textbox, it executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/19/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub txtCurrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentPage.TextChanged
        Try
            If IsNumeric(txtCurrentPage.Text) Then
                CallToGenerateCustomReport()                                'Call to generate the custom report
            Else : litClientMessage.Text = "<script language=javascript>alert('Invalid page number entered, please re-enter.');</script>" 'Alert to user about invalid page number
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the last page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrentPage.Text = txtTotalPage.Text                                             'Set the Page Index
            CallToGenerateCustomReport()                                                        'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the previous page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If Convert.ToInt64(txtCurrentPage.Text) > 1 Then                                'If the first page is already displayed then exit
                txtCurrentPage.Text = txtCurrentPage.Text - 1                               'decrement the page number
            End If
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the first page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrentPage.Text = 1                                                         'Set the first page index to 1
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the next page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Try
            If Convert.ToInt64(txtCurrentPage.Text) < Convert.ToInt64(txtTotalPage.Text) Then 'check if this is not the last page
                txtCurrentPage.Text = txtCurrentPage.Text + 1                               'increment the page number
            End If
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the Next: 2 page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If Convert.ToInt64(txtCurrentPage.Text) + 2 <= Convert.ToInt64(txtTotalPage.Text) Then 'check if incrementing the page counter exhausts the page
                txtCurrentPage.Text = txtCurrentPage.Text + 2                               'increment the page number by 2
            End If
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the Next: 3 page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If Convert.ToInt64(txtCurrentPage.Text) + 3 <= Convert.ToInt64(txtTotalPage.Text) Then 'check if incrementing the page counter exhausts the page
                txtCurrentPage.Text = txtCurrentPage.Text + 3                               'increment the page number by 3
            End If
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the Next: 4 page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If Convert.ToInt64(txtCurrentPage.Text) + 4 <= Convert.ToInt64(txtTotalPage.Text) Then  'check if incrementing the page counter exhausts the page
                txtCurrentPage.Text = txtCurrentPage.Text + 4                               'increment the page number by 4
            End If
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method executed when the Next: 5 page icon is clicked on the DataGrid Page
    ''' </summary>
    ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
    '''     this executes to display the records for the page
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If Convert.ToInt64(txtCurrentPage.Text) + 5 <= Convert.ToInt64(txtTotalPage.Text) Then  'check if incrementing the page counter exhausts the page
                txtCurrentPage.Text = txtCurrentPage.Text + 5                               'increment the page number by 5
            End If
            CallToGenerateCustomReport()                                                    'Call to generate the custom report
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to pick up a selected range of rows
    ''' </summary>
    ''' <remarks> Returns the table with limited rows
    ''' </remarks>
    ''' <param name="dtResultTable">Table which contains the search result to be displayed in the DataGrid</param>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Function GetRowsInPageRange(ByVal dtCustomReportResultTable As DataTable) As DataTable
        Try
            Dim intCustomReportResultsRowIndex As Integer                                                   'Declare an index variable
            Dim dtResultTableCopy As DataTable                                                          'Declare a datatable object and instantiate
            dtResultTableCopy = dtCustomReportResultTable.Clone()                                           'Clone the DataTable
            Dim iRowIndex As Integer = txtCurrentPage.Text * Session("PagingRows")  'Declare a row index and set it to the first row in the table which has ot be displayed
            For intCustomReportResultsRowIndex = iRowIndex - Session("PagingRows") To iRowIndex - 1        'Loop through the row index from the table
                If ((intCustomReportResultsRowIndex >= 0) And (dtCustomReportResultTable.Rows.Count > intCustomReportResultsRowIndex)) Then 'Ensure that the row exists
                    dtResultTableCopy.ImportRow(dtCustomReportResultTable.Rows(intCustomReportResultsRowIndex)) 'import the range of rows
                End If
            Next
            Return dtResultTableCopy
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
