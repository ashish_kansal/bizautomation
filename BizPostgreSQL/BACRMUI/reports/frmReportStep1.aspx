﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmReportStep1.aspx.vb" Inherits=".frmReportStep1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Create New Report</title>
    <link href="jqTree/jquery.treeview.css" rel="stylesheet" />
    <script src="jqTree/jquery.treeview.js"></script>

    <style type="text/css">
        #SearchForm {
            border: 1px solid #ebebeb;
            /*width: 300px;*/
            background-color: #fff;
        }

        .Searchinput {
            border-left: none !important;
            border-right: none !important;
            border-top: none !important;
            border-bottom: none !important;
            background: none !important;
            border-color: transparent !important; /*Fix IE*/
            width: 270px;
            outline: none !important;
            color: Gray !important;
        }

        #tblReport button {
            width: 16px;
            height: 16px;
            outline: none;
            border: none;
            background: url('../Images/btnSearch.gif') left center no-repeat;
            text-indent: -999px;
            vertical-align: middle;
        }

        .SearchBox {
            padding: 5px;
            background-color: #E6E6E7;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var strSearchConst = 'Quick Find';

        $(document).ready(function () {
            $.expr[":"].contains = $.expr.createPseudo(function (arg) {
                return function (elem) {
                    return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                };
            });

            $("#ulModuleGroup").treeview({
                control: "#treecontrol",
                collapsed: true
            });

            $("#ulModuleGroup a").click(function () {
                $("#ulModuleGroup li").removeClass("selected");
                $(this).parent('li').addClass("selected");

                //console.log($(this).attr("id"));
            });

            $("#SearchForm").keypress(function (e) {
                if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                    on_filter();
                    return false;
                }
            });

            $('#txtSearchGroup').change(function () {
                on_filter();
            });

            $('#btnGo').click(function () {
                on_filter();
                return false;
            });

            $('#txtSearchGroup').focus(function () {
                $('#SearchForm').css('border-color', '#09c');
                $('#txtSearchGroup').css('outline', 'none');
                $('#txtSearchGroup').css('color', '#515E81');
                if ($('#txtSearchGroup').val() == strSearchConst)
                    $('#txtSearchGroup').val('');
            });

            $('#txtSearchGroup').blur(function () {
                $('#SearchForm').css('border-color', '#ebebeb');
                $('#txtSearchGroup').css('color', 'gray');
                if ($('#txtSearchGroup').val() == "")
                    $('#txtSearchGroup').val(strSearchConst);
            });
        });

        function on_filter() {
            var tval = $("#txtSearchGroup").val();

            if (tval != '' && tval != strSearchConst) {
                $("#ulModuleGroup li").hide();
                $("#ulModuleGroup ul li").hide().filter(":contains('" + tval + "')").find('li').andSelf().show().parent().parent().show();
                //$('#treecontrol a:eq(1)').click();

                //$("li[class='collapsable']").children('.hitarea').trigger('click');
                $("li[class*='expandable']").children('.hitarea').trigger('click');
                //$('#ulModuleGroup a:eq(2)').click();
            }
            else {
                $("#ulModuleGroup li").show();
                //$('#treecontrol a:eq(0)').click();

                //$("li[class='expandable']").children('.hitarea').trigger('click');
                $("li[class*='collapsable']").children('.hitarea').trigger('click');
                //$("li[class='collapsable']").children('.hitarea').trigger('click');
            }
            //$("#ulModuleGroup li").hide().filter(":contains('" + tval + "')").find('li').show().andSelf().show();
        }

        function Save() {
            if (document.getElementById("txtReportName").value == '') {
                alert("Enter Report Name");
                document.getElementById("txtReportName").focus();
                return false;
            }

            var selNode = $("#ulModuleGroup li .selected a");

            if ($(selNode).length) {
                $("#hfModuleID").val($(selNode).parent().parent().attr("id").split('_')[1]);
                $("#hfGroupID").val($(selNode).attr("id").split('_')[1]);

                //console.log($(selNode).parent().parent().attr("id"));
                //console.log($(selNode).attr("id"));
            }
            else {
                alert("Select Report Module");
                return false;
            }
        }

    </script>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Create New Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div style="background-color: #fff;">
        <asp:Table ID="tblReport" CellPadding="0" CellSpacing="0" runat="server"
            Width="100%" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label>Report Name :</label>
                            </div>
                            <div class="col-md-6 form-group">
                                <asp:TextBox ID="txtReportName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <label>Report Description :</label>
                            </div>
                            <div class="col-md-6 form-group">
                                <asp:TextBox ID="txtReportDescription" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <label>Report Type :</label>
                            </div>
                            <div class="col-md-6 form-group">
                                <asp:DropDownList ID="ddlReportType" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="0">Tabular</asp:ListItem>
                                    <asp:ListItem Value="1">Summary</asp:ListItem>
                                    <asp:ListItem Value="2">Matrix</asp:ListItem>
                                    <asp:ListItem Value="3">KPI</asp:ListItem>
                                    <asp:ListItem Value="4">ScoreCard</asp:ListItem>
                                    <asp:ListItem Value="5">KPI and Fields</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                
                            </div>
                            <div class="col-md-6 form-group">
                               <asp:Button ID="btnSave" runat="server" Text="Create" CssClass="btn btn-primary" OnClientClick="javascript:return Save();"></asp:Button>
                                <asp:HiddenField ID="hfModuleID" runat="server" />
                                <asp:HiddenField ID="hfGroupID" runat="server" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </asp:TableCell>
                <asp:TableCell>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="col-md-4">
                                <label>Select Report Module :</label>
                            </div>
                            <div class="col-md-8 form-inline" id="SearchForm">
                                 <button type="submit" id="btnGo">
                                        </button>
                                        <asp:TextBox ID="txtSearchGroup" runat="server" CssClass="form-control" MaxLength="20" Text="Quick Find"></asp:TextBox>
                                <asp:Literal ID="litModuleGroup" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>
