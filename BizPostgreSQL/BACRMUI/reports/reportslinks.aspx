<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="reportslinks.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.reportslinks" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="mytree" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Pre-Defined Reports</title>
    <script type="text/javascript">
        function OpenAddReportWindow(numReportID) {
            window.open("../reports/frmEditCustomReport.aspx?numReportID=" + numReportID, "EditCustomReport", "toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=600,height=300,scrollbars=yes,resizable=yes");
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Reports.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Predefined Reports&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <asp:Table ID="tblMile" BorderWidth="1" runat="server" Width="100%" CellPadding="0"
                    CellSpacing="0" GridLines="None" CssClass="aspTable" ForeColor="White">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataList ID="dlRepotList" runat="server" Width="100%" CellPadding="0" CellSpacing="2">
                                <ItemStyle BackColor="#52658C" />
                                <ItemTemplate>
                                    <table width="100%" cellpadding="2" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnk" CommandName="Link" runat="server" Style="text-decoration: none"
                                                    ForeColor="White">
                                                    <asp:Label ID="lbl" runat="server" CssClass="normal6" Text='<%# DataBinder.Eval(Container,"DataItem.RptHeading") %>'>
                                                    </asp:Label>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <SelectedItemTemplate>
                                    <table width="100%" cellpadding="2" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblReptID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.RptID") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:LinkButton ID="Linkbutton1" CommandName="Link" Style="text-decoration: none"
                                                    ForeColor="White" runat="server">
                                                    <b>
                                                        <asp:Label ID="Label2" runat="server" CssClass="normal6" Text='<%# DataBinder.Eval(Container,"DataItem.RptHeading") %>'>
                                                        </asp:Label></b>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Table ID="tblSubLink" Width="100%" runat="server" CellPadding="0" CellSpacing="0"
                                                    BackColor="white">
                                                </asp:Table>
                                            </td>
                                        </tr>
                                    </table>
                                </SelectedItemTemplate>
                            </asp:DataList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
        </div>
</asp:Content>
