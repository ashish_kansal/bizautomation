﻿
//***Get Report Response***//
function GetReportResponse(boolSave) {

    if (!boolInitial) {
        responseJson.NoRows = parseInt($('#txtNoRows').val()) == NaN ? 0 : parseInt($('#txtNoRows').val());
        responseJson.bitHideSummaryDetail = $('#bitHideSummaryDetail').prop('checked');

        responseJson.vcReportName = $("#txtReportName").val();
        responseJson.vcReportDescription = $("#txtReportDescription").val();

        if (responseJson.ReportFormatType == 3 || responseJson.ReportFormatType == 5) {
            responseJson.KPIMeasureField = $("#ddlKPIMeasureType").val();

            responseJson.DateFieldFilter = $("#ddlDateFieldFilter").val();
            responseJson.DateFieldValue = $("#ddlDateFieldRange").val();

            responseJson.KPIThresold = [];
            responseJson.KPIThresold.push({ 'ThresoldType': $("#ddlKPIThreshold").val(), 'Value1': (parseFloat($('#txtKPIThreshold').val()) == NaN ? 0 : parseInt($('#txtKPIThreshold').val())) });
        }
        else if (responseJson.ReportFormatType == 4) {
            responseJson.KPIMeasureField = $("#ddlKPIMeasureType").val();

            responseJson.DateFieldFilter = $("#ddlDateFieldFilter").val();
            responseJson.DateFieldValue = $("#ddlDateFieldRange").val();

            responseJson.KPIThresold = [];
            responseJson.KPIThresold.push({ 'ThresoldType': 'ge', 'Value1': (parseFloat($('#txtScoreCardThreshold1').val()) == NaN ? 0 : parseInt($('#txtScoreCardThreshold1').val())), 'Value2': (parseFloat($('#txtScoreCardThreshold2').val()) == NaN ? 0 : parseInt($('#txtScoreCardThreshold2').val())) });
        }
    }

    $.ajax({
        type: "POST",
        async: false,
        url: "../common/CustomReportsService.asmx/GetReportResponse",
        data: "{lngRptID:'" + $("#hfReportID").val() + "',strReportJson:'" + JSON.stringify(responseJson) + "',boolSave:'" + boolSave + "',boolInit:'" + boolInitial + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response.d);
            responseData = $.parseJSON(response.d);

            //console.log(responseData.ResultData);
            data = $.parseJSON(responseData.ResultData);

            responseJson = responseData;
            responseJson.ResultData = "";
            //console.log(responseJson);

            if (boolSave) {
                $('#lblmsg').text('Report saved successfully.');

                $('#lblmsg').removeClass('errorInfo');
                $('#lblmsg').removeClass('successInfo');
                $('#lblmsg').removeClass('info');

                $('#lblmsg').addClass('successInfo');

                $('#lblmsg').fadeIn().delay(5000).fadeOut();
            }
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#lblmsg').text('Error: Can\'t display the report until you fix the error in Reports.');

            $('#lblmsg').removeClass('errorInfo');
            $('#lblmsg').removeClass('successInfo');
            $('#lblmsg').removeClass('info');

            $('#lblmsg').addClass('errorInfo');

            $('#lblmsg').fadeIn().delay(5000).fadeOut();
        },
        complete: function () {
        }
    });
}

//***Get DropDown Data***//
function GetDropDownData(numListID, vcListItemType, vcDbColumnName, vcAssociatedControlType) {
    var result = "";

    $.ajax({
        type: "POST",
        async: false,
        url: "../common/CustomReportsService.asmx/GetDropDownData",
        data: "{numListID:'" + numListID + "',vcListItemType:'" + vcListItemType + "',vcDbColumnName:'" + vcDbColumnName + "',vcAssociatedControlType:'" + vcAssociatedControlType + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log($.parseJSON(response.d));
            result = $.parseJSON(response.d);
        },
        failure: function (response) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });

    return result;
}

//***Check is object is Blank***//
$.isBlank = function (obj) {
    return (!obj || $.trim(obj) === "");
};

//***Check is object is contains***//
$.expr[":"].contains = $.expr.createPseudo(function (arg) {
    return function (elem) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

//***Report Column List ProtoType***//
function ReportColumnList(numGroupID, vcGroupName, numFormFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, vcAssociatedControlType, vcListItemType, numListID, bitCustomField, bitAllowSorting, bitAllowGrouping, bitAllowAggregate, bitAllowFiltering) {
    this.numGroupID = numGroupID;
    this.vcGroupName = vcGroupName;
    this.numFormFieldId = numFormFieldId;
    this.vcFieldName = vcFieldName;
    this.vcDbColumnName = vcDbColumnName;
    this.vcFieldDataType = vcFieldDataType;
    this.vcAssociatedControlType = vcAssociatedControlType;
    this.vcListItemType = vcListItemType;
    this.numListID = numListID;
    this.bitCustomField = bitCustomField;
    this.bitAllowSorting = bitAllowSorting;
    this.bitAllowGrouping = bitAllowGrouping;
    this.bitAllowAggregate = bitAllowAggregate;
    this.bitAllowFiltering = bitAllowFiltering;
}

//***Find in Report Column List array***//
function findInColumnList(FieldName, FieldValue) {

    var new_arr = $.grep(arrReportColumnList, function (ColumnList, i) {
        //return (ColumnList["numFormFieldId"] == FieldValue.split('_')[0] && ColumnList["bitCustomField"] == FieldValue.split('_')[1]);
        return ColumnList[FieldName] == FieldValue;
    });

    return new_arr[0];
}

//***Filter List ProtoType***//
function FilterList(Column, ColValue, OperatorType, ColText) {
    this.Column = Column;
    this.ColValue = ColValue;
    this.OperatorType = OperatorType;
    this.ColText = ColText;
}

//***Aggregate Sum ProtoType***//
function AggregateSum(Column, aggType, RowNo, ColumnNo, Value, iGroups) {
    this.Column = Column;
    this.aggType = aggType;
    this.RowNo = RowNo;
    this.ColumnNo = ColumnNo;
    this.Value = Value;
    this.iGroups = iGroups;
}

//***Find in Aggregate Sum array***//
function findAggregateSum(arrayList, FieldName, FieldValue) {

    var new_arr = $.grep(arrayList, function (ColumnList, i) {
        return ColumnList[FieldName] == FieldValue;
    });

    return new_arr;
}

//***filter Condition Operators & Mapping with Controls Type***//
filterConditionOperators = {
    "eq": "equals", "ne": "not equal to",
    "gt": "greater than", "ge": "greater or equal",
    "lt": "less than", "le": "less or equal",
    "co": "contains", "sw": "starts with", "nc": "does not contain"
    //,"ex": "excludes", "wi": "within", "in": "includes", 
};

typeFilterOperatorsMap = {
    //TextBox_V,TextBox_M,SelectBox_N,DateField_V
    "TextBox_V": ["eq", "ne", "co", "nc", "sw"],
    "TextBox_M": ["eq", "ne", "lt", "gt", "le", "ge"],
    "TextBox_N": ["eq", "ne", "lt", "gt", "le", "ge"],
    "SelectBox_N": ["eq", "ne"],
    "SelectBox_V": ["eq", "ne"],
    "CheckBoxList_N": ["eq", "ne"],
    "CheckBoxList_V": ["eq", "ne"],
    "CheckBox_V": ["eq", "ne"],
    "CheckBox_Y": ["eq", "ne"],
    "DateField_D": ["eq", "ne", "lt", "gt", "le", "ge"],
};

//***Error Message List for Filter Logic***//
ErrorMessageList = {
    'ReportBooleanFilter': {
        'error_bad_number': 'The filter logic references an undefined filter: {0}.',
        'error_generic': 'Your filter is imprecise or incorrect. Please see the help for tips on filter logic.',
        'error_missing_left_operand': 'Your filter is missing left operand to {0}.',
        'error_missing_operand': 'Your filter is missing an operand.',
        'error_missing_operation': 'Your filter is missing an operation.',
        'error_missing_right_operand': 'Your filter is missing right operand to AND or OR.',
        'error_paren': 'Your filter is missing a parenthesis.',
        'error_typo': 'Check the spelling in your filter logic.',
        'error_unused_filters1': 'Filter conditions {0} are defined but not referenced in your filter logic.',
        'requiredFieldError': 'You must enter a value.'
    }
}

function getErrorMessageListLabel(a, b) {
    return ErrorMessageList[a][b];
}


//***Aggregate Summary Column Title***//
function funAggregateColumnTitle() {
    var tdAggregate = $("<td></td>");

    $.each(responseJson.Aggregate, function (k, item1) {
        var str = "";

        switch (item1.aggType) {
            case "sum":
                str = "Sum of No. of";
                break;
            case "avg":
                str = "Average of No. of";
                break;
            case "max":
                str = "Largest of No. of";
                break;
            case "min":
                str = "Smallest of No. of";
                break;
        }

        div = $("<div></div>").addClass("summaryGrp aggItem-" + item1.Column + "-" + item1.aggType).append(str + " " + findInColumnList('numFormFieldId', item1.Column).vcFieldName)

        tdAggregate.append(div)
    });

    tdAggregate.append("<div class='summaryRecordCount'>Record Count </div>");

    return tdAggregate;
}

//***Aggregate Summary Column Value Calculation & Display***//
function funAggregateColumn(arrSummary, cssClass) {
    var tdSummary = $("<td></td>").addClass(cssClass).addClass("money");

    total = _.reduce(findAggregateSum(arrSummary, 'aggType', "RC"), function (memo, num) { return Number(num.Value) + memo; }, 0);

    $.each(responseJson.Aggregate, function (k, item1) {

        switch (item1.aggType) {
            case "sum":
                sum = _.reduce(findAggregateSum(arrSummary, 'aggType', item1.aggType), function (memo, num) { return Number(num.Value) + memo; }, 0);
                tdSummary.append(Number(sum).toFixed(2) + "<br />");
                break;

            case "avg":
                sum = _.reduce(findAggregateSum(arrSummary, 'aggType', item1.aggType), function (memo, num) { return Number(num.Value) + memo; }, 0);
                average = sum / total;
                average = average || 0;
                tdSummary.append(Number(average).toFixed(2) + "<br />");
                break;

            case "max":
                max = _.max(findAggregateSum(arrSummary, 'aggType', item1.aggType), function (num) { return Number(num.Value); })["Value"];
                tdSummary.append(max + "<br />");
                break;

            case "min":
                min = _.min(findAggregateSum(arrSummary, 'aggType', item1.aggType), function (num) { return Number(num.Value); })["Value"];
                tdSummary.append(min + "<br />");
                break;
        }
    });

    tdSummary.append(total);

    return tdSummary;
}


//***Bind Date Field Filter***// 
function BindDateFieldFilter() {
    var new_arr = $.grep(arrReportColumnList, function (ColumnList, i) {
        return ColumnList["vcFieldDataType"] == 'D';
    });

    var TreeGroupData = _.groupBy(new_arr, function (innerArray) {
        return innerArray["numGroupID"];
    });

    $("#ddlDateFieldFilter").append('<option value="0~False">--Select One--</option>');

    $.each(TreeGroupData, function (i, TreeGroup) {
        var optgroup = $("<optgroup label='" + TreeGroup[0]["vcGroupName"] + "'></optgroup>");

        $.each(TreeGroup, function (j, item) {
            optgroup.append('<option value="' + item.numFormFieldId + '">' + item.vcFieldName + '</option>');
        });

        $("#ddlDateFieldFilter").append(optgroup);
    });

    $("#ddlDateFieldRange").append('<option value="AllTime">All Time</option>');
    $("#ddlDateFieldRange").append('<option value="Custom">Custom</option>');
    $("#ddlDateFieldRange").append('<optgroup label="Calendar Year">');
    $("#ddlDateFieldRange").append('<option value="CurYear">Current CY</option>');
    $("#ddlDateFieldRange").append('<option value="PreYear">Previous CY</option>');
    $("#ddlDateFieldRange").append('<option value="Pre2Year">Previous 2 CY</option>');
    $("#ddlDateFieldRange").append('<option value="Ago2Year">2 CY Ago</option>');
    $("#ddlDateFieldRange").append('<option value="NextYear">Next CY</option>');
    $("#ddlDateFieldRange").append('<option value="CurPreYear">Current and Previous CY</option>');
    $("#ddlDateFieldRange").append('<option value="CurPre2Year">Current and Previous 2 CY</option>');
    $("#ddlDateFieldRange").append('<option value="CurNextYear">Current and Next CY</option>');
    $("#ddlDateFieldRange").append('</optgroup>');
    $("#ddlDateFieldRange").append('<optgroup label="Calendar Quarter">');
    $("#ddlDateFieldRange").append('<option value="CuQur">Current CQ</option>');
    $("#ddlDateFieldRange").append('<option value="CurNextQur">Current and Next CQ</option>');
    $("#ddlDateFieldRange").append('<option value="CurPreQur">Current and Previous CQ</option>');
    $("#ddlDateFieldRange").append('<option value="NextQur">Next CQ</option>');
    $("#ddlDateFieldRange").append('<option value="PreQur">Previous CQ</option>');
    $("#ddlDateFieldRange").append('</optgroup>');
    $("#ddlDateFieldRange").append('<optgroup label="Calendar Month">');
    $("#ddlDateFieldRange").append('<option value="LastMonth">Last Month</option>');
    $("#ddlDateFieldRange").append('<option value="ThisMonth">This Month</option>');
    $("#ddlDateFieldRange").append('<option value="NextMonth">Next Month</option>');
    $("#ddlDateFieldRange").append('<option value="CurPreMonth">Current and Previous Month</option>');
    $("#ddlDateFieldRange").append('<option value="CurNextMonth">Current and Next Month</option>');
    $("#ddlDateFieldRange").append('</optgroup>');
    $("#ddlDateFieldRange").append('<optgroup label="Calendar Week">');
    $("#ddlDateFieldRange").append('<option value="LastWeek">Last Week</option>');
    $("#ddlDateFieldRange").append('<option value="ThisWeek">This Week</option>');
    $("#ddlDateFieldRange").append('<option value="NextWeek">Next Week</option>');
    $("#ddlDateFieldRange").append('</optgroup>');
    $("#ddlDateFieldRange").append('<optgroup label="Day">');
    $("#ddlDateFieldRange").append('<option value="Yesterday">Yesterday</option>');
    $("#ddlDateFieldRange").append('<option value="Today">Today</option>');
    $("#ddlDateFieldRange").append('<option value="Tomorrow">Tomorrow</option>');
    $("#ddlDateFieldRange").append('<option value="Last7Day">Last 7 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Last30Day">Last 30 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Last60Day">Last 60 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Last90Day">Last 90 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Last120Day">Last 120 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Next7Day">Next 7 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Next30Day">Next 30 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Next60Day">Next 60 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Next90Day">Next 90 Days</option>');
    $("#ddlDateFieldRange").append('<option value="Next120Day">Next 120 Days</option>');
    $("#ddlDateFieldRange").append('</optgroup>');

    setupCal('imgFilter_FromDate', 'txtFilter_FromDate', $('#hfDateFormat').val(), false);
    setupCal('imgFilter_ToDate', 'txtFilter_ToDate', $('#hfDateFormat').val(), false);
}

//***Bind Report Column List Tree***//
function BindTreeControl() {
    var TreeGroupData = _.groupBy(arrReportColumnList, function (innerArray) {
        return innerArray["numGroupID"];
    });

    //console.log(TreeGroupData);

    $.each(TreeGroupData, function (i, TreeGroup) {
        //console.log(TreeGroup);
        var liGroup = $("<li></li>").append('<span class="folder">' + TreeGroup[0]["vcGroupName"] + '</span>');
        var ulChild = $("<ul></ul>").addClass("FieldList");

        $.each(TreeGroup, function (j, item) {
            //console.log(item);
            var liChild = $("<li></li>")

            if (item["vcListItemType"] == '' && (item["vcFieldDataType"] == 'N' || item["vcFieldDataType"] == 'M')) {
                liChild.html('<a href="#" id="' + item["numFormFieldId"] + '"><img class="field-number" />' + item["vcFieldName"] + '</a>');
            }
            else if (item["vcFieldDataType"] == 'D') {
                liChild.html('<a href="#" id="' + item["numFormFieldId"] + '"><img class="field-date" />' + item["vcFieldName"] + '</a>');
            }
            else { //(item["vcFieldDataType"] == 'V') {
                liChild.html('<a href="#" id="' + item["numFormFieldId"] + '"><img class="field-letter" />' + item["vcFieldName"] + '</a>');
            }

            ulChild.append(liChild);
        });

        liGroup.append(ulChild);
        $("#browser").append(liGroup);
    });
}


//***Hide All Menu,PopUp***//
function HideAllMenu() {
    $('#GridMenu').css("display", "none");
    $('#GroupMenu').css("display", "none");
    $('#LayoutFormatMenu').css("display", "none");
    $('#FilterMenu').css("display", "none");
    $('#MatrixBreakMenu').css("display", "none");
    $('#aggItemMenu').css("display", "none");

    thSelIndex = -1;
    thSelGroupIndex = -1;
}

function LoadReportControls(boolGetResponse) {
    if (boolGetResponse)
        GetReportResponse(false);

    if (boolInitial == true) {
        $("#lblReportName").text(responseJson.vcReportName + " (Report Type: " + responseJson.vcGroupName + ")");

        $("#txtReportName").val(responseJson.vcReportName);
        $("#txtReportDescription").val(responseJson.vcReportDescription);

        $('#txtNoRows').val(responseJson.NoRows);
        $('#bitHideSummaryDetail').prop('checked', responseJson.bitHideSummaryDetail);
    }

    //responseJson.vcReportName = ''
    //responseJson.vcReportDescription = '';

    $('#tdNoRows').hide();
    $('#tdHideSummaryDetail').hide();

    if (responseJson.ReportFormatType == 0) { //Layout Format Tabular
        $("#ReportLayoutFormatType").text("Tabular Format");
        responseJson.GroupColumn = [];

        responseJson.ColumnBreaks = [];
        responseJson.RowBreaks = [];
        responseJson.Aggregate = [];
        $('#tdNoRows').show();
    }
    else if (responseJson.ReportFormatType == 1) { //Layout Format Summary
        $("#ReportLayoutFormatType").text("Summary Format");

        responseJson.ColumnBreaks = [];
        responseJson.RowBreaks = [];
        //responseJson.Aggregate = [];
        $('#tdHideSummaryDetail').show();
        $('#tdNoRows').show();
    }
    else if (responseJson.ReportFormatType == 2) { //Layout Format Matrix
        $("#ReportLayoutFormatType").text("Matrix Format");
        responseJson.GroupColumn = [];
        responseJson.ColumnList = [];
    }
    else if (responseJson.ReportFormatType == 3 || responseJson.ReportFormatType == 4) { //Layout Format KPI / ScoreCard
        responseJson.ColumnBreaks = [];
        responseJson.GroupColumn = [];
        responseJson.ColumnList = [];

        responseJson.RowBreaks = [];
        responseJson.Aggregate = [];

        $('#GridTable').hide();
        $('#MatrixTable').hide();
        $('#ReportLayoutFormatType').hide();
        $('#RemoveAllColumns').hide();
        $('#spnPreviewCaption').text("No Preview for " + (responseJson.ReportFormatType == 3 ? "KPI" : "ScoreCard") + " Report.");

        $('#trKPIScoreCard').show();

        if (boolInitial == true) {
            var new_arr = $.grep(arrReportColumnList, function (ColumnList, i) {
                return ColumnList["vcFieldDataType"] == 'M';
            });

            var TreeGroupData = _.groupBy(new_arr, function (innerArray) {
                return innerArray["numGroupID"];
            });

            $("#ddlKPIMeasureType").append('<option value="0_0_False">Record Count</option>');

            $.each(TreeGroupData, function (i, TreeGroup) {
                var selectOptionGroup = $('<optgroup label="' + TreeGroup[0]["vcGroupName"] + '"></optgroup');

                $.each(TreeGroup, function (i, item) {
                    selectOptionGroup.append('<option value="' + item.numFormFieldId + '">' + item.vcFieldName + '</option>')
                });

                if (TreeGroup[0]["numGroupID"] == 3) {
                    selectOptionGroup.append('<option value="3_872_False">Average Order Amt</option>')
                    selectOptionGroup.append('<option value="3_873_False">Total Profit Revenue</option>')
                    selectOptionGroup.append('<option value="3_874_False">Average Profit Margin</option>')
                }

                $('#ddlKPIMeasureType').append(selectOptionGroup);
            });

            $('#lblDateCaption').text('Date Measure');
            $("#ddlDateFieldFilter option[value='0~False']").remove();

            if (responseJson.ReportFormatType == 3) {
                $('#spnDateCustom').hide();

                $('#lblDateRangeCaption').text('Period').css("font-weight", "bold");

                $("#ddlDateFieldRange").text('');
                $("#ddlDateFieldRange").append('<option value="Year">This Year vs. Last Year</option>');
                $("#ddlDateFieldRange").append('<option value="Quarter">This Quarter vs. Last Quarter</option>');
                $("#ddlDateFieldRange").append('<option value="Month">This Month vs. Last Month</option>');
                $("#ddlDateFieldRange").append('<option value="Week">This Week vs. Last Week</option>');

                $('#trScoreCard').hide();
            }
            else {
                $('#tdKPI').hide();
            }
        }

        $('#ddlKPIMeasureType').find("option[value='" + responseJson.KPIMeasureField + "']").prop("selected", true);

        if (responseJson.KPIThresold.length > 0) {
            if (responseJson.ReportFormatType == 3) {
                $('#ddlKPIThreshold').find("option[value='" + responseJson.KPIThresold[0].ThresoldType + "']").prop("selected", true);
                $('#txtKPIThreshold').val(responseJson.KPIThresold[0].Value1);
            }
            else if (responseJson.ReportFormatType == 4) {
                $('#txtScoreCardThreshold1').val(responseJson.KPIThresold[0].Value1);
                $('#txtScoreCardThreshold2').val(responseJson.KPIThresold[0].Value2);
            }
        }
    } else if (responseJson.ReportFormatType == 5) {
        responseJson.GroupColumn = [];

        responseJson.ColumnBreaks = [];
        responseJson.RowBreaks = [];
        responseJson.Aggregate = [];

        responseJson.RowBreaks = [];
        responseJson.Aggregate = [];

        $('#tdHideSummaryDetail').hide();
        $('#tdNoRows').show();
        $('#RemoveAllColumns').show();
        $('#spnPreviewCaption').text("No Preview for KPI Report.");

        $('#trKPIScoreCard').show();

        if (boolInitial == true) {
            var new_arr = $.grep(arrReportColumnList, function (ColumnList, i) {
                return ColumnList["vcFieldDataType"] == 'M';
            });

            var TreeGroupData = _.groupBy(new_arr, function (innerArray) {
                return innerArray["numGroupID"];
            });

            $("#ddlKPIMeasureType").append('<option value="0_0_False">Record Count</option>');

            $.each(TreeGroupData, function (i, TreeGroup) {
                var selectOptionGroup = $('<optgroup label="' + TreeGroup[0]["vcGroupName"] + '"></optgroup');

                $.each(TreeGroup, function (i, item) {
                    selectOptionGroup.append('<option value="' + item.numFormFieldId + '">' + item.vcFieldName + '</option>')
                });

                selectOptionGroup.append('<option value="3_872_False">Average Order Amt</option>')
                selectOptionGroup.append('<option value="3_873_False">Total Profit Revenue</option>')
                selectOptionGroup.append('<option value="3_874_False">Average Profit Margin</option>')

                $('#ddlKPIMeasureType').append(selectOptionGroup);
            });

            $('#lblDateCaption').text('Date Measure');
            $("#ddlDateFieldFilter option[value='0~False']").remove();

            $('#spnDateCustom').hide();

            $('#lblDateRangeCaption').text('Period').css("font-weight", "bold");

            $("#ddlDateFieldRange").text('');
            $("#ddlDateFieldRange").append('<option value="Year">This Year vs. Last Year</option>');
            $("#ddlDateFieldRange").append('<option value="Quarter">This Quarter vs. Last Quarter</option>');
            $("#ddlDateFieldRange").append('<option value="Month">This Month vs. Last Month</option>');
            $("#ddlDateFieldRange").append('<option value="Week">This Week vs. Last Week</option>');

            $('#trScoreCard').hide();
        }

        $('#ddlKPIMeasureType').find("option[value='" + responseJson.KPIMeasureField + "']").prop("selected", true);

        if (responseJson.KPIThresold.length > 0) {
            $('#ddlKPIThreshold').find("option[value='" + responseJson.KPIThresold[0].ThresoldType + "']").prop("selected", true);
            $('#txtKPIThreshold').val(responseJson.KPIThresold[0].Value1);
        }
    }

    $("#ddlDateFieldFilter").find("option[value='" + responseJson.DateFieldFilter + "']").prop("selected", true);
    $("#ddlDateFieldRange").find("option[value='" + responseJson.DateFieldValue + "']").prop("selected", true);
    $("#txtFilter_FromDate").val(responseJson.FromDate);
    $("#txtFilter_ToDate").val(responseJson.ToDate);

    if ($("#ddlDateFieldRange").val() == 'Custom') {
        $('#spnDateCustom').show();
    }
    else {
        $('#spnDateCustom').hide();
    }

    $("#ddlRecordFilter").find("option[value='" + responseJson.RecordFilter + "']").prop("selected", true);

    makeDroppable();
    CreateFilterLogic();
    CreateFilterCondition(false);
}


function JQueryOnLoad() {

    //***Start Tree Control Operation***//

    //***Search Report Column List***//
    $("#SearchForm").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            on_filter();
            return false;
        }
    });

    $('#txtSearchGroup').change(function () {
        on_filter();
    });

    $('#btnGo').click(function () {
        on_filter();
        return false;
    });

    $('#txtSearchGroup').focus(function () {
        $('#SearchForm').css('border-color', '#09c');
        $('#txtSearchGroup').css('outline', 'none');
        $('#txtSearchGroup').css('color', '#515E81');
        if ($('#txtSearchGroup').val() == strSearchConst)
            $('#txtSearchGroup').val('');
    });

    $('#txtSearchGroup').blur(function () {
        $('#SearchForm').css('border-color', '#ebebeb');
        $('#txtSearchGroup').css('color', 'gray');
        if ($('#txtSearchGroup').val() == "")
            $('#txtSearchGroup').val(strSearchConst);
    });

    $('#btnFieldSearchAll').click(function () {
        $("#browser li").show();
        return false;
    });

    $('#btnFieldSearchLetter').click(function () {
        on_filterType($(this).attr("class"));
        return false;
    });

    $('#btnFieldSearchNumber').click(function () {
        on_filterType($(this).attr("class"));
        return false;
    });

    $('#btnFieldSearchDate').click(function () {
        on_filterType($(this).attr("class"));
        return false;
    });

    function on_filter() {
        var tval = $("#txtSearchGroup").val();

        if (tval != '' && tval != strSearchConst) {
            $("#browser li").hide();
            $("#browser ul li").hide().filter(":contains('" + tval + "')").find('li').andSelf().show().parent().parent().show();
            $("li[class*='expandable']").children('.hitarea').trigger('click');
        }
        else {
            $("#browser li").show();
        }
    }

    function on_filterType(type) {
        //console.log(type);
        $("#browser li").hide();
        $("#browser ul li").hide().find("[class*='" + $.trim(type.replace("btnSearch", "")) + "']").parent().parent().show().parent().parent().show();
        $("li[class*='expandable']").children('.hitarea').trigger('click');
    }

    //***END Tree Control Operation***//

    //**Show - Record Filter**//
    $('#ddlRecordFilter').change(function () {
        responseJson.RecordFilter = $("#ddlRecordFilter").val();
        LoadReportControls(true);
        return false;
    });

    //***Date Field Filter***//
    $('#btnDateFieldFilter').click(function () {
        responseJson.DateFieldFilter = $("#ddlDateFieldFilter").val();
        responseJson.DateFieldValue = $("#ddlDateFieldRange").val();

        var FromDate = $("#txtFilter_FromDate");

        if (!isDate($.trim(FromDate.val()), $('#hfValidDateFormat').val())) {
            FromDate.val("");
        }

        var ToDate = $("#txtFilter_ToDate");

        if (!isDate($.trim(ToDate.val()), $('#hfValidDateFormat').val())) {
            ToDate.val("");
        }
        responseJson.FromDate = $.trim($(FromDate).val());
        responseJson.ToDate = $.trim($(ToDate).val());
        LoadReportControls(true);
        return false;
    });

    $('#ddlDateFieldFilter').change(function () {
        responseJson.DateFieldFilter = $("#ddlDateFieldFilter").val();
        LoadReportControls(true);
        return false;
    });

    $('#ddlDateFieldRange').change(function () {
        if ($("#ddlDateFieldRange").val() == 'Custom') {
            $('#spnDateCustom').show();
        }
        else if (responseJson.ReportFormatType != 3) {
            $('#spnDateCustom').hide();
            responseJson.DateFieldFilter = $("#ddlDateFieldFilter").val();
            responseJson.DateFieldValue = $("#ddlDateFieldRange").val();
            var FromDate = $("#txtFilter_FromDate");

            if (!isDate($.trim(FromDate.val()), $('#hfValidDateFormat').val())) {
                FromDate.val("");
            }

            var ToDate = $("#txtFilter_ToDate");

            if (!isDate($.trim(ToDate.val()), $('#hfValidDateFormat').val())) {
                ToDate.val("");
            }
            responseJson.FromDate = $.trim($(FromDate).val());
            responseJson.ToDate = $.trim($(ToDate).val());
            LoadReportControls(true);
            return false;
        }
    });

    //***Any click on Page Hide Popup Menu***//
    $(document).click(function () {
        $('#GridTable tr th').children('.thhover').remove();
        HideAllMenu();
    });


    //***resizable Column List Tree Control***//
    $("#container").resizable({
        alsoResize: ".SearchBox, .treeDiv",
        minWidth: 255,
        maxWidth: 500,
        handles: 'e'
    });


    //***Column List Tree Control Drag & Drop***//
    $("#browser ul li").draggable({
        //connectToSortable: "#GridTable",
        //scope: "thColumn",
        connectToSortable: ".JCLLeft,.JCLRight",
        helper: function () {
            var text = $(this).text();//this.children[0].innerText;
            var result = "<div id='" + $(this).find('a').attr("id") + "' style='white-space:nowrap;' class='MoveField'>" + text + "</div>";
            return result;
        },
        revert: "invalid",
        //cursor: "move",
        //distance: "1",
        cursorAt: {
            //bottom: 10,
            //right: 10
            top: -10,
            left: -10
        },
        start: function (e, ui) {
            //$(ui.helper).addClass("ui-draggable-helper");
        },
        drag: function (e, ui) {
            var p = $("#GridTable");
            var position = p.position();

            var p2 = $("#fieldFiltersSection");
            var position2 = p2.position();

            var p3 = $("#MatrixTable");
            var position3 = p3.position();

            //console.log( "left: " + position.left  + ", top: " + position.top );
            //console.log( "height: " + $("#GridTable").height()  + ", width: " + $("#GridTable").width() );
            if ((e.clientX >= position.left && e.clientX <= position.left + $("#GridTable").width() &&
            e.clientY >= position.top && e.clientY <= position.top + $("#GridTable").height())
                && $.inArray($(this).find('a').attr("id"), responseJson.ColumnList) == -1 && $.inArray($(this).find('a').attr("id"), responseJson.GroupColumn) == -1
                ) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else if (e.clientX >= position2.left && e.clientX <= position2.left + $("#fieldFiltersSection").width() &&
                e.clientY >= position2.top && e.clientY <= position2.top + $("#fieldFiltersSection").height()
                 && ($("#fieldFiltersSection").find('.editMode').length == 0) && $("#FilterLogicSection").find('.editMode').length == 0) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else if ((e.clientX >= position3.left && e.clientX <= position3.left + $("#MatrixTable").width() &&
            e.clientY >= position3.top && e.clientY <= position3.top + $("#MatrixTable").height())
            && $.inArray($(this).find('a').attr("id"), responseJson.ColumnBreaks) == -1 && $.inArray($(this).find('a').attr("id"), responseJson.RowBreaks) == -1
            ) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else {
                $(ui.helper).removeClass("ui-draggable-helperAccept");
                $(ui.helper).addClass("ui-draggable-helperNotAccept");
            }
        },
    });


    //***Column List Tree Control Double Click add to Report (Only for Tabular and Summary Report)***//
    $("#browser a").dblclick(function () {
        if (responseJson.ReportFormatType == 0 || responseJson.ReportFormatType == 1 || responseJson.ReportFormatType == 5) {
            fieldID = $(this).attr("id");

            if ($.inArray(fieldID, responseJson.ColumnList) > -1 || $.inArray(fieldID, responseJson.GroupColumn) > -1) {
                return false;
            }
            else {
                responseJson.ColumnList.push(fieldID);
                LoadReportControls(true);
            }
        }
    });


    //***Remove all Columns***//
    $("#RemoveAllColumns").click(function () {
        responseJson.ColumnList = [];
        responseJson.GroupColumn = [];

        responseJson.ColumnBreaks = [];
        responseJson.RowBreaks = [];
        responseJson.Aggregate = [];

        LoadReportControls(false);
    });


    //***Change Report Format Type***//
    $("#ReportLayoutFormatType").click(function (e) {
        //$('#LayoutFormatMenu').css("display", "none");
        HideAllMenu();

        GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
        GridTop = $(this).offset().top + +$(this).outerHeight();

        $('#LayoutFormatMenu').css("left", GridLeft);
        $('#LayoutFormatMenu').css("top", GridTop);

        $('#LayoutFormatMenu').css("display", "inline-block");
        e.stopPropagation();
        return false;
    });

    //***Layout Format Type Change***//
    $("#LayoutFormatTabular,#LayoutFormatSummary,#LayoutFormatMatrix").click(function (e) {
        responseJson.ReportFormatType = $(this).parent().index();

        LoadReportControls(false);
    });


    //***Start Report Tabular & Summary Popup menu***//

    $("#GridTable tr").delegate('th', 'mouseenter mouseleave', function (e) {
        if ($("#GridTable tr").find("[id='DragHelp']").length == 0) {
            if (e.type == 'mouseenter') {
                if ($(this).children('.thhover').length == 0) {
                    var div = '<div class="thhover"></div>';
                    $(this).append(div);
                }
            }
            else {
                if ($('#GridMenu').css('display') !== 'block' || thSelIndex != $(this).index()) {
                    $(this).children('.thhover').remove();
                }
            }
        }

        $(".thhover").click(function (e) {
            //$('#GroupMenu').css("display", "none");
            HideAllMenu();

            //thSelGroupIndex = -1;

            $('#GridTable tr th').children('.thhover').not(this).remove();

            thSelIndex = $(this).parent().index();

            GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
            GridTop = $(this).parent().offset().top + $(this).parent().outerHeight();

            $('#GridMenu').css("left", "528px");
            $('#GridMenu').css("top", "250px");

            thColId = $('#GridTable tr th:nth-child(' + (thSelIndex + 1) + ')').attr("id");

            $('#GridMenu ul li').removeClass("x-item-disabled");

            var objColumn = findInColumnList("numFormFieldId", thColId);

            if (objColumn.bitAllowSorting == 'False') {
                $('#GridMenu ul li').find("[id='menuSortAscending']").parent().addClass("x-item-disabled");
                $('#GridMenu ul li').find("[id='menuSortDescending']").parent().addClass("x-item-disabled");
            }

            if (objColumn.bitAllowGrouping == 'False') {
                $('#GridMenu ul li').find("[id='menuGroupColumn']").parent().addClass("x-item-disabled");
            }

            if (responseJson.ReportFormatType != 1 || (objColumn.vcFieldDataType != 'N' && objColumn.vcFieldDataType != 'M') || objColumn.bitAllowAggregate == 'False') {
                $('#GridMenu ul li').find("[id='menuSummarizeColumn']").parent().addClass("x-item-disabled");
            }

            if (responseJson.SortColumn == thColId) {
                //console.log(SortColumn);
                //console.log(SortDirection);

                if (responseJson.SortDirection == "asc") {
                    $('#GridMenu ul li').find("[id='menuSortAscending']").parent().addClass("x-item-disabled");
                }
                else {
                    $('#GridMenu ul li').find("[id='menuSortDescending']").parent().addClass("x-item-disabled");
                }
            }

            if (responseJson.GroupColumn.length == 3) {
                $('#GridMenu ul li').find("[id='menuGroupColumn']").parent().addClass("x-item-disabled");
            }

            $('#GridMenu').css("display", "inline-block");
            e.stopPropagation();
            return false;
            //console.log("asdasdasdasd");
        });
    });

    $("#menuSortAscending").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelIndex != -1) {
            responseJson.SortColumn = $('#GridTable tr th:nth-child(' + (thSelIndex + 1) + ')').attr("id");
            responseJson.SortDirection = "asc";
            thSelIndex = -1;
            LoadReportControls(true);
        }
    });

    $("#menuSortDescending").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelIndex != -1) {
            responseJson.SortColumn = $('#GridTable tr th:nth-child(' + (thSelIndex + 1) + ')').attr("id");
            responseJson.SortDirection = "desc";
            thSelIndex = -1;
            LoadReportControls(true);
        }
    });

    $("#menuGroupColumn").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelIndex != -1) {
            fieldID = $('#GridTable tr th:nth-child(' + (thSelIndex + 1) + ')').attr("id");

            //for (var i = 0; i < thGroupBy.length; ++i) {
            //    console.log(thGroupBy[i]);
            //}

            //for (var i = 0; i < thColumn.length; ++i) {
            //    console.log(thColumn[i]);
            //}

            if ($.inArray(fieldID, responseJson.GroupColumn) == -1) {
                responseJson.GroupColumn.push(fieldID);
            }

            if ($.inArray(fieldID, responseJson.ColumnList) != -1) {
                responseJson.ColumnList.splice(thSelIndex, 1);
            }

            responseJson.ReportFormatType = 1;

            thSelIndex = -1;
            LoadReportControls(false);
        }
    });

    $("#menuSummarizeColumn").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelIndex != -1) {
            fieldID = findInColumnList("numFormFieldId", $('#GridTable tr th:nth-child(' + (thSelIndex + 1) + ')').attr("id"));

            if (fieldID.vcFieldDataType != 'N' && fieldID.vcFieldDataType != 'M')
                return false;

            if (fieldID.bitAllowAggregate == 'False')
                return false;

            arrFindSummaryField = findAggregateSum(responseJson.Aggregate, "Column", fieldID.numFormFieldId)

            if (arrFindSummaryField.length > 0) {
                if (findAggregateSum(arrFindSummaryField, "aggType", "sum").length > 0)
                    $('#chkSum').prop('checked', true);

                if (findAggregateSum(arrFindSummaryField, "aggType", "avg").length > 0)
                    $('#chkAverage').prop('checked', true);

                if (findAggregateSum(arrFindSummaryField, "aggType", "max").length > 0)
                    $('#chkMax').prop('checked', true);

                if (findAggregateSum(arrFindSummaryField, "aggType", "min").length > 0)
                    $('#chkMin').prop('checked', true);
            }
            else if (responseJson.Aggregate.length == 8)
                return false;

            $("#spnMatrixSummaryFieldLabel").text(fieldID.vcFieldName);
            $("#hdnMatrixSummaryFieldID").val(fieldID.numFormFieldId);

            $("#dialog-form").dialog("open");
        }
    });

    $("#menuRemoveColumn").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelIndex != -1) {
            responseJson.ColumnList.splice(thSelIndex, 1);
            thSelIndex = -1;
            LoadReportControls(false);
        }
    });

    $("#GroupMoveUp").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelGroupIndex != -1) {
            responseJson.GroupColumn.splice(thSelGroupIndex - 1, 0, responseJson.GroupColumn.splice(thSelGroupIndex, 1)[0]);
            thSelGroupIndex = -1;
            LoadReportControls(false);
        }
    });

    $("#GroupMoveDown").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelGroupIndex != -1) {
            responseJson.GroupColumn.splice(thSelGroupIndex + 1, 0, responseJson.GroupColumn.splice(thSelGroupIndex, 1)[0]);
            thSelGroupIndex = -1;
            LoadReportControls(false);
        }
    });

    $("#GroupRemove").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelGroupIndex != -1) {
            responseJson.GroupColumn.splice(thSelGroupIndex, 1);
            thSelGroupIndex = -1;
            LoadReportControls(false);
        }
    });

    //$("#GroupSortAscending").click(function (e) {
    //    if ($(this).parent().hasClass('x-item-disabled')) {
    //        e.stopPropagation();
    //        return false;
    //    }

    //    if (thSelGroupIndex != -1) {

    //        thSelGroupIndex = -1;
    //        makeDroppable();
    //    }
    //});

    //$("#GroupSortDescending").click(function (e) {
    //    if ($(this).parent().hasClass('x-item-disabled')) {
    //        e.stopPropagation();
    //        return false;
    //    }

    //    if (thSelGroupIndex != -1) {

    //        thSelGroupIndex = -1;
    //        makeDroppable();
    //    }
    //});

    //***END Report Tabular & Summary Popup menu***//


    //***All menu general function for add active class***//
    $("#GridMenu ul,#GroupMenu ul,#LayoutFormatMenu ul,#MatrixBreakMenu ul,#FilterMenu ul,#aggItemMenu ul").delegate('li', 'mouseenter mouseleave', function (e) {
        if ($(this).hasClass('x-item-disabled') == false && $(this).hasClass('x-menu-sep') == false) {
            if (e.type == 'mouseenter') {
                $(this).addClass("x-menu-active");
            }
            else {
                $(this).removeClass("x-menu-active");
            }
        }
    });

    //***Tabular & Summary Report Column Sorting***//
    $("#GridTable tr").delegate('a', 'click', function (e) {
        var th = $(this).parent();

        if (findInColumnList("numFormFieldId", $(th).attr("id")).bitAllowSorting == 'False')
            return false;

        if (responseJson.SortColumn == $(th).attr("id")) {
            responseJson.SortDirection = responseJson.SortDirection == "asc" ? "desc" : "asc";
        }
        else {
            responseJson.SortColumn = $(th).attr("id");
            responseJson.SortDirection = "asc";
        }

        LoadReportControls(true);
    });


    //***Add new Filter condition***//
    $("#AddNewFilter,#FilterAddNewCondition").click(function (e) {
        if ($("#fieldFiltersSection").find('.editMode').length == 0 && $("#FilterLogicSection").find('.editMode').length == 0) {
            responseJson.filters.push({ 'Column': arrReportColumnList[0].numFormFieldId, 'ColValue': '', 'OperatorType': 'eq', 'ColText': '' });
            CreateFilterCondition(true);
        }

        HideAllMenu();
        return false;
    });


    //***Add Filter Logic condition***//
    $("#FilterLogic").click(function (e) {
        if (responseJson.FilterLogic.length == 0 && responseJson.filters.length > 0) {
            FilterLogicString = [];

            for (i = 0; i < responseJson.filters.length; i++) {
                FilterLogicString.push((i + 1));
            }

            responseJson.FilterLogic = FilterLogicString.join(" AND ");
        }

        if (responseJson.FilterLogic.length > 0) {
            CreateFilterLogic();
            CreateFilterCondition(false);

            CreateEditModeFilterLogicSection();
            $("#fieldFiltersSection").find('.filterItemContainer').addClass('filterSectionHighlight');
        }

        HideAllMenu();
        return false;
    });

    //***Mouse Hover on Filter Condition***//
    $("#fieldFiltersSection").delegate('.filterItemContainer .filterItem .detailMode', 'mouseenter mouseleave', function (e) {
        if ($("#fieldFiltersSection").find('.editMode').length == 0 && $("#FilterLogicSection").find('.editMode').length == 0) {
            if (e.type == 'mouseenter') {
                $(this).addClass("filterHover");
                $(this).find('.editLink').show();
                $(this).find('.removeLink').show();
            }
            else {
                $(this).removeClass("filterHover");
                $(this).find('.editLink').hide();
                $(this).find('.removeLink').hide();
            }
        }
    });

    //***Mouse Hover on Filter Logic Condition***//
    $("#FilterLogicSection").delegate('.detailMode', 'mouseenter mouseleave', function (e) {
        if ($("#fieldFiltersSection").find('.editMode').length == 0 && $("#FilterLogicSection").find('.editMode').length == 0) {
            if (e.type == 'mouseenter') {
                $(this).addClass("filterHover");
                $(this).find('.editLink').show();
                $(this).find('.removeLink').show();
            }
            else {
                $(this).removeClass("filterHover");
                $(this).find('.editLink').hide();
                $(this).find('.removeLink').hide();
            }
        }
    });

    //***Edit Filter Condition***//
    $("#fieldFiltersSection").delegate('.fLink.editLink', 'click', function (e) {
        CreateEditModeFilterCondition($(this).parent().parent().parent());
    });

    //***Remove Filter Condition***//
    $("#fieldFiltersSection").delegate('.fLink.removeLink', 'click', function (e) {
        if (responseJson.FilterLogic.length > 0) {

            $(this).parent().parent().find('.detailContainer').addClass('removed');
            $(this).parent().parent().find('.undoLink').show();

            $('#fieldFiltersSection').removeClass("x-menu-active");
            $('#fieldFiltersSection').find('.editLink').hide();
            $('#fieldFiltersSection').find('.removeLink').hide();
            $(this).parent().parent().removeClass("filterHover");

            FilterRemoveIndex = $(this).parent().parent().parent().parent().index();

            CreateEditModeFilterLogicSection();
            FilterLogicValidate();

            $("#fieldFiltersSection").find('.filterItemContainer').addClass('filterSectionHighlight');
        }
        else {
            responseJson.filters.splice($(this).parent().parent().parent().parent().index(), 1);
            LoadReportControls(true);

            $('#fieldFiltersSection').removeClass("x-menu-active");

            $('#fieldFiltersSection').find('.editLink').hide();
            $('#fieldFiltersSection').find('.removeLink').hide();
            $('#fieldFiltersSection').find('.undoLink').hide();
        }
    });


    //***Undo Filter Condition***//
    $("#fieldFiltersSection").delegate('.fLink.undoLink', 'click', function (e) {
        $("#FilterLogicSection").find(".detailMode").removeClass('x-hide-display');
        $("#FilterLogicSection").find(".editMode").remove();
        $("#FilterLogicSection").find(".FilterLogicError").remove();

        $('#AddNewFilter').prop('disabled', false);
        $('#thFilterMenu').prop('disabled', false);

        $("#fieldFiltersSection").find('.filterItemContainer').removeClass('filterSectionHighlight');

        $('#fieldFiltersSection').find('.undoLink').hide();
        $('#fieldFiltersSection').find('.detailContainer').removeClass('removed');

        FilterRemoveIndex = -1;

        //e.stopPropagation();
        return false;
    });


    $("#fieldFiltersSection").delegate('.editMode #FilterEditModeColumnList', 'change', function (e) {

        var filteOperator = $("#fieldFiltersSection").find('.editMode #FilterEditModeOperator');
        $(filteOperator).empty();

        SelectColumn = findInColumnList('numFormFieldId', $(this).val());

        var filterOperatorList = typeFilterOperatorsMap[SelectColumn.vcAssociatedControlType + "_" + SelectColumn.vcFieldDataType]
        $.each(filterOperatorList, function (i, OperatorType) {
            $(filteOperator).append('<option value="' + OperatorType + '">' + filterConditionOperators[OperatorType] + '</option>')
        });

        var spnFilterEditMode = $("#fieldFiltersSection").find('.editMode #spnFilterEditMode');
        $(spnFilterEditMode).text('');

        if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox' || SelectColumn.vcAssociatedControlType == 'CheckBoxList') {
            var ddlData = GetDropDownData(SelectColumn.numListID, SelectColumn.vcListItemType, SelectColumn.vcDbColumnName, SelectColumn.vcAssociatedControlType);

            var ddlFilterEditMode = $('<select id="ddlFilterEditMode" multiple="multiple"></select>');

            $.each(ddlData, function (i, data) {
                ddlFilterEditMode.append('<option value="' + data.numID + '">' + data.vcData + '</option>')
            });

            spnFilterEditMode.append(ddlFilterEditMode);
            $("#ddlFilterEditMode").dropdownchecklist({ width: 180, maxDropHeight: 350 });
        }
        else if (SelectColumn.vcAssociatedControlType == 'DateField') {
            spnFilterEditMode.append('<input type="text" id="txtFilter_calDate" autocomplete="off"/>');
            spnFilterEditMode.append('&nbsp;&nbsp;');
            spnFilterEditMode.append('<img id="imgFilter_calDate" class="hyperlink" style="border-width:0px;" src="../Images/Calender.gif">');
            setupCal('imgFilter_calDate', 'txtFilter_calDate', $('#hfDateFormat').val(), false);
        }
        else if (SelectColumn.vcFieldDataType == 'M') {
            spnFilterEditMode.append('<input type="text" id="FilterEditModeValue" autocomplete="off" class="required_money {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>');
        }
        else if (SelectColumn.vcFieldDataType == 'N') {
            spnFilterEditMode.append('<input type="text" id="FilterEditModeValue" autocomplete="off" class="required_integer {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>');
        }
        else {
            spnFilterEditMode.append('<input type="text" id="FilterEditModeValue" autocomplete="off"/>');
        }
    });

    $("#fieldFiltersSection").delegate('.editMode #FilterEditModeOk', 'click', function (e) {
        SelectColumn = findInColumnList('numFormFieldId', $(this).parent().find('#FilterEditModeColumnList').val());

        var colValue = "";
        var colText = "";

        if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox' || SelectColumn.vcAssociatedControlType == 'CheckBoxList') {
            ddlFilterEditMode1 = $(this).parent().find('#ddlFilterEditMode');
            //console.log($(ddlFilterEditMode1).val());
            //console.log($(ddlFilterEditMode1).text());

            $(ddlFilterEditMode1).find('option:selected').each(function (i, selected) {
                //console.log($(selected).val());
                //console.log($(selected).text());

                if (colValue != "") {
                    colValue += ",";
                    colText += ",";
                }
                colValue += $(selected).val();
                colText += $(selected).text();
            });

            if (colValue == "") {
                colValue = '0';
            }
            //for (i = 0; i < ddlFilterEditMode.options.length; i++) {
            //    if (ddlFilterEditMode.options[i].selected && (ddlFilterEditMode.options[i].value != "")) {
            //        if (colValue != "") {
            //            colValue += ",";
            //            colText += ",";
            //        }
            //        colValue += ddlFilterEditMode.options[i].value;
            //        colText += ddlFilterEditMode.options[i].text;
            //    }
            //}

            //console.log(colValue);
            //console.log(colText);
        }
        else if (SelectColumn.vcAssociatedControlType == 'DateField') {
            var DateValue = $.trim($(this).parent().find('#txtFilter_calDate').val());

            if (!isDate(DateValue, $('#hfValidDateFormat').val())) {
                $(this).parent().find('.FilterValueError').html(" Error: Invalid date").show();
                return false;
            }

            colValue = DateValue;
            colText = DateValue;
        }
        else {
            colValue = $(this).parent().find('#FilterEditModeValue').val();
            colText = $(this).parent().find('#FilterEditModeValue').val();
        }

        responseJson.filters[$(this).parent().parent().parent().index()] = new FilterList($(this).parent().find('#FilterEditModeColumnList').val(), colValue, $(this).parent().find('#FilterEditModeOperator').val(), colText);

        $('#AddNewFilter').prop('disabled', false);
        $('#thFilterMenu').prop('disabled', false);

        if (NewFilterCondition == true && responseJson.FilterLogic.length > 0) {
            responseJson.FilterLogic = '(' + responseJson.FilterLogic + ') AND ' + ($(this).parent().parent().parent().index() + 1);
        }

        LoadReportControls(true);

        //e.stopPropagation();
        return false;
    });

    $("#fieldFiltersSection").delegate('.editMode #FilterEditModeCancel', 'click', function (e) {

        //console.log($(this).parent().parent());
        var fieldFilter = $(this).parent().parent();
        fieldFilter.find(".detailMode").removeClass('x-hide-display');
        fieldFilter.find(".editMode").remove();

        $('#AddNewFilter').prop('disabled', false);
        $('#thFilterMenu').prop('disabled', false);

        if (NewFilterCondition == true) {
            responseJson.filters.pop();
            CreateFilterCondition(false);
        }

        //e.stopPropagation();
        return false;
    });

    $("#FilterLogicSection").delegate('.fLink.editLink', 'click', function (e) {
        CreateEditModeFilterLogicSection();

        $("#fieldFiltersSection").find('.filterItemContainer').addClass('filterSectionHighlight');
    });

    $("#FilterLogicSection").delegate('.fLink.removeLink,.editMode #FilterLogicEditModeRemove', 'click', function (e) {
        responseJson.FilterLogic = "";
        $("#FilterLogicSection").html('');

        if (FilterRemoveIndex > -1) {
            responseJson.filters.splice(FilterRemoveIndex, 1);
            FilterRemoveIndex = -1;
        }

        LoadReportControls(true);

        $('#FilterLogicSection').removeClass("x-menu-active");
        $('#FilterLogicSection').find('.editLink').hide();
        $('#FilterLogicSection').find('.removeLink').hide();
    });

    $("#FilterLogicSection").delegate('.editMode #txtFilterLogicValue', 'keyup', function (e) {
        FilterLogicValidate();
    });

    function FilterLogicValidate() {
        try {
            for (b = [], c = 0; c < responseJson.filters.length; c++)
                FilterRemoveIndex == c || b.push(c + 1);

            if (FilterRemoveIndex > -1) {
                $("#FilterLogicSection").find('#FilterLogicEditModeCancel').prop('disabled', true)
            }

            InputCondition = $.trim($("#FilterLogicSection").find('#txtFilterLogicValue').val());

            //console.log(InputCondition);
            var message = tokenizer.getFiltersInUseMessage(b);

            if (message != null) {
                $("#FilterLogicSection").find('.FilterLogicError').html(message).show();
                $("#FilterLogicSection").find('#txtFilterLogicValue').addClass('x-form-invalid');

                $("#FilterLogicSection").find('#FilterLogicEditModeOk').prop('disabled', true)
            }
            else {
                $("#FilterLogicSection").find('.FilterLogicError').hide();
                $("#FilterLogicSection").find('#txtFilterLogicValue').removeClass('x-form-invalid');

                $("#FilterLogicSection").find('#FilterLogicEditModeOk').prop('disabled', false)
            }
        }
        catch (e) {
            var message = e.message ? e.message : e;
            $("#FilterLogicSection").find('.FilterLogicError').html(message).show();
            $("#FilterLogicSection").find('#txtFilterLogicValue').addClass('x-form-invalid');
            $("#FilterLogicSection").find('#FilterLogicEditModeOk').prop('disabled', true)

            //console.log(e.message?e.message:e);
        }
    }

    $("#FilterLogicSection").delegate('.editMode #FilterLogicEditModeOk', 'click', function (e) {

        InputCondition = $.trim($("#FilterLogicSection").find('#txtFilterLogicValue').val());

        $('#AddNewFilter').prop('disabled', false);
        $('#thFilterMenu').prop('disabled', false);

        if (FilterRemoveIndex > -1) {
            var a = [], b = /\d+/g, c = InputCondition.match(/\s+|\w+|\d+|\(|\)|\W+/g);

            //console.log(c);
            for (var e = 0; e < c.length; e++) {
                var d = c[e], f = d.match(b);

                if (f && 1 == f.length && f[0] === d) {
                    if (d > FilterRemoveIndex)
                        a.push(parseInt(f[0]) - 1)
                    else
                        a.push(d)
                }
                else a.push(d)
            }

            InputCondition = a.join("")

            responseJson.filters.splice(FilterRemoveIndex, 1);
            FilterRemoveIndex = -1;
        }

        responseJson.FilterLogic = InputCondition;

        LoadReportControls(true);

        return false;
    });

    $("#FilterLogicSection").delegate('.editMode #FilterLogicEditModeCancel', 'click', function (e) {
        $("#FilterLogicSection").find(".detailMode").removeClass('x-hide-display');
        $("#FilterLogicSection").find(".editMode").remove();
        $("#FilterLogicSection").find(".FilterLogicError").remove();

        $('#AddNewFilter').prop('disabled', false);
        $('#thFilterMenu').prop('disabled', false);

        $("#fieldFiltersSection").find('.filterItemContainer').removeClass('filterSectionHighlight');

        //e.stopPropagation();
        return false;
    });

    $(".thFilterMenu").click(function (e) {
        if ($("#fieldFiltersSection").find('.editMode').length == 0 && $("#FilterLogicSection").find('.editMode').length == 0) {
            //$('#GridMenu').css("display", "none");
            HideAllMenu();

            //thSelIndex = -1;

            thSelGroup = $(this).parent().attr("id");
            thSelGroupIndex = $.inArray(thSelGroup, responseJson.GroupColumn);

            //console.log(thSelGroup);

            GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
            GridTop = $(this).parent().offset().top + $(this).parent().outerHeight();

            $('#FilterMenu').css("left", "344px");
            $('#FilterMenu').css("top", "70px");

            $('#FilterMenu ul li').removeClass("x-item-disabled");

            if (responseJson.filters.length == 0 || responseJson.FilterLogic.length > 0) {
                $('#FilterMenu ul li').find("[id='FilterLogic']").parent().addClass("x-item-disabled");
            }

            $('#FilterMenu').css("display", "inline-block");
            $('#FilterMenu').width("240px");

            e.stopPropagation();
            return false;
        }
    });



    $("#MatrixBreakMenuRemoveGroup").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (thSelIndex != -1) {
            if (thSelIndex == 0)
                responseJson.ColumnBreaks.pop();
            else if (thSelIndex == 1)
                responseJson.RowBreaks.pop();

            thSelIndex = -1;
            makeDroppable();
        }
    });

    $("#MatrixSummarizeField").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (summaryGrpClass != "") {
            fieldID = findInColumnList("numFormFieldId", summaryGrpClass.split('-')[1]);
            arrFindSummaryField = findAggregateSum(responseJson.Aggregate, "Column", fieldID.numFormFieldId)

            if (arrFindSummaryField.length > 0) {
                if (findAggregateSum(arrFindSummaryField, "aggType", "sum").length > 0)
                    $('#chkSum').prop('checked', true);

                if (findAggregateSum(arrFindSummaryField, "aggType", "avg").length > 0)
                    $('#chkAverage').prop('checked', true);

                if (findAggregateSum(arrFindSummaryField, "aggType", "max").length > 0)
                    $('#chkMax').prop('checked', true);

                if (findAggregateSum(arrFindSummaryField, "aggType", "min").length > 0)
                    $('#chkMin').prop('checked', true);
            }

            $("#spnMatrixSummaryFieldLabel").text(fieldID.vcFieldName);
            $("#hdnMatrixSummaryFieldID").val(fieldID.numFormFieldId);

            $("#dialog-form").dialog("open");
        }
    });

    $("#MatrixRemoveSummary").click(function (e) {
        if ($(this).parent().hasClass('x-item-disabled')) {
            e.stopPropagation();
            return false;
        }

        if (summaryGrpClass != "") {

            responseJson.Aggregate = _(responseJson.Aggregate).reject(function (el) { return el.Column === summaryGrpClass.split('-')[1] && el.aggType === summaryGrpClass.split('-')[2]; });

            summaryGrpClass = "";
            makeDroppable();
        }
    });

    $("#MatrixTable").delegate('tr td .summaryGrp', 'mouseenter mouseleave', function (e) {
        if (e.type == 'mouseenter') {
            if ($(this).children('.matrixMenuTrigger').length == 0) {
                var div = '<div class="matrixMenuTrigger"></div>';
                $(this).append(div);
            }
        }
        else {
            $(this).children('.matrixMenuTrigger').remove();
        }

        $(".matrixMenuTrigger").click(function (e) {
            HideAllMenu();

            summaryGrpClass = $(this).parent().attr('class').split(' ')[1];

            $('#MatrixTable tr td .summaryGrp').children('.matrixMenuTrigger').not(this).remove();

            GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
            GridTop = $(this).parent().offset().top + $(this).parent().outerHeight();

            $('#aggItemMenu').css("left", GridLeft);
            $('#aggItemMenu').css("top", GridTop);

            $('#aggItemMenu').css("display", "inline-block");
            e.stopPropagation();
            return false;
        });
    });

    $("#dialog-form").dialog({
        autoOpen: false,
        height: 200,
        width: 350,
        modal: true,
        closeOnEscape: false,
        buttons: {
            "Apply": function () {

                responseJson.Aggregate = _(responseJson.Aggregate).reject(function (el) { return el.Column === $("#hdnMatrixSummaryFieldID").val() && (el.aggType === "sum" || el.aggType === "avg" || el.aggType === "max" || el.aggType === "min"); });

                if ($('#chkSum').prop('checked'))
                    responseJson.Aggregate.push({ 'Column': $("#hdnMatrixSummaryFieldID").val(), 'aggType': 'sum' });

                if ($('#chkAverage').prop('checked'))
                    responseJson.Aggregate.push({ 'Column': $("#hdnMatrixSummaryFieldID").val(), 'aggType': 'avg' });

                if ($('#chkMax').prop('checked'))
                    responseJson.Aggregate.push({ 'Column': $("#hdnMatrixSummaryFieldID").val(), 'aggType': 'max' });

                if ($('#chkMin').prop('checked'))
                    responseJson.Aggregate.push({ 'Column': $("#hdnMatrixSummaryFieldID").val(), 'aggType': 'min' });

                cleardialogForm();
                $(this).dialog("close");
                LoadReportControls(true);
            },
            Cancel: function () {
                cleardialogForm();
                $(this).dialog("close");
            }
        },
        close: function () {
            cleardialogForm();
        }
    });

    //Hide Detail Summary Clicked
    $("#bitHideSummaryDetail").change(function () {
        if ($(this).is(":checked")) {
            responseJson.bitHideSummaryDetail = true;
        } else {
            responseJson.bitHideSummaryDetail = false;
        }

        LoadReportControls(false);
    });

    function cleardialogForm() {
        $('#chkSum').prop('checked', false);
        $('#chkAverage').prop('checked', false);
        $('#chkMax').prop('checked', false);
        $('#chkMin').prop('checked', false);
        $("#hdnMatrixSummaryFieldID").val("");
        $("#spnMatrixSummaryFieldLabel").text("");
        summaryGrpClass = "";
    }
}



function BindGroupHeader(Group, FirstGroup, GroupIndex, GroupID, GroupName, GroupRecordCount) {
    var trGroup = $("<tr class='group-hd'></tr>");

    var divGroupMenu = '';

    if (FirstGroup == 0) {
        divGroupMenu = '<div class="thGroupMenu"></div>';
    }

    var tdGroupTitle = $("<div></div>").addClass("GroupClassTitle").css("padding-left", 20 * GroupIndex).html(findInColumnList('numFormFieldId', GroupID).vcFieldName + ' : ' + GroupName + ' (' + GroupRecordCount + ' Records)');
    var tdGroup = $("<td id=" + GroupID + "></td>").attr("class", "GroupClass" + GroupIndex).attr("colspan", responseJson.ColumnList.length).append(divGroupMenu).append(tdGroupTitle);
    trGroup.append(tdGroup);

    $("#GridTable tbody").append(trGroup);

    //Adds aggregated value
    if (responseJson.Aggregate.length > 0) {
        var trGroupAggregate = $("<tr></tr>");
        var tdGroupAggregate;
        $.each(responseJson.ColumnList, function (i, column) {
            tdGroupAggregate = null;

            var exist = $.grep(responseJson.Aggregate, function (obj) {
                return obj.Column === column;
            });

            if (exist.length === 0) {
                tdGroupAggregate = $("<td class=\"GroupClass0\" style='border-top:0px !important; text-align:right; vertical-align:top'></td>");
                trGroupAggregate.append(tdGroupAggregate);
            }
            else {


                tdGroupAggregate = $("<td class=\"GroupClass0\" style='border-top:0px !important; text-align:right; font-weight:bold; vertical-align:top'></td>");
                var total = 0;
                var max = 0;
                var min = 0;

                $.each(Group, function (data, obj) {
                    total += obj[exist[0].Column];
                    if (max != null && obj[exist[0].Column] > max) {
                        max = obj[exist[0].Column];
                    }
                    if (data == 0) {
                        min = obj[exist[0].Column];
                    }
                    else if (obj[exist[0].Column] < min) {
                        min = obj[exist[0].Column];
                    }
                });

                var finalResult = "{SUM}{AVG}{MAX}{MIN}";

                $.each(exist, function (i, obj) {
                    if (obj.aggType == "sum") {
                        finalResult = finalResult.replace("{SUM}", "Total " + total.toFixed(2));
                    } else if (obj.aggType == "avg") {
                        if (total == null || total == 0) {
                            finalResult = finalResult.replace("{AVG}","<br />" + "Average 0");
                        } else {
                            finalResult = finalResult.replace("{AVG}", "<br />" + "Average " + (total / Group.length).toFixed(2));
                        }
                    } else if (obj.aggType == "max") {
                        finalResult = finalResult.replace("{MAX}", "<br />" + "Max " + max.toFixed(2));
                    } else if (obj.aggType == "min") {
                        finalResult = finalResult.replace("{MIN}", "<br />" + "Min " + min.toFixed(2));
                    }
                });

                finalResult = finalResult.replace("{SUM}", "");
                finalResult = finalResult.replace("{AVG}", "");
                finalResult = finalResult.replace("{MAX}", "");
                finalResult = finalResult.replace("{MIN}", "");

                if (finalResult.startsWith("<br />")) {
                    finalResult = finalResult.substring(6, finalResult.length)
                }

                tdGroupAggregate.append(finalResult);
                trGroupAggregate.append(tdGroupAggregate);
            }
        });

        //$.each(responseJson.Aggregate, function (k, item1) {
        //    var str = "";

        //    switch (item1.aggType) {
        //        case "sum":
        //            str = "Sum of No. of";
        //            break;
        //        case "avg":
        //            str = "Average of No. of";
        //            break;
        //        case "max":
        //            str = "Largest of No. of";
        //            break;
        //        case "min":
        //            str = "Smallest of No. of";
        //            break;
        //    }

        //    div = $("<div></div>").addClass("summaryGrp aggItem-" + item1.Column + "-" + item1.aggType).append(str + " " + findInColumnList('numFormFieldId', item1.Column).vcFieldName)

        //    trGroupAggregate.append(div)
        //});

        $("#GridTable tbody").append(trGroupAggregate);
    }
}


function BindGroupZone(GroupIndex) {
    var trGroup = $("<tr></tr>");

    var tdGroupTitle = $("<div></div>").css("padding-left", 20 * GroupIndex).html('Drop a field here to create a grouping.');
    var tdGroup = $("<td></td>").attr("class", "group-dzone-hd").attr("colspan", responseJson.ColumnList.length).append(tdGroupTitle);
    trGroup.append(tdGroup);
    $("#GridTable tbody").append(trGroup);
}


function BindTableData(TableData) {
    if (!responseJson.bitHideSummaryDetail) {
        $.each(TableData, function (i, item) {
            var tr = $("<tr></tr>");

            $("#GridTable tr th").each(function () {
                var text1 = new String(item[$(this).attr("id")]);
                var objColumn = findInColumnList("numFormFieldId", $(this).attr("id"));
                if (objColumn.vcFieldDataType == 'N' || objColumn.vcFieldDataType == 'M') {
                    var td = $("<td style='text-align:right;'></td>").html(text1.length == 0 ? '&nbsp;' : formatNumberReport(text1));
                } else {
                    var td = $("<td></td>").html(text1.length == 0 ? '&nbsp;' : text1);
                }


                tr.append(td);
            })
            $("#GridTable tbody").append(tr);

        });
    }
}

function formatNumberReport(n) {
    if ($.isNumeric(n)) {
        return parseFloat(n).toLocaleString("en-US");
    } else {
        return n;
    }
}

function CreateFilterCondition(addNewFilter) {

    $("#fieldFiltersSection").html('');
    $.each(responseJson.filters, function (i, item) {
        var filterItemContainer = $('<div></div>').addClass('filterItemContainer');

        var filterPrefix = $('<div></div>').addClass('filterPrefix');

        if (responseJson.FilterLogic.length > 0) {
            filterPrefix.append((i + 1) + '.');
        }
        else {
            if (i > 0) {
                filterPrefix.append('AND');
            }
        }

        filterItemContainer.append(filterPrefix);

        var fieldFilter = $('<div></div>').addClass('filterItem').addClass('fieldFilter');

        var detailMode = $('<div></div>').addClass('detailMode');

        detailMode.append('<div class="detailContainer"><b>' + findInColumnList('numFormFieldId', item.Column).vcFieldName + '</b> ' + filterConditionOperators[item.OperatorType] + ' <b style="white-space: initial;">"' + item.ColText + '"</b></div>');

        var fLinks = $('<span></span>').addClass('fLinks');
        fLinks.append('<a class="fLink editLink" style="display:none">Edit</a>');
        fLinks.append('<a class="fLink removeLink" style="display:none">Remove</a>');
        fLinks.append('<a class="fLink undoLink" style="display:none">Undo</a>');

        detailMode.append(fLinks);

        fieldFilter.append(detailMode);

        filterItemContainer.append(fieldFilter);

        $("#fieldFiltersSection").append(filterItemContainer);

        NewFilterCondition = false;

        if (addNewFilter && i == responseJson.filters.length - 1) {
            NewFilterCondition = true;
            CreateEditModeFilterCondition(fieldFilter);
        }
    });
}


function CreateEditModeFilterCondition(fieldFilter) {
    //console.log($(this).parent().parent().parent());
    //var fieldFilter = $(this).parent().parent().parent();

    fieldFilter.find(".detailMode").addClass('x-hide-display');

    var editMode = $('<div></div>').addClass('editMode');

    //console.log(arrFilters[$(this).parent().parent().parent().parent().index()].numFormFieldId);
    SelectedFilterIndex = responseJson.filters[fieldFilter.parent().index()];
    SelectColumn = findInColumnList('numFormFieldId', SelectedFilterIndex.Column);


    var FilterEditModeColumnList = $('<select id="FilterEditModeColumnList"></select>');

    var TreeGroupData = _.groupBy(arrReportColumnList, function (innerArray) {
        return innerArray["numGroupID"];
    });

    $.each(TreeGroupData, function (i, TreeGroup) {

        var selectOptionGroup = $('<optgroup label="' + TreeGroup[0]["vcGroupName"] + '"></optgroup');

        $.each(TreeGroup, function (i, item) {
            selectOptionGroup.append('<option value="' + item.numFormFieldId + '">' + item.vcFieldName + '</option>')
        });

        FilterEditModeColumnList.append(selectOptionGroup);
    });

    FilterEditModeColumnList.find("option[value='" + SelectedFilterIndex.Column + "']").prop("selected", true);
    editMode.append(FilterEditModeColumnList);
    editMode.append('&nbsp;&nbsp;');

    var filteOperator = $('<select id="FilterEditModeOperator"></select>');
    var filterOperatorList = typeFilterOperatorsMap[SelectColumn.vcAssociatedControlType + "_" + SelectColumn.vcFieldDataType]
    $.each(filterOperatorList, function (i, OperatorType) {
        filteOperator.append('<option value="' + OperatorType + '">' + filterConditionOperators[OperatorType] + '</option>')
    });
    filteOperator.find("option[value='" + SelectedFilterIndex.OperatorType + "']").prop("selected", true);
    editMode.append(filteOperator);
    editMode.append('&nbsp;&nbsp;');

    var spnFilterEditMode = $('<span id="spnFilterEditMode"></span>');

    if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox' || SelectColumn.vcAssociatedControlType == 'CheckBoxList') {
        var ddlData = GetDropDownData(SelectColumn.numListID, SelectColumn.vcListItemType, SelectColumn.vcDbColumnName, SelectColumn.vcAssociatedControlType);

        var ddlFilterEditMode = $('<select id="ddlFilterEditMode" multiple="multiple"></select>');

        $.each(ddlData, function (i, data) {
            ddlFilterEditMode.append('<option value="' + data.numID + '">' + data.vcData + '</option>')
        });

        var separated = SelectedFilterIndex.ColValue.split(",");

        $.each(separated, function (i, selValue) {
            ddlFilterEditMode.find("option[value='" + selValue + "']").prop("selected", true);
        });

        spnFilterEditMode.append(ddlFilterEditMode);
        spnFilterEditMode.append('&nbsp;&nbsp;');
    }
    else if (SelectColumn.vcAssociatedControlType == 'DateField') {
        spnFilterEditMode.append('<input type="text" id="txtFilter_calDate" value="' + SelectedFilterIndex.ColValue + '" autocomplete="off"/>');
        spnFilterEditMode.append('&nbsp;&nbsp;');
        spnFilterEditMode.append('<img id="imgFilter_calDate" class="hyperlink" style="border-width:0px;" src="../Images/Calender.gif">');
    }
    else if (SelectColumn.vcFieldDataType == 'M') {
        spnFilterEditMode.append('<input type="text" id="FilterEditModeValue" value="' + SelectedFilterIndex.ColValue + '" autocomplete="off" class="required_money {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>');
    }
    else if (SelectColumn.vcFieldDataType == 'N') {
        spnFilterEditMode.append('<input type="text" id="FilterEditModeValue" value="' + SelectedFilterIndex.ColValue + '" autocomplete="off" class="required_integer {required:false ,number:true, maxlength:12, messages:{number:\'provide valid value!\'}} money"/>');
    }
    else {
        spnFilterEditMode.append('<input type="text" id="FilterEditModeValue" value="' + SelectedFilterIndex.ColValue + '" autocomplete="off"/>');
    }

    editMode.append(spnFilterEditMode);
    editMode.append('&nbsp;&nbsp;');

    editMode.append('<input type="submit" id="FilterEditModeOk" value="OK" class="buttonNew"/>');
    editMode.append('&nbsp;');
    editMode.append('<input type="submit" id="FilterEditModeCancel" value="Cancel" class="buttonNew"/>');

    var FilterValueError = $('<span></span>').addClass('FilterValueError').hide();
    editMode.append(FilterValueError);
    //detailMode.append('<div class="detailContainer"><b>' + findInColumnList('numFormFieldId',item.numFormFieldId).vcFieldName + '</b> ' + item.vcOperator + ' <b>"' + item.vcValue + '"</b></div>');

    //var fLinks = $('<span></span>').addClass('fLinks');
    //fLinks.append('<a class="fLink editLink" style="">Edit</a>');
    //fLinks.append('<a class="fLink removeLink" style="">Remove</a>');
    //fLinks.append('<a class="fLink undoLink" style="display:none">Undo</a>');

    //detailMode.append(fLinks);

    fieldFilter.append(editMode);

    if (SelectColumn.vcAssociatedControlType == 'SelectBox' || SelectColumn.vcAssociatedControlType == 'CheckBox' || SelectColumn.vcAssociatedControlType == 'CheckBoxList') {
        $("#ddlFilterEditMode").dropdownchecklist({ width: 180, maxDropHeight: 350 });
    }
    else if (SelectColumn.vcAssociatedControlType == 'DateField') {
        setupCal('imgFilter_calDate', 'txtFilter_calDate', $('#hfDateFormat').val(), false);
    }

    $('#fieldFiltersSection').removeClass("x-menu-active");
    $('#fieldFiltersSection').find('.editLink').hide();
    $('#fieldFiltersSection').find('.removeLink').hide();

    $('#AddNewFilter').prop('disabled', true);
    $('#thFilterMenu').prop('disabled', true);
}


function CreateFilterLogic() {
    $("#FilterLogicSection").html('');

    var detailMode = $('<div></div>').addClass('detailMode');

    if (responseJson.FilterLogic.length > 0) {
        detailMode.append('Filter Logic: <b>' + responseJson.FilterLogic + '</b>');

        var fLinks = $('<span></span>').addClass('fLinks');
        fLinks.append('<a class="fLink editLink" style="display:none">Edit</a>');
        fLinks.append('<a class="fLink removeLink" style="display:none">Remove</a>');

        detailMode.append(fLinks);

        $("#FilterLogicSection").append(detailMode);

        //CreateFilterCondition(false);
    }
}

function CreateEditModeFilterLogicSection() {

    $("#FilterLogicSection").find(".detailMode").addClass('x-hide-display');

    var editMode = $('<div></div>').addClass('editMode');

    editMode.append('Filter Logic: ');

    editMode.append('<input type="text" id="txtFilterLogicValue" value="' + responseJson.FilterLogic + '" style="width:600px" autocomplete="off"/>');
    editMode.append('&nbsp;&nbsp;');

    editMode.append('<input type="submit" id="FilterLogicEditModeOk" value="OK" class="buttonNew"/>');
    editMode.append('&nbsp;');
    editMode.append('<input type="submit" id="FilterLogicEditModeCancel" value="Cancel" class="buttonNew"/>');
    editMode.append('&nbsp;');
    editMode.append('<input type="submit" id="FilterLogicEditModeRemove" value="Remove" class="buttonNew"/>');

    $("#FilterLogicSection").append(editMode);

    var FilterLogicError = $('<div></div>').addClass('FilterLogicError').hide();
    $("#FilterLogicSection").append(FilterLogicError);

    $('#FilterLogicSection').removeClass("x-menu-active");
    $('#FilterLogicSection').find('.editLink').hide();
    $('#FilterLogicSection').find('.removeLink').hide();

    $('#AddNewFilter').prop('disabled', true);
    $('#thFilterMenu').prop('disabled', true);
}



function makeDroppable() {
    isValidDrop = true;

    $('#GridTable').find('thead tr').html('');
    $('#GridTable tbody').html('');

    $('#MatrixTable thead').html('');
    $('#MatrixTable tbody').html('');

    if (responseJson.ReportFormatType == 0 || responseJson.ReportFormatType == 1 || responseJson.ReportFormatType == 5) { //For Tabular & Summary Format
        if (responseJson.ColumnList.length == 0) {
            var a = $("<th id='DragHelp'></th>").html('Add columns by dragging fields into the preview pane.');
            $('#GridTable').find('thead tr').prepend(a);
        }
        else {

            for (var i = 0; i < responseJson.ColumnList.length; ++i) {
                var Field = findInColumnList('numFormFieldId', responseJson.ColumnList[i]);

                if (Field != undefined) {
                    var a = $("<th id=" + responseJson.ColumnList[i] + "></th>").html("<a href='#'>" + Field.vcFieldName + "</a>");

                    if (responseJson.SortColumn == findInColumnList('numFormFieldId', responseJson.ColumnList[i]).numFormFieldId) {
                        //console.log(SortColumn);
                        //console.log(SortDirection);

                        if (responseJson.SortDirection == 'asc')
                            $(a).append("&nbsp;<span class='sortAsc'>&nbsp;&nbsp;</span>");
                        else
                            $(a).append("&nbsp;<span class='sortDesc'>&nbsp;&nbsp;</span>");
                    }

                    $('#GridTable').find('thead tr').append(a);
                }
            }

            if (responseJson.ReportFormatType == 1 || responseJson.ReportFormatType == 5) {
                if (data == null)
                    data = [];

                if (responseJson.GroupColumn.length > 0) {

                    var Group0 = _.groupBy(data, function (innerArray) {
                        return innerArray[responseJson.GroupColumn[0]];
                    });

                    //console.log(Group0);
                    //console.log(JSON.stringify(Group0));

                    var GroupCount0 = 0;
                    var GroupCount1 = 0;
                    var GroupCount2 = 0;

                    $.each(Group0, function (i, Group0Data) {
                        //console.log(Group0Data);

                        BindGroupHeader(Group0Data, GroupCount0, 0, responseJson.GroupColumn[0], i /*Group0Data[0][thGroupBy[0]]*/, Group0Data.length);

                        if (responseJson.GroupColumn.length > 1) {
                            var Group1 = _.groupBy(Group0Data, function (innerArray) {
                                return innerArray[responseJson.GroupColumn[1]];
                            });

                            $.each(Group1, function (j, Group1Data) {
                                BindGroupHeader(Group1Data, GroupCount1, 1, responseJson.GroupColumn[1], j /*Group1Data[0][thGroupBy[1]]*/, Group1Data.length);

                                if (responseJson.GroupColumn.length > 2) {
                                    var Group2 = _.groupBy(Group1Data, function (innerArray) {
                                        return innerArray[responseJson.GroupColumn[2]];
                                    });

                                    $.each(Group2, function (k, Group2Data) {
                                        BindGroupHeader(Group2Data, GroupCount2, 2, responseJson.GroupColumn[2], k /*Group2Data[0][thGroupBy[2]]*/, Group2Data.length);

                                        BindTableData(Group2Data);

                                        GroupCount2++;
                                    });
                                }
                                else {
                                    if (GroupCount1 == 0)
                                        BindGroupZone(2);

                                    BindTableData(Group1Data);
                                }

                                GroupCount1++;
                            });
                        }
                        else {
                            if (GroupCount0 == 0)
                                BindGroupZone(1);

                            BindTableData(Group0Data);
                        }
                        GroupCount0++;
                    });

                    $(".thGroupMenu").click(function (e) {
                        //$('#GridMenu').css("display", "none");
                        HideAllMenu();

                        //thSelIndex = -1;

                        thSelGroup = $(this).parent().attr("id");
                        thSelGroupIndex = $.inArray(thSelGroup, responseJson.GroupColumn);

                        //console.log(thSelGroup);

                        GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
                        GridTop = $(this).parent().offset().top + $(this).parent().outerHeight();

                        $('#GroupMenu').css("left", GridLeft);
                        $('#GroupMenu').css("top", GridTop);

                        $('#GroupMenu ul li').removeClass("x-item-disabled");

                        if (thSelGroupIndex == 0) {
                            $('#GroupMenu ul li').find("[id='GroupMoveUp']").parent().addClass("x-item-disabled");
                        }

                        if (thSelGroupIndex == 2 || (thSelGroupIndex == 0 && responseJson.GroupColumn.length == 1) || (thSelGroupIndex == 1 && responseJson.GroupColumn.length == 2) || (thSelGroupIndex == 2 && responseJson.GroupColumn.length == 3)) {
                            $('#GroupMenu ul li').find("[id='GroupMoveDown']").parent().addClass("x-item-disabled");
                        }

                        $('#GroupMenu').css("display", "inline-block");
                        e.stopPropagation();
                        return false;
                        //console.log("asdasdasdasd");
                    });
                }
                else {
                    BindGroupZone(0);

                    BindTableData(data);
                }
            }
            else {
                BindTableData(data);
            }
        }
    }
    else if (responseJson.ReportFormatType == 2) { //For Matrix Format
        if (data == null)
            data = [];
        var result = pivot(data, responseJson.RowBreaks, responseJson.ColumnBreaks, {});

        //console.log(result[0][0]);
        //console.log(result[1][0]);
        //console.log(result[2][0]);

        //console.log(result.columnHeaders);
        //console.log(result.rowHeaders);
        //console.log(result);
        arrAggregateSum = new Array();

        for (i = 0; i < result.rowHeaders.length; i++) {
            for (j = 0; j < result.columnHeaders.length; j++) {
                var max = 0, min = 0, total = 0, sum = 0, average = 0;

                if (typeof result[i][j] != "undefined") {
                    total = result[i][j].length;

                    $.each(responseJson.Aggregate, function (k, item1) {

                        switch (item1.aggType) {
                            case "sum":
                                sum = _.reduce(result[i][j], function (memo, num) { return Number(num[item1.Column]) + memo; }, 0);
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, sum, i));

                                break;
                            case "avg":
                                sum = _.reduce(result[i][j], function (memo, num) { return Number(num[item1.Column]) + memo; }, 0);
                                average = sum / total;
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, sum, i));

                                break;
                            case "max":
                                max = _.max(result[i][j], function (num) { return Number(num[item1.Column]); })[item1.Column];
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, max, i));

                                break;
                            case "min":
                                min = _.min(result[i][j], function (num) { return Number(num[item1.Column]); })[item1.Column];
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, min, i));

                                break;
                        }
                    });

                    arrAggregateSum.push(new AggregateSum("Record Count", "RC", i, j, total, i));
                }
                else {
                    total = 0;

                    $.each(responseJson.Aggregate, function (k, item1) {

                        switch (item1.aggType) {
                            case "sum":
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, 0, i));

                                break;
                            case "avg":
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, 0, i));

                                break;
                            case "max":
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, 0, i));

                                break;
                            case "min":
                                arrAggregateSum.push(new AggregateSum(item1.Column, item1.aggType, i, j, 0, i));

                                break;
                        }

                    });

                    arrAggregateSum.push(new AggregateSum("Record Count", "RC", i, j, 0, i));
                }
            }
        }

        //console.log(arrAggregateSum);


        /****Start Column Creation****/

        var tr = $("<tr></tr>");

        var a = $("<th></th>").html("");
        tr.append(a);

        if (responseJson.ColumnBreaks.length > 0) {
            var ColumnName = $("<th></th>").append(findInColumnList('numFormFieldId', responseJson.ColumnBreaks[0]).vcFieldName);
            ColumnName.append('<div class="thMatrixColumnMenu"></div>');
            tr.append(ColumnName);

            var GroupColumns = _.groupBy(result.columnHeaders, function (innerArray) {
                return innerArray[0];
            });

            $.each(GroupColumns, function (i, item) {
                var ColumnHead = $("<th colspan='" + item.length + "'></th>").html(item[0][0]);
                tr.append(ColumnHead);
            });

            var a = $("<th rowspan='" + responseJson.ColumnBreaks.length + "' class='money' style='font-weight:bold;'></th>").addClass("grandTotal").html("Grand Total");
            tr.append(a);

            $('#MatrixTable').find('thead').append(tr);
        }
        else {
            var ColumnName = $("<th></th>").attr("class", "group-dzone-hd").html("Drop a field here to <br/>create a column grouping.");
            tr.append(ColumnName);

            var a = $("<th class='money' style='font-weight:bold;'></th>").addClass("grandTotal").html("Grand Total");
            tr.append(a);

            $('#MatrixTable').find('thead').append(tr);
        }

        /****END Column Creation****/

        /****Start Row Creation****/

        var tr = $("<tr></tr>");

        if (responseJson.RowBreaks.length == 0) {
            var ColumnName = $("<th></th>").attr("class", "group-dzone-hd").html("Drop a field here to <br/>create a row grouping.");
            tr.append(ColumnName);
        }
        else {
            var ColumnName = $("<th></th>").html(findInColumnList('numFormFieldId', responseJson.RowBreaks[0]).vcFieldName);
            ColumnName.append('<div class="thMatrixRowMenu"></div>');
            tr.append(ColumnName);
        }

        var ColumnName = $("<th></th>").attr("class", "group-dzone-Summary").html("Drop summarizable fields into the matrix.");
        ColumnName.attr('colspan', ($("#MatrixTable thead tr:nth-child(1) th").length - 1));
        tr.append(ColumnName);

        $("#MatrixTable tbody").append(tr);

        //iGroups = 0;

        if (responseJson.RowBreaks.length > 0) {
            $.each(result.rowHeaders, function (i, item) {
                var tr = $("<tr></tr>");

                var RowHead = $("<td></td>").html(item[0]);
                tr.append(RowHead);

                var tdAggregate = funAggregateColumnTitle
                tr.append(tdAggregate);

                arrRows = findAggregateSum(arrAggregateSum, 'RowNo', i)

                if (responseJson.ColumnBreaks.length > 0) {
                    for (j = 0; j < result.columnHeaders.length ; j++) {
                        var max = 0, min = 0, total = 0, sum = 0, average = 0;
                        var td = $("<td></td>").addClass("money");

                        arrColumns = findAggregateSum(arrRows, 'ColumnNo', j)

                        var td = funAggregateColumn(arrColumns)
                        tr.append(td);
                    }
                }

                var td = $("<td></td>");

                var td = funAggregateColumn(arrRows, "grandTotal")
                tr.append(td);

                $("#MatrixTable tbody").append(tr);


                /****Start Sub Total Creation****/

                //var tr = $("<tr></tr>").addClass("subTotalRow");
                //tr.append("<td colspan='" + responseJson.RowBreaks.length + "' class='money' style='font-weight:bold;'>Sub Total</td>")
                //var tdAggregate = funAggregateColumnTitle
                //tr.append(tdAggregate);

                //arrSubTotalSummary = findAggregateSum(arrAggregateSum, 'iGroups', iGroups)

                //for (j = 0; j < $("#MatrixTable thead tr:nth-child(1) th").length - 3; j++) {
                //    arrSummary = findAggregateSum(arrSubTotalSummary, 'ColumnNo', j)

                //    var td = funAggregateColumn(arrSummary)
                //    tr.append(td);
                //}

                //var td = funAggregateColumn(arrSubTotalSummary, "grandTotal")
                //tr.append(td);

                //$("#MatrixTable tbody").append(tr);

                /****END Sub Total Creation****/

                //iGroups++;
            });
        }

        /****END Row Creation****/


        /****Start Grand Total Creation****/

        var tr = $("<tr></tr>").addClass("grandTotalRow");
        tr.append("<td colspan='" + responseJson.RowBreaks.length + "' class='money' style='font-weight:bold;'>Grand Total</td>")
        var tdAggregate = funAggregateColumnTitle
        tr.append(tdAggregate);

        for (j = 0; j < $("#MatrixTable thead tr:nth-child(1) th").length - 3 ; j++) {
            arrSummary = findAggregateSum(arrAggregateSum, 'ColumnNo', j)

            var td = funAggregateColumn(arrSummary, "grandTotal")
            tr.append(td);
        }

        var td = funAggregateColumn(arrAggregateSum, "grandTotal")
        tr.append(td);

        $("#MatrixTable tbody").append(tr);

        /****END Grand Total Creation****/

        $(".thMatrixColumnMenu").click(function (e) {
            HideAllMenu();

            thSelIndex = 0;

            GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
            GridTop = $(this).parent().offset().top + $(this).parent().outerHeight();

            $('#MatrixBreakMenu').css("left", GridLeft);
            $('#MatrixBreakMenu').css("top", GridTop);

            $('#MatrixBreakMenu').css("display", "inline-block");
            e.stopPropagation();
            return false;

        });

        $(".thMatrixRowMenu").click(function (e) {
            HideAllMenu();

            thSelIndex = 1;

            GridLeft = $(this).offset().left;//- $(".thhover").offset().left;
            GridTop = $(this).parent().offset().top + $(this).parent().outerHeight();

            $('#MatrixBreakMenu').css("left", GridLeft);
            $('#MatrixBreakMenu').css("top", GridTop);

            $('#MatrixBreakMenu').css("display", "inline-block");
            e.stopPropagation();
            return false;

        });
    }



    $('#JCLRgrips').html('');
    $("#JCLRgrips").css("width", $("#GridTable").outerWidth());
    $("#JCLRgrips").css("position", "relative");

    $("#GridTable tr th").each(function () {
        GridHeight = $("#GridTable").outerHeight();

        //console.log($(this).offset().left);
        //console.log($("#GridTable").offset().left);
        //console.log($(this).outerWidth());

        GridLeft = $(this).offset().left - $("#GridTable").offset().left;
        GridRight = $(this).offset().left - $("#GridTable").offset().left + $(this).outerWidth() / 2;

        var thLeft = '<div class="JCLLeft" style="height:' + GridHeight + 'px;left:' + GridLeft + 'px;width:' + $(this).outerWidth() / 2 + 'px;"><div class="JCLRgripLeft" style="height:' + GridHeight + 'px;"></div></div>';
        var thRight = '<div class="JCLRight" style="height:' + GridHeight + 'px;left:' + GridRight + 'px;width:' + $(this).outerWidth() / 2 + 'px;"><div class="JCLRgripRight" style="height:' + GridHeight + 'px;"></div></div>';

        //var thText = '<div class="JCLRgripLeft" style="height:' + GridHeight + 'px;left:' + GridLeft + 'px"></div><div class="JCLRgripRight" style="height:' + GridHeight + 'px;left:' + GridRight + 'px"></div>';

        $('#JCLRgrips').append(thLeft);
        $('#JCLRgrips').append(thRight);
    });

    $('#JCLRgripsGrp').html('');
    //$("#JCLRgripsGrp").css("height", $("#GridTable").outerHeight());
    $("#JCLRgripsGrp").css("position", "relative");

    $(".group-hd td").each(function (index) {
        GridWidth = $("#GridTable").outerWidth();

        //console.log($(this).offset().top);
        //console.log($("#GridTable").offset().top);
        //console.log($("#GridTable").outerWidth());

        //GridLeft = $(this).offset().top - $("#GridTable").offset().top - 5;
        //GridRight = $(this).offset().top - $("#GridTable").offset().top + $(this).outerHeight() / 2;

        //var thLeft = '<div class="JCLGrpTop" style="width:' + GridWidth + 'px;top:' + GridLeft + 'px;height:' + (($(this).outerHeight() / 2) + 5) + 'px;" id="' + $.inArray($(this).attr("id"), thGroupBy) + '"><div class="JCLRgripGrpTop" style="width:' + GridWidth + 'px;"></div></div>';
        //var thRight = '<div class="JCLGrpBottom" style="width:' + GridWidth + 'px;top:' + GridRight + 'px;height:' + (($(this).outerHeight() / 2) + 5) + 'px;" id="' + $.inArray($(this).attr("id"), thGroupBy) + '"><div class="JCLRgripGrpBottom" style="width:' + GridWidth + 'px;"></div></div>';

        GridLeft = $(this).offset().top - $("#GridTable").offset().top;
        GridRight = $(this).offset().top - $("#GridTable").offset().top + $(this).outerHeight() / 2;

        var thLeft = '<div class="JCLGrpTop" style="width:' + GridWidth + 'px;top:' + GridLeft + 'px;height:' + (($(this).outerHeight() / 2)) + 'px;" id="' + $.inArray($(this).attr("id"), responseJson.GroupColumn) + '"><div class="JCLRgripGrpTop" style="width:' + GridWidth + 'px;"></div></div>';
        var thRight = '<div class="JCLGrpBottom" style="width:' + GridWidth + 'px;top:' + GridRight + 'px;height:' + (($(this).outerHeight() / 2)) + 'px;" id="' + $.inArray($(this).attr("id"), responseJson.GroupColumn) + '"><div class="JCLRgripGrpBottom" style="width:' + GridWidth + 'px;"></div></div>';

        //var thText = '<div class="JCLRgripLeft" style="height:' + GridHeight + 'px;left:' + GridLeft + 'px"></div><div class="JCLRgripRight" style="height:' + GridHeight + 'px;left:' + GridRight + 'px"></div>';

        $('#JCLRgripsGrp').append(thLeft);
        $('#JCLRgripsGrp').append(thRight);
    });

    //$('#GridTable tr').hover(function () {
    //    console.log('sadasd');
    //    $(this).addClass('thhover');
    //}, function () {
    //    $(this).removeClass('thhover');
    //});

    var drpOptionsGroup = function (event, ui) {
        fieldID = ui.helper.attr("id");

        //console.log(ui.helper);

        if (ui.helper.hasClass('MoveField') || ui.helper.hasClass('MoveHeader')) {
            thIndex = Math.floor($(this).index() / 2);

            if ($.inArray(fieldID, responseJson.GroupColumn) > -1)
                return false;

            if ($(this).hasClass('JCLGrpTop')) {
                responseJson.GroupColumn.splice(thIndex, 0, fieldID)
            }
            else {
                responseJson.GroupColumn.splice(thIndex + 1, 0, fieldID)
            }

            thIndex1 = $.inArray(fieldID, responseJson.ColumnList);

            if (thIndex1 > -1)
                responseJson.ColumnList.splice(thIndex1, 1);
        }
        else if (ui.helper.hasClass('MoveGroup')) {
            thIndex = $(this).attr("id");

            thIndex1 = $.inArray(fieldID, responseJson.GroupColumn);

            if (thIndex1 == thIndex) {
                $(this).removeClass("placeholderhover1");
                return false;
            }

            if ($(this).hasClass('JCLGrpTop')) {
                responseJson.GroupColumn.splice(thIndex, 0, responseJson.GroupColumn.splice(thIndex1, 1)[0]);
            }
            else {
                responseJson.GroupColumn.splice(thIndex + 1, 0, responseJson.GroupColumn.splice(thIndex1, 1)[0]);
            }
        }

        isGroupChange = true;
        LoadReportControls(true);
    };

    $(".JCLGrpTop,.JCLGrpBottom").droppable({
        //scope: "thGroup",
        greedy: true,
        //activeClass: "ui-state-default",
        //hoverClass: "placeholderhover1",
        //hoverClass: "ui-state-hover",
        //accept: "#browser ul li",
        accept: function (dropElem) {
            //dropElem was the dropped element, return true or false to accept/refuse it

            if ($(dropElem).parent().hasClass('group-hd')) {
                fieldID = $(dropElem).attr("id");

                if (responseJson.GroupColumn.length == 1)
                    return false;
            }
            else if ($(dropElem).parent().hasClass('FieldList')) {
                fieldID = $(dropElem).find('a').attr("id");

                if (responseJson.GroupColumn.length == 3)
                    return false;

                if ($.inArray(fieldID, responseJson.GroupColumn) > -1) {
                    return false;
                }
            }
            else if ($(dropElem).parent().hasClass('HeaderList')) {
                fieldID = $(dropElem).attr("id");

                if (responseJson.GroupColumn.length == 3)
                    return false;

                if ($.inArray(fieldID, responseJson.GroupColumn) > -1) {
                    return false;
                }
            }

            return true;
        },
        over: function (e, ui) {
            isValidDrop = false;
            $(".JCLLeft,.JCLRight").removeClass("placeholderhover");

            $(this).addClass("placeholderhover1");
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover1");
            isValidDrop = true;
        },
        drop: drpOptionsGroup,
        //tolerance: "touch",
        tolerance: 'pointer'
    });

    var drpOptions = function (event, ui) {

        if (responseJson.ReportFormatType == 0 || responseJson.ReportFormatType == 1 || responseJson.ReportFormatType == 5) {
            fieldID = ui.helper.attr("id");

            //console.log(ui.helper);

            thIndex = Math.floor($(this).index() / 2);

            if (ui.helper.hasClass('MoveField') || ui.helper.hasClass('MoveGroup')) {

                if (isGroupChange == false) {
                    if ($.inArray(fieldID, responseJson.ColumnList) > -1)
                        return false;

                    if ($(this).hasClass('JCLLeft')) {
                        responseJson.ColumnList.splice(thIndex, 0, fieldID)
                    }
                    else {
                        responseJson.ColumnList.splice(thIndex + 1, 0, fieldID)
                    }

                    thIndex1 = $.inArray(fieldID, responseJson.GroupColumn);

                    if (thIndex1 > -1)
                        responseJson.GroupColumn.splice(thIndex1, 1);
                }
            }
            else if (ui.helper.hasClass('MoveHeader')) {
                thIndex1 = $.inArray(fieldID, responseJson.ColumnList);

                if (thIndex1 == thIndex) {
                    $(this).removeClass("placeholderhover");
                    return false;
                }

                if ($(this).hasClass('JCLLeft')) {
                    responseJson.ColumnList.splice(thIndex, 0, responseJson.ColumnList.splice(thIndex1, 1)[0]);
                }
                else {
                    responseJson.ColumnList.splice(thIndex + 1, 0, responseJson.ColumnList.splice(thIndex1, 1)[0]);
                }
            }

            isGroupChange = false;

            LoadReportControls(true);
        }
    };

    $(".JCLLeft,.JCLRight").droppable({
        //scope: "thColumn",
        greedy: true,
        //activeClass: "ui-state-default",
        //hoverClass: "placeholderhover",
        //hoverClass: "ui-state-hover",
        //accept: "#browser ul li",
        accept: function (dropElem) {
            //dropElem was the dropped element, return true or false to accept/refuse it

            //console.log(dropElem);

            if ($(dropElem).parent().hasClass('group-hd')) {
                fieldID = $(dropElem).attr("id");
            }
            else if ($(dropElem).parent().hasClass('FieldList')) {
                fieldID = $(dropElem).find('a').attr("id");

                if ($.inArray(fieldID, responseJson.ColumnList) > -1 || $.inArray(fieldID, responseJson.GroupColumn) > -1) {
                    return false;
                }
            }
            else if ($(dropElem).parent().hasClass('HeaderList')) {
                fieldID = $(dropElem).attr("id");
            }

            return isValidDrop;
        },
        over: function (e, ui) {

            //$(".group-hd td").each(function () {
            //    var p = $(this);
            //    var position = p.position();
            //    //isValidDrop = true;

            //    if (e.clientX >= position.left && e.clientX <= position.left + $(this).width() &&
            //    e.clientY >= position.top - 6 && e.clientY <= position.top + 6 + $(this).height()) {
            //        isValidDrop = false;
            //        //return false;
            //        //$("#browser ul li").draggable("option", "scope", "thGroup");

            //        //console.log($("#browser ul li").draggable("option", "scope"))
            //    }
            //});

            if (isValidDrop)
                $(this).addClass("placeholderhover");
            else
                $(".JCLLeft,.JCLRight").removeClass("placeholderhover");

            //return true;
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover");
        },
        drop: drpOptions,
        //tolerance: "touch",
        tolerance: 'pointer'
    });

    $("#ReportResult").droppable({
        greedy: true,
        accept: "#browser ul li",
        drop: drpOptions,
        //tolerance: "touch",
        tolerance: 'pointer'
    });

    var browserDrpOptions = function (event, ui) {
        fieldID = ui.helper.attr("id");

        if (ui.helper.hasClass('MoveHeader')) {
            thIndex = $.inArray(fieldID, responseJson.ColumnList);
            responseJson.ColumnList.splice(thIndex, 1);
        }
        else if (ui.helper.hasClass('MoveField')) {
            thIndex = $.inArray(fieldID, responseJson.GroupColumn);
            responseJson.GroupColumn.splice(thIndex, 1);
        }

        LoadReportControls(true);
    };

    $("#browser").droppable({
        greedy: true,
        accept: "#GridTable thead tr th,#GridTable tbody .group-hd td",
        drop: browserDrpOptions,
        //tolerance: "touch",
        tolerance: 'pointer'
    });

    var FiltersSectionDrpOptions = function (event, ui) {
        responseJson.filters.push({ 'Column': ui.helper.attr("id"), 'ColValue': '', 'OperatorType': 'eq', 'ColText': '' });
        $(this).removeClass("placeholderhover2");
        CreateFilterCondition(true);
    };

    $("#fieldFiltersSection").droppable({
        greedy: true,
        //accept: "#browser ul li,#GridTable thead tr th,#GridTable tbody .group-hd td",
        drop: FiltersSectionDrpOptions,
        accept: function (dropElem) {
            if ($("#fieldFiltersSection").find('.editMode').length != 0 || $("#FilterLogicSection").find('.editMode').length != 0) {
                return false;
            }

            if ($(dropElem).parent().hasClass('group-hd')) {
                fieldID = $(dropElem).attr("id");
            }
            else if ($(dropElem).parent().hasClass('FieldList')) {
                fieldID = $(dropElem).find('a').attr("id");
            }
            else if ($(dropElem).parent().hasClass('HeaderList')) {
                fieldID = $(dropElem).attr("id");
            }

            if (findInColumnList("numFormFieldId", fieldID).bitAllowFiltering == 'False')
                return false;

            return true;
        },
        //tolerance: "touch",
        tolerance: 'pointer',
        over: function (e, ui) {
            $(this).addClass("placeholderhover2");
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover2");
        },
    });

    var drpOptionsGroupZone = function (event, ui) {
        fieldID = ui.helper.attr("id");

        if (responseJson.GroupColumn.length == 3)
            return false;

        if ($.inArray(fieldID, responseJson.GroupColumn) > -1)
            return false;

        if (findInColumnList("numFormFieldId", fieldID).bitAllowGrouping == 'False')
            return false;

        responseJson.GroupColumn.splice(responseJson.GroupColumn.length, 0, fieldID)

        thIndex1 = $.inArray(fieldID, responseJson.ColumnList);

        if (thIndex1 > -1)
            responseJson.ColumnList.splice(thIndex1, 1);

        LoadReportControls(true);
    };

    $("#GridTable tbody tr .group-dzone-hd").droppable({
        accept: "#browser ul li,#GridTable thead tr th",
        drop: drpOptionsGroupZone,
        //tolerance: "touch",
        tolerance: 'pointer',
        over: function (e, ui) {
            isValidDrop = false;
            $(".JCLLeft,.JCLRight").removeClass("placeholderhover");

            $(this).addClass("placeholderhover1");
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover1");
            isValidDrop = true;
        },
    });

    var drpOptionsMatrixTableColumns = function (event, ui) {
        if (responseJson.ColumnBreaks.length == 1)
            return false;

        fieldID = ui.helper.attr("id");

        if (findInColumnList("numFormFieldId", fieldID).bitAllowGrouping == 'False')
            return false;

        if ($.inArray(fieldID, responseJson.RowBreaks) > -1)
            return false;

        responseJson.ColumnBreaks.push(fieldID)

        LoadReportControls(true);
    };

    $("#MatrixTable thead tr .group-dzone-hd").droppable({
        accept: "#browser ul li",
        drop: drpOptionsMatrixTableColumns,
        //tolerance: "touch",
        tolerance: 'pointer',
        over: function (e, ui) {
            $(this).addClass("placeholderhover1");
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover1");
        },
    });

    var drpOptionsMatrixTableRows = function (event, ui) {
        if (responseJson.RowBreaks.length == 1)
            return false;

        fieldID = ui.helper.attr("id");

        if (findInColumnList("numFormFieldId", fieldID).bitAllowGrouping == 'False')
            return false;

        if ($.inArray(fieldID, responseJson.ColumnBreaks) > -1)
            return false;

        responseJson.RowBreaks.push(fieldID)

        LoadReportControls(true);
    };

    $("#MatrixTable tbody tr .group-dzone-hd").droppable({
        accept: "#browser ul li",
        drop: drpOptionsMatrixTableRows,
        //tolerance: "touch",
        tolerance: 'pointer',
        over: function (e, ui) {
            $(this).addClass("placeholderhover1");
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover1");
        },
    });

    var drpOptionsMatrixTableSummary = function (event, ui) {

        fieldID = findInColumnList("numFormFieldId", ui.helper.attr("id"));

        if (fieldID.vcFieldDataType != 'N' && fieldID.vcFieldDataType != 'M')
            return false;

        if (fieldID.bitAllowAggregate == 'False')
            return false;

        arrFindSummaryField = findAggregateSum(responseJson.Aggregate, "Column", fieldID.numFormFieldId)

        if (arrFindSummaryField.length > 0) {
            if (findAggregateSum(arrFindSummaryField, "aggType", "sum").length > 0)
                $('#chkSum').prop('checked', true);

            if (findAggregateSum(arrFindSummaryField, "aggType", "avg").length > 0)
                $('#chkAverage').prop('checked', true);

            if (findAggregateSum(arrFindSummaryField, "aggType", "max").length > 0)
                $('#chkMax').prop('checked', true);

            if (findAggregateSum(arrFindSummaryField, "aggType", "min").length > 0)
                $('#chkMin').prop('checked', true);
        }
        else if (responseJson.Aggregate.length == 8)
            return false;

        //                responseJson.Aggregate.push({ 'Column': fieldID.numFormFieldId, 'aggType': 'sum'});
        //                responseJson.Aggregate.push({ 'Column': fieldID.numFormFieldId, 'aggType': 'avg'});  
        //                responseJson.Aggregate.push({ 'Column': fieldID.numFormFieldId, 'aggType': 'max'});
        //                responseJson.Aggregate.push({ 'Column': fieldID.numFormFieldId, 'aggType': 'min'});

        $("#spnMatrixSummaryFieldLabel").text(fieldID.vcFieldName);
        $("#hdnMatrixSummaryFieldID").val(fieldID.numFormFieldId);

        $("#dialog-form").dialog("open");

    };

    $("#MatrixTable tbody tr .group-dzone-Summary").droppable({
        accept: "#browser ul li",
        drop: drpOptionsMatrixTableSummary,
        //tolerance: "touch",
        tolerance: 'pointer',
        over: function (e, ui) {
            $(this).addClass("placeholderhover1");
        },
        out: function (e, ui) {
            $(this).removeClass("placeholderhover1");
        },
    });

    //$("a", "#GridTable tr th").unbind("click");

    $("#GridTable thead tr th").draggable({
        connectToSortable: ".JCLLeft,.JCLRight,#browser,.JCLGrpTop,.JCLGrpBottom,#GridTable tbody tr .group-dzone-hd,#fieldFiltersSection",
        helper: function () {
            var text = $(this).text();//this.children[0].innerText;
            var result = "<div id='" + this.id + "' style='white-space:nowrap;' class='MoveHeader'>" + text + "</div>";
            return result;
        },
        revert: "invalid",
        //cursor: "move",
        //distance: "1",
        cursorAt: {
            //bottom: 10,
            //right: 10
            top: -10,
            left: -10
        },
        start: function (e, ui) {
            //$(ui.helper).addClass("ui-draggable-helper");
        },
        drag: function (e, ui) {
            var p = $("#browser");
            var position = p.position();

            var p1 = $("#GridTable");
            var position1 = p1.position();

            var p2 = $("#fieldFiltersSection");
            var position2 = p2.position();

            //console.log( "left: " + position.left  + ", top: " + position.top );
            //console.log( "height: " + $("#GridTable").height()  + ", width: " + $("#GridTable").width() );
            if (e.clientX >= position.left && e.clientX <= position.left + $("#browser").width() &&
            e.clientY >= position.top && e.clientY <= position.top + $("#browser").height()) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else if (e.clientX >= position1.left && e.clientX <= position1.left + $("#GridTable").width() &&
        e.clientY >= position1.top && e.clientY <= position1.top + $("#GridTable").height()) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else if (e.clientX >= position2.left && e.clientX <= position2.left + $("#fieldFiltersSection").width() &&
        e.clientY >= position2.top && e.clientY <= position2.top + $("#fieldFiltersSection").height()
            && ($("#fieldFiltersSection").find('.editMode').length == 0) && $("#FilterLogicSection").find('.editMode').length == 0) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else {
                $(ui.helper).removeClass("ui-draggable-helperAccept");
                $(ui.helper).addClass("ui-draggable-helperNotAccept");
            }
            // console.log("You clicked x:" + e.clientX + " y:" + e.clientY);
        },
        //snapMode: 'inner',
        //snapTolerance: 15
        // stop: function(e, ui)
        // {
        //  console.log("You clicked x:" + e.clientX + " y:" + e.clientY);
        // },
    });

    $("#GridTable tbody .group-hd td").draggable({
        connectToSortable: ".JCLLeft,.JCLRight,#browser,.JCLGrpTop,.JCLGrpBottom,#fieldFiltersSection",
        helper: function () {
            var text = $(this).text();//this.children[0].innerText;
            var result = "<div id='" + this.id + "' style='white-space:nowrap;' class='MoveGroup'>" + text + "</div>";
            return result;
        },
        revert: "invalid",
        //cursor: "move",
        //distance: "1",
        cursorAt: {
            //bottom: 10,
            //right: 10
            top: -10,
            left: -10
        },
        start: function (e, ui) {
            //$(ui.helper).addClass("ui-draggable-helper");
        },
        drag: function (e, ui) {
            var p = $("#browser");
            var position = p.position();

            var p1 = $("#GridTable");
            var position1 = p1.position();

            var p2 = $("#fieldFiltersSection");
            var position2 = p2.position();

            //console.log( "left: " + position.left  + ", top: " + position.top );
            //console.log( "height: " + $("#GridTable").height()  + ", width: " + $("#GridTable").width() );
            if (e.clientX >= position.left && e.clientX <= position.left + $("#browser").width() &&
            e.clientY >= position.top && e.clientY <= position.top + $("#browser").height()) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else if (e.clientX >= position1.left && e.clientX <= position1.left + $("#GridTable").width() &&
        e.clientY >= position1.top && e.clientY <= position1.top + $("#GridTable").height()) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else if (e.clientX >= position2.left && e.clientX <= position2.left + $("#fieldFiltersSection").width() &&
    e.clientY >= position2.top && e.clientY <= position2.top + $("#fieldFiltersSection").height()
        && ($("#fieldFiltersSection").find('.editMode').length == 0) && $("#FilterLogicSection").find('.editMode').length == 0) {
                $(ui.helper).removeClass("ui-draggable-helperNotAccept");
                $(ui.helper).addClass("ui-draggable-helperAccept");
            }
            else {
                $(ui.helper).removeClass("ui-draggable-helperAccept");
                $(ui.helper).addClass("ui-draggable-helperNotAccept");
            }
            // console.log("You clicked x:" + e.clientX + " y:" + e.clientY);
        },
        //snapMode: 'inner',
        //snapTolerance: 15
        // stop: function(e, ui)
        // {
        //  console.log("You clicked x:" + e.clientX + " y:" + e.clientY);
        // },
    });
}
