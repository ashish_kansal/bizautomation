﻿tokenizer = { LPAREN_ID: 0, RPAREN_ID: 1, AND_ID: 2, OR_ID: 3, NOT_ID: 4, INTEGER_ID: 5,

    LPAREN: function () { this.id = tokenizer.LPAREN_ID },
    RPAREN: function () { this.id = tokenizer.RPAREN_ID },
    AND: function () { this.id = tokenizer.AND_ID; this.operation = "AND" },
    OR: function () { this.id = tokenizer.OR_ID; this.operation = "OR" },
    NOT: function () { this.id = tokenizer.NOT_ID; this.operation = "NOT" },
    INTEGER: function (a) { this.id = tokenizer.INTEGER_ID; this.value = parseInt(a, 10) },

    TokenParsingException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "error_typo") },
    ParenMismatchException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "error_typo") },
    TooManyValuesException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "error_typo") },
    UnexpectedTokenException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "error_typo") },
    EmptyInputException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "requiredFieldError") },
    MissingOperandException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "error_typo") },
    EndOfTokensException: function () { this.message = getErrorMessageListLabel("ReportBooleanFilter", "error_typo") },
    Parser: function (a) { this.input = a },
    Node: function (a, b, c) { this.left = a; this.op = b; this.right = c },

    getTokens: function () {

        var a = [], b = /\d+/g, c = InputCondition.match(/\s+|\w+|\d+|\(|\)|\W+/g);

        if (!c) throw new tokenizer.MissingOperandException;

        for (var e = 0; e < c.length; e++) {
            var d = c[e].trim(), f = d.match(b);

            if ("(" === d) a.push(new tokenizer.LPAREN);
            else if (")" === d) a.push(new tokenizer.RPAREN);
            else if ("AND" === d.toUpperCase()) a.push(new tokenizer.AND);
            else if ("OR" === d.toUpperCase()) a.push(new tokenizer.OR);
            else if ("NOT" === d.toUpperCase()) a.push(new tokenizer.NOT);
            else if (f && 1 == f.length && f[0] === d) a.push(new tokenizer.INTEGER(d));
            else if (!(d.match(/\s+/i) || "" === d)) throw new tokenizer.TokenParsingException;
        }

        return a
    },

    getTree: function () {
        if ($.isBlank(InputCondition))
            throw new tokenizer.EmptyInputException;

        var a = tokenizer.getTokens(), b = []; tokenizer.index = 0;
        a = tokenizer._exp(a, b);

        if (0 < b.length)
            throw new tokenizer.TooManyValuesException;

        return a
    },

    getFilterNumbers: function () {
        for (var a = this.getTree(), b = {}; a instanceof tokenizer.Node; ) {
            null !== a.left && (b[a.left] = 0), a = a.right;
        }

        !(a instanceof tokenizer.Node) && null !== a && (b[a] = 0);

        return b
    },

    getFiltersInUseMessage: function (a) {

        for (var b = this.getFilterNumbers(), c = [], e = [], d = 0; d < a.length; d++)
            "undefined" === typeof b[a[d]] ? c.push(a[d]) : b[a[d]]++;

        for (var f in b)
            b[f] || e.push(f);
        //        var message = "";

        //        if (0 < c.length)
        //            message = getErrorMessageListLabel("ReportBooleanFilter", "error_unused_filters1").replace("{0}", c.join(","))
        //        else if (0 < e.length)
        //            message = getErrorMessageListLabel("ReportBooleanFilter", "error_bad_number").replace("{0}", e.join(","))
        //        else
        //            message = "";

        //        if (message.length > 0)
        //            throw new (message);

        return 0 < c.length ? getErrorMessageListLabel("ReportBooleanFilter", "error_unused_filters1").replace("{0}", c.join(",")) : 0 < e.length ? getErrorMessageListLabel("ReportBooleanFilter", "error_bad_number").replace("{0}", e.join(",")) : null
    },

    _exp: function (a, b) {
        if (this.index >= a.length)
            throw new tokenizer.EndOfTokensException;

        for (; this.index < a.length; ) {
            var c = a[this.index];
            this.index++;

            switch (c.id) {
                case tokenizer.INTEGER_ID: b.push(c.value); break;
                case tokenizer.LPAREN_ID: b.push("("); break;
                case tokenizer.RPAREN_ID:
                    if (2 > b.length)
                        throw new tokenizer.ParenMismatchException;

                    c = b.pop();
                    if ("(" !== b.pop())
                        throw new tokenizer.ParenMismatchException;

                    b.push(c); break;

                case tokenizer.NOT_ID:
                    b.push(new tokenizer.Node(null, c.operation, this._exp(a, b))); break;

                case tokenizer.AND_ID:
                case tokenizer.OR_ID:
                    if (0 === b.length)
                        throw new tokenizer.MissingOperandException(c.operation);

                    var e = b.pop();

                    if ("number" !== typeof e)
                        throw new tokenizer.MissingOperandException(c.operation);

                    b.push(new tokenizer.Node(e, c.operation, this._exp(a, b))); break;

                default:
                    throw new tokenizer.UnexpectedTokenException;
            }
        }

        c = b.pop();

        if ("(" === c)
            throw new tokenizer.ParenMismatchException(this.index);

        return c
    }
};