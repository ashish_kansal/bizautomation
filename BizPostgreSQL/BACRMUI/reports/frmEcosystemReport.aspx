<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmEcosystemReport.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmEcosystemReport"%>
<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Ecosystem Report</title>
		
		<SCRIPT language="JavaScript" src="../javascript/date-picker.js" type="text/javascript">
		</SCRIPT>
		<script language="javascript">
		function OpenSelTeam(repNo)
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenTerritory(repNo)
		{
			window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenAsItsType(repNo)
		{
			window.open("../Reports/frmSelectAsItsType.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type="+repNo,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function OpenFld()
		{
			window.open("../Reports/frmSelectReportFlds.aspx",'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
		function PopupCheck()
		{
		    document.Form1.btnGo1.click()
		}
		</script>
	</HEAD>
	<body>
		
		<form id="Form1" method="post" runat="server">
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
		
		
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<table width="100%">
				<tr>
					<td class="normal1" align="right">Customer or Vendor<FONT color="#ff3333"> *</FONT></td>
					<td class="normal1"><asp:textbox id="txtCompName" Runat="server" cssclass="signup" width="130"></asp:textbox>
						<asp:button id="btnGo" Runat="server" CssClass="button" Text="Go"></asp:button>
						<asp:dropdownlist id="ddlCompany" Runat="server" CssClass="signup" Width="200" AutoPostBack="True"></asp:dropdownlist>
					</td>
				</tr>
				<tr>
					<td align="right" colspan="4"><asp:button id="btnAddtoMyRtpList" Runat="server" CssClass="button" Text="Add To My Reports"></asp:button></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; Ecosystem 
									Report&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right"><asp:button id="btnSelectAsits" Runat="server" CssClass="button" Text="Select 'As Its' Type"
							Width="120"></asp:button>&nbsp;
						<asp:button id="btnSelectFields" Runat="server" CssClass="button" Text="Select Fields" Width="120"></asp:button>&nbsp;
						<asp:button id="btnExportToExcel" Runat="server" CssClass="button" Text="Export to Excel" Width="120"></asp:button>&nbsp;
						<asp:button id="btnPrint" Runat="server" CssClass="button" Text="Print" Width="50"></asp:button>&nbsp;
						<asp:button id="btnBack" Runat="server" CssClass="button" Text="Back" Width="50"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="Table9" Runat="server" Width="100%" BorderWidth="1" CellPadding="0" CellSpacing="0" CssClass="aspTable"
				Height="400" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:DataGrid ID="dgEcosystemReport" CssClass="dg" Width="100%" Runat="server" BorderColor="white"
							AutoGenerateColumns="False" AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numCompanyID1" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="numDivisionID1" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="ContactID1" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="tintCRMType1" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="numCompanyID2" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="numDivisionID2" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="ContactID2" Visible=false></asp:BoundColumn>
								<asp:BoundColumn DataField="tintCRMType2" Visible=false></asp:BoundColumn>
								<asp:buttoncolumn CommandName="Company1" DataTextField="Entity1" SortExpression="Entity1" HeaderText="<font color=white>Entity 1,Division</font>"></asp:buttoncolumn>
								<asp:BoundColumn DataField="SelectedData1" SortExpression="SelectedData1" HeaderText="<font color=white>Selected Field Data</font>"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Association">
									<ItemTemplate>
										<asp:Label ID="lblAssociation" Runat="server">To</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:buttoncolumn  CommandName="Company2" DataTextField ="Entity2" SortExpression="Entity2" HeaderText="<font color=white>Entity 2,Division</font>"></asp:buttoncolumn>
								<asp:BoundColumn DataField="SelectedData2" SortExpression="SelectedData2" HeaderText="<font color=white>Selected Field Data</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Type" SortExpression="Type" HeaderText="<font color=white>''As Its'' Type</font>"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<asp:Button ID="btnGo1" Runat="server" style="display:none"></asp:Button>
			</ContentTemplate>
			<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			<asp:PostBackTrigger ControlID="btnPrint" />
			</Triggers>
			</asp:updatepanel>
			</form>
	</body>
</HTML>
