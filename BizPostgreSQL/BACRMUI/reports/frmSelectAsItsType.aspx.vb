Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmSelectAsItsType : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblTerritories As System.Web.UI.WebControls.Table
        Protected WithEvents hdnValue As System.Web.UI.WebControls.TextBox
        Protected WithEvents lstTeamAvail As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstTeamAdd As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnRemove As System.Web.UI.WebControls.Button

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                If Not IsPostBack Then
                    Dim objPredefinedReports As New PredefinedReports
                    Dim dtAsItsType As DataTable
                    dtAsItsType = objPredefinedReports.GetAsItsType()
                    lstTeamAvail.DataSource = dtAsItsType
                    lstTeamAvail.DataTextField = "vcData"
                    lstTeamAvail.DataValueField = "numListItemID"
                    lstTeamAvail.DataBind()
                     ' = "Reports"
                    Dim objRep As New PredefinedReports
                    Dim dtReportAsItsType As DataTable
                    objRep.UserCntID = Session("UserContactID")
                    objRep.DomainID = Session("DomainID")
                    objRep.ReportType = Convert.ToInt16(GetQueryStringVal("Type"))
                    dtReportAsItsType = objRep.GetAsItsTypeForReport
                    lstTeamAdd.DataSource = dtReportAsItsType
                    lstTeamAdd.DataTextField = "vcData"
                    lstTeamAdd.DataValueField = "numListItemID"
                    lstTeamAdd.DataBind()
                End If
                btnAdd.Attributes.Add("OnClick", "return move(document.Form1.lstTeamAvail,document.Form1.lstTeamAdd)")
                btnRemove.Attributes.Add("OnClick", "return remove(document.Form1.lstTeamAdd,document.Form1.lstTeamAvail)")
                btnSave.Attributes.Add("onclick", "Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objRep As New PredefinedReports
                objRep.strTerritory = hdnValue.Text
                objRep.UserCntID = Session("UserContactID")
                objRep.DomainID = Session("DomainID")
                objRep.ReportType = Convert.ToInt16(GetQueryStringVal("Type"))
                objRep.ManageAsItsTypeForForRept()
                Response.Write("<script>opener.PopupCheck(); self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace