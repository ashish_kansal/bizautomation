<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCaseProjectCosting.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCaseProjectCosting" %>
<%@ Register TagPrefix="menu1" TagName="menu" src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <menu1:menu id="webmenu1" runat="server"></menu1:menu>
			
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>
   <table width="100%">
				<tr>
					<td class="normal1" noWrap><td  class="normal1" align="center">
				        <table >
				            <tr>
				                <td >&nbsp;&nbsp;Organization</td>
						        <td>
									<asp:textbox id="txtCompName" Runat="server" width="130" cssclass="signup"></asp:textbox>&nbsp;
								</td>
						        <td align="left" >
						            <asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
								</td>
								<td>
								    <asp:dropdownlist id="ddlCompany" Runat="server" Width="200" AutoPostBack="True" CssClass="signup"></asp:dropdownlist>
								</td>
						    </tr>
						</table>
					</td>					
					
				</tr>
			</table>
			
			<table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
			    <td   colspan="5" align="right">
			            <asp:button id="btnAddtoMyRtpList" CssClass="button" Text="Add To My Reports" Runat="server"></asp:button>&nbsp;
			            <asp:button id="btnExportToExcel" CssClass="button" Width="120" Runat="server" Text="Export to Excel"></asp:button>&nbsp;
						<asp:button id="btnPrint" CssClass="button" Width="50" Runat="server" Text="Print"></asp:button>&nbsp;
						<asp:button id="btnBack" CssClass="button" Width="50" Runat="server" Text="Back"></asp:button>&nbsp;
			      </td>
			</tr>
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; 
									Projects & Cases Costing&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1">
					    Include History <asp:CheckBox runat="server"  AutoPostBack="true" Checked="true" id="radHistory" />
					</td>
					<td class="normal1">&nbsp;<asp:RadioButton ID="radCase" runat="server" GroupName="filter" Text="Cases" AutoPostBack="true" />
						<asp:dropdownlist id="ddlCase" CssClass="normal1" Runat="server" Width="200" AutoPostBack="True"></asp:dropdownlist>
					</td>
					<td class="normal1">&nbsp;<asp:RadioButton ID="radPro" runat="server" GroupName="filter" Text="Project" AutoPostBack="true" />
						<asp:dropdownlist id="ddlPro" CssClass="normal1" Runat="server" Width="200" AutoPostBack="True"></asp:dropdownlist>
					</td>
					<td align="right" class="normal7">
					Total Profit or Loss : <asp:Label runat="server" ID="lblProfit"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
						
					</td>
				</tr>
			</table>
			<asp:table id="Table9" Height="400" Width="100%" Runat="server" BorderWidth="1" CellPadding="0" CssClass="aspTable"
				CellSpacing="0" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
					    <asp:datagrid id="dgCosting" AllowSorting="true" Runat="server" Width="100%" AutoGenerateColumns="False"
							 CssClass="dg">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font color=white>Name, Created On, By</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Type" SortExpression="Type"  HeaderText="<font color=white>Type</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="TimeEntered" SortExpression="TimeEntered"  HeaderText="<font color=white>Time Entered On, By</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="TimeType" SortExpression="TimeType"  HeaderText="<font color=white>Time Type</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="RateHr" SortExpression="RateHr"  HeaderText="<font color=white>Rate/Hr</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Hrs" SortExpression="Hrs" HeaderText="<font color=white>Hrs Entered</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="AmountBilled" SortExpression="AmountBilled"  HeaderText="<font color=white>AmountBilled </font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="labourrate" SortExpression="labourrate"  HeaderText="<font color=white>Labour Cost</font>"></asp:BoundColumn>
								</Columns>
			</asp:datagrid>
				    </asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			<asp:PostBackTrigger ControlID="btnPrint" />
			</Triggers>
			</asp:updatepanel>
    </form>
</body>
</html>
