Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.Reports
    Public Class frmBestAccountDealClosed : Inherits BACRMPage

        Dim SortField As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                     ' = "Reports"
                    BindDatagrid()
                End If
                If txtSortCharSales.Text <> "" Then
                    BindDatagrid()
                    txtSortCharSales.Text = ""
                End If
                litMessage.Text = ""
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities

                lblTotalRecord.Text = GetQueryStringVal( "NoRecords")
                With objOpportunity
                    .UserCntID = IIf(GetQueryStringVal( "UserCntID") <> "", GetQueryStringVal( "UserCntID"), Session("UserContactID"))
                    If GetQueryStringVal( "frm") = "DealsClosed" Then
                        .OppStatus = 1
                    ElseIf GetQueryStringVal( "frm") = "OppPipeline" Then
                        .OppStatus = 0
                    End If

                    .DomainID = Session("DomainID")
                    .DivisionID = GetQueryStringVal( "DivId")
                    .startDate = GetQueryStringVal( "stDate")
                    .endDate = GetQueryStringVal( "enDate")
                End With
                dtOpportunity = objOpportunity.GetOpportuntityBestAccountDealClosedReport

                Dim dv As DataView = New DataView(dtOpportunity)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgOpportuntity.DataSource = dv
                dgOpportuntity.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgOpportuntity_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOpportuntity.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace