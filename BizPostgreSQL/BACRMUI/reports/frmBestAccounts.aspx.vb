Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmBestAccounts : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
        Dim dtBestAccounts As DataTable
        Dim objPredefinedReports As New PredefinedReports
        Dim SortField As String
        Dim FromDate As String
        Dim ToDate As String
#End Region

       
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then
                    ' = "Reports"

                    GetUserRightsForPage(8, 35)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    LoadDropdowns()
                    ddlCriteria.SelectedIndex = 1
                    ddlYear.Items.FindByText(Year(Now())).Selected = True
                    PopulateData()
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(17)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(17)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Private Sub LoadDropdowns()
            Try
                Dim Count As Integer
                ddlMonth.Items.Add("-- Select One --")
                For Count = 1 To 12
                    ddlMonth.Items.Add(MonthName(Count))
                Next
                ddlYear.Items.Add("-- Select One --")
                ddlYear.Items.Add(Year(Now()) - 2)
                ddlYear.Items.Add(Year(Now()) - 1)
                ddlYear.Items.Add(Year(Now()))
                ddlYear.Items.Add(Year(Now()) + 1)
                ddlYear.Items.Add(Year(Now()) + 2)
                ddlYear.Items.Add(Year(Now()) + 3)
                ddlYear.Items.Add(Year(Now()) + 4)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCriteria_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCriteria.SelectedIndexChanged
            Try
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub DisplayRecords(ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal intFromMonth As Integer, ByVal intToMonth As Integer)
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = dtFromDate
                objPredefinedReports.ToDate = dtToDate
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.TerritoryID = Session("TerritoryID")
                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select

                objPredefinedReports.ReportType = 17
                dtBestAccounts = objPredefinedReports.GetBestAccounts
                Session("dtBestAccounts") = dtBestAccounts
                Dim dv As DataView = New DataView(dtBestAccounts)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgBestAccounts.DataSource = dv
                dgBestAccounts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
            Try
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
            Try
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlQuarter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuarter.SelectedIndexChanged
            Try
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                PopulateData()
                Response.Redirect("../reports/frmBestAccountsPrint.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&fdt=" & ViewState("StartDate") & "&tdt=" & ViewState("EndDate") & "&ReportType=" & rdlReportType.SelectedIndex, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgBestAccounts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgBestAccounts.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub PopulateData()
            Try
                Dim objDashboard As New DashBoard
                Select Case ddlCriteria.SelectedIndex
                    Case 0
                        lblYear.Visible = False
                        ddlYear.Visible = False
                        lblQuarter.Visible = False
                        ddlQuarter.Visible = False
                        lblMonth.Visible = False
                        ddlMonth.Visible = False
                    Case 1
                        lblYear.Visible = True
                        ddlYear.Visible = True
                        lblQuarter.Visible = False
                        ddlQuarter.Visible = False
                        lblMonth.Visible = False
                        ddlMonth.Visible = False
                        Dim intCrtMth As Int16 = Month(Now())
                        'If ddlYear.SelectedIndex > 0 Then
                        '    If ddlYear.SelectedItem.Value = Year(Now()) Then
                        '        FromDate = ddlYear.SelectedItem.Text & "0101"
                        '        ToDate = Now()
                        '        DisplayRecords(ddlYear.SelectedItem.Text & "0101", Format(Now(), "yyyyMMdd"), 1, intCrtMth)

                        '    Else
                        '        FromDate = ddlYear.SelectedItem.Text & "0101"
                        '        ToDate = ddlYear.SelectedItem.Text & "1231"
                        '        DisplayRecords(ddlYear.SelectedItem.Text & "0101", ddlYear.SelectedItem.Text & "1231", 1, 12)
                        '    End If
                        'End If
                        If ddlYear.SelectedIndex > 0 Then
                            If ddlYear.SelectedItem.Value = Year(Now()) Then
                                '' DisplayRecords("01/01/" & ddlYear.SelectedItem.Text, Now(), 1, intCrtMth)
                                ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, 1, 1)
                                ViewState("EndDate") = Now()
                                DisplayRecords(New Date(ddlYear.SelectedItem.Text, 1, 1), Now(), 1, intCrtMth)
                            Else
                                '' DisplayRecords("01/01/" & ddlYear.SelectedItem.Text, "12/31/" & ddlYear.SelectedItem.Text, 1, 12)
                                ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, 1, 1)
                                ViewState("EndDate") = New Date(ddlYear.SelectedItem.Text, 1, 1).AddMonths(12).AddDays(-1)
                                DisplayRecords(New Date(ddlYear.SelectedItem.Text, 1, 1), New Date(ddlYear.SelectedItem.Text, 1, 1).AddMonths(12).AddDays(-1), 1, 12)
                            End If
                        End If
                    Case 2
                        lblYear.Visible = True
                        ddlYear.Visible = True
                        lblQuarter.Visible = True
                        ddlQuarter.Visible = True
                        lblMonth.Visible = False
                        ddlMonth.Visible = False
                        Dim intCrtMth As Int16 = Month(Now())
                        If ddlQuarter.SelectedIndex > 0 And ddlYear.SelectedIndex > 0 Then
                            Select Case ddlQuarter.SelectedIndex
                                Case 1
                                    'FromDate = ddlYear.SelectedItem.Text & "0101"
                                    'ToDate = ddlYear.SelectedItem.Text & "0331"
                                    'DisplayRecords(ddlYear.SelectedItem.Text & "0101", ddlYear.SelectedItem.Text & "0331", 1, 3)
                                    ''DisplayRecords("01/01/" & ddlYear.SelectedItem.Text, Now(), 1, 3)
                                    ''DisplayRecords(New Date(ddlYear.SelectedItem.Text,1, 1), Now(), 1, 3) -- Commented by Siva
                                    ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, 1, 1)
                                    ViewState("EndDate") = New Date(ddlYear.SelectedItem.Text, 1, 1).AddMonths(3).AddDays(-1)
                                    DisplayRecords(New Date(ddlYear.SelectedItem.Text, 1, 1), New Date(ddlYear.SelectedItem.Text, 1, 1).AddMonths(3).AddDays(-1), 1, 3)
                                Case 2
                                    'FromDate = ddlYear.SelectedItem.Text & "0401"
                                    'ToDate = ddlYear.SelectedItem.Text & "0630"
                                    'DisplayRecords(ddlYear.SelectedItem.Text & "0401", ddlYear.SelectedItem.Text & "0630", 4, 6)
                                    ''DisplayRecords("04/01/" & ddlYear.SelectedItem.Text, Now(), 4, 6)
                                    '' DisplayRecords(New Date(ddlYear.SelectedItem.Text, 4, 1), Now(), 4, 6) -- Commented by Siva
                                    ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, 4, 1)
                                    ViewState("EndDate") = New Date(ddlYear.SelectedItem.Text, 4, 1).AddMonths(3).AddDays(-1)
                                    DisplayRecords(New Date(ddlYear.SelectedItem.Text, 4, 1), New Date(ddlYear.SelectedItem.Text, 4, 1).AddMonths(3).AddDays(-1), 4, 6)
                                Case 3
                                    'FromDate = ddlYear.SelectedItem.Text & "0701"
                                    'ToDate = ddlYear.SelectedItem.Text & "0930"
                                    'DisplayRecords(ddlYear.SelectedItem.Text & "0701", ddlYear.SelectedItem.Text & "0930", 7, 9)
                                    '' DisplayRecords("07/01/" & ddlYear.SelectedItem.Text, Now(), 7, 9)
                                    ''  DisplayRecords(New Date(ddlYear.SelectedItem.Text, 7, 1), Now(), 7, 9) -- Commented by siva

                                    ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, 7, 1)
                                    ViewState("EndDate") = New Date(ddlYear.SelectedItem.Text, 7, 1).AddMonths(3).AddDays(-1)
                                    DisplayRecords(New Date(ddlYear.SelectedItem.Text, 7, 1), New Date(ddlYear.SelectedItem.Text, 7, 1).AddMonths(3).AddDays(-1), 7, 9)
                                Case 4
                                    'FromDate = ddlYear.SelectedItem.Text & "1001"
                                    'ToDate = ddlYear.SelectedItem.Text & "1231"
                                    'DisplayRecords(ddlYear.SelectedItem.Text & "1001", ddlYear.SelectedItem.Text & "1231", 10, 12)
                                    '' DisplayRecords("10/01/" & ddlYear.SelectedItem.Text, Now(), 10, 12)
                                    ''DisplayRecords(New Date(ddlYear.SelectedItem.Text, 10, 1), Now(), 10, 12) -- Commented by siva
                                    ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, 10, 1)
                                    ViewState("EndDate") = New Date(ddlYear.SelectedItem.Text, 10, 1).AddMonths(3).AddDays(-1)
                                    DisplayRecords(New Date(ddlYear.SelectedItem.Text, 10, 1), New Date(ddlYear.SelectedItem.Text, 10, 1).AddMonths(3).AddDays(-1), 10, 12)
                            End Select
                        End If
                    Case 3
                        lblYear.Visible = True
                        ddlYear.Visible = True
                        lblQuarter.Visible = False
                        ddlQuarter.Visible = False
                        lblMonth.Visible = True
                        ddlMonth.Visible = True
                        ''Dim CurrentDate As String =  Format(ddlMonth.SelectedIndex, "00") & "/01/" & ddlYear.SelectedItem.Value --  Siva Discuss with Anoop
                        If ddlYear.SelectedIndex > 0 And ddlMonth.SelectedIndex > 0 Then Dim CurrentDate As String = New Date(ddlYear.SelectedItem.Value, Format(ddlMonth.SelectedIndex, "00"), 1)
                        If ddlYear.SelectedIndex > 0 And ddlMonth.SelectedIndex > 0 Then
                            ' DisplayRecords("10/01/" & ddlYear.SelectedItem.Text, Now(), 10, 12)
                            '' DisplayRecords(Format(ddlMonth.SelectedIndex + 1, "00") & "/01/" & ddlYear.SelectedItem.Text, Format(ddlMonth.SelectedIndex + 1, "00") & "/31/" & ddlYear.SelectedItem.Text, Format(ddlMonth.SelectedIndex + 1, "00"), Format(ddlMonth.SelectedIndex + 1, "00")) -- Siva
                            ViewState("StartDate") = New Date(ddlYear.SelectedItem.Text, Format(ddlMonth.SelectedIndex, "00"), 1)
                            ViewState("EndDate") = New Date(ddlYear.SelectedItem.Text, Format(ddlMonth.SelectedIndex, "00"), 1).AddMonths(1).AddDays(-1)
                            DisplayRecords(New Date(ddlYear.SelectedItem.Text, Format(ddlMonth.SelectedIndex, "00"), 1), New Date(ddlYear.SelectedItem.Text, Format(ddlMonth.SelectedIndex, "00"), 1).AddMonths(1).AddDays(-1), Format(ddlMonth.SelectedIndex, "00"), Format(ddlMonth.SelectedIndex, "00"))
                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 28
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgBestAccounts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBestAccounts.ItemCommand
            Try
                If e.CommandName = "Company" Then
                    If e.Item.Cells(3).Text = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=BestAccounts&DivID=" & e.Item.Cells(1).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                    ElseIf e.Item.Cells(3).Text = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=BestAccounts&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                    ElseIf e.Item.Cells(3).Text = 0 Then
                        Response.Redirect("../Leads/frmLeads.aspx?frm=BestAccounts&DivID=" & e.Item.Cells(1).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgBestAccounts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBestAccounts.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplDealClosed As HyperLink
                    Dim hplOppPipeline As HyperLink
                    hplDealClosed = e.Item.FindControl("hplDealsClosed")
                    hplDealClosed.NavigateUrl = "#"
                    hplDealClosed.Attributes.Add("onclick", "return OpenindRept('../Reports/frmBestAccountDealClosed.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=DealsClosed&DivID=" & e.Item.Cells(1).Text & "&stDate=" & ViewState("StartDate") & "&enDate=" & ViewState("EndDate") & "&NoRecords=" & hplDealClosed.Text & "')")
                    hplOppPipeline = e.Item.FindControl("hplOpportunitiesInPipeline")
                    hplOppPipeline.NavigateUrl = "#"
                    hplOppPipeline.Attributes.Add("onclick", "return OpenindRept('../Reports/frmBestAccountDealClosed.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=OppPipeline&DivID=" & e.Item.Cells(1).Text & "&stDate=" & ViewState("StartDate") & "&enDate=" & ViewState("EndDate") & "&NoRecords=" & hplOppPipeline.Text & "')")
                End If
                '' Response.Redirect("../Reports/frmBestAccountDealClosed.aspx?DivID=" & e.Item.Cells(1).Text & "&stDate=" & ViewState("StartDate") & "&enDate=" & ViewState("EndDate"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgBestAccounts, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                PopulateData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace