﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventoryGridReport.aspx.vb"
    Inherits="BACRM.UserInterface.MultiComp.InventoryGridReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register  Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Inventory Report </title>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Date Range:</label>
                    <asp:DropDownList ID="ddlDateRange" runat="server" CssClass="form-control"  AppendDataBoundItems="True" AutoPostBack="True">
                <asp:ListItem Text="-Select One--" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Last 30 days" Value="14"></asp:ListItem>
                <asp:ListItem Value="15">Last 60 days</asp:ListItem>
                <asp:ListItem Value="16">Last 90 days</asp:ListItem>
                <asp:ListItem Value="17">Month to date</asp:ListItem>
                <asp:ListItem Value="18">Quarter to date</asp:ListItem>
                <asp:ListItem Value="19">Year to date</asp:ListItem>
            </asp:DropDownList>
                    <label>Period:</label>
                    <asp:TextBox ID="txtPeriod" runat="server" ReadOnly="True"></asp:TextBox><asp:HiddenField
                    ID="hfPeriod" runat="server" />
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                     <label>
                    From
                </label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                 <label>
                    To
                </label>
                  <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                        </div>
                     <div class="form-group">
                    <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"></asp:Button>
                          </div>
                     <div class="form-group">
                    <asp:UpdatePanel ID="excelupdate" runat="server">
                        <ContentTemplate>
<asp:LinkButton runat="server" ID="ibExportExcel" CssClass="btn btn-success" AlternateText="Export to Excel"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton> 
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ibExportExcel" />
                        </Triggers>
                    </asp:UpdatePanel>
                        </div>
                     
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
                <label>Based On:</label>
                 <asp:DropDownList ID="ddlBasedOn" runat="server" CssClass="form-control"
                    >
                    <asp:ListItem Text="-Select One--" Value="0"></asp:ListItem>
                    <asp:ListItem Value="8">Average Cost</asp:ListItem>
                    <asp:ListItem Value="9">COGS</asp:ListItem>
                    <asp:ListItem Value="10">Profit</asp:ListItem>
                    <asp:ListItem Value="11">Amount Sold</asp:ListItem>
                    <asp:ListItem Value="12">Most Units Sold</asp:ListItem>
                    <asp:ListItem Value="12">Highest Return-Rate</asp:ListItem>
                </asp:DropDownList>
                <asp:CheckBox ID="chkFilter" runat="server" Text="Filter On" />
                <asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control"
                    >
                    <asp:ListItem Text="-All Item Types--" Value="21"></asp:ListItem>
                    <asp:ListItem Value="1">Services</asp:ListItem>
                    <asp:ListItem Value="2">Inventory Items</asp:ListItem>
                    <asp:ListItem Value="3">Non-Inventory Items</asp:ListItem>
                    <asp:ListItem Value="4">Serialized Items</asp:ListItem>
                    <asp:ListItem Value="5">Kits</asp:ListItem>
                    <asp:ListItem Value="6">Assemblies</asp:ListItem>
                    <asp:ListItem Value="7">Matrix Items</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlItemClass" runat="server" CssClass="form-control"
                    >
                    <asp:ListItem Text="-Select One--" Value=""></asp:ListItem>
                    <asp:ListItem Text="Item" Value="vcItemName"></asp:ListItem>
                    <asp:ListItem Text="Warehouse" Value="vcWarehouse"></asp:ListItem>
                    <asp:ListItem Text="SKU" Value="vcSKU"></asp:ListItem>
                    <asp:ListItem Text="ModelID" Value="vcModelID"></asp:ListItem>
                    <asp:ListItem Text="Vendor" Value="vcCompanyName"></asp:ListItem>
                    <asp:ListItem Text="Description" Value="txtItemDesc"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control"
                    >
                    <asp:ListItem Text="-Select One--" Value=""></asp:ListItem>
                    <asp:ListItem Text="Item" Value="vcItemName"></asp:ListItem>
                    <asp:ListItem Text="Warehouse" Value="vcWarehouse"></asp:ListItem>
                    <asp:ListItem Text="SKU" Value="vcSKU"></asp:ListItem>
                    <asp:ListItem Text="ModelID" Value="vcModelID"></asp:ListItem>
                    <asp:ListItem Text="Vendor" Value="vcCompanyName"></asp:ListItem>
                    <asp:ListItem Text="Description" Value="txtItemDesc"></asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnGo" CssClass="btn btn-primary" Text="Go" runat="server"></asp:Button>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
           <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Inventory Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <table align="right" width="100%" class="normal1">
        <%-- <tr>
            <td colspan="2" valign="top">
                IV Total :<asp:Label ID="lblIVTotal" runat="server"></asp:Label>
                </td>
                    </tr>--%>
        <tr>
            <td colspan="2" valign="top">
                <asp:DataGrid ID="dgReport" AutoGenerateColumns="False" runat="server" Width="100%"
                    AllowCustomPaging="True" AllowPaging="false" AllowSorting="false" PageSize="50"
                    CssClass="table table-responsive table-bordered tblDataGrid">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Label ID="lblRecordCount" Visible="false" runat="server"></asp:Label>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
        </div>
</asp:Content>
