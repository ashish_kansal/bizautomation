﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports

Public Class frmItemInventoryAndOppOrderReport
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                Dim m_aryRightsForPage As Integer() = GetUserRightsForPage(8, 133)
                BindReport()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindReport()
        Try
            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1

            Dim objReport As New PredefinedReports
            objReport.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim ds As DataSet = objReport.GetItemInventoryAndOrderdQtyReport(CCommon.ToLong(radItem.SelectedValue), CCommon.ToInteger(txtCurrrentPage.Text), If(CCommon.ToLong(ConfigurationManager.AppSettings("ReportPageSize")) <= 0, 100, CCommon.ToLong(ConfigurationManager.AppSettings("ReportPageSize"))))


            bizPager.PageSize = CCommon.ToLong(ConfigurationManager.AppSettings("ReportPageSize"))
            If bizPager.PageSize = 0 OrElse bizPager.PageSize < 0 Then bizPager.PageSize = 100
            bizPager.RecordCount = ds.Tables(1).Rows(0)(0)
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
            gvReport.DataSource = ds.Tables(0)
            gvReport.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        Catch exception As Exception

        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            txtCurrrentPage.Text = 1
            BindReport()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ibExportExcel_Click(sender As Object, e As EventArgs) Handles ibExportExcel.Click
        Try
            Dim objReport As New PredefinedReports
            objReport.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim ds As DataSet = objReport.GetItemInventoryAndOrderdQtyReport(0, 1, 100000000)

            Dim dtInventory As DataTable = ds.Tables(0)
            dtInventory.Columns("numItemCode").ColumnName = "Item Code"
            dtInventory.Columns("vcItemName").ColumnName = "Item Name"
            dtInventory.Columns("vcItemClassification").ColumnName = "Item Classification"
            dtInventory.Columns("numOnHand").ColumnName = "On Hand"
            dtInventory.Columns("numOnOrder").ColumnName = "On Order"
            dtInventory.Columns("numAllocation").ColumnName = "On Allocation"
            dtInventory.Columns("numBackOrder").ColumnName = "On BackOrder"
            dtInventory.Columns("numQtyOpenPO").ColumnName = "Open Purchase Orders"
            dtInventory.Columns("numQtyClosedPO").ColumnName = "Closed Purchase Orders"
            dtInventory.Columns("numQtyPOReturn").ColumnName = "Purchase Return"
            dtInventory.Columns("numQtyOpenSO").ColumnName = "Open Sales Orders"
            dtInventory.Columns("numQtyClosedSO").ColumnName = "Closed Sales Orders"
            dtInventory.Columns("numQtySOReturn").ColumnName = "Sales Return"


            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=InventoryOrderReport" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".csv")
            Response.Charset = ""
            Response.ContentType = "text/csv"
            Dim stringWrite As New System.IO.StringWriter
            CSVExport.ProduceCSV(dtInventory, stringWrite, True)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

End Class