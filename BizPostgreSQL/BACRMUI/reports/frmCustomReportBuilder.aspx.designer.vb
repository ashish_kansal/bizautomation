'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmCustomReportBuilder

    '''<summary>
    '''Head1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

    '''<summary>
    '''frmCustomReportBuilder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmCustomReportBuilder As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''webmenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents webmenu1 As Global.webmenu

    '''<summary>
    '''tblCustomReports control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblCustomReports As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''btnOrgReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnOrgReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''tblOrgReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblOrgReport As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''rbCRMType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbCRMType As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''ddlRelationShips control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRelationShips As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cbIncludeCustomFieldsOrg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbIncludeCustomFieldsOrg As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''rbIncludeContacts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbIncludeContacts As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''trContactTypes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trContactTypes As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''ddlContactTypes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlContactTypes As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnOppItemsReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnOppItemsReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''tblOppAndItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblOppAndItems As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''cbIncludeCustomFieldsOppItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbIncludeCustomFieldsOppItems As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''rbOppItemType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbOppItemType As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''btnLeadsReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnLeadsReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''tblLeads control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblLeads As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''tblReportParameters control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblReportParameters As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''tsCustomRptParams control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tsCustomRptParams As Global.Microsoft.Web.UI.WebControls.TabStrip

    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblReportType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReportType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnRunReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRunReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mpages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mpages As Global.Microsoft.Web.UI.WebControls.MultiPage

    '''<summary>
    '''tblReportLayout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblReportLayout As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''rbReportType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbReportType As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''tblReportColumns control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblReportColumns As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''tblFieldsGroupHeaders control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblFieldsGroupHeaders As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''tblFieldsGroup1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblFieldsGroup1 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''ddlReportColumnGroup1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup1 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlReportColumnGroup1Sel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup1Sel As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''tblFieldsGroup2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblFieldsGroup2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''ddlReportColumnGroup2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup2 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlReportColumnGroup2Sel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup2Sel As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''tblFieldsGroup3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblFieldsGroup3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''ddlReportColumnGroup3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup3 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlReportColumnGroup3Sel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup3Sel As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''tblFieldsGroup4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblFieldsGroup4 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''ddlReportColumnGroup4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup4 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlReportColumnGroup4Sel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup4Sel As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''tblFieldsGroup5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblFieldsGroup5 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''ddlReportColumnGroup5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup5 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlReportColumnGroup5Sel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReportColumnGroup5Sel As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''Table1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Table1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''dgAggeregateColumns control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgAggeregateColumns As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''tblColumnsOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblColumnsOrder As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''lstReportColumnsOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstReportColumnsOrder As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''tblSelectCriteria control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblSelectCriteria As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''ddlStandardFilters control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStandardFilters As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''calFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calFrom As Global.BACRM.Include.calandar

    '''<summary>
    '''calTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calTo As Global.BACRM.Include.calandar

    '''<summary>
    '''btnChooseTeams control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnChooseTeams As Global.System.Web.UI.HtmlControls.HtmlInputButton

    '''<summary>
    '''btnChooseTerritories control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnChooseTerritories As Global.System.Web.UI.HtmlControls.HtmlInputButton

    '''<summary>
    '''hdTeams control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdTeams As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdTerritories control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdTerritories As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''rdlReportType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdlReportType As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''rbAdvFilterJoiningCondition control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbAdvFilterJoiningCondition As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''ddlField1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlField1 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlCondition1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCondition1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtValue1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValue1 As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''ddlField2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlField2 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlCondition2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCondition2 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtValue2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValue2 As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''ddlField3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlField3 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlCondition3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCondition3 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtValue3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValue3 As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''ddlField4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlField4 As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''ddlCondition4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCondition4 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtValue4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValue4 As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''hdReportType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdReportType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdReportName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdReportName As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdReportDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdReportDesc As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdCustomReportConfig control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdCustomReportConfig As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''litClientSideScript control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litClientSideScript As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''btnAddToMyReports control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddToMyReports As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGo As Global.System.Web.UI.WebControls.Button
End Class
