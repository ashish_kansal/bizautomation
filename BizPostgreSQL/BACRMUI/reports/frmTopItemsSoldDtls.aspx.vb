Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class frmTopItemsSoldDtls : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents dgProductRec As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DisplayRecords()
                 ' = "Reports"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                Dim dtProducts As DataTable
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateFromFormattedDate(GetQueryStringVal( "From"), Session("DateFormat"))
                objPredefinedReports.ToDate = GetDateFrmFormattedDateAddDays(GetQueryStringVal( "To"), Session("DateFormat"))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportType = 50
                objPredefinedReports.ItemCode = GetQueryStringVal( "ItemCode")
                objPredefinedReports.SortOrder = GetQueryStringVal( "Type")
                dtProducts = objPredefinedReports.GetRepItemDtl
                dgProductRec.DataSource = dtProducts
                dgProductRec.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
