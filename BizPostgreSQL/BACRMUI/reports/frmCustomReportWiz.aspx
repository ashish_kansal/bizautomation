<%@ Page Language="vb" EnableEventValidation="false" AspCompat="true" AutoEventWireup="false"
    CodeBehind="frmCustomReportWiz.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCustomReportWiz"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Report Builder</title>
    <script language="JavaScript" src="../javascript/CustomReports.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function GetName() {
            // alert(document.getElementById('lstReportColumnsOrder')[document.getElementById('lstReportColumnsOrder').selectedIndex].text);
            document.getElementById("txtOrdSelName").value = document.getElementById('lstReportColumnsOrder')[document.getElementById('lstReportColumnsOrder').selectedIndex].text
            //document.form1.txtOrdSelName.value = document.form1.lstReportColumnsOrder[document.form1.lstReportColumnsOrder.options.selectedIndex].text
            return false;
        }

        function CheckKPI() {
            var ultraTab = 'A'
            if (ultraTab == 'A') {
                return true;
            }
            else {
                return true;
            }
        }

        function SetName() {
            document.getElementById('lstReportColumnsOrder')[document.getElementById('lstReportColumnsOrder').selectedIndex].text = document.getElementById("txtOrdSelName").value
            //document.form1.lstReportColumnsOrder[document.form1.lstReportColumnsOrder.options.selectedIndex].text = document.form1.txtOrdSelName.value
        }
        function ChangeTabIndex() {
            var tabStrip = $find('radOppTab');

            var selectedIndex = Math.abs(tabStrip.get_selectedIndex());
            if (selectedIndex != 0) {
                tabStrip.set_selectedIndex(selectedIndex - 1);
            }
            ChangeEd()
            return false
        }
        function ChangeTabFIndex() {
            var tabStrip = $find('radOppTab');

            var selectedIndex = Math.abs(tabStrip.get_selectedIndex());
            tabStrip.set_selectedIndex(selectedIndex + 1);
            ChangeEd()
            return false

        }


        function ChangeTTabIndex() {
            var tabStrip = $find('radOppTab');

            var selectedIndex = Math.abs(tabStrip.get_selectedIndex());
            // alert(document.getElementById('radOppTab__ctl1_chkLinerGrid').checked)
            if (document.getElementById('chkLinerGrid').checked == true) {
                tabStrip.set_selectedIndex(selectedIndex - 2);
            }
            else {
                tabStrip.set_selectedIndex(selectedIndex - 1);
            }
            ChangeEd()
            return false
        }
        function CheckOrderListLength() {
            if (document.getElementById('lstReportColumnsOrder').length > 0) {
                OrderList()
                return true
            }
                //            if (document.form1.lstReportColumnsOrder.length > 0) {
                //                OrderList();
                //                return true;
                //            }
            else {
                var radTab = $find("uwItemTab");
                radTab.set_selectedIndex(4);
                alert('No Fields are Selected');
                return false;
            }
        }

        function OrderList() {
            var text = ""
            var values = ""
            var varReportColumnsOrder = document.getElementById('lstReportColumnsOrder');  //document.form1.lstReportColumnsOrder;  
            for (var iIndex = 0; iIndex < varReportColumnsOrder.length; iIndex++) {
                if (iIndex == 0) {
                    values = varReportColumnsOrder[iIndex].value;
                    text = varReportColumnsOrder[iIndex].text;
                }
                else {
                    values = values + "|" + varReportColumnsOrder[iIndex].value;
                    text = text + "|" + varReportColumnsOrder[iIndex].text;
                }
            }
            //alert(values+'@@@@@'+text)

            document.getElementById('txtOrderListValues').value = values
            document.getElementById('txtOrderListText').value = text
            //document.form1.txtOrderListValues.value = values;
            //document.form1.txtOrderListText.textContent = text;

            ChangeEd()
        }
        function Check() {

            if (document.getElementById('txtReportName').value == "") {
                alert("Enter The Report Name")
                return false
            }
                //            if (document.form1.txtReportName.value == "") {
                //                alert("Enter the Report Name.")
                //                return false;
                //            }
            else {
                OrderList();
            }
            //return false
        }
        function CheckDropDown() {
            var Value = document.getElementById("ddlModule").value
            var group = document.getElementById("lstGroup").value

            //var Value = document.form1.ddlModule.value; 
            //var group = document.lstGroup.value;  

            if (Value == 11 && group == 31) {
                document.getElementById("chkRecCount").disabled = true
                //document.form1.chkRecCount.disabled = true;
            }
            else {
                document.getElementById("chkRecCount").disabled = false
                //document.form1.chkRecCount.disabled = false;
            }
            return false
            //document.getElementById("radOppTab__ctl0_ddlRelContact")
            //document.getElementById("radOppTab__ctl0_ddlRelOrg")

        }
        function openScheduler(a, b) {

            if (a == 0) {
                var CustRptScheduler = window.open('../reports/frmCustRptScheduler.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=' + b, 'CustRptScheduler', 'toolbar=no,titlebar=no,top=300,width=850,height=500,scrollbars=no,resizable=no')
                CustRptScheduler.focus()
            }
            else {
                var CustRptScheduler = window.open('../reports/frmCustRptScheduler.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ScheduleId=' + a + '&ReportId=' + b, 'CustRptScheduler', 'toolbar=no,titlebar=no,top=300,width=850,height=500,scrollbars=no,resizable=no')
                CustRptScheduler.focus()
            }

            return false
        }
        function DelSchedule(a) {

            OrderList()
            //document.getElementById("txtScheduleId").value = a
            // document.getElementById("btndelSchedule").click()
            return true;
        }
        function Bind() {

            OrderList()
            document.getElementById("btndelSchedule").click()
            //document.form1.btndelSchedule.click();
        }
        function ChangeEd() {
            // alert('ChangeEd')
            var value = document.form1.ddlGrp1List.value.split("~")[3];  //document.getElementById("ddlGrp1List").value.split('~')[3];
            ////alert(value)
            if (value == 'Date Field' || value == 'Date Field U' || value == 'Date Field D') {
                document.getElementById("ddlGrp1Filter").disabled = false;
                //document.form1.ddlGrp1List.disabled = false;
            }
            else {
                ////  alert('else')
                document.getElementById("ddlGrp1Filter").disabled = true;
                //document.form1.ddlGrp1List.disabled = true;
            }

            return false
        }
        function ReportList() {
            window.location.href = '../Reports/frmCustomRptList.aspx'
        }
        function AdvFltCheck(a) {
            if (a == '0') {
                document.getElementById('hplAdvFilter').style["display"] = 'none';
                document.getElementById('hplClrAdvFilter').style["display"] = '';
                document.getElementById('lblAdvFltTxt').style["display"] = '';
                document.getElementById('txtAdvFilter').style["display"] = '';
                document.getElementById('pnlAndOr').style["display"] = 'none';
                document.getElementById('txtchkAdvFilter').value = 1;

                //                document.form1.hplAdvFilter.style["display"] = 'none';
                //                document.form1.hplClrAdvFilter.style["display"] = '';
                //                document.form1.lblAdvFltTxt.style["display"] = '';
                //                document.form1.txtAdvFilter.style["display"] = '';
                //                document.form1.pnlAndOr.style["display"] = 'none';
                //                document.form1.txtchkAdvFilter.value = 1;
            }
            else {

                document.getElementById('hplAdvFilter').style["display"] = '';
                document.getElementById('hplClrAdvFilter').style["display"] = 'none';
                document.getElementById('lblAdvFltTxt').style["display"] = 'none';
                document.getElementById('txtAdvFilter').style["display"] = 'none';
                document.getElementById('pnlAndOr').style["display"] = '';
                document.getElementById('txtchkAdvFilter').value = 0;

                //                document.form1.hplAdvFilter.style["display"] = '';
                //                document.form1.hplClrAdvFilter.style["display"] = 'none';
                //                document.form1.lblAdvFltTxt.style["display"] = 'none';
                //                document.form1.txtAdvFilter.style["display"] = 'none';
                //                document.form1.pnlAndOr.style["display"] = '';
                //                document.form1.txtchkAdvFilter.value = 0;
            }

            return false;
        }
        function openPop(i) {
            //alert(i)

            var a = document.getElementById('ddlField' + i).value;
            var type = document.getElementById('ddlField' + i).value.split('~')[2];

            if (type == 'Date Field' || type == 'Date Field U' || type == 'Date Field D') {

                var CustomReportVal = window.open("../reports/frmCustomReportVal.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ddl=" + a + "&i=" + i, 'CustomReportVal', 'width=100px,resizable=yes,height=240px,status=no,titlebar=no,top=200,left=250')
                CustomReportVal.focus();
            }
            else {
                var CustomReportVal = window.open("../reports/frmCustomReportVal.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ddl=" + a + "&i=" + i, 'CustomReportVal', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=200,left=250')
                CustomReportVal.focus()
            }
            return false
        }
        function ChkFilterType(ddl, i) {

            var type = document.getElementById('' + ddl).value.split('~')[2]

            if (type == 'Drop Down List' || type == 'Customdata' || type == 'Check box' || type == 'Contact' || type == 'Date Field' || type == 'Date Field U' || type == 'Date Field D' || type == 'State' || type == 'Terriotary' || type == 'UOM' || type == 'ItemGroup' || type == 'IncomeAccount' || type == 'AssetAcCOA' || type == 'COG' || type == 'Currency') {
                document.getElementById('Value' + i).disabled = true;
                if (type != 'Date Field' && type != 'Date Field U' && type != 'Date Field D') {
                    document.getElementById('imgHpl' + i).src = "../images/SEARCH_ICON.GIF"
                }
                else {
                    document.getElementById('imgHpl' + i).src = "../images/SEARCH_ICON.GIF"
                }

                document.getElementById('imgHpl' + i).style['display'] = ''
            }
            else {
                //alert('a')
                document.getElementById('Value' + i).disabled = false;
                document.getElementById('imgHpl' + i).style['display'] = 'none'
            }
            return false
        }
        function InserFldValue(val, vald, i) {
            // alert(val)
            // alert(i)            
            document.getElementById('Value' + i).value = val
            document.getElementById('Valued' + i).value = vald
        }

        function SelectGroupField(i) {
            var chkBox = $("input[id$='chkGroupAll_" + i + "']");

            $('input:checkbox').each(function () {

                if ($(this).attr('group_name') == i) {
                    this.checked = chkBox.is(':checked');
                }
            });
        }
    </script>
    <style type="text/css">
        .WebTab_Main {
            height: 23px;
            font-weight: bold;
            font-size: 11px;
            font-family: Arial;
        }

        .WebTab_Image {
            leftsidewidth: 7;
            rightsidewidth: 8;
            shiftofimages: 0;
            background-image: url(../images/ig_tab_winXP3.gif);
            fillstyle: LeftMergedWithCenter;
        }

        .WebTab_Selected {
            height: 23px;
            color: white;
            background-image: url(../images/ig_tab_winXPs3.gif);
        }

        .WebTab_Hover {
            background-image: url(../images/ig_tab_winXPs3.gif);
            height: 23px;
            color: white;
        }
   </style>
    <style>
    .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
        
   .hs {
        border: 1px solid #e4e4e4;
        border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    text-align: center;
    font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="pull-left form-inline">
                    <label>Report Name :</label>
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtReportName" MaxLength="150"></asp:TextBox>
                    <label>Report Description :</label>
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtReportDesc" MaxLength="500"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right form-inline">
                      <asp:UpdatePanel ID="Updatepanel1" runat="server">
                        <ContentTemplate>
                             <asp:LinkButton runat="server" ID="btnSave" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save Report</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnExportExcel" CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp;Export To Excel</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnExportPdf" CssClass="btn btn-primary"><i class="fa fa-file-pdf-o"></i>&nbsp;Export To Pdf</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnSaveClose" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save & Close</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnClose" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;Close</asp:LinkButton>
                         
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSave" />
                            <asp:PostBackTrigger ControlID="btnExportExcel" />
                            <asp:PostBackTrigger ControlID="btnExportPdf" />
                            <asp:PostBackTrigger ControlID="btnSaveClose" />
                            <asp:PostBackTrigger ControlID="btnClose" />
                        </Triggers>
                    </asp:UpdatePanel>
                    
                              
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" SelectedIndex="0" MultiPageID="radMultiPage_OppTab"
        ClientIDMode="Static">
        <Tabs>
            <telerik:RadTab Text="Report Type" Value="ReportType" PageViewID="radPageView_ReportType">
            </telerik:RadTab>
            <telerik:RadTab Text="Grid Type" Value="GridType" PageViewID="radPageView_GridType">
            </telerik:RadTab>
            <telerik:RadTab Text="Summation" Value="Summation" PageViewID="radPageView_Summation">
            </telerik:RadTab>
            <telerik:RadTab Text="Grouping" Value="Grouping" PageViewID="radPageView_Grouping">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Report Fields &nbsp;&nbsp;" Value="ReportFields"
                PageViewID="radPageView_ReportFields">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Column Order &nbsp;&nbsp;" Value="ColumnOrder"
                PageViewID="radPageView_ColumnOrder">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Filters &nbsp;&nbsp;" Value="Filters" PageViewID="radPageView_Filters">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Report Preveiw&nbsp;&nbsp;" Value="ReportPreveiw"
                PageViewID="radPageView_ReportPreveiw">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Report Scheduler&nbsp;&nbsp;" Visible="false" Value="ReportScheduler"
                PageViewID="radPageView_ReportScheduler">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_ReportType" runat="server">
            <asp:Table ID="tblContacts" runat="server" Width="100%" GridLines="None" CssClass="aspTable"
                Height="300">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <div class="row">
                            <div class="col-md-12 form-inline">
                                
                                    <label>Select The Module From Which Data is to be Fetched :</label>
                                     <asp:DropDownList AutoPostBack="true" runat="server" ID="ddlModule"
                                        CssClass="form-control">
                                    </asp:DropDownList>
                               
                                    <label>Select The Group From Which Data is to be Fetched :</label>
                                    <asp:ListBox runat="server" AutoPostBack="true" ID="lstGroup" Height="200px"
                                        CssClass="form-control"></asp:ListBox>
                               
                            </div>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Bottom">
                    <asp:TableCell VerticalAlign="Bottom">
                        <div class="pull-right">
                            <asp:LinkButton ID="btn1" runat="server" OnClientClick="return ChangeTabFIndex()" CssClass="btn btn-primary">&nbsp;Next<i class="fa fa-arrow-right"></i></asp:LinkButton>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_GridType" runat="server" Style="border-style: hidden">
            <div class="row">
                <div class="col-md-12 form-inline">
                    <label>Select The Type Of Grid</label>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6 form-inline">
                    <asp:RadioButton ID="chkLinerGrid" Checked="true" CssClass="normal1" runat="server"
                            GroupName="grpGrid" Text="Liner Grid" />
                    <img src="../images/tabularReportLayout.gif" hspace="20" width="140" height="100" />
                </div>                    
                <div class="col-md-6 form-inline">
                     <asp:RadioButton ID="chkSummationGrid" runat="server" CssClass="normal1" GroupName="grpGrid"
                            Text="Summation Grid" />
                    <img src="../images/reportsummarystyle.gif" hspace="20" width="140" height="100" />
                </div>
                    </div>
                <div class="col-md-12">
                    <div class="pull-right">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return ChangeTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
                        
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnBuildSummation">Next&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Summation" runat="server">
            <asp:Table runat="server" ID="tblSum" Width="100%" CellSpacing="0" GridLines="Both"
                CssClass="table table-responsive table-bordered" Style="border-style: hidden">
            </asp:Table>
          
            <div class="col-md-12 form-group">
                <div class="pull-right">
                    <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="return ChangeTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
                        
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnBuildFields">Next&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>
                </div>
            </div>
            
            <%--<asp:Table runat="server" ID="tblSumm" Width="100%" CellSpacing="0" GridLines="Both"
                CellPadding="0" Height="300" Style="border-style: hidden">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell VerticalAlign="Top">
                        <br />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="bottom">
                    <asp:TableCell>
                       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>--%>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Grouping" runat="server">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label>
                            Summarize information by
                        </label>
                    </div>
                    <div class="col-md-4">
                        <span id="spnGrp1List" runat="server"></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <label>Sort by Record Count</label>

                    </div>
                    <div class="col-md-4">
                        <asp:CheckBox ID="chkSortRecCount" runat="server" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-inline">
                        <div class="col-md-3">
                            <label>Sort Order</label>
                        </div>
                        <div class="col-md-4">
                            <asp:DropDownList CssClass="form-control" ID="ddlGrp1Order" runat="server">
                                <asp:ListItem Value="Asc" Text="Ascending"></asp:ListItem>
                                <asp:ListItem Value="Desc" Text="Descending"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-inline">
                        <div class="col-md-3">
                            <label for="brkdat0" class="normal1">
                                Group Dates By</label>
                        </div>
                        <div class="col-md-4">
                            <asp:DropDownList CssClass="form-control" Enabled="false" ID="ddlGrp1Filter" runat="server">
                                <asp:ListItem Value="0">Day</asp:ListItem>
                                <asp:ListItem Value="1">Calendar Week</asp:ListItem>
                                <asp:ListItem Value="2">Calendar Month</asp:ListItem>
                                <asp:ListItem Value="3">Calendar Quarter</asp:ListItem>
                                <asp:ListItem Value="4">Calendar Year</asp:ListItem>
                                <asp:ListItem Value="5">Fiscal Quarter</asp:ListItem>
                                <asp:ListItem Value="6">Fiscal Year</asp:ListItem>
                                <asp:ListItem Value="7">Calendar Month in Year</asp:ListItem>
                                <asp:ListItem Value="8">Calendar Day in Month</asp:ListItem>
                                <asp:ListItem Value="9">Last Week Vs This Week</asp:ListItem>
                                <asp:ListItem Value="10">Last Month Vs This Month</asp:ListItem>
                                <asp:ListItem Value="11">Last Year Vs This Year</asp:ListItem>
                                <asp:ListItem Value="12">Last Quarter Vs This Quarter</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-3">
                        <label>DrillDown Report</label>
                    </div>
                    <div class="col-md-4">
                        <asp:CheckBox ID="chkDrllDownRpt" runat="server" />
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pull-right">
                <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="return ChangeTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
                        
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="LinkButton4" OnClientClick="return ChangeTabFIndex()">Next&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>
            </div>
           </div>
            <%--<asp:Table runat="server" ID="Table1" Width="100%" CellSpacing="0" CellPadding="0"
                Height="300" CssClass="aspTable">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell HorizontalAlign="left">
                        <br />
                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Bottom">
                    <asp:TableCell>  </asp:TableCell>
                </asp:TableRow>
            </asp:Table>--%>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ReportFields" runat="server">
            <span runat="server" id="spnSelColumns"></span>
            <asp:Table runat="server" ID="tblFileds" Width="100%" CellSpacing="0" CellPadding="0"
                Style="border-style: hidden">
            </asp:Table>
            <div class="col-md-12">
                <div class="pull-right">
            <asp:LinkButton ID="LinkButton5" runat="server" OnClientClick="return ChangeTTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnBuildListItems">Next&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>
                </div>
                </div>
            <%--<asp:Table runat="server" ID="Table2" Width="100%" CellSpacing="0" GridLines="Both"
                CellPadding="0" Height="300" Style="border: 0px solid white">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Bottom">
                    <asp:TableCell>
                        
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>--%>
            <table class="normal1" border="0" cellspacing="4" style="visibility: hidden;">
                <tr>
                    <td>Indicator&nbsp;
                    </td>
                    <td>
                        <asp:DropDownList CssClass="signup" Width="220px" ID="ddlKPI" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button CssClass="button" Text="Add" Width="60" runat="server" ID="btnAddKPI" />
                    </td>
                </tr>
            </table>
            <asp:Table runat="server" ID="TableKPI" Width="100%" CellSpacing="0" GridLines="Both"
                CellPadding="0" Height="300" Visible="false">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                        <asp:DataGrid ID="dgKPI" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numReportOptionGroupId" HeaderText="GroupId" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numFieldGroupID" HeaderText="FieldGroupID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="vcIndicator" HeaderText="Indicator" Visible="true"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Period" ItemStyle-VerticalAlign="Bottom">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlPeriod" Width="100%" CssClass="signup" AutoPostBack="false"
                                            runat="server" DataSource="<%# PopulatePeriod() %>" DataTextField="vcKPIMaster"
                                            DataValueField="numKPIMasterId">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Priority" ItemStyle-VerticalAlign="Bottom">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlPriority" Width="100%" CssClass="signup" AutoPostBack="false"
                                            runat="server" DataSource="<%# PopulatePriority() %>" DataTextField="vcKPIMaster"
                                            DataValueField="numKPIMasterId">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Status Icon" ItemStyle-VerticalAlign="Bottom">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlStatus" Width="100%" CssClass="signup" AutoPostBack="false"
                                            runat="server" DataSource="<%# PopulateStatusIcon() %>" DataTextField="vcKPIMaster"
                                            DataValueField="numKPIMasterId">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                            Visible='<%# IIf(Eval("numReportOptionGroupId")=0,false,true) %>' CommandArgument='<%# Eval("numReportOptionGroupId") %>'></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ColumnOrder" runat="server">
            <asp:Table runat="server" ID="tblColumnsOrder" Width="100%" CellSpacing="2" CellPadding="2"
                Height="300" CssClass="aspTable">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top" CssClass="normal1" Width="30%" Style="padding-left: 50px;">
                        <span style="font-weight: bold">Set Order for your Selected Columns</span><br>
                        <span style="font-style: italic">(Fields at the top will appear on the left and first
                            100 columns will be displayed only)</span><br>
                        <span runat="server" id="spnListOrder">
                            <asp:ListBox ID="lstReportColumnsOrder" runat="server" CssClass="signup" Width="250"
                                Height="200" EnableViewState="true"></asp:ListBox>
                        </span>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" CssClass="normal1" Width="5%">
											&nbsp;<input type="button" id="btnMoveupTopColumns" Class="btn btn-sm btn-primary" value="    Top   " onclick="javascript: moveTop(document.getElementById('lstReportColumnsOrder'))">
											<br>
											<br />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img id="btnMoveupColumns" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.getElementById('lstReportColumnsOrder'))"/>
											<br>
											<br>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img id="btnMoveDownColumns" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.getElementById('lstReportColumnsOrder'))"/>
											<br><br>
											<input type="button" id="btnMoveupLastColumns" Class="btn btn-sm btn-primary" value="Bottom" onclick="javascript: moveBottom(document.getElementById('lstReportColumnsOrder'))">
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" CssClass="normal1">&nbsp;</asp:TableCell>
                </asp:TableRow>
               
               
                    
              
            </asp:Table>
            <div class="row">
                <div class="col-md-12 form-inline">
                    <label>
                        Set The Column Name for the Selected Field :
                    </label>
                    <INPUT TYPE="text" class="form-control" id='txtOrdSelName' NAME='txtOrdSelName' />
                    <input name="btnSetName" type="button" id="btnSetName" Class="btn btn-primary" style="width:50" value="Set" onclick="SetName()" />
                </div>
            </div>
            <div class="col-md-12">
                        <div class="pull-right">
                             <asp:LinkButton ID="LinkButton6" runat="server" OnClientClick="return ChangeTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnBuildFilterList">Next&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>
                        </div>
                    </div>
                   
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Filters" runat="server">
            <asp:Table runat="server" ID="tblFilters" Width="100%" CellSpacing="0" GridLines="none"
                CellPadding="0" Height="300" CssClass="aspTable">
                <asp:TableRow>
                    <asp:TableCell>
                        <div class="row">
                            <div class="col-md-12 form-group form-inline">
                                <div class="col-md-2">
                                    <label>Standard Filters</label>
                                </div>
                                <div class="col-md-5">
                                    <span runat="server" id="spnStdFlt"></span>&nbsp;
                                     <asp:DropDownList ID="ddlCustomTime" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">Custom</asp:ListItem>
                                        <asp:ListItem Value="yesterday">Yesterday</asp:ListItem>
                                        <asp:ListItem Value="today">Today</asp:ListItem>
                                        <asp:ListItem Value="tomorrow">Tomorrow</asp:ListItem>
                                        <asp:ListItem Value="last7">Last 7 Days</asp:ListItem>
                                        <asp:ListItem Value="last15">Last 15 Days</asp:ListItem>
                                        <asp:ListItem Value="last30">Last 30 Days</asp:ListItem>
                                        <asp:ListItem Value="last60">Last 60 Days</asp:ListItem>
                                        <asp:ListItem Value="last90">Last 90 Days</asp:ListItem>
                                        <asp:ListItem Value="last120">Last 120 Days</asp:ListItem>
                                        <asp:ListItem Value="next7">Next 7 Days</asp:ListItem>
                                        <asp:ListItem Value="next15">Next 15 Days</asp:ListItem>
                                        <asp:ListItem Value="next30">Next 30 Days</asp:ListItem>
                                        <asp:ListItem Value="next60">Next 60 Days</asp:ListItem>
                                        <asp:ListItem Value="next90">Next 90 Days</asp:ListItem>
                                        <asp:ListItem Value="next120">Next 120 Days</asp:ListItem>
                                        <asp:ListItem Value="lastweek">Last Week</asp:ListItem>
                                        <asp:ListItem Value="thisweek">This Week</asp:ListItem>
                                        <asp:ListItem Value="nextweek">Next Week</asp:ListItem>
                                        <asp:ListItem Value="lastmonth">Last Month</asp:ListItem>
                                        <asp:ListItem Value="thismonth">This Month</asp:ListItem>
                                        <asp:ListItem Value="nextmonth">Next Month</asp:ListItem>
                                        <asp:ListItem Value="lastthismonth">Current and Previous Month</asp:ListItem>
                                        <asp:ListItem Value="thisnextmonth">Current and Next Month</asp:ListItem>
                                        <asp:ListItem Value="cury">Current Calendar Year</asp:ListItem>
                                        <asp:ListItem Value="prevy">Previous Calendar Year</asp:ListItem>
                                        <asp:ListItem Value="prev2y">Previous 2 Calendar Years</asp:ListItem>
                                        <asp:ListItem Value="ago2y">2 Calendar Years Ago</asp:ListItem>
                                        <asp:ListItem Value="nexty">Next Calendar Year</asp:ListItem>
                                        <asp:ListItem Value="prevcury">Current and Previous Calendar Year </asp:ListItem>
                                        <asp:ListItem Value="prevcur2y">Current and Previous 2 Calendar Years </asp:ListItem>
                                        <asp:ListItem Value="curnexty">Current and Next Calendar Year </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                   <label> &nbsp;Or&nbsp;</label>
                                </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="col-md-2">
                                    <label> From Date :</label>
                                </div>
                                <div class="col-md-2">
                                    <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                                </div>
                                <div class="col-md-1">
                                    <label>To Date :</label>
                                </div>
                                <div class="col-md-2">
                                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                                </div>
                                <div class="clearfix"></div>
                                
                            </div>
                        </div>
                       
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="normal1">
                        <div class="row">
                            <div class="col-md-12 form-group form-inline">
                                <div class="col-md-2">
                                    <label>Comparison Filter</label>
                                </div>
                                <div class="col-md-7">
                                    <span id="spnddlCompFlt1" runat="server"></span>
                                &nbsp;
                                     <asp:DropDownList ID="ddlCompFOpe" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="=">equals</asp:ListItem>
                                        <asp:ListItem Value="!=">not equal to</asp:ListItem>
                                        <asp:ListItem Value="<">less than</asp:ListItem>
                                        <asp:ListItem Value=">">greater than</asp:ListItem>
                                        <asp:ListItem Value="'>='">greater or equal</asp:ListItem>
                                        <asp:ListItem Value="'<='">less or equal</asp:ListItem>
                                    </asp:DropDownList>
                                &nbsp;
                                    <span id="spnddlCompFlt2" runat="server"></span>
                                </div> 
                                <div class="clearfix"></div> 
                            </div>
                        </div>                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="normal1">
                        <div class="row">
                            <div class="col-md-12 form-group form-inline">
                                <div class="col-md-2">
                                    <label>No of Records Display :</label>
                                </div>
                                <div class="col-md-4">
                                     <asp:DropDownList ID="ddlNoRows" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="1000" Value="1000"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="pnllinear" runat="server" class="col-md-12 form-group form-inline">
                                <div class="col-md-2" runat="server" id="tdOrderFlt">
                                <label>Records Sort By : </label>
                                </div>
                                <div class="col-md-5">
                                    <span runat="server" id="SpanorderFlt"></span>
                               
                                    <asp:DropDownList CssClass="form-control" ID="ddlOrderFilter" runat="server">
                                        <asp:ListItem Value="Asc" Text="Ascending"></asp:ListItem>
                                        <asp:ListItem Value="Desc" Text="Descending"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="normal1">
                       <div class="row">
                           <div class="col-md-12 form-group form-inline">
                               <div class="col-md-6 form-group" style="line-height:50px;">
                               <asp:RadioButton runat="server" Text="" GroupName="FilterGroup" ID="radMyself" />&nbsp;
                                    When&nbsp;<span runat="server" id="spnMyself"></span>&nbsp;MySelf
                           </div>
                               <div class="clearfix"></div>
                                <div class="col-md-6 form-group" style="line-height:50px;">
                               <asp:RadioButton runat="server" Text="" GroupName="FilterGroup" ID="radTeamSelected" />&nbsp;
                                    When&nbsp;<span runat="server" id="spnTeam"></span>&nbsp;Team Selected
                           </div>
                               <div class="clearfix"></div>
                               <div class="col-md-6 form-group" style="line-height:50px;">
                              <asp:RadioButton runat="server" Text="" GroupName="FilterGroup" ID="radTerriotary" />&nbsp;
                                    When&nbsp;<span runat="server" id="spnTerr"></span>&nbsp;Territary Selected
                           </div>
                               <div class="clearfix"></div>
                               </div>
                       </div>
                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                        <asp:Panel ID="pnlAndOr" runat="server">
                            <div class="col-md-12 form-group">
                                <asp:Label ID="Label1" runat="server" Text="&nbsp;Select Whether And/Or for the filters"
                                CssClass="normal1"></asp:Label>
                            <asp:RadioButton ID="radAnd" Checked="true" CssClass="normal7" runat="server" Text="And"
                                GroupName="AndOr" />
                            <asp:RadioButton ID="radOr" CssClass="normal7" runat="server" Text="Or" GroupName="AndOr" />
                            </div>
                            
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                        <div class="col-md-12 form-group form-inline">
                             <span id="list" runat="server"></span>
                        <table id="tblAddFilters" runat="server" class="table  table-responsive">
                        </table>
                        </div>
                       
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                        <div class="row">
                            <div class="col-md-12 form-group form-inline">
                                <div class="col-md-3">
                                    <asp:HyperLink ID="hplAdvFilter" Text="Advanced Filter" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                    <asp:HyperLink ID="hplClrAdvFilter" Style="display: none" Text="Clear Advanced Filter"
                                        runat="server" CssClass="hyperlink"></asp:HyperLink>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                     <asp:Label ID="lblAdvFltTxt" Style="display: none" CssClass="normal7" Text="Advanced Filter Condition &nbsp;&nbsp;Used above condition Ex : ( 1 or 2) and 3 "
                                        runat="server"></asp:Label>
                                     <asp:TextBox runat="server" Style="display: none" ToolTip="Used above condition Ex :( 1 or 2) and 3"
                                        ID="txtAdvFilter" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <table>
                            <tr>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Bottom">
                    <asp:TableCell ColumnSpan="2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                <asp:Button CssClass="btn btn-primary" Text="Add" runat="server" ID="btnAddFilterRow" />&nbsp;
                                 <asp:Button CssClass="btn btn-primary" Text="Report Preview" runat="server" ID="btnReport" />
                            </div>
                                <div class="pull-right">
                                    <asp:LinkButton ID="LinkButton7" runat="server" OnClientClick="return ChangeTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
                                </div>
                                </div>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ReportPreveiw" runat="server">
            <asp:Table runat="server" ID="tblReport" Width="100%" CellSpacing="0" CellPadding="0"
                Height="300">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell>
                        <asp:DataGrid ID="dgReport" AllowSorting="true" runat="server" Width="100%" CssClass="table table-responsive table-bordered"
                            AutoGenerateColumns="false">
                            <HeaderStyle CssClass="hs" />

                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                    </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="rowGrid" VerticalAlign="Top">
                    <asp:TableCell>
                        <asp:Table runat="server" ID="tblSummaryGrid" Width="100%" CellSpacing="0" CellPadding="0"
                            CssClass="table table-responsive tblDataGrid">
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Bottom">
                    <asp:TableCell>		
                        <div class="col-md-12">
                        <div class="pull-right">
                             <asp:LinkButton ID="LinkButton8" runat="server" OnClientClick="return ChangeTabIndex()" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>							       
									       </div>
                            </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ReportScheduler" runat="server">
            <asp:Button runat="server" ID="btnNewScheduler" Width="100px" CssClass="button" Text="New Schedule" />
            <asp:DataGrid ID="dgScheduler" AllowSorting="true" runat="server" Width="100%" CssClass="table table-responsive tblDataGrid"
                AutoGenerateColumns="false">
                
                <Columns>
                    <asp:BoundColumn DataField="numScheduleId" Visible="False"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Email">
                        <ItemTemplate>
                            <asp:HyperLink ID="hplSchdeule" runat="server" CssClass="hyperlink" Text='<%# DataBinder.Eval(Container.DataItem, "varScheduleName") %>'
                                Target="_blank">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="vcReportName" HeaderText="ReportName"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vcReportDescription" HeaderText="ReportDescription"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Interaval" HeaderText="Interval,Duration"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="">
                        <ItemTemplate>
                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
            <input type="button" class="btn btn-primary" onclick="return ChangeTabIndex()"
                value="Back" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Custom Report Wizard
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server"
    ClientIDMode="Static">
    <asp:TextBox ID="noFilterRows" Text="4" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="noSummationRows" Text="4" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSql" Style="display: none" Text="" runat="server" Width="100%"
        Height="50px"></asp:TextBox>
    <asp:TextBox ID="txtOrderListText" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtOrderListValues" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtReportId" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtScheduleId" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtchkAdvFilter" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtAdvFilterStr" Width="80%" Text="" Style="display: none" runat="server"></asp:TextBox>
    <asp:DropDownList Style="display: none" runat="server" ID="dio">
    </asp:DropDownList>
    <asp:TextBox ID="Textbox1" Text="0" Width="100%" Height="50px" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btndelSchedule" runat="server" Style="display: none" />
</asp:Content>
