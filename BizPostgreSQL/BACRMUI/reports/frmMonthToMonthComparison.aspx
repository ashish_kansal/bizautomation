<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmMonthToMonthComparison.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmMonthToMonthComparison" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Month To Month Comparison</title>
    <script language="javascript">
        function OpenSelTeam(repNo) {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenAsItsType(repNo) {
            window.open("../Reports/frmSelectAsItsType.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
           <div class="pull-left">
               <div class="form-inline">
                   <div class="form-group">
            <asp:RadioButtonList ID="rbtnlistType"  runat="server" RepeatDirection="Horizontal"
                                AutoPostBack="True">
                                <asp:ListItem Selected="True">Teamwise</asp:ListItem>
                                <asp:ListItem>Territorywise</asp:ListItem>
                            </asp:RadioButtonList>
                       </div>
                   <div class="form-group">
             <asp:DropDownList ID="ddlTeam1" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                       </div>
                   <div class="form-group">
             <asp:DropDownList ID="ddlTeam2" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                       </div>
               </div>
        </div>
            </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left form-inline">
                <label>Compare Year</label>
                <asp:DropDownList ID="ddlYear1" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                <label>Month</label>
                <asp:DropDownList ID="ddlMonth1" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                <label>To Year</label>
                <asp:DropDownList ID="ddlYear2" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                <label>Month</label>
                <asp:DropDownList ID="ddlMonth2" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                    <asp:LinkButton ID="btnExportToExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Month To Month Comparison
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="Table9" BorderWidth="1" Height="400" runat="server" Width="100%" BorderColor="black"
        CssClass="table table-responsive table-bordered" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server" ID="tblData" BorderWidth="0px">
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
