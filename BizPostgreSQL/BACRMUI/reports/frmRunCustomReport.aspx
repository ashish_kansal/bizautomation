﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmRunCustomReport.aspx.vb" Inherits=".frmRunCustomReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .rgMasterTable {
            border-collapse: collapse !important;
            border-color: #dad8d8 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <asp:Button ID="btnSendEmail" runat="server" Text="Send Email" CssClass="button" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    <asp:Label ID="lblReportName" runat="server" Text="Label"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:Panel ID="pnlReport" runat="server">
        <telerik:RadGrid ID="rgReportData" runat="server" Width="100%"
            CssClass="dg" AutoGenerateColumns="true" GridLines="Both"
            ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
            AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
            <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />

        </telerik:RadGrid>
    </asp:Panel>
    <asp:HiddenField ID="hdnReportID" runat="server" />
</asp:Content>
