<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false"  CodeBehind="frmTimeExpPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmTimeExpPrint" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
    <title>Untitled Page</title>

</head>
<body>
    <form id="form1" runat="server">
    <TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" CssClass="print_head" Runat="server">Project Status Report</asp:Label>
					</td>
				</tr>
			</table>
			<script language="Javascript" type="text/javascript" >
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
						<asp:DataGrid ID="dgContactRole" CssClass="dg" Width="100%" Runat="server" BorderColor="white"
							AutoGenerateColumns="False" AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numUserID" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font color=white>Employee</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Position" SortExpression="Position" HeaderText="<font color=white>Position</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="BillTime" SortExpression="BillTime" HeaderText="<font color=white>B-Time</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="BillTimeAmt" DataFormatString="{0:#,##0.00}" SortExpression="BillTimeAmt" HeaderText="<font color=white>BT Amount</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="NonBillTime" SortExpression="NonBillTime" HeaderText="<font color=white>NB-Time</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="NonBillTimeAmt" DataFormatString="{0:#,##0.00}" SortExpression="NonBillTimeAmt" HeaderText="<font color=white>NBT Amount</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="BillExp" DataFormatString="{0:#,##0.00}" SortExpression="BillExp" HeaderText="<font color=white>B-Expense</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="NonBillExp" DataFormatString="{0:#,##0.00}"  SortExpression="NonBillExp" HeaderText="<font color=white>NB-Expense</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="NonAppLeave" SortExpression="NonAppLeave" HeaderText="<font color=white>Approved PL & NPL</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="AppLeave" SortExpression="AppLeave" HeaderText="<font color=white>Open Leave Req.<br/> Hrs., Reviewer</font>"></asp:BoundColumn>
							</Columns>
							</asp:DataGrid>
					    <script language="JavaScript" type="text/javascript" >
				                function Lfprintcheck()
				                {
					                this.print()
					                document.location.href = "frmTimeExpRept.aspx";
				                }
			                </script>
					
    </form>
</body>
</html>
