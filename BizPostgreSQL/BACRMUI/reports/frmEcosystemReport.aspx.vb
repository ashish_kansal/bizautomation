Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmEcosystemReport : Inherits BACRMPage

        Dim dtEcosystem As DataTable
        Dim objPredefinedReports As New PredefinedReports
        Dim SortField As String
       
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                btnSelectAsits.Attributes.Add("onclick", "return OpenAsItsType(26)")
                btnSelectFields.Attributes.Add("onclick", "return OpenFld()")
                If Not IsPostBack Then
                     ' = "Reports"
                    
                    GetUserRightsForPage(8, 57)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    DisplayRecords()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.UserCntID = Session("UserContactID")
                If ddlCompany.SelectedIndex > 0 Then
                    objPredefinedReports.Customer = ddlCompany.SelectedValue
                Else : objPredefinedReports.Customer = 0
                End If

                If ddlCompany.SelectedIndex > 0 Then
                    objPredefinedReports.Division = ddlCompany.SelectedValue
                Else : objPredefinedReports.Division = 0
                End If

                objPredefinedReports.TerritoryID = Session("TerritoryID")

                objPredefinedReports.ReportType = 26
                dtEcosystem = objPredefinedReports.GetEcoSystemReport

                Dim dv As DataView = New DataView(dtEcosystem)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgEcosystemReport.DataSource = dv
                dgEcosystemReport.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEcosystemReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgEcosystemReport.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlAsItsFilter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 34
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                FillCustomer()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Function FillCustomer()
            Try
                
                With objCommon
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .Filter = Trim(txtCompName.Text) & "%"
                    ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    ddlCompany.DataTextField = "vcCompanyname"
                    ddlCompany.DataValueField = "numDivisionID"
                    ddlCompany.DataBind()
                End With
                ddlCompany.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEcosystemReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEcosystemReport.ItemCommand
            Try
                If e.CommandName = "Company1" Then
                    If e.Item.Cells(3).Text = 1 Then

                        Response.Redirect("../Account/frmProspects.aspx?frm=Ecosystem&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(2).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    ElseIf e.Item.Cells(3).Text = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=Ecosystem&CmpID=" & e.Item.Cells(0).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    End If
                End If
                If e.CommandName = "Company2" Then
                    If e.Item.Cells(7).Text = 1 Then
                        Response.Redirect("../Account/frmProspects.aspx?frm=Ecosystem&CmpID=" & e.Item.Cells(4).Text & "&DivID=" & e.Item.Cells(5).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(6).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    ElseIf e.Item.Cells(7).Text = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=Ecosystem&CmpID=" & e.Item.Cells(4).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(5).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(6).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgEcosystemReport, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Server.Transfer("../reports/frmEcosystemReportPrint.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Customer=" & ddlCompany.SelectedValue & "&DivID=" & ddlCompany.SelectedValue & "&sortby=" & SortField)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace