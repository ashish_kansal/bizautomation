<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmTopProducers.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmTopProducers" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Top Producers</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                <label>
                    From
                </label>
                <BizCalendar:Calendar ID="calFrom" runat="server" />
                <label>
                    To
                </label>
                <BizCalendar:Calendar ID="calTo" runat="server" />
                <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
            </div>
                </div>
            <div class="pull-right">
                <div class="form-inline">
                <asp:RadioButtonList ID="rdlReportType" runat="server" AutoPostBack="True" CssClass="normal1"
                    RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
    <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                    <asp:LinkButton ID="btnChooseTeams" runat="server" CssClass="btn btn-primary">Choose Teams</asp:LinkButton>
                             <asp:LinkButton ID="btnChooseTerritories" runat="server" CssClass="btn btn-primary">Choose Territories</asp:LinkButton>
                    <asp:LinkButton ID="btnExport" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                            <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExport" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
            </div>
        </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Top Producers
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:DataGrid ID="dgTopProducers" runat="server" Width="100%" AllowSorting="True"
       AutoGenerateColumns="False" CssClass="table table-responsive table-bordered tblDataGrid">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="Employee" SortExpression="Employee" HeaderText="Employee Name">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="ProspectsOwned" SortExpression="ProspectsOwned" DataFormatString="{0:#,###}"
                HeaderText="Prospects Owned"></asp:BoundColumn>
            <asp:BoundColumn DataField="AccountsOwned" SortExpression="AccountsOwned" DataFormatString="{0:#,###}"
                HeaderText="Accounts Owned"></asp:BoundColumn>
            <asp:BoundColumn DataField="OpportunitiesOpened" SortExpression="OpportunitiesOpened"
                DataFormatString="{0:#,###}" HeaderText="Opportunities Opened"></asp:BoundColumn>
            <asp:BoundColumn DataField="DealsWon" SortExpression="DealsWon" DataFormatString="{0:#,###}"
                HeaderText="Deals Won"></asp:BoundColumn>
            <asp:BoundColumn DataField="DealsLost" SortExpression="DealsLost" DataFormatString="{0:#,###}"
                HeaderText="Deals Lost"></asp:BoundColumn>
            <asp:BoundColumn DataField="PercentageWon" SortExpression="PercentageWon" DataFormatString="{0:f}"
                HeaderText="% Won"></asp:BoundColumn>
            <asp:BoundColumn DataField="AmountWon" SortExpression="AmountWon" DataFormatString="{0:#,###.00}"
                HeaderText="Amount Won"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
        </div>
</asp:Content>
