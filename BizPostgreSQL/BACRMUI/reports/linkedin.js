﻿$(document).ready(function () {
    if ($("#hfEnableLinkedin").val() == 1) {
        $("#btnLinkedinLogin").css('display', 'block');
    } else {
        $("#btnLinkedinLogin").css('display', 'none');
    }
})
function onLinkedInLoad() {
    IN.UI.Authorize().place();
    IN.Event.on(IN, "auth", onLinkedInAuth);
    return false;
}
function onLinkedInAuth() {
    IN.API.Profile("me")
    .fields("id", "firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "email-address", "educations", "date-of-birth")
    .result(displayProfiles)
    .error(displayProfilesErrors);
}

function displayProfiles(profiles) {
    member = profiles.values[0];
    //alert(JSON.stringify(member));
    //alert(member.id);

    //            document.getElementById("lblName").innerHTML = member.firstName + " " + member.lastName + "<br/> Location is " + member.location.name;
    $("#vcFirstName_51R").val(member.firstName);
    $("#vcLastName_52R").val(member.lastName);
    $("#vcEmail_53R").val(member.emailAddress);
    $("#hfLinkedinUrl").val(member.publicProfileUrl);
    $("#hfLinkedinId").val(member.id);
}
function displayProfilesErrors(error) {
    profilesDiv = document.getElementById("profiles");
    profilesDiv.innerHTML = error.message;
    console.log(error);
}