Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmOpportunitySources : Inherits BACRMPage

       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
        Dim intaryRights() As Integer
        Dim objPredefinedReports As New PredefinedReports
        Dim dtOpportunitySources As DataTable
#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then
                    ' = "Reports"

                    GetUserRightsForPage(8, 33)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If

                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    DisplayRecords()
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(13)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(13)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                Dim intPReportsCount As Integer
                'Declare the DataTable Row Object.
                Dim trRow As TableRow
                'Declare The DataTable Cell Objects.
                Dim tcColName As TableCell
                Dim tcColValue As TableCell
                Dim strImage As String

                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))

                'Set the Rights
                'If intaryRights(RIGHTSTYPE.VIEW) = RIGHTSVALUE.ONLY_OWNER Then
                objPredefinedReports.UserCntID = Session("UserContactID")
                'Else
                '   objPredefinedReports.UserID = 0
                'End If
                'If intaryRights(RIGHTSTYPE.VIEW) = RIGHTSVALUE.ONLY_TERRITORY Then
                objPredefinedReports.TerritoryID = Session("UserTerID")
                'Else
                '    objPredefinedReports.TerritoryID = 0
                'End If
                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select

                objPredefinedReports.ReportType = 13
                Dim intRowCount As Integer
                dtOpportunitySources = objPredefinedReports.GetOpportunitySources

                For intRowCount = 1 To tblData.Rows.Count - 1
                    tblData.Rows(intRowCount).Cells.Clear()
                Next
                For intPReportsCount = 0 To dtOpportunitySources.Rows.Count - 1
                    'Get a New Row Object.
                    trRow = New TableRow
                    'Get the New Column Objects and assign the values And Add to row.
                    'Set the image strng for graph.
                    strImage = "<img src='../images/bottom_line.gif' width='" & dtOpportunitySources.Rows(intPReportsCount).Item("Percentage") * 5 & "' height='20' > "

                    ' NAME
                    tcColName = New TableCell
                    tcColName.CssClass = "text"
                    tcColName.Controls.Add(New LiteralControl(dtOpportunitySources.Rows(intPReportsCount).Item("Source")))
                    tcColName.HorizontalAlign = HorizontalAlign.Right
                    'tcColName.Width="40%"
                    tcColName.Height.Point(20)
                    tcColName.VerticalAlign = VerticalAlign.Bottom
                    trRow.Cells.Add(tcColName)

                    ' VALUE
                    tcColValue = New TableCell
                    tcColValue.CssClass = "text"
                    tcColValue.Controls.Add(New LiteralControl(strImage & "&nbsp; " & Format(dtOpportunitySources.Rows(intPReportsCount).Item("Percentage"), "#,###.00") & "%"))
                    tcColValue.HorizontalAlign = HorizontalAlign.Left
                    trRow.Cells.Add(tcColValue)

                    'Add the row to the table
                    tblData.Rows.Add(trRow)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                Dim strPath As String = Server.MapPath("../documents/docs")
                DisplayRecords()
                objPredefinedReports.ExportToExcel(dtOpportunitySources.DataSet, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "OpportunitySources", Session("DateFormat"))
                Dim strref As String = "../Documents/docs/" & Session("UserName") & "_" & Session("UserID") & "_OpportunitySources.csv"

                Response.Write("<script language=javascript>")
                Response.Write("alert('Exported Successfully !')")
                Response.Write("</script>")

                Response.Write("<script language=javascript>")
                Response.Write("window.open('" & strref & "')")
                Response.Write("</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/frmOpportunitySourcesPrint.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&fdt=" & calFrom.SelectedDate & "&tdt=" & DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) & "&ReportType=" & rdlReportType.SelectedIndex, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 27
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace