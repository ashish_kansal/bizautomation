﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports System.Collections.Generic
Imports System.Linq.Expressions
Imports System.Reflection
Imports System.IO
Imports ClosedXML.Excel
Imports HtmlAgilityPack
Imports System.Text.RegularExpressions

Public Class frmCustomReportRun
    Inherits BACRMPage

    Dim objReportManage As New CustomReportsManage
    Dim lngReportID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngReportID = CCommon.ToLong(GetQueryStringVal("ReptID"))

            If Not IsPostBack Then
                If Not Page.Master.FindControl("frmBizSorting2") Is Nothing Then
                    Page.Master.FindControl("frmBizSorting2").Visible = False
                End If
                txtCurrrentPage.Text = 1
                BindReport(1)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Function BindReport(Optional ByVal currentPage As Long = 0, Optional ByVal isFromExport As Boolean = False) As String
        Dim objReportManage As New CustomReportsManage

        Try
            Dim ds As DataSet

            objReportManage.ReportID = lngReportID
            objReportManage.DomainID = Session("DomainID")
            objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objReportManage.UserCntID = Session("UserContactID")

            ds = objReportManage.GetReportListMasterDetail

            Dim html As String = "<table class='table table-responsive table-bordered'>"
            Dim trHeader As String = ""
            Dim trRow As String = ""
            Dim cell As String = ""


            If ds.Tables.Count > 0 Then
                lblReportName.Text = ds.Tables(0).Rows(0)("vcReportName")
                lblReportDesc.Text = ds.Tables(0).Rows(0)("vcReportDescription")
                lblGeneratedOn.Text = FormattedDateFromDate(Now, Session("DateFormat"))

                Dim objReport As ReportObject = objReportManage.GetDatasetToReportObject(ds)

                Dim dsColumnList As DataSet
                objReportManage.IsReportRun = True
                dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

                Try
                    objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, ds, objReport, False, True, dsColumnList, currentPage:=currentPage)
                Catch ex As Exception
                    If ex.Message = "ReportNoColumnFound" Then
                        btnExpToExcel.Visible = False
                        btnAddMyreport.Visible = False
                        html += "<tr><td class='errorInfo'>Error: Can't display the report until you fix the error in Reports.</td></tr>"
                        html += "</table>"
                        divReportData.InnerHtml = html
                        Exit Function
                    End If
                End Try

                objReportManage.CurrentPage = currentPage
                Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute()

                If dtData.Rows.Count > 100 Then
                    bizPager.PageSize = dtData.Rows.Count
                Else
                    bizPager.PageSize = 100
                End If

                bizPager.RecordCount = objReportManage.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                html += objReportManage.GetCustomReportHtml(ds, dsColumnList, dtData, objReport, False)
            End If

            html += "</table>"

            If isFromExport Then
                Return html
            Else
                divReportData.InnerHtml = html
                Return ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(objReportManage.textQuery, Session("DomainID"), Session("UserContactID"), Request)
            Throw
        End Try
    End Function
    Function CalculateAggregateSummary(objReport As ReportObject, queryTotal As IEnumerable(Of System.Data.DataRow)) As String
        Try
            Dim strCellSummary As String = ""

            Dim total As Integer = queryTotal.Count()

            For Each str As AggregateObject In objReport.Aggregate

                Select Case str.aggType
                    Case "sum"
                        If total = 0 Then
                            strCellSummary += "0" & "<br/>"
                        Else
                            strCellSummary += String.Format("{0:#.00}", queryTotal.Sum(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                        End If

                    Case "avg"
                        If total = 0 Then
                            strCellSummary += "0" & "<br/>"
                        Else
                            strCellSummary += String.Format("{0:#.00}", queryTotal.Average(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                        End If

                    Case "max"
                        If total = 0 Then
                            strCellSummary += "0" & "<br/>"
                        Else
                            strCellSummary += String.Format("{0:#.00}", queryTotal.Max(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                        End If

                    Case "min"
                        If total = 0 Then
                            strCellSummary += "0" & "<br/>"
                        Else
                            strCellSummary += String.Format("{0:#.00}", queryTotal.Min(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                        End If

                End Select
            Next

            strCellSummary = strCellSummary & total
            Return strCellSummary
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetPeriodTitle(objReport As ReportObject)
        Try
            Select Case objReport.DateFieldValue
                Case "AllTime" : GetPeriodTitle = "All Time"
                Case "Custom"
                    If objReport.FromDate.Length > 0 AndAlso objReport.ToDate.Length > 0 Then
                        Dim fromDate As DateTime = objReport.FromDate
                        Dim toDate As DateTime = objReport.ToDate
                        GetPeriodTitle = String.Format("{0:MM/dd/YYYY} - {1:MM/dd/YYYY}", objReport.FromDate, objReport.ToDate)
                    End If
                    'Year
                Case "CurYear" : GetPeriodTitle = "Current CY"
                Case "PreYear" : GetPeriodTitle = "Previous CY"
                Case "Pre2Year" : GetPeriodTitle = "Previous 2 CY"
                Case "Ago2Year" : GetPeriodTitle = "2 FY Ago"
                Case "NextYear" : GetPeriodTitle = "Next CY"
                Case "CurPreYear" : GetPeriodTitle = "Current and Previous CY"
                Case "CurPre2Year" : GetPeriodTitle = "Current and Previous 2 CY"
                Case "CurNextYear" : GetPeriodTitle = "Current and Next CY"
                    'Quarter
                Case "CuQur" : GetPeriodTitle = "Current CQ"
                Case "CurNextQur" : GetPeriodTitle = "Current and Next CQ"
                Case "CurPreQur" : GetPeriodTitle = "Current and Previous CQ"
                Case "NextQur" : GetPeriodTitle = "Next CQ"
                Case "PreQur" : GetPeriodTitle = "Previous CQ"
                    'Month
                Case "LastMonth" : GetPeriodTitle = "Last Month"
                Case "ThisMonth" : GetPeriodTitle = "This Month"
                Case "NextMonth" : GetPeriodTitle = "Next Month"
                Case "CurPreMonth" : GetPeriodTitle = "Current and Previous Month"
                Case "CurNextMonth" : GetPeriodTitle = "Current and Next Month"
                    'Week
                Case "LastWeek" : GetPeriodTitle = "Last Week"
                Case "ThisWeek" : GetPeriodTitle = "This Week"
                Case "NextWeek" : GetPeriodTitle = "Next Week"
                    'Day
                Case "Yesterday" : GetPeriodTitle = "Yesterday"
                Case "Today" : GetPeriodTitle = "Today"
                Case "Tomorrow" : GetPeriodTitle = "Tomorrow"
                Case "Last7Day" : GetPeriodTitle = "Last 7 Days"
                Case "Last30Day" : GetPeriodTitle = "Last 30 Days"
                Case "Last60Day" : GetPeriodTitle = "Last 60 Days"
                Case "Last90Day" : GetPeriodTitle = "Last 90 Days"
                Case "Last120Day" : GetPeriodTitle = "Last 120 Days"
                Case "Next7Day" : GetPeriodTitle = "Next 7 Days"
                Case "Next30Day" : GetPeriodTitle = "Next 30 Days"
                Case "Next60Day" : GetPeriodTitle = "Next 60 Days"
                Case "Next90Day" : GetPeriodTitle = "Next 90 Days"
                Case "Next120Day" : GetPeriodTitle = "Next 120 Days"
                    'KPI Period
                Case "Year" : GetPeriodTitle = "This Year vs. Last Year"
                Case "Quarter" : GetPeriodTitle = "This Quarter vs. Last Quarter"
                Case "Month" : GetPeriodTitle = "This Month vs. Last Month"
                Case "Week" : GetPeriodTitle = "This Week vs. Last Week"

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            Dim frm = GetQueryStringVal("frm")
            If frm = "Dashboard" Then
                Response.Redirect("../ReportDashboard/frmNewDashBoard.aspx", False)
            Else : Response.Redirect("../reports/frmCustomReportList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnAddMyreport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMyreport.Click
        Try
            objReportManage.ReportID = lngReportID
            objReportManage.UserCntID = Session("UserContactId")
            objReportManage.DomainID = Session("DomainId")
            objReportManage.tintReportType = 1
            objReportManage.ManageUserReportList()
            BindReport(txtCurrrentPage.Text)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Response.Redirect("../reports/frmReportStep2.aspx?ReptID=" & lngReportID, False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Fix for error when exporting to excel- Control 'dgReport' of type 'GridView' must be placed inside a form tag with runat=server. 
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Private Sub btnExpToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpToExcel.Click
        Try
            Dim objReportManage As New CustomReportsManage
            Dim ds As DataSet

            objReportManage.ReportID = lngReportID
            objReportManage.DomainID = Session("DomainID")
            objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objReportManage.UserCntID = Session("UserContactID")

            ds = objReportManage.GetReportListMasterDetail

            If ds.Tables.Count > 0 Then
                Dim stringWrite As IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As HtmlTextWriter = New HtmlTextWriter(stringWrite)
                Dim reportHtml As String = ""

                If ds.Tables(0).Rows(0)("tintReportType") = 0 Then 'Tabular
                    Dim objReport As ReportObject = objReportManage.GetDatasetToReportObject(ds)

                    Dim dsColumnList As DataSet
                    objReportManage.IsReportRun = True
                    dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

                    Try
                        objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, ds, objReport, False, True, dsColumnList, currentPage:=0)
                        Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute()

                        If Not dtData Is Nothing Then

                            Dim listColumns As New List(Of DataColumn)
                            For Each column As DataColumn In dtData.Columns
                                If objReport.ColumnList.Contains(column.ColumnName) Then
                                    Dim strColumn As String() = column.ColumnName.Split("_")

                                    Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                    If dr.Length > 0 Then
                                        If dtData.Columns.Contains(CCommon.ToString(dr(0)("vcFieldname"))) Then
                                            column.ColumnName = CCommon.ToString(dr(0)("vcFieldname")) & column.Ordinal
                                        Else
                                            column.ColumnName = CCommon.ToString(dr(0)("vcFieldname"))
                                        End If
                                    End If
                                Else
                                    listColumns.Add(column)
                                End If
                            Next

                            For Each column As DataColumn In listColumns
                                dtData.Columns.Remove(column)
                            Next
                        End If

                        Response.Clear()
                        Response.AddHeader("content-disposition", String.Format("attachment;filename={0}_{1}.csv", lblReportName.Text.Trim.Replace(" ", "_"), Format(Now, "MM.dd.yyyy")))
                        Response.Charset = ""
                        Response.ContentType = "text/csv"
                        CSVExport.ProduceCSV(dtData, stringWrite, True)
                        Response.Write(stringWrite.ToString())
                        Response.End()
                    Catch ex As Exception
                        Throw
                    End Try
                Else
                    reportHtml = BindReport(isFromExport:=True)

                    If Not String.IsNullOrWhiteSpace(reportHtml) Then
                        Dim fs As New MemoryStream()
                        Dim workbook As New XLWorkbook
                        Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Report Data")

                        Dim doc As New HtmlAgilityPack.HtmlDocument
                        doc.LoadHtml(reportHtml)

                        Dim i As Int32 = 1
                        Dim j As Int32 = 1
                        Dim nodeCollection As HtmlAgilityPack.HtmlNodeCollection
                        Dim htmlBody = doc.DocumentNode.SelectSingleNode("//table//tr")
                        Dim TotalRecord As HtmlNode = HtmlNode.CreateNode("")

                        If reportHtml.Contains("Total Records") Then
                            Dim nodeTotalRecord As HtmlNode = HtmlNode.CreateNode("<th>Total Records</th>")
                            htmlBody.AppendChild(nodeTotalRecord)
                        End If

                        Dim columnValue As String = ""

                        For Each row As HtmlAgilityPack.HtmlNode In doc.DocumentNode.SelectNodes("//table//tr")
                            Dim isTdcollection As Boolean
                            If Not row.SelectNodes("th") Is Nothing Then
                                nodeCollection = row.SelectNodes("th")
                            Else
                                nodeCollection = row.SelectNodes("td")
                                isTdcollection = True
                            End If

                            If reportHtml.Contains("Total Records") And isTdcollection Then
                                For Each node In nodeCollection
                                    If node.InnerHtml.Contains("Total Records") Then
                                        Dim recordOwnerName As String = node.InnerHtml.Split(":")(1)
                                        recordOwnerName = recordOwnerName.Remove(recordOwnerName.Length - 1, 1)
                                        TotalRecord = HtmlNode.CreateNode(recordOwnerName)
                                    End If
                                Next
                                nodeCollection.Add(TotalRecord)
                            End If

                            For Each col As HtmlAgilityPack.HtmlNode In nodeCollection
                                columnValue = ReplaceHexadecimalSymbols(col.InnerText)

                                If Double.TryParse(columnValue, Nothing) Then
                                    workSheet.Cell(i, j).SetValue(columnValue).SetDataType(XLCellValues.Number)
                                ElseIf IsDate(columnValue) Then
                                    workSheet.Cell(i, j).SetValue(columnValue).SetDataType(XLCellValues.DateTime)
                                Else
                                    workSheet.Cell(i, j).SetValue(columnValue).SetDataType(XLCellValues.Text)
                                    If isTdcollection Then
                                        If columnValue.Contains("Total Records") Then

                                            workSheet.Cell(i, j).SetValue(Regex.Replace(columnValue, "\([^)]*\)", "").Replace("&nbsp;", "").Trim()).SetDataType(XLCellValues.Text)
                                        Else
                                            workSheet.Cell(i, j).SetValue(columnValue).SetDataType(XLCellValues.Text)
                                        End If

                                    End If
                                End If

                                If i = 1 Then
                                    workSheet.Cell(i, j).Style.Font.Bold = True
                                    workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                                End If

                                j = j + 1
                            Next
                            i = i + 1
                            j = 1
                        Next
                        workSheet.Columns.AdjustToContents()

                        Dim httpResponse As HttpResponse = Response
                        httpResponse.Clear()
                        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        httpResponse.AddHeader("content-disposition", String.Format("attachment;filename={0}_{1}.xlsx", lblReportName.Text.Trim.Replace(" ", "_"), Format(Now, "MM.dd.yyyy")))


                        Using MemoryStream As New MemoryStream
                            workbook.SaveAs(MemoryStream)
                            MemoryStream.WriteTo(httpResponse.OutputStream)
                            MemoryStream.Close()
                        End Using

                        httpResponse.Flush()
                        httpResponse.End()
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub

    Private Function ReplaceHexadecimalSymbols(ByVal txt As String) As String
        Try
            Dim r As String = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]"
            Return Regex.Replace(txt, r, "", RegexOptions.Compiled)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindReport(bizPager.CurrentPageIndex)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class