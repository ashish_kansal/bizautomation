Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmTimeExpRept : Inherits BACRMPage

        Dim objPredefinedReports As New PredefinedReports
        Dim SortField As String
       

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then
                    ' = "Reports"

                    GetUserRightsForPage(8, 66)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    DisplayRecords()
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(24)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                If ddlFilter.SelectedIndex = 0 Then
                    txtFromDate.Text = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                    txtToDate.Text = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                    objPredefinedReports.FromDate = txtFromDate.Text
                    objPredefinedReports.ToDate = txtToDate.Text
                Else
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If ddlFilter.SelectedValue = 1 Then
                        txtFromDate.Text = strNow
                        txtToDate.Text = DateAdd(DateInterval.Day, 1, strNow)
                        objPredefinedReports.FromDate = txtFromDate.Text
                        objPredefinedReports.ToDate = txtToDate.Text
                    ElseIf ddlFilter.SelectedValue = 2 Then
                        txtToDate.Text = strNow
                        txtFromDate.Text = DateAdd(DateInterval.Day, -1, strNow)
                        objPredefinedReports.ToDate = txtToDate.Text
                        objPredefinedReports.FromDate = txtFromDate.Text
                    ElseIf ddlFilter.SelectedValue = 3 Then
                        Dim k As Integer
                        k = strNow.DayOfWeek
                        txtFromDate.Text = DateAdd(DateInterval.Day, -(k), strNow)
                        txtToDate.Text = DateAdd(DateInterval.Day, -(k), DateAdd(DateInterval.Day, 7, CDate(txtFromDate.Text)))
                        objPredefinedReports.FromDate = txtFromDate.Text
                        objPredefinedReports.ToDate = txtToDate.Text
                    ElseIf ddlFilter.SelectedValue = 4 Then
                        Dim k As Integer
                        Dim strDate As Date = DateAdd(DateInterval.Day, -7, strNow)
                        k = strDate.DayOfWeek
                        txtFromDate.Text = DateAdd(DateInterval.Day, -(k), strDate)
                        txtToDate.Text = DateAdd(DateInterval.Day, -(k), DateAdd(DateInterval.Day, 7, CDate(txtFromDate.Text)))
                        objPredefinedReports.FromDate = txtFromDate.Text
                        objPredefinedReports.ToDate = txtToDate.Text
                    ElseIf ddlFilter.SelectedValue = 5 Then
                        Dim k As Integer

                        txtFromDate.Text = New Date(strNow.Year, strNow.Month, 1)
                        objPredefinedReports.FromDate = txtFromDate.Text
                        If strNow.Month + 1 > 12 Then
                            txtToDate.Text = New Date(strNow.Year, strNow.Month, 1)
                        Else : txtToDate.Text = New Date(strNow.Year, strNow.Month + 1, 1)
                        End If
                        objPredefinedReports.ToDate = txtToDate.Text
                    ElseIf ddlFilter.SelectedValue = 6 Then
                        Dim k As Integer
                        txtToDate.Text = New Date(strNow.Year, strNow.Month, 1)
                        If strNow.Month - 1 = 0 Then
                            txtFromDate.Text = New Date(strNow.Year - 1, 12, 1)
                        Else : txtFromDate.Text = New Date(strNow.Year, strNow.Month - 1, 1)
                        End If
                        objPredefinedReports.FromDate = txtFromDate.Text
                        objPredefinedReports.ToDate = txtToDate.Text
                    ElseIf ddlFilter.SelectedValue = 7 Then
                        Dim k As Integer
                        txtFromDate.Text = New Date(strNow.Year, 1, 1)
                        txtToDate.Text = New Date(strNow.Year + 1, 1, 1)
                        objPredefinedReports.FromDate = txtFromDate.Text
                        objPredefinedReports.ToDate = txtToDate.Text
                    End If
                End If

                objPredefinedReports.UserCntID = Session("UserContactID")
                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.byteMode = 1
                    Case 1 : objPredefinedReports.byteMode = 2
                End Select
                objPredefinedReports.ReportType = 24
                Dim dv As DataView = New DataView(objPredefinedReports.GetTimeAndExp)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgContactRole.DataSource = dv
                dgContactRole.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgContactRole_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContactRole.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(9).Attributes.Add("onclick", "return OpenLeaveDetails(" & e.Item.Cells(0).Text & ",'" & txtFromDate.Text & "','" & txtToDate.Text & "')")
                    e.Item.Cells(10).Attributes.Add("onclick", "return OpenLeaveDetails(" & e.Item.Cells(0).Text & ",'" & txtFromDate.Text & "','" & txtToDate.Text & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgContactRole_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContactRole.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/frmTimeExpPrint.aspx?fdt=" & calFrom.SelectedDate & "&tdt=" & calTo.SelectedDate & "&ReportType=" & rdlReportType.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 44
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgContactRole, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace