﻿Option Explicit On
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Namespace BACRM.UserInterface.MultiComp
    Partial Public Class InventoryGridReport

        Inherits BACRMPage
        Dim objMultiAccounting As New MultiCompany
        Dim objItem As New CItems

        'Dim DomainID As Integer
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim objRow As DataRow
            Dim objDT As New DataTable
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then

                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.sb_FillComboFromDBwithSel(ddlItemClass, 36, Session("DomainID")) ''tem Classification
                    ddlItemClass.Items(0).Text = "-All Item Class--"
                    ddlItemClass.Items(0).Value = 22


                    objItem = New CItems
                    objItem.DomainID = Session("DomainID")

                    objDT = objItem.GetWareHouses
                    objRow = objDT.NewRow
                    objRow.Item("vcWareHouse") = "-All WareHouse--"
                    objRow.Item("numWareHouseID") = 0
                    objDT.Rows.Add(objRow)
                    objDT.AcceptChanges()

                    ddlWareHouse.DataSource = objDT
                    ddlWareHouse.DataTextField = "vcWareHouse"
                    ddlWareHouse.DataValueField = "numWareHouseID"
                    ddlWareHouse.SelectedIndex = objDT.Rows.Count - 1
                    ddlWareHouse.DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Private Sub FillGrid()
            Try
                Dim dbc As BoundColumn
                Dim dtTB As New DataTable

                dtTB.Columns.Add("numItemCode", Type.GetType("System.String"))
                dtTB.Columns.Add("vcItemName", Type.GetType("System.String"))
                'dtTB.Columns.Add("vcModelID", Type.GetType("System.String"))
                dtTB.Columns.Add("vcSKU", Type.GetType("System.String"))
                dtTB.Columns.Add("ItemClass", Type.GetType("System.String"))
                dtTB.Columns.Add("ItemDesc", Type.GetType("System.String"))
                dtTB.Columns.Add("numOnHand", Type.GetType("System.Double"))
                dtTB.Columns.Add("numAllocation", Type.GetType("System.Double"))
                dtTB.Columns.Add("AVGCOST", Type.GetType("System.Double"))
                dtTB.Columns.Add("TotalInventory", Type.GetType("System.Double"))
                dtTB.Columns.Add("InventoryValue", Type.GetType("System.Double"))

                'dtTB.Columns.Add("numOnOrder", Type.GetType("System.Double"))
                'dtTB.Columns.Add("numBackOrder", Type.GetType("System.Double"))
                'dtTB.Columns.Add("Amount", Type.GetType("System.Double"))
                'dtTB.Columns.Add("SalesUnit", Type.GetType("System.Double"))
                'dtTB.Columns.Add("ReturnPer", Type.GetType("System.Double"))

                dbc = New BoundColumn
                dbc.DataField = "numItemCode"
                'dbc.HeaderText = "<font color=white>" & "Item Id" & "</font>"
                dbc.HeaderText = "Item Id"
                dbc.ItemStyle.Width = 100
                dbc.HeaderStyle.Width = 100
                dbc.SortExpression = "vcItemName"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "vcItemName"
                'dbc.HeaderText = "<font color=white>" & "Item Name" & "</font>"
                dbc.HeaderText = "Item Name"
                dbc.ItemStyle.Width = 100
                dbc.HeaderStyle.Width = 100
                dbc.SortExpression = "vcItemName"
                dgReport.Columns.Add(dbc)

                'dbc = New BoundColumn
                'dbc.DataField = "vcModelID"
                ''dbc.HeaderText = "<font color=white>" & "Model Id" & "</font>"
                'dbc.HeaderText = "Model Id"
                'dbc.ItemStyle.Width = 60
                'dbc.HeaderStyle.Width = 60
                'dbc.SortExpression = "vcModelID"
                'dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "vcSKU"
                dbc.HeaderText = "SKU"
                dbc.ItemStyle.Width = 60
                dbc.HeaderStyle.Width = 60
                dbc.SortExpression = "vcSKU"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "ItemClass"
                dbc.ItemStyle.Width = 60
                dbc.HeaderStyle.Width = 60
                'dbc.HeaderText = "<font color=white>" & "Item Classification" & "</font>"
                dbc.HeaderText = "Item Classification"
                dbc.SortExpression = "ItemClass"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "ItemDesc"
                dbc.ItemStyle.Width = 100
                dbc.HeaderStyle.Width = 100
                'dbc.HeaderText = "<font color=white>" & "Description" & "</font>"
                dbc.HeaderText = "Description"
                dbc.SortExpression = "ItemDesc"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "numOnHand"
                dbc.ItemStyle.Width = 40
                dbc.HeaderStyle.Width = 40
                'dbc.HeaderText = "<font color=white>" & "Qty-on-hand" & "</font>"
                dbc.HeaderText = "Qty-on-hand"
                dbc.SortExpression = "numOnHand"
                dgReport.Columns.Add(dbc)


                dbc = New BoundColumn
                dbc.DataField = "numAllocation"
                dbc.ItemStyle.Width = 40
                dbc.HeaderStyle.Width = 40
                'dbc.HeaderText = "<font color=white>" & "On-allocation" & "</font>"
                dbc.HeaderText = "On-allocation"
                dbc.SortExpression = "numAllocation"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "AVGCOST"
                dbc.ItemStyle.Width = 40
                dbc.HeaderStyle.Width = 40
                'dbc.HeaderText = "<font color=white>" & "Sold Amount" & "</font>"
                dbc.HeaderText = "Avg Cost"
                dbc.SortExpression = "AVGCOST"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "TotalInventory"
                dbc.ItemStyle.Width = 40
                dbc.HeaderStyle.Width = 40
                dbc.HeaderText = "Total Inventory (OnHand + OnAllocation)"
                dbc.SortExpression = "TotalInventory"
                dgReport.Columns.Add(dbc)

                dbc = New BoundColumn
                dbc.DataField = "InventoryValue"
                dbc.ItemStyle.Width = 40
                dbc.HeaderStyle.Width = 40
                dbc.HeaderText = "Inventory Value (Avg Cost * TotalInventory)"
                dbc.SortExpression = "InventoryValue"
                dgReport.Columns.Add(dbc)

                'dbc = New BoundColumn
                'dbc.DataField = "numOnOrder"
                'dbc.ItemStyle.Width = 40
                'dbc.HeaderStyle.Width = 40
                ''dbc.HeaderText = "<font color=white>" & "On-order" & "</font>"
                'dbc.HeaderText = "On-order"
                'dbc.SortExpression = "numOnOrder"
                'dgReport.Columns.Add(dbc)

                'dbc = New BoundColumn
                'dbc.DataField = "numBackOrder"
                'dbc.ItemStyle.Width = 40
                'dbc.HeaderStyle.Width = 40
                ''dbc.HeaderText = "<font color=white>" & "On-back-order" & "</font>"
                'dbc.HeaderText = "On-back-order"
                'dbc.SortExpression = "numBackOrder"
                'dgReport.Columns.Add(dbc)

                'dbc = New BoundColumn
                'dbc.DataField = "Amount"
                'dbc.ItemStyle.Width = 40
                'dbc.HeaderStyle.Width = 40
                ''dbc.HeaderText = "<font color=white>" & "Sold Amount" & "</font>"
                'dbc.HeaderText = "Sold Amount"
                'dbc.SortExpression = "Amount"
                'dgReport.Columns.Add(dbc)

                'dbc = New BoundColumn
                'dbc.DataField = "SalesUnit"
                'dbc.ItemStyle.Width = 40
                'dbc.HeaderStyle.Width = 40
                ''dbc.HeaderText = "<font color=white>" & "Units Sold" & "</font>"
                'dbc.HeaderText = "Units Sold"
                'dbc.SortExpression = "SalesUnit"
                'dgReport.Columns.Add(dbc)

                'dbc = New BoundColumn
                'dbc.DataField = "ReturnPer"
                'dbc.ItemStyle.Width = 40
                'dbc.HeaderStyle.Width = 40
                ''dbc.HeaderText = "<font color=white>" & "Units Returned (%)" & "</font>"
                'dbc.HeaderText = "Units Returned (%)"
                'dbc.SortExpression = "ReturnPer"
                'dgReport.Columns.Add(dbc)


                Dim objDt As New DataTable
                If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
                Dim strDateCondition As String = ""
                Dim intRow As Integer
                Dim objRow As DataRow

                If hfPeriod.Value.Length > 0 Then
                    strDateCondition = " bintModifiedDate between '" & Replace(hfPeriod.Value & "'", "To", "' And '")
                End If

                With objMultiAccounting
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .DateRange = ddlDateRange.SelectedValue
                    .BasedOn = ddlBasedOn.SelectedValue
                    .ItemType = IIf(chkFilter.Checked, ddlItemType.SelectedValue, 21)
                    .WarehouseID = IIf(chkFilter.Checked, ddlWareHouse.SelectedValue, 0)
                    .ItemClass = 22
                    .ItemClassID = IIf(chkFilter.Checked, IIf(ddlItemClass.SelectedValue = "22", 0, ddlItemClass.SelectedValue), 0)
                    .DateCondition = strDateCondition
                    .FromDate = calFrom.SelectedDate
                    .ToDate = calTo.SelectedDate

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = CCommon.ToLong(ConfigurationManager.AppSettings("ReportPageSize"))
                    If .PageSize = 0 OrElse .PageSize < 0 Then .PageSize = 100
                    .TotalRecords = 0

                    objDt = .GetInventoryReport()
                End With

                'Dim ReportTotal As Double = 0
                'For intRow = 0 To objDt.Rows.Count - 1
                '    'objRow = dtTB.NewRow
                '    'objRow.Item("numItemCode") = objDt.Rows(intRow).Item("numItemCode")
                '    'objRow.Item("vcItemName") = objDt.Rows(intRow).Item("vcItemName")
                '    'objRow.Item("vcModelID") = objDt.Rows(intRow).Item("vcModelID")
                '    'objRow.Item("ItemClass") = objDt.Rows(intRow).Item("ItemClass")
                '    'objRow.Item("ItemDesc") = objDt.Rows(intRow).Item("ItemDesc")
                '    'objRow.Item("numOnHand") = objDt.Rows(intRow).Item("numOnHand")
                '    'objRow.Item("numAllocation") = objDt.Rows(intRow).Item("numAllocation")
                '    'objRow.Item("AVGCOST") = objDt.Rows(intRow).Item("AVGCOST")
                '    'objRow.Item("TotalInventory") = objDt.Rows(intRow).Item("TotalInventory")
                '    'objRow.Item("InventoryValue") = objDt.Rows(intRow).Item("InventoryValue")

                '    ''objRow.Item("numOnOrder") = objDt.Rows(intRow).Item("numOnOrder")
                '    ''objRow.Item("numBackOrder") = objDt.Rows(intRow).Item("numBackOrder")
                '    ''objRow.Item("Amount") = objDt.Rows(intRow).Item("Amount")
                '    ''objRow.Item("SalesUnit") = objDt.Rows(intRow).Item("SalesUnit")
                '    ''objRow.Item("ReturnPer") = objDt.Rows(intRow).Item("ReturnPer")

                '    'dtTB.Rows.Add(objRow)
                '    'dtTB.AcceptChanges()

                '    ReportTotal = ReportTotal + CCommon.ToDouble(objDt.Rows(intRow).Item("InventoryValue"))
                'Next

                If objMultiAccounting.TotalRecords = 0 Then
                    lblRecordCount.Text = 0
                Else
                    lblRecordCount.Text = String.Format("{0:#,###}", objMultiAccounting.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblRecordCount.Text / 100
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblRecordCount.Text Mod 100) = 0 Then
                        txtTotalPage.Text = strTotalPage(0)
                    Else
                        txtTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecords.Text = lblRecordCount.Text
                End If

                bizPager.PageSize = CCommon.ToLong(ConfigurationManager.AppSettings("ReportPageSize"))
                If bizPager.PageSize = 0 OrElse bizPager.PageSize < 0 Then bizPager.PageSize = 100
                bizPager.RecordCount = objMultiAccounting.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'lblIVTotal.Text = CCommon.ToDouble(objDt.Compute("SUM(InventoryValue)", ""))
                dgReport.DataSource = objDt
                dgReport.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                FillGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub BindData(ByVal sortExpression As String)
            Try
                Dim dv As New DataView
                dv = dgReport.DataSource
                If sortExpression.Length > 0 Then
                    dv.Sort = sortExpression
                End If
                dgReport.DataSource = dv
                dgReport.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                If ValidateFilter() = False Then
                    Exit Sub
                End If

                FillGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Function ValidateFilter() As Boolean
            Dim bResult As Boolean = True
            Try
                If ddlDateRange.SelectedIndex = 0 AndAlso calFrom.SelectedDate Is Nothing AndAlso calTo.SelectedDate Is Nothing Then
                    litMessage.Text = "Select A Valid Date Range"
                    bResult = False
                End If

                If ddlBasedOn.SelectedIndex = 0 Then
                    litMessage.Text = "Select A Valid Report Based On"
                    bResult = False
                End If

                If calFrom.SelectedDate Is Nothing Then
                    litMessage.Text = "Select From Date"
                    bResult = False
                ElseIf calTo.SelectedDate Is Nothing Then
                    litMessage.Text = "Select To Date"
                    bResult = False
                End If

            Catch ex As Exception
                Throw ex
            End Try
            Return bResult
        End Function

        Private Sub ddlDateRange_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDateRange.SelectedIndexChanged, btnApply.Click
            Try
                Dim dt As New DataTable
                Dim strFinDate As String

                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainID")
                objCOA.Mode = 2
                dt = objCOA.GetFinancialYear().Tables(0)

                If dt.Rows.Count = 0 Then
                    litMessage.Text = "Please set current financial year from Accounting->Finacial Year before you can see the results."
                    Exit Sub
                End If

                Dim strDateFormat As String = Session("DateFormat").ToString().ToLower().Replace("m", "M").Replace("Month", "MMM")
                strFinDate = String.Format(dt.Rows(0).Item("dtPeriodFrom"), strDateFormat)

                If ddlDateRange.SelectedValue = 0 AndAlso calFrom.SelectedDate Is Nothing AndAlso calTo.SelectedDate Is Nothing Then
                    litMessage.Text = "Select A Valid Date Range"
                    Exit Sub

                ElseIf sender.GetType.Name = "Button" AndAlso CCommon.ToString(DirectCast(sender, System.Web.UI.WebControls.Button).ID) = "btnApply" Then
                    If ValidateFilter() = False Then
                        Exit Sub
                    End If

                    txtPeriod.Text = calFrom.SelectedDate & " To " & calTo.SelectedDate
                    hfPeriod.Value = calFrom.SelectedDate & " To " & calTo.SelectedDate
                End If

                If ddlDateRange.SelectedItem.Text = "Last 30 days" Then
                    txtPeriod.Text = Format(DateAdd(DateInterval.Day, -30, Date.Now), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                    hfPeriod.Value = Format(DateAdd(DateInterval.Day, -30, Date.Now), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                    calFrom.SelectedDate = DateAdd(DateInterval.Day, -30, Date.Now)
                    calTo.SelectedDate = Date.Now

                ElseIf ddlDateRange.SelectedItem.Text = "Last 60 days" Then
                    txtPeriod.Text = Format(DateAdd(DateInterval.Day, -60, Date.Now), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                    hfPeriod.Value = Format(DateAdd(DateInterval.Day, -60, Date.Now), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                    calFrom.SelectedDate = DateAdd(DateInterval.Day, -60, Date.Now)
                    calTo.SelectedDate = Date.Now

                ElseIf ddlDateRange.SelectedItem.Text = "Last 90 days" Then
                    txtPeriod.Text = Format(DateAdd(DateInterval.Day, -90, Date.Now), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                    hfPeriod.Value = Format(DateAdd(DateInterval.Day, -90, Date.Now), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                    calFrom.SelectedDate = DateAdd(DateInterval.Day, -90, Date.Now)
                    calTo.SelectedDate = Date.Now

                ElseIf ddlDateRange.SelectedItem.Text = "Month to date" Then
                    txtPeriod.Text = Format(DateAdd(DateInterval.Day, -Day(Date.Now) + 1, Date.Now), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                    hfPeriod.Value = Format(DateAdd(DateInterval.Day, -Day(Date.Now) + 1, Date.Now), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                    calFrom.SelectedDate = DateAdd(DateInterval.Day, -Day(Date.Now) + 1, Date.Now)
                    calTo.SelectedDate = Date.Now

                ElseIf ddlDateRange.SelectedItem.Text = "Quarter to date" Then

                    If Month(Date.Now) <= 3 Then
                        txtPeriod.Text = Format(DateTime.Parse(dt.Rows(0).Item("dtPeriodFrom").ToString()), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                        hfPeriod.Value = Format(DateTime.Parse(dt.Rows(0).Item("dtPeriodFrom").ToString()), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                        calFrom.SelectedDate = DateTime.Parse(dt.Rows(0).Item("dtPeriodFrom").ToString())
                        calTo.SelectedDate = Date.Now

                    ElseIf Month(Date.Now) <= 6 And Month(Date.Now) > 3 Then
                        txtPeriod.Text = Format(DateAdd(DateInterval.Month, 3, dt.Rows(0).Item("dtPeriodFrom")), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                        hfPeriod.Value = Format(DateAdd(DateInterval.Month, 3, dt.Rows(0).Item("dtPeriodFrom")), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                        calFrom.SelectedDate = DateAdd(DateInterval.Month, 3, dt.Rows(0).Item("dtPeriodFrom"))
                        calTo.SelectedDate = Date.Now

                    ElseIf Month(Date.Now) <= 9 And Month(Date.Now) > 6 Then
                        txtPeriod.Text = Format(DateAdd(DateInterval.Month, 6, dt.Rows(0).Item("dtPeriodFrom")), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                        hfPeriod.Value = Format(DateAdd(DateInterval.Month, 6, dt.Rows(0).Item("dtPeriodFrom")), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                        calFrom.SelectedDate = DateAdd(DateInterval.Month, 6, dt.Rows(0).Item("dtPeriodFrom"))
                        calTo.SelectedDate = Date.Now

                    ElseIf Month(Date.Now) <= 12 And Month(Date.Now) > 9 Then
                        txtPeriod.Text = Format(DateAdd(DateInterval.Month, 9, dt.Rows(0).Item("dtPeriodFrom")), strDateFormat) & " To " & Format(Date.Now, strDateFormat)
                        hfPeriod.Value = Format(DateAdd(DateInterval.Month, 9, dt.Rows(0).Item("dtPeriodFrom")), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                        calFrom.SelectedDate = DateAdd(DateInterval.Month, 9, dt.Rows(0).Item("dtPeriodFrom"))
                        calTo.SelectedDate = Date.Now

                    End If
                ElseIf ddlDateRange.SelectedItem.Text = "Year to date" Then
                    txtPeriod.Text = strFinDate & " To " & Format(Date.Now, strDateFormat)
                    hfPeriod.Value = Format(DateTime.Parse(dt.Rows(0).Item("dtPeriodFrom").ToString()), "MM/dd/yyyy") & " To " & Format(Date.Now, "MM/dd/yyyy")

                    calFrom.SelectedDate = strFinDate
                    calTo.SelectedDate = Date.Now

                End If

                btnGo_Click(sender, e)
                Exit Sub
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub dgReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgReport.SortCommand
            Try
                Dim sortExpression As String
                Dim sortDirection As String
                sortExpression = Session("SortExp")
                sortDirection = Session("SortDir")

                If sortExpression <> e.SortExpression Then
                    sortExpression = e.SortExpression
                    sortDirection = "asc"
                Else
                    If (sortDirection = "asc") Then
                        sortDirection = "desc"
                    Else
                        sortDirection = "asc"
                    End If
                End If
                Session("SortExp") = sortExpression
                Session("SortDir") = sortDirection
                BindData(sortExpression + " " + sortDirection)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                FillGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub ibExportExcel_Click(sender As Object, e As System.EventArgs) Handles ibExportExcel.Click

            If ValidateFilter() = False Then
                Exit Sub
            End If

            Dim dtInventory As New DataTable
            If objMultiAccounting Is Nothing Then objMultiAccounting = New MultiCompany
            Dim strDateCondition As String = ""
            Dim intRow As Integer
            Dim objRow As DataRow

            If hfPeriod.Value.Length > 0 Then
                strDateCondition = " bintModifiedDate between '" & Replace(hfPeriod.Value & "'", "To", "' And '")
            End If

            With objMultiAccounting
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .DateRange = ddlDateRange.SelectedValue
                .BasedOn = ddlBasedOn.SelectedValue
                .ItemType = IIf(chkFilter.Checked, ddlItemType.SelectedValue, 21)
                .WarehouseID = IIf(chkFilter.Checked, ddlWareHouse.SelectedValue, 0)
                .ItemClass = 22
                .ItemClassID = IIf(chkFilter.Checked, IIf(ddlItemClass.SelectedValue = "22", 0, ddlItemClass.SelectedValue), 0)
                .DateCondition = strDateCondition
                .FromDate = calFrom.SelectedDate
                .ToDate = calTo.SelectedDate

                dtInventory = .GetInventoryReport()
            End With

            dtInventory.Columns("numItemCode").ColumnName = "Item Id"
            dtInventory.Columns("vcItemName").ColumnName = "Item Name"
            'dtInventory.Columns("vcModelID").ColumnName = "Model Id"
            dtInventory.Columns("vcSKU").ColumnName = "SKU"
            dtInventory.Columns("ItemClass").ColumnName = "Item Classification"
            dtInventory.Columns("ItemDesc").ColumnName = "Description"
            dtInventory.Columns("numOnHand").ColumnName = "Qty On-Hand"
            'dtInventory.Columns("numOnOrder").ColumnName = "On-order"
            dtInventory.Columns("numAllocation").ColumnName = "On-allocation"
            'dtInventory.Columns("numBackOrder").ColumnName = "On-back-order"
            dtInventory.Columns("AVGCOST").ColumnName = "Avg Cost"
            dtInventory.Columns("TotalInventory").ColumnName = "Total Inventory"
            dtInventory.Columns("InventoryValue").ColumnName = "Inventory Value"
            'dtInventory.Columns("Amount").ColumnName = "Sold Amount"
            'dtInventory.Columns("SalesUnit").ColumnName = "Units Sold"
            'dtInventory.Columns("ReturnPer").ColumnName = "Units Returned (%)"

            'dtInventory.Columns.Remove("COGS")
            'dtInventory.Columns.Remove("PROFIT")
            'dtInventory.Columns.Remove("SALESRETURN")
            dtInventory.AcceptChanges()

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=InventoryReport" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".csv")
            Response.Charset = ""
            Response.ContentType = "text/csv"
            Dim stringWrite As New System.IO.StringWriter
            CSVExport.ProduceCSV(dtInventory, stringWrite, True)
            Response.Write(stringWrite.ToString())
            Response.End()
        End Sub
    End Class
End Namespace