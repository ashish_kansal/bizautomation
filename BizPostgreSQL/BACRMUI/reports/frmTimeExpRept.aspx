<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmTimeExpRept.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmTimeExpRept" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Time,Expense & Leave</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenLeaveDetails(a, b, c) {
            window.open("../TimeAndExpense/frmApproveLeave.aspx?UserID=" + a + "&fdt=" + b + "&tdt=" + c, '', 'toolbar=no,titlebar=no,left=150, top=300,width=900,height=500,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
     <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                <label>
                    From
                </label>
                <BizCalendar:Calendar ID="calFrom" runat="server" />
                <label>
                    To
                </label>
                <BizCalendar:Calendar ID="calTo" runat="server" />
                <asp:Button ID="btnSearch" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
            </div>
                </div>
            <div class="pull-right">
                <div class="form-inline">
                <asp:RadioButtonList ID="rdlReportType" runat="server" AutoPostBack="True" CssClass="normal1"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                </asp:RadioButtonList>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label ID="lblFilter" runat="server">
                        Filter
                    </label>
                    <asp:DropDownList ID="ddlFilter" CssClass="form-control" runat="server" AutoPostBack="True">
                                <asp:ListItem>-- Select One --</asp:ListItem>
                                <asp:ListItem Value="1">Today</asp:ListItem>
                                <asp:ListItem Value="2">Yesterday</asp:ListItem>
                                <asp:ListItem Value="3">This Week</asp:ListItem>
                                <asp:ListItem Value="4">Last Week</asp:ListItem>
                                <asp:ListItem Value="5">This Month</asp:ListItem>
                                <asp:ListItem Value="6">Last Month</asp:ListItem>
                                <asp:ListItem Value="7">This Year</asp:ListItem>
                            </asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                    <asp:LinkButton ID="btnChooseTeams" runat="server" CssClass="btn btn-primary">Choose Teams</asp:LinkButton>
                    <asp:LinkButton ID="btnExportToExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Time & Expense
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:DataGrid ID="dgContactRole" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" runat="server" BorderColor="white"
        AutoGenerateColumns="False" AllowSorting="True">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="numUserID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Employee"></asp:BoundColumn>
            <asp:BoundColumn DataField="Position" SortExpression="Position" HeaderText="Position"></asp:BoundColumn>
            <asp:BoundColumn DataField="BillTime" SortExpression="BillTime" HeaderText="B-Time"></asp:BoundColumn>
            <asp:BoundColumn DataField="BillTimeAmt" DataFormatString="{0:#,##0.00}" SortExpression="BillTimeAmt"
                HeaderText="BT Amount"></asp:BoundColumn>
            <asp:BoundColumn DataField="NonBillTime" SortExpression="NonBillTime" HeaderText="NB-Time"></asp:BoundColumn>
            <asp:BoundColumn DataField="NonBillTimeAmt" DataFormatString="{0:#,##0.00}" SortExpression="NonBillTimeAmt"
                HeaderText="NBT Amount"></asp:BoundColumn>
            <asp:BoundColumn DataField="BillExp" DataFormatString="{0:#,##0.00}" SortExpression="BillExp"
                HeaderText="B-Expense"></asp:BoundColumn>
            <asp:BoundColumn DataField="NonBillExp" DataFormatString="{0:#,##0.00}" SortExpression="NonBillExp"
                HeaderText="NB-Expense"></asp:BoundColumn>
            <asp:ButtonColumn DataTextField="NonAppLeave" SortExpression="NonAppLeave" HeaderText="Approved PL & NPL"></asp:ButtonColumn>
            <asp:ButtonColumn DataTextField="AppLeave" SortExpression="AppLeave" HeaderText="Open Leave Req.<br/> Hrs., Reviewer"></asp:ButtonColumn>
        </Columns>
    </asp:DataGrid>
    <asp:TextBox ID="txtFromDate" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtToDate" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo" runat="server" Style="display: none"></asp:Button>
</asp:Content>
