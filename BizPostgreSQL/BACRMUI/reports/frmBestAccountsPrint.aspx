<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBestAccountsPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmBestAccountsPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>frmBestAccountsPrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	
		
		<SCRIPT language="JavaScript" type="text/javascript" src="../javascript/date-picker.js">
		</SCRIPT>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Best Accounts Report</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:DataGrid ID="dgBestAccounts" CssClass="dg" Width="100%" CellPadding="0" CellSpacing="0" Runat="server"
				AutoGenerateColumns="false" AllowSorting="True">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="ClosedDealsAmount" SortExpression="ClosedDealsAmount" ItemStyle-HorizontalAlign="Left"
						HeaderText="<font color=white>Closed Deals(Amount)</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="DealsClosed" SortExpression="DealsClosed" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Deals Closed</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="OpportunitiesInPipelineAmount" SortExpression="OpportunitiesInPipelineAmount"
						ItemStyle-HorizontalAlign="Center" HeaderText="<font color=white>Opportunities in Pipeline (Amount)</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="OpportunitiesInPipeline" SortExpression="OpportunitiesInPipeline" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Opportunities in the Pipeline</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="CustomerDivision" SortExpression="CustomerDivision" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Customer-Division</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Industry" SortExpression="Industry" ItemStyle-HorizontalAlign="Left"
						HeaderText="<font color=white>Industr</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="AccountOwner" SortExpression="AccountOwner" ItemStyle-HorizontalAlign="Center"
						HeaderText="<font color=white>Account Owner</font>"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<script language="JavaScript" type="text/javascript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmBestAccounts.aspx";
			}
			</script>
			
		</form>
	</body>
</HTML>
