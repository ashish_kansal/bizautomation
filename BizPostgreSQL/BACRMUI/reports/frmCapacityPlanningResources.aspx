﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmCapacityPlanningResources.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCapacityPlanningResources" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-header > .box-tools {
            position: absolute;
            right: 0px;
            top: 3px;
        }

        .manage-wip-date-range li {
            background-color: #dae3f3;
            color: #000;
            font-weight: bold;
        }

        .manage-wip-date-range li a {
            padding: 7px 15px;
            border-top: 0px;
        }

        .table-calendar, .table-calendar th, .table-calendar td {
            border-color: #ababab !important;
            border-width: 1px !important;
            border-style: solid !important;
        }

        .table-calendar th {
            background: #fafafa !important;
            text-align: center;
            padding: 2px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        $(document).ready(function () {
            $.when(BindTeams(), BindEmpoyees(), BindProcess()).then(function () {
                BindReportData();
            });

            $("#ddlProcess").change(function () {
                $("#ddlMilestone").find('option').not(':first').remove();
                $("#ddlStage").find('option').not(':first').remove();
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindMilestone()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlMilestone").change(function () {
                $("#ddlStage").find('option').not(':first').remove();
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindStages()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlStage").change(function () {
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindTasks()).then(function () {
                        BindReportData();
                    });
                }
            });
        });

        function ProcessTypeChanged() {
            ClearControls();

            $.when(BindProcess()).then(function () {
                if (parseInt($("input[name='ProcessType']:checked").val()) === 0) {
                    $("#liProcess").hide();
                    $("#liMilestone").hide();
                    $("#liStage").hide();
                    $("#liTask").hide();
                } else {
                    $("#liProcess").show();
                    $("#liMilestone").show();
                    $("#liStage").show();
                    $("#liTask").show();
                }

                BindReportData();
            });
        }

        function ClearControls() {
            $("#ddlProcess").find('option').not(':first').remove();
            $("#ddlMilestone").find('option').not(':first').remove();
            $("#ddlStage").find('option').not(':first').remove();
            $("#btnDropDownTask").html("All" + " <span class='caret'></span>");
            $("#ulTaskOptions").html("");
  
            $("#divMain").html("");
        }

        function BindTeams() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetListDetails',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "listID": 35                            
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetListDetailsResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ulTeamOptions").append("<li><input type='checkbox' onchange='TeamSelectionChanged();' id='chkTeam" + value.numListItemID.toString() + "' /><label>" + replaceNull(value.vcData) + "</label></li>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading teams.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading teams: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading teams");
                    }
                }
            });
        }

        function TeamSelectionChanged() {
            try {
                var selected = 0;
                var arrTeams = [];

                $("#ulTeamOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        arrTeams.push($(this).attr("id").replace("chkTeam", ""));
                        selected++;
                    }
                });

                $("#btnDropDownTeam").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                $("#btnDropDownEmployee").html("All" + " <span class='caret'></span>");
                $("#ulEmployeeOptions input[type='checkbox']").each(function () {
                    $(this).is(":checked").prop("checked", "false");
                    
                    if (jQuery.inArray($(this).closest("li").attr("team"), arrTeams) === -1) {
                        $(this).closest("li").hide();
                    } else {
                        $(this).closest("li").show();
                    }
                });

                $("#divMain").html("");
                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetSelectedTeamValues() {
            try {
                var selectedValues = "";

                $("#ulTeamOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkTeam", "");
                    }
                });

                return selectedValues;
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function BindEmpoyees() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetEmployees',
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetEmployeesResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ulEmployeeOptions").append("<li team='" + value.numTeam.toString() + "'><input type='checkbox' onchange='EmployeeSelectionChanged();' id='chkEmployee" + value.numContactID.toString() + "' /><label>" + replaceNull(value.vcUserName) + "</label></li>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading employees.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading employees: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading employees");
                    }
                }
            });
        }

        function EmployeeSelectionChanged() {
            try {
                var selected = 0;

                $("#ulEmployeeOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selected++;
                    }
                });

                $("#btnDropDownEmployee").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                $("#divMain").html("");
                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetSelectedEmployeeValues() {
            try {
                var selectedValues = "";

                $("#ulEmployeeOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkEmployee", "");
                    }
                });

                return selectedValues;
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function BindProcess() {
            $("#ddlProcess").find('option').not(':first').remove();

            return $.ajax({
                type: "POST",
                url: '../admin/frmAdminBusinessProcess.aspx/WebMethodGetProcessList',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": "<%# Session("DomainID")%>"
                    , "Mode": parseInt($("input[name='ProcessType']:checked").val())
                }),
                success: function (data) {
                    try {
                        var Jresponse = $.parseJSON(data.d);

                        $.each(Jresponse, function (index, value) {
                            $("#ddlProcess").append("<option value=" + value.Slp_Id + ">" + value.Slp_Name + "</option>");
                        });
                    } catch (err) {
                        alert("Unknown error occurred while loading process.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading process: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading process");
                    }
                }
            });
        }
            
        function BindMilestone() {
            try {
                $("#ddlMilestone").find('option').not(':first').remove();

                if (parseInt($("#ddlProcess").val()) > 0) {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsService.svc/GetMilestonesByProcess',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "processID": parseInt($("#ddlProcess").val())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetMilestonesByProcessResult);
                                if (obj != null && obj.length > 0) {
                                    $.each(obj, function (index, value) {
                                        $("#ddlMilestone").append("<option value=" + (index + 1) + ">" + value.vcMileStoneName + "</option>");
                                    });
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading milestones.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading milestones.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading milestones.");
            }
        }

        function BindStages() {
            try {
                $("#ddlStage").find('option').not(':first').remove();

                if (parseInt($("#ddlMilestone").val()) > 0) {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsService.svc/GetStagesByProcess',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "processID": parseInt($("#ddlProcess").val())
                            , "milestoneName": replaceNull($("#ddlMilestone option:selected").text())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetStagesByProcessResult);
                                if (obj != null && obj.length > 0) {
                                    $.each(obj, function (index, value) {
                                        $("#ddlStage").append("<option value=" + value.numStageDetailsId + ">" + value.vcStageName + "</option>");
                                    });
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading milestones.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading milestones.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading milestones.");
            }
        }

        function BindTasks() {
            try {
                $("#ulTaskOptions").html("");

                if (parseInt($("#ddlStage").val()) > 0) {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsTaskService.svc/GetTasksByStage',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "stageDetailsID": parseInt($("#ddlStage").val())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetTasksByStageResult);
                                if (obj != null && obj.length > 0) {
                                    $.each(obj, function (index, value) {
                                        $("#ulTaskOptions").append("<li><input type='checkbox' onchange='TaskSelectionChanged();' id='chkTask" + value.numTaskId.toString() + "' /><label>" + replaceNull(value.vcTaskName) + "</label></li>");
                                    });
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading tasks.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading tasks.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading tasks.");
            }
        }

        function TaskSelectionChanged() {
            try {
                var selected = 0;

                $("#ulTaskOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selected++;
                    }
                });

                $("#btnDropDownTask").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                $("#divMain").html("");
                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetSelectedTaskValues() {
            try {
                var selectedValues = "";

                $("#ulTaskOptions input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkTask", "");
                    }
                });

                return selectedValues;
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function BindReportData() {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                if (datepicker.get_selectedDate() != null) {
                    var view = parseInt($("#hdnDateRange").val());

                    var todayDate = new Date(datepicker.get_selectedDate());
                    var fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 0, 0, 0, 0);
                    var toDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 23, 59, 59, 999);

                    if (view === 2) {
                        var weekStart = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay()));
                        var weekEnd = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay() + 6));

                        fromDate = new Date(weekStart.getFullYear(), weekStart.getMonth(), weekStart.getDate(), 0, 0, 0, 0);
                        toDate = new Date(weekEnd.getFullYear(), weekEnd.getMonth(), weekEnd.getDate(), 23, 59, 59, 999);
                    } else if (view === 3) {
                        fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1, 0, 0, 0, 0);
                        toDate = new Date(todayDate.getFullYear(), todayDate.getMonth() + 1, 0, 23, 59, 59, 999);
                    }

                    $.ajax({
                        type: "POST",
                        url: '../WebServices/StagePercentageDetailsService.svc/GetCapacityPlanningResourceReportData',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "teams": GetSelectedTeamValues()
                            , "employees": GetSelectedEmployeeValues()
                            , "processType": parseInt($("input[name='ProcessType']:checked").val())
                            , "processID": parseInt($("#ddlProcess").val())
                            , "milestoneName": (parseInt($("#ddlMilestone").val()) > 0 ? replaceNull($("#ddlMilestone option:selected").text()) : "")
                            , "stageDetailsID": parseInt($("#ddlStage").val())
                            , "taskIds": GetSelectedTaskValues()
                            , "fromDate": "\/Date(" + fromDate.getTime() + ")\/"
                            , "toDate": "\/Date(" + toDate.getTime() + ")\/"
                            , "view": view
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetCapacityPlanningResourceReportDataResult);

                                if (obj != null) {
                                    if (view === 1) {
                                        DisplayDayData(obj)
                                    } else if (view === 2) {
                                        DisplayWeekData(obj)
                                    } else if (view === 3) {
                                        DisplayMonthData(obj)
                                    }
                                }
                            } catch (err) {
                                alert("Unknown error occurred while loading report data.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred while loading report data.");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error ocurred while loading report data.");
            }
        }

        function DisplayDayData(obj) {
            try {
                var teams = $.parseJSON(obj.Teams);
                var employees = $.parseJSON(obj.Employees);
                var reportData = $.parseJSON(obj.ReportData);

                var html = "";
                html += "<table class='table table-bordered'>";
                html += "<thead>";
                html += "<tr>";
                html += "<th>Source</th>";
                html += "<th style='width:100px;'>Date</th>";
                html += "<th style='width:200px;'>Type</th>";
                html += "<th style='width:100px;'>Hours</th>";
                html += "<th>Details</th>";
                html += "</tr>";
                html += "</thead>";
                html += "<tbody>";
                if (teams != null && teams.length > 0) {
                    html += LoadTeams(1, null, null, teams, employees, reportData);
                }
                html += "</tbody>";
                html += "</table>";

                $("#divMain").html(html);
            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function DisplayWeekData(obj) {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = new Date(datepicker.get_selectedDate());

                var weekStart = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay()));
                var weekEnd = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay() + 6));

                var tempWeekStart = new Date(weekStart);
                var tempWeekEnd = new Date(weekEnd);

                var html = "<table class='table table-calendar'><thead><tr><th></th>";

                while (weekStart <= weekEnd) {
                    html += ("<th>" + weekStart.getDate() + " " + weekStart.getMonthNameShort() + "</th>");
                    weekStart = new Date(weekStart.setDate(weekStart.getDate() + 1));
                }
                html += "</tr>";
                html += "</thead>";

                var teams = $.parseJSON(obj.Teams);
                var employees = $.parseJSON(obj.Employees);
                var reportData = $.parseJSON(obj.ReportData);

                if (teams != null && teams.length > 0) {
                    html += LoadTeams(2, tempWeekStart, tempWeekEnd, teams, employees, reportData);
                }


                html += "</table>";

                $("#divMain").html(html);
            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function DisplayMonthData(obj) {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = datepicker.get_selectedDate();

                var monthStart = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1, 0, 0, 0, 0);
                var monthEnd = new Date(todayDate.getFullYear(), todayDate.getMonth() + 1, 0, 0, 0, 0, 0);

                var tempMonthStart = new Date(monthStart);
                var tempMonthEnd = new Date(monthEnd);

                var diffDate = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
                var days = Math.round(diffDate);

                var html = "<table class='table table-calendar'><thead><tr><th colspan='" + (days + 2) + "'>" + monthStart.getMonthNameShort() + " " + monthStart.getFullYear() + "</th><tr><th></th>";

                while (monthStart <= monthEnd) {
                    html += ("<th>" + monthStart.getDate() + "</th>");
                    monthStart = new Date(monthStart.setDate(monthStart.getDate() + 1));
                }
                html += "</tr>";
                html += "</thead>";

                var teams = $.parseJSON(obj.Teams);
                var employees = $.parseJSON(obj.Employees);
                var reportData = $.parseJSON(obj.ReportData);

                if (teams != null && teams.length > 0) {
                    html += LoadTeams(3, tempMonthStart, tempMonthEnd, teams, employees, reportData);
                }

                html += "</table>";

                $("#divMain").html(html);


            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function LoadTeams(view, startDate, endDate, teams, employees, reportData) {
            var html = "";
            var colspan = 0;

            if (view === 1) {
                colspan = 5;
            } else if (view === 2) {
                colspan = 8;
            } else if (view === 3) {
                var diffDate = (endDate - startDate) / (1000 * 60 * 60 * 24);
                var days = Math.round(diffDate);
                colspan = days + 2;
            }

            $.each(teams, function (index, e) {
                html += "<tr class='teamParent" + index + "'><td style='font-weight:bold;padding:3px;' colspan='" + colspan + "'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"teamChild" + index + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a>  " + replaceNull(e.vcData) + "</td></tr>";
                html += LoadEmployees(index, view, startDate, endDate, (e.numListItemID || 0), employees, reportData);
            });

            return html;
        }

        function LoadEmployees(teamIndex, view, startDate, endDate, teamID, employees, reportData) {
            var html = "";

            if (view === 1) {
                if (employees != null && employees.length > 0) {
                    $.each(employees, function (index, e) {
                        if ((e.numTeam || 0) === teamID) {
                            html += "<tr class='teamChild" + teamIndex + " employeeParent" + e.numContactId + "'><td colspan='5' style='padding-left:40px;font-weight:bold;padding-top:3px;padding-bottom:3px;'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"employeeChild" + e.numContactId + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a> " + replaceNull(e.vcContactName) + "</td></tr>";

                            if (reportData != null && reportData.length > 0) {
                                var obj = reportData.filter(function (result) {
                                    return result.numUserCntID === e.numContactId;
                                });

                                if (obj.length > 0) {
                                    obj.forEach(function (o) {
                                        html += "<tr class='teamChild" + teamIndex + " employeeChild" + e.numContactId + "'>";
                                        html += "<td>" + replaceNull(o.vcSource) + "</td>";
                                        html += "<td style='text-align:center;'>" + replaceNull(o.vcDate) + "</td>";
                                        html += "<td>" + replaceNull(o.vcType) + "</td>";
                                        html += "<td style='text-align:right;'>" + replaceNull(o.vcHours) + "</td>";
                                        html += "<td>" + replaceNull(o.vcDetails) + "</td>";
                                        html += "</tr>";
                                    });
                                }
                            }
                        }
                    });
                }
            } else {
                if (employees != null && employees.length > 0) {
                    $.each(employees, function (index, e) {
                        if ((e.numTeam || 0) === teamID) {
                            var tempWeekStart = new Date(startDate);
                            var tempWeekEnd = new Date(endDate);
                            if (view === 2) {
                                html += "<tr class='teamChild" + teamIndex + "'><td style='padding-left:40px;height:106px; white-space:nowrap;'>" + replaceNull(e.vcContactName) + "</td>";
                            } else if (view === 3) {
                                html += "<tr class='teamChild" + teamIndex + "'><td style='padding-left:40px;white-space:nowrap;'>" + replaceNull(e.vcContactName) + "</td>";
                            }

                            while (tempWeekStart <= tempWeekEnd) {
                                var obj = reportData.filter(function (result) {
                                    return result.numUserCntID === e.numContactId && (result.numTotalMinutes || 0) > 0 && new Date(result.dtDate).toLocaleDateString() === new Date(tempWeekStart.getFullYear(), tempWeekStart.getMonth(), tempWeekStart.getDate(), 0, 0, 0, 0).toLocaleDateString();
                                });

                                if (view === 2) {
                                    if (obj.length > 0) {
                                        var capacityLoad = parseInt(obj[0].numCapacityLoad || 0);
                                        var backgroundColor = "";

                                        if (capacityLoad <= 50) {
                                            backgroundColor = "background-color:#ccfcd9";
                                        } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                            backgroundColor = "background-color:#ffffcd";
                                        } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                            backgroundColor = "background-color:#ffdc7d";
                                        } else if (capacityLoad > 100) {
                                            backgroundColor = "background-color:#ffd1d1";
                                        }


                                        html += ("<td style='height:106px; width:200px;" + backgroundColor + "'  assigneeCapacityLoad='" + (obj[0].numCapacityLoad || 0) + "' gradeCapacityLoad='" + (obj[0].numCapacityLoad || 0) + "'>");
                                        html += ("<b>Total Hours: </b><a style='color:#004eff;text-decoration:underline;' href='javascript:OpenDetail(\"" + tempWeekStart.toLocaleDateString() + "\");'>" + (replaceNull(obj[0].vcTotalHours).endsWith(":00") ? parseInt(replaceNull(obj[0].vcTotalHours).replace(":00", "")) : replaceNull(obj[0].vcTotalHours)) + "</a><br/>");
                                        html += "(<b>Max:</b> " + (replaceNull(obj[0].vcMaxHours).endsWith(":00") ? parseInt(replaceNull(obj[0].vcMaxHours).replace(":00", "")) : replaceNull(obj[0].vcMaxHours)) + ")<br/>";
                                        if (parseInt($("#ddlProcess").val()) === 3 || parseInt($("#ddlProcess").val()) === 0) {
                                            html += ("Build Tasks: " + (obj[0].numTotalBuildTasks || 0) + "<br/>");
                                        }
                                        if (parseInt($("#ddlProcess").val()) === 1 || parseInt($("#ddlProcess").val()) === 0) {
                                            html += ("Project Tasks: " + (obj[0].numTotalProjectTasks || 0) + "<br/>");
                                        }
                                        html += ("Capacity Load: " + (obj[0].numCapacityLoad || 0) + "%");
                                        html += "</td>";
                                    } else {
                                        html += ("<td style='height:106px; width:200px;'></td>");
                                    }
                                } else if (view === 3) {
                                    if (obj.length > 0) {
                                        var capacityLoad = parseInt(obj[0].numCapacityLoad || 0);
                                        var backgroundColor = "";

                                        if (capacityLoad <= 50) {
                                            backgroundColor = "background-color:#ccfcd9";
                                        } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                            backgroundColor = "background-color:#ffffcd";
                                        } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                            backgroundColor = "background-color:#ffdc7d";
                                        } else if (capacityLoad > 100) {
                                            backgroundColor = "background-color:#ffd1d1";
                                        }


                                        html += ("<td style='text-align:center;" + backgroundColor + "' assigneeCapacityLoad='" + (obj[0].numCapacityLoad || 0) + "' gradeCapacityLoad='" + (obj[0].numCapacityLoad || 0) + "'>");
                                        html += "<a style='color:#004eff;text-decoration:underline;' href='javascript:OpenDetail(\"" + tempWeekStart.toLocaleDateString() + "\");'>" + (replaceNull(obj[0].vcTotalHours).endsWith(":00") ? parseInt(replaceNull(obj[0].vcTotalHours).replace(":00", "")) : replaceNull(obj[0].vcTotalHours)) + "<a>";
                                        html += "</td>";
                                    } else {
                                        html += ("<td></td>");
                                    }
                                }

                                tempWeekStart = new Date(tempWeekStart.setDate(tempWeekStart.getDate() + 1));
                            }

                            html += "</tr>";
                        }
                    });
                }
            }

            return html;
        }

        function dateSelected(sender, eventArgs) {
            BindReportData();
        }

        function DateRangeChanged(dateRange) {
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");
            $("[id$=hdnDateRange]").val(dateRange);
            BindReportData();

            return true;
        }

        function ExpandCollapse(cssClass, a) {
            if ($(a).html() === "<i class=\"fa fa-chevron-up\"></i>") {
                $(a).html("<i class=\"fa fa-chevron-right\"></i>");
                $("tr." + cssClass).hide();
            } else {
                $(a).html("<i class=\"fa fa-chevron-up\"></i>");
                $("tr." + cssClass).each(function () {
                    if ($(this).is('[class*=" stageParent"]')) {
                        $(this).find("a.aExpandCollapse").html("<i class=\"fa fa-chevron-up\"></i>");
                    }
                });
                $("tr." + cssClass).show();
            }
        }

        function OpenDetail(date) {
            var datepicker = $find("<%= rdpDate.ClientID%>");
            datepicker.set_selectedDate(new Date(date));
            DateRangeChanged(1);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-inline">
                    <li>
                        <div class="form-inline">
                            <label>Team</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownTeam" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulTeamOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Employee</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownEmployee" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulEmployeeOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <input type="radio" name="ProcessType" value="0" checked="checked" onchange="ProcessTypeChanged();" />
                        <b>All</b>
                        <input type="radio" name="ProcessType" value="3" onchange="ProcessTypeChanged();" />
                        <b>Build</b>
                        <input type="radio" name="ProcessType" value="1" onchange="ProcessTypeChanged();" />
                        <b>Project</b>
                    </li>
                    <li id="liProcess" style="display:none">
                        <div class="form-inline">
                            <label>Process</label>
                            <select class="form-control" id="ddlProcess">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li id="liMilestone" style="display:none">
                        <div class="form-inline">
                            <label>Milestone</label>
                            <select class="form-control" id="ddlMilestone">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li id="liStage" style="display:none">
                        <div class="form-inline">
                            <label>Stage</label>
                            <select class="form-control" id="ddlStage">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li id="liTask" style="display:none">
                        <div class="form-inline">
                            <label>Task</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownTask" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All
                              <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulTaskOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <label>Base Cell Color on</label>
                    <select class="form-control" id="ddlCellColor">
                        <option value="1" selected="selected">Assignee Capacity Load</option>
                        <option value="2">Grade Capacity Load</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Capacity Planning (Resources)&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <ul class="list-inline" style="margin-bottom: 0px;">
        <li style="vertical-align: top;">
            <telerik:RadDatePicker ID="rdpDate" runat="server" DateInput-CssClass="form-control" Width="100" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png">
                <ClientEvents OnDateSelected="dateSelected" />
            </telerik:RadDatePicker>
        </li>
        <li>
            <ul class="nav nav-pills manage-wip-date-range">
                <li id="liDateRange1" role="presentation"><a href="javascript:DateRangeChanged(1);">Day</a></li>
                <li id="liDateRange2" role="presentation" class="active"><a href="javascript:DateRangeChanged(2);">Week</a></li>
                <li id="liDateRange3" role="presentation"><a href="javascript:DateRangeChanged(3);">Month</a></li>
            </ul>
        </li>
        <li style="vertical-align: top;">
            <button class="btn btn-flat btn-primary" onclick="return LoadPrevious();">Previous</button>
            <button class="btn btn-flat btn-primary" onclick="return LoadNext();">Next</button>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive" id="divMain">

            </div>
        </div>
    </div>

    <input type="hidden" id="hdnDateRange" value="2" />
</asp:Content>
