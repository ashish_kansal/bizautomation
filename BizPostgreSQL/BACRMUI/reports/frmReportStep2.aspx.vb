﻿Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Common

Public Class frmReportStep2
    Inherits BACRMPage

    Dim objReportManage As New CustomReportsManage
    Dim lngReportID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngReportID = CCommon.ToLong(GetQueryStringVal("ReptID"))

            If Not IsPostBack Then
                hfReportID.Value = lngReportID
                bindReportModuleGroupField()
                hfDateFormat.Value = CCommon.GetDateFormat()
                hfValidDateFormat.Value = CCommon.GetValidationDateFormat()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindReportModuleGroupField()
        Try
            objReportManage = New CustomReportsManage
            objReportManage.ReportID = lngReportID
            objReportManage.DomainID = Session("DomainID")

            Dim ds As DataSet
            ds = objReportManage.GetReportModuleGroupFieldMaster
            'Dim dv As New DataView(ds.Tables(0))
            'dv.Sort = "vcFieldGroupName, vcFieldName"
            If ds.Tables.Count > 0 Then
                Dim sb As New System.Text.StringBuilder
                Dim iRowIndex As Integer = 0

                sb.Append(vbCrLf & "var arrReportColumnList = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

                For Each dr As DataRow In ds.Tables(0).Rows
                    sb.Append("arrReportColumnList[" & iRowIndex & "] = new ReportColumnList('" & dr("numReportFieldGroupID").ToString & "','" _
                                                                                                & dr("vcFieldGroupName").ToString & "','" _
                                                                                                & dr("numReportFieldGroupID").ToString & "_" & dr("numFieldID").ToString & "_" & dr("bitCustom").ToString() & "','" _
                                                                                                & dr("vcFieldName").ToString() & "','" _
                                                                                                & dr("vcDbColumnName").ToString() & "','" _
                                                                                                & dr("vcFieldDataType").ToString() & "','" _
                                                                                                & dr("vcAssociatedControlType").ToString() & "','" _
                                                                                                & dr("vcListItemType").ToString() & "','" _
                                                                                                & dr("numListID").ToString() & "','" _
                                                                                                & dr("bitCustom").ToString() & "','" _
                                                                                                & dr("bitAllowSorting").ToString() & "','" _
                                                                                                & dr("bitAllowGrouping").ToString() & "','" _
                                                                                                & dr("bitAllowAggregate").ToString() & "','" _
                                                                                                & dr("bitAllowFiltering").ToString() & "');" & vbCrLf)
                    iRowIndex += 1
                Next

                ClientScript.RegisterClientScriptBlock(Me.GetType, "ReportModuleGroupFieldListScript", sb.ToString, True)

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class