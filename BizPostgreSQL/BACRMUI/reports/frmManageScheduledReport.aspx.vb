﻿Imports BACRM.BusinessLogic.Common

Public Class frmManageScheduledReport
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnSRGID.Value = CCommon.ToLong(GetQueryStringVal("srgID"))
                rdpStartDate.Calendar.FocusedDate = DateTime.UtcNow.AddMinutes(Convert.ToInt32(Session("ClientMachineUTCTimeOffset")))
                LoadDropdowns()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#Region "Private Methods"

    Private Sub LoadDropdowns()
        Try
            Dim objDashboardTemplate As New BACRM.BusinessLogic.CustomReports.DashboardTemplate
            objDashboardTemplate.DomainID = CCommon.ToLong(Session("DomainID"))
            objDashboardTemplate.UserCntID = CCommon.ToLong(Session("UserContactID"))
            ddlDashboardTemplates.DataSource = objDashboardTemplate.GetAllByDomain()
            ddlDashboardTemplates.DataTextField = "vcTemplateName"
            ddlDashboardTemplates.DataValueField = "numTemplateID"
            ddlDashboardTemplates.DataBind()
            ddlDashboardTemplates.Items.Insert(0, New ListItem("-- Select One --", "0"))

            Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
            objCampaign.DomainID = Session("DomainID")
            objCampaign.ModuleID = 45
            ddlEmailTemplates.DataSource = objCampaign.GetEmailTemplates
            ddlEmailTemplates.DataTextField = "VcDocName"
            ddlEmailTemplates.DataValueField = "numGenericDocID"
            ddlEmailTemplates.DataBind()
            ddlEmailTemplates.Items.Insert(0, New ListItem("-- Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class