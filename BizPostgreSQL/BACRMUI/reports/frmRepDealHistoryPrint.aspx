<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmRepDealHistoryPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmRepDealHistoryPrint" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>frmRepDealHistoryPrint</title>
		
		
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		
			<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" CssClass="print_head" Runat="server">Project Status Report</asp:Label>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:datagrid id="dgDeals" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn Visible="False" DataField="numOppId" HeaderText="numOppId"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="numCompanyID" HeaderText="numCompanyID"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="numTerID" HeaderText="numTerID"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="numDivisionID" HeaderText="numDivisionID"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="tintCRMType" HeaderText="tintCRMType"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="numContactID" HeaderText="numContactID"></asp:BoundColumn>
					<asp:ButtonColumn DataTextField="Name" SortExpression="Name" HeaderText="<font color=white>Name</font>"
						CommandName="Name"></asp:ButtonColumn>
					<asp:ButtonColumn DataTextField="Company" SortExpression="Company" HeaderText="<font color=white>Organization</font>"
						CommandName="Customer"></asp:ButtonColumn>
					<asp:ButtonColumn DataTextField="Contact" SortExpression="Contact" HeaderText="<font color=white>Contact</font>"
						CommandName="Contact"></asp:ButtonColumn>
					<asp:BoundColumn DataField="vcusername" SortExpression="vcusername" HeaderText="<font color=white>Purchasing Mgr</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="Type" SortExpression="Type" HeaderText="<font color=white>Type</font>"></asp:BoundColumn>
					<asp:BoundColumn DataField="STAGE" SortExpression="STAGE" HeaderText="<font color=white>Status</font>"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="<font color=white>Due Date</font>" SortExpression="CloseDate">
						<ItemTemplate>
							<%# ReturnName(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="monPAmount" DataFormatString="{0:f}" SortExpression="monPAmount" HeaderText="<font color=white>Amount</font>"></asp:BoundColumn>
				</Columns>
				<PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			
			<script language="JavaScript">
				function Lfprintcheck()
				{
					this.print()
					history.back()
					//document.location.href = "frmRepDealHistory.aspx";
				}
			</script>
		</form>
	</body>
</HTML>
