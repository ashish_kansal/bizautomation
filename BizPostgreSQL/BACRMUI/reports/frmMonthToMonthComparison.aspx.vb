Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmMonthToMonthComparison : Inherits BACRMPage

       
        Dim dtMonthToMonth As DataTable
        Dim dtMonthToMonth1 As DataTable
        Dim dsSplit As DataSet
        Dim objPredefinedReports As New PredefinedReports

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then

                    GetUserRightsForPage(8, 58)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                    ' = "Reports"
                    Dim objUserAccess As New UserAccess
                    Dim dtTeamUsers As DataTable
                    objUserAccess.UserCntID = Session("UserContactID")
                    objUserAccess.DomainID = Session("DomainID")
                    dtTeamUsers = objUserAccess.GetTeamForUsers

                    Dim intCount As Integer
                    For intCount = 0 To dtTeamUsers.Rows.Count - 1
                        ddlTeam1.Items.Add(New ListItem(dtTeamUsers.Rows(intCount).Item("vcData"), dtTeamUsers.Rows(intCount).Item("numListItemID")))
                        ddlTeam2.Items.Add(New ListItem(dtTeamUsers.Rows(intCount).Item("vcData"), dtTeamUsers.Rows(intCount).Item("numListItemID")))
                    Next
                    LoadYear(ddlYear1)
                    LoadYear(ddlYear2)
                    LoadMonth(ddlMonth1)
                    LoadMonth(ddlMonth2)
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                tblData.Rows.Clear()
                Dim intPReportsCount As Integer
                Dim objDashboard As New DashBoard
                'Declare the DataTable Row Object.
                Dim tblRow As TableRow
                'Declare The DataTable Cell Objects.
                Dim tblCell As TableCell
                Dim lbl As Label
                Dim strImage As String

                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.UserCntID = Session("UserContactID")

                ''  objPredefinedReports.FirstStartDate = Format(ddlMonth1.SelectedIndex + 1, "00") & "/01/" & ddlYear1.SelectedItem.Text
                objPredefinedReports.FirstStartDate = New Date(ddlYear1.SelectedItem.Text, Format(ddlMonth1.SelectedIndex + 1, "00"), 1)
                ''objPredefinedReports.FirstEndDate = Format(ddlMonth1.SelectedIndex + 1, "00") & "/" & objDashboard.GetLastDay(Format(ddlMonth1.SelectedIndex + 1, "00") & "/01/" & ddlYear1.SelectedItem.Text) & "/" & ddlYear1.SelectedItem.Text

                objPredefinedReports.FirstEndDate = New Date(ddlYear1.SelectedItem.Text, Format(ddlMonth1.SelectedIndex + 1, "00"), objDashboard.GetLastDay(New Date(ddlYear1.SelectedItem.Text, Format(ddlMonth1.SelectedIndex + 1, "00"), 1)))

                ''objPredefinedReports.SecondStartDate = Format(ddlMonth2.SelectedIndex + 1, "00") & "/01/" & ddlYear2.SelectedItem.Text
                objPredefinedReports.SecondStartDate = New Date(ddlYear2.SelectedItem.Text, Format(ddlMonth2.SelectedIndex + 1, "00"), 1)
                ''objPredefinedReports.SecondEndDate = Format(ddlMonth2.SelectedIndex + 1, "00") & "/" & objDashboard.GetLastDay(Format(ddlMonth2.SelectedIndex + 1, "00") & "/01/" & ddlYear2.SelectedItem.Text) & "/" & ddlYear2.SelectedItem.Text

                objPredefinedReports.SecondEndDate = New Date(ddlYear2.SelectedItem.Text, Format(ddlMonth2.SelectedIndex + 1, "00"), objDashboard.GetLastDay(New Date(ddlYear2.SelectedItem.Text, Format(ddlMonth2.SelectedIndex + 1, "00"), 1)))

                If ddlTeam1.SelectedIndex <> -1 And ddlTeam2.SelectedIndex <> -1 Then
                    objPredefinedReports.CompareFirst = ddlTeam1.SelectedValue
                    objPredefinedReports.CompareSecond = ddlTeam2.SelectedValue
                    objPredefinedReports.SortOrder = rbtnlistType.SelectedIndex

                    Dim intRowCount As Integer

                    dsSplit = objPredefinedReports.GetMonthToMonthComparison
                    dtMonthToMonth = dsSplit.Tables(0)
                    dtMonthToMonth1 = dsSplit.Tables(1)

                    For intRowCount = 1 To tblData.Rows.Count - 1
                        tblData.Rows(intRowCount).Cells.Clear()
                    Next

                    Dim FirstDisplay As String
                    FirstDisplay = ddlTeam1.SelectedItem.Text
                    Dim SecondDisplay As String
                    SecondDisplay = ddlTeam2.SelectedItem.Text

                    For intPReportsCount = 0 To dtMonthToMonth.Rows.Count - 1
                        tblRow = New TableRow
                        tblCell = New TableCell
                        tblRow = New TableRow
                        tblCell.Text = FirstDisplay
                        tblCell.CssClass = "normal1"
                        tblCell.Width = Unit.Pixel(100)
                        tblCell.HorizontalAlign = HorizontalAlign.Right
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        lbl = New Label
                        lbl.Height = Unit.Pixel(1)
                        tblCell.Height = Unit.Pixel(1)
                        Dim strcolour As String = "LawnGreen"
                        lbl.BackColor = System.Drawing.Color.FromName(strcolour)
                        If (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)) <> 0 Then
                            lbl.Width = Unit.Pixel(dtMonthToMonth.Rows(intPReportsCount).Item(0) * 400 / (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)))
                        End If
                        tblCell.Controls.Add(lbl)
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblCell.Text = String.Format("{0:#,##0.00}", dtMonthToMonth.Rows(intPReportsCount).Item(0))
                        tblRow.Cells.Add(tblCell)
                        tblData.Rows.Add(tblRow)
                    Next

                    For intPReportsCount = 0 To dtMonthToMonth1.Rows.Count - 1
                        tblRow = New TableRow
                        tblCell = New TableCell
                        tblRow = New TableRow
                        tblCell.Text = SecondDisplay
                        tblCell.CssClass = "normal1"
                        tblCell.Width = Unit.Pixel(100)
                        tblCell.HorizontalAlign = HorizontalAlign.Right
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        lbl = New Label
                        lbl.Height = Unit.Pixel(1)
                        tblCell.Height = Unit.Pixel(1)
                        Dim strcolour As String = "LightBlue"
                        lbl.BackColor = System.Drawing.Color.FromName(strcolour)
                        If (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)) <> 0 Then
                            lbl.Width = Unit.Pixel(dtMonthToMonth1.Rows(intPReportsCount).Item(0) * 400 / (dtMonthToMonth.Rows(intPReportsCount).Item(0) + dtMonthToMonth1.Rows(intPReportsCount).Item(0)))
                        End If
                        tblCell.Controls.Add(lbl)
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblCell.Text = String.Format("{0:#,##0.00}", dtMonthToMonth1.Rows(intPReportsCount).Item(0))
                        tblRow.Cells.Add(tblCell)
                        tblData.Rows.Add(tblRow)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadYear(ByVal ddlYear As DropDownList)
            Try
                'ddlYear.Items.Add("-- Select Year --")
                ddlYear.Items.Add(Year(Now()) - 2)
                ddlYear.Items.Add(Year(Now()) - 1)
                ddlYear.Items.Add(Year(Now()))
                ddlYear.Items.Add(Year(Now()) + 1)
                ddlYear.Items.Add(Year(Now()) + 2)
                ddlYear.Items.Add(Year(Now()) + 3)
                ddlYear.Items.Add(Year(Now()) + 4)
                ddlYear.Items.FindByValue(Year(Now())).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadMonth(ByVal ddListMonth As DropDownList)
            Try
                Dim Count As Integer
                For Count = 1 To 12
                    ddListMonth.Items.Add(New ListItem(MonthName(Count), Count))
                Next
                ddListMonth.Items.FindByValue(Month(Now)).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub rbtnlistType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                Dim strPath As String = Server.MapPath("")

                objPredefinedReports.ExportToExcel(dsSplit, Server.MapPath(""), strPath, Session("UserName"), Session("UserID"), "MonthToMonthReport", Session("DateFormat"))

                Response.Write("<script language=javascript>")
                Response.Write("alert('Exported Successfully !')")
                Response.Write("</script>")
                Dim strref As String = Session("SiteType") & "//" & Request.ServerVariables("HTTP_HOST") & Request.ApplicationPath() & "/reports/" & Session("UserName") & "_" & Session("UserID") & "_MonthToMonthReport.csv"

                Response.Write("<script language=javascript>")
                Response.Write("window.open('" & strref & "','','')")
                Response.Write("</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 35
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rbtnlistType_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlistType.SelectedIndexChanged
            Try
                If rbtnlistType.SelectedIndex = 0 Then
                    ddlTeam1.Items.Clear()
                    ddlTeam2.Items.Clear()

                    Dim objUserAccess As New UserAccess
                    Dim dtTeamUsers As DataTable
                    objUserAccess.UserCntID = Session("UserContactID")
                    objUserAccess.DomainID = Session("DomainID")
                    dtTeamUsers = objUserAccess.GetTeamForUsers

                    Dim intCount As Integer
                    For intCount = 0 To dtTeamUsers.Rows.Count - 1
                        ddlTeam1.Items.Add(New ListItem(dtTeamUsers.Rows(intCount).Item("vcData"), dtTeamUsers.Rows(intCount).Item("numListItemID")))
                        ddlTeam2.Items.Add(New ListItem(dtTeamUsers.Rows(intCount).Item("vcData"), dtTeamUsers.Rows(intCount).Item("numListItemID")))
                    Next
                Else
                    ddlTeam1.Items.Clear()
                    ddlTeam2.Items.Clear()

                    Dim objUserAccess As New UserAccess
                    Dim dtTeamUsers As DataTable
                    objUserAccess.UserCntID = Session("UserContactID")
                    objUserAccess.DomainID = Session("DomainID")
                    dtTeamUsers = objUserAccess.GetTerritoryForUsers

                    Dim intCount As Integer
                    For intCount = 0 To dtTeamUsers.Rows.Count - 1
                        ddlTeam1.Items.Add(New ListItem(dtTeamUsers.Rows(intCount).Item("vcTerName"), dtTeamUsers.Rows(intCount).Item("numTerritoryID")))
                        ddlTeam2.Items.Add(New ListItem(dtTeamUsers.Rows(intCount).Item("vcTerName"), dtTeamUsers.Rows(intCount).Item("numTerritoryID")))
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Response.Redirect("../reports/frmMonthToMonthComparisonPrint.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&year1=" & ddlYear1.SelectedItem.Text & "&month1=" & ddlMonth1.SelectedIndex & "&year2=" & ddlYear2.SelectedItem.Text & "&month2=" & ddlMonth2.SelectedIndex & " &team1=" & ddlTeam1.SelectedValue & " &team2=" & ddlTeam2.SelectedValue & "&listtype=" & rbtnlistType.SelectedIndex, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

    End Class
End Namespace