﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptSalesConversionReportDetail.aspx.vb"
    Inherits=".rptSalesConversionReportDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <title>Sales Conversion Performance Detail : Leads / Prospects converted to Accounts
    </title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function OpenWindow(a, b) {
            var str;
            if (b == 0) {
                str = "../Leads/frmLeads.aspx?listDivID=" + a;
            }
            else if (b == 1) {
                str = "../prospects/frmProspects.aspx?DivID=" + a;
            }
            else if (b == 2) {
                str = "../account/frmAccounts.aspx?DivId=" + a;
            }

            opener.frames.document.location.href = str;
        }

        function OpenOpp(a, b) {

            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=&OpID=" + a + "&OppType=" + b;
            opener.frames.document.location.href = str;
        }
    </script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblTitle" runat="server">Sales Conversion Performance Detail</asp:Label>&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1">
                <strong>Employee:
                    <asp:Label ID="lblEmployee" runat="server"></asp:Label></strong>
            </td>
            <td align="right">
                &nbsp;
                <asp:Button ID="btnExportToExcel" Width="120" CssClass="button" Text="Export to Excel"
                    runat="server"></asp:Button>&nbsp;
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                    Text="Close" />
            </td>
        </tr>
    </table>
    <asp:Table ID="Table9" Width="100%" runat="server" Height="350" GridLines="None"
        BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvSalesConversionDetail" runat="server" AutoGenerateColumns="False"
                    CssClass="dg" Width="100%" DataKeyNames="numContactId">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField HeaderText="Lead / Prospect  Name" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="javascript:OpenWindow(<%# Eval("numDivisionId") %>,<%# Eval("tintCRMType") %>)">
                                    <%# Eval("vcCompanyName")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Created" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# ReturnName(DataBinder.Eval(Container.DataItem, "DVCreatedDate"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Converted Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# ReturnName(DataBinder.Eval(Container.DataItem, "FirSalesDate"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Days it took" DataField="DaysTook" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="1st Conversion Amt" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# String.Format("{0:##,#00.00}", Eval("FirSalesAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="All Sale Orders" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# String.Format("{0:##,#00.00}", Eval("TotalSalesIncome"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="gvSalesConversionSalesOrder" runat="server" AutoGenerateColumns="False"
                    CssClass="dg" Width="100%">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField HeaderText="Lead / Prospect  Name" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="javascript:OpenWindow(<%# Eval("numDivisionId") %>,<%# Eval("tintCRMType") %>)">
                                    <%# Eval("vcCompanyName")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Order ID" HeaderStyle-ForeColor="White">
                            <ItemTemplate>
                                <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>')">
                                    <%# Eval("vcPOppName") %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="bintCreatedDate" HeaderText="Date Created"></asp:BoundField>
                        <asp:BoundField HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monDealAmount"
                            HtmlEncode="false"></asp:BoundField>
                        <asp:BoundField HeaderText="Open/Completed" DataField="vcDealStatus"></asp:BoundField>
                        <asp:BoundField HeaderText="Status" DataField="vcStatus"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
