﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmCapacityPlanningTasks.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCapacityPlanningTasks" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-header > .box-tools {
            position: absolute;
            right: 0px;
            top: 3px;
        }

        .manage-wip-date-range li {
            background-color: #dae3f3;
            color: #000;
            font-weight: bold;
        }

            .manage-wip-date-range li a {
                padding: 7px 15px;
                border-top: 0px;
            }

        .table-calendar, .table-calendar th, .table-calendar td {
            border-color: #ababab !important;
            border-width: 1px !important;
            border-style: solid !important;
        }

            .table-calendar th {
                background: #fafafa !important;
                text-align: center;
                padding: 2px !important;
            }
    </style>
    <script type="text/javascript">
        var globalEmployees = null;
        var globalTeams = null;

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        $(document).ready(function () {
            ProcessTypeChanged();

            $("#ddlProcess").change(function () {
                $("#ddlMilestone").find('option').not(':first').remove();
                $("#ddlStage").find('option').not(':first').remove();
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindMilestone()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlMilestone").change(function () {
                $("#ddlStage").find('option').not(':first').remove();
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindStages()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlStage").change(function () {
                $("#ulTaskOptions").html("");
                $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
                $("#ulGradeOptions input[type='checkbox']").each(function () {
                    $(this).prop("checked", "");
                });
                $("#divMain").html("");

                if (parseInt($(this).val()) > 0) {
                    $.when(BindTasks()).then(function () {
                        BindReportData();
                    });
                }
            });

            $("#ddlCellColor").change(function () {
                try {
                    var capacityType = $(this).val();

                    $("#divMain table td").each(function () {
                        var capacityLoad = 0;

                        if (capacityType === "1" && $(this).attr("assigneeCapacityLoad") != null) {
                            capacityLoad = parseInt($(this).attr("assigneeCapacityLoad") || 0)
                        } else if (capacityType === "2" && $(this).attr("gradeCapacityLoad") != null) {
                            capacityLoad = parseInt($(this).attr("gradeCapacityLoad") || 0)
                        }

                        var backgroundColor = "";

                        if (capacityLoad <= 50) {
                            backgroundColor = "#ccfcd9";
                        } else if (capacityLoad > 50 && capacityLoad <= 75) {
                            backgroundColor = "#ffffcd";
                        } else if (capacityLoad > 75 && capacityLoad <= 100) {
                            backgroundColor = "#ffdc7d";
                        } else if (capacityLoad > 100) {
                            backgroundColor = "#ffd1d1";
                        }

                        if (($(this).attr("assigneeCapacityLoad") != null || $(this).attr("gradeCapacityLoad") != null) && $(this).html() != "") {
                            $(this).css("background-color", backgroundColor);
                        }
                    });
                } catch (e) {
                    alert("Unknown errro occurred.");
                }
            });
        })

        function ProcessTypeChanged() {
            ClearControls();

            $.when(BindProcess()).then(function () {
                BindReportData();
            });
        }

        function ClearControls() {
            $("#ddlProcess").find('option').not(':first').remove();
            $("#ddlMilestone").find('option').not(':first').remove();
            $("#ddlStage").find('option').not(':first').remove();
            $("#btnDropDownTask").html("All" + " <span class='caret'></span>");
            $("#ulTaskOptions").html("");
            $("#ulGradeOptions input[type='checkbox']").each(function () {
                $(this).prop("checked", "");
            });
            $("#btnDropDownGrade").html("All" + " <span class='caret'></span>");
            $("#divMain").html("");
        }

        function BindProcess() {
            $("#ddlProcess").find('option').not(':first').remove();

            return $.ajax({
                type: "POST",
                url: '../admin/frmAdminBusinessProcess.aspx/WebMethodGetProcessList',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": "<%# Session("DomainID")%>"
                            , "Mode": $("input[name='ProcessType']:checked").val()
                }),
                success: function (data) {
                    try {
                        var Jresponse = $.parseJSON(data.d);

                        $.each(Jresponse, function (index, value) {
                            $("#ddlProcess").append("<option value=" + value.Slp_Id + ">" + value.Slp_Name + "</option>");
                        });

                        if ($("#ddlProcess option").length > 1) {
                            $('#ddlProcess option:eq(1)').attr('selected', 'selected');
                            BindMilestone();
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading process.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading process: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while loading process");
                    }
                }
            });
            }

            function BindMilestone() {
                try {
                    $("#ddlMilestone").find('option').not(':first').remove();

                    if (parseInt($("#ddlProcess").val()) > 0) {
                        return $.ajax({
                            type: "POST",
                            url: '../WebServices/StagePercentageDetailsService.svc/GetMilestonesByProcess',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "processID": parseInt($("#ddlProcess").val())
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.GetMilestonesByProcessResult);
                                    if (obj != null && obj.length > 0) {
                                        $.each(obj, function (index, value) {
                                            $("#ddlMilestone").append("<option value=" + (index + 1) + ">" + value.vcMileStoneName + "</option>");
                                        });
                                    }
                                } catch (err) {
                                    alert("Unknown error occurred while loading milestones.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred while loading milestones: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred while loading milestones.");
                                }
                            }
                        });
                    }
                } catch (e) {
                    alert("Unknown error ocurred while loading milestones.");
                }
            }

            function BindStages() {
                try {
                    $("#ddlStage").find('option').not(':first').remove();

                    if (parseInt($("#ddlMilestone").val()) > 0) {
                        return $.ajax({
                            type: "POST",
                            url: '../WebServices/StagePercentageDetailsService.svc/GetStagesByProcess',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "processID": parseInt($("#ddlProcess").val())
                                , "milestoneName": replaceNull($("#ddlMilestone option:selected").text())
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.GetStagesByProcessResult);
                                    if (obj != null && obj.length > 0) {
                                        $.each(obj, function (index, value) {
                                            $("#ddlStage").append("<option value='" + value.numStageDetailsId + "'>" + value.vcStageName + "</option>");
                                        });
                                    }
                                } catch (err) {
                                    alert("Unknown error occurred while loading stages.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred while loading stages: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred while loading milestones.");
                                }
                            }
                        });
                    }
                } catch (e) {
                    alert("Unknown error ocurred while loading milestones.");
                }
            }

            function BindTasks() {
                try {
                    $("#ulTaskOptions").html("");

                    if (parseInt($("#ddlStage").val()) > 0) {
                        return $.ajax({
                            type: "POST",
                            url: '../WebServices/StagePercentageDetailsTaskService.svc/GetTasksByStage',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "stageDetailsID": parseInt($("#ddlStage").val())
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.GetTasksByStageResult);
                                    if (obj != null && obj.length > 0) {
                                        $.each(obj, function (index, value) {
                                            $("#ulTaskOptions").append("<li><input type='checkbox' onchange='TaskSelectionChanged();' id='chkTask" + value.numTaskId.toString() + "' /><label>" + replaceNull(value.vcTaskName) + "</label></li>");
                                        });
                                    }
                                } catch (err) {
                                    alert("Unknown error occurred while loading tasks.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred while loading tasks: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred while loading tasks.");
                                }
                            }
                        });
                    }
                } catch (e) {
                    alert("Unknown error ocurred while loading tasks.");
                }
            }

            function TaskSelectionChanged() {
                try {
                    var selected = 0;

                    $("#ulTaskOptions input[type='checkbox']").each(function () {
                        if ($(this).is(":checked")) {
                            selected++;
                        }
                    });

                    $("#btnDropDownTask").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                    $("#divMain").html("");
                    BindReportData();
                } catch (e) {
                    alert("Unknown error occurred.");
                }
            }

            function GradeSelectionChanged() {
                try {
                    var selected = 0;

                    $("#ulGradeOptions input[type='checkbox']").each(function () {
                        if ($(this).is(":checked")) {
                            selected++;
                        }
                    });

                    $("#btnDropDownGrade").html((selected > 0 ? (selected.toString() + " Selected") : "All") + " <span class='caret'></span>");

                    $("#divMain").html("");
                    BindReportData();
                } catch (e) {
                    alert("Unknown error occurred.");
                }
            }

            function GetSelectedTaskValues() {
                try {
                    var selectedValues = "";

                    $("#ulTaskOptions input[type='checkbox']").each(function () {
                        if ($(this).is(":checked")) {
                            selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkTask", "");
                        }
                    });

                    return selectedValues;
                } catch (e) {
                    alert("Unknown error occurred.");
                }
            }

            function GetSelectedGradeValues() {
                try {
                    var selectedValues = "";

                    $("#ulGradeOptions input[type='checkbox']").each(function () {
                        if ($(this).is(":checked")) {
                            selectedValues = selectedValues + (selectedValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkGrade", "");
                        }
                    });

                    return selectedValues;
                } catch (e) {
                    alert("Unknown error occurred.");
                }
            }

            function BindReportData() {
                try {
                    var datepicker = $find("<%= rdpDate.ClientID%>");
                    if (datepicker.get_selectedDate() != null) {
                        if (parseInt($("#ddlProcess").val()) > 0) {
                            var view = parseInt($("#hdnDateRange").val());

                            var todayDate = new Date(datepicker.get_selectedDate());
                            var fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 0, 0, 0, 0);
                            var toDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 23, 59, 59, 999);

                            if (view === 2) {
                                var weekStart = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay()));
                                var weekEnd = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay() + 6));

                                fromDate = new Date(weekStart.getFullYear(), weekStart.getMonth(), weekStart.getDate(), 0, 0, 0, 0);
                                toDate = new Date(weekEnd.getFullYear(), weekEnd.getMonth(), weekEnd.getDate(), 23, 59, 59, 999);
                            } else if (view === 3) {
                                fromDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1, 0, 0, 0, 0);
                                toDate = new Date(todayDate.getFullYear(), todayDate.getMonth() + 1, 0, 23, 59, 59, 999);
                            }


                            $.ajax({
                                type: "POST",
                                url: '../WebServices/StagePercentageDetailsService.svc/GetCapacityPlanningTasksReportData',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "processType": parseInt($("input[name='ProcessType']:checked").val())
                                    , "processID": parseInt($("#ddlProcess").val())
                                    , "milestoneName": (parseInt($("#ddlMilestone").val()) > 0 ? replaceNull($("#ddlMilestone option:selected").text()) : "")
                                    , "stageDetailsID": parseInt($("#ddlStage").val())
                                    , "taskIds": GetSelectedTaskValues()
                                    , "gradeIds": GetSelectedGradeValues()
                                    , "fromDate": "\/Date(" + fromDate.getTime() + ")\/"
                                    , "toDate": "\/Date(" + toDate.getTime() + ")\/"
                                    , "view": view
                                }),
                                success: function (data) {
                                    try {
                                        var obj = $.parseJSON(data.GetCapacityPlanningTasksReportDataResult);

                                        if (obj != null) {
                                            if (view === 1) {
                                                DisplayDayData(obj)
                                            } else if (view === 2) {
                                                DisplayWeekData(obj)
                                            } else if (view === 3) {
                                                DisplayMonthData(obj)
                                            }
                                        }
                                    } catch (err) {
                                        alert("Unknown error occurred while loading report data.");
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        alert("Error occurred while loading report data: " + replaceNull(objError.ErrorMessage));
                                    } else {
                                        alert("Unknown error ocurred while loading report data.");
                                    }
                                }
                            });
                        }
                    } else {
                        alert("Select date.");
                    }
                } catch (e) {
                    alert("Unknown error ocurred while loading report data.");
                }
            }

        function DisplayDayData(obj) {
            try {
                var milestones = $.parseJSON(obj.Milestones);
                var stages = $.parseJSON(obj.Stages);
                var tasks = $.parseJSON(obj.Tasks);
                var reportData = $.parseJSON(obj.ReportData);
                var employees = $.parseJSON(obj.Employees);
                var teams = $.parseJSON(obj.Teams);
                var grades = $.parseJSON(obj.Grades);

                var html = "";
                html += "<table class='table table-bordered'>";
                html += "<thead>";
                html += "<tr>";
                html += "<th>Work Order</th>";
                html += "<th>Item, Qty Processed (Remaining)</th>";
                html += "<th style='width:200px;'>Time Required (Started)</th>";
                html += "<th style='width:145px;'>Projected Completion</th>";
                html += "<th style='width:145px;'>Release/Due Date</th>";
                html += "<th>Resource</th>";
                html += "<th>Team</th>";
                html += "<th>Grade</th>";
                html += "</tr>";
                html += "</thead>";
                html += "<tbody>";
                if (milestones != null && milestones.length > 0) {
                    html += LoadMilestones(1, null, null, milestones, stages, tasks, reportData, employees, teams, grades);
                }
                html += "</tbody>";
                html += "</table>";                

                $("#divMain").html(html);
            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function DisplayWeekData(obj) {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = new Date(datepicker.get_selectedDate());

                var weekStart = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay()));
                var weekEnd = new Date(todayDate.setDate(todayDate.getDate() - todayDate.getDay() + 6));

                var tempWeekStart = new Date(weekStart);
                var tempWeekEnd = new Date(weekEnd);

                var html = "<table class='table table-calendar'><thead><tr><th></th>";

                while (weekStart <= weekEnd) {
                    html += ("<th>" + weekStart.getDate() + " " + weekStart.getMonthNameShort() + "</th>");
                    weekStart = new Date(weekStart.setDate(weekStart.getDate() + 1));
                }
                html += "</tr>";
                html += "</thead>";

                var milestones = $.parseJSON(obj.Milestones);
                var stages = $.parseJSON(obj.Stages);
                var tasks = $.parseJSON(obj.Tasks);
                var reportData = $.parseJSON(obj.ReportData);

                if (milestones != null && milestones.length > 0) {
                    html += LoadMilestones(2, tempWeekStart, tempWeekEnd, milestones, stages, tasks, reportData, null, null, null);
                }


                html += "</table>";

                $("#divMain").html(html);
            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function DisplayMonthData(obj) {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = datepicker.get_selectedDate();

                var monthStart = new Date(todayDate.getFullYear(), todayDate.getMonth(), 1, 0, 0, 0, 0);
                var monthEnd = new Date(todayDate.getFullYear(), todayDate.getMonth() + 1, 0, 0, 0, 0, 0);

                var tempMonthStart = new Date(monthStart);
                var tempMonthEnd = new Date(monthEnd);

                var diffDate = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
                var days = Math.round(diffDate);

                var html = "<table class='table table-calendar'><thead><tr><th colspan='" + (days + 2) + "'>" + monthStart.getMonthNameShort() + " " + monthStart.getFullYear() + "</th><tr><th></th>";

                while (monthStart <= monthEnd) {
                    html += ("<th>" + monthStart.getDate() + "</th>");
                    monthStart = new Date(monthStart.setDate(monthStart.getDate() + 1));
                }
                html += "</tr>";
                html += "</thead>";

                var milestones = $.parseJSON(obj.Milestones);
                var stages = $.parseJSON(obj.Stages);
                var tasks = $.parseJSON(obj.Tasks);
                var reportData = $.parseJSON(obj.ReportData);

                if (milestones != null && milestones.length > 0) {
                    html += LoadMilestones(3, tempMonthStart, tempMonthEnd, milestones, stages, tasks, reportData, null, null, null);
                }


                html += "</table>";

                $("#divMain").html(html);


            } catch (e) {
                alert("Unknown error ocurred while showing report data.");
            }
        }

        function LoadMilestones(view, startDate, endDate, milestones, stages, tasks, reportData, employees, teams, grades) {
            var html = "";
            var colspan = 0;

            if (view === 1) {
                colspan = 8;
            } else if (view === 2) {
                colspan = 8;
            } else if (view === 3) {
                var diffDate = (endDate - startDate) / (1000 * 60 * 60 * 24);
                var days = Math.round(diffDate);
                colspan = days + 2;
            }

            $.each(milestones, function (index, e) {
                html += "<tr class='milestoneParent" + index + "'><td style='font-weight:bold;padding:3px;' colspan='" + colspan + "'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"milestoneChild" + index + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a>  " + replaceNull(e.vcMileStoneName) + "</td></tr>";
                html += LoadStages(index, view, startDate, endDate, replaceNull(e.vcMileStoneName), stages, tasks, reportData, employees, teams, grades);
            });

            return html;
        }

        function LoadStages(milestoneIndex, view, startDate, endDate, milestone, stages, tasks, reportData, employees, teams, grades) {
            var html = "";
            var colspan = 0;

            if (view === 1) {
                colspan = 8;
            } else if (view === 2) {
                colspan = 8;
            } else if (view === 3) {
                var diffDate = (endDate - startDate) / (1000 * 60 * 60 * 24);
                var days = Math.round(diffDate);
                colspan = days + 2;
            }

            if (stages != null && stages.length > 0) {
                $.each(stages, function (index, e) {
                    if (e.vcMileStoneName === milestone) {
                        html += "<tr class='milestoneChild" + milestoneIndex + " stageParent" + index + "'><td style='padding-left:20px;font-weight:bold;padding-top:3px;padding-bottom:3px;' colspan='" + colspan + "'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"stageChild" + index + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a>  " + replaceNull(e.vcStageName) + "</td></tr>";
                        html += LoadTasks(milestoneIndex, index, view, startDate, endDate, (e.numStageDetailsId || 0), tasks, reportData, employees, teams, grades);
                    }
                });
            }

            return html;
        }

        function LoadTasks(milestoneIndex, stageIndex, view, weekStart, weekEnd, stageDetailsId, tasks, reportData, employees, teams, grades) {
            var html = "";

            if (view === 1) {
                globalEmployees = employees;
                globalTeams = teams

                if (tasks != null && tasks.length > 0) {
                    $.each(tasks, function (index, e) {
                        if (e.numStageDetailsId === stageDetailsId) {
                            html += "<tr class='milestoneChild" + milestoneIndex + " stageChild" + stageIndex + " taskParent" + index + "'><td colspan='8' style='padding-left:40px;font-weight:bold;padding-top:3px;padding-bottom:3px;'><a href='#' class='aExpandCollapse' onclick=ExpandCollapse(\"taskChild" + e.numTaskID + "\",this);return false;' style='padding-right:5px;color:#000'><i class='fa fa-chevron-up'></i></a> " + replaceNull(e.vcTaskName) + "</td></tr>";

                            if (reportData != null && reportData.length > 0) {
                                var obj = reportData.filter(function (result) {
                                    return result.numTaskID === e.numTaskID;
                                });

                                if (obj.length > 0) {
                                    obj.forEach(function (o) {
                                        html += "<tr class='milestoneChild" + milestoneIndex + " stageChild" + stageIndex + " taskChild" + o.numTaskID + "'>";
                                        html += "<td><a target='_blank' href='../Items/frmWorkOrder.aspx?WOID=" + (o.numWOId || 0) + "'>" + replaceNull(o.vcWorkOrderName) + "</a></td>";
                                        html += "<td><a target='_blank' href-='../Items/frmKitDetails.aspx?ItemCode=" + (o.numItemCode || 0) + "&frm=All Items'>" + replaceNull(o.vcItemName) + "</a>, " + (o.numProcessedQty || 0) + " (" + (o.numRemainingQty || 0) + ")</td>";
                                        html += "<td>" + replaceNull(o.vcTimeRequired) + (replaceNull(o.dtStartDate).length > 0 ? " (" + o.dtStartDate + ")" : "") + "</td>";
                                        html += "<td style='text-align:center'>" + o.dtFinishDate + "</td>";
                                        html += "<td style='text-align:center'>" + o.dtReleaseDate + "</td>";
                                        html += "<td>";                                        
                                        html += "<input type='hidden' value='" + (o.numTaskID || 0) + "' class='hdnTaskID' />";
                                        html += "<input type='hidden' value='" + (o.numActualTaskID || 0) + "' class='hdnActualTaskID' />";
                                        html += "<input type='hidden' value='" + (o.numAssignTo || 0) + "' class='hdnAssignee' />";
                                        html += "<select class='form-control ddlAssignee' onChange='AssigneeChanged(this);' style='color:#000;background-color:##AssigneeBackColor##' >";
                                        html += "<option value='0'>-- Select One --</option>"

                                        if (employees != null && employees.length > 0) {
                                            employees.forEach(function (emp) {
                                                var capacityLoad = (emp.numCapacityLoad || 0);
                                                var backgroundColor = "";

                                                if (capacityLoad <= 50) {
                                                    backgroundColor = "#ccfcd9";
                                                } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                                    backgroundColor = "#ffffcd";
                                                } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                                    backgroundColor = "#ffdc7d";
                                                } else if (capacityLoad > 100) {
                                                    backgroundColor = "#ffd1d1";
                                                }

                                                if ((o.numAssignTo || 0) === emp.numUserDetailId) {
                                                    html = html.replace("##AssigneeBackColor##", backgroundColor);
                                                    html += "<option  style='background-color:" + backgroundColor + "' value='" + emp.numUserDetailId + "' selected='selected'>" + replaceNull(emp.vcUserName) + " (" + (emp.numCapacityLoad || 0) + "%)</option>";
                                                } else {
                                                    html += "<option  style='background-color:" + backgroundColor + "' value='" + emp.numUserDetailId + "'>" + replaceNull(emp.vcUserName) + " (" + (emp.numCapacityLoad || 0) + "%)</option>";
                                                }
                                            });
                                        }

                                        html += "</select>";
                                        html += "</td>";
                                        html += "<td>";
                                        html += "<select class='form-control ddlTeam' onChange='TeamChanged(this);' style='color:#000;background-color:##TeamBackColor##' >";
                                        html += "<option value='0'>-- Select One --</option>"
                                        if (teams != null && teams.length > 0) {
                                            teams.forEach(function (team) {
                                                var capacityLoad = (team.numCapacityLoad || 0);
                                                var backgroundColor = "";

                                                if (capacityLoad <= 50) {
                                                    backgroundColor = "#ccfcd9";
                                                } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                                    backgroundColor = "#ffffcd";
                                                } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                                    backgroundColor = "#ffdc7d";
                                                } else if (capacityLoad > 100) {
                                                    backgroundColor = "#ffd1d1";
                                                }

                                                if ((o.numTeam || 0) === team.numTeam) {
                                                    html = html.replace("##TeamBackColor##", backgroundColor);
                                                    html += "<option  style='background-color:" + backgroundColor + "' value='" + team.numTeam + "' selected='selected'>" + replaceNull(team.vcTeamName) + " (" + (team.numCapacityLoad || 0) + "%)</option>";
                                                } else {
                                                    html += "<option  style='background-color:" + backgroundColor + "' value='" + team.numTeam + "'>" + replaceNull(team.vcTeamName) + " (" + (team.numCapacityLoad || 0) + "%)</option>";
                                                }
                                            });
                                        }
                                        html += "</select>";
                                        html += "</td>";
                                        html += "<td>";
                                        html += "<select class='form-control ddlGrade'  onChange='GradeChanged(this);' style='color:#000;background-color:##GradeBackColor##' >";
                                        html += "<option value='0'>-- Select One --</option>";
                                        if (grades != null && grades.length > 0) {
                                            grades.forEach(function (grade) {
                                                if (o.numTaskID === grade.numTaskID) {
                                                    html += LoadGradeDropDownOption("A+", (grade.APlus || 0), (o.vcGrade==="A+"?true:false));
                                                    html += LoadGradeDropDownOption("A", (grade.A || 0),(o.vcGrade === "A" ? true : false));
                                                    html += LoadGradeDropDownOption("A-", (grade.AMinus || 0), (o.vcGrade === "A-" ? true : false));
                                                    html += LoadGradeDropDownOption("B+", (grade.BPlus || 0), (o.vcGrade === "B+" ? true : false));
                                                    html += LoadGradeDropDownOption("B", (grade.B || 0), (o.vcGrade === "B" ? true : false));
                                                    html += LoadGradeDropDownOption("B-", (grade.BMinus || 0), (o.vcGrade === "B-" ? true : false));
                                                    html += LoadGradeDropDownOption("C+", (grade.CPlus || 0), (o.vcGrade === "C+" ? true : false));
                                                    html += LoadGradeDropDownOption("C", (grade.C || 0), (o.vcGrade === "C" ? true : false));
                                                    html += LoadGradeDropDownOption("C-", (grade.CMinus || 0), (o.vcGrade === "C-" ? true : false));

                                                    var capacityLoad = -1;

                                                    if (o.vcGrade === "A+") {
                                                        capacityLoad = (grade.APlus || 0);
                                                    } else if (o.vcGrade === "A") {
                                                        capacityLoad = (grade.A || 0);
                                                    } else if (o.vcGrade === "A-") {
                                                        capacityLoad = (grade.AMinus || 0);
                                                    } else if (o.vcGrade === "B+") {
                                                        capacityLoad = (grade.BPlus || 0);
                                                    } else if (o.vcGrade === "B") {
                                                        capacityLoad = (grade.B || 0);
                                                    } else if (o.vcGrade === "B-") {
                                                        capacityLoad = (grade.BMinus || 0);
                                                    } else if (o.vcGrade === "C+") {
                                                        capacityLoad = (grade.CPlus || 0);
                                                    } else if (o.vcGrade === "C") {
                                                        capacityLoad = (grade.C || 0);
                                                    } else if (o.vcGrade === "C-") {
                                                        capacityLoad = (grade.CMinus || 0);
                                                    }


                                                    var backgroundColor = "";
                                                    if (capacityLoad >= 0 && capacityLoad <= 50) {
                                                        backgroundColor = "#ccfcd9";
                                                    } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                                        backgroundColor = "#ffffcd";
                                                    } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                                        backgroundColor = "#ffdc7d";
                                                    } else if (capacityLoad > 100) {
                                                        backgroundColor = "#ffd1d1";
                                                    }

                                                    html = html.replace("##GradeBackColor##", backgroundColor);
                                                }
                                            });
                                        }
                                        html += "</select>";
                                        html += "</td>";
                                        html += "</tr>";
                                    });
                                }
                            }
                        }
                    });
                }
            } else {
                if (tasks != null && tasks.length > 0) {
                    tasks.forEach(function (e) {
                        if (e.numStageDetailsId === stageDetailsId) {
                            var tempWeekStart = new Date(weekStart);
                            var tempWeekEnd = new Date(weekEnd);
                            if (view === 2) {
                                html += "<tr class='milestoneChild" + milestoneIndex + " stageChild" + stageIndex + "'><td style='padding-left:40px;height:106px; white-space:nowrap;'>" + replaceNull(e.vcTaskName) + "</td>";
                            } else if (view === 3) {
                                html += "<tr class='milestoneChild" + milestoneIndex + " stageChild" + stageIndex + "'><td style='padding-left:40px;white-space:nowrap;'>" + replaceNull(e.vcTaskName) + "</td>";
                            }

                            while (tempWeekStart <= tempWeekEnd) {
                                var obj = reportData.filter(function (result) {
                                    return result.numTaskID === e.numTaskID && parseInt(result.numTotalTasks || 0) > 0 && new Date(result.dtDate).toLocaleDateString() === new Date(tempWeekStart.getFullYear(), tempWeekStart.getMonth(), tempWeekStart.getDate(), 0, 0, 0, 0).toLocaleDateString();
                                });


                                if (view === 2) {
                                    if (obj.length > 0) {
                                        var capacityLoad = 0;

                                        if ($("#ddlCellColor").val() === "2") {
                                            capacityLoad = parseInt(obj[0].numGradeCapacityLoad || 0)
                                        } else {
                                            capacityLoad = parseInt(obj[0].numAssigneeCapacityLoad || 0)
                                        }

                                        var backgroundColor = "";

                                        if (capacityLoad <= 50) {
                                            backgroundColor = "background-color:#ccfcd9";
                                        } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                            backgroundColor = "background-color:#ffffcd";
                                        } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                            backgroundColor = "background-color:#ffdc7d";
                                        } else if (capacityLoad > 100) {
                                            backgroundColor = "background-color:#ffd1d1";
                                        }


                                        html += ("<td style='height:106px; width:200px;" + backgroundColor + "'  assigneeCapacityLoad='" + (obj[0].numAssigneeCapacityLoad || 0) + "' gradeCapacityLoad='" + (obj[0].numGradeCapacityLoad || 0) + "'>");
                                        html += ("<b>Assigned Tasks: </b><a style='color:#004eff;text-decoration:underline;' href='javascript:OpenDetail(\"" + tempWeekStart.toLocaleDateString() + "\");'>" + obj[0].numTotalTasks + "</a> (<b>Max:</b> " + (obj[0].numMaxTasks || 0) + ") <br/>");
                                        html += ("Capacity Load: " + (obj[0].numAssigneeCapacityLoad || 0) + "% <br/>");
                                        html += ("<b>Grade Capacity: </b>" + (obj[0].numGradeCapacity || 0) + "<br/>");
                                        html += ("Grade Capacity Load: " + (obj[0].numGradeCapacityLoad || 0) + "%");
                                        html += "</td>";
                                    } else {
                                        html += ("<td style='height:106px; width:200px;'></td>");
                                    }
                                } else if (view === 3) {
                                    if (obj.length > 0) {
                                        var capacityLoad = 0;

                                        if ($("#ddlCellColor").val() === "2") {
                                            capacityLoad = parseInt(obj[0].numGradeCapacityLoad || 0)
                                        } else {
                                            capacityLoad = parseInt(obj[0].numAssigneeCapacityLoad || 0)
                                        }

                                        var backgroundColor = "";

                                        if (capacityLoad <= 50) {
                                            backgroundColor = "background-color:#ccfcd9";
                                        } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                            backgroundColor = "background-color:#ffffcd";
                                        } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                            backgroundColor = "background-color:#ffdc7d";
                                        } else if (capacityLoad > 100) {
                                            backgroundColor = "background-color:#ffd1d1";
                                        }


                                        html += ("<td style='text-align:center;" + backgroundColor + "' assigneeCapacityLoad='" + (obj[0].numAssigneeCapacityLoad || 0) + "' gradeCapacityLoad='" + (obj[0].numGradeCapacityLoad || 0) + "'>");
                                        html += "<a style='color:#004eff;text-decoration:underline;' href='javascript:OpenDetail(\"" + tempWeekStart.toLocaleDateString() + "\");'>" + (obj[0].numTotalTasks || 0) + "</a>";
                                        html += "</td>";
                                    } else {
                                        html += ("<td></td>");
                                    }
                                }

                                tempWeekStart = new Date(tempWeekStart.setDate(tempWeekStart.getDate() + 1));
                            }

                            html += "</tr>";
                        }
                    });
                }
            }

            return html;
        }

        function LoadGradeDropDownOption(text,value,isSelected) {
            var capacityLoad = (value || 0);
            var backgroundColor = "";

            if (capacityLoad <= 50) {
                backgroundColor = "#ccfcd9";
            } else if (capacityLoad > 50 && capacityLoad <= 75) {
                backgroundColor = "#ffffcd";
            } else if (capacityLoad > 75 && capacityLoad <= 100) {
                backgroundColor = "#ffdc7d";
            } else if (capacityLoad > 100) {
                backgroundColor = "#ffd1d1";
            }

            return "<option  style='background-color:" + backgroundColor + "' value='" + text + "'" + (isSelected ? " selected='selected'": "") + ">" + text + " (" + (value || 0) + "%)</option>";            
        }

        function DateRangeChanged(dateRange) {
            $("[id*=liDateRange]").removeClass("active");
            $("#liDateRange" + dateRange).addClass("active");
            $("[id$=hdnDateRange]").val(dateRange);
            BindReportData();

            return true;
        }

        function LoadPrevious() {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = datepicker.get_selectedDate();

                if ($("[id$=hdnDateRange]").val() === "3") {
                    datepicker.set_selectedDate(new Date(todayDate.setMonth(todayDate.getMonth() - 1)));
                } else if ($("[id$=hdnDateRange]").val() === "2") {
                    datepicker.set_selectedDate(new Date(todayDate.setDate(todayDate.getDate() - 7)));
                } else {
                    datepicker.set_selectedDate(new Date(todayDate.setDate(todayDate.getDate() - 1)));
                }

                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function LoadNext() {
            try {
                var datepicker = $find("<%= rdpDate.ClientID%>");
                var todayDate = datepicker.get_selectedDate();

                if ($("[id$=hdnDateRange]").val() === "3") {
                    datepicker.set_selectedDate(new Date(todayDate.setMonth(todayDate.getMonth() + 1)));
                } else if ($("[id$=hdnDateRange]").val() === "2") {
                    datepicker.set_selectedDate(new Date(todayDate.setDate(todayDate.getDate() + 7)));
                } else {
                    datepicker.set_selectedDate(new Date(todayDate.setDate(todayDate.getDate() + 1)));
                }

                BindReportData();
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function dateSelected(sender, eventArgs) {
            BindReportData();
        }

        function AssigneeChanged(ddl) {
            if (parseInt($(ddl).val()) > 0) {
                var tr = $(ddl).closest("tr");
                var hdnAssignee = $(tr).find(".hdnAssignee");
                var hdnActualTaskID = $(tr).find(".hdnActualTaskID");

                $.ajax({
                    type: "POST",
                    url: '../WebServices/StagePercentageDetailsTaskService.svc/ChangeAssignee',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "taskID": parseInt($(hdnActualTaskID).val())
                        , "assignedTo": parseInt($(ddl).val())
                    }),
                    beforeSend: function () {
                        $("[id$=UpdateProgress]").show();
                    },
                    complete: function () {
                        $("[id$=UpdateProgress]").hide();
                    },
                    success: function (data) {
                        $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(ddl).val($(hdnAssignee).val());

                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } else {
                alert("Select task assignee.");
            }
        }

        function TeamChanged(ddl) {
            $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));

            var tr = $(ddl).closest("tr");
            var ddlAssignee = $(tr).find(".ddlAssignee");
            var hdnAssignee = $(tr).find(".hdnAssignee");
            var hdnTaskID = $(tr).find(".hdnTaskID");

            if (ddlAssignee != null) {
                $(ddlAssignee).css("background-color", "");
                $(ddlAssignee).find('option').not(':first').remove();

                if (globalEmployees != null && globalEmployees.length > 0) {
                    globalEmployees.forEach(function (emp) {
                        if (emp.numTeam == $(ddl).val() || $(ddl).val() == "0") {
                            var capacityLoad = (emp.numCapacityLoad || 0);
                            var backgroundColor = "";

                            if (capacityLoad <= 50) {
                                backgroundColor = "#ccfcd9";
                            } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                backgroundColor = "#ffffcd";
                            } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                backgroundColor = "#ffdc7d";
                            } else if (capacityLoad > 100) {
                                backgroundColor = "#ffd1d1";
                            }

                            if (parseInt($(hdnAssignee).val()) === emp.numUserDetailId) {
                                $(ddlAssignee).css("background-color", backgroundColor);
                                $(ddlAssignee).append("<option  style='background-color:" + backgroundColor + "' value='" + emp.numUserDetailId + "' selected='selected'>" + replaceNull(emp.vcUserName) + " (" + (emp.numCapacityLoad || 0) + "%)</option>");
                            } else {
                                $(ddlAssignee).append("<option  style='background-color:" + backgroundColor + "' value='" + emp.numUserDetailId + "'>" + replaceNull(emp.vcUserName) + " (" + (emp.numCapacityLoad || 0) + "%)</option>");
                            }
                        }
                    });
                }
            }
        }

        function GradeChanged(ddl) {
            $(ddl).css("background-color", $(ddl).find("option:selected").css("background-color"));

            var tr = $(ddl).closest("tr");
            var ddlAssignee = $(tr).find(".ddlAssignee");
            var hdnAssignee = $(tr).find(".hdnAssignee");
            var hdnTaskID = $(tr).find(".hdnTaskID");

            if (ddlAssignee != null) {
                $(ddlAssignee).css("background-color", "");
                $(ddlAssignee).find('option').not(':first').remove();

                if (globalEmployees != null && globalEmployees.length > 0) {
                    globalEmployees.forEach(function (emp) {
                        var grades = $.parseJSON(emp.vcTaskGrades);

                        var inArray = false;
                        for (var i = 0; i < grades.length; i++) {
                            if (grades[i]["numTaskID"] == $(hdnTaskID).val() && grades[i]["vcGradeId"] == $(ddl).val()) {
                                inArray = true;
                            }
                        }

                        if (inArray || $(ddl).val() == "0") {
                            var capacityLoad = (emp.numCapacityLoad || 0);
                            var backgroundColor = "";

                            if (capacityLoad <= 50) {
                                backgroundColor = "#ccfcd9";
                            } else if (capacityLoad > 50 && capacityLoad <= 75) {
                                backgroundColor = "#ffffcd";
                            } else if (capacityLoad > 75 && capacityLoad <= 100) {
                                backgroundColor = "#ffdc7d";
                            } else if (capacityLoad > 100) {
                                backgroundColor = "#ffd1d1";
                            }

                            if (parseInt($(hdnAssignee).val()) === emp.numUserDetailId) {
                                $(ddlAssignee).css("background-color", backgroundColor);
                                $(ddlAssignee).append("<option  style='background-color:" + backgroundColor + "' value='" + emp.numUserDetailId + "' selected='selected'>" + replaceNull(emp.vcUserName) + " (" + (emp.numCapacityLoad || 0) + "%)</option>");
                            } else {
                                $(ddlAssignee).append("<option  style='background-color:" + backgroundColor + "' value='" + emp.numUserDetailId + "'>" + replaceNull(emp.vcUserName) + " (" + (emp.numCapacityLoad || 0) + "%)</option>");
                            }
                        }
                    });
                }
            }
        }

        function ExpandCollapse(cssClass, a) {
            if ($(a).html() === "<i class=\"fa fa-chevron-up\"></i>") {
                $(a).html("<i class=\"fa fa-chevron-right\"></i>");
                $("tr." + cssClass).hide();
            } else {
                $(a).html("<i class=\"fa fa-chevron-up\"></i>");
                $("tr." + cssClass).each(function () {
                    if ($(this).is('[class*=" stageParent"]')) {
                        $(this).find("a.aExpandCollapse").html("<i class=\"fa fa-chevron-up\"></i>");
                    }
                });
                $("tr." + cssClass).show();
            }
        }

        function OpenDetail(date) {
            var datepicker = $find("<%= rdpDate.ClientID%>");
            datepicker.set_selectedDate(new Date(date));
            DateRangeChanged(1);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-inline">
                    <li>
                        <input type="radio" name="ProcessType" value="3" checked="checked" onchange="ProcessTypeChanged();" />
                        <b>Build</b>
                        <input type="radio" name="ProcessType" value="1" onchange="ProcessTypeChanged();" />
                        <b>Project</b>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Process</label>
                            <select class="form-control" id="ddlProcess">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Milestone</label>
                            <select class="form-control" id="ddlMilestone">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Stage</label>
                            <select class="form-control" id="ddlStage">
                                <option value="0">-- Select One --</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Task</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownTask" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All
                              <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulTaskOptions" style="padding-left: 10px; padding-right: 5px;">
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <label>Grade</label>
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-default btn-flat dropdown-toggle" id="btnDropDownGrade" type="button" style="border-color: #d2d6de; background-color: #fff;" data-toggle="dropdown">
                                    All
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="ulGradeOptions" style="padding-left: 10px; padding-right: 5px;">
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeA+' /><label>A+</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeA' /><label>A</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeA-' /><label>A-</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeB+' /><label>B+</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeB' /><label>B</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeB-' /><label>B-</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeC+' /><label>C+</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeC' /><label>C</label></li>
                                    <li>
                                        <input type='checkbox' onchange='GradeSelectionChanged();' id='chkGradeC-' /><label>C-</label></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <label>Base Cell Color on</label>
                    <select class="form-control" id="ddlCellColor">
                        <option value="1" selected="selected">Assignee Capacity Load</option>
                        <option value="2">Grade Capacity Load</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Capacity Planning (Tasks)&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <ul class="list-inline" style="margin-bottom: 0px;">
        <li style="vertical-align: top;">
            <telerik:RadDatePicker ID="rdpDate" runat="server" DateInput-CssClass="form-control" Width="100" DateInput-Width="100" DatePopupButton-HoverImageUrl="~/images/calendar25.png" DatePopupButton-ImageUrl="~/images/calendar25.png">
                <ClientEvents OnDateSelected="dateSelected" />
            </telerik:RadDatePicker>
        </li>
        <li>
            <ul class="nav nav-pills manage-wip-date-range">
                <li id="liDateRange1" role="presentation" class="active"><a href="javascript:DateRangeChanged(1);">Day</a></li>
                <li id="liDateRange2" role="presentation"><a href="javascript:DateRangeChanged(2);">Week</a></li>
                <li id="liDateRange3" role="presentation"><a href="javascript:DateRangeChanged(3);">Month</a></li>
            </ul>
        </li>
        <li style="vertical-align: top;">
            <button class="btn btn-flat btn-primary" onclick="return LoadPrevious();">Previous</button>
            <button class="btn btn-flat btn-primary" onclick="return LoadNext();">Next</button>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" id="divMain">
        </div>
    </div>

    <input type="hidden" id="hdnDateRange" value="1" />
</asp:Content>
