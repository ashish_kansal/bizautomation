<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmTopItemsSoldDtls.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmTopItemsSoldDtls"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
		
		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
		<title>Items Sold</title>

		
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<table>
                <td width="100%"></td>
                <td  align="center">
		           <asp:UpdateProgress ID ="up1"  runat="server" DynamicLayout="false">
				        <ProgressTemplate>
				         <asp:Image ID="Image1"  ImageUrl="~/images/updating.gif" runat="server"/>
				         </ProgressTemplate>
    			    </asp:UpdateProgress>
    		    </td>
	    </table>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>

			<asp:datagrid id="dgProductRec" Width="100%" CssClass="dg" Runat="server" BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
			</asp:datagrid>
			</ContentTemplate>
			</asp:updatepanel>

		</form>
	</body>
</HTML>
