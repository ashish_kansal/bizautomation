'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Reports

    Partial Public Class frmForeCastReptItem

        '''<summary>
        '''ddlYear control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlYear As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlQuarter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlQuarter As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlItem control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlItem As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''btnShow control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnShow As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''lblForecastamt control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblForecastamt As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''chkShowEmpFor control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkShowEmpFor As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''chkShowPartFor control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkShowPartFor As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''btnAddtoMyRtpList control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddtoMyRtpList As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnTeams control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnTeams As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnTer control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnTer As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnExcel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnExcel As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnCancel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tblDetails control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblDetails As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''btnGo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
