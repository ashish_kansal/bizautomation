Imports System
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
'Imports ASPNET.StarterKit.Chart
Imports System.Web.UI.DataVisualization.Charting
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class ChartGenerator : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                ' This is a quick check for referrer server name against host server
                ' For best security practice in a deployment application,
                ' Image should only be rendered for authenticated users only.
                If Not (Request.UrlReferrer Is Nothing) Then
                    ''   If (Request.UrlReferrer.Host.ToLower() = Environment.UserDomainName.ToLower() Or Request.UrlReferrer.Host.ToLower() = "localhost") Then
                    ' set return type to png image format
                    Response.ContentType = "image/png"

                    Dim xValues, yValues, chartType, print As String
                    Dim boolPrint As Boolean

                    ' Get input parameters from query string
                    chartType = "pie"
                    xValues = GetQueryStringVal( "xValues")
                    yValues = GetQueryStringVal( "yValues")
                    print = GetQueryStringVal( "Print")

                    If chartType Is Nothing Then
                        chartType = ""
                    End If

                    ' check for printing option 
                    If print Is Nothing Then
                        boolPrint = False
                    Else
                        Try
                            boolPrint = Convert.ToBoolean(print)
                        Catch
                        End Try
                    End If

                    If Not (xValues Is Nothing) And Not (yValues Is Nothing) Then
                        Dim bgColor As Color

                        If boolPrint Then
                            bgColor = Color.White
                        Else
                            bgColor = Color.FromArgb(245, 245, 245)
                        End If
                        Dim StockBitMap As Bitmap
                        Dim memStream As New MemoryStream()

                        Dim Chart1 As Chart = New Chart()
                        Chart1.Series.Add(New Series())
                        Chart1.Width = "400"

                        Dim strArray As String() = yValues.Split("|".ToCharArray())

                        Dim yVal As Double() = New Double(strArray.Length - 1) {}
                        For i As Integer = 0 To strArray.Length - 1
                            yVal(i) = Double.Parse(strArray(i))
                        Next

                        Dim xName As String() = xValues.Split("|".ToCharArray())

                        Chart1.Series(0).Points.DataBindXY(xName, yVal)

                        Chart1.Palette = System.Web.UI.DataVisualization.Charting.ChartColorPalette.Grayscale
                        Chart1.ImageType = System.Web.UI.DataVisualization.Charting.ChartImageType.Png

                        Chart1.ChartAreas.Add(New ChartArea())
                        Chart1.ChartAreas(0).Area3DStyle.Enable3D = True

                        Select Case chartType
                            Case "pie"
                                'Dim pc As New PieChart(bgColor)
                                'pc.CollectDataPoints(xValues.Split("|".ToCharArray()), yValues.Split("|".ToCharArray()))
                                'StockBitMap = pc.Draw()
                                'Chart1.Legends.Add(New Legend("Type"))
                                'Chart1.Legends("Type").Title = "Type"
                                'Chart1.Series(0).Legend = "Type"
                                Chart1.Series(0).ChartType = SeriesChartType.Pie
                        End Select

                        ' Render BitMap Stream Back To Client
                        'StockBitMap.Save(memStream, ImageFormat.Png)
                        'memStream.WriteTo(Response.OutputStream)

                        Chart1.SaveImage(memStream)
                        memStream.WriteTo(Response.OutputStream)
                    End If
                    ''End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub 'Page_Load

    End Class 'ChartGenerator 
End Namespace '
