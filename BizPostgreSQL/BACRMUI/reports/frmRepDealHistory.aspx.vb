Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports System.ComponentModel.Component

Namespace BACRM.UserInterface.Reports
    Public Class frmRepDealHistory : Inherits BACRMPage

       
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim dtDealHistory As DataTable
        Dim objPredefinedReports As New PredefinedReports
        Dim SortField As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then

                    GetUserRightsForPage(8, 65)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExport.Visible = False
                    End If
                    calFrom.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, -7, Date.UtcNow))
                    calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    DisplayRecords()
                    ' = "Reports"
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(9)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(9)")
                If ddlFilter.SelectedItem.Value = 5 Then
                    tdOrg.Attributes.Add("style", "display:""")
                Else : tdOrg.Attributes.Add("style", "display:none")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(calFrom.SelectedDate))
                objPredefinedReports.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)))
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.SortOrder = ddlFilter.SelectedItem.Value
                If ddlCompany.SelectedIndex = -1 Then
                    objPredefinedReports.Division = 0
                Else : objPredefinedReports.Division = ddlCompany.SelectedItem.Value
                End If

                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select
                objPredefinedReports.ReportType = 9
                dtDealHistory = objPredefinedReports.GetRepDealHstr
                Dim dv As DataView = New DataView(dtDealHistory)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgDeals.DataSource = dv
                dgDeals.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Dim intDivision As Integer
                If ddlCompany.SelectedIndex = -1 Then
                    intDivision = 0
                Else : intDivision = ddlCompany.SelectedItem.Value
                End If
                Response.Redirect("../reports/frmRepDealHistoryPrint.aspx?fdt=" & calFrom.SelectedDate & "&tdt=" & DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) & "&ReportType=" & (rdlReportType.SelectedIndex + 1) & "&Filter=" & ddlFilter.SelectedItem.Value & "&Div=" & intDivision & "&SortField=" & SortField, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 43
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgDeals_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDeals.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgDeals_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDeals.ItemCommand
            Try
                Dim OpporID, CompanyId, DivisionID, CRMTYPE, TerritryID, ContactID As Long
                If Not e.CommandName = "Sort" Then
                    OpporID = e.Item.Cells(0).Text()
                    CompanyId = e.Item.Cells(1).Text()
                    TerritryID = e.Item.Cells(2).Text()
                    DivisionID = e.Item.Cells(3).Text()
                    CRMTYPE = e.Item.Cells(4).Text()
                    ContactID = e.Item.Cells(5).Text()
                End If
                If e.CommandName = "Customer" Then
                    If CRMTYPE = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=DealHistory&CmpID=" & CompanyId & "&DivID=" & DivisionID & "& CRMTYPE=" & CRMTYPE & "&CntID=" & ContactID & "&SI1=" & SI1 & "&SI2=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                    ElseIf CRMTYPE = 2 Then

                    End If
                ElseIf e.CommandName = "Contact" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=DealHistory&CmpID=" & CompanyId & "&DivID=" & DivisionID & "& CRMTYPE=" & CRMTYPE & "&fdas89iu=098jfd&CntId=" & ContactID & "&SI1=" & SI1 & "&SI2=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                ElseIf e.CommandName = "Name" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=DealHistory&CmpID=" & CompanyId & "&opId=" & OpporID & "&DivID=" & DivisionID & "&CRMTYPE=" & CRMTYPE & "&CntID=" & ContactID & "&SI1=" & SI1 & "&SI2=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2, False)

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnCompanyGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCompanyGo.Click
            Try
                FillCustomer()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Public Function FillCustomer()
            Try
                
                With objCommon
                    .DomainID = Session("DomainID")
                    .Filter = Trim(txtCompName.Text) & "%"
                    .UserCntID = Session("UserContactID")
                    ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    ddlCompany.DataTextField = "vcCompanyname"
                    ddlCompany.DataValueField = "numDivisionID"
                    ddlCompany.DataBind()
                End With
                ddlCompany.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=FileName" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".xls")
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As New System.IO.StringWriter
                Dim htmlWrite As New HtmlTextWriter(stringWrite)
                ClearControls(dgDeals)
                dgDeals.GridLines = GridLines.Both
                dgDeals.HeaderStyle.Font.Bold = True
                dgDeals.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.End()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub ClearControls(ByVal control As Control)
            Try
                Dim i As Integer
                For i = control.Controls.Count - 1 To 0 Step -1
                    ClearControls(control.Controls(i))
                Next i

                If TypeOf control Is System.Web.UI.WebControls.Image Then control.Parent.Controls.Remove(control)
                If (Not TypeOf control Is TableCell) Then
                    If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                        Dim literal As New LiteralControl
                        control.Parent.Controls.Add(literal)
                        Try
                            literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                        Catch
                        End Try
                        control.Parent.Controls.Remove(control)
                    Else
                        If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                            Dim literal As New LiteralControl
                            control.Parent.Controls.Add(literal)
                            literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                            literal.Text = Replace(literal.Text, "white", "black")
                            control.Parent.Controls.Remove(control)
                        End If
                    End If
                End If
                Return
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
