<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCases.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmCases" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Cases</title>
    <script language="javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
        function AddContact() {
            if (document.form1.uwOppTab__ctl2_ddlAssocContactId.value == 0) {
                alert("Select Contact");
                document.form1.uwOppTab.tabIndex = 2;
                document.form1.uwOppTab__ctl2_ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('uwOppTab__ctl2_dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('uwOppTab__ctl2_dgContact_ctl' + str + '_txtContactID').value == document.form1.uwOppTab__ctl2_ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
            <label>
                From
            </label>
            <BizCalendar:Calendar ID="calFrom" runat="server" />
            <label>To</label>
            <BizCalendar:Calendar ID="calTo" runat="server" />
            <asp:Button ID="btnSearch" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
        </div>
            <div class="pull-right">
                 <asp:RadioButtonList ID="rdlReportType" CssClass="normal1" runat="server" AutoPostBack="True"
                    RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <asp:ListItem Value="3">Territory Selected</asp:ListItem>
                </asp:RadioButtonList>
            </div>
                </div>
            </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <div class="pull-left">
                <label>
                    Filter:
                </label>
                <asp:DropDownList ID="ddlFilter" CssClass="form-control" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="0">-- Select One --</asp:ListItem>
                                <asp:ListItem Value="1">Select All</asp:ListItem>
                            </asp:DropDownList>
            </div>
            <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnAddtoMyRtpList" CssClass="btn btn-primary" runat="server"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                <asp:LinkButton ID="btnChooseTeams" CssClass="btn btn-primary" runat="server">Choose Teams</asp:LinkButton>
                 <asp:LinkButton ID="btnChooseTerritories" CssClass="btn btn-primary" runat="server"
                                        >Choose Territories</asp:LinkButton>
                <asp:LinkButton ID="btnExportToExcel" CssClass="btn btn-primary" runat="server" ><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                <asp:LinkButton ID="btnPrint" CssClass="btn btn-primary" runat="server"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server"  ><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       <asp:PostBackTrigger ControlID="btnChooseTerritories" />
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                </asp:UpdatePanel>
                
            </div>
                </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Cases(Agent)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:DataList ID="dlCases" Width="100%" runat="server" RepeatDirection="Vertical">
        <AlternatingItemStyle CssClass="tr1"></AlternatingItemStyle>
        <ItemStyle></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <SelectedItemStyle CssClass="is"></SelectedItemStyle>
        <ItemTemplate>
            <b>
                <asp:LinkButton ID="button1" runat="server" CssClass="text_bold" Text='<%# DataBinder.Eval(Container.DataItem, "Nam") %>'
                    CommandName="Name" />
            </b>
        </ItemTemplate>
        <SelectedItemTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" class="text_bold">
                        <b>
                            <%# DataBinder.Eval(Container.DataItem, "Nam") %>
                        </b>
                        <asp:Label ID="lblAssignTo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcAssignTo") %>'
                            Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgReptCases" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" runat="server" BorderColor="white"
                            AllowSorting="True" AutoGenerateColumns="False">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ContactID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numCaseid" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="Company" HeaderText="Account,Division">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplCompany" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Company") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="vcCaseNumber" HeaderText="Case No">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplCases" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcCaseNumber") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="Status"></asp:BoundColumn>
                                <asp:BoundColumn DataField="bintCreatedDate" SortExpression="bintCreatedDate" HeaderText="Date & Time opened"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="intTargetResolveDate" HeaderText="Resolve Date">
                                    <ItemTemplate>
                                        <%# ReturnName(DataBinder.Eval(Container.DataItem, "intTargetResolveDate")) %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="textSubject" SortExpression="textSubject" HeaderText="Subject"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </SelectedItemTemplate>
    </asp:DataList>
    <asp:Button ID="btnGo" Style="display: none" runat="server"></asp:Button>
        </div>
</asp:Content>
