Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Partial Public Class frmCustomRptList
    Inherits BACRMPage : Dim strColumn As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then

                LoadDropDowns()
                If Session("List") <> "CustRpt" Then
                    Session("ListDetails") = Nothing
                    'txtCurrrentPage.Text = 1
                    'txtSortOrder.Text = "D"
                Else
                    Dim str As String()
                    str = Session("ListDetails").split(",")
                    txtSortColumn.Text = str(2)
                    txtSortChar.Text = str(0)
                    txtCurrrentPage.Text = str(1)
                    txtSortOrder.Text = str(3)
                End If
                Session("List") = "CustRpt"
                BindDataGrid()
            End If
            'If txtSortChar.Text <> "" Then
            '    'ViewState("SortChar") = txtSortChar.Text
            '    txtSortColumn.Text = "ReportId"
            '    txtSortOrder.Text = "A"
            '    BindDataGrid()
            '    txtSortChar.Text = ""
            'End If
            'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.frames['topframe'].SelectTabByValue('6');}else{ parent.frames['topframe'].SelectTabByValue('6'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDropDowns()
        Try
            Dim ds As DataSet
            Dim ojReports As New CustomReports
            ojReports.numCustModuleId = 0
            ds = ojReports.GetModules()
            ddlModule.DataSource = ds.Tables(0)
            ddlModule.DataTextField = "vcCustModuleName"
            ddlModule.DataValueField = "numCustModuleId"
            ddlModule.DataBind()
            ddlModule.Items.Insert(0, "--Select One--")
            ddlModule.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDataGrid()
        Try
            'Dim SortChar As Char
            'If ViewState("SortChar") <> "" Then
            '    SortChar = ViewState("SortChar")
            'Else : SortChar = "0"
            'End If
            

            Dim ObjReport As New CustomReports
            Dim dtTable As DataTable
            ObjReport.DomainID = Session("DomainId")
            ObjReport.UserCntID = Session("UserContactId")
            ObjReport.SortCharacter = txtSortChar.Text.Trim()

            'If txtCurrrentPage.Text.Trim <> "" Then
            '    ObjReport.CurrentPage = txtCurrrentPage.Text
            'Else : ObjReport.CurrentPage = 1
            'End If
            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            ObjReport.CurrentPage = txtCurrrentPage.Text.Trim()

            ObjReport.PageSize = Session("PagingRows")
            ObjReport.TotalRecords = 0
            If txtSortColumn.Text <> "" Then
                ObjReport.columnName = txtSortColumn.Text.Trim() 'ViewState("Column")
            Else : ObjReport.columnName = "ReportId"
            End If
            If txtSortOrder.Text = "D" Then
                ObjReport.columnSortOrder = "Desc"
            Else : ObjReport.columnSortOrder = "Asc"
            End If
            ObjReport.numCustModuleId = ddlModule.SelectedValue
            ObjReport.FileDescription = txtReportND.Text
            Session("ListDetails") = txtSortChar.Text.Trim() & "," & txtCurrrentPage.Text & "," & txtSortColumn.Text & "," & txtSortOrder.Text
            dtTable = ObjReport.getReportList()
            'If ObjReport.TotalRecords = 0 Then
            '    hidenav.Visible = False
            '    lblRecordCount.Text = 0
            'Else
            '    'hidenav.Visible = True
            '    'lblRecordCount.Text = String.Format("{0:#,###}", ObjReport.TotalRecords)
            '    'Dim strTotalPage As String()
            '    'Dim decTotalPage As Decimal
            '    'decTotalPage = lblRecordCount.Text / Session("PagingRows")
            '    'decTotalPage = Math.Round(decTotalPage, 2)
            '    'strTotalPage = CStr(decTotalPage).Split(".")
            '    'If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
            '    '    lblTotal.Text = strTotalPage(0)
            '    '    txtTotalPage.Text = strTotalPage(0)
            '    'Else
            '    '    lblTotal.Text = strTotalPage(0) + 1
            '    '    txtTotalPage.Text = strTotalPage(0) + 1
            '    'End If
            '    'txtTotalRecords.Text = lblRecordCount.Text
            'End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = ObjReport.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            dgReport.DataSource = dtTable
            dgReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            'txtCurrrentPage.Text = 1
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReport.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim reportId As Integer = e.Item.Cells(0).Text()
                Dim objReport As New CustomReports
                objReport.UserCntID = Session("UserContactId")
                objReport.DomainID = Session("DomainId")
                objReport.DeleteCustomReport(reportId)
                BindDataGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim hplrun As HyperLink
                Dim hplEdit As HyperLink
                Dim hplMyReport As HyperLink
                hplrun = e.Item.FindControl("hplrun")
                hplEdit = e.Item.FindControl("hplEdit")
                hplMyReport = e.Item.FindControl("hplMyReport")
                hplrun.NavigateUrl = "../reports/frmCustomReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=" & e.Item.Cells(0).Text
                hplEdit.NavigateUrl = "../reports/frmCustomReportWiz.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportId=" & e.Item.Cells(0).Text
                hplMyReport.Attributes.Add("onclick", "return AddToMyReport('" & e.Item.Cells(0).Text & "')")
                hplMyReport.NavigateUrl = "#"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgReport.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If txtSortColumn.Text.Trim() <> strColumn Then
                txtSortColumn.Text = strColumn
                txtSortOrder.Text = "A"
            Else
                If txtSortOrder.Text = "A" Then
                    txtSortOrder.Text = "D"
                Else : txtSortOrder.Text = "A"
                End If
            End If
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMyReport.Click
        Try
            If txtReportId.Text <> "" Then
                Dim ObjReport As New CustomReports
                ObjReport.ReportID = CInt(txtReportId.Text)
                ObjReport.UserCntID = Session("UserContactId")
                ObjReport.DomainID = Session("DomainId")
                ObjReport.AddToMyReports()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
        Try
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class