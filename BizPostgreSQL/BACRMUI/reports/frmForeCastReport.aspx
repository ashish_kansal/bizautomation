<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmForeCastReport.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmForeCastReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>ForeCast Report</title>
    <script language="javascript" type="text/javascript">
        function OpenForAmt(a) {
            window.open("../Forecasting/frmUpdateForAmt.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ForId=" + a, '', 'toolbar=no,titlebar=no,left=800, top=300,width=175,height=40,scrollbars=no,resizable=yes')
        }
        function OpenSelTeam() {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=19", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=19", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <asp:CheckBox ID="chkShowEmpFor" Checked="true" AutoPostBack="true" runat="server"
                                Text="Show Employee Forecasts" />
                            <asp:CheckBox ID="chkShowPartFor" runat="server" AutoPostBack="true" Text="Show Partner Forecasts" />
            </div>
            <div class="pull-right form-inline">
                <label>Forecasting Report for</label>
                <asp:DropDownList ID="ddlYear" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                <label>of Quarter</label>
                <asp:DropDownList ID="ddlQuarter" CssClass="form-control" runat="server" AutoPostBack="True"
                                >
                                <asp:ListItem Value="0" Selected="True">--Select One--</asp:ListItem>
                                <asp:ListItem Value="1">First Quarter</asp:ListItem>
                                <asp:ListItem Value="2">Second Quarter</asp:ListItem>
                                <asp:ListItem Value="3">Third Quarter</asp:ListItem>
                                <asp:ListItem Value="4">Fourth Quarter</asp:ListItem>
                            </asp:DropDownList>
                <label>Total Forecast (Amount):</label>
                <asp:Label ID="lblForecastamt" CssClass="text" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">

        
     <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                    <asp:LinkButton ID="btnTeams" runat="server" CssClass="btn btn-primary">Choose Teams</asp:LinkButton>
                             <asp:LinkButton ID="btnTer" runat="server" CssClass="btn btn-primary">Choose Territories</asp:LinkButton>
                    <asp:LinkButton ID="btnExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnTeams" />
                            <asp:PostBackTrigger ControlID="btnTer" />
                       <asp:PostBackTrigger ControlID="btnExcel" />
                       <asp:PostBackTrigger ControlID="btnCancel" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
            </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Forecasting Report (Revenue)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <table id="tblDetails" cellspacing="1" class="table table-responsive table-bordered" cellpadding="1" width="100%" border="0"
        runat="server">
        <tr class="hs">
            <th class="normalbol" width="10%">
                Month
            </th>
            <th class="normalbol" width="10%">
                Quota
            </th>
            <th class="normalbol" width="10%">
                Closed
            </th>
            <th class="normalbol" width="10%">
                Opportunity Pipeline
            </th>
            <th class="normalbol" width="20%">
                Opportunity Probability %
            </th>
            <th class="normalbol" width="20%">
                Opportunity Probable Amount
            </th>
            <th class="normalbol" width="20%">
                Forecast Amount
            </th>
        </tr>
    </table>
    <asp:PlaceHolder ID="phlDetails" runat="server"></asp:PlaceHolder>
    <asp:Button ID="btnGo" runat="server" Style="display: none"></asp:Button>
        </div>
</asp:Content>
