<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="ActivityReportCompany.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.ActivityReportCompany" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Activity Report</title>
    <script language="javascript" type="text/javascript">

        function opendetail(url) {
            //alert(url);
            window.opener.location.href = url;
            self.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Individual Performance Analysis
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" align="center" cellpadding="0" width="100%" class="aspTable">
        <tr>
            <td>
                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td class="normal1" align="right">
                            User Account:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblUserAcc" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Opportunities Opened:
                        </td>
                        <td class="normal1" width="50">
                            <asp:Label ID="lblOppOpened" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkOppOpened" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            <td class="normal1" align="right">
                                Total Number of Accounts Added:
                            </td>
                            <td class="normal1">
                                <asp:Label ID="lblTotAcc" runat="server"></asp:Label>
                            </td>
                            <td class="normal1">
                                <asp:HyperLink ID="lnkTotAcc" runat="server" Target="_blank" CssClass="hyperlink">Open List</asp:HyperLink>
                            </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Amount:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblOppAmt" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Communications Scheduled:
                        </td>
                        <td class="normal1" width="50">
                            <asp:Label ID="lblComSch" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkComSch" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Deals Closed(Won):
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealCloseWon" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkDealCloseWon" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Communications Completed:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblComComp" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkComComp" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Amount:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealWonAmt" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Communications Past Due:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblComPast" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkComPast" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Deals Closed(Lost):
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealLost" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="hplDealLostLink" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Tasks Scheduled:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTaskSch" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTaskSch" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Amount:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealLostAmt" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Tasks Completed:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTaskComp" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTaskComp" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Conversion (Won vs Lost)%:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblConversion" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Tasks Past Due:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTaskPast" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTaskPast" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Leads Promoted:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblLeadProm" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkLeadProm" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Cases Opened:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCaseOpen" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkCaseOpen" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Total Prospects Added:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTotPros" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTotPros" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Cases Completed:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCaseComp" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkCaseComp" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Number of Prospects promoted to Accounts:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblNoPros" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkNoPros" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Cases Past Due:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCasePast" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkCasePast" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
