Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Partial Class frmCaseComparison : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim SortChar As Char
            Dim dtLeads As DataTable
            Dim objLeads As New CLeads
            With objLeads
                .RepType = GetQueryStringVal("type")
                .Condition = GetQueryStringVal("RepType").Trim
                .startDate = GetQueryStringVal("stDate")
                .endDate = GetQueryStringVal("enDate")
                .strUserIDs = GetQueryStringVal("UserCntId")
                .TeamType = 5
            End With
            dtLeads = objLeads.GetPerAnaysCaseList
            dgList.DataSource = dtLeads
            dgList.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim hpl As HyperLink
                hpl = e.Item.FindControl("hplOpen")
                hpl.NavigateUrl = "../reports/ActivityReportCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&stDate=" & GetQueryStringVal("stDate") & "&enDate=" & GetQueryStringVal("enDate") & "&type=" & 2 & "&RepType=" & GetQueryStringVal("RepType") & "&UserID=" & e.Item.Cells(0).Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
