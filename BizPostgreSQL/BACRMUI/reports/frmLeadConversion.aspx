<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmLeadConversion.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmLeadConversion" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Lead Conversion</title>
    <script language="javascript" type="text/javascript">
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenTerritory(repNo) {
            window.open("../Reports/frmSelectTerritories.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
        function Lfprintcheck() {
            this.print()
            //history.back()
            // document.location.href = "frmLeadActivity.aspx";
        }

    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12 form-inline">
            <div class="pull-left">
             
                From
                <BizCalendar:Calendar ID="calFrom" runat="server" />
                To
                <BizCalendar:Calendar ID="calTo" runat="server" />
                <asp:Button ID="btnSearch" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
                    </div>
            
            <div class="pull-right">
                
                <asp:RadioButtonList ID="rdlReportType" runat="server" AutoPostBack="True" CssClass="normal1"
                    RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">My Self</asp:ListItem>
                    <asp:ListItem Value="2">Team Selected</asp:ListItem>
                    <%--<asp:ListItem Value="3">Territory S"elected</asp:ListItem>--%>
                </asp:RadioButtonList>
                    </div>
            
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12 form-inline">
             <div class="pull-left">
                
                 
                        <asp:Label ID="lblEmployee" runat="server">Employee or Partner</asp:Label>
               
               
                         <asp:DropDownList ID="ddlEmployee" CssClass="form-control" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                   
                        <asp:Label ID="lblLeads" runat="server">Leads</asp:Label>
                    
                        <asp:DropDownList ID="ddlLeads" CssClass="form-control" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="0">All</asp:ListItem>
                                <asp:ListItem Value="5">My Leads</asp:ListItem>
                                <asp:ListItem Value="1">Web Leads</asp:ListItem>
                                <asp:ListItem Value="2">Public Leads</asp:ListItem>
                            </asp:DropDownList>
                    
                </div>
            
                <div class="pull-right">
                   
                    <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                    <asp:LinkButton ID="btnChooseTeams" runat="server" CssClass="btn btn-primary" >Choose Teams</asp:LinkButton>
                     <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary" 
                                        OnClientClick="Lfprintcheck()"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp;Back</asp:LinkButton>
                </div>
                    
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <asp:RadioButtonList ID="rdlLeadConversion" runat="server" AutoPostBack="True" CssClass="normal1"
                    RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">% of Leads have been converted into Prospects</asp:ListItem>
                    <asp:ListItem Value="2">% of Leads have generated money (deals won) for us</asp:ListItem>
                </asp:RadioButtonList>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Lead Conversion(%)
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table9" CellPadding="0" CellSpacing="0" Height="400" runat="server"
        BackColor="#f5f5f5" Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell Height="10px"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow HorizontalAlign="Center">
            <asp:TableCell Width="10px"></asp:TableCell>
            <asp:TableCell CssClass="normal1">
                <asp:Image ID="ChartImageAssigned" runat="server" Visible="False" BorderWidth="0"></asp:Image><br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label
                    ID="AmountWon1" Text="" runat="server"> </asp:Label><br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label
                    ID="Label1" Text="" runat="server"> </asp:Label>
            </asp:TableCell>
            <asp:TableCell CssClass="normal1">
                <asp:Image ID="ChartImageOwned" runat="server" Visible="False" BorderWidth="0"></asp:Image><br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label
                    ID="AmountWon2" Text="" runat="server"> </asp:Label><br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label
                    ID="TotalAmountWon" Text="" runat="server"> </asp:Label>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Button ID="btnGo" runat="server" Style="display: none"></asp:Button>
</asp:Content>
