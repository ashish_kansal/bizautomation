<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOpenOppPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmOpenOppPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
                 <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
				        <TR>
					        <TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						        <asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					        </TD>
					        <TD class="print_head" align="right">
						        <b>Report Date :&nbsp;&nbsp;</b>
						        <asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				        </TR>
	              </table>
	              <br />
	            <table cellspacing="0" cellpadding="0" border="0" width="100%">
				        <tr>
					        <td class="rep_head" align="center">
						        Open Opportunity Report
					        </td>
				        </tr>
				        
	            </table>
	            <script language="Javascript" type="text/javascript">
				    setInterval("Lfprintcheck()",1000)
			    </script>
	            <br />
			    <asp:Table id="tblOpenOpportunity" runat ="server" width="100%" Height="400" GridLines ="none" BorderColor ="black"  CellPadding ="0" CellSpacing ="0" BorderWidth="1">
			        <asp:TableRow>
			            <asp:TableCell VerticalAlign ="top">
			                <asp:DataGrid ID="dgOpportunityOpen" CssClass ="dg" Width ="100%" runat ="server" BorderColor ="white"  AutoGenerateColumns ="false" AllowSorting="true">
			                <AlternatingItemStyle  CssClass="ais" />
			                <ItemStyle CssClass="is"></ItemStyle>
			                <HeaderStyle CssClass="hs"></HeaderStyle>	
			                    <Columns>
			                    <asp:BoundColumn DataField="numOppID" Visible ="false"></asp:BoundColumn>
			                    <asp:ButtonColumn DataTextField ="OpportunityName" CommandName ="Opportuntity" SortExpression ="OpportunityName" HeaderText ="<font color=white>Opportunity Name</font>"></asp:ButtonColumn>
			                    <asp:TemplateColumn HeaderText="<font color=white>Date Created</font>" SortExpression="bintCreatedDate">
			                    <ItemTemplate>
			                  <%# ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate"))%>
			                    </ItemTemplate>
			                    </asp:TemplateColumn> 
			                    <asp:BoundColumn DataField ="LastMileStoneCompleted" DataFormatString="{0:f}" SortExpression="LastMileStoneCompleted" HeaderText="<font Color=white>Last Milestone Completed (%)</font>"></asp:BoundColumn> 
			                    <asp:BoundColumn DataField ="LastStageCompleted"  SortExpression="LastStageCompleted" HeaderText="<font Color=white>Last Stage Completed</font>"></asp:BoundColumn> 
			                    <asp:BoundColumn DataField ="numRecOwner" Visible ="true" SortExpression="numRecOwner" HeaderText ="<font color=white >Record Owner</font>"></asp:BoundColumn>
			                    <asp:BoundColumn DataField ="numAssignedTo" Visible ="true" SortExpression="numAssignedTo" HeaderText ="<font color=white> Assigned To</font>"></asp:BoundColumn>
			                    <asp:BoundColumn DataField ="monPAmount" DataFormatString="{0:#,#0.00}" Visible = "true" SortExpression ="monPAmount" HeaderText ="<font color=white>Deal Amount</font>"></asp:BoundColumn>
			                    </Columns>	
			                </asp:DataGrid>
			            </asp:TableCell>
			        </asp:TableRow>
			    </asp:Table>
			    <script language="JavaScript"  type="text/javascript">
                     function Lfprintcheck()
                     {
	                    this.print()
	                    //history.back()
	                    document.location.href = "frmOpenOpportunities.aspx";
                     }
			    </script>
    </form>
</body>
</html>
