﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmCustomReportRun.aspx.vb" Inherits=".frmCustomReportRun" ValidateRequest="false" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Custom Report List</title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("th:contains('Serial No')").css({ width: "400px" });
        });
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }
       #tblReport a{
            text-decoration:underline;
        }
        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                Report Name :
                <asp:Label runat="server" ID="lblReportName" CssClass="normal7"></asp:Label>
                Report Description :
                <asp:Label runat="server" ID="lblReportDesc" CssClass="normal7"></asp:Label>
                Report Generated Date :
                <asp:Label runat="server" ID="lblGeneratedOn" CssClass="normal7"></asp:Label>
            </div>
             <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                         <asp:LinkButton runat="server" ID="btnEdit" CssClass="btn btn-primary" ><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnExpToExcel" CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp;Export To Excel</asp:LinkButton>
                                    <%--<asp:Button Text="Export To Pdf" runat="server" ID="btnExpToPdf" CssClass="button" />--%>
                                    <asp:LinkButton runat="server" ID="btnAddMyreport" CssClass="btn btn-primary" ><i class="fa fa-plus-circle"></i>&nbsp;Add To My Reports</asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnclose" CssClass="btn btn-primary" ><i class="fa fa-times-circle-o"></i>&nbsp;Close</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnEdit" />
                       <asp:PostBackTrigger ControlID="btnExpToExcel" />
                       <asp:PostBackTrigger ControlID="btnAddMyreport" />
                       <asp:PostBackTrigger ControlID="btnclose" />
                   </Triggers>
                </asp:UpdatePanel>
                
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Custom Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive" id="divReportData" runat="server">

            </div>
        </div>
    </div>

    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
</asp:Content>
