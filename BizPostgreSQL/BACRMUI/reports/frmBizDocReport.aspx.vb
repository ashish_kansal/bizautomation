Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Reports
    Public Class frmBizDocReport : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnExportToExcel As System.Web.UI.WebControls.Button
        Protected WithEvents btnChooseTeams As System.Web.UI.WebControls.Button
        Protected WithEvents btnChooseTerritories As System.Web.UI.WebControls.Button
        Protected WithEvents btnPrint As System.Web.UI.WebControls.Button
        Protected WithEvents btnBack As System.Web.UI.WebControls.Button
        Protected WithEvents Table9 As System.Web.UI.WebControls.Table
        Protected WithEvents ddlSortOrder As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlChooseBizDoc As System.Web.UI.WebControls.DropDownList
        Protected WithEvents rdlReportType As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents dgBizDocReport As System.Web.UI.WebControls.DataGrid
        Protected WithEvents btnAddtoMyRtpList As System.Web.UI.WebControls.Button
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim dtBizDocReport As DataTable
        Dim objPredefinedReports As New PredefinedReports
        Dim SortField As String
       
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then
                    
                    GetUserRightsForPage(8, 56)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else : If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    End If
                     ' = "Reports"
                    Dim ds As DataSet
                    Dim objBizDoc As New PredefinedReports
                    ds = objBizDoc.GetBizDocType.DataSet
                    ddlChooseBizDoc.Items.Add("-- Select One --")
                    Dim intCount As Integer
                    For intCount = 0 To ds.Tables(0).Rows.Count - 1
                        ddlChooseBizDoc.Items.Add(New ListItem(ds.Tables(0).Rows(intCount).Item("vcData"), ds.Tables(0).Rows(intCount).Item("numListItemID")))
                    Next
                    DisplayRecords()
                End If
                btnChooseTeams.Attributes.Add("onclick", "return OpenSelTeam(23)")
                btnChooseTerritories.Attributes.Add("onclick", "return OpenTerritory(23)")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.UserCntID = Session("UserContactID")
                If ddlSortOrder.SelectedIndex > 0 Then
                    objPredefinedReports.SortOrder = ddlSortOrder.SelectedIndex
                Else : objPredefinedReports.SortOrder = 0
                End If

                If ddlChooseBizDoc.SelectedIndex > 0 Then
                    objPredefinedReports.ChooseBizDoc = ddlChooseBizDoc.SelectedValue
                Else : objPredefinedReports.ChooseBizDoc = 0
                End If

                Select Case rdlReportType.SelectedIndex
                    Case 0 : objPredefinedReports.UserRights = 1
                    Case 1 : objPredefinedReports.UserRights = 2
                    Case 2 : objPredefinedReports.UserRights = 3
                End Select

                objPredefinedReports.ReportType = 23
                dtBizDocReport = objPredefinedReports.GetBizDocReport

                Dim dv As DataView = New DataView(dtBizDocReport)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgBizDocReport.DataSource = dv
                dgBizDocReport.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub rdlReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdlReportType.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
            Try
                Server.Transfer("../reports/frmBizDocReportPrint.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReportType=" & rdlReportType.SelectedIndex & "&sortorder=" & ddlSortOrder.SelectedIndex & "&ChooseBizDoc=" & ddlChooseBizDoc.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../reports/reportslinks.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlSortOrder_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSortOrder.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlChooseBizDoc_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlChooseBizDoc.SelectedIndexChanged
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgBizDocReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgBizDocReport.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddtoMyRtpList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddtoMyRtpList.Click
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.UserCntID = Session("UserContactID")
                objPredefinedReports.ReportID = 33
                objPredefinedReports.AddToMyReportList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgBizDocReport_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocReport.ItemCommand
            Try
                If e.CommandName = "Company" Then
                    If e.Item.Cells(3).Text = 1 Then
                        Response.Redirect("../prospects/frmProspects.aspx?frm=BizDocReport&CmpID=" & e.Item.Cells(0).Text & "&DivID=" & e.Item.Cells(1).Text & "&CRMTYPE=1&CntID=" & e.Item.Cells(2).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    ElseIf e.Item.Cells(3).Text = 2 Then
                        Response.Redirect("../Account/frmAccounts.aspx?frm=BizDocReport&CmpID=" & e.Item.Cells(0).Text & "&klds+7kldf=fjk-las&DivId=" & e.Item.Cells(1).Text & "&CRMTYPE=2&CntID=" & e.Item.Cells(2).Text & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm1 & "&frm2=" & frm2)

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgBizDocReport_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocReport.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(12).Attributes.Add("onclick", "return OpenBizInvoice('" & e.Item.Cells(4).Text & "','" & e.Item.Cells(5).Text & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                ExportToExcel.DataGridToExcel(dgBizDocReport, Response)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                DisplayRecords()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace