<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmGeneratedCustomReport.aspx.vb" Inherits="frmGeneratedCustomReport"%>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Generated Custom Report</title>
		<SCRIPT language="JavaScript" src="../javascript/CustomReports.js" type="text/javascript"></SCRIPT>
	</HEAD>
	<body >
		<form id="frmGeneratedCustomReport" method="post" runat="server">
		<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>
			<asp:table id="tblCustomReports" Runat="server" GridLines="None" Width="100%"
				CellSpacing="0" CellPadding="0" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<table cellSpacing="0" id="tblCustomReportHeaderSeaction" runat="Server" cellPadding="0" border="0">
							<tr>
								<td vAlign="bottom" align="right" colspan="3">
									<asp:button id="btnAddToMyReport" Runat="server" Text="Add to My Reports" CssClass="button" style="padding-right:5px;"></asp:button>
									<asp:button id="btnClose" Runat="server" Text="Close" CssClass="button" style="padding-right:5px;"></asp:button>
									<asp:button id="btnDeleteCustomReport" Runat="server"  CssClass="button Delete" Text="X" style="padding-right:5px;"></asp:button>
									<asp:button id="btnBackToMyReports" Runat="server" Text="Back" CssClass="button" style="padding-right:5px;"></asp:button>
									<asp:button id="btnExportToExcel" Runat="server" Text="Export to Excel" CssClass="button"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="bottom" width="130">
									<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
										<tr>
											<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp; Generated Report 
												&nbsp;&nbsp;&nbsp;
											</td>
										</tr>
									</table>
								</td>
								<td valign="middle" align="left" class="Text_bold">
									<asp:Label ID="lblTotalRecordCount" Runat="server"></asp:Label>
									<asp:literal id="litClientMessage" Runat="server" EnableViewState="False"></asp:literal>
									<asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
								</td>
								<td valign="top" align="right">
									<table>
										<tr>
											<td>
												<asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
											<td class="normal1">
												<asp:linkbutton id="lnk2" runat="server" CausesValidation="False">2</asp:linkbutton></td>
											<td class="normal1">
												<asp:linkbutton id="lnk3" runat="server" CausesValidation="False">3</asp:linkbutton></td>
											<td class="normal1">
												<asp:linkbutton id="lnk4" runat="server" CausesValidation="False">4</asp:linkbutton></td>
											<td class="normal1">
												<asp:linkbutton id="lnk5" runat="server" CausesValidation="False">5</asp:linkbutton></td>
											<td>
												<asp:linkbutton id="lnkFirst" runat="server" CausesValidation="False">
													<div class="LinkArrow"><<</div>
												</asp:linkbutton></td>
											<td>
												<asp:linkbutton id="lnkPrevious" runat="server" CausesValidation="False">
													<div class="LinkArrow"><</div>
												</asp:linkbutton></td>
											<td class="normal1">
												<asp:label id="lblPage" runat="server">Page</asp:label></td>
											<td>
												<asp:textbox id="txtCurrentPage" runat="server" Width="18px" CssClass="signup" MaxLength="5"
													AutoPostBack="True" Text="1"></asp:textbox></td>
											<td class="normal1">&nbsp;
												<asp:label id="lblOf" runat="server">of</asp:label></td>
											<td class="normal1">
												<asp:label id="lblTotal" runat="server"></asp:label></td>
											<td>
												<asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow" CausesValidation="False">
													<div class="LinkArrow">></div>
												</asp:linkbutton></td>
											<td>
												<asp:linkbutton id="lnkLast" runat="server" CausesValidation="False">
													<div class="LinkArrow">>></div>
												</asp:linkbutton></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<asp:table id="tblCustomReportContainer" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server"  CssClass="aspTable"
							Width="100%" BorderColor="black" GridLines="None" Height="415">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<asp:DataGrid id="dgCustomReport" runat="server" AllowSorting="True" Width="100%" AutoGenerateColumns="True"
										CssClass="dg" AlternatingItemStyle-CssClass="is" ItemStyle-CssClass="ais" HeaderStyle-CssClass="hs"
										HeaderStyle-Wrap="False" HeaderStyle-ForeColor="White" BorderWidth="1px"
										CellPadding="0" CellSpacing="0" ShowHeader="True" ShowFooter="True" FooterStyle-CssClass="hs"
										HeaderStyle-Height="24px"></asp:DataGrid>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table><asp:textbox id="txtColumnSorted" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtSortOrder" Runat="server" Visible="False"></asp:textbox>
			<asp:literal id="litClientSideScript" Visible="False" Runat="Server"></asp:literal>
			</ContentTemplate>
			<Triggers>
			<asp:PostBackTrigger ControlID="btnExportToExcel" />
			</Triggers>
			</asp:updatepanel>

			<div id="divColHeader" style="Z-INDEX: 101; LEFT: 300px; BACKGROUND-IMAGE: none; VISIBILITY: hidden; WIDTH: 350px; POSITION: absolute; TOP: 50px; HEIGHT: 100px; BACKGROUND-COLOR: transparent">
				<table cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<td vAlign="bottom">
							<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
								<tr>
									<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp;Add to My 
										Reports&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</TR>
				</table>
				<table style="BORDER-COLLAPSE: collapse" borderColor="black" height="75" cellSpacing="0"
					cellPadding="0" width="100%" bgColor="white" border="1">
					<tr>
						<td>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td vAlign="middle" align="right" colSpan="2" height="24"><input class="button" id="btnAddToMyReports" onclick="makeDataTransfer(document.frmGeneratedCustomReport.txtReportName,document.frmGeneratedCustomReport.txtReportDescription,'divColHeader');"
											type="button" value="Add to My Reports" name="btnAddToMyReports"> <input class="button" id="btnCloseAddToMyReports" onclick="HideDivElement('divColHeader')"
											type="button" value="Close" name="btnCloseAddToMyReports">&nbsp;
									</td>
								</tr>
								<tr>
									<td class="normal1" vAlign="middle" align="right" width="30%">Report Name
									</td>
									<td class="normal1" vAlign="middle">&nbsp;&nbsp;<input class="signup" id="txtReportName" type="text" maxLength="100" Width="130" Height="176">
									</td>
								</tr>
								<tr>
									<td class="normal1" vAlign="middle" align="right">Report Description
									</td>
									<td valign="middle" class="normal1">
										&nbsp;&nbsp;<input class="signup" id="txtReportDescription" type="text" Width="130" Height="176" maxlength="50">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
