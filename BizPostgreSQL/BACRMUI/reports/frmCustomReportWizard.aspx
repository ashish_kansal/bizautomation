<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false"  EnableViewState="true" CodeBehind="frmCustomReportWizard.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmCustomReportWizard" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Custom Report Builder</title>
    <script language="JavaScript" src="../javascript/CustomReports.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
      function AssignedToList(ControlId,ControlName,numFieldGroupID,boolSummation,ControlType)
            {
                this.ControlName = ControlName;
		        this.ControlId = ControlId;	            
		        this.numFieldGroupID = numFieldGroupID;
		        this.boolSummation = boolSummation;
		        this.ControlType = ControlType;
            }
            function BindSortOrderList()
            {
                
                var ultraTab = igtab_getTabById("uwOppTab");
                //var selectedIndex = ultraTab.getSelectedIndex();
                ultraTab.setSelectedIndex(2);
                var index = 0
                var i;
                for(i=document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').options.length-1;i>=0;i--)
                    {
                     document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').remove(i);
                    }

                for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
	                {	
	                    if (document.getElementById('uwOppTab__ctl1_'+ arrFieldConfig[iIndex].ControlId).value != null)
	                    {
	                      if(document.getElementById('uwOppTab__ctl1_'+ arrFieldConfig[iIndex].ControlId).checked == true)
	                      { 
    	                      document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').options[index] = new Option(arrFieldConfig[iIndex].ControlName,arrFieldConfig[iIndex].ControlId +'~'+ arrFieldConfig[iIndex].ControlType +'~'+ arrFieldConfig[iIndex].boolSummation )	                    
    	                      index = index +1;
	                      }
	                      else
	                      {
	                        
	                      }
	                    }
	                }
                //alert(uwOppTab__ctl1_vcFirstName~6)
                return false
            }
            function BindFilterList()
            {
                var ultraTab = igtab_getTabById("uwOppTab");
                //var selectedIndex = ultraTab.getSelectedIndex();
                ultraTab.setSelectedIndex(3);
                var index = 0
                var i;
                document.getElementById('uwOppTab__ctl0_lstGroup')
                 document.getElementById('uwOppTab__ctl3_list').innerHTML = ""
               for (var i =0;i<=4;i++)
               {
                        
                    var strspan50 ;
		            strspan50 = "<table class='normal1'><tr id=tr1><td id=td1>"
		            strspan50 = strspan50 + "Select Field <select id=ddlField"+i+" name=ddlField"+i+" style='WIDTH: 150px'  class='signup'>"
		             strspan50=strspan50+"<OPTION value='0'>--Select One--</OPTION>"			           			    
		            if (document.getElementById('uwOppTab__ctl0_lstGroup').value == 4 || document.getElementById('uwOppTab__ctl0_lstGroup').value == 5)
		              {
		                  strspan50=strspan50+"<optgroup label='Contacts'>"		              
                          for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
	                        {
    	                       
	                    	    if (arrFieldConfig[iIndex].numFieldGroupID == 6)
	                               {
                                    strspan50=strspan50+"<OPTION value="+arrFieldConfig[iIndex].ControlId+'~'+arrFieldConfig[iIndex].ControlType+">"+ arrFieldConfig[iIndex].ControlName+"</OPTION>"			           			    
                                   }
	                        }
	                        strspan50 = strspan50 + "</optgroup>"
	                   }
	                   
	                    if (document.getElementById('uwOppTab__ctl0_lstGroup').value == 5 ||document.getElementById('uwOppTab__ctl0_lstGroup').value == 6 ||document.getElementById('uwOppTab__ctl0_lstGroup').value == 7)
		              { 
	                         strspan50=strspan50+"<optgroup label='Accounts'>"		              
                                for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
	                            {	if (arrFieldConfig[iIndex].numFieldGroupID == 7)
	                                   {
                                        strspan50=strspan50+"<OPTION value="+arrFieldConfig[iIndex].ControlId+'~'+arrFieldConfig[iIndex].ControlType+">"+ arrFieldConfig[iIndex].ControlName+"</OPTION>"			           			    
                                       }
	                            }
	                            strspan50 = strspan50 + "</optgroup>"
	                   }
	                     strspan50 = strspan50 + "</select></td><td>"
	                     strspan50 = strspan50 + " <select class='signup' id=Operator"+i+" name=Operator"+i+" >"
                         strspan50 = strspan50 + " <option value='='>equals</option>"
                         strspan50 = strspan50 + "<option value='!='>not equal to</option>"
                         strspan50 = strspan50 + "<option value='<'>less than</option>"
                         strspan50 = strspan50 + "<option value='>'>greater than</option>"
                         strspan50 = strspan50 + "<option value='<='>less or equal</option>"
                         strspan50 = strspan50 + "<option value='>='>greater or equal</option>"
                         strspan50 = strspan50 + "<option value='Like'>contains</option>"
                          strspan50 = strspan50 + "<option value='Not Like'>does not contain</option>"
                         strspan50 = strspan50 + "<option value=sw>starts with</option>"                         
                         strspan50 = strspan50 + "<option value=Ew>end with</option>"  
                         strspan50 = strspan50 + "</select>"
                         strspan50 = strspan50 + "&nbsp;<input class='signup' type='text' title=Value"+i+" name=Value"+i+" id=Value"+i+" maxlength='1000' value=''>"
                         strspan50 = strspan50 + "</td></tr><table>"
                  /*   strspan50 = strspan50 + "</select></td><td><input type='text' title='Value 1' name='pv1' id='pv1' maxlength='1000' value=""></td></tr><table>"*/
	                 document.getElementById('uwOppTab__ctl3_list').innerHTML = document.getElementById('uwOppTab__ctl3_list').innerHTML + strspan50;
	            }
               
                return false
            }
            function AddFilterRow()
            {   
               
                   var i = Math.abs(document.getElementById('noFilterRows').value) + 1
                   
                    document.getElementById('noFilterRows').value = i
                 var strspan50 ;
		            strspan50 = "<table class='normal1'><tr id=tr1><td id=td1>"
		            strspan50 = strspan50 + "Select Field <select id=ddlField"+i+" name=ddlField"+i+" style='WIDTH: 150px'  class='signup'>"
		             strspan50=strspan50+"<OPTION value='0'>--Select One--</OPTION>"			           			    
		            
		              if (document.getElementById('uwOppTab__ctl0_lstGroup').value == 4 || document.getElementById('uwOppTab__ctl0_lstGroup').value == 5)
		              {
		                  strspan50=strspan50+"<optgroup label='Contacts'>"		              
                          for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
	                        {
    	                       
	                    	    if (arrFieldConfig[iIndex].numFieldGroupID == 6)
	                               {
                                    strspan50=strspan50+"<OPTION value="+arrFieldConfig[iIndex].ControlId+'~'+arrFieldConfig[iIndex].ControlType+">"+ arrFieldConfig[iIndex].ControlName+"</OPTION>"			           			    
                                   }
	                        }
	                        strspan50 = strspan50 + "</optgroup>"
	                   }
	                   
	                    if (document.getElementById('uwOppTab__ctl0_lstGroup').value == 5 ||document.getElementById('uwOppTab__ctl0_lstGroup').value == 6 ||document.getElementById('uwOppTab__ctl0_lstGroup').value == 7)
		              { 
	                         strspan50=strspan50+"<optgroup label='Accounts'>"		              
                                for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
	                            {	if (arrFieldConfig[iIndex].numFieldGroupID == 7)
	                                   {
                                        strspan50=strspan50+"<OPTION value="+arrFieldConfig[iIndex].ControlId+'~'+arrFieldConfig[iIndex].ControlType+">"+ arrFieldConfig[iIndex].ControlName+"</OPTION>"			           			    
                                       }
	                            }
	                            strspan50 = strspan50 + "</optgroup>"
	                   }
	              
	                     strspan50 = strspan50 + "</select></td><td>"
	                     strspan50 = strspan50 + " <select class='signup' id=Operator"+i+" name=Operator"+i+" >"
                         strspan50 = strspan50 + " <option value='='>equals</option>"
                         strspan50 = strspan50 + "<option value='!='>not equal to</option>"
                         strspan50 = strspan50 + "<option value='<'>less than</option>"
                         strspan50 = strspan50 + "<option value='>'>greater than</option>"
                         strspan50 = strspan50 + "<option value='<='>less or equal</option>"
                         strspan50 = strspan50 + "<option value='>='>greater or equal</option>"
                         strspan50 = strspan50 + "<option value='Like'>contains</option>"
                         strspan50 = strspan50 + "<option value='Not Like'>does not contain</option>"
                         strspan50 = strspan50 + "<option value=sw>starts with</option>"                         
                         strspan50 = strspan50 + "<option value=Ew>end with</option>"                         
                         strspan50 = strspan50 + "</select>"
                         strspan50 = strspan50 + "&nbsp;<input class='signup' type='text' name=Value"+i+" id=Value"+i+" maxlength='1000' value=''>"
                         strspan50 = strspan50 + "</td></tr><table>"
                  /*   strspan50 = strspan50 + "</select></td><td><input type='text' title='Value 1' name='pv1' id='pv1' maxlength='1000' value=""></td></tr><table>"*/
	                 document.getElementById('uwOppTab__ctl3_list').innerHTML = document.getElementById('uwOppTab__ctl3_list').innerHTML + strspan50;
	                 return false;
            }
            function Summation()
            {
               
                alert('in summation')
                    var ultraTab = igtab_getTabById("uwOppTab");
                //var selectedIndex = ultraTab.getSelectedIndex();
                ultraTab.setSelectedIndex(4);
                    // var i = 0
                     
                      var length
                    document.getElementById('uwOppTab__ctl4_spnSummation').innerHTML = ""
                    for (var i =0;i<=4;i++)
                    {
                          strspan50 = "<table class='normal1'><tr id=tr1><td id=td1>"
		                  strspan50 = strspan50 + "Select Field <select id=ddlFieldSummation"+i+" name=ddlFieldSummation"+i+" style='WIDTH: 150px'  class='signup'>"
		                  strspan50=strspan50+"<OPTION value='0'>--Select One--</OPTION>"			           			    
//		                  for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
                          for(var iIndex=0;iIndex<document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').length;iIndex++)
		                  
	                        {
    	                       
	                    	    if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value.split('~')[3] == 'True')
	                               {
	                                
                                    strspan50=strspan50+"<OPTION value="+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value.split('~')[0]+">"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].text+"</OPTION>"			           			    
                                   }
	                        }
	                     strspan50 = strspan50 + "</select></td><td>"
	                     strspan50 = strspan50 + " <select class='signup' id=OperatorSummation"+i+" name=OperatorSummation"+i+" >"
                         strspan50 = strspan50 + " <option value='Sum'>Sum</option>"
                         strspan50 = strspan50 + "<option value='Avg'>Average</option>"
                         strspan50 = strspan50 + "<option value='Max'>Largest Value</option>"
                         strspan50 = strspan50 + "<option value='Min'>Smallest Value</option>"  
                         strspan50 = strspan50 + "</select>"
                         strspan50 = strspan50 + "</td></tr><table>"
                         document.getElementById('uwOppTab__ctl4_spnSummation').innerHTML = document.getElementById('uwOppTab__ctl4_spnSummation').innerHTML + strspan50;
                    }
		          
                         
                         return false
            }
            function AddSummationRow()
            {
                //alert(document.getElementById('noSummationRows').value)
                    var ultraTab = igtab_getTabById("uwOppTab");
                //var selectedIndex = ultraTab.getSelectedIndex();
                ultraTab.setSelectedIndex(4);
                    var i = Math.abs(document.getElementById('noSummationRows').value) + 1
                    alert(i)
                    document.getElementById('noSummationRows').value = i
                      var strspan50 ;
                 
		            strspan50 = "<table class='normal1'><tr id=tr1><td id=td1>"
		            strspan50 = strspan50 + "Select Field <select id=ddlFieldSummation"+i+" name=ddlFieldSummation"+i+" style='WIDTH: 150px'  class='signup'>"
		             strspan50=strspan50+"<OPTION value='0'>--Select One--</OPTION>"			           			    
		                for(var iIndex=0;iIndex<document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').length;iIndex++)
		                  
	                        {
    	                       
	                    	    if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value.split('~')[3] == 'True')
	                               {
                                    strspan50=strspan50+"<OPTION value="+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value.split('~')[0]+">"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].text+"</OPTION>"			           			    
                                   }
	                        }
	                     strspan50 = strspan50 + "</select></td><td>"
	                     strspan50 = strspan50 + " <select class='signup' id=OperatorSummation"+i+" name=OperatorSummation"+i+" >"
                         strspan50 = strspan50 + " <option value='Sum'>Sum</option>"
                         strspan50 = strspan50 + "<option value='Avg'>Average</option>"
                         strspan50 = strspan50 + "<option value='Max'>Largest Value</option>"
                         strspan50 = strspan50 + "<option value='Min'>Smallest Value</option>"                    
                         strspan50 = strspan50 + "</select>"
                         strspan50 = strspan50 + "</td></tr><table>"
                         document.getElementById('uwOppTab__ctl4_spnSummation').innerHTML = document.getElementById('uwOppTab__ctl4_spnSummation').innerHTML + strspan50;
                         return false
            }
            function Report()
            {
                 var ultraTab = igtab_getTabById("uwOppTab");
                // ultraTab.setSelectedIndex(5);
                 
                 document.getElementById('txtColumnsSelected').value=document.getElementById('uwOppTab__ctl1_spnSelColumns').innerHTML
                 document.getElementById('txtColumnlist').value=document.getElementById('uwOppTab__ctl2_spnListOrder').innerHTML                 
                 document.getElementById('txtspanList').value = document.getElementById('uwOppTab__ctl3_list').innerHTML
                 document.getElementById('txtspanSummation').value = document.getElementById('uwOppTab__ctl4_spnSummation').innerHTML
                 
                  var Fields = ""
                 
                  for (var iIndex=0;iIndex<document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').length;iIndex++)
                  {
                    if (iIndex == 0)
                    {
                          
                            
                            
                            
                       if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[2] == 'Contact')
                        {   
                          if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[1] == 6)
                          {
                               Fields = "dbo.fn_GetContactName(ACI."+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"                            
                          }
                          else
                          {
                               Fields = "dbo.fn_GetContactName(" + document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"                          
                          }
                          
                        }
                        else if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[2]== 'Drop Down List')
                        {
                             if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[1] == 6)
                          {
                               Fields = "dbo.fn_GetListItemName(ACI."+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                          }
                          else
                          {
                             Fields = "dbo.fn_GetListItemName(" + document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                          }
                           
                        }
                        else
                        {
                              if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[1] == 6)
                          {
                             Fields = "ACI."+document.getElementById("uwOppTab__ctl2_lstReportColumnsOrder")(iIndex).value.split('~')[0] + " as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                          }
                          else
                          {
                             Fields = document.getElementById("uwOppTab__ctl2_lstReportColumnsOrder")(iIndex).value.split('~')[0] + " as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                          }
                           
                        }
                        
                    }
                    else
                    {
                       if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[2] == 'Contact')
                        {
                              if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[1] == 6)
                              {
                                Fields = Fields+",dbo.fn_GetContactName(ACI."+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                              }
                              else
                              {
                                Fields = Fields+",dbo.fn_GetContactName(" + document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                              }
                        }
                        else if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[2]== 'Drop Down List')
                        {
                            if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[1] == 6)
                              {
                                Fields = Fields+",dbo.fn_GetListItemName(ACI."+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                              }
                              else
                              {
                                Fields = Fields+",dbo.fn_GetListItemName(" + document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + ") as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                              }
                            
                        }
                        else
                        {
                            if (document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[1] == 6)
                              {
                                Fields = Fields+",ACI."+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + " as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                              }
                              else
                              {
                                Fields = Fields+"," + document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0] + " as  '"+ document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).text+"'"
                              }
                            
                        }
                        
                        
                        //Fields = Fields+","+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')(iIndex).value.split('~')[0]
                    }
                  }
                 
                  document.getElementById('txtFields').value = Fields
                  // alert(document.getElementById('txtFields').value)
                  /*
                    =================================================================================================
                  */
                //  var iFilters = 0
                var length = document.getElementById('noFilterRows').value
                var Rel = "And"
                alert(document.getElementById("uwOppTab__ctl3_radAnd").checked)
                if (document.getElementById("uwOppTab__ctl3_radAnd").checked)
                {
                     Rel = " And "
                }
                else
                {
                     Rel = " Or "
                }
                alert(Rel)
                var FilterFields=""
                  for (var iFilters = 0 ; iFilters < length ;iFilters++)
                  {
                    if (iFilters == 0)
                    {
                        if (document.getElementById('ddlField'+iFilters).value != 0)
                          {
                                    
                                if (document.getElementById('ddlField'+iFilters).value.split('~')[1] == 6)
                                {
                                        
                                     if (document.getElementById('Operator'+iFilters).value=="Like")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {
                                                FilterFields =" dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =" dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields = " ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"                                        
                                           }                                       
                                        
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Not Like")
                                     {
                                            if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields = "  dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields = " dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =" ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="sw")
                                     {
                                            if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields = " dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =" dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields = " ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Ew")
                                     {
                                         
                                          if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields = " dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =" dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields = " ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                         
                                         
                                     }
                                     else
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =" dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =" dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =" ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                        
                                     }
                                    
                                }
                                else
                                {
                                     if (document.getElementById('Operator'+iFilters).value=="Like")
                                     {
                                        
                                         if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =Rel + " dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =Rel + " dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =Rel  +document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                        
                                        
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Not Like")
                                     {
                                            if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =Rel + " dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =Rel + " dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =Rel +document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"                                            
                                           }
                                         
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="sw")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =Rel + " dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =Rel + " dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =Rel +document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '"+document.getElementById('Value'+iFilters).value+"%'"                                            
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Ew")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =Rel + " dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =Rel + " dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =Rel +document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                         
                                     }
                                     else
                                     {
                                          if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =Rel + " dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =Rel +" dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =Rel +document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                        
                                     }
                                    
                                }
                            
                          }
                    }
                    else
                    {
                        if (document.getElementById('ddlField'+iFilters).value != 0)
                          {
                          
                               if (document.getElementById('ddlField'+iFilters).value.split('~')[1] == 6)
                                {
                                      if (document.getElementById('Operator'+iFilters).value=="Like")
                                     {
                                          if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields + Rel +"  dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                        
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Not Like")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+" And ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="sw")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Ew")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                         
                                     }
                                     else
                                     {
                                          if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName(ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                        
                                     }
                                     
                                  //  FilterFields =FilterFields+"And ACI."+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                }
                                else
                                {
                                    if (document.getElementById('Operator'+iFilters).value=="Like")
                                     {
                                          if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  "+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"    
                                           }
                                        
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Not Like")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  "+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '%"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="sw")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  "+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '"+document.getElementById('Value'+iFilters).value+"%'"
                                           }
                                         
                                     }
                                     else if (document.getElementById('Operator'+iFilters).value=="Ew")
                                     {
                                           if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  "+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" Like '%"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                         
                                     }
                                     else
                                     {
                                          if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Contact')
                                           {                                                
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetContactName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else if (document.getElementById('ddlField'+iFilters).value.split('~')[2]=='Drop')
                                           {
                                                FilterFields =FilterFields+ Rel +"  dbo.fn_GetListItemName("+document.getElementById('ddlField'+iFilters).value.split('~')[0]+") "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                           else
                                           {
                                                FilterFields =FilterFields+ Rel +"  "+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                           }
                                        
                                     }
                                    //FilterFields =FilterFields+"And"+document.getElementById('ddlField'+iFilters).value.split('~')[0]+" "+document.getElementById('Operator'+iFilters).value+" '"+document.getElementById('Value'+iFilters).value+"'"
                                }
                            
                          }
                    }
                    
                  }
                  alert(FilterFields)
                 document.getElementById('txtFilterFields').value=""
                 document.getElementById('txtFilterFields').value=FilterFields
                 
                 /*
                 =============================================================================================================
                 =============================================================================================================
                 Summation Rows
                 =============================================================================================================
                 =============================================================================================================
                  */
                  var iSumationFields = ""
                  //alert(document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value.split('~')[0])
                  
                 for ( var iSumFilter = 0;iSumFilter<document.getElementById('noSummationRows').value;iSumFilter++)
                 {
                        
                        
                        if (document.getElementById('ddlFieldSummation'+iSumFilter).value != 0)
                        {
                            if (iSumFilter == 0)
                            {
                                if (document.getElementById('ddlFieldSummation'+iSumFilter).value.split('~')[1]==6)
                                {
                                    iSumationFields = document.getElementById('OperatorSummation'+iSumFilter).value +"(ACI."+document.getElementById('ddlFieldSummation'+iSumFilter).value.split('~')[0]+") as " +document.getElementById('ddlFieldSummation'+iSumFilter)[document.getElementById('ddlFieldSummation'+iSumFilter).selectedIndex].text
                                }
                                else
                                {
                                    iSumationFields = document.getElementById('OperatorSummation'+iSumFilter).value +"("+document.getElementById('ddlFieldSummation'+iSumFilter).value.split('~')[0]+") as " +document.getElementById('ddlFieldSummation'+iSumFilter)[document.getElementById('ddlFieldSummation'+iSumFilter).selectedIndex].text
                                }    
                            }
                            else
                            {
                            
                                if (document.getElementById('ddlFieldSummation'+iSumFilter).value.split('~')[1]==6)
                                {
                                    iSumationFields = iSumationFields+","+document.getElementById('OperatorSummation'+iSumFilter).value +"(ACI."+document.getElementById('ddlFieldSummation'+iSumFilter).value.split('~')[0]+") as " +document.getElementById('ddlFieldSummation'+iSumFilter)[document.getElementById('ddlFieldSummation'+iSumFilter).selectedIndex].text
                                }
                                else
                                {
                                    iSumationFields = iSumationFields+","+document.getElementById('OperatorSummation'+iSumFilter).value +"("+document.getElementById('ddlFieldSummation'+iSumFilter).value.split('~')[0]+") as " +document.getElementById('ddlFieldSummation'+iSumFilter)[document.getElementById('ddlFieldSummation'+iSumFilter).selectedIndex].text
                                }
                            }
                            
                        }
                 }
                 document.getElementById('txtSummationFields').value = iSumationFields               
                 alert( document.getElementById('txtSummationFields').value)
                  
                  
            }
            function Save()
            {
                if(document.getElementById('txtReportName').value == "")
                {
                    alert("Enter The Report Name")
                    return false
                }
                else
                {
	                
                var CheckedFields=""
                var IsChecked=""
                for(var iIndex=0;iIndex<arrFieldConfig.length-1;iIndex++)
	                {	
	                    if (document.getElementById('uwOppTab__ctl1_'+ arrFieldConfig[iIndex].ControlId).value != null)
	                    {
	                        if (iIndex==0)
	                        {
	                             CheckedFields= CheckedFields + arrFieldConfig[iIndex].ControlId
    	                         IsChecked = IsChecked+document.getElementById('uwOppTab__ctl1_'+ arrFieldConfig[iIndex].ControlId).checked
    	                    }
    	                    else
    	                    {
    	                         CheckedFields= CheckedFields +"|"+ arrFieldConfig[iIndex].ControlId
    	                         IsChecked = IsChecked+"|"+document.getElementById('uwOppTab__ctl1_'+ arrFieldConfig[iIndex].ControlId).checked
    	                    }
	                    }	                    
	                }
	                document.getElementById('txtCheckedFields').value=""
	                document.getElementById('txtIsChecked').value=""
	                document.getElementById('txtCheckedFields').value=CheckedFields
	                document.getElementById('txtIsChecked').value=IsChecked
	                alert(CheckedFields)
	                alert(IsChecked)
	              
	                /*
	                ====OrderList
	                */
	                 var OrderFieldsValue=""
                     var OrderFieldsText=""
	                 for(var iIndex=0;iIndex<document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder').length;iIndex++)
		                  
	                        {
	                            if (iIndex==0)
	                            {
	                                  OrderFieldsValue=document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value
    	                              OrderFieldsText= document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].text 
    	                        }
    	                        else
    	                        {
    	                            OrderFieldsValue= OrderFieldsValue+'|'+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].value
    	                            OrderFieldsText= OrderFieldsText +'|'+document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder')[iIndex].text                             
    	                        }
	                        }
	                      alert(OrderFieldsValue)
	                     alert(OrderFieldsText)   
	                     document.getElementById('txtOrderFieldsValue').value=OrderFieldsValue
	                     document.getElementById('txtOrderFieldsText').value=OrderFieldsText
	                   /*
	                   FilterList
	                   */     
	                   var length = document.getElementById('noFilterRows').value
                        var FilterFieldsText=""
                        var FilterFieldsValue=""
                        var FilterFieldsOperator=""
                        var FilterValue=""
                          for (var iFilters = 0 ; iFilters < length ;iFilters++)
                          {
                            if (iFilters == 0)
                            {
                                var FilterFieldsText0=""
                                var FilterFieldsValue0=""
                                var FilterFieldsOperator0=""
                                var FilterValue0=""
                                if (document.getElementById('ddlField'+iFilters).value != 0)
                                  {
                                    FilterFieldsValue0 = document.getElementById('ddlField'+iFilters).value
                                    FilterFieldsText0 =  document.getElementById('ddlField'+iFilters)[document.getElementById('ddlField'+iFilters).selectedIndex].text
                                    FilterFieldsOperator0 = document.getElementById('Operator'+iFilters).value
                                    FilterValue0 = document.getElementById('Value'+iFilters).value
                                     FilterFieldsText = FilterFieldsText0
                                    FilterFieldsValue =FilterFieldsValue0
                                    FilterFieldsOperator =FilterFieldsOperator0
                                    FilterValue = FilterValue0
                                  }                                 
                                 
                            }
                            else
                            {
                                var FilterFieldsText0=""
                                var FilterFieldsValue0=""
                                var FilterFieldsOperator0=""
                                var FilterValue0=""
                                if (document.getElementById('ddlField'+iFilters).value != 0)
                                  {
                                
                                    FilterFieldsValue0 = document.getElementById('ddlField'+iFilters).value
                                    FilterFieldsText0 =  document.getElementById('ddlField'+iFilters)[document.getElementById('ddlField'+iFilters).selectedIndex].text
                                    FilterFieldsOperator0 = document.getElementById('Operator'+iFilters).value
                                    FilterValue0 = document.getElementById('Value'+iFilters).value
                                    FilterFieldsText =FilterFieldsText+'|'+ FilterFieldsText0
                                    FilterFieldsValue =FilterFieldsValue+'|'+FilterFieldsValue0
                                    FilterFieldsOperator =FilterFieldsOperator+'|'+FilterFieldsOperator0
                                    FilterValue = FilterValue+'|'+FilterValue0
                                      
                                  }                                 
                                
                            }
                          }
                          document.getElementById('txtFilterFieldsText').value=FilterFieldsText
                          document.getElementById('txtFilterFieldsValue').value=FilterFieldsValue
                          document.getElementById('txtFilterFieldsOperator').value=FilterFieldsOperator
                          document.getElementById('txtFilterValue').value=FilterValue
                           alert(FilterFieldsText)
                          alert(FilterFieldsValue)
                          alert(FilterFieldsOperator)
                          alert(FilterValue)
                          
                          var lengthSummation = document.getElementById('noSummationRows').value
                        var sumFieldsText=""
                        var sumrFieldsValue=""
                        var sumFieldsOperator=""
                       // var FilterValue=""
                           for (var iFilters = 0 ; iFilters < lengthSummation ;iFilters++)
                          {
                            if (iFilters == 0)
                            {
                               if (document.getElementById('ddlFieldSummation'+iFilters).value != 0)
                                  {
                                    sumrFieldsValue = document.getElementById('ddlFieldSummation'+iFilters).value
                                    sumFieldsText =  document.getElementById('ddlFieldSummation'+iFilters)[document.getElementById('ddlFieldSummation'+iFilters).selectedIndex].text
                                    sumFieldsOperator = document.getElementById('OperatorSummation'+iFilters).value                                 
                                  } 
                            }
                            else
                            {
                                    sumrFieldsValue =sumFieldsText+'|'+document.getElementById('ddlFieldSummation'+iFilters).value
                                    sumFieldsText = sumFieldsText+'|'+document.getElementById('ddlFieldSummation'+iFilters)[document.getElementById('ddlFieldSummation'+iFilters).selectedIndex].text
                                    sumFieldsOperator = sumFieldsOperator+'|'+document.getElementById('OperatorSummation'+iFilters).value  
                            }
                          }
                          document.getElementById('txtsumFieldsText').value=sumFieldsText
                          document.getElementById('txtsumrFieldsValue').value=sumrFieldsValue
                          document.getElementById('txtsumFieldsOperator').value=sumFieldsOperator
                          alert(sumFieldsText)
                          alert(sumrFieldsValue)
                          alert(sumFieldsOperator)
	                return true
	             }
            }
            
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:textbox ID="txtSummation" Text="" runat="server" style=" display:none"></asp:textbox>
                <asp:textbox ID="noFilterRows" Text="4" runat="server" style=" display:none"></asp:textbox>
                <asp:textbox ID="noSummationRows" Text="4" runat="server" style=" display:none"></asp:textbox>
                 <asp:literal id="litClientScript" Runat="server" EnableViewState="False"></asp:literal>
                 <asp:textbox ID="txtReportId" Text="0" runat="server" style=" display:none"></asp:textbox>
                <asp:textbox ID="txtspanList" Text="" runat="server" style=" display:none"></asp:textbox>
                 <asp:textbox ID="txtspanSummation" Text="" runat="server" style=" display:none"></asp:textbox>                  
                  <asp:textbox ID="txtColumnlist" Text="" runat="server" style=" display:none"></asp:textbox>
                   <asp:textbox ID="txtColumnsSelected" Text="" runat="server" style=" display:none"></asp:textbox>                  
                 <asp:textbox ID="txtFields" Text="" runat="server"  style=" display:none"></asp:textbox>
                 <asp:textbox ID="txtFilterFields" Text="" runat="server" style=" display:none" ></asp:textbox>
                   <asp:textbox ID="txtSummationFields" Text="" runat="server" style=" display:none"></asp:textbox>
                  
                   <asp:textbox ID="txtCheckedFields" Text="" runat="server"  style=" display:none" ></asp:textbox>
                  <asp:textbox ID="txtIsChecked" Text="" runat="server"  style=" display:none" ></asp:textbox>
                   <asp:textbox ID="txtOrderFieldsValue" Text="" runat="server"  style=" display:none" ></asp:textbox>
                  <asp:textbox ID="txtOrderFieldsText" Text="" runat="server" style=" display:none"  ></asp:textbox>
                 <asp:textbox ID="txtFilterFieldsText" Text="" runat="server" style=" display:none"  ></asp:textbox>
                  <asp:textbox ID="txtFilterFieldsValue" Text="" runat="server"  style=" display:none" ></asp:textbox>
                 <asp:textbox ID="txtFilterFieldsOperator" Text="" runat="server"  style=" display:none" ></asp:textbox>
                   <asp:textbox ID="txtFilterValue" Text="" runat="server"  style=" display:none" ></asp:textbox>
                  <asp:textbox ID="txtsumFieldsText" Text="" runat="server"  style=" display:none" ></asp:textbox>
                  <asp:textbox ID="txtsumrFieldsValue" Text="" runat="server"  style=" display:none" ></asp:textbox>
                   <asp:textbox ID="txtsumFieldsOperator" Text="" runat="server"  style=" display:none" ></asp:textbox>
                <asp:textbox ID="txtSql" Text="" runat="server"  style=" display:none" ></asp:textbox>
               <asp:textbox ID="Textbox1" Text="" runat="server"   TextMode="MultiLine" Width="100%" style=" display:none"></asp:textbox> 
                   
    <br />
    <br />
        <table width="100%">
            <tr>
                <td align="right" class="normal1">
                    Report Name :<asp:TextBox CssClass="signup" runat="server" id= "txtReportName" Width="250px" MaxLength="150"></asp:TextBox>
                    Report Description :<asp:TextBox CssClass="signup" runat="server" id= "txtReportDesc" Width="250px" MaxLength="300"></asp:TextBox>
                    <asp:Button Text="Save Report " CssClass="button" ID ="btnSave" runat="server" />
                </td>
            </tr>
        </table>
      	        <igtab:ultrawebtab  EnableViewState="true"  AutoPostBack="false" ImageDirectory="" style="POSITION:absolute;z-index:101;" id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                      <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                        <igtab:Tab Text="&nbsp;&nbsp;Report Type&nbsp;&nbsp;" >
                            <ContentTemplate>                                    
                                    <asp:table id="tblContacts" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTable"
									Height="300">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
											<br>
											 <table class="normal1">
											        
											        <tr>
											            <td  colspan="2">
											                Select The Module From Which Data is to be Fetched  :<asp:DropDownList AutoPostBack="true" runat="server" ID="ddlModule" Width="200px" CssClass="signup"></asp:DropDownList>
											            </td>
											        </tr>
											        <tr>
											            <td>
											                Select The Group From Which Data is to be Fetched  :
											            </td>
											            <td>
											                <asp:ListBox runat="server" ID="lstGroup" Width="200px" Height="200px" CssClass="signup"></asp:ListBox>
											            </td>
											        </tr>	
											        <tr >
											            <td colspan="2">
    											            <asp:button CssClass="button" Text="Next" runat="server" ID="btnNext1" />
											            </td>
											        </tr>										 
											 </table>
										</asp:TableCell>
										</asp:TableRow>
									</asp:table>
                            
                            </ContentTemplate>
                        </igtab:Tab>
                        <igtab:Tab Text="&nbsp;&nbsp;Report Fields &nbsp;&nbsp;">
                            <ContentTemplate>
                                <asp:Table Runat="server" ID="Table2" width="100%" BorderWidth="1" cellspacing="0" GridLines="Both"
									cellpadding="0" BorderColor="Black">
									<asp:TableRow>
									    
									    <asp:TableCell ColumnSpan="2">
									        <span runat="server" id="spnSelColumns"></span>
									    </asp:TableCell>
									
									</asp:TableRow>
									</asp:Table>
                                	<asp:Table Runat="server" ID="tblReportColumns" width="100%" BorderWidth="1" cellspacing="0" GridLines="Both"
									cellpadding="0" BorderColor="Black">
									<asp:TableRow>
									    
									    <asp:TableCell ColumnSpan="2">
									        <asp:button CssClass="button" Text="Next" runat="server" ID="btnNext2" />											        
									    </asp:TableCell>
									
									</asp:TableRow>
									
								</asp:Table>                            
                            </ContentTemplate>
                        </igtab:Tab>
                         <igtab:Tab Text="&nbsp;&nbsp;Column Order &nbsp;&nbsp;">
                            <ContentTemplate>
                                <asp:Table Runat="server" ID="tblColumnsOrder" width="100%" BorderWidth="1px" cellspacing="2"
									cellpadding="2" BorderColor="Black">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1" width="30%" style="PADDING-Left:50px;">
											<span style="FONT-WEIGHT: bold">Set Order for your Selected Columns</span><br>
											<span style="FONT-STYLE: italic">(Fields at the top will appear on the left and first 100 columns will be displayed only)</span><br>
											<span runat="server" id="spnListOrder">
											<asp:ListBox id="lstReportColumnsOrder" runat="server" CssClass="signup" Width="250" Height="200"
												EnableViewState="true"></asp:ListBox></span>
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1" width="5%">
											&nbsp;<input type="button" id="btnMoveupTopColumns" Class="button" value="    Top   " onclick="javascript:moveTop(document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder'))">
											<br>
											<br />
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="button" id="btnMoveupColumns" Class="button" style="FONT-FAMILY: Webdings"
												value="5" onclick="javascript:MoveUp(document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder'))">
											<br>
											<br>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="button" id="btnMoveDownColumns" Class="button" style="FONT-FAMILY: Webdings"
												value="6" onclick="javascript:MoveDown(document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder'))">
											<br><br>
											&nbsp;<input type="button" id="btnMoveupLastColumns" Class="button" value="Bottom" onclick="javascript:moveBottom(document.getElementById('uwOppTab__ctl2_lstReportColumnsOrder'))">
										</asp:TableCell>
										<asp:TableCell VerticalAlign="Middle" CssClass="normal1">&nbsp;</asp:TableCell>
									</asp:TableRow>
										<asp:TableRow>
									    <asp:TableCell ColumnSpan="2">
                                        
                                            <asp:button CssClass="button" Text="Next" runat="server" ID="btnNext3" />											        
									    </asp:TableCell>
									
									</asp:TableRow>
								</asp:Table>                           
                            </ContentTemplate>
                        </igtab:Tab>
                      
                         <igtab:Tab Text="&nbsp;&nbsp;Filters &nbsp;&nbsp;">
                            <ContentTemplate>
                                	<asp:Table Runat="server" ID="tblFilters" width="100%" BorderWidth="1" cellspacing="0" GridLines="Both"
									cellpadding="0" BorderColor="Black">
									<asp:TableRow>
									    <asp:TableCell>	
									    <asp:Label runat="server" Text="&nbsp;&nbsp;&nbsp;Select Whether And/Or for the filters" CssClass="normal1"></asp:Label>							         
									        <asp:RadioButton ID="radAnd" Checked="true" CssClass="signup" runat="server" Text="And" GroupName="AndOr" />
									        <asp:RadioButton ID="radOr" CssClass="signup" runat="server" Text="Or" GroupName="AndOr" />
									    </asp:TableCell>
									</asp:TableRow>
									<asp:TableRow>
									    <asp:TableCell>
									    <span id="list" runat="server"></span>
									        <table id="tblAddFilters" class="normal1">
									        
									        </table>
									    
									    </asp:TableCell>
									
									</asp:TableRow>
									
									<asp:TableRow>
									    <asp:TableCell ColumnSpan="2">
									        <asp:button CssClass="button" Text="ADD" runat="server" ID="btnAddFilterRow" />
									        <asp:button CssClass="button" Text="Next" runat="server" ID="btnNext4" />											        
									    </asp:TableCell>
									
									</asp:TableRow>
									
								</asp:Table>                            
                            </ContentTemplate>
                        </igtab:Tab>
                        
                         <igtab:Tab Text="&nbsp;&nbsp;Summation&nbsp;&nbsp;">
                            <ContentTemplate>
                                	<asp:Table Runat="server" ID="Table1" width="100%" BorderWidth="1" cellspacing="0" GridLines="Both"
									cellpadding="0" BorderColor="Black">
									
									<asp:TableRow>    								
									    <asp:TableCell>
									    <span id="spnSummation" runat="server"></span>
									    
									    </asp:TableCell>
									
									</asp:TableRow>
									
									<asp:TableRow>
									    <asp:TableCell ColumnSpan="2">
									        <asp:button CssClass="button" Text="ADD" runat="server" ID="btnAddSummationRow" />
									        <asp:button CssClass="button" Text="Next" runat="server" ID="Button2" />
									        <asp:button CssClass="button" Text="Report" runat="server" ID="btnReport" />
									        											        
									    </asp:TableCell>
									
									</asp:TableRow>
									
								</asp:Table>                            
                            </ContentTemplate>
                        </igtab:Tab>
                           <igtab:Tab Text="&nbsp;&nbsp;Report Preveiw&nbsp;&nbsp;">
                            <ContentTemplate>
                            <br />
                                	<asp:Table Runat="server" ID="tblReport" width="100%" BorderWidth="1" cellspacing="0" 
									cellpadding="0" BorderColor="Black" Height="300">
									<asp:TableRow  VerticalAlign="Top">
									    <asp:TableCell>
									    <asp:datagrid id="dgReport" AllowSorting="true" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="True"
							                BorderColor="white">
							                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							                <ItemStyle CssClass="is"></ItemStyle>
							                <HeaderStyle CssClass="hs"></HeaderStyle>
							            </asp:datagrid>
									    </asp:TableCell>			    
									
									</asp:TableRow>								
									
								</asp:Table>                            
                            </ContentTemplate>
                        </igtab:Tab>
                      </Tabs>
                </igtab:ultrawebtab>
                 
             
    </form>
</body>
</html>
