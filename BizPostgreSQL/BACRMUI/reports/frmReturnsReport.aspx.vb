﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmReturnsReport
        Inherits BACRMPage
        Dim objOpportunity As MOpportunity
       
        
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                GetUserRightsForPage(10, 14)
                If Not IsPostBack Then
                    BindGrid()
                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then
                        btnExport.Visible = False
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub BindGrid()
            Try
                If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .CurrentPage = 1
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .Mode = 0
                    .DivisionID = 0 'used to get items specific to division
                End With

                dgSalesReturns.DataSource = objOpportunity.GetReturnsReport
                dgSalesReturns.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment;filename=FileName" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".xls")
            'Response.Charset = ""
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.ContentType = "application/vnd.xls"
            'Dim stringWrite As New System.IO.StringWriter
            'Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'ClearControls(dgSalesReturns)
            'dgSalesReturns.GridLines = GridLines.Both
            'dgSalesReturns.HeaderStyle.Font.Bold = True
            'dgSalesReturns.RenderControl(htmlWrite)
            'Response.Write(stringWrite.ToString())
            'Response.End()
            ExportToExcel.DataGridToExcel(dgSalesReturns, Response)
        End Sub
        'Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)

        'End Sub
        'Sub ClearControls(ByVal control As Control)
        '    Try
        '        Dim i As Integer
        '        For i = control.Controls.Count - 1 To 0 Step -1
        '            ClearControls(control.Controls(i))
        '        Next i

        '        If TypeOf control Is System.Web.UI.WebControls.Image Then control.Parent.Controls.Remove(control)

        '        If (Not TypeOf control Is TableCell) Then
        '            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
        '                Dim literal As New LiteralControl
        '                control.Parent.Controls.Add(literal)
        '                Try
        '                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
        '                Catch
        '                End Try
        '                control.Parent.Controls.Remove(control)
        '            Else
        '                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
        '                    Dim literal As New LiteralControl
        '                    control.Parent.Controls.Add(literal)
        '                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
        '                    literal.Text = Replace(literal.Text, "white", "black")
        '                    control.Parent.Controls.Remove(control)
        '                End If
        '            End If
        '        End If
        '        Return
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../Opportunity/frmSalesReturns.aspx?type=1", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class

End Namespace