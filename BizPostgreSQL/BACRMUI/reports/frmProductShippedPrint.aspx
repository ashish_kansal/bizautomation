<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProductShippedPrint.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmProductShippedPrint"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>frmProductShippedPrint</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">		
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
		

			<br>
			<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="print_head" align="left"><b>Report By :</b>&nbsp;&nbsp;
						<asp:Label Runat="server" ID="lblreportby" CssClass="print_head" Width="129px"></asp:Label>
					</TD>
					<TD class="print_head" align="right">
						<b>Report Date :&nbsp;&nbsp;</b>
						<asp:Label ID="lblcurrent" Runat="server" Width="62px" CssClass="print_head"></asp:Label></TD>
				</TR>
			</TABLE>
			<BR>
			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tr>
					<td class="rep_head" align="center">
						<asp:Label ID="lblReportHeader" Runat="server">Product Shipped</asp:Label>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table cellpadding="1" cellspacing="1" border="0">
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label3" runat="server" Font-Names="Verdana" Font-Size="9px" Visible="TRUE" Width="38px"> From :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lblfromdt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="62px"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="normal1" align="right">
									<asp:Label id="Label4" runat="server" Font-Names="Verdana" Font-Size="9px"> Upto :</asp:Label>&nbsp;&nbsp;</TD>
								<TD class="normal1" align="left">
									<asp:Label id="lbltodt" runat="server" Font-Names="Verdana" Font-Size="9px" Width="59px"></asp:Label></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
			<script language="Javascript">
				setInterval("Lfprintcheck()",1000)
			</script>
			<BR>
			<asp:datagrid id="dgProductRec" Width="100%" CssClass="dg" Runat="server" BorderColor="white"
				AllowSorting="True" AutoGenerateColumns="False">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="numCompanyID" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="numDivisionID" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="ContactID" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="numOppID" Visible="False"></asp:BoundColumn>
					<asp:TemplateColumn SortExpression="LastLoggedIn" HeaderText="<font color=white>Date of Event</font>">
						<ItemTemplate>
							<%# ReturnName(DataBinder.Eval(Container.DataItem, "bintShippedDate")) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Event" SortExpression="Event" HeaderText="<font color=white>Event</font>"></asp:BoundColumn>
					<asp:ButtonColumn CommandName="Opp" DataTextField="vcPOppName" SortExpression="vcPOppName" HeaderText="<font color=white>Order/Deal ID</font>"></asp:ButtonColumn>
					<asp:ButtonColumn CommandName="Company" DataTextField="Company" SortExpression="Company" HeaderText="<font color=white>Shipping Company</font>"></asp:ButtonColumn>
					<asp:BoundColumn DataField="vcTrackingURL" SortExpression="vcTrackingURL" HeaderText="<font color=white>Tracking URL</font>"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			

			<script language="JavaScript">
			function Lfprintcheck()
			{
				this.print()
				//history.back()
				document.location.href = "frmProductShipped.aspx";
			}
			</script>
		</form>
	</body>
</HTML>
