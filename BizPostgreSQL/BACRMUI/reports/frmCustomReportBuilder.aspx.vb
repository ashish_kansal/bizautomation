''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmCustomReportBuilder
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is the Custom Report Builder Initiation Interface
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	10/27/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Class frmCustomReportBuilder
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim objCustomReports As New CustomReports
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time thepage is called. In this event we will 
    '''     get the data from the DB create the form.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/27/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Put user code to initialize the page here
             ' = "Reports"
            With objCustomReports
                .DomainID = Session("DomainID")                                 'Set the Domain ID
                .UserCntID = Session("UserContactID")                                 'Set the Domain ID
                .XMLFilePath = CCommon.GetDocumentPhysicalPath() 'set the file path
            End With
            If Not IsPostBack Then                                              'The form loads for the first time
                callToBindRelationships()                                       'Call to databing the relationships drop down
                callToBindContactTypes()                                        'Call to databing the Contact Types drop down
                'callFunctionForTeamsAndTerritories()                            'Call the fucntion which binds the teams and territories for this user
                callToBindAdditionalTriggers()                                  'Attach client events to form elements
                rbReportType.Checked = True                                     'The Radio button is checked by default
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to bind the relationship drop down list
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/27/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindRelationships()
        Try
            Dim dtRelations As DataTable                                            'declare a datatable
            dtRelations = objCustomReports.getRelationshipsDetails(5) 'call to get the relationships
            Dim dRow As DataRow                                                     'declare a datarow
            dRow = dtRelations.NewRow()                                             'get a new row object
            dtRelations.Rows.InsertAt(dRow, 0)                                      'insert the row
            dtRelations.Rows(0).Item("numItemID") = 0                               'set the value for first entry
            dtRelations.Rows(0).Item("vcItemName") = "All Relationships"            'set the text for the first entry
            dtRelations.Rows(0).Item("vcItemType") = "L"                            'type= list
            dtRelations.Rows(0).Item("flagConst") = 0                               'requried by the stored procedure
            ddlRelationShips.DataSource = dtRelations.DefaultView                   'set the datasource
            ddlRelationShips.DataTextField = "vcItemName"                           'set the text attribute
            ddlRelationShips.DataValueField = "numItemID"                           'set the value attribute
            ddlRelationShips.DataBind()                                             'databind the drop down
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to bind the Contact Types drop down list
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/27/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindContactTypes()
        Try
            Dim dtContactTypes As DataTable                                         'declare a datatable
            dtContactTypes = objCustomReports.getRelationshipsDetails(8) 'call to get the Contact Types
            Dim dRow As DataRow                                                     'declare a datarow
            dRow = dtContactTypes.NewRow()                                          'get a new row object
            dtContactTypes.Rows.InsertAt(dRow, 0)                                   'insert the row
            dtContactTypes.Rows(0).Item("numItemID") = 0                            'set the value for first entry
            dtContactTypes.Rows(0).Item("vcItemName") = "All Types"                 'set the text for the first entry
            dtContactTypes.Rows(0).Item("vcItemType") = "L"                         'type= list
            dtContactTypes.Rows(0).Item("flagConst") = 0                            'requried by the stored procedure
            ddlContactTypes.DataSource = dtContactTypes.DefaultView                 'set the datasource
            ddlContactTypes.DataTextField = "vcItemName"                            'set the text attribute
            ddlContactTypes.DataValueField = "numItemID"                            'set the value attribute
            ddlContactTypes.DataBind()                                              'databind the drop down
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to initiate the binding of Teams and Territories with the hidden variables.
    ''' </summary>
    ''' <remarks>
    '''     The teams and territories which are selected for the user are stored in hidden variables. If no specifically
    '''     done for the user then all those which are assigned are taken into view
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    'Function callFunctionForTeamsAndTerritories()
    '    Dim objUserAccess As New UserAccess                                 'Create an User Access Object
    '    objUserAccess.UserId = Session("UserId")                            'Set the User Id
    '    objUserAccess.DomainID = Session("DomainID")                        'Set the Domain id

    '    Dim dtTeams As DataTable                                            'Declare a DataTable
    '    dtTeams = objCustomReports.getSelectedTeamsForUser()                'Get the Teams selected for the user
    '    If dtTeams.Rows.Count = 0 Then                                      'If there are no teams selected specifically
    '        dtTeams = objUserAccess.GetTeamForUsers                         'Get the Assigned Teams
    '    End If
    '    If dtTeams.Rows.Count = 0 Then
    '        rdlReportType.Items(1).Attributes.Add("style", "disabled:true;") 'Disable the radio
    '    Else
    '        Dim drTeamRows As DataRow                                       'Declare a datarow
    '        Dim sTeamList As New System.Text.StringBuilder                  'Declare a String Builder object
    '        For Each drTeamRows In dtTeams.Rows                             'Loop through the teams
    '            sTeamList.Append(drTeamRows.Item("numListItemId") & ",")    'Keep appendng the team Id
    '        Next
    '        hdTeams.Value = sTeamList.ToString(0, sTeamList.ToString.Length - 1) 'Remove the last comma
    '    End If

    '    Dim dtTerritories As DataTable                                      'Declare a DataTable for Territories
    '    dtTerritories = objCustomReports.getSelectedTerritoriesForUser      'Get the Territories selected for the user
    '    If dtTerritories.Rows.Count = 0 Then                                'If there are no territories selected specifically
    '        dtTerritories = objUserAccess.GetTerritoryForUsers              'Get the Assigned Territories
    '    End If
    '    If dtTerritories.Rows.Count = 0 Then
    '        rdlReportType.Items(2).Attributes.Add("style", "disabled:true;") 'Disable the radio
    '    Else
    '        Dim drTerritoryRows As DataRow                                  'Declare a datarow
    '        Dim sTerritoryList As New System.Text.StringBuilder             'Declare a String Builder object
    '        For Each drTerritoryRows In dtTerritories.Rows                  'Loop through the territories
    '            sTerritoryList.Append(drTerritoryRows.Item("numTerritoryId") & ",") 'Keep appendng the territory Id
    '        Next
    '        hdTerritories.Value = sTerritoryList.ToString(0, sTerritoryList.ToString.Length - 1) 'Remove the last comma
    '    End If
    'End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to attach additional client side events to form controls
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/27/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindAdditionalTriggers()
        Try
            rbIncludeContacts.Attributes.Add("onclick", "IncludeExcludeContactTypes();") 'Call to add client events when the radios are clicked
            btnRunReport.Attributes.Add("onclick", "return ReAssessReportGenerationCriteria('OpenReport');") 'Call to add client events to re-access the criteria for the generation of the Report
            btnAddToMyReports.Attributes.Add("onclick", "ReAssessReportGenerationCriteria('SaveReport');") 'Call to add client events to re-access the criteria for adding the Report as My Reports
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is raised when any of the Report's Go button is clicked
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/28/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Public Sub btnGetReportParameters_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Dim numReportOptionsTypeId As Integer                           'Declare a variable to store the id of the data type for report
            If e.CommandName = "OrgReport" Then                             'if Organization Report is requested
                numReportOptionsTypeId = 1                                  'Org Report is represented by 1
            ElseIf e.CommandName = "OppItemsReport" Then                    'if Opportunities and Items Report is requested
                numReportOptionsTypeId = 2                                  'Org Report is represented by 2
            ElseIf e.CommandName = "LeadsReport" Then                       'if Leads Report is requested
                numReportOptionsTypeId = 3                                  'Org Report is represented by 3
            End If
            hdReportType.Value = numReportOptionsTypeId                     'Set the Report Type in hidden variable
            callToBindReportFields(numReportOptionsTypeId)                  'Call to display the Report Config Form
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function follows a common set of steps which results in displaying a new Custom Report Config Form
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/05/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindReportFields(ByVal numReportOptionsTypeId As Integer)
        Try
            If numReportOptionsTypeId = 1 Then                              'Org Report is represented by 1
                lblReportType.Text = "Organizations"                        'Display the Type of the Report
            ElseIf numReportOptionsTypeId = 2 Then                          'Org Report is represented by 2
                lblReportType.Text = "Opportunities & Items"                'Display the Type of the Report
            ElseIf numReportOptionsTypeId = 3 Then                          'Org Report is represented by 3
                lblReportType.Text = "Leads"                                'Display the Type of the Report
            End If

            Dim dtReportFields, dtCustomReportFields As DataTable           'declare a datatable to store the default fields and the custom fields resp.
            Dim drCustomReportFieldsCopy As DataRow                         'Declare a DataRow
            dtReportFields = objCustomReports.getColumnsForCustomReport(numReportOptionsTypeId) 'call to get the default fields
            If numReportOptionsTypeId = 1 And cbIncludeCustomFieldsOrg.Checked Then 'Custom fields are requested for Organization Report
                dtCustomReportFields = objCustomReports.getCustomColumnsForOrgContactCustomReport(ddlRelationShips.SelectedValue, ddlContactTypes.SelectedValue) 'Custom Fields for Org and Contact
                For Each drCustomReportFieldsCopy In dtCustomReportFields.Rows
                    dtReportFields.ImportRow(drCustomReportFieldsCopy)      'Import the answers rows
                Next
            ElseIf numReportOptionsTypeId = 2 And cbIncludeCustomFieldsOppItems.Checked Then 'Opportunity and Items Report and with Custom Fields inlcuded
                dtCustomReportFields = objCustomReports.getCustomColumnsForOppItemCustomReport() 'Custom Fields for Opportunity and Items
                For Each drCustomReportFieldsCopy In dtCustomReportFields.Rows
                    dtReportFields.ImportRow(drCustomReportFieldsCopy)      'Import the answers rows
                Next
            End If
            If numReportOptionsTypeId = 1 And rbIncludeContacts.Items(1).Selected Then 'If Include Contact is "No"
                Dim drColumnTableRows As DataRow                            'Declare a Datarow
                For Each drColumnTableRows In dtReportFields.Select("numFieldGroupID = 2") 'Filter all contacts details
                    dtReportFields.Rows.Remove(drColumnTableRows)           'Remove the rows as these are not used
                Next
            End If
            trContactTypes.Attributes.Remove("style")
            If rbIncludeContacts.Items(1).Selected Then                     'If Show Contact is Now selected
                trContactTypes.Attributes.Add("style", "display:none")      'Hide the Contact Types
            Else : trContactTypes.Attributes.Add("style", "display:inline")    'Make the Contact Types visible
            End If

            callToBindReportFields(dtReportFields)                          'Call to bind the columns to the list boxes
            tblReportParameters.Visible = True                              'Make the table visible
            callToBindAggeragateColumns(dtReportFields)                     'Call to bind the datagrid of aggeregated columns
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to bind the columns to the fieldgroups drop down
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/28/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindReportFields(ByVal dtReportFields As DataTable)
        Try
            Dim dvReportFieldsOrder, dvReportFieldsGroup1, dvReportFieldsGroup2, dvReportFieldsGroup3, dvReportFieldsGroup4, dvReportFieldsGroup5 As DataView 'declare a databviews for each of the list box
            dvReportFieldsGroup1 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup2 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup3 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup4 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsGroup5 = dtReportFields.DefaultView                           'get the default view
            dvReportFieldsOrder = dtReportFields.DefaultView                            'get the default view

            dvReportFieldsGroup1.RowFilter = "numFieldGroupID = 1 And boolDefaultColumnSelection = 0" 'set the rowfilter to only Organization Details
            If dvReportFieldsGroup1.Count > 0 Then
                dvReportFieldsGroup1.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup1.DataSource = dvReportFieldsGroup1                 'set a datashource 
                ddlReportColumnGroup1.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                ddlReportColumnGroup1.DataValueField = "vcDbFieldName"                  'set the data text format
                ddlReportColumnGroup1.DataBind()                                        'databind to display the data
                tblFieldsGroup1.Visible = True                                          'Display the Row

                dvReportFieldsGroup1.RowFilter = "numFieldGroupID = 1 And boolDefaultColumnSelection = 1" 'set the rowfilter to only Organization Details
                dvReportFieldsGroup1.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup1Sel.DataSource = dvReportFieldsGroup1              'set a datashource 
                ddlReportColumnGroup1Sel.DataTextField = "vcScrFieldName"               'set the text for the listbox
                ddlReportColumnGroup1Sel.DataValueField = "vcDbFieldName"               'set the data text format
                ddlReportColumnGroup1Sel.DataBind()                                     'databind to display the data
            Else : tblFieldsGroup1.Visible = False                                         'Hide the Row
            End If

            dvReportFieldsGroup2.RowFilter = "numFieldGroupID = 2 And boolDefaultColumnSelection = 0" 'set the rowfilter to only Contact Details
            If dvReportFieldsGroup2.Count > 0 Then
                dvReportFieldsGroup2.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup2.DataSource = dvReportFieldsGroup2                 'set a datashource 
                ddlReportColumnGroup2.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                ddlReportColumnGroup2.DataValueField = "vcDbFieldName"                  'set the data text format
                ddlReportColumnGroup2.DataBind()                                        'databind to display the data
                tblFieldsGroup2.Visible = True                                          'Display the Row

                dvReportFieldsGroup2.RowFilter = "numFieldGroupID = 2 And boolDefaultColumnSelection = 1" 'set the rowfilter to only Organization Details
                dvReportFieldsGroup2.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup2Sel.DataSource = dvReportFieldsGroup2              'set a datashource 
                ddlReportColumnGroup2Sel.DataTextField = "vcScrFieldName"               'set the text for the listbox
                ddlReportColumnGroup2Sel.DataValueField = "vcDbFieldName"               'set the data text format
                ddlReportColumnGroup2Sel.DataBind()                                     'databind to display the data
            Else : tblFieldsGroup2.Visible = False                                         'Hide the Row
            End If

            dvReportFieldsGroup3.RowFilter = "numFieldGroupID = 3 And boolDefaultColumnSelection = 0" 'set the rowfilter to only Opportunity Details
            If dvReportFieldsGroup3.Count > 0 Then
                dvReportFieldsGroup3.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup3.DataSource = dvReportFieldsGroup3                 'set a datashource 
                ddlReportColumnGroup3.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                ddlReportColumnGroup3.DataValueField = "vcDbFieldName"                  'set the data text format
                ddlReportColumnGroup3.DataBind()                                        'databind to display the data
                tblFieldsGroup3.Visible = True                                          'Display the Row

                dvReportFieldsGroup3.RowFilter = "numFieldGroupID = 3 And boolDefaultColumnSelection = 1" 'set the rowfilter to only Organization Details
                dvReportFieldsGroup3.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup3Sel.DataSource = dvReportFieldsGroup3              'set a datashource 
                ddlReportColumnGroup3Sel.DataTextField = "vcScrFieldName"               'set the text for the listbox
                ddlReportColumnGroup3Sel.DataValueField = "vcDbFieldName"               'set the data text format
                ddlReportColumnGroup3Sel.DataBind()                                     'databind to display the data
            Else : tblFieldsGroup3.Visible = False                                         'Hide the Row
            End If

            dvReportFieldsGroup4.RowFilter = "numFieldGroupID = 4 And boolDefaultColumnSelection = 0" 'set the rowfilter to only Item Details
            If dvReportFieldsGroup4.Count > 0 Then
                dvReportFieldsGroup4.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup4.DataSource = dvReportFieldsGroup4                 'set a datashource 
                ddlReportColumnGroup4.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                ddlReportColumnGroup4.DataValueField = "vcDbFieldName"                  'set the data text format
                ddlReportColumnGroup4.DataBind()                                        'databind to display the data
                tblFieldsGroup4.Visible = True                                          'Display the Row

                dvReportFieldsGroup4.RowFilter = "numFieldGroupID = 4 And boolDefaultColumnSelection = 1" 'set the rowfilter to only Organization Details
                dvReportFieldsGroup4.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup4Sel.DataSource = dvReportFieldsGroup4              'set a datashource 
                ddlReportColumnGroup4Sel.DataTextField = "vcScrFieldName"               'set the text for the listbox
                ddlReportColumnGroup4Sel.DataValueField = "vcDbFieldName"               'set the data text format
                ddlReportColumnGroup4Sel.DataBind()                                     'databind to display the data
            Else : tblFieldsGroup4.Visible = False                                         'Hide the Row
            End If

            dvReportFieldsGroup5.RowFilter = "numFieldGroupID = 5 And boolDefaultColumnSelection = 0" 'set the rowfilter to only Lead Details
            If dvReportFieldsGroup5.Count > 0 Then
                dvReportFieldsGroup5.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup5.DataSource = dvReportFieldsGroup5                 'set a datashource 
                ddlReportColumnGroup5.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                ddlReportColumnGroup5.DataValueField = "vcDbFieldName"                  'set the data text format
                ddlReportColumnGroup5.DataBind()                                        'databind to display the data
                tblFieldsGroup5.Visible = True                                          'Display the Row

                dvReportFieldsGroup5.RowFilter = "numFieldGroupID = 5 And boolDefaultColumnSelection = 1" 'set the rowfilter to only Organization Details
                dvReportFieldsGroup5.Sort = "numOrderAppearance asc"                    'set the sort
                ddlReportColumnGroup5Sel.DataSource = dvReportFieldsGroup5              'set a datashource 
                ddlReportColumnGroup5Sel.DataTextField = "vcScrFieldName"               'set the text for the listbox
                ddlReportColumnGroup5Sel.DataValueField = "vcDbFieldName"               'set the data text format
                ddlReportColumnGroup5Sel.DataBind()                                     'databind to display the data
            Else : tblFieldsGroup5.Visible = False                                         'Hide the Row
            End If

            dvReportFieldsOrder.RowFilter = "boolDefaultColumnSelection = 1" 'Set the order columns row filter
            dvReportFieldsOrder.Sort = "numFieldGroupID, numOrderAppearance asc"        'Set the Order of display
            lstReportColumnsOrder.DataSource = dvReportFieldsGroup5                     'set a datashource 
            lstReportColumnsOrder.DataTextField = "vcScrFieldName"                      'set the text for the listbox
            lstReportColumnsOrder.DataValueField = "vcDbFieldName"                      'set the data text format
            lstReportColumnsOrder.DataBind()                                            'databind to display the data

            ddlField1.DataSource = dvReportFieldsGroup5                                 'set a datashource 
            ddlField1.DataTextField = "vcScrFieldName"                                  'set the text for the listbox
            ddlField1.DataValueField = "vcDbFieldName"                                  'set the data text format
            ddlField1.DataBind()                                                        'databind to display the data
            ddlField1.Items.Insert(0, New ListItem("--None--", "None"))

            ddlField2.DataSource = dvReportFieldsGroup5                                 'set a datashource 
            ddlField2.DataTextField = "vcScrFieldName"                                  'set the text for the listbox
            ddlField2.DataValueField = "vcDbFieldName"                                  'set the data text format
            ddlField2.DataBind()                                                        'databind to display the data
            ddlField2.Items.Insert(0, New ListItem("--None--", "None"))

            ddlField3.DataSource = dvReportFieldsGroup5                                 'set a datashource 
            ddlField3.DataTextField = "vcScrFieldName"                                  'set the text for the listbox
            ddlField3.DataValueField = "vcDbFieldName"                                  'set the data text format
            ddlField3.DataBind()                                                        'databind to display the data
            ddlField3.Items.Insert(0, New ListItem("--None--", "None"))

            ddlField4.DataSource = dvReportFieldsGroup5                                 'set a datashource 
            ddlField4.DataTextField = "vcScrFieldName"                                  'set the text for the listbox
            ddlField4.DataValueField = "vcDbFieldName"                                  'set the data text format
            ddlField4.DataBind()                                                        'databind to display the data
            ddlField4.Items.Insert(0, New ListItem("--None--", "None"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to bind the datagrid of aggeregated columns
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	10/28/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Function callToBindAggeragateColumns(ByVal dtReportFields As DataTable)
        Try
            Dim drRecordCountRow As DataRow                                             'Declare a new DataRow
            drRecordCountRow = dtReportFields.NewRow()                                  'Get a new row instance of the table
            drRecordCountRow.Item("vcDbFieldName") = "numRecordCount"                   'Set the row db column as the Record Count
            drRecordCountRow.Item("vcScrFieldName") = "Record Count"                    'Set the row's screen value as "Record Count"
            drRecordCountRow.Item("numFieldGroupID") = 0                                'Set the default field group
            drRecordCountRow.Item("boolSummationPossible") = 1                          'Summation is possible on this column
            dtReportFields.Rows.InsertAt(drRecordCountRow, 0)                           'Add the Record Count row to the datatable

            dtReportFields.Columns.Add(New DataColumn("vcColumnSum", System.Type.GetType("System.String"), "'<input Type=CheckBox id=cbSum' + [vcDbFieldName] + ' name=""' + [vcScrFieldName] + '"" value=' + [vcDbFieldName] + ' onClick=""javascript: SetColumnListToSummarize(this,' + [numFieldGroupID] + ');"">'")) 'Add a column which will contain the Summation checkbox
            dtReportFields.Columns.Add(New DataColumn("vcColumnAvg", System.Type.GetType("System.String"), "'<input Type=CheckBox id=cbAvg' + [vcDbFieldName] + ' name=' + [vcScrFieldName] + ' value=' + [vcDbFieldName] + ' onClick=""javascript: SetColumnListToSummarize(this,' + [numFieldGroupID] + ');"">'")) 'Add a column which will contain the Average checkbox
            dtReportFields.Columns.Add(New DataColumn("vcColumnLValue", System.Type.GetType("System.String"), "'<input Type=CheckBox id=cbLValue' + [vcDbFieldName] + ' name=' + [vcScrFieldName] + ' value=' + [vcDbFieldName] + ' onClick=""javascript: SetColumnListToSummarize(this,' + [numFieldGroupID] + ');"">'")) 'Add a column which will contain the Largest Value checkbox
            dtReportFields.Columns.Add(New DataColumn("vcColumnSValue", System.Type.GetType("System.String"), "'<input Type=CheckBox id=cbSValue' + [vcDbFieldName] + ' name=' + [vcScrFieldName] + ' value=' + [vcDbFieldName] + ' onClick=""javascript: SetColumnListToSummarize(this,' + [numFieldGroupID] + ');"">'")) 'Add a column which will contain the Smallest Value checkbox

            Dim dvReportAggegratedFields As DataView = dtReportFields.DefaultView       'Get the default view
            dvReportAggegratedFields.RowFilter = "boolSummationPossible = 1"            'Filter the columns those can be aggregated
            dgAggeregateColumns.DataSource = dvReportAggegratedFields                   'Set the source of the datagrid
            dgAggeregateColumns.DataBind()                                              'Databind the datagrid
            dgAggeregateColumns.Items(0).Cells(3).Text = ""                             'There should be no checkboxes for Avg of Record Count
            dgAggeregateColumns.Items(0).Cells(4).Text = ""                             'There should be no checkboxes for Largest Value of Record Count
            dgAggeregateColumns.Items(0).Cells(5).Text = ""                             'There should be no checkboxes for Smallest Value of Record Count
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is raised when the report is being saves as My Report
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	11/05/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Private Sub btnAddToMyReports_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToMyReports.Click
        Try
            Dim sCustRptOrderedColumns As String = hdCustomReportConfig.Value           'To contain the list of columns in the Generated Report
            objCustomReports.SaveCustomReportAsMyReport(sCustRptOrderedColumns)         'Save the xml config into the database
            objCustomReports.DeleteCustomReportConfigXMLfile()                          'Delete the existing Config XML file
            Dim numReportOptionsTypeId As Integer = hdReportType.Value                  'Set the Report Type from the hidden variable
            callToBindReportFields(numReportOptionsTypeId)                              'Call to display the Report Config Form
            litClientSideScript.Text = "<script language='javascript'>alert('The Report has been added to your \'My Reports\', you can proceed to create another custom report.');</script>" 'Attach client side script to the literal to close this window
            litClientSideScript.Visible = True                          'Make the Client Side Literal visible
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
