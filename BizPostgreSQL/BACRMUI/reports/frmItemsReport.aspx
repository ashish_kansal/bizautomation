<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmItemsReport.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmItemsReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Items Reports</title>
    <script>

        function OpenItemDetails(a, b) {
            window.open('../reports/frmTopItemsSoldDtls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&From=' + $("#ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate").val() + '&To=' + $("#ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate").val() + '&Type=' + b, '', 'toolbar=no,titlebar=no,left=300, top=100,width=700,height=400,scrollbars=yes,resizable=yes');
            return false;
        }
        function PrintIt() {
            $(".button").hide();
            $("#tbl1").hide();
            this.print();
            $(".button").show();
            $("#tbl1").show();
            return false;
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                      <label>
                        From
                    </label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" />
                    <label>
                        To
                    </label>
                    <BizCalendar:Calendar ID="calTo" runat="server" />
                    <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary"></asp:Button>
                    </div>
                </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                      <label>
                        Customer
                    </label>
                    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                    <label>
                        Sort By
                    </label>
                    <asp:DropDownList ID="ddlSortBy" runat="server" CssClass="form-control" AutoPostBack="true"
                                >
                                <asp:ListItem Value="0">Sales Order Amount</asp:ListItem>
                                <asp:ListItem Value="1">Units Sold</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
             <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnAddtoMyRtpList" runat="server" CssClass="btn btn-primary"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                             
                    <asp:LinkButton ID="btnExportToExcel" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary"><i class="fa fa-print"></i>&nbsp; Print</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                            
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       <asp:PostBackTrigger ControlID="btnPrint" />
                       <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Top Items Sold
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="Table9" Width="100%" Height="400" runat="server" GridLines="None"
        BorderColor="black" CssClass="aspTable" CellSpacing="0" CellPadding="0" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgItems" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" runat="server" BorderColor="white"
                    AutoGenerateColumns="False" AllowSorting="True">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcItemName" SortExpression="vcItemName" HeaderText="Item Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcModelID" SortExpression="vcModelID" HeaderText="Model Id"></asp:BoundColumn>
                        <asp:BoundColumn DataField="txtItemDesc" SortExpression="txtItemDesc" HeaderText="Description"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Type" SortExpression="Type" HeaderText="Type"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TotalItem" SortExpression="TotalItem" HeaderText="Units Sold"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Amount" SortExpression="Amount" DataFormatString="{0:#,##0.00}"
                            HeaderText="Amount"></asp:BoundColumn>
                        <asp:BoundColumn DataField="OnHand" SortExpression="OnHand" HeaderText="On Hand"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="OnOrder" SortExpression="OnOrder" HeaderText="On Order"></asp:ButtonColumn>
                        <asp:ButtonColumn DataTextField="BackOrder" SortExpression="BackOrder" HeaderText="Backorder"></asp:ButtonColumn>
                        <asp:ButtonColumn DataTextField="Allocation" SortExpression="Allocation" HeaderText="On Allocation"></asp:ButtonColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
