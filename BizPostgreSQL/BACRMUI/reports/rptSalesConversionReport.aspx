﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptSalesConversionReport.aspx.vb"
    Inherits=".rptSalesConversionReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Sales Conversion Performance : Leads / Prospects converted to Accounts</title>
    <script language="javascript">
        function OpenSalesConversion(CId, Type) {
            //            window.open("../Reports/rptSalesConversionReportDetail.aspx?CId=" + CId + "&SD=" + document.getElementById('calFrom_txtDate').value
            //                 + "&ED=" + document.getElementById('calTo_txtDate').value + "&RT=" + document.getElementById('ddlFilter').options[document.getElementById('ddlFilter').selectedIndex].value + "&Type=" + Type, '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=500,scrollbars=yes,resizable=yes')

            window.open("../Reports/rptSalesConversionReportDetail.aspx?CId=" + CId + "&SD=" + $('#ctl00_ctl00_MainContent_FiltersAndViews_calFrom_txtDate').val()
                 + "&ED=" + $('#ctl00_ctl00_MainContent_FiltersAndViews_calTo_txtDate').val() + "&RT=" + document.form1.ctl00_ctl00_MainContent_FiltersAndViews_ddlFilter.options[document.form1.ctl00_ctl00_MainContent_FiltersAndViews_ddlFilter.selectedIndex].value + "&Type=" + Type, '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenSelTeam(repNo) {

            window.open("../Forecasting/frmSelectTeams.aspx?Type=" + repNo, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnGo.click()
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="AutoID">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="form-inline">
            <label>From</label>
            <BizCalendar:Calendar ID="calFrom" runat="server" />
            <label>To</label>
            <BizCalendar:Calendar ID="calTo" runat="server" />
            <asp:Button ID="btnGo" runat="server" CssClass="btn btn-primary" Text="Go" ClientIDMode="Static" />
            <label>For records where</label>
                <asp:DropDownList ID="ddlFilter" CssClass="form-control" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="1">Employee is the Assignee </asp:ListItem>
                    <asp:ListItem Value="2" Selected="True">Employee is the Owner</asp:ListItem>
                </asp:DropDownList>
               
            <div class="pull-right">
                <asp:UpdatePanel ID="updateprogress1" runat="server" >
                    <ContentTemplate>
                        <asp:LinkButton ID="btnAddtoMyRtpList" CssClass="btn btn-primary" runat="server"><i  class="fa fa-plus-circle"></i>&nbsp; Add To My Reports</asp:LinkButton>
                <asp:LinkButton ID="btnChooseTeams" CssClass="btn btn-primary" runat="server">Choose Teams</asp:LinkButton>
                <asp:LinkButton ID="btnExportToExcel" CssClass="btn btn-primary" runat="server" ><i class="fa fa-file-excel-o"></i>&nbsp; Export to Excel</asp:LinkButton>
                    </ContentTemplate>
                   <Triggers>
                       <asp:PostBackTrigger ControlID="btnAddtoMyRtpList" />
                       <asp:PostBackTrigger ControlID="btnChooseTeams" />
                       
                       <asp:PostBackTrigger ControlID="btnExportToExcel" />
                       
                   </Triggers>
                </asp:UpdatePanel>
            </div>
                 </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <strong>Total conversion % Avg:</strong>
                <asp:Label ID="lblTotalConversion" runat="server"></asp:Label>&nbsp;
                <strong>Avg. # Leads/Prospects Per Day:</strong>
                <asp:Label ID="lblLeadPerDay" runat="server"></asp:Label>&nbsp;
                <%--<strong>Lead/Prospects Req. to make 1 Sale:</strong> <asp:Label ID="lblLeadToSale" runat="server"></asp:Label><br />--%>
                <strong>Avg. Income per 1st Conversion:</strong>
                <asp:Label ID="lblAvg1stIncome" runat="server"></asp:Label>&nbsp;
                <strong>Avg. Total Income per Conversion:</strong>
                <asp:Label ID="lblIncome" runat="server"></asp:Label>
        </div>
    </div>

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Sales Conversion Performance
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:Table ID="Table9" Width="100%" runat="server" Height="350" GridLines="None"
        BorderColor="black" CssClass="aspTable" BorderWidth="0" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvSalesConversion" runat="server" AutoGenerateColumns="False" CssClass="table table-responsive table-bordered tblDataGrid"
                    Width="100%" DataKeyNames="numContactId">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:BoundField HeaderText="Employee Name" DataField="EmployeeName" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Leads / Prospects" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="javascript:void(0);" onclick="OpenSalesConversion('<%# Eval("numContactId") %>','2');">
                                    <%# Eval("TotalLP")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Conversion %" DataField="TotalLPConvertedPer" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Tot Number Converted" DataField="TotalLPConverted" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Avg. days to convert" DataField="AvgDayConvert" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Avg. Income 1st  conversion" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# String.Format("{0:##,#00.00}", Eval("AvgFirstSales"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="All Sales Orders" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="javascript:void(0);" onclick="OpenSalesConversion('<%# Eval("numContactId") %>','3');">
                                    <%# String.Format("{0:##,#00.00}", Eval("TotalSales"))%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
        </div>
</asp:Content>
