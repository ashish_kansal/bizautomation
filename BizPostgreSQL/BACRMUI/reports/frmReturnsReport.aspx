﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmReturnsReport.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits="BACRM.UserInterface.Opportunities.frmReturnsReport"
    EnableViewState="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Sales Returns </title>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                   <asp:RadioButtonList ID="rblReturnType" runat="server" CssClass="normal1" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Sales Return" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Purchase Return" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
                </div>
            </div>
             <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                    <asp:LinkButton ID="btnExport" runat="server"
                                        CssClass="btn btn-primary"><i class="fa fa-file-excel-o"></i>&nbsp; Export</asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-primary"><i class="fa fa-arrow-left"></i>&nbsp; Back</asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                       <asp:PostBackTrigger ControlID="btnExport" />
                            <asp:PostBackTrigger ControlID="btnBack" />
                   </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Sales Returns
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
   &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="3" valign="top">
                <asp:Table ID="table3" Width="100%" runat="server" Height="350" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataGrid ID="dgSalesReturns" runat="server" AutoGenerateColumns="False" CssClass="table table-responsive table-bordered tblDataGrid"
                                Width="100%" AllowSorting="True">
                                <Columns>
                                    <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName" ReadOnly="true" />
                                    <asp:BoundColumn HeaderText="Model ID" DataField="vcModelID" ReadOnly="true" />
                                    <asp:BoundColumn HeaderText="Item Description" DataField="ItemDesc" ReadOnly="true"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundColumn HeaderText="Number Sold/Purchased" DataField="Quantity" ReadOnly="true"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundColumn HeaderText="Amount Sold/Purchased" DataField="AmountSold" ReadOnly="true"
                                        DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundColumn HeaderText="Number Returned" DataField="quantityreturned" ReadOnly="true"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundColumn HeaderText="Amount Returned" DataField="AmountReturned" ReadOnly="true"
                                        DataFormatString="{0:0.##}" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundColumn HeaderText="Return %" DataField="ReturnPercent" ReadOnly="true"
                                        ItemStyle-HorizontalAlign="Center" DataFormatString="{0:0.##}" />
                                </Columns>
                                <AlternatingItemStyle CssClass="ais" />
                                <ItemStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
        </div>
</asp:Content>
