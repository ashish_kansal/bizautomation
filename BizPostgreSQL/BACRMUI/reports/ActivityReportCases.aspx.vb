Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Public Class ActivityReportCases : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCurrrentPage As System.Web.UI.WebControls.TextBox
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents dgCases As System.Web.UI.WebControls.DataGrid
        Protected WithEvents txtTotalPage As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtTotalRecords As System.Web.UI.WebControls.TextBox
        Dim strColumn As String

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                     ' = "Reports"
                    txtCurrrentPage.Text = 1
                    BindDatagrid()
                End If
                'If txtSortChar.Text <> "" Then
                '    BindDatagrid()
                '    txtSortChar.Text = ""
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                Dim SortChar As Char
                Dim dtLeads As DataTable
                Dim objLeads As New CLeads
                With objLeads
                    .UserCntID = IIf(GetQueryStringVal( "UserCntID") <> "", GetQueryStringVal( "UserCntID"), Session("UserContactID"))
                    .TeamType = 5
                    .DomainID = Session("DomainID")
                    .RepType = GetQueryStringVal( "type")
                    .Condition = GetQueryStringVal( "RepType").Trim
                    .startDate = GetQueryStringVal( "stDate")
                    .endDate = GetQueryStringVal( "enDate")

                    'If txtCurrrentPage.Text.Trim <> "" Then
                    '    .CurrentPage = txtCurrrentPage.Text
                    'Else : .CurrentPage = 1
                    'End If
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()


                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If strColumn <> "" Then
                        .columnName = strColumn
                    Else : .columnName = "cs.bintcreateddate"
                    End If
                    If Session("Asc") = 1 Then
                        .columnSortOrder = "Asc"
                    Else : .columnSortOrder = "Desc"
                    End If
                End With
                dtLeads = objLeads.GetPerAnaysCaseList
                If objLeads.TotalRecords = 0 Then
                    'hidenav.Visible = False
                    'lblRecordCountCases.Text = 0
                Else
                    'hidenav.Visible = True
                    'lblRecordCountCases.Text = String.Format("{0:#,###}", objLeads.TotalRecords)
                    'Dim strTotalPage As String()
                    'Dim decTotalPage As Decimal
                    'decTotalPage = lblRecordCountCases.Text / Session("PagingRows")
                    'decTotalPage = Math.Round(decTotalPage, 2)
                    'strTotalPage = CStr(decTotalPage).Split(".")
                    'If (lblRecordCountCases.Text Mod Session("PagingRows")) = 0 Then
                    '    lblTotal.Text = strTotalPage(0)
                    '    txtTotalPage.Text = strTotalPage(0)
                    'Else
                    '    lblTotal.Text = strTotalPage(0) + 1
                    '    txtTotalPage.Text = strTotalPage(0) + 1
                    'End If
                    'txtTotalRecords.Text = lblRecordCountCases.Text
                End If

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objLeads.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                dgCases.DataSource = dtLeads
                dgCases.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgCases_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCases.SortCommand
            Try
                strColumn = e.SortExpression.ToString()
                If Session("Column") <> strColumn Then
                    Session("Column") = strColumn
                    Session("Asc") = 0
                Else
                    If Session("Asc") = 0 Then
                        Session("Asc") = 1
                    Else : Session("Asc") = 0
                    End If
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace