Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Prospects

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmCustRptScheduler : Inherits BACRMPage

        Dim objCustReport As New CustomReports
        Dim lintScheduleId As Integer = 0
        Dim ReportId As Integer = 0

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If GetQueryStringVal("ScheduleId") <> "" Then
                    lintScheduleId = GetQueryStringVal("ScheduleId")
                Else : lintScheduleId = CInt(txtScheduleId.Text)
                End If
                ReportId = GetQueryStringVal( "ReportId")
                If Not IsPostBack Then
                    
                    LoadDropDown()
                    If Not ddlCustRpt.Items.FindByValue(ReportId) Is Nothing Then
                        ddlCustRpt.ClearSelection()
                        ddlCustRpt.Items.FindByValue(ReportId).Selected = True
                    End If
                    LoadSavedInformation()
                End If
                txtDays.Attributes.Add("onkeypress", "CheckNumber(2)")
                txtMonthlyDays.Attributes.Add("onkeypress", "CheckNumber(2)")
                txtMonthlyWeekDays.Attributes.Add("onkeypress", "CheckNumber(2)")
                btnSave.Attributes.Add("onclick", "return Save()")
                btnAddEmployee.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDropDown()
            Try
                objCustReport.UserCntID = Session("UserContactId")
                objCustReport.DomainID = Session("DomainId")
                ddlCustRpt.DataSource = objCustReport.getCustomReportUser()
                ddlCustRpt.DataTextField = "vcReportName"
                ddlCustRpt.DataValueField = "numReportId"
                ddlCustRpt.DataBind()
                Dim listitem As New ListItem
                listitem.Text = "--SelectOne--"
                listitem.Value = "0"
                ddlCustRpt.Items.Insert(0, listitem)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LoadSavedInformation()
            Dim dtScheduleDtl As DataTable
            Try
                objCustReport.ScheduleId = lintScheduleId
                dtScheduleDtl = objCustReport.GetSchedulerDtl
                If dtScheduleDtl.Rows.Count > 0 Then
                    txtTemplateName.Text = dtScheduleDtl.Rows(0)("varScheduleName")
                    If dtScheduleDtl.Rows(0)("chrIntervalType") = "D" Then
                        rdlRecurringTemplate.Items(0).Selected = True
                        txtDays.Text = dtScheduleDtl.Rows(0)("tintIntervalDays")
                    ElseIf dtScheduleDtl.Rows(0)("chrIntervalType") = "M" Then
                        rdlRecurringTemplate.Items(1).Selected = True

                        If dtScheduleDtl.Rows(0)("tintMonthlyType") = 0 Then
                            rdbMonthlyDays.Checked = True
                            ''ddlMonthlyDays.SelectedItem.Value = dtScheduleDtl.Rows(0)("tintFirstDet")
                            If Not ddlMonthlyDays.Items.FindByValue(dtScheduleDtl.Rows(0)("tintFirstDet")) Is Nothing Then
                                ddlMonthlyDays.Items.FindByValue(dtScheduleDtl.Rows(0)("tintFirstDet")).Selected = True
                            End If
                            txtMonthlyDays.Text = dtScheduleDtl.Rows(0)("tintIntervalDays")
                            'pnlMonthly.Visible = True
                            'pnlDaily.Visible = False
                            'pnlYearly.Visible = False
                        ElseIf dtScheduleDtl.Rows(0)("tintMonthlyType") = 1 Then
                            rdbMonthlyWeekDays.Checked = True
                            If Not ddlMonthlyWeekDays.Items.FindByValue(dtScheduleDtl.Rows(0)("tintFirstDet")) Is Nothing Then
                                ddlMonthlyWeekDays.Items.FindByValue(dtScheduleDtl.Rows(0)("tintFirstDet")).Selected = True
                            End If

                            txtMonthlyWeekDays.Text = dtScheduleDtl.Rows(0)("tintIntervalDays")
                            ''ddlMonthlyWeek.SelectedItem.Value = dtScheduleDtl.Rows(0)("tintWeekDays")
                            If Not ddlMonthlyWeek.Items.FindByValue(dtScheduleDtl.Rows(0)("tintWeekDays")) Is Nothing Then
                                ddlMonthlyWeek.Items.FindByValue(dtScheduleDtl.Rows(0)("tintWeekDays")).Selected = True
                            End If
                        End If
                    ElseIf dtScheduleDtl.Rows(0)("chrIntervalType") = "Y" Then
                        rdlRecurringTemplate.Items(2).Selected = True
                        ''ddlYearlyMonth.SelectedItem.Value = dtScheduleDtl.Rows(0)("tintMonths")
                        If Not ddlYearlyMonth.Items.FindByValue(dtScheduleDtl.Rows(0)("tintMonths")) Is Nothing Then
                            ddlYearlyMonth.Items.FindByValue(dtScheduleDtl.Rows(0)("tintMonths")).Selected = True
                        End If
                        If Not ddlYearlyDays.Items.FindByValue(dtScheduleDtl.Rows(0)("tintFirstDet")) Is Nothing Then
                            ddlYearlyDays.Items.FindByValue(dtScheduleDtl.Rows(0)("tintFirstDet")).Selected = True
                        End If
                    End If
                    CalStartDate.SelectedDate = dtScheduleDtl.Rows(0)("dtStartDate")

                    If dtScheduleDtl.Rows(0)("bitEndType") = 1 Then
                        rdlNoEndDate.Checked = True
                    ElseIf dtScheduleDtl.Rows(0)("bitEndType") = 0 Then
                        rdlStopAfter.Checked = True
                        If dtScheduleDtl.Rows(0)("bitEndTransactionType") = 0 Then
                            rdlEndDate.Checked = True
                            CalEndDate.SelectedDate = dtScheduleDtl.Rows(0)("dtEndDate")
                        Else
                            rdlNoTransaction.Checked = True
                            txtNoTransactions.Text = dtScheduleDtl.Rows(0)("numNoTransaction")
                        End If
                    End If
                End If
                GetContacts()
                BindContactGrid()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GetContacts()
            Try
                With objCustReport
                    .DomainID = Session("DomainId")
                    .ScheduleId = lintScheduleId
                    .DivisionID = 1
                End With
                ddlEmployeeList.DataSource = objCustReport.GetContactInfo()
                ddlEmployeeList.DataTextField = "Name"
                ddlEmployeeList.DataValueField = "numContactID"
                ddlEmployeeList.DataBind()
                Dim item As New ListItem
                item.Text = "--SelectOne--"
                item.Value = 0
                ddlEmployeeList.Items.Insert(0, item)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindContactGrid()
            Try
                With objCustReport
                    .ContactID = ddlEmployeeList.SelectedValue
                    .ScheduleId = lintScheduleId
                    .GetContactScheduled()
                End With
                Dim dtTable As DataTable
                dtTable = objCustReport.GetContactScheduled()
                Session("dtSchConGTable") = dtTable
                dgEmployee.DataSource = dtTable
                dgEmployee.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                save()
                Response.Write("<script>window.opener.Bind();window.close()</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub save()
            Try
                With objCustReport
                    .ReportID = ddlCustRpt.SelectedValue
                    .ScheduleId = lintScheduleId
                    .ScheduleName = txtTemplateName.Text
                    .DomainID = Session("DomainId")
                    If rdlRecurringTemplate.SelectedValue = 0 Then
                        .IntervalType = "D"
                        .IntervalDays = txtDays.Text
                        ''pnlDaily.Style.visibility = "hidden"
                    ElseIf rdlRecurringTemplate.SelectedValue = 1 Then
                        .IntervalType = "M"
                        If rdbMonthlyDays.Checked = True Then
                            .MonthlyType = 0
                            .FirstDet = ddlMonthlyDays.SelectedItem.Value
                            .IntervalDays = txtMonthlyDays.Text
                        ElseIf rdbMonthlyWeekDays.Checked = True Then
                            .MonthlyType = 1
                            .FirstDet = ddlMonthlyWeekDays.SelectedItem.Value
                            .IntervalDays = txtMonthlyWeekDays.Text
                            .WeekDays = ddlMonthlyWeek.SelectedItem.Value
                        End If
                    ElseIf rdlRecurringTemplate.SelectedValue = 2 Then
                        .IntervalType = "Y"
                        .Months = ddlYearlyMonth.SelectedItem.Value
                        .FirstDet = ddlYearlyDays.SelectedItem.Value
                    End If
                    .dtStartDate = CalStartDate.SelectedDate

                    If rdlNoEndDate.Checked = True Then
                        .EndType = 1
                    ElseIf rdlEndDate.Checked = True Then
                        .dtEndDate = CalEndDate.SelectedDate
                        .EndType = 0
                        .EndTransactionType = 0
                    ElseIf rdlNoTransaction.Checked = True Then
                        .EndTransactionType = 1
                        .NoTransaction = txtNoTransactions.Text
                    End If
                    lintScheduleId = .SaveScheduler()
                    txtScheduleId.Text = lintScheduleId
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ClearAllDatas()
            Try
                txtTemplateName.Text = ""
                txtDays.Text = "1"
                txtMonthlyDays.Text = "1"
                txtMonthlyWeekDays.Text = "1"
                CalStartDate.SelectedDate = ""
                CalEndDate.SelectedDate = ""
                rdlNoEndDate.Checked = True
                rdlRecurringTemplate.Items(0).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAddEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEmployee.Click
            Try
                If ddlEmployeeList.SelectedValue <> 0 Then
                    If lintScheduleId = 0 Then save()
                    With objCustReport
                        .ContactID = ddlEmployeeList.SelectedValue
                        .ScheduleId = lintScheduleId
                        .AddContactScheduled()
                    End With
                    GetContacts()
                    BindContactGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmployee_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmployee.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    With objCustReport
                        .ContactID = CLng(e.Item.Cells(0).Text)
                        .ScheduleId = lintScheduleId
                        .DelContactScheduled()
                    End With
                    GetContacts()
                    BindContactGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgEmployee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEmployee.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hplEmail As HyperLink
                    hplEmail = e.Item.FindControl("hplEmail")
                    hplEmail.NavigateUrl = "#"
                    If Session("CompWindow") = 1 Then
                        hplEmail.NavigateUrl = "mailto:" & hplEmail.Text
                    Else : hplEmail.Attributes.Add("onclick", "return OpemEmail('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=" & hplEmail.Text & "&ContID=" & e.Item.Cells(0).Text & "')")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace