Imports System.Data
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports System.Configuration

Namespace BACRM.UserInterface.Reports
    Partial Public Class frmAvgSalesCycle1Print : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                BindDatagrid()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                lblreportby.Text = Session("UserName")
                lblcurrent.Text = FormattedDateFromDate(Now(), Session("DateFormat"))
                'Set teh dates to a range of one week ending today.
                lbltodt.Text = FormattedDateFromDate(GetQueryStringVal( "todt"), Session("DateFormat"))
                lblfromdt.Text = FormattedDateFromDate(GetQueryStringVal( "fdt"), Session("DateFormat"))

                'Dim dtLeads As New DataTable
                Dim dtSales As DataTable
                Dim objSales As New AvgSalesCycle

                objSales.FromDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lblfromdt.Text))
                objSales.ToDate = DateAdd(DateInterval.Minute, Session("ClientMachineUTCTimeOffset"), CDate(lbltodt.Text))

                Dim SortChar As Char
                If GetQueryStringVal( "SortChar") <> "" Then
                    SortChar = GetQueryStringVal( "SortChar")
                Else : SortChar = "0"
                End If
                With objSales
                    Select Case Integer.Parse(GetQueryStringVal( "rdlReportType"))
                        Case 0 : objSales.UserRights = 1
                        Case 1 : objSales.UserRights = 2
                        Case 2 : objSales.UserRights = 3
                    End Select
                    .UserCntID = Session("UserContactID")
                    .TeamMemID = GetQueryStringVal( "TeamMemID")
                    .ListType = IIf(GetQueryStringVal( "ListType") = True, 0, 1)
                    .TeamType = 31
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If (GetQueryStringVal( "Sort") = "") Then
                        .Sort = 0
                    Else : .Sort = CInt(GetQueryStringVal( "Sort"))
                    End If

                    .DomainID = Session("DomainID")
                    If GetQueryStringVal( "Column") <> "" Then
                        .columnName = ViewState("Column")
                    Else : .columnName = "bintcreateddate"
                    End If
                    If Session("Asc") = 1 Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                End With
                dtSales = objSales.GetAvgSalesList
                If dtSales.Rows.Count > 0 Then
                    lbl1.Text = Format(dtSales.Compute("AVG(monPAmount)", String.Empty), "#,##0.00")
                    lbl2.Text = Format(dtSales.Compute("SUM(days)", String.Empty) / dtSales.Rows.Count, "#,##0.00")
                Else
                    lbl1.Text = "0.00"
                    lbl2.Text = "0.00"
                End If

                Dim dv As DataView = New DataView(dtSales)
                dv.Sort = objSales.columnName & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgAvgSales.DataSource = dv
                dgAvgSales.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
