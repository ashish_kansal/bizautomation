<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmActivityReport.aspx.vb"
    Inherits="BACRM.UserInterface.Reports.frmActivityReport" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Activity Report</title>
    <script language="javascript">
        function Back() {
            window.location.href = "../reports/reportslinks.aspx";
            return false;
        }
        function OpenSelTeam() {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=5", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenindRept(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,left=200, top=300,width=700,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.form1.btnMonthGo.click()
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <div class="form-inline">
            <asp:Button ID="btnAddtoMyRtpList" CssClass="button btn btn-primary" Text="Add To My Reports" runat="server">
                                    </asp:Button>
            <asp:Button ID="btnTeams" Text="Choose Teams" CssClass="button btn btn-primary" runat="server"></asp:Button>
            <asp:Button ID="btnBack" Text="Back" CssClass="button btn btn-primary" runat="server"></asp:Button>
        </div>
    </div>
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Performance Analysis
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="AutoID">
    <style>
        .tbnf .row {
            margin-bottom:12px;
        }
    </style>
    <div class="table-responsive">
    <asp:Table ID="tblOppr" runat="server" Width="100%" GridLines="None" CssClass="aspTable"
        CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell>
                <div class="col-md-8 tbnf col-md-offset-2">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-inline">
                               <label>Pick Month</label> 
                                <asp:DropDownList ID="ddlYear" runat="server"  CssClass="signup form-control">
                                            <asp:ListItem Value="0">--Year--</asp:ListItem>
                                        </asp:DropDownList>
                                 <asp:DropDownList ID="ddlMonth" runat="server" CssClass="signup form-control">
                                            <asp:ListItem Value="0">--Month--</asp:ListItem>
                                            <asp:ListItem Value="01">January</asp:ListItem>
                                            <asp:ListItem Value="02">February</asp:ListItem>
                                            <asp:ListItem Value="03">March</asp:ListItem>
                                            <asp:ListItem Value="04">April</asp:ListItem>
                                            <asp:ListItem Value="05">May</asp:ListItem>
                                            <asp:ListItem Value="06">June</asp:ListItem>
                                            <asp:ListItem Value="07">July</asp:ListItem>
                                            <asp:ListItem Value="08">August</asp:ListItem>
                                            <asp:ListItem Value="09">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList>
                                 <asp:Button ID="btnMonthGo" runat="server" CssClass="button btn btn-primary" Text="Go" ClientIDMode="Static">
                            </asp:Button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-inline">
                            <label>Selected Teams</label>
                            <asp:ListBox ID="lstTeam" runat="server"  CssClass="signup form-control" SelectionMode="Multiple" style="min-width:200px"></asp:ListBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-inline">
                                <label>Pick Day</label>
                                <BizCalendar:Calendar ID="calDay" runat="server" />
                                <asp:Button ID="btnDayGo" runat="server" CssClass="button btn btn-primary" Text="Go"></asp:Button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-inline">
                                <label>Pick Week Ending</label>
                                <BizCalendar:Calendar ID="calMonth" runat="server" />
                                <asp:Button ID="btnWeekGo" runat="server" CssClass="button btn btn-primary" Text="Go"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
                <table cellspacing="1" cellpadding="1" width="100%" border="0" class="table table-responsive table-bordered">
                    <tr>
                        <td class="normal1" align="right">
                            Opportunities Opened:
                        </td>
                        <td class="normal1" width="50">
                            <asp:Label ID="lblOppOpened" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkOppOpened" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplOppOpnCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                            <td class="normal1" align="right">
                                Total Number of Accounts Added:
                            </td>
                            <td class="normal1">
                                <asp:Label ID="lblTotAcc" runat="server"></asp:Label>
                            </td>
                            <td class="normal1">
                                <asp:HyperLink ID="lnkTotAcc" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                                &nbsp;
                                <asp:HyperLink ID="hplTotAccCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                            </td>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Amount:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblOppAmt" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Communications Scheduled:
                        </td>
                        <td class="normal1" width="50">
                            <asp:Label ID="lblComSch" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkComSch" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplComSchCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Deals Closed(Won):
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealCloseWon" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkDealCloseWon" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplDealCloseWonCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Communications Completed:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblComComp" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkComComp" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplComCompCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Amount:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealWonAmt" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Communications Past Due:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblComPast" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkComPast" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplComPastCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Deals Closed(Lost):
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealLost" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="hplDealLostLink" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplDealLostLinkCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Tasks Scheduled:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTaskSch" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTaskSch" runat="server" Target="_blank" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplTaskSchCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Amount:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblDealLostAmt" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Tasks Completed:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTaskComp" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTaskComp" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplTaskCompCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Conversion (Won vs Lost)%:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblConversion" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                        </td>
                        <td class="normal1" align="right">
                            Tasks Past Due:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTaskPast" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTaskPast" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplTaskPastCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Leads Promoted:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblLeadProm" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkLeadProm" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplLeadPromCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Cases Opened:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCaseOpen" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkCaseOpen" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplCaseOpenCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Total Prospects Added:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTotPros" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkTotPros" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplTotProsCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Cases Completed:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCaseComp" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkCaseComp" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplCaseCompCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Number of Prospects promoted to Accounts:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblNoPros" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkNoPros" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplNoProsCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Cases Past Due:
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblCasePast" runat="server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:HyperLink ID="lnkCasePast" Target="_blank" runat="server" CssClass="hyperlink">Open List</asp:HyperLink>
                            &nbsp;
                            <asp:HyperLink ID="hplCasePastCmp" Target="_blank" runat="server" CssClass="hyperlink">Compare</asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <br>
                <asp:Table ID="tblUsers" runat="server" Width="100%" GridLines="None">
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtUserId" runat="server" Style="display: none"></asp:TextBox>
        </div>
</asp:Content>
