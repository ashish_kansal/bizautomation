﻿Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Common

Public Class frmManageKPIReport
    Inherits BACRMPage

    Dim objReportManage As New CustomReportsManage
    Dim lngKPIGroupID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngKPIGroupID = CCommon.ToLong(GetQueryStringVal("KPIGroupID"))

            If Not IsPostBack Then
                If lngKPIGroupID > 0 Then
                    BindKPIGroup()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindKPIGroup()
        Try
            objReportManage = New CustomReportsManage
            objReportManage.ReportKPIGroupID = lngKPIGroupID
            objReportManage.DomainID = Session("DomainID")

            Dim dt As DataTable = objReportManage.GetReportKPIGroupListMaster

            If dt.Rows.Count = 1 Then
                txtReportName.Text = dt.Rows(0)("vcKPIGroupName")
                txtReportDescription.Text = dt.Rows(0)("vcKPIGroupDescription")

                If rdbType.Items.FindByValue(dt.Rows(0)("tintKPIGroupReportType")) IsNot Nothing Then
                    rdbType.ClearSelection()
                    rdbType.Items.FindByValue(dt.Rows(0)("tintKPIGroupReportType")).Selected = True
                End If
                rdbType.Enabled = False
                pnlStep2.Visible = True

                bidReportDropDown()
                bindKPIReport()
            Else
                Response.Redirect("../reports/frmKPIReportList.aspx")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub bidReportDropDown()
        Try
            objReportManage = New CustomReportsManage
            objReportManage.ReportKPIGroupID = lngKPIGroupID
            objReportManage.DomainID = Session("DomainID")
            objReportManage.tintMode = 2

            Dim dt As DataTable = objReportManage.GetReportKPIGroupListMaster

            ddlCustomReport.DataSource = dt
            ddlCustomReport.DataTextField = "vcReportName"
            ddlCustomReport.DataValueField = "numReportID"
            ddlCustomReport.DataBind()

            ddlCustomReport.Items.Insert(0, New ListItem("--Select--", 0))
            ddlCustomReport.ClearSelection()
            ddlCustomReport.Items.FindByValue("0").Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub bindKPIReport()
        Try
            objReportManage = New CustomReportsManage
            objReportManage.ReportKPIGroupID = lngKPIGroupID
            objReportManage.DomainID = Session("DomainID")
            objReportManage.tintMode = 1

            Dim dt As DataTable = objReportManage.GetReportKPIGroupListMaster

            gvReport.DataSource = dt
            gvReport.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            lngKPIGroupID = Save()
            If lngKPIGroupID > 0 Then
                Response.Redirect("../reports/frmManageKPIReport.aspx?KPIGroupID=" & lngKPIGroupID, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            lngKPIGroupID = Save()
            If lngKPIGroupID > 0 Then
                Response.Redirect("../reports/frmKPIReportList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function Save() As Long
        Try
            objReportManage = New CustomReportsManage
            objReportManage.ReportKPIGroupID = lngKPIGroupID
            objReportManage.DomainID = Session("DomainID")
            objReportManage.KPIGroupName = txtReportName.Text.Trim
            objReportManage.KPIGroupDescription = txtReportDescription.Text.Trim
            objReportManage.tintKPIGroupReportType = rdbType.SelectedValue
            objReportManage.ManageReportKPIGroupListMaster()

            Return objReportManage.ReportKPIGroupID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnAddReport_Click(sender As Object, e As EventArgs) Handles btnAddReport.Click
        Try
            objReportManage = New CustomReportsManage
            objReportManage.ReportKPIGroupID = lngKPIGroupID
            objReportManage.ReportID = ddlCustomReport.SelectedValue
            objReportManage.tintMode = 1
            objReportManage.ManageReportKPIGroupListMaster()

            bidReportDropDown()
            bindKPIReport()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvReport_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReport.RowCommand
        Try
            If e.CommandName = "DeleteReport" Then
                Dim gvRow As GridViewRow
                gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                Dim objReportManage As New CustomReportsManage
                objReportManage.ReportKPIGroupID = lngKPIGroupID
                objReportManage.ReportID = gvReport.DataKeys(gvRow.DataItemIndex).Value
                objReportManage.tintMode = 3
                objReportManage.ManageReportKPIGroupListMaster()

                bidReportDropDown()
                bindKPIReport()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class